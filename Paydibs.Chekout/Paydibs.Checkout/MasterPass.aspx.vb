﻿Imports LoggerII
Imports System.IO
Imports System
Imports System.Data
Imports System.Security.Cryptography
Imports Com.MasterCard.Masterpass.Merchant
Imports Com.MasterCard.Sdk

Public Class MasterPass
    Inherits System.Web.UI.Page

    Public objLoggerII As LoggerII.CLoggerII

    Private stHostInfo As Common.STHost
    Private stPayInfo As Common.STPayInfo

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim szReqToken As String
        Try
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
            DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "MasterPass-test-started")

            SetConfigurations()
            szReqToken = RequestToken(stHostInfo)
            ShoppingCartRequest(szReqToken)
            MerchantInit(szReqToken)

        Catch ex As Exception

        End Try

    End Sub

    '********************************************************************************************
    ' Class Name:		MasterPass
    ' Function Name:	SetConfigurations
    ' Function Type:	NONE
    ' Parameter:        
    ' Description:		Load MasterPass configuration setting
    ' History:			9 Sept 2016
    '********************************************************************************************
    Private Sub SetConfigurations()
        Dim szHostUrl As String = ""
        Dim szConsumerKey As String = ""
        Dim szKeystorePath As String = ""
        Dim szKeystorePassword As String = ""
        Dim bIsSandbox As Boolean = ""

        'Need to load consumerkey & password from terminal table

        szHostUrl = "https://sandbox.api.mastercard.com" 'ConfigurationManager.AppSettings("ApiHostUrl")
        szConsumerKey = "hf0th_Ycgv7Be3yZFHy_vTYtzE8DTdrEQv3QezMRb7f2fe4f!0c382e2b3de9499593f88dfb9e6df98f0000000000000000" 'Retrieve from db. stHostInfo/stPayInfo
        szKeystorePath = "Y:\IPG_SG_OoiMei\MasterPass\SBX_GHLTestKey_sandbox.p12" 'Server.MapPath(ConfigurationManager.AppSettings("KeystorePath"))
        szKeystorePassword = "pASSW0RD1" 'Retrieve from db. stHostInfo/stPayInfo
        bIsSandbox = "true" 'ConfigurationManager.AppSettings("IsSandbox")
        Dim privateKey = New X509Certificates.X509Certificate2(szKeystorePath, szKeystorePassword).PrivateKey

        Core.MasterCardApiConfiguration.Sandbox = bIsSandbox
        Core.MasterCardApiConfiguration.ConsumerKey = szConsumerKey
        Core.MasterCardApiConfiguration.PrivateKey = privateKey

    End Sub

    '********************************************************************************************
    ' Class Name:		MasterPass
    ' Function Name:	RequestToken
    ' Function Type:	NONE
    ' Parameter:        
    ' Description:		Request Token from MasterPass
    ' History:			14 Sept 2016
    '********************************************************************************************
    Private Function RequestToken(ByRef st_HostInfo As Common.STHost) As String
        Dim szResponseURL As String = ""
        Dim objReqTokenRes As Core.Model.RequestTokenResponse = New Core.Model.RequestTokenResponse
        Dim szToken As String

        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "TESTING-ReqToken start")
        szResponseURL = "https://test2pay.ghl.com/IPGSGOM/response_masterpass.aspx" 'st_HostInfo.szReturnURL
        objReqTokenRes = Core.Api.RequestTokenApi.Create(szResponseURL)
        szToken = objReqTokenRes.OauthToken
        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "TESTINg-token-" + szToken)

    End Function

    '********************************************************************************************
    ' Class Name:		MasterPass
    ' Function Name:	ShoppingCartRequest
    ' Function Type:	NONE
    ' Parameter:        
    ' Description:		Create Shopping Cart Request - Pass in ProdDesc/OrderNumber
    ' History:			14 Sept 2016
    '********************************************************************************************

    Private Sub ShoppingCartRequest(ByRef sz_ReqToken)
        Dim objShoppingCartReq As Model.ShoppingCartRequest = New Model.ShoppingCartRequest
        Dim objShoppingCart As Model.ShoppingCart = New Model.ShoppingCart
        Dim objShoppingCartItem As Model.ShoppingCartItem = New Model.ShoppingCartItem
        Dim objShoppingCartRes As Model.ShoppingCartResponse = New Model.ShoppingCartResponse

        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "TESTINg-shopcartreq")

        'objShoppingCartItem.WithImageURL("")
        objShoppingCartItem.WithValue(10100) 'TxnAmount as long value
        objShoppingCartItem.Description = "ProdDesc Testing"    'ProdDesc/OrderNumber
        objShoppingCartItem.WithQuantity(1) 'default to 1 as payment gateway not handle itemize

        objShoppingCart.WithSubtotal(10100) 'TxnAmount as long value
        objShoppingCart.WithCurrencyCode("MYR") 'CurrencyCode
        objShoppingCart.WithShoppingCartItem(objShoppingCartItem)

        objShoppingCartReq.WithShoppingCart(objShoppingCart)
        objShoppingCartReq.WithOAuthToken(sz_ReqToken)

        objShoppingCartRes = Api.ShoppingCartApi.Create(objShoppingCartReq)
        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "TESTINg-shopcartreq-end")

    End Sub

    '********************************************************************************************
    ' Class Name:		MasterPass
    ' Function Name:	MerchantInit
    ' Function Type:	NONE
    ' Parameter:        
    ' Description:		Create an instance of MerchantInitializationRequest
    ' History:			14 Sept 2016
    '********************************************************************************************
    Private Sub MerchantInit(ByRef sz_ReqToken)
        Dim objMerchantInitReq As Model.MerchantInitializationRequest = New Model.MerchantInitializationRequest
        Dim objMerchantInitRes As Model.MerchantInitializationResponse = New Model.MerchantInitializationResponse

        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "TESTINg-merchinit")
        objMerchantInitReq.OriginUrl = "https://test2pay.ghl.com/IPGSGOM/masterpass.aspx"  'st_HostInfo.szURL
        objMerchantInitReq.OAuthToken = sz_ReqToken

        objMerchantInitRes = Api.MerchantInitializationApi.Create(objMerchantInitReq)
        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "TESTINg-merchinit-end")

    End Sub

End Class