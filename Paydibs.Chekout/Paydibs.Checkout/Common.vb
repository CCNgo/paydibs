Imports LoggerII
Imports System.Text.RegularExpressions
Imports System.Xml      ' Added, 6 Mar 2015, for XmlReader in szGetTagValue
Imports System.IO       ' Added, 6 Mar 2015, for StringReader in szGetTagValue

'Module Common
Public Class Common

    Public Const C_TXN_SUCCESS_0 = "Payment Successful"
    Public Const C_TXN_FAILED_1 = "Payment Failed"
    Public Const C_TXN_PENDING_2 = "Payment Pending"                    ' Added, 22 Sept 2013
    Public Const C_TXN_REVERSED_9 = "Payment Reversed"                  ' Added, 22 Sept 2013
    Public Const C_TXN_EXPIRED_6 = "Payment Expired"                    ' Added, 22 Sept 2013
    Public Const C_COM_TIMEOUT_5 = "Timeout Out Failed"
    Public Const C_FAILED_HOTCARD_9 = "Hot Card / Hot Name"
    Public Const C_PENDING_RESP_30 = "Payment Pending For Response"
    Public Const C_QUERY_HOST_33 = "Querying Host"
    Public Const C_FAILED_TO_SEND_46 = "Payment Failed To Send" '"Out of Per Txn Amt Limit" '
    Public Const C_EXCEED_PER_TXN_AMT_LIMIT_50 = "Out of Per Txn Amt Limit" '"Txn Amt Limit Out Of Range (Min MYR 1.00 and Max MYR 30000.00)" '   'Added, 16 Jan 2014. Modified 6 Feb 2018, changed from "Exceeded" to "Out of"
    Public Const C_TXN_NOT_FOUND_21 = "Payment Not Exists"
    Public Const C_DB_UNKNOWN_ERROR_901 = "Failed to Connect DB"
    Public Const C_INVALID_HOST_902 = "Failed to Get Host Information"
    Public Const C_FAILED_SET_TXN_STATUS_905 = "Failed to Set Transaction Status"
    Public Const C_FAILED_STORE_TXN_906 = "Failed to Store Transaction"     ' Failed bInsertNewTxn
    Public Const C_FAILED_UPDATE_TXN_RESP_907 = "Failed to Update Transaction Response"
    Public Const C_FAILED_FORM_REQ_914 = "Failed to Form Message"
    Public Const C_FAILED_COM_INIT_917 = "Failed to Initiate Communications"
    Public Const C_INVALID_MID_2901 = "Invalid Merchant ID"                  ' Failed bGetMerchant in bInitPayment
    Public Const C_COMMON_INVALID_FIELD_2902 = "Invalid Field"
    Public Const C_COMMON_MISSING_ELEMENT_2903 = "Missing Required Field"   ' Failed bPaymentMain
    Public Const C_INVALID_INPUT_2906 = "Invalid Input"                     ' Failed bCheckUniqueTxn, bInitPayment
    Public Const C_INVALID_HOST_REPLY_2907 = "Invalid Host Reply"
    Public Const C_INVALID_SIGNATURE_2909 = "Invalid Hash Value"
    Public Const C_LATE_HOST_REPLY_2908 = "Late Host Reply"
    Public Const C_COMMON_UNKNOWN_ERROR_2999 = "Internal Error"             'Failed to GenTxnID, bInsertTxnResp, bCheckUniqueTxn, bInsertNewTxn, bProcessSale, bInitPayment
    Public Const C_DEFAULT_LANGUAGE_CODE = "EN"                                     'Added, 26 Jul 2013
    Public Const C_3DESAPPKEYENCP = "25F4C4263E05868016F3CECEBBF027DC3286B002016EF737D8F5362773A2D742"   'Added, 11 Oct 2017, card data key encrypted by KEK retrieved from DB-p
    Public Const C_3DESAPPKEYENCS = "67D7DC68CA3334ABC0FA785990457C696A65C2DCF92A01001974979C2DDE1D9A"  '"3537146182309002FEB7EBCE55ED3BBA71BED7CEB1D18CAA"  DB-stg
    Public Const C_3DESAPPKEY2 = "4648245271298991EDA6FCDF66FE4CCB60ADC6BDA0C09DBB" 'To encrypt/decrypt non card data
    Public Const C_3DESAPPVECTOR = "0000000000000000"                               'Added, 19 Nov 2013
    Public Const C_TXN_DECLINED_BY_FDS_400 = "Declined by ExtFDS Rules"                   'Added 18 apr 2016  fraudwall
    Public Const C_TXN_DECLINED_BY_FDS_299 = "Declined by ExtFDS Fraud Score"   'Added 18 apr 2016  fraudwall
    Public Const C_TXN_CANCELLED_17 = "Payment Cancelled At Payment Page"
    Public Const C_TXN_DECLINED_BY_FDS = "Declined by FDS Rules"                'Added by OM, 2 May 2019. Block Foreign Card.
    Public Const C_TXN_CC_ACQBANK = "Visa/MasterCard"                           'Added by OM, 7 May 2019. CC's AcqBank return value

    Public Shared szKEK As String           'Added, 3 Oct 2017
    Public Shared C_3DESAPPKEY As String    'Added, 11 Oct 2017

    Private objLoggerII As LoggerII.CLoggerII       'Added, 25 Nov 2013
    Private m_objTxnProc As TxnProc = Nothing       'Added, 24 Nov 2013
    Private m_objTxnProcOB As TxnProcOB = Nothing   'Added, 24 Nov 2013

    'Added , 30 May 2017, SOP
    Public Enum SOP_STATUS
        TOKEN_CREATED = 1
        TOKEN_SENT = 2
        TOKEN_USED = 3
    End Enum

    Public Enum TXN_STATUS
        TXN_SUCCESS = 0                     '1
        TXN_FAILED = 1                      '4
        TXN_PENDING = 2                     'Processing
        TXN_PENDING_CONFIRM_BOOKING = 3     'Host returned approved for the payment, pending OpenSkies AddPymt reply OR trying to send AddPymt request to OpenSkies, need extend booking
        TXN_TIMEOUT_HOST_RETURN = 4         'Host no response for 1 hour 45 minutes, need extend booking, stop and generate exception report
        TXN_TIMEOUT_HOST_UNPROCESSED = 5    'Host's reply status is "not processed" for 1 hour 45 minutes, no need extend booking, no need exception report
        TIME_OUT_FAILED = 6                 'Txn existed in Request table for more than 2 hours
        TXN_HOST_UNPROCESSED = 7            'Host's reply status is "not processed"
        TXN_HOST_RETURN_TIMEOUT = 8         'Host no response/timeout
        TXN_REVERSED = 9                    'Txn reversed successfully
        TXN_NOT_FOUND = -1                  'Added, 3 Sept 2013
        TXN_INT_ERR = -2                    'Added, 3 Sept 2013
        TXN_EXPIRED_IN_PYMTPAGE = -3        'Added  1 Nov 2017. Expired in Payment Page
        TXN_AUTH_SUCCESS = 15               'Added  14 April 2015. Auth&Capture. Txn status Auth Success returned to merchant in Query resp
        TXN_CAPTURE_SUCCESS = 16            'Added  14 April 2015. Auth&Capture. Txn status Capture Success returned to merchant in Query resp
        TXN_REFUND_SUCCESS = 10             'Added, 31 May 2015. Refund. Txn status Refund Success returned to merchant in Query resp
        TXN_CANCELLED = 17

        PENDING_RESP = 30
        PENDING_REVERSAL = 31
        PROCESSING_RESP = 32
        'QUERYING_HOST = 33                 'Commented  1 Dec 2015
        PENDING_REFUND = 33                 'Added  8 Jul 2015
        PENDING_CAPTURE = 34                'Added  5 Aug 2016

        FDS_BlOCKCARD = 41                  'Added by OM, 2 May 2019. Block Foreign Card. Txn STatus follow Blacklist Card Range
        EXCEED_PER_TXN_AMT_LIMIT = 50       'Added, 16 Jan 2014
        DECLINED_BY_EXT_FDS = 51            'Added, 12 Jul 2016, FraudWall
        DECLINED_BY_PROMOTION = 52          'Added 07 Oct 2016, Promotion
        DECLINED_BY_VEENROLL = 53
        DECLINED_BY_PA = 54

        E_FAILED_STORE_TXN = 906    ' Failed bInsertNewTxn
        E_INVALID_MERCHANT = 2901
        INVALID_INPUT = 2906        ' Failed GetData, bInitPayment, bPaymentMain
        INVALID_HOST_REPLY = 2907
        LATE_HOST_REPLY = 2908      ' Late reply from Host
        INVALID_HOST_IP = 2909
        INVALID_HOST_REPLY_GATEWAYTXNID = 2910
        INVALID_HOST = 2911
        INTERNAL_ERROR = 2999       ' Failed bInitPayment, bProcessSale

    End Enum

    Public Enum TXN_STATE
        INITIAL = 1             'Received request for a particular txn for the 1st time from AA Web or other merchants

        SENT_TO_HOST = 2        'Can be redirect through client's browser to Host,e.g.CIMB(popup) OR sent through WebComm to Host  'PENDING_PROCESS = 2
        DECLINE_FDS = 3         'Added, 12 Oct 2013, declined by FDS

        SUCCESS = 4
        FAILED = 5              ' Failed spInsTxnResp, bCheckUniqueTxn, bInsertNewTxn
        ABORT = 6               ' Failed GenTxnID, GetData, bPaymentMain
        ROLLBACK_PENDING = 7    'Reversal timeout/pending
        ROLLBACK_SUCCESS = 8    'Reversal success
        COMMIT_PENDING = 9
        COMMIT = 10
        ROLLBACK_FAILED = 11    'Reversal failed
        COMMIT_FAILED = 12

        RECV_ADD_SEATS = 13     'Received add seats reply from OpenSkies/NewSkies   - Added on 16 Aug 2009, to cater for add seats

        SENT_2ND_ENTRY = 15     'Replied an entry page to client, e.g. UserID entry(BCA); Tokens info entry(Mandiri)
        RECV_2ND_ENTRY = 16     'e.g. Received UserID (BCA);Tokens info(Mandiri)                  '    'PROCESSING = 3
        RECV_FROM_HOST = 17     'Received reply from Host
        SENT_QUERY_HOST = 18    'Sent query request to Host
        RECV_QUERY_HOST = 19    'Received query reply from Host
        SENT_QUERY_OS = 20      'Sent query request to OpenSkies/NewSkies
        RECV_QUERY_OS = 21      'Received query reply from OpenSkies/NewSkies
        SENT_CONFIRM_OS = 22    'Sent Add/Confirm Payment request to OpenSkies/NewSkies
        RECV_CONFIRM_OS = 23    'Received Add/Confirm Payment reply from OpenSkies/NewSkies
        RECV_HOST_QUERY = 24    'Received Host's Query request, e.g. BCA
        SENT_HOST_QUERY = 25    'Sent Host's Query reply, e.g. BCA
        SENT_REPLY_CLIENT = 26  'Sent reply to client/merchant
        SENT_VOID_HOST = 27    'Sent Reversal request to Host     - Added on 10 Apr 2010
        RECV_VOID_HOST = 28    'Received Reversal reply from Host - Added on 10 Apr 2010
        SENT_REFUND_HOST = 29   'Sent Refund request to Host            - Added  8 July 2015
        RECV_REFUND_HOST = 30   'Received Refund reply from Host        - Added  8 July 2015
        INSTL_NOT_ENTITLE = 31  'Installment Failed due to not able to get routing based on card BIN range. Added  25 May 2016
    End Enum

    Public Enum TXN_Type
        'PAY = 3
        PAY = 3
        REVERSAL = 13
    End Enum

    Public Enum ERROR_CODE
        E_SUCCESS = 0 '

        E_TXN_SUCCESSFUL = 1 'From NA--
        E_TXN_INITIATED = 2 'From NA--
        E_TXN_AUTHORIZED = 3 'From NA--
        E_TXN_FAILED = 4 'From NA--
        E_COM_TIMEOUT = 5 'From NA--
        E_TXN_REVERSED = 7 'From NA--
        E_TXN_VOIDED = 8 'From NA
        E_TXN_HOTCARD = 9

        E_TXN_PENDING_PAY_RESP = 30 'From NA
        E_TXN_PENDING_REVERSAL_RESP = 31 'From NA
        E_TXN_PENDING_VOID_RESP = 32 'From NA
        E_TXN_PENDING_AUTH_RESP = 33 'From NA

        E_INVALID_PAN = 101 'From NA
        E_INVALID_PAN_LENGTH = 102 'From NA
        E_INVALID_PAN_UNKNOWN = 103 'From NA
        E_INVALID_TRACK2_FORMAT = 104 'From NA

        E_DB_UNKNOWN_ERROR = 901 ' From NA--
        E_INVALID_HOST = 902 ' From NA--
        E_FAILED_GET_REV_RESP = 903 'From NA--
        E_FAILED_REVERSAL = 904 ' From NA--
        E_FAILED_SET_TXN_STATUS = 905 'From NA--
        E_FAILED_STORE_TXN = 906            ' Failed bInsertNewTxn
        E_FAILED_UPDATE_TXN_RESP = 907 ' From NA--
        E_FAILED_FORM_REQ = 914 ' From NA--
        E_FAILED_COM_INIT = 917 ' From NA--

        E_COMMON_INVALID_FIELD = 2902 '
        E_COMMON_MISSING_ELEMENT = 2903

        E_INVALID_INPUT = 2906              ' Failed bCheckUniqueTxn
        E_INVALID_HOST_REPLY = 2907
        E_INVALID_MID = 2901 '
        E_INVALID_SIGNATURE = 2909 ''

        E_COMMON_UNKNOWN_ERROR = 2999       ' Failed GenTxnID, bInsertTxnResp, bCheckUniqueTxn, bInsertNewTxn


        'E_PAN_AUTH_NOT_AVAILABLE = 16
        'E_PAN_NOT_PARTICIPATING = 17

        'E_DB_INVALID_FIELD = 101
        'E_DB_INVALID_TYPE = 102
        'E_DB_MISSING_FIELD = 106
        'E_DB_FAIL_INSERT = 111
        'E_DB_FAIL_UPDATE = 112
        'E_DB_FAIL_DELETE = 113
        'E_DB_FAIL_QUERY = 114

        'E_CERT_INVALID_FILE = 141
        'E_CERT_FILE_EXISTED = 142
        'E_CERT_SIGNATURE_FAILED = 143
        'E_CERT_CHAIN_INCOMPLETE = 144
        E_CERT_ERROR = 51 '

        E_COM_BAD_REQUEST = 41 '
        E_COM_UNAUTHORIZED_URL = 42
        E_COM_FORBIDDEN_URL = 43
        E_COM_URL_UNREACHABLE = 44 '
        E_COM_FILE_ERROR = 45

        'E_MERCHANT_DOES_NOT_EXIST = 241
        'E_MERCHANT_ALREADY_EXIST = 242
        'E_MERCHANT_INACTIVE = 243
        'E_MERCHANT_PWD_MISMATCH = 244
        'E_MERCHANT_PWD_FORMAT = 245
        'E_MERCHANT_ACQ_INVALID = 246
        'E_MERCHANT_INVALID_CURRENCY = 247

        E_CRYPTO_INIT_KEY = 51
        E_CRYPTO_FAIL = 52 ''
        E_CRYPTO_UNKNOWN_ERROR = 53

        'E_CRANGE_NOT_IN_RANGE = 331
        'E_CRANGE_INVALID_BIN = 332

        E_TXN_FAIL_GEN_ID = 61
        E_TXN_INVALID_STATE = 62 '
        E_TXN_NOT_FOUND = 63 '
        E_TXN_INVALID_AMOUNT = 64 '

        E_CAPTURE_FAILED = 500

        'Added  31 May 2017. VisaCheckout
        E_VC_INVALID_DATA = 400
        E_VC_INVALID_DATA_ACCESS_LEVEL = 401
        E_VC_INVALID_KEY_ID_ACCOUNT = 403
        E_VC_INVALID_CALLID = 404
        E_VC_INVALID_TOKEN = 409

    End Enum

    '************************************
    'Transaction Member variables
    '************************************
    Public Structure STPayInfo

        'Request fields
        Public szTxnType As String
        Public szMerchantID As String
        Public szMerchantName As String
        Public szGatewayID As String
        Public szMerchantTxnID As String
        Public szMerchantOrdID As String
        Public szMerchantOrdDesc As String
        Public szTxnAmount As String
        Public szB4TaxAmt As String         'Added, 11 Mar 2015, for TuneDirect GST
        Public szTaxAmt As String           'Added, 11 Mar 2015, for TuneDirect GST
        Public szBaseTxnAmount As String    '3 Aug 2009, base currency booking confirmation enhancement
        Public szToTxnAmount As String      'Added, 10 Nov 2013
        Public szRTxnAmount As String       '11 Apr 2010, Reversal transaction amount
        Public szCurrencyCode As String
        Public szBaseCurrencyCode As String '3 Aug 2009, base currency booking confirmation enhancement
        Public szFXCurrencyCode As String   'Added, 25 Aug 2014
        Public szFXTxnAmount As String      'Added, 25 Aug 2014
        Public szFXCurrencyRate As String   'Added, 26 Aug 2014
        Public szFXCurrencyMarkUp As String 'Added, 27 Aug 2014
        Public szFXCurrencyOriRate As String 'Added, 27 Aug 2014
        Public szRCurrencyCode As String    '11 Apr 2010, Reversal currency code
        Public szIssuingBank As String
        'Public szAcqBank As String              'Added 17 Apr 2018
        Public szLanguageCode As String
        Public szCardPAN As String
        Public szClearCardPAN As String         'Added, 17 May 2016, for OCP token creation
        Public szCardExp As String              'Added, 9 Oct 2015
        Public szCardHolder As String           'Added, 14 Dec 2013
        Public szCVV2 As String
        Public szCardType As String
        Public szCardTypeDesc As String         'Added, 9 Dec 2013
        Public szSessionID As String
        Public szMerchantSessionID As String    'Added, 10 Jul 2014
        Public szMerchantReturnURL As String
        Public szMerchantApprovalURL As String      'Added, 14 Jun 2014
        Public szMerchantUnApprovalURL As String    'Added, 14 Jun 2014
        Public szMerchantCancelURL As String        'Added, 2 Nov 2015
        Public szMerchantSupportURL As String
        Public szRMerchantReturnURL As String   '11 Apr 2010, Reversal Merchant Return URL
        Public szMerchantCallbackURL As String  'Added  16 Oct 2014
        Public szMerchantTermsURL As String     'Added, 15 Aug 2013
        Public szParam1 As String               'Index (token) - length 100
        Public szParam2 As String               'CardPAN - length 100
        Public szMaskedParam2 As String
        Public szParam3 As String               'Expired date - length 100
        Public szMaskedParam3 As String
        Public szParam4 As String               'Card holder name - length 100
        Public szParam5 As String               'Card issueing bank - length 100
        Public szParam6 As String               'Added on 16 Aug 2009, to cater for add seats - change to misc - length 50
        Public szParam7 As String               'misc - length 50
        Public szParam8 As String               'misc - length 50
        Public szParam9 As String               'misc - length 50
        Public szParam10 As String              'misc - length 50
        Public szParam11 As String              'misc - length 255
        Public szParam12 As String              'misc - length 255
        Public szParam13 As String              'misc - length 255
        Public szParam14 As String              'misc - length 255
        Public szParam15 As String              'misc - length 255
        Public szHashMethod As String
        Public szHashValue As String
        Public szHashValue2 As String           'Added, 28 Apr 2016, response hash value 2 which includes OrderNumber and Param6 because some plugins like Magento update order based on these 2 fields
        Public szHashKey As String
        Public szMerchantCity As String         'Added  6 Jun 2014. Firefly FFCTB
        Public szMerchantCountry As String      'Added  6 Jun 2014. Firefly FFCTB
        Public szMerchantAddress As String      'Added, 29 Jul 2013
        Public szMerchantContactNo As String    'Added, 29 Jul 2013
        Public szMerchantEmailAddr As String    'Added, 29 Jul 2013
        Public szMerchantNotifyEmailAddr As String  'Added, 8 Apr 2014
        Public szMerchantWebSiteURL As String   'Added, 29 Jul 2013
        Public szMerchantValidDomain As String  'Added, 11 Oct 2013
        Public szMerchantIP As String           'Added, 1 Aug 2013
        Public szPymtMethod As String           'Added, 11 Aug 2013
        Public szPMEntry As String              'Added, 23 Aug 2013
        Public szFromRetrySvc As String         'Added, 3 Oct 2013
        Public bMultiEntries As Boolean         'Added, 11 Oct 2013
        Public szTokenType As String            'Added, 17 Jul 2014
        Public szToken As String                'Added, 17 Jul 2014
        Public szCustOCP As String              'Added, 8 Aug 2014
        Public szOBCountryCode As String        'Added, 25 Aug 2014
        Public szProtocol As String             'Added, 13 Sept 2016, to support callback resp protocol
        Public szAcqCountryCode As String       'Added, 24 Oct 2016, support fraud score email template by country

        Public szMaskedCardPAN As String
        Public szTxnID As String
        Public szMachineID As String
        Public szRecv2ndEntry As String     'e.g. BCA (UserID entry), Mandiri (Tokens info entry)
        Public szHTML As String             'Added on 16 Mar 2010, to cater for Hosts Listing Page
        Public szRespContent As String      'Added, 11 Sept 2015, for Bitnet's response stream

        Public iPayAmount As Double
        Public iTxnType As Integer          '29 Feb 2016, , Check and found no use, chg to store TrxType call from which szTrxType to query,1=VOID, 2=REFUND and Else=PAY
        Public szPostMethod As String
        Public szTxnDateTime As String      'Date Created
        Public iReqRes As Integer
        Public iTxnIDMaxLen As Integer      'Added on 30 Oct 2009
        'Public bReplyMerchant As Boolean   'Added, 21 Apr 2011, for KTB, to cater for send query to Host which will reply in another session to GW query return page, so, no need reply merchant on same query session and just let query return page to reply merchant instead
        'Commented 28 Jan 2015
        Public iPKID As Integer             'Added, 5 Sept 2013

        'Merchant
        Public szMerchantName2 As String        'Added  5 Oct 2017, field for merchant name from db even there's a merchant name on payment request.
        Public szMerchantPassword As String
        Public szPaymentTemplate As String
        Public szErrorTemplate As String
        Public szRunningNo As String
        Public iAllowReversal As Integer
        Public iAllowFDS As Integer             'Added  Jul 2013
        Public iAllowExtFDS As Integer          'Added 14 Mar 2016, fraudwall
        Public iCollectShipAddr As Integer      'Added, 26 Jul 2013; Modified 29 Sept 2013, iAllowShipAddr to iCollectShipAddr
        Public iCollectBillAddr As Integer      'Added, 25 Oct 2013
        Public iAllowMaxMind As Integer         'Added, 6 Aug 2013
        Public szFraudByAmt As String           'Added on 23 Jul 2013  PG
        Public iAllowPayment As Integer         'Added, 7 Aug 2013
        Public iAllowQuery As Integer           'Added, 7 Aug 2013
        Public iAllowOB As Integer              'Added, 7 Aug 2013
        Public iAllowOTC As Integer             'Added, 2 Sept 2014
        Public iAllowWallet As Integer          'Added, 7 Aug 2013
        Public iAllowOCP As Integer             'Added, 17 Jul 2014
        Public iAllowCallBack As Integer        'Added, 17 Jul 2014
        Public iAllowFX As Integer              'Added, 24 Jul 2014
        Public iRouteByParam1 As Integer        'Added, 11 Apr 2014
        Public iPymtPageTimeout_S As Integer    'Added, 26 Jul 2013; Modified 29 Sept 2013, iCardEntryTimeout_S to iPymtPageTimeout_S
        Public iCardTypeProfileID As Integer    'Added on 25 Jul 2013  PG
        Public i3DAccept As Integer             'Added, 28 Jul 2013
        Public iSelfMPI As Integer              'Added  22 May 2014, Firefly FF
        Public iVISA As Integer                 'Added, 31 Jul 2013
        Public iMasterCard As Integer           'Added, 31 Jul 2013
        Public iAMEX As Integer                 'Added, 31 Jul 2013
        Public iJCB As Integer                  'Added, 6 Aug 2013
        Public iDiners As Integer               'Added, 6 Aug 2013
        Public iCUP As Integer                  'Added, 6 Aug 2013
        Public iMasterPass As Integer           'Added  20 Sept 2016. MasterPass
        Public iVisaCheckout As Integer         'Added  3 Apr 2017. VisaCheckout
        Public iSamsungPay As Integer           'Added SSP, 15 Oct 2017. SamsungPay
        Public iPageTimeout As Integer          'Added, 10 Aug 2013
        Public iSvcType As Integer              'Added, 23 Aug 2013
        Public iNeedAddOSPymt As Integer
        Public iTxnExpired_S As Integer         'Added, 22 Sept 2013
        Public iReceipt As Integer                  'Added, 29 Sept 2013
        Public iPymtNotificationEmail As Integer    'Added, 29 Sept 2013
        Public iPymtNotificationSMS As Integer      'Added, 29 Sept 2013
        Public iAutoReversal As Integer             'Added, 3 Oct 2013
        Public iRespMethod As Integer               'Added, 3 Oct 2013
        Public dPerTxnAmtLimit As Double            'Added, 7 Nov 2013
        Public iHitLimitAct As Integer              'Added, 08 April 2019, Joyce
        Public iHCProfileID As Integer              'Added, 5 Dec 2013, OB Host Country Profile ID
        Public dMerchFXRate As Double               'Added, 20 Aug 2014, Merchant Preferencial Foreign Exchange Rate (+/-)
        Public iShowMerchantAddr As Integer         'Added, 2 Oct 2014
        Public iShowMerchantLogo As Integer         'Added, 13 Oct 2014
        Public szExtraCSS As String                 'Added, 14 Oct 2014
        Public iOTCExpiryHour As Integer            'Added, 15 Apr 2015
        Public iReturnCardData As Integer           'Added, 7 Oct 2015
        Public szSettleTAID As String               'Added, 3 May 2016, MOTO-Settle
        Public szSettleAmount As String             'Added, 3 May 2016, MOTO-Settle
        Public szSettleCount As String              'Added, 3 May 2016, MOTO-Settle
        Public iMCCCode As Integer                  'Added  Fraudwall
        Public iFDSActionID As Integer              'Added  Fraudwall
        Public iFDSRespCode As Integer              'Added  Fraudwall
        Public szFDSCustomerID As String            'Added  Fraudwall
        Public szFDSAuthCode As String              'Added  Fraudwall

        Public szFDSRespMsg As String               'Added FW
        Public szFDSError As String                 'Added FW
        Public szFDSDeclineRule As String           'Added FW
        Public szFDSScoreReason As String           'Added FW
        Public szFDSEmailTemplate As String         'Added FW

        Public szPostCode As String                 'Added  29 Mar 2017, for SMI/Paymaya

        Public iCVVRequire As Integer             'Added 12 Feb 2018, Tik FX

        'Res to client
        Public iTxnStatus As Integer
        Public iTxnState As Integer
        Public iMerchantTxnStatus As Integer
        Public iQueryStatus As Integer
        Public iDuration As Integer
        'Res from Host
        Public szHostTxnID As String
        Public szRespCode As String
        Public szAuthCode As String
        Public szBankRespMsg As String
        Public szHostDate As String
        Public szHostTime As String
        Public szHostMID As String
        Public szHostTID As String
        Public szHostTxnStatus As String
        Public szHostTxnAmount As String
        Public szHostOrderNumber As String
        Public szHostGatewayTxnID As String
        Public szHostCurrencyCode As String
        Public szHostTxnType As String      'Added on 2 Oct 2009
        Public szEncryptKeyPath As String   'Added on 4 Nov 2009, Encrypt key file path
        Public szDecryptKeyPath As String   'Added on 4 Nov 2009, Decrypt key file path
        Public szRedirectURL As String      'Added on 21 Mar 2010
        Public szECI As String              'Added on 5 Oct 2011  for CCGW
        Public szPayerAuth As String        'Added on 5 Oct 2011  for CCGW
        Public szAVS As String              'Added on 17 July 2013  for PG
        Public szHostTtlRefundAmt As String 'Added on 15 Sept 2015  REFUND
        Public szHostRefundTxnID As String  'Added on 26 Feb 2016 . Store Refund TxnID different everytime and from orig TxnID
        Public szHostTtlCaptureAmt As String    'Added on 5 Aug 2016  Auth&Capture

        'Res from Host - Action
        Public iAction As Integer
        Public bVerified As Boolean
        'Res from OpenSkies
        Public iOSRet As Integer
        Public iErrSet As Integer
        Public iErrNum As Integer
        'Http connection details
        Public iHttpTimeoutSeconds As Integer

        'Error
        Public szTxnMsg As String   ' RespMesg
        Public szQueryMsg As String
        Public szErrDesc As String
        Public szDate As String
        Public szTime As String

        ''Added on 23 Jul 2013 PG''
        Public szCustIP As String
        Public szBillAddr As String
        Public szBillCity As String
        Public szBillRegion As String
        Public szBillPostal As String
        Public szBillCountry As String          'Added, 31 Jul 2013
        Public szShipAddr As String
        Public szShipCity As String
        Public szShipRegion As String
        Public szShipPostal As String
        Public szShipCountry As String          'Added, 31 Jul 2013
        Public szCustName As String             'Added, 23 Aug 2013
        Public szCustEmail As String
        Public szCustPhone As String
        Public szCustMAC As String              'Added, 23 Aug 2013
        Public szBINCountry As String           'Added, 3 Oct 2013
        Public szBINName As String
        Public szBINPhone As String

        ''Added  19 May 2014, Firefly FF
        Public szCAVV As String                 '40 character. eg: AAABCJFQEQAAAAABkFARAAAAAAA=
        Public sz3dsXID As String               '40 character. eg: NHRkRExIcmhUSHJ3WHFoOU9BMjA=
        Public sz3DFlag As String               '5 character. eg: 01VYY
        Public sz3dsEnrolled As String          '1 character which from sz3DFlag.Substring(3, 1) = Y
        Public sz3dsStatus As String            '1 character which from sz3DFlag.Substring(4, 1) = Y
        Public szMPICode As String              '1 character which from sz3DFlag.Substring(0,1) = 0 (Success). Added  13 Jun 2014, Firefly FFCIMB

        'Added  4 July 2014. GlobalPay Cybersource
        Public szUUID As String
        Public szUTCDateTime As String
        Public szAirlineCode As String      'Added  3 March 2015. Reuse AirlineCode as Trxn Security Code for GPay Reversal.

        ' Added  27 Jan 2015, OTC
        Public szOTCSecureCode As String
        Public szReqTime As String
        Public szDueTime As String
        Public szHostCountryName As String        'Added display in OTC receipt

        'Added  21 Apr 2015. Auth&Capture
        Public szCTxnAmount As String
        Public szCCurrencyCode As String
        Public bRespCodeCS As Boolean      'Added  25 Aug 2016.
        Public szTotCaptureAmt As String    'Added  30 Aug 2016.

        'Added  10 Jul 2015. REFUND
        Public szTxnAmountOri As String
        Public szTotRefundAmt As String
        'Added 20 Nov 2015. Promotion
        Public iPromoExist As Integer
        Public lPromoList As List(Of String)
        Public iPromoID As Integer
        Public szPromoCode As String
        Public szPromoOriAmt As String
        Public szPromotion As String
        Public iPromoActionID As Integer
        Public iPromoConfirmID As Integer

        'Added  25 Nov 2015. Installment
        Public iInstallment As Integer
        Public szInstallPlan As String
        Public szInstallMthTerm As String
        Public iAllowInstallment As Integer
        Public iEntitleInstallment As Integer

        ' Added 711, 25 May 2016
        Property szDetailID As String

        'Added  2 Aug 2016. HostGroup for FPX2D
        Public szHGBankID As String
        'Added  10 Oct 2016. MasterPass
        Public szWalletID As String
        Public szCheckoutStatus As String
        Public szLongAccessToken As String
        Public szPreCheckoutTxnID As String
        Public szPreCheckoutCardLast4 As String
        Public szPreCheckoutCardId As String
        Public szPreCheckoutAddrId As String
        Public szReqToken As String             'Added 9 Jan 2017
        Public szPairingReqToken As String      'Added 9 Jan 2017
        Public szReqVerifier As String          'Added 10 Jan 2017
        Public szPairingVerifier As String      'Added 10 Jan 2017
        Public szCheckoutResourceURL As String  'Added 10 Jan 2017
        Public szCardList As String             'Added 25 Feb 2017 - Masterpass-2
        Public szHostTxnState As String         'Added BTP, 26 Dec 2017
        Public iGMTDiffMins As Integer          'Added OTC, 21 Feb 2017, to get host country local time

    End Structure

    '************************************
    'Transaction Member variables
    '************************************
    Public Structure STHost
        Public szTID As String
        Public szMID As String
        Public szServerCertName As String
        Public szHostTemplate As String
        Public szPaymentTemplate As String
        Public szSecondEntryTemplate As String
        Public szMesgTemplate As String 'Added on 27 Oct 2009
        Public szURL As String
        Public szQueryURL As String
        Public szReversalURL As String
        Public szAcknowledgementURL As String
        Public szCancelURL As String
        Public szReturnURL As String
        Public szReturnURL2 As String   'Added on 22 Oct 2009
        Public szReturnIPAddresses As String
        Public szDBConnStr As String
        Public szOSPymtCode As String
        Public szSendKey As String      'Secret key to form hash value for request to Host
        Public szReturnKey As String    'Secret key to form hash value for response from Host
        Public szHashMethod As String
        Public szHashValue As String
        Public szSecretKey As String
        Public szInitVector As String
        Public szReserved As String     'Reserved field to store Host reply, especially for the whole encrypted reply
        Public szReserved2 As String    'Added, 24 Sept 2015, for Bitnet
        Public szAcquirerID As String
        Public szPayeeCode As String
        Public szRunningNo As String    'Trace Number/Running Number - Added on 19 Sept 2010
        Public szDesc As String         'Host Description - Added, 9 Dec 2013
        Public szProtocol As String     'Security Protocol, e.g. SSL3, TLS, TLS1.1, TLS1.2 - Added, 21 Jun 2015
        Public szChannelCfg1 As String  'Added, 1 Sept 2015, Bitnet
        Public szChannelCfg2 As String  'Added, 1 Sept 2015, Bitnet
        Public szFixedCurrency As String 'Added, 25 Sept 2015, Bitnet
        Public szLogoPath As String     'Added 23 Oct 2015
        Public szCertId As String       'Added 05 feb 2016, for GPUPOP to store Cert Serial Number get from 1st time in Query String

        Public iChannelID As Integer
        Public iHostID As Integer
        Public iPortNumber As Integer
        Public iQueryPortNumber As Integer
        Public iReversalPortNumber As Integer
        Public iAcknowledgementPortNumber As Integer
        Public iCancelPortNumber As Integer
        Public iTimeOut As Long
        Public iChannelTimeOut As Long  ' Timeout - Added, 18 Nov 2015, MOTO, cater for receive timeout between PG and MPG
        Public iOTCRevTimeOut As Long               ' OTC Reversal Timeout - Added  24 Jul 2015
        Public iRequire2ndEntry As Integer
        Public iNeedReplyAcknowledgement As Integer
        Public iNeedRedirectOTP As Integer          'Added on 21 Mar 2010
        Public iTxnStatusActionID As Integer
        Public iHostReplyMethod As Integer
        Public iGatewayTxnIDFormat As Integer
        Public iQueryFlag As Integer                ' Query flag to determine whether to query the host
        Public iIsActive As Integer                 ' Flag to indicate whether Host is enabled or disabled
        Public iAllowQuery As Integer               ' Added  9 Jun 2014. Firefly FFCTB
        Public iAllowReversal As Integer            ' Flag to indicate whether Host supports Reversal or Refund - Added on 10 Apr 2010
        Public iNoOfRunningNo As Integer            ' Number of digits for Trace Number or Running Number - Added on 18 Sept 2010
        Public iRunningNoUniquePerDay As Integer    ' Boolean to indicate whether Trace Number or Running Number is unique per day - Added on 18 Sept 2010
        Public iLogRes As Integer                   ' Added  23 Oct 2014. Firefly FFAMBANK
        Public bIsHashOptional As Boolean           ' Added  27 Aug 2015. PBB.
        Public iOTCGenMethod As Integer             ' Added 711, 8 Apr 2016, to specify wether OTC Secure Code generated by eGHL(1) or Host(2)
        Public szSelectedCurrExpnt As String        ' Added  13 Sept 2017. FYAMEX
        Public iMerchantHostTimeout As Integer      ' Added, 4 Jan 2018, Atomy FPX 1 hour expiry
        Public iHostMPI As Integer                  ' MPGS (RHB), 11 Jun 2018, =1 is need to perform 3D authetication(MPI) before send to bank for payment. ' MPGS (Alliance), 4 Feb 2020, =2 use MPGS Hosted Checkout Payment page to collect CC details.

    End Structure

    Public Sub InitHostInfo(ByRef st_HostInfo As STHost)
        st_HostInfo.szTID = ""
        st_HostInfo.szMID = ""
        st_HostInfo.szServerCertName = ""
        st_HostInfo.szHostTemplate = ""
        st_HostInfo.szPaymentTemplate = ""
        st_HostInfo.szSecondEntryTemplate = ""
        st_HostInfo.szMesgTemplate = ""             'Added on 27 Oct 2009
        st_HostInfo.szURL = ""
        st_HostInfo.szQueryURL = ""
        st_HostInfo.szReversalURL = ""
        st_HostInfo.szAcknowledgementURL = ""
        st_HostInfo.szCancelURL = ""
        st_HostInfo.szReturnURL = ""
        st_HostInfo.szReturnURL2 = ""               'Added on 22 Oct 2009
        st_HostInfo.szReturnIPAddresses = ""
        st_HostInfo.szDBConnStr = ""
        st_HostInfo.szOSPymtCode = ""
        st_HostInfo.szSendKey = ""
        st_HostInfo.szReturnKey = ""
        st_HostInfo.szHashMethod = ""
        st_HostInfo.szHashValue = ""
        st_HostInfo.szSecretKey = ""
        st_HostInfo.szInitVector = ""
        st_HostInfo.szReserved = ""
        st_HostInfo.szReserved2 = ""                'Added, 24 Sept 2015, for Bitnet
        st_HostInfo.szAcquirerID = ""
        st_HostInfo.szPayeeCode = ""
        st_HostInfo.szRunningNo = ""                'Added on 19 Sept 2010
        st_HostInfo.szDesc = ""                     'Added on 9 Dec 2013
        st_HostInfo.szProtocol = ""                 'Added, 21 Jun 2015
        st_HostInfo.szChannelCfg1 = ""              'Added, 1 Sept 2015
        st_HostInfo.szChannelCfg2 = ""              'Added, 1 Sept 2015
        st_HostInfo.szFixedCurrency = ""            'Added, 25 Sept 2015
        st_HostInfo.szLogoPath = ""                 'Added 23 Oct 2015, for BancNet
        st_HostInfo.szCertId = ""                   'Added 05 feb 2016, for GPUPOP

        'st_HostInfo.sz3DESAppKey = "3537146182309002FEB7EBCE55ED3BBA71BED7CEB1D18CAA"
        'st_HostInfo.sz3DESAppVector = "0000000000000000"

        st_HostInfo.iChannelID = 0
        st_HostInfo.iHostID = 0
        st_HostInfo.iPortNumber = 0
        st_HostInfo.iQueryPortNumber = 0
        st_HostInfo.iReversalPortNumber = 0
        st_HostInfo.iAcknowledgementPortNumber = 0
        st_HostInfo.iCancelPortNumber = 0
        st_HostInfo.iTimeOut = 900                  'seconds
        st_HostInfo.iChannelTimeOut = 900           'seconds - Added, 18 Nov 2015, MOTO
        st_HostInfo.iOTCRevTimeOut = 0              'seconds - Added  24 Jul 2015
        st_HostInfo.iRequire2ndEntry = 0
        st_HostInfo.iNeedReplyAcknowledgement = 0
        st_HostInfo.iNeedRedirectOTP = 0            'Added on 21 Mar 2010
        st_HostInfo.iTxnStatusActionID = 0
        st_HostInfo.iHostReplyMethod = 0
        st_HostInfo.iGatewayTxnIDFormat = 0
        st_HostInfo.iQueryFlag = 1
        st_HostInfo.iIsActive = 1
        st_HostInfo.iAllowQuery = 0                 ' Added  9 Jun 2014. Firefly FFCTB
        st_HostInfo.iAllowReversal = 0              ' Added on 10 Apr 2010
        st_HostInfo.iNoOfRunningNo = 0              ' Added on 18 Sept 2010
        st_HostInfo.iRunningNoUniquePerDay = 0      ' Added on 18 Sept 2010
        st_HostInfo.iLogRes = 1                     ' Added  23 Oct 2014. Firefly FFAMBANK
        st_HostInfo.bIsHashOptional = False         ' Added  27 Aug 2015. PBB. Flag to determine whether to reject txns that do not have hash value returned
        st_HostInfo.iOTCGenMethod = 0               ' Added 711, 8 Apr 2016, to specify wether OTC Secure Code generated by eGHL(1) or Host(2)
        st_HostInfo.szSelectedCurrExpnt = ""        ' Added  13 Sept 2017. FYAMEX
        st_HostInfo.iMerchantHostTimeout = 0        ' Added, 4 Jan 2018, Atomy FPX 1 hour timeout
        st_HostInfo.iHostMPI = 0                    ' MPGS (RHB), 11 Jun 2018

    End Sub

    Public Sub InitPayInfo(ByRef st_PayInfo As STPayInfo)

        st_PayInfo.szTxnType = ""
        st_PayInfo.szMerchantID = ""
        st_PayInfo.szMerchantName = ""
        st_PayInfo.szGatewayID = ""
        st_PayInfo.szMerchantTxnID = ""
        st_PayInfo.szMerchantOrdID = ""
        st_PayInfo.szMerchantOrdDesc = ""
        st_PayInfo.szTxnAmount = ""
        st_PayInfo.szB4TaxAmt = ""          'Added, 11 Mar 2015, for TuneDirect GST
        st_PayInfo.szTaxAmt = ""            'Added, 11 Mar 2015, for TuneDirect GST
        st_PayInfo.szBaseTxnAmount = ""     '3 Aug 2009, base currency booking confirmation enhancement
        st_PayInfo.szToTxnAmount = ""       'Added, 10 Nov 2013
        st_PayInfo.szRTxnAmount = ""        '11 Apr 2010, Reversal transaction amount
        st_PayInfo.szCurrencyCode = ""
        st_PayInfo.szBaseCurrencyCode = ""  '3 Aug 2009, base currency booking confirmation enhancement
        st_PayInfo.szFXCurrencyCode = ""    'Added, 25 Aug 2014
        st_PayInfo.szFXTxnAmount = ""       'Added, 25 Aug 2014
        st_PayInfo.szFXCurrencyRate = ""    'Added, 26 Aug 2014
        st_PayInfo.szFXCurrencyMarkUp = ""  'Added, 27 Aug 2014
        st_PayInfo.szFXCurrencyOriRate = "" 'Added, 27 Aug 2014
        st_PayInfo.szRCurrencyCode = ""     '11 Apr 2010, Reversal currency code
        st_PayInfo.szIssuingBank = ""
        'st_PayInfo.szAcqBank = ""           'Added 17 Apr 2018
        st_PayInfo.szLanguageCode = ""
        st_PayInfo.szCardPAN = ""
        st_PayInfo.szClearCardPAN = ""          'Added, 17 May 2016, for OCP token creation
        st_PayInfo.szCardExp = ""               'Added, 9 Oct 2015
        st_PayInfo.szCardHolder = ""            'Added, 14 Dec 2013
        st_PayInfo.szCVV2 = ""
        st_PayInfo.szCardType = ""
        st_PayInfo.szCardTypeDesc = ""          'Added, 9 Dec 2013
        st_PayInfo.szSessionID = ""
        st_PayInfo.szMerchantSessionID = ""     'Added, 10 Jul 2014
        st_PayInfo.szMerchantReturnURL = ""
        st_PayInfo.szMerchantCallbackURL = ""       'Added  16 Oct 2014
        st_PayInfo.szMerchantApprovalURL = ""       'Added, 14 Jun 2014
        st_PayInfo.szMerchantUnApprovalURL = ""     'Added, 14 Jun 2014
        st_PayInfo.szMerchantCancelURL = ""         'Added, 2 Nov 2015
        st_PayInfo.szMerchantSupportURL = ""
        st_PayInfo.szRMerchantReturnURL = ""    '11 Apr 2010, Reversal Merchant Return URL
        st_PayInfo.szMerchantTermsURL = ""      'Added, 15 Aug 2013
        st_PayInfo.szParam1 = ""
        st_PayInfo.szParam2 = ""
        st_PayInfo.szMaskedParam2 = ""
        st_PayInfo.szParam3 = ""
        st_PayInfo.szMaskedParam3 = ""
        st_PayInfo.szParam4 = ""
        st_PayInfo.szParam5 = ""
        st_PayInfo.szParam6 = ""            'Added on 16 Aug 2009, to cater for add seats
        st_PayInfo.szParam7 = ""
        st_PayInfo.szParam8 = ""
        st_PayInfo.szParam9 = ""
        st_PayInfo.szParam10 = ""
        st_PayInfo.szParam11 = ""
        st_PayInfo.szParam12 = ""
        st_PayInfo.szParam13 = ""
        st_PayInfo.szParam14 = ""
        st_PayInfo.szParam15 = ""
        st_PayInfo.szHashMethod = ""
        st_PayInfo.szHashValue = ""
        st_PayInfo.szHashValue2 = ""            'Added, 28 Apr 2016
        st_PayInfo.szHashKey = ""
        st_PayInfo.szPostMethod = ""
        st_PayInfo.szTxnDateTime = ""
        st_PayInfo.szMerchantCity = ""          'Added  6 Jun 2014. Firefly FFCTB
        st_PayInfo.szMerchantCountry = ""       'Added  6 Jun 2014. Firefly FFCTB
        st_PayInfo.szMerchantAddress = ""       'Added, 29 Jul 2013
        st_PayInfo.szMerchantContactNo = ""     'Added, 29 Jul 2013
        st_PayInfo.szMerchantEmailAddr = ""     'Added, 29 Jul 2013
        st_PayInfo.szMerchantNotifyEmailAddr = ""   'Added, 8 Apr 2014
        st_PayInfo.szMerchantWebSiteURL = ""    'Added, 29 Jul 2013
        st_PayInfo.szMerchantValidDomain = ""   'Added, 11 Oct 2013
        st_PayInfo.szMerchantIP = ""            'Added, 1 Aug 2013
        st_PayInfo.szPymtMethod = ""            'Added, 11 Aug 2013
        st_PayInfo.szPMEntry = "0"              'Added, 23 Aug 2013
        st_PayInfo.szFromRetrySvc = "0"         'Added, 3 Oct 2013
        st_PayInfo.bMultiEntries = False        'Added, 11 Oct 2013
        st_PayInfo.szTokenType = ""             'Added, 17 Jul 2014
        st_PayInfo.szToken = ""                 'Added, 17 Jul 2014
        st_PayInfo.szCustOCP = ""               'Added, 8 Aug 2014
        st_PayInfo.szOBCountryCode = ""         'Added, 25 Aug 2014
        st_PayInfo.szProtocol = ""              'Added, 13 Sept 2016
        st_PayInfo.szAcqCountryCode = ""        'Added, 24 Oct 2016

        st_PayInfo.iReqRes = 1
        st_PayInfo.iTxnIDMaxLen = 0             'Added on 30 Oct 2009
        'st_PayInfo.bReplyMerchant = True        'Added, 20 Apr 2011; Commented 28 Jan 2015

        st_PayInfo.szMaskedCardPAN = ""
        st_PayInfo.szTxnID = ""
        st_PayInfo.szMachineID = ""
        st_PayInfo.szRecv2ndEntry = ""
        st_PayInfo.szHTML = ""                  'Added on 16 Mar 2010
        st_PayInfo.szRespContent = ""           'Added, 11 Sept 2015, for Bitnet's response stream

        st_PayInfo.iTxnStatus = -1
        st_PayInfo.iTxnState = -1
        st_PayInfo.iMerchantTxnStatus = -1
        st_PayInfo.iPKID = -1                   'Added, 5 Sept 2013

        st_PayInfo.szHostTxnID = ""
        st_PayInfo.szRespCode = ""
        st_PayInfo.szAuthCode = ""
        st_PayInfo.szBankRespMsg = ""
        st_PayInfo.szHostDate = ""
        st_PayInfo.szHostTime = ""
        st_PayInfo.szHostMID = ""
        st_PayInfo.szHostTID = ""
        st_PayInfo.szHostTxnStatus = ""
        st_PayInfo.szHostTxnAmount = ""
        st_PayInfo.szHostOrderNumber = ""
        st_PayInfo.szHostGatewayTxnID = ""
        st_PayInfo.szHostCurrencyCode = ""
        st_PayInfo.szHostTxnType = ""       ' Added on 2 Oct 2009
        st_PayInfo.szEncryptKeyPath = ""
        st_PayInfo.szDecryptKeyPath = ""
        st_PayInfo.szRedirectURL = ""       ' Added on 21 Mar 2010
        st_PayInfo.szECI = ""               'Added on 5 Oct 2011  for CCGW
        st_PayInfo.szPayerAuth = ""         'Added on 5 Oct 2011  for CCGW
        st_PayInfo.szAVS = ""               'Added on 17 July 2013  for PG
        st_PayInfo.szHostTtlRefundAmt = 0   'Added on 15 Sept 2015  REFUND
        st_PayInfo.szHostRefundTxnID = ""   'Added on 26 Feb 2016 . Store Refund TxnID different everytime and from orig TxnID
        st_PayInfo.szHostTtlCaptureAmt = 0  'Added on 5 Aug 2016  Auth&Capture

        st_PayInfo.iAction = 0
        st_PayInfo.bVerified = True    ' Added on 22 Apr 2009 for verification of host's replied fields

        st_PayInfo.iOSRet = 99
        st_PayInfo.iErrSet = -1
        st_PayInfo.iErrNum = -1
        st_PayInfo.iHttpTimeoutSeconds = 0

        st_PayInfo.iQueryStatus = 2    ' Query error
        st_PayInfo.iDuration = -1
        st_PayInfo.szTxnMsg = ""
        st_PayInfo.szQueryMsg = ""
        st_PayInfo.szErrDesc = ""

        st_PayInfo.szMerchantName2 = ""     'Added  5 Oct 2017, field for merchant name from db even there's a merchant name on payment request.
        st_PayInfo.szMerchantPassword = ""
        st_PayInfo.szPaymentTemplate = ""
        st_PayInfo.szErrorTemplate = ""
        st_PayInfo.szRunningNo = ""
        st_PayInfo.iAllowReversal = 0
        st_PayInfo.iAllowFDS = 0        ' Added  Jul 2013
        st_PayInfo.iAllowExtFDS = 0     ' Added 14 Mar 2016, fraudwall
        st_PayInfo.iCollectShipAddr = 0 ' Added, 26 Jul 2013; Modified 29 Sept 2013, iAllowShipAddr to iCollectShipAddr
        st_PayInfo.iCollectBillAddr = 0 ' Added, 25 Oct 2013
        st_PayInfo.iAllowMaxMind = 0    ' Added, 6 Aug 2013
        st_PayInfo.szFraudByAmt = ""    ' Added on 23 Jul 2013  PG
        st_PayInfo.iAllowPayment = 0    ' Added, 7 Aug 2013
        st_PayInfo.iAllowQuery = 0      ' Added, 7 Aug 2013
        st_PayInfo.iAllowOB = 0         ' Added, 7 Aug 2013
        st_PayInfo.iAllowOTC = 0        ' Added, 2 Sept 2014
        st_PayInfo.iAllowWallet = 0     ' Added, 7 Aug 2013
        st_PayInfo.iAllowOCP = 0        ' Added, 17 Jul 2014
        st_PayInfo.iAllowCallBack = 0   ' Added, 17 Jul 2014
        st_PayInfo.iAllowFX = 0         ' Added, 24 Jul 2014
        st_PayInfo.iRouteByParam1 = 0   ' Added, 11 Apr 2014
        st_PayInfo.i3DAccept = 3        ' Added, 28 Jul 2013; 1 - 3D only; 2 - N3D only; 3 - 3D+N3D
        st_PayInfo.iSelfMPI = 0         ' Added  22 May 2014, Firefly FF
        st_PayInfo.iVISA = 0            ' Added, 31 Jul 2013
        st_PayInfo.iMasterCard = 0      ' Added, 31 Jul 2013
        st_PayInfo.iAMEX = 0            ' Added, 31 Jul 2013
        st_PayInfo.iJCB = 0             ' Added, 6 Aug 2013
        st_PayInfo.iDiners = 0          ' Added, 6 Aug 2013
        st_PayInfo.iCUP = 0             ' Added, 6 Aug 2013
        st_PayInfo.iMasterPass = 0      ' Added  20 Sept 2016. MasterPass
        st_PayInfo.iVisaCheckout = 0    ' Added  3 Apr 2017. VisaCheckout
        st_PayInfo.iSamsungPay = 0      'Added SSP, 15 Oct 2017. SamsungPay
        st_PayInfo.iPageTimeout = 0     ' Added, 10 Aug 2013
        st_PayInfo.iSvcType = 0         ' Added, 23 Aug 2013
        st_PayInfo.iNeedAddOSPymt = 0
        st_PayInfo.iTxnExpired_S = 0            ' Added, 22 Sept 2013
        st_PayInfo.iReceipt = 0                 ' Added, 29 Sept 2013
        st_PayInfo.iPymtNotificationEmail = 0   ' Added, 29 Sept 2013
        st_PayInfo.iPymtNotificationSMS = 0     ' Added, 29 Sept 2013
        st_PayInfo.iAutoReversal = 0            ' Added, 3 Oct 2013
        st_PayInfo.iRespMethod = 1              ' Added, 3 Oct 2013
        st_PayInfo.dPerTxnAmtLimit = 0          ' Added, 7 Nov 2013
        st_PayInfo.iHCProfileID = 0             ' Added, 5 Dec 2013
        st_PayInfo.dMerchFXRate = 0             ' Added, 20 Aug 2014
        st_PayInfo.iShowMerchantAddr = 1        ' Added, 2 Oct 2014
        st_PayInfo.iShowMerchantLogo = 1        ' Added, 13 Oct 2014
        st_PayInfo.szExtraCSS = ""              ' Added, 14 Oct 2014
        st_PayInfo.iOTCExpiryHour = 0           ' Added, 15 Apr 2015
        st_PayInfo.iReturnCardData = 0          ' Added, 7 Oct 2015
        st_PayInfo.szSettleTAID = ""             ' Added, 3 May 2016, MOTO-Settle
        st_PayInfo.szSettleAmount = ""          ' Added, 3 May 2016, MOTO-Settle
        st_PayInfo.szSettleCount = ""           ' Added, 3 May 2016, MOTO-Settle

        'Added  Fraudwall
        st_PayInfo.iMCCCode = 0
        st_PayInfo.iFDSActionID = 0
        st_PayInfo.iFDSRespCode = 0
        st_PayInfo.szFDSCustomerID = ""
        st_PayInfo.szFDSAuthCode = ""

        st_PayInfo.szFDSRespMsg = ""                'Added FW, 29 Sept 2016
        st_PayInfo.szFDSError = ""                  'Added FW
        st_PayInfo.szFDSDeclineRule = ""            'Added FW
        st_PayInfo.szFDSScoreReason = ""            'Added FW
        st_PayInfo.szFDSEmailTemplate = ""          'Added FW

        st_PayInfo.szPostCode = ""                  'Added  29 Mar 2017, for SMI/Paymaya
        st_PayInfo.iCVVRequire = 1                  'Added, 12 Feb 2018, Tik FX

        st_PayInfo.szDate = ""
        st_PayInfo.szTime = ""

        ''Added on 23 Jul 2013  PG''
        st_PayInfo.szCustIP = ""
        st_PayInfo.szCustName = ""      'Added, 23 Aug 2013
        st_PayInfo.szCustEmail = ""
        st_PayInfo.szCustPhone = ""
        st_PayInfo.szCustMAC = ""       'Added, 23 Aug 2013
        st_PayInfo.szBillAddr = ""
        st_PayInfo.szBillCity = ""
        st_PayInfo.szBillRegion = ""
        st_PayInfo.szBillPostal = ""
        st_PayInfo.szBillCountry = ""   'Added, 31 Jul 2013
        st_PayInfo.szShipAddr = ""
        st_PayInfo.szShipCity = ""
        st_PayInfo.szShipRegion = ""
        st_PayInfo.szShipPostal = ""
        st_PayInfo.szShipCountry = ""   'Added, 31 Jul 2013
        st_PayInfo.szBINCountry = ""    'Added, 3 Oct 2013
        st_PayInfo.szBINName = ""
        st_PayInfo.szBINPhone = ""

        'Added  19 May 2014, Firefly FF
        st_PayInfo.szCAVV = ""
        st_PayInfo.sz3dsXID = ""
        st_PayInfo.sz3DFlag = ""
        st_PayInfo.sz3dsEnrolled = ""
        st_PayInfo.sz3dsStatus = ""
        st_PayInfo.szMPICode = ""       'Added  13 Jun 2014, Firefly FFCIMB

        'Added  3 July 2014. GlobalPay Cybersource
        st_PayInfo.szUUID = ""
        st_PayInfo.szUTCDateTime = ""
        st_PayInfo.szAirlineCode = ""   'Added  3 March 2015. Reuse AirlineCode as Trxn Security Code for GPay Reversal.

        'Added  27 Jan 2015, OTC
        st_PayInfo.szOTCSecureCode = ""
        st_PayInfo.szReqTime = ""
        st_PayInfo.szDueTime = ""
        st_PayInfo.szHostCountryName = ""

        'Added  10 Jul 2015. REFUND
        st_PayInfo.szTxnAmountOri = 0
        st_PayInfo.szTotRefundAmt = 0
        'Added 20 Nov 2015, Promotion
        st_PayInfo.iPromoExist = -1
        st_PayInfo.iPromoID = -1
        st_PayInfo.lPromoList = Nothing
        st_PayInfo.szPromoCode = ""
        st_PayInfo.szPromoOriAmt = ""
        st_PayInfo.szPromotion = ""
        st_PayInfo.iPromoActionID = 0
        st_PayInfo.iPromoConfirmID = 0

        'Added  25 Nov 2015. Installment
        st_PayInfo.iInstallment = 0
        st_PayInfo.szInstallPlan = ""
        st_PayInfo.szInstallMthTerm = ""
        st_PayInfo.iAllowInstallment = 0
        st_PayInfo.iEntitleInstallment = 0

        'Added  2 Aug 2016. HostGroup FPX2D
        st_PayInfo.szHGBankID = ""

        'Added  25 Aug 2016. Auth&Capture
        st_PayInfo.szCTxnAmount = ""
        st_PayInfo.szCCurrencyCode = ""
        st_PayInfo.bRespCodeCS = False
        st_PayInfo.szTotCaptureAmt = 0  'Added  30 Aug 2016.
        'Adde  10 Oct 2016. MasterPass
        st_PayInfo.szWalletID = ""
        st_PayInfo.szCheckoutStatus = ""
        st_PayInfo.szLongAccessToken = ""
        st_PayInfo.szPreCheckoutTxnID = ""
        st_PayInfo.szPreCheckoutCardLast4 = ""
        st_PayInfo.szPreCheckoutCardId = ""
        st_PayInfo.szPreCheckoutAddrId = ""
        st_PayInfo.szReqToken = ""              'Added 9 Jan 2017
        st_PayInfo.szPairingReqToken = ""       'Added 9 Jan 2017
        st_PayInfo.szReqVerifier = ""           'Added 10 Jan 2017
        st_PayInfo.szPairingVerifier = ""       'Added 10 Jan 2017
        st_PayInfo.szCheckoutResourceURL = ""   'Added 10 Jan 2017
        st_PayInfo.szCardList = ""              'Added 25 Feb 2017. Masterpass-2

        ' Added 711, 25 May 2016
        st_PayInfo.szDetailID = ""

        ' Added BTP, 26 Dec 2017
        st_PayInfo.szHostTxnState = ""
        st_PayInfo.iGMTDiffMins = 0                ' Added OTC, 21 Feb 2017, to get host country local time

        'Added, 3 Oct 2017, gets KEK of DEK
        If (True <> m_objTxnProc.bGetKEK(st_PayInfo)) Then
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "> Unable retrieve KEK")
        End If
    End Sub

    Public Function szFormatPlaceHolder(ByRef szOriText As String, ByVal szTextToFind As String, ByVal szTextToReplace As String)

        If InStr(szOriText, szTextToFind) Then
            If (szTextToReplace.Trim.Length > 0) Then
                szOriText = szOriText.Replace(szTextToFind, szTextToReplace)
            Else
                szOriText = szOriText.Replace(szTextToFind, "")
            End If
        End If

    End Function

    '********************************************************************************************
    ' Class Name:		Common
    ' Function Name:	szPad
    ' Function Type:	String
    ' Parameter:		sz_Src     - Source string to be padded
    '                   i_Length   - Length of output string
    '                   sz_PadSide - Which side to pad
    '                   sz_PadChar - What character to pad
    ' Description:		Perform padding
    ' History:			16 Nov 2010 
    '********************************************************************************************
    Public Function szPad(ByVal sz_Src As String, ByVal i_Length As Integer, ByVal sz_PadSide As String, ByVal sz_PadChar As String) As String
        If ("R" = sz_PadSide.ToUpper) Then
            sz_Src = sz_Src.PadRight(i_Length, sz_PadChar)
        ElseIf ("L" = sz_PadSide.ToUpper) Then
            sz_Src = sz_Src.PadLeft(i_Length, sz_PadChar)
        End If

        Return sz_Src
    End Function

    '********************************************************************************************
    ' Class Name:		Common
    ' Function Name:	szReplaceSpecialChar
    ' Function Type:	String
    ' Parameter:		sz_Src     - Source string to be replace
    '                   sz_Type    - C for convert special character to tag, R is convert tag back to special character
    ' Description:		Replace special char to specific tag or vice versa
    ' History:			23 Sept 2011  
    ' Modified:         3 Dec 2015  Installment.
    '********************************************************************************************
    Public Function szReplaceSpecialChar(ByVal sz_Src As String, Optional ByVal sz_Type As String = "") As String
        Dim SpecialChars As Char() = "!@#$%^&*(){}[]""_+<>?/-:.'".ToCharArray()  'Modified  2 Jun 2016, added apostrophe, add fullstop on 30 Nov 2018
        Dim sb As New Text.StringBuilder

        Try
            If sz_Type = "C" Then
                sz_Src = sz_Src.Replace("!", "[EXC]") '.Replace("?", "[QUE]").Replace("#", "[HASH]")

            ElseIf sz_Type = "R" Then 'Type R will replace tag to special character. Current Regex is not function in proper way because of "[]" this had been use as pattern define
                sz_Src = sz_Src.Replace("[EXC]", "!") '.Replace("[QUE]", "?").Replace("[HASH]", "#")

            Else
                For Each ch As Char In sz_Src
                    If Array.IndexOf(SpecialChars, ch) = -1 Then
                        sb.Append(ch)
                    End If
                Next

                sz_Src = sb.ToString().Trim() 'Remove space for string have space in between. example: PhoneNo=(18) 2238990821. Modified  25 May 2016, removed .Replace(" ", ""), for FPXD payment desc not accepts special characters
            End If

        Catch ex As Exception
            sz_Src = ""
        End Try

        Return sz_Src
    End Function

    '********************************************************************************************
    ' Class Name:		Common
    ' Function Name:	szFormatStringRestructure
    ' Function Type:	String
    ' Parameter:		asz_Src, sz_Value    - Source string to be restructure
    ' Description:		Restruture the string with subtring function and return a new string output
    ' History:			8 Dec 2011 
    '********************************************************************************************
    Public Function szFormatStringRestructure(ByVal asz_Src() As String, ByVal sz_Value As String) As String
        Dim szOutput As String = ""
        Dim szSubStr As String = ""
        Dim aszIndicator() As String
        Dim i As Integer

        Try
            'input asz_Src example = R:2 / L:2 / M:2:2 / [param3]
            For i = 0 To asz_Src.GetUpperBound(0) - 1
                aszIndicator = Split(asz_Src(i), ":")
                Select Case aszIndicator(0).ToUpper()
                    Case "R"
                        szSubStr = Right(sz_Value, aszIndicator(1))
                    Case "L"
                        szSubStr = Left(sz_Value, aszIndicator(1))
                    Case "M"
                        szSubStr = sz_Value.Substring(aszIndicator(1), aszIndicator(2))
                    Case "F"            'Added  30 June 2014, GlobalPay Cybersource
                        szSubStr = aszIndicator(1)
                End Select
                szOutput += szSubStr
            Next

        Catch ex As Exception
            szOutput = ""
        End Try

        Return szOutput
    End Function

    '********************************************************************************************
    ' Class Name:		Common
    ' Function Name:	szMask
    ' Function Type:	String
    ' Parameter:		asz_Src, sz_Value    - Source string to be mask
    ' Description:		Mask the string and return masked string output
    ' History:			11 March 2015  - eBPG Maybank-CC
    '********************************************************************************************
    Public Function szMask(ByVal asz_Src() As String, ByVal sz_Value As String) As String
        Dim szOutput As String = ""
        Dim szSubStr As String = ""
        Dim szMaskStr As String = ""
        Dim szInsert As String = ""
        Dim aszIndicator() As String
        Dim i As Integer
        Dim j As Integer

        Try
            'input asz_Src example = R:2 / L:2 / M:1:2 / [field to mask. eg.cvv2=123 -> result: 1xx / xx3 / 1xx]
            'Explanation: R:2 mean mask right 2 character, L:2 mean mask left 2 character, M:1:2 mean mask from index 1 count 2 character.
            For i = 0 To asz_Src.GetUpperBound(0) - 1
                aszIndicator = Split(asz_Src(i), ":")
                Select Case aszIndicator(0).ToUpper()
                    Case "R"
                        szSubStr = Microsoft.VisualBasic.Strings.Left(sz_Value, sz_Value.Length() - aszIndicator(1))
                        szMaskStr = szSubStr.PadRight(sz_Value.Length(), "x")
                    Case "L"
                        szSubStr = Microsoft.VisualBasic.Strings.Right(sz_Value, sz_Value.Length() - aszIndicator(1))
                        szMaskStr = szSubStr.PadLeft(sz_Value.Length(), "x")
                    Case "M"
                        szSubStr = sz_Value.Remove(aszIndicator(1), aszIndicator(2))
                        For j = 0 To aszIndicator(2) - 1
                            szInsert = szInsert + "x"
                        Next
                        szMaskStr = szSubStr.Insert(aszIndicator(1), szInsert)
                End Select
            Next

            szOutput = szMaskStr

        Catch ex As Exception
            szOutput = ""
        End Try

        Return szOutput
    End Function

    '********************************************************************************************
    ' Class Name:		Common
    ' Function Name:	szMaskRev
    ' Function Type:	String
    ' Parameter:		asz_Src, sz_Value    - Source string to be mask
    ' Description:		Reverse Mask string and return masked string output
    ' History:			9 April 2015  - eBPG Maybank-CC
    '********************************************************************************************
    Public Function szRevMask(ByVal asz_Src() As String, ByVal sz_Value As String) As String
        Dim szOutput As String = ""
        Dim szSubStr As String = ""
        Dim szMaskStr As String = ""
        Dim szInsert As String = ""
        Dim aszIndicator() As String
        Dim i As Integer
        Dim j As Integer

        Try
            'input asz_Src example = R:2 / L:2 / M:1:2 / [field to reverse mask. eg.cvv2=1234 -> result: xx34 / 12xx / x23x]
            'Explanation: R:2 mean right 2 character NOT mask, L:2 mean left 2 character NOT mask, M:1:2 mean character from index 1 count 2 NOT mask
            For i = 0 To asz_Src.GetUpperBound(0) - 1
                aszIndicator = Split(asz_Src(i), ":")
                Select Case aszIndicator(0).ToUpper()
                    Case "R"
                        szSubStr = Microsoft.VisualBasic.Strings.Right(sz_Value, sz_Value.Length() - (sz_Value.Length() - aszIndicator(1)))
                        szMaskStr = szSubStr.PadLeft(sz_Value.Length(), "x")
                    Case "L"
                        szSubStr = Microsoft.VisualBasic.Strings.Left(sz_Value, sz_Value.Length() - (sz_Value.Length() - aszIndicator(1)))
                        szMaskStr = szSubStr.PadRight(sz_Value.Length(), "x")
                    Case "M"
                        szSubStr = sz_Value.Substring(aszIndicator(1), aszIndicator(2))
                        szMaskStr = szSubStr.PadLeft(Convert.ToInt16(aszIndicator(1)) + Convert.ToInt16(aszIndicator(2)), "x").PadRight(sz_Value.Length, "x")
                End Select
            Next

            szOutput = szMaskStr

        Catch ex As Exception
            szOutput = ""
        End Try

        Return szOutput
    End Function

    '********************************************************************************************
    ' Class Name:		Common
    ' Function Name:	bGetHostInfo
    ' Function Type:	Boolean.  True if get host information successfully else false
    ' Parameter:		st_HostInfo, st_PayInfo
    ' Description:		Get host information
    ' History:			24 Nov 2013
    '********************************************************************************************
    Public Function bGetHostInfo(ByRef st_HostInfo As Common.STHost, ByRef st_PayInfo As Common.STPayInfo) As Boolean
        'Modified 13 Aug 2015, added "WA"
        If ("OB" = st_PayInfo.szPymtMethod.ToUpper() Or "WA" = st_PayInfo.szPymtMethod.ToUpper() Or "OTC" = st_PayInfo.szPymtMethod.ToUpper()) Then ' Modified  27 Jan 2015, OTC
            Return m_objTxnProcOB.bGetHostInfo(st_HostInfo, st_PayInfo)
        Else
            Return m_objTxnProc.bGetHostInfo(st_HostInfo, st_PayInfo)
        End If
    End Function

    Public Function bUpdateHostInfo(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost) As Boolean
        If ("OB" = st_PayInfo.szPymtMethod.ToUpper() Or "WA" = st_PayInfo.szPymtMethod.ToUpper()) Then
            Return m_objTxnProcOB.bUpdateHostInfo(st_PayInfo, st_HostInfo)
        Else
            Return m_objTxnProc.bUpdateHostInfo(st_PayInfo, st_HostInfo)
        End If
    End Function

    Public Function bGetTxnStatusAction(ByRef st_HostInfo As Common.STHost, ByRef st_PayInfo As Common.STPayInfo) As Boolean
        If ("OB" = st_PayInfo.szPymtMethod.ToUpper() Or "WA" = st_PayInfo.szPymtMethod.ToUpper() Or "OTC" = st_PayInfo.szPymtMethod.ToUpper()) Then 'Modified  27 Jan 2015, OTC
            Return m_objTxnProcOB.bGetTxnStatusAction(st_HostInfo, st_PayInfo)
        Else
            Return m_objTxnProc.bGetTxnStatusAction(st_HostInfo, st_PayInfo)
        End If
    End Function

    Public Function bGetTerminal(ByRef st_HostInfo As Common.STHost, ByRef st_PayInfo As Common.STPayInfo) As Boolean
        If ("OB" = st_PayInfo.szPymtMethod.ToUpper() Or "WA" = st_PayInfo.szPymtMethod.ToUpper() Or "OTC" = st_PayInfo.szPymtMethod.ToUpper()) Then 'Modified  27 Jan 2015, OTC
            Return m_objTxnProcOB.bGetTerminal(st_HostInfo, st_PayInfo)
        Else
            Return m_objTxnProc.bGetTerminal(st_HostInfo, st_PayInfo)
        End If
    End Function

    'Added, 16 Apr 2014
    Public Function bGetTerminalEx(ByRef st_HostInfo As Common.STHost, ByRef st_PayInfo As Common.STPayInfo) As Boolean
        If ("OB" = st_PayInfo.szPymtMethod.ToUpper() Or "WA" = st_PayInfo.szPymtMethod.ToUpper()) Then
            Return m_objTxnProcOB.bGetTerminalEx(st_HostInfo, st_PayInfo)
        Else
            Return m_objTxnProc.bGetTerminalEx(st_HostInfo, st_PayInfo)
        End If
    End Function

    Public Function bGetMerchantHostRunningNo(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost) As Boolean
        If ("OB" = st_PayInfo.szPymtMethod.ToUpper() Or "WA" = st_PayInfo.szPymtMethod.ToUpper()) Then
            Return m_objTxnProcOB.bGetMerchantHostRunningNo(st_PayInfo, st_HostInfo)
        Else
            Return m_objTxnProc.bGetMerchantHostRunningNo(st_PayInfo, st_HostInfo)
        End If
    End Function

    Public Function bInsertNewTxn(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost) As Boolean
        If ("OB" = st_PayInfo.szPymtMethod.ToUpper() Or "WA" = st_PayInfo.szPymtMethod.ToUpper() Or "OTC" = st_PayInfo.szPymtMethod.ToUpper()) Then ' Modified  27 Jan 2015, OTC
            Return m_objTxnProcOB.bInsertNewTxn(st_PayInfo, st_HostInfo)
        Else
            Return m_objTxnProc.bInsertNewTxn(st_PayInfo, st_HostInfo)
        End If
    End Function

    Public Function bGetTxnResult(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost) As Boolean
        If ("OB" = st_PayInfo.szPymtMethod.ToUpper() Or "WA" = st_PayInfo.szPymtMethod.ToUpper() Or "OTC" = st_PayInfo.szPymtMethod.ToUpper()) Then ' Modified  27 Jan 2015, OTC
            Return m_objTxnProcOB.bGetTxnResult(st_PayInfo, st_HostInfo)
        Else
            Return m_objTxnProc.bGetTxnResult(st_PayInfo, st_HostInfo)
        End If
    End Function

    Public Function bGetTxnResult3(ByRef st_PayInfo As Common.STPayInfo) As Boolean
        If ("OB" = st_PayInfo.szPymtMethod.ToUpper() Or "WA" = st_PayInfo.szPymtMethod.ToUpper() Or "OTC" = st_PayInfo.szPymtMethod.ToUpper()) Then ' Modified  27 Jan 2015, OTC
            Return m_objTxnProcOB.bGetTxnResult3(st_PayInfo)
        Else
            Return m_objTxnProc.bGetTxnResult3(st_PayInfo)
        End If
    End Function

    Public Function bUpdateTxnParams(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost) As Boolean
        If ("OB" = st_PayInfo.szPymtMethod.ToUpper() Or "WA" = st_PayInfo.szPymtMethod.ToUpper()) Then
            Return m_objTxnProcOB.bUpdateTxnParams(st_PayInfo)
        Else
            Return m_objTxnProc.bUpdateTxnParams(st_PayInfo, st_HostInfo)
        End If
    End Function

    Public Function bUpdateTxnStatus(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost) As Boolean
        If ("OB" = st_PayInfo.szPymtMethod.ToUpper() Or "WA" = st_PayInfo.szPymtMethod.ToUpper() Or "OTC" = st_PayInfo.szPymtMethod.ToUpper()) Then 'Modified 19 Apr 2015, added checking of Method OTC
            Return m_objTxnProcOB.bUpdateTxnStatus(st_PayInfo, st_HostInfo)
        Else
            Return m_objTxnProc.bUpdateTxnStatus(st_PayInfo, st_HostInfo)
        End If
    End Function

    Public Function bUpdateTxnStatusEx(ByRef st_PayInfo As Common.STPayInfo) As Boolean
        If ("OB" = st_PayInfo.szPymtMethod.ToUpper() Or "WA" = st_PayInfo.szPymtMethod.ToUpper()) Then
            Return m_objTxnProcOB.bUpdateTxnStatusEx(st_PayInfo)
        Else
            Return m_objTxnProc.bUpdateTxnStatusEx(st_PayInfo)
        End If
    End Function

    Public Function bInsertTxnResp(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost) As Boolean
        If ("OB" = st_PayInfo.szPymtMethod.ToUpper() Or "WA" = st_PayInfo.szPymtMethod.ToUpper() Or "OTC" = st_PayInfo.szPymtMethod.ToUpper()) Then ' Modified OTC, 24 Nov 2016, OTC
            If (True = m_objTxnProcOB.bInsertTxnResp(st_PayInfo, st_HostInfo)) Then
                'Added, 19 Dec 2013, update PG..TB_PayTxnRef
                m_objTxnProc.bUpdatePayTxnRef(st_PayInfo, st_HostInfo)

                Return True
            Else
                Return False
            End If
        Else
            Return m_objTxnProc.bInsertTxnResp(st_PayInfo, st_HostInfo)
        End If
    End Function

    Public Function bGetReqTxn(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost) As Boolean
        If ("OB" = st_PayInfo.szPymtMethod.ToUpper() Or "WA" = st_PayInfo.szPymtMethod.ToUpper() Or "OTC" = st_PayInfo.szPymtMethod.ToUpper()) Then ' Modified  27 Jan 2015, OTC
            Return m_objTxnProcOB.bGetReqTxn(st_PayInfo, st_HostInfo)
        Else
            Return m_objTxnProc.bGetReqTxn(st_PayInfo, st_HostInfo)
        End If
    End Function

    Public Function bGetResTxn(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost) As Boolean
        If ("OB" = st_PayInfo.szPymtMethod.ToUpper() Or "WA" = st_PayInfo.szPymtMethod.ToUpper() Or "OTC" = st_PayInfo.szPymtMethod.ToUpper()) Then ' Added OTC, 9 Mar 2017
            Return m_objTxnProcOB.bGetResTxn(st_PayInfo, st_HostInfo)
        Else
            Return m_objTxnProc.bGetResTxn(st_PayInfo, st_HostInfo)
        End If
    End Function

    'Added, 16 Apr 2014
    Public Function bGetTxnParam(ByRef st_PayInfo As Common.STPayInfo) As Boolean
        If ("OB" = st_PayInfo.szPymtMethod.ToUpper() Or "WA" = st_PayInfo.szPymtMethod.ToUpper()) Then
            Return m_objTxnProcOB.bGetTxnParam(st_PayInfo)
        Else
            Return m_objTxnProc.bGetTxnParam(st_PayInfo)
        End If
    End Function

    Public Function bGetMerchantIDbyTxnID(ByRef st_PayInfo As Common.STPayInfo) As Boolean
        If ("OB" = st_PayInfo.szPymtMethod.ToUpper() Or "WA" = st_PayInfo.szPymtMethod.ToUpper()) Then
            Return m_objTxnProcOB.bGetMerchantIDbyTxnID(st_PayInfo)
        Else
            Return m_objTxnProc.bGetMerchantIDbyTxnID(st_PayInfo)
        End If
    End Function

    'Added, 30 Oct 2014
    Public Function bCheckLateResp(ByRef st_PayInfo As Common.STPayInfo, ByVal sz_ReplyDateTime As String) As Boolean
        If ("OB" = st_PayInfo.szPymtMethod.ToUpper() Or "WA" = st_PayInfo.szPymtMethod.ToUpper() Or "OTC" = st_PayInfo.szPymtMethod.ToUpper()) Then ' Modified  27 Jan 2015, OTC
            Return m_objTxnProcOB.bCheckLateResp(st_PayInfo, sz_ReplyDateTime)
        Else
            Return m_objTxnProc.bCheckLateResp(st_PayInfo, sz_ReplyDateTime)
        End If
    End Function
    'Added 19 Jan 2016
    Public Function bGetExtResTxn(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost) As Boolean
        If ("OB" = st_PayInfo.szPymtMethod.ToUpper() Or "WA" = st_PayInfo.szPymtMethod.ToUpper() Or "OTC" = st_PayInfo.szPymtMethod.ToUpper()) Then ' Modified  27 Jan 2015, OTC
            Return m_objTxnProcOB.bGetExtResTxn(st_PayInfo, st_HostInfo, st_HostInfo.szQueryURL)
        Else
            Return m_objTxnProc.bGetExtResTxn(st_PayInfo, st_HostInfo, st_HostInfo.szQueryURL)
        End If
    End Function
    'Added 26 Feb 2016
    Public Function bCheckNInsRefundTxn(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost) As Integer
        If ("OB" = st_PayInfo.szPymtMethod.ToUpper() Or "WA" = st_PayInfo.szPymtMethod.ToUpper() Or "OTC" = st_PayInfo.szPymtMethod.ToUpper()) Then ' Modified  27 Jan 2015, OTC
            Return m_objTxnProcOB.bCheckNInsRefundTxn(st_PayInfo, st_HostInfo)
        Else
            Return m_objTxnProc.bCheckNInsRefundTxn(st_PayInfo, st_HostInfo)
        End If
    End Function
    'Added  22 Aug 2016. Auth&Capture
    Public Function bCheckNInsCaptureTxn(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost) As Integer
        If ("OB" = st_PayInfo.szPymtMethod.ToUpper() Or "WA" = st_PayInfo.szPymtMethod.ToUpper() Or "OTC" = st_PayInfo.szPymtMethod.ToUpper()) Then ' Modified  27 Jan 2015, OTC

        Else
            Return m_objTxnProc.bCheckNInsCaptureTxn(st_PayInfo, st_HostInfo)
        End If
    End Function
    'Added 26 Feb 2016
    Public Function bGetTxnResult2(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost) As Boolean
        If ("OB" = st_PayInfo.szPymtMethod.ToUpper() Or "WA" = st_PayInfo.szPymtMethod.ToUpper() Or "OTC" = st_PayInfo.szPymtMethod.ToUpper()) Then ' Modified  27 Jan 2015, OTC
            Return m_objTxnProcOB.bGetTxnResult2(st_PayInfo, st_HostInfo)
        Else
            Return m_objTxnProc.bGetTxnResult2(st_PayInfo, st_HostInfo)
        End If
    End Function
    '********************************************************************************************
    ' Class Name:		Common
    ' Function Name:	bUpdateOriTxnStatus
    ' Function Type:	Boolean. Return value
    ' Parameter:        
    ' Description:		This function update to original txnstatus
    ' History:			2 March 2016
    '********************************************************************************************
    Public Function bUpdateOriTxnStatus(ByRef st_PayInfo As Common.STPayInfo) As Boolean
        If ("OB" = st_PayInfo.szPymtMethod.ToUpper() Or "WA" = st_PayInfo.szPymtMethod.ToUpper()) Then
            Return m_objTxnProcOB.bUpdateOriTxnStatus(st_PayInfo)
        Else
            Return m_objTxnProc.bUpdateOriTxnStatus(st_PayInfo)
        End If
    End Function
    '********************************************************************************************
    ' Class Name:		Common
    ' Function Name:	bUpdateRefundTxnStatus
    ' Function Type:	Boolean. Return value
    ' Parameter:        
    ' Description:		This function update to refund status
    ' History:			2 March 2016
    '********************************************************************************************
    Public Function bUpdateRefundTxnStatus(ByRef st_PayInfo As Common.STPayInfo) As Boolean
        If ("OB" = st_PayInfo.szPymtMethod.ToUpper() Or "WA" = st_PayInfo.szPymtMethod.ToUpper()) Then
            Return m_objTxnProcOB.bUpdateRefundTxnStatus(st_PayInfo)
        Else
            Return m_objTxnProc.bUpdateRefundTxnStatus(st_PayInfo)
        End If
    End Function
    '********************************************************************************************
    ' Class Name:		Common
    ' Function Name:	bGetRefundResTxn
    ' Function Type:	Boolean. Return value
    ' Parameter:        
    ' Description:		This function get response to refund status
    ' History:			2 March 2016
    '********************************************************************************************
    Public Function bGetRefundResTxn(ByRef st_PayInfo As Common.STPayInfo) As Boolean
        If ("OB" = st_PayInfo.szPymtMethod.ToUpper() Or "WA" = st_PayInfo.szPymtMethod.ToUpper()) Then
            Return m_objTxnProcOB.bGetRefundResTxn(st_PayInfo)
        End If
    End Function
    '********************************************************************************************
    ' Class Name:		Common
    ' Function Name:	bMapBankRespCode
    ' Function Type:	Boolean. Return value
    ' Parameter:        
    ' Description:		This function get response message with bank ref code
    ' History:			05 May 2016
    '********************************************************************************************
    Public Function bMapBankRespCode(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost, ByVal sz_BankRespCode As String) As Boolean
        If ("OB" = st_PayInfo.szPymtMethod.ToUpper() Or "WA" = st_PayInfo.szPymtMethod.ToUpper()) Then

        Else
            Return m_objTxnProc.bMapBankRespCode(st_PayInfo, st_HostInfo, sz_BankRespCode)
        End If
    End Function

    '********************************************************************************************
    ' Class Name:		Common
    ' Function Name:	bCheckExpCurrency
    ' Function Type:	Boolean. Return value
    ' Parameter:        
    ' Description:		Get currency exponent from CL_CurrencyCode table based on Currency Code
    ' History:			15 Feb 2017
    '********************************************************************************************
    Public Function bCheckExpCurrency(ByVal sz_CurrencyCode As String, ByVal i_HostID As Integer, ByRef st_PayInfo As Common.STPayInfo) As Boolean
        If ("OB" = st_PayInfo.szPymtMethod.ToUpper() Or "WA" = st_PayInfo.szPymtMethod.ToUpper() Or "OTC" = st_PayInfo.szPymtMethod.ToUpper()) Then
            Return m_objTxnProcOB.bCheckExpCurrency(sz_CurrencyCode, i_HostID)
        Else
            Return m_objTxnProc.bCheckExpCurrency(sz_CurrencyCode, i_HostID)
        End If
    End Function

    '********************************************************************************************
    ' Class Name:		Common
    ' Function Name:	bGetTxnStatusActionEx
    ' Function Type:	Boolean. Return value
    ' Parameter:        
    ' Description:		This function get response message with bank ref code
    ' History:			08 Nov 2016
    '********************************************************************************************
    Public Function bGetTxnStatusActionEx(ByRef sz_TxnField As String, ByRef sz_TxnStatus As String, ByRef sz_TxnAction As String, ByRef st_HostInfo As Common.STHost, ByRef st_PayInfo As Common.STPayInfo) As Boolean
        If ("OB" = st_PayInfo.szPymtMethod.ToUpper() Or "WA" = st_PayInfo.szPymtMethod.ToUpper() Or "OTC" = st_PayInfo.szPymtMethod.ToUpper()) Then 'Modified  27 Jan 2015, OTC
            Return m_objTxnProcOB.bGetTxnStatusActionEx(sz_TxnField, sz_TxnStatus, sz_TxnAction, st_HostInfo, st_PayInfo)
        Else
            Return m_objTxnProc.bGetTxnStatusActionEx(sz_TxnField, sz_TxnStatus, sz_TxnAction, st_HostInfo, st_PayInfo)
        End If
    End Function
    '********************************************************************************************
    ' Class Name:		Common
    ' Function Name:	bInsertLateResponse
    ' Function Type:	Boolean. Return value
    ' Parameter:        
    ' Description:		This function insert late response
    ' History:			22 Sep 2017
    '********************************************************************************************
    Public Function bInsertLateResponse(ByRef st_PayInfo As Common.STPayInfo) As Boolean
        If ("OB" = st_PayInfo.szPymtMethod.ToUpper() Or "WA" = st_PayInfo.szPymtMethod.ToUpper() Or "OTC" = st_PayInfo.szPymtMethod.ToUpper()) Then
            Return m_objTxnProcOB.bInsertLateResponse(st_PayInfo)
        End If
    End Function

    '********************************************************************************************
    ' Class Name:		Common
    ' Function Name:	szGetTagValue
    ' Function Type:	String. Return value
    ' Parameter:        sz_SOAPMesg [in, string]    -  string to be searched from
    '                   sz_Tag2Search [in, string]  - string to be searched
    ' Description:		This function search tag value from SOAP message
    ' History:			30 Oct 2013
    '********************************************************************************************
    Public Function szGetTagValue(ByVal sz_SOAPMesg As String, ByVal sz_Tag2Search As String) As String
        Dim szResult As String = ""
        Dim szTemp As String = ""   'Added  18 Nov 2013
        Dim iStart As Integer = 0   'Added  18 Nov 2013

        Try
            szTemp = sz_SOAPMesg    'Added  18 Nov 2013, stores the original SOAP message containing original case

            Using reader As XmlReader = XmlReader.Create(New StringReader(sz_SOAPMesg.ToLower()))
                If (True = reader.ReadToFollowing(sz_Tag2Search.ToLower())) Then
                    szResult = reader.ReadElementContentAsString()
                    iStart = InStr(1, szTemp, szResult, CompareMethod.Text)     'Added  18 Nov 2013, CompareMethod.Text is case NOT sensitive
                    szResult = szTemp.Substring(iStart - 1, szResult.Length)    'Added  18 Nov 2013, 
                Else
                    szResult = ""
                End If
            End Using

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            " > " + sz_Tag2Search + " value(" + szResult + ") from SOAP")

        Catch ex As Exception
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            " > Exception: " + ex.Message + " - unable to complete searching " + sz_Tag2Search + " from SOAP")
        End Try

        Return szResult
    End Function

    '********************************************************************************************
    ' Class Name:		Common
    ' Function Name:	ContainsSpecialChars
    ' Function Type:	
    ' Parameter:		s - string
    ' Description:		
    ' History:			19 Nov 2018
    '********************************************************************************************
    Public Function ContainsSpecialChars(s As String) As Boolean
        'Return s.IndexOfAny("[~`!@#$%^&*()[]""_+=|{}'-:;.,<>/?]".ToCharArray) <> -1
        Return s.IndexOfAny("[~`!@#$%^&*()[]""_+=|{}':;.,<>/?]".ToCharArray) <> -1
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	Constructor
    ' Function Type:	
    ' Parameter:		o_TxnProc, o_TxnProcOB
    ' Description:		
    ' History:			24 Nov 2013
    '********************************************************************************************
    Public Sub New(ByVal o_TxnProc As TxnProc, ByVal o_TxnProcOB As TxnProcOB, ByRef o_LoggerII As LoggerII.CLoggerII)
        Try
            m_objTxnProc = o_TxnProc
            m_objTxnProcOB = o_TxnProcOB

            objLoggerII = o_LoggerII
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	Constructor
    ' Function Type:	
    ' Parameter:		o_TxnProc
    ' Description:		
    ' History:			20 Oct 2017
    '********************************************************************************************
    Public Sub New(ByVal o_TxnProc As TxnProc, ByRef o_LoggerII As LoggerII.CLoggerII)
        Try
            m_objTxnProc = o_TxnProc

            objLoggerII = o_LoggerII
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	Constructor
    ' Function Type:	
    ' Parameter:		None
    ' Description:		
    ' History:			24 Nov 2013
    '********************************************************************************************
    Public Sub New(ByRef o_LoggerII As LoggerII.CLoggerII)
        Try
            objLoggerII = o_LoggerII
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class

