﻿Imports LoggerII
Imports System.IO
Imports System
Imports System.Net
Imports System.Threading

Public Class TestPage
    Inherits System.Web.UI.Page

    Private objLoggerII As LoggerII.CLoggerII

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim iRet As Integer = -1
        Dim iTimeout As Integer
        Dim objHttpWebRequest As HttpWebRequest = Nothing
        Dim objHTTPWebResponse As HttpWebResponse = Nothing
        Dim objStreamReader As StreamReader = Nothing
        Dim szStr As String = ""
        'Dim szHTTPStrArray() As String     'Commented by Jeff, 15 Aug 2013
        'Dim szFieldArray() As String
        Dim iStartTickCount As Integer
        'Dim iLoop As Integer
        Dim iResCode As Integer
        Dim sz_URL As String = "https://migs.mastercard.com.au/vpcpay"
        Dim sz_HTTPString As String = "vpc_Version=1&vpc_Command=queryDR&vpc_AccessCode=60C8349D&vpc_MerchTxnRef=TIS000GHLSIT13110800002&vpc_Merchant=TEST028600027000&vpc_User=AMAuser0&vpc_Password=abc1234"
        Dim sz_HTTPResponse As String = ""

        If Not Page.IsPostBack Then
            Try
                '======================================== Sets HttpWebRequest properties ====================================
                iTimeout = 60 * 1000 'converts input timeout param from second to milliseconds

                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "TESTING PAGE")

                ServicePointManager.CertificatePolicy = New SecPolicy

                iStartTickCount = System.Environment.TickCount()

                '=========================================== Post(GET) request ==================================================
                ' Create the web request   
                objHttpWebRequest = DirectCast(WebRequest.Create(sz_URL + "?" + sz_HTTPString), HttpWebRequest)  ' Sends data
                objHttpWebRequest.CookieContainer = New CookieContainer()
                objHttpWebRequest.KeepAlive = False
                objHttpWebRequest.Timeout = iTimeout    '(in milliseconds)

                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Finished posting HTTP data")

                '============================================= Get response =====================================================
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Getting HTTP response....")

                ' Get response   
                objHTTPWebResponse = DirectCast(objHttpWebRequest.GetResponse(), HttpWebResponse)

                If (objHttpWebRequest.HaveResponse And objHTTPWebResponse.StatusCode = HttpStatusCode.OK) Then
                    objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "HTTP response received.")

                    ' Get the response stream into a reader   
                    objStreamReader = New StreamReader(objHTTPWebResponse.GetResponseStream())

                    szStr = objStreamReader.ReadToEnd()

                    '================ Implements Timeout for getting HTTP reply from host <START> ====================
                    Dim objRespReader As RespReader
                    Dim oThreadStart As ThreadStart
                    Dim oThread As Thread
                    Dim iElapsedTime As Integer

                    objRespReader = New RespReader
                    objRespReader.SetLogger(objLoggerII)
                    objRespReader.SetReader(objStreamReader)

                    oThreadStart = New ThreadStart(AddressOf objRespReader.WaitForResp)
                    oThread = New Thread(oThreadStart)
                    oThread.Start()

                    'iStartTickCount = System.Environment.TickCount()
                    iElapsedTime = System.Environment.TickCount() - iStartTickCount

                    While ((iElapsedTime < iTimeout) And Not objRespReader.IsCompleted())
                        System.Threading.Thread.Sleep(50)
                        iElapsedTime = System.Environment.TickCount() - iStartTickCount
                    End While
                    objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp Elapsed Time(ms): " & iElapsedTime.ToString())

                    If (iElapsedTime >= iTimeout) Then
                        ' Timeout
                        iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp Timeout, iRet(" + iRet.ToString() + ")")
                    Else
                        'Completed AND No Timeout. Could be got reply or exception
                        If (True = objRespReader.IsSucceeded()) Then
                            ' Console application output   
                            sz_HTTPResponse = szStr
                        Else
                            'Exception within timeout period
                            iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp exception within timeout period, iRet(" + iRet.ToString() + ")")
                        End If
                    End If

                    oThread.Abort()
                    oThreadStart = Nothing
                    oThread = Nothing
                    'objRespReader.Dispose()
                    objRespReader = Nothing

                    'Releases the resources of the response stream
                    'NOTE: Failure to close the stream will cause application to run out of connections.
                    objStreamReader.Close()
                    If Not objStreamReader Is Nothing Then objStreamReader = Nothing
                    '================== Implements Timeout for getting HTTP reply from host <END> =====================
                Else
                    objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "ERROR: Response was not received.")
                End If

                'Releases the resources of the response
                objHTTPWebResponse.Close()

                iRet = 0

            Catch webEx As WebException
                Try
                    objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException: " + webEx.ToString)

                    iRet = Common.ERROR_CODE.E_COM_URL_UNREACHABLE

                    If webEx.Status = WebExceptionStatus.Timeout Then
                        iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException: Timeout")
                    End If

                    If webEx.Status = WebExceptionStatus.ProtocolError Then
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Response Status Code :" + CStr(CType(webEx.Response, HttpWebResponse).StatusCode))

                        'Re: HttpStatusCode Enumeration
                        iResCode = CInt(CType(webEx.Response, HttpWebResponse).StatusCode)
                        If iResCode = 400 Then
                            iRet = Common.ERROR_CODE.E_COM_BAD_REQUEST
                            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Bad Request could not be understood by the server.")
                        ElseIf iResCode = 401 Then
                            iRet = Common.ERROR_CODE.E_COM_UNAUTHORIZED_URL
                            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Unauthorized, the requested resource requires authentication.")
                        ElseIf iResCode = 403 Then
                            iRet = Common.ERROR_CODE.E_COM_FORBIDDEN_URL
                            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Forbidden, the server refuses to fulfill the request.")
                        ElseIf iResCode = 404 Then
                            iRet = Common.ERROR_CODE.E_COM_URL_UNREACHABLE
                            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Not Found, the requested resource does not exist on the server.")
                        End If
                    End If
                Catch exp As Exception
                    objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Exception: " + exp.ToString)
                End Try

            Catch ioEx As IOException
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "IO Exception: " + ioEx.ToString)
                iRet = Common.ERROR_CODE.E_COM_FILE_ERROR

            Catch ex As Exception
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Exception: " + ex.ToString)
                iRet = Common.ERROR_CODE.E_COMMON_UNKNOWN_ERROR

            Finally
                'Commented by Jeff, 15 Aug 2013
                'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "iRet: " + CStr(iRet))
            End Try


        End If
    End Sub
End Class