Imports System.IO
Imports LoggerII

Public Class RespReader
    'Implements IDisposable

    Private objStreamReader As StreamReader
    Private objLoggerII As LoggerII.CLoggerII
    Private bCompleted As Boolean
    Private bSucceeded As Boolean
    Private szOutStream As String

    Public Sub New()
        objStreamReader = Nothing
        objLoggerII = Nothing
        bCompleted = False
        bSucceeded = False
        szOutStream = ""
    End Sub

    'Public Sub Dispose() Implements System.IDisposable.Dispose
        ' Calling GC.SuppressFinalize in Dispose is an important optimization to ensure
        ' resources released promptly.
    '    GC.SuppressFinalize(Me)
    'End Sub

    Public Sub SetReader(ByRef o_Reader As StreamReader)
        objStreamReader = o_Reader
    End Sub

    Public Function IsCompleted()
        IsCompleted = bCompleted
    End Function

    Public Function IsSucceeded()
        IsSucceeded = bSucceeded
    End Function

    Public Function GetRespStream()
        GetRespStream = szOutStream
    End Function

    Public Sub SetLogger(ByRef o_LoggerII As LoggerII.CLoggerII)
        objLoggerII = o_LoggerII
    End Sub

    Public Sub WaitForResp()
        bCompleted = False
        bSucceeded = False
        szOutStream = ""

        Try
            ' This function will wait forever until terminated abruptly or exception
            szOutStream = objStreamReader.ReadToEnd
            bSucceeded = True
            objLoggerII.Log(objLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp() Response stream completely received")   ' + szOutStream)    'Commented  7 Oct 2013, szOutStream

        Catch ex As Exception
            objLoggerII.Log(objLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp() exception: " + ex.Message)
            bSucceeded = False
        Finally
            bCompleted = True
            'objLoggerII.Log(objLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp() Completed with bSucceeded(" + CStr(bSucceeded) + ")")      'Commented  7 Oct 2013
        End Try
    End Sub
End Class
