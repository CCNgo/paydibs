﻿Imports LoggerII
Imports System.Text.RegularExpressions

' ******************************************
' Added By: 
' Date Created: 30 Dec 2016
' Description: Class for identifying the Card Type via Regular Expressions
' ******************************************
Public Class CardTypeIdentifier

    Private objLoggerII As LoggerII.CLoggerII
    Public CardTypes As New Microsoft.VisualBasic.Collection()
    Public RegexCardTypes As New Microsoft.VisualBasic.Collection()
    Public RegexAcqBIN As New Microsoft.VisualBasic.Collection()

    ' ******************************************
    ' Added By: 
    ' Date Created: 30 Dec 2016
    ' Function Type: Constructor
    ' Description: Initializes the collections to be used for processing the identification of card type
    ' ******************************************
    Public Sub New()
        Try
            ' Adding Card Types
            If (0 = CardTypes.Count) Then
                CardTypes.Add("V")      'Visa
                CardTypes.Add("M")      'MasterCard
                CardTypes.Add("A")      'AmericanExpress
                CardTypes.Add("D")      'DinersClub
                CardTypes.Add("D2")     'Discover
                CardTypes.Add("J")      'JCB
            End If

            ' Adding Regular Expressions for Card Types
            If (0 = RegexCardTypes.Count) Then
                ' All Visa card numbers start with a 4. New cards have 16 digits. Old cards have 13.
                RegexCardTypes.Add("^4[0-9]{12}(?:[0-9]{3})?$", "V")

                ' MasterCard numbers either start with the numbers 51 through 55 or with the numbers 2221 through 2720. All have 16 digits.
                RegexCardTypes.Add("^(?:5[1-5][0-9]{2}|222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12}$", "M")

                ' American Express card numbers start with 34 or 37 and have 15 digits.
                RegexCardTypes.Add("^3[47][0-9]{13}$", "A")

                ' Diners Club card numbers begin with 300 through 305, 36 or 38. All have 14 digits. There are Diners Club cards that begin with 5 and have 16 digits. These are a joint venture between Diners Club and MasterCard, and should be processed like a MasterCard.
                RegexCardTypes.Add("^3(?:0[0-5]|[68][0-9])[0-9]{11}$", "D")

                ' Discover card numbers begin with 6011 or 65. All have 16 digits.
                RegexCardTypes.Add("^6(?:011|5[0-9]{2})[0-9]{12}$", "D2")

                ' JCB cards beginning with 2131 or 1800 have 15 digits. JCB cards beginning with 35 have 16 digits.
                RegexCardTypes.Add("^(?:2131|1800|35\d{3})\d{11}$", "J")
            End If

            ' Adding Regular Expressions for Acquirer Bin of Card Types
            If (0 = RegexAcqBIN.Count) Then
                ' AcqBin length goes upto 11 but usually its 6

                ' Visa: BIN start with a 4.
                RegexAcqBIN.Add("^4[0-9]{5,10}$", "V")

                ' Master Card: BIN start with the numbers 51 through 55.
                RegexAcqBIN.Add("^5[1-5]{1}[0-9]{4,9}$", "M")

                ' American Express: BIN start with a 37.
                RegexAcqBIN.Add("^37[0-9]{4,9}$", "A")

                ' Diners Club: BIN begin with 300 through 305, 36 or 38.
                RegexAcqBIN.Add("^(?:36[0-9]{2}|38[0-9]{2}|30[0-5][0-9])[0-9]{2,7}$", "D")

                ' Discover: BIN begin with 6011 or 65.
                RegexAcqBIN.Add("^(?:65[0-9]{2}|6011)[0-9]{2,7}$", "D2")

                ' JCB: BIN begin with 35.
                RegexAcqBIN.Add("^35[0-9]{4,9}$", "J")
            End If
        Catch ex As Exception
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), _
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
                            " > Common::PreCardTypesIdentifier() Exception: " + ex.Message)
        End Try


    End Sub

    ' ******************************************
    ' Added By: 
    ' Date Created: 30 Dec 2016
    ' Function Type: Destructor
    ' Description: Clean Up collections
    ' ******************************************
    Protected Overrides Sub Finalize()
        ' Clean up system resources
        RegexAcqBIN.Clear()
        RegexCardTypes.Clear()
        CardTypes.Clear()
    End Sub

    '********************************************************************************************
    ' Class Name:		CardTypeIdentifier
    ' Function Name:	szCardTypeIdentifier
    ' Function Type:	String
    ' Parameter:		sz_CCN     - Credit Card Number
    ' Description:		Identifies the type of given CCN or returns empty string if CCN does not match any Card Type
    ' History:			30 Dec 2016 .
    ' Modified:         
    '********************************************************************************************
    Public Function szCardTypeIdentifier(ByVal sz_CCN As String) As String
        Dim szRet As String = ""
        sz_CCN = sz_CCN.Replace("-", Nothing) 'Remove dashes

        Try
            ' Looping through each CardTypes collection
            For Each CardType As Object In CardTypes
                Dim sz_Regex As String = RegexCardTypes.Item(CardType)
                Dim rgx As New Regex(sz_Regex)
                If (True = rgx.IsMatch(sz_CCN)) Then
                    szRet = CardType
                    Exit For
                End If
            Next CardType
        Catch ex As Exception
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), _
            DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
            " > CardTypeIdentifier::szCardTypeIdentifier() Exception: " + ex.Message)
        End Try

        Return szRet
    End Function

    '********************************************************************************************
    ' Class Name:		CardTypeIdentifier
    ' Function Name:	szAcqBinIdentifier
    ' Function Type:	String
    ' Parameter:		sz_BIN     - Acquirer BIN
    ' Description:		Identifies the type of given AcqBIN or returns empty string if AcqBIN does not match any Card Type
    ' History:			30 Dec 2016 .
    ' Modified:         
    '********************************************************************************************
    Public Function szAcqBinIdentifier(ByVal sz_BIN As String) As String
        Dim szRet As String = ""

        Try
            ' Looping through each CardTypes collection
            For Each CardType As Object In CardTypes
                Dim sz_Regex As String = RegexAcqBIN.Item(CardType)
                Dim rgx As New Regex(sz_Regex)
                If (True = rgx.IsMatch(sz_BIN)) Then
                    szRet = CardType
                    Exit For
                End If
            Next CardType
        Catch ex As Exception
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), _
            DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
            " > CardTypeIdentifier::szAcqBinIdentifier() Exception: " + ex.Message)
        End Try

        Return szRet
    End Function

End Class
