﻿Imports System.IO
Imports System
Imports System.Data
Imports System.Web

Partial Public Class RespFrmGW
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim szResult, szField, szValue As String
        Dim aszResult As String()
        Dim aszFV As String()

        Response.Cache.SetNoStore()

        szResult = szGetAllHTTPVal()

        aszResult = szResult.Split("&")

        szResult = "<b>Response Received</b><br>" + szResult + "<br><br><b>Response Parsed<br></b>"

        Try
            szResult = szResult + "<table border=1><tr><td bgcolor=yellow><b>Field</b></td><td bgcolor=yellow><b>Value</b></td></tr>"

            Dim iCounter As Integer
            For iCounter = 0 To aszResult.Length
                aszFV = aszResult(iCounter).Split("=")
                szField = aszFV(0)
                szValue = aszFV(1)

                szResult = szResult + "<tr><td>" + szField + "</td><td>" + szValue + "</td></tr>"
            Next
        Catch
            lblResp.Text = "Internal Error!"
        End Try

        szResult = szResult + "</table>"
        lblResp.Text = szResult

    End Sub

    Public Function szGetAllHTTPVal() As String
        Dim szFormValues As String = ""
        Dim szTempKey As String
        Dim szTempVal As String

        Dim iCount As Integer = 0
        Dim iLoop As Integer = 0

        iCount = Request.Form.Count

        If (iCount > 0) Then
            For iLoop = 1 To iCount
                szTempKey = ""
                szTempKey = Request.Form.GetKey(iLoop - 1)

                szFormValues = szFormValues + szTempKey + "="

                szTempVal = ""
                szTempVal = Server.UrlDecode(Request.Form.Get(iLoop - 1))

                If ((szTempKey <> "")) Then
                    If (iLoop = iCount) Then
                        szFormValues = szFormValues + szTempVal
                    Else
                        szFormValues = szFormValues + szTempVal + "&"
                    End If
                End If
            Next iLoop
        Else
            iCount = Request.QueryString.Count

            For iLoop = 1 To iCount
                szTempKey = ""
                szTempKey = Request.QueryString.GetKey(iLoop - 1)

                szFormValues = szFormValues + szTempKey + "="

                szTempVal = ""
                szTempVal = Server.UrlDecode(Request.QueryString.Get(iLoop - 1))

                If ((szTempKey <> "")) Then
                    If (iLoop = iCount) Then
                        szFormValues = szFormValues + szTempVal
                    Else
                        szFormValues = szFormValues + szTempVal + "&"
                    End If
                End If
            Next iLoop
        End If

        Return szFormValues
    End Function

End Class