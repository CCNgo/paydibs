﻿Imports LoggerII
Imports System.IO
Imports System
Imports System.Data
Imports System.Security.Cryptography
Imports System.Threading
Imports System.Text.RegularExpressions
Imports System.Web      ' HttpContext for Current.Response.Redirect, Current.Server.MapPath; HttpServerUtility/HttpUtility for Server.UrlEncode
Imports System.Web.Mail ' MailMessage

Public Class respVTCPay_s2s
    Inherits System.Web.UI.Page

    Public Const C_ISSUING_BANK = "VTC"
    Public Const C_TXN_TYPE = "PAY"
    Public Const C_PYMT_METHOD = "OB"
    Public g_szHTML As String = ""
    Public objLoggerII As LoggerII.CLoggerII

    Private objCommon As Common
    Private objTxnProcOB As TxnProcOB
    Private objTxnProc As TxnProc
    Private objHash As Hash
    Private objResponse As Response

    Private stHostInfo As Common.STHost
    Private stPayInfo As Common.STPayInfo

    Private szDBConnStr As String = ""
    Private szLogPath As String = ""
    Private iLogLevel As String = ""
    Private szReplyDateTime As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Put user code to initialize the page here
        Dim bGetHostInfo As Boolean = False

        Try
            'Get Host's reply date and time
            szReplyDateTime = System.DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss.fff")

            'Set to No Cache to make this page expired when user clicks the "back" button on browser
            Response.Cache.SetNoStore()

            If Not Page.IsPostBack Then
                'Initialization of HostInfo and PayInfo structures
                objCommon.InitHostInfo(stHostInfo)
                objCommon.InitPayInfo(stPayInfo)

                stPayInfo.szIssuingBank = C_ISSUING_BANK
                stPayInfo.szTxnType = C_TXN_TYPE
                stPayInfo.szPymtMethod = C_PYMT_METHOD
                stPayInfo.iHttpTimeoutSeconds = Convert.ToInt32(ConfigurationManager.AppSettings("HttpTimeoutSeconds"))

                'Get Host information - HostID, HostTemplate, NeedReplyAcknowledgement, TxnStatusActionID, HostReplyMethod, OSPymtCode
                bGetHostInfo = objTxnProcOB.bGetHostInfo(stHostInfo, stPayInfo)
                If (bGetHostInfo) Then
                    If True <> bResponseMain() Then
                        If (stPayInfo.iTxnStatus <> Common.TXN_STATUS.TXN_NOT_FOUND And stPayInfo.iTxnStatus <> Common.TXN_STATUS.LATE_HOST_REPLY And stPayInfo.iTxnStatus <> Common.TXN_STATUS.INVALID_HOST_IP And stPayInfo.iTxnStatus <> Common.TXN_STATUS.INVALID_HOST_REPLY_GATEWAYTXNID And stPayInfo.iTxnStatus <> Common.TXN_STATUS.INVALID_HOST_REPLY) Then
                            'log failed response
                            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                  DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                  stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " -S2S Invalid response received!")
                        End If
                    End If

                    'Good/Failed/Pending response already returned to merchant in bResponseMain()
                    'Error response also returned to merchant as above
                    'Now, only inserts the respective response with the final TxnStatus into database.
                    'Good/Failed/Error responses are considered as finalized and can be deleted from Request table and inserted into Response table.
                    'Pending txns that need retry need to stay in Request table for Retry Service to process accordingly.

                    If (stPayInfo.iTxnStatus <> Common.TXN_STATUS.TXN_NOT_FOUND And stPayInfo.iTxnStatus <> Common.TXN_STATUS.LATE_HOST_REPLY And stPayInfo.iTxnStatus <> Common.TXN_STATUS.INVALID_HOST_IP And stPayInfo.iTxnStatus <> Common.TXN_STATUS.INVALID_HOST_REPLY_GATEWAYTXNID And stPayInfo.iTxnStatus <> Common.TXN_STATUS.INVALID_HOST_REPLY) Then
                        'Action 3 (Retry query host); Action 4 (Retry confirm booking)
                        'Txns that belong to these two types of action do not need to be inserted into Response table yet,
                        'because once the spInsTxnResp stored procedure is called, the original request will be deleted from
                        'Request table. These txns that need retry should stay in Request table for Retry Service to process.

                        If ((stPayInfo.iAction <> 3) And (stPayInfo.iAction <> 4)) Then
                            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                            stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + "-S2S Inserting TxnResp. GatewayTxnID[" + stPayInfo.szTxnID + "] MerchantTxnStatus[" + stPayInfo.iMerchantTxnStatus.ToString() + "] TxnStatus[" + stPayInfo.iTxnStatus.ToString() + "] TxnState[" + stPayInfo.iTxnState.ToString() + "] RespMesg[" + stPayInfo.szTxnMsg + "].")

                            If (stPayInfo.szTxnID <> "" And stPayInfo.szTxnAmount <> "" And (stPayInfo.szHostTxnStatus <> "" Or stPayInfo.szRespCode <> "") And stPayInfo.szMerchantOrdID <> "") Then
                                If True <> objTxnProcOB.bInsertTxnResp(stPayInfo, stHostInfo) Then
                                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                    stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + "-S2S Error: " + stPayInfo.szErrDesc)
                                Else
                                    'Added, 21 Apr 2014, update PG..TB_PayTxnRef
                                    If True <> objTxnProc.bUpdatePayTxnRef(stPayInfo, stHostInfo) Then
                                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                        stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + "-S2S Error: " + stPayInfo.szErrDesc)
                                    End If
                                End If
                                'Added  12 Feb 2015 -S2Scallback
                                objResponse.bMerchantCallBackResp(stPayInfo, stHostInfo)
                            Else
                                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                "-S2S GatewayTxnID(" + stPayInfo.szTxnID + ")/TxnAmount(" + stPayInfo.szTxnAmount + ")/HostTxnStatus(" + stPayInfo.szHostTxnStatus + ")/RespCode(" + stPayInfo.szRespCode + ")/OrderNumber(" + stPayInfo.szMerchantOrdID + ") are empty from response. Bypass InsertTxnResp.")
                            End If
                        End If
                    End If
                Else
                    'For this scenario, most probably Datebase error, the Retry Service will query the host for status
                    'once database is up.
                    stPayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_PENDING
                    stPayInfo.iTxnStatus = Common.TXN_STATUS.TXN_PENDING
                    stPayInfo.szTxnMsg = Common.C_INVALID_HOST_902
                    stPayInfo.szErrDesc = "> Failed to get Host Info for " + stPayInfo.szIssuingBank + " " + stPayInfo.szTxnType + " response"

                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    "-S2S Error: " + stPayInfo.szErrDesc)

                    'bResponseMain() is not run, therefore, unable to get other reply values.
                    'Merchant will only get TxnType, IssuingBank, TxnState, TxnStatus, RespMesg
                    'Without MerchantPymtID, the reply is meaningless for the merchant. Therefore, no need reply.
                End If
            End If
        Catch ex As Exception
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            " > " + C_ISSUING_BANK + "-S2S response Page_Load() Exception: " + ex.Message)
        End Try
    End Sub

    '********************************************************************************************
    ' Class Name:		respVTCPay_s2s
    ' Function Name:	bResponseMain
    ' Function Type:	Boolean.  Return False if error.
    ' Parameter:		NONE
    ' Description:		A main function to get the response info and handle the process
    ' History:			22 Aug 2009
    '********************************************************************************************
    Private Function bResponseMain() As Boolean
        Dim bReturn = True

        Try
            'Log all form's response fields
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            C_ISSUING_BANK + "-S2S Response: " + szGetAllHTTPVal())


            'Get Response data
            If True <> objResponse.bGetData(stPayInfo, stHostInfo) Then
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                               DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "-S2S Error1_1 GetData: " + stPayInfo.szErrDesc)

                ' stPayInfo.szTxnMsg specified in bGetData
                stPayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                stPayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                stPayInfo.iTxnState = Common.TXN_STATE.ABORT

                'NOTE: May not be able to be inserted into Response table if mandatory fields are missing due to failure in bGetData()
                Return False
            End If

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + "-S2S > bResponseMain() > " + stPayInfo.szParam1)

            'Added  7 Jan 2011, get MerchantID, CurrencyCode, GatewayTxnID, MerchantPymtID based on Param1
            If True <> objTxnProcOB.bGetTxnFromParam(stPayInfo, stHostInfo) Then
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + "-S2S > bResponseMain() > " + stPayInfo.szErrDesc + ". Replace space with + and retry")
                Return False
            End If

            'Added  7 Jan 2011, Get Terminal account details based on Currency Code
            If True <> objTxnProcOB.bGetTerminal(stHostInfo, stPayInfo) Then
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + "-S2S > bResponseMain() > " + stPayInfo.szErrDesc)
                Return False
            End If

            If 0 <> objResponse.iSendRecvHost(stPayInfo, stHostInfo, stPayInfo.szTxnType, stHostInfo.szAcknowledgementURL) Then 'stHostInfo.szURL) Then
                Return False
            End If

            'Add check szHostTxnAmount = szFXTxnAmount and szHostGatewayTxnID =szTxnID to verify data resp correct
            Dim szTempAmt As String = Nothing
            If (stPayInfo.szHostCurrencyCode = stPayInfo.szCurrencyCode) Then
                szTempAmt = stPayInfo.szTxnAmount
            Else
                szTempAmt = stPayInfo.szFXTxnAmount
            End If

            If Not stPayInfo.szTxnID.Equals(stPayInfo.szHostGatewayTxnID) Or Not szTempAmt.Equals(stPayInfo.szHostTxnAmount) Then
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + "-S2S > bResponseMain() > Invalid VTC response data with TrxID[" + stPayInfo.szTxnID + "][" + stPayInfo.szHostGatewayTxnID + "] and Amount[" + szTempAmt + "][" + stPayInfo.szHostTxnAmount + "]")
                ' update trx status to invalid host reply
                stPayInfo.iTxnStatus = Common.TXN_STATUS.INVALID_HOST_REPLY
                Return False
            End If

            If True <> bProcessSaleRes(stPayInfo, stHostInfo, szReplyDateTime, g_szHTML) Then
                Return False
            End If

        Catch ex As Exception
            bReturn = False

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + "-S2S > bResponseMain() Exception: " + ex.Message)
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		respVTCPay_s2s
    ' Function Name:	GetAllHTTPVal
    ' Function Type:	Boolean.  Return False if error.
    ' Parameter:		NONE
    ' Description:		Get all form's or query string's values
    ' History:			22 Aug 2009
    '********************************************************************************************
    Public Function szGetAllHTTPVal() As String
        Dim szFormValues As String
        Dim szTempKey As String
        Dim szTempVal As String

        Dim iCount As Integer = 0
        Dim iLoop As Integer = 0

        szFormValues = "[User IP: " + Request.UserHostAddress + "] "

        iCount = Request.Form.Count

        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                        " -S2S > No. of Form Response Fields (" + iCount.ToString() + ")")

        If (iCount > 0) Then
            For iLoop = 1 To iCount
                szTempKey = ""
                szTempKey = Request.Form.GetKey(iLoop - 1)

                szFormValues = szFormValues + szTempKey + "="

                szTempVal = ""
                szTempVal = Server.UrlDecode(Request.Form.Get(iLoop - 1))

                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                "-S2S > Field(" + szTempKey + ") Value(" + szTempVal + ")")

                'Mask CardPAN
                If ((szTempKey <> "")) Then
                    If (szTempKey.ToLower = "cardpan") Then
                        If szTempVal.Length > 10 Then
                            szFormValues = szFormValues + szTempVal.Substring(0, 6) + "".PadRight(szTempVal.Length - 6 - 4, "X") + szTempVal.Substring(szTempVal.Length - 4, 4) + "&"
                        Else
                            szFormValues = szFormValues + "".PadRight(szTempVal.Length, "X") + "&"
                        End If
                    Else
                        If (iLoop = iCount) Then
                            szFormValues = szFormValues + szTempVal
                        Else
                            szFormValues = szFormValues + szTempVal + "&"
                        End If
                    End If
                End If
            Next iLoop
        Else
            iCount = Request.QueryString.Count

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            "-S2S > No. of QueryString Response Fields (" + iCount.ToString() + ")")

            For iLoop = 1 To iCount
                szTempKey = ""
                szTempKey = Request.QueryString.GetKey(iLoop - 1)

                szFormValues = szFormValues + szTempKey + "="

                szTempVal = ""
                szTempVal = Server.UrlDecode(Request.QueryString.Get(iLoop - 1))

                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                "-S2S > Field(" + szTempKey + ") Value(" + szTempVal + ")")

                'Mask CardPAN
                If ((szTempKey <> "")) Then
                    If (szTempKey.ToLower = "cardpan") Then
                        If szTempVal.Length > 10 Then
                            szFormValues = szFormValues + szTempVal.Substring(0, 6) + "".PadRight(szTempVal.Length - 6 - 4, "X") + szTempVal.Substring(szTempVal.Length - 4, 4) + "&"
                        Else
                            szFormValues = szFormValues + "".PadRight(szTempVal.Length, "X") + "&"
                        End If
                    Else
                        If (iLoop = iCount) Then
                            szFormValues = szFormValues + szTempVal
                        Else
                            szFormValues = szFormValues + szTempVal + "&"
                        End If
                    End If
                End If
            Next iLoop
        End If

        Return szFormValues
    End Function

    '********************************************************************************************
    ' Class Name:		Response
    ' Function Name:	bProcessSaleRes
    ' Function Type:	Boolean.  Return False if error.
    ' Parameter:		
    ' Description:		Process payment response
    ' History:			29 Oct 2008
    '********************************************************************************************
    Public Function bProcessSaleRes(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost, ByVal sz_ReplyDateTime As String, ByRef sz_HTML As String) As Boolean
        Dim bReturn As Boolean = True
        Dim szQueryString As String = ""
        Dim iMesgSet As Integer = -1
        Dim iMesgNum As Integer = -1
        Dim szMesg As String = ""           ' Added on 18 Apr 2010
        Dim iRet As Integer = -1

        Dim objHash As Hash
        Dim objMailMesg As New MailMessage
        Dim objMail As SmtpMail
        Dim szBody As String = ""

        objHash = New Hash(objLoggerII)
        st_PayInfo.szHashValue = ""

        Try
            '- Verify Host return IP addresses (optional)
            '- Check late response - exceeding 2 hours from request datetime since booking PO will be released after 2 hours
            '- Update Txn State
            '- Verify response data with request data
            '- Reply Acknowledgement to Host if needed
            '- Get merchant password
            '- Get HostTxnStatus's action
            If True <> bInitResponse(st_PayInfo, st_HostInfo, sz_ReplyDateTime, sz_HTML) Then
                Return False
            End If

            'Process Host reply based on action type configured in database according to Host TxnStatus
            If (1 = st_PayInfo.iAction) Then                'Payment approved by Host
                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_SUCCESS
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_SUCCESS
                st_PayInfo.szTxnMsg = Common.C_TXN_SUCCESS_0
                st_PayInfo.iTxnState = Common.TXN_STATE.COMMIT
            ElseIf (2 = st_PayInfo.iAction) Then    'Failed by Host
                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.szTxnMsg = Common.C_TXN_FAILED_1
                st_PayInfo.iTxnState = Common.TXN_STATE.COMMIT_FAILED

            ElseIf (3 = st_PayInfo.iAction) Then    'Unconfirmed status from Host, need to query Host until getting reply or timeout
                'Update Request table with Action 3 for Retry Service to pick up and query the respective Host
                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_PENDING
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_PENDING
                st_PayInfo.szTxnMsg = "Host returned not processed/unknown status, pending query Host"
                st_PayInfo.iTxnState = Common.TXN_STATE.RECV_FROM_HOST

            ElseIf (5 = st_PayInfo.iAction Or 6 = st_PayInfo.iAction) Then    'Due to query account invalid password, need to send email alert and stop querying Host by setting QueryFlag to OFF until QueryFlag is manually set to ON
                'Overwrite Action from 5 to 3 and then update Request table with Action 3 for Retry Service to pick up
                'and query the respective Host
                'Requested by AirAsia's Bill on 8 June 2009, added a setting to enable and disable the Query Flag OFF feature
                'This is due to Maybank's response code 20 is inaccurate, it not only indicates invalid query account's
                'password provided by merchant; It also indicates internal network error.
                If (5 = st_PayInfo.iAction) Then
                    st_HostInfo.iQueryFlag = 0
                    If True <> objTxnProcOB.bUpdateHostInfo(st_PayInfo, st_HostInfo) Then
                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                        st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + "-S2S " + st_PayInfo.szErrDesc)
                    End If
                End If

                'Sends email alert (http://support.microsoft.com/kb/555287)
                objMailMesg.From = ConfigurationSettings.AppSettings("EmailSendFrom")
                objMailMesg.To = ConfigurationSettings.AppSettings("EmailSendTo")
                objMailMesg.Cc = ConfigurationSettings.AppSettings("EmailSendCc")
                objMailMesg.BodyFormat = MailFormat.Text
                objMailMesg.Priority = MailPriority.High

                If (5 = st_PayInfo.iAction) Then
                    objMailMesg.Subject = "Online Banking Alert - " + st_PayInfo.szIssuingBank + " Query Flag OFF"

                    szBody = "Dear all," + vbNewLine + vbNewLine
                    szBody = szBody + "This is an auto generated email. Please be informed that Query Flag for " + st_PayInfo.szIssuingBank + " is currently set to OFF due to Query rejected with error Unauthorized Query UserName and Password." + vbNewLine + vbNewLine
                    szBody = szBody + "Please liaise with the bank to check this Query account accordingly. After that, kindly inform Paydibs Support Team to re-configure the Query UserName and Password and then turn ON Query Flag." + vbNewLine + vbNewLine
                    szBody = szBody + "Thank you," + vbNewLine
                    szBody = szBody + "Online Banking Payment Gateway" + vbNewLine
                Else
                    If ("" <> st_PayInfo.szHostTxnStatus) Then
                        objMailMesg.Subject = "Online Banking Alert - " + st_PayInfo.szIssuingBank + " Response Code " + st_PayInfo.szHostTxnStatus
                    Else
                        objMailMesg.Subject = "Online Banking Alert - " + st_PayInfo.szIssuingBank + " Response Code " + st_PayInfo.szRespCode
                    End If

                    szBody = "Dear all," + vbNewLine + vbNewLine
                    szBody = szBody + "This is an auto generated email. Please be informed that Response Code as stated in email title for " + st_PayInfo.szIssuingBank + " was received." + vbNewLine + vbNewLine
                    szBody = szBody + "Please liaise with the bank to check this accordingly. After that, kindly inform Paydibs Support Team for further actions." + vbNewLine + vbNewLine
                    szBody = szBody + "Thank you," + vbNewLine
                    szBody = szBody + "Online Banking Payment Gateway" + vbNewLine
                End If

                objMailMesg.Body = szBody

                objMailMesg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusing", 2) 'Send the message using the network (SMTP over the network).
                objMailMesg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpusessl", False)  'Use SSL for the connection (True or False)
                objMailMesg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout", 1200)
                objMailMesg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpserver", ConfigurationSettings.AppSettings("EmailServer"))
                objMailMesg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpserverport", ConfigurationSettings.AppSettings("EmailPort"))
                objMailMesg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", 1)    'Use basic clear-text authentication, have to provide user name and password through sendusername and sendpassword fields
                objMailMesg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", ConfigurationSettings.AppSettings("EmailUserName"))
                objMailMesg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", ConfigurationSettings.AppSettings("EmailPassword"))

                objMail.SmtpServer = ConfigurationSettings.AppSettings("EmailServer")
                objMail.Send(objMailMesg)

                st_PayInfo.iAction = 3

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_TIMEOUT_HOST_RETURN
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_PENDING
                st_PayInfo.szTxnMsg = "Pending query Host"
                st_PayInfo.iTxnState = Common.TXN_STATE.RECV_FROM_HOST

                'Added 20 Jun 2011, for Mandiri, to cater for Pay response status "Pending" received from Host, need to send Reversal to Host
            ElseIf (7 = st_PayInfo.iAction) Then
                'Update Request table with Action 7 for Retry Service to pick up and send Reversal to Host
                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_HOST_UNPROCESSED
                st_PayInfo.szTxnMsg = "Host returned Pay pending status, pending Reversal to Host"
                st_PayInfo.iTxnState = Common.TXN_STATE.RECV_FROM_HOST
            End If

            'Update Request table with the respective Txn State and Action so that Retry Service can process
            'accordingly for both Action 3 (retry query host), Action 4 (retry confirm booking) and Action 7 (retry Reversal)
            'At the same time, also update Request table with BankRefNo (st_PayInfo.szHostTxnID), OSPymtCode (st_HostInfo.szOSPymtCode),
            'OSRet (iRet), ErrSet (iMesgSet), ErrNum (iMesgNum)
            'Added (7 = st_PayInfo.iAction) , 20 Jun 2011, for Mandiri, to cater for Pay response status "Pending" received from Host, need to send Reversal to Host
            If ((3 = st_PayInfo.iAction) Or (4 = st_PayInfo.iAction) Or (7 = st_PayInfo.iAction)) Then
                If True <> objTxnProcOB.bUpdateTxnStatus(st_PayInfo, st_HostInfo) Then
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + "-S2S " + st_PayInfo.szErrDesc)
                End If
            End If

            ''******************************************************************
            '' NO REPLY TO MERCHANT NEEDED
            ''******************************************************************
            'Send Gateway response to merchant using the same Host's reply method
            ''iSendReply2Merchant(st_PayInfo, st_HostInfo, sz_HTML)  
            'Even If (True = bRet), no need update DB for TxnState SENT_REPLY_CLIENT so that TxnState like COMMIT/COMMIT_FAILED won't be overwritten.

        Catch ex As Exception
            bReturn = False

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + "-S2S > Exception: " + ex.Message)

            'Added on 12 Mar 2011, to solve "The operation has timed out" exception after calling NewSkies Web Service ConfirmBooking()
            'Without adding this, even though payment was approved but when encountered operation timeout during booking confirmation,
            'DDGW will return TxnStatus -1 and TxnState 2 to merchant that requires DDGW to confirm booking
            If (1 = st_PayInfo.iAction And 1 = st_PayInfo.iNeedAddOSPymt) Then  'Approved by bank and need booking confirmation
                bReturn = True

                If ("THE OPERATION HAS TIMED OUT" = ex.Message.ToUpper.Trim()) Then
                    st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_PENDING_CONFIRM_BOOKING   'Merchant will extend booking based on this status
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_PENDING_CONFIRM_BOOKING
                    st_PayInfo.szTxnMsg = "Pending: Payment approved but booking status is unknown due to communication error with booking system, retry in progress"
                    st_PayInfo.iTxnState = Common.TXN_STATE.SENT_CONFIRM_OS

                    st_PayInfo.iAction = 4  'Retry ConfirmBooking
                Else
                    st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_PENDING
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_PENDING
                    st_PayInfo.szTxnMsg = "Pending: Status unknown, query Host in progress"
                    st_PayInfo.iTxnState = Common.TXN_STATE.RECV_FROM_HOST

                    st_PayInfo.iAction = 3  'Retry query bank
                End If

                'Update Request table with the respective Txn State and Action so that Retry Service can retry confirm booking
                'At the same time, also update Request table with BankRefNo (st_PayInfo.szHostTxnID), OSPymtCode (st_HostInfo.szOSPymtCode)
                If True <> objTxnProcOB.bUpdateTxnStatus(st_PayInfo, st_HostInfo) Then
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + "-S2S " + st_PayInfo.szErrDesc)
                End If

                ''******************************************************************
                '' NO REPLY TO MERCHANT NEEDED
                ''******************************************************************
                'Send Gateway response to merchant using the same Host's reply method
                'iSendReply2Merchant(st_PayInfo, st_HostInfo, sz_HTML)
                'Even If (True = bRet), no need update DB for TxnState SENT_REPLY_CLIENT so that TxnState like COMMIT/COMMIT_FAILED won't be overwritten.
            End If
        End Try

        If Not objMailMesg Is Nothing Then objMailMesg = Nothing

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		Response
    ' Function Name:	bInitResponse
    ' Function Type:	Boolean.  Return False if error.
    ' Parameter:		
    ' Description:		Update Txn State, Verify response data with request data, Reply Acknowledgement to Host if needed,
    '                   Get merchant password, Get HostTxnStatus's action
    ' History:			31 Oct 2008
    '********************************************************************************************
    Private Function bInitResponse(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost, ByVal sz_ReplyDateTime As String, ByRef sz_HTML As String) As Boolean
        Dim bReturn As Boolean = True
        Dim aszReturnIPAddresses() As String
        Dim iLoop As Integer = 0
        Dim iRet As Integer = -1
        Dim bMatch As Boolean = False

        Try
            'Verify Host return IP addresses (optional)
            If (st_HostInfo.szReturnIPAddresses <> "") Then
                aszReturnIPAddresses = Split(st_HostInfo.szReturnIPAddresses, ";")
                bMatch = False
                For iLoop = 0 To aszReturnIPAddresses.GetUpperBound(0)
                    If (aszReturnIPAddresses(iLoop) = HttpContext.Current.Request.UserHostAddress) Then
                        bMatch = True

                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                        "S2S-GatewayTxnID(" + st_PayInfo.szTxnID + ") Host reply IP matched " + aszReturnIPAddresses(iLoop))
                    End If
                Next

                If (False = bMatch) Then
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.INVALID_HOST_IP

                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    "S2S-GatewayTxnID(" + st_PayInfo.szTxnID + ") Host reply IP (" + HttpContext.Current.Request.UserHostAddress + ") does not match IP list (" + st_HostInfo.szReturnIPAddresses + ")")
                    Return False
                End If
            End If

            'Added checking of DDGW Aggregator on 3 Apr 2010, no need check late Host response for DDGW Aggregator
            If ("" = ConfigurationManager.AppSettings("HostsListTemplate")) Then
                ' Check late reply
                ' (2 hours x 60 minutes x 60 seconds = 7200 seconds) - PO 'll be released after 2 hrs from its reserved time.
                ' So, host reply came back 2 hours after the request was registered in Gateway 'll be treated as late reply and 'll be discarded.
                If True <> objTxnProcOB.bCheckLateResp(st_PayInfo, sz_ReplyDateTime) Then
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    "S2S-GatewayTxnID(" + st_PayInfo.szTxnID + ") " + st_PayInfo.szErrDesc)

                    Return False
                End If
            End If

            'Verify response data with request data stored in database.
            'At the same time, retrieve some request data from database based on Gateway TxnID to return to client's server.
            If True <> objResponse.bVerifyResTxn(st_PayInfo, st_HostInfo, sz_HTML) Then
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                "S2S-GatewayTxnID(" + st_PayInfo.szTxnID + ") > bVerifyResTxn failed: " + st_PayInfo.szErrDesc)

                Return False
            End If

            'Get Host's TxnStatus's action
            If True <> objTxnProcOB.bGetTxnStatusAction(st_HostInfo, st_PayInfo) Then

                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + "-S2S > bInitResponse() " + st_PayInfo.szErrDesc)
                Return False
            End If

        Catch ex As Exception
            bReturn = False

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + "-S2S > bInitResponse() Exception: " + ex.Message)
        End Try

        Return bReturn
    End Function

    Public Sub New()
        szLogPath = ConfigurationManager.AppSettings("LogPath")
        iLogLevel = Convert.ToInt32(ConfigurationManager.AppSettings("LogLevel"))

        '2 Apr 2010, moved InitLogger() from down to up here, or else Hash will get empty objLoggerII and encountered exception
        InitLogger()

        objTxnProc = New TxnProc(objLoggerII)           'Modified 20 Oct 2017, moved above objCommon
        objCommon = New Common(objTxnProc, objLoggerII) 'Modified 20 Oct 2017, added objTxnProc
        objHash = New Hash(objLoggerII)
        objTxnProcOB = New TxnProcOB(objLoggerII)
        objResponse = New Response(objLoggerII)
    End Sub

    Public Sub InitLogger()
        If (objLoggerII Is Nothing) Then

            If (szLogPath Is Nothing) Then
                szLogPath = "C:\\OB.log"
            End If

            If (iLogLevel < 0 Or iLogLevel > 5) Then
                iLogLevel = 3
            End If

            objLoggerII = LoggerII.CLoggerII.GetInstanceX(szLogPath + "_" + C_ISSUING_BANK + ".log", iLogLevel, 255)

        End If
    End Sub

    Protected Overrides Sub Finalize()
        If Not objLoggerII Is Nothing Then
            objLoggerII.Dispose()
            objLoggerII = Nothing
        End If

        If Not objCommon Is Nothing Then objCommon = Nothing
        If Not objTxnProcOB Is Nothing Then objTxnProcOB = Nothing
        If Not objTxnProc Is Nothing Then objTxnProc = Nothing
        If Not objHash Is Nothing Then objHash = Nothing

        If Not objResponse Is Nothing Then
            objResponse.Dispose()
            objResponse = Nothing
        End If

        MyBase.Finalize()
    End Sub

End Class