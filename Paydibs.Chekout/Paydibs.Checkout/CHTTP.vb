Imports LoggerII
Imports System.IO   ' StreamWriter
Imports System
Imports System.Threading
Imports System.Net  ' WebRequest
Imports System.Xml  ' Added, 5 Oct 2013


Public Class CHTTP

    Private objLoggerII As LoggerII.CLoggerII

    '********************************************************************************************
    ' Class Name:		CHTTP
    ' Function Name:	iHTTPost
    ' Function Type:	boolean
    ' Parameter:        sz_URL [in, string]         - Posting URL
    '                   sz_HTTPString [in, string]  - HTTP string to be posted
    '                   i_Timeout [in, integer]     - Connection timeout(in second)
    ' Description:		Perform HTTP posting
    ' History:			19 Nov 2008
    '********************************************************************************************
    Public Function iHTTPostOnly(ByVal sz_URL As String, ByVal sz_HTTPString As String, ByVal sz_ContentType As String, ByVal i_Timeout As Integer, Optional ByVal sz_SecurityProtocol As String = "TLS") As Integer
        Dim iRet As Integer = -1
        Dim objHttpWebRequest As HttpWebRequest
        Dim objStreamWriter As StreamWriter
        Dim sbInStream As System.Text.StringBuilder
        Dim iTimeout As Integer
        Dim iResCode As Integer
        'Dim iLoop As Integer               'Commented  15 Aug 2013
        'Dim szHTTPStrArray() As String
        'Dim szFieldArray() As String

        Try
            '======================================== Sets HttpWebRequest properties ====================================
            sbInStream = New System.Text.StringBuilder
            sbInStream.Append(sz_HTTPString)
            iTimeout = i_Timeout * 1000 'converts input timeout param from second to milliseconds

            objHttpWebRequest = WebRequest.Create(sz_URL)                       'Posting URL
            objHttpWebRequest.ContentType = sz_ContentType 'default value
            objHttpWebRequest.Method = "POST"                                   'default value
            objHttpWebRequest.ContentLength = sbInStream.Length ' Must be set b4 retrieving stream for sending data
            objHttpWebRequest.KeepAlive = False
            objHttpWebRequest.Timeout = iTimeout                                '(in milliseconds)

            'Modified 20 Jun 2015, added logging of Security Protocol
            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Timeout period(" + iTimeout.ToString() + " ms) Protocol(" + sz_SecurityProtocol + ") Posting HTTP data " + sz_HTTPString + " to " + sz_URL)

            'Ignore invalid server certificates or else may encounter WebException with e.Status = WebExceptionStatus.TrustFailure
            'When the CertificatePolicy property is set to an ICertificatePolicy interface instance, the ServicePointManager
            'uses the certificate policy defined in that instance instead of the default certificate policy.
            'The default certificate policy allows valid certificates, as well as valid certificates that have expired.
            ServicePointManager.CertificatePolicy = New SecPolicy

            'Added, 21 Jun 2015
            If (sz_SecurityProtocol.Trim() <> "") Then
                Select Case sz_SecurityProtocol.Trim().ToUpper()
                    Case "SSL3"
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3
                    Case "TLS12"
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
                    Case "TLS11"
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11
                    Case "TLS"
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                    Case Else
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3
                End Select

                ' allows for validation of SSL conversations
                ServicePointManager.ServerCertificateValidationCallback = Function() True   'C#: delegate { return true }
            End If

            'Commented  15 Aug 2013
            'If (InStr(sz_HTTPString, "&") > 0) Then
            '    szHTTPStrArray = Split(sz_HTTPString, "&")
            'ElseIf (InStr(sz_HTTPString, "$") > 0) Then
            '    szHTTPStrArray = Split(sz_HTTPString, "$")
            'Else
            '    ReDim szHTTPStrArray(0)             'Added  on 5 Jan 2011 for ENETS
            '    szHTTPStrArray(0) = sz_HTTPString   'Only one value, no need split
            'End If

            'For iLoop = 0 To szHTTPStrArray.GetUpperBound(0)
            '    'Added on 19 Aug 2009
            '    szHTTPStrArray(iLoop) = Trim(Replace(Replace(Replace(szHTTPStrArray(iLoop), vbCrLf, ""), vbCr, ""), vbLf, ""))
            '    If ("==" = Right(szHTTPStrArray(iLoop), 2)) Then
            '        'Differentiate between "field==" from "field=h==" 
            '        'Replace "field==" to "field=@" and "field=h==" to "field=h@@"
            '        If ((szHTTPStrArray(iLoop).IndexOf("=") + 1) = (szHTTPStrArray(iLoop).Length() - 1)) Then
            '            szHTTPStrArray(iLoop) = Left(szHTTPStrArray(iLoop), Len(szHTTPStrArray(iLoop)) - 1) + "@"
            '        Else
            '            szHTTPStrArray(iLoop) = Left(szHTTPStrArray(iLoop), Len(szHTTPStrArray(iLoop)) - 2) + "@@"
            '        End If
            '    ElseIf ("=" = Right(szHTTPStrArray(iLoop), 1)) Then
            '        'Differentiate between "field=" (empty value) from "field=value="
            '        'Checks if "=" is the character at the last position of the field value OR, e.g. "field=value="
            '        'if "=" is the field delimiter, e.g. "field="
            '        'Make sure "=" does not exist at the last character of szHTTPStrArray or else
            '        'the array result from the next Split by "=" will encounter out of bound exception
            '        If (szHTTPStrArray(iLoop).IndexOf("=") <> szHTTPStrArray(iLoop).Length() - 1) Then
            '            szHTTPStrArray(iLoop) = Left(szHTTPStrArray(iLoop), Len(szHTTPStrArray(iLoop)) - 1) + "@"
            '        End If
            '    End If

            '    szFieldArray = Split(szHTTPStrArray(iLoop), "=")

            '    'Added on 19 Aug 2009
            '    If ("@@" = Right(szFieldArray(1), 2)) Then
            '        szFieldArray(1) = Left(szFieldArray(1), Len(szFieldArray(1)) - 2) + "=="
            '    ElseIf ("@" = Right(szFieldArray(1), 1)) Then
            '        szFieldArray(1) = Left(szFieldArray(1), Len(szFieldArray(1)) - 1) + "="
            '    End If

            '    objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), " Field(" + szFieldArray(0) + ") Value(" + szFieldArray(1) + ")")
            'Next iLoop

            '=========================================== Post request ==================================================
            '- StreamWriter constructor creates a StreamWriter with UTF-8 encoding whose GetPreamble method returns an
            'empty byte array. The BaseStream property is initialized using the stream parameter (i.e. objHttpWebRequest.GetRequestStream)
            '- GetRequestStream method returns a stream to use to send data for the HttpWebRequest. Once the Stream
            'instance has been returned, you can send data with the HttpWebRequest by using the Stream.Write method.
            'NOTE:  Must set the value of the ContentLength property before retrieving the stream.
            '       When POST method is used, must get the request stream, write the data to be posted,
            '       and close the stream. This method blocks waiting for content to post; If there is no time-out set and
            '       content is not provided, application will block indefinitely.
            'CAUTION: Must call the Stream.Close method to close the stream and release the connection for reuse.
            '         Failure to close the stream will cause application to run out of connections.
            objHttpWebRequest.MaximumAutomaticRedirections = 1
            objHttpWebRequest.AllowAutoRedirect = True
            objStreamWriter = New StreamWriter(objHttpWebRequest.GetRequestStream())
            objStreamWriter.Write(sbInStream.ToString)  ' Sends data
            objStreamWriter.Close() ' Closes the stream and release the connection for reuse to avoid running out of connections

            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Posted")

            iRet = 0

        Catch webEx As WebException
            Try
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException: " + webEx.ToString)

                iRet = Common.ERROR_CODE.E_COM_URL_UNREACHABLE

                If webEx.Status = WebExceptionStatus.Timeout Then
                    iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                    objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException: Timeout")
                End If

                If webEx.Status = WebExceptionStatus.ProtocolError Then
                    objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Response Status Code :" + CStr(CType(webEx.Response, HttpWebResponse).StatusCode))

                    'Re: HttpStatusCode Enumeration
                    iResCode = CInt(CType(webEx.Response, HttpWebResponse).StatusCode)
                    If iResCode = 400 Then
                        iRet = Common.ERROR_CODE.E_COM_BAD_REQUEST
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Bad Request could not be understood by the server.")
                    ElseIf iResCode = 401 Then
                        iRet = Common.ERROR_CODE.E_COM_UNAUTHORIZED_URL
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Unauthorized, the requested resource requires authentication.")
                    ElseIf iResCode = 403 Then
                        iRet = Common.ERROR_CODE.E_COM_FORBIDDEN_URL
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Forbidden, the server refuses to fulfill the request.")
                    ElseIf iResCode = 404 Then
                        iRet = Common.ERROR_CODE.E_COM_URL_UNREACHABLE
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Not Found, the requested resource does not exist on the server.")
                    End If
                End If
            Catch exp As Exception
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Exception: " + exp.ToString)
            End Try

        Catch ioEx As IOException
            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "IO Exception: " + ioEx.ToString)
            iRet = Common.ERROR_CODE.E_COM_FILE_ERROR

        Catch ex As Exception
            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Exception: " + ex.ToString)
            iRet = Common.ERROR_CODE.E_COMMON_UNKNOWN_ERROR

        Finally
            'Commented  15 Aug 2013
            'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "iRet: " + CStr(iRet))
        End Try

        Return iRet
    End Function

    '********************************************************************************************
    ' Class Name:		CHTTP
    ' Function Name:	iHTTPostWithRecv
    ' Function Type:	boolean
    ' Parameter:        sz_URL [in, string]             - Posting URL
    '                   sz_HTTPString [in, string]      - HTTP string to be posted
    '                   i_Timeout [in, integer]         - Connection timeout(in second)
    '                   sz_HTTPResponse [out, string]   - HTTP string received
    ' Description:		Perform HTTP posting
    ' History:			19 Nov 2008
    '                   Modified 19 Jun 2015, added Security_Protocol optional param
    '                   Modified 13 Feb 2018, Fraudwall, added Optional ByVal i_ContentLength As Integer = 1
    '********************************************************************************************
    Public Function iHTTPostWithRecv(ByVal sz_URL As String, ByVal sz_HTTPString As String, ByVal i_Timeout As Integer, ByRef sz_HTTPResponse As String, ByVal sz_LogString As String, Optional ByVal sz_SecurityProtocol As String = "TLS", Optional ByVal i_ContentLength As Integer = 1) As Integer
        Dim iRet As Integer = -1
        Dim objHttpWebRequest As HttpWebRequest
        Dim objHTTPWebResponse As HttpWebResponse
        Dim objStreamWriter As StreamWriter
        Dim objStreamReader As StreamReader = Nothing       'Modified Joyce 4 Apr 2019,  add "= Nothing" as requested by PCI
        Dim sbInStream As System.Text.StringBuilder
        Dim iStartTickCount As Integer
        Dim iTimeout As Integer
        Dim iResCode As Integer
        'Dim iLoop As Integer           'Commented  15 Aug 2013
        'Dim szHTTPStrArray() As String
        'Dim szFieldArray() As String
        'Dim szLogValue As String       'Commented  15 Aug 2013

        Try
            '======================================== Sets HttpWebRequest properties ====================================
            sbInStream = New System.Text.StringBuilder
            sbInStream.Append(sz_HTTPString)
            iTimeout = i_Timeout * 1000 'converts input timeout param from second to milliseconds

            objHttpWebRequest = WebRequest.Create(sz_URL)                       'Posting URL
            objHttpWebRequest.ContentType = "application/x-www-form-urlencoded" 'default value
            objHttpWebRequest.Method = "POST"                                   'default value
            If (1 = i_ContentLength) Then   'Modified 13 Feb 2018, Fraudwall
                objHttpWebRequest.ContentLength = sbInStream.Length ' Must be set b4 retrieving stream for sending data
            End If
            objHttpWebRequest.KeepAlive = False
            objHttpWebRequest.Timeout = iTimeout                                '(in milliseconds)

            'Modified 20 Jun 2015, added logging of Security Protocol
            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Timeout period(" + iTimeout.ToString() + " ms) Protocol(" + sz_SecurityProtocol + ") Posting HTTP data " + sz_LogString + " to " + sz_URL)

            'Ignore invalid server certificates or else may encounter WebException with e.Status = WebExceptionStatus.TrustFailure
            'When the CertificatePolicy property is set to an ICertificatePolicy interface instance, the ServicePointManager
            'uses the certificate policy defined in that instance instead of the default certificate policy.
            'The default certificate policy allows valid certificates, as well as valid certificates that have expired.
            ServicePointManager.CertificatePolicy = New SecPolicy

            'Added, 19 Jun 2015
            If (sz_SecurityProtocol.Trim() <> "") Then
                Select Case sz_SecurityProtocol.Trim().ToUpper()
                    Case "SSL3"
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3
                    Case "TLS12"
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
                    Case "TLS11"
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11
                    Case "TLS"
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                    Case Else
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3
                End Select

                ' allows for validation of SSL conversations
                ServicePointManager.ServerCertificateValidationCallback = Function() True   'C#: delegate { return true }
            End If

            iStartTickCount = System.Environment.TickCount()

            'Commented  15 Aug 2013
            'Modified because Credit Card can not log clear value
            'If ("" = sz_LogString) Then
            '    If (InStr(sz_HTTPString, "&") > 0) Then
            '        szHTTPStrArray = Split(sz_HTTPString, "&")
            '    ElseIf (InStr(sz_HTTPString, "$") > 0) Then
            '        szHTTPStrArray = Split(sz_HTTPString, "$")
            '    End If
            'Else
            '    If (InStr(sz_LogString, "&") > 0) Then
            '        szHTTPStrArray = Split(sz_LogString, "&")
            '    ElseIf (InStr(sz_LogString, "$") > 0) Then
            '        szHTTPStrArray = Split(sz_LogString, "$")
            '    End If
            'End If

            'For iLoop = 0 To szHTTPStrArray.GetUpperBound(0)
            '    'Added on 19 Aug 2009
            '    szHTTPStrArray(iLoop) = Trim(Replace(Replace(Replace(szHTTPStrArray(iLoop), vbCrLf, ""), vbCr, ""), vbLf, ""))
            '    If ("==" = Right(szHTTPStrArray(iLoop), 2)) Then
            '        'Differentiate between "field==" from "field=h==" 
            '        'Replace "field==" to "field=@" and "field=h==" to "field=h@@"
            '        If ((szHTTPStrArray(iLoop).IndexOf("=") + 1) = (szHTTPStrArray(iLoop).Length() - 1)) Then
            '            szHTTPStrArray(iLoop) = Left(szHTTPStrArray(iLoop), Len(szHTTPStrArray(iLoop)) - 1) + "@"
            '        Else
            '            szHTTPStrArray(iLoop) = Left(szHTTPStrArray(iLoop), Len(szHTTPStrArray(iLoop)) - 2) + "@@"
            '        End If
            '    ElseIf ("=" = Right(szHTTPStrArray(iLoop), 1)) Then
            '        'Differentiate between "field=" (empty value) from "field=value="
            '        'Checks if "=" is the character at the last position of the field value OR, e.g. "field=value="
            '        'if "=" is the field delimiter, e.g. "field="
            '        'Make sure "=" does not exist at the last character of szHTTPStrArray or else
            '        'the array result from the next Split by "=" will encounter out of bound exception
            '        If (szHTTPStrArray(iLoop).IndexOf("=") <> szHTTPStrArray(iLoop).Length() - 1) Then
            '            szHTTPStrArray(iLoop) = Left(szHTTPStrArray(iLoop), Len(szHTTPStrArray(iLoop)) - 1) + "@"
            '        End If
            '    End If

            '    szFieldArray = Split(szHTTPStrArray(iLoop), "=")

            '    'Added on 19 Aug 2009
            '    If ("@@" = Right(szFieldArray(1), 2)) Then
            '        szFieldArray(1) = Left(szFieldArray(1), Len(szFieldArray(1)) - 2) + "=="
            '    ElseIf ("@" = Right(szFieldArray(1), 1)) Then
            '        szFieldArray(1) = Left(szFieldArray(1), Len(szFieldArray(1)) - 1) + "="
            '    End If

            '    objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), " Field(" + szFieldArray(0) + ") Value(" + szFieldArray(1) + ")")
            'Next iLoop

            '=========================================== Post request ==================================================
            '- StreamWriter constructor creates a StreamWriter with UTF-8 encoding whose GetPreamble method returns an
            'empty byte array. The BaseStream property is initialized using the stream parameter (i.e. objHttpWebRequest.GetRequestStream)
            '- GetRequestStream method returns a stream to use to send data for the HttpWebRequest. Once the Stream
            'instance has been returned, you can send data with the HttpWebRequest by using the Stream.Write method.
            'NOTE:    Must set the value of the ContentLength property before retrieving the stream.
            'CAUTION: Must call the Stream.Close method to close the stream and release the connection for reuse.
            '         Failure to close the stream will cause application to run out of connections.
            objStreamWriter = New StreamWriter(objHttpWebRequest.GetRequestStream())
            objStreamWriter.Write(sbInStream.ToString)  ' Sends data
            objStreamWriter.Close() ' Closes the stream and release the connection for reuse to avoid running out of connections

            'Modified 18 Jan 2018, combined the following logs to here, to save logs
            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Posted,getting resp")

            '============================================= Get response =================================================
            'Commented  18 Jan 2018, to save logs
            'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Getting HTTP response....")

            '- GetResponse method returns a WebResponse instance containing the response from the Internet resource.
            'The actual instance returned is an instance of HttpWebResponse, and can be typecast to that class to access
            'HTTP-specific properties. When POST method is used, must get the request stream, write the data to be posted,
            'and close the stream. This method blocks waiting for content to post; If there is no time-out set and
            'content is not provided, application will block indefinitely.

            objHTTPWebResponse = objHttpWebRequest.GetResponse()

            If (objHttpWebRequest.HaveResponse And (objHTTPWebResponse.StatusCode = HttpStatusCode.OK Or objHTTPWebResponse.StatusCode = HttpStatusCode.Created)) Then 'Modified 711, 29 Mar 2016, 711 successful txn returns as created
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "HTTP response received.")

                objStreamReader = New StreamReader(objHTTPWebResponse.GetResponseStream)

                '================ Implements Timeout for getting HTTP reply from host <START> ====================
                Dim objRespReader As RespReader
                Dim oThreadStart As ThreadStart
                Dim oThread As Thread
                Dim iElapsedTime As Integer

                objRespReader = New RespReader
                objRespReader.SetLogger(objLoggerII)
                objRespReader.SetReader(objStreamReader)

                oThreadStart = New ThreadStart(AddressOf objRespReader.WaitForResp)
                oThread = New Thread(oThreadStart)
                oThread.Start()

                'iStartTickCount = System.Environment.TickCount()
                iElapsedTime = System.Environment.TickCount() - iStartTickCount

                While ((iElapsedTime < iTimeout) And Not objRespReader.IsCompleted())
                    System.Threading.Thread.Sleep(50)
                    iElapsedTime = System.Environment.TickCount() - iStartTickCount
                End While
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp Elapsed Time(ms): " & iElapsedTime.ToString())

                If (iElapsedTime >= iTimeout) Then
                    ' Timeout
                    iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                    objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp Timeout, iRet(" + iRet.ToString() + ")")
                Else
                    'Completed AND No Timeout. Could be got reply or exception
                    If (True = objRespReader.IsSucceeded()) Then
                        'Received reply within timeout period
                        sz_HTTPResponse = objRespReader.GetRespStream()

                        If ("" = sz_HTTPResponse) Then  'Added  4 jun 2014. Firefly FF
                            iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp Response received with empty string. Set to Timeout.")
                        End If
                    Else
                        'Exception within timeout period
                        iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp exception within timeout period, iRet(" + iRet.ToString() + ")")
                    End If
                End If

                oThread.Abort()
                oThreadStart = Nothing
                oThread = Nothing
                'objRespReader.Dispose()
                objRespReader = Nothing

                'Releases the resources of the response stream
                'NOTE: Failure to close the stream will cause application to run out of connections.
                objStreamReader.Close()
                If Not objStreamReader Is Nothing Then objStreamReader = Nothing 'Added on 1 Mar 2009
                '================== Implements Timeout for getting HTTP reply from host <END> =====================
            Else
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "ERROR: Response was not received.")
            End If

            'Releases the resources of the response
            objHTTPWebResponse.Close()

            If (-1 = iRet) Then 'Modified  4 Jun 2014, added checking of (-1=iRet), Firefly FF
                iRet = 0
            End If

        Catch webEx As WebException
            Try
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException: " + webEx.ToString)

                iRet = Common.ERROR_CODE.E_COM_URL_UNREACHABLE

                If webEx.Status = WebExceptionStatus.Timeout Then
                    iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                    objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException: Timeout")
                End If

                If webEx.Status = WebExceptionStatus.ProtocolError Then
                    objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Response Status Code :" + CStr(CType(webEx.Response, HttpWebResponse).StatusCode))

                    'Re: HttpStatusCode Enumeration
                    iResCode = CInt(CType(webEx.Response, HttpWebResponse).StatusCode)
                    If iResCode = 400 Then
                        iRet = Common.ERROR_CODE.E_COM_BAD_REQUEST
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Bad Request could not be understood by the server.")
                    ElseIf iResCode = 401 Then
                        iRet = Common.ERROR_CODE.E_COM_UNAUTHORIZED_URL
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Unauthorized, the requested resource requires authentication.")
                    ElseIf iResCode = 403 Then
                        iRet = Common.ERROR_CODE.E_COM_FORBIDDEN_URL
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Forbidden, the server refuses to fulfill the request.")
                    ElseIf iResCode = 404 Then
                        iRet = Common.ERROR_CODE.E_COM_URL_UNREACHABLE
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Not Found, the requested resource does not exist on the server.")
                    End If
                End If
            Catch exp As Exception
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Exception: " + exp.ToString)
            End Try

        Catch ioEx As IOException
            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "IO Exception: " + ioEx.ToString)
            iRet = Common.ERROR_CODE.E_COM_FILE_ERROR

        Catch ex As Exception
            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Exception: " + ex.ToString)
            iRet = Common.ERROR_CODE.E_COMMON_UNKNOWN_ERROR

        Finally
            'Commented  15 Aug 2013
            'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "iRet: " + CStr(iRet))
        End Try

        Return iRet
    End Function

    '********************************************************************************************
    ' Class Name:		CHTTP
    ' Function Name:	iHTTPostWithRecvEx
    ' Function Type:	boolean
    ' Parameter:        sz_URL [in, string]             - Posting URL
    '                   sz_HTTPString [in, string]      - HTTP string to be posted
    '                   i_Timeout [in, integer]         - Connection timeout(in second)
    '                   sz_HTTPResponse [out, string]   - HTTP string received
    ' Description:		Perform HTTP posting
    ' History:			16 Jun 2010
    '********************************************************************************************
    'Modified by SUKI 20 Mar 2019. Added optional sz_KeyID and sz_hmac. eNETS
    'Public Function iHTTPostWithRecvEx(ByVal sz_URL As String, ByVal sz_HTTPString As String, ByVal i_Timeout As Integer, ByVal sz_ContentType As String, ByRef sz_HTTPResponse As String, Optional ByVal sz_SecurityProtocol As String = "TLS") As Integer
    Public Function iHTTPostWithRecvEx(ByVal sz_URL As String, ByVal sz_HTTPString As String, ByVal i_Timeout As Integer, ByVal sz_ContentType As String, ByRef sz_HTTPResponse As String, Optional ByVal sz_SecurityProtocol As String = "TLS",
                                       Optional ByVal sz_KeyID As String = "", Optional ByVal sz_hmac As String = "") As Integer
        Dim iRet As Integer = -1
        Dim objHttpWebRequest As HttpWebRequest
        Dim objHTTPWebResponse As HttpWebResponse
        Dim objStreamWriter As StreamWriter
        Dim objStreamReader As StreamReader = Nothing       'Modified by Joyce 4 Apr 2019,  add "= Nothing" as requested by PCI
        Dim sbInStream As System.Text.StringBuilder
        Dim iStartTickCount As Integer
        Dim iTimeout As Integer
        Dim iResCode As Integer
        'Dim iLoop As Integer               'Commented  15 Aug 2013
        'Dim szHTTPStrArray() As String
        'Dim szFieldArray() As String

        Try
            '======================================== Sets HttpWebRequest properties ====================================
            sbInStream = New System.Text.StringBuilder
            sbInStream.Append(sz_HTTPString)
            iTimeout = i_Timeout * 1000 'converts input timeout param from second to milliseconds

            objHttpWebRequest = WebRequest.Create(sz_URL)                       'Posting URL
            'Modified by SUKI 20 Mar 2019. Added "ENETS/JSON" condition. eNETS
            'objHttpWebRequest.ContentType = sz_ContentType
            If ("ENETS/JSON" = sz_ContentType.Trim().ToUpper()) Then
                objHttpWebRequest.ContentType = "application/json"
                Dim myWebHeaderCollection As WebHeaderCollection = objHttpWebRequest.Headers
                myWebHeaderCollection.Add("keyId", sz_KeyID)
                myWebHeaderCollection.Add("hmac", sz_hmac)
            Else
                objHttpWebRequest.ContentType = sz_ContentType
            End If

            objHttpWebRequest.Method = "POST"                                   'default value
            objHttpWebRequest.ContentLength = sbInStream.Length ' Must be set b4 retrieving stream for sending data
            objHttpWebRequest.KeepAlive = False
            objHttpWebRequest.Timeout = iTimeout                                '(in milliseconds)

            'Modified 20 Jun 2015, added logging of Security Protocol
            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Timeout period(" + iTimeout.ToString() + " ms) Protocol (" + sz_SecurityProtocol + ") Posting HTTP data " + sz_HTTPString + " to " + sz_URL)

            'Ignore invalid server certificates or else may encounter WebException with e.Status = WebExceptionStatus.TrustFailure
            'When the CertificatePolicy property is set to an ICertificatePolicy interface instance, the ServicePointManager
            'uses the certificate policy defined in that instance instead of the default certificate policy.
            'The default certificate policy allows valid certificates, as well as valid certificates that have expired.
            ServicePointManager.CertificatePolicy = New SecPolicy

            'Added, 19 Jun 2015
            If (sz_SecurityProtocol.Trim() <> "") Then
                Select Case sz_SecurityProtocol.Trim().ToUpper()
                    Case "SSL3"
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3
                    Case "TLS12"
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
                    Case "TLS11"
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11
                    Case "TLS"
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                    Case Else
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3
                End Select

                ' allows for validation of SSL conversations
                ServicePointManager.ServerCertificateValidationCallback = Function() True   'C#: delegate { return true }
            End If

            iStartTickCount = System.Environment.TickCount()

            'Commented  15 Aug 2013
            'If (InStr(sz_HTTPString, "&") > 0) Then
            '    szHTTPStrArray = Split(sz_HTTPString, "&")
            'ElseIf (InStr(sz_HTTPString, "$") > 0) Then
            '    szHTTPStrArray = Split(sz_HTTPString, "$")
            'Else 'Added 17 Feb 2011, for Mandiri, only one string which does not contain "&"/"$"
            '    ReDim szHTTPStrArray(-1)
            'End If

            'For iLoop = 0 To szHTTPStrArray.GetUpperBound(0)
            '    'Added on 19 Aug 2009
            '    szHTTPStrArray(iLoop) = Trim(Replace(Replace(Replace(szHTTPStrArray(iLoop), vbCrLf, ""), vbCr, ""), vbLf, ""))
            '    If ("==" = Right(szHTTPStrArray(iLoop), 2)) Then
            '        'Differentiate between "field==" from "field=h==" 
            '        'Replace "field==" to "field=@" and "field=h==" to "field=h@@"
            '        If ((szHTTPStrArray(iLoop).IndexOf("=") + 1) = (szHTTPStrArray(iLoop).Length() - 1)) Then
            '            szHTTPStrArray(iLoop) = Left(szHTTPStrArray(iLoop), Len(szHTTPStrArray(iLoop)) - 1) + "@"
            '        Else
            '            szHTTPStrArray(iLoop) = Left(szHTTPStrArray(iLoop), Len(szHTTPStrArray(iLoop)) - 2) + "@@"
            '        End If
            '    ElseIf ("=" = Right(szHTTPStrArray(iLoop), 1)) Then
            '        'Differentiate between "field=" (empty value) from "field=value="
            '        'Checks if "=" is the character at the last position of the field value OR, e.g. "field=value="
            '        'if "=" is the field delimiter, e.g. "field="
            '        'Make sure "=" does not exist at the last character of szHTTPStrArray or else
            '        'the array result from the next Split by "=" will encounter out of bound exception
            '        If (szHTTPStrArray(iLoop).IndexOf("=") <> szHTTPStrArray(iLoop).Length() - 1) Then
            '            szHTTPStrArray(iLoop) = Left(szHTTPStrArray(iLoop), Len(szHTTPStrArray(iLoop)) - 1) + "@"
            '        End If
            '    End If

            '    szFieldArray = Split(szHTTPStrArray(iLoop), "=")

            '    'Added on 19 Aug 2009
            '    If ("@@" = Right(szFieldArray(1), 2)) Then
            '        szFieldArray(1) = Left(szFieldArray(1), Len(szFieldArray(1)) - 2) + "=="
            '    ElseIf ("@" = Right(szFieldArray(1), 1)) Then
            '        szFieldArray(1) = Left(szFieldArray(1), Len(szFieldArray(1)) - 1) + "="
            '    End If

            '    objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), " Field(" + szFieldArray(0) + ") Value(" + szFieldArray(1) + ")")
            'Next iLoop

            '=========================================== Post request ==================================================
            '- StreamWriter constructor creates a StreamWriter with UTF-8 encoding whose GetPreamble method returns an
            'empty byte array. The BaseStream property is initialized using the stream parameter (i.e. objHttpWebRequest.GetRequestStream)
            '- GetRequestStream method returns a stream to use to send data for the HttpWebRequest. Once the Stream
            'instance has been returned, you can send data with the HttpWebRequest by using the Stream.Write method.
            'NOTE:    Must set the value of the ContentLength property before retrieving the stream.
            'CAUTION: Must call the Stream.Close method to close the stream and release the connection for reuse.
            '         Failure to close the stream will cause application to run out of connections.

            objStreamWriter = New StreamWriter(objHttpWebRequest.GetRequestStream())
            objStreamWriter.Write(sbInStream.ToString)  ' Sends data
            objStreamWriter.Close() ' Closes the stream and release the connection for reuse to avoid running out of connections

            'Modified 18 Jan 2018, combined the following logs to here, to save logs
            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Posted,getting resp")

            '============================================= Get response =================================================
            'Commented  18 Jan 2018, to save logs
            'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Getting HTTP response....")

            '- GetResponse method returns a WebResponse instance containing the response from the Internet resource.
            'The actual instance returned is an instance of HttpWebResponse, and can be typecast to that class to access
            'HTTP-specific properties. When POST method is used, must get the request stream, write the data to be posted,
            'and close the stream. This method blocks waiting for content to post; If there is no time-out set and
            'content is not provided, application will block indefinitely.

            objHTTPWebResponse = objHttpWebRequest.GetResponse()

            If (objHttpWebRequest.HaveResponse And objHTTPWebResponse.StatusCode = HttpStatusCode.OK) Then
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "HTTP response received.")

                objStreamReader = New StreamReader(objHTTPWebResponse.GetResponseStream)

                '================ Implements Timeout for getting HTTP reply from host <START> ====================
                Dim objRespReader As RespReader
                Dim oThreadStart As ThreadStart
                Dim oThread As Thread
                Dim iElapsedTime As Integer

                objRespReader = New RespReader
                objRespReader.SetLogger(objLoggerII)
                objRespReader.SetReader(objStreamReader)

                oThreadStart = New ThreadStart(AddressOf objRespReader.WaitForResp)
                oThread = New Thread(oThreadStart)
                oThread.Start()

                'iStartTickCount = System.Environment.TickCount()
                iElapsedTime = System.Environment.TickCount() - iStartTickCount

                While ((iElapsedTime < iTimeout) And Not objRespReader.IsCompleted())
                    System.Threading.Thread.Sleep(50)
                    iElapsedTime = System.Environment.TickCount() - iStartTickCount
                End While
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp Elapsed Time(ms): " & iElapsedTime.ToString())

                If (iElapsedTime >= iTimeout) Then
                    ' Timeout
                    iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                    objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp Timeout, iRet(" + iRet.ToString() + ")")
                Else
                    'Completed AND No Timeout. Could be got reply or exception
                    If (True = objRespReader.IsSucceeded()) Then
                        'Received reply within timeout period
                        'Modified by SUKI 20 Mar 2019. Added "ENETS/JSON" condition. eNETS
                        'sz_HTTPResponse = objRespReader.GetRespStream()
                        If ("ENETS/JSON" = sz_ContentType.Trim().ToUpper()) Then
                            sz_HTTPResponse = "message=" + objRespReader.GetRespStream() + "&hmac=" + objHTTPWebResponse.Headers("hmac") + "&keyId=" + objHTTPWebResponse.Headers("keyId")
                        Else
                            sz_HTTPResponse = objRespReader.GetRespStream()
                        End If

                    Else
                        'Exception within timeout period
                        iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp exception within timeout period, iRet(" + iRet.ToString() + ")")
                    End If
                End If

                oThread.Abort()
                oThreadStart = Nothing
                oThread = Nothing
                'objRespReader.Dispose()
                objRespReader = Nothing

                'Releases the resources of the response stream
                'NOTE: Failure to close the stream will cause application to run out of connections.
                objStreamReader.Close()
                If Not objStreamReader Is Nothing Then objStreamReader = Nothing 'Added on 1 Mar 2009
                '================== Implements Timeout for getting HTTP reply from host <END> =====================
            Else
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "ERROR: Response was not received.")
            End If

            'Releases the resources of the response
            objHTTPWebResponse.Close()

            iRet = 0

        Catch webEx As WebException
            Try
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException: " + webEx.ToString)

                iRet = Common.ERROR_CODE.E_COM_URL_UNREACHABLE

                If webEx.Status = WebExceptionStatus.Timeout Then
                    iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                    objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException: Timeout")
                End If

                If webEx.Status = WebExceptionStatus.ProtocolError Then
                    objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Response Status Code :" + CStr(CType(webEx.Response, HttpWebResponse).StatusCode))

                    'Re: HttpStatusCode Enumeration
                    iResCode = CInt(CType(webEx.Response, HttpWebResponse).StatusCode)
                    If iResCode = 400 Then
                        iRet = Common.ERROR_CODE.E_COM_BAD_REQUEST
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Bad Request could not be understood by the server.")
                    ElseIf iResCode = 401 Then
                        iRet = Common.ERROR_CODE.E_COM_UNAUTHORIZED_URL
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Unauthorized, the requested resource requires authentication.")
                    ElseIf iResCode = 403 Then
                        iRet = Common.ERROR_CODE.E_COM_FORBIDDEN_URL
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Forbidden, the server refuses to fulfill the request.")
                    ElseIf iResCode = 404 Then
                        iRet = Common.ERROR_CODE.E_COM_URL_UNREACHABLE
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Not Found, the requested resource does not exist on the server.")
                    End If
                End If
            Catch exp As Exception
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Exception: " + exp.ToString)
            End Try

        Catch ioEx As IOException
            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "IO Exception: " + ioEx.ToString)
            iRet = Common.ERROR_CODE.E_COM_FILE_ERROR

        Catch ex As Exception
            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Exception: " + ex.ToString)
            iRet = Common.ERROR_CODE.E_COMMON_UNKNOWN_ERROR

        Finally
            'Commented  15 Aug 2013
            'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "iRet: " + CStr(iRet))
        End Try

        Return iRet
    End Function

    '********************************************************************************************
    ' Class Name:		CHTTP
    ' Function Name:	iHTTPGetWithRecv
    ' Function Type:	boolean
    ' Parameter:        sz_URL [in, string]             - Posting URL
    '                   sz_HTTPString [in, string]      - HTTP string to be posted
    '                   i_Timeout [in, integer]         - Connection timeout(in second)
    '                   sz_HTTPResponse [out, string]   - HTTP string received
    ' Description:		Perform HTTP Get posting
    ' History:			22 April 2009   
    '********************************************************************************************
    Public Function iHTTPGetWithRecv(ByVal sz_URL As String, ByVal sz_HTTPString As String, ByVal i_Timeout As Integer, ByRef sz_HTTPResponse As String, Optional ByVal sz_SecurityProtocol As String = "TLS", Optional ByVal sz_Authenticate As String = "") As Integer 'Modified  30 Nov 2017, Boost, Add sz_Authenticate to cater GET&BEARER
        Dim iRet As Integer = -1
        Dim iTimeout As Integer
        Dim objHttpWebRequest As HttpWebRequest
        Dim objHTTPWebResponse As HttpWebResponse = Nothing
        Dim objStreamReader As StreamReader = Nothing       'Modified Joyce 4 Apr 2019,  add "= Nothing" as requested by PCI
        Dim szStr As String = ""
        'Dim szHTTPStrArray() As String     'Commented  15 Aug 2013
        'Dim szFieldArray() As String
        Dim iStartTickCount As Integer
        'Dim iLoop As Integer
        Dim iResCode As Integer

        Try
            '======================================== Sets HttpWebRequest properties ====================================
            iTimeout = i_Timeout * 1000 'converts input timeout param from second to milliseconds

            'Modified 20 Jun 2015, added logging of Security Protocol
            'Modified 17 Jan 2018, added Authenticate
            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Timeout period(" + iTimeout.ToString() + " ms) Protocol(" + sz_SecurityProtocol + ") Authenticate(" + sz_Authenticate + ") Posting HTTP data " + sz_HTTPString + " to " + sz_URL)

            ServicePointManager.CertificatePolicy = New SecPolicy

            'Added, 19 Jun 2015
            If (sz_SecurityProtocol.Trim() <> "") Then
                Select Case sz_SecurityProtocol.Trim().ToUpper()
                    Case "SSL3"
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3
                    Case "TLS12"
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
                    Case "TLS11"
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11
                    Case "TLS"
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                    Case Else
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3
                End Select

                ' allows for validation of SSL conversations
                ServicePointManager.ServerCertificateValidationCallback = Function() True   'C#: delegate { return true }
            End If

            iStartTickCount = System.Environment.TickCount()

            'Commented  15 Aug 2013
            'If (InStr(sz_HTTPString, "&") > 0) Then
            '    szHTTPStrArray = Split(sz_HTTPString, "&")
            'ElseIf (InStr(sz_HTTPString, "$") > 0) Then
            '    szHTTPStrArray = Split(sz_HTTPString, "$")
            'End If

            'For iLoop = 0 To szHTTPStrArray.GetUpperBound(0)
            '    'Added on 19 Aug 2009
            '    szHTTPStrArray(iLoop) = Trim(Replace(Replace(Replace(szHTTPStrArray(iLoop), vbCrLf, ""), vbCr, ""), vbLf, ""))
            '    If ("==" = Right(szHTTPStrArray(iLoop), 2)) Then
            '        'Differentiate between "field==" from "field=h==" 
            '        'Replace "field==" to "field=@" and "field=h==" to "field=h@@"
            '        If ((szHTTPStrArray(iLoop).IndexOf("=") + 1) = (szHTTPStrArray(iLoop).Length() - 1)) Then
            '            szHTTPStrArray(iLoop) = Left(szHTTPStrArray(iLoop), Len(szHTTPStrArray(iLoop)) - 1) + "@"
            '        Else
            '            szHTTPStrArray(iLoop) = Left(szHTTPStrArray(iLoop), Len(szHTTPStrArray(iLoop)) - 2) + "@@"
            '        End If
            '    ElseIf ("=" = Right(szHTTPStrArray(iLoop), 1)) Then
            '        'Differentiate between "field=" (empty value) from "field=value="
            '        'Checks if "=" is the character at the last position of the field value OR, e.g. "field=value="
            '        'if "=" is the field delimiter, e.g. "field="
            '        'Make sure "=" does not exist at the last character of szHTTPStrArray or else
            '        'the array result from the next Split by "=" will encounter out of bound exception
            '        If (szHTTPStrArray(iLoop).IndexOf("=") <> szHTTPStrArray(iLoop).Length() - 1) Then
            '            szHTTPStrArray(iLoop) = Left(szHTTPStrArray(iLoop), Len(szHTTPStrArray(iLoop)) - 1) + "@"
            '        End If
            '    End If

            '    szFieldArray = Split(szHTTPStrArray(iLoop), "=")

            '    'Added on 19 Aug 2009
            '    If ("@@" = Right(szFieldArray(1), 2)) Then
            '        szFieldArray(1) = Left(szFieldArray(1), Len(szFieldArray(1)) - 2) + "=="
            '    ElseIf ("@" = Right(szFieldArray(1), 1)) Then
            '        szFieldArray(1) = Left(szFieldArray(1), Len(szFieldArray(1)) - 1) + "="
            '    End If

            '    objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), " Field(" + szFieldArray(0) + ") Value(" + szFieldArray(1) + ")")
            'Next iLoop

            '=========================================== Post(GET) request ==================================================
            ' Create the web request
            If (sz_URL.Substring(sz_URL.Length - 1) = "/") Then
                objHttpWebRequest = DirectCast(WebRequest.Create(sz_URL + sz_HTTPString), HttpWebRequest)  ' Sends data 'Added  30 Nov 2017, Boost, Cater sz_URL with "/" at last character
            Else
                objHttpWebRequest = DirectCast(WebRequest.Create(sz_URL + "?" + sz_HTTPString), HttpWebRequest)  ' Sends data
            End If
            'objHttpWebRequest = DirectCast(WebRequest.Create(sz_URL + sz_HTTPString), HttpWebRequest)  ' Sends data
            If (sz_Authenticate <> "") Then 'Added  30 Nov 2017, Boost, Cater GET&Bearer
                objHttpWebRequest.Headers.Add("Authorization", sz_Authenticate)
            End If
            objHttpWebRequest.KeepAlive = False
            objHttpWebRequest.Timeout = iTimeout    '(in milliseconds)

            'Modified 18 Jan 2018, combined the following logs to here, to save logs
            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Posted,getting resp")

            '============================================= Get response =================================================
            'Commented  18 Jan 2018, to save logs
            'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Getting HTTP response....")

            ' Get response   
            objHTTPWebResponse = DirectCast(objHttpWebRequest.GetResponse(), HttpWebResponse)

            If (objHttpWebRequest.HaveResponse And objHTTPWebResponse.StatusCode = HttpStatusCode.OK) Then
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "HTTP response received.")

                ' Get the response stream into a reader   
                objStreamReader = New StreamReader(objHTTPWebResponse.GetResponseStream())

                szStr = objStreamReader.ReadToEnd()

                '================ Implements Timeout for getting HTTP reply from host <START> ====================
                Dim objRespReader As RespReader
                Dim oThreadStart As ThreadStart
                Dim oThread As Thread
                Dim iElapsedTime As Integer

                objRespReader = New RespReader
                objRespReader.SetLogger(objLoggerII)
                objRespReader.SetReader(objStreamReader)

                oThreadStart = New ThreadStart(AddressOf objRespReader.WaitForResp)
                oThread = New Thread(oThreadStart)
                oThread.Start()

                'iStartTickCount = System.Environment.TickCount()
                iElapsedTime = System.Environment.TickCount() - iStartTickCount

                While ((iElapsedTime < iTimeout) And Not objRespReader.IsCompleted())
                    System.Threading.Thread.Sleep(50)
                    iElapsedTime = System.Environment.TickCount() - iStartTickCount
                End While
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp Elapsed Time(ms): " & iElapsedTime.ToString())

                If (iElapsedTime >= iTimeout) Then
                    ' Timeout
                    iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                    objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp Timeout, iRet(" + iRet.ToString() + ")")
                Else
                    'Completed AND No Timeout. Could be got reply or exception
                    If (True = objRespReader.IsSucceeded()) Then
                        ' Console application output   
                        sz_HTTPResponse = szStr

                        If ("" = sz_HTTPResponse) Then 'Added  2 Jun 2014. Firefly FF
                            iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp Response received with empty string. Set to Timeout.")
                        End If
                    Else
                        'Exception within timeout period
                        iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp exception within timeout period, iRet(" + iRet.ToString() + ")")
                    End If
                End If

                oThread.Abort()
                oThreadStart = Nothing
                oThread = Nothing
                'objRespReader.Dispose()
                objRespReader = Nothing

                'Releases the resources of the response stream
                'NOTE: Failure to close the stream will cause application to run out of connections.
                objStreamReader.Close()
                If Not objStreamReader Is Nothing Then objStreamReader = Nothing
                '================== Implements Timeout for getting HTTP reply from host <END> =====================
            Else
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "ERROR: Response was not received.")
            End If

            'Releases the resources of the response
            objHTTPWebResponse.Close()

            If (-1 = iRet) Then 'Modified  2 Jun 2014, added checking of (-1=iRet), Firefly FF
                iRet = 0
            End If

        Catch webEx As WebException
            Try
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException: " + webEx.ToString)

                iRet = Common.ERROR_CODE.E_COM_URL_UNREACHABLE

                If webEx.Status = WebExceptionStatus.Timeout Then
                    iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                    objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException: Timeout")
                End If

                If webEx.Status = WebExceptionStatus.ProtocolError Then
                    objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Response Status Code :" + CStr(CType(webEx.Response, HttpWebResponse).StatusCode))
                    'Re: HttpStatusCode Enumeration
                    iResCode = CInt(CType(webEx.Response, HttpWebResponse).StatusCode)
                    If iResCode = 400 Then
                        iRet = Common.ERROR_CODE.E_COM_BAD_REQUEST
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Bad Request could not be understood by the server.")
                    ElseIf iResCode = 401 Then
                        iRet = Common.ERROR_CODE.E_COM_UNAUTHORIZED_URL
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Unauthorized, the requested resource requires authentication.")
                    ElseIf iResCode = 403 Then
                        iRet = Common.ERROR_CODE.E_COM_FORBIDDEN_URL
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Forbidden, the server refuses to fulfill the request.")
                    ElseIf iResCode = 404 Then
                        iRet = Common.ERROR_CODE.E_COM_URL_UNREACHABLE
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Not Found, the requested resource does not exist on the server.")
                    End If
                End If
            Catch exp As Exception
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Exception: " + exp.ToString)
            End Try

        Catch ioEx As IOException
            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "IO Exception: " + ioEx.ToString)
            iRet = Common.ERROR_CODE.E_COM_FILE_ERROR

        Catch ex As Exception
            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Exception: " + ex.ToString)
            iRet = Common.ERROR_CODE.E_COMMON_UNKNOWN_ERROR

        Finally
            'Commented  15 Aug 2013
            'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "iRet: " + CStr(iRet))
        End Try

        Return iRet
    End Function

    '********************************************************************************************
    ' Class Name:		CHTTP
    ' Function Name:	iHTTPGetWithRecvDB
    ' Function Type:	boolean
    ' Parameter:        sz_URL [in, string]             - Posting URL
    '                   sz_HTTPString [in, string]      - HTTP string to be posted
    '                   i_Timeout [in, integer]         - Connection timeout(in second)
    '                   sz_HTTPResponse [out, string]   - HTTP string received
    ' Description:		Perform HTTP Get posting and retry query DB for status (KTB)
    ' History:			19 November 2014
    ' Added By          
    '********************************************************************************************
    Public Function iHTTPGetWithRecvDB(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost, ByVal obj_Common As Common, ByVal sz_URL As String, ByVal sz_HTTPString As String, ByVal i_Timeout As Integer, ByRef sz_HTTPResponse As String, Optional ByVal sz_SecurityProtocol As String = "TLS") As Integer
        Dim iRet As Integer = -1
        Dim iTimeout As Integer
        Dim objHttpWebRequest As HttpWebRequest
        Dim objHTTPWebResponse As HttpWebResponse = Nothing
        Dim objStreamReader As StreamReader = Nothing       'Modified Joyce 4 Apr 2019,  add "= Nothing" as requested by PCI
        Dim szStr As String = ""
        'Dim szHTTPStrArray() As String     'Commented  15 Aug 2013
        'Dim szFieldArray() As String
        Dim iStartTickCount As Integer
        'Dim iLoop As Integer
        Dim iResCode As Integer
        Dim iDBTimeOut As Integer = 0

        Try
            '======================================== Sets HttpWebRequest properties ====================================
            iTimeout = i_Timeout * 1000 'converts input timeout param from second to milliseconds

            'Modified 20 Jun 2015, added logging of Security Protocol
            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Timeout period(" + iTimeout.ToString() + " ms) Protocol(" + sz_SecurityProtocol + ") Posting HTTP data " + sz_HTTPString + " to " + sz_URL)

            ServicePointManager.CertificatePolicy = New SecPolicy

            'Added, 19 Jun 2015
            If (sz_SecurityProtocol.Trim() <> "") Then
                Select Case sz_SecurityProtocol.Trim().ToUpper()
                    Case "SSL3"
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3
                    Case "TLS12"
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
                    Case "TLS11"
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11
                    Case "TLS"
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                    Case Else
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3
                End Select

                ' allows for validation of SSL conversations
                ServicePointManager.ServerCertificateValidationCallback = Function() True   'C#: delegate { return true }
            End If

            iStartTickCount = System.Environment.TickCount()

            '=========================================== Post(GET) request ==================================================
            ' Create the web request   
            objHttpWebRequest = DirectCast(WebRequest.Create(sz_URL + "?" + sz_HTTPString), HttpWebRequest)  ' Sends data

            objHttpWebRequest.KeepAlive = False
            objHttpWebRequest.Timeout = iTimeout    '(in milliseconds)

            'Modified 18 Jan 2018, combined the following logs to here, to save logs
            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Posted,getting resp")

            '============================================= Get response =================================================
            'Commented  18 Jan 2018, to save logs
            'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Getting HTTP response....")

            ' Get response   
            objHTTPWebResponse = DirectCast(objHttpWebRequest.GetResponse(), HttpWebResponse)

            If (objHttpWebRequest.HaveResponse And objHTTPWebResponse.StatusCode = HttpStatusCode.OK) Then
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "HTTP response received.")

                ' Get the response stream into a reader   
                objStreamReader = New StreamReader(objHTTPWebResponse.GetResponseStream())

                szStr = objStreamReader.ReadToEnd()

                '================ Implements Timeout for getting HTTP reply from host <START> ====================
                Dim objRespReader As RespReader
                Dim oThreadStart As ThreadStart
                Dim oThread As Thread
                Dim iElapsedTime As Integer

                objRespReader = New RespReader
                objRespReader.SetLogger(objLoggerII)
                objRespReader.SetReader(objStreamReader)

                oThreadStart = New ThreadStart(AddressOf objRespReader.WaitForResp)
                oThread = New Thread(oThreadStart)
                oThread.Start()

                'iStartTickCount = System.Environment.TickCount()
                iElapsedTime = System.Environment.TickCount() - iStartTickCount

                While ((iElapsedTime < iTimeout) And Not objRespReader.IsCompleted())
                    System.Threading.Thread.Sleep(50)
                    iElapsedTime = System.Environment.TickCount() - iStartTickCount
                End While
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp Elapsed Time(ms): " & iElapsedTime.ToString())

                If (iElapsedTime >= iTimeout) Then
                    ' Timeout
                    iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                    objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp Timeout, iRet(" + iRet.ToString() + ")")
                Else
                    'Completed AND No Timeout. Could be got reply or exception
                    If (True = objRespReader.IsSucceeded()) Then
                        ' Console application output   
                        sz_HTTPResponse = szStr

                        iElapsedTime = 0
                        iStartTickCount = System.Environment.TickCount()
                        iDBTimeOut = Convert.ToInt32(ConfigurationManager.AppSettings("DBQueryTimeoutSeconds"))

                        While (iElapsedTime < iDBTimeOut * 1000)
                            obj_Common.bGetResTxn(st_PayInfo, st_HostInfo)

                            'If (True = bRet) Then
                            If (-1 = st_PayInfo.iQueryStatus) Then ' -1 indicate that record found in Res table
                                Exit While
                            Else
                                Thread.Sleep(3000) 'sleep for 3 seconds
                            End If

                            iElapsedTime = System.Environment.TickCount() - iStartTickCount
                        End While


                        If ("" = sz_HTTPResponse And -1 <> st_PayInfo.iQueryStatus) Then 'indicate timeout if record not found in Res table and the res data is empty
                            iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp Response received with empty string. Set to Timeout.")
                        End If
                    Else
                        'Exception within timeout period
                        iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp exception within timeout period, iRet(" + iRet.ToString() + ")")
                    End If
                End If

                oThread.Abort()
                oThreadStart = Nothing
                oThread = Nothing
                'objRespReader.Dispose()
                objRespReader = Nothing

                'Releases the resources of the response stream
                'NOTE: Failure to close the stream will cause application to run out of connections.
                objStreamReader.Close()
                If Not objStreamReader Is Nothing Then objStreamReader = Nothing
                '================== Implements Timeout for getting HTTP reply from host <END> =====================
            Else
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "ERROR: Response was not received.")
            End If

            'Releases the resources of the response
            objHTTPWebResponse.Close()

            If (-1 = iRet) Then 'Modified  2 Jun 2014, added checking of (-1=iRet), Firefly FF
                iRet = 0
            End If

        Catch webEx As WebException
            Try
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException: " + webEx.ToString)

                iRet = Common.ERROR_CODE.E_COM_URL_UNREACHABLE

                If webEx.Status = WebExceptionStatus.Timeout Then
                    iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                    objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException: Timeout")
                End If

                If webEx.Status = WebExceptionStatus.ProtocolError Then
                    objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Response Status Code :" + CStr(CType(webEx.Response, HttpWebResponse).StatusCode))

                    'Re: HttpStatusCode Enumeration
                    iResCode = CInt(CType(webEx.Response, HttpWebResponse).StatusCode)
                    If iResCode = 400 Then
                        iRet = Common.ERROR_CODE.E_COM_BAD_REQUEST
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Bad Request could not be understood by the server.")
                    ElseIf iResCode = 401 Then
                        iRet = Common.ERROR_CODE.E_COM_UNAUTHORIZED_URL
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Unauthorized, the requested resource requires authentication.")
                    ElseIf iResCode = 403 Then
                        iRet = Common.ERROR_CODE.E_COM_FORBIDDEN_URL
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Forbidden, the server refuses to fulfill the request.")
                    ElseIf iResCode = 404 Then
                        iRet = Common.ERROR_CODE.E_COM_URL_UNREACHABLE
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Not Found, the requested resource does not exist on the server.")
                    End If
                End If
            Catch exp As Exception
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Exception: " + exp.ToString)
            End Try

        Catch ioEx As IOException
            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "IO Exception: " + ioEx.ToString)
            iRet = Common.ERROR_CODE.E_COM_FILE_ERROR

        Catch ex As Exception
            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Exception: " + ex.ToString)
            iRet = Common.ERROR_CODE.E_COMMON_UNKNOWN_ERROR

        Finally
            'Commented  15 Aug 2013
            'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "iRet: " + CStr(iRet))
        End Try

        Return iRet
    End Function

    '********************************************************************************************
    ' Class Name:		CHTTP
    ' Function Name:	iWSPostWithRecv
    ' Function Type:	integer
    ' Parameter:        sz_URL [in, string]             - Posting URL
    '                   sz_SOAPAction [in, string]      - SOAP action of web service to call
    '                   sz_SOAPString [in, string]      - SOAP string to be posted
    '                   i_Timeout [in, integer]         - Connection timeout(in second)
    '                   sz_SOAPResponse [out, string]   - SOAP string received
    '                   sz_UserName [optional in, string]   - User Name to access the web service
    '                   sz_Password [optional in, string]   - Password to access the web service
    ' Description:		Perform SOAP posting
    ' History:			5 Oct 2013
    '********************************************************************************************
    Public Function iWSPostWithRecv(ByVal sz_URL As String, ByVal sz_SOAPAction As String, ByVal sz_SOAPString As String, ByVal i_Timeout As Integer, ByRef sz_SOAPResponse As String, Optional ByVal sz_UserName As String = "", Optional ByVal sz_Password As String = "", Optional ByVal sz_SecurityProtocol As String = "TLS") As Integer
        Dim iRet As Integer = -1
        Dim objHttpWebRequest As HttpWebRequest
        Dim objHTTPWebResponse As HttpWebResponse
        Dim objStreamWriter As StreamWriter
        Dim objStreamReader As StreamReader = Nothing       'Modified Joyce 4 Apr 2019,  add "= Nothing" as requested by PCI

        Dim sbInStream As System.Text.StringBuilder
        'Dim objXMLDoc As XmlDocument

        'Dim objStringWriter As StringWriter
        'Dim objXMLTextWriter As XmlTextWriter

        Dim szCredentials As String = ""
        Dim iTimeout As Integer
        Dim iResCode As Integer
        Dim iStartTickCount As Integer

        Try
            '======================================== Sets HttpWebRequest properties ====================================
            'objXMLDoc = New XmlDocument
            'objXMLDoc.LoadXml(sz_SOAPString)

            sbInStream = New System.Text.StringBuilder
            sbInStream.Append(sz_SOAPString)

            iTimeout = i_Timeout * 1000 'converts input timeout param from second to milliseconds

            objHttpWebRequest = WebRequest.Create(sz_URL)                       'Posting URL

            objHttpWebRequest.Headers.Add("SOAPAction", sz_SOAPAction)

            If (sz_UserName <> "" And sz_Password <> "") Then
                szCredentials = String.Format("{0}:{1}", sz_UserName, sz_Password)
                objHttpWebRequest.Headers.Add("Authorization", "basic " + System.Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes(szCredentials)))
                objHttpWebRequest.Credentials = New NetworkCredential(sz_UserName, sz_Password)
            End If

            objHttpWebRequest.ContentType = "text/xml"                          '"text/xml;charset=\"utf-8\""
            objHttpWebRequest.Method = "POST"                                   'objHttpWebRequest.Accept = "text/xml"

            objHttpWebRequest.ContentLength = sbInStream.Length                 ' Must be set b4 retrieving stream for sending data

            objHttpWebRequest.KeepAlive = False
            objHttpWebRequest.Timeout = iTimeout                                '(in milliseconds)

            'Modified 20 Jun 2015, added logging of Security Protocol
            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Timeout period(" + iTimeout.ToString() + " ms) Protocol(" + sz_SecurityProtocol + ") Posting SOAP data [" + sz_SOAPString + "] to [" + sz_URL + "]")

            'Ignore invalid server certificates or else may encounter WebException with e.Status = WebExceptionStatus.TrustFailure
            'When the CertificatePolicy property is set to an ICertificatePolicy interface instance, the ServicePointManager
            'uses the certificate policy defined in that instance instead of the default certificate policy.
            'The default certificate policy allows valid certificates, as well as valid certificates that have expired.
            ServicePointManager.CertificatePolicy = New SecPolicy

            'Added, 19 Jun 2015
            If (sz_SecurityProtocol.Trim() <> "") Then
                Select Case sz_SecurityProtocol.Trim().ToUpper()
                    Case "SSL3"
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3
                    Case "TLS12"
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
                    Case "TLS11"
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11
                    Case "TLS"
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                    Case Else
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3
                End Select

                ' allows for validation of SSL conversations
                ServicePointManager.ServerCertificateValidationCallback = Function() True   'C#: delegate { return true }
            End If

            iStartTickCount = System.Environment.TickCount()

            '=========================================== Post request ==================================================
            '- StreamWriter constructor creates a StreamWriter with UTF-8 encoding whose GetPreamble method returns an
            'empty byte array. The BaseStream property is initialized using the stream parameter (i.e. objHttpWebRequest.GetRequestStream)
            '- GetRequestStream method returns a stream to use to send data for the HttpWebRequest. Once the Stream
            'instance has been returned, you can send data with the HttpWebRequest by using the Stream.Write method.
            'NOTE:    Must set the value of the ContentLength property before retrieving the stream.
            'CAUTION: Must call the Stream.Close method to close the stream and release the connection for reuse.
            '         Failure to close the stream will cause application to run out of connections.

            objStreamWriter = New StreamWriter(objHttpWebRequest.GetRequestStream())

            objStreamWriter.Write(sbInStream.ToString)  ' Sends data
            'objXMLDoc.Save(objStreamWriter)             ' Sends data

            objStreamWriter.Close() ' Closes the stream and release the connection for reuse to avoid running out of connections

            'Modified 18 Jan 2018, combined the following logs to here, to save logs
            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Posted,getting resp")

            '============================================= Get response =================================================
            'Commented  18 Jan 2018, to save logs
            'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Getting SOAP response....")

            '- GetResponse method returns a WebResponse instance containing the response from the Internet resource.
            'The actual instance returned is an instance of HttpWebResponse, and can be typecast to that class to access
            'HTTP-specific properties. When POST method is used, must get the request stream, write the data to be posted,
            'and close the stream. This method blocks waiting for content to post; If there is no time-out set and
            'content is not provided, application will block indefinitely.

            objHTTPWebResponse = objHttpWebRequest.GetResponse()

            If (objHttpWebRequest.HaveResponse And objHTTPWebResponse.StatusCode = HttpStatusCode.OK) Then
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "SOAP response received.")

                objStreamReader = New StreamReader(objHTTPWebResponse.GetResponseStream)

                '================ Implements Timeout for getting HTTP reply from host <START> ====================
                Dim objRespReader As RespReader
                Dim oThreadStart As ThreadStart
                Dim oThread As Thread
                Dim iElapsedTime As Integer

                objRespReader = New RespReader
                objRespReader.SetLogger(objLoggerII)
                objRespReader.SetReader(objStreamReader)

                oThreadStart = New ThreadStart(AddressOf objRespReader.WaitForResp)
                oThread = New Thread(oThreadStart)
                oThread.Start()

                'iStartTickCount = System.Environment.TickCount()
                iElapsedTime = System.Environment.TickCount() - iStartTickCount

                While ((iElapsedTime < iTimeout) And Not objRespReader.IsCompleted())
                    System.Threading.Thread.Sleep(50)
                    iElapsedTime = System.Environment.TickCount() - iStartTickCount
                End While
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp Elapsed Time(ms): " & iElapsedTime.ToString())

                If (iElapsedTime >= iTimeout) Then
                    ' Timeout
                    iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                    objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp Timeout, iRet(" + iRet.ToString() + ")")
                Else
                    'Completed AND No Timeout. Could be got reply or exception
                    If (True = objRespReader.IsSucceeded()) Then
                        'Received reply within timeout period

                        'objXMLDoc.Load(objHTTPWebResponse.GetResponseStream)
                        ''objXMLDoc.Save(Console.Out)
                        'StringWriter object to get data from XML document
                        'objStringWriter = New StringWriter
                        'objXMLTextWriter = New XmlTextWriter(objStringWriter)
                        'objXMLDoc.WriteTo(objXMLTextWriter)
                        'objStringWriter.ToString()

                        sz_SOAPResponse = objRespReader.GetRespStream()
                    Else
                        'Exception within timeout period
                        iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp exception within timeout period, iRet(" + iRet.ToString() + ")")
                    End If
                End If

                oThread.Abort()
                oThreadStart = Nothing
                oThread = Nothing
                'objRespReader.Dispose()
                objRespReader = Nothing

                'Releases the resources of the response stream
                'NOTE: Failure to close the stream will cause application to run out of connections.
                objStreamReader.Close()
                If Not objStreamReader Is Nothing Then objStreamReader = Nothing 'Added on 1 Mar 2009
                '================== Implements Timeout for getting HTTP reply from host <END> =====================
            Else
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "ERROR: Response was not received.")
            End If

            'Releases the resources of the response
            objHTTPWebResponse.Close()

            iRet = 0

        Catch webEx As WebException
            Try
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException: " + webEx.ToString)

                iRet = Common.ERROR_CODE.E_COM_URL_UNREACHABLE

                If webEx.Status = WebExceptionStatus.Timeout Then
                    iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                    objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException: Timeout")
                End If

                If webEx.Status = WebExceptionStatus.ProtocolError Then
                    objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Response Status Code :" + CStr(CType(webEx.Response, HttpWebResponse).StatusCode))

                    'Re: HttpStatusCode Enumeration
                    iResCode = CInt(CType(webEx.Response, HttpWebResponse).StatusCode)
                    If iResCode = 400 Then
                        iRet = Common.ERROR_CODE.E_COM_BAD_REQUEST
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Bad Request could not be understood by the server.")
                    ElseIf iResCode = 401 Then
                        iRet = Common.ERROR_CODE.E_COM_UNAUTHORIZED_URL
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Unauthorized, the requested resource requires authentication.")
                    ElseIf iResCode = 403 Then
                        iRet = Common.ERROR_CODE.E_COM_FORBIDDEN_URL
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Forbidden, the server refuses to fulfill the request.")
                    ElseIf iResCode = 404 Then
                        iRet = Common.ERROR_CODE.E_COM_URL_UNREACHABLE
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Not Found, the requested resource does not exist on the server.")
                    End If
                End If
            Catch exp As Exception
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Exception: " + exp.ToString)
            End Try

        Catch ioEx As IOException
            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "IO Exception: " + ioEx.ToString)
            iRet = Common.ERROR_CODE.E_COM_FILE_ERROR

        Catch ex As Exception
            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Exception: " + ex.ToString)
            iRet = Common.ERROR_CODE.E_COMMON_UNKNOWN_ERROR

        Finally
            'Commented  15 Aug 2013
            'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "iRet: " + CStr(iRet))
        End Try

        Return iRet
    End Function

    '********************************************************************************************
    ' Class Name:		CHTTP
    ' Function Name:	iHTTPostWithRecvAuth
    ' Function Type:	boolean
    ' Parameter:        sz_URL [in, string]             - Posting URL
    '                   sz_HTTPString [in, string]      - HTTP string to be posted
    '                   i_Timeout [in, integer]         - Connection timeout(in second)
    '                   sz_HTTPResponse [out, string]   - HTTP string received
    ' Description:		Perform HTTP posting with header authentication
    ' History:			9 Jul 2014. Added  GlobalPay CyberSource2
    '                   18 Aug 2015. Modified added sz_ContentType input param
    '                   20 Aug 2015. Modified added sz_Authenticate input param
    '********************************************************************************************
    Public Function iHTTPostWithRecvAuth(ByVal sz_URL As String, ByVal sz_HTTPString As String, ByVal i_Timeout As Integer, ByRef sz_HTTPResponse As String, ByVal sz_LogString As String, ByVal sz_ContentType As String, ByVal sz_Authenticate As String, Optional ByVal sz_UserName As String = "", Optional ByVal sz_Password As String = "", Optional ByVal sz_SecurityProtocol As String = "TLS") As Integer
        Dim iRet As Integer = -1
        Dim objHttpWebRequest As HttpWebRequest
        Dim objHTTPWebResponse As HttpWebResponse
        Dim objStreamWriter As StreamWriter
        Dim objStreamReader As StreamReader = Nothing          'Modified Joyce 4 Apr 2019,  add "= Nothing" as requested by PCI
        Dim sbInStream As System.Text.StringBuilder
        Dim iStartTickCount As Integer
        Dim iTimeout As Integer
        Dim iResCode As Integer
        Dim byteToSend() As Byte    'Added, 23 Aug 2015
        Dim objWriter               'Added, 23 Aug 2015
        Dim bInStream = False       'Added, 18 Jan 2018

        Try
            '======================================== Sets HttpWebRequest properties ====================================
            'Modified 18 Aug 2015, added checking of sz_HTTPString, Bitnet
            If (sz_HTTPString <> "") Then
                If ("json" = sz_ContentType.ToLower() Or "x-www-form-urlencoded" = sz_ContentType.ToLower()) Then    'Modified 23 Aug 2015, added checking of sz_ContentType
                    byteToSend = System.Text.Encoding.UTF8.GetBytes(sz_HTTPString)
                Else
                    sbInStream = New System.Text.StringBuilder
                    sbInStream.Append(sz_HTTPString)
                    bInStream = True
                End If
            End If

            iTimeout = i_Timeout * 1000 'converts input timeout param from second to milliseconds

            objHttpWebRequest = WebRequest.Create(sz_URL)                       'Posting URL

            'objHttpWebRequest.ContentType = "application/x-www-form-urlencoded" 'default value
            'Modified 18 Aug 2015, take ContentType from input param
            If ("" = sz_ContentType) Then
                sz_ContentType = "x-www-form-urlencoded"    'default value
            End If
            objHttpWebRequest.ContentType = "application/" + sz_ContentType

            objHttpWebRequest.Method = "POST"                                   'default value

            'Modified 18 Aug 2015, added checking of sz_HTTPString, Bitnet
            'Modified 18 Jan 2018, added checking of bInStream to solve object reference not set due to ContentType has been set to "x-www-form-urlencoded" even though for InStream
            If (sz_HTTPString <> "") Then
                If (("json" = sz_ContentType.ToLower() Or "x-www-form-urlencoded" = sz_ContentType.ToLower()) And (False = bInStream)) Then    'Modified 23 Aug 2015, added checking of sz_ContentType
                    objHttpWebRequest.ContentLength = byteToSend.Length ' Must be set b4 retrieving stream for sending data
                Else
                    objHttpWebRequest.ContentLength = sbInStream.Length ' Must be set b4 retrieving stream for sending data
                End If
            End If

            objHttpWebRequest.KeepAlive = False
            objHttpWebRequest.Timeout = iTimeout                                '(in milliseconds)

            If (sz_UserName <> "" And sz_Password <> "") Then
                Dim C As String
                C = String.Format("{0}:{1}", sz_UserName, sz_Password)

                'Modified 19 Aug 2015, added "json", for Bitnet
                If ("json" = sz_ContentType.ToLower()) Then
                    'NOTE: Basic or basic, case sensitive, will be declined with exception "The remote server returned an error: (401) Unauthorized"

                    If ("basic" = sz_Authenticate.ToLower()) Then
                        sz_Authenticate = sz_Authenticate + " " + Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(C))
                    End If
                    objHttpWebRequest.Headers.Add("Authorization", sz_Authenticate)
                Else
                    objHttpWebRequest.Headers.Add("Authorization", "basic " + Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes(C)))
                    objHttpWebRequest.Credentials = New NetworkCredential(sz_UserName, sz_Password)
                End If
            End If

            'var request = System.Net.WebRequest.Create(objBitcoin.uri_authenticate) as System.Net.HttpWebRequest;
            'request.KeepAlive = true;
            'request.Method = "POST";
            'byte[] encodedByte = System.Text.ASCIIEncoding.ASCII.GetBytes(objBitcoin.client_id + ":" + objBitcoin.secret); //("<CLIENT_ID>:<SECRET>");
            'string base64Encoded = Convert.ToBase64String(encodedByte);
            'request.Headers.Add("Authorization", "Basic " + base64Encoded);
            'request.ContentType = "application/json";

            'Modified 20 Jun 2015, added logging of Security Protocol
            'Modified 16 Jan 2018, added logging of authenticate
            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Timeout period(" + iTimeout.ToString() + " ms) Protocol(" + sz_SecurityProtocol + ") Authenticate(" + sz_Authenticate + ") Posting HTTP data " + sz_LogString + " to " + sz_URL)

            'Ignore invalid server certificates or else may encounter WebException with e.Status = WebExceptionStatus.TrustFailure
            'When the CertificatePolicy property is set to an ICertificatePolicy interface instance, the ServicePointManager
            'uses the certificate policy defined in that instance instead of the default certificate policy.
            'The default certificate policy allows valid certificates, as well as valid certificates that have expired.
            ServicePointManager.CertificatePolicy = New SecPolicy

            'Added, 19 Jun 2015
            If (sz_SecurityProtocol.Trim() <> "") Then
                Select Case sz_SecurityProtocol.Trim().ToUpper()
                    Case "SSL3"
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3
                    Case "TLS12"
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
                    Case "TLS11"
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11
                    Case "TLS"
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                    Case Else
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3
                End Select

                ' allows for validation of SSL conversations
                ServicePointManager.ServerCertificateValidationCallback = Function() True   'C#: delegate { return true }
            End If

            iStartTickCount = System.Environment.TickCount()

            '=========================================== Post request ==================================================
            '- StreamWriter constructor creates a StreamWriter with UTF-8 encoding whose GetPreamble method returns an
            'empty byte array. The BaseStream property is initialized using the stream parameter (i.e. objHttpWebRequest.GetRequestStream)
            '- GetRequestStream method returns a stream to use to send data for the HttpWebRequest. Once the Stream
            'instance has been returned, you can send data with the HttpWebRequest by using the Stream.Write method.
            'NOTE:    Must set the value of the ContentLength property before retrieving the stream.
            'CAUTION: Must call the Stream.Close method to close the stream and release the connection for reuse.
            '         Failure to close the stream will cause application to run out of connections.

            'Modified 18 Aug 2015, added checking of sz_HTTPString, Bitnet
            'Modified 18 Jan 2018, added checking of bInStream to solve object reference not set due to ContentType has been set to "x-www-form-urlencoded" even though for InStream
            If (sz_HTTPString <> "") Then
                If (("json" = sz_ContentType.ToLower() Or "x-www-form-urlencoded" = sz_ContentType.ToLower()) And (False = bInStream)) Then             'Modified 23 Aug 2015, added checking of sz_ContentType
                    objWriter = objHttpWebRequest.GetRequestStream()
                    objWriter.Write(byteToSend, 0, byteToSend.Length)   ' Sends data
                    objWriter.Close()
                Else
                    objStreamWriter = New StreamWriter(objHttpWebRequest.GetRequestStream())
                    objStreamWriter.Write(sbInStream.ToString)  ' Sends data
                    objStreamWriter.Close() ' Closes the stream and release the connection for reuse to avoid running out of connections
                End If
            End If
            'Modified 18 Jan 2018, combined the following logs to here, to save logs
            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Posted,getting resp")

            '============================================= Get response =================================================
            'Commented  18 Jan 2018, to save logs
            'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Getting HTTP response....")

            '- GetResponse method returns a WebResponse instance containing the response from the Internet resource.
            'The actual instance returned is an instance of HttpWebResponse, and can be typecast to that class to access
            'HTTP-specific properties. When POST method is used, must get the request stream, write the data to be posted,
            'and close the stream. This method blocks waiting for content to post; If there is no time-out set and
            'content is not provided, application will block indefinitely.

            objHTTPWebResponse = objHttpWebRequest.GetResponse()

            'Modified 24 Aug 2015, added checking of StatusCode Created 201, for Bitnet
            If (objHttpWebRequest.HaveResponse And (objHTTPWebResponse.StatusCode = HttpStatusCode.OK Or objHTTPWebResponse.StatusCode = HttpStatusCode.Created)) Then
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "HTTP response received.")

                objStreamReader = New StreamReader(objHTTPWebResponse.GetResponseStream)

                '================ Implements Timeout for getting HTTP reply from host <START> ====================
                Dim objRespReader As RespReader
                Dim oThreadStart As ThreadStart
                Dim oThread As Thread
                Dim iElapsedTime As Integer

                objRespReader = New RespReader
                objRespReader.SetLogger(objLoggerII)
                objRespReader.SetReader(objStreamReader)

                oThreadStart = New ThreadStart(AddressOf objRespReader.WaitForResp)
                oThread = New Thread(oThreadStart)
                oThread.Start()

                'iStartTickCount = System.Environment.TickCount()
                iElapsedTime = System.Environment.TickCount() - iStartTickCount

                While ((iElapsedTime < iTimeout) And Not objRespReader.IsCompleted())
                    System.Threading.Thread.Sleep(50)
                    iElapsedTime = System.Environment.TickCount() - iStartTickCount
                End While
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp Elapsed Time(ms): " & iElapsedTime.ToString())

                If (iElapsedTime >= iTimeout) Then
                    ' Timeout
                    iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                    objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp Timeout, iRet(" + iRet.ToString() + ")")
                Else
                    'Completed AND No Timeout. Could be got reply or exception
                    If (True = objRespReader.IsSucceeded()) Then
                        'Received reply within timeout period
                        sz_HTTPResponse = objRespReader.GetRespStream()

                        If ("" = sz_HTTPResponse) Then  'Added  4 jun 2014. Firefly FF
                            iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp Response received with empty string. Set to Timeout.")
                        End If
                    Else
                        'Exception within timeout period
                        iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp exception within timeout period, iRet(" + iRet.ToString() + ")")
                    End If
                End If

                oThread.Abort()
                oThreadStart = Nothing
                oThread = Nothing
                'objRespReader.Dispose()
                objRespReader = Nothing

                'Releases the resources of the response stream
                'NOTE: Failure to close the stream will cause application to run out of connections.
                objStreamReader.Close()
                If Not objStreamReader Is Nothing Then objStreamReader = Nothing 'Added on 1 Mar 2009
                '================== Implements Timeout for getting HTTP reply from host <END> =====================
            Else
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "ERROR: Response was not received.")
            End If

            'Releases the resources of the response
            objHTTPWebResponse.Close()

            If (-1 = iRet) Then 'Added  4 Jun 2014. Firefly FF
                iRet = 0
            End If

        Catch webEx As WebException
            Try
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException: " + webEx.ToString)

                iRet = Common.ERROR_CODE.E_COM_URL_UNREACHABLE

                If webEx.Status = WebExceptionStatus.Timeout Then
                    iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                    objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException: Timeout")
                End If

                If webEx.Status = WebExceptionStatus.ProtocolError Then
                    objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Response Status Code :" + CStr(CType(webEx.Response, HttpWebResponse).StatusCode))

                    'Re: HttpStatusCode Enumeration
                    iResCode = CInt(CType(webEx.Response, HttpWebResponse).StatusCode)
                    If iResCode = 400 Then
                        iRet = Common.ERROR_CODE.E_COM_BAD_REQUEST
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Bad Request could not be understood by the server.")
                    ElseIf iResCode = 401 Then
                        iRet = Common.ERROR_CODE.E_COM_UNAUTHORIZED_URL
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Unauthorized, the requested resource requires authentication.")
                    ElseIf iResCode = 403 Then
                        iRet = Common.ERROR_CODE.E_COM_FORBIDDEN_URL
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Forbidden, the server refuses to fulfill the request.")
                    ElseIf iResCode = 404 Then
                        iRet = Common.ERROR_CODE.E_COM_URL_UNREACHABLE
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Not Found, the requested resource does not exist on the server.")
                    End If
                End If
            Catch exp As Exception
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Exception: " + exp.ToString)
            End Try

        Catch ioEx As IOException
            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "IO Exception: " + ioEx.ToString)
            iRet = Common.ERROR_CODE.E_COM_FILE_ERROR

        Catch ex As Exception
            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Exception: " + ex.ToString)
            iRet = Common.ERROR_CODE.E_COMMON_UNKNOWN_ERROR

        Finally
            'Commented  15 Aug 2013
            'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "iRet: " + CStr(iRet))
        End Try

        Return iRet
    End Function

    '********************************************************************************************
    ' Class Name:		CHTTP
    ' Function Name:	iHTTPostWithRecvDB
    ' Function Type:	Integer
    ' Parameter:        sz_URL [in, string]             - Posting URL
    '                   sz_HTTPString [in, string]      - HTTP string to be posted
    '                   i_Timeout [in, integer]         - Connection timeout(in second)
    '                   sz_HTTPResponse [out, string]   - HTTP string received
    ' Description:		Perform HTTP posting, check DB for update
    ' History:			24 Feb 2016                   
    '********************************************************************************************
    Public Function iHTTPostWithRecvDB(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost, ByVal obj_Common As Common, ByVal sz_URL As String, ByVal sz_HTTPString As String, ByVal i_Timeout As Integer, ByRef sz_HTTPResponse As String, ByVal sz_LogString As String, Optional ByVal sz_SecurityProtocol As String = "TLS") As Integer
        Dim iRet As Integer = -1
        Dim objHttpWebRequest As HttpWebRequest
        Dim objHTTPWebResponse As HttpWebResponse
        Dim objStreamWriter As StreamWriter
        Dim sbInStream As System.Text.StringBuilder
        Dim iStartTickCount As Integer
        Dim iTimeout As Integer
        Dim iResCode As Integer
        Dim iDBTimeOut As Integer = 0

        Try
            '======================================== Sets HttpWebRequest properties ====================================
            sbInStream = New System.Text.StringBuilder
            sbInStream.Append(sz_HTTPString)
            iTimeout = i_Timeout * 1000 'converts input timeout param from second to milliseconds

            objHttpWebRequest = WebRequest.Create(sz_URL)                       'Posting URL
            objHttpWebRequest.ContentType = "application/x-www-form-urlencoded" 'default value
            objHttpWebRequest.Method = "POST"                                   'default value
            objHttpWebRequest.ContentLength = sbInStream.Length ' Must be set b4 retrieving stream for sending data
            objHttpWebRequest.KeepAlive = False
            objHttpWebRequest.Timeout = iTimeout                                '(in milliseconds)

            'Modified 20 Jun 2015, added logging of Security Protocol
            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Timeout period(" + iTimeout.ToString() + " ms) Protocol(" + sz_SecurityProtocol + ") Posting HTTP data " + sz_LogString + " to " + sz_URL)

            'Ignore invalid server certificates or else may encounter WebException with e.Status = WebExceptionStatus.TrustFailure
            'When the CertificatePolicy property is set to an ICertificatePolicy interface instance, the ServicePointManager
            'uses the certificate policy defined in that instance instead of the default certificate policy.
            'The default certificate policy allows valid certificates, as well as valid certificates that have expired.
            ServicePointManager.CertificatePolicy = New SecPolicy

            'Added, 19 Jun 2015
            If (sz_SecurityProtocol.Trim() <> "") Then
                Select Case sz_SecurityProtocol.Trim().ToUpper()
                    Case "SSL3"
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3
                    Case "TLS12"
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
                    Case "TLS11"
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11
                    Case "TLS"
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                    Case Else
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3
                End Select

                ' allows for validation of SSL conversations
                ServicePointManager.ServerCertificateValidationCallback = Function() True   'C#: delegate { return true }
            End If

            iStartTickCount = System.Environment.TickCount()
            '=========================================== Post request ==================================================
            '- StreamWriter constructor creates a StreamWriter with UTF-8 encoding whose GetPreamble method returns an
            'empty byte array. The BaseStream property is initialized using the stream parameter (i.e. objHttpWebRequest.GetRequestStream)
            '- GetRequestStream method returns a stream to use to send data for the HttpWebRequest. Once the Stream
            'instance has been returned, you can send data with the HttpWebRequest by using the Stream.Write method.
            'NOTE:    Must set the value of the ContentLength property before retrieving the stream.
            'CAUTION: Must call the Stream.Close method to close the stream and release the connection for reuse.
            '         Failure to close the stream will cause application to run out of connections.

            objStreamWriter = New StreamWriter(objHttpWebRequest.GetRequestStream())
            objStreamWriter.Write(sbInStream.ToString)  ' Sends data
            objStreamWriter.Close() ' Closes the stream and release the connection for reuse to avoid running out of connections

            'Modified 18 Jan 2018, combined the following logs to here, to save logs
            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Posted,getting resp")

            '============================================= Get response =================================================
            'Commented  18 Jan 2018, to save logs
            'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Getting HTTP response....")

            '- GetResponse method returns a WebResponse instance containing the response from the Internet resource.
            'The actual instance returned is an instance of HttpWebResponse, and can be typecast to that class to access
            'HTTP-specific properties. When POST method is used, must get the request stream, write the data to be posted,
            'and close the stream. This method blocks waiting for content to post; If there is no time-out set and
            'content is not provided, application will block indefinitely.

            objHTTPWebResponse = objHttpWebRequest.GetResponse()

            If (objHttpWebRequest.HaveResponse And objHTTPWebResponse.StatusCode = HttpStatusCode.OK) Then
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "HTTP response received.")

                '================ Implements Timeout for getting HTTP reply from host <START> ====================               
                Dim iElapsedTime As Integer
                iElapsedTime = 0
                iStartTickCount = System.Environment.TickCount()
                iDBTimeOut = Convert.ToInt32(ConfigurationManager.AppSettings("DBQueryTimeoutSeconds"))
                Dim bGetResponseInDB As Boolean = False

                While (iElapsedTime < iDBTimeOut * 1000)
                    If (2 = st_PayInfo.iTxnType) Then
                        obj_Common.bGetRefundResTxn(st_PayInfo)
                        If (-1 = st_PayInfo.iQueryStatus And 33 <> st_PayInfo.iTxnStatus) Then ' -1 indicate that record found in Res table
                            bGetResponseInDB = True
                            Exit While
                        End If
                    Else
                        obj_Common.bGetResTxn(st_PayInfo, st_HostInfo)
                        If (1 = st_PayInfo.iTxnType) Then
                            st_PayInfo.szTxnType = "VOID"
                        End If
                        If (-1 = st_PayInfo.iQueryStatus And 31 <> st_PayInfo.iTxnStatus) Then ' -1 indicate that record found in Res table
                            bGetResponseInDB = True
                            Exit While
                        End If
                    End If
                    Thread.Sleep(3000) 'sleep for 3 seconds
                    iElapsedTime = System.Environment.TickCount() - iStartTickCount
                End While

                If (bGetResponseInDB = False) Then 'indicate timeout if record not found in Res table and the res data is empty
                    iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                    objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp Response received with empty string. Set to Timeout.")
                End If
            Else
                'Exception within timeout period
                iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp exception within timeout period, iRet(" + iRet.ToString() + ")")
            End If

            'Releases the resources of the response
            objHTTPWebResponse.Close()

            If (-1 = iRet) Then 'Modified  4 Jun 2014, added checking of (-1=iRet), Firefly FF
                iRet = 0
            End If

        Catch webEx As WebException
            Try
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException: " + webEx.ToString)

                iRet = Common.ERROR_CODE.E_COM_URL_UNREACHABLE

                If webEx.Status = WebExceptionStatus.Timeout Then
                    iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                    objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException: Timeout")
                End If

                If webEx.Status = WebExceptionStatus.ProtocolError Then
                    objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Response Status Code :" + CStr(CType(webEx.Response, HttpWebResponse).StatusCode))

                    'Re: HttpStatusCode Enumeration
                    iResCode = CInt(CType(webEx.Response, HttpWebResponse).StatusCode)
                    If iResCode = 400 Then
                        iRet = Common.ERROR_CODE.E_COM_BAD_REQUEST
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Bad Request could not be understood by the server.")
                    ElseIf iResCode = 401 Then
                        iRet = Common.ERROR_CODE.E_COM_UNAUTHORIZED_URL
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Unauthorized, the requested resource requires authentication.")
                    ElseIf iResCode = 403 Then
                        iRet = Common.ERROR_CODE.E_COM_FORBIDDEN_URL
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Forbidden, the server refuses to fulfill the request.")
                    ElseIf iResCode = 404 Then
                        iRet = Common.ERROR_CODE.E_COM_URL_UNREACHABLE
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Not Found, the requested resource does not exist on the server.")
                    End If
                End If
            Catch exp As Exception
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Exception: " + exp.ToString)
            End Try

        Catch ioEx As IOException
            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "IO Exception: " + ioEx.ToString)
            iRet = Common.ERROR_CODE.E_COM_FILE_ERROR

        Catch ex As Exception
            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Exception: " + ex.ToString)
            iRet = Common.ERROR_CODE.E_COMMON_UNKNOWN_ERROR

        Finally
        End Try
        Return iRet
    End Function

    Public Function iHTTPostWithRecvMPGS(ByVal sz_URL As String, ByVal sz_HTTPString As String, ByVal i_Timeout As Integer, ByRef sz_HTTPResponse As String, ByVal sz_LogString As String, ByVal sz_ContentType As String, ByVal sz_Authenticate As String, Optional ByVal sz_UserName As String = "", Optional ByVal sz_Password As String = "", Optional ByVal sz_SecurityProtocol As String = "TLS") As Integer
        Dim iRet As Integer = -1
        Dim objHttpWebRequest As HttpWebRequest
        Dim objHTTPWebResponse As HttpWebResponse
        Dim objStreamWriter As StreamWriter
        Dim objStreamReader As StreamReader = Nothing       'Modified Joyce 4 Apr 2019,  add "= Nothing" as requested by PCI
        Dim sbInStream As System.Text.StringBuilder
        Dim iStartTickCount As Integer
        Dim iTimeout As Integer
        Dim iResCode As Integer
        Dim byteToSend() As Byte = Nothing
        Dim byteISO8859() As Byte = Nothing
        Dim objWriter
        Dim bInStream = False

        Try
            '======================================== Sets HttpWebRequest properties ====================================
            'Modified 18 Aug 2015, added checking of sz_HTTPString, Bitnet
            sz_HTTPString = HttpUtility.UrlDecode(sz_HTTPString)

            If (sz_HTTPString <> "") Then
                byteToSend = Encoding.UTF8.GetBytes(sz_HTTPString)
                byteISO8859 = Encoding.Convert(Encoding.UTF8, Encoding.GetEncoding("iso-8859-1"), byteToSend)
            End If

            iTimeout = i_Timeout * 1000 'converts input timeout param from second to milliseconds
            objHttpWebRequest = WebRequest.Create(sz_URL)                       'Posting URL

            If ("" = sz_ContentType) Then
                sz_ContentType = "x-www-form-urlencoded"    'default value
            End If

            objHttpWebRequest.ContentType = "application/" + sz_ContentType
            objHttpWebRequest.Method = "POST"                                   'default value
            objHttpWebRequest.ContentLength = byteISO8859.Length
            objHttpWebRequest.KeepAlive = False
            objHttpWebRequest.Timeout = iTimeout                                '(in milliseconds)

            If (sz_UserName <> "" And sz_Password <> "") Then
                Dim C As String
                C = String.Format("{0}:{1}", sz_UserName, sz_Password)

                'NOTE: Basic Or basic, case sensitive, will be declined with exception "The remote server returned an error: (401) Unauthorized"
                objHttpWebRequest.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes(C)))
                objHttpWebRequest.Credentials = New NetworkCredential(sz_UserName, sz_Password)
            End If

            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(),
                            "Timeout period(" + iTimeout.ToString() + " ms) Protocol(" + sz_SecurityProtocol + ") Authenticate(" + sz_Authenticate + ") Posting HTTP data " + sz_LogString + " to " + sz_URL)

            'Ignore invalid server certificates or else may encounter WebException with e.Status = WebExceptionStatus.TrustFailure
            'When the CertificatePolicy property is set to an ICertificatePolicy interface instance, the ServicePointManager
            'uses the certificate policy defined in that instance instead of the default certificate policy.
            'The default certificate policy allows valid certificates, as well as valid certificates that have expired.
            ServicePointManager.CertificatePolicy = New SecPolicy

            'Added, 19 Jun 2015
            If (sz_SecurityProtocol.Trim() <> "") Then
                Select Case sz_SecurityProtocol.Trim().ToUpper()
                    Case "SSL3"
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3
                    Case "TLS12"
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
                    Case "TLS11"
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11
                    Case "TLS"
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                    Case Else
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3
                End Select

                ' allows for validation of SSL conversations
                ServicePointManager.ServerCertificateValidationCallback = Function() True   'C#: delegate { return true }
            End If

            iStartTickCount = System.Environment.TickCount()

            '=========================================== Post request ==================================================
            '- StreamWriter constructor creates a StreamWriter with UTF-8 encoding whose GetPreamble method returns an
            'empty byte array. The BaseStream property is initialized using the stream parameter (i.e. objHttpWebRequest.GetRequestStream)
            '- GetRequestStream method returns a stream to use to send data for the HttpWebRequest. Once the Stream
            'instance has been returned, you can send data with the HttpWebRequest by using the Stream.Write method.
            'NOTE:    Must set the value of the ContentLength property before retrieving the stream.
            'CAUTION: Must call the Stream.Close method to close the stream and release the connection for reuse.
            '         Failure to close the stream will cause application to run out of connections.

            'Modified 18 Aug 2015, added checking of sz_HTTPString, Bitnet
            'Modified 18 Jan 2018, added checking of bInStream to solve object reference not set due to ContentType has been set to "x-www-form-urlencoded" even though for InStream
            If (sz_HTTPString <> "") Then
                objWriter = objHttpWebRequest.GetRequestStream()
                objWriter.Write(byteToSend, 0, byteToSend.Length)   ' Sends data
                objWriter.Close()
            End If
            'Modified 18 Jan 2018, combined the following logs to here, to save logs
            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Posted,getting resp")

            '============================================= Get response =================================================
            'Commented  18 Jan 2018, to save logs
            'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Getting HTTP response....")

            '- GetResponse method returns a WebResponse instance containing the response from the Internet resource.
            'The actual instance returned is an instance of HttpWebResponse, and can be typecast to that class to access
            'HTTP-specific properties. When POST method is used, must get the request stream, write the data to be posted,
            'and close the stream. This method blocks waiting for content to post; If there is no time-out set and
            'content is not provided, application will block indefinitely.

            objHTTPWebResponse = objHttpWebRequest.GetResponse()

            'Modified 24 Aug 2015, added checking of StatusCode Created 201, for Bitnet
            If (objHttpWebRequest.HaveResponse And (objHTTPWebResponse.StatusCode = HttpStatusCode.OK Or objHTTPWebResponse.StatusCode = HttpStatusCode.Created)) Then
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "HTTP response received.")

                objStreamReader = New StreamReader(objHTTPWebResponse.GetResponseStream)

                '================ Implements Timeout for getting HTTP reply from host <START> ====================
                Dim objRespReader As RespReader
                Dim oThreadStart As ThreadStart
                Dim oThread As Thread
                Dim iElapsedTime As Integer

                objRespReader = New RespReader
                objRespReader.SetLogger(objLoggerII)
                objRespReader.SetReader(objStreamReader)

                oThreadStart = New ThreadStart(AddressOf objRespReader.WaitForResp)
                oThread = New Thread(oThreadStart)
                oThread.Start()

                'iStartTickCount = System.Environment.TickCount()
                iElapsedTime = System.Environment.TickCount() - iStartTickCount

                While ((iElapsedTime < iTimeout) And Not objRespReader.IsCompleted())
                    System.Threading.Thread.Sleep(50)
                    iElapsedTime = System.Environment.TickCount() - iStartTickCount
                End While
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp Elapsed Time(ms): " & iElapsedTime.ToString())

                If (iElapsedTime >= iTimeout) Then
                    ' Timeout
                    iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                    objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp Timeout, iRet(" + iRet.ToString() + ")")
                Else
                    'Completed AND No Timeout. Could be got reply or exception
                    If (True = objRespReader.IsSucceeded()) Then
                        'Received reply within timeout period
                        sz_HTTPResponse = objRespReader.GetRespStream()

                        If ("" = sz_HTTPResponse) Then  'Added  4 jun 2014. Firefly FF
                            iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp Response received with empty string. Set to Timeout.")
                        End If
                    Else
                        'Exception within timeout period
                        iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp exception within timeout period, iRet(" + iRet.ToString() + ")")
                    End If
                End If

                oThread.Abort()
                oThreadStart = Nothing
                oThread = Nothing
                'objRespReader.Dispose()
                objRespReader = Nothing

                'Releases the resources of the response stream
                'NOTE: Failure to close the stream will cause application to run out of connections.
                objStreamReader.Close()
                If Not objStreamReader Is Nothing Then objStreamReader = Nothing 'Added on 1 Mar 2009
                '================== Implements Timeout for getting HTTP reply from host <END> =====================
            Else
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "ERROR: Response was not received.")
            End If

            'Releases the resources of the response
            objHTTPWebResponse.Close()

            If (-1 = iRet) Then 'Added  4 Jun 2014. Firefly FF
                iRet = 0
            End If

        Catch webEx As WebException
            Try
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException: " + webEx.ToString)

                iRet = Common.ERROR_CODE.E_COM_URL_UNREACHABLE

                If webEx.Status = WebExceptionStatus.Timeout Then
                    iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                    objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException: Timeout")
                End If

                If webEx.Status = WebExceptionStatus.ProtocolError Then
                    objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Response Status Code :" + CStr(CType(webEx.Response, HttpWebResponse).StatusCode))

                    'Re: HttpStatusCode Enumeration
                    iResCode = CInt(CType(webEx.Response, HttpWebResponse).StatusCode)
                    If iResCode = 400 Then
                        iRet = Common.ERROR_CODE.E_COM_BAD_REQUEST
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Bad Request could not be understood by the server.")
                    ElseIf iResCode = 401 Then
                        iRet = Common.ERROR_CODE.E_COM_UNAUTHORIZED_URL
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Unauthorized, the requested resource requires authentication.")
                    ElseIf iResCode = 403 Then
                        iRet = Common.ERROR_CODE.E_COM_FORBIDDEN_URL
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Forbidden, the server refuses to fulfill the request.")
                    ElseIf iResCode = 404 Then
                        iRet = Common.ERROR_CODE.E_COM_URL_UNREACHABLE
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Not Found, the requested resource does not exist on the server.")
                    End If
                End If
            Catch exp As Exception
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Exception: " + exp.ToString)
            End Try

        Catch ioEx As IOException
            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "IO Exception: " + ioEx.ToString)
            iRet = Common.ERROR_CODE.E_COM_FILE_ERROR

        Catch ex As Exception
            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Exception: " + ex.ToString)
            iRet = Common.ERROR_CODE.E_COMMON_UNKNOWN_ERROR

        Finally
            'Commented  15 Aug 2013
            'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "iRet: " + CStr(iRet))
        End Try

        Return iRet
    End Function

    Public Sub New(ByRef o_LoggerII As LoggerII.CLoggerII)
        Try
            objLoggerII = o_LoggerII
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class
