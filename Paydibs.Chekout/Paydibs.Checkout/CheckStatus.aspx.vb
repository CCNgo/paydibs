﻿Imports LoggerII
Imports System.IO
'Imports System
'Imports System.Data
'Imports System.Security.Cryptography
Imports System.Threading
Imports MailerService
'Imports System.Text.RegularExpressionsf
'Imports System.Timers

Public Class CheckStatus
    Inherits System.Web.UI.Page
    Private objLoggerII As LoggerII.CLoggerII
    Private objCommon As Common
    Private objTxnProcOB As TxnProcOB
    Private objTxnProc As TxnProc
    Private stPayInfo As Common.STPayInfo
    Private bTimeOut As Boolean = False
    Private stHostInfo As Common.STHost
    Private objHash As Hash
    Private objResponse As Response
    Private objHTTP As CHTTP
    Private objJS As New System.Web.Script.Serialization.JavaScriptSerializer

    'Public Const stPayInfo.szIssuingBank = "M2U2"   'Commented , 17 Feb 2016

    Public Const C_TXN_TYPE = "PAY"
    Public Const C_PYMT_METHOD = "OB"

    Public g_szHTML As String = ""
    Private szLogPath As String = ""
    Private iLogLevel As String = ""
    Private szReplyDateTime As String = ""
    Public g_JSONResp As New Dictionary(Of String, String)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim bGetHostInfo As Boolean = False
        Dim szHTTPString As String = ""

        Try
            'Set to No Cache to make this page expired when user clicks the "back" button on browser
            Response.Cache.SetNoStore()

            If Not Page.IsPostBack Then
                'Initialization of HostInfo and PayInfo structures
                objCommon.InitHostInfo(stHostInfo)
                objCommon.InitPayInfo(stPayInfo)

                stPayInfo.szIssuingBank = Request("AcqBank") 'Request("IssuingBank")    'Modified 17 Feb 2016, do not hardcode issuing bank so that this page can be reused for other banks
                stPayInfo.szTxnType = C_TXN_TYPE
                stPayInfo.szPymtMethod = C_PYMT_METHOD
                stPayInfo.iHttpTimeoutSeconds = Convert.ToInt32(ConfigurationManager.AppSettings("HttpTimeoutSeconds"))

                'Log all form's request fields
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                "CheckStatus Req: " + szGetAllHTTPVal())

                'Get Host information - HostID, HostTemplate, NeedReplyAcknowledgement, TxnStatusActionID, HostReplyMethod, OSPymtCode
                bGetHostInfo = objTxnProcOB.bGetHostInfo(stHostInfo, stPayInfo)
                If (bGetHostInfo) Then
                    If True <> bCheckStatus() Then
                        'Return failed response to merchant
                        '-----------  ResponseErrHandler()
                    Else
                        iSendReply2Merchant()
                    End If
                Else
                    ResponseErrHandler()
                End If

            End If
        Catch ex As Exception
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                           DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                          " > " + stPayInfo.szIssuingBank + " response Page_Load() Exception: " + ex.Message)
        End Try

    End Sub

    '********************************************************************************************
    ' Class Name:		CheckStatus
    ' Function Name:	bCheckStatus
    ' Function Type:	Boolean.  Return False if error.
    ' Parameter:		
    ' Description:		check the result from response table returned by host
    ' History:			13 Jan 2016
    '********************************************************************************************
    Private Function bCheckStatus() As Boolean

        Dim bReturn = True
        'Dim bRet = False
        Dim iRet As Integer = -1
        'Dim szTagSearch As String = ""
        Dim iStart As Integer = -1
        Dim iEnd As Integer = -1
        Dim iDBTimeOut As Integer = 0
        ' Dim szTmpSvcID As String = ""

        Dim iStartTickCount As Integer
        Dim iElapsedTime As Integer


        Try
            iElapsedTime = 0
            iStartTickCount = System.Environment.TickCount()
            iDBTimeOut = Convert.ToInt32(ConfigurationManager.AppSettings("DBQueryTimeoutSeconds"))
            stPayInfo.szTxnID = Request.QueryString("ID")
            stPayInfo.szMerchantID = Request.QueryString("MerchantID")    'Added, 8 Mar 2016, M2U2

            'Added, 8 Mar 2016, verify hash
            If True <> objTxnProc.bGetMerchant(stPayInfo) Then
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > " + stPayInfo.szErrDesc)
                Return False
            End If

            stPayInfo.szHashKey = Request.QueryString("MerchantID") + Request.QueryString("ID") + Request.QueryString("AcqBank") 'Request.QueryString("IssuingBank")
            stPayInfo.szHashMethod = "SHA512" 'stPayInfo.szHashMethod = "SHA2"
            stPayInfo.szHashValue = Server.UrlDecode(Request("Sign")).Replace(" ", "+")

            If True <> objHash.bCheckReqHash(stPayInfo) Then
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > " + stPayInfo.szErrDesc)
                Return False
            End If

            While (iElapsedTime < 1.5 * 60 * 1000) 'should set to 1.8 minutes or lower to prevent System.Threading.ThreadAbortException: Thread was being aborted.
                objTxnProcOB.bGetResTxn(stPayInfo, stHostInfo)

                If (-1 = stPayInfo.iQueryStatus) Then ' -1 indicate that record found in Res table
                    Exit While
                Else
                    Thread.Sleep(5 * 1000) 'sleep for (? second)
                End If
                iElapsedTime = System.Environment.TickCount() - iStartTickCount
            End While

            If (-1 <> stPayInfo.iQueryStatus) Then
                bTimeOut = True
            End If

            'Added to check Query PG timeout
            If (True = bTimeOut) Then
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "Get Response TimeOut = " + bTimeOut.ToString() + " for GatewayTxnID (" + stPayInfo.szTxnID + "),process Query from M2U")

                stPayInfo.szTxnID = Request.QueryString("ID")
                objTxnProcOB.bGetReqTxn(stPayInfo, stHostInfo)
                stPayInfo.szTxnType = "QUERY"
                '================================== ======================================
                'GetTerminal account details based on Currency Code
                If True <> objTxnProcOB.bGetTerminal(stHostInfo, stPayInfo) Then
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bGetTerminal() > " + stPayInfo.szErrDesc)
                    Return False
                Else
                    iRet = objResponse.iSendQueryHost(stPayInfo, stHostInfo) ' Send Query to host/bank

                    'Get Host's TxnStatus's action
                    If True <> objCommon.bGetTxnStatusAction(stHostInfo, stPayInfo) Then

                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                        stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bCheckStatus() " + stPayInfo.szErrDesc)
                        Return False
                    End If

                    'Process Host reply based on action type configured in database according to Host TxnStatus
                    If (1 = stPayInfo.iAction) Then                'Payment approved by Host
                        stPayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_SUCCESS

                        'Modified  29 April 2015. Auth&Capture
                        If ("AUTH" = stPayInfo.szTxnType) Then
                            stPayInfo.iTxnStatus = Common.TXN_STATUS.TXN_AUTH_SUCCESS
                        Else
                            stPayInfo.iTxnStatus = Common.TXN_STATUS.TXN_SUCCESS
                        End If

                        stPayInfo.szTxnMsg = Common.C_TXN_SUCCESS_0
                        stPayInfo.iTxnState = Common.TXN_STATE.COMMIT

                        'Added, 19 Apr 2015, for OTC
                        If ("OTC" = stPayInfo.szPymtMethod) Then
                            If (-1 = stPayInfo.iQueryStatus) Then  'Record exists in Request table
                                stPayInfo.iAction = 4  'Retry Service to pickup and only send response to merchat if exceeded permissible Reversal period
                            End If
                        End If

                        iSendReply2Merchant()
                    ElseIf (2 = stPayInfo.iAction) Then    'Failed by Host
                        stPayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                        stPayInfo.iTxnStatus = Common.TXN_STATUS.TXN_FAILED
                        stPayInfo.szTxnMsg = Common.C_TXN_FAILED_1
                        stPayInfo.iTxnState = Common.TXN_STATE.COMMIT_FAILED
                        iSendReply2Merchant()
                    ElseIf (3 = stPayInfo.iAction) Then    'Unconfirmed status from Host, need to query Host until getting reply or timeout
                        'Update Request table with Action 3 for Retry Service to pick up and query the respective Host
                        stPayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_PENDING
                        stPayInfo.iTxnStatus = Common.TXN_STATUS.TXN_PENDING
                        stPayInfo.szTxnMsg = "Host returned not processed/unknown status, pending query Host"
                        stPayInfo.iTxnState = Common.TXN_STATE.RECV_FROM_HOST

                        CheckStatusButton.Visible = True 'make the button visible
                    End If

                End If

            Else
                Return True
            End If
        Catch ex As Exception
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                           DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                          " > " + stPayInfo.szIssuingBank + " bCheckStatus() : " + ex.ToString())
        End Try

    End Function

    '********************************************************************************************
    ' Class Name:		CheckStatus (copied from Response_fpx)
    ' Function Name:	ResponseErrHandler
    ' Function Type:	NONE
    ' Parameter:        
    ' Description:		Error handler
    ' History:			22 Aug 2009
    '********************************************************************************************
    Private Sub ResponseErrHandler()
        Dim szHTTPString As String = ""
        Dim iRet As Integer = -1

        Try
            'Generates response hash value
            'stPayInfo.szHashValue = objHash.szGetSaleResHash(stPayInfo)
            stPayInfo.szHashValue2 = objHash.szGetSaleResHash2(stPayInfo)
            'Generates HTTP string to be replied to merchant
            szHTTPString = objResponse.szGetReplyMerchantHTTPString(stPayInfo, stHostInfo)

            'Send HTTP string reply to merchant using the same method used by Host
            'Modified 4 Oct 2016, added st_HostInfo, KTB redirect resp query
            If (stPayInfo.szMerchantReturnURL <> "") Then
                iRet = objResponse.iHTTPSend(stHostInfo.iHostReplyMethod, szHTTPString, stPayInfo.szMerchantReturnURL, stPayInfo, stHostInfo, g_szHTML)
                If (iRet <> 0) Then
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > Failed to send error reply iRet(" + CStr(iRet) + ") " + szHTTPString + " to merchant URL " + stPayInfo.szMerchantReturnURL)
                Else
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > Error reply to merchant sent.")
                End If
            Else
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > MerchantRURL empty. Bypass sending error reply " + szHTTPString + " to merchant")
            End If

            'Another alternative - NOTE: If MerchantRURL contains "?" and param, can not use this alternative
            'coz it does not support additional params in the error template.
            'objResponse.iLoadErrUI(stPayInfo.szMerchantReturnURL, stPayInfo, g_szHTML)
        Catch ex As Exception
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            " > ResponseErrHandler() Exception: " + ex.Message)
        End Try
    End Sub

    Public Sub InitLogger()
        If (objLoggerII Is Nothing) Then

            If (szLogPath Is Nothing) Then
                szLogPath = "C:\\OB.log"
            End If

            If (iLogLevel < 0 Or iLogLevel > 5) Then
                iLogLevel = 3
            End If

            'stPayInfo.szIssuingBank = Request("IssuingBank")
            'objLoggerII = New LoggerII.CLoggerII(
            objLoggerII = LoggerII.CLoggerII.GetInstanceX(szLogPath + "_" + stPayInfo.szIssuingBank + ".log", iLogLevel, 255)

        End If
    End Sub

    Public Function iSendReply2Merchant() As Integer
        Dim szHTTPString As String = ""
        Dim iRet As Integer = -1
        Dim szMerchantReturnURL As String = ""

        Try
            'Generates response hash value
            'stPayInfo.szHashValue = objHash.szGetSaleResHash(stPayInfo)
            stPayInfo.szHashValue2 = objHash.szGetSaleResHash2(stPayInfo)

            'Generates HTTP string to be replied to merchant
            szHTTPString = szGetReplyMerchantHTTPString()

            szMerchantReturnURL = stPayInfo.szMerchantReturnURL
            If ("PAY" = stPayInfo.szTxnType.ToUpper()) Then
                If (0 = stPayInfo.iMerchantTxnStatus) Then
                    If (stPayInfo.szMerchantApprovalURL <> "") Then
                        szMerchantReturnURL = stPayInfo.szMerchantApprovalURL
                    End If
                Else
                    If (stPayInfo.szMerchantUnApprovalURL <> "") Then
                        szMerchantReturnURL = stPayInfo.szMerchantUnApprovalURL
                    End If
                End If
            End If

            'Send HTTP string reply to merchant using the same method used by Host
            iRet = iHTTPSend(1, szHTTPString, szMerchantReturnURL, "")
            If (iRet <> 0) Then
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > Failed to send reply iRet(" + CStr(iRet) + ") " + szHTTPString + " to merchant URL " + szMerchantReturnURL)
            End If

            ' Callback - START
            If (1 = stPayInfo.iAllowCallBack) Then
                If ("" <> stPayInfo.szMerchantCallbackURL) Then

                    Dim iCallBackStatus As Integer = -1
                    objTxnProc.bCheckCallBackStatus(stPayInfo, iCallBackStatus)

                    If (1 <> iCallBackStatus) Then
                        Dim szResp As String = ""
                        Dim szCallbackStatus As String = ""

                        'ORIGINAL PLAN: Wait for "ok"/"OK" to stop sending response, maximum 3 times
                        'callback response string
                        szHTTPString = szGetReplyMerchantHTTPString(1)
                        'iRet = objHTTP.iHTTPCallbackPostWithRecv(st_PayInfo.szMerchantReturnURL, szHTTPString, st_PayInfo.iHttpTimeoutSeconds, szResp, "")

                        For t = 0 To 4 '2
                            iRet = objHTTP.iHTTPostWithRecv(stPayInfo.szMerchantCallbackURL, szHTTPString, stPayInfo.iHttpTimeoutSeconds, szResp, "")

                            If (0 = iRet) Then
                                szCallbackStatus = "success"
                                iCallBackStatus = 1     'S2Scallback. To check is Callback had been successfully sent before

                                If ("" <> szResp.Trim()) Then
                                    szCallbackStatus = szCallbackStatus + " and recv acknowledgement (" + Left(szResp.Trim(), 10) + " ....)"
                                    Exit For
                                End If
                            Else
                                szCallbackStatus = "fail"
                                iCallBackStatus = 0     'S2Scallback
                            End If

                            Thread.Sleep(50)
                        Next

                        objTxnProc.bInsertCallBackStatus(stPayInfo, iCallBackStatus)    'S2Scallback

                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "Callback response [" + szHTTPString + "], Status [" + szCallbackStatus + "]")
                    End If
                End If
            End If
            'Callback - END

        Catch ex As Exception
            iRet = -1
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > iSendReply2Merchant() Exception: " + ex.Message)
        End Try

        Return iRet
    End Function

    Public Function szGetReplyMerchantHTTPString(Optional ByVal iCallBack As Integer = 0) As String
        Dim szHTTPString As String = Nothing

        'support S2S to bank but redirect back to merchant
        If (1 <> stPayInfo.iRespMethod) Then 'Server2Server
            'szHTTPString = "TxnType=" + Server.UrlEncode(stPayInfo.szTxnType) + "&"
            'szHTTPString = szHTTPString + "Method=" + stPayInfo.szPymtMethod + "&"
            'szHTTPString = szHTTPString + "MerchantID=" + stPayInfo.szMerchantID + "&"
            'szHTTPString = szHTTPString + "MerchantPymtID=" + stPayInfo.szMerchantTxnID + "&"
            'szHTTPString = szHTTPString + "MerchantOrdID=" + Server.UrlEncode(stPayInfo.szMerchantOrdID) + "&"
            'szHTTPString = szHTTPString + "MerchantTxnAmt=" + stPayInfo.szTxnAmount + "&"
            'szHTTPString = szHTTPString + "MerchantCurrCode=" + Server.UrlEncode(stPayInfo.szCurrencyCode) + "&"
            'szHTTPString = szHTTPString + "Sign=" + stPayInfo.szHashValue + "&"   'moved up here and added + "&"
            'szHTTPString = szHTTPString + "PTxnID=" + stPayInfo.szTxnID + "&"
            'szHTTPString = szHTTPString + "AcqBank=" + Server.UrlEncode(stPayInfo.szIssuingBank) + "&"
            'szHTTPString = szHTTPString + "PTxnStatus=" + stPayInfo.iMerchantTxnStatus.ToString() + "&"
            'szHTTPString = szHTTPString + "AuthCode=" + Server.UrlEncode(stPayInfo.szAuthCode) + "&"
            'szHTTPString = szHTTPString + "BankRefNo=" + Server.UrlEncode(stPayInfo.szHostTxnID) + "&"
            'szHTTPString = szHTTPString + "PTxnMsg=" + Server.UrlEncode(stPayInfo.szTxnMsg)             'removed + "&"
            g_JSONResp.Add("TxnType", stPayInfo.szTxnType)
            g_JSONResp.Add("Method", stPayInfo.szPymtMethod)
            g_JSONResp.Add("MerchantID", stPayInfo.szMerchantID)
            g_JSONResp.Add("MerchantPymtID", stPayInfo.szMerchantTxnID)
            g_JSONResp.Add("MerchantOrdID", stPayInfo.szMerchantOrdID)
            g_JSONResp.Add("MerchantTxnAmt", stPayInfo.szTxnAmount)
            g_JSONResp.Add("MerchantCurrCode", stPayInfo.szCurrencyCode)
            g_JSONResp.Add("PTxnID", stPayInfo.szTxnID)
            g_JSONResp.Add("PTxnStatus", stPayInfo.iMerchantTxnStatus.ToString())
            g_JSONResp.Add("PTxnMsg", System.Uri.EscapeDataString(stPayInfo.szTxnMsg))
            g_JSONResp.Add("AcqBank", stPayInfo.szIssuingBank)
            g_JSONResp.Add("AuthCode", stPayInfo.szAuthCode)
            g_JSONResp.Add("BankRefNo", stPayInfo.szHostTxnID)
            g_JSONResp.Add("Sign", stPayInfo.szHashValue2)

            szHTTPString = Server.UrlEncode(objJS.Serialize(g_JSONResp))
        Else    'Redirect
            szHTTPString = "<INPUT type='hidden' name='TxnType' value='" + Server.UrlEncode(stPayInfo.szTxnType) + "'>" + vbCrLf
            szHTTPString = szHTTPString + "<INPUT type='hidden' name='Method' value='" + stPayInfo.szPymtMethod + "'>" + vbCrLf
            szHTTPString = szHTTPString + "<INPUT type='hidden' name='MerchantID' value='" + stPayInfo.szMerchantID + "'>" + vbCrLf
            szHTTPString = szHTTPString + "<INPUT type='hidden' name='MerchantPymtID' value='" + stPayInfo.szMerchantTxnID + "'>" + vbCrLf
            szHTTPString = szHTTPString + "<INPUT type='hidden' name='MerchantOrdID' value='" + Server.UrlEncode(stPayInfo.szMerchantOrdID) + "'>" + vbCrLf
            szHTTPString = szHTTPString + "<INPUT type='hidden' name='MerchantTxnAmt' value='" + stPayInfo.szTxnAmount + "'>" + vbCrLf
            szHTTPString = szHTTPString + "<INPUT type='hidden' name='MerchantCurrCode' value='" + stPayInfo.szCurrencyCode + "'>" + vbCrLf
            szHTTPString = szHTTPString + "<INPUT type='hidden' name='Sign' value='" + stPayInfo.szHashValue + "'>" + vbCrLf
            szHTTPString = szHTTPString + "<INPUT type='hidden' name='PTxnID' value='" + stPayInfo.szTxnID + "'>" + vbCrLf
            szHTTPString = szHTTPString + "<INPUT type='hidden' name='AcqBank' value='" + Server.UrlEncode(stPayInfo.szIssuingBank) + "'>" + vbCrLf
            szHTTPString = szHTTPString + "<INPUT type='hidden' name='PTxnStatus' value='" + stPayInfo.iMerchantTxnStatus.ToString() + "'>" + vbCrLf
            szHTTPString = szHTTPString + "<INPUT type='hidden' name='AuthCode' value='" + Server.UrlEncode(stPayInfo.szAuthCode) + "'>" + vbCrLf
            szHTTPString = szHTTPString + "<INPUT type='hidden' name='BankRefNo' value='" + Server.UrlEncode(stPayInfo.szHostTxnID) + "'>" + vbCrLf
            szHTTPString = szHTTPString + "<INPUT type='hidden' name='PTxnMsg' value='" + Server.UrlEncode(stPayInfo.szTxnMsg) + "'>" + vbCrLf
        End If

        'forming Callback reply string
        If (1 = iCallBack) Then
            'szHTTPString = "TxnType=" + Server.UrlEncode(stPayInfo.szTxnType) + "&"
            'szHTTPString = szHTTPString + "Method=" + stPayInfo.szPymtMethod + "&"
            'szHTTPString = szHTTPString + "MerchantID=" + stPayInfo.szMerchantID + "&"
            'szHTTPString = szHTTPString + "MerchantPymtID=" + stPayInfo.szMerchantTxnID + "&"
            'szHTTPString = szHTTPString + "MerchantOrdID=" + Server.UrlEncode(stPayInfo.szMerchantOrdID) + "&"
            'szHTTPString = szHTTPString + "MerchantTxnAmt=" + stPayInfo.szTxnAmount + "&"
            'szHTTPString = szHTTPString + "MerchantCurrCode=" + Server.UrlEncode(stPayInfo.szCurrencyCode) + "&"
            'szHTTPString = szHTTPString + "Sign=" + stPayInfo.szHashValue + "&"   'moved up here and added + "&"
            'szHTTPString = szHTTPString + "PTxnID=" + stPayInfo.szTxnID + "&"
            'szHTTPString = szHTTPString + "AcqBank=" + Server.UrlEncode(stPayInfo.szIssuingBank) + "&"
            'szHTTPString = szHTTPString + "PTxnStatus=" + stPayInfo.iMerchantTxnStatus.ToString() + "&"
            'szHTTPString = szHTTPString + "AuthCode=" + Server.UrlEncode(stPayInfo.szAuthCode) + "&"
            'szHTTPString = szHTTPString + "BankRefNo=" + Server.UrlEncode(stPayInfo.szHostTxnID) + "&"
            'szHTTPString = szHTTPString + "PTxnMsg=" + Server.UrlEncode(stPayInfo.szTxnMsg)             'removed + "&"

            g_JSONResp.Add("TxnType", stPayInfo.szTxnType)
            g_JSONResp.Add("Method", stPayInfo.szPymtMethod)
            g_JSONResp.Add("MerchantID", stPayInfo.szMerchantID)
            g_JSONResp.Add("MerchantPymtID", stPayInfo.szMerchantTxnID)
            g_JSONResp.Add("MerchantOrdID", stPayInfo.szMerchantOrdID)
            g_JSONResp.Add("MerchantTxnAmt", stPayInfo.szTxnAmount)
            g_JSONResp.Add("MerchantCurrCode", stPayInfo.szCurrencyCode)
            g_JSONResp.Add("PTxnID", stPayInfo.szTxnID)
            g_JSONResp.Add("PTxnStatus", stPayInfo.iMerchantTxnStatus.ToString())
            g_JSONResp.Add("PTxnMsg", System.Uri.EscapeDataString(stPayInfo.szTxnMsg))
            g_JSONResp.Add("AcqBank", stPayInfo.szIssuingBank)
            g_JSONResp.Add("AuthCode", stPayInfo.szAuthCode)
            g_JSONResp.Add("BankRefNo", stPayInfo.szHostTxnID)
            g_JSONResp.Add("Sign", stPayInfo.szHashValue2)

            szHTTPString = Server.UrlEncode(objJS.Serialize(g_JSONResp))
        End If

        Return szHTTPString
    End Function

    '********************************************************************************************
    ' Class Name:		CheckStatus(copied from Payment)
    ' Function Name:	iHTTPSend
    ' Function Type:	Integer. Return 0 if success else -1
    ' Parameter:        i_SendMethod [in, integer]
    '                   - 2 (server-to-server, post send ONLY);
    '                   - 3 (server-to-server, post send and wait for reply)
    '                   - 5 (server-to-server, get send and wait for reply)
    '                   sz_HTTPString [in/out, string]  - HTTP string to be sent
    '                   sz_URL [in, string]         - Sending URL
    ' Description:		HTTP send function to send HTTP String to the specified URL
    ' History:			17 Dec 2008
    '                   addded LogString param
    '                   added SecurityProtocol param
    Public Function iHTTPSend(ByVal i_SendMethod As Integer, ByRef sz_HTTPString As String, ByVal sz_URL As String, ByVal sz_LogString As String, Optional ByVal sz_SecurityProtocol As String = "TLS") As Integer
        Dim iRet As Integer = 0
        Dim szURL() As String
        Dim iPosition As Integer = 0
        Dim szHTTPResponse As String = ""
        Dim szPostMethod() As String        'for Bitnet
        Dim szAuthenticate As String = ""   'for Bitnet

        Try
            stPayInfo.iHttpTimeoutSeconds = Convert.ToInt32(ConfigurationManager.AppSettings("HttpTimeoutSeconds"))

            'added checking of SendMethod <> 1 And <> 4, followed Response.vb's iHTTPSend()
            'added checking of sz_HTTPString
            If (sz_HTTPString <> "" And i_SendMethod <> 1 And i_SendMethod <> 4) Then
                'Cater for MerchantRURL that contains "?" or "$", e.g. "http://MerchantRURL?page=1&event=response" or https://149.122.26.33:6443/skylights/cgi-bin/skylights?page=DEBITRETURN&event=response
                'For merchants that use Get method(Query string), they can use "$" to replace "?" if they need to include
                '"?" in MerchantRURL param, e.g. Response.Redirect("http://url/page?MerchantRURL$page=1").

                iPosition = InStr(sz_URL, "?")  'e.g. 25 which is 1 base of the location
                If (iPosition > 0) Then
                    szURL = Split(sz_URL, "?")
                    If ((InStr(sz_URL, "?=") <= 0) And (InStr(sz_URL, "=") > iPosition)) Then
                        sz_HTTPString = szURL(1) + "&" + sz_HTTPString
                    End If
                    sz_URL = szURL(0)
                Else
                    iPosition = InStr(sz_URL, "$")  'e.g. 25 which is 1 base of the location
                    If (iPosition > 0) Then
                        szURL = Split(sz_URL, "$")
                        If ((InStr(sz_URL, "$=") <= 0) And (InStr(sz_URL, "=") > iPosition)) Then
                            sz_HTTPString = szURL(1) + "&" + sz_HTTPString
                        End If
                        sz_URL = szURL(0)
                    End If
                End If
            End If

            'Added Send Method 1 (Redirect) 
            If (1 = i_SendMethod Or 4 = i_SendMethod) Then
                'checking of "?"
                szURL = Split(sz_URL, "?")
                sz_URL = szURL(0)
                '///////////////////////////////////////////

                iLoadRedirectUI(sz_URL, sz_HTTPString, g_szHTML)

                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > Redirected to [" + sz_URL + "] " + sz_HTTPString) 'sz_LogString) 'sz_HTTPString) - 

            ElseIf (2 = i_SendMethod) Then
                'HTTP post ONLY
                iRet = objHTTP.iHTTPostOnly(sz_URL, sz_HTTPString, "application/x-www-form-urlencoded", stPayInfo.iHttpTimeoutSeconds, sz_SecurityProtocol)
                If (iRet <> 0) Then
                    Return iRet
                End If

                ' added Security Protocol
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                 DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                 stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > Posted " + sz_LogString + " to " + sz_URL + " Protocol(" + sz_SecurityProtocol + ")")

            ElseIf (3 = i_SendMethod) Then
                'HTTP POST and wait for reply until timeout
                'added Security Protocol
                iRet = objHTTP.iHTTPostWithRecv(sz_URL, sz_HTTPString, stPayInfo.iHttpTimeoutSeconds, szHTTPResponse, "", sz_SecurityProtocol)
                If (iRet <> 0) Then
                    Return iRet
                End If

                'added checking of iLogRes
                If (1 = stHostInfo.iLogRes) Then
                    'added Security Protocol
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > Posted " + sz_LogString + " to " + sz_URL + " and received response [" + szHTTPResponse + "] Protocol[" + sz_SecurityProtocol + "]") 'Modified  2 Jun 2014. Firefly FF. sz_HTTPString change to sz_LogString
                Else
                    'added Security Protocol
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > Posted " + sz_LogString + " to " + sz_URL + " and received response [log disabled] Protocol[" + sz_SecurityProtocol + "]")
                End If

                sz_HTTPString = szHTTPResponse

            ElseIf (5 = i_SendMethod) Then
                'HTTP GET and wait for reply until timeout
                'added Security Protocol
                iRet = objHTTP.iHTTPGetWithRecv(sz_URL, sz_HTTPString, stPayInfo.iHttpTimeoutSeconds, szHTTPResponse, sz_SecurityProtocol)
                If (iRet <> 0) Then
                    Return iRet
                End If

                'added Security Protocol
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > Posted " + sz_LogString + " to " + sz_URL + " and received response [" + szHTTPResponse + "] Protocol[" + sz_SecurityProtocol + "]")  'Modified  2 Jun 2014. Firefly FF. sz_HTTPString change to sz_LogString

                sz_HTTPString = szHTTPResponse

            ElseIf (6 = i_SendMethod) Then
                'HTTP POST and wait for reply until timeout
                'added Security Protocol
                iRet = objHTTP.iHTTPostWithRecvEx(sz_URL, sz_HTTPString, stPayInfo.iHttpTimeoutSeconds, "text/xml", szHTTPResponse, sz_SecurityProtocol)

                If (iRet <> 0) Then
                    Return iRet
                End If

                'added Security Protocol
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > Posted " + sz_LogString + " to " + sz_URL + " and received response [" + szHTTPResponse + "] Protocol[" + sz_SecurityProtocol + "]")  'Modified  2 Jun 2014. Firefly FF. sz_HTTPString change to sz_LogString

                sz_HTTPString = szHTTPResponse

            ElseIf (7 = i_SendMethod) Then  'GlobalPay CyberSource
                'HTTP POST with header authentication and wait for reply until timeout
                'added Security Protocol
                'added "" as sz_ContentType
                'added "" as sz_Authenticate
                iRet = objHTTP.iHTTPostWithRecvAuth(sz_URL, sz_HTTPString, stPayInfo.iHttpTimeoutSeconds, szHTTPResponse, "", "", "", stHostInfo.szTID, stHostInfo.szAcquirerID, sz_SecurityProtocol)
                If (iRet <> 0) Then
                    Return iRet
                End If

                'added Security Protocol
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > Posted " + sz_LogString + " to " + sz_URL + " and received response [" + szHTTPResponse + "] Protocol[" + sz_SecurityProtocol + "]")

                sz_HTTPString = szHTTPResponse

            ElseIf (8 = i_SendMethod) Then 'KTB
                'HTTP GET and retry query DB for status
                'added Security Protocol
                iRet = objHTTP.iHTTPGetWithRecvDB(stPayInfo, stHostInfo, objCommon, sz_URL, sz_HTTPString, stPayInfo.iHttpTimeoutSeconds, szHTTPResponse, sz_SecurityProtocol)
                If (iRet <> 0) Then
                    Return iRet
                End If

                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > Posted " + sz_LogString + " to " + sz_URL + " and received response [" + szHTTPResponse + "] Protocol[" + sz_SecurityProtocol + "]")

                sz_HTTPString = szHTTPResponse

            ElseIf (9 = i_SendMethod) Then  'Bitnet
                'HTTP POST with header authentication and wait for reply until timeout, using JSON format

                szPostMethod = Split(stPayInfo.szPostMethod, "&")

                If ("BEARER" = szPostMethod(1).ToUpper()) Then
                    szAuthenticate = szPostMethod(1) + " " + stPayInfo.szParam1
                Else
                    szAuthenticate = szPostMethod(1)
                End If

                iRet = objHTTP.iHTTPostWithRecvAuth(sz_URL, sz_HTTPString, stPayInfo.iHttpTimeoutSeconds, szHTTPResponse, "", "json", szAuthenticate, stPayInfo.szEncryptKeyPath, stPayInfo.szDecryptKeyPath, sz_SecurityProtocol)
                If (iRet <> 0) Then
                    Return iRet
                End If

                'added Security Protocol
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > Posted " + sz_LogString + " to " + sz_URL + " and received response [" + szHTTPResponse + "] Protocol[" + sz_SecurityProtocol + "]")

                sz_HTTPString = szHTTPResponse
            End If

        Catch ex As Exception
            iRet = -1

            stPayInfo.szErrDesc = "iHTTPSend() exception: " + ex.StackTrace + " " + ex.Message
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > iHTTPSend() exception: " + ex.Message + " for sending " + sz_HTTPString + " to " + sz_URL)
        End Try

        Return iRet
    End Function

    '********************************************************************************************
    ' Class Name:		CheckStatus (copied from Payment)
    ' Function Name:	iLoadRedirectUI
    ' Function Type:	Integer
    ' Parameter:		sz_HTML [out, string]   -   Customized content of the web page
    ' Description:		Load the redirect (auto submit hidden fields) template
    ' History:			11 Apr 2010
    '********************************************************************************************
    Public Function iLoadRedirectUI(ByVal sz_URL As String, ByVal sz_HTTPString As String, ByRef sz_HTML As String) As Integer
        Dim iReturn As Integer = 0
        Dim szTemplatePath As String = ""
        Dim szHTMLContent As String = ""

        szTemplatePath = ConfigurationManager.AppSettings("RedirectTemplate")

        If szTemplatePath <> "" Then
            GetTemplate(szTemplatePath, szHTMLContent)

            If szHTMLContent <> "" Then
                objCommon.szFormatPlaceHolder(szHTMLContent, "[HOSTPARAMS]", sz_HTTPString)
                objCommon.szFormatPlaceHolder(szHTMLContent, "[HOSTURL]", sz_URL)

                sz_HTML = szHTMLContent
            Else
                iReturn = -1
            End If
        Else
            iReturn = -1
        End If

        Return iReturn
    End Function

    Private Function GetTemplate(ByVal sz_FilePath As String, ByRef sz_HTMLContent As String) As Boolean
        Dim objStreamReader As StreamReader = Nothing
        Dim szTemplateFile As String = ""
        Dim bReturn As Boolean = True

        Try
            szTemplateFile = Server.MapPath(sz_FilePath)

            'Load page
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > Load (" + szTemplateFile + ")")

            objStreamReader = System.IO.File.OpenText(szTemplateFile)
            sz_HTMLContent = objStreamReader.ReadToEnd()
            objStreamReader.Close()
        Catch ex As Exception
            Trace.Write(ex.ToString)
            bReturn = False
        Finally
            If Not objStreamReader Is Nothing Then objStreamReader = Nothing
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		CheckStatus 
    ' Function Name:	 MessageBox
    ' Function Type:	 
    ' Parameter:		MessageBox ["string"]
    ' Description:		to display a message box or alert
    ' History:			Jan 2016
    '********************************************************************************************
    Private Sub MessageBox(ByVal msg As String)
        Dim lbl As New Label
        lbl.Text = "<script language='javascript'>" & Environment.NewLine &
               "window.alert('" + msg + "')</script>"
        Page.Controls.Add(lbl)
    End Sub


    Protected Sub CheckStatusButton_Click(sender As Object, e As EventArgs) Handles CheckStatusButton.Click
        Dim bRet As Boolean
        Dim bGetMerchantInfo As Boolean = False

        Try
            stPayInfo.szTxnID = Request.QueryString("ID")

            'Added, 10 Mar 2016, add hash value verification
            stPayInfo.szMerchantID = Request.QueryString("MerchantID")    'Added, 8 Mar 2016, M2U2

            If True <> objTxnProc.bGetMerchant(stPayInfo) Then
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > " + stPayInfo.szErrDesc)

                lblResult.Text = "Unable to check status. If you have made payment, please email TxnID[" & stPayInfo.szTxnID & "] to " + ConfigurationManager.AppSettings("SupportEmail") + " for further assistance."

                GoTo Cleanup
            End If

            stPayInfo.szHashKey = Request.QueryString("MerchantID") + Request.QueryString("ID") + Request.QueryString("AcqBank") 'Request.QueryString("IssuingBank")
            stPayInfo.szHashMethod = "SHA512" 'stPayInfo.szHashMethod = "SHA2"
            stPayInfo.szHashValue = Server.UrlDecode(Request("Sign")).Replace(" ", "+")

            If True <> objHash.bCheckReqHash(stPayInfo) Then
                lblResult.Text = "Invalid request. If you have already made the payment, please email to " + ConfigurationManager.AppSettings("SupportEmail") + " for further assistance."

                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > " + stPayInfo.szErrDesc)
                GoTo Cleanup
            End If

            stPayInfo.szPymtMethod = C_PYMT_METHOD
            bRet = objTxnProcOB.bGetResTxn(stPayInfo, stHostInfo)
            If (bRet = True) Then
                If (-1 = stPayInfo.iQueryStatus) Then   ' -1 indicate that record found in Res table
                    If (0 = stPayInfo.iTxnStatus) Then
                        lblResult.Text = "Payment successful"
                        Thread.Sleep(2000)
                        bGetMerchantInfo = objTxnProc.bGetMerchant(stPayInfo)

                        iSendReply2Merchant()

                    ElseIf (1 = stPayInfo.iTxnStatus) Then
                        lblResult.Text = "Payment failed"
                        Thread.Sleep(2000)
                        bGetMerchantInfo = objTxnProc.bGetMerchant(stPayInfo)

                        iSendReply2Merchant()

                    ElseIf (6 = stPayInfo.iTxnStatus) Then
                        lblResult.Text = "Transaction expired. If you have made payment, please email TxnID[" & stPayInfo.szTxnID & "] to " + ConfigurationManager.AppSettings("SupportEmail") + " for further assistance."
                        GoTo Cleanup
                    End If
                Else
                    'Send query 
                    stPayInfo.szTxnID = Request.QueryString("ID")

                    If True <> objTxnProcOB.bGetReqTxn(stPayInfo, stHostInfo) Then
                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                        stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > " + stPayInfo.szErrDesc)

                        lblResult.Text = "Unable to check status. If you have made payment, please email TxnID[" & stPayInfo.szTxnID & "] to " + ConfigurationManager.AppSettings("SupportEmail") + " for further assistance."

                        GoTo Cleanup
                    End If

                    stPayInfo.szTxnType = "QUERY"

                    'GetTerminal account details based on Currency Code
                    If True <> objTxnProcOB.bGetTerminal(stHostInfo, stPayInfo) Then
                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                        stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > " + stPayInfo.szErrDesc)

                        lblResult.Text = "Unable to check status. If you have made payment, please email TxnID[" & stPayInfo.szTxnID & "] to " + ConfigurationManager.AppSettings("SupportEmail") + " for further assistance."

                        GoTo Cleanup
                    Else
                        ' Send Query to host/bank
                        If (objResponse.iSendQueryHost(stPayInfo, stHostInfo) < 0) Then
                            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                            stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + "> Error: Failed to send Query request to " + stPayInfo.szIssuingBank)

                            lblResult.Text = "Unable to check status. Please check status again or if you have made payment, please email TxnID[" & stPayInfo.szTxnID & "] to " + ConfigurationManager.AppSettings("SupportEmail") + " for further assistance."

                            GoTo Cleanup
                        End If

                        'Get Host's TxnStatus's action
                        If True <> objCommon.bGetTxnStatusAction(stHostInfo, stPayInfo) Then

                            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                            stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > " + stPayInfo.szErrDesc)

                            lblResult.Text = "Unable to check status. If you have made payment, please email TxnID[" & stPayInfo.szTxnID & "] to " + ConfigurationManager.AppSettings("SupportEmail") + " for further assistance."

                            GoTo Cleanup
                        End If

                        'Process Host reply based on action type configured in database according to Host TxnStatus
                        If (1 = stPayInfo.iAction) Then                'Payment approved by Host
                            stPayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_SUCCESS

                            'Modified  29 April 2015. Auth&Capture
                            If ("AUTH" = stPayInfo.szTxnType) Then
                                stPayInfo.iTxnStatus = Common.TXN_STATUS.TXN_AUTH_SUCCESS
                            Else
                                stPayInfo.iTxnStatus = Common.TXN_STATUS.TXN_SUCCESS
                            End If

                            stPayInfo.szTxnMsg = Common.C_TXN_SUCCESS_0
                            stPayInfo.iTxnState = Common.TXN_STATE.COMMIT

                            'Added, 19 Apr 2015, for OTC
                            If ("OTC" = stPayInfo.szPymtMethod) Then
                                If (-1 = stPayInfo.iQueryStatus) Then  'Record exists in Request table
                                    stPayInfo.iAction = 4  'Retry Service to pickup and only send response to merchat if exceeded permissible Reversal period
                                End If
                            End If

                            iSendReply2Merchant()

                        ElseIf (2 = stPayInfo.iAction) Then    'Failed by Host
                            stPayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                            stPayInfo.iTxnStatus = Common.TXN_STATUS.TXN_FAILED
                            stPayInfo.szTxnMsg = Common.C_TXN_FAILED_1
                            stPayInfo.iTxnState = Common.TXN_STATE.COMMIT_FAILED

                            iSendReply2Merchant()

                        ElseIf (3 = stPayInfo.iAction) Then    'Unconfirmed status from Host, need to query Host until getting reply or timeout
                            'Update Request table with Action 3 for Retry Service to pick up and query the respective Host
                            stPayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_PENDING
                            stPayInfo.iTxnStatus = Common.TXN_STATUS.TXN_PENDING
                            stPayInfo.szTxnMsg = "Host returned not processed/unknown status, pending query Host"
                            stPayInfo.iTxnState = Common.TXN_STATE.RECV_FROM_HOST

                            lblResult.Text = "We did not receive your payment. Please check status again or if you have already made the payment, please email TxnID[" & stPayInfo.szTxnID & "] to " + ConfigurationManager.AppSettings("SupportEmail") + " for further assistance."
                        End If
                    End If
                End If
            Else
                lblResult.Text = "Unable to check status. If you have already made the payment, please email TxnID[" & stPayInfo.szTxnID & "] to " + ConfigurationManager.AppSettings("SupportEmail") + " for further assistance."
            End If
        Catch ex As Exception
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                           DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                          " > " + stPayInfo.szIssuingBank + " Exception: " + ex.Message)
        End Try

Cleanup:  'Added, 10 Mar 2016
    End Sub

    '********************************************************************************************
    ' Class Name:		Payment
    ' Function Name:	GetAllHTTPVal
    ' Function Type:	Boolean.  Return False if error.
    ' Parameter:		NONE
    ' Description:		Get all form's or query string's values
    ' History:			4 Oct 2008
    '********************************************************************************************
    Public Function szGetAllHTTPVal() As String
        Dim szFormValues As String
        Dim szTempKey As String
        Dim szTempVal As String

        Dim iCount As Integer = 0
        Dim iLoop As Integer = 0

        'Added, 11 Oct 2013, session ID
        'Added, 25 Oct 2013, UserHostName, UserBrowser
        szFormValues = "[UserIP: " + Request.UserHostAddress + "] [UserHostName: " + Request.UserHostName + "] [UserBrowser:" + Request.UserAgent + "] [SessionID:" + HttpContext.Current.Session.SessionID.ToString() + "] "

        iCount = Request.Form.Count

        'Modified 8 Aug 2013
        'objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
        '                DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
        '                " > No. of Form Request Fields (" + iCount.ToString() + ")")

        If (iCount > 0) Then
            For iLoop = 1 To iCount
                szTempKey = ""
                szTempKey = Request.Form.GetKey(iLoop - 1)

                szFormValues = szFormValues + szTempKey + "="

                szTempVal = ""
                szTempVal = Server.UrlDecode(Request.Form.Get(iLoop - 1))

                'Mask CardPAN
                If ((szTempKey <> "")) Then
                    If (szTempKey.ToLower = "cardpan") Or (szTempKey.ToLower = "cardno") Or (szTempKey.ToLower = "cardno_1") Or (szTempKey.ToLower = "cardno_2") Or (szTempKey.ToLower = "cardno_3") Or (szTempKey.ToLower = "cardno_4") Or (szTempKey.ToLower = "epmonth2") Or (szTempKey.ToLower = "epyear2") Or (szTempKey.ToLower = "cvv2") Or (szTempKey.ToLower = "cardcvv2") Or (szTempKey.ToLower = "param2") Or (szTempKey.ToLower = "param3") Or (szTempKey.ToLower = "cardexp") Then
                        If szTempVal.Length > 10 Then
                            szTempVal = szTempVal.Substring(0, 6) + "".PadRight(szTempVal.Length - 6 - 4, "X") + szTempVal.Substring(szTempVal.Length - 4, 4)
                            'szFormValues = szFormValues + szTempVal.Substring(0, 6) + "".PadRight(szTempVal.Length - 6 - 4, "X") + szTempVal.Substring(szTempVal.Length - 4, 4) + "&"
                        Else
                            szTempVal = "".PadRight(szTempVal.Length, "X")
                            'szFormValues = szFormValues + "".PadRight(szTempVal.Length, "X") + "&"
                        End If
                    End If

                    'Modified 8 Aug 2013
                    'objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
                    '            DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
                    '            " > Field(" + szTempKey + ") Value(" + szTempVal + ")")

                    If (iLoop = iCount) Then
                        szFormValues = szFormValues + szTempVal
                    Else
                        szFormValues = szFormValues + szTempVal + "&"
                    End If
                End If
            Next iLoop
        Else
            iCount = Request.QueryString.Count

            'objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
            '                DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
            '                " > No. of QueryString Request Fields (" + iCount.ToString() + ")")

            For iLoop = 1 To iCount
                szTempKey = ""
                szTempKey = Request.QueryString.GetKey(iLoop - 1)

                szFormValues = szFormValues + szTempKey + "="

                szTempVal = ""
                szTempVal = Server.UrlDecode(Request.QueryString.Get(iLoop - 1))

                'Mask CardPAN
                If ((szTempKey <> "")) Then
                    If (szTempKey.ToLower = "cardpan") Or (szTempKey.ToLower = "cardno") Or (szTempKey.ToLower = "cardno_1") Or (szTempKey.ToLower = "cardno_2") Or (szTempKey.ToLower = "cardno_3") Or (szTempKey.ToLower = "cardno_4") Or (szTempKey.ToLower = "epmonth2") Or (szTempKey.ToLower = "epyear2") Or (szTempKey.ToLower = "cvv2") Or (szTempKey.ToLower = "cardcvv2") Or (szTempKey.ToLower = "param2") Or (szTempKey.ToLower = "param3") Or (szTempKey.ToLower = "cardexp") Then
                        If szTempVal.Length > 10 Then
                            szTempVal = szTempVal.Substring(0, 6) + "".PadRight(szTempVal.Length - 6 - 4, "X") + szTempVal.Substring(szTempVal.Length - 4, 4)
                            'szFormValues = szFormValues + szTempVal.Substring(0, 6) + "".PadRight(szTempVal.Length - 6 - 4, "X") + szTempVal.Substring(szTempVal.Length - 4, 4) + "&"
                        Else
                            szTempVal = "".PadRight(szTempVal.Length, "X")
                            'szFormValues = szFormValues + "".PadRight(szTempVal.Length, "X") + "&"
                        End If
                    End If

                    'Modified 8 Aug 2013
                    'objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
                    '            DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
                    '            " > Field(" + szTempKey + ") Value(" + szTempVal + ")")

                    If (iLoop = iCount) Then
                        szFormValues = szFormValues + szTempVal
                    Else
                        szFormValues = szFormValues + szTempVal + "&"
                    End If
                End If
            Next iLoop
        End If

        Return szFormValues
    End Function

    Public Sub New()
        szLogPath = ConfigurationManager.AppSettings("LogPath")
        iLogLevel = Convert.ToInt32(ConfigurationManager.AppSettings("LogLevel"))

        '2 Apr 2010, moved InitLogger() from down to up here, or else Hash will get empty objLoggerII and encountered exception
        InitLogger()

        objTxnProc = New TxnProc(objLoggerII)
        objTxnProcOB = New TxnProcOB(objLoggerII)
        objCommon = New Common(objTxnProc, objTxnProcOB, objLoggerII)

        objHash = New Hash(objLoggerII)
        objResponse = New Response(objLoggerII)
        objHTTP = New CHTTP(objLoggerII)

    End Sub
End Class