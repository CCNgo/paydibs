'Imports Common
Imports LoggerII
Imports System.IO
Imports System
Imports System.Data
Imports System.Security.Cryptography
Imports System.Threading
Imports System.Text.RegularExpressions

'This class handles ENETS Redirected responses for TxnType "PAY" only.
'Other transaction types will not be a response page like this that the bank needs to return the reply, it would be
' OB Gateway sends the request and then waits for the bank reply within the same session.
'Therefore, a response page like this is not needed for other transaction types.
Public Class respENETS
    Inherits System.Web.UI.Page

    Public Const C_ISSUING_BANK = "ENETS"
    Public Const C_TXN_TYPE = "PAY"
    'Public Const C_CURRENCY_CODE = "SGD"    'Added on 04 Jan 2010  for ENETS, for Merchant Private Key retrieval based on MerchantID and CurrencyCode
    Public Const C_PYMT_METHOD = "OB"
    Public g_szHTML As String = ""
    Public objLoggerII As LoggerII.CLoggerII

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private objCommon As Common
    Private objTxnProcOB As TxnProcOB
    Private objTxnProc As TxnProc
    Private objHash As Hash
    Private objResponse As Response

    Private stHostInfo As Common.STHost
    Private stPayInfo As Common.STPayInfo

    Private szLogPath As String = ""
    Private iLogLevel As String = ""
    Private szReplyDateTime As String = ""
    Private bTimeOut As Boolean = False

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim bGetHostInfo As Boolean = False

        Try
            'Get Host's reply date and time
            szReplyDateTime = System.DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss.fff")

            'Set to No Cache to make this page expired when user clicks the "back" button on browser
            Response.Cache.SetNoStore()

            If Not Page.IsPostBack Then
                'Initialization of HostInfo and PayInfo structures
                objCommon.InitHostInfo(stHostInfo)
                objCommon.InitPayInfo(stPayInfo)

                'stPayInfo.szMerchantID = Request("MerchantID")        'Added on 04 Jan 2010  for ENETS; Modified 27 Apr 2011, modified constant C_SERVICE_ID to Request("MerchantID")
                stPayInfo.szIssuingBank = C_ISSUING_BANK
                stPayInfo.szTxnType = C_TXN_TYPE
                'stPayInfo.szCurrencyCode = C_CURRENCY_CODE  'Added on 04 Jan 2010  for ENETS
                stPayInfo.szPymtMethod = C_PYMT_METHOD
                stPayInfo.iHttpTimeoutSeconds = Convert.ToInt32(ConfigurationManager.AppSettings("HttpTimeoutSeconds"))

                'Get Host information - HostID, HostTemplate, NeedReplyAcknowledgement, TxnStatusActionID, HostReplyMethod, OSPymtCode
                bGetHostInfo = objTxnProcOB.bGetHostInfo(stHostInfo, stPayInfo)
                If (bGetHostInfo) Then
                    'Load waiting page based on LanguageCode, that's why have to get GatewayTxnID from Host first,
                    'then, look for the original request's LanguageCode then only can load the respective waiting page
                    'in bVerifyResTxn() inside bResponseMain()

                    If True <> bResponseMain() Then
                        If (stPayInfo.iTxnStatus <> Common.TXN_STATUS.LATE_HOST_REPLY And stPayInfo.iTxnStatus <> Common.TXN_STATUS.INVALID_HOST_IP And stPayInfo.iTxnStatus <> Common.TXN_STATUS.INVALID_HOST_REPLY_GATEWAYTXNID) Then
                            'Return failed response to merchant
                            ResponseErrHandler()
                        End If
                    End If
                    'Good/Failed/Pending response already returned to merchant in bResponseMain()
                    'Error response also returned to merchant as above
                    'Now, only inserts the respective response with the final TxnStatus into database.
                    'Good/Failed/Error responses are considered as finalized and can be deleted from Request table and inserted into Response table.
                    'Pending txns that need retry need to stay in Request table for Retry Service to process accordingly.
                    If (True = bTimeOut) Then
                        'Added  on 13 Jan 2011, checking of stPayInfo.iQueryStatus, if -1, meaning already in Response table
                        If (stPayInfo.iTxnStatus <> Common.TXN_STATUS.LATE_HOST_REPLY And stPayInfo.iTxnStatus <> Common.TXN_STATUS.INVALID_HOST_IP And stPayInfo.iTxnStatus <> Common.TXN_STATUS.INVALID_HOST_REPLY_GATEWAYTXNID) Then
                            'Action 3 (Retry query host); Action 4 (Retry confirm booking)
                            'Txns that belong to these two types of action do not need to be inserted into Response table yet,
                            'because once the spInsTxnResp stored procedure is called, the original request will be deleted from
                            'Request table. These txns that need retry should stay in Request table for Retry Service to process.

                            If ((stPayInfo.iAction <> 3) And (stPayInfo.iAction <> 4)) Then
                                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                            stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > Inserting TxnResp. GatewayTxnID[" + stPayInfo.szTxnID + "] MerchantTxnStatus[" + stPayInfo.iMerchantTxnStatus.ToString() + "] TxnStatus[" + stPayInfo.iTxnStatus.ToString() + "] TxnState[" + stPayInfo.iTxnState.ToString() + "] RespMesg[" + stPayInfo.szTxnMsg + "].")

                                If (stPayInfo.szTxnID <> "" And stPayInfo.szTxnAmount <> "" And (stPayInfo.szHostTxnStatus <> "" Or stPayInfo.szRespCode <> "") And stPayInfo.szMerchantOrdID <> "") Then
                                    If True <> objTxnProcOB.bInsertTxnResp(stPayInfo, stHostInfo) Then
                                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                    stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > Error: " + stPayInfo.szErrDesc)
                                    Else
                                        'Added, 31 Mar 2014, update PG..TB_PayTxnRef
                                        If True <> objTxnProc.bUpdatePayTxnRef(stPayInfo, stHostInfo) Then
                                            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                        stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > Error: " + stPayInfo.szErrDesc)
                                        End If
                                    End If
                                Else
                                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                " > GatewayTxnID(" + stPayInfo.szTxnID + ")/TxnAmount(" + stPayInfo.szTxnAmount + ")/HostTxnStatus(" + stPayInfo.szHostTxnStatus + ")/RespCode(" + stPayInfo.szRespCode + ")/OrderNumber(" + stPayInfo.szMerchantOrdID + ") are empty from response. Bypass InsertTxnResp.")
                                End If
                            End If
                        End If
                    End If
                Else
                        'For this scenario, most probably Datebase error, the Retry Service will query the host for status
                        'once database is up.
                        stPayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_PENDING
                    stPayInfo.iTxnStatus = Common.TXN_STATUS.TXN_PENDING
                    stPayInfo.szTxnMsg = Common.C_INVALID_HOST_902
                    stPayInfo.szErrDesc = "> Failed to get Host Info for " + stPayInfo.szIssuingBank + " " + stPayInfo.szTxnType + " response"

                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    "Error: " + stPayInfo.szErrDesc)

                    'bResponseMain() is not run, therefore, unable to get other reply values.
                    'Merchant will only get TxnType, IssuingBank, TxnState, TxnStatus, RespMesg
                    'Without MerchantPymtID, the reply is meaningless for the merchant. Therefore, no need reply.
                    'ResponseErrHandler()
                End If
            End If
        Catch ex As Exception
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            " > " + C_ISSUING_BANK + " response Page_Load() Exception: " + ex.Message)
        End Try
    End Sub

    '********************************************************************************************
    ' Class Name:		Response_enets
    ' Function Name:	ResponseErrHandler
    ' Function Type:	NONE
    ' Parameter:        
    ' Description:		Error handler
    ' History:			18 Nov 2008
    '********************************************************************************************
    Private Sub ResponseErrHandler()
        Dim szHTTPString As String = ""
        Dim iRet As Integer = -1

        Try
            'Generates response hash value
            'stPayInfo.szHashValue = objHash.szGetSaleResHash(stPayInfo)
            stPayInfo.szHashValue2 = objHash.szGetSaleResHash2(stPayInfo)   'Added, 2 May 2016

            'Generates HTTP string to be replied to merchant
            szHTTPString = objResponse.szGetReplyMerchantHTTPString(stPayInfo, stHostInfo)

            'Send HTTP string reply to merchant using the same method used by Host
            'Modified 4 Oct 2016, added st_HostInfo, KTB redirect resp query
            If (stPayInfo.szMerchantReturnURL <> "") Then
                iRet = objResponse.iHTTPSend(stHostInfo.iHostReplyMethod, szHTTPString, stPayInfo.szMerchantReturnURL, stPayInfo, stHostInfo, g_szHTML)
                If (iRet <> 0) Then
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > Failed to send error reply iRet(" + CStr(iRet) + ") " + szHTTPString + " to merchant URL " + stPayInfo.szMerchantReturnURL)
                Else
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > Error reply to merchant sent.")
                End If
            Else
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > MerchantRURL empty. Bypass sending error reply " + szHTTPString + " to merchant")
            End If

            'Another alternative - NOTE: If MerchantRURL contains "?" and param, can not use this alternative
            'coz it does not support additional params in the error template.
            'objResponse.iLoadErrUI(stPayInfo.szMerchantReturnURL, stPayInfo, g_szHTML)
        Catch ex As Exception
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            " > ResponseErrHandler() Exception: " + ex.Message)
        End Try
    End Sub

    '********************************************************************************************
    ' Class Name:		Response_enets
    ' Function Name:	bResponseMain
    ' Function Type:	Boolean.  Return False if error.
    ' Parameter:		NONE
    ' Description:		A main function to get the response info and handle the process
    ' History:			28 Oct 2008
    '********************************************************************************************
    Private Function bResponseMain() As Boolean
        Dim bReturn = True
        Dim iDBTimeOut As Integer = 0
        Dim bGetMerchantInfo As Boolean = False

        Try
            'Log one empty line for easier reference for new response transaction
            'objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
            '                DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "")

            'Log all form's response fields
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            C_ISSUING_BANK + " Redirect Resp: " + szGetAllHTTPVal())
            'Get Response data
            If True <> objResponse.bGetData(stPayInfo, stHostInfo) Then
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                "> Error1_1 GetData: " + stPayInfo.szErrDesc)
                ' stPayInfo.szTxnMsg specified in bGetData
                stPayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                stPayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                stPayInfo.iTxnState = Common.TXN_STATE.ABORT

                'NOTE: May not be able to be inserted into Response table if mandatory fields are missing due to failure in bGetData()
                Return False
            End If

            'Ensure GatewayTxnID is passed back by Host before proceed. GatewayTxnID is important for reconciliation.
            If (stPayInfo.szTxnID = "") Then
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                "> Error1_2 bResponseMain(): Missing GatewayTxnID from Host's reply.")

                stPayInfo.szTxnMsg = Common.C_INVALID_HOST_REPLY_2907
                stPayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                stPayInfo.iTxnStatus = Common.TXN_STATUS.INVALID_HOST_REPLY
                stPayInfo.iTxnState = Common.TXN_STATE.ABORT

                'NOTE: May not be able to be inserted into Response table since GatewayTxnID is missing.
                Return False
            End If

            objTxnProcOB.bGetReqTxn(stPayInfo, stHostInfo)

            Dim iStartTickCount As Integer
            Dim iElapsedTime As Integer

            iElapsedTime = 0
            iStartTickCount = System.Environment.TickCount()
            iDBTimeOut = Convert.ToInt32(ConfigurationManager.AppSettings("DBQueryTimeoutSeconds"))

            While (iElapsedTime < iDBTimeOut * 1000)
                'bRet = objTxnProcOB.bGetResTxn(stPayInfo, stHostInfo)
                objTxnProcOB.bGetResTxn(stPayInfo, stHostInfo)
                'If (True = bRet) Then
                If (-1 = stPayInfo.iQueryStatus) Then ' -1 indicate that record found in Res table
                    Exit While
                Else
                    Thread.Sleep(3000) 'sleep for 3 seconds
                End If

                iElapsedTime = System.Environment.TickCount() - iStartTickCount
            End While

            'If (False = bRet) Then 'To update iAction in Req Table
            If (-1 <> stPayInfo.iQueryStatus) Then
                bTimeOut = True
            End If

            If (True = bTimeOut) Then
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "Get Response TimeOut = " + bTimeOut.ToString() + " for GatewayTxnID (" + stPayInfo.szTxnID + "),process Redirect resp")

                If True <> objResponse.bProcessSaleRes(stPayInfo, stHostInfo, szReplyDateTime, g_szHTML) Then
                    Return False
                End If
            Else
                bGetMerchantInfo = objTxnProc.bGetMerchant(stPayInfo)
                If True <> bProcessSaleRes(stPayInfo, stHostInfo, szReplyDateTime, g_szHTML) Then
                    Return False
                End If
            End If

        Catch ex As Exception
            bReturn = False

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bResponseMain() Exception: " + ex.Message)
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		Response_enets
    ' Function Name:	GetAllHTTPVal
    ' Function Type:	Boolean.  Return False if error.
    ' Parameter:		NONE
    ' Description:		Get all form's or query string's values
    ' History:			28 Oct 2008
    '********************************************************************************************
    Public Function szGetAllHTTPVal() As String
        Dim szFormValues As String
        Dim szTempKey As String
        Dim szTempVal As String

        Dim iCount As Integer = 0
        Dim iLoop As Integer = 0

        szFormValues = "[UserIP: " + Request.UserHostAddress + "] [UserHostName: " + Request.UserHostName + "] [UserBrowser:" + Request.UserAgent + "] [SessionID:" + HttpContext.Current.Session.SessionID.ToString() + "] "

        iCount = Request.Form.Count

        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                        " > No. of Form Redirect Response Fields (" + iCount.ToString() + ")")

        If (iCount > 0) Then
            For iLoop = 1 To iCount
                szTempKey = ""
                szTempKey = Request.Form.GetKey(iLoop - 1)

                szFormValues = szFormValues + szTempKey + "="

                szTempVal = ""
                szTempVal = Server.UrlDecode(Request.Form.Get(iLoop - 1))

                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                " > Field(" + szTempKey + ") Value(" + szTempVal + ")")

                'Mask CardPAN
                If ((szTempKey <> "")) Then
                    If (szTempKey.ToLower = "cardpan") Then
                        If szTempVal.Length > 10 Then
                            szFormValues = szFormValues + szTempVal.Substring(0, 6) + "".PadRight(szTempVal.Length - 6 - 4, "X") + szTempVal.Substring(szTempVal.Length - 4, 4) + "&"
                        Else
                            szFormValues = szFormValues + "".PadRight(szTempVal.Length, "X") + "&"
                        End If
                    Else
                        If (iLoop = iCount) Then
                            szFormValues = szFormValues + szTempVal
                        Else
                            szFormValues = szFormValues + szTempVal + "&"
                        End If
                    End If
                End If
            Next iLoop
        Else
            iCount = Request.QueryString.Count
            iCount = Request.Form.Count

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            " > No. of QueryString Response Fields (" + iCount.ToString() + ")")

            For iLoop = 1 To iCount
                szTempKey = ""
                szTempKey = Request.QueryString.GetKey(iLoop - 1)

                szFormValues = szFormValues + szTempKey + "="

                szTempVal = ""
                szTempVal = Server.UrlDecode(Request.QueryString.Get(iLoop - 1))

                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                " > Field(" + szTempKey + ") Value(" + szTempVal + ")")

                'Mask CardPAN
                If ((szTempKey <> "")) Then
                    If (szTempKey.ToLower = "cardpan") Then
                        If szTempVal.Length > 10 Then
                            szFormValues = szFormValues + szTempVal.Substring(0, 6) + "".PadRight(szTempVal.Length - 6 - 4, "X") + szTempVal.Substring(szTempVal.Length - 4, 4) + "&"
                        Else
                            szFormValues = szFormValues + "".PadRight(szTempVal.Length, "X") + "&"
                        End If
                    Else
                        If (iLoop = iCount) Then
                            szFormValues = szFormValues + szTempVal
                        Else
                            szFormValues = szFormValues + szTempVal + "&"
                        End If
                    End If
                End If
            Next iLoop
        End If

        Return szFormValues
    End Function


    '********************************************************************************************
    ' Class Name:		Response
    ' Function Name:	bProcessSaleRes
    ' Function Type:	Boolean.  Return False if error.
    ' Parameter:		
    ' Description:		Process payment response
    ' History:			29 Oct 2008
    '********************************************************************************************
    Public Function bProcessSaleRes(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost, ByVal sz_ReplyDateTime As String, ByRef sz_HTML As String) As Boolean
        Dim bReturn As Boolean = True
        Dim szQueryString As String = ""
        Dim iMesgSet As Integer = -1
        Dim iMesgNum As Integer = -1
        Dim szMesg As String = ""           ' Added on 18 Apr 2010
        Dim iRet As Integer = -1

        Dim objHash As Hash
        Dim szBody As String = ""
        Dim szTxnMsg As String = ""         ' Added on 20 Apr 2017, Buyer Bank Name

        szTxnMsg = st_PayInfo.szTxnMsg      ' Added on 20 Apr 2017, Buyer Bank Name

        objHash = New Hash(objLoggerII, objCommon)      'Modified 15 Apr 2014, added objCommon
        st_PayInfo.szHashValue = ""

        Try
            '- Verify Host return IP addresses (optional)
            '- Check late response - exceeding 2 hours from request datetime since booking PO will be released after 2 hours
            '- Update Txn State
            '- Verify response data with request data
            '- Reply Acknowledgement to Host if needed
            '- Get merchant password
            '- Get HostTxnStatus's action
            'If True <> bInitResponse(st_PayInfo, st_HostInfo, sz_ReplyDateTime, sz_HTML) Then
            '    Return False
            'End If

            'Process Host reply based on action type configured in database according to Host TxnStatus
            If (Common.TXN_STATUS.TXN_SUCCESS = st_PayInfo.iTxnStatus) Then                'Payment approved by Host
                'Else    'Success (Payment approved, no need to confirm booking for this merchant)
                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_SUCCESS
                st_PayInfo.szTxnMsg = Common.C_TXN_SUCCESS_0
                st_PayInfo.iTxnState = Common.TXN_STATE.COMMIT
                'End If

            ElseIf (Common.TXN_STATUS.TXN_FAILED = st_PayInfo.iTxnStatus) Then    'Failed by Host
                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.szTxnMsg = Common.C_TXN_FAILED_1
                st_PayInfo.iTxnState = Common.TXN_STATE.COMMIT_FAILED

            ElseIf (Common.TXN_STATUS.TXN_PENDING = st_PayInfo.iTxnStatus) Then    'Unconfirmed status from Host, need to query Host until getting reply or timeout
                'Update Request table with Action 3 for Retry Service to pick up and query the respective Host
                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_PENDING
                st_PayInfo.szTxnMsg = "Host returned not processed/unknown status"
                st_PayInfo.iTxnState = Common.TXN_STATE.RECV_FROM_HOST
            End If

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "Get Response TimeOut = " + bTimeOut.ToString() + " for GatewayTxnID (" + stPayInfo.szTxnID + ")")

            If (True = bTimeOut) Then
                If (1 = st_HostInfo.iAllowReversal) Then
                    st_PayInfo.iAction = 7
                    st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.TIME_OUT_FAILED
                    st_PayInfo.szTxnMsg = "Timeout Host"
                    st_PayInfo.iTxnState = Common.TXN_STATE.ROLLBACK_PENDING
                Else
                    st_PayInfo.iAction = 3
                    st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_PENDING
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_PENDING
                    st_PayInfo.szTxnMsg = "Pending Host"
                    st_PayInfo.iTxnState = Common.TXN_STATE.SENT_QUERY_HOST
                End If

                'Update Request table with the respective Txn State and Action so that Retry Service can retry confirm booking
                'At the same time, also update Request table with BankRefNo (st_PayInfo.szHostTxnID), OSPymtCode (st_HostInfo.szOSPymtCode)
                If True <> objTxnProcOB.bUpdateTxnStatus(st_PayInfo, st_HostInfo) Then
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " Update req table failed [" + st_PayInfo.szErrDesc + "], could be removed by server-to-server resp, getting from resp table..")
                    'if update TxnStatus failed, try to check for Response table again. Because Req had been just move to Res by server 2 server. - CCGW
                    Dim bRet As Boolean = False
                    bRet = objTxnProcOB.bGetResTxn(stPayInfo, stHostInfo)
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " Got response [" + bRet.ToString() + "]")

                    If (True = bRet) Then
                        If (Common.TXN_STATUS.TXN_SUCCESS = stPayInfo.iTxnStatus) Then                'Payment approved by Host
                            'Else    'Success (Payment approved, no need to confirm booking for this merchant)
                            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_SUCCESS
                            st_PayInfo.szTxnMsg = Common.C_TXN_SUCCESS_0
                            st_PayInfo.iTxnState = Common.TXN_STATE.COMMIT
                            'End If

                        ElseIf (Common.TXN_STATUS.TXN_FAILED = stPayInfo.iTxnStatus) Then    'Failed by Host
                            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                            st_PayInfo.szTxnMsg = Common.C_TXN_FAILED_1
                            st_PayInfo.iTxnState = Common.TXN_STATE.COMMIT_FAILED

                        ElseIf (Common.TXN_STATUS.TXN_PENDING = stPayInfo.iTxnStatus) Then    'Unconfirmed status from Host, need to query Host until getting reply or timeout
                            'Update Request table with Action 3 for Retry Service to pick up and query the respective Host
                            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_PENDING
                            st_PayInfo.szTxnMsg = "Host returned not processed/unknown status"
                            st_PayInfo.iTxnState = Common.TXN_STATE.RECV_FROM_HOST
                        End If
                    End If
                End If
            End If

            If (szTxnMsg <> "") Then                'Added, 20 Apr 2017, Buyer Bank Name
                st_PayInfo.szTxnMsg = szTxnMsg
            End If

            ''Update Request table with the respective Txn State and Action so that Retry Service can retry confirm booking
            ''At the same time, also update Request table with BankRefNo (st_PayInfo.szHostTxnID), OSPymtCode (st_HostInfo.szOSPymtCode)
            'If True <> objTxnProcOB.bUpdateTxnStatus(st_PayInfo, st_HostInfo) Then
            '    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
            '                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
            '                    st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " " + st_PayInfo.szErrDesc)
            'End If

            'Send Gateway response to merchant using the same Host's reply method
            objResponse.iSendReply2Merchant(st_PayInfo, st_HostInfo, sz_HTML)
            'Even If (True = bRet), no need update DB for TxnState SENT_REPLY_CLIENT so that TxnState like COMMIT/COMMIT_FAILED won't be overwritten.

        Catch ex As Exception
            bReturn = False

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Exception: " + ex.Message)

            'Added on 12 Mar 2011, to solve "The operation has timed out" exception after calling NewSkies Web Service ConfirmBooking()
            'Without adding this, even though payment was approved but when encountered operation timeout during booking confirmation,
            'DDGW will return TxnStatus -1 and TxnState 2 to merchant that requires DDGW to confirm booking
            If (1 = st_PayInfo.iAction And 1 = st_PayInfo.iNeedAddOSPymt) Then  'Approved by bank and need booking confirmation
                bReturn = True

                If ("THE OPERATION HAS TIMED OUT" = ex.Message.ToUpper.Trim()) Then
                    st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_PENDING_CONFIRM_BOOKING   'Merchant will extend booking based on this status
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_PENDING_CONFIRM_BOOKING
                    st_PayInfo.szTxnMsg = "Pending: Payment approved but booking status is unknown due to communication error with booking system, retry in progress"
                    st_PayInfo.iTxnState = Common.TXN_STATE.SENT_CONFIRM_OS

                    st_PayInfo.iAction = 4  'Retry ConfirmBooking
                Else
                    st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_PENDING
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_PENDING
                    st_PayInfo.szTxnMsg = "Pending: Status unknown, query Host in progress"
                    st_PayInfo.iTxnState = Common.TXN_STATE.RECV_FROM_HOST

                    st_PayInfo.iAction = 3  'Retry query bank
                End If

                'Update Request table with the respective Txn State and Action so that Retry Service can retry confirm booking
                'At the same time, also update Request table with BankRefNo (st_PayInfo.szHostTxnID), OSPymtCode (st_HostInfo.szOSPymtCode)
                If True <> objTxnProcOB.bUpdateTxnStatus(st_PayInfo, st_HostInfo) Then
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " " + st_PayInfo.szErrDesc)
                End If

                st_HostInfo.iHostReplyMethod = 1
                'Send Gateway response to merchant using the same Host's reply method
                objResponse.iSendReply2Merchant(st_PayInfo, st_HostInfo, sz_HTML)
                'Even If (True = bRet), no need update DB for TxnState SENT_REPLY_CLIENT so that TxnState like COMMIT/COMMIT_FAILED won't be overwritten.
            End If
        End Try

        Return bReturn
    End Function

    Public Sub New()
        szLogPath = ConfigurationManager.AppSettings("LogPath")
        iLogLevel = Convert.ToInt32(ConfigurationManager.AppSettings("LogLevel"))

        '2 Apr 2010, moved InitLogger() from down to up here, or else Hash will get empty objLoggerII and encountered exception
        InitLogger()

        objTxnProc = New TxnProc(objLoggerII)
        objCommon = New Common(objTxnProc, objLoggerII)
        objHash = New Hash(objLoggerII)
        objTxnProcOB = New TxnProcOB(objLoggerII)
        objResponse = New Response(objLoggerII)
    End Sub

    Public Sub InitLogger()
        If (objLoggerII Is Nothing) Then

            If (szLogPath Is Nothing) Then
                szLogPath = "C:\\OB.log"
            End If

            If (iLogLevel < 0 Or iLogLevel > 5) Then
                iLogLevel = 3
            End If

            'objLoggerII = New LoggerII.CLoggerII(
            objLoggerII = LoggerII.CLoggerII.GetInstanceX(szLogPath + "_" + C_ISSUING_BANK + ".log", iLogLevel, 255)

        End If
    End Sub

    Protected Overrides Sub Finalize()
        If Not objLoggerII Is Nothing Then
            objLoggerII.Dispose()
            objLoggerII = Nothing
        End If

        If Not objCommon Is Nothing Then objCommon = Nothing
        If Not objTxnProcOB Is Nothing Then objTxnProcOB = Nothing
        If Not objTxnProc Is Nothing Then objTxnProc = Nothing
        If Not objHash Is Nothing Then objHash = Nothing

        If Not objResponse Is Nothing Then
            objResponse.Dispose()
            objResponse = Nothing
        End If

        MyBase.Finalize()
    End Sub
End Class
