﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CheckStatus.aspx.vb" Inherits="Paydibs.Checkout.CheckStatus" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">    
    <title>Check Status</title>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">

   <p style="text-align: center"> If you don't receive any status reply after successful payment, please click the button below.</p>


        <p>
            &nbsp;</p>
        <p style="text-align: center">
            <asp:Label ID="lblResult" runat="server"></asp:Label>
        </p>
        <p style="text-align: center">
            <asp:Button ID="CheckStatusButton" runat="server" style="text-align: center" class="btn btn-primary" Text="Check Status Manually" Visible="False" OnClick="CheckStatusButton_Click" />
        </p>


    </form>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>

</body>
</html>
<%= g_szHTML%>
