﻿Imports System
Imports System.IO
Imports System.Data
Imports System.Web
Imports LoggerII
Imports System.Security.Cryptography
Imports Com.MasterCard.Masterpass.Merchant
Imports Com.MasterCard.Sdk
Imports System.Net
Imports System.Data.SqlClient
Imports System.Security

Public Class MasterPass
    Implements IDisposable

    Private objLoggerII As LoggerII.CLoggerII
    Private objHash As Hash
    Private szDBConnStr As String = ""
    Private objCommon As Common
    Private objTxnProc As TxnProc
    Private objTxnProcOB As TxnProcOB
    Private Server As System.Web.HttpServerUtility = System.Web.HttpContext.Current.Server 'Server.UrlEncode, Server.MapPath; If HttpUtility just Server.UrlEncode

    Public Function MPStandardCheckout(ByVal st_HostInfo As Common.STHost, ByVal st_PayInfo As Common.STPayInfo, ByVal sz_ReqStr As String) As String
        Dim szMPString As String = ""
        Dim objStreamReader As StreamReader = Nothing
        Dim szLine As String = ""
        Dim aszConfig() As String = Nothing
        Dim szEncryptReqStr As String = ""

        Try
            st_PayInfo.szIssuingBank = "MasterPass"

            If True <> objCommon.bGetHostInfo(st_HostInfo, st_PayInfo) Then
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
                                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + "> Error Get Host: " + st_PayInfo.szErrDesc)
            End If

            If True <> objTxnProc.bGetTerminalEx2(st_HostInfo, st_PayInfo) Then 'If True <> objCommon.bGetTerminal(st_HostInfo, st_PayInfo) Then
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
                                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + "> Error Get Terminal: HostID(" + st_HostInfo.iHostID.ToString() + ") with CurrencyCode(" + st_PayInfo.szCurrencyCode + ") Not Support") '+ st_PayInfo.szErrDesc)

                Return szMPString       'Added  2 Jun 2017.
            End If

            If (True = bSetConfigurations(st_HostInfo, st_PayInfo)) Then
                st_PayInfo.szParam1 = szRequestToken(st_HostInfo, st_PayInfo)
                'st_PayInfo.szReqToken = szRequestToken(st_HostInfo, st_PayInfo)
            Else
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
                        st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + "> Failed set configuration - " + st_PayInfo.szIssuingBank)

                Return szMPString       'Added  2 Jun 2017.
            End If

            If ("" <> st_PayInfo.szParam1) Then
                'If ("" <> st_PayInfo.szReqToken) Then
                bShoppingCartRequest(st_PayInfo.szParam1, st_PayInfo)
                bMerchantInit(st_PayInfo.szParam1, st_PayInfo) 'st_HostInfo)
            Else
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
                        st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + "> Failed get token - " + st_PayInfo.szIssuingBank)

                Return szMPString       'Added  2 Jun 2017.
            End If

            szEncryptReqStr = objHash.szComputeSHA512Hash(Common.C_3DESAPPKEY + Server.UrlDecode(sz_ReqStr).Replace("+", " "))

            If (st_HostInfo.szHostTemplate <> "") Then
                objStreamReader = System.IO.File.OpenText(Server.MapPath(st_HostInfo.szHostTemplate))
                Do
                    szLine = objStreamReader.ReadLine()

                    If ("" = szLine) Then
                        Exit Do
                    End If

                    aszConfig = Split(szLine, "|")
                    If ((aszConfig(0).Trim().ToUpper() = st_PayInfo.szTxnType.ToUpper()) And (aszConfig(1).Trim().ToUpper() = "REQ") And (aszConfig(2).Trim().ToUpper() = "SC")) Then
                        Exit Do
                    End If
                Loop Until (szLine Is Nothing)

                objStreamReader.Close()
            End If

            szMPString = aszConfig(3).Trim()

            For iLoop = 4 To aszConfig.GetUpperBound(0)
                'String.Replace is case sensitive. Need to make sure the host template using lower case
                Select Case aszConfig(iLoop).Trim().ToLower()
                    Case "[param1]"
                        szMPString = szMPString.Replace(aszConfig(iLoop).Trim(), st_PayInfo.szParam1)
                    Case "[gatewayreturnurl]"
                        szMPString = szMPString.Replace(aszConfig(iLoop).Trim(), st_HostInfo.szReturnURL + "?" + sz_ReqStr + "&MPID=" + szEncryptReqStr)
                    Case "[payeecode]"
                        szMPString = szMPString.Replace(aszConfig(iLoop).Trim(), st_HostInfo.szPayeeCode)
                End Select
            Next

        Catch ex As Exception
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), _
               DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "MPStandardCheckout() exception: " + ex.ToString)
        End Try

        If Not objStreamReader Is Nothing Then objStreamReader = Nothing

        Return szMPString

    End Function

    Public Function MPPairingCheckout(ByVal st_HostInfo As Common.STHost, ByRef st_PayInfo As Common.STPayInfo, ByVal sz_ReqStr As String) As Boolean
        'Dim szMPString As String = ""
        Dim bRet As Boolean = False
        Dim objStreamReader As StreamReader = Nothing
        Dim szLine As String = ""
        Dim aszConfig() As String = Nothing
        'Dim szEncryptReqStr As String = ""
        ''NOTE: Is Pairing with checkout (PWC) or Pairing no checkout (PNC)
        Dim bCheckout As Boolean = True     'Currently only support Pair with checkout
        ''''''''
        Try
            st_PayInfo.szIssuingBank = "MasterPass"

            If True <> objCommon.bGetHostInfo(st_HostInfo, st_PayInfo) Then
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
                                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + "> Error Get Host: " + st_PayInfo.szErrDesc)
            End If

            If True <> objTxnProc.bGetTerminalEx2(st_HostInfo, st_PayInfo) Then 'If True <> objCommon.bGetTerminal(st_HostInfo, st_PayInfo) Then
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
                                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + "> Error Get Terminal: " + st_PayInfo.szErrDesc)
            End If

            If (False = bSetConfigurations(st_HostInfo, st_PayInfo)) Then
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
                    st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + "> Failed set configuration - " + st_PayInfo.szIssuingBank)

                Return bRet 'szMPString
            End If

            'Modified  31 Mar 2017. Masterpass MSC
            If ("MSC" <> st_PayInfo.szTokenType) Then
                '1st call-Pairing Request Token 
                st_PayInfo.szPairingReqToken = szRequestToken(st_HostInfo, st_PayInfo)
            End If

            'Call when Pairing with Checkout
            If (True = bCheckout) Then
                '2nd call-Request Token (szparam1 = szoAuthToken)
                st_PayInfo.szReqToken = szRequestToken(st_HostInfo, st_PayInfo)

                If ("" <> st_PayInfo.szReqToken) Then
                    bShoppingCartRequest(st_PayInfo.szReqToken, st_PayInfo)
                    bMerchantInit(st_PayInfo.szReqToken, st_PayInfo)
                Else
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
                            st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + "> Failed get Request Token - " + st_PayInfo.szIssuingBank)
                End If
            Else    'Pairing no checkout
                If ("" <> st_PayInfo.szPairingReqToken) Then
                    bMerchantInit(st_PayInfo.szPairingReqToken, st_PayInfo)
                Else
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
                            st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + "> Failed get Pairing Request Token - " + st_PayInfo.szIssuingBank)
                End If
            End If

            ''Pairing will generate new LAT, remove previous invalid LAT before update ReqToken and PairingReqToken
            'If ("" <> st_PayInfo.szLongAccessToken) Then
            '    st_PayInfo.szLongAccessToken = ""
            'End If

            'If (True = objTxnProc.bInsertUpdateMPToken(st_PayInfo)) Then
            '    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
            '        DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "> MasterPass Token updated.")
            'End If

            bRet = True

        Catch ex As Exception
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), _
               DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "MPPairingCheckout() exception: " + ex.ToString)

            Return bRet
        End Try

        If Not objStreamReader Is Nothing Then objStreamReader = Nothing

        Return bRet 'szMPString

    End Function

    Public Function MPPreCheckout(ByVal st_HostInfo As Common.STHost, ByRef st_PayInfo As Common.STPayInfo, ByVal sz_ReqStr As String) As Boolean
        'Dim szMPString As String = ""
        Dim bReturn As Boolean = False
        Dim objStreamReader As StreamReader = Nothing
        Dim szLine As String = ""
        Dim aszConfig() As String = Nothing
        Dim szEncryptReqStr As String = ""

        Try
            st_PayInfo.szIssuingBank = "MasterPass"

            If True <> objCommon.bGetHostInfo(st_HostInfo, st_PayInfo) Then
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
                                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + "> Error Get Host: " + st_PayInfo.szErrDesc)
            End If

            If True <> objTxnProc.bGetTerminalEx2(st_HostInfo, st_PayInfo) Then 'If True <> objCommon.bGetTerminal(st_HostInfo, st_PayInfo) Then
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
                                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + "> Error Get Terminal: " + st_PayInfo.szErrDesc)
            End If

            If (False = bSetConfigurations(st_HostInfo, st_PayInfo)) Then
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
                    st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + "> Failed set configuration - " + st_PayInfo.szIssuingBank)

                Return bReturn
            End If

            st_PayInfo.szIssuingBank = ""   'To avoid CustomizePaymentPage bGetHostCountry error

            'Call PrecheckoutData Service, IF PreCheckout return True continue to ExpressCheckoutSvc ELSE should back to call Pairing With Checkout
            If (False = bPreCheckoutRequest(st_PayInfo)) Then
                Return bReturn
            End If

            bReturn = True
        Catch ex As Exception
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), _
               DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "MPPreCheckout() exception: " + ex.ToString)
        End Try

        If Not objStreamReader Is Nothing Then objStreamReader = Nothing

        Return bReturn

    End Function

    '********************************************************************************************
    ' Class Name:		Masterpass
    ' Function Name:	SetConfigurations
    ' Function Type:	NONE
    ' Parameter:        
    ' Description:		Load MasterPass configuration setting
    ' History:			9 Sept 2016
    '********************************************************************************************
    Public Function bSetConfigurations(ByVal st_HostInfo As Common.STHost, ByVal st_PayInfo As Common.STPayInfo) As Boolean
        Dim bReturn As Boolean = False
        Dim szHostUrl As String = ""
        Dim szConsumerKey As String = ""
        Dim szKeystorePath As String = ""
        'Dim szKeystorePassword As String = ""          'Modified Joyce 05 Apr 2019, chg string to securestring as PCI request
        Dim szKeystorePassword As SecureString = New SecureString()
        Dim bIsSandbox As Boolean = False

        Try
            'Need to load consumerkey & password from terminal table
            szHostUrl = st_HostInfo.szURL '"https://sandbox.api.mastercard.com" 'ConfigurationManager.AppSettings("ApiHostUrl")
            szConsumerKey = st_HostInfo.szAcquirerID ' "hf0th_Ycgv7Be3yZFHy_vTYtzE8DTdrEQv3QezMRb7f2fe4f!0c382e2b3de9499593f88dfb9e6df98f0000000000000000" 'Retrieve from db. stHostInfo/stPayInfo
            szKeystorePath = st_PayInfo.szEncryptKeyPath '"Y:\IPG_SG_OoiMei\MasterPass\SBX_GHLTestKey_sandbox.p12" 'Server.MapPath(ConfigurationManager.AppSettings("KeystorePath"))

            'Modified Joyce 05 Apr 2019, as szKeystorePassword change datatype to secure string
            'szKeystorePassword = objHash.szDecrypt3DES(st_HostInfo.szSendKey, Common.C_3DESAPPKEY, Common.C_3DESAPPVECTOR) '"pASSW0RD1" 'Retrieve from db. stHostInfo/stPayInfo
            szKeystorePassword = New NetworkCredential("", objHash.szDecrypt3DES(st_HostInfo.szSendKey, Common.C_3DESAPPKEY, Common.C_3DESAPPVECTOR)).SecurePassword
            bIsSandbox = ConfigurationManager.AppSettings("IsSandbox")
            'Added by OM, 6 Jul 2017, X509Certificates.X509KeyStorageFlags.MachineKeySet Or X509Certificates.X509KeyStorageFlags.Exportable Or X509Certificates.X509KeyStorageFlags.PersistKeySet
            'Dim privateKey = New X509Certificates.X509Certificate2(szKeystorePath, szKeystorePassword, X509Certificates.X509KeyStorageFlags.MachineKeySet Or X509Certificates.X509KeyStorageFlags.Exportable Or X509Certificates.X509KeyStorageFlags.PersistKeySet).PrivateKey

            'Modified Joyce 05 Apr 2019, as as szKeystorePassword change datatype to secure string
            'Dim privateKey = New X509Certificates.X509Certificate2(szKeystorePath, szKeystorePassword, X509Certificates.X509KeyStorageFlags.MachineKeySet).PrivateKey
            Dim privateKey = New X509Certificates.X509Certificate2(szKeystorePath, New NetworkCredential("", szKeystorePassword).Password, X509Certificates.X509KeyStorageFlags.MachineKeySet).PrivateKey

            'ServicePointManager.CertificatePolicy = New SecPolicy
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
            'ServicePointManager.ServerCertificateValidationCallback = Function() True
            Com.MasterCard.Sdk.Core.MasterCardApiConfiguration.Sandbox = bIsSandbox
            Com.MasterCard.Sdk.Core.MasterCardApiConfiguration.ConsumerKey = szConsumerKey
            Com.MasterCard.Sdk.Core.MasterCardApiConfiguration.PrivateKey = privateKey

            bReturn = True
        Catch ex As Exception
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), _
               DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "bSetConfigurations() exception: " + ex.ToString)
        End Try

        Return bReturn

    End Function

    '********************************************************************************************
    ' Class Name:		Masterpass
    ' Function Name:	RequestToken
    ' Function Type:	NONE
    ' Parameter:        
    ' Description:		Request Token from MasterPass
    ' History:			14 Sept 2016
    '********************************************************************************************
    Public Function szRequestToken(ByVal st_HostInfo As Common.STHost, ByVal st_PayInfo As Common.STPayInfo) As String
        Dim szResponseURL As String = ""
        Dim objReqTokenRes As Com.MasterCard.Sdk.Core.Model.RequestTokenResponse = New Com.MasterCard.Sdk.Core.Model.RequestTokenResponse
        Dim szToken As String = ""
        Dim szPairingToken As String = ""

        Try
            'ServicePointManager.CertificatePolicy = New SecPolicy
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
            'ServicePointManager.ServerCertificateValidationCallback = Function() True

            szResponseURL = st_HostInfo.szReturnURL  ''"https://test2pay.ghl.com/IPGSGOM/response_masterpass.aspx"
            objReqTokenRes = Com.MasterCard.Sdk.Core.Api.RequestTokenApi.Create(szResponseURL)
            szToken = objReqTokenRes.OauthToken

        Catch ex As Exception
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), _
               DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "szRequestToken() exception: " + ex.ToString)
        End Try

        Return szToken

    End Function

    '********************************************************************************************
    ' Class Name:		Masterpass
    ' Function Name:	ShoppingCartRequest
    ' Function Type:	NONE
    ' Parameter:        
    ' Description:		Create Shopping Cart Request - Pass in ProdDesc/OrderNumber
    ' History:			14 Sept 2016
    '********************************************************************************************

    Public Function bShoppingCartRequest(ByVal sz_ReqToken As String, ByVal st_PayInfo As Common.STPayInfo) As Boolean
        Dim bReturn As Boolean = False
        Dim objShoppingCartReq As Model.ShoppingCartRequest = New Model.ShoppingCartRequest
        Dim objShoppingCart As Model.ShoppingCart = New Model.ShoppingCart
        Dim objShoppingCartItem As Model.ShoppingCartItem = New Model.ShoppingCartItem
        Dim objShoppingCartRes As Model.ShoppingCartResponse = New Model.ShoppingCartResponse

        Try
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12

            With objShoppingCartItem
                'objShoppingCartItem.WithImageURL("")
                .WithValue(st_PayInfo.szTxnAmount.Replace(".", ""))  'TxnAmount as long value - 10100
                .Description = st_PayInfo.szMerchantOrdDesc 'ProdDesc/OrderNumber
                .WithQuantity(1) 'default to 1 as payment gateway not handle itemize
            End With

            With objShoppingCart
                .WithSubtotal(st_PayInfo.szTxnAmount.Replace(".", "")) 'TxnAmount as long value
                .WithCurrencyCode(st_PayInfo.szCurrencyCode) 'CurrencyCode
                .WithShoppingCartItem(objShoppingCartItem)
            End With

            With objShoppingCartReq
                .WithShoppingCart(objShoppingCart)
                .WithOAuthToken(sz_ReqToken)
            End With

            objShoppingCartRes = Api.ShoppingCartApi.Create(objShoppingCartReq)

            bReturn = True
        Catch ex As Exception
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), _
               DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "bShoppingCartRequest() exception: " + ex.ToString)
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		Masterpass
    ' Function Name:	MerchantInit
    ' Function Type:	NONE
    ' Parameter:        
    ' Description:		Create an instance of MerchantInitializationRequest
    ' History:			14 Sept 2016
    '********************************************************************************************
    Public Function bMerchantInit(ByVal sz_ReqToken As String, ByRef st_PayInfo As Common.STPayInfo) As Boolean
        Dim bReturn As Boolean = False
        Dim objMerchantInitReq As Model.MerchantInitializationRequest = New Model.MerchantInitializationRequest
        Dim objMerchantInitRes As Model.MerchantInitializationResponse = New Model.MerchantInitializationResponse

        Try
            'If ("MPE" = st_PayInfo.szTokenType.ToUpper()) Then
            'objMerchantInitReq.OriginUrl = st_PayInfo.szMerchantValidDomain
            'Else
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
            objMerchantInitReq.OriginUrl = ConfigurationManager.AppSettings("GWPaymentURL")
            'End If
            objMerchantInitReq.OAuthToken = sz_ReqToken

            objMerchantInitRes = Api.MerchantInitializationApi.Create(objMerchantInitReq)

            'st_PayInfo.szReqToken = objMerchantInitRes.OAuthToken 'Keep in Token_MPE table

            bReturn = True
        Catch ex As Exception
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), _
               DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "bMerchantInit() exception: " + ex.ToString)
        End Try

    End Function

    '********************************************************************************************
    ' Class Name:		Masterpass
    ' Function Name:	bPreCheckoutRequest
    ' Function Type:	NONE
    ' Parameter:        
    ' Description:		Create PreCheckout Request
    ' History:			11 Nov 2016
    '********************************************************************************************
    Public Function bPreCheckoutRequest(ByRef st_PayInfo As Common.STPayInfo) As Boolean
        Dim bReturn As Boolean = False
        Dim szCardMM As String = ""
        Dim js As New System.Web.Script.Serialization.JavaScriptSerializer
        Dim dict As New Dictionary(Of String, Object)

        Try
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12

            Dim objPreCheckReq As Model.PrecheckoutDataRequest = New Model.PrecheckoutDataRequest() _
                .WithPairingDataTypes(New Model.PairingDataTypes() _
                                    .WithPairingDataType(New Model.PairingDataType().WithType("CARD")) _
                                    .WithPairingDataType(New Model.PairingDataType().WithType("ADDRESS")) _
                                    .WithPairingDataType(New Model.PairingDataType().WithType("PROFILE")))
            ''.WithPairingDataType(New Model.PairingDataType().WithType("REWARD_PROGRAM")) _
            Dim objPreCheckRes As Model.PrecheckoutDataResponse = Api.PrecheckoutDataApi.Create(st_PayInfo.szLongAccessToken, objPreCheckReq)

            st_PayInfo.szLongAccessToken = objPreCheckRes.LongAccessToken

            If (True = objTxnProc.bInsertUpdateMPToken(st_PayInfo)) Then
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "PreCheckout Long Access Token updated.")
            End If

            st_PayInfo.szPreCheckoutTxnID = objPreCheckRes.PrecheckoutData.PrecheckoutTransactionId

            'Modified  24 Feb 2017. Masterpass-2 - start
            dict.Add("PreCheckoutId", st_PayInfo.szPreCheckoutTxnID)
            dict.Add("Cards", objPreCheckRes.PrecheckoutData.Cards.Card)
            st_PayInfo.szCardList = js.Serialize(dict)
            'st_PayInfo.szCardList = "{""PreCheckoutId"":""" + st_PayInfo.szPreCheckoutTxnID + """,""Cards"":["
            'For Each card In objPreCheckRes.PrecheckoutData.Cards.Card
            '    st_PayInfo.szPreCheckoutCardLast4 = card.LastFour
            '    If (1 = card.ExpiryMonth.ToString().Length()) Then
            '        szCardMM = "0" + card.ExpiryMonth.ToString()
            '    Else
            '        szCardMM = card.ExpiryMonth.ToString()
            '    End If
            '    st_PayInfo.szCardExp = card.ExpiryYear.ToString() + szCardMM
            '    st_PayInfo.szPreCheckoutCardId = card.CardId

            '    st_PayInfo.szCardList += "{""CardID"":""" + st_PayInfo.szPreCheckoutCardId + """,""CardHolderName"":""" + card.CardHolderName _
            '        + """,""CardType"":""" + card.BrandName + """,""CardLastFour"":""" + card.LastFour + """,""CardExpired"":""" + st_PayInfo.szCardExp + """}"
            '    'If card Is objPreCheckRes.PrecheckoutData.Cards.Card.Last Then
            '    '    'do something with your last item'
            '    'End If
            'Next
            '{"Precheckoug":"123","cards":[{"cid":"fdsaf","xp":"fdsa"},{"cid":"fdsaf","xp":"fdsa"},{"cid":"fdsaf","xp":"fdsa"},{"cid":"fdsaf","xp":"fdsa"}]}
            'st_PayInfo.szCardList += "]}"

            'Modified  24 Feb 2017. Masterpass-2 - end
            'Temporary turn off get shipping address from precheckout. If Masterpass Ligthbox javascript is not include then it will coz object reference error due to precheckout res no return shipping address obj
            'If (Not (objPreCheckRes.PrecheckoutData.ShippingAddresses Is Nothing)) Then
            '    For Each addr In objPreCheckRes.PrecheckoutData.ShippingAddresses.ShippingAddress
            '        st_PayInfo.szPreCheckoutAddrId = addr.AddressId
            '    Next
            'End If
            bReturn = True

        Catch ex As Exception
            Dim szMsg As String = ""
            'If (ex.ToString.Contains("De-Serialization failed")) Then
            If (True = objTxnProc.bInsertUpdateMPToken(st_PayInfo, 1)) Then
                szMsg = "Could be LAT expired and pairing needed. PreCheckout Long Access Token removed."
            Else
                szMsg = "Could be LAT expired and pairing needed. Removed LAT Failed."
            End If
            'Else
            'szMsg = ex.ToString
            'End If

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), _
                DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "bPreCheckoutRequest() exception: " + szMsg)
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		Masterpass
    ' Function Name:	bExpressCheckoutService
    ' Function Type:	NONE
    ' Parameter:        
    ' Description:		Call the Express Checkout Service to retrieve the full details of the 
    '                   customer's payment, shipping, and reward information
    ' History:			14 Nov 2016
    '********************************************************************************************

    Public Function bExpressCheckoutService(ByRef st_PayInfo As Common.STPayInfo, st_HostInfo As Common.STHost) As Boolean
        Dim bReturn As Boolean = False
        Dim objXCheckoutReq As Model.ExpressCheckoutRequest = New Model.ExpressCheckoutRequest
        Dim objXCheckoutRes As Model.ExpressCheckoutResponse = New Model.ExpressCheckoutResponse
        'Dim szCardMM As String = ""

        Try
            st_PayInfo.szIssuingBank = "MasterPass"

            If True <> objCommon.bGetHostInfo(st_HostInfo, st_PayInfo) Then
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
                                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + "> Error Get Host: " + st_PayInfo.szErrDesc)
            End If

            If True <> objTxnProc.bGetTerminalEx2(st_HostInfo, st_PayInfo) Then 'If True <> objCommon.bGetTerminal(st_HostInfo, st_PayInfo) Then
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
                                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + "> Error Get Terminal: " + st_PayInfo.szErrDesc)
            End If

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12

            With objXCheckoutReq
                .PrecheckoutTransactionId = st_PayInfo.szPreCheckoutTxnID '"insert_precheckoutTransactionId_here"
                .MerchantCheckoutId = st_HostInfo.szPayeeCode '"insert_checkoutId_here"
                .OriginUrl = ConfigurationManager.AppSettings("GWPaymentURL")
                .AdvancedCheckoutOverride = False
                .CurrencyCode = st_PayInfo.szCurrencyCode
                .OrderAmount = st_PayInfo.szTxnAmount.Replace(".", "")
                .DigitalGoods = True 'False
                .CardId = st_PayInfo.szPreCheckoutCardId '"consumer_selected_cardId"
                '.ShippingAddressId = "consumer_selected_shippingAddressId"
                '.RewardProgramId = "consumer_selected_rewardId"
            End With

            objXCheckoutRes = Api.ExpressCheckoutApi.Create(st_PayInfo.szLongAccessToken, objXCheckoutReq)

            If (Not objXCheckoutRes Is Nothing) Then
                st_PayInfo.szParam2 = objXCheckoutRes.Checkout.Card.AccountNumber
                st_PayInfo.szCardPAN = st_PayInfo.szParam2
                If st_PayInfo.szCardPAN.Length > 10 Then
                    st_PayInfo.szMaskedCardPAN = st_PayInfo.szCardPAN.Substring(0, 6) + "".PadRight(st_PayInfo.szCardPAN.Length - 6 - 4, "X") + st_PayInfo.szCardPAN.Substring(st_PayInfo.szCardPAN.Length - 4, 4)
                Else
                    st_PayInfo.szMaskedCardPAN = "".PadRight(st_PayInfo.szCardPAN.Length, "X")
                End If

                'If (1 = objXCheckoutRes.Checkout.Card.ExpiryMonth.ToString().Length()) Then
                '    szCardMM = "0" + objXCheckoutRes.Checkout.Card.ExpiryMonth.ToString()
                'Else
                '    szCardMM = objXCheckoutRes.Checkout.Card.ExpiryMonth.ToString()
                'End If

                'st_PayInfo.szCardExp = objXCheckoutRes.Checkout.Card.ExpiryYear.ToString() + szCardMM
                st_PayInfo.szCardExp = objXCheckoutRes.Checkout.Card.ExpiryYear.ToString() + Right(("0" + objXCheckoutRes.Checkout.Card.ExpiryMonth.ToString()), 2)
                st_PayInfo.szParam3 = st_PayInfo.szCardExp

                st_PayInfo.szParam4 = objXCheckoutRes.Checkout.Card.CardHolderName
                'st_PayInfo.szCardType = objXCheckoutRes.Checkout.Card.BrandName

                If (st_PayInfo.szCardPAN <> "") Then
                    If ("4" = Left(st_PayInfo.szCardPAN, 1)) Then
                        st_PayInfo.szCardType = "V"
                    ElseIf ("5" = Left(st_PayInfo.szCardPAN, 1) Or "2" = Left(st_PayInfo.szCardPAN, 1)) Then
                        st_PayInfo.szCardType = "M"
                    ElseIf ("34" = Left(st_PayInfo.szCardPAN, 2) Or "37" = Left(st_PayInfo.szCardPAN, 2)) Then
                        st_PayInfo.szCardType = "A"
                    ElseIf ("36" = Left(st_PayInfo.szCardPAN, 2) Or "38" = Left(st_PayInfo.szCardPAN, 2)) Then
                        st_PayInfo.szCardType = "D"
                    ElseIf ("35" = Left(st_PayInfo.szCardPAN, 2)) Then
                        st_PayInfo.szCardType = "J"
                    End If
                End If

                st_PayInfo.szCustPhone = objXCheckoutRes.Checkout.Contact.PhoneNumber
                st_PayInfo.szWalletID = objXCheckoutRes.Checkout.WalletID
                st_PayInfo.szLongAccessToken = objXCheckoutRes.LongAccessToken

                If ("" <> st_PayInfo.szLongAccessToken) Then
                    If (True = objTxnProc.bInsertUpdateMPToken(st_PayInfo)) Then
                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "> MasterPass (EC) Long Access Token updated.")
                    End If
                End If

                'Dim objBillingAddr As Model.Address = objCard.BillingAddress
                'Dim objContact As Model.Contact = objCheckout.Contact
                'Dim objAuthOptions As Model.AuthenticationOptions = objCheckout.AuthenticationOptions
                'Dim szPreCheckoutTxnID As String = objCheckout.PreCheckoutTransactionId
                'Dim objRewardPrg As Model.RewardProgram = objCheckout.RewardProgram
                'Dim objShippingAddr As Model.ShippingAddress = objCheckout.ShippingAddress
                'Dim TxnID As String = objCheckout.TransactionId

                bReturn = True
            Else
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), _
                DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "bExpressCheckoutService() Failed to retrieve card details from Express Checkout")
            End If
        Catch ex As Exception
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), _
                DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "bExpressCheckoutService() exception: " + ex.ToString)
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		Masterpass
    ' Function Name:	MPPostbackMerchantTxn
    ' Function Type:	NONE
    ' Parameter:        
    ' Description:		Log merchant txn to Masterpass
    ' History:			14 Sept 2016
    '********************************************************************************************
    Public Function MPPostbackMerchantTxn(ByVal st_HostInfo As Common.STHost, ByVal st_PayInfo As Common.STPayInfo) As Boolean

        '         //Create an instance of MerchantTransactions
        '     MerchantTransactions merchantTransactions = New MerchantTransactions()
        '         .With_MerchantTransactions(new MerchantTransaction()
        '     .WithTransactionId(YOUR_TANSACTION_ID)
        '     .WithPurchaseDate("2016-05-27T12:38:40.479+05:30")
        '     .WithExpressCheckoutIndicator(False)
        '     .WithApprovalCode("sample")
        '     .WithTransactionStatus("Success")
        '             .WithOrderAmount((long)76239)
        '     .WithCurrency("USD")
        '             .WithConsumerKey(YOUR_CONSUMER _KEY));

        ' //Call the PostbackService with required params
        'MerchantTransactions merchantTransactionsResponse = PostbackApi.Create(merchantTransactions);

        Dim bReturn As Boolean = False
        Dim objMerchantTxns As Model.MerchantTransactions = New Model.MerchantTransactions
        Dim objMerchantTxnsRes As Model.MerchantTransactions = New Model.MerchantTransactions
        Dim objTxn As Model.MerchantTransaction = New Model.MerchantTransaction
        Dim osOffset As DateTimeOffset
        Dim szStatus As String = ""

        Try
            osOffset = New DateTimeOffset(st_PayInfo.szTxnDateTime)
            'postback call in bInsertTxnResp function from bank response page. 

            If (0 = st_PayInfo.iTxnStatus) Then
                szStatus = "Success"
            Else
                szStatus = "Failure"
            End If

            bGetComsumerKey(st_PayInfo, st_HostInfo)

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12

            With objTxn
                .WithConsumerKey(st_HostInfo.szAcquirerID)
                .WithTransactionId(st_PayInfo.szParam12) 'Checkout.TransactionId / CheckoutID
                '.WithTransactionId(st_PayInfo.szTxnID)
                .WithPurchaseDate(osOffset.ToString("yyyy-MM-dd'T'HH:mm:ss.fffzzz"))
                .WithApprovalCode(st_PayInfo.szAuthCode)
                '.WithTransactionStatus("Success")   'Only Log success txnn
                .WithTransactionStatus(szStatus)
                .WithOrderAmount(st_PayInfo.szTxnAmount.Replace(".", ""))  'TxnAmount as long value - 10100
                .WithCurrency(st_PayInfo.szCurrencyCode)
            End With

            With objMerchantTxns
                .With_MerchantTransactions(objTxn)
            End With

            objMerchantTxnsRes = Api.PostbackApi.Create(objMerchantTxns)

            bReturn = True

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), _
            DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "GatewayTxnID [" + st_PayInfo.szTxnID + "] MP TransactionId [" + st_PayInfo.szParam12 + "] Update MP done.")

        Catch ex As Exception
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), _
               DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "MPPostbackMerchantTxn() exception: " + ex.ToString)
        End Try

        Return bReturn

    End Function

    '********************************************************************************************
    ' Class Name:		Masterpass
    ' Function Name:	bGetComsumerKey
    ' Function Type:	Boolean.  True if unique else false
    ' Parameter:		
    ' Description:		Get ConsumerKey from database
    ' History:			5 March 2018
    '********************************************************************************************
    Public Function bGetComsumerKey(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim iQueryStatus As Integer = 0
        Dim iRowsAffected As Integer = 0
        Dim dr As SqlDataReader = Nothing

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetTerminalByHostName", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_HostName", SqlDbType.VarChar, 20)

            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@sz_HostName").Value = "MASTERPASS"

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If objCommand.Parameters("@i_Return").Value = 0 Then
                If dr.Read() Then
                    st_HostInfo.szAcquirerID = dr("AcquirerID").ToString
                End If
            Else
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), _
                DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "Failed to retrieve ConsumerKey.")
            End If
            
            dr.Close()

            bReturn = True

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetComsumerKey() exception: " + ex.StackTrace + ex.Message + " > MerchantPymtID(" + st_PayInfo.szMerchantTxnID + ")"

            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    Public Sub New(ByRef o_LoggerII As LoggerII.CLoggerII)
        Try
            objLoggerII = o_LoggerII
            objTxnProc = New TxnProc(objLoggerII)
            objTxnProcOB = New TxnProcOB(objLoggerII)
            objCommon = New Common(objTxnProc, objTxnProcOB, objLoggerII)
            objHash = New Hash(objLoggerII)

            'szDBConnStr = ConfigurationManager.AppSettings("DBString2")  'Modified 11 Oct 2017, use DBString2 encrypted by non card key
            szDBConnStr = ConfigurationManager.AppSettings("PGDBString")  'Modified 11 Oct 2017, use DBString2 encrypted by non card key

            'Added, 26 Jul 2013, cater for encrypted database string
            If (InStr(szDBConnStr.ToLower(), "uid=") <= 0) Then
                szDBConnStr = objHash.szDecrypt3DES(szDBConnStr, Common.C_3DESAPPKEY2, Common.C_3DESAPPVECTOR)
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#Region "IDisposable Support"
    Private disposedValue As Boolean ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: dispose managed state (managed objects).
            End If

            ' TODO: free unmanaged resources (unmanaged objects) and override Finalize() below.
            ' TODO: set large fields to null.
        End If
        Me.disposedValue = True
    End Sub

    ' TODO: override Finalize() only if Dispose(ByVal disposing As Boolean) above has code to free unmanaged resources.
    'Protected Overrides Sub Finalize()
    '    ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
    '    Dispose(False)
    '    MyBase.Finalize()
    'End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        If Not objCommon Is Nothing Then objCommon = Nothing
        If Not objTxnProc Is Nothing Then objTxnProc = Nothing
        If Not objTxnProcOB Is Nothing Then objTxnProcOB = Nothing 'Added, 25 Nov 2013
        If Not objHash Is Nothing Then objHash = Nothing

        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
