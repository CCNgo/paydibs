Imports LoggerII
Imports System.IO
Imports System
Imports System.Data
Imports System.Security.Cryptography
Imports System.Threading
Imports System.Text.RegularExpressions
Imports System.Web              'HttpContext for Current.Response.Redirect, Current.Server.MapPath; HttpServerUtility/HttpUtility for Server.UrlEncode
Imports System.Web.Mail         'MailMessage
Imports System.Xml              'Added  30 Oct 2013, for XmlReader in szGetTagValue
Imports System.Globalization    'Added, 17 Feb 2014, CultureInfo
Imports System.Net              'Added, 17 Jun 2015, ServicePointManager
Imports Newtonsoft.Json.Linq    'Added, 3 Sept 2015, for Bitnet
Imports Newtonsoft.Json         'Aded  4 Dec 2017, to get iso8601 format date time as raw string
Imports System.Security
'Imports MailService

Public Class Response
    Implements IDisposable

    Private objLoggerII As LoggerII.CLoggerII
    Private objCommon As Common
    Private objTxnProc As TxnProc
    Private objTxnProcOB As TxnProcOB             'Added, 19 Nov 2013
    Private objHash As Hash
    Private objHTTP As CHTTP
    Private Server As System.Web.HttpServerUtility = System.Web.HttpContext.Current.Server 'Server.UrlEncode, Server.MapPath; If HttpUtility just Server.UrlEncode
    Private objJS As New System.Web.Script.Serialization.JavaScriptSerializer

    Public g_JSONResp As New Dictionary(Of String, String)
    Public g_JSONLog As New Dictionary(Of String, String)

    '********************************************************************************************
    ' Class Name:		Response
    ' Function Name:	bGetData
    ' Function Type:	NONE
    ' Parameter:		NONE
    ' Description:		Data collection from the HTTP POST and GET collection.
    ' History:			29 Oct 2008
    ' Modified:         16 June 2016  DOKU. Added Optional sz_ResS2S parameter
    '********************************************************************************************
    Public Function bGetData(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost, Optional ByVal sz_ResS2S As String = "") As Boolean
        Dim bReturn As Boolean = False

        Dim iLoop As Integer
        Dim iCounter As Integer = 0
        Dim iIndex As Integer = 0
        Dim iField As Integer = 0
        Dim iStart As Integer = 0           'Added on 25 Oct 2009
        Dim iEnd As Integer = 0             'Added on 25 Oct 2009
        Dim szEqual As String = ""          'Added on 25 Oct 2009
        Dim szDelimiterEx As String = ""    'Added on  4 Mar 2010
        Dim szConfigFile As String = ""
        Dim objStreamReader As StreamReader = Nothing
        Dim szLine As String = ""
        Dim szReqOrRes As String = ""
        Dim szConfig() As String = Nothing
        Dim szFieldName() As String = Nothing
        Dim szFieldValue() As String = Nothing
        Dim szValue As String = ""
        Dim szHashValue As String = ""
        Dim aszValue() As String = Nothing
        Dim szField As String = ""
        Dim szFormat As String = ""
        Dim szHashMethod As String = ""
        Dim bFound As Boolean = False
        Dim szOperator As String = ""
        Dim szInterOperator As String = ""
        Dim iNextCounter As Integer = 0
        Dim bLastVerified As Boolean = False
        Dim bVerified As Boolean = False
        Dim bLastVerifiedDone As Boolean = False
        Dim stPayInfo As Common.STPayInfo = Nothing     'Added on 16 June 2009, declare local PayInfo structure so that will not overwrite st_PayInfo which is storing response data
        Dim aszSplit() As String = Nothing              'Added on 24 Nov 2010 
        Dim bReturnKeyDecrypt As Boolean = False        'Added, 26 Apr 2011
        Dim aszDecode() As String                       'Added 18 Sept 2015, for AllDebit

        Dim objJson As JObject                          'Added, 3 Sept 2015, for Bitnet
        Dim szJsonElement
        Dim iJsonElement As Integer = 0
        Dim aszJsonFields() As String

        Try
            'Request Object (Re: Microsoft Visual Studio.NET 2003 Documentation - Request Object object[IIS] described)
            'The Request object retrieves the values that the client browser passed to the server during an HTTP request.
            'If the specified variable is not in one of the preceding five collections, the Request object returns EMPTY.
            'All variables can be accessed directly by calling Request(variable) without the collection name. In this case, the Web server searches the collections in the following order:
            '1.QueryString
            '2.Form
            '3.Cookies
            '4.ClientCertificate
            '5.ServerVariables
            'If a variable with the same name exists in more than one collection, the Request object returns the first instance that the object encounters.
            'It is strongly recommended that when referring to members of a collection the full name be used. For example, rather than Request.("AUTH_USER") use Request.ServerVariables("AUTH_USER"). This allows the server to locate the item more quickly.

            'Parse Host reply based on the respective Host configuration file
            'Added iReqRes on 23 Aug 2009
            stPayInfo.iReqRes = 2
            stPayInfo.szPymtMethod = st_PayInfo.szPymtMethod            'Added, 25 Nov 2013
            szReqOrRes = "RES" + sz_ResS2S                              'Modified  15 June 2016. DOKU. Added sz_ResS2S to support different response message format of Redirect and S2S
            szConfigFile = Server.MapPath(st_HostInfo.szHostTemplate)
            objStreamReader = System.IO.File.OpenText(szConfigFile)

            Do
                szLine = objStreamReader.ReadLine()

                'Added, 5 Sept 2013
                If ("" = szLine) Then
                    Exit Do
                End If

                szConfig = Split(szLine, "|")

                If ((szConfig(0).Trim().ToUpper() = st_PayInfo.szTxnType.ToUpper()) And (szConfig(1).Trim().ToUpper() = szReqOrRes.ToUpper())) Then
                    '######################################################################################################
                    'Added, 28 Nov 2013, search for bank response's field name for [HashValue] and then
                    'ensure if [HashValue] is defined in template, then response must contain the Hash Field Name and with value
                    '******************************************* WARNING **************************************************
                    'This checking is vital to avoid hacker just removed or renamed the response message's Hash Field Name,
                    'then gateway will not be able to search the hash field name defined in template, and therefore,
                    'will not perform hash value verification!
                    '******************************************************************************************************
                    'iStart = InStr(szLine, "[HashValue]", CompareMethod.Text)       'Position of [HashValue]
                    'If (iStart > 0) Then
                    '    szLine = szLine.Substring(0, iStart - 2)                    'Whole string until end of hash field name
                    '    iStart = InStrRev(szLine, "|")                              'Position of "|HashFieldName"

                    '    If (iStart > 0) Then
                    '        szHashFieldName = szLine.Substring(iStart)              'Retrieved HashFieldName
                    '        If (szHashFieldName <> "") Then
                    '            If ("" = Server.UrlDecode(HttpContext.Current.Request(szHashFieldName))) Then
                    '                st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                    '                st_PayInfo.szErrDesc = "> Expected Hash Field (" + szHashFieldName + ") is empty. Discard " + st_PayInfo.szIssuingBank + " response"

                    '                If Not objStreamReader Is Nothing Then
                    '                    objStreamReader.Close()
                    '                    objStreamReader = Nothing
                    '                End If

                    '                GoTo CleanUp
                    '            End If
                    '        End If
                    '    End If
                    'End If
                    '######################################################################################################

                    bFound = True
                    Exit Do
                End If
            Loop Until (szLine Is Nothing)

            If Not objStreamReader Is Nothing Then  'Added, 5 Sept 2013, checking of Is Nothing
                objStreamReader.Close()
                objStreamReader = Nothing           'Added, 5 Sept 2013
            End If

            If (False = bFound) Then
                st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                st_PayInfo.szErrDesc = "> TxnType(" + szReqOrRes + "_" + st_PayInfo.szTxnType + ") not found in Config Template for " + st_PayInfo.szIssuingBank

                GoTo CleanUp
            End If

            'Added, 3 Sept 2015, for Bitnet
            If ("JSON" = szConfig(2).Trim()) Then
                objJson = JObject.Parse(st_PayInfo.szRespContent)
            End If

            'Parse text line of host reply message configuration and populate payment information
            'Txn Type|Request/Response?|Post Method|Host Field Name|Gateway Field Variable to be filled with Host's value|....
            'CIMB:PAY|RES|GET|billAccountNo|[OrderNumber]|billReferenceNo|[GatewayTxnID]|amount|[TxnAmount]|paymentRefNo|[BankRefNo]|status|[TxnStatus]
            For iLoop = 3 To szConfig.GetUpperBound(0)

                If (0 <> (iLoop Mod 2)) Then
                    'Verify Host Field Name
                    'Commented on 2 Oct 2009
                    'If (InStr(szConfig(iLoop), "[") > 0) Or (InStr(szConfig(iLoop), "]") > 0) Then
                    '    st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                    '    st_PayInfo.szErrDesc = "> Invalid host field name(" + szConfig(iLoop) + ") in Config Template for " + st_PayInfo.szIssuingBank

                    '    GoTo CleanUp
                    'End If
                    If ("[=" = Left(szConfig(iLoop).Trim(), 2)) Then
                        szValue = Mid(szConfig(iLoop).Trim(), 3, szConfig(iLoop).Trim().Length - 3)
                    ElseIf ("0" = szConfig(iLoop).Trim()) Then
                        'Gets the whole reply message, especially reply from host that is encrypted
                        szValue = HttpContext.Current.Request.QueryString.Get(Convert.ToInt32(szConfig(iLoop).Trim()))
                    ElseIf ("NF@" = Left(szConfig(iLoop).Trim(), 3)) Then
                        'Added on 24 Nov 2010  to cater for response which does not contain field name but contains delimiter.
                        'NF@ - NF ==> No Field Name; @x ==> Index x
                        'Example value: 1&123&ABCDEF
                        'Example format: TXNTYPE|RES|&|NF@0|[RespCode] ==> Retrieve Index 0 from response string (which does not contain field name) to RespCode
                        'NOTE: This is efficient ONLY for single field retrieval, NOT efficient for multiple fields retrieval because it involves Split each time to retrieve value of a particular index for each field
                        'For multiple fields extraction, use QUERY|RES|GET|0|[Reserved]{\&!0![RespCode]!4![BankRefNo]\}
                        aszSplit = Split(szConfig(iLoop), "@")
                        'Gets the whole reply message
                        szValue = HttpContext.Current.Request.QueryString.Get(0)
                        aszValue = Split(szValue, szConfig(2))
                        szValue = aszValue(aszSplit(1))

                    ElseIf ("JSON" = szConfig(2).Trim()) Then   'Added, 3 Sept 2015
                        If ("[HEADER," = Left(szConfig(iLoop).Trim(), 8).ToUpper()) Then   'Added, 15 Sept 2015
                            'e.g. [Header,Digest]|[HashValue]
                            Dim szHeader() As String
                            szHeader = Split(szConfig(iLoop).Trim().Replace("]", ""), ",")
                            szValue = HttpContext.Current.Request.Headers().Get(szHeader(1))

                            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > Field[" + szHeader(1) + "] Value[" + szValue + "]")
                        Else
                            'Modified 15 Sept 2015, moved from below to here
                            aszJsonFields = Split(szConfig(iLoop).Trim(), ":")

                            'Cater for retrieving value from nested objects
                            iJsonElement = 0
                            For Each szJsonField As String In aszJsonFields
                                If (0 = iJsonElement) Then                              'The 1st element
                                    szJsonElement = objJson(szJsonField)

                                    If (1 = aszJsonFields.Length()) Then                'Single element
                                        szValue = szJsonElement                         'Retrieve the respective value
                                    End If
                                ElseIf (aszJsonFields.Length() - 1 = iJsonElement) Then 'The last element
                                    szValue = szJsonElement(szJsonField)                'Retrieve the respective value
                                Else
                                    szJsonElement = szJsonElement(szJsonField)          'The 2nd till the ..th element b4 the last element
                                End If
                                iJsonElement = iJsonElement + 1
                            Next

                            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > Field[" + szConfig(iLoop).Trim() + "] Value[" + szValue + "]")
                        End If

                    ElseIf (("[" = Left(szConfig(iLoop).Trim(), 1)) And ("[=" <> Left(szConfig(iLoop).Trim(), 2))) Then  'Added 17 Sept 2015, for AllDebit, to support dynamic field name, e.g. [TID]|[TxnStatus]
                        Select Case szConfig(iLoop).Trim().ToLower()
                            Case "[tid]"
                                'get MerchantID, Currency
                                If ("" <> st_PayInfo.szTxnID) Then
                                    If (True <> objCommon.bGetReqTxn(st_PayInfo, st_HostInfo)) Then
                                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                        stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bGetData() " + stPayInfo.szErrDesc)
                                        Return False
                                    End If
                                End If

                                'Get TID
                                If True <> objCommon.bGetTerminal(st_HostInfo, st_PayInfo) Then
                                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                    stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bGetData() " + stPayInfo.szErrDesc)
                                    Return False
                                End If

                                szField = st_HostInfo.szTID
                        End Select

                        szValue = Server.UrlDecode(HttpContext.Current.Request(szField))
                        szField = ""

                    ElseIf ("XML" = szConfig(2).Trim()) Then
                        szValue = szGetTagValue(st_PayInfo.szRespContent, szConfig(iLoop), st_PayInfo)

                    ElseIf ("POST&ENETS" = szConfig(2).Trim()) Then     'Added by SUKI 20 Mar 2019 - eNets integration
                        szValue = Server.UrlDecode(HttpContext.Current.Request(szConfig(iLoop)))

                        If ("" = szValue) Then
                            If ("hmac" = szConfig(iLoop)) Then
                                szValue = HttpContext.Current.Request.Headers.Item("hmac")
                            Else
                                HttpContext.Current.Request.InputStream.Seek(0, 0)
                                HttpContext.Current.Request.InputStream.Position = 0
                                Dim sr As StreamReader = New StreamReader(HttpContext.Current.Request.InputStream)
                                szValue = sr.ReadToEnd
                            End If
                        End If

                    Else
                        'Gets field value based on field name
                        'Removed Trim() on 15 Oct 2009 to avoid exception if the field requested does not exist
                        szValue = Server.UrlDecode(HttpContext.Current.Request(szConfig(iLoop)))
                        'Added  12 Sept 2012, to cater for possibility of MBB and MBBAmex credit response having "&amp;" in front of field name via non-IE browser
                        If ("" = szValue) Then
                            szValue = Server.UrlDecode(HttpContext.Current.Request("amp;" + szConfig(iLoop)))
                        End If
                    End If
                Else
                    'Verify Gateway Field Value tag
                    If (InStr(szConfig(iLoop), "[") <= 0) Or (InStr(szConfig(iLoop), "]") <= 0) Then
                        st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                        st_PayInfo.szErrDesc = "> Invalid gateway field value tag(" + szConfig(iLoop) + ") in Config Template for " + st_PayInfo.szIssuingBank

                        GoTo CleanUp
                    End If

                    szFormat = ""
                    szFormat = szConfig(iLoop).Trim()   ' Added on 25 Oct 2010, without lowercase
                    szField = szConfig(iLoop).Trim().ToLower()
                    iIndex = InStr(szField, "{")    'InStr starts with 1
                    If (iIndex > 0) Then
                        'Example: Hash|[HashValue]{#[HashMethod]![ReturnKey]![-]!RespCode![-]!InvID![-]!InvRef![-]!InvCy![-]!InvTotal![-]!InvTest![-]!MerchRef#}
                        szFormat = szFormat.Substring(iIndex).Replace("}", "")  'Modified on 25 Oct 2010, changed szField. to szFormat.Substring
                        szField = szField.Substring(0, iIndex - 1)
                    Else
                        szFormat = ""                   ' Added on 25 Oct 2010
                    End If

                    Select Case szField
                        Case "[ordernumber]"
                            st_PayInfo.szMerchantOrdID = szValue
                        Case "[gatewaytxnid]"
                            st_PayInfo.szTxnID = szValue
                        Case "[txnamount]"      'Amount in 2 decimals
                            st_PayInfo.szTxnAmount = szValue
                            st_PayInfo.szHostTxnAmount = szValue    'Added, 16 Jan 2018, to solve gateway and host amount mismatched for host resp which got redirect and s2s due to bGetReqTxn in redirect resp page overwritten st_PayInfo.szTxnAmount in bGetData
                        Case "[txnamount_2]"    'Amount in cents whereby the last two digits are decimal points
                            If ("" <> szValue) Then
                                'Added, 14 Nov 2013, checking of szValue's length to cater for only 1 to 2 digits returned
                                If (1 = szValue.Length()) Then
                                    szValue = "00" + szValue
                                ElseIf (2 = szValue.Length()) Then
                                    szValue = "0" + szValue
                                End If
                                st_PayInfo.szTxnAmount = Left(szValue, szValue.Length - 2) + "." + Right(szValue, 2)
                            End If
                        Case "[txnamtexpn_2]"    'Amount without decimal point but the last two digits are Zero     'Added on 6  Jan 2016, MIGS exponent currency
                            If ("" <> szValue) Then
                                If ("JPY" = st_PayInfo.szCurrencyCode Or "KRW" = st_PayInfo.szCurrencyCode Or "VND" = st_PayInfo.szCurrencyCode Or "IDR" = st_PayInfo.szCurrencyCode) Then
                                    st_PayInfo.szTxnAmount = szValue + ".00"
                                Else
                                    'Added, 14 Nov 2013, checking of szValue's length to cater for only 1 to 2 digits returned
                                    If (1 = szValue.Length()) Then
                                        szValue = "00" + szValue
                                    ElseIf (2 = szValue.Length()) Then
                                        szValue = "0" + szValue
                                    End If
                                    st_PayInfo.szTxnAmount = Left(szValue, szValue.Length - 2) + "." + Right(szValue, 2)
                                End If
                            End If
                        Case "[txnamount_02]"   'Added on 25 Oct 2009. 12 digits, leading zeroes, no decimal point, last 2 are decimal values. On 24 Jun 2010, modified CDbl to Convert.ToDecimal becoz CDbl will round up the value.
                            If ("" <> szValue) Then
                                st_PayInfo.szTxnAmount = CStr(Convert.ToDecimal(Left(szValue, szValue.Length - 2) + "." + Right(szValue, 2)))
                            End If
                        Case "[bankrefno]"
                            st_PayInfo.szHostTxnID = szValue
                        Case "[param1]"
                            st_PayInfo.szParam1 = szValue
                        Case "[param2]"
                            st_PayInfo.szParam2 = szValue
                        Case "[param3]"
                            st_PayInfo.szParam3 = szValue
                        Case "[param4]"
                            st_PayInfo.szParam4 = szValue
                        Case "[param5]"
                            st_PayInfo.szParam5 = szValue
                        Case "[param6]"
                            st_PayInfo.szParam6 = szValue
                        Case "[param7]"
                            st_PayInfo.szParam7 = szValue
                        Case "[param8]"
                            st_PayInfo.szParam8 = szValue
                        Case "[param9]"
                            st_PayInfo.szParam9 = szValue
                        Case "[param10]"
                            st_PayInfo.szParam10 = szValue
                        Case "[param11]"
                            st_PayInfo.szParam11 = szValue
                        Case "[param12]"
                            st_PayInfo.szParam12 = szValue
                        Case "[param13]"
                            st_PayInfo.szParam13 = szValue
                        Case "[param14]"
                            st_PayInfo.szParam14 = szValue
                        Case "[param15]"
                            st_PayInfo.szParam15 = szValue
                        Case "[currencycode]"   'ASCII currency code, e.g. MYR, IDR..
                            st_PayInfo.szCurrencyCode = szValue
                        Case "[mid]"
                            st_PayInfo.szHostMID = szValue
                        Case "[tid]"
                            st_PayInfo.szHostTID = szValue
                        Case "[txnstatus]"
                            If ("" = szValue) Then  'Added  20 June 2016. DOKU
                                szValue = "-"
                            End If
                            st_PayInfo.szHostTxnStatus = szValue
                        Case "[respcode]"
                            If ("" = szValue) Then  'Added  20 June 2016. DOKU
                                szValue = "-"
                            End If
                            st_PayInfo.szRespCode = szValue
                        Case "[authcode]"
                            st_PayInfo.szAuthCode = szValue
                        Case "[respmesg]"
                            st_PayInfo.szBankRespMsg = szValue
                        Case "[date]"
                            st_PayInfo.szHostDate = szValue
                        Case "[time]"
                            st_PayInfo.szHostTime = szValue
                        Case "[cardpan]"
                            st_PayInfo.szCardPAN = szValue

                            If (st_PayInfo.szCardPAN.Length > 20) Then
                                st_PayInfo.szCardPAN = st_PayInfo.szCardPAN.Substring(0, 20)
                            End If

                            If st_PayInfo.szCardPAN.Length > 10 Then
                                st_PayInfo.szMaskedCardPAN = st_PayInfo.szCardPAN.Substring(0, 6) + "".PadRight(st_PayInfo.szCardPAN.Length - 6 - 4, "X") + st_PayInfo.szCardPAN.Substring(st_PayInfo.szCardPAN.Length - 4, 4)
                            Else
                                st_PayInfo.szMaskedCardPAN = "".PadRight(st_PayInfo.szCardPAN.Length, "X")
                            End If
                        Case "[sessionid]"
                            st_PayInfo.szMerchantSessionID = szValue    'Modified 16 Jul 2014, SessionID to MerchantSessionID
                        Case "[hosttxntype]"                    'Added on 2 Oct 2009
                            st_PayInfo.szHostTxnType = szValue
                        Case "[eci]"                            'Added on 5 Oct 2011  for CCGW
                            st_PayInfo.szECI = szValue
                        Case "[payerauth]"                      'Added on 5 Oct 2011  for CCGW
                            st_PayInfo.szPayerAuth = szValue
                        Case "[avs]"                            'Added on 17 July 2013 for PG
                            st_PayInfo.szAVS = szValue
                        Case "[securecode]"                     'Added  17 Apr 2015, for OTC
                            st_PayInfo.szOTCSecureCode = szValue
                        Case "[hashvalue]"
                            'Added, 30 Sept 2013, checking of szValue is empty, to avoid exception if there is no hash value returned from bank for failed transaction
                            If (szValue <> "") Then
                                st_HostInfo.szHashValue = szValue.Replace(" ", "+") 'Added replacement on 3 Apr 2010
                            End If
                        Case "[hashvalue_optional]"             'Added  27 Aug 2015. PBB, cater for response message that does not hv hash when declined (proceed to query bank) but hv hash when approved;this is to avoid declined response being rejected by Gateway due to no hashing when Gateway expects it has hashing.
                            If (szValue <> "") Then
                                st_HostInfo.szHashValue = szValue.Replace(" ", "+")
                            Else
                                st_HostInfo.bIsHashOptional = True
                            End If
                        Case "[reserved]"
                            st_HostInfo.szReserved = szValue    'Stores the whole encrypted reply message especially
                        Case "[reserved2]"
                            st_HostInfo.szReserved2 = szValue   'Added, 24 Sept 2015, Stores the host's returned field into reserved2 field, for Bitnet
                        Case "[iso3166]"
                            'Maps currency from ISO4217 (3 alpha characters) to ISO3166 (3 numeric digits) format
                            st_PayInfo.szCurrencyCode = objTxnProc.bGetCurrency(szValue, st_PayInfo)
                        Case "[certid]"
                            st_HostInfo.szCertId = szValue    'Added 16 Feb 2016, GPUPOP, to store Cert Serial Number return by UPOP
                        Case "[settleamount_2]"
                            If ("" <> szValue) Then
                                'Added, 14 Nov 2013, checking of szValue's length to cater for only 1 to 2 digits returned
                                If (1 = szValue.Length()) Then
                                    szValue = "00" + szValue
                                ElseIf (2 = szValue.Length()) Then
                                    szValue = "0" + szValue
                                End If
                                st_PayInfo.szHostTxnAmount = Left(szValue, szValue.Length - 2) + "." + Right(szValue, 2)
                            End If
                        Case "[settleiso3166]"
                            'Maps currency from ISO4217 (3 alpha characters) to ISO3166 (3 numeric digits) format
                            st_PayInfo.szHostCurrencyCode = objTxnProc.bGetCurrency(szValue, st_PayInfo)
                        Case "[refundamount_2]"
                            If ("" <> szValue) Then
                                'Added, 14 Nov 2013, checking of szValue's length to cater for only 1 to 2 digits returned
                                If (1 = szValue.Length()) Then
                                    szValue = "00" + szValue
                                ElseIf (2 = szValue.Length()) Then
                                    szValue = "0" + szValue
                                End If
                                st_PayInfo.szRTxnAmount = Left(szValue, szValue.Length - 2) + "." + Right(szValue, 2)
                            End If
                        Case "[refundiso3166]"
                            'Maps currency from ISO4217 (3 alpha characters) to ISO3166 (3 numeric digits) format
                            st_PayInfo.szRCurrencyCode = objTxnProc.bGetCurrency(szValue, st_PayInfo)

                            'Added  21 Oct 2016. MasterPass - start
                        Case "[checkoutstatus]"
                            st_PayInfo.szCheckoutStatus = szValue
                        Case "[merchantid]"
                            st_PayInfo.szMerchantID = szValue
                        Case "[merchanttxnid]"
                            st_PayInfo.szMerchantTxnID = szValue
                        Case "[paymentdesc]"
                            st_PayInfo.szMerchantOrdDesc = szValue
                        Case "[merchantreturnurl]"
                            st_PayInfo.szMerchantReturnURL = szValue
                        Case "[custip]"
                            st_PayInfo.szCustIP = szValue
                        Case "[custname]"
                            st_PayInfo.szCustName = szValue
                        Case "[custemail]"
                            st_PayInfo.szCustEmail = szValue
                        Case "[custphone]"
                            st_PayInfo.szCustPhone = szValue
                        Case "[merchantapprovalurl]"
                            st_PayInfo.szMerchantApprovalURL = szValue
                        Case "[merchantunapprovalurl]"
                            st_PayInfo.szMerchantUnApprovalURL = szValue
                        Case "[merchantcallbackurl]"
                            st_PayInfo.szMerchantCallbackURL = szValue
                        Case "[promocode]"
                            st_PayInfo.szPromoCode = szValue
                        Case "[languagecode]"
                            st_PayInfo.szLanguageCode = szValue
                        Case "[token]"
                            st_PayInfo.szToken = szValue
                            'Added  21 Oct 2016. MasterPass - end
                        Case "[redirecturl]"
                            st_PayInfo.szRedirectURL = szValue
                        Case Else
                            'Added 18 Sept 2015, for AllDebit
                            'e.g. {DECODE},[BankRefNo], { and } will be removed above
                            If ("decode," = Left(szFormat.ToLower(), 7)) Then
                                aszDecode = Split(szFormat, ",")
                                Select Case aszDecode(1).ToLower()
                                    Case "[bankrefno]"
                                        st_PayInfo.szHostTxnID = objHash.szDecode(szValue)
                                End Select
                                szFormat = ""

                            Else
                                'Modified 18 Sept 2015, for AllDebit, moved from below to else here
                                'Unsupported value tag
                                st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                                st_PayInfo.szErrDesc = "> Unsupported gateway field value tag(" + szConfig(iLoop) + ") in Config Template for " + st_PayInfo.szIssuingBank

                                GoTo CleanUp
                            End If
                    End Select

                    'Verification of field value based on format specified
                    'Example: Hashing    : #[HashMethod]![ReturnKey]![-]!RespCode![-]!InvID![-]!InvRef![-]!InvCy![-]!InvTotal![-]!InvTest![-]!MerchRef#
                    'Example: Fixed Value: [=5]
                    'Example: Encryption : *[3DES]!&!BillRef1![GatewayTxnID]!TxDate![Date]!TxTime![Time]!Currency![CurrencyCode]!Amount![TxnAmount]!ConfNum![BankRefNo]!ReturnCode![RespCode]!ReturnMessage![RespMesg]!Md5![HashValue]{#[HashMethod]:BillRef1:[]:Amount:[]:Currency:[]:ConfNum#}*
                    If (szFormat <> "") Then
                        'Encryption - The whole reply message is encrypted
                        If ("*" = Left(szFormat, 1)) Then
                            szValue = szFormat.Replace("*", "")

                            'Modify  25 Nov 2013, CashU Sales S2S response format
                            'Added checking of "[xml]" then split by "~"
                            If ("[xml]" = Left(szValue, 5).ToLower()) Then
                                aszValue = Split(szValue, "~")
                            Else
                                aszValue = Split(szValue, "!")  'Original splitted by "!"
                            End If

                            If ("[3des]" = aszValue(0).Trim().ToLower() Or ("[scb]" = aszValue(0).Trim().ToLower())) Then
                                Dim szFieldSet
                                Dim szFieldSets() As String
                                Dim szFieldEx() As String

                                If ("[3des]" = aszValue(0).Trim().ToLower()) Then
                                    'Added on 16 June 2009, to get MerchantID, CurrencyCode, HostID in order to further bGetTerminal to get SecretKey and InitVector
                                    If ("" <> st_PayInfo.szTxnID) Then
                                        'NOTE: The following config template is NOT YET SUPPORTED!!!!!!
                                        'GatewayTxnID is not encrypted in response, only certain fields are encrypted using 3DES, e.g. ConfNum and Amount
                                        '*** Must put GatewayTxnID before/on the left of other encrypted fields ***
                                        'e.g. BankRef1|[GatewayTxnID]|ConfNum|[Reserved]{*[3DES]!!![BankRefNo]*}|Amount|[Reserved]{*[3DES]!!![TxnAmount]*}
                                        'bGetReqTxn() will be called again in bVerifyResTxn()
                                        stPayInfo.szTxnID = st_PayInfo.szTxnID

                                        'Modified 25 Nov 2013, use objCommon instead of objTxnProc, to cater for different DB connection by Payment Method
                                        If (True <> objCommon.bGetReqTxn(stPayInfo, st_HostInfo)) Then
                                            'Added checking of HostsListTemplate for DDGW Aggregator on 3 Apr 2010 to support duplicate Host response
                                            If (ConfigurationManager.AppSettings("HostsListTemplate") <> "") Then
                                                If (-1 = stPayInfo.iQueryStatus) Then
                                                    'Modified 25 Nov 2013, use objCommon instead of objTxnProc, to cater for different DB connection by Payment Method
                                                    If (True <> objCommon.bGetResTxn(stPayInfo, st_HostInfo)) Then
                                                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                                        stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bGetData() " + stPayInfo.szErrDesc)

                                                        Return False
                                                    End If
                                                Else
                                                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                                    stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bGetData() " + stPayInfo.szErrDesc)
                                                    Return False
                                                End If
                                            Else
                                                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                                stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bGetData() " + stPayInfo.szErrDesc)

                                                Return False
                                            End If
                                        End If
                                    Else
                                        'For bank response which the whole content is encrypted, no GatewayTxnID is available like the above,
                                        'DDGW will need to retrieve the respective merchant's SecretKey to decrypt the payment response.
                                        'Since the whole content is encrypted, GW has no way to identify to which merchant that the response belongs and therefore
                                        'the respective MerchantID identifying merchant, IssuingBank and CurrencyCode need to be
                                        'defined in each response_[IssuingBank]_[MerchantID].aspx and pass to this bGetData().
                                        '0|[Reserved]{*[3DES]!&!BillRef1![GatewayTxnID]!Amount![TxnAmount]*}
                                        stPayInfo.szMerchantID = st_PayInfo.szMerchantID
                                        stPayInfo.szCurrencyCode = st_PayInfo.szCurrencyCode
                                    End If

                                    'Added on 16 June 2009
                                    'Get Terminal and Channel information - MID, TID, SendKey, ReturnKey, SecretKey, InitVector, PayeeCode, PayURL, QueryURL, CfgFile, ReturnURL
                                    'Modified 25 Nov 2013, use objCommon instead of objTxnProc, to cater for different DB connection by Payment Method
                                    If True <> objCommon.bGetTerminal(st_HostInfo, stPayInfo) Then
                                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                        stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bGetData() " + stPayInfo.szErrDesc)
                                        Return False
                                    End If

                                    'Dim szKey As String = st_HostInfo.szSecretKey
                                    'Modified on 25 May 2009
                                    'NOTE: Use szDecrypt3DEStoHex instead of szDecrypt3DES because original clear text(secret key) is HEX value
                                    Dim szKey As String = objHash.szDecrypt3DEStoHex(st_HostInfo.szSecretKey, Common.C_3DESAPPKEY, Common.C_3DESAPPVECTOR)
                                    Dim szIV As String = st_HostInfo.szInitVector

                                    'Decrypts the whole encrypted reply message stored in Reserved field
                                    szValue = objHash.szDecrypt3DES(st_HostInfo.szReserved, szKey, szIV)

                                ElseIf ("[scb]" = aszValue(0).Trim().ToLower()) Then    'Added comment  for [scb] as "Sale Cypher Batch" on 3 Jan 2011
                                    szValue = "decrypt^" + st_HostInfo.szReserved

                                    'Decrypts the whole encrypted reply message stored in Reserved field
                                    szValue = objHash.szGetHash(szValue, st_HostInfo.szHashMethod, st_PayInfo, st_HostInfo) 'Added on 15 Dec 2010 , add 'stHostInfo' for uobThai to get secret key for encryption
                                End If

                                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                " Decrypted Value(" + szValue + ")")

                                'e.g. BillRef1=20050718-021&BillRef2=Richard&TxDate=18072005&TxTime=180203&ExDate=18072005&ExTime=166607&Currency=458&Amount=1000.99&ConfNum=669987&ReturnCode=000&ReturnMessage=Approved.&Active=1&Md5=cb2fe847bfb14fb64cd79f40a058347b
                                szFieldSets = Split(szValue, aszValue(1).Trim(), -1, CompareMethod.Text)

                                'Gets number of field sets (e.g. 2 sets ==> Field1=Value1&Field2=Value2)
                                iField = 0
                                For Each szFieldSet In szFieldSets
                                    'Added on 8 Aug 2009, to cater for the presence of rightmost "==" or "=", normally for encrypted or hash value
                                    'Replace the last two "==" or last "=" character(s) with "@@" or "@" accordingly to avoid inaccurate
                                    'array set from the result of split by "="

                                    'Added on 19 Aug 2009
                                    szFieldSet = Trim(Replace(Replace(Replace(szFieldSet, vbCrLf, ""), vbCr, ""), vbLf, ""))
                                    If ("==" = Right(szFieldSet, 2)) Then
                                        'Differentiate between "field==" from "field=h==" 
                                        'Replace "field==" to "field=@" and "field=h==" to "field=h@@"
                                        If ((szFieldSet.IndexOf("=") + 1) = (szFieldSet.Length() - 1)) Then
                                            szFieldSet = Left(szFieldSet, Len(szFieldSet) - 1) + "@"
                                        Else
                                            szFieldSet = Left(szFieldSet, Len(szFieldSet) - 2) + "@@"
                                        End If
                                    ElseIf ("=" = Right(szFieldSet, 1)) Then
                                        'Differentiate between "field=" (empty value) from "field=value="
                                        'Checks if "=" is the character at the last position of the field value OR, e.g. "field=value="
                                        'if "=" is the field delimiter, e.g. "field="
                                        'Make sure "=" does not exist at the last character of szFieldSet or else
                                        'the array result from the next Split by "=" will encounter out of bound exception
                                        If (szFieldSet.IndexOf("=") <> szFieldSet.Length() - 1) Then
                                            szFieldSet = Left(szFieldSet, Len(szFieldSet) - 1) + "@"
                                        End If
                                    End If

                                    szFieldEx = Split(szFieldSet, "=")
                                    If (1 = szFieldEx.GetUpperBound(0)) Then
                                        iField = iField + 1
                                    End If
                                Next

                                ReDim szFieldName(iField - 1)
                                ReDim szFieldValue(iField - 1)

                                'Populates Field Names and Values array
                                iField = 0
                                For Each szFieldSet In szFieldSets
                                    'Added on 8 Aug 2009, to cater for the presence of rightmost "==" or "=", normally for encrypted or hash value
                                    'Replace the last two "==" or last "=" character(s) with "@@" or "@" accordingly to avoid inaccurate
                                    'array set from the result of split by "="

                                    'Added on 19 Aug 2009
                                    szFieldSet = Trim(Replace(Replace(Replace(szFieldSet, vbCrLf, ""), vbCr, ""), vbLf, ""))
                                    If ("==" = Right(szFieldSet, 2)) Then
                                        'Differentiate between "field==" from "field=h==" 
                                        'Replace "field==" to "field=@" and "field=h==" to "field=h@@"
                                        If ((szFieldSet.IndexOf("=") + 1) = (szFieldSet.Length() - 1)) Then
                                            szFieldSet = Left(szFieldSet, Len(szFieldSet) - 1) + "@"
                                        Else
                                            szFieldSet = Left(szFieldSet, Len(szFieldSet) - 2) + "@@"
                                        End If
                                    ElseIf ("=" = Right(szFieldSet, 1)) Then
                                        'Differentiate between "field=" (empty value) from "field=value="
                                        'Checks if "=" is the character at the last position of the field value OR, e.g. "field=value="
                                        'if "=" is the field delimiter, e.g. "field="
                                        'Make sure "=" does not exist at the last character of szFieldSet or else
                                        'the array result from the next Split by "=" will encounter out of bound exception
                                        If (szFieldSet.IndexOf("=") <> szFieldSet.Length() - 1) Then
                                            szFieldSet = Left(szFieldSet, Len(szFieldSet) - 1) + "@"
                                        End If
                                    End If

                                    szFieldEx = Split(szFieldSet, "=")

                                    If (1 = szFieldEx.GetUpperBound(0)) Then
                                        'Added on 8 Aug 2009, to cater for the presence of rightmost "==" or "=", normally for encrypted or hash value
                                        'Replace the last two "@@" or last "@" character(s) back to the original "==" or "=" accordingly
                                        'to get back the original value since no split will be used now
                                        'Do not use Replace because do not want to replace "@" which exists not in the last two or last character(s)
                                        If ("@@" = Right(szFieldEx(1), 2)) Then
                                            szFieldEx(1) = Left(szFieldEx(1), Len(szFieldEx(1)) - 2) + "=="
                                        ElseIf ("@" = Right(szFieldEx(1), 1)) Then
                                            szFieldEx(1) = Left(szFieldEx(1), Len(szFieldEx(1)) - 1) + "="
                                        End If

                                        szFieldName(iField) = szFieldEx(0)
                                        szFieldValue(iField) = szFieldEx(1)
                                        iField = iField + 1
                                    End If
                                Next

                                'Matches each Field Name from message template against Field Names array to get its respective Field Value.
                                'Then, assigns the respective Field Value to the respective Gateway Varialbes as defined in the message template.
                                'e.g. BillRef1![GatewayTxnID]!TxDate![Date]!TxTime![Time]!Currency![CurrencyCode]!Amount![TxnAmount]!ConfNum![BankRefNo]!ReturnCode![RespCode]!ReturnMessage![RespMesg]!Md5![HashValue]{#[HashMethod]:BillRef1:[]:Amount:[]:Currency:[]:ConfNum#}*
                                bFound = False
                                For iCounter = 2 To aszValue.GetUpperBound(0)
                                    If (0 = (iCounter Mod 2)) Then
                                        For iField = 0 To szFieldName.GetUpperBound(0)
                                            If (aszValue(iCounter).Trim().ToLower() = szFieldName(iField).ToLower()) Then
                                                szValue = szFieldValue(iField)
                                                bFound = True
                                                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                                " Field(" + szFieldName(iField) + ") Value(" + szFieldValue(iField) + ")")
                                                Exit For
                                            End If
                                        Next
                                    Else
                                        szFormat = ""
                                        szField = aszValue(iCounter).Trim().ToLower()
                                        iIndex = InStr(szField, "{")    'InStr starts with 1
                                        If (iIndex > 0) Then
                                            'Example1: Md5|[HashValue]{#[HashMethod]:BillRef1:[]:Amount:[]:Currency:[]:ConfNum#}
                                            'Example2: Hash|[HashValue]{#[HashMethod]![ReturnKey]![-]!RespCode![-]!InvID![-]!InvRef![-]!InvCy![-]!InvTotal![-]!InvTest![-]!MerchRef#}
                                            szFormat = szField.Substring(iIndex).Replace("}", "")
                                            szField = szField.Substring(0, iIndex - 1)
                                        End If

                                        Select Case szField
                                            Case "[ordernumber]"
                                                st_PayInfo.szMerchantOrdID = szValue
                                            Case "[gatewaytxnid]"
                                                st_PayInfo.szTxnID = szValue
                                            Case "[txnamount]"      'Amount in 2 decimals
                                                st_PayInfo.szTxnAmount = szValue
                                            Case "[txnamount_2]"    'Amount in cents whereby the last two digits are decimal points
                                                If ("" <> szValue) Then
                                                    'Added, 14 Nov 2013, checking of szValue's length to cater for only 1 to 2 digits returned
                                                    If (1 = szValue.Length()) Then
                                                        szValue = "00" + szValue
                                                    ElseIf (2 = szValue.Length()) Then
                                                        szValue = "0" + szValue
                                                    End If
                                                    st_PayInfo.szTxnAmount = Left(szValue, szValue.Length - 2) + "." + Right(szValue, 2)
                                                End If
                                            Case "[txnamtexpn_2]"    'Amount without decimal point but the last two digits are Zero     'Added on 6  Jan 2016, MIGS exponent currency
                                                If ("" <> szValue) Then
                                                    If ("JPY" = st_PayInfo.szCurrencyCode Or "KRW" = st_PayInfo.szCurrencyCode Or "VND" = st_PayInfo.szCurrencyCode Or "IDR" = st_PayInfo.szCurrencyCode) Then
                                                        st_PayInfo.szTxnAmount = szValue + ".00"
                                                    Else
                                                        'Added, 14 Nov 2013, checking of szValue's length to cater for only 1 to 2 digits returned
                                                        If (1 = szValue.Length()) Then
                                                            szValue = "00" + szValue
                                                        ElseIf (2 = szValue.Length()) Then
                                                            szValue = "0" + szValue
                                                        End If
                                                        st_PayInfo.szTxnAmount = Left(szValue, szValue.Length - 2) + "." + Right(szValue, 2)
                                                    End If
                                                End If
                                            Case "[txnamount_02]"   'Added on 25 Oct 2009. 12 digits, leading zeroes, no decimal point, last 2 are decimal values. On 24 Jun 2010, modified CDbl to Convert.ToDecimal becoz CDbl will round up the value.
                                                If ("" <> szValue) Then
                                                    st_PayInfo.szTxnAmount = CStr(Convert.ToDecimal(Left(szValue, szValue.Length - 2) + "." + Right(szValue, 2)))
                                                End If
                                            Case "[bankrefno]"
                                                st_PayInfo.szHostTxnID = szValue
                                            Case "[param1]"
                                                st_PayInfo.szParam1 = szValue
                                            Case "[param2]"
                                                st_PayInfo.szParam2 = szValue
                                            Case "[param3]"
                                                st_PayInfo.szParam3 = szValue
                                            Case "[param4]"
                                                st_PayInfo.szParam4 = szValue
                                            Case "[param5]"
                                                st_PayInfo.szParam5 = szValue
                                            Case "[param6]"
                                                st_PayInfo.szParam6 = szValue
                                            Case "[param7]"
                                                st_PayInfo.szParam7 = szValue
                                            Case "[param8]"
                                                st_PayInfo.szParam8 = szValue
                                            Case "[param9]"
                                                st_PayInfo.szParam9 = szValue
                                            Case "[param10]"
                                                st_PayInfo.szParam10 = szValue
                                            Case "[param11]"
                                                st_PayInfo.szParam11 = szValue
                                            Case "[param12]"
                                                st_PayInfo.szParam12 = szValue
                                            Case "[param13]"
                                                st_PayInfo.szParam13 = szValue
                                            Case "[param14]"
                                                st_PayInfo.szParam14 = szValue
                                            Case "[param15]"
                                                st_PayInfo.szParam15 = szValue
                                            Case "[currencycode]"   'ASCII currency code, e.g. MYR, IDR..
                                                st_PayInfo.szCurrencyCode = szValue
                                            Case "[mid]"
                                                st_PayInfo.szHostMID = szValue
                                            Case "[tid]"
                                                st_PayInfo.szHostTID = szValue
                                            Case "[txnstatus]"
                                                st_PayInfo.szHostTxnStatus = szValue
                                            Case "[respcode]"
                                                st_PayInfo.szRespCode = szValue
                                            Case "[authcode]"
                                                st_PayInfo.szAuthCode = szValue
                                            Case "[respmesg]"
                                                st_PayInfo.szBankRespMsg = szValue
                                            Case "[date]"
                                                st_PayInfo.szHostDate = szValue
                                            Case "[time]"
                                                st_PayInfo.szHostTime = szValue
                                            Case "[cardpan]"
                                                st_PayInfo.szCardPAN = szValue

                                                If (st_PayInfo.szCardPAN.Length > 20) Then
                                                    st_PayInfo.szCardPAN = st_PayInfo.szCardPAN.Substring(0, 20)
                                                End If

                                                If st_PayInfo.szCardPAN.Length > 10 Then
                                                    st_PayInfo.szMaskedCardPAN = st_PayInfo.szCardPAN.Substring(0, 6) + "".PadRight(st_PayInfo.szCardPAN.Length - 6 - 4, "X") + st_PayInfo.szCardPAN.Substring(st_PayInfo.szCardPAN.Length - 4, 4)
                                                Else
                                                    st_PayInfo.szMaskedCardPAN = "".PadRight(st_PayInfo.szCardPAN.Length, "X")
                                                End If
                                            Case "[sessionid]"
                                                st_PayInfo.szMerchantSessionID = szValue    'Modified 16 Jul 2014, SessionID to MerchantSessionID
                                            Case "[hashvalue]"
                                                'Added, 30 Sept 2013, checking of szValue is empty to avoid exception when hash value not returned by bank for failed transaction
                                                If (szValue <> "") Then
                                                    st_HostInfo.szHashValue = szValue.Replace(" ", "+") 'Added replacement on 3 Apr 2010
                                                End If
                                            Case "[reserved]"
                                                st_HostInfo.szReserved = szValue
                                            Case Else
                                                'Unsupported value tag
                                                st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                                                st_PayInfo.szErrDesc = "> Unsupported gateway field value tag(" + szConfig(iLoop) + ") in Config Template for " + st_PayInfo.szIssuingBank

                                                GoTo CleanUp
                                        End Select

                                        'Sets szValue to empty so that the next Gateway variable will take empty szValue
                                        'if the Field Name defined in message template does not match any Field Name of
                                        'reply message or else the next Gateway variable will take the last szValue which
                                        'is not empty and belongs to the previous Gateway variable.
                                        szValue = ""
                                    End If
                                Next

                            ElseIf ("[xml]" = aszValue(0).Trim().ToLower()) Then 'Added  22 Nov 2013, CashU
                                Dim iNum As Integer = 0
                                Dim iBegin As Integer = 0
                                Dim szTmp As String = ""

                                For iNum = 1 To aszValue.GetUpperBound(0)
                                    If (0 <> (iNum Mod 2)) Then 'fieldname from host
                                        szValue = szGetTagValue(st_HostInfo.szReserved, aszValue(iNum).Trim(), st_PayInfo)
                                    Else
                                        If (0 = InStr(aszValue(iNum).ToLower(), "#")) Then 'Check for hashing field
                                            Select Case aszValue(iNum).ToLower()
                                                Case "[gatewaytxnid]"
                                                    st_PayInfo.szTxnID = szValue
                                                Case "[txnamount]"
                                                    st_PayInfo.szTxnAmount = szValue
                                                Case "[currencycode]"
                                                    st_PayInfo.szCurrencyCode = szValue
                                                Case "[bankrefno]"
                                                    st_PayInfo.szHostTxnID = szValue
                                                Case "[txnstatus]"
                                                    st_PayInfo.szHostTxnStatus = szValue
                                            End Select
                                        Else
                                            st_HostInfo.szHashValue = szValue

                                            iBegin = InStr(aszValue(iNum), "#")
                                            szTmp = aszValue(iNum).Substring(iBegin - 1, aszValue(iNum).Length - (iBegin - 1))
                                            szFormat = szTmp 'szFormat is hashing format extract from response and pass to below hash verification
                                        End If
                                    End If
                                Next iNum

                                'get MerchantID
                                If ("" <> st_PayInfo.szTxnID) Then
                                    If (True <> objCommon.bGetReqTxn(st_PayInfo, st_HostInfo)) Then
                                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                        stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bGetData() " + stPayInfo.szErrDesc)
                                        Return False
                                    End If
                                End If

                                'Get Payee Code
                                If True <> objCommon.bGetTerminal(st_HostInfo, st_PayInfo) Then
                                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                    stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bGetData() " + stPayInfo.szErrDesc)
                                    Return False
                                End If

                            ElseIf ("[vpc]" = aszValue(0).Trim().ToLower()) Then
                                'Added  22 September 2017, Metrobank
                                'Generate SHA256 HMAC hash for VPC response. 
                                'Generated hash using the secure key stored in eGHL, should match the secure hash from the host response.
                                Dim iCount As Integer
                                Dim i As Integer
                                Dim vpcResponse As String = ""
                                Dim vpcField As String = ""
                                Dim vpcValue As String = ""

                                iCount = HttpContext.Current.Request.QueryString.Count

                                For i = 1 To iCount
                                    vpcField = HttpContext.Current.Request.QueryString.GetKey(i - 1)
                                    vpcValue = HttpContext.Current.Request.QueryString.Get(i - 1)

                                    'Excluding vpc_securehash and vpc_securehashtype field for raw hash value
                                    If ((vpcField <> "")) Then
                                        If (vpcField.ToLower <> "vpc_securehash" And vpcField.ToLower <> "vpc_securehashtype") Then
                                            vpcResponse += vpcField + "=" + vpcValue

                                            'Do not append "&" on the last vpc field.
                                            If i <> iCount Then
                                                vpcResponse += "&"
                                            End If
                                        End If
                                    End If

                                Next i

                                'get MerchantID
                                If ("" <> st_PayInfo.szTxnID) Then
                                    If (True <> objCommon.bGetReqTxn(st_PayInfo, st_HostInfo)) Then
                                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                        stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bGetData() " + stPayInfo.szErrDesc)
                                        Return False
                                    End If
                                End If

                                'Get Payee Code
                                If True <> objCommon.bGetTerminal(st_HostInfo, st_PayInfo) Then
                                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                    stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bGetData() " + stPayInfo.szErrDesc)
                                    Return False
                                End If

                                szHashValue = objHash.szGetHash(vpcResponse, st_HostInfo.szHashMethod, st_PayInfo, st_HostInfo)

                                If (szHashValue.ToLower() <> st_HostInfo.szHashValue.ToLower()) Then
                                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                    " > bGetData() Response Hash Value mismatched - Host(" + st_HostInfo.szHashValue + ") Gateway(" + szHashValue + ") Hash Method(" + szHashMethod + ")")
                                    GoTo CleanUp
                                End If

                                szFormat = ""

                            ElseIf ("[sfn]" = aszValue(0).Trim().ToLower()) Then    'Added  7 July 2014. GlobalPay Cybersource
                                '[sfn] = Signed Field Name. Hashing fields cannot be fixed in template. Cybersource will return different signed field names respectively for success and failed transaction.
                                szValue = st_HostInfo.szReserved

                                'To support request and response different hash method. 
                                If (InStr(st_HostInfo.szHashMethod, "|") > 0) Then
                                    Dim aszTemp As String()
                                    aszTemp = st_HostInfo.szHashMethod.Split("|")
                                    st_HostInfo.szHashMethod = aszTemp(1)
                                End If

                                If ("" = st_HostInfo.szSecretKey) Then
                                    If ("" <> st_PayInfo.szTxnID) Then
                                        stPayInfo.szTxnID = st_PayInfo.szTxnID

                                        If (True <> objCommon.bGetReqTxn(stPayInfo, st_HostInfo)) Then
                                            If (-1 = stPayInfo.iQueryStatus) Then   'Existed in Response table
                                                If (True <> objCommon.bGetResTxn(stPayInfo, st_HostInfo)) Then
                                                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                                    stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bGetData() " + stPayInfo.szErrDesc)

                                                    Return False
                                                End If
                                            Else
                                                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                                stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bGetData() " + stPayInfo.szErrDesc)
                                                Return False
                                            End If
                                        End If
                                    End If '("" <> st_PayInfo.szTxnID)

                                    'Added  4 Nov 2016. HSBC MIGS SHA256
                                    If True <> objTxnProc.bGetMerchant(stPayInfo) Then
                                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                        stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bGetData() " + stPayInfo.szErrDesc)
                                        Return False
                                    End If

                                    'Format: Product Code|Bank MID
                                    If (1 = stPayInfo.iRouteByParam1) Then      'Added  4 Nov 2016. HSBC MIGS SHA256
                                        If True <> objTxnProc.bGetTxnParam(stPayInfo) Then
                                            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                            stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bGetData() " + stPayInfo.szErrDesc)
                                            Return False
                                        End If

                                        If (stPayInfo.szParam1 <> "") Then
                                            Dim aszMID() As String

                                            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                            stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + "> Route by Param1: " + stPayInfo.szParam1)

                                            aszMID = Split(stPayInfo.szParam1, "|")

                                            If (aszMID.GetUpperBound(0) >= 1) Then
                                                st_HostInfo.szMID = aszMID(1)

                                                'Get terminal info by Bank MID
                                                If True <> objTxnProc.bGetTerminalEx(st_HostInfo, stPayInfo) Then
                                                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                                    stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + "> Error: " + stPayInfo.szErrDesc)
                                                    Return False
                                                End If
                                            End If
                                        End If
                                    Else
                                        'Modified  4 Nov 2016, moved into Else
                                        If True <> objCommon.bGetTerminal(st_HostInfo, stPayInfo) Then
                                            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                            stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bGetData() " + stPayInfo.szErrDesc)
                                            Return False
                                        End If
                                    End If
                                End If

                                szHashValue = objHash.szGetHash(szValue, st_HostInfo.szHashMethod, st_PayInfo, st_HostInfo)

                                'Compare Hash
                                If ("" <> st_HostInfo.szHashValue) Then
                                    'Added szEqual checking on 25 Oct 2009
                                    If (szEqual <> "") Then
                                        'Compares szEqual with szHashValue returned by objHash.szGetHash() function
                                        If (szEqual.ToLower() <> szHashValue.ToLower()) Then
                                            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                                                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                            " > bGetData() Response Hash Value mismatched - Defined Value(" + szEqual + ") Gateway(" + szHashValue + ") Hash Method(" + szHashMethod + ")")
                                            GoTo CleanUp
                                        End If
                                    Else
                                        If (szHashValue.ToLower() <> st_HostInfo.szHashValue.ToLower()) Then
                                            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                                                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                            " > bGetData() Response Hash Value mismatched - Host(" + st_HostInfo.szHashValue + ") Gateway(" + szHashValue + ") Hash Method(" + szHashMethod + ")")
                                            GoTo CleanUp
                                        End If
                                    End If
                                Else
                                    'Added, 28 Nov 2013
                                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                    " > bGetData() Expected Response Hash Value EMPTY, discard response")
                                    GoTo CleanUp
                                End If

                                szFormat = ""

                            ElseIf ("[btc]" = aszValue(0).Trim().ToLower()) Then    'Added, 24 Sept 2015. Bitnet
                                '[btc] = Bitcoin
                                'e.g. [Header,Authorization]|[Reserved]|[Header,Date]|[Reserved2]|[Header,Digest]|[HashValue]{*[BTC]*}
                                'Combination of Date and Digest HMACSHA256 calculate Signature and verify Signature
                                'Field[Date] Value[Sun Sep 20 16:31:16 2015 GMT]
                                'Field[Digest] Value[SHA-256=19ee273f0c20bb37dbd4902bbd182efe9d654c6892eabcb2362030fe39587531]
                                'Field[Authorization] Value[Signature keyId="e564c727-e914-4a29-aa1d-0e93a3980615",signature="E/4RHopG4vGbXdIoyxt5KdQvB9UEQ4zkCHdloBWRZtc=",headers="date digest",algorithm="hmac-sha256"]
                                'If valid, then SHA256 the whole response to calculate Digest and verify Digest

                                'Retrieves signature value from response
                                Dim iStartIndex As Integer = 0
                                Dim iEndIndex As Integer = 0

                                iStartIndex = st_HostInfo.szReserved.IndexOf("signature")
                                iStartIndex = st_HostInfo.szReserved.IndexOf("=", iStartIndex)
                                iStartIndex = st_HostInfo.szReserved.IndexOf("""", iStartIndex) + 1
                                iEndIndex = st_HostInfo.szReserved.IndexOf("""", iStartIndex)
                                szValue = st_HostInfo.szReserved.Substring(iStartIndex, iEndIndex - iStartIndex)

                                If ("" = st_HostInfo.szSecretKey) Then
                                    If ("" <> st_PayInfo.szTxnID) Then
                                        stPayInfo.szTxnID = st_PayInfo.szTxnID

                                        If (True <> objCommon.bGetReqTxn(stPayInfo, st_HostInfo)) Then
                                            If (-1 = stPayInfo.iQueryStatus) Then   'Existed in Response table
                                                If (True <> objCommon.bGetResTxn(stPayInfo, st_HostInfo)) Then
                                                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                                    stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bGetData() " + stPayInfo.szErrDesc)

                                                    Return False
                                                End If
                                            Else
                                                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                                stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bGetData() " + stPayInfo.szErrDesc)
                                                Return False
                                            End If
                                        End If
                                    End If '("" <> st_PayInfo.szTxnID)

                                    If True <> objCommon.bGetTerminal(st_HostInfo, stPayInfo) Then
                                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                        stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bGetData() " + stPayInfo.szErrDesc)
                                        Return False
                                    End If
                                End If

                                'Forms signature value
                                ' Dim szSecretBytes = Encoding.UTF8.GetBytes(st_HostInfo.szAcquirerID)
                                'Dim szValueBytes = Encoding.UTF8.GetBytes("date: " + st_HostInfo.szReserved2 + vbLf + "digest: " + st_PayInfo.szHashValue)
                                Dim szSignature As String = ""

                                szSignature = objHash.szGetHash("date: " + st_HostInfo.szReserved2 + vbLf + "digest: " + st_HostInfo.szHashValue, "HMSHA256", st_PayInfo, st_HostInfo)

                                'Using objHMACSHA256 = New HMACSHA256(szSecretBytes)
                                '    Dim btHash = objHMACSHA256.ComputeHash(szValueBytes)
                                '    szSignature = Convert.ToBase64String(btHash)
                                'End Using

                                'Compares response's signature with self generated signature
                                If (szValue = szSignature) Then
                                    'Generates hash value
                                    szValue = objHash.szGetHash(st_PayInfo.szRespContent, st_HostInfo.szHashMethod, st_PayInfo, st_HostInfo)

                                    'Compares hash value
                                    If (st_HostInfo.szHashValue.Replace("SHA-256=", "") <> szValue) Then
                                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                        " > Invalid " + st_PayInfo.szIssuingBank + " Response Hash, Host[" + st_HostInfo.szHashValue + "], Gateway[" + szValue + "], discard response")
                                        GoTo CleanUp
                                    End If
                                Else
                                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                    " > Invalid " + st_PayInfo.szIssuingBank + " Response Signature, Host[" + szValue + "], Gateway[" + szSignature + "], discard response")
                                    GoTo CleanUp
                                End If

                                szFormat = ""
                                szValue = ""
                                'ADD START SUKI 20190320 [eNets integration]
                            ElseIf ("[enets]" = aszValue(0).Trim().ToLower()) Then
                                'szValue = *[enets]!msg![]!netsMid![mid]!netsAmountDeducted![txnamount_2]!currencyCode![currencycode]!merchantTxnRef![gatewaytxnid]!netsTxnStatus![respcode]!netsTxnRespCode![txnstatus]!netsTxnMsg![respmesg]!bankRefCode![bankrefno]

                                objJson = JObject.Parse(st_HostInfo.szReserved)
                                aszJsonFields = Split(szConfig(iLoop).Trim(), "!")

                                iJsonElement = 0
                                For Each szJsonField As String In aszJsonFields
                                    If (0 <> (iJsonElement Mod 2)) Then
                                        If (1 = iJsonElement) Then                              'The 1st element
                                            szJsonElement = objJson(szJsonField)
                                        Else
                                            szValue = szJsonElement(szJsonField)
                                        End If
                                    Else
                                        If (InStr(szJsonField, "[") <= 0) Or (InStr(szJsonField, "]") <= 0) Then
                                            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                                            st_PayInfo.szErrDesc = "> Invalid gateway field value tag(" + szJsonField + ") in Config Template for " + st_PayInfo.szIssuingBank

                                            GoTo CleanUp
                                        End If

                                        szFormat = ""
                                        szFormat = szJsonField.Trim()
                                        szField = szJsonField.Trim().ToLower()
                                        iIndex = InStr(szField, "{")
                                        If (iIndex > 0) Then
                                            szFormat = szFormat.Substring(iIndex).Replace("}", "")
                                            szField = szField.Substring(0, iIndex - 1)
                                        Else
                                            szFormat = ""                   '
                                        End If

                                        Select Case szField
                                            Case "[ordernumber]"
                                                st_PayInfo.szMerchantOrdID = szValue
                                            Case "[gatewaytxnid]"
                                                st_PayInfo.szTxnID = szValue
                                            Case "[txnamount]"      'Amount in 2 decimals
                                                st_PayInfo.szTxnAmount = szValue
                                                st_PayInfo.szHostTxnAmount = szValue    'Added, 16 Jan 2018, to solve gateway and host amount mismatched for host resp which got redirect and s2s due to bGetReqTxn in redirect resp page overwritten st_PayInfo.szTxnAmount in bGetData
                                            Case "[txnamount_2]"    'Amount in cents whereby the last two digits are decimal points
                                                If ("" <> szValue) Then
                                                    'Added, 14 Nov 2013, checking of szValue's length to cater for only 1 to 2 digits returned
                                                    If (1 = szValue.Length()) Then
                                                        szValue = "00" + szValue
                                                    ElseIf (2 = szValue.Length()) Then
                                                        szValue = "0" + szValue
                                                    End If
                                                    st_PayInfo.szTxnAmount = Left(szValue, szValue.Length - 2) + "." + Right(szValue, 2)
                                                End If
                                            Case "[bankrefno]"
                                                st_PayInfo.szHostTxnID = szValue
                                            Case "[currencycode]"   'ASCII currency code, e.g. MYR, IDR..
                                                st_PayInfo.szCurrencyCode = szValue
                                            Case "[mid]"
                                                st_PayInfo.szHostMID = szValue
                                            Case "[tid]"
                                                st_PayInfo.szHostTID = szValue
                                            Case "[txnstatus]"
                                                If ("" = szValue) Then
                                                    szValue = "-"
                                                Else
                                                    szValue = szValue.Substring(5, 5)       'eNets sample "stageRespCode":"0006-00000"
                                                End If
                                                st_PayInfo.szHostTxnStatus = szValue
                                            Case "[respcode]"
                                                If ("" = szValue) Then
                                                    szValue = "-"
                                                End If
                                                st_PayInfo.szRespCode = szValue
                                            Case "[authcode]"
                                                st_PayInfo.szAuthCode = szValue
                                            Case "[respmesg]"
                                                st_PayInfo.szBankRespMsg = szValue
                                            Case Else
                                                If (InStr(szJsonField, "*") >= 0) Then
                                                Else
                                                    st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                                                    st_PayInfo.szErrDesc = "> Unsupported gateway field value tag(" + szJsonField + ") in Config Template for " + st_PayInfo.szIssuingBank

                                                    GoTo CleanUp
                                                End If
                                        End Select
                                    End If
                                    iJsonElement = iJsonElement + 1
                                Next
                                'ADD E N D SUKI 20190320 [eNets integration]
                            End If  'If ("[3des]" = aszValue(0).Trim().ToLower()) Then
                        End If  'If ("*" = Left(szFormat, 1)) Then

                        'Cater for hash value verification included in the encrypted reply above.
                        'Verification of field value based on format specified
                        'Example: Hashing    : #[HashMethod]![ReturnKey]![-]!RespCode![-]!InvID![-]!InvRef![-]!InvCy![-]!InvTotal![-]!InvTest![-]!MerchRef#
                        'Example: Fixed Value: [=5]
                        If (szFormat <> "") Then
                            If ("#" = Left(szFormat, 1)) Then
                                'Added on 25 Oct 2009, to cater for comparison of this value with the hash result.
                                'e.g. "#=0=[SendKey]..." becoz hash result from szComputeHash() may not be necessary hash value,
                                'it could be hash verification result. So, this cater for comparing whether the hash verification
                                'result is equal to the value specified here.
                                'Example: checkvalue|[HashValue]{#=0=[=CHECK]![^]!merid![^]!orderno![^]!amount![^]!currencycode![^]!transdate![^]!transtype![^]!status![^]!checkvalue#}
                                szEqual = ""
                                If ("=" = Mid(szFormat, 2, 1)) Then
                                    iStart = szFormat.IndexOf("=", 0) 'IndexOf is zero-based
                                    iEnd = szFormat.IndexOf("=", 2)
                                    szEqual = szFormat.Substring(iStart + 1, iEnd - iStart - 1)
                                End If

                                szValue = szFormat.Replace("#", "")

                                'Added on 30 Oct 2009
                                If (szEqual <> "") Then
                                    szValue = szValue.Replace("=" + szEqual + "=", "")
                                End If

                                'Example1: #[HashMethod]:BillRef1:[]:Amount:[]:Currency:[]:ConfNum#}
                                'Example2: #[HashMethod]![ReturnKey]![-]!RespCode![-]!InvID![-]!InvRef![-]!InvCy![-]!InvTotal![-]!InvTest![-]!MerchRef#}
                                If (InStr(szValue, "!") > 0) Then
                                    aszValue = Split(szValue, "!")
                                Else
                                    'Cater for decrypted reply message contains Hash value. Normally this will not be
                                    'configured in message template because once the reply message can be successfully
                                    'decrypted, it is secure enough to trust the reply message. Further verification for
                                    'hash value is unnecessary.
                                    aszValue = Split(szValue, ":")
                                End If

                                szValue = ""
                                szHashValue = ""
                                szHashMethod = ""
                                ' Hash Method
                                If ("[hashmethod]" = aszValue(0).Trim().ToLower()) Then
                                    szHashMethod = st_HostInfo.szHashMethod
                                ElseIf ("[=" = Left(aszValue(0).Trim().ToLower(), 2)) Then 'Added, 24 Jul 2011, for Mandiri, to cater for self defined Hash Method
                                    szHashMethod = Mid(aszValue(0).Trim(), 3, aszValue(0).Trim().Length - 3)
                                Else
                                    szHashMethod = Server.UrlDecode(HttpContext.Current.Request(aszValue(0).Trim()))
                                End If

                                bReturnKeyDecrypt = False   'Added, 26 Apr 2011

                                For iCounter = 1 To aszValue.GetUpperBound(0)
                                    If (0 <> (iCounter Mod 2)) Then ' Field
                                        'Added on 16 June 2009, to get MerchantID, CurrencyCode, HostID in order to further bGetTerminal to get ReturnKey
                                        'Added [returnkeypath] on 9 Nov 2009
                                        'Added  4 Apr 2014. Affin bank [hostpassword3], combined  17 Sept 2014
                                        If (("[returnkey]" = aszValue(iCounter).Trim().ToLower()) Or ("[returnkeyhex]" = aszValue(iCounter).Trim().ToLower()) Or ("[returnkeypath]" = aszValue(iCounter).Trim().ToLower()) Or ("[hostpassword3]" = aszValue(iCounter).Trim().ToLower())) Then
                                            If ("" <> st_PayInfo.szTxnID) Then
                                                'NOTE: The following config template is NOT YET SUPPORTED!!!!!!
                                                'GatewayTxnID is not encrypted in response, only certain fields are encrypted using 3DES, e.g. ConfNum and Amount
                                                '*** Must put GatewayTxnID before/on the left of other encrypted fields ***
                                                'e.g. BankRef1|[GatewayTxnID]|ConfNum|[Reserved]{*[3DES]!!![BankRefNo]*}|Amount|[Reserved]{*[3DES]!!![TxnAmount]*}
                                                'bGetReqTxn() will be called again in bVerifyResTxn()
                                                stPayInfo.szTxnID = st_PayInfo.szTxnID

                                                'Modified 25 Nov 2013, use objCommon instead of objTxnProc, to cater for different DB connection by Payment Method
                                                If (True <> objCommon.bGetReqTxn(stPayInfo, st_HostInfo)) Then
                                                    'Commented checking of HostsListTemplate,  8 Apr 2014, for Alliance CC
                                                    'Added checking of HostsListTemplate for DDGW Aggregator on 3 Apr 2010 to support duplicate Host response
                                                    'If (ConfigurationManager.AppSettings("HostsListTemplate") <> "") Then

                                                    'Else
                                                    '    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
                                                    '                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
                                                    '                    stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bGetData() " + stPayInfo.szErrDesc)

                                                    '    Return False
                                                    'End If
                                                    'Modified  8 Apr 2014, moved from above to here
                                                    If (-1 = stPayInfo.iQueryStatus) Then
                                                        'Modified 25 Nov 2013, use objCommon instead of objTxnProc, to cater for different DB connection by Payment Method
                                                        If (True <> objCommon.bGetResTxn(stPayInfo, st_HostInfo)) Then
                                                            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                                            stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bGetData() " + stPayInfo.szErrDesc)

                                                            Return False
                                                        End If
                                                    Else
                                                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                                        stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bGetData() " + stPayInfo.szErrDesc)
                                                        Return False
                                                    End If
                                                End If
                                            Else
                                                'For bank response which the whole content is encrypted, no GatewayTxnID is available like the above,
                                                'DDGW will need to retrieve the respective merchant's SecretKey to decrypt the payment response.
                                                'Since the whole content is encrypted, GW has no way to identify to which merchant that the response belongs and therefore
                                                'the respective MerchantID identifying merchant, IssuingBank and CurrencyCode need to be
                                                'defined in each response_[IssuingBank]_[MerchantID].aspx and pass to this bGetData().
                                                '0|[Reserved]{*[3DES]!&!BillRef1![GatewayTxnID]!Amount![TxnAmount]*}
                                                stPayInfo.szMerchantID = st_PayInfo.szMerchantID
                                                stPayInfo.szCurrencyCode = st_PayInfo.szCurrencyCode
                                            End If

                                            'Added on 16 June 2009
                                            'Get Terminal and Channel information - MID, TID, SendKey, ReturnKey, SecretKey, InitVector, PayeeCode, PayURL, QueryURL, CfgFile, ReturnURL
                                            'Modified 25 Nov 2013, use objCommon instead of objTxnProc, to cater for different DB connection by Payment Method
                                            'Modified 19 Apr 2015, added checking of OTCSecureCode
                                            If ("" = st_PayInfo.szOTCSecureCode) Then
                                                If True <> objCommon.bGetTerminal(st_HostInfo, stPayInfo) Then
                                                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                                    stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bGetData() " + stPayInfo.szErrDesc)
                                                    Return False
                                                End If
                                            End If

                                            'Added, 17 Apr 2014, to cater for routing by Param1
                                            'Format: Product Code|Bank MID
                                            If ("CC" = st_PayInfo.szPymtMethod) Then
                                                If True <> objTxnProc.bGetMerchant(stPayInfo) Then
                                                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                                    stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bGetData() " + stPayInfo.szErrDesc)
                                                    Return False
                                                End If

                                                If (1 = stPayInfo.iRouteByParam1) Then
                                                    If True <> objTxnProc.bGetTxnParam(stPayInfo) Then
                                                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                                        stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bGetData() " + stPayInfo.szErrDesc)
                                                        Return False
                                                    End If

                                                    If (stPayInfo.szParam1 <> "") Then
                                                        Dim aszMID() As String

                                                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                        stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + "> Route by Param1: " + stPayInfo.szParam1)

                                                        aszMID = Split(stPayInfo.szParam1, "|")

                                                        If (aszMID.GetUpperBound(0) >= 1) Then
                                                            st_HostInfo.szMID = aszMID(1)

                                                            'Get terminal info by Bank MID
                                                            If True <> objTxnProc.bGetTerminalEx(st_HostInfo, stPayInfo) Then
                                                                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                                                stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + "> Error: " + stPayInfo.szErrDesc)
                                                                Return False
                                                            End If
                                                        End If
                                                    End If
                                                End If
                                            End If
                                        End If

                                        Select Case aszValue(iCounter).Trim().ToLower()
                                            Case "[returnkey]"
                                                'Modified on 25 May 2009
                                                'NOTE: Use szDecrypt3DES instead of szDecrypt3DEStoHex because original encrypted value(Hash Key) is not in hexadecimal value
                                                'Added, 26 Apr 2011, to avoid decrypt st_HostInfo.szReturnKey(storing clear Return Key) during [RIGHT] of the string and get bad data error exception coz Return Key had been decrypted on [LEFT] side of string and stored in st_HostInfo.szReturnKey(storing clear Return Key)
                                                If (False = bReturnKeyDecrypt) Then
                                                    szValue = objHash.szDecrypt3DES(st_HostInfo.szReturnKey, Common.C_3DESAPPKEY, Common.C_3DESAPPVECTOR)
                                                    st_HostInfo.szReturnKey = szValue
                                                    bReturnKeyDecrypt = True
                                                Else
                                                    '2nd time processing of [returnkey]
                                                    szValue = st_HostInfo.szReturnKey  'This to assign the clear Return Key to szValue/ to overwrite szValue's previous value
                                                End If

                                            Case "[returnkeyhex]"
                                                'Added on 3 Jun 2009
                                                'NOTE: Use szDecrypt3DEStoHex instead of szDecrypt3DES because original encrypted value(Hash Key) is in hexadecimal value
                                                'Added, 26 Apr 2011, to avoid decrypt st_HostInfo.szReturnKey(storing clear Return Key) during [RIGHT] of the string and get bad data error exception coz Return Key had been decrypted on [LEFT] side of string and stored in st_HostInfo.szReturnKey(storing clear Return Key)
                                                If (False = bReturnKeyDecrypt) Then
                                                    szValue = objHash.szDecrypt3DEStoHex(st_HostInfo.szReturnKey, Common.C_3DESAPPKEY, Common.C_3DESAPPVECTOR)
                                                    st_HostInfo.szReturnKey = szValue
                                                    bReturnKeyDecrypt = True
                                                Else
                                                    '2nd time processing of [returnkeyhex]
                                                    szValue = st_HostInfo.szReturnKey  'This to assign the clear Return Key to szValue/ to overwrite szValue's previous value
                                                End If

                                            Case "[hostpassword3]"      'Added  4 Apr 2014. Affin bank - the merchant key is from us to Affin and is under host table instead of terminal table.
                                                szValue = objHash.szDecrypt3DES(st_HostInfo.szSecretKey, Common.C_3DESAPPKEY, Common.C_3DESAPPVECTOR)

                                            Case "[gatewaykey]" 'Added, 24 Jul 2011, for Mandiri, cater for DDGW's Password for hashing for DDGW's response redirect from payment.aspx to response_xx.aspx
                                                szValue = Common.C_3DESAPPKEY

                                                'Added the following on 6 Oct 2009
                                            Case "[mid]"
                                                szValue = st_HostInfo.szMID
                                            Case "[acquirerid]"
                                                szValue = st_HostInfo.szAcquirerID
                                            Case "[tid]"
                                                szValue = st_HostInfo.szTID
                                            Case "[payeecode]"
                                                szValue = st_HostInfo.szPayeeCode   'Added  21 Nov 2013, CashU response hash
                                            Case "[bankrefno]"
                                                szValue = st_PayInfo.szHostTxnID    'Added  21 Nov 2013, CashU response hash
                                            Case "[gatewaytxnid]"
                                                szValue = stPayInfo.szTxnID
                                            Case "[merchanttxnid]"          'Added  4 Apr 2014. Affin bank
                                                szValue = stPayInfo.szMerchantTxnID
                                            Case "[gatewayreturnurl2]"      'Added  4 Apr 2014. Affin bank
                                                szValue = st_HostInfo.szReturnURL2.ToLower()
                                            Case "[txnamount]"
                                                szValue = stPayInfo.szTxnAmount
                                            Case "[txnamount_n2]"   'Request txn amount in 2 decimals, plus N2 format "###,###,###.##". On 24 Jun 2010, modified CDbl to Convert.ToDecimal becoz CDbl will round up the value.
                                                szValue = String.Format("{0:N2}", Convert.ToDecimal(stPayInfo.szTxnAmount))
                                            Case "[txnamount_2]"
                                                szValue = stPayInfo.szTxnAmount.Replace(".", "")
                                            Case "[txnamount_02]"   'Added on 25 Oct 2009. 12 digits, leading zeroes, no decimal point, last 2 are decimal values
                                                szValue = stPayInfo.szTxnAmount.Replace(".", "")
                                                szValue = "".PadLeft(12 - szValue.Length(), "0") + szValue
                                            Case "[basetxnamount]"
                                                szValue = stPayInfo.szBaseTxnAmount
                                            Case "[basetxnamount_2]"
                                                szValue = stPayInfo.szBaseTxnAmount.Replace(".", "")
                                            Case "[basetxnamount_02]"   'Added on 25 Oct 2009. 12 digits, leading zeroes, no decimal point, last 2 are decimal values
                                                szValue = stPayInfo.szBaseTxnAmount.Replace(".", "")
                                                szValue = "".PadLeft(12 - szValue.Length(), "0") + szValue
                                            Case "[currencycode]"
                                                szValue = stPayInfo.szCurrencyCode
                                            Case "[basecurrencycode]"
                                                szValue = stPayInfo.szBaseCurrencyCode
                                            Case "[txnstatus]"      'Added on 8 Mar 2010
                                                szValue = st_PayInfo.szHostTxnStatus
                                            Case "[respcode]"
                                                szValue = st_PayInfo.szRespCode
                                            Case "[3dsxid]"         'Added, 2 Oct 2015, for Metrobank
                                                szValue = st_PayInfo.sz3dsXID
                                            Case "[authcode]"       'Added  20 Aug 2015. PBB
                                                szValue = st_PayInfo.szAuthCode
                                            Case "[returnkeypath]"  'Added  MCash
                                                szValue = stPayInfo.szDecryptKeyPath
                                                'ADD START SUKI 20190320 [eNets integration]
                                            Case "[reserved]"
                                                szValue = st_HostInfo.szReserved
                                                'ADD E N D SUKI 20190320 [eNets integration]
                                            Case Else
                                                If ("0" = szConfig(3).Trim()) Then
                                                    'Can't just use HttpContext.Current.Request("FieldName") because will get the encrypted value only
                                                    'Encrypted string behind "?" in the query string, e.g. "?ABCD"
                                                    For iField = 0 To szFieldName.GetUpperBound(0)
                                                        If (aszValue(iCounter).Trim().ToLower() = szFieldName(iField).ToLower()) Then
                                                            szValue = szFieldValue(iField)
                                                            bFound = True
                                                            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                                            " Field(" + szFieldName(iField) + ") Value(" + szFieldValue(iField) + ")")
                                                            Exit For
                                                        End If
                                                    Next

                                                    If (False = bFound) Then
                                                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                                        " Field(" + aszValue(iCounter).Trim() + ") not found from Host reply")
                                                        szValue = ""
                                                    End If
                                                Else
                                                    'Added checking of aszValue for "{" and "[=" to support formatting for szValue, 6 Oct 2009
                                                    If (("{" <> Left(aszValue(iCounter), 1)) And ("[=" <> Left(aszValue(iCounter), 2))) Then
                                                        '07 Sept 2009 - Removed Trim to allow spaces in field's value in order to avoid hash value mismatched
                                                        'Commented  11 Nov 2013, HSBC Credit, replaced UrlDecode to HtmlDecode because UrlDecode is not able to decode space " " to "+"
                                                        'szValue = Server.UrlDecode(HttpContext.Current.Request(aszValue(iCounter)))
                                                        szValue = HttpUtility.HtmlDecode(HttpContext.Current.Request(aszValue(iCounter)))
                                                        'Added  29th Nov 2017, Boost, Get JSON value
                                                        If ("JSON" = szConfig(2).Trim()) Then
                                                            szValue = szGetJsonTagValue(st_PayInfo.szRespContent, aszValue(iCounter))

                                                        End If
                                                    Else
                                                        If ("[=" = Left(aszValue(iCounter).Trim(), 2)) Then
                                                            szValue = Mid(aszValue(iCounter).Trim(), 3, aszValue(iCounter).Trim().Length - 3)

                                                        ElseIf ("{" = Left(aszValue(iCounter).Trim(), 1)) Then   ' "{" indicates format
                                                            If ("{iso3166}[currencycode]" = Left(aszValue(iCounter).Trim().ToLower(), 23)) Then
                                                                szValue = objTxnProc.bGetCurrency(stPayInfo.szCurrencyCode, stPayInfo)

                                                            ElseIf ("{iso3166}[basecurrencycode]" = Left(aszValue(iCounter).Trim().ToLower(), 27)) Then
                                                                szValue = objTxnProc.bGetCurrency(stPayInfo.szBaseCurrencyCode, stPayInfo)

                                                            ElseIf ("{ISO3166" = Left(aszValue(iCounter).Trim().ToUpper(), 8)) Then
                                                                'Maps currency from ISO4217 (3 alpha characters) to ISO3166 (3 numeric digits) format
                                                                szValue = objTxnProc.bGetCurrency(stPayInfo.szCurrencyCode, stPayInfo)

                                                            ElseIf ("{LOW@" = Left(aszValue(iCounter).Trim().ToUpper(), 5)) Then 'Added  22 Nov 2013, for CashU, require lower case for Hash Value
                                                                szValue = aszValue(iCounter).Trim().Replace("{", "").Replace("}", "")
                                                                aszSplit = Split(szValue, "@")

                                                                Select Case aszSplit(aszSplit.GetUpperBound(0)).ToLower()
                                                                    Case "[currencycode]"
                                                                        szValue = stPayInfo.szCurrencyCode.ToLower()
                                                                    Case "[gatewaytxnid]"
                                                                        szValue = stPayInfo.szTxnID.ToLower()
                                                                    Case "[payeecode]"
                                                                        szValue = st_HostInfo.szPayeeCode.ToLower()
                                                                    Case "[returnkey]"
                                                                        szValue = objHash.szDecrypt3DES(st_HostInfo.szReturnKey, Common.C_3DESAPPKEY, Common.C_3DESAPPVECTOR).ToLower()
                                                                End Select

                                                            ElseIf ("{decode," = Left(aszValue(iCounter).Trim().ToLower(), 8)) Then  'Added 15 Sept 2015, for AllDebit
                                                                'e.g. {DECODE},[BankRefNo] OR {DECODE},pp_refCode                    'Modified 18 Sept 2015, "{decode}," to "{decode," coz "}" already being removed above
                                                                aszDecode = Split(aszValue(iCounter).Trim(), ",")
                                                                Select Case aszDecode(1).ToLower()
                                                                    Case "[bankrefno]"
                                                                        szValue = objHash.szDecode(stPayInfo.szHostTxnID)
                                                                    Case Else
                                                                        szValue = objHash.szDecode(HttpUtility.HtmlDecode(HttpContext.Current.Request(aszDecode(1))))
                                                                End Select
                                                            ElseIf ("{JSONORITAG@" = Left(aszValue(iCounter).Trim().ToUpper(), 12)) Then 'Added  4 Dec 2017, To get raw string from JSON ISO8601 format date time
                                                                szValue = aszValue(iCounter).Trim().Replace("{", "").Replace("}", "")
                                                                aszSplit = Split(szValue, "@")
                                                                If (aszSplit.Length > 1) Then
                                                                    Dim settings = New JsonSerializerSettings With {.DateParseHandling = DateParseHandling.None}
                                                                    Dim objJ As JObject
                                                                    objJ = JsonConvert.DeserializeObject(Of JObject)(st_PayInfo.szRespContent, settings)
                                                                    szValue = objJ(aszSplit(1))
                                                                End If
                                                            Else
                                                                'szValue = System.DateTime.Now.ToString(aszValue(iCounter).Trim().Replace("{", "").Replace("}", ""))
                                                                szValue = Convert.ToDateTime(stPayInfo.szTxnDateTime).ToString(aszValue(iCounter).Trim().Replace("{", "").Replace("}", ""))
                                                            End If
                                                        End If
                                                    End If
                                                End If
                                        End Select

                                        szHashValue = szHashValue + szValue
                                        'Test 8 Mar 2010
                                        'objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                        '                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                        '                " TESTING-HashValue-(" + szHashValue + ")")
                                    Else
                                        ' Delimiter
                                        'Commented the following on 4 Mar 2010
                                        'szHashValue = szHashValue + aszValue(iCounter).Trim().Replace("[", "").Replace("]", "")

                                        'Added szDelimiterEx and Replace "PIPE" by "|" on 4 Mar 2010
                                        'Added checking of Instr ("PIPE") on 2 Apr 2010
                                        If (InStr(aszValue(iCounter).Trim().ToUpper(), "PIPE") > 0) Then
                                            szDelimiterEx = aszValue(iCounter).Trim().ToUpper().Replace("[", "").Replace("]", "").Replace("PIPE", "|")
                                        Else
                                            szDelimiterEx = aszValue(iCounter).Trim().Replace("[", "").Replace("]", "")
                                        End If
                                        szHashValue = szHashValue + szDelimiterEx

                                        'Test 8 Mar 2010
                                        'objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
                                        '                DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
                                        '                " szHashValue(" + szHashValue + ")")
                                    End If
                                Next

                                'Test 8 Mar 2010
                                'objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                '                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                '                " TESTING - szHashValue(" + szHashValue + ")-" + szHashMethod)

                                szHashValue = objHash.szGetHash(szHashValue, szHashMethod, stPayInfo, st_HostInfo) 'Added on 15 Dec 2010 , add 'stHostInfo' for uobThai to get secret key for encryption

                                'Verify response Hash Value with Hash Value calculated above by Gateway
                                'Added case-insensitive hash value comparison, 12 May 2009
                                'Added checking of st_HostInfo.szHashValue, 6 Oct 2009
                                If ("" <> st_HostInfo.szHashValue) Then
                                    'Added szEqual checking on 25 Oct 2009
                                    If (szEqual <> "") Then
                                        'Compares szEqual with szHashValue returned by objHash.szGetHash() function
                                        If (szEqual.ToLower() <> szHashValue.ToLower()) Then
                                            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                                                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                            " > bGetData() Response Hash Value mismatched - Defined Value(" + szEqual + ") Gateway(" + szHashValue + ") Hash Method(" + szHashMethod + ")")
                                            GoTo CleanUp
                                        End If
                                    Else
                                        If (szHashValue.ToLower() <> st_HostInfo.szHashValue.ToLower()) Then
                                            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                                                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                            " > bGetData() Response Hash Value mismatched - Host(" + st_HostInfo.szHashValue + ") Gateway(" + szHashValue + ") Hash Method(" + szHashMethod + ")")
                                            GoTo CleanUp
                                        End If
                                    End If
                                Else
                                    'Added, 28 Nov 2013
                                    'Modified  1 Sept 2015, PBB, added checking of IsHashOptional
                                    If (False = st_HostInfo.bIsHashOptional) Then
                                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                        " > bGetData() Expected Response Hash Value EMPTY, discard response")

                                        GoTo CleanUp
                                    Else
                                        'Added  1 Sept 2015. PBB. Query will be sent via bProcessSaleRes->InitResponse
                                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                        " > bGetData() Expected Response Hash Value EMPTY, proceed to Query")
                                    End If
                                End If

                            ElseIf ("[=" = Left(szFormat, 2)) Then
                                If (szValue <> szFormat.Replace("[=", "").Replace("]", "")) Then
                                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                    " > bGetData() Response Value mismatched with defined value - Host(" + szValue + ") Defined(" + szFormat.Replace("[=", "").Replace("]", "") + ")")
                                    GoTo CleanUp
                                End If

                                ' Added on 21 Apr 2009 to cater for verification of additional fields, especially for RespCode,
                                ' in order to further determine whether the transaction is approved on top of the successful RespCode
                                ' e.g. BilltoBill: RespCode|[respcode]{^InvTotal![=]!InvPaid^}
                                ' e.g. FPX       : RespCode|[respcode]{^DebitStatus![=]![=0]![&]!CreditStatus![=]![=0]^}
                            ElseIf ("^" = Left(szFormat, 1)) Then
                                szValue = szFormat.Replace("^", "")
                                aszValue = Split(szValue, "!")

                                szField = ""
                                szOperator = ""
                                szValue = ""
                                szInterOperator = ""

                                iNextCounter = 0
                                bLastVerifiedDone = False
                                bLastVerified = False
                                bVerified = False

                                '                    Field Operator 
                                ' e.g. BilltoBill: InvTotal![=]!InvPaid
                                '                       0    1    2
                                ' e.g. FPX       : DebitStatus![=]![=0]![&]!CreditStatus![=]![=0]
                                '                       0       1    2   3         4      5    6
                                For iCounter = 0 To aszValue.GetUpperBound(0)
                                    If (0 = (iCounter Mod 4)) Then                  ' Field
                                        szField = Server.UrlDecode(HttpContext.Current.Request(aszValue(iCounter).Trim()))
                                        iNextCounter = iCounter + 1
                                    Else
                                        If (iCounter = iNextCounter) Then           ' Operator
                                            szOperator = aszValue(iCounter).Trim().Replace("[", "").Replace("]", "")
                                            iNextCounter = iCounter + 2

                                        ElseIf (iCounter = iNextCounter - 1) Then   ' Value - Defined fixed value OR Query String value
                                            If ("[=" = Left(aszValue(iCounter).Trim(), 2)) Then
                                                szValue = aszValue(iCounter).Trim().Replace("[=", "").Replace("]", "")
                                            Else
                                                szValue = Server.UrlDecode(HttpContext.Current.Request(aszValue(iCounter).Trim()))
                                            End If

                                            iNextCounter = iCounter + 3

                                            ' Verify the first set of expression (A=B), (A<>B)
                                            Select Case szOperator.ToLower()
                                                Case "="
                                                    If (szField.ToLower() = szValue.ToLower()) Then
                                                        If (bLastVerifiedDone = False) Then
                                                            bLastVerified = True
                                                            bLastVerifiedDone = True
                                                        Else
                                                            bVerified = True
                                                            bLastVerifiedDone = False
                                                        End If
                                                    Else
                                                        If (bLastVerifiedDone = False) Then
                                                            bLastVerified = False
                                                            bLastVerifiedDone = True
                                                        Else
                                                            bVerified = False
                                                            bLastVerifiedDone = False
                                                        End If
                                                    End If

                                                Case "<>"
                                                    If (szField.ToLower() <> szValue.ToLower()) Then
                                                        If (bLastVerifiedDone = False) Then
                                                            bLastVerified = True
                                                            bLastVerifiedDone = True
                                                        Else
                                                            bVerified = True
                                                            bLastVerifiedDone = False
                                                        End If
                                                    Else
                                                        If (bLastVerifiedDone = False) Then
                                                            bLastVerified = False
                                                            bLastVerifiedDone = True
                                                        Else
                                                            bVerified = False
                                                            bLastVerifiedDone = False
                                                        End If
                                                    End If

                                                Case Else
                                                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                                                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                                    " > bGetData() Unsupported verification format(" + szFormat + ")")
                                                    GoTo CleanUp
                                            End Select

                                        ElseIf (iCounter = iNextCounter - 2) Then   ' Inter Operator
                                            szInterOperator = aszValue(iCounter).Trim().Replace("[", "").Replace("]", "")
                                        End If
                                    End If
                                Next

                                st_PayInfo.bVerified = bLastVerified

                                'Added on 24 Dec 2010  for UOB Thai
                                'Decryption + Fixed length format
                                'Example: PAY|RES|GET|boamsg|[reserved]{%OrderNo@21@6!TxnDate@27@8!TxnTime@35@6!txnamount_02@64@12!RespCode@76@2!CustRef1@84@19!Tid@189@8!AutCode@109@16%}
                            ElseIf ("%" = Left(szFormat, 1)) Then

                                Dim iNum As Integer
                                Dim aszFieldSubStr() As String
                                Dim szTemp As String

                                If ("%*" = Left(szFormat, 2)) Then 'Added  26 Aug 2014, Sync with old production code, UOBTH
                                    If ("" = st_HostInfo.szSecretKey) Then 'If not Host based then only run bGetTerminal to get the secret key 'Added  26 Aug 2014, Sync with old production code, UOBTH
                                        If True <> objCommon.bGetTerminal(st_HostInfo, st_PayInfo) Then
                                            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                            stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bGetData() " + stPayInfo.szErrDesc)
                                            Return False
                                        End If
                                    End If

                                    szTemp = "decrypt^" + st_HostInfo.szReserved
                                    'Decrypted string szTemp is fixed length format
                                    szTemp = objHash.szGetHash(szTemp, st_HostInfo.szHashMethod, st_PayInfo, st_HostInfo)
                                Else
                                    szTemp = st_HostInfo.szReserved 'Added, 14 Dec 2012 'Added  26 Aug 2014, Sync with old production code, to Setup UOBTH in SG
                                End If

                                '%Mid@9@10!OrderNo@21@6!TxnDate@27@8!TxnTime@35@6!txnamount_02@64@12!RespCode@76@2!CustRef1@84@19!Tid@189@8!AutCode@109@16%
                                szFormat = szFormat.Replace("%", "")
                                aszValue = Split(szFormat, "!")

                                'For each response message field
                                For iNum = 0 To aszValue.GetUpperBound(0)
                                    aszFieldSubStr = Split(aszValue(iNum), "@")
                                    szValue = szTemp.Substring(aszFieldSubStr(1), aszFieldSubStr(2))

                                    Select Case aszFieldSubStr(0).ToLower()
                                        Case "gatewaytxnid"
                                            st_PayInfo.szTxnID = szValue
                                        Case "txnamount_02"
                                            If ("" <> szValue) Then
                                                st_PayInfo.szTxnAmount = CStr(Convert.ToDecimal(Left(szValue, szValue.Length - 2) + "." + Right(szValue, 2)))
                                            End If
                                        Case "respcode"
                                            st_PayInfo.szRespCode = szValue
                                        Case "authcode"
                                            st_PayInfo.szAuthCode = szValue
                                        Case "mid"
                                            st_PayInfo.szHostMID = szValue
                                        Case "orderno"
                                            st_PayInfo.szMerchantOrdID = szValue
                                        Case "txndate"
                                            st_PayInfo.szHostDate = szValue
                                        Case "txntime"
                                            st_PayInfo.szHostTime = szValue
                                        Case "tid"
                                            st_PayInfo.szHostTID = szValue
                                        Case "eci"                      'Added  18 Aug 2015. PBB
                                            st_PayInfo.szECI = szValue
                                        Case Else
                                            'Unsupported value tag
                                            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                                            st_PayInfo.szErrDesc = " > bGetData() Unsupported field tag(" + aszFieldSubStr(0) + ") in " + st_PayInfo.szIssuingBank + " message template"
                                            GoTo CleanUp
                                    End Select

                                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                    " > Field(" + aszFieldSubStr(0) + ") Value(" + szValue + ")")
                                Next iNum

                                'Added  26 Aug 2014, Sync with old production code, UOBTH
                                'Commented   1 Sept 2015, not used
                                'If ("" = st_PayInfo.szMerchantID) Then
                                '    If True <> objCommon.bGetMerchantIDbyTxnID(st_PayInfo) Then
                                '        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
                                '                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
                                '                            stPayInfo.szMerchantTxnID + " > bGetData() " + stPayInfo.szErrDesc)
                                '        'Return False
                                '    End If
                                'End If

                                'Added, 23 Jul 2011, for Maybank Firefly, to cater for all response data returned from bank into 1 response field, delimited by a character.
                                'Example of response message: 2011-07-22T17:00:44.000 08&status=00&transAmount=2500&referenceNo=I92IRD&payeeCode=1VG&approvalCode=&bankRefNo=M2U03520342844400643&TransStatus=00&accountNo=F01I92IRD17362217350&corpName=FIREFLY
                                'Example of response format : [Reserved]{\&=!AccountNo![GatewayTxnID]!Status![RespCode]!TransAmount![TxnAmount_2]!BankRefNo![BankRefNo]\}
                            ElseIf ("\" = Left(szFormat, 1)) Then
                                Dim szFieldSets() As String
                                Dim szFieldSet
                                Dim szFieldEx() As String

                                szValue = szFormat.Replace("\", "") '&=!RefNo![RespCode]!Amount![TxnAmount]
                                aszValue = Split(szValue, "!")      'aszValue(0)="&=" aszValue(1)="AccountNo" aszValue(2)="[GatewayTxnID]"
                                aszValue(0) = aszValue(0).ToUpper.Replace("[PIPE]", "|") 'aszValue(0)="|="

                                'Splits the whole reply message stored in Reserved field by field delimiter Left(aszValue(0),1)
                                szFieldSets = Split(st_HostInfo.szReserved, Left(aszValue(0), 1)) 'szFieldSets(0)="2011-06-07T10:11:02.000 08" szFieldSets(1)="status=00"

                                'For each field set(field name=field value pairs), replace the right most field value "=" to "@" if field delimiter is "="
                                'Add dummy field name to cater for field set which does not contain field name with "=" and only has field value
                                'Remove vbCrLf, vbCr, vbLf from each field set
                                'Gets total number of pairs of field name and field value
                                iCounter = 1
                                iField = 0
                                For Each szFieldSet In szFieldSets
                                    'Cater for the presence of rightmost "==" or "=", normally for encrypted or hash value
                                    'Replace the last two "==" or last "=" character(s) with "@@" or "@" accordingly to avoid inaccurate
                                    'array set from the result of split by "="

                                    szFieldSet = Trim(Replace(Replace(Replace(szFieldSet, vbCrLf, ""), vbCr, ""), vbLf, ""))

                                    'If field delimiter is "=", then only replace the right most field value "=" to "@"
                                    If ("=" = Right(aszValue(0), 1)) Then
                                        If ("==" = Right(szFieldSet, 2)) Then
                                            'Differentiate between "field==" from "field=h==" 
                                            'Replace "field==" to "field=@" and "field=h==" to "field=h@@"
                                            If ((szFieldSet.IndexOf("=") + 1) = (szFieldSet.Length() - 1)) Then
                                                szFieldSet = Left(szFieldSet, Len(szFieldSet) - 1) + "@"
                                            Else
                                                szFieldSet = Left(szFieldSet, Len(szFieldSet) - 2) + "@@"
                                            End If
                                        ElseIf ("=" = Right(szFieldSet, 1)) Then
                                            'Differentiate between "field=" (empty value) from "field=value="
                                            'Checks if "=" is the character at the last position of the field value OR, e.g. "field=value="
                                            'if "=" is the field delimiter, e.g. "field="
                                            'Make sure "=" does not exist at the last character of szFieldSet or else
                                            'the array result from the next Split by "=" will encounter out of bound exception
                                            If (szFieldSet.IndexOf("=") <> szFieldSet.Length() - 1) Then
                                                szFieldSet = Left(szFieldSet, Len(szFieldSet) - 1) + "@"
                                            End If
                                        End If
                                    End If

                                    'Added, 23 Jul 2011, add dummy field name to cater for field set which does not contain field name with "=" and only has field value
                                    If (szFieldSet.IndexOf(Right(aszValue(0), 1)) < 0) Then
                                        szFieldSet = "Field" + iCounter.ToString() + Right(aszValue(0), 1) + szFieldSet    'Field1=2011-07-22T17:00:44.000 08
                                        iCounter = iCounter + 1
                                    End If

                                    'Gets total number of pairs of field name and field value
                                    szFieldEx = Split(szFieldSet, Right(aszValue(0), 1))
                                    If (1 = szFieldEx.GetUpperBound(0)) Then
                                        iField = iField + 1
                                    End If
                                Next

                                'Populate pairs of field name and field value from response data
                                ReDim szFieldName(iField - 1)
                                ReDim szFieldValue(iField - 1)

                                iCounter = 1
                                iField = 0
                                For Each szFieldSet In szFieldSets
                                    'Cater for the presence of rightmost "==" or "=", normally for encrypted or hash value
                                    'Replace the last two "==" or last "=" character(s) with "@@" or "@" accordingly to avoid inaccurate
                                    'array set from the result of split by "="

                                    'Use LTrim to allow spaces for field's value or else may encounter hash value mismatched
                                    szFieldSet = LTrim(Replace(Replace(Replace(szFieldSet, vbCrLf, ""), vbCr, ""), vbLf, ""))

                                    'If field delimiter is "=", then only replace the right most field value "=" to "@"
                                    If ("=" = Right(aszValue(0), 1)) Then
                                        If ("==" = Right(szFieldSet, 2)) Then
                                            'Differentiate between "field==" from "field=h==" 
                                            'Replace "field==" to "field=@" and "field=h==" to "field=h@@"
                                            If ((szFieldSet.IndexOf("=") + 1) = (szFieldSet.Length() - 1)) Then
                                                szFieldSet = Left(szFieldSet, Len(szFieldSet) - 1) + "@"
                                            Else
                                                szFieldSet = Left(szFieldSet, Len(szFieldSet) - 2) + "@@"
                                            End If
                                        ElseIf ("=" = Right(szFieldSet, 1)) Then
                                            'Differentiate between "field=" (empty value) from "field=value="
                                            'Checks if "=" is the character at the last position of the field value OR, e.g. "field=value="
                                            'if "=" is the field delimiter, e.g. "field="
                                            'Make sure "=" does not exist at the last character of szFieldSet or else
                                            'the array result from the next Split by "=" will encounter out of bound exception
                                            If (szFieldSet.IndexOf("=") <> szFieldSet.Length() - 1) Then
                                                szFieldSet = Left(szFieldSet, Len(szFieldSet) - 1) + "@"
                                            End If
                                        End If
                                    End If

                                    'Added, 23 Jul 2011, add dummy field name to cater for field set which does not contain field name with "=" and only has field value
                                    If (szFieldSet.IndexOf(Right(aszValue(0), 1)) < 0) Then
                                        szFieldSet = "Field" + iCounter.ToString() + Right(aszValue(0), 1) + szFieldSet    'Field1=2011-07-22T17:00:44.000 08
                                        iCounter = iCounter + 1
                                    End If

                                    'Populate pairs of field name and field value
                                    szFieldEx = Split(szFieldSet, Right(aszValue(0), 1))

                                    If (1 = szFieldEx.GetUpperBound(0)) Then    'Got field name and field value for the field set
                                        'Cater for the presence of rightmost "==" or "=", normally for encrypted or hash value
                                        'Replace the last two "@@" or last "@" character(s) back to the original "==" or "=" accordingly
                                        'to get back the original value since no split will be used now
                                        'Do not use Replace because do not want to replace "@" which exists not in the last two or last character(s)
                                        If ("=" = Right(aszValue(0), 1)) Then
                                            If ("@@" = Right(szFieldEx(1), 2)) Then
                                                szFieldEx(1) = Left(szFieldEx(1), Len(szFieldEx(1)) - 2) + "=="
                                            ElseIf ("@" = Right(szFieldEx(1), 1)) Then
                                                szFieldEx(1) = Left(szFieldEx(1), Len(szFieldEx(1)) - 1) + "="
                                            End If
                                        End If

                                        szFieldName(iField) = szFieldEx(0)  'Field name
                                        szFieldValue(iField) = szFieldEx(1) 'Field value
                                        iField = iField + 1
                                    End If
                                Next

                                'Matches each Field Name from message template against Field Names array to get its respective Field Value.
                                'Then, assigns the respective Field Value to the respective Gateway Varialbes as defined in the message template.
                                'Example of response format : [Reserved]{\&=!AccountNo![GatewayTxnID]!Status![TxnStatus]!TransAmount![TxnAmount_2]!BankRefNo![BankRefNo]\}
                                For iCounter = 1 To aszValue.GetUpperBound(0)   'aszValue(0)="&=" aszValue(1)="AccountNo" aszValue(2)="[GatewayTxnID]"
                                    If (0 <> (iCounter Mod 2)) Then
                                        For iField = 0 To szFieldName.GetUpperBound(0)
                                            If (aszValue(iCounter).Trim().ToLower() = szFieldName(iField).ToLower()) Then
                                                szValue = szFieldValue(iField)
                                                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                                " Field(" + szFieldName(iField) + ") Value(" + szFieldValue(iField) + ")")
                                                Exit For
                                            End If
                                        Next
                                    Else
                                        szFormat = ""
                                        szField = aszValue(iCounter).Trim().ToLower()
                                        iIndex = InStr(szField, "{")    'InStr starts with 1
                                        If (iIndex > 0) Then
                                            'Example1: Md5|[HashValue]{#[HashMethod]:BillRef1:[]:Amount:[]:Currency:[]:ConfNum#}
                                            'Example2: Hash|[HashValue]{#[HashMethod]![ReturnKey]![-]!RespCode![-]!InvID![-]!InvRef![-]!InvCy![-]!InvTotal![-]!InvTest![-]!MerchRef#}
                                            szFormat = szField.Substring(iIndex).Replace("}", "")
                                            szField = szField.Substring(0, iIndex - 1)
                                        End If

                                        Select Case szField
                                            Case "[ordernumber]"
                                                st_PayInfo.szMerchantOrdID = szValue
                                            Case "[gatewaytxnid]"
                                                st_PayInfo.szTxnID = szValue
                                            Case "[txnamount]"      'Amount in 2 decimals
                                                st_PayInfo.szTxnAmount = szValue
                                            Case "[txnamount_2]"    'Amount in cents whereby the last two digits are decimal points
                                                If ("" <> szValue) Then
                                                    'Added, 14 Nov 2013, checking of szValue's length to cater for only 1 to 2 digits returned
                                                    If (1 = szValue.Length()) Then
                                                        szValue = "00" + szValue
                                                    ElseIf (2 = szValue.Length()) Then
                                                        szValue = "0" + szValue
                                                    End If
                                                    st_PayInfo.szTxnAmount = Left(szValue, szValue.Length - 2) + "." + Right(szValue, 2)
                                                End If
                                            Case "[txnamtexpn_2]"    'Amount without decimal point but the last two digits are Zero     'Added on 6  Jan 2016, MIGS exponent currency
                                                If ("" <> szValue) Then
                                                    If ("JPY" = st_PayInfo.szCurrencyCode Or "KRW" = st_PayInfo.szCurrencyCode Or "VND" = st_PayInfo.szCurrencyCode Or "IDR" = st_PayInfo.szCurrencyCode) Then
                                                        st_PayInfo.szTxnAmount = szValue + ".00"
                                                    Else
                                                        'Added, 14 Nov 2013, checking of szValue's length to cater for only 1 to 2 digits returned
                                                        If (1 = szValue.Length()) Then
                                                            szValue = "00" + szValue
                                                        ElseIf (2 = szValue.Length()) Then
                                                            szValue = "0" + szValue
                                                        End If
                                                        st_PayInfo.szTxnAmount = Left(szValue, szValue.Length - 2) + "." + Right(szValue, 2)
                                                    End If
                                                End If
                                            Case "[txnamount_02]"   'Added on 25 Oct 2009. 12 digits, leading zeroes, no decimal point, last 2 are decimal values. On 24 Jun 2010, modified CDbl to Convert.ToDecimal becoz CDbl will round up the value.
                                                If ("" <> szValue) Then
                                                    st_PayInfo.szTxnAmount = CStr(Convert.ToDecimal(Left(szValue, szValue.Length - 2) + "." + Right(szValue, 2)))
                                                End If
                                            Case "[bankrefno]"
                                                st_PayInfo.szHostTxnID = szValue
                                            Case "[param1]"
                                                st_PayInfo.szParam1 = szValue
                                            Case "[param2]"
                                                st_PayInfo.szParam2 = szValue
                                            Case "[param3]"
                                                st_PayInfo.szParam3 = szValue
                                            Case "[param4]"
                                                st_PayInfo.szParam4 = szValue
                                            Case "[param5]"
                                                st_PayInfo.szParam5 = szValue
                                            Case "[param6]"
                                                st_PayInfo.szParam6 = szValue
                                            Case "[param7]"
                                                st_PayInfo.szParam7 = szValue
                                            Case "[param8]"
                                                st_PayInfo.szParam8 = szValue
                                            Case "[param9]"
                                                st_PayInfo.szParam9 = szValue
                                            Case "[param10]"
                                                st_PayInfo.szParam10 = szValue
                                            Case "[param11]"
                                                st_PayInfo.szParam11 = szValue
                                            Case "[param12]"
                                                st_PayInfo.szParam12 = szValue
                                            Case "[param13]"
                                                st_PayInfo.szParam13 = szValue
                                            Case "[param14]"
                                                st_PayInfo.szParam14 = szValue
                                            Case "[param15]"
                                                st_PayInfo.szParam15 = szValue
                                            Case "[currencycode]"   'ASCII currency code, e.g. MYR, IDR..
                                                st_PayInfo.szCurrencyCode = szValue
                                            Case "[mid]"
                                                st_PayInfo.szHostMID = szValue
                                            Case "[tid]"
                                                st_PayInfo.szHostTID = szValue
                                            Case "[txnstatus]"
                                                st_PayInfo.szHostTxnStatus = szValue
                                            Case "[respcode]"
                                                st_PayInfo.szRespCode = szValue
                                            Case "[authcode]"
                                                st_PayInfo.szAuthCode = szValue
                                            Case "[respmesg]"
                                                st_PayInfo.szBankRespMsg = szValue
                                            Case "[date]"
                                                st_PayInfo.szHostDate = szValue
                                            Case "[time]"
                                                st_PayInfo.szHostTime = szValue
                                            Case "[cardpan]"
                                                st_PayInfo.szCardPAN = szValue

                                                If (st_PayInfo.szCardPAN.Length > 20) Then
                                                    st_PayInfo.szCardPAN = st_PayInfo.szCardPAN.Substring(0, 20)
                                                End If

                                                If st_PayInfo.szCardPAN.Length > 10 Then
                                                    st_PayInfo.szMaskedCardPAN = st_PayInfo.szCardPAN.Substring(0, 6) + "".PadRight(st_PayInfo.szCardPAN.Length - 6 - 4, "X") + st_PayInfo.szCardPAN.Substring(st_PayInfo.szCardPAN.Length - 4, 4)
                                                Else
                                                    st_PayInfo.szMaskedCardPAN = "".PadRight(st_PayInfo.szCardPAN.Length, "X")
                                                End If
                                            Case "[sessionid]"
                                                st_PayInfo.szMerchantSessionID = szValue            'Modified 16 Jul 2014, SessionID to MerchantSessionID
                                            Case "[hashvalue]"
                                                st_HostInfo.szHashValue = szValue.Replace(" ", "+") 'Added replacement on 3 Apr 2010
                                            Case "[reserved]"
                                                st_HostInfo.szReserved = szValue
                                            Case Else
                                                'Unsupported value tag
                                                st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                                                st_PayInfo.szErrDesc = "> Unsupported gateway field value tag(" + szConfig(iLoop) + ") in Config Template for " + st_PayInfo.szIssuingBank

                                                GoTo CleanUp
                                        End Select

                                        'Sets szValue to empty so that the next Gateway variable will take empty szValue
                                        'if the Field Name defined in message template does not match any Field Name of
                                        'reply message or else the next Gateway variable will take the last szValue which
                                        'is not empty and belongs to the previous Gateway variable.
                                        szValue = ""
                                    End If
                                Next

                            ElseIf ("*" = Left(szFormat, 1)) Then 'Added  23 Dec 2013, Alliance CC; Combined  5 May 2014
                                Dim szHashStr As String = ""
                                Dim aszHash() As String = Nothing
                                Dim szTemp As String = ""
                                Dim iCnt As Integer

                                szValue = szFormat.Replace("*", "")
                                aszValue = Split(szValue, "!")

                                szHashMethod = ""

                                ' Hash Method
                                If ("[hashmethod]" = aszValue(0).Trim().ToLower()) Then
                                    szHashMethod = st_HostInfo.szHashMethod
                                ElseIf ("[=" = Left(aszValue(0).Trim().ToLower(), 2)) Then 'Added, 24 Jul 2011, for Mandiri, to cater for self defined Hash Method
                                    szHashMethod = Mid(aszValue(0).Trim(), 3, aszValue(0).Trim().Length - 3)
                                Else
                                    szHashMethod = Server.UrlDecode(HttpContext.Current.Request(aszValue(0).Trim()))
                                End If

                                If ("" <> st_PayInfo.szTxnID) Then
                                    stPayInfo.szTxnID = st_PayInfo.szTxnID

                                    If (True <> objCommon.bGetReqTxn(stPayInfo, st_HostInfo)) Then
                                        'Commented checking of HostsListTemplate,  5 May 2014, for Alliance CC
                                        'Added checking of HostsListTemplate for DDGW Aggregator on 3 Apr 2010 to support duplicate Host response
                                        'If (ConfigurationManager.AppSettings("HostsListTemplate") <> "") Then

                                        'Else
                                        '    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
                                        '                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
                                        '                    stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bGetData() " + stPayInfo.szErrDesc)

                                        '    Return False
                                        'End If
                                        'Modified 5 May 2014, moved from above to here
                                        If (-1 = stPayInfo.iQueryStatus) Then
                                            'Modified 25 Nov 2013, use objCommon instead of objTxnProc, to cater for different DB connection by Payment Method
                                            If (True <> objCommon.bGetResTxn(stPayInfo, st_HostInfo)) Then
                                                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                                stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bGetData() " + stPayInfo.szErrDesc)

                                                Return False
                                            End If
                                        Else
                                            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                            stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bGetData() " + stPayInfo.szErrDesc)
                                            Return False
                                        End If
                                    End If

                                    If True <> objCommon.bGetTerminal(st_HostInfo, stPayInfo) Then
                                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                        stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bGetData() " + stPayInfo.szErrDesc)
                                        Return False
                                    End If
                                Else
                                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(), " > bGetData() GatewayTxnID is empty to retrive/decrpyt data!")
                                    GoTo CleanUp
                                End If

                                szHashStr = objHash.szGetHash(st_HostInfo.szHashValue, szHashMethod, stPayInfo, st_HostInfo)
                                aszHash = Split(szHashStr, vbLf) 'Split with newline "\n"

                                'szHashStr only value. To match the value back to the exact field, iCnt need to add 2 to match the response hash format with (*) form from template
                                'And need to compare values in st_PayInfo to check is the clear response value is match with the value in hashvalue
                                'Currently only amount and txn status will check - is the response genuine
                                'NOTE: IF RESPONSE CLEAR VALUE NOT MATCH WITH HASH VALUE, DO WE NEED TO JUST REPLACE st_payInfo VALUE WITH THE VALUE THAT GET FROM HASH?
                                iCnt = 2
                                For iCounter = 0 To aszHash.GetUpperBound(0)
                                    Select Case aszValue(iCnt).ToLower()
                                        Case "[gatewaytxnid]"
                                            szTemp = aszHash(iCounter)
                                            If (szTemp <> st_PayInfo.szTxnID) Then
                                                objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(), " > bGetData() response value unmatch with hashing value")
                                                GoTo CleanUp
                                            End If

                                        Case "[txnamount_2]"    'Amount in cents whereby the last two digits are decimal points
                                            If ("" <> aszHash(iCounter)) Then
                                                If (1 = aszHash(iCounter).Length()) Then
                                                    szTemp = "00" + aszHash(iCounter)
                                                ElseIf (2 = aszHash(iCounter).Length()) Then
                                                    szTemp = "0" + aszHash(iCounter)
                                                End If
                                                szTemp = Left(aszHash(iCounter), aszHash(iCounter).Length - 2) + "." + Right(aszHash(iCounter), 2)
                                            End If

                                            If (szTemp <> st_PayInfo.szTxnAmount) Then
                                                objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(), " > bGetData() response value unmatch with hashing value")
                                                GoTo CleanUp
                                            End If

                                        Case "[bankrefno]"
                                            'keep for verification 
                                        Case "[txnstatus]"
                                            szTemp = aszHash(iCounter)
                                            If (szTemp <> st_PayInfo.szHostTxnStatus) Then
                                                objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(), " > bGetData() response value unmatch with hashing value")
                                                GoTo CleanUp
                                            End If

                                        Case "[authcode]"
                                            'keep for verification 
                                    End Select
                                    iCnt += 2
                                Next

                            ElseIf ("MASK" = Left(szFormat, 4)) Then
                                'Masking field value with 'x'
                                'Example: {MASK@R:2@[cvv2]}. Note: The sequance of R(right)/L(left)/M(Middle) detemine the output string format.
                                '         [cvv2] = 123 - In template as {MASK@R:2@[cvv2]}. Result = xx3
                                '         [cvv2] = 789 - In template as {MASK@L:2@[cvv2]}. Result = 7xx
                                '         [param3] = 333344445555666 - In template set as {MASK@M:6:6@[param3]}. Result = 333344xxxxxx666

                                Dim szTemp As String = ""
                                szTemp = szFormat.Replace("{MASK@", "").Replace("}", "")
                                aszValue = Split(szTemp, "@")
                                szValue = objCommon.szMask(aszValue, szValue)
                                'take the last array's value
                                Select Case aszValue(aszValue.Length - 1).ToLower()
                                    'Add case for new fields when needed
                                    Case "[param2]"
                                        st_PayInfo.szParam2 = szValue
                                End Select
                            Else
                                objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                " > bGetData() Unsupported verification format(" + szFormat + ")")
                                GoTo CleanUp
                            End If  'If ("#" = Left(szFormat, 1)) Then

                        End If  'If (szFormat <> "") Then
                    End If  'If (szFormat <> "") Then
                End If  'If (0 <> (iLoop Mod 2)) Then

            Next iLoop

            bReturn = True

        Catch ex As Exception
            st_PayInfo.szErrDesc = "Fail to get response data. Exceptions: " + ex.StackTrace + ex.Message()
            st_PayInfo.szTxnMsg = "System Error: Fail to get response data."

            bReturn = False
        End Try

CleanUp:
        If Not objStreamReader Is Nothing Then
            objStreamReader.Close()         'Added, 5 Sept 2013
            objStreamReader = Nothing
        End If

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		Response
    ' Function Name:	bProcessSaleRes
    ' Function Type:	Boolean.  Return False if error.
    ' Parameter:		
    ' Description:		Process payment response
    ' History:			29 Oct 2008
    '                   Modified 23 Jun 2015, added optional b_VerifyHostIP param
    '********************************************************************************************
    Public Function bProcessSaleRes(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost, ByVal sz_ReplyDateTime As String, ByRef sz_HTML As String, Optional ByVal b_VerifyHostIP As Boolean = False) As Boolean
        Dim bReturn As Boolean = True
        Dim szQueryString As String = ""
        Dim iMesgSet As Integer = -1
        Dim iMesgNum As Integer = -1
        Dim szMesg As String = ""           ' Added on 18 Apr 2010
        Dim iRet As Integer = -1

        Dim objMailMesg As New MailMessage
        Dim objMail As SmtpMail = Nothing
        Dim szBody As String = ""

        st_PayInfo.szHashValue = ""

        Try
            '- Verify Host return IP addresses (optional)
            '- Check late response - exceeding 2 hours from request datetime since booking PO will be released after 2 hours
            '- Update Txn State
            '- Verify response data with request data
            '- Reply Acknowledgement to Host if needed
            '- Get merchant password
            '- Get HostTxnStatus's action

            If True <> bInitResponse(st_PayInfo, st_HostInfo, sz_ReplyDateTime, sz_HTML, b_VerifyHostIP) Then
                Return False
            End If

            'Process Host reply based on action type configured in database according to Host TxnStatus
            If (1 = st_PayInfo.iAction) Then                'Payment approved by Host
                'Need to confirm booking for this merchant
                'If (1 = st_PayInfo.iNeedAddOSPymt) Then
                'OpenSkies Integration - (1) DisplayReservation based on PNR/Order Number (2) Confirm booking if necessary
                'The above operation will be performed by UASOAPClient.dll's ConfirmBooking interface function
                'OB Gateway will call it via a wrapper class library (managed code written in Visual C++ .NET), i.e. SOAPClient.dll
                'Input params : OrderNumber, MerchantPymtID, GatewayTxnID, HostTxnID, 
                '               IssuingBank (for Remarks in ChangeReservation), TxnAmount, CurrencyCode, OpenSkies Payment Code by Host
                'Output params: iRet, MesgSet and MesgNum will be available if iRet is 2 or 5
                'iRet:  - 0		Success (Payment approved and booking is successfully confirmed for Merchant ID that needs Gateway to confirm booking)
                '       - 1		(Failed - OS Amount mismatched with request's TxnAmount)
                '	    - 2		(Failed - OS returned error other than "Comm Error")
                '	    - 3		(Failed - OS pymt status "OK" but DistributionOption not "E")
                '	    - 4		(Pending - OS pymt status "PN" after a maximum of 2 retries ==> caller to retry ConfirmBooking)
                '	    - 5		(Unconfirmed - Comm error with Reservation System after a maximum of 2 retries ==> caller to retry ConfirmBooking)
                '	    - -1	(Failed - UASOAPClient.dll system error) - Exception, HTTP error, HTTP timeout, Exception,
                '               Invalid input params, Unknown OpenSkies Payment Status, XML parsing error
                '       - -2	(Failed - SOAPClient.dll system error)
                '- If "PN", retry DisplayReservation 2 times, then if still "PN" at the 2nd time, return error, DD GW should flag
                '- this in database, so that the Service will retry and if up to 1 hour 45 minutes still like this then exception report will be generated (similar to Mandiri Exception Report), show status
                '- the respective AirAsia team can check based on status.
                '- If "WA/ER" - warning/error, retry DisplayReservation until timeout or max number of times? exception report, show status
                '- If "OK", check DistributionOption, is it "E", if yes return success, if no, call ChangeReservation BUT put
                '- empty for <payments> tag, e.g. <payments />, AND put <DistributionOption> to "E" again, after that,
                '- Call DisplayReservation - maybe no need to handle this, just put in Exception Report, as suggested by Bill, 5 Nov 2008

                ' Added on 9 Apr 2009, to cater for certain host which does not return anything but just status
                ' to pass empty fields value checking by UASOAPClient.dll for field(5th), which is BankRefNo.
                'If ("" = st_PayInfo.szHostTxnID) Then
                'st_PayInfo.szHostTxnID = "-"
                'End If

                'ConfirmBooking Start
                '' Added on 3 Aug 2009, base currency booking confirmation enhancement
                ''Base Currency is DIFFERENT from Payment Currency, then, confirm booking using Base Currency and Base Amount
                'If (st_PayInfo.szBaseCurrencyCode <> "" And (st_PayInfo.szBaseCurrencyCode.ToUpper() <> st_PayInfo.szCurrencyCode.ToUpper())) Then
                '    'Added on 14 Apr 2010
                '    If ("NS" = st_PayInfo.szGatewayID.ToUpper()) Then
                '        Dim WS As New NSWebService.Service1 ' Added on 18 Apr 2010

                '        WS.Url = ConfigurationManager.AppSettings("NSWebSvcURL")

                '        objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), _
                '                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
                '                        " > Confirming Booking")

                '        iRet = WS.ConfirmBooking(st_PayInfo.szMerchantOrdID, st_PayInfo.szTxnID, st_PayInfo.szMerchantTxnID, st_PayInfo.szHostTxnID, st_PayInfo.szBaseTxnAmount, st_PayInfo.szBaseCurrencyCode, st_HostInfo.szOSPymtCode, st_PayInfo.szIssuingBank, szMesg)

                '        objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), _
                '                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
                '                        " > Base Currency ConfirmBooking OrderNumber(" + st_PayInfo.szMerchantOrdID + ") GatewayTxnID(" + st_PayInfo.szTxnID + ") MerchantPymtID(" + st_PayInfo.szMerchantTxnID + ") HostTxnID(" + st_PayInfo.szHostTxnID + ") BaseTxnAmount(" + st_PayInfo.szBaseTxnAmount + ") BaseCurrencyCode(" + st_PayInfo.szBaseCurrencyCode + ") TxnAmount(" + st_PayInfo.szTxnAmount + ") CurrencyCode(" + st_PayInfo.szCurrencyCode + ") OSPymtCode(" + st_HostInfo.szOSPymtCode + ") IssuingBank(" + st_PayInfo.szIssuingBank + ")")
                '    Else
                '        iRet = objSOAP.ConfirmBooking(ConfigurationManager.AppSettings("UASOAPClientPath"), st_PayInfo.szMerchantOrdID, st_PayInfo.szTxnID, st_PayInfo.szMerchantTxnID, st_PayInfo.szHostTxnID, st_PayInfo.szBaseTxnAmount, st_PayInfo.szBaseCurrencyCode, st_HostInfo.szOSPymtCode, st_PayInfo.szIssuingBank, iMesgSet, iMesgNum)

                '        objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), _
                '                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
                '                        " > Base Currency ConfirmBooking UASOAPClientDLL(" + ConfigurationManager.AppSettings("UASOAPClientPath") + ") OrderNumber(" + st_PayInfo.szMerchantOrdID + ") GatewayTxnID(" + st_PayInfo.szTxnID + ") MerchantPymtID(" + st_PayInfo.szMerchantTxnID + ") HostTxnID(" + st_PayInfo.szHostTxnID + ") BaseTxnAmount(" + st_PayInfo.szBaseTxnAmount + ") BaseCurrencyCode(" + st_PayInfo.szBaseCurrencyCode + ") TxnAmount(" + st_PayInfo.szTxnAmount + ") CurrencyCode(" + st_PayInfo.szCurrencyCode + ") OSPymtCode(" + st_HostInfo.szOSPymtCode + ") IssuingBank(" + st_PayInfo.szIssuingBank + ")")
                '    End If
                'Else
                '    'Base Currency is EMPTY or Base Currency is SAME as Payment Currency, then, confirm booking using Payment Currency and Payment Amount
                '    'Added on 14 Apr 2010
                '    If ("NS" = st_PayInfo.szGatewayID.ToUpper()) Then
                '        Dim WS As New NSWebService.Service1 ' Added on 18 Apr 2010

                '        WS.Url = ConfigurationManager.AppSettings("NSWebSvcURL")

                '        objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), _
                '                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
                '                        " > Confirming Booking")

                '        iRet = WS.ConfirmBooking(st_PayInfo.szMerchantOrdID, st_PayInfo.szTxnID, st_PayInfo.szMerchantTxnID, st_PayInfo.szHostTxnID, st_PayInfo.szTxnAmount, st_PayInfo.szCurrencyCode, st_HostInfo.szOSPymtCode, st_PayInfo.szIssuingBank, szMesg)

                '        objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), _
                '                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
                '                        " > ConfirmBooking OrderNumber(" + st_PayInfo.szMerchantOrdID + ") GatewayTxnID(" + st_PayInfo.szTxnID + ") MerchantPymtID(" + st_PayInfo.szMerchantTxnID + ") HostTxnID(" + st_PayInfo.szHostTxnID + ") TxnAmount(" + st_PayInfo.szTxnAmount + ") CurrencyCode(" + st_PayInfo.szCurrencyCode + ") OSPymtCode(" + st_HostInfo.szOSPymtCode + ") IssuingBank(" + st_PayInfo.szIssuingBank + ")")
                '    Else
                '        iRet = objSOAP.ConfirmBooking(ConfigurationManager.AppSettings("UASOAPClientPath"), st_PayInfo.szMerchantOrdID, st_PayInfo.szTxnID, st_PayInfo.szMerchantTxnID, st_PayInfo.szHostTxnID, st_PayInfo.szTxnAmount, st_PayInfo.szCurrencyCode, st_HostInfo.szOSPymtCode, st_PayInfo.szIssuingBank, iMesgSet, iMesgNum)

                '        objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), _
                '                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
                '                        " > ConfirmBooking UASOAPClientDLL(" + ConfigurationManager.AppSettings("UASOAPClientPath") + ") OrderNumber(" + st_PayInfo.szMerchantOrdID + ") GatewayTxnID(" + st_PayInfo.szTxnID + ") MerchantPymtID(" + st_PayInfo.szMerchantTxnID + ") HostTxnID(" + st_PayInfo.szHostTxnID + ") TxnAmount(" + st_PayInfo.szTxnAmount + ") CurrencyCode(" + st_PayInfo.szCurrencyCode + ") OSPymtCode(" + st_HostInfo.szOSPymtCode + ") IssuingBank(" + st_PayInfo.szIssuingBank + ")")
                '    End If
                'End If

                'objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), _
                '                DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
                '                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > ConfirmBooking iRet(" + iRet.ToString() + ") MesgSet(" + iMesgSet.ToString() + ") MesgNum(" + iMesgNum.ToString() + ") Mesg(" + szMesg + ")")

                'st_PayInfo.iOSRet = iRet
                'st_PayInfo.iErrSet = iMesgSet
                'st_PayInfo.iErrNum = iMesgNum

                ''Map iRet to TxnStatus returned to merchant.
                'Select Case iRet
                '    Case 0  'Success (Payment approved and booking is successfully confirmed for Merchant ID that needs Gateway to confirm booking)
                '        st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_SUCCESS
                '        st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_SUCCESS
                '        st_PayInfo.szTxnMsg = Common.C_TXN_SUCCESS_0
                '        st_PayInfo.iTxnState = Common.TXN_STATE.COMMIT

                '        ' Modified on 13 Apr 2009, changed status to 3 but no need retry
                '    Case 1  'Failed - Now: Booking unconfirmed due to partial payment not allowed; Previously: OS Amount mismatched with request's TxnAmount
                '        'st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                '        'st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_FAILED
                '        st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_PENDING_CONFIRM_BOOKING   'Merchant will extend booking based on this status
                '        st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_PENDING_CONFIRM_BOOKING
                '        st_PayInfo.szTxnMsg = "Failed: Payment approved but booking unconfirmed due to partial payment not allowed"
                '        st_PayInfo.iTxnState = Common.TXN_STATE.RECV_CONFIRM_OS
                '        'st_PayInfo.iTxnState = Common.TXN_STATE.COMMIT_FAILED

                '        ' Modified on 13 Apr 2009, changed status to 3 but no need retry
                '    Case 2  'Failed - OS returned error other than "Comm Error"
                '        'st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                '        'st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_FAILED
                '        st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_PENDING_CONFIRM_BOOKING   'Merchant will extend booking based on this status
                '        st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_PENDING_CONFIRM_BOOKING
                '        st_PayInfo.szTxnMsg = "Booking Extended: Payment approved but OS returned error MsgSet[" + iMesgSet.ToString() + "] MsgNum [" + iMesgNum.ToString() + "]"
                '        st_PayInfo.iTxnState = Common.TXN_STATE.RECV_CONFIRM_OS
                '        'st_PayInfo.iTxnState = Common.TXN_STATE.COMMIT_FAILED

                '        ' Modified on 13 Apr 2009, changed status to 0 (Success) but no need retry
                '    Case 3  'Failed - OS pymt status "OK" but DistributionOption not "E"
                '        'st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                '        'st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_FAILED
                '        st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_SUCCESS
                '        st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_SUCCESS
                '        st_PayInfo.szTxnMsg = "Partial Success: Payment approved, booking confirmed but DistributionOption not changed to E"
                '        st_PayInfo.iTxnState = Common.TXN_STATE.COMMIT
                '        'st_PayInfo.iTxnState = Common.TXN_STATE.COMMIT_FAILED

                '    Case 4  'Pending - OS pymt status "PN" after a maximum of 2 retries ==> Retry service to retry ConfirmBooking
                '        st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_PENDING_CONFIRM_BOOKING   'Merchant will extend booking based on this status
                '        st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_PENDING_CONFIRM_BOOKING
                '        st_PayInfo.szTxnMsg = "Pending: Payment approved but booking is in Pending status"
                '        st_PayInfo.iTxnState = Common.TXN_STATE.RECV_CONFIRM_OS

                '        st_PayInfo.iAction = 4  'Retry ConfirmBooking

                '    Case 5  'Unconfirmed - Comm error with Reservation System after a maximum of 2 retries ==> Retry service to retry ConfirmBooking
                '        st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_PENDING_CONFIRM_BOOKING   'Merchant will extend booking based on this status
                '        st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_PENDING_CONFIRM_BOOKING
                '        st_PayInfo.szTxnMsg = "Unknown: Payment approved but booking status is unknown due to communication error returned by the booking system"
                '        st_PayInfo.iTxnState = Common.TXN_STATE.RECV_CONFIRM_OS

                '        st_PayInfo.iAction = 4  'Retry ConfirmBooking

                '    Case -1 'Failed - UASOAPClient.dll's system error => Exception, HTTP error, HTTP timeout, Invalid input params, Unknown OpenSkies Payment Status, XML parsing error
                '        st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_PENDING_CONFIRM_BOOKING
                '        st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_PENDING_CONFIRM_BOOKING
                '        st_PayInfo.szTxnMsg = "Unknown: Payment approved but encountered internal system error 1, could be HTTP timeout Booking Engine"
                '        st_PayInfo.iTxnState = Common.TXN_STATE.SENT_CONFIRM_OS

                '        st_PayInfo.iAction = 4  'Retry ConfirmBooking

                '        ' Added -9 and Action 4, 13 Apr 2009, for SOAPClient.dll's exception, do not retry, will cause W3SVC die!!
                '    Case -2, -9 'Failed - SOAPClient.dll's system error
                '        'st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                '        'st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_FAILED
                '        st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_PENDING_CONFIRM_BOOKING
                '        st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_PENDING_CONFIRM_BOOKING
                '        st_PayInfo.szTxnMsg = "Unknown: Payment approved but encountered internal system error 2"
                '        st_PayInfo.iTxnState = Common.TXN_STATE.SENT_CONFIRM_OS
                '        'st_PayInfo.iTxnState = Common.TXN_STATE.COMMIT_FAILED

                '        'st_PayInfo.iAction = 4  'Retry ConfirmBooking

                '    Case Else   'Unknown iRet value
                '        st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                '        st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_FAILED
                '        st_PayInfo.szTxnMsg = "Failed: Payment approved but received unknown status from internal component"
                '        st_PayInfo.iTxnState = Common.TXN_STATE.COMMIT_FAILED
                'End Select
                'ConfirmBooking End

                'Else    'Success (Payment approved, no need to confirm booking for this merchant)
                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_SUCCESS

                'Modified  29 April 2015. Auth&Capture
                If ("AUTH" = st_PayInfo.szTxnType) Then
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_AUTH_SUCCESS
                Else
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_SUCCESS
                End If

                st_PayInfo.szTxnMsg = Common.C_TXN_SUCCESS_0
                st_PayInfo.iTxnState = Common.TXN_STATE.COMMIT

                'Added, 19 Apr 2015, for OTC
                If ("OTC" = st_PayInfo.szPymtMethod) Then
                    If (-1 = st_PayInfo.iQueryStatus) Then  'Record exists in Request table
                        st_PayInfo.iAction = 4  'Retry Service to pickup and only send response to merchat if exceeded permissible Reversal period
                    End If
                End If
                'End If

            ElseIf (2 = st_PayInfo.iAction) Then    'Failed by Host
                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.szTxnMsg = Common.C_TXN_FAILED_1
                st_PayInfo.iTxnState = Common.TXN_STATE.COMMIT_FAILED

            ElseIf (3 = st_PayInfo.iAction) Then    'Unconfirmed status from Host, need to query Host until getting reply or timeout
                'Update Request table with Action 3 for Retry Service to pick up and query the respective Host
                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_PENDING
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_PENDING
                st_PayInfo.szTxnMsg = "Host returned not processed/unknown status, pending query Host"
                st_PayInfo.iTxnState = Common.TXN_STATE.RECV_FROM_HOST

            ElseIf (5 = st_PayInfo.iAction Or 6 = st_PayInfo.iAction) Then    'Due to query account invalid password, need to send email alert and stop querying Host by setting QueryFlag to OFF until QueryFlag is manually set to ON
                'Overwrite Action from 5 to 3 and then update Request table with Action 3 for Retry Service to pick up
                'and query the respective Host
                'Requested by AirAsia's Bill on 8 June 2009, added a setting to enable and disable the Query Flag OFF feature
                'This is due to Maybank's response code 20 is inaccurate, it not only indicates invalid query account's
                'password provided by merchant; It also indicates internal network error.
                If (5 = st_PayInfo.iAction) Then
                    st_HostInfo.iQueryFlag = 0
                    'Modified 25 Nov 2013, use objCommon instead of objTxnProc, to cater for different DB connection by Payment Method
                    If True <> objCommon.bUpdateHostInfo(st_PayInfo, st_HostInfo) Then
                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                        st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " " + st_PayInfo.szErrDesc)
                    End If
                End If

                'Sends email alert (http://support.microsoft.com/kb/555287)
                objMailMesg.From = ConfigurationManager.AppSettings("EmailSendFrom")
                objMailMesg.To = ConfigurationManager.AppSettings("EmailSendTo")
                objMailMesg.Cc = ConfigurationManager.AppSettings("EmailSendCc")
                objMailMesg.BodyFormat = MailFormat.Text
                objMailMesg.Priority = MailPriority.High

                If (5 = st_PayInfo.iAction) Then
                    objMailMesg.Subject = "Online Banking Alert - " + st_PayInfo.szIssuingBank + " Query Flag OFF"

                    szBody = "Dear all," + vbNewLine + vbNewLine
                    szBody = szBody + "This is an auto generated email. Please be informed that Query Flag for " + st_PayInfo.szIssuingBank + " is currently set to OFF due to Query rejected with error Unauthorized Query UserName and Password." + vbNewLine + vbNewLine
                    szBody = szBody + "Please liaise with the bank to check this Query account accordingly. After that, kindly inform Paydibs Support Team to re-configure the Query UserName and Password and then turn ON Query Flag." + vbNewLine + vbNewLine
                    szBody = szBody + "Thank you," + vbNewLine
                    szBody = szBody + "Online Banking Payment Gateway" + vbNewLine    'Removed AirAsia,  15 Jul 2011
                    'szBody = szBody + "AirAsia Berhad, Malaysia"   'Commented  15 Jul 2011
                Else
                    If ("" <> st_PayInfo.szHostTxnStatus) Then
                        objMailMesg.Subject = "Online Banking Alert - " + st_PayInfo.szIssuingBank + " Response Code " + st_PayInfo.szHostTxnStatus
                    Else
                        objMailMesg.Subject = "Online Banking Alert - " + st_PayInfo.szIssuingBank + " Response Code " + st_PayInfo.szRespCode
                    End If

                    szBody = "Dear all," + vbNewLine + vbNewLine
                    szBody = szBody + "This is an auto generated email. Please be informed that Response Code as stated in email title for " + st_PayInfo.szIssuingBank + " was received." + vbNewLine + vbNewLine
                    szBody = szBody + "Please liaise with the bank to check this accordingly. After that, kindly inform Paydibs Support Team for further actions." + vbNewLine + vbNewLine
                    szBody = szBody + "Thank you," + vbNewLine
                    szBody = szBody + "Online Banking Payment Gateway" + vbNewLine    'Removed "AirAsia",  15 Jul 2011
                    'szBody = szBody + "AirAsia Berhad, Malaysia"   'Commented  15 Jul 2011
                End If

                objMailMesg.Body = szBody

                objMailMesg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusing", 2) 'Send the message using the network (SMTP over the network).
                objMailMesg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpusessl", False)  'Use SSL for the connection (True or False)
                objMailMesg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout", 1200)
                objMailMesg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpserver", ConfigurationManager.AppSettings("EmailServer"))
                objMailMesg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpserverport", ConfigurationManager.AppSettings("EmailPort"))
                objMailMesg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", 1)    'Use basic clear-text authentication, have to provide user name and password through sendusername and sendpassword fields
                objMailMesg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", ConfigurationManager.AppSettings("EmailUserName"))
                objMailMesg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", ConfigurationManager.AppSettings("EmailPassword"))

                objMail.SmtpServer = ConfigurationManager.AppSettings("EmailServer")
                objMail.Send(objMailMesg)

                st_PayInfo.iAction = 3

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_TIMEOUT_HOST_RETURN
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_PENDING
                st_PayInfo.szTxnMsg = "Pending query Host"
                st_PayInfo.iTxnState = Common.TXN_STATE.RECV_FROM_HOST

                'Added 20 Jun 2011, for Mandiri, to cater for Pay response status "Pending" received from Host, need to send Reversal to Host
            ElseIf (7 = st_PayInfo.iAction) Then
                'Update Request table with Action 7 for Retry Service to pick up and send Reversal to Host
                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_HOST_UNPROCESSED
                st_PayInfo.szTxnMsg = "Host returned Pay pending status, pending Reversal to Host"
                st_PayInfo.iTxnState = Common.TXN_STATE.RECV_FROM_HOST
            End If

            'Added, 20 Apr 2017, FPX Buyer Bank Name
            'Modified 31 May 2017, added FPX Buyer Name
            'Modified 24 Jul 2017, removed checking of szBankRespMsg which stores BuyerBankBranch which is optional,will cause BuyerName not returned
            'Modified  30 Oct 2017. Rakuten to request add txn status message
            'Modified  24 Nov 2017, Melaka Security requested to add Debit AuthCode
            If (Left(st_PayInfo.szIssuingBank, 3) = "FPX") Then
                st_PayInfo.szTxnMsg = st_PayInfo.szBankRespMsg + "|" + st_HostInfo.szReserved + "|" + st_PayInfo.szTxnMsg + "|" + st_PayInfo.szRespCode
                st_PayInfo.szBankRespMsg = st_PayInfo.szTxnMsg
            End If

            'Update Request table with the respective Txn State and Action so that Retry Service can process
            'accordingly for both Action 3 (retry query host), Action 4 (retry confirm booking) and Action 7 (retry Reversal)
            'At the same time, also update Request table with BankRefNo (st_PayInfo.szHostTxnID), OSPymtCode (st_HostInfo.szOSPymtCode),
            'OSRet (iRet), ErrSet (iMesgSet), ErrNum (iMesgNum)
            'Added (7 = st_PayInfo.iAction) , 20 Jun 2011, for Mandiri, to cater for Pay response status "Pending" received from Host, need to send Reversal to Host
            If ((3 = st_PayInfo.iAction) Or (4 = st_PayInfo.iAction) Or (7 = st_PayInfo.iAction)) Then
                'Modified 25 Nov 2013, use objCommon instead of objTxnProc, to cater for different DB connection by Payment Method
                If True <> objCommon.bUpdateTxnStatus(st_PayInfo, st_HostInfo) Then
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " " + st_PayInfo.szErrDesc)
                End If
            End If

            'Modified 9 May 2017, added checking of OTC, no need send reply to merchant, OTC Notify Service will notify
            If (st_PayInfo.szPymtMethod <> "OTC") Then
                'Send Gateway response to merchant using the same Host's reply method
                'Modified 24 Nov 2016, removed checking of OTC payment method
                iSendReply2Merchant(st_PayInfo, st_HostInfo, sz_HTML)
            End If
            'Even If (True = bRet), no need update DB for TxnState SENT_REPLY_CLIENT so that TxnState like COMMIT/COMMIT_FAILED won't be overwritten.

        Catch ex As Exception
            bReturn = False

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Exception: " + ex.Message)

            'Added on 12 Mar 2011, to solve "The operation has timed out" exception after calling NewSkies Web Service ConfirmBooking()
            'Without adding this, even though payment was approved but when encountered operation timeout during booking confirmation,
            'DDGW will return TxnStatus -1 and TxnState 2 to merchant that requires DDGW to confirm booking
            If (1 = st_PayInfo.iAction And 1 = st_PayInfo.iNeedAddOSPymt) Then  'Approved by bank and need booking confirmation
                bReturn = True

                If ("THE OPERATION HAS TIMED OUT" = ex.Message.ToUpper.Trim()) Then
                    st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_PENDING_CONFIRM_BOOKING   'Merchant will extend booking based on this status
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_PENDING_CONFIRM_BOOKING
                    st_PayInfo.szTxnMsg = "Pending: Payment approved but booking status is unknown due to communication error with booking system, retry in progress"
                    st_PayInfo.iTxnState = Common.TXN_STATE.SENT_CONFIRM_OS

                    st_PayInfo.iAction = 4  'Retry ConfirmBooking
                Else
                    st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_PENDING
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_PENDING
                    st_PayInfo.szTxnMsg = "Pending: Status unknown, query Host in progress"
                    st_PayInfo.iTxnState = Common.TXN_STATE.RECV_FROM_HOST

                    st_PayInfo.iAction = 3  'Retry query bank
                End If

                'Update Request table with the respective Txn State and Action so that Retry Service can retry confirm booking
                'At the same time, also update Request table with BankRefNo (st_PayInfo.szHostTxnID), OSPymtCode (st_HostInfo.szOSPymtCode)
                'Modified 25 Nov 2013, use objCommon instead of objTxnProc, to cater for different DB connection by Payment Method
                If True <> objCommon.bUpdateTxnStatus(st_PayInfo, st_HostInfo) Then
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " " + st_PayInfo.szErrDesc)
                End If

                'Modified 9 May 2017, added checking of OTC, no need send reply to merchant, OTC Notify Service will notify
                If (st_PayInfo.szPymtMethod <> "OTC") Then
                    'Send Gateway response to merchant using the same Host's reply method
                    iSendReply2Merchant(st_PayInfo, st_HostInfo, sz_HTML)
                End If

                'Even If (True = bRet), no need update DB for TxnState SENT_REPLY_CLIENT so that TxnState like COMMIT/COMMIT_FAILED won't be overwritten.
            End If
        End Try

        If Not objMailMesg Is Nothing Then objMailMesg = Nothing

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		Response
    ' Function Name:	bProcessSaleOrder
    ' Function Type:	Boolean.  Return False if error.
    ' Parameter:		
    ' Description:		Process payment order response
    ' History:			05 Oct 2009
    '********************************************************************************************
    'Public Function bProcessSaleOrder(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost, ByVal sz_ReplyDateTime As String, ByRef sz_HTML As String) As Boolean
    '    Dim bReturn As Boolean = True
    '    Dim szQueryString As String = ""
    '    Dim iRet As Integer = -1

    '    Try
    '        '- Verify Host return IP addresses (optional)
    '        '- Get order details
    '        If True <> bInitOrderResponse(st_PayInfo, st_HostInfo, sz_HTML) Then
    '            Return False
    '        End If

    '    Catch ex As Exception
    '        bReturn = False

    '        objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), _
    '                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
    '                        st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Exception: " + ex.Message)
    '    End Try

    '    Return bReturn
    'End Function
    '********************************************************************************************
    ' Class Name:		Response
    ' Function Name:	bSendReply2Merchant
    ' Function Type:	NONE
    ' Parameter:        
    ' Description:		Error handler
    ' History:			18 Nov 2008
    '********************************************************************************************
    Public Function iSendReply2Merchant(ByRef st_PayInfo As Common.STPayInfo, ByVal st_HostInfo As Common.STHost, ByRef sz_HTML As String) As Integer
        Dim szHTTPString As String = ""
        Dim iRet As Integer = -1

        'Dim PGMailer As New MailService.Mailer                'Added, 24 Jan 2014
        'Dim PGMailerStatus As New MailService.MailerStatus    'Added, 24 Jan 2014
        Dim objMail As New Mail(objLoggerII)
        Dim iMailStatus As Integer = -1
        Dim szStatusMsg As String = ""
        Dim iShopperEmailNotifyStatus As Integer = -1           'Added  21 Nov 2014 - ENS
        Dim iMerchantEmailNotifyStatus As Integer = -1          'Added  21 Nov 2014 - ENS

        Dim szMerchantReturnURL As String = ""                  'Added, 30 Jun 2014
        Dim szLogString As String = ""                          'Added, 20 May 2016, to avoid logging card expiry

        Dim bSendFraudEmail As Boolean = True                   'Added, 11 Apr 2019, Joyce

        Try
            'Generates response hash value
            'st_PayInfo.szHashValue = objHash.szGetSaleResHash(st_PayInfo)
            'Added, 28 Apr 2016, generates response hash value 2 which includes OrderNumber and Param6 because some plugins like Magento update order based on these 2 fields
            st_PayInfo.szHashValue2 = objHash.szGetSaleResHash2(st_PayInfo)

            'Modified  4 Oct 2017. Retrieve Param1 (VCO-CallID) for VCO update payment
            If (1 = st_PayInfo.iAllowInstallment And "CC" = st_PayInfo.szPymtMethod Or "V" = st_PayInfo.szGatewayID) Then  'Merchant allowed Installment. Modified  17 Jun 2016, added Method checking
                'get param10 which is storing HIID. Function not checking true/false and Return False because if not get any value, no EPPMonth and EPP_YN returned only.
                objTxnProc.bGetTxnParam(st_PayInfo)
                If ("" <> st_PayInfo.szParam10) Then
                    st_PayInfo.iInstallment = Convert.ToInt16(st_PayInfo.szParam10)
                    objTxnProc.bGetInstallmentDetails(st_PayInfo)
                End If
            End If

            'Generates HTTP string to be replied to merchant
            szHTTPString = szGetReplyMerchantHTTPString(st_PayInfo, st_HostInfo, szLogString)

            'Added, 30 Jun 2014
            szMerchantReturnURL = st_PayInfo.szMerchantReturnURL
            If (0 = st_PayInfo.iMerchantTxnStatus) Then
                If (st_PayInfo.szMerchantApprovalURL <> "") Then
                    szMerchantReturnURL = st_PayInfo.szMerchantApprovalURL
                End If
            ElseIf (1 = st_PayInfo.iMerchantTxnStatus) Then
                If (st_PayInfo.szMerchantUnApprovalURL <> "") Then
                    szMerchantReturnURL = st_PayInfo.szMerchantUnApprovalURL
                End If
            End If

            'Added checking of DDGW Aggregator Model on 28 Mar 2010
            'Commented  28 Oct 2013, replaced with checking iReceipt set in Merchant table
            'If (ConfigurationManager.AppSettings("HostsListTemplate") <> "") Then

            'Modified 31 Mar 2015, for BNB, modified 1 = st_PayInfo.iReceipt to bLoadReceipt
            'Modified 26 Mar 2015, added checking of "BNB"
            'Modified 31 Mar 2015, added Trim for szTokenType to avoid szTokenType not initialized as "" or any value, will hit object reference not set to an instance of an object error
            If (1 = st_PayInfo.iReceipt Or "BNB" = Trim(st_PayInfo.szTokenType).ToUpper()) Then
                If (1 = st_HostInfo.iHostReplyMethod Or 4 = st_HostInfo.iHostReplyMethod) Then
                    'Modified 8 Apr 2015, added checking of BNB and iLoadReceiptBNBUI
                    If ("BNB" = Trim(st_PayInfo.szTokenType).ToUpper()) Then
                        iRet = iLoadReceiptBNBUI(sz_HTML, st_PayInfo, st_HostInfo)
                    Else
                        'Modified 30 Jun 2014, use szMerchantReturnURL instead of st_PayInfo.szMerchantReturnURL
                        iRet = iLoadReceiptUI(szMerchantReturnURL, szHTTPString, sz_HTML, st_PayInfo, st_HostInfo)
                    End If

                    If (iRet <> 0) Then
                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                        st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Failed to load receipt for MerchantRURL(" + szMerchantReturnURL + ") Params(" + szHTTPString + ")")
                    Else
                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                        st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Loaded receipt for MerchantRURL(" + szMerchantReturnURL + ") Params(" + szLogString + ")")   'Modified 22 May 2016, from szHTTPString to szLogString, do not log clear Card Expiry
                    End If
                Else
                    'Send HTTP string reply to merchant using the same method used by Host
                    'Modified 30 Jun 2014, use szMerchantReturnURL instead of st_PayInfo.szMerchantReturnURL
                    'Modified 4 Oct 2016, added st_HostInfo, KTB redirect query
                    iRet = iHTTPSend(st_HostInfo.iHostReplyMethod, szHTTPString, szMerchantReturnURL, st_PayInfo, st_HostInfo, sz_HTML, "TLS", szLogString)
                    If (iRet <> 0) Then
                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                        st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Failed to send reply iRet(" + CStr(iRet) + ") " + szHTTPString + " to merchant URL " + szMerchantReturnURL)
                    Else
                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                        st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Reply to merchant sent.")
                    End If
                End If
            Else
                'Send HTTP string reply to merchant using the same method used by Host
                'Modified 30 Jun 2014, use szMerchantReturnURL instead of st_PayInfo.szMerchantReturnURL
                'Modified 4 Oct 2016, KTB redirect resp query
                iRet = iHTTPSend(st_HostInfo.iHostReplyMethod, szHTTPString, szMerchantReturnURL, st_PayInfo, st_HostInfo, sz_HTML, "TLS", szLogString)
                If (iRet <> 0) Then
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Failed to send reply iRet(" + CStr(iRet) + ") " + szHTTPString + " to merchant URL " + szMerchantReturnURL)
                Else
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Reply to merchant sent.")
                End If
            End If

            'Added  3 Oct 2014, Callback
            bMerchantCallBackResp(st_PayInfo, st_HostInfo)  'Combined  27 Feb 2015

            'Added, 24 Jan 2014, Email Notification
            'Modified 18 Feb 2014, added checking of TxnType "PAY"
            '1 - Customer only
            '2 - Merchant only
            '3 - Customer and Merchant
            'Modified  13 April 2015.  For Auth&Capture, only Capture needs to send email notification.
            If (Common.TXN_STATUS.TXN_SUCCESS = st_PayInfo.iMerchantTxnStatus And ("PAY" = st_PayInfo.szTxnType.ToUpper() Or "CAPTURE" = st_PayInfo.szTxnType.ToUpper()) And st_PayInfo.iPymtNotificationEmail > 0) Then
                'Dim objeMail As New MailService.eMail
                Dim szTo() As String = Nothing          'Added, 24 Jan 2014
                Dim szCc() As String = Nothing          'Added, 24 Jan 2014, not used
                Dim szBcc() As String = Nothing         'Added, 24 Jan 2014, not used
                Dim szReceiptEmailBody As String = ""   'Added, 29 Jan 2014
                Dim szToAddress() As String = Nothing   'Added, 15 Jan 2015
                Dim szSubject As String = ""

                Dim iShopperEmailNotifyStatusEx As Integer = -1     'Added, 21 Apr 2017
                Dim iMerchantEmailNotifyStatusEx As Integer = -1    'Added, 21 Apr 2017

                ReDim szCc(0)                           'Added, 10 Feb 2014
                ReDim szBcc(0)                          'Added, 10 Feb 2014
                ReDim szTo(0)                           'Added, 15 Jan 2015

                szCc(0) = ""                            'Added, 10 Feb 2014
                szBcc(0) = ""                           'Added, 10 Feb 2014

                'Added  28 Nov 2014, ENS
                objTxnProc.bCheckNotifyStatus(st_PayInfo, iShopperEmailNotifyStatus, iMerchantEmailNotifyStatus)

                'Added, 21 Apr 2017, set default email notify status even though not yet sent, to avoid concurrent email notification sending attempt which may cause duplicate emails            
                If ((1 = st_PayInfo.iPymtNotificationEmail Or 3 = st_PayInfo.iPymtNotificationEmail) And iShopperEmailNotifyStatus <> 1) Then
                    iShopperEmailNotifyStatusEx = 1
                End If
                If ((2 = st_PayInfo.iPymtNotificationEmail Or 3 = st_PayInfo.iPymtNotificationEmail) And iMerchantEmailNotifyStatus <> 1) Then
                    iMerchantEmailNotifyStatusEx = 1
                End If

                'Added, 21 Apr 2017, moved this from below to here, to avoid concurrent email notification sending attempt which may cause duplicate emails            
                If (True = objTxnProc.bInsertNotifyStatus(st_PayInfo, iShopperEmailNotifyStatusEx, iMerchantEmailNotifyStatusEx)) Then
                    'Commented  14 Jan 2015, no need check Is Nothing
                    'If (Not szTo Is Nothing) Then
                    'Added, 17 Jun 2015, to support TLS 1.2, for PCI
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12

                    ' allows for validation of SSL conversations
                    ServicePointManager.ServerCertificateValidationCallback = Function() True   'C#: delegate { return true }

                    'IPGMailer.Url = ConfigurationManager.AppSettings("EmailServer")    'Added, 7 Feb 2014
                    'IPGMailer.Timeout = 60000 'Milliseconds, 60s

                    'Added, 17 Feb 2014, differentiate shopper and merchant's payment notification
                    '1 - Customer only; 2 - Merchant only; 3 - Customer & Merchant
                    'Customer Email Notification
                    'Modified  28 Nov 2014, added checking of iShopperEmailNotifyStatus
                    If ((1 = st_PayInfo.iPymtNotificationEmail Or 3 = st_PayInfo.iPymtNotificationEmail) And iShopperEmailNotifyStatus <> 1) Then
                        szTo(0) = st_PayInfo.szCustEmail

                        If (iLoadReceiptEmailUI_Shopper(szReceiptEmailBody, st_PayInfo, st_HostInfo) <> 0) Then
                            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                            st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Failed to load shopper receipt email body for (" + st_PayInfo.iPymtNotificationEmail.ToString() + ":" + szTo(0) + ")")
                        Else
                            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                            st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Loaded shopper receipt email body for (" + st_PayInfo.iPymtNotificationEmail.ToString() + ":" + szTo(0) + ")")
                        End If

                        szSubject = ConfigurationManager.AppSettings("EmailSubject") + " [" + st_PayInfo.szMerchantName + "]" '" [" + st_PayInfo.szMerchantTxnID + "]"
                        iMailStatus = objMail.SendMail(szTo(0), szReceiptEmailBody, szSubject)
                        szStatusMsg = objMail.StatusMessage(iMailStatus)

                        st_PayInfo.iOSRet = iMailStatus
                        st_PayInfo.szErrDesc = szStatusMsg

                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Shopper email notification [" + szTo(0) + "] [" + iMailStatus.ToString() + ":" + szStatusMsg + "]")

                        iShopperEmailNotifyStatus = iMailStatus

                        'objeMail.To = szTo(0)
                        'objeMail.Cc = szCc(0)
                        'objeMail.Bcc = szBcc(0)
                        'objeMail.Subject = ConfigurationManager.AppSettings("EmailSubject") + " [" + st_PayInfo.szMerchantTxnID + "]"
                        'objeMail.FromName = ConfigurationManager.AppSettings("EmailFromName")
                        'objeMail.Body = szReceiptEmailBody

                        'PGMailerStatus = PGMailer.SendMail(objeMail)
                        ''PGMailerStatus = PGMailer.SendMail(ConfigurationManager.AppSettings("EmailFromAddr"), ConfigurationManager.AppSettings("EmailFromName"), szTo, szCc, szBcc, ConfigurationManager.AppSettings("EmailSubject") + " (" + st_PayInfo.szMerchantTxnID + ")", True, szReceiptEmailBody)
                        'If (Not PGMailerStatus Is Nothing) Then
                        '    st_PayInfo.iOSRet = PGMailerStatus.StatusCode
                        '    st_PayInfo.szErrDesc = PGMailerStatus.StatusMessage

                        '    'Added, 11 Feb 2014
                        '    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                        '    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                        '    st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Shopper email notification [" + szTo(0) + "] [" + PGMailerStatus.StatusCode.ToString() + ":" + PGMailerStatus.StatusMessage + "]")

                        '    iShopperEmailNotifyStatus = PGMailerStatus.StatusCode  'Added  21 Nov 2014 - ENS
                        'End If
                    End If

                    'Merchant Email Notification
                    'Modified  28 Nov 2014, added checking of iMerchantEmailNotifyStatus
                    If ((2 = st_PayInfo.iPymtNotificationEmail Or 3 = st_PayInfo.iPymtNotificationEmail) And iMerchantEmailNotifyStatus <> 1) Then
                        'szTo(0) = st_PayInfo.szMerchantNotifyEmailAddr  'Commented  14 Jan 2015
                        szToAddress = Split(st_PayInfo.szMerchantNotifyEmailAddr, ",")  'Modified 14 Jan 2015, support multiple merchant payment notification email addresses

                        If (iLoadReceiptEmailUI_Merchant(szReceiptEmailBody, st_PayInfo, st_HostInfo) <> 0) Then
                            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                            st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Failed to load merchant receipt email body for (" + st_PayInfo.iPymtNotificationEmail.ToString() + ":" + st_PayInfo.szMerchantNotifyEmailAddr + ")")
                        Else
                            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                            st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Loaded merchant receipt email body for (" + st_PayInfo.iPymtNotificationEmail.ToString() + ":" + st_PayInfo.szMerchantNotifyEmailAddr + ")")
                        End If

                        'Added, 15 Jan 2015, for loop
                        For iCount = 0 To szToAddress.GetUpperBound(0)
                            szSubject = ConfigurationManager.AppSettings("EmailSubject") + " [" + st_PayInfo.szMerchantTxnID + "]"
                            iMailStatus = objMail.SendMail(szToAddress(iCount), szReceiptEmailBody, szSubject)
                            szStatusMsg = objMail.StatusMessage(iMailStatus)

                            st_PayInfo.iOSRet = iMailStatus
                            st_PayInfo.szErrDesc = szStatusMsg

                            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Merchant email notification " + CStr(iCount + 1) + " [" + szToAddress(iCount) + "] [" + iMailStatus.ToString() + ":" + szStatusMsg + "]")

                            iMerchantEmailNotifyStatus = iMailStatus
                            'Modified 15 Jan 2015, added szToAddress
                            ''szTo(0) = szToAddress(iCount)
                            'objeMail.To = szToAddress(iCount)
                            'objeMail.Cc = szCc(0)
                            'objeMail.Bcc = szBcc(0)
                            'objeMail.Subject = ConfigurationManager.AppSettings("EmailSubject") + " [" + st_PayInfo.szMerchantTxnID + "]"
                            'objeMail.FromName = ConfigurationManager.AppSettings("EmailFromName")
                            'objeMail.Body = szReceiptEmailBody

                            'PGMailerStatus = PGMailer.SendMail(objeMail)
                            ''PGMailerStatus = PGMailer.SendMail(ConfigurationManager.AppSettings("EmailFromAddr"), ConfigurationManager.AppSettings("EmailFromName"), szTo, szCc, szBcc, ConfigurationManager.AppSettings("EmailSubject") + " (" + st_PayInfo.szMerchantTxnID + ")", True, szReceiptEmailBody)
                            'If (Not PGMailerStatus Is Nothing) Then
                            '    'Added, 11 Feb 2014
                            '    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                            '    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            '    st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Merchant email notification " + CStr(iCount + 1) + " [" + szTo(0) + "] [" + PGMailerStatus.StatusCode.ToString() + ":" + PGMailerStatus.StatusMessage + "]")

                            '    iMerchantEmailNotifyStatus = PGMailerStatus.StatusCode  'Added  21 Nov 2014 - ENS
                            'End If
                        Next
                    End If
                Else
                    'Added, 21 Apr 2017, to avoid duplicate email notification due to concurrent redirect and s2s payment response
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > InsertNotifyStatus failed due to already inserted by other payment resp")
                End If

                'Added  21 Nov 2014. - ENS
                'Commented  21 Apr 2017, avoid duplicate email notification due to concurrent bank response
                'objTxnProc.bInsertNotifyStatus(st_PayInfo, iShopperEmailNotifyStatus, iMerchantEmailNotifyStatus)
                'End If
            End If

            'Added 11 Apr 2019, Joyce. 
            'To check only success trx and over per trx amount limit only send fraud email applicable for iHitLimitAct = 0.
            If (Common.TXN_STATUS.TXN_SUCCESS = st_PayInfo.iMerchantTxnStatus And ("PAY" = st_PayInfo.szTxnType.ToUpper() Or "CAPTURE" = st_PayInfo.szTxnType.ToUpper()) And st_PayInfo.iHitLimitAct = 0) Then
                If ((0 = st_PayInfo.dPerTxnAmtLimit And True <> objTxnProc.bCheckTxnAmountMinMaxLimit(st_PayInfo)) Or (("IDR" <> st_PayInfo.szCurrencyCode And "VND" <> st_PayInfo.szCurrencyCode) And (st_PayInfo.dPerTxnAmtLimit > 0) And (Convert.ToDecimal(st_PayInfo.szTxnAmount) > st_PayInfo.dPerTxnAmtLimit))) Then
                    bSendFraudEmail = iSendFraudEmailNotification(st_PayInfo, st_HostInfo)

                    If (True <> bSendFraudEmail) Then
                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > iSendReply2Merchant > Error: Send Fraud Email Fail")
                    Else
                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > iSendReply2Merchant > Send Fraud Email Success")
                    End If
                End If
            End If
        Catch ex As Exception
            iRet = -1

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > iSendReply2Merchant() Exception: " + ex.Message)
        Finally
            'If Not PGMailer Is Nothing Then PGMailer = Nothing 'Added, 24 Jan 2014
            'If Not PGMailerStatus Is Nothing Then PGMailerStatus = Nothing 'Added, 24 Jan 2014
        End Try

        Return iRet
    End Function

    '********************************************************************************************
    ' Class Name:		Response
    ' Function Name:	szGetReplyMerchantHTTPString
    ' Function Type:	String
    ' Parameter:		None
    ' Description:		Form HTTP string to reply merchant
    ' History:			18 Nov 2008
    '                   Modified 20 May 2016, added sz_LogString parameter
    '********************************************************************************************
    Public Function szGetReplyMerchantHTTPString(ByRef st_PayInfo As Common.STPayInfo, ByVal st_HostInfo As Common.STHost, Optional ByRef sz_LogString As String = "") As String
        Dim szHTTPString As String = ""
        Dim szParamString As String = ""    'Added, 16 Jan 2014
        Dim szTxnAmount As String = ""      'Added, 2 Sept 2014, for FX
        Dim szCurrencyCode As String = ""   'Added, 2 Sept 2014, for FX
        Dim szCardType As String = ""       'Added, 9 Oct 2015, for MyDin

        'Added, 1 Aug 2014, generate One-Click Payment token
        'Modified 11 Aug 2014, added ToLower() when generate One-Click Token
        'Modified 12 Aug 2014, added checking of szCustOCP for shopper's consent to save card info
        st_PayInfo.szToken = ""

        'Modified  4 Apr 2017. Generate token for success auth transaction.
        If ("CC" = st_PayInfo.szPymtMethod.ToUpper() And 1 <= st_PayInfo.iAllowOCP And "1" = st_PayInfo.szCustOCP And
                (Common.TXN_STATUS.TXN_SUCCESS = st_PayInfo.iTxnStatus Or Common.TXN_STATUS.TXN_AUTH_SUCCESS = st_PayInfo.iTxnStatus)) Then
            If (1 = st_PayInfo.iAllowOCP) Then
                'Modified 17 May 2016, generate OCP token based on clear CardPAN instead of masked CardPAN
                st_PayInfo.szToken = objHash.szGetHash(st_PayInfo.szMerchantID.ToLower() + st_PayInfo.szClearCardPAN + st_PayInfo.szCustEmail.ToLower(), "MD5", st_PayInfo, st_HostInfo)
            ElseIf (3 = st_PayInfo.iAllowOCP) Then  'Added, 12 Feb 2018, Tik FX
                st_PayInfo.szToken = objHash.szGetHash(st_PayInfo.szMerchantID.ToLower() + st_PayInfo.szClearCardPAN + st_PayInfo.szCustPhone, "MD5", st_PayInfo, st_HostInfo)  'Added 12 Feb 2018, Tik FX
            Else
                'Modified 2 Aug 2017, added checking of szToken
                If ("" = st_PayInfo.szToken) Then
                    'Add  8 Aug 2017, to check credit card exists in DB
                    If True <> objTxnProc.bGetOCPToken(st_PayInfo, 1) Then
                        'Add  for Sunlife Token format YYYYMMDD{ 12 digit for running number}
                        If True <> objTxnProc.bGetTokenRunningNo(st_PayInfo, st_PayInfo.iAllowOCP) Then
                            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                            st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + "> Error: " + st_PayInfo.szErrDesc)
                        Else
                            Dim iTokenLen As Integer = 0
                            iTokenLen = st_PayInfo.iTxnIDMaxLen - st_PayInfo.szRunningNo.Length
                            If (iTokenLen > 0) Then
                                st_PayInfo.szToken = System.DateTime.Now.ToString("yyyyMMdd") + "".PadRight(iTokenLen, "0") + st_PayInfo.szRunningNo
                            Else
                                st_PayInfo.szToken = System.DateTime.Now.ToString("yyyyMMdd") + st_PayInfo.szRunningNo
                            End If
                        End If
                    Else
                        'Clear Card Expiry
                        st_PayInfo.szParam3 = objHash.szDecryptAES(st_PayInfo.szParam3, Common.C_3DESAPPKEY)

                        'Added, 15 Aug 2014, overwrite the retrieved Card Expiry with the keyed in Card Expiry if different and update Token_OCP table
                        If (Trim(st_PayInfo.szCardExp) <> st_PayInfo.szParam3) Then
                            st_PayInfo.szParam3 = st_PayInfo.szCardExp
                            'Update CardExp in TOKEN_OCP
                            objTxnProc.bUpdateOCPToken(st_PayInfo, objHash.szEncryptAES(st_PayInfo.szParam3, Common.C_3DESAPPKEY), 1)
                        End If
                    End If
                End If
            End If
            'Added, 27 Jan 2016
            st_PayInfo.szToken = st_PayInfo.szToken.Replace("+", "").Replace(" ", "")
        End If

        'Added, 2 Sept 2014, checking of FXCurrencyCode
        If ("" = st_PayInfo.szFXCurrencyCode) Then
            szTxnAmount = st_PayInfo.szTxnAmount
            szCurrencyCode = st_PayInfo.szCurrencyCode
        Else
            szTxnAmount = st_PayInfo.szBaseTxnAmount        'For FX, returns original TxnAmount in payment response,
            szCurrencyCode = st_PayInfo.szBaseCurrencyCode  'instead of FXTxnAmount stored in st_PayInfo.szTxnAmount assigned in bVerifyResTxn()
        End If

        'Added, 9 Oct 2015, for MyDin
        If (st_PayInfo.szMaskedCardPAN <> "") Then
            If ("4" = Left(st_PayInfo.szMaskedCardPAN, 1)) Then
                szCardType = "VISA"
            ElseIf ("5" = Left(st_PayInfo.szMaskedCardPAN, 1) Or "2" = Left(st_PayInfo.szMaskedCardPAN, 1)) Then    'Modified 20 May 2016, Or "2", for 2 series MasterCard
                szCardType = "MASTERCARD"
            ElseIf ("34" = Left(st_PayInfo.szMaskedCardPAN, 2) Or "37" = Left(st_PayInfo.szMaskedCardPAN, 2)) Then
                szCardType = "AMEX"
            ElseIf ("36" = Left(st_PayInfo.szMaskedCardPAN, 2) Or "38" = Left(st_PayInfo.szMaskedCardPAN, 2)) Then
                szCardType = "DINERS"
            ElseIf ("35" = Left(st_PayInfo.szMaskedCardPAN, 2)) Then
                szCardType = "JCB"
            End If

            'Added, 20 May 2016, for MyDin Card Type
            If (2 = st_PayInfo.szCardType.Length()) Then
                szCardType = st_PayInfo.szCardType
            End If
        End If

        'If host replies using Redirect/Page-to-page method, Gateway reply to merchant also needs to be redirected to merchant
        'by returning a page which will auto submit these hidden fields to merchant server, better than using Response.Redirect
        'which will cause these fields to be visible on client's browser during redirection.
        If (1 = st_HostInfo.iHostReplyMethod Or 4 = st_HostInfo.iHostReplyMethod) Then
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "TRACE-res-" + st_PayInfo.szHashValue2)

            szHTTPString = "<INPUT type='hidden' name='TxnType' value='" + Server.UrlEncode(st_PayInfo.szTxnType) + "'>" + vbCrLf
            szHTTPString = szHTTPString + "<INPUT type='hidden' name='Method' value='" + st_PayInfo.szPymtMethod + "'>" + vbCrLf     'Added, 16 Aug 2013; Modified 9 Dec 2013, add Method as variable instead of 'CC'
            szHTTPString = szHTTPString + "<INPUT type='hidden' name='MerchantID' value='" + st_PayInfo.szMerchantID + "'>" + vbCrLf
            szHTTPString = szHTTPString + "<INPUT type='hidden' name='MerchantPymtID' value='" + st_PayInfo.szMerchantTxnID + "'>" + vbCrLf
            szHTTPString = szHTTPString + "<INPUT type='hidden' name='MerchantOrdID' value='" + Server.UrlEncode(st_PayInfo.szMerchantOrdID) + "'>" + vbCrLf    'Added, 16 Aug 2013

            'Modified 2 Sept 2014, use szTxnAmount and szCurrencyCode instead of st_PayInfo.szTxnAmount, for FX
            szHTTPString = szHTTPString + "<INPUT type='hidden' name='MerchantTxnAmt' value='" + szTxnAmount + "'>" + vbCrLf
            szHTTPString = szHTTPString + "<INPUT type='hidden' name='MerchantCurrCode' value='" + szCurrencyCode + "'>" + vbCrLf

            'szHTTPString = szHTTPString + "<INPUT type='hidden' name='HashMethod' value='" + Server.UrlEncode(st_PayInfo.szHashMethod) + "'>" + vbCrLf 'Modified 10 Aug 2013, due to SHA1 and MD5 are not secure enough, only allow SHA256
            'szHTTPString = szHTTPString + "<INPUT type='hidden' name='HashValue' value='" + st_PayInfo.szHashValue + "'>" + vbCrLf
            szHTTPString = szHTTPString + "<INPUT type='hidden' name='Sign' value='" + st_PayInfo.szHashValue2 + "'>" + vbCrLf                          'Added, 28 Apr 2016, response hash value 2 which includes OrderNumber and Param6 because some plugins like Magento update order based on these 2 fields
            szHTTPString = szHTTPString + "<INPUT type='hidden' name='PTxnID' value='" + st_PayInfo.szTxnID + "'>" + vbCrLf
            'Added by OM, 7 May 2019. CC's AcqBank return value
            If ("CC" = st_PayInfo.szPymtMethod) Then
                szHTTPString = szHTTPString + "<INPUT type='hidden' name='AcqBank' value='" + Server.UrlEncode(Common.C_TXN_CC_ACQBANK) + "'>" + vbCrLf    'Added, 16 Aug 2013
            Else
                szHTTPString = szHTTPString + "<INPUT type='hidden' name='AcqBank' value='" + Server.UrlEncode(st_PayInfo.szIssuingBank) + "'>" + vbCrLf    'Added, 16 Aug 2013
            End If
            szHTTPString = szHTTPString + "<INPUT type='hidden' name='PTxnStatus' value='" + st_PayInfo.iMerchantTxnStatus.ToString() + "'>" + vbCrLf

            'Commented  23 Aug 2013
            'If (st_PayInfo.szHostTxnStatus <> "") Then
            '    szHTTPString = szHTTPString + "<INPUT type='hidden' name='RespCode' value='" + Server.UrlEncode(st_PayInfo.szHostTxnStatus) + "'>" + vbCrLf
            'Else
            '    szHTTPString = szHTTPString + "<INPUT type='hidden' name='RespCode' value='" + Server.UrlEncode(st_PayInfo.szRespCode) + "'>" + vbCrLf
            'End If

            If ("" <> st_PayInfo.szAuthCode) Then
                szHTTPString = szHTTPString + "<INPUT type='hidden' name='AuthCode' value='" + Server.UrlEncode(st_PayInfo.szAuthCode) + "'>" + vbCrLf
            End If

            szHTTPString = szHTTPString + "<INPUT type='hidden' name='BankRefNo' value='" + Server.UrlEncode(st_PayInfo.szHostTxnID) + "'>" + vbCrLf    'Added, 10 Sept 2013

            'Added, 7 Oct 2015, returning card data, requested by MyDin
            If (1 = st_PayInfo.iReturnCardData And ("CC" = st_PayInfo.szPymtMethod Or "MO" = st_PayInfo.szPymtMethod)) Then 'Modified 22 May 2017, added checking of CC
                szHTTPString = szHTTPString + "<INPUT type='hidden' name='CardNoMask' value='" + st_PayInfo.szMaskedCardPAN + "'>" + vbCrLf
                szHTTPString = szHTTPString + "<INPUT type='hidden' name='CardType' value='" + szCardType + "'>" + vbCrLf
                'szHTTPString = szHTTPString + "<INPUT type='hidden' name='CardExp' value='" + st_PayInfo.szCardExp + "'>" + vbCrLf 'Commented  20 May 2016, moved to below
                szHTTPString = szHTTPString + "<INPUT type='hidden' name='CardHolder' value='" + st_PayInfo.szCardHolder + "'>" + vbCrLf  'Modified 22 May 2017, removed EscapeDataString
            End If

            'Added, 1 Aug 2014, One-Click Payment
            If (st_PayInfo.szToken <> "") Then
                szHTTPString = szHTTPString + "<INPUT type='hidden' name='TokenType' value='OCP'>" + vbCrLf
                szHTTPString = szHTTPString + "<INPUT type='hidden' name='Token' value='" + st_PayInfo.szToken + "'>" + vbCrLf
            End If
            'Added 30 Nov 2015, Promotion
            If (st_PayInfo.szPromoCode <> "") Then
                szHTTPString = szHTTPString + "<INPUT type='hidden' name='PromoCode' value='" + st_PayInfo.szPromoCode + "'>" + vbCrLf
                szHTTPString = szHTTPString + "<INPUT type='hidden' name='PromoOriAmt' value='" + st_PayInfo.szPromoOriAmt + "'>" + vbCrLf
            End If

            'Added  6 Nov 2015, Param67
            If ("" <> st_PayInfo.szParam6) Then
                szHTTPString = szHTTPString + "<INPUT type='hidden' name='Param6' value='" + Server.UrlEncode(st_PayInfo.szParam6) + "'>" + vbCrLf
            End If
            If ("" <> st_PayInfo.szParam7) Then
                szHTTPString = szHTTPString + "<INPUT type='hidden' name='Param7' value='" + Server.UrlEncode(st_PayInfo.szParam7) + "'>" + vbCrLf
            End If

            'Added  24 May 2016, Installment
            If ("" <> st_PayInfo.szInstallMthTerm) Then
                szHTTPString = szHTTPString + "<INPUT type='hidden' name='EPPMonth' value='" + Server.UrlEncode(st_PayInfo.szInstallMthTerm) + "'>" + vbCrLf
                szHTTPString = szHTTPString + "<INPUT type='hidden' name='EPP_YN' value='1'>" + vbCrLf
            End If

            'Added, 9 May 2017, added RespTime as requested by PH merchant
            'szHTTPString = szHTTPString + "<INPUT type='hidden' name='RespTime' value='" + st_PayInfo.szTxnDateTime + "'>" + vbCrLf    'Modified 22 May 2017, removed UrlEncode
            szHTTPString = szHTTPString + "<INPUT type='hidden' name='PTxnMsg' value='" + st_PayInfo.szTxnMsg + "'>" + vbCrLf       'Modified 22 May 2017, removed EscapeDataString

            'Added, 20 May 2016, do not log clear card expiry
            sz_LogString = szHTTPString

            'Added, 20 May 2016, moved from above to here, to avoid being logged
            If (1 = st_PayInfo.iReturnCardData And "CC" = st_PayInfo.szPymtMethod) Then 'Modified 22 May 2017, added checking of CC
                'Added, 20 May 2016, do not log clear card expiry
                sz_LogString = sz_LogString + "<INPUT type='hidden' name='CardExp' value='XXXX'>" + vbCrLf

                szHTTPString = szHTTPString + "<INPUT type='hidden' name='CardExp' value='" + st_PayInfo.szCardExp + "'>" + vbCrLf
            End If

            'Added, 16 Jan 2014, cater to return additional parameters from MerchantRURL
            'Commented  22 Jan 2014, not required since Form action="URL?param1=value1" is also valid,
            'if excluded it from the action URL and include in hidden field, the action URL could be not able to get the value
            'Example: 'http://www.systechadvisor.com/demo/opencart/index.php?route=payment/ghl/
            'Example: 'http://www.systechadvisor.com/demo/opencart/index.php?route=payment/ghl/&ref=2
            'Note: ";" (indicates "&") in szMerchantReturnURL had been replaced with "&" in Payment.vb's bGetSaleReqData()
            'If (st_PayInfo.szMerchantReturnURL.IndexOf("?") > 0) Then
            '    szReturnURL = Split(st_PayInfo.szMerchantReturnURL, "?")
            '    szParamString = szReturnURL(1)                      'route=payment/ghl/ or route=payment/ghl/&ref=2
            '    szFieldValue = Split(szParamString, "&")

            '    For Each szFieldValueEx In szFieldValue             '0:route=payment/ghl/ or 0:route=payment/ghl/ 1:ref=2
            '        szParamPair = Split(szFieldValueEx, "=")
            '        szHTTPString = szHTTPString + "<INPUT type='hidden' name='" + szParamPair(0) + "' value='" + szParamPair(1) + "'>" + vbCrLf
            '    Next
            'End If

            'szHTTPString = szHTTPString + "<INPUT type='hidden' name='SessionID' value='" + Server.UrlEncode(st_PayInfo.szSessionID) + "'>" + vbCrLf   'Modified 10 Aug 2013, unused field

            'szHTTPString = szHTTPString + "<INPUT type='hidden' name='MerchantOrdID' value='" + Server.UrlEncode(st_PayInfo.szMerchantOrdID) + "'>" + vbCrLf
            'szHTTPString = szHTTPString + "<INPUT type='hidden' name='BaseTxnAmount' value='" + st_PayInfo.szBaseTxnAmount + "'>" + vbCrLf
            'szHTTPString = szHTTPString + "<INPUT type='hidden' name='BaseCurrencyCode' value='" + st_PayInfo.szBaseCurrencyCode + "'>" + vbCrLf
            'szHTTPString = szHTTPString + "<INPUT type='hidden' name='Param1' value='" + Server.UrlEncode(st_PayInfo.szParam1) + "'>" + vbCrLf
            'szHTTPString = szHTTPString + "<INPUT type='hidden' name='Param2' value='" + Server.UrlEncode(st_PayInfo.szParam2) + "'>" + vbCrLf
            'szHTTPString = szHTTPString + "<INPUT type='hidden' name='Param3' value='" + Server.UrlEncode(st_PayInfo.szParam3) + "'>" + vbCrLf
            'szHTTPString = szHTTPString + "<INPUT type='hidden' name='Param4' value='" + Server.UrlEncode(st_PayInfo.szParam4) + "'>" + vbCrLf
            'szHTTPString = szHTTPString + "<INPUT type='hidden' name='Param5' value='" + Server.UrlEncode(st_PayInfo.szParam5) + "'>" + vbCrLf
            'szHTTPString = szHTTPString + "<INPUT type='hidden' name='TxnState' value='" + Server.UrlEncode(st_PayInfo.iTxnState.ToString()) + "'>" + vbCrLf
        Else
            ''''szHTTPString = "TxnType=" + Server.UrlEncode(st_PayInfo.szTxnType) + "&"
            ''''szHTTPString = szHTTPString + "Method=" + st_PayInfo.szPymtMethod + "&"                         'Added, 16 Aug 2013; Modified 9 Dec 2013, add Method as variable instead of 'CC'
            ''''szHTTPString = szHTTPString + "MerchantID=" + st_PayInfo.szMerchantID + "&"
            ''''szHTTPString = szHTTPString + "MerchantPymtID=" + st_PayInfo.szMerchantTxnID + "&"
            ''''szHTTPString = szHTTPString + "MerchantOrdID=" + Server.UrlEncode(st_PayInfo.szMerchantOrdID) + "&"     'Added, 16 Aug 2013

            '''''Modified 2 Sept 2014, use szTxnAmount and szCurrencyCode instead of st_PayInfo.szTxnAmount, for FX
            ''''szHTTPString = szHTTPString + "MerchantTxnAmt=" + szTxnAmount + "&"
            ''''szHTTPString = szHTTPString + "MerchantCurrCode=" + Server.UrlEncode(szCurrencyCode) + "&"

            '''''szHTTPString = szHTTPString + "HashMethod=" + Server.UrlEncode(st_PayInfo.szHashMethod) + "&" 'Modified 10 Aug 2013, due to SHA1 and MD5 are not secure enough, only allow SHA256
            '''''szHTTPString = szHTTPString + "HashValue=" + st_PayInfo.szHashValue + "&"   'Modified 10 Aug 2013, moved up here and added + "&"
            ''''szHTTPString = szHTTPString + "Sign=" + st_PayInfo.szHashValue2 + "&"   'Added, 28 Apr 2016, response hash value 2 which includes OrderNumber and Param6 because some plugins like Magento update order based on these 2 fields
            ''''szHTTPString = szHTTPString + "PTxnID=" + st_PayInfo.szTxnID + "&"
            ''''szHTTPString = szHTTPString + "AcqBank=" + Server.UrlEncode(st_PayInfo.szIssuingBank) + "&"     'Added, 16 Aug 2013
            ''''szHTTPString = szHTTPString + "PTxnStatus=" + st_PayInfo.iMerchantTxnStatus.ToString() + "&"
            '''''Commented  23 Aug 2013
            '''''If (st_PayInfo.szHostTxnStatus <> "") Then
            '''''    szHTTPString = szHTTPString + "RespCode=" + Server.UrlEncode(st_PayInfo.szHostTxnStatus) + "&"
            '''''Else
            '''''    szHTTPString = szHTTPString + "RespCode=" + Server.UrlEncode(st_PayInfo.szRespCode) + "&"   'Modified 10 Aug 2013, added + "&"
            '''''End If
            ''''szHTTPString = szHTTPString + "AuthCode=" + Server.UrlEncode(st_PayInfo.szAuthCode) + "&"
            ''''szHTTPString = szHTTPString + "BankRefNo=" + Server.UrlEncode(st_PayInfo.szHostTxnID) + "&"     'Added, 10 Sept 2013

            g_JSONResp.Add("TxnType", st_PayInfo.szTxnType)
            g_JSONResp.Add("Method", st_PayInfo.szPymtMethod)
            g_JSONResp.Add("MerchantID", st_PayInfo.szMerchantID)
            g_JSONResp.Add("MerchantPymtID", st_PayInfo.szMerchantTxnID)
            g_JSONResp.Add("MerchantOrdID", st_PayInfo.szMerchantOrdID)
            'Modified 10 Apr 2019, MerchantTxnAmt and MerchantCurrCode should follow FX Currency Checking. If FX Currency = 1, should return base currency and base amount; else return currency and txn amount
            'g_JSONResp.Add("MerchantTxnAmt", st_PayInfo.szTxnAmount)
            'g_JSONResp.Add("MerchantCurrCode", st_PayInfo.szCurrencyCode)
            g_JSONResp.Add("MerchantTxnAmt", szTxnAmount)
            g_JSONResp.Add("MerchantCurrCode", szCurrencyCode)

            g_JSONResp.Add("PTxnID", st_PayInfo.szTxnID)
            g_JSONResp.Add("PTxnStatus", st_PayInfo.iMerchantTxnStatus.ToString())
            g_JSONResp.Add("PTxnMsg", System.Uri.EscapeDataString(st_PayInfo.szTxnMsg))
            'Added by OM, 7 May 2019. CC's AcqBank return value
            If ("CC" = st_PayInfo.szPymtMethod) Then
                g_JSONResp.Add("AcqBank", Common.C_TXN_CC_ACQBANK)
            Else
                g_JSONResp.Add("AcqBank", st_PayInfo.szIssuingBank)
            End If
            g_JSONResp.Add("BankRefNo", st_PayInfo.szHostTxnID)
            g_JSONResp.Add("Sign", st_PayInfo.szHashValue2)

            If ("" <> st_PayInfo.szAuthCode) Then
                'szHTTPString = szHTTPString + "AuthCode=" + Server.UrlEncode(st_PayInfo.szAuthCode) + "&"
                g_JSONResp.Add("AuthCode", st_PayInfo.szAuthCode)
            End If

            'Added, 1 Aug 2014, One-Click Payment
            If (st_PayInfo.szToken <> "") Then
                'szHTTPString = szHTTPString + "TokenType=OCP&"
                'szHTTPString = szHTTPString + "Token=" + st_PayInfo.szToken + "&"
                g_JSONResp.Add("TokenType", "OCP")
                g_JSONResp.Add("Token", st_PayInfo.szToken)
            End If
            'Added 30 Nov 2015, Promotion
            If (st_PayInfo.szPromoCode <> "") Then
                'szHTTPString = szHTTPString + "PromoCode=" + st_PayInfo.szPromoCode + "&"       'Modified 28 Apr 2016, replaced  hidden field because this is supposed to be GET string
                'szHTTPString = szHTTPString + "PromoOriAmt=" + st_PayInfo.szPromoOriAmt + "&"   'Modified 28 Apr 2016, replaced  hidden field because this is supposed to be GET string
                g_JSONResp.Add("PromoCode", st_PayInfo.szPromoCode)
                g_JSONResp.Add("PromoOriAmt", st_PayInfo.szPromoOriAmt)
            End If

            'Added  6 Nov 2015, Param67
            If ("" <> st_PayInfo.szParam6) Then
                'szHTTPString = szHTTPString + "Param6=" + Server.UrlEncode(st_PayInfo.szParam6) + "&"
                g_JSONResp.Add("Param6", st_PayInfo.szParam6)
            End If
            If ("" <> st_PayInfo.szParam7) Then
                'szHTTPString = szHTTPString + "Param7=" + Server.UrlEncode(st_PayInfo.szParam7) + "&"
                g_JSONResp.Add("Param7", st_PayInfo.szParam7)
            End If

            'Added  23 May 2016, Installment S2S. If MthTerm not empty will return this 2 fields
            If ("" <> st_PayInfo.szInstallMthTerm) Then
                'szHTTPString = szHTTPString + "EPPMonth=" + Server.UrlEncode(st_PayInfo.szInstallMthTerm) + "&"
                'szHTTPString = szHTTPString + "EPP_YN=1&"
                g_JSONResp.Add("EPPMonth", st_PayInfo.szInstallMthTerm)
                g_JSONResp.Add("EPP_YN", "1")
            End If

            'Added, 9 May 2017, added RespTime as requested by PH merchant
            'szHTTPString = szHTTPString + "RespTime=" + System.Uri.EscapeDataString(st_PayInfo.szTxnDateTime) + "&" 'Modified 22 May 2017, changed UrlEncode to EscapeDataString
            'szHTTPString = szHTTPString + "PTxnMsg=" + System.Uri.EscapeDataString(st_PayInfo.szTxnMsg)          'Modified 10 Aug 2013, removed + "&". Modified 15 May 2017, changed UrlEncode to EscapeDataString, to replace space to %20, else will be +

            'Added, 7 Oct 2015, returning card data, requested by MyDin
            If (1 = st_PayInfo.iReturnCardData And ("CC" = st_PayInfo.szPymtMethod Or "MO" = st_PayInfo.szPymtMethod)) Then 'Modified 22 May 2017, added checking of CC
                'szHTTPString = szHTTPString + "&CardNoMask=" + st_PayInfo.szMaskedCardPAN
                'szHTTPString = szHTTPString + "&CardType=" + szCardType
                g_JSONResp.Add("CardNoMask", st_PayInfo.szMaskedCardPAN)
                g_JSONResp.Add("CardType", szCardType)
                'szHTTPString = szHTTPString + "CardExp=" + st_PayInfo.szCardExp + "&"  'Commented  20 May 2016, moved to below
                If (st_PayInfo.szCardHolder <> "") Then
                    'szHTTPString = szHTTPString + "&CardHolder=" + System.Uri.EscapeDataString(st_PayInfo.szCardHolder)  'Modified 15 May 2017, changed UrlEncode to EscapeDataString, to replace space to %20, else will be +
                    g_JSONResp.Add("CardHolder", System.Uri.EscapeDataString(st_PayInfo.szCardHolder))
                End If
            End If

            'Added, 20 May 2016, do not log clear card expiry
            'sz_LogString = szHTTPString
            g_JSONLog = g_JSONResp

            If (1 = st_PayInfo.iReturnCardData And "CC" = st_PayInfo.szPymtMethod) Then 'Modified 22 May 2017, added checking of CC
                'Added, 20 May 2016, do not log clear card expiry
                'sz_LogString = sz_LogString + "&CardExp=XXXX"
                g_JSONLog.Add("CardExp", "XXXX")
                'szHTTPString = szHTTPString + "&CardExp=" + st_PayInfo.szCardExp  'Commented  20 May 2016, moved to below
                g_JSONResp.Add("CardExp", st_PayInfo.szCardExp)
            End If

            sz_LogString = objJS.Serialize(g_JSONLog)
            szHTTPString = Server.UrlEncode(objJS.Serialize(g_JSONResp))

            'szHTTPString = szHTTPString + "MerchantOrdID=" + Server.UrlEncode(st_PayInfo.szMerchantOrdID) + "&"
            'szHTTPString = szHTTPString + "BaseTxnAmount=" + st_PayInfo.szBaseTxnAmount + "&"
            'szHTTPString = szHTTPString + "BaseCurrencyCode=" + Server.UrlEncode(st_PayInfo.szBaseCurrencyCode) + "&"
            'szHTTPString = szHTTPString + "Param1=" + Server.UrlEncode(st_PayInfo.szParam1) + "&"
            'szHTTPString = szHTTPString + "Param2=" + Server.UrlEncode(st_PayInfo.szParam2) + "&"
            'szHTTPString = szHTTPString + "Param3=" + Server.UrlEncode(st_PayInfo.szParam3) + "&"
            'szHTTPString = szHTTPString + "Param4=" + Server.UrlEncode(st_PayInfo.szParam4) + "&"
            'szHTTPString = szHTTPString + "Param5=" + Server.UrlEncode(st_PayInfo.szParam5) + "&"
            'szHTTPString = szHTTPString + "TxnState=" + Server.UrlEncode(st_PayInfo.iTxnState.ToString()) + "&"
            'szHTTPString = szHTTPString + "BankRefNo=" + Server.UrlEncode(st_PayInfo.szHostTxnID) + "&"
        End If

        Return szHTTPString
    End Function

    '********************************************************************************************
    ' Class Name:		Response
    ' Function Name:	szGetReplyMerchantCallbackHTTPString
    ' Function Type:	String
    ' Parameter:		None
    ' Description:		Form HTTP string to reply merchant
    ' History:			7 Oct 2014
    '                   Modified 20 May 2016, added sz_LogString parameter
    '********************************************************************************************
    Public Function szGetReplyMerchantCallbackHTTPString(ByRef st_PayInfo As Common.STPayInfo, ByVal st_HostInfo As Common.STHost, Optional ByRef sz_LogString As String = "") As String
        Dim szHTTPString As String = ""
        Dim szParamString As String = ""    'Added, 16 Jan 2014
        Dim szTxnAmount As String = ""      'Added, 2 Sept 2014, for FX
        Dim szCurrencyCode As String = ""   'Added, 2 Sept 2014, for FX
        Dim szCardType As String = ""       'Added, 9 Oct 2015, for MyDin

        'Added, 1 Aug 2014, generate One-Click Payment token
        'Modified 11 Aug 2014, added ToLower() when generate One-Click Token
        'Modified 12 Aug 2014, added checking of szCustOCP for shopper's consent to save card info

        'Modified 2 Aug 2017, commented. Uncomment on 4 May 2018 due to FPX return OCP and token=01
        st_PayInfo.szToken = ""

        'Modified  4 Apr 2017. Generate token for success auth transaction.
        If ("CC" = st_PayInfo.szPymtMethod.ToUpper() And 1 <= st_PayInfo.iAllowOCP And "1" = st_PayInfo.szCustOCP And
                (Common.TXN_STATUS.TXN_SUCCESS = st_PayInfo.iTxnStatus Or Common.TXN_STATUS.TXN_AUTH_SUCCESS = st_PayInfo.iTxnStatus)) Then
            If (1 = st_PayInfo.iAllowOCP) Then
                'Modified 1 Sept 2016, same like szGetReplyMerchantHTTPString modified on 1 May 2016,
                'generate OCP token based on clear CardPAN instead of masked CardPAN
                st_PayInfo.szToken = objHash.szGetHash(st_PayInfo.szMerchantID.ToLower() + st_PayInfo.szClearCardPAN + st_PayInfo.szCustEmail.ToLower(), "MD5", st_PayInfo, st_HostInfo)
            ElseIf (3 = st_PayInfo.iAllowOCP) Then  'Added, 12 Feb 2018, Tik FX
                st_PayInfo.szToken = objHash.szGetHash(st_PayInfo.szMerchantID.ToLower() + st_PayInfo.szClearCardPAN + st_PayInfo.szCustPhone, "MD5", st_PayInfo, st_HostInfo)  'Added 12 Feb 2018, Tik FX
            Else
                'Modified 2 Aug 2017, added checking of szToken
                If ("" = st_PayInfo.szToken) Then
                    'Add  8 Aug 2017, to check credit card exists in DB
                    If True <> objTxnProc.bGetOCPToken(st_PayInfo, 1) Then
                        'Add  for Sunlife Token format YYYYMMDD{ 12 digit for running number}
                        If True <> objTxnProc.bGetTokenRunningNo(st_PayInfo, st_PayInfo.iAllowOCP) Then
                            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                            st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + "> Error: " + st_PayInfo.szErrDesc)
                        Else
                            Dim iTokenLen As Integer = 0
                            iTokenLen = st_PayInfo.iTxnIDMaxLen - st_PayInfo.szRunningNo.Length
                            If (iTokenLen > 0) Then
                                st_PayInfo.szToken = System.DateTime.Now.ToString("yyyyMMdd") + "".PadRight(iTokenLen, "0") + st_PayInfo.szRunningNo
                            Else
                                st_PayInfo.szToken = System.DateTime.Now.ToString("yyyyMMdd") + st_PayInfo.szRunningNo
                            End If
                        End If
                    Else
                        'Clear Card Expiry
                        st_PayInfo.szParam3 = objHash.szDecryptAES(st_PayInfo.szParam3, Common.C_3DESAPPKEY)

                        'Added, 15 Aug 2014, overwrite the retrieved Card Expiry with the keyed in Card Expiry if different and update Token_OCP table
                        If (Trim(st_PayInfo.szCardExp) <> st_PayInfo.szParam3) Then
                            st_PayInfo.szParam3 = st_PayInfo.szCardExp
                            'Update CardExp in TOKEN_OCP
                            objTxnProc.bUpdateOCPToken(st_PayInfo, objHash.szEncryptAES(st_PayInfo.szParam3, Common.C_3DESAPPKEY), 1)
                        End If
                    End If
                End If
            End If
            'Added, 27 Jan 2016
            st_PayInfo.szToken = st_PayInfo.szToken.Replace("+", "").Replace(" ", "")
        End If

        'Added, 2 Sept 2014, checking of FXCurrencyCode
        If ("" = st_PayInfo.szFXCurrencyCode) Then
            szTxnAmount = st_PayInfo.szTxnAmount
            szCurrencyCode = st_PayInfo.szCurrencyCode
        Else
            szTxnAmount = st_PayInfo.szBaseTxnAmount        'For FX, returns original TxnAmount in payment response,
            szCurrencyCode = st_PayInfo.szBaseCurrencyCode  'instead of FXTxnAmount stored in st_PayInfo.szTxnAmount assigned in bVerifyResTxn()
        End If

        'Added, 9 Oct 2015, for MyDin
        If (st_PayInfo.szMaskedCardPAN <> "") Then
            If ("4" = Left(st_PayInfo.szMaskedCardPAN, 1)) Then
                szCardType = "VISA"
            ElseIf ("5" = Left(st_PayInfo.szMaskedCardPAN, 1) Or "2" = Left(st_PayInfo.szMaskedCardPAN, 1)) Then    'Modified 20 May 2016, for 2 series MasterCard
                szCardType = "MASTERCARD"
            ElseIf ("34" = Left(st_PayInfo.szMaskedCardPAN, 2) Or "37" = Left(st_PayInfo.szMaskedCardPAN, 2)) Then
                szCardType = "AMEX"
            ElseIf ("36" = Left(st_PayInfo.szMaskedCardPAN, 2) Or "38" = Left(st_PayInfo.szMaskedCardPAN, 2)) Then
                szCardType = "DINERS"
            ElseIf ("35" = Left(st_PayInfo.szMaskedCardPAN, 2)) Then
                szCardType = "JCB"
            End If

            'Added, 20 May 2016, for MyDin Card Type
            If (2 = st_PayInfo.szCardType.Length()) Then
                szCardType = st_PayInfo.szCardType
            End If
        End If

        'If host replies using Redirect/Page-to-page method, Gateway reply to merchant also needs to be redirected to merchant
        'by returning a page which will auto submit these hidden fields to merchant server, better than using Response.Redirect
        'which will cause these fields to be visible on client's browser during redirection.

        ''''szHTTPString = "TxnType=" + Server.UrlEncode(st_PayInfo.szTxnType) + "&"
        ''''szHTTPString = szHTTPString + "Method=" + st_PayInfo.szPymtMethod + "&"                         'Added, 16 Aug 2013; Modified 9 Dec 2013, add Method as variable instead of 'CC'
        ''''szHTTPString = szHTTPString + "MerchantID=" + st_PayInfo.szMerchantID + "&"
        ''''szHTTPString = szHTTPString + "MerchantPymtID=" + Server.UrlEncode(st_PayInfo.szMerchantTxnID) + "&"
        ''''szHTTPString = szHTTPString + "MerchantOrdID=" + Server.UrlEncode(st_PayInfo.szMerchantOrdID) + "&"     'Added, 16 Aug 2013

        '''''Modified 2 Sept 2014, use szTxnAmount and szCurrencyCode instead of st_PayInfo.szTxnAmount, for FX
        ''''szHTTPString = szHTTPString + "MerchantTxnAmt=" + szTxnAmount + "&"
        ''''szHTTPString = szHTTPString + "MerchantCurrCode=" + Server.UrlEncode(szCurrencyCode) + "&"

        '''''szHTTPString = szHTTPString + "HashMethod=" + Server.UrlEncode(st_PayInfo.szHashMethod) + "&" 'Modified 10 Aug 2013, due to SHA1 and MD5 are not secure enough, only allow SHA256
        '''''szHTTPString = szHTTPString + "HashValue=" + st_PayInfo.szHashValue + "&"   'Modified 10 Aug 2013, moved up here and added + "&"
        ''''szHTTPString = szHTTPString + "Sign=" + st_PayInfo.szHashValue2 + "&" 'Added, 28 Apr 2016, response hash value 2 which includes OrderNumber and Param6 because some plugins like Magento update order based on these 2 fields
        ''''szHTTPString = szHTTPString + "PTxnID=" + st_PayInfo.szTxnID + "&"
        ''''szHTTPString = szHTTPString + "AcqBank=" + Server.UrlEncode(st_PayInfo.szIssuingBank) + "&"     'Added, 16 Aug 2013
        ''''szHTTPString = szHTTPString + "PTxnStatus=" + st_PayInfo.iMerchantTxnStatus.ToString() + "&"
        ''''szHTTPString = szHTTPString + "PTxnMsg=" + System.Uri.EscapeDataString(st_PayInfo.szTxnMsg) + "&"              'Modified 10 Aug 2013, removed + "&"   'Modified 15 May 2017, changed UrlEncode to EscapeDataString, to replace space to %20, else will be +
        '''''Commented  23 Aug 2013
        '''''If (st_PayInfo.szHostTxnStatus <> "") Then
        '''''    szHTTPString = szHTTPString + "RespCode=" + Server.UrlEncode(st_PayInfo.szHostTxnStatus) + "&"
        '''''Else
        '''''    szHTTPString = szHTTPString + "RespCode=" + Server.UrlEncode(st_PayInfo.szRespCode) + "&"   'Modified 10 Aug 2013, added + "&"
        '''''End If
        ''''szHTTPString = szHTTPString + "AuthCode=" + Server.UrlEncode(st_PayInfo.szAuthCode) + "&"
        ''''szHTTPString = szHTTPString + "BankRefNo=" + Server.UrlEncode(st_PayInfo.szHostTxnID) + "&"     'Added, 10 Sept 2013

        g_JSONResp.Add("TxnType", st_PayInfo.szTxnType) 'Server.UrlEncode(st_PayInfo.szTxnType))
        g_JSONResp.Add("Method", st_PayInfo.szPymtMethod)
        g_JSONResp.Add("MerchantID", st_PayInfo.szMerchantID)
        g_JSONResp.Add("MerchantPymtID", st_PayInfo.szMerchantTxnID)
        g_JSONResp.Add("MerchantOrdID", st_PayInfo.szMerchantOrdID)
        'Modified 10 Apr 2019, MerchantTxnAmt and MerchantCurrCode should follow FX Currency Checking. If FX Currency = 1, should return base currency and base amount; else return currency and txn amount
        'g_JSONResp.Add("MerchantTxnAmt", st_PayInfo.szTxnAmount)
        'g_JSONResp.Add("MerchantCurrCode", st_PayInfo.szCurrencyCode) 'Server.UrlEncode(st_PayInfo.szCurrencyCode))
        g_JSONResp.Add("MerchantTxnAmt", szTxnAmount)
        g_JSONResp.Add("MerchantCurrCode", Server.UrlEncode(szCurrencyCode))

        g_JSONResp.Add("PTxnID", st_PayInfo.szTxnID)
        g_JSONResp.Add("PTxnStatus", st_PayInfo.iMerchantTxnStatus.ToString())
        g_JSONResp.Add("PTxnMsg", System.Uri.EscapeDataString(st_PayInfo.szTxnMsg))
        'Added by OM, 7 May 2019. CC's AcqBank return value
        If ("CC" = st_PayInfo.szPymtMethod) Then
            g_JSONResp.Add("AcqBank", Common.C_TXN_CC_ACQBANK)
        Else
            g_JSONResp.Add("AcqBank", st_PayInfo.szIssuingBank) 'Server.UrlEncode(st_PayInfo.szIssuingBank))
        End If
        g_JSONResp.Add("BankRefNo", st_PayInfo.szHostTxnID) 'Server.UrlEncode(st_PayInfo.szHostTxnID))
        g_JSONResp.Add("Sign", st_PayInfo.szHashValue2)

        If ("" <> st_PayInfo.szAuthCode) Then
            'szHTTPString = szHTTPString + "AuthCode=" + Server.UrlEncode(st_PayInfo.szAuthCode) + "&"
            g_JSONResp.Add("AuthCode", Server.UrlEncode(st_PayInfo.szAuthCode))
        End If

        'Added, 1 Aug 2014, One-Click Payment
        If (st_PayInfo.szToken <> "") Then
            'szHTTPString = szHTTPString + "TokenType=OCP&"
            'szHTTPString = szHTTPString + "Token=" + st_PayInfo.szToken + "&"
            g_JSONResp.Add("TokenType", "OCP")
            g_JSONResp.Add("Token", st_PayInfo.szToken)
        End If

        'Added 30 Nov 2015, Promotion
        If (st_PayInfo.szPromoCode <> "") Then
            'szHTTPString = szHTTPString + "PromoCode=" + st_PayInfo.szPromoCode + "&"
            'szHTTPString = szHTTPString + "PromoOriAmt=" + st_PayInfo.szPromoOriAmt + "&"
            g_JSONResp.Add("PromoCode", st_PayInfo.szPromoCode)
            g_JSONResp.Add("PromoOriAmt", st_PayInfo.szPromoOriAmt)
        End If
        'Added  6 Nov 2015, Param67
        If ("" <> st_PayInfo.szParam6) Then
            'szHTTPString = szHTTPString + "Param6=" + Server.UrlEncode(st_PayInfo.szParam6) + "&"
            g_JSONResp.Add("Param6", st_PayInfo.szParam6)
        End If
        If ("" <> st_PayInfo.szParam7) Then
            'szHTTPString = szHTTPString + "Param7=" + Server.UrlEncode(st_PayInfo.szParam7) + "&"
            g_JSONResp.Add("Param7", st_PayInfo.szParam7)
        End If

        'szHTTPString = szHTTPString + "RespTime=" + System.Uri.EscapeDataString(st_PayInfo.szTxnDateTime) + "&"     'Added, 12 May 2017, requested by PH merchant. 'Modified 22 May 2017, changed UrlEncode to EscapeDataString

        'Added, 7 Oct 2015, returning card data, requested by MyDin
        If (1 = st_PayInfo.iReturnCardData And ("CC" = st_PayInfo.szPymtMethod Or "MO" = st_PayInfo.szPymtMethod)) Then 'Modified 22 May 2017, added checking of CC
            ''szHTTPString = szHTTPString + "CardNoMask=" + st_PayInfo.szMaskedCardPAN + "&"
            ''szHTTPString = szHTTPString + "CardType=" + szCardType + "&"
            'szHTTPString = szHTTPString + "CardExp=" + st_PayInfo.szCardExp + "&"  'Commented  20 May 2016, moved to below
            g_JSONResp.Add("CardNoMask", st_PayInfo.szMaskedCardPAN)
            g_JSONResp.Add("CardType", szCardType)
            If (st_PayInfo.szCardHolder <> "") Then
                'szHTTPString = szHTTPString + "CardHolder=" + System.Uri.EscapeDataString(st_PayInfo.szCardHolder) + "&"   'Modified 15 May 2017, changed UrlEncode to EscapeDataString, to replace space to %20, else will be +
                g_JSONResp.Add("CardHolder", System.Uri.EscapeDataString(st_PayInfo.szCardHolder))
            End If
        End If
        'Added, 20 May 2016, do not log clear card expiry
        'sz_LogString = szHTTPString
        g_JSONLog = g_JSONResp

        'Added, 20 May 2016, do not log clear card expiry
        If (1 = st_PayInfo.iReturnCardData And "CC" = st_PayInfo.szPymtMethod) Then 'Modified 22 May 2017, added checking of CC
            'sz_LogString = sz_LogString + "CardExp=XXXX"
            g_JSONLog.Add("CardExp", "XXXX")
            'szHTTPString = szHTTPString + "CardExp=" + st_PayInfo.szCardExp  'Commented  20 May 2016, moved to below
            g_JSONResp.Add("CardExp", st_PayInfo.szCardExp)
        End If

        'szHTTPString = szHTTPString + "MerchantOrdID=" + Server.UrlEncode(st_PayInfo.szMerchantOrdID) + "&"
        'szHTTPString = szHTTPString + "BaseTxnAmount=" + st_PayInfo.szBaseTxnAmount + "&"
        'szHTTPString = szHTTPString + "BaseCurrencyCode=" + Server.UrlEncode(st_PayInfo.szBaseCurrencyCode) + "&"
        'szHTTPString = szHTTPString + "Param1=" + Server.UrlEncode(st_PayInfo.szParam1) + "&"
        'szHTTPString = szHTTPString + "Param2=" + Server.UrlEncode(st_PayInfo.szParam2) + "&"
        'szHTTPString = szHTTPString + "Param3=" + Server.UrlEncode(st_PayInfo.szParam3) + "&"
        'szHTTPString = szHTTPString + "Param4=" + Server.UrlEncode(st_PayInfo.szParam4) + "&"
        'szHTTPString = szHTTPString + "Param5=" + Server.UrlEncode(st_PayInfo.szParam5) + "&"
        'szHTTPString = szHTTPString + "TxnState=" + Server.UrlEncode(st_PayInfo.iTxnState.ToString()) + "&"
        'szHTTPString = szHTTPString + "BankRefNo=" + Server.UrlEncode(st_PayInfo.szHostTxnID) + "&"
        sz_LogString = objJS.Serialize(g_JSONLog)
        szHTTPString = Server.UrlEncode(objJS.Serialize(g_JSONResp))

        Return szHTTPString
    End Function

    '********************************************************************************************
    ' Class Name:		Response
    ' Function Name:	szGetReplyMerchantOTCHTTPString
    ' Function Type:	String
    ' Parameter:		None
    ' Description:		Form HTTP string to reply merchant
    ' History:			19 Apr 2015
    '********************************************************************************************
    Public Function szGetReplyHostHTTPString_OTC(ByRef st_PayInfo As Common.STPayInfo, ByVal st_HostInfo As Common.STHost) As String
        Dim szHTTPString As String = ""
        Dim szParamString As String = ""

        'Note: No need Server.UrlEncode because string is replied on the same session to Host without browser involved
        ''szHTTPString = szHTTPString + "MerchantID=" + st_PayInfo.szIssuingBank + "&"
        ''szHTTPString = szHTTPString + "SecureCode=" + st_PayInfo.szOTCSecureCode + "&"
        ''szHTTPString = szHTTPString + "MerchantTxnAmt=" + st_PayInfo.szTxnAmount + "&"
        ''szHTTPString = szHTTPString + "MerchantCurrCode=" + st_PayInfo.szCurrencyCode + "&"
        ''szHTTPString = szHTTPString + "HostTxnID=" + st_PayInfo.szHostTxnID + "&"
        ''szHTTPString = szHTTPString + "MerchantPymtID=" + st_PayInfo.szMerchantTxnID + "&"
        ''szHTTPString = szHTTPString + "PTxnID=" + st_PayInfo.szTxnID + "&"
        ''szHTTPString = szHTTPString + "MerchantOrdID=" + st_PayInfo.szMerchantOrdID + "&"
        ''szHTTPString = szHTTPString + "PTxnStatus=" + st_PayInfo.iMerchantTxnStatus.ToString() + "&"
        ''szHTTPString = szHTTPString + "PTxnMsg=" + st_PayInfo.szTxnMsg + "&"
        ''''szHTTPString = szHTTPString + "HashValue=" + st_PayInfo.szHashValue + "&"
        ''szHTTPString = szHTTPString + "Sign=" + st_PayInfo.szHashValue2   'Added, 28 Apr 2016, response hash value 2 which includes OrderNumber and Param6 because some plugins like Magento update order based on these 2 fields

        g_JSONResp.Add("MerchantID", st_PayInfo.szIssuingBank)
        g_JSONResp.Add("MerchantPymtID", st_PayInfo.szMerchantTxnID)
        g_JSONResp.Add("MerchantOrdID", st_PayInfo.szMerchantOrdID)
        g_JSONResp.Add("MerchantTxnAmt", st_PayInfo.szTxnAmount)
        g_JSONResp.Add("MerchantCurrCode", st_PayInfo.szCurrencyCode)
        g_JSONResp.Add("PTxnID", st_PayInfo.szTxnID)
        g_JSONResp.Add("PTxnStatus", st_PayInfo.iMerchantTxnStatus.ToString())
        g_JSONResp.Add("PTxnMsg", st_PayInfo.szTxnMsg)
        g_JSONResp.Add("SecureCode", st_PayInfo.szOTCSecureCode)
        g_JSONResp.Add("BankRefNo", st_PayInfo.szHostTxnID)
        g_JSONResp.Add("Sign", st_PayInfo.szHashValue2)

        szHTTPString = objJS.Serialize(g_JSONResp)

        Return szHTTPString
    End Function

    '********************************************************************************************
    ' Class Name:		Response
    ' Function Name:	iLoadRedirectUI
    ' Function Type:	Integer
    ' Parameter:		sz_HTML [out, string]   -   Customized content of the web page
    ' Description:		Load the redirect (auto submit hidden fields) template
    ' History:			25 Feb 2009
    '********************************************************************************************
    Public Function iLoadRedirectUI(ByVal sz_URL As String, ByVal sz_HTTPString As String, ByRef sz_HTML As String) As Integer
        Dim iReturn As Integer = 0
        Dim szTemplatePath As String = ""
        Dim szHTMLContent As String = ""

        szTemplatePath = ConfigurationManager.AppSettings("RedirectTemplate")

        If szTemplatePath <> "" Then
            GetTemplate(szTemplatePath, szHTMLContent)

            If szHTMLContent <> "" Then
                objCommon.szFormatPlaceHolder(szHTMLContent, "[HOSTPARAMS]", sz_HTTPString)
                objCommon.szFormatPlaceHolder(szHTMLContent, "[HOSTURL]", sz_URL)

                sz_HTML = szHTMLContent
            Else
                iReturn = -1
                'Response.Write("szHTMLContent==empty")
            End If
        Else
            iReturn = -1
        End If

        Return iReturn
    End Function

    '********************************************************************************************
    ' Class Name:		Response
    ' Function Name:	iLoadRedirectPymtUI
    ' Function Type:	Integer
    ' Parameter:		None
    ' Description:		Load the redirect payment template
    ' History:			28 Mar 2010	Created
    '********************************************************************************************
    Public Function iLoadRedirectPymtUI(ByVal sz_URL As String, ByRef sz_HTML As String) As Integer 'Changed to public  28 Sept 2011, for response_klikpay
        Dim iReturn As Integer = 0
        Dim szTemplatePath As String = ""
        Dim szHTMLContent As String = ""

        szTemplatePath = ConfigurationManager.AppSettings("RedirectPymtTemplate")

        If szTemplatePath <> "" Then
            GetTemplate(szTemplatePath, szHTMLContent)
            If szHTMLContent <> "" Then
                If sz_URL <> "" Then
                    objCommon.szFormatPlaceHolder(szHTMLContent, "[REDIRECTURL]", sz_URL)
                Else
                    Return -1
                End If

                sz_HTML = szHTMLContent
            Else
                iReturn = -1
            End If
        Else
            iReturn = -1
        End If

        Return iReturn
    End Function

    '********************************************************************************************
    ' Class Name:		Response
    ' Function Name:	iLoadReceiptUI
    ' Function Type:	Integer
    ' Parameter:		None
    ' Description:		Load payment receipt template
    ' History:			28 Mar 2010	Created
    '                   1 Jun 2017  Modified, changed from Private to Public to be called by Payment.aspx
    '********************************************************************************************
    Public Function iLoadReceiptUI(ByVal sz_URL As String, ByVal sz_HTTPString As String, ByRef sz_HTML As String, ByVal st_PayInfo As Common.STPayInfo, ByVal st_HostInfo As Common.STHost) As Integer
        Dim iReturn As Integer = 0
        Dim szTemplatePath As String = ""
        Dim szHTMLContent As String = ""
        Dim szTxnStatus As String = ""
        Dim szReceiptTemplate() As String
        Dim szPymtMethod As String = ""         'Added, 9 Dec 2013
        Dim szCCDetails As String = ""          'Added, 9 Dec 2013
        Dim szHideReceipt As String = "true"    'Added, 15 Dec 2013
        Dim szHideMerchantLogo As String = ""
        Dim szHideMerchantAddr As String = ""

        szTemplatePath = ConfigurationManager.AppSettings("ReceiptTemplate")

        If szTemplatePath <> "" Then
            szReceiptTemplate = Split(szTemplatePath, ".")
            szTemplatePath = szReceiptTemplate(0) + "_" + st_PayInfo.szLanguageCode + "." + szReceiptTemplate(1)

            'Modified 12 Aug 2014, checking of GetTemplate, cater for if failed to retrieve template, try retrieve based on default language code
            If (False = GetTemplate(szTemplatePath, szHTMLContent)) Then
                If (False = GetTemplate(szReceiptTemplate(0) + "_" + Common.C_DEFAULT_LANGUAGE_CODE + "." + szReceiptTemplate(1), szHTMLContent)) Then
                    iReturn = -1
                    Return iReturn
                End If
            End If

            If szHTMLContent <> "" Then
                'Modified 31 Mar 2015, modified sz_URL <> "" to bLoadReceipt
                If (sz_URL <> "") Then
                    'Modified 22 Jun 2015, modified 0 to TXN_SUCCESS
                    If (Common.TXN_STATUS.TXN_SUCCESS = st_PayInfo.iTxnStatus) Then
                        szTxnStatus = "Successful"          'Modified 28 Jan 2014, Success to Successful
                        szHideReceipt = "false"             'Added, 15 Dec 2013

                        'Added, 22 Jun 2015, added AUTH_SUCCESS
                    ElseIf (Common.TXN_STATUS.TXN_AUTH_SUCCESS = st_PayInfo.iTxnStatus) Then
                        szTxnStatus = "Pre-Auth Successful"
                        szHideReceipt = "false"

                        'Modified 22 Jun 2005, modified 1 to TXN_FAILED
                    ElseIf (Common.TXN_STATUS.TXN_FAILED = st_PayInfo.iTxnStatus Or Common.TXN_STATUS.INVALID_HOST_REPLY = st_PayInfo.iTxnStatus Or Common.TXN_STATUS.DECLINED_BY_VEENROLL = st_PayInfo.iTxnStatus) Then
                        szTxnStatus = "Failed. Please Try Again."   'Modified 25 Jul 2014, added "Please Try Again."
                    ElseIf (Common.TXN_STATUS.DECLINED_BY_EXT_FDS = st_PayInfo.iTxnStatus) Then        ' 25 May 2017
                        szTxnStatus = "Declined"
                    Else
                        szTxnStatus = "Pending"
                    End If

                    'Added, 9 Dec 2013
                    If ("CC" = st_PayInfo.szPymtMethod) Then
                        If (True = objTxnProc.bGetCardType(st_PayInfo)) Then
                            szPymtMethod = st_PayInfo.szCardTypeDesc
                        End If

                        'Added, 9 Dec 2013
                        szCCDetails = "".PadRight(st_PayInfo.szCardPAN.Length() - 4, "X") + Right(st_PayInfo.szCardPAN, 4)
                        'Commented  28 Sept 2016, new payment page
                        'szCCDetails = "<tr><th>Card Number:</th><td>" + szCCDetails + "</td></tr>"
                        'szCCDetails = szCCDetails + "<tr><th>Cardholder Name:</th><td>" + st_PayInfo.szCardHolder + "</td></tr>"
                        ''Modified 17 Feb 2014, added checking of TxnStatus 0
                        'If (0 = st_PayInfo.iTxnStatus) Then
                        '    szCCDetails = szCCDetails + "<tr><th>APPR Code:</th><td>" + st_PayInfo.szAuthCode + "</td></tr>"
                        'End If
                        'Added, 28 Sept 2016, new payment page's receipt
                        'Modified 12 Jan 2016, added checking of template version
                        If (("templates3" = Left(szTemplatePath, 10).ToLower()) Or ("rttemplates" = Left(szTemplatePath, 11).ToLower())) Then
                            szCCDetails = "<dl class=""dl-horizontal""><dt><strong>Card Number</strong></dt><dd>" + szCCDetails + "</dd></dl>"
                            szCCDetails = szCCDetails + "<dl class=""dl-horizontal""><dt><strong>Cardholder Name</strong></dt><dd>" + st_PayInfo.szCardHolder + "</dd></dl>"
                            If (0 = st_PayInfo.iTxnStatus) Then
                                szCCDetails = szCCDetails + "<dl class=""dl-horizontal""><dt><strong>APPR Code</strong></dt><dd>" + st_PayInfo.szAuthCode + "</dd></dl>"
                            End If
                        Else
                            szCCDetails = "<tr><th>Card Number</th><td>:</td><td>" + szCCDetails + "</td></tr>"
                            szCCDetails = szCCDetails + "<tr><th>Cardholder Name</th><td>:</td><td>" + st_PayInfo.szCardHolder + "</td></tr>"
                            'Modified 17 Feb 2014, added checking of TxnStatus 0
                            If (0 = st_PayInfo.iTxnStatus) Then
                                szCCDetails = szCCDetails + "<tr><th>APPR Code</th><td>:</td><td>" + st_PayInfo.szAuthCode + "</td></tr>"
                            End If
                        End If


                    ElseIf ("OB" = st_PayInfo.szPymtMethod Or "WA" = st_PayInfo.szPymtMethod) Then  'Modified 22 Oct 2014, added "WA" for e-Wallet, e.g. CashU
                        szPymtMethod = st_HostInfo.szDesc

                        'Added, 1 Jun 2015, for FPXD
                        'Modified 22 Jun 2015, added FFFPX
                        If (st_PayInfo.szIssuingBank <> "") Then
                            If ("FPX" = Left(st_PayInfo.szIssuingBank.Trim(), 3).ToUpper() Or ("FFFPX" = st_PayInfo.szIssuingBank.Trim().ToUpper())) Then
                                'Modified 12 Jan 2016, added checking of template version
                                If (("templates3" = Left(szTemplatePath, 10).ToLower()) Or "rttemplates" = Left(szTemplatePath, 11).ToLower()) Then
                                    'Added, 20 Apr 2017, added Seller Order No, Buyer Bank Name and Status, as requested by FPX Guideline
                                    szCCDetails = szCCDetails + "<dl class=""dl-horizontal""><dt><strong>Seller Order No</strong></dt><dd>" + st_PayInfo.szTxnID + "</dd></dl>"
                                    szCCDetails = szCCDetails + "<dl class=""dl-horizontal""><dt><strong>FPX Txn ID</strong></dt><dd>" + st_PayInfo.szHostTxnID + "</dd></dl>"

                                    'Added, 31 May 2017
                                    szReceiptTemplate = Split(st_PayInfo.szTxnMsg, "|")
                                    If (szReceiptTemplate.GetUpperBound(0) > 0 And szReceiptTemplate(0) <> "") Then
                                        szCCDetails = szCCDetails + "<dl class=""dl-horizontal""><dt><strong>Buyer Bank Name</strong></dt><dd>" + szReceiptTemplate(0) + "</dd></dl>"
                                    End If

                                    If (Common.TXN_STATUS.TXN_FAILED = st_PayInfo.iTxnStatus) Then
                                        'If ("48" = st_PayInfo.szRespCode) Then
                                        '    szCCDetails = szCCDetails + "<dl class=""dl-horizontal""><dt><strong>Status</strong></dt><dd>" + "Maximum Transaction Limit Exceeded (MYR 30000.00)" + "</dd></dl>"
                                        'End If

                                        'If ("2A" = st_PayInfo.szRespCode) Then
                                        '    szCCDetails = szCCDetails + "<dl class=""dl-horizontal""><dt><strong>Status</strong></dt><dd>" + "Transaction Amount Is Lower Than Minimum Limit (MYR 1.00)" + "</dd></dl>"
                                        'End If
                                        szCCDetails = szCCDetails + "<dl class=""dl-horizontal""><dt><strong>Status</strong></dt><dd>" + "Failed" + "</dd></dl>"
                                    Else
                                        szCCDetails = szCCDetails + "<dl class=""dl-horizontal""><dt><strong>Status</strong></dt><dd>" + szTxnStatus + "</dd></dl>"
                                    End If
                                Else
                                    szCCDetails = szCCDetails + "<tr><th>FPX Txn ID:</th><td>" + st_PayInfo.szHostTxnID + "</td></tr>"
                                End If
                            End If
                        End If
                    End If

                    If (0 = st_PayInfo.iShowMerchantLogo) Then  ' 04 may 2017
                        szHideMerchantLogo = "hide"             'Bootstrap hide class
                    End If
                    If (0 = st_PayInfo.iShowMerchantAddr) Then  ' 04 may 2017
                        szHideMerchantAddr = "hide"             'Bootstrap hide class
                    End If

                    objCommon.szFormatPlaceHolder(szHTMLContent, "[MERCHANTADDR]", st_PayInfo.szMerchantAddress)            'Added, 14 Dec 2013
                    objCommon.szFormatPlaceHolder(szHTMLContent, "[MERCHANTCONTACTNO]", st_PayInfo.szMerchantContactNo)     'Added, 14 Dec 2013
                    objCommon.szFormatPlaceHolder(szHTMLContent, "[MERCHANTEMAILADDR]", st_PayInfo.szMerchantEmailAddr)     'Added, 14 Dec 2013
                    objCommon.szFormatPlaceHolder(szHTMLContent, "[MERCHANTWEBSITEURL]", st_PayInfo.szMerchantWebSiteURL)   'Added, 14 Dec 2013
                    If ("" <> st_PayInfo.szHostDate And ("FPX" = Left(st_PayInfo.szIssuingBank.Trim(), 3).ToUpper() Or ("FFFPX" = st_PayInfo.szIssuingBank.Trim().ToUpper()))) Then
                        Dim dt As DateTime
                        dt = DateTime.ParseExact(st_PayInfo.szHostDate, "yyyyMMddHHmmss", CultureInfo.CreateSpecificCulture("en-us"))
                        objCommon.szFormatPlaceHolder(szHTMLContent, "[TXNDATETIME]", dt.ToString("dd MMM yyyy hh:mm:ss tt", CultureInfo.CreateSpecificCulture("en-us")))  'Added 6 Nov 2017, cater FPX B2B Receipt Date Time
                    Else
                        objCommon.szFormatPlaceHolder(szHTMLContent, "[TXNDATETIME]", System.DateTime.Now.ToString("dd MMM yyyy hh:mm:ss tt", CultureInfo.CreateSpecificCulture("en-us")))  'Added, 9 Dec 2013; Modified 17 Feb 2014, added CultureInfo or else some machine not able to display AM/PM
                    End If

                    objCommon.szFormatPlaceHolder(szHTMLContent, "[PYMTMETHOD]", szPymtMethod)                                          'Added, 9 Dec 2013
                    objCommon.szFormatPlaceHolder(szHTMLContent, "[CCDETAILS]", szCCDetails)                                            'Added, 9 Dec 2013
                    objCommon.szFormatPlaceHolder(szHTMLContent, "[MERCHANTNAME]", st_PayInfo.szMerchantName)
                    objCommon.szFormatPlaceHolder(szHTMLContent, "[ORDERDESC]", st_PayInfo.szMerchantOrdDesc)
                    objCommon.szFormatPlaceHolder(szHTMLContent, "[TXNAMOUNT]", st_PayInfo.szTxnAmount)
                    objCommon.szFormatPlaceHolder(szHTMLContent, "[CURRENCYCODE]", st_PayInfo.szCurrencyCode)
                    objCommon.szFormatPlaceHolder(szHTMLContent, "[MERCHANTTXNID]", st_PayInfo.szMerchantTxnID)
                    objCommon.szFormatPlaceHolder(szHTMLContent, "[ORDERNUMBER]", st_PayInfo.szMerchantOrdID)                 'Added, 14 Dec 2013
                    objCommon.szFormatPlaceHolder(szHTMLContent, "[GATEWAYTXNID]", st_PayInfo.szTxnID)
                    objCommon.szFormatPlaceHolder(szHTMLContent, "[BANKREFNO]", st_PayInfo.szHostTxnID)                     'Added, 1 Jun 2015
                    objCommon.szFormatPlaceHolder(szHTMLContent, "[ISSUINGBANK]", st_PayInfo.szIssuingBank)
                    objCommon.szFormatPlaceHolder(szHTMLContent, "[HOSTTXNID]", st_PayInfo.szHostTxnID)
                    objCommon.szFormatPlaceHolder(szHTMLContent, "[TXNSTATUS]", szTxnStatus)
                    objCommon.szFormatPlaceHolder(szHTMLContent, "[HOSTPARAMS]", sz_HTTPString)
                    objCommon.szFormatPlaceHolder(szHTMLContent, "[HOSTURL]", sz_URL)
                    objCommon.szFormatPlaceHolder(szHTMLContent, "[REDIRECTMS]", ConfigurationManager.AppSettings("ReceiptRedirectMS"))
                    objCommon.szFormatPlaceHolder(szHTMLContent, "[REDIRECTSEC]", Left(ConfigurationManager.AppSettings("ReceiptRedirectMS"), ConfigurationManager.AppSettings("ReceiptRedirectMS").Length() - 3))
                    objCommon.szFormatPlaceHolder(szHTMLContent, "[ISHIDERECEIPT]", szHideReceipt)                          'Added, 15 Dec 2013
                    objCommon.szFormatPlaceHolder(szHTMLContent, "[MERCHANTID]", st_PayInfo.szMerchantID)                     'Added, 24 Jun 2014, to show Merchant logo on receipt page
                    objCommon.szFormatPlaceHolder(szHTMLContent, "[HIDEMERCHANTLOGO]", szHideMerchantLogo)                  ' 04 may 2017
                    objCommon.szFormatPlaceHolder(szHTMLContent, "[HIDEMERCHANTADDR]", szHideMerchantAddr)                  ' 04 may 2017
                Else
                    Return -1
                End If

                sz_HTML = szHTMLContent
            Else
                iReturn = -1
            End If
        Else
            iReturn = -1
        End If

        Return iReturn
    End Function

    '********************************************************************************************
    ' Class Name:		Response
    ' Function Name:	iLoadReceiptBNBUI
    ' Function Type:	Integer
    ' Parameter:		None
    ' Description:		Load Buy Now Button payment receipt template
    ' History:			8 Apr 2015	Created
    '                   1 Jun 2017  Modified, changed Private to Public to be called by Payment.aspx
    '********************************************************************************************
    Public Function iLoadReceiptBNBUI(ByRef sz_HTML As String, ByVal st_PayInfo As Common.STPayInfo, ByVal st_HostInfo As Common.STHost) As Integer
        Dim iReturn As Integer = 0
        Dim szTemplatePath As String = ""
        Dim szHTMLContent As String = ""
        Dim szTxnStatus As String = ""
        Dim szReceiptTemplate() As String
        Dim szPymtMethod As String = ""         'Added, 9 Dec 2013
        Dim szCCDetails As String = ""          'Added, 9 Dec 2013
        Dim szHideReceipt As String = "true"    'Added, 15 Dec 2013

        szTemplatePath = ConfigurationManager.AppSettings("ReceiptTemplate_BNB")

        If szTemplatePath <> "" Then
            szReceiptTemplate = Split(szTemplatePath, ".")
            szTemplatePath = szReceiptTemplate(0) + "_" + st_PayInfo.szLanguageCode + "." + szReceiptTemplate(1)

            'Modified 12 Aug 2014, checking of GetTemplate, cater for if failed to retrieve template, try retrieve based on default language code
            If (False = GetTemplate(szTemplatePath, szHTMLContent)) Then
                If (False = GetTemplate(szReceiptTemplate(0) + "_" + Common.C_DEFAULT_LANGUAGE_CODE + "." + szReceiptTemplate(1), szHTMLContent)) Then
                    iReturn = -1
                    Return iReturn
                End If
            End If

            If szHTMLContent <> "" Then
                If ("BNB" = Trim(st_PayInfo.szTokenType).ToUpper()) Then
                    If (0 = st_PayInfo.iTxnStatus) Then
                        szTxnStatus = "Successful"          'Modified 28 Jan 2014, Success to Successful
                        szHideReceipt = "false"             'Added, 15 Dec 2013
                    ElseIf (1 = st_PayInfo.iTxnStatus) Then
                        szTxnStatus = "Failed. Please Try Again."   'Modified 25 Jul 2014, added "Please Try Again."
                    ElseIf (Common.TXN_STATUS.DECLINED_BY_EXT_FDS = st_PayInfo.iTxnStatus) Then        ' 25 May 2017
                        szTxnStatus = "Declined"
                    Else
                        szTxnStatus = "Pending"
                    End If

                    'Added, 9 Dec 2013
                    If ("CC" = st_PayInfo.szPymtMethod) Then
                        If (True = objTxnProc.bGetCardType(st_PayInfo)) Then
                            szPymtMethod = st_PayInfo.szCardTypeDesc
                        End If

                        'Added, 9 Dec 2013
                        szCCDetails = "".PadRight(st_PayInfo.szCardPAN.Length() - 4, "X") + Right(st_PayInfo.szCardPAN, 4)

                        'Commented  6 Oct 2016, new payment page
                        'szCCDetails = "<tr><th>Card Number:</th><td>" + szCCDetails + "</td></tr>"
                        'szCCDetails = szCCDetails + "<tr><th>Cardholder Name:</th><td>" + st_PayInfo.szCardHolder + "</td></tr>"
                        'Modified 17 Feb 2014, added checking of TxnStatus 0
                        'If (0 = st_PayInfo.iTxnStatus) Then
                        '    szCCDetails = szCCDetails + "<tr><th>APPR Code:</th><td>" + st_PayInfo.szAuthCode + "</td></tr>"
                        'End If

                        'Modified 13 Jan 2017, added checking of szTemplatePath for new payment page
                        If (("templates3" = Left(szTemplatePath, 10).ToLower()) Or ("rttemplates" = Left(szTemplatePath, 11).ToLower())) Then
                            szCCDetails = "<dl class=""dl-horizontal""><dt><strong>Card Number</strong></dt><dd>" + szCCDetails + "</dd></dl>"
                            szCCDetails = szCCDetails + "<dl class=""dl-horizontal""><dt><strong>Cardholder Name</strong></dt><dd>" + st_PayInfo.szCardHolder + "</dd></dl>"
                            If (0 = st_PayInfo.iTxnStatus) Then
                                szCCDetails = szCCDetails + "<dl class=""dl-horizontal""><dt><strong>APPR Code</strong></dt><dd>" + st_PayInfo.szAuthCode + "</dd></dl>"
                            End If
                        Else
                            'Added, 6 Oct 2016, new payment page's receipt
                            szCCDetails = "<tr><th>Card Number</th><td>:</td><td>" + szCCDetails + "</td></tr>"
                            szCCDetails = szCCDetails + "<tr><th>Cardholder Name</th><td>:</td><td>" + st_PayInfo.szCardHolder + "</td></tr>"
                            'Added, 13 Jan 2017
                            If (0 = st_PayInfo.iTxnStatus) Then
                                szCCDetails = szCCDetails + "<tr><th>APPR Code</th><td>:</td><td>" + st_PayInfo.szAuthCode + "</td></tr>"
                            End If
                        End If


                    ElseIf ("OB" = st_PayInfo.szPymtMethod Or "WA" = st_PayInfo.szPymtMethod) Then  'Modified 22 Oct 2014, added "WA" for e-Wallet, e.g. CashU
                        szPymtMethod = st_HostInfo.szDesc
                    End If

                    objCommon.szFormatPlaceHolder(szHTMLContent, "[MERCHANTADDR]", st_PayInfo.szMerchantAddress)            'Added, 14 Dec 2013
                    objCommon.szFormatPlaceHolder(szHTMLContent, "[MERCHANTCONTACTNO]", st_PayInfo.szMerchantContactNo)     'Added, 14 Dec 2013
                    objCommon.szFormatPlaceHolder(szHTMLContent, "[MERCHANTEMAILADDR]", st_PayInfo.szMerchantEmailAddr)     'Added, 14 Dec 2013
                    objCommon.szFormatPlaceHolder(szHTMLContent, "[MERCHANTWEBSITEURL]", st_PayInfo.szMerchantWebSiteURL)   'Added, 14 Dec 2013
                    objCommon.szFormatPlaceHolder(szHTMLContent, "[TXNDATETIME]", System.DateTime.Now.ToString("dd MMM yyyy hh:mm:ss tt", CultureInfo.CreateSpecificCulture("en-us")))  'Added, 9 Dec 2013; Modified 17 Feb 2014, added CultureInfo or else some machine not able to display AM/PM
                    objCommon.szFormatPlaceHolder(szHTMLContent, "[PYMTMETHOD]", szPymtMethod)                                          'Added, 9 Dec 2013
                    objCommon.szFormatPlaceHolder(szHTMLContent, "[CCDETAILS]", szCCDetails)                                            'Added, 9 Dec 2013
                    objCommon.szFormatPlaceHolder(szHTMLContent, "[MERCHANTNAME]", st_PayInfo.szMerchantName)
                    objCommon.szFormatPlaceHolder(szHTMLContent, "[ORDERDESC]", st_PayInfo.szMerchantOrdDesc)
                    objCommon.szFormatPlaceHolder(szHTMLContent, "[TXNAMOUNT]", st_PayInfo.szTxnAmount)
                    objCommon.szFormatPlaceHolder(szHTMLContent, "[CURRENCYCODE]", st_PayInfo.szCurrencyCode)
                    objCommon.szFormatPlaceHolder(szHTMLContent, "[MERCHANTTXNID]", st_PayInfo.szMerchantTxnID)
                    objCommon.szFormatPlaceHolder(szHTMLContent, "[ORDERNUMBER]", st_PayInfo.szMerchantOrdID)                 'Added, 14 Dec 2013
                    objCommon.szFormatPlaceHolder(szHTMLContent, "[GATEWAYTXNID]", st_PayInfo.szTxnID)
                    objCommon.szFormatPlaceHolder(szHTMLContent, "[ISSUINGBANK]", st_PayInfo.szIssuingBank)
                    objCommon.szFormatPlaceHolder(szHTMLContent, "[HOSTTXNID]", st_PayInfo.szHostTxnID)
                    objCommon.szFormatPlaceHolder(szHTMLContent, "[TXNSTATUS]", szTxnStatus)
                    objCommon.szFormatPlaceHolder(szHTMLContent, "[ISHIDERECEIPT]", szHideReceipt)                          'Added, 15 Dec 2013
                    objCommon.szFormatPlaceHolder(szHTMLContent, "[MERCHANTID]", st_PayInfo.szMerchantID)                     'Added, 24 Jun 2014, to show Merchant logo on receipt page
                Else
                    Return -1
                End If

                sz_HTML = szHTMLContent
            Else
                iReturn = -1
            End If
        Else
            iReturn = -1
        End If

        Return iReturn
    End Function

    '********************************************************************************************
    ' Class Name:		Response
    ' Function Name:	iLoadReceiptEmailUI_Shopper
    ' Function Type:	Integer
    ' Parameter:		None
    ' Description:		Load payment receipt email template for shopper
    ' History:			29 Jan 2014	Created
    '                   13 Feb 2014 Modified function name "_Shopper"
    '********************************************************************************************
    Public Function iLoadReceiptEmailUI_Shopper(ByRef sz_HTML As String, ByVal st_PayInfo As Common.STPayInfo, ByVal st_HostInfo As Common.STHost) As Integer
        Dim iReturn As Integer = 0
        Dim szTemplatePath As String = ""
        Dim szHTMLContent As String = ""
        Dim szReceiptTemplate() As String
        Dim szPymtMethod As String = ""         'Added, 9 Dec 2013
        Dim szCCDetails As String = ""          'Added, 9 Dec 2013

        'Modified OTC, 23 Nov 2016
        If ("OTC" = st_PayInfo.szPymtMethod And Common.TXN_STATUS.TXN_SUCCESS <> st_PayInfo.iTxnStatus) Then
            szTemplatePath = ConfigurationManager.AppSettings("EmailBodyTemplate_Shopper_OTC")
        Else
            szTemplatePath = ConfigurationManager.AppSettings("EmailBodyTemplate_Shopper")  'Modified 13 Feb 2014, added "_Shopper"
        End If

        If szTemplatePath <> "" Then
            szReceiptTemplate = Split(szTemplatePath, ".")
            szTemplatePath = szReceiptTemplate(0) + "_" + st_PayInfo.szLanguageCode + "." + szReceiptTemplate(1)

            'Modified 12 Aug 2014, checking of GetTemplate, cater for if failed to retrieve template, try retrieve based on default language code
            If (False = GetTemplate(szTemplatePath, szHTMLContent)) Then
                If (False = GetTemplate(szReceiptTemplate(0) + "_" + Common.C_DEFAULT_LANGUAGE_CODE + "." + szReceiptTemplate(1), szHTMLContent)) Then
                    iReturn = -1
                    Return iReturn
                End If
            End If

            If szHTMLContent <> "" Then
                'Added, 9 Dec 2013
                If ("CC" = st_PayInfo.szPymtMethod) Then
                    If (True = objTxnProc.bGetCardType(st_PayInfo)) Then
                        szPymtMethod = st_PayInfo.szCardTypeDesc
                    End If

                    'Modified 17 Dec 2014
                    szCCDetails = szCCDetails + "<tr>"
                    szCCDetails = szCCDetails + "<td width=""150"">Card Number</td>"
                    szCCDetails = szCCDetails + "<td width=""25"">:</td>"
                    szCCDetails = szCCDetails + "<td width=""400"">" + "".PadRight(st_PayInfo.szCardPAN.Length() - 4, "X") + Right(st_PayInfo.szCardPAN, 4) + "</td>"
                    szCCDetails = szCCDetails + "</tr>"

                    szCCDetails = szCCDetails + "<tr>"
                    szCCDetails = szCCDetails + "<td width=""150"">Cardholder Name</td>"
                    szCCDetails = szCCDetails + "<td width=""25"">:</td>"
                    szCCDetails = szCCDetails + "<td width=""400"">" + st_PayInfo.szCardHolder + "</td>"
                    szCCDetails = szCCDetails + "</tr>"

                    szCCDetails = szCCDetails + "<tr>"
                    szCCDetails = szCCDetails + "<td width=""150"">APPR Code</td>"
                    szCCDetails = szCCDetails + "<td width=""25"">:</td>"
                    szCCDetails = szCCDetails + "<td width=""400"">" + st_PayInfo.szAuthCode + "</td>"
                    szCCDetails = szCCDetails + "</tr>"

                ElseIf ("OB" = st_PayInfo.szPymtMethod Or "WA" = st_PayInfo.szPymtMethod) Then  'Modified 22 Oct 2014, added "WA" for e-Wallet, e.g. CashU
                    szPymtMethod = st_HostInfo.szDesc
                End If

                objCommon.szFormatPlaceHolder(szHTMLContent, "[CUSTNAME]", st_PayInfo.szCustName.ToUpper())                             'Modified 18 Feb 2014, added ToUpper
                objCommon.szFormatPlaceHolder(szHTMLContent, "[CURRENCYCODE]", st_PayInfo.szCurrencyCode)
                objCommon.szFormatPlaceHolder(szHTMLContent, "[TXNAMOUNT]", st_PayInfo.szTxnAmount)
                objCommon.szFormatPlaceHolder(szHTMLContent, "[MERCHANTNAME]", st_PayInfo.szMerchantName)
                objCommon.szFormatPlaceHolder(szHTMLContent, "[MERCHANTWEBSITEURL]", st_PayInfo.szMerchantWebSiteURL)                   'Added, 14 Dec 2013
                objCommon.szFormatPlaceHolder(szHTMLContent, "[TXNDATETIME]", System.DateTime.Now.ToString("dd MMM yyyy hh:mm:ss tt", CultureInfo.CreateSpecificCulture("en-us")))  'Added, 9 Dec 2013
                objCommon.szFormatPlaceHolder(szHTMLContent, "[MERCHANTTXNID]", st_PayInfo.szMerchantTxnID)
                objCommon.szFormatPlaceHolder(szHTMLContent, "[ORDERNUMBER]", st_PayInfo.szMerchantOrdID)                                'Added, 14 Dec 2013
                objCommon.szFormatPlaceHolder(szHTMLContent, "[ORDERDESC]", st_PayInfo.szMerchantOrdDesc)
                objCommon.szFormatPlaceHolder(szHTMLContent, "[PYMTMETHOD]", szPymtMethod)                                              'Added, 9 Dec 2013
                objCommon.szFormatPlaceHolder(szHTMLContent, "[CCDETAILS]", szCCDetails)                                                'Added, 9 Dec 2013
                objCommon.szFormatPlaceHolder(szHTMLContent, "[GATEWAYTXNID]", st_PayInfo.szTxnID)
                objCommon.szFormatPlaceHolder(szHTMLContent, "[MERCHANTCONTACTNO]", st_PayInfo.szMerchantContactNo)
                objCommon.szFormatPlaceHolder(szHTMLContent, "[MERCHANTEMAILADDR]", st_PayInfo.szMerchantEmailAddr)
                objCommon.szFormatPlaceHolder(szHTMLContent, "[CARDTYPEURL]", ConfigurationManager.AppSettings("CardTypeURL"))          'Added, 26 Aug 2015
                objCommon.szFormatPlaceHolder(szHTMLContent, "[MERCHANTID]", st_PayInfo.szMerchantID)                                     'Added, 26 Aug 2015
                'Added -OTC, 23 Nov 2016, for OTC Receipt
                objCommon.szFormatPlaceHolder(szHTMLContent, "[PYMTDUETIME]", st_PayInfo.szDueTime)
                objCommon.szFormatPlaceHolder(szHTMLContent, "[SECURECODE]", String.Format("{0}{1}", st_HostInfo.szOSPymtCode, st_PayInfo.szOTCSecureCode))   'Added 7 Mar 2017, Artajasa, display Prefix infront secure code
                objCommon.szFormatPlaceHolder(szHTMLContent, "[HOSTNAME]", st_HostInfo.szDesc)
                'Added, 17 May 2017, for OTC email reference number
                objCommon.szFormatPlaceHolder(szHTMLContent, "[PYMTCOUNTRY]", st_PayInfo.szHostCountryName)
                objCommon.szFormatPlaceHolder(szHTMLContent, "[ISSUINGBANK]", st_HostInfo.szDesc)

                sz_HTML = szHTMLContent
            Else
                iReturn = -1
            End If
        Else
            iReturn = -1
        End If

        Return iReturn
    End Function

    '********************************************************************************************
    ' Class Name:		Response
    ' Function Name:	iLoadReceiptEmailUI_Merchant
    ' Function Type:	Integer
    ' Parameter:		None
    ' Description:		Load payment receipt email template for merchant
    ' History:			13 Feb 2014 Created
    '********************************************************************************************
    Public Function iLoadReceiptEmailUI_Merchant(ByRef sz_HTML As String, ByVal st_PayInfo As Common.STPayInfo, ByVal st_HostInfo As Common.STHost) As Integer
        Dim iReturn As Integer = 0
        Dim szTemplatePath As String = ""
        Dim szHTMLContent As String = ""
        Dim szReceiptTemplate() As String
        Dim szPymtMethod As String = ""
        Dim szTxnAmount As String = ""      'Added, 2 Sept 2014, for FX
        Dim szCurrencyCode As String = ""   'Added, 2 Sept 2014, for FX

        szTemplatePath = ConfigurationManager.AppSettings("EmailBodyTemplate_Merchant")

        If szTemplatePath <> "" Then
            szReceiptTemplate = Split(szTemplatePath, ".")
            szTemplatePath = szReceiptTemplate(0) + "_" + st_PayInfo.szLanguageCode + "." + szReceiptTemplate(1)

            'Modified 12 Aug 2014, checking of GetTemplate, cater for if failed to retrieve template, try retrieve based on default language code
            If (False = GetTemplate(szTemplatePath, szHTMLContent)) Then
                If (False = GetTemplate(szReceiptTemplate(0) + "_" + Common.C_DEFAULT_LANGUAGE_CODE + "." + szReceiptTemplate(1), szHTMLContent)) Then
                    iReturn = -1
                    Return iReturn
                End If
            End If

            If szHTMLContent <> "" Then
                'Added, 9 Dec 2013
                If ("CC" = st_PayInfo.szPymtMethod) Then
                    If (True = objTxnProc.bGetCardType(st_PayInfo)) Then
                        szPymtMethod = st_PayInfo.szCardTypeDesc
                    End If

                ElseIf ("OB" = st_PayInfo.szPymtMethod Or "WA" = st_PayInfo.szPymtMethod) Then  'Modified 22 Oct 2014, added "WA" for e-Wallet, e.g. CashU
                    szPymtMethod = st_HostInfo.szDesc
                End If

                'Added, 2 Sept 2014, checking of FXCurrencyCode
                If ("" = st_PayInfo.szFXCurrencyCode) Then
                    szCurrencyCode = st_PayInfo.szCurrencyCode
                    szTxnAmount = st_PayInfo.szTxnAmount
                Else
                    szCurrencyCode = st_PayInfo.szBaseCurrencyCode  'For FX, shows original CurrencyCode and TxnAmount to merchant,
                    szTxnAmount = st_PayInfo.szBaseTxnAmount        'instead of FXCurrencyCode and FXTxnAmount stored in st_PayInfo.szTxnAmount assigned in bVerifyResTxn()
                End If

                objCommon.szFormatPlaceHolder(szHTMLContent, "[TXNDATETIME]", System.DateTime.Now.ToString("dd MMM yyyy hh:mm:ss tt", CultureInfo.CreateSpecificCulture("en-us")))  'Added, 9 Dec 2013
                objCommon.szFormatPlaceHolder(szHTMLContent, "[GATEWAYTXNID]", st_PayInfo.szTxnID)
                objCommon.szFormatPlaceHolder(szHTMLContent, "[MERCHANTNAME]", st_PayInfo.szMerchantName)

                'Modified 2 Sept 2014, use szCurrencyCode and szTxnAmount instead of st_PayInfo.szCurrencyCode
                objCommon.szFormatPlaceHolder(szHTMLContent, "[CURRENCYCODE]", szCurrencyCode)
                objCommon.szFormatPlaceHolder(szHTMLContent, "[TXNAMOUNT]", szTxnAmount)

                objCommon.szFormatPlaceHolder(szHTMLContent, "[CUSTNAME]", st_PayInfo.szCustName.ToUpper()) 'Modified 18 Feb 2014, added ToUpper
                objCommon.szFormatPlaceHolder(szHTMLContent, "[PYMTMETHOD]", szPymtMethod)                  'Added, 9 Dec 2013
                objCommon.szFormatPlaceHolder(szHTMLContent, "[MERCHANTTXNID]", st_PayInfo.szMerchantTxnID)
                objCommon.szFormatPlaceHolder(szHTMLContent, "[ORDERNUMBER]", st_PayInfo.szMerchantOrdID)     'Added, 14 Dec 2013
                objCommon.szFormatPlaceHolder(szHTMLContent, "[ORDERDESC]", st_PayInfo.szMerchantOrdDesc)

                sz_HTML = szHTMLContent
            Else
                iReturn = -1
            End If
        Else
            iReturn = -1
        End If

        Return iReturn
    End Function

    '********************************************************************************************
    ' Class Name:		Payment
    ' Function Name:	iSendFraudEmailNotification
    ' Function Type:	NONE
    ' Parameter:        iShopperEmailNotifyStatus, iMerchantEmailNotifyStatus
    ' Description:		Send Email Notification
    ' History:			08 April 2019, Created by Joyce
    '********************************************************************************************
    Public Function iSendFraudEmailNotification(ByVal st_PayInfo As Common.STPayInfo, ByVal st_HostInfo As Common.STHost) As Integer
        Dim iRet As Integer = -1
        Dim szTo() As String = Nothing
        Dim szCc() As String = Nothing
        Dim szBcc() As String = Nothing
        Dim szToAddress() As String = Nothing

        Dim szReceiptEmailBody As String = ""
        Dim objResp As New Response(objLoggerII)
        Dim objMail As New Mail(objLoggerII)
        Dim iMailStatus As Integer = -1
        Dim szStatusMsg As String = ""
        Dim szSubject As String = ""

        Try
            ReDim szCc(0)
            ReDim szBcc(0)
            ReDim szTo(0)

            szCc(0) = ""
            szBcc(0) = ""

            If (Not szTo Is Nothing) Then
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12

                ' allows for validation of SSL conversations
                ServicePointManager.ServerCertificateValidationCallback = Function() True   'C#: delegate { return true }

                szTo(0) = ConfigurationManager.AppSettings("FraudEmail").ToString()

                If (objResp.iLoadFraudEmailUI(szReceiptEmailBody, st_PayInfo, st_HostInfo) <> 0) Then
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                        st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Failed to load fraud receipt email body for (" + st_PayInfo.iPymtNotificationEmail.ToString() + ":" + szTo(0) + ")")
                Else
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                        st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Loaded fraud receipt email body for (" + st_PayInfo.iPymtNotificationEmail.ToString() + ":" + szTo(0) + ")")
                End If



                szSubject = ConfigurationManager.AppSettings("EmailSubject_Fraud") + ": " + st_PayInfo.szMerchantName + " - Exceeded Transaction Amount"
                iMailStatus = objMail.SendMail(szTo(0), szReceiptEmailBody, szSubject)
                szStatusMsg = objMail.StatusMessage(iMailStatus)

                st_PayInfo.iOSRet = iMailStatus
                st_PayInfo.szErrDesc = szStatusMsg

                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                        st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Fraud email notification [" + szTo(0) + "] [" + iMailStatus.ToString() + ":" + szStatusMsg + "]")


                iRet = iMailStatus
            End If

        Catch ex As Exception
            'iRet = -1
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > iSendFraudEmailNotification() Exception: " + ex.Message)
        Finally
            If Not objResp Is Nothing Then
                objResp.Dispose()
                objResp = Nothing
            End If

            If Not objMail Is Nothing Then
                objMail = Nothing
            End If
        End Try
        Return iRet
    End Function

    '********************************************************************************************
    ' Class Name:		Response
    ' Function Name:	iLoadFraudEmailUI
    ' Function Type:	Integer
    ' Parameter:		None
    ' Description:		Load fraud alert email template
    ' History:			02 March 2019	Created by Joyce
    '********************************************************************************************
    Public Function iLoadFraudEmailUI(ByRef sz_HTML As String, ByVal st_PayInfo As Common.STPayInfo, ByVal st_HostInfo As Common.STHost) As Integer
        Dim iReturn As Integer = 0
        Dim szTemplatePath As String = ""
        Dim szHTMLContent As String = ""
        Dim szReceiptTemplate() As String
        Dim szPymtMethod As String = ""
        Dim szTxnAmount As String = ""      'Added, 2 Sept 2014, for FX
        Dim szCurrencyCode As String = ""   'Added, 2 Sept 2014, for FX

        szTemplatePath = ConfigurationManager.AppSettings("EmailBodyTemplate_Fraud")

        If szTemplatePath <> "" Then
            szReceiptTemplate = Split(szTemplatePath, ".")
            szTemplatePath = szReceiptTemplate(0) + "_" + st_PayInfo.szLanguageCode + "." + szReceiptTemplate(1)

            'Modified 12 Aug 2014, checking of GetTemplate, cater for if failed to retrieve template, try retrieve based on default language code
            If (False = GetTemplate(szTemplatePath, szHTMLContent)) Then
                If (False = GetTemplate(szReceiptTemplate(0) + "_" + Common.C_DEFAULT_LANGUAGE_CODE + "." + szReceiptTemplate(1), szHTMLContent)) Then
                    iReturn = -1
                    Return iReturn
                End If
            End If

            If szHTMLContent <> "" Then
                'Added, 9 Dec 2013
                If ("CC" = st_PayInfo.szPymtMethod) Then
                    If (True = objTxnProc.bGetCardType(st_PayInfo)) Then
                        szPymtMethod = st_PayInfo.szCardTypeDesc
                    End If

                ElseIf ("OB" = st_PayInfo.szPymtMethod Or "WA" = st_PayInfo.szPymtMethod) Then  'Modified 22 Oct 2014, added "WA" for e-Wallet, e.g. CashU
                    szPymtMethod = st_HostInfo.szDesc
                End If

                'Merchant ID
                objCommon.szFormatPlaceHolder(szHTMLContent, "[MERCHANTID]", st_PayInfo.szMerchantID)
                'Merchant Name
                objCommon.szFormatPlaceHolder(szHTMLContent, "[MERCHANTNAME]", st_PayInfo.szMerchantName)
                'Per Txn Amt Limi
                objCommon.szFormatPlaceHolder(szHTMLContent, "[PERTXNAMTLIMIT]", Convert.ToDouble(st_PayInfo.dPerTxnAmtLimit).ToString("f2"))
                'Current Txn Amt
                objCommon.szFormatPlaceHolder(szHTMLContent, "[CURRENTTXNAMTLIMIT]", st_PayInfo.szTxnAmount)
                'Merchant Txn ID
                objCommon.szFormatPlaceHolder(szHTMLContent, "[MERCHANTTXNID]", st_PayInfo.szMerchantTxnID)
                'Date Time
                objCommon.szFormatPlaceHolder(szHTMLContent, "[TXNDATETIME]", System.DateTime.Now.ToString("dd MMM yyyy hh:mm:ss tt", CultureInfo.CreateSpecificCulture("en-us")))  'Added, 9 Dec 2013

                sz_HTML = szHTMLContent
            Else
                iReturn = -1
            End If
        Else
            iReturn = -1
        End If

        Return iReturn
    End Function


    '********************************************************************************************
    ' Class Name:		Response
    ' Function Name:	iLoadWaitUI
    ' Function Type:	Integer
    ' Parameter:		sz_HTML [out, string]   -   Customized content of the web page
    ' Description:		Load the waiting template
    ' History:			18 Nov 2008
    '********************************************************************************************
    Public Function iLoadWaitUI(ByRef sz_HTML As String, ByVal sz_LanguageCode As String) As Integer
        Dim iReturn As Integer = 0
        Dim szTemplatePath As String = ""
        Dim szWaitTemplate() As String
        Dim szHTMLContent As String = ""

        szTemplatePath = ConfigurationManager.AppSettings("WaitingTemplate")
        'Maps to the respective waiting page based on Language Code
        szWaitTemplate = Split(szTemplatePath, ".")
        szTemplatePath = szWaitTemplate(0) + "_" + sz_LanguageCode + "." + szWaitTemplate(1)

        If szTemplatePath <> "" Then
            GetTemplate(szTemplatePath, szHTMLContent)

            If szHTMLContent <> "" Then
                'Commented the following on 12 Mar 2009 coz the waiting page will not be able to be shown to the browser
                'if the following method is used
                'sz_HTML = szHTMLContent
                HttpContext.Current.Response.Write(szHTMLContent)
                HttpContext.Current.Response.Flush()
            Else
                iReturn = -1
                'Response.Write("szHTMLContent==empty")
            End If
        Else
            iReturn = -1
        End If

        Return iReturn
    End Function

    '********************************************************************************************
    ' Class Name:		Response
    ' Function Name:	iLoadErrUI
    ' Function Type:	Integer
    ' Parameter:		
    ' Description:		Load error template
    ' History:			18 Nov 2008	Created
    '********************************************************************************************
    Public Function iLoadErrUI(ByVal sz_URL As String, ByVal st_PayInfo As Common.STPayInfo, ByRef sz_HTML As String) As Integer
        Dim iReturn As Integer = 0
        Dim szTemplatePath As String = ""
        Dim szHTMLContent As String = ""

        szTemplatePath = ConfigurationManager.AppSettings("ErrTemplate")

        If szTemplatePath <> "" Then
            GetTemplate(szTemplatePath, szHTMLContent)
            If szHTMLContent <> "" Then
                If sz_URL <> "" Then
                    objCommon.szFormatPlaceHolder(szHTMLContent, "[REDIRECTURL]", sz_URL)
                Else
                    objCommon.szFormatPlaceHolder(szHTMLContent, "[REDIRECTURL]", "error.htm")
                End If

                iReturn = iCustomizeErrPage(szHTMLContent, st_PayInfo)
                If iReturn <> 0 Then
                    iReturn = -1
                    Return iReturn
                End If

                sz_HTML = szHTMLContent
            Else
                iReturn = -1
                'Response.Write("szHTMLContent==empty")
            End If
        Else
            iReturn = -1
        End If

        Return iReturn
    End Function

    '********************************************************************************************
    ' Class Name:		Response
    ' Function Name:	iLoadOrderUI
    ' Function Type:	Integer
    ' Parameter:		sz_HTML [out, string]   -   Customized content of the web page
    ' Description:		Load the order template
    ' History:			12 Oct 2009
    '********************************************************************************************
    Public Function iLoadOrderUI(ByVal sz_TemplatePath As String, ByVal sz_ConfigFilePath As String, ByVal b_Status As Boolean, ByVal st_PayInfo As Common.STPayInfo, ByVal st_HostInfo As Common.STHost, ByRef sz_HTML As String) As Integer
        Dim iReturn As Integer = 0
        Dim iLoop As Integer
        Dim szHTMLContent As String = ""
        Dim szConfigFile As String = ""
        Dim szLine As String = ""
        Dim szField As String = ""
        Dim szValue As String = ""
        Dim szConfig() As String
        Dim bFound As Boolean = False
        Dim objStreamReader As StreamReader

        Try
            szConfigFile = Server.MapPath(sz_ConfigFilePath)
            objStreamReader = System.IO.File.OpenText(szConfigFile)
            Do
                szLine = objStreamReader.ReadLine()
                szConfig = Split(szLine, "|")
                If ((szConfig(0).Trim().ToUpper() = st_PayInfo.szTxnType.ToUpper()) And (szConfig(1).Trim().ToUpper() = "REQ")) Then
                    bFound = True
                    Exit Do
                End If
            Loop Until (szLine Is Nothing)

            objStreamReader.Close()

            If (False = bFound) Then
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + "> TxnType(REQ_" + st_PayInfo.szTxnType + ") not found in Config Template for " + st_PayInfo.szIssuingBank)
                GoTo CleanUp
            End If

            If sz_TemplatePath <> "" Then
                GetTemplate(sz_TemplatePath, szHTMLContent)

                If szHTMLContent <> "" Then
                    For iLoop = 3 To szConfig.GetUpperBound(0)
                        'e.g. ORDER|REQ|POST|[STATUS]|STATUS|[MID]|MID|[TXNDATETIME]|{ddMMyyyyhhmmss}|[GATEWAYTXNID]|GATEWAYTXNID|[TID]|TID|[TXNAMOUNT]|TXNAMOUNT|[CURRENCYCODE]|CURRENCYCODE|[ORDERDESC]|ORDERDESC
                        If (0 <> (iLoop Mod 2)) Then
                            szField = szConfig(iLoop).Trim()
                        Else
                            szValue = ""
                            Select Case szConfig(iLoop).Trim().ToLower()
                                Case ""               'Empty value
                                    szValue = ""
                                Case "status"
                                    If (True = b_Status) Then
                                        szValue = "true"
                                    Else
                                        szValue = "false"
                                    End If
                                Case "status_int"
                                    If (True = b_Status) Then
                                        szValue = "1"
                                    Else
                                        szValue = "0"
                                    End If
                                Case "mid"
                                    If (st_HostInfo.szMID <> "") Then
                                        szValue = st_HostInfo.szMID
                                    End If
                                Case "tid"
                                    If (st_HostInfo.szTID <> "") Then
                                        szValue = st_HostInfo.szTID
                                    End If
                                Case "gatewaytxnid"
                                    If (st_PayInfo.szTxnID <> "") Then
                                        szValue = st_PayInfo.szTxnID
                                    End If
                                Case "currencycode"
                                    If (st_PayInfo.szCurrencyCode <> "") Then
                                        szValue = st_PayInfo.szCurrencyCode
                                    End If
                                Case "basecurrencycode"
                                    If (st_PayInfo.szBaseCurrencyCode <> "") Then
                                        szValue = st_PayInfo.szBaseCurrencyCode
                                    End If
                                Case "txnamount"      'Take the request's txn amount which is in 2 decimals
                                    If (st_PayInfo.szTxnAmount <> "") Then
                                        szValue = st_PayInfo.szTxnAmount
                                    End If
                                Case "txnamount_2"    'Amount in cents whereby the last two digits are decimal points
                                    If (st_PayInfo.szTxnAmount <> "") Then
                                        szValue = st_PayInfo.szTxnAmount.Replace(".", "")
                                    End If
                                Case "txnamount_02"   'Added on 25 Oct 2009. 12 digits, leading zeroes, no decimal point, last 2 are decimal values
                                    If (st_PayInfo.szTxnAmount <> "") Then
                                        szValue = st_PayInfo.szTxnAmount.Replace(".", "")
                                        szValue = "".PadLeft(12 - szValue.Length(), "0") + szValue
                                    End If
                                Case "basetxnamount"
                                    If (st_PayInfo.szBaseTxnAmount <> "") Then
                                        szValue = st_PayInfo.szBaseTxnAmount
                                    End If
                                Case "basetxnamount_2"
                                    If (st_PayInfo.szBaseTxnAmount <> "") Then
                                        szValue = st_PayInfo.szBaseTxnAmount.Replace(".", "")
                                    End If
                                Case "basetxnamount_02"   'Added on 25 Oct 2009. 12 digits, leading zeroes, no decimal point, last 2 are decimal values
                                    If (st_PayInfo.szBaseTxnAmount <> "") Then
                                        szValue = st_PayInfo.szBaseTxnAmount.Replace(".", "")
                                        szValue = "".PadLeft(12 - szValue.Length(), "0") + szValue
                                    End If
                                Case "orderdesc"
                                    If (st_PayInfo.szMerchantOrdDesc <> "") Then
                                        szValue = st_PayInfo.szMerchantOrdDesc
                                    End If
                                Case "ordernumber"
                                    If (st_PayInfo.szMerchantOrdID <> "") Then
                                        szValue = st_PayInfo.szMerchantOrdID
                                    End If
                                Case Else
                                    'Check if it is a default value, e.g. [=AIRASIA]
                                    If ("[=" = Left(szConfig(iLoop).Trim(), 2)) Then
                                        szValue = Mid(szConfig(iLoop).Trim(), 3, szConfig(iLoop).Trim().Length - 3)

                                    ElseIf ("{" = Left(szConfig(iLoop).Trim(), 1)) Then   ' "{" indicates format
                                        If ("{iso3166}[currencycode]" = Left(szConfig(iLoop).Trim().ToLower(), 23)) Then
                                            If (st_PayInfo.szCurrencyCode <> "") Then
                                                szValue = objTxnProc.bGetCurrency(st_PayInfo.szCurrencyCode, st_PayInfo)
                                            End If
                                        ElseIf ("{iso3166}[basecurrencycode]" = Left(szConfig(iLoop).Trim().ToLower(), 27)) Then
                                            If (st_PayInfo.szBaseCurrencyCode <> "") Then
                                                szValue = objTxnProc.bGetCurrency(st_PayInfo.szBaseCurrencyCode, st_PayInfo)
                                            End If
                                        ElseIf ("{ISO3166" = Left(szConfig(iLoop).Trim().ToUpper(), 8)) Then
                                            'Maps currency from ISO4217 (3 alpha characters) to ISO3166 (3 numeric digits) format
                                            If (st_PayInfo.szCurrencyCode <> "") Then
                                                szValue = objTxnProc.bGetCurrency(st_PayInfo.szCurrencyCode, st_PayInfo)
                                            End If
                                        Else
                                            If (st_PayInfo.szTxnDateTime <> "") Then
                                                szValue = Convert.ToDateTime(st_PayInfo.szTxnDateTime).ToString(szConfig(iLoop).Trim().Replace("{", "").Replace("}", ""))
                                            End If
                                        End If
                                    End If
                            End Select

                            objCommon.szFormatPlaceHolder(szHTMLContent, szField, szValue)
                        End If
                    Next

                    sz_HTML = szHTMLContent

                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    szHTMLContent)

                    'NOTE: If the above does not work, then try this
                    'HttpContext.Current.Response.Write(szHTMLContent)
                    'HttpContext.Current.Response.Flush()
                Else
                    iReturn = -1

                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + "> TxnType(REQ_" + st_PayInfo.szTxnType + ") template content is empty")
                End If
            Else
                iReturn = -1

                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + "> TxnType(REQ_" + st_PayInfo.szTxnType + ") template path is empty")
            End If

        Catch ex As Exception
            iReturn = -1

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            "iLoadOrderUI() exception: " + ex.Message)
        End Try

CleanUp:

        Return iReturn
    End Function

    '********************************************************************************************
    ' Class Name:		Response
    ' Function Name:	iLoadRequeryButtonUI
    ' Function Type:	Integer
    ' Parameter:		sz_HTML [out, string]   -   Customized content of the web page
    ' Description:		Load the requery button template
    ' History:			12 Oct 2015 for BancNet
    '********************************************************************************************
    Public Function iLoadRequeryButtonUI(ByVal sz_URL As String, ByVal sz_HTTPString As String, ByRef sz_HTML As String) As Integer
        Dim iReturn As Integer = 0
        Dim szTemplatePath As String = ""
        Dim szHTMLContent As String = ""

        szTemplatePath = ConfigurationManager.AppSettings("RedirectReqBtnTemplate")

        If szTemplatePath <> "" Then
            GetTemplate(szTemplatePath, szHTMLContent)

            If szHTMLContent <> "" Then
                objCommon.szFormatPlaceHolder(szHTMLContent, "[HOSTPARAMS]", sz_HTTPString)
                objCommon.szFormatPlaceHolder(szHTMLContent, "[HOSTURL]", sz_URL)

                sz_HTML = szHTMLContent
            Else
                iReturn = -1
            End If
        Else
            iReturn = -1
        End If

        Return iReturn
    End Function

    '********************************************************************************************
    ' Class Name:		Response
    ' Function Name:	GetTemplate
    ' Function Type:	NONE
    ' Parameter:		sz_FilePath [in, string]        -   File name of the file to be read
    '                   sz_HTMLContent [out, string]    -   Content of the display file
    ' Description:		Get the corresponding template in order to form reply HTML page
    ' History:			17 Oct 2008
    '                   Modified 12 Aug 2014, changed to return Boolean intead of Sub
    '********************************************************************************************
    Private Function GetTemplate(ByVal sz_FilePath As String, ByRef sz_HTMLContent As String) As Boolean
        Dim objStreamReader As StreamReader = Nothing
        Dim szTemplateFile As String = ""
        Dim bReturn As Boolean = True   'Added, 12 Aug 2014

        Try
            szTemplateFile = Server.MapPath(sz_FilePath)    'HttpContext.Current.Server.MapPath
            objStreamReader = System.IO.File.OpenText(szTemplateFile)
            sz_HTMLContent = objStreamReader.ReadToEnd()
            objStreamReader.Close()

        Catch ex As Exception
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            "Response GetTemplate() exception: " + ex.Message)
            bReturn = False 'Added, 12 Aug 2014
        Finally
            If Not objStreamReader Is Nothing Then objStreamReader = Nothing
        End Try

        Return bReturn  'Added, 12 Aug 2014
    End Function

    '********************************************************************************************
    ' Class Name:		Response
    ' Function Name:	iCustomizeErrPage
    ' Function Type:	Integer   
    ' Parameter:		sz_HTMLContent
    ' Description:		Customize content/return fields on error reply page
    ' History:			17 Oct 2008
    '********************************************************************************************
    Private Function iCustomizeErrPage(ByRef sz_HTMLContent As String, ByVal st_PayInfo As Common.STPayInfo) As Integer
        Dim iReturn As Integer = -1

        objCommon.szFormatPlaceHolder(sz_HTMLContent, "[TXNTYPE]", st_PayInfo.szTxnType)
        objCommon.szFormatPlaceHolder(sz_HTMLContent, "[MERCHANTID]", st_PayInfo.szMerchantID)
        objCommon.szFormatPlaceHolder(sz_HTMLContent, "[MERCHANTTXNID]", st_PayInfo.szMerchantTxnID)
        objCommon.szFormatPlaceHolder(sz_HTMLContent, "[ORDERNUMBER]", st_PayInfo.szMerchantOrdID)
        objCommon.szFormatPlaceHolder(sz_HTMLContent, "[TXNAMOUNT]", String.Format("{0:0.00}", st_PayInfo.szTxnAmount))
        objCommon.szFormatPlaceHolder(sz_HTMLContent, "[CURRENCYCODE]", st_PayInfo.szCurrencyCode)
        objCommon.szFormatPlaceHolder(sz_HTMLContent, "[ISSUINGBANK]", st_PayInfo.szIssuingBank)
        objCommon.szFormatPlaceHolder(sz_HTMLContent, "[SESSIONID]", st_PayInfo.szMerchantSessionID)    'Modified 16 Jul 2014, SessionID to MerchantSessionID
        objCommon.szFormatPlaceHolder(sz_HTMLContent, "[GATEWAYTXNID]", st_PayInfo.szTxnID)
        objCommon.szFormatPlaceHolder(sz_HTMLContent, "[TXNSTATE]", st_PayInfo.iTxnState.ToString)
        objCommon.szFormatPlaceHolder(sz_HTMLContent, "[TXNSTATUS]", st_PayInfo.iMerchantTxnStatus.ToString)
        objCommon.szFormatPlaceHolder(sz_HTMLContent, "[BANKREFNO]", st_PayInfo.szHostTxnID)
        objCommon.szFormatPlaceHolder(sz_HTMLContent, "[RESPMESG]", st_PayInfo.szTxnMsg)
        objCommon.szFormatPlaceHolder(sz_HTMLContent, "[HASHMETHOD]", st_PayInfo.szHashMethod)
        'objCommon.szFormatPlaceHolder(sz_HTMLContent, "[HASHVALUE]", st_PayInfo.szHashValue)
        objCommon.szFormatPlaceHolder(sz_HTMLContent, "[HASHVALUE2]", st_PayInfo.szHashValue2)          'Added, 28 Apr 2016, response hash value 2 which includes OrderNumber and Param6 because some plugins like Magento update order based on these 2 fields

        iReturn = 0

        Return iReturn
    End Function

    '********************************************************************************************
    ' Class Name:		Payment
    ' Function Name:	iWSSend
    ' Function Type:	Integer. Return 0 if success else -1
    ' Parameter:        sz_String [in/out, string]  - string to be sent/receive via web service
    '                   sz_URL [in, string]         - Sending URL
    ' Description:		Send SOAP via web service to the specified URL
    ' History:			6 Oct 2013
    '********************************************************************************************
    Public Function iWSSend(ByRef sz_String As String, ByVal sz_URL As String, ByVal st_PayInfo As Common.STPayInfo, ByVal st_HostInfo As Common.STHost) As Integer
        Dim iRet As Integer = -1
        Dim iRetCode As Integer = -1
        Dim szResp As String = ""
        Dim szValues() As String
        Dim szSOAPMsg As String = ""
        Dim szActionURL As String = ""
        Dim iCount As Integer = 0
        'Chg Start JOYCE 20190402, change string to secure string
        'Dim szPassword As String = ""
        Dim szPassword As SecureString = New SecureString()
        'Chg E n d JOYCE 20190402, change string to secure string


        Try
            'e.g. QUERY|REQ|WS|[]|[=<SOAP-ENV:Envelope xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"xmlns:xsd="http://www.w3org/2001/XMLSchema"xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><SOAP-ENV:Body><RequestPayeeInfo xmlns="http://www.openuri.org/"><userid>#ACQUIRERID</userid><password>#HOSTPASSWORD3</password><payeecode>#PAYEECODE</payeecode><billNo>#BILLNO</billNo><Amt>#AMT</Amt></RequestPayeeInfo></SOAP-ENV:Body></SOAP-ENV:Envelope>]|[!]|[=http://www.openuri.org/RequestPayeeInfo]|[!#ACQUIRERID!]|[AcquirerID]|[!#HOSTPASSWORD3!]|[HostPassword3]|[!#PAYEECODE!]|[PayeeCode]|[!#BILLNO!]|[GatewayTxnID]|[!#AMT!]|[TxnAmount_2]
            'e.g. <SOAP-ENV:Envelope xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"xmlns:xsd="http://www.w3org/2001/XMLSchema"xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><SOAP-ENV:Body><RequestPayeeInfo xmlns="http://www.openuri.org/"><userid>bill</userid><password>bill</password><payeecode>1DJ</payeecode><billNo>#BILLNO</billNo><Amt>#AMT</Amt></RequestPayeeInfo></SOAP-ENV:Body></SOAP-ENV:Envelope>!http://www.openuri.org/RequestPayeeInfo!#ACQUIRERID![AcquirerID]!#HOSTPASSWORD3![HostPassword3]!#PAYEECODE![PayeeCode]!#BILLNO![GatewayTxnID]!#AMT![TxnAmount_2]
            szValues = Split(sz_String, "!")
            szSOAPMsg = szValues(0).Trim()
            szActionURL = szValues(1).Trim()

            For iCount = 2 To szValues.GetUpperBound(0)
                'Added on 4 Jan 2011, "And (0 = iCount Mod 2)",  for POLI, to cater for checking of field name only
                'and bypass checking of value which may contain "#", in order to avoid value been overwritten incorrectly
                If ((InStr(szValues(iCount).Trim(), "#") > 0) And (0 = iCount Mod 2)) Then
                    szSOAPMsg = szSOAPMsg.Replace(szValues(iCount).Trim(), szValues(iCount + 1).Trim())
                End If
            Next

            szSOAPMsg = objCommon.szReplaceSpecialChar(szSOAPMsg, "R") 'Add  23 Sept 2011 (POLi). Replace tag back to special character where the special character exist in iGetQueryString with format {SCHAR@...}

            If ("WS" = st_PayInfo.szPostMethod.ToUpper()) Then
                'Modified 22 Jun 2015, added Security Protocol
                iRetCode = objHTTP.iWSPostWithRecv(sz_URL, szActionURL, szSOAPMsg, st_PayInfo.iHttpTimeoutSeconds, szResp, st_HostInfo.szProtocol)
            ElseIf ("WS2" = st_PayInfo.szPostMethod.ToUpper()) Then
                'Chg Start JOYCE 20190402, change string to secure string
                'szPassword = objHash.szDecrypt3DES(st_HostInfo.szSecretKey, Common.C_3DESAPPKEY, Common.C_3DESAPPVECTOR)
                szPassword = New NetworkCredential("", objHash.szDecrypt3DES(st_HostInfo.szSecretKey, Common.C_3DESAPPKEY, Common.C_3DESAPPVECTOR)).SecurePassword
                'Chg E n d JOYCE 20190402, change string to secure string

                'Chg Start JOYCE 20190402, change string to secure string
                ''Modified 22 Jun 2015, added Security Protocol
                'iRetCode = objHTTP.iWSPostWithRecv(sz_URL, szActionURL, szSOAPMsg, st_PayInfo.iHttpTimeoutSeconds, szResp, st_HostInfo.szAcquirerID, szPassword, st_HostInfo.szProtocol) 'Modified  30 Oct 2013, use szAcquirerID
                iRetCode = objHTTP.iWSPostWithRecv(sz_URL, szActionURL, szSOAPMsg, st_PayInfo.iHttpTimeoutSeconds, szResp, st_HostInfo.szAcquirerID, New NetworkCredential("", szPassword).Password, st_HostInfo.szProtocol)
                'Chg E n d JOYCE 20190402, change string to secure string
            End If

            If (iRetCode <> 0) Then
                'Modified 22 Jun 2015, added Security Protocol
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Failed to send SOAP [" + szSOAPMsg + "] to [" + sz_URL + "] Protocol[" + st_HostInfo.szProtocol + "]")

                Return iRet
            Else
                iRet = 0
                sz_String = Server.HtmlDecode(szResp)
            End If

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Posted SOAP [" + szSOAPMsg + "] to [" + sz_URL + "] and recv resp [" + szResp + "]")

        Catch ex As Exception
            iRet = -1

            st_PayInfo.szErrDesc = "iWSSend() exception: " + ex.StackTrace + " " + ex.Message  ' .Message
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > iWSSend() exception: " + ex.Message + " for sending SOAP [" + szSOAPMsg + "] to [" + sz_URL + "]")
        End Try

        Return iRet
    End Function

    '********************************************************************************************
    ' Class Name:		Response
    ' Function Name:	iHTTPSend
    ' Function Type:	NONE
    ' Parameter:        i_SendMethod [in, integer] - Reply method:
    '                   - 1 (page-to-page, redirect);
    '                   - 2 (server-to-server, send ONLY);
    '                   - 3 (server-to-server, send and wait for reply)
    '                   - 4 (pop-up bank page-to-merchant parent page, redirect)
    '                   sz_HTTPString [in, string]  - HTTP string to be sent
    '                   sz_URL [in, string]         - Sending URL
    '                   st_PayInfo [in, structure]  - Payment info structure
    ' Description:		Error handler
    ' History:			18 Nov 2008
    '                   Modified 21 Jun 2015, added SecurityProtocol param
    '                   Modified 4 Oct 2016, added st_HostInfo param, KTB redirect resp query
    '********************************************************************************************
    Public Function iHTTPSend(ByVal i_SendMethod As Integer, ByRef sz_HTTPString As String, ByVal sz_URL As String, ByVal st_PayInfo As Common.STPayInfo, ByVal st_HostInfo As Common.STHost, ByRef sz_HTML As String, Optional ByVal sz_SecurityProtocol As String = "TLS", Optional ByVal sz_LogString As String = "") As Integer
        Dim iRet As Integer = 0
        Dim szURL() As String
        Dim iPosition As Integer = 0
        Dim szHTTPResponse As String = ""
        Dim aszPostMethod() As String        'Added  30 Nov 2017, Boost. Modified 25 Jan 2018, changed szPostMethod to aszPostMethod
        Dim szAuthenticate As String = ""   'Added  30 Nov 2017, Boost

        Try
            If (i_SendMethod <> 1 And i_SendMethod <> 4) Then
                'Cater for MerchantRURL that contains "?" or "$", e.g. "http://MerchantRURL?page=1&event=response" or https://149.122.26.33:6443/skylights/cgi-bin/skylights?page=DEBITRETURN&event=response
                'For merchants that use Get method(Query string), they can use "$" to replace "?" if they need to include
                '"?" in MerchantRURL param, e.g. Response.Redirect("http://url/page?MerchantRURL$page=1").
                iPosition = InStr(sz_URL, "?")  'e.g. 25 which is 1 base of the location
                If (iPosition > 0) Then
                    szURL = Split(sz_URL, "?")
                    If ((InStr(sz_URL, "?=") <= 0) And (InStr(sz_URL, "=") > iPosition)) Then
                        If (sz_HTTPString <> "") Then
                            sz_HTTPString = szURL(1) + "&" + sz_HTTPString
                        Else
                            sz_HTTPString = szURL(1)
                        End If
                    End If
                    sz_URL = szURL(0)
                Else
                    iPosition = InStr(sz_URL, "$")  'e.g. 25 which is 1 base of the location
                    If (iPosition > 0) Then
                        szURL = Split(sz_URL, "$")
                        If ((InStr(sz_URL, "$=") <= 0) And (InStr(sz_URL, "=") > iPosition)) Then
                            If (sz_HTTPString <> "") Then
                                sz_HTTPString = szURL(1) + "&" + sz_HTTPString
                            Else
                                sz_HTTPString = szURL(1)
                            End If
                        End If
                        sz_URL = szURL(0)
                    End If
                End If
            End If

            If (1 = i_SendMethod Or 4 = i_SendMethod) Then
                'Commented  10 Feb 2014, not required because if MerchantRURL contains "?", merchant also wants Gateway
                'to redirect to the original URL which contains "?", e.g. "MerchantRURL=http://sangat.pro/eghl/opencart/index.php?route=payment/ghl/callback"
                'Added, 16 Jan 2014, checking of "?"
                'szURL = Split(sz_URL, "?")
                'sz_URL = szURL(0)
                '///////////////////////////////////////////

                iLoadRedirectUI(sz_URL, sz_HTTPString, sz_HTML)

                'Modified 20 May 2016, added checking of sz_LogString
                If (sz_LogString <> "") Then
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Redirected to [" + sz_URL + "] " + sz_LogString)
                Else
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Redirected to [" + sz_URL + "] " + sz_HTTPString)
                End If

                'Host uses page-to-page method to redirect reply via client's browser to OB Gateway's reply page
                ' OB Gateway also should redirect reply to merchant so that user can view the final result.
                'HttpContext.Current.Response.Redirect(sz_URL + "?" + sz_HTTPString)

                'objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
                '                DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
                '                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Redirected " + sz_URL + "?" + sz_HTTPString)

            ElseIf (2 = i_SendMethod) Then
                'Host uses server-to-server method to reply  OB Gateway
                'User is unable to see  OB Gateway's reply page,  OB Gateway should also do a
                'server-to-server reply to merchant without having to wait for merchant's acknowledgement.
                'Modified 22 Jun 2015, added Security Protocol
                iRet = objHTTP.iHTTPostOnly(sz_URL, sz_HTTPString, "application/x-www-form-urlencoded", st_PayInfo.iHttpTimeoutSeconds, sz_SecurityProtocol)
                If (iRet <> 0) Then
                    Return iRet
                End If

                'Modified 20 May 2016, added checking of sz_LogString
                If (sz_LogString <> "") Then
                    'Modified 22 Jun 2015, added Security Protocol
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Posted " + sz_LogString + " to " + sz_URL + " Protocol(" + sz_SecurityProtocol + ")")
                Else
                    'Modified 22 Jun 2015, added Security Protocol
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Posted " + sz_HTTPString + " to " + sz_URL + " Protocol(" + sz_SecurityProtocol + ")")
                End If

                'Modified on 20 Jul 2009, from Else to ElseIf (3 = i_SendMethod) Then
            ElseIf (3 = i_SendMethod) Then
                'HTTP post and wait for reply until timeout
                'Modified 22 Jun 2015, added Security Protocol
                iRet = objHTTP.iHTTPostWithRecv(sz_URL, sz_HTTPString, st_PayInfo.iHttpTimeoutSeconds, szHTTPResponse, "", sz_SecurityProtocol)
                If (iRet <> 0) Then
                    Return iRet
                End If

                'Modified 20 May 2016, added checking of sz_LogString
                If (sz_LogString <> "") Then
                    'Modified 22 Jun 2015, added Security Protocol
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Posted " + sz_LogString + " to " + sz_URL + " and received response. Protocol(" + sz_SecurityProtocol + ")")
                Else
                    'Modified 22 Jun 2015, added Security Protocol
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Posted " + sz_HTTPString + " to " + sz_URL + " and received response. Protocol(" + sz_SecurityProtocol + ")")
                End If

                sz_HTTPString = szHTTPResponse

                'Added on 20 Jul 2009
            ElseIf (5 = i_SendMethod) Then
                'HTTP GET and wait for reply until timeout
                'Modified 22 Jun 2015, added Security Protocol
                If ("GET_AUTH&BASIC" = st_PayInfo.szPostMethod.ToUpper()) Then    'Added, 25 Jan 2018, for POLi
                    aszPostMethod = Split(st_PayInfo.szPostMethod, "&")
                    szAuthenticate = aszPostMethod(1) + " " + Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(String.Format("{0}:{1}", st_PayInfo.szEncryptKeyPath, st_PayInfo.szDecryptKeyPath)))
                End If

                iRet = objHTTP.iHTTPGetWithRecv(sz_URL, sz_HTTPString, st_PayInfo.iHttpTimeoutSeconds, szHTTPResponse, sz_SecurityProtocol, szAuthenticate) 'Modified 25 Jan 2018, added szAuthenticate
                If (iRet <> 0) Then
                    Return iRet
                End If

                'Modified 20 May 2016, added checking of sz_LogString
                If (sz_LogString <> "") Then
                    'Modified 22 Jun 2015, added Security Protocol
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Posted " + sz_LogString + " to " + sz_URL + " and received response. Protocol(" + sz_SecurityProtocol + ")")
                Else
                    'Modified 22 Jun 2015, added Security Protocol
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Posted " + sz_HTTPString + " to " + sz_URL + " and received response. Protocol(" + sz_SecurityProtocol + ")")
                End If

                sz_HTTPString = szHTTPResponse

            ElseIf (8 = i_SendMethod) Then 'Added, 4 Oct 2016, to solve KTB S2S resp unabled to be parsed due to user cancelled coz no GatewayTxnID 'Added 18 Nov 2014, KTB. 
                'HTTP GET and retry query DB for status
                'Modified 21 Jun 2015, added Security Protocol
                iRet = objHTTP.iHTTPGetWithRecvDB(st_PayInfo, st_HostInfo, objCommon, sz_URL, sz_HTTPString, st_PayInfo.iHttpTimeoutSeconds, szHTTPResponse, sz_SecurityProtocol)
                If (iRet <> 0) Then
                    Return iRet
                End If

                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Posted " + sz_LogString + " to " + sz_URL + " and received response [" + szHTTPResponse + "] Protocol[" + sz_SecurityProtocol + "]")

                sz_HTTPString = szHTTPResponse
            ElseIf (11 = i_SendMethod) Then 'Added7 April 2017, TBank
                'HTTP POST in Bytes       
                iRet = objHTTP.iHTTPostWithRecvAuth(sz_URL, sz_HTTPString, st_PayInfo.iHttpTimeoutSeconds, szHTTPResponse, "", "x-www-form-urlencoded", "", "", "", sz_SecurityProtocol)
                If (iRet <> 0) Then
                    Return iRet
                End If

                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Posted " + sz_LogString + " to " + sz_URL + " and received response [" + szHTTPResponse + "] Protocol[" + sz_SecurityProtocol + "]")

                sz_HTTPString = szHTTPResponse

            ElseIf (12 = i_SendMethod) Then 'Added  30 Nov 2017, Boost
                aszPostMethod = Split(st_PayInfo.szPostMethod, "&")

                If ("BEARER" = aszPostMethod(1).ToUpper()) Then
                    szAuthenticate = aszPostMethod(1) + " " + st_PayInfo.szParam1
                Else
                    szAuthenticate = aszPostMethod(1)
                End If

                iRet = objHTTP.iHTTPGetWithRecv(sz_URL, sz_HTTPString, st_PayInfo.iHttpTimeoutSeconds, szHTTPResponse, sz_SecurityProtocol, szAuthenticate)
                If (iRet <> 0) Then
                    Return iRet
                End If

                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Posted " + sz_LogString + " to " + sz_URL + " and received response [" + szHTTPResponse + "] Protocol[" + sz_SecurityProtocol + "]")
                sz_HTTPString = szHTTPResponse
            End If

        Catch ex As Exception
            iRet = -1

            st_PayInfo.szErrDesc = "iHTTPSend() exception: " + ex.StackTrace + " " + ex.Message  ' .Message
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > iHTTPSend() exception: " + ex.Message + " for sending " + sz_HTTPString + " to " + sz_URL)
        End Try

        Return iRet
    End Function

    '********************************************************************************************
    ' Class Name:		Response
    ' Function Name:	bInitResponse
    ' Function Type:	Boolean.  Return False if error.
    ' Parameter:		
    ' Description:		Update Txn State, Verify response data with request data, Reply Acknowledgement to Host if needed,
    '                   Get merchant password, Get HostTxnStatus's action
    ' History:			31 Oct 2008
    '********************************************************************************************
    Private Function bInitResponse(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost, ByVal sz_ReplyDateTime As String, ByRef sz_HTML As String, Optional ByVal b_VerifyHostIP As Boolean = False) As Boolean
        Dim bReturn As Boolean = True
        Dim aszReturnIPAddresses() As String
        Dim iLoop As Integer = 0
        Dim iRet As Integer = -1
        Dim bMatch As Boolean = False
        Dim szXForwarded As String = ""

        Try
            'Verify Host return IP addresses (optional)
            'Commented  22 Jun 2015, only redirect will call objResponse.bProcessSalesRes and come to here, no need verify return IP
            'Modified 23 Jun 2015, added checking of b_VerifyHostIP
            'Added, 5 Feb 2018, HTTP_X_FORWARDED_FOR will have value if proxy is used during DR for IP routing
            'szXForwarded = HttpContext.Current.Request.ServerVariables("HTTP_X_FORWARDED_FOR")
            'If szXForwarded <> "" And Not (szXForwarded Is Nothing) Then
            '    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
            '                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
            '                            "GatewayTxnID(" + st_PayInfo.szTxnID + ") Host reply IP HTTP_X_FORWARDED_FOR " + szXForwarded)
            'End If

            If (True = b_VerifyHostIP And st_HostInfo.szReturnIPAddresses <> "") Then
                aszReturnIPAddresses = Split(st_HostInfo.szReturnIPAddresses, ";")
                bMatch = False
                For iLoop = 0 To aszReturnIPAddresses.GetUpperBound(0)
                    If (aszReturnIPAddresses(iLoop) = HttpContext.Current.Request.UserHostAddress) Then
                        bMatch = True

                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                        "GatewayTxnID(" + st_PayInfo.szTxnID + ") Host reply IP matched " + aszReturnIPAddresses(iLoop))
                    End If
                Next

                If (False = bMatch) Then
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.INVALID_HOST_IP

                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    "GatewayTxnID(" + st_PayInfo.szTxnID + ") Host reply IP (" + HttpContext.Current.Request.UserHostAddress + ") does not match IP list (" + st_HostInfo.szReturnIPAddresses + ")")
                    Return False
                End If
            End If

            'Added checking of DDGW Aggregator on 3 Apr 2010, no need check late Host response for DDGW Aggregator
            If ("" = ConfigurationManager.AppSettings("HostsListTemplate")) Then
                ' Check late reply
                ' (2 hours x 60 minutes x 60 seconds = 7200 seconds) - PO 'll be released after 2 hrs from its reserved time.
                ' So, host reply came back 2 hours after the request was registered in Gateway 'll be treated as late reply and 'll be discarded.
                'Modified 25 Nov 2013, use objCommon instead of objTxnProc, to cater for different DB connection by Payment Method
                If (st_PayInfo.szPymtMethod <> "OTC") Then
                    If True <> objCommon.bCheckLateResp(st_PayInfo, sz_ReplyDateTime) Then
                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                        "GatewayTxnID(" + st_PayInfo.szTxnID + ") " + st_PayInfo.szErrDesc)

                        Return False
                    End If
                End If
            End If

            'Verify response data with request data stored in database.
            'At the same time, retrieve some request data from database based on Gateway TxnID to return to client's server.
            If (st_PayInfo.szPymtMethod <> "OTC") Then
                If True <> bVerifyResTxn(st_PayInfo, st_HostInfo, sz_HTML) Then
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    "GatewayTxnID(" + st_PayInfo.szTxnID + ") > bVerifyResTxn failed: " + st_PayInfo.szErrDesc)

                    Return False
                End If
            End If

            'Get Merchant Password, NeedAddOSPymt flag to determine whether need to confirm booking with OpenSkies if pymt approved for txns received from this merchant
            If True <> objTxnProc.bGetMerchant(st_PayInfo) Then
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > bInitResponse() " + st_PayInfo.szErrDesc)
                Return False
            End If

            'Modified 28 Oct 2013, moved from inside of bVerifyResTxn (get MerchantID) to below bGetMerchant, to get iReceipt
            'Commented  28 Oct 2013, replaced by iReceipt
            'Modified 19 Dec 2013, added checking of FPX, in order to avoid wait page to overwrite acknowledgement texts to be replied to FPX
            'If ("" = ConfigurationManager.AppSettings("HostsListTemplate")) Then
            'Modified 8 Apr 2015, for BNB, added checking of BNB
            'Modified 22 Jun 2015, added checking of "FPXD" and "FFFPX"
            If (0 = st_PayInfo.iReceipt And st_PayInfo.szIssuingBank <> "FPX" And st_PayInfo.szIssuingBank <> "FPXD" And st_PayInfo.szIssuingBank <> "FFFPX" And Trim(st_PayInfo.szTokenType).ToUpper() <> "BNB") Then
                'Load the respective waiting page based on LanguageCode
                'Host uses page-to-page reply, user is able to see on their browser
                If (1 = st_HostInfo.iHostReplyMethod Or 4 = st_HostInfo.iHostReplyMethod) Then
                    iLoadWaitUI(sz_HTML, st_PayInfo.szLanguageCode)
                End If
            End If

            'Added on 11 Apr 2010, decrypt Merchant Password if its length > 8 characters
            'If (st_PayInfo.szMerchantPassword.Length() > 8) Then
            '    st_PayInfo.szMerchantPassword = objHash.szDecrypt3DES(st_PayInfo.szMerchantPassword, Common.C_3DESAPPKEY, Common.C_3DESAPPVECTOR)
            'End If

            'Moved bGetTerminal from below to here on 30 Oct 2009
            'Modified 25 Nov 2013, use objCommon instead of objTxnProc, to cater for different DB connection by Payment Method
            If (st_PayInfo.szPymtMethod <> "OTC") Then
                If True <> objCommon.bGetTerminal(st_HostInfo, st_PayInfo) Then
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > bInitResponse() " + st_PayInfo.szErrDesc)
                    Return False
                End If

                'Added  27 Aug 2015. PBB. Overwrite szReturnKey get from bGetTerminal.
                'Due to PBB's declined transaction does not have hash value returned and therefore have to force sending Query.
                If (True = st_HostInfo.bIsHashOptional) Then
                    st_HostInfo.szReturnKey = ""
                End If
            End If

            'Added on 26 Feb 2009, to support query host if HostReplyMethod is 1 or 4 and hashing is not involved.
            'HostReplyMethod 4 indicates host reply returned through pop-up bank payment window (Child Window) triggers
            'merchant's window (Parent Window) to redirect to OB Gateway.
            'HostReplyMethod 1 indicates non pop-up bank payment window (Single Window) redirects host reply
            'to  OB Gateway.
            'If there is no hashing involved in the host reply, need to send query to host to get the actual status
            'because host reply returned through user's browser redirection can not be trusted.
            '========================================================================================================
            '29 May 2009, NOTE: Must set ReturnKey to dummy key "1" or any value if HostReplyMethod is 1 or 4 but
            'DOES NOT need to query Host because Host reply is secure enough although no hashing but got encryption
            '========================================================================================================
            If (1 = st_HostInfo.iHostReplyMethod Or 4 = st_HostInfo.iHostReplyMethod) Then
                If (st_HostInfo.szReturnKey = "") Then
                    If (1 = st_HostInfo.iQueryFlag) Then
                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                        st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > " + st_PayInfo.szIssuingBank + " redirected reply (no hashing) to GW, Query Host required")

                        'Moved bGetTerminal above on 30 Oct 2009
                        'If True <> objTxnProc.bGetTerminal(st_HostInfo, st_PayInfo) Then
                        '    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
                        '                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
                        '                    st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > bInitResponse() " + st_PayInfo.szErrDesc)
                        '    Return False
                        'End If

                        ' Query host for payment status ONLY for host that does not support hash but uses redirect method
                        ' to reply  OB Gateway
                        st_PayInfo.szHostTxnStatus = "" ' Overwrites Host Txn Status/Response Code with blank before getting it from Host Query reply
                        st_PayInfo.szRespCode = ""
                        ' Overwritting TxnType from "PAY" to "QUERY" is to ensure the respective action in bGetTxnStatusAction() can be retrieved
                        st_PayInfo.szTxnType = "QUERY"
                        iRet = iSendQueryHost(st_PayInfo, st_HostInfo)
                        If (iRet <> 0) Then 'Modified on 7 Mar 2011, HLB, changed < 0 to <> 0 becoz iRet=44(Failed to send);iRet=5(Timeout);<0:Exception
                            st_PayInfo.szTxnType = "PAY"

                            ' Sets Action to 3 so that response_[Host] class's Page_Load() will not insert this txn into
                            ' Response table in order for Retry Service to retry query Host.
                            st_PayInfo.iAction = 3

                            st_PayInfo.szTxnMsg = "Failed to send Query request/Timeout Query response from " + st_PayInfo.szIssuingBank
                            st_PayInfo.iMerchantTxnStatus = objCommon.TXN_STATUS.TXN_TIMEOUT_HOST_RETURN
                            st_PayInfo.iTxnStatus = objCommon.TXN_STATUS.TXN_PENDING
                            st_PayInfo.iTxnState = objCommon.TXN_STATE.SENT_QUERY_HOST

                            'Added on 5 Apr 2010
                            'Modified 25 Nov 2013, use objCommon instead of objTxnProc, to cater for different DB connection by Payment Method
                            If True <> objCommon.bUpdateTxnStatus(st_PayInfo, st_HostInfo) Then
                                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " " + st_PayInfo.szErrDesc)
                            End If

                            'Modified 7 Mar 2011, changed False to True, so that DDGW will not return failed status
                            'if bank response is approved but failed to send query request/timeout query response to banks,
                            'for banks that do not support security in their payment response message
                            Return True
                            'Return False
                        End If
                    Else
                        'Sets Action to 5 if QueryFlag is OFF so that email alert can be sent and the necessary actions
                        'can be conducted. Once everything is settled, QueryFlag needs to be set to ON.
                        st_PayInfo.iAction = 5

                        Return True
                    End If
                End If
                'ElseIf (2 = st_HostInfo.iHostReplyMethod) Then  'For SCIB
                '    If (False = bMatch) Then
                '        st_PayInfo.iTxnStatus = Common.TXN_STATUS.INVALID_HOST_IP

                '        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
                '                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
                '                        "GatewayTxnID(" + st_PayInfo.szTxnID + ") Host reply IP (" + HttpContext.Current.Request.UserHostAddress + ") does not match IP list (" + st_HostInfo.szReturnIPAddresses + ")")
                '        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
                '                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "QueryURL-> " + st_HostInfo.szQueryURL)
                '        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
                '                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "secretkey-> " + st_HostInfo.szSecretKey)
                '        'if ("" = CheckQueryURL And "" = ) 
                '        Return False
                '        'ENd If
                '    End If
            End If


            'NOTE: bGetHostInfo has been called in Page_Load() b4 calling this function, therefore,
            'st_HostInfo.iHostID needed for bGetTerminal() and bGetTxnStatusAction() already populated with the respective HostID value

            'Check whether to send reply acknowledgement to Host
            If (1 = st_HostInfo.iNeedReplyAcknowledgement) Then
                'Get Terminal and Channel information - CfgFile, ExtraURL
                If (st_HostInfo.szAcknowledgementURL = "") Then
                    'Modified 25 Nov 2013, use objCommon instead of objTxnProc, to cater for different DB connection by Payment Method
                    If True <> objCommon.bGetTerminal(st_HostInfo, st_PayInfo) Then
                        st_PayInfo.szTxnType = "PAY"

                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                        st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > bInitResponse() " + st_PayInfo.szErrDesc)
                        Return False
                    End If
                End If

                iSendACK(st_PayInfo, st_HostInfo)
            End If

            'Get Host's TxnStatus's action
            'Modified 25 Nov 2013, use objCommon instead of objTxnProc, to cater for different DB connection by Payment Method
            If True <> objCommon.bGetTxnStatusAction(st_HostInfo, st_PayInfo) Then
                'st_PayInfo.szTxnType = "PAY"          'Modified  19 Aug 2016. Auth&Capture
                If ("AUTH" <> st_PayInfo.szTxnType.ToUpper()) Then
                    st_PayInfo.szTxnType = "PAY"
                End If

                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > bInitResponse() " + st_PayInfo.szErrDesc)
                Return False
            End If

            'Modified  20 April 2015. Auth&Capture, added checking of Auth and Capture
            If ("AUTH" <> st_PayInfo.szTxnType.ToUpper()) Then
                st_PayInfo.szTxnType = "PAY"
            End If

        Catch ex As Exception
            'Modified 9 Mar 2016, modified False to True, to solve Krungsri redirected 2 times within few seconds and caused PG returned status -1 to merchant
            'Cater for returning status 2(pending) instead of -1 to merchant
            'Krungsri: bInitResponse() Exception: An error occurred while communicating with the remote host. The error code is 0x80070057.
            bReturn = True

            st_PayInfo.iAction = 3  'Added 9 Mar 2016, to update Request table in order to trigger auto query bank

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > bInitResponse() Exception: " + ex.Message)
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		Response
    ' Function Name:	bInitOrderResponse
    ' Function Type:	Boolean.  Return False if error.
    ' Parameter:		
    ' Description:		Verify IP, Retrieve order info accordingly
    ' History:			5 Oct 2009
    '********************************************************************************************
    'Private Function bInitOrderResponse(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost, ByVal sz_DBConnStr As String, ByRef sz_HTML As String) As Boolean
    '    Dim bReturn As Boolean = True
    '    Dim aszReturnIPAddresses() As String
    '    Dim iLoop As Integer = 0
    '    Dim iRet As Integer = -1
    '    Dim bMatch As Boolean = False

    '    Try
    '        'Verify Host return IP addresses (optional)
    '        If (st_HostInfo.szReturnIPAddresses <> "") Then
    '            aszReturnIPAddresses = Split(st_HostInfo.szReturnIPAddresses, ";")
    '            bMatch = False
    '            For iLoop = 0 To aszReturnIPAddresses.GetUpperBound(0)
    '                If (aszReturnIPAddresses(iLoop) = HttpContext.Current.Request.UserHostAddress) Then
    '                    bMatch = True

    '                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
    '                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
    '                                    "GatewayTxnID(" + st_PayInfo.szTxnID + ") Host reply IP matched " + aszReturnIPAddresses(iLoop))
    '                End If
    '            Next

    '            If (False = bMatch) Then
    '                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INVALID_HOST_IP

    '                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
    '                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
    '                                "GatewayTxnID(" + st_PayInfo.szTxnID + ") Host reply IP (" + HttpContext.Current.Request.UserHostAddress + ") does not match IP list (" + st_HostInfo.szReturnIPAddresses + ")")
    '                Return False
    '            End If
    '        End If

    '        'Retrieve order details from database based on GatewayTxnID to return to client's server which could be the bank
    '        If True <> objTxnProc.bGetReqTxn(st_PayInfo, st_HostInfo) Then
    '            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
    '                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
    '                            st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > bInitOrderResponse() " + st_PayInfo.szErrDesc)

    '            Return False
    '        End If

    '        'Get Merchant Password, NeedAddOSPymt flag to determine whether need to confirm booking with OpenSkies if pymt approved for txns received from this merchant
    '        If True <> objTxnProc.bGetMerchant(st_PayInfo) Then
    '            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
    '                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
    '                            st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > bInitOrderResponse() " + st_PayInfo.szErrDesc)
    '            Return False
    '        End If

    '        'Added on 11 Apr 2010, decrypt Merchant Password if its length > 8 characters
    '        If (st_PayInfo.szMerchantPassword.Length() > 8) Then
    '            st_PayInfo.szMerchantPassword = objHash.szDecrypt3DES(st_PayInfo.szMerchantPassword, Common.C_3DESAPPKEY, Common.C_3DESAPPVECTOR)
    '        End If

    '        If True <> objTxnProc.bGetTerminal(st_HostInfo, st_PayInfo) Then
    '            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
    '                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
    '                            st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > bInitOrderResponse() " + st_PayInfo.szErrDesc)
    '            Return False
    '        End If

    '        st_PayInfo.szTxnType = "ORDER"

    '    Catch ex As Exception
    '        bReturn = False

    '        objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), _
    '                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
    '                        st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > bInitOrderResponse() Exception: " + ex.Message)
    '    End Try

    '    Return bReturn
    'End Function

    '********************************************************************************************
    ' Class Name:		Response
    ' Function Name:	iSendACK
    ' Function Type:	Integer. Return -1 if error
    ' Parameter:        
    ' Description:		Send acknowledgement to Host
    ' History:			1 Mar 2009
    '********************************************************************************************
    Public Function iSendACK(ByVal st_PayInfo As Common.STPayInfo, ByVal st_HostInfo As Common.STHost) As Integer
        Dim szACKString As String = ""
        Dim iRet As Integer = -1

        Try
            'Generates acknowledgement string to be sent to Host
            'Added iReqRes on 23 Aug 2009
            st_PayInfo.iReqRes = 1
            iRet = iGetQueryString(szACKString, st_HostInfo.szHostTemplate, "REQ", "ACK", st_PayInfo, st_HostInfo)
            If (iRet < 0) Then
                Return iRet
            End If

            'Send Acknowledgement request HTTP string to Host
            If ("WS" = st_PayInfo.szPostMethod Or "WS2" = st_PayInfo.szPostMethod) Then
                iRet = iWSSend(szACKString, st_HostInfo.szAcknowledgementURL, st_PayInfo, st_HostInfo)
            Else
                'Send method is POST only(2), no need to process acknowledgement reply regardless whether host replies or not
                'Modified 22 Jun 2015, added Security Protocol
                'Modified 4 Oct 2016, KTB redirect query
                iRet = iHTTPSend(2, szACKString, st_HostInfo.szAcknowledgementURL, st_PayInfo, st_HostInfo, "", st_HostInfo.szProtocol)
            End If

            If (iRet <> 0) Then
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Failed to send acknowledgement request/Timeout acknowledgement response, iRet(" + CStr(iRet) + ") " + szACKString + " to " + st_HostInfo.szAcknowledgementURL)
            Else
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Acknowledgement request sent to " + st_PayInfo.szIssuingBank)

                iRet = 0
            End If
        Catch ex As Exception
            iRet = -1

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > iSendACK() Exception: " + ex.Message)
        End Try

        Return iRet
    End Function

    '********************************************************************************************
    ' Class Name:		Response
    ' Function Name:	iSendQueryHost
    ' Function Type:	Integer. Return -1 if error
    ' Parameter:        
    ' Description:		Send query request to Host and wait for query reply
    ' History:			26 Feb 2009
    '********************************************************************************************
    Public Function iSendQueryHost(ByRef st_PayInfo As Common.STPayInfo, ByVal st_HostInfo As Common.STHost) As Integer
        Dim szQueryString As String = ""
        Dim iRet As Integer = -1

        Try
            'Generates query string to be sent to Host
            'Added iReqRes on 23 Aug 2009
            st_PayInfo.iReqRes = 1

            iRet = iGetQueryString(szQueryString, st_HostInfo.szHostTemplate, "REQ", "QUERY", st_PayInfo, st_HostInfo)
            If (iRet < 0) Then
                Return iRet
            End If

            'Send Query request HTTP string to Host AND wait for Host's reply
            If ("WS" = st_PayInfo.szPostMethod.ToUpper() Or "WS2" = st_PayInfo.szPostMethod.ToUpper()) Then
                iRet = iWSSend(szQueryString, st_HostInfo.szQueryURL, st_PayInfo, st_HostInfo)

            ElseIf ("GETONLY" = st_PayInfo.szPostMethod.ToUpper()) Then
                'Added, 5 Apr 2011, to form query string(GET, "&" as field delimiter) and POST send ONLY without receiving response
                'For Thailand SCIB or bank that does not support query response in same session, i.e.
                'POST send to bank and then bank will server-to-server send query response to GW's another URL
                'Modified 22 Jun 2015, added Security Protocol
                'Modified 4 Oct 2016, added st_HostInfo, KTB redirect resp query
                iRet = iHTTPSend(2, szQueryString, st_HostInfo.szQueryURL, st_PayInfo, st_HostInfo, "", st_HostInfo.szProtocol)

            ElseIf ("GET" = Left(st_PayInfo.szPostMethod.ToUpper(), 3)) Then
                'Added on 20 Jul 2009, to cater for HTTP string to be sent to Host using POST or GET method
                If (st_PayInfo.szPostMethod.Trim().Length() > 3) Then
                    'Modified 4 Oct 2016, KTB, solve not able get S2S resp n then proceed to query bank due to redirect resp has no hashing
                    If ("GET&RECVDB" = st_PayInfo.szPostMethod.Trim().ToUpper()) Then        'Added 18 Nov 2014, KTB
                        'Modified 21 Jun 2015, added Security Protocol
                        'Modified 4 Oct 2016, KTB redirect resp query
                        iRet = iHTTPSend(8, szQueryString, st_HostInfo.szQueryURL, st_PayInfo, st_HostInfo, "", st_HostInfo.szProtocol)
                    ElseIf ("GET&BYTES" = st_PayInfo.szPostMethod.Trim().ToUpper()) Then        'Added 23 Feb 2016, UPOP                        '
                        iRet = iHTTPSend(11, szQueryString, st_HostInfo.szQueryURL, st_PayInfo, st_HostInfo, "", st_HostInfo.szProtocol)
                    ElseIf ("GET&BEARER" = st_PayInfo.szPostMethod.ToUpper()) Then              'Added  30 November 2017, Boost
                        iRet = iHTTPSend(12, szQueryString, st_HostInfo.szQueryURL, st_PayInfo, st_HostInfo, "", st_HostInfo.szProtocol)
                    ElseIf ("GET&RECV" = st_PayInfo.szPostMethod.ToUpper()) Then              'Added 8 May 2019, EXPAY
                        iRet = iHTTPSend(3, szQueryString, st_HostInfo.szQueryURL, st_PayInfo, st_HostInfo, "", st_HostInfo.szProtocol)
                    Else
                        'e.g. GET$, GET&
                        'Modified 22 Jun 2015, added Security Protocol
                        'Modified 4 Oct 2016, added st_HostInfo, KTB redirect resp query
                        iRet = iHTTPSend(5, szQueryString, st_HostInfo.szQueryURL, st_PayInfo, st_HostInfo, "", st_HostInfo.szProtocol)
                    End If
                Else
                    'HTTP string using post with recv method
                    'Modified 22 Jun 2015, added Security Protocol
                    'Modified 4 Oct 2016, added st_HostInfo, KTB redirect resp query
                    iRet = iHTTPSend(3, szQueryString, st_HostInfo.szQueryURL, st_PayInfo, st_HostInfo, "", st_HostInfo.szProtocol)
                End If
            ElseIf ("POSTONLY" = st_PayInfo.szPostMethod.ToUpper()) Then
                'Added on 26 Jul 2010
                'Modified 22 Jun 2015, added Security Protocol
                'Modified 4 Oct 2016, added st_HostInfo, KTB redirect resp query
                iRet = iHTTPSend(2, szQueryString, st_HostInfo.szQueryURL, st_PayInfo, st_HostInfo, "", st_HostInfo.szProtocol)
            Else
                'Modified 22 Jun 2015, added Security Protocol
                'Modified 4 Oct 2016, added st_HostInfo, KTB redirect resp query
                iRet = iHTTPSend(3, szQueryString, st_HostInfo.szQueryURL, st_PayInfo, st_HostInfo, "", st_HostInfo.szProtocol)
            End If

            If (iRet <> 0) Then
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Failed to send Query request/Timeout Query response, iRet(" + CStr(iRet) + ") " + szQueryString + " to " + st_HostInfo.szQueryURL)
            Else
                'Sent Query successfully
                'Added, 5 Apr 2011, for SCIB
                If ("GETONLY" = st_PayInfo.szPostMethod.ToUpper()) Then
                    'Sent Query to bank using query string(GET, delimiter "&") without expecting Query response to be returned in the same session
                    'Query response will be sent by the bank to Query Return URL
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Query request sent to " + st_PayInfo.szIssuingBank + " and " + st_PayInfo.szIssuingBank + " will send Query response to Query Return URL")
                    Return 0
                ElseIf ("" = szQueryString) Then
                    'Added, 5 Apr 2011, for KTB or banks that need DDGW to
                    'Sent Query to bank using query string(GET, delimiter "&") and expects an empty string is returned from the bank
                    'and at the same time Query response will be sent by the bank to Query Return URL
                    'This is for banks that somehow do not return response to Query Return URL if "GETONLY" is used by DDGW
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Query request sent to " + st_PayInfo.szIssuingBank + " and " + st_PayInfo.szIssuingBank + " will send Query response to Query Return URL")
                    Return 0
                End If

                'Modified 21 Oct 2015, added logging of szQueryString
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Query request sent to " + st_PayInfo.szIssuingBank + " and received reply [" + szQueryString + "]")

                ' Parses Host's query reply
                If (False = bParseQueryString(szQueryString, st_PayInfo, st_HostInfo)) Then

                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Failed to parse Query response " + szQueryString + " (Error:" + st_PayInfo.szErrDesc + ")")

                    Return -1
                End If

                'Compare certain Gateway's values with the corresponding Host's value
                If bCompareOriginalReq(st_PayInfo) <> True Then
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Error: " + st_PayInfo.szErrDesc)
                    Return -1
                End If

                iRet = 0
            End If
        Catch ex As Exception
            iRet = -1

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > iSendQueryHost() Exception: " + ex.Message)
        End Try

        Return iRet
    End Function

    '********************************************************************************************
    ' Class Name:		Response
    ' Function Name:	iSendRecvHost
    ' Function Type:	Integer. Return -1 if error
    ' Parameter:        
    ' Description:		Send request and receive response from Host
    ' History:			12 Jun 2010
    '********************************************************************************************
    Public Function iSendRecvHost(ByRef st_PayInfo As Common.STPayInfo, ByVal st_HostInfo As Common.STHost, ByVal sz_TxnType As String, ByVal sz_URL As String) As Integer
        Dim szQueryString As String = ""
        Dim iRet As Integer = -1

        Try
            'Generates query string to be sent to Host
            'Added iReqRes on 23 Aug 2009
            st_PayInfo.iReqRes = 1
            iRet = iGetQueryString(szQueryString, st_HostInfo.szHostTemplate, "REQ", sz_TxnType, st_PayInfo, st_HostInfo)
            If (iRet < 0) Then
                Return iRet
            End If

            'Send Query request HTTP string to Host AND wait for Host's reply
            If ("WS" = st_PayInfo.szPostMethod Or "WS2" = st_PayInfo.szPostMethod) Then
                iRet = iWSSend(szQueryString, sz_URL, st_PayInfo, st_HostInfo)
            ElseIf ("GET" = Left(st_PayInfo.szPostMethod.ToUpper(), 3)) Then
                'Added on 20 Jul 2009, to cater for HTTP string to be sent to Host using POST or GET method
                If (st_PayInfo.szPostMethod.Trim().Length() > 3) Then
                    'e.g. GET$, GET&
                    'Modified 22 Jun 2015, added Security Protocol
                    'Modified 4 Oct 2016, added st_HostInfo, KTB redirect resp query
                    iRet = iHTTPSend(5, szQueryString, sz_URL, st_PayInfo, st_HostInfo, "", st_HostInfo.szProtocol)
                Else
                    'HTTP string using post with recv method
                    'Modified 22 Jun 2015, added Security Protocol
                    'Modified 4 Oct 2016, added st_HostInfo, KTB redirect resp query
                    iRet = iHTTPSend(3, szQueryString, sz_URL, st_PayInfo, st_HostInfo, "", st_HostInfo.szProtocol)
                End If
            Else
                'Modified 22 Jun 2015, added Security Protocol
                'Modified 4 Oct 2016, added st_HostInfo, KTB redirect resp query
                iRet = iHTTPSend(3, szQueryString, sz_URL, st_PayInfo, st_HostInfo, "", st_HostInfo.szProtocol)
            End If

            If (iRet <> 0) Then
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Failed to send " + sz_TxnType + " request/Timeout response, iRet(" + CStr(iRet) + ") " + szQueryString + " to " + sz_URL)
            Else
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > " + sz_TxnType + " request sent to " + st_PayInfo.szIssuingBank + " and received reply [" + szQueryString + "]") 'Modified 30 Nov 2017, added szQueryString

                ' Parses Host's query reply
                If (False = bParseQueryString(szQueryString, st_PayInfo, st_HostInfo)) Then

                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Failed to parse " + sz_TxnType + " response " + szQueryString + " (Error:" + st_PayInfo.szErrDesc + ")")

                    Return -1
                End If

                iRet = 0
            End If
        Catch ex As Exception
            iRet = -1

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > iSendRecvHost() Exception: " + ex.Message)
        End Try

        Return iRet
    End Function

    '********************************************************************************************
    ' Class Name:		Response
    ' Function Name:	iSendRequeryButton
    ' Function Type:	Integer. Return -1 if error
    ' Parameter:        
    ' Description:		Send requery button page to client
    ' History:			12 Oct 2015 for BancNet
    '********************************************************************************************
    Public Function iSendRequeryButton(ByVal st_PayInfo As Common.STPayInfo, ByRef sz_HTML As String, ByVal sz_HTTPString As String, ByVal sz_URL As String) As Integer
        Dim iRet As Integer = -1

        Try
            iLoadRequeryButtonUI(sz_URL, sz_HTTPString, sz_HTML)
            iRet = 0
        Catch ex As Exception
            iRet = -1

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > iSendRequeryButton() Exception: " + ex.Message)
        End Try
        Return iRet
    End Function

    '********************************************************************************************
    ' Class Name:		Payment
    ' Function Name:	iSendEmailNotification
    ' Function Type:	NONE
    ' Parameter:        iShopperEmailNotifyStatus, iMerchantEmailNotifyStatus, stPayInfo, stHostInfo
    ' Description:		Send Email Notification
    ' History:			9 Jun 2016 - FPXD
    '********************************************************************************************
    Public Function iSendEmailNotification(ByVal i_ShopperEmailNotifyStatus As Integer, ByVal i_MerchantEmailNotifyStatus As Integer, ByVal st_PayInfo As Common.STPayInfo, ByVal st_HostInfo As Common.STHost) As Integer
        Dim iRet As Integer = -1
        Dim szTo() As String = Nothing          'Added, 24 Jan 2014
        Dim szCc() As String = Nothing          'Added, 24 Jan 2014, not used
        Dim szBcc() As String = Nothing         'Added, 24 Jan 2014, not used
        Dim szToAddress() As String = Nothing   'Added, 15 Jan 2015

        Dim szReceiptEmailBody As String = ""   'Added, 29 Jan 2014

        'Dim PGMailer As New MailService.Mailer                'Added, 24 Jan 2014
        'Dim PGMailerStatus As New MailService.MailerStatus    'Added, 24 Jan 2014
        Dim objResp As New Response(objLoggerII)
        'Dim objeMail As New MailService.eMail
        Dim objMail As New Mail(objLoggerII)
        Dim iMailStatus As Integer = -1
        Dim szStatusMsg As String = ""
        Dim szSubject As String = ""

        Dim iShopperEmailNotifyStatus As Integer = -1           'Added, 21 Apr 2017
        Dim iMerchantEmailNotifyStatus As Integer = -1          'Added, 21 Apr 2017

        Try
            ReDim szCc(0)                           'Added, 10 Feb 2014
            ReDim szBcc(0)                          'Added, 10 Feb 2014
            ReDim szTo(0)                           'Added, 15 Jan 2015

            szCc(0) = ""                            'Added, 10 Feb 2014
            szBcc(0) = ""                           'Added, 10 Feb 2014

            If (Not szTo Is Nothing) Then
                'Added, 21 Apr 2017, set default email notify status even though not yet sent, to avoid concurrent email notification sending attempt which may cause duplicate emails            
                If ((1 = st_PayInfo.iPymtNotificationEmail Or 3 = st_PayInfo.iPymtNotificationEmail) And i_ShopperEmailNotifyStatus <> 1) Then
                    iShopperEmailNotifyStatus = 1
                End If
                If ((2 = st_PayInfo.iPymtNotificationEmail Or 3 = st_PayInfo.iPymtNotificationEmail) And i_MerchantEmailNotifyStatus <> 1) Then
                    iMerchantEmailNotifyStatus = 1
                End If

                'Added, 21 Apr 2017, moved this from below to here, to avoid concurrent email notification sending attempt which may cause duplicate emails            
                If (True = objTxnProc.bInsertNotifyStatus(st_PayInfo, iShopperEmailNotifyStatus, iMerchantEmailNotifyStatus)) Then
                    'Added, 17 Jun 2015, to support TLS 1.2, for PCI
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12

                    ' allows for validation of SSL conversations
                    ServicePointManager.ServerCertificateValidationCallback = Function() True   'C#: delegate { return true }

                    'IPGMailer.Url = ConfigurationManager.AppSettings("EmailServer")    'Added, 7 Feb 2014
                    'IPGMailer.Timeout = 60000 'Milliseconds, 60s

                    'Added, 17 Feb 2014, differentiate shopper and merchant's payment notification
                    '1 - Customer only; 2 - Merchant only; 3 - Customer & Merchant
                    'Customer Email Notification
                    If ((1 = st_PayInfo.iPymtNotificationEmail Or 3 = st_PayInfo.iPymtNotificationEmail) And i_ShopperEmailNotifyStatus <> 1) Then
                        szTo(0) = st_PayInfo.szCustEmail

                        If (objResp.iLoadReceiptEmailUI_Shopper(szReceiptEmailBody, st_PayInfo, st_HostInfo) <> 0) Then
                            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                            st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Failed to load shopper receipt email body for (" + st_PayInfo.iPymtNotificationEmail.ToString() + ":" + szTo(0) + ")")
                        Else
                            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                            st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Loaded shopper receipt email body for (" + st_PayInfo.iPymtNotificationEmail.ToString() + ":" + szTo(0) + ")")
                        End If

                        szSubject = ConfigurationManager.AppSettings("EmailSubject") + " [" + st_PayInfo.szMerchantName + "]" '" [" + st_PayInfo.szMerchantTxnID + "]"
                        iMailStatus = objMail.SendMail(szTo(0), szReceiptEmailBody, szSubject)
                        szStatusMsg = objMail.StatusMessage(iMailStatus)

                        st_PayInfo.iOSRet = iMailStatus
                        st_PayInfo.szErrDesc = szStatusMsg

                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                        st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Shopper email notification [" + szTo(0) + "] [" + iMailStatus.ToString() + ":" + szStatusMsg + "]")

                        i_ShopperEmailNotifyStatus = iMailStatus
                        'objeMail.To = szTo(0)
                        'objeMail.Cc = szCc(0)
                        'objeMail.Bcc = szBcc(0)
                        'objeMail.Subject = ConfigurationManager.AppSettings("EmailSubject") + " [" + st_PayInfo.szMerchantTxnID + "]"
                        'objeMail.FromName = ConfigurationManager.AppSettings("EmailFromName")
                        'objeMail.Body = szReceiptEmailBody

                        'PGMailerStatus = PGMailer.SendMail(objeMail)

                        ''PGMailerStatus = PGMailer.SendMail(ConfigurationManager.AppSettings("EmailFromAddr"), ConfigurationManager.AppSettings("EmailFromName"), szTo, szCc, szBcc, ConfigurationManager.AppSettings("EmailSubject") + " (" + st_PayInfo.szMerchantTxnID + ")", True, szReceiptEmailBody)
                        'If (Not PGMailerStatus Is Nothing) Then
                        '    st_PayInfo.iOSRet = PGMailerStatus.StatusCode
                        '    st_PayInfo.szErrDesc = PGMailerStatus.StatusMessage

                        '    'Added, 11 Feb 2014
                        '    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                        '    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                        '    st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Shopper email notification [" + szTo(0) + "] [" + PGMailerStatus.StatusCode.ToString() + ":" + PGMailerStatus.StatusMessage + "]")

                        '    i_ShopperEmailNotifyStatus = PGMailerStatus.StatusCode  'Added  21 Nov 2014 - ENS
                        'End If
                    End If

                    'Merchant Email Notification
                    If ((2 = st_PayInfo.iPymtNotificationEmail Or 3 = st_PayInfo.iPymtNotificationEmail) And i_MerchantEmailNotifyStatus <> 1) Then
                        'szTo(0) = st_PayInfo.szMerchantNotifyEmailAddr  'Commented  14 Jan 2015
                        szToAddress = Split(st_PayInfo.szMerchantNotifyEmailAddr, ",")  'Modified 15 Jan 2015, support multiple merchant payment notification email addresses

                        If (objResp.iLoadReceiptEmailUI_Merchant(szReceiptEmailBody, st_PayInfo, st_HostInfo) <> 0) Then
                            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                            st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Failed to load merchant receipt email body for (" + st_PayInfo.iPymtNotificationEmail.ToString() + ":" + st_PayInfo.szMerchantNotifyEmailAddr + ")")
                        Else
                            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                            st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Loaded merchant receipt email body for (" + st_PayInfo.iPymtNotificationEmail.ToString() + ":" + st_PayInfo.szMerchantNotifyEmailAddr + ")")
                        End If

                        'Added, 15 Jan 2015, for loop
                        For iCount = 0 To szToAddress.GetUpperBound(0)
                            szSubject = ConfigurationManager.AppSettings("EmailSubject") + " [" + st_PayInfo.szMerchantTxnID + "]"
                            iMailStatus = objMail.SendMail(szToAddress(iCount), szReceiptEmailBody, szSubject)
                            szStatusMsg = objMail.StatusMessage(iMailStatus)

                            st_PayInfo.iOSRet = iMailStatus
                            st_PayInfo.szErrDesc = szStatusMsg

                            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Merchant email notification " + CStr(iCount + 1) + " [" + szToAddress(iCount) + "] [" + iMailStatus.ToString() + ":" + szStatusMsg + "]")

                            i_MerchantEmailNotifyStatus = iMailStatus
                            'Modified 15 Jan 2015, added szToAddress
                            ''szTo(0) = szToAddress(iCount)
                            'objeMail.To = szToAddress(iCount)
                            'objeMail.Cc = szCc(0)
                            'objeMail.Bcc = szBcc(0)
                            'objeMail.Subject = ConfigurationManager.AppSettings("EmailSubject") + " [" + st_PayInfo.szMerchantTxnID + "]"
                            'objeMail.FromName = ConfigurationManager.AppSettings("EmailFromName")
                            'objeMail.Body = szReceiptEmailBody

                            'PGMailerStatus = PGMailer.SendMail(objeMail)
                            ''PGMailerStatus = PGMailer.SendMail(ConfigurationManager.AppSettings("EmailFromAddr"), ConfigurationManager.AppSettings("EmailFromName"), szTo, szCc, szBcc, ConfigurationManager.AppSettings("EmailSubject") + " (" + st_PayInfo.szMerchantTxnID + ")", True, szReceiptEmailBody)
                            'If (Not PGMailerStatus Is Nothing) Then
                            '    'Added, 11 Feb 2014
                            '    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                            '    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            '    st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Merchant email notification [" + szTo(0) + "] [" + PGMailerStatus.StatusCode.ToString() + ":" + PGMailerStatus.StatusMessage + "]")

                            '    i_MerchantEmailNotifyStatus = PGMailerStatus.StatusCode  'Added  21 Nov 2014 - ENS
                            'End If
                        Next
                    End If
                Else
                    'Added, 21 Apr 2017, to avoid duplicate email notification due to concurrent redirect and s2s payment response
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > InsertNotifyStatus failed due to already inserted by other payment resp")
                End If

                'Added  21 Nov 2014. - ENS
                'Commented  21 Apr 2017, moved to above to avoid concurrent email notification sending attempt which may cause duplicate emails
                'objTxnProc.bInsertNotifyStatus(st_PayInfo, i_ShopperEmailNotifyStatus, i_MerchantEmailNotifyStatus)
            End If

        Catch ex As Exception
            'iRet = -1
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > iSendEmailNotification() Exception: " + ex.Message)
        Finally
            If Not objResp Is Nothing Then
                objResp.Dispose()
                objResp = Nothing
            End If
        End Try

        Return iRet
    End Function

    '********************************************************************************************
    ' Class Name:		Response
    ' Function Name:	bParseQueryString
    ' Function Type:	
    ' Parameter:		
    ' Description:		Parses HTTP query string
    ' History:			27 Feb 2009
    '********************************************************************************************
    Public Function bParseQueryString(ByVal sz_QueryString As String, ByRef st_PayInfo As Common.STPayInfo, ByVal st_HostInfo As Common.STHost, Optional ByVal sz_Res As String = "") As Boolean
        Dim bReturn As Boolean = False

        Dim iLoop As Integer = 0
        Dim iCounter As Integer = 0
        Dim iIndex As Integer = 0
        Dim iField As Integer = 0
        Dim szConfigFile As String = ""
        Dim objStreamReader As StreamReader
        Dim szLine As String = ""
        Dim szReqOrRes As String = ""
        Dim szConfig() As String
        Dim szFieldSet
        Dim szFieldSets() As String
        Dim szField() As String
        Dim szFieldName() As String
        Dim szFieldValue() As String
        Dim szValue As String = ""
        Dim szHashValue As String = ""
        Dim aszValue() As String
        Dim szFieldEx As String = ""
        Dim szFormat As String = ""
        Dim szHashMethod As String = ""
        Dim szDelimiter
        Dim szDelimiterEx As String = ""    'Added on 6 Apr 2010
        Dim bFound As Boolean = False
        Dim stPayInfo As Common.STPayInfo 'Added on 16 June 2009

        Try
            'Parse Host reply based on the respective Host configuration file
            'Added iReqRes on 23 Aug 2009
            stPayInfo.iReqRes = 2
            szReqOrRes = "RES" + sz_Res
            szConfigFile = Server.MapPath(st_HostInfo.szHostTemplate)
            objStreamReader = System.IO.File.OpenText(szConfigFile)

            Do
                szLine = objStreamReader.ReadLine()

                'Added, 10 Sept 2013
                If ("" = szLine) Then
                    Exit Do
                End If

                szConfig = Split(szLine, "|")

                'Added on 12 Jun 2010 ==> Or (szConfig(0).Trim().ToUpper() = st_PayInfo.szTxnType)
                If (((szConfig(0).Trim().ToUpper() = "QUERY") Or (szConfig(0).Trim().ToUpper() = st_PayInfo.szTxnType)) And (szConfig(1).Trim().ToUpper() = szReqOrRes.ToUpper())) Then
                    bFound = True
                    Exit Do
                End If
            Loop Until (szLine Is Nothing)

            objStreamReader.Close()

            If (False = bFound) Then
                st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                st_PayInfo.szErrDesc = " > TxnType(" + szReqOrRes + "_" + st_PayInfo.szTxnType + ") not found in Config Template for " + st_PayInfo.szIssuingBank

                GoTo CleanUp
            End If

            'Gets all response field names and field values into array
            Select Case szConfig(2).Trim().ToUpper()
                Case "LF"
                    szDelimiter = vbLf
                Case "CR"
                    szDelimiter = vbCr
                Case "CRLF"
                    szDelimiter = vbCrLf
                Case "PIPE"   'Added  9 Jul 2012, for FPX
                    szDelimiter = "|"
                Case Else
                    szDelimiter = szConfig(2).Trim()
            End Select

            If ("" = szDelimiter) Then
                st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                st_PayInfo.szErrDesc = " > Row delimiter setting should not be empty, must be either LF/CR/CRLF or other non-empty delimiter"

                GoTo CleanUp
            End If

            'Added on 20 Jul 2009 for checking of <> "GET" And <> "POST" coz if it is "GET" then "GET" cannot be treated as a delimiter and should be parsed differently
            'This portion is for row delimiter for reply message
            If (szConfig(2).Trim().ToUpper() <> "WS" And Left(szConfig(2).Trim().ToUpper(), 3) <> "GET" And szConfig(2).Trim().ToUpper() <> "POST" And szConfig(2).Trim().ToUpper() <> "JSON") Then 'Modified  5 Dec 2017, Boost, Cater for JSON
                ' Splits as case NOT sensitive, e.g. <BR> is same as <br>
                szFieldSets = Split(sz_QueryString, szDelimiter, -1, CompareMethod.Text)

                iField = 0
                For Each szFieldSet In szFieldSets
                    'Added on 8 Aug 2009, to cater for the presence of rightmost "==" or "=", normally for encrypted or hash value
                    'Replace the last two "==" or last "=" character(s) with "@@" or "@" accordingly to avoid inaccurate
                    'array set from the result of split by "="

                    'Added on 19 Aug 2009
                    szFieldSet = Trim(Replace(Replace(Replace(szFieldSet, vbCrLf, ""), vbCr, ""), vbLf, ""))
                    If ("==" = Right(szFieldSet, 2)) Then
                        'Differentiate between "field==" from "field=h==" 
                        'Replace "field==" to "field=@" and "field=h==" to "field=h@@"
                        If ((szFieldSet.IndexOf("=") + 1) = (szFieldSet.Length() - 1)) Then
                            szFieldSet = Left(szFieldSet, Len(szFieldSet) - 1) + "@"
                        Else
                            szFieldSet = Left(szFieldSet, Len(szFieldSet) - 2) + "@@"
                        End If
                    ElseIf ("=" = Right(szFieldSet, 1)) Then
                        'Differentiate between "field=" (empty value) from "field=value="
                        'Checks if "=" is the character at the last position of the field value OR, e.g. "field=value="
                        'if "=" is the field delimiter, e.g. "field="
                        'Make sure "=" does not exist at the last character of szFieldSet or else
                        'the array result from the next Split by "=" will encounter out of bound exception
                        If (szFieldSet.IndexOf("=") <> szFieldSet.Length() - 1) Then
                            szFieldSet = Left(szFieldSet, Len(szFieldSet) - 1) + "@"
                        End If
                    End If

                    szField = Split(szFieldSet, "=")
                    If (1 = szField.GetUpperBound(0)) Then
                        iField = iField + 1
                    Else
                        'Added  12 Jul 2012, to cater for FPX
                        szField = Split(szFieldSet, ":")
                        If (1 = szField.GetUpperBound(0)) Then
                            iField = iField + 1
                        End If
                    End If
                Next

                ReDim szFieldName(iField - 1)
                ReDim szFieldValue(iField - 1)

                iField = 0
                For Each szFieldSet In szFieldSets
                    'Added on 8 Aug 2009, to cater for the presence of rightmost "==" or "=", normally for encrypted or hash value
                    'Replace the last two "==" or last "=" character(s) with "@@" or "@" accordingly to avoid inaccurate
                    'array set from the result of split by "="

                    'Added on 19 Aug 2009
                    szFieldSet = Trim(Replace(Replace(Replace(szFieldSet, vbCrLf, ""), vbCr, ""), vbLf, ""))
                    If ("==" = Right(szFieldSet, 2)) Then
                        'Differentiate between "field==" from "field=h==" 
                        'Replace "field==" to "field=@" and "field=h==" to "field=h@@"
                        If ((szFieldSet.IndexOf("=") + 1) = (szFieldSet.Length() - 1)) Then
                            szFieldSet = Left(szFieldSet, Len(szFieldSet) - 1) + "@"
                        Else
                            szFieldSet = Left(szFieldSet, Len(szFieldSet) - 2) + "@@"
                        End If
                    ElseIf ("=" = Right(szFieldSet, 1)) Then
                        'Differentiate between "field=" (empty value) from "field=value="
                        'Checks if "=" is the character at the last position of the field value OR, e.g. "field=value="
                        'if "=" is the field delimiter, e.g. "field="
                        'Make sure "=" does not exist at the last character of szFieldSet or else
                        'the array result from the next Split by "=" will encounter out of bound exception
                        If (szFieldSet.IndexOf("=") <> szFieldSet.Length() - 1) Then
                            szFieldSet = Left(szFieldSet, Len(szFieldSet) - 1) + "@"
                        End If
                    End If

                    szField = Split(szFieldSet, "=")

                    If (1 = szField.GetUpperBound(0)) Then
                        'Added on 8 Aug 2009, to cater for the presence of rightmost "==" or "=", normally for encrypted or hash value
                        'Replace the last two "@@" or last "@" character(s) back to the original "==" or "=" accordingly
                        'to get back the original value since no split will be used now
                        'Do not use Replace because do not want to replace "@" which exists not in the last two or last character(s)
                        If ("@@" = Right(szField(1), 2)) Then
                            szField(1) = Left(szField(1), Len(szField(1)) - 2) + "=="
                        ElseIf ("@" = Right(szField(1), 1)) Then
                            szField(1) = Left(szField(1), Len(szField(1)) - 1) + "="
                        End If

                        szFieldName(iField) = szField(0)
                        szFieldValue(iField) = szField(1)
                        iField = iField + 1
                    Else
                        'Added  12 Jul 2012, to cater for FPX
                        szField = Split(szFieldSet, ":")
                        If (1 = szField.GetUpperBound(0)) Then
                            szFieldName(iField) = szField(0)
                            szFieldValue(iField) = szField(1)
                            iField = iField + 1
                        End If
                    End If
                Next
            End If

            'Parse text line of host reply message configuration and populate payment information
            'Txn Type|Request/Response?|Post Method|Host Field Name|Gateway Field Variable to be filled with Host's value|....
            'CIMB:QUERY|RES|GET|billAccountNo|[OrderNumber]|billReferenceNo|[GatewayTxnID]|amount|[TxnAmount]|paymentRefNo|[BankRefNo]|status|[TxnStatus]
            'Not yet supported: [CurrencyCode_9]|[DATETIME_yy-MM-dd hh:mm:ss.fff]|[HashValue_Field1|Field2|Field3]
            For iLoop = 3 To szConfig.GetUpperBound(0)
                If (0 <> (iLoop Mod 2)) Then
                    'Verify Host Field Name
                    If (InStr(szConfig(iLoop), "[") > 0) Or (InStr(szConfig(iLoop), "]") > 0) Then
                        st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                        st_PayInfo.szErrDesc = " > Invalid host field name(" + szConfig(iLoop) + ") in Config Template for " + st_PayInfo.szIssuingBank

                        GoTo CleanUp
                    End If

                    If ("[=" = Left(szConfig(iLoop).Trim(), 2)) Then
                        szValue = Mid(szConfig(iLoop).Trim(), 3, szConfig(iLoop).Trim().Length - 3)
                    ElseIf ("0" = szConfig(iLoop).Trim()) Then
                        'Added on 20 Aug 2009, Retrieve the whole reply string
                        szValue = sz_QueryString
                    Else
                        bFound = False

                        If ("WS" = szConfig(2).Trim().ToUpper()) Then
                            szValue = szGetTagValue(sz_QueryString, szConfig(iLoop).Trim(), st_PayInfo)
                            If (szValue <> "") Then
                                bFound = True
                                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                " Field(" + szConfig(iLoop).Trim() + ") Value(" + szValue + ")")
                            End If
                        ElseIf ("HTML" = szConfig(2).Trim().ToUpper()) Then
                            'Added "HTML" on 13 Jun 2010
                            szValue = szGetHTMLTagValue(sz_QueryString, szConfig(iLoop).Trim(), st_PayInfo)
                            If (szValue <> "") Then
                                bFound = True
                                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                " Field(" + szConfig(iLoop).Trim() + ") Value(" + szValue + ")")
                            End If
                        ElseIf ("JSON" = szConfig(2).Trim().ToUpper()) Then 'Added  5 Dec 2017, Boost, Cater for JSON
                            'Added "HTML" on 13 Jun 2010
                            szValue = szGetJsonTagValue(sz_QueryString, szConfig(iLoop).Trim())
                            If (szValue <> "") Then
                                bFound = True
                                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                " Field(" + szConfig(iLoop).Trim() + ") Value(" + szValue + ")")
                            End If
                        Else
                            For iField = 0 To szFieldName.GetUpperBound(0)
                                If (szConfig(iLoop).Trim().ToLower() = szFieldName(iField).ToLower()) Then
                                    szValue = szFieldValue(iField)
                                    bFound = True
                                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                    " Field(" + szFieldName(iField) + ") Value(" + szFieldValue(iField) + ")")
                                    Exit For
                                End If
                            Next
                        End If

                        If (False = bFound) Then
                            'Commented GoTo CleanUp on 30 June 2009, to cater for optional reply fields, if field not found, set default value as empty
                            'st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                            'st_PayInfo.szErrDesc = " > Field(" + szConfig(iLoop).Trim() + ") not found from " + st_PayInfo.szIssuingBank + " Host reply"
                            'GoTo CleanUp

                            'Added the following on 30 June 2009
                            szValue = ""
                            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                            " Field(" + szConfig(iLoop).Trim() + ") not found from " + st_PayInfo.szIssuingBank + " reply, set empty")
                        End If
                    End If
                Else
                    'Verify Gateway Field Value tag
                    If (InStr(szConfig(iLoop), "[") <= 0) Or (InStr(szConfig(iLoop), "]") <= 0) Then
                        st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                        st_PayInfo.szErrDesc = " > Invalid gateway field value tag(" + szConfig(iLoop) + ") in Config Template for " + st_PayInfo.szIssuingBank

                        GoTo CleanUp
                    End If

                    szFormat = ""
                    szFieldEx = szConfig(iLoop).Trim().ToLower()
                    iIndex = InStr(szFieldEx, "{")    'InStr starts with 1
                    If (iIndex > 0) Then
                        'Example: Hash|[HashValue]{#[HashMethod]![ReturnKey]![-]!RespCode![-]!InvID![-]!InvRef![-]!InvCy![-]!InvTotal![-]!InvTest![-]!MerchRef#}
                        szFormat = szFieldEx.Substring(iIndex).Replace("}", "")
                        szFieldEx = szFieldEx.Substring(0, iIndex - 1)
                    End If

                    Select Case szFieldEx
                        Case "[ordernumber]"
                            st_PayInfo.szHostOrderNumber = szValue
                        Case "[gatewaytxnid]"
                            st_PayInfo.szHostGatewayTxnID = szValue
                        Case "[txnamount]"      'Amount in 2 decimals
                            st_PayInfo.szHostTxnAmount = szValue
                        Case "[txnamount_(2)]"  'Amount in cents whereby the last two digits are decimal points
                            st_PayInfo.szHostTxnAmount = szValue.Replace(".", "")
                        Case "[bankrefno]"
                            st_PayInfo.szHostTxnID = szValue
                        Case "[param1]"
                            st_PayInfo.szParam1 = szValue
                        Case "[param2]"
                            st_PayInfo.szParam2 = szValue
                        Case "[param3]"
                            st_PayInfo.szParam3 = szValue
                        Case "[param4]"
                            st_PayInfo.szParam4 = szValue
                        Case "[param5]"
                            st_PayInfo.szParam5 = szValue
                        Case "[param6]"
                            st_PayInfo.szParam6 = szValue
                        Case "[param7]"
                            st_PayInfo.szParam7 = szValue
                        Case "[param8]"
                            st_PayInfo.szParam8 = szValue
                        Case "[param9]"
                            st_PayInfo.szParam9 = szValue
                        Case "[param10]"
                            st_PayInfo.szParam10 = szValue
                        Case "[param11]"
                            st_PayInfo.szParam11 = szValue
                        Case "[param12]"
                            st_PayInfo.szParam12 = szValue
                        Case "[param13]"
                            st_PayInfo.szParam13 = szValue
                        Case "[param14]"
                            st_PayInfo.szParam14 = szValue
                        Case "[param15]"
                            st_PayInfo.szParam15 = szValue
                        Case "[currencycode]"   'ASCII currency code, e.g. MYR, IDR..
                            st_PayInfo.szHostCurrencyCode = szValue
                        Case "[mid]"
                            st_PayInfo.szHostMID = szValue
                        Case "[tid]"
                            st_PayInfo.szHostTID = szValue
                        Case "[txnstatus]"
                            'Added default value "-" if szValue is empty on 24 Aug 2009 to cater for AliPay empty
                            '<trade_status> if AliPay did not receive the out_trade_no (GatewayTxnID)
                            If ("" = szValue) Then
                                szValue = "-"
                            End If
                            st_PayInfo.szHostTxnStatus = szValue
                        Case "[respcode]"
                            'Added default value "-" if szValue is empty on 24 Aug 2009 to cater for AliPay empty
                            '<trade_status> if AliPay did not receive the out_trade_no (GatewayTxnID)
                            If ("" = szValue) Then
                                szValue = "-"
                            End If
                            st_PayInfo.szRespCode = szValue
                        Case "[authcode]"
                            st_PayInfo.szAuthCode = szValue
                        Case "[respmesg]"
                            st_PayInfo.szBankRespMsg = szValue
                        Case "[date]"
                            st_PayInfo.szHostDate = szValue
                        Case "[time]"
                            st_PayInfo.szHostTime = szValue
                        Case "[cardpan]"
                            st_PayInfo.szCardPAN = szValue

                            If (st_PayInfo.szCardPAN.Length > 20) Then
                                st_PayInfo.szCardPAN = st_PayInfo.szCardPAN.Substring(0, 20)
                            End If

                            If st_PayInfo.szCardPAN.Length > 10 Then
                                st_PayInfo.szMaskedCardPAN = st_PayInfo.szCardPAN.Substring(0, 6) + "".PadRight(st_PayInfo.szCardPAN.Length - 6 - 4, "X") + st_PayInfo.szCardPAN.Substring(st_PayInfo.szCardPAN.Length - 4, 4)
                            Else
                                st_PayInfo.szMaskedCardPAN = "".PadRight(st_PayInfo.szCardPAN.Length, "X")
                            End If
                        Case "[sessionid]"
                            st_PayInfo.szMerchantSessionID = szValue            'Modified 16 Jul 2014, SessionID to MerchantSessionID
                        Case "[hashvalue]"
                            st_HostInfo.szHashValue = szValue.Replace(" ", "+") 'Added replacement on 3 Apr 2010
                        Case "[reserved]"   'Added on 20 Aug 2009
                            st_HostInfo.szReserved = szValue
                            'Case "[redirecturl]"                                ' Added  on 3 Jan 2011. POLi
                            '    stPayInfo.szRedirectURL = szValue
                        Case "[txnamount_02]"   'Added on 25 Oct 2009. 12 digits, leading zeroes, no decimal point, last 2 are decimal values. On 24 Jun 2010, modified CDbl to Convert.ToDecimal becoz CDbl will round up the value.
                            If ("" <> szValue) Then
                                st_PayInfo.szHostTxnAmount = CStr(Convert.ToDecimal(Left(szValue, szValue.Length - 2) + "." + Right(szValue, 2)))
                            End If
                        Case "[eci]"
                            st_PayInfo.szECI = szValue
                        Case "[payerauth]"
                            st_PayInfo.szPayerAuth = szValue
                        Case "[ttlrefundamt]"
                            'If "" <> szValue Then
                            '    If "null" = szValue.ToLower() Then
                            '        szValue = ""     'Set to empty
                            '    End If
                            'End If
                            st_PayInfo.szHostTtlRefundAmt = szValue
                        Case "[txnamount_2d]"           'Added by HS on 3 Apr 2019. POLi. Format 2 decimal place to cater the value is without decimal point or 1 decimal place. 
                            If ("" <> szValue) Then
                                Dim strArr() As String = szValue.Split(".")

                                If (Convert.ToDecimal(szValue) Mod 1 = 0) Then
                                    st_PayInfo.szHostTxnAmount = FormatNumber(CDbl(szValue), 2)
                                ElseIf (strArr.Length = 1 And strArr(1).Length < 3) Then
                                    st_PayInfo.szHostTxnAmount = FormatNumber(CDbl(szValue), 2)
                                Else
                                    st_PayInfo.szHostTxnAmount = szValue
                                End If
                            End If
                        Case Else
                            'Unsupported value tag
                            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                            st_PayInfo.szErrDesc = " > Unsupported gateway field value tag(" + szConfig(iLoop) + ") in Config Template for " + st_PayInfo.szIssuingBank

                            GoTo CleanUp
                    End Select

                    'Verification of field value based on format specified
                    'Example: #[HashMethod]![ReturnKey]![-]!RespCode![-]!InvID![-]!InvRef![-]!InvCy![-]!InvTotal![-]!InvTest![-]!MerchRef#
                    'Example: [=5]
                    If (szFormat <> "") Then
                        'Added on 20 Aug 2009
                        'Encryption - The whole reply message is encrypted
                        If ("*" = Left(szFormat, 1)) Then
                            szValue = szFormat.Replace("*", "")
                            'szValue: e.g. [3DES]!&!BillRef1![GatewayTxnID]!TxDate![Date]!TxTime![Time]!Currency![CurrencyCode]!Amount![TxnAmount]!ReturnCode![RespCode]!ReturnMessage![RespMesg]!Md5![HashValue]
                            aszValue = Split(szValue, "!")

                            If ("[3des]" = aszValue(0).Trim().ToLower() Or ("[scb]" = aszValue(0).Trim().ToLower())) Then
                                Dim szFldSet
                                Dim szFldSets() As String
                                Dim szFldEx() As String

                                If ("[3des]" = aszValue(0).Trim().ToLower()) Then
                                    'Modified on 25 May 2009
                                    'NOTE: Use szDecrypt3DEStoHex instead of szDecrypt3DES because original clear text(secret key) is HEX value
                                    'Dim szKey As String = stHostInfo.szSecretKey
                                    Dim szKey As String = objHash.szDecrypt3DEStoHex(st_HostInfo.szSecretKey, Common.C_3DESAPPKEY, Common.C_3DESAPPVECTOR)
                                    Dim szIV As String = st_HostInfo.szInitVector

                                    'Decrypts the whole encrypted reply message stored in Reserved field
                                    szValue = objHash.szDecrypt3DES(st_HostInfo.szReserved, szKey, szIV)

                                ElseIf ("[scb]" = aszValue(0).Trim().ToLower()) Then
                                    szValue = "decrypt^" + st_HostInfo.szReserved

                                    'Decrypts the whole encrypted reply message stored in Reserved field
                                    szValue = objHash.szGetHash(szValue, st_HostInfo.szHashMethod, st_PayInfo, st_HostInfo) 'Added on 15 Dec 2010 , add 'stHostInfo' for uobThai to get secret key for encryption 'Added 15 March 2017, SCBTH_MY chg from stPayInfo to st_PayInfo
                                End If

                                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                " Decrypted Value(" + szValue + ")")

                                Dim szTempValues As String = ""
                                If ("WS" = aszValue(1).Trim().ToUpper()) Then
                                    szTempValues = szValue
                                Else
                                    'szValue: e.g. BillRef1=20050718-021&BillRef2=Richard&TxDate=18072005&TxTime=180203&ExDate=18072005&ExTime=166607&Currency=458&Amount=1000.99&ConfNum=669987&ReturnCode=000&ReturnMessage=Approved.&Active=1&Md5=cb2fe847bfb14fb64cd79f40a058347b
                                    'szValue: e.g. BillRef1=330700000000000073&TxDate=30042009&TxTime=180815&Currency=458&Amount=190.99&ReturnCode=666&ReturnMessage=NOT FOUND
                                    szFldSets = Split(szValue, aszValue(1).Trim(), -1, CompareMethod.Text)

                                    'Gets number of field sets (e.g. 2 sets ==> Field1=Value1&Field2=Value2)
                                    iField = 0
                                    For Each szFldSet In szFldSets
                                        'Added on 8 Aug 2009, to cater for the presence of rightmost "==" or "=", normally for encrypted or hash value
                                        'Replace the last two "==" or last "=" character(s) with "@@" or "@" accordingly to avoid inaccurate
                                        'array set from the result of split by "="

                                        'Added on 19 Aug 2009
                                        szFldSet = Trim(Replace(Replace(Replace(szFldSet, vbCrLf, ""), vbCr, ""), vbLf, ""))
                                        If ("==" = Right(szFldSet, 2)) Then
                                            'Differentiate between "field==" from "field=h==" 
                                            'Replace "field==" to "field=@" and "field=h==" to "field=h@@"
                                            If ((szFldSet.IndexOf("=") + 1) = (szFldSet.Length() - 1)) Then
                                                szFldSet = Left(szFldSet, Len(szFldSet) - 1) + "@"
                                            Else
                                                szFldSet = Left(szFldSet, Len(szFldSet) - 2) + "@@"
                                            End If
                                        ElseIf ("=" = Right(szFldSet, 1)) Then
                                            'Differentiate between "field=" (empty value) from "field=value="
                                            'Checks if "=" is the character at the last position of the field value OR, e.g. "field=value="
                                            'if "=" is the field delimiter, e.g. "field="
                                            'Make sure "=" does not exist at the last character of szFldSet or else
                                            'the array result from the next Split by "=" will encounter out of bound exception
                                            If (szFldSet.IndexOf("=") <> szFldSet.Length() - 1) Then
                                                szFldSet = Left(szFldSet, Len(szFldSet) - 1) + "@"
                                            End If
                                        End If

                                        szFldEx = Split(szFldSet, "=")
                                        If (1 = szFldEx.GetUpperBound(0)) Then
                                            iField = iField + 1
                                            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(),
                                                            " Field(" + szFldEx(0) + ") Value(" + szFldEx(1) + ")")
                                        End If
                                    Next

                                    ReDim szFieldName(iField - 1)
                                    ReDim szFieldValue(iField - 1)

                                    'Populates Field Names and Values array
                                    iField = 0
                                    For Each szFldSet In szFldSets
                                        'Added on 8 Aug 2009, to cater for the presence of rightmost "==" or "=", normally for encrypted or hash value
                                        'Replace the last two "==" or last "=" character(s) with "@@" or "@" accordingly to avoid inaccurate
                                        'array set from the result of split by "="

                                        'Added on 19 Aug 2009
                                        szFldSet = Trim(Replace(Replace(Replace(szFldSet, vbCrLf, ""), vbCr, ""), vbLf, ""))
                                        If ("==" = Right(szFldSet, 2)) Then
                                            'Differentiate between "field==" from "field=h==" 
                                            'Replace "field==" to "field=@" and "field=h==" to "field=h@@"
                                            If ((szFldSet.IndexOf("=") + 1) = (szFldSet.Length() - 1)) Then
                                                szFldSet = Left(szFldSet, Len(szFldSet) - 1) + "@"
                                            Else
                                                szFldSet = Left(szFldSet, Len(szFldSet) - 2) + "@@"
                                            End If
                                        ElseIf ("=" = Right(szFldSet, 1)) Then
                                            'Differentiate between "field=" (empty value) from "field=value="
                                            'Checks if "=" is the character at the last position of the field value OR, e.g. "field=value="
                                            'if "=" is the field delimiter, e.g. "field="
                                            'Make sure "=" does not exist at the last character of szFldSet or else
                                            'the array result from the next Split by "=" will encounter out of bound exception
                                            If (szFldSet.IndexOf("=") <> szFldSet.Length() - 1) Then
                                                szFldSet = Left(szFldSet, Len(szFldSet) - 1) + "@"
                                            End If
                                        End If

                                        szFldEx = Split(szFldSet, "=")

                                        If (1 = szFldEx.GetUpperBound(0)) Then
                                            'Added on 8 Aug 2009, to cater for the presence of rightmost "==" or "=", normally for encrypted or hash value
                                            'Replace the last two "@@" or last "@" character(s) back to the original "==" or "=" accordingly
                                            'to get back the original value since no split will be used now
                                            'Do not use Replace because do not want to replace "@" which exists not in the last two or last character(s)
                                            If ("@@" = Right(szFldEx(1), 2)) Then
                                                szFldEx(1) = Left(szFldEx(1), Len(szFldEx(1)) - 2) + "=="
                                            ElseIf ("@" = Right(szFldEx(1), 1)) Then
                                                szFldEx(1) = Left(szFldEx(1), Len(szFldEx(1)) - 1) + "="
                                            End If

                                            szFieldName(iField) = szFldEx(0)
                                            szFieldValue(iField) = szFldEx(1)
                                            iField = iField + 1
                                        End If
                                    Next
                                End If
                                'Matches each Field Name from message template against Field Names array to get its respective Field Value.
                                'Then, assigns the respective Field Value to the respective Gateway Varialbes as defined in the message template.
                                'e.g. BillRef1![GatewayTxnID]!TxDate![Date]!TxTime![Time]!Currency![CurrencyCode]!Amount![TxnAmount]!ConfNum![BankRefNo]!ReturnCode![RespCode]!ReturnMessage![RespMesg]!Md5![HashValue]{#[HashMethod]:BillRef1:[]:Amount:[]:Currency:[]:ConfNum#}*
                                bFound = False
                                For iCounter = 2 To aszValue.GetUpperBound(0)
                                    If (0 = (iCounter Mod 2)) Then
                                        If ("WS" = aszValue(1).Trim().ToUpper()) Then
                                            szValue = szGetTagValue(szTempValues, aszValue(iCounter).Trim(), st_PayInfo)
                                            If (szValue <> "") Then
                                                bFound = True
                                                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                                " Field(" + aszValue(iCounter).Trim() + ") Value(" + szValue + ")")
                                            End If

                                        Else
                                            For iField = 0 To szFieldName.GetUpperBound(0)
                                                If (aszValue(iCounter).Trim().ToLower() = szFieldName(iField).ToLower()) Then
                                                    szValue = szFieldValue(iField)
                                                    bFound = True
                                                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                                    " Field(" + szFieldName(iField) + ") Value(" + szFieldValue(iField) + ")")
                                                    Exit For
                                                End If
                                            Next
                                        End If
                                    Else
                                        szFormat = ""
                                        szFieldEx = aszValue(iCounter).Trim().ToLower()
                                        iIndex = InStr(szFieldEx, "{")    'InStr starts with 1
                                        If (iIndex > 0) Then
                                            'Example1: Md5|[HashValue]{#[HashMethod]:BillRef1:[]:Amount:[]:Currency:[]:ConfNum#}
                                            'Example2: Hash|[HashValue]{#[HashMethod]![ReturnKey]![-]!RespCode![-]!InvID![-]!InvRef![-]!InvCy![-]!InvTotal![-]!InvTest![-]!MerchRef#}
                                            szFormat = szFieldEx.Substring(iIndex).Replace("}", "")
                                            szFieldEx = szFieldEx.Substring(0, iIndex - 1)
                                        End If

                                        Select Case szFieldEx
                                            Case "[ordernumber]"    'Added on 13 Nov 2009, put HostGatewayTxnID instead of TxnID because it is returned by the bank
                                                st_PayInfo.szHostOrderNumber = szValue
                                            Case "[gatewaytxnid]"   'Added on 13 Nov 2009, reason is same as the above
                                                st_PayInfo.szHostGatewayTxnID = szValue
                                            Case "[txnamount]"      'Amount in 2 decimals   'Added on 13 Nov 2009, reason is same as the above
                                                st_PayInfo.szHostTxnAmount = szValue
                                            Case "[txnamount_2]"    'Amount without decimal point but the last two digits are decimal values
                                                If ("" <> szValue) Then
                                                    'Added, 14 Nov 2013, checking of szValue's length to cater for only 1 to 2 digits returned
                                                    If (1 = szValue.Length()) Then
                                                        szValue = "00" + szValue
                                                    ElseIf (2 = szValue.Length()) Then
                                                        szValue = "0" + szValue
                                                    End If
                                                    st_PayInfo.szHostTxnAmount = Left(szValue, szValue.Length - 2) + "." + Right(szValue, 2)
                                                End If
                                            Case "[txnamount_02]"   'Added on 25 Oct 2009. 12 digits, leading zeroes, no decimal point, last 2 are decimal values. On 24 Jun 2010, modified CDbl to Convert.ToDecimal becoz CDbl will round up the value.
                                                If ("" <> szValue) Then
                                                    st_PayInfo.szHostTxnAmount = CStr(Convert.ToDecimal(Left(szValue, szValue.Length - 2) + "." + Right(szValue, 2)))
                                                End If
                                            Case "[currencycode]"   'ASCII currency code, e.g. MYR, IDR..
                                                st_PayInfo.szHostCurrencyCode = szValue

                                                'Case "[ordernumber]"
                                                '    stPayInfo.szMerchantOrdID = szValue
                                                'Case "[gatewaytxnid]"
                                                '    stPayInfo.szTxnID = szValue
                                                'Case "[txnamount]"      'Amount in 2 decimals
                                                '    stPayInfo.szTxnAmount = szValue
                                                'Case "[txnamount_2]"    'Amount in cents whereby the last two digits are decimal points
                                                '    stPayInfo.szTxnAmount = Left(szValue, szValue.Length - 2) + "." + Right(szValue, 2)
                                                'Case "[txnamount_02]"   'Added on 25 Oct 2009. 12 digits, leading zeroes, no decimal point, last 2 are decimal values
                                                '    stPayInfo.szTxnAmount = CStr(CDbl(Left(szValue, szValue.Length - 2) + "." + Right(szValue, 2)))

                                            Case "[bankrefno]"
                                                st_PayInfo.szHostTxnID = szValue
                                            Case "[param1]"
                                                st_PayInfo.szParam1 = szValue
                                            Case "[param2]"
                                                st_PayInfo.szParam2 = szValue
                                            Case "[param3]"
                                                st_PayInfo.szParam3 = szValue
                                            Case "[param4]"
                                                st_PayInfo.szParam4 = szValue
                                            Case "[param5]"
                                                st_PayInfo.szParam5 = szValue
                                            Case "[param6]"
                                                st_PayInfo.szParam6 = szValue
                                            Case "[param7]"
                                                st_PayInfo.szParam7 = szValue
                                            Case "[param8]"
                                                st_PayInfo.szParam8 = szValue
                                            Case "[param9]"
                                                st_PayInfo.szParam9 = szValue
                                            Case "[param10]"
                                                st_PayInfo.szParam10 = szValue
                                            Case "[param11]"
                                                st_PayInfo.szParam11 = szValue
                                            Case "[param12]"
                                                st_PayInfo.szParam12 = szValue
                                            Case "[param13]"
                                                st_PayInfo.szParam13 = szValue
                                            Case "[param14]"
                                                st_PayInfo.szParam14 = szValue
                                            Case "[param15]"
                                                st_PayInfo.szParam15 = szValue
                                            Case "[mid]"
                                                st_PayInfo.szHostMID = szValue
                                            Case "[tid]"
                                                st_PayInfo.szHostTID = szValue
                                            Case "[txnstatus]"
                                                'Added default value "-" if szValue is empty on 24 Aug 2009 to cater for AliPay empty
                                                '<trade_status> if AliPay did not receive the out_trade_no (GatewayTxnID)
                                                If ("" = szValue) Then
                                                    szValue = "-"
                                                End If
                                                st_PayInfo.szHostTxnStatus = szValue
                                            Case "[respcode]"
                                                'Added default value "-" if szValue is empty on 24 Aug 2009 to cater for AliPay empty
                                                '<trade_status> if AliPay did not receive the out_trade_no (GatewayTxnID)
                                                If ("" = szValue) Then
                                                    szValue = "-"
                                                End If
                                                st_PayInfo.szRespCode = szValue
                                            Case "[authcode]"
                                                st_PayInfo.szAuthCode = szValue
                                            Case "[respmesg]"
                                                st_PayInfo.szBankRespMsg = szValue
                                            Case "[date]"
                                                st_PayInfo.szHostDate = szValue
                                            Case "[time]"
                                                st_PayInfo.szHostTime = szValue
                                            Case "[cardpan]"
                                                st_PayInfo.szCardPAN = szValue

                                                If (st_PayInfo.szCardPAN.Length > 20) Then
                                                    st_PayInfo.szCardPAN = st_PayInfo.szCardPAN.Substring(0, 20)
                                                End If

                                                If st_PayInfo.szCardPAN.Length > 10 Then
                                                    st_PayInfo.szMaskedCardPAN = st_PayInfo.szCardPAN.Substring(0, 6) + "".PadRight(stPayInfo.szCardPAN.Length - 6 - 4, "X") + stPayInfo.szCardPAN.Substring(stPayInfo.szCardPAN.Length - 4, 4)
                                                Else
                                                    st_PayInfo.szMaskedCardPAN = "".PadRight(st_PayInfo.szCardPAN.Length, "X")
                                                End If
                                            Case "[sessionid]"
                                                st_PayInfo.szMerchantSessionID = szValue            'Modified 16 Jul 2014, SessionID to MerchantSessionID
                                            Case "[hashvalue]"
                                                st_HostInfo.szHashValue = szValue.Replace(" ", "+") 'Added replacement on 3 Apr 2010

                                                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                                " HashValue (" + szValue + ")")
                                            Case "[reserved]"
                                                st_HostInfo.szReserved = szValue
                                            Case Else
                                                'Unsupported value tag
                                                st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                                                st_PayInfo.szErrDesc = "> Unsupported gateway field value tag(" + szConfig(iLoop) + ") in Config Template for " + stPayInfo.szIssuingBank

                                                GoTo CleanUp
                                        End Select

                                        'Sets szValue to empty so that the next Gateway variable will take empty szValue
                                        'if the Field Name defined in message template does not match any Field Name of
                                        'reply message or else the next Gateway variable will take the last szValue which
                                        'is not empty and belongs to the previous Gateway variable.
                                        szValue = ""
                                    End If
                                Next
                            End If  'If ("[3des]" = aszValue(0).Trim().ToLower()) Then
                        End If  'If ("*" = Left(szFormat, 1)) Then

                        If (szFormat <> "") Then
                            If ("#" = Left(szFormat, 1)) Then
                                szValue = szFormat.Replace("#", "")
                                aszValue = Split(szValue, "!")

                                szValue = ""
                                szHashValue = ""
                                szHashMethod = ""
                                ' Hash Method
                                If ("[hashmethod]" = aszValue(0).Trim().ToLower()) Then
                                    szHashMethod = st_HostInfo.szHashMethod
                                ElseIf ("[=" = Left(aszValue(0).Trim(), 2)) Then    'Added, 22 Oct 2013, to support different response hash method from request hash method, CashU
                                    szHashMethod = aszValue(0).Trim().Replace("[=", "").Replace("]", "")
                                Else
                                    szHashMethod = Server.UrlDecode(HttpContext.Current.Request(aszValue(0).Trim()))
                                End If

                                For iCounter = 1 To aszValue.GetUpperBound(0)
                                    If (0 <> (iCounter Mod 2)) Then ' Field
                                        'Added on 16 June 2009, to get MerchantID, CurrencyCode, HostID in order to further bGetTerminal to get ReturnKey
                                        If (("[returnkey]" = aszValue(iCounter).Trim().ToLower()) Or ("[returnkeyhex]" = aszValue(iCounter).Trim().ToLower())) Then
                                            If ("" <> st_PayInfo.szTxnID) Then
                                                'NOTE: The following config template is NOT YET SUPPORTED!!!!!!
                                                'GatewayTxnID is not encrypted in response, only certain fields are encrypted using 3DES, e.g. ConfNum and Amount
                                                '*** Must put GatewayTxnID before/on the left of other encrypted fields ***
                                                'e.g. BankRef1|[GatewayTxnID]|ConfNum|[Reserved]{*[3DES]!!![BankRefNo]*}|Amount|[Reserved]{*[3DES]!!![TxnAmount]*}
                                                'bGetReqTxn() will be called again in bVerifyResTxn()
                                                stPayInfo.szTxnID = st_PayInfo.szTxnID

                                                'Modified 25 Nov 2013, use objCommon instead of objTxnProc, to cater for different DB connection by Payment Method
                                                If (True <> objCommon.bGetReqTxn(stPayInfo, st_HostInfo)) Then
                                                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                                stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bParseQueryString() " + stPayInfo.szErrDesc)

                                                    Return False
                                                End If
                                            Else
                                                'For bank response which the whole content is encrypted, no GatewayTxnID is available like the above,
                                                'DDGW will need to retrieve the respective merchant's SecretKey to decrypt the payment response.
                                                'Since the whole content is encrypted, GW has no way to identify to which merchant that the response belongs and therefore
                                                'the respective MerchantID identifying merchant, IssuingBank and CurrencyCode need to be
                                                'defined in each response_[IssuingBank]_[MerchantID].aspx and pass to this bGetData().
                                                '0|[Reserved]{*[3DES]!&!BillRef1![GatewayTxnID]!Amount![TxnAmount]*}
                                                stPayInfo.szMerchantID = st_PayInfo.szMerchantID
                                                stPayInfo.szCurrencyCode = st_PayInfo.szCurrencyCode
                                            End If

                                            'Added on 16 June 2009
                                            'Get Terminal and Channel information - MID, TID, SendKey, ReturnKey, SecretKey, InitVector, PayeeCode, PayURL, QueryURL, CfgFile, ReturnURL
                                            'Modified 25 Nov 2013, use objCommon instead of objTxnProc, to cater for different DB connection by Payment Method
                                            If True <> objCommon.bGetTerminal(st_HostInfo, stPayInfo) Then
                                                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                                stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bParseQueryString() " + stPayInfo.szErrDesc)
                                                Return False
                                            End If
                                        End If

                                        Select Case aszValue(iCounter).Trim().ToLower()
                                            Case "[returnkey]"
                                                'Modified on 25 May 2009
                                                'NOTE: Use szDecrypt3DES instead of szDecrypt3DEStoHex because original encrypted value(Hash Key) is not in hexadecimal value
                                                'szValue = st_HostInfo.szReturnKey
                                                szValue = objHash.szDecrypt3DES(st_HostInfo.szReturnKey, Common.C_3DESAPPKEY, Common.C_3DESAPPVECTOR)
                                                st_HostInfo.szReturnKey = szValue

                                            Case "[returnkeyhex]"
                                                'Added on 3 Jun 2009
                                                'NOTE: Use szDecrypt3DEStoHex instead of szDecrypt3DES because original encrypted value(Hash Key) is in hexadecimal value
                                                szValue = objHash.szDecrypt3DEStoHex(st_HostInfo.szReturnKey, Common.C_3DESAPPKEY, Common.C_3DESAPPVECTOR)
                                                st_HostInfo.szReturnKey = szValue
                                            Case Else
                                                '07 Sept 2009 - Removed Trim to allow spaces in field's value in order to avoid hash value mismatched
                                                szValue = Server.UrlDecode(HttpContext.Current.Request(aszValue(iCounter)))
                                        End Select

                                        szHashValue = szHashValue + szValue
                                    Else
                                        ' Delimiter
                                        'Commented the following on 6 Apr 2010
                                        'szHashValue = szHashValue + aszValue(iCounter).Trim().Replace("[", "").Replace("]", "")

                                        'Added szDelimiterEx and Replace "PIPE" by "|" on 6 Apr 2010
                                        'Added checking of Instr ("PIPE")
                                        If (InStr(aszValue(iCounter).Trim().ToUpper(), "PIPE") > 0) Then
                                            szDelimiterEx = aszValue(iCounter).Trim().ToUpper().Replace("[", "").Replace("]", "").Replace("PIPE", "|")
                                        Else
                                            szDelimiterEx = aszValue(iCounter).Trim().Replace("[", "").Replace("]", "")
                                        End If

                                        szHashValue = szHashValue + szDelimiterEx
                                    End If
                                Next

                                'Added on 4 Nov 2009 Public and Private Key file path assignment from st_PayInfo to stPayInfo
                                stPayInfo.szDecryptKeyPath = st_PayInfo.szDecryptKeyPath
                                stPayInfo.szEncryptKeyPath = st_PayInfo.szEncryptKeyPath

                                szHashValue = objHash.szGetHash(szHashValue, szHashMethod, stPayInfo, st_HostInfo) 'Added on 15 Dec 2010 , add 'stHostInfo' for uobThai to get secret key for encryption

                                'Verify response Hash Value with Hash Value calculated above by Gateway
                                'Added case-insensitive hash value comparison, 12 May 2009
                                If (szHashValue.ToLower() <> st_HostInfo.szHashValue.ToLower()) Then
                                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                    " > bGetData() Response Hash Value mismatched - Host(" + st_HostInfo.szHashValue + ") Gateway(" + szHashValue + ") Hash Method(" + szHashMethod + ")")
                                    GoTo CleanUp
                                End If

                            ElseIf ("[=" = Left(szFormat, 2)) Then
                                If (szValue <> szFormat.Replace("[=", "").Replace("]", "")) Then
                                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                    " > bGetData() Response Value mismatched with defined value - Host(" + szValue + ") Defined(" + szFormat.Replace("[=", "").Replace("]", "") + ")")
                                    GoTo CleanUp
                                End If

                            ElseIf ("%" = Left(szFormat, 1)) Then       'Added  25 Aug 2015. PBB

                                Dim iNum As Integer
                                Dim aszFieldSubStr() As String
                                Dim szTemp As String

                                If ("%*" = Left(szFormat, 2)) Then
                                    If ("" = st_HostInfo.szSecretKey) Then 'If not Host based then only run bGetTerminal to get the secret key 
                                        If True <> objCommon.bGetTerminal(st_HostInfo, stPayInfo) Then
                                            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                            stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bGetData() " + stPayInfo.szErrDesc)
                                            Return False
                                        End If
                                    End If

                                    szTemp = "decrypt^" + st_HostInfo.szReserved
                                    'Decrypted string szTemp is fixed length format
                                    szTemp = objHash.szGetHash(szTemp, st_HostInfo.szHashMethod, stPayInfo, st_HostInfo)
                                Else
                                    szTemp = st_HostInfo.szReserved
                                End If

                                '%Mid@9@10!OrderNo@21@6!TxnDate@27@8!TxnTime@35@6!txnamount_02@64@12!RespCode@76@2!CustRef1@84@19!Tid@189@8!AutCode@109@16%
                                szFormat = szFormat.Replace("%", "")
                                aszValue = Split(szFormat, "!")

                                'For each response message field
                                For iNum = 0 To aszValue.GetUpperBound(0)
                                    aszFieldSubStr = Split(aszValue(iNum), "@")
                                    szValue = szTemp.Substring(aszFieldSubStr(1), aszFieldSubStr(2))

                                    Select Case aszFieldSubStr(0).ToLower()
                                        Case "gatewaytxnid"
                                            st_PayInfo.szTxnID = szValue    'Modified  19 Nov 2015, use st_ instead of st, for all of the following cases
                                        Case "txnamount_02"
                                            If ("" <> szValue) Then
                                                st_PayInfo.szTxnAmount = CStr(Convert.ToDecimal(Left(szValue, szValue.Length - 2) + "." + Right(szValue, 2)))
                                            End If
                                        Case "respcode"
                                            st_PayInfo.szRespCode = szValue
                                        Case "respcode_cs"                      'Added, 25 Nov 2016. Case Sensitive for PBB to retrieve Query response code. When OTP cancelled, resp will have no hashing and trigger querying host.
                                            st_PayInfo.szRespCode = szValue
                                            st_PayInfo.bRespCodeCS = True
                                        Case "authcode"
                                            st_PayInfo.szAuthCode = szValue
                                        Case "mid"
                                            st_PayInfo.szHostMID = szValue
                                        Case "orderno"
                                            st_PayInfo.szMerchantOrdID = szValue
                                        Case "txndate"
                                            st_PayInfo.szHostDate = szValue
                                        Case "txntime"
                                            st_PayInfo.szHostTime = szValue
                                        Case "tid"
                                            st_PayInfo.szHostTID = szValue
                                        Case "eci"
                                            st_PayInfo.szECI = szValue
                                        Case Else
                                            'Unsupported value tag
                                            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                                            st_PayInfo.szErrDesc = " > bGetData() Unsupported field tag(" + aszFieldSubStr(0) + ") in " + stPayInfo.szIssuingBank + " message template"
                                            GoTo CleanUp
                                    End Select

                                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                    " > Field(" + aszFieldSubStr(0) + ") Value(" + szValue + ")")
                                Next iNum
                            Else
                                objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                " > bGetData() Unsupported verification format(" + szFormat + ")")
                                GoTo CleanUp
                            End If  'If ("#" = Left(szFormat, 1)) Then
                        End If  'If (szFormat <> "") Then
                    End If  'If (szFormat <> "") Then
                End If
            Next iLoop

            bReturn = True

        Catch ex As Exception
            st_PayInfo.szErrDesc = "Fail to parse response data. Exceptions: " + ex.Message()
            st_PayInfo.szTxnMsg = "System Error: Fail to parse response data."

            bReturn = False
        End Try

CleanUp:
        If Not objStreamReader Is Nothing Then objStreamReader = Nothing

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		Response
    ' Function Name:	szGetTagValue
    ' Function Type:	String. Return value
    ' Parameter:        sz_SOAPMesg [in, string]    -  string to be searched from
    '                   sz_Tag2Search [in, string]  - string to be searched
    ' Description:		This function search tag value from SOAP message
    ' History:			27 Feb 2009
    '                   30 Oct 2013,  commented the whole function, replaced with new one
    '********************************************************************************************
    'Public Function szGetTagValue(ByVal sz_SOAPMesg As String, ByVal sz_Tag2Search As String, ByVal st_PayInfo As Common.STPayInfo) As String
    '    Dim iStart As Integer = 0
    '    Dim iEnd As Integer = 0
    '    Dim szFilterString As String = ""
    '    Dim szResult As String = ""
    '    Dim szTemp As String = ""   'Added on 26 Oct 2009

    '    Try
    '        'Added szTemp on 26 Oct 2009 to store the lower case of sz_SOAPMesg because IndexOf is case sensitive,
    '        'cannot use IndexOfAny because the index returned is one more than the index returned by IndexOf.
    '        'IndexOfAny will return the index of the 1st occurence in this instance of any character in a specified array of Unicode characters.
    '        szTemp = sz_SOAPMesg.ToLower()
    '        iStart = szTemp.IndexOf(sz_Tag2Search.ToLower()) 'zero based - first occurence of sz_Tag2Search
    '        If (iStart < 0) Then    'Not found
    '            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
    '                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
    '                            st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > " + sz_Tag2Search + " not found from SOAP")

    '            szResult = ""
    '            Return szResult
    '        End If

    '        szFilterString = sz_SOAPMesg.Substring(iStart + sz_Tag2Search.Length) 'from end of sz_Tag2Search up to end of sz_SOAPMesg

    '        iStart = szFilterString.IndexOf(">") + 1            'Start offset of the searched tag's value
    '        iEnd = szFilterString.IndexOf("<") - 1              'End offset of the searched tag's value
    '        szResult = szFilterString.Substring(iStart, iEnd)   'Searched tag's value

    '        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
    '                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
    '                        st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Found " + sz_Tag2Search + " value(" + szResult + ") from SOAP")

    '    Catch ex As Exception
    '        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
    '                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
    '                        st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Exception: " + ex.Message + " - unable to complete searching " + sz_Tag2Search + " from SOAP")
    '    End Try

    '    Return szResult
    'End Function

    '********************************************************************************************
    ' Class Name:		Response
    ' Function Name:	szGetTagValue
    ' Function Type:	String. Return value
    ' Parameter:        sz_SOAPMesg [in, string]    -  string to be searched from
    '                   sz_Tag2Search [in, string]  - string to be searched
    ' Description:		This function search tag value from SOAP message
    ' History:			30 Oct 2013
    '********************************************************************************************
    Public Function szGetTagValue(ByVal sz_SOAPMesg As String, ByVal sz_Tag2Search As String, ByVal st_PayInfo As Common.STPayInfo) As String
        Dim szResult As String = ""
        'Dim szTemp As String = ""   'Added  18 Nov 2013
        Dim iStart As Integer = 0   'Added  18 Nov 2013

        Try
            'szTemp = sz_SOAPMesg    'Added  18 Nov 2013, stores the original SOAP message containing original case

            'Using reader As XmlReader = XmlReader.Create(New StringReader(sz_SOAPMesg.ToLower()))
            '    If (True = reader.ReadToFollowing(sz_Tag2Search.ToLower())) Then
            '        szResult = reader.ReadElementContentAsString()
            '        iStart = InStr(1, szTemp, szResult, CompareMethod.Text)     'Added  18 Nov 2013, CompareMethod.Text is case NOT sensitive
            '        szResult = szTemp.Substring(iStart - 1, szResult.Length)    'Added  18 Nov 2013, 
            '    Else
            '        szResult = ""
            '    End If
            'End Using

            'Modified  14 Apr 2016.
            Using reader As XmlReader = XmlReader.Create(New StringReader(sz_SOAPMesg))     'XMLReader read the original message
                While reader.Read()
                    If sz_Tag2Search.ToLower() = reader.Name.ToLower() Then     'reader.Name - current node. Change ToLower() for comparision
                        szResult = reader.ReadElementContentAsString()          'Get result value from reader
                        Exit While                                              'Exit while when found
                    End If
                End While
            End Using

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > " + sz_Tag2Search + " value(" + szResult + ") from SOAP")

        Catch ex As Exception
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Exception: " + ex.Message + " - unable to complete searching " + sz_Tag2Search + " from SOAP")
        End Try

        Return szResult
    End Function

    '********************************************************************************************
    ' Class Name:		Response
    ' Function Name:	szGetHTMLTagValue
    ' Function Type:	String. Return value
    ' Parameter:        sz_HTML [in, string]    -  string to be searched from
    '                   sz_Tag2Search [in, string]  - string to be searched
    ' Description:		This function search HTML tag value from HTML page's FORM
    ' History:			13 Jun 2010
    '********************************************************************************************
    Public Function szGetHTMLTagValue(ByVal sz_HTML As String, ByVal sz_Tag2Search As String, ByVal st_PayInfo As Common.STPayInfo) As String
        Dim iStart As Integer = 0
        Dim iEnd As Integer = 0
        Dim szFilterString As String = ""
        Dim szResult As String = ""
        Dim szTemp As String = ""

        Try
            'szTemp to store the lower case of sz_HTML because IndexOf is case sensitive,
            'cannot use IndexOfAny because the index returned is one more than the index returned by IndexOf.
            'IndexOfAny will return the index of the 1st occurence in this instance of any character in a specified array of Unicode characters.
            'Example:
            '<form id="frmSubmit" action="https://www.airasia.com/billweb/rhbbank.php" method="POST" target="_self">
            '    <input type="hidden" name="MerchantID" value="5142">
            '    <input type="hidden" name="OrderNo" value="PGA07A7MMHT000000424">
            '    <input type="hidden" name="AddRefNo" value="">
            '    <input type="hidden" name="Amount" value="112.0000">
            '    <input type="hidden" name="Status" value="Unpaid">
            '    <input type="hidden" name="ReturnCode" value="1">
            '    <input type="hidden" name="DateTime" value="7/15/2010 3:16:11 PM">
            '    <input type="hidden" name="HashCount" value="78661871048.726424">
            '</form>

            szTemp = sz_HTML.ToLower()
            iStart = szTemp.IndexOf(sz_Tag2Search.ToLower()) 'zero based - first occurence of sz_Tag2Search

            If (iStart < 0) Then    'Not found
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > " + sz_Tag2Search + " not found from HTML page")

                szResult = ""
                Return szResult
            End If

            szFilterString = sz_HTML.Substring(iStart + sz_Tag2Search.Length) 'from end of sz_Tag2Search up to end of sz_HTML
            iStart = szFilterString.IndexOf("=") + 1            'Start offset of the searched tag's value
            iEnd = szFilterString.IndexOf(">") - 1              'End offset of the searched tag's value

            'Added on 16 Feb 2011, to fix parsing problem for HLB.
            'Although (iEnd = iEnd - iStart) was not added previously, so happened worked for RHB query resp.
            'HLB:<td>053999<input type="hidden" name="responseCode" value="053999"></td>
            'HLB:B4 adding the below statement, after parsing will get 053999</td>
            'Syntax of Substring: iStart:Start Index (starts with 0, IndexOf also starts with 0), iEnd:Retrieve how many from source string
            iEnd = iEnd - iStart 'This is to calculate length of the value which include the double quotes (") in front of the value
            szResult = szFilterString.Substring(iStart, iEnd).Replace("""", "").Replace("'", "").Replace(">", "").Replace(vbCrLf, "").Trim() 'Searched tag's value

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Found " + sz_Tag2Search + " value(" + szResult + ") from HTML page")

        Catch ex As Exception
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Exception: " + ex.Message + " - unable to complete searching " + sz_Tag2Search + " from HTML page")
        End Try

        Return szResult
    End Function

    '********************************************************************************************
    ' Class Name:		Response
    ' Function Name:	iGetQueryString
    ' Function Type:	Integer
    ' Parameter:		sz_QueryString [out, string]    -   Query string to be generated
    '                   sz_ConfigFilePath [in, string]  -   Path of config file to customize HTML content's fields
    '                   sz_ReqOrRes [in, string]        -   Request or Response indicator
    ' Description:		Generates query string based on host configuration template
    ' History:			26 Feb 2009
    '                   Modified 18 Dec 2013, Private to Public, for FPX response_fpx
    '                   Modified  18 Oct 2016. MasterPass. sz_LogParams
    '********************************************************************************************
    Public Function iGetQueryString(ByRef sz_QueryString As String, ByVal sz_ConfigFilePath As String, ByVal sz_ReqOrRes As String, ByVal sz_TxnType As String, ByRef st_PayInfo As Common.STPayInfo, ByVal st_HostInfo As Common.STHost, Optional ByRef sz_LogParams As String = "") As Integer
        Dim iReturn As Integer = -1
        Dim iLoop As Integer
        Dim iCounter As Integer
        Dim objStreamReader As StreamReader
        Dim szConfigFile As String = ""
        Dim szLine As String = ""
        Dim szConfig() As String
        Dim szParams As String = ""
        Dim szDelimiter As String = ""
        Dim szValue As String = ""
        Dim szHashValue As String = ""
        Dim aszValue() As String
        Dim bFound As Boolean = False
        Dim iStart As Integer
        Dim iEnd As Integer
        Dim bDefineFieldName As Boolean = False
        Dim stPayInfo As Common.STPayInfo 'Added on 16 June 2009
        Dim szLogValue As String = ""       'Added  12 Oct 2016. MasterPass
        Dim szDelimiterEx As String = ""
        Dim szTxnAmount As String = ""          'Added, 11 Dec 2017, for FX
        Dim szCurrencyCode As String = ""       'Added, 11 Dec 2017, for FX

        'Txn Type|Request/Response?|Post Method|Field Name|Field Value|....
        'PAY/QUERY|REQ|GET|billAccountNo|[OrderNumber]|billReferenceNo|[GatewayTxnID]|billReferenceNo2|[]|billReferenceNo3|[]|billReferenceNo4|[]|amount|[TxnAmount_2Decimals]|payeeResponseURL|[GatewayReturnURL]|
        Try
            sz_QueryString = ""

            'Added, 11 Dec 2017, for FX
            If (st_PayInfo.szFXTxnAmount <> "" And st_PayInfo.szFXTxnAmount <> "0.00") Then
                szTxnAmount = st_PayInfo.szFXTxnAmount
            Else
                szTxnAmount = st_PayInfo.szTxnAmount
            End If

            'Added, 11 Dec 2017, for FX
            If (Trim(st_PayInfo.szFXCurrencyCode) <> "") Then
                szCurrencyCode = st_PayInfo.szFXCurrencyCode
            Else
                szCurrencyCode = st_PayInfo.szCurrencyCode
            End If

            'Added iReqRes on 23 Aug 2009
            stPayInfo.iReqRes = st_PayInfo.iReqRes

            'Added, 22 Jun 2015
            stPayInfo.szPymtMethod = st_PayInfo.szPymtMethod

            szConfigFile = Server.MapPath(sz_ConfigFilePath)
            objStreamReader = System.IO.File.OpenText(szConfigFile)
            Do
                szLine = objStreamReader.ReadLine()

                'Added, 10 Sept 2013
                If ("" = szLine) Then
                    Exit Do
                End If

                szConfig = Split(szLine, "|")
                If ((szConfig(0).Trim().ToUpper() = sz_TxnType.ToUpper()) And (szConfig(1).Trim().ToUpper() = sz_ReqOrRes.ToUpper())) Then
                    bFound = True
                    Exit Do
                End If
            Loop Until (szLine Is Nothing)

            objStreamReader.Close()

            If (False = bFound) Then
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + "> TxnType(" + sz_ReqOrRes + "_" + sz_TxnType + ") not found in Config Template for " + st_PayInfo.szIssuingBank)
                GoTo CleanUp
            End If

            'Parse text line of host request message configuration and form param string
            'Txn Type|Request/Response?|Post Method|Field Name|Field Value|....
            'PAY|REQ|GET|billAccountNo|[OrderNumber]|billReferenceNo|[GatewayTxnID]|billReferenceNo2|[]|billReferenceNo3|[]|billReferenceNo4|[]|amount|[TxnAmount_2Decimals]|payeeResponseURL|[GatewayReturnURL]|
            'billAccountNo=[OrderNumber]&billReferenceNo=[GatewayTxnID]....
            'PAY|REQ|GET|[sendstring=]|[=Login]|[$]|[=1DJ]|[$]|[=1]|[$]|[txnamount]|[$]|[=1]|[$]|[GatewayTxnID]|[$]|[]|[$]|[]|[$]|[GatewayReturnURL]
            st_PayInfo.szPostMethod = szConfig(2).Trim().ToUpper()
            szParams = ""
            sz_LogParams = "" 'Added  12 Oct 2016. MasterPass
            bDefineFieldName = False
            For iLoop = 3 To szConfig.GetUpperBound(0)
                If (0 <> (iLoop Mod 2)) Then
                    'Get and populate Field Name into param string
                    'Modified on 20 Jul 2009, "GET" = st_PayInfo.szPostMethod TO Left(st_PayInfo.szPostMethod.ToUpper(), 3)
                    If (("GET" = Left(st_PayInfo.szPostMethod.ToUpper(), 3)) Or ("WS" = st_PayInfo.szPostMethod) Or ("TCP" = Left(st_PayInfo.szPostMethod.ToUpper(), 3)) Or ("JSON" = st_PayInfo.szPostMethod)) Then 'Added  9 Jul 2012 for FPX
                        'Determine whether it is standard or self-defined field name
                        iStart = InStr(szConfig(iLoop), "[") 'Position starts with 1
                        iEnd = InStr(szConfig(iLoop), "]")
                        If (iStart > 0) Then
                            'Self-defined field name
                            If (iEnd <= 0) Then
                                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + "> Invalid field name tag(" + szConfig(iLoop) + ") in Config Template for " + st_PayInfo.szIssuingBank)

                                GoTo CleanUp
                            End If

                            bDefineFieldName = True

                            'Added PIPE  10 Jul 2012, for FPX, define field name as "|"
                            If ("[PIPE]" = szConfig(iLoop).ToUpper()) Then
                                szConfig(iLoop) = "[|]"
                            End If

                            szConfig(iLoop) = szConfig(iLoop).Substring(iStart, szConfig(iLoop).Length - 2) 'Substring is zero-based
                        Else
                            'Standard query string field names, e.g. field1=value1&field2=value2&field3=value3
                            If (3 = iLoop) Then     'First Field Name should be without "&" in front of the field name
                                szDelimiter = ""
                            Else
                                'Added on 20 Jul 2009
                                If ("GET" = Left(st_PayInfo.szPostMethod.ToUpper(), 3)) Then
                                    If (st_PayInfo.szPostMethod.Length() > 3) Then
                                        szDelimiter = Mid(st_PayInfo.szPostMethod, 4, 1)
                                    Else
                                        szDelimiter = "&"
                                    End If
                                Else
                                    szDelimiter = "&"   'Subsequent field name should have "&" in front of the field name
                                End If
                            End If
                        End If

                        If (bDefineFieldName) Then
                            szParams = szParams + szConfig(iLoop).Trim()
                        Else
                            szParams = szParams + szDelimiter + szConfig(iLoop).Trim() + "="
                        End If

                    ElseIf ("POST" = st_PayInfo.szPostMethod.ToUpper() Or "POSTONLY" = st_PayInfo.szPostMethod.ToUpper()) Then
                        'Added POSTONLY on 26 Jul 2010
                        'Hidden field
                        '<INPUT type="hidden" name="MerchantID" value="[MerchantID]">
                        szDelimiter = "<INPUT type='hidden' name='"
                        szParams = szParams + szDelimiter + szConfig(iLoop).Trim() + "' value='"
                        'Added  18 Oct 2016. MasterPass
                        sz_LogParams = sz_LogParams + szDelimiter + szConfig(iLoop).Trim() + "' value='"
                    End If
                Else
                    'Verify Field Value tag
                    If ((InStr(szConfig(iLoop), "[") <= 0) Or (InStr(szConfig(iLoop), "]") <= 0)) And (InStr(szConfig(iLoop), "{") <= 0) Then
                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                        st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + "> Invalid field value tag(" + szConfig(iLoop) + ") in Config Template for " + st_PayInfo.szIssuingBank)
                        GoTo CleanUp
                    End If

                    'Populate Field Value into param string

                    'Added on 17 June 2009, to get MerchantID, CurrencyCode, HostID in order to further bGetTerminal to get SendKey, ReturnKey, SecretKey
                    Select Case szConfig(iLoop).Trim().ToLower()
                        'Added "[secretkey]", "[secretkeyhex]", "[sendkeypath]", "[returnkeypath]" on 5 June 2010
                        '-SPay, added "[hostpassword3hexl]"
                        Case "[secretkey]", "[secretkeyhex]", "[hostpassword1]", "[hostpassword2]", "[hostpassword3]", "[hostpassword1hex]", "[hostpassword2hex]", "[hostpassword3hex]", "[hostpassword3hexl]", "[sendkeypath]", "[returnkeypath]"
                            If ("" <> st_PayInfo.szTxnID) Then
                                'NOTE: The following config template is NOT YET SUPPORTED!!!!!!
                                'GatewayTxnID is not encrypted in response, only certain fields are encrypted using 3DES, e.g. ConfNum and Amount
                                '*** Must put GatewayTxnID before/on the left of other encrypted fields ***
                                'e.g. BankRef1|[GatewayTxnID]|ConfNum|[Reserved]{*[3DES]!!![BankRefNo]*}|Amount|[Reserved]{*[3DES]!!![TxnAmount]*}
                                'bGetReqTxn() will be called again in bVerifyResTxn()
                                stPayInfo.szTxnID = st_PayInfo.szTxnID

                                'Modified 25 Nov 2013, use objCommon instead of objTxnProc, to cater for different DB connection by Payment Method
                                If (True <> objCommon.bGetReqTxn(stPayInfo, st_HostInfo)) Then
                                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > iGetQueryString() " + stPayInfo.szErrDesc)

                                    Return False
                                End If
                            Else
                                'For bank response which the whole content is encrypted, no GatewayTxnID is available like the above,
                                'DDGW will need to retrieve the respective merchant's SecretKey to decrypt the payment response.
                                'Since the whole content is encrypted, GW has no way to identify to which merchant that the response belongs and therefore
                                'the respective MerchantID identifying merchant, IssuingBank and CurrencyCode need to be
                                'defined in each response_[IssuingBank]_[MerchantID].aspx and pass to this bGetData().
                                '0|[Reserved]{*[3DES]!&!BillRef1![GatewayTxnID]!Amount![TxnAmount]*}
                                stPayInfo.szMerchantID = st_PayInfo.szMerchantID
                                stPayInfo.szCurrencyCode = szCurrencyCode       'Modified 11 Dec 2017, removed st_PayInfo
                            End If

                            'Added on 17 June 2009
                            'Get Terminal and Channel information - MID, TID, SendKey, ReturnKey, SecretKey, InitVector, PayeeCode, PayURL, QueryURL, CfgFile, ReturnURL
                            'Modified 25 Nov 2013, use objCommon instead of objTxnProc, to cater for different DB connection by Payment Method
                            If True <> objCommon.bGetTerminal(st_HostInfo, stPayInfo) Then
                                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > iGetQueryString() " + stPayInfo.szErrDesc)
                                Return False
                            End If
                    End Select

                    'CIMBClicks: [=AIRASIA]|[OrderNumber]|[GatewayTxnID]|[]|[TxnAmount]|[TxnAmount_(2)]|[GatewayReturnURL]|
                    '[Param1]|[Param2]|[Param3]|[Param4]|[Param5]|[CurrencyCode]|[OrderDesc]|[CardPAN]|[SessionID]| [MID]|[TID]|
                    'Not yet supported: [CurrencyCode_9]|[DATETIME_yy-MM-dd hh:mm:ss.fff]|[HashValue_Field1|Field2|Field3]
                    szLogValue = "" 'Added  12 Oct 2016. MasterPass
                    Select Case szConfig(iLoop).Trim().ToLower()
                        Case "[]"               'Empty value
                            szValue = ""
                        Case "[merchantid]"
                            szValue = st_PayInfo.szMerchantID
                        Case "[merchanttxnid]"
                            szValue = st_PayInfo.szMerchantTxnID
                        Case "[ordernumber]"
                            szValue = st_PayInfo.szMerchantOrdID
                        Case "[gatewaytxnid]"
                            szValue = st_PayInfo.szTxnID
                        Case "[txnamount]"      'Take the request's txn amount which is in 2 decimals. Modified 11 Dec 2017, removed st_PayInfo
                            szValue = szTxnAmount
                        Case "[txnamount_2]"    'Amount in cents whereby the last two digits are decimal points. Modified 11 Dec 2017, removed st_PayInfo
                            szValue = szTxnAmount.Replace(".", "")
                        Case "[txnamtexpn_2]"    'Amount without decimal point but the last two digits are Zero     'Added on 6  Jan 2016, MIGS exponent currency  -Masterpass-GP 14 Mar 2017. Modified 11 Dec 2017, removed st_PayInfo
                            If ("JPY" = szCurrencyCode Or "KRW" = szCurrencyCode Or "VND" = szCurrencyCode Or "IDR" = szCurrencyCode) Then
                                szValue = Left(szTxnAmount, szTxnAmount.Length() - 3)
                            Else
                                szValue = szTxnAmount.Replace(".", "")
                            End If
                        Case "[txnamount_02]"   'Added on 25 Oct 2009. 12 digits, leading zeroes, no decimal point, last 2 are decimal values. Modified 11 Dec 2017, removed st_PayInfo
                            szValue = szTxnAmount.Replace(".", "")
                            szValue = "".PadLeft(12 - szValue.Length(), "0") + szValue
                        Case "[gatewayreturnurl]"
                            'Added [languagecode] on 12 June 2009 to support guide_EN.htm, guide_*.htm for SCB's BackURL
                            szValue = Replace(st_HostInfo.szReturnURL.ToLower(), "[languagecode]", st_PayInfo.szLanguageCode)
                        Case "[gatewayreturnurl2]"  'Added on 22 Oct 2009
                            szValue = Replace(st_HostInfo.szReturnURL2.ToLower(), "[languagecode]", st_PayInfo.szLanguageCode)
                        Case "[gatewayreturnurlsvcid]"  'Added on 16 June 2009 to include MerchantID in Return URL
                            'e.g. https://ddgwsg.airasia.com/directdebit/response_pbb.aspx to
                            '     https://ddgwsg.airasia.com/directdebit/response_pbb_A08.aspx
                            szValue = Replace(st_HostInfo.szReturnURL.Trim().ToLower(), "[languagecode]", stPayInfo.szLanguageCode)
                            If (InStr(szValue, ".aspx") > 0) Then
                                szValue = szValue.Substring(0, szValue.Length() - 5) + "_" + stPayInfo.szMerchantID + ".aspx"
                            End If
                        Case "[gatewayreturnurl2svcid]"  'Added on 22 Oct 2009
                            szValue = Replace(st_HostInfo.szReturnURL2.Trim().ToLower(), "[languagecode]", stPayInfo.szLanguageCode)
                            If (InStr(szValue, ".aspx") > 0) Then
                                szValue = szValue.Substring(0, szValue.Length() - 5) + "_" + stPayInfo.szMerchantID + ".aspx"
                            End If
                        Case "[param1]"
                            szValue = st_PayInfo.szParam1
                        Case "[param2]"
                            szValue = st_PayInfo.szParam2
                            'Added  12 Oct 2016. MasterPass
                            If (szValue <> "") Then
                                szLogValue = szValue.Substring(0, 6) + "".PadRight(szValue.Length - 6 - 4, "X") + szValue.Substring(szValue.Length - 4, 4)
                            End If
                        Case "[param3]"
                            szValue = st_PayInfo.szParam3
                            'Added  12 Oct 2016. MasterPass
                            If (szValue <> "") Then
                                szLogValue = "".PadLeft(szValue.Length, "X")
                            End If
                        Case "[param4]"
                            szValue = st_PayInfo.szParam4
                        Case "[param5]"
                            szValue = st_PayInfo.szParam5
                        Case "[param6]"
                            szValue = st_PayInfo.szParam6
                        Case "[param7]"
                            szValue = st_PayInfo.szParam7
                        Case "[param8]"
                            szValue = st_PayInfo.szParam8
                        Case "[param9]"
                            szValue = st_PayInfo.szParam9
                        Case "[param10]"
                            szValue = st_PayInfo.szParam10
                        Case "[param11]"
                            szValue = st_PayInfo.szParam11
                        Case "[param12]"
                            szValue = st_PayInfo.szParam12
                        Case "[param13]"
                            szValue = st_PayInfo.szParam13
                        Case "[param14]"
                            szValue = st_PayInfo.szParam14
                        Case "[param15]"
                            szValue = st_PayInfo.szParam15
                        Case "[currencycode]"
                            szValue = szCurrencyCode        'Modified 11 Dec 2017, removed st_PayInfo
                        Case "[orderdesc]"
                            szValue = st_PayInfo.szMerchantOrdDesc
                        Case "[cardpan]"
                            szValue = st_PayInfo.szCardPAN
                            'Added  12 Oct 2016. MasterPass
                            If (szValue <> "") Then
                                szLogValue = szValue.Substring(0, 6) + "".PadRight(szValue.Length - 6 - 4, "X") + szValue.Substring(szValue.Length - 4, 4)
                            End If
                        Case "[cvv2]"       'Added  8 Nov 2016. MasterPass
                            szValue = st_PayInfo.szCVV2
                            If (szValue <> "") Then
                                szLogValue = "".PadLeft(st_PayInfo.szCVV2.Length, "X")
                            End If
                        Case "[sessionid]"
                            szValue = st_PayInfo.szMerchantSessionID    'Modified 16 Jul 2014, SessionID to MerchantSessionID
                        Case "[mid]"
                            szValue = st_HostInfo.szMID
                        Case "[tid]"
                            szValue = st_HostInfo.szTID
                        Case "[acquirerid]"                    'Added on 15 June 2009, to store e.g. Query UserID, Product ID which is different per merchant per bank
                            szValue = st_HostInfo.szAcquirerID
                        Case "[payeecode]"                     'Added on 15 June 2009, to store Payee Code which is different per merchant per bank
                            szValue = st_HostInfo.szPayeeCode
                        Case "[hostpassword1]"                  'Added on 17 June 2009, to retrieve value in Password field in Terminals table
                            szValue = objHash.szDecrypt3DES(st_HostInfo.szSendKey, Common.C_3DESAPPKEY, Common.C_3DESAPPVECTOR)
                        Case "[hostpassword2]"                  'Added on 17 June 2009, to retrieve value in ReturnKey field in Terminals table
                            szValue = objHash.szDecrypt3DES(st_HostInfo.szReturnKey, Common.C_3DESAPPKEY, Common.C_3DESAPPVECTOR)
                        Case "[hostpassword3]"                  'Added on 17 June 2009, to retrieve value in SecretKey field in Terminals table
                            szValue = objHash.szDecrypt3DES(st_HostInfo.szSecretKey, Common.C_3DESAPPKEY, Common.C_3DESAPPVECTOR)
                        Case "[hostpassword1hex]"               'Added on 17 June 2009, to retrieve hex-value in Password field in Terminals table
                            szValue = objHash.szDecrypt3DEStoHex(st_HostInfo.szSendKey, Common.C_3DESAPPKEY, Common.C_3DESAPPVECTOR)
                        Case "[hostpassword2hex]"               'Added on 17 June 2009, to retrieve hex-value in ReturnKey field in Terminals table
                            szValue = objHash.szDecrypt3DEStoHex(st_HostInfo.szReturnKey, Common.C_3DESAPPKEY, Common.C_3DESAPPVECTOR)
                        Case "[hostpassword3hex]"               'Added on 17 June 2009, to retrieve hex-value in SecretKey field in Terminals table
                            szValue = objHash.szDecrypt3DEStoHex(st_HostInfo.szSecretKey, Common.C_3DESAPPKEY, Common.C_3DESAPPVECTOR)
                        Case "[hostpassword3hexl]"               '-SPay, added to retrieve hex-value in SecretKey field in Terminals table in lowercase
                            szValue = objHash.szDecrypt3DEStoHex(st_HostInfo.szSecretKey, Common.C_3DESAPPKEY, Common.C_3DESAPPVECTOR).ToLower()
                        Case "[sendkeypath]"                    'Added on 5 June 2010
                            szValue = stPayInfo.szEncryptKeyPath
                        Case "[returnkeypath]"                  'Added on 5 June 2010
                            szValue = stPayInfo.szDecryptKeyPath
                        Case "[date_yyyymmddhhmmss]"            'Added on 1 Oct 2010
                            szValue = st_PayInfo.szTxnDateTime.Replace("-", "").Replace(":", "").Replace(" ", "")
                        Case "[date_yyyymmdd]"                  'Added on 1 Oct 2010
                            szValue = Left(st_PayInfo.szTxnDateTime.Replace("-", "").Replace(":", "").Replace(" ", ""), 8)
                        Case "[date_yyyy-mm-dd]"                  'Added on 1 Oct 2010
                            szValue = Left(st_PayInfo.szTxnDateTime, 10)
                        Case "[date_ddmmyyyy_hh:mm:ss]"         'Added  28 Nov 2013, Alliance Bank CC
                            'original date format is 2013-11-29 15:10:10
                            szValue = Convert.ToDateTime(st_PayInfo.szTxnDateTime).ToString("ddMMyyyy HH:mm:ss")
                        Case "[date_yyyymmddthh:mm:ssz]"         'Added  30 June 2014, GlobalPay Cybersource
                            'original date format is 2013-11-29 15:10:10
                            szValue = Convert.ToDateTime(st_PayInfo.szTxnDateTime).ToString("yyyy-MM-dd'T'HH:mm:ss'Z'")
                        Case "[utc_yyyymmddthh:mm:ssz]"         'Added  4 July 2014, GlobalPay Cybersource
                            szValue = DateTime.UtcNow.ToString("yyyy-MM-dd'T'HH:mm:ss'Z'")
                            st_PayInfo.szUTCDateTime = szValue   'assign value to stPayInfo.szUTCDateTime so that hashing use the same value
                        Case "[reserved]"                       'Added  9 Jul 2012, for FPX
                            szValue = st_HostInfo.szReserved
                        Case "[authcode]"                       'Added 12 Sep 2016, for Alipay
                            szValue = st_PayInfo.szAuthCode
                        Case "[cardholder]"                     'Added  10 Oct 2016, MasterPass.
                            szValue = st_PayInfo.szCardHolder
                        Case "[cardtype]"
                            szValue = st_PayInfo.szCardType      'Added  10 Oct 2016, MasterPass.

                            If ("" = szValue) Then
                                If (True = objTxnProc.bGetCardType(st_PayInfo)) Then
                                    szValue = st_PayInfo.szCardType
                                End If
                            Else
                                szValue = Left(st_PayInfo.szCardType, 1)
                            End If
                        Case "[walletid]"
                            szValue = st_PayInfo.szWalletID     'Added  10 Oct 2016, MasterPass.
                        Case "[merchantname]"
                            szValue = st_PayInfo.szMerchantName 'Added  27 Oct 2016, MasterPass.
                        Case "[languagecode]"
                            szValue = stPayInfo.szLanguageCode  'Added  6 Mar 2017, MasterPass-2.
                        Case "[uuid]"                           'Added  30 June 2014, GlobalPay Cybersource
                            szValue = Guid.NewGuid().ToString()
                            st_PayInfo.szUUID = szValue          'assign value to stPayInfo.szUUID, so that hashing use the same UUID value
                        Case "[custemail]"              'Added  16 Jun 2014. Firefly FFCIMB
                            szValue = st_PayInfo.szCustEmail
                        Case "[billaddr]"               'Added  16 Jun 2014. Firefly FFCIMB
                            szValue = st_PayInfo.szBillAddr
                            If ("" = szValue) Then
                                szValue = "1"
                                'If ("" = szValue) Then      'Added  16 Oct 2015, GlobaPay Cybersource (AVS)
                                '    szValue = "-"
                            End If

                            ' CYBS-Masterpass
                        Case "[billcity]"
                            szValue = st_PayInfo.szBillCity
                        Case "[billregion]"
                            szValue = st_PayInfo.szBillRegion
                        Case "[billpostal]"
                            szValue = st_PayInfo.szBillPostal


                        Case "[billcountry2]"            'Added  16 Jun 2014. Firefly FFCIMB
                            szValue = st_PayInfo.szBillCountry

                            If ("" = szValue) Then      'Added, 22 Jul 2014. GlobalPay CyberSource
                                szValue = st_PayInfo.szMerchantCountry
                            End If
                        Case "[unixtimestamp]" 'Added on 8 May by HS, EXPAY
                            szValue = DateDiff("s", "01/01/1970 00:00:00", stPayInfo.szTxnDateTime).ToString()
                        Case Else
                            'Check if it is a default value, e.g. [=AIRASIA]
                            If ("[=" = Left(szConfig(iLoop).Trim(), 2)) Then
                                szValue = Mid(szConfig(iLoop).Trim(), 3, szConfig(iLoop).Trim().Length - 3)
                            ElseIf ("{" = Left(szConfig(iLoop).Trim(), 1)) Then   ' "{" indicates format
                                If ("{LF" = Left(szConfig(iLoop).Trim().ToUpper(), 3)) Then         'Added  4 July 2012, for FPX request message which needs to have Line Feed '\n'
                                    szValue = szConfig(iLoop).Trim().Replace("{LF}", vbLf)
                                ElseIf ("{CR" = Left(szConfig(iLoop).Trim().ToUpper(), 3)) Then     'Added  4 July 2012. Additional to support Carriage Return '\r'
                                    szValue = szConfig(iLoop).Trim().Replace("{CR}", vbCr)
                                ElseIf ("{CRLF" = Left(szConfig(iLoop).Trim().ToUpper(), 5)) Then   'Added  4 July 2012. Additional to support for '\r\n'
                                    szValue = szConfig(iLoop).Trim().Replace("{CRLF}", vbCrLf)
                                ElseIf ("{IF@" = Left(szConfig(iLoop).Trim().ToUpper(), 4)) Then    'Added  27 Oct 2016. MasterPass
                                    '{IF@@EN@vi@VN@[LanguageCode]}
                                    'Moved from below to here.
                                    Dim szDefaultValue As String = ""

                                    szValue = szConfig(iLoop).Trim().Replace("{", "").Replace("}", "")
                                    aszValue = Split(szValue, "@")

                                    'Source string to be IF ELSE
                                    Select Case aszValue(aszValue.GetUpperBound(0)).ToLower()
                                        Case "[languagecode]"
                                            szValue = st_PayInfo.szLanguageCode

                                        Case "[cardtype]"   'Added  29 Oct 2013, for HSBC Credit
                                            szValue = st_PayInfo.szCardType

                                            'Added, 29 Oct 2013
                                            If ("" = szValue) Then
                                                If (True = objTxnProc.bGetCardType(st_PayInfo)) Then
                                                    szValue = st_PayInfo.szCardType
                                                End If
                                            Else
                                                szValue = Left(st_PayInfo.szCardType, 1) 'Added, 20 May 2016, for MyDin Card Type
                                            End If
                                    End Select

                                    'Look for any default value, skips the last element which is the field
                                    For iCounter = 1 To (aszValue.GetUpperBound(0) - 1)
                                        If (0 <> (iCounter Mod 2)) Then
                                            If ("" = aszValue(iCounter).ToLower()) Then
                                                szDefaultValue = aszValue(iCounter + 1)
                                                Exit For
                                            End If
                                        End If
                                    Next

                                    'Look for value mapping from Merchant's request to Host's request, skips the last element which is the field
                                    bFound = False
                                    For iCounter = 1 To (aszValue.GetUpperBound(0) - 1)
                                        If (0 <> (iCounter Mod 2)) Then
                                            If (szValue.ToLower() = aszValue(iCounter).ToLower()) Then
                                                szValue = aszValue(iCounter + 1)
                                                bFound = True
                                                Exit For
                                            End If
                                        End If
                                    Next

                                    If (False = bFound) Then
                                        szValue = szDefaultValue
                                    End If



                                ElseIf ("{SCHAR@" = Left(szConfig(iLoop).Trim().ToUpper(), 7)) Then
                                    'Added on 22 Sept 2011  for POLi - replace special character exist in authetication code
                                    'Example: [!#hostpassword1!]|{SCHAR@[hostpassword1]} Where this happen in SOAPMsg and need to split by '!'
                                    szValue = szConfig(iLoop).Trim().Replace("{SCHAR", "").Replace("}", "")
                                    aszValue = Split(szValue, "@")

                                    'Source string that needs to be replace
                                    Select Case aszValue(1).ToLower()
                                        Case "[hostpassword1]"
                                            szValue = objHash.szDecrypt3DES(st_HostInfo.szSendKey, Common.C_3DESAPPKEY, Common.C_3DESAPPVECTOR)
                                        Case "[hostpassword2]"
                                            szValue = objHash.szDecrypt3DES(st_HostInfo.szReturnKey, Common.C_3DESAPPKEY, Common.C_3DESAPPVECTOR)
                                        Case "[hostpassword3]"
                                            szValue = objHash.szDecrypt3DES(st_HostInfo.szSecretKey, Common.C_3DESAPPKEY, Common.C_3DESAPPVECTOR)
                                        Case "[orderdesc]"  'Combined  23 Apr 2014
                                            szValue = st_PayInfo.szMerchantOrdDesc
                                    End Select

                                    szValue = szValue.Replace("!", "[EXC]")
                                    'szValue = objCommon.szReplaceSpecialChar(szValue, "C") 'C for convert special character to specific tag

                                ElseIf ("{RIGHT@" = Left(szConfig(iLoop).Trim().ToUpper(), 7) Or "{LEFT@" = Left(szConfig(iLoop).Trim().ToUpper(), 6)) Then
                                    'Added, 6 Oct 2016, KTB
                                    'Example: {RIGHT@6@[GatewayTxnID]} ==> Retrieve the right 6 characters from GatewayTxnID
                                    'Format : (0):RIGHT;(1):<How many characters to take from right side>;(2):<Field to be retrieved>
                                    szValue = szConfig(iLoop).Trim().Replace("{", "").Replace("}", "")
                                    aszValue = Split(szValue, "@")

                                    If ("{RIGHT@" = Left(szConfig(iLoop).Trim().ToUpper(), 7)) Then
                                        aszValue(0) = "R"
                                    Else
                                        aszValue(0) = "L"
                                    End If

                                    'Source string to be retrieved from RIGHT/LEFT
                                    Select Case aszValue(2).ToLower()
                                        Case "[gatewaytxnid]"
                                            szValue = st_PayInfo.szTxnID
                                        Case "[param2]"
                                            szValue = st_PayInfo.szParam2
                                        Case "[param3]"
                                            szValue = st_PayInfo.szParam3
                                        Case "[param3_exp_date_masked]"
                                            szValue = st_PayInfo.szParam3
                                        Case "[param4]"
                                            szValue = st_PayInfo.szParam4
                                        Case "[param5]"
                                            szValue = st_PayInfo.szParam5
                                        Case "[param6]"
                                            szValue = st_PayInfo.szParam6
                                        Case "[param7]"
                                            szValue = st_PayInfo.szParam7
                                        Case "[param8]"
                                            szValue = st_PayInfo.szParam8
                                        Case "[param9]"
                                            szValue = st_PayInfo.szParam9
                                        Case "[param10]"
                                            szValue = st_PayInfo.szParam10
                                        Case "[param11]"
                                            szValue = st_PayInfo.szParam11
                                        Case "[param12]"
                                            szValue = st_PayInfo.szParam12
                                        Case "[param13]"
                                            szValue = st_PayInfo.szParam13
                                        Case "[param14]"
                                            szValue = st_PayInfo.szParam14
                                        Case "[param15]"
                                            szValue = st_PayInfo.szParam15
                                        Case "[custphone]"
                                            szValue = st_PayInfo.szCustPhone 'Added  21 Oct 2015, for GP which allows a maximum number of digits for bill phone
                                            ' Crossing out this - CYBS Masterpass
                                            'If (szValue <> "") Then
                                            '    szLogValue = "".PadRight(szValue.Length, "X")
                                            'End If

                                        Case "[merchantname]"
                                            szValue = st_PayInfo.szMerchantName
                                            ' CYBS Masterpass
                                            'If (szValue <> "") Then
                                            '    szLogValue = "".PadRight(szValue.Length, "X")
                                            'End If
                                    End Select

                                    If ("R" = aszValue(0).ToUpper()) Then
                                        szValue = Right(szValue, Integer.Parse(aszValue(1)))
                                        'szLogValue = "".PadLeft(szValue.Length, "X")
                                    Else
                                        szValue = Left(szValue, Integer.Parse(aszValue(1)))
                                        'szLogValue = "".PadLeft(szValue.Length, "X")
                                    End If

                                ElseIf ("{SR@" = Left(szConfig(iLoop).Trim().ToUpper(), 4)) Then        'Added  12 Oct 2016. MasterPass
                                    'Added on 8 Dec 2011  Currently use for Card Expired Date different format require by bank. This only can be use on restructure 1 string format to another string format
                                    'Example: {SR@R:2@L:2@M:2:4@[param3]}. Note: The sequance of R(right)/L(left)/M(Middle) will determine the output string format.
                                    '         [param3] = 022013 / [param3] = abcdefghijk
                                    '         CardExpireDate format require by bank is MMYY. In template as {SR@L:2@R:2@[param3]}. Result = 0213
                                    '         CardExpireDate format require by bank is YYMM. In template as {SR@R:2@L:2@[param3]}. Result = 1302
                                    '         Other string format require. In template set as {SR@R:1@M:3:3@[param3]}. Result = adef
                                    'Added  30 June 2014, GlobalPay Cybersource, add in FIX value which can be defined in template. 
                                    'Example: {SR@L:2@F:-@R:4@[param3]}. Result = 02-2013. Where 'F' indicates a fix value to be added.

                                    'need to check for stpayinfo. field and get the value b4 pass in to restructure
                                    Dim szField As String = ""
                                    szValue = szConfig(iLoop).Trim().Replace("{SR@", "").Replace("}", "")
                                    aszValue = Split(szValue, "@")

                                    'take the last array's value
                                    Select Case aszValue(aszValue.Length - 1).ToLower()
                                        'Add case for new fields when needed
                                        Case "[param3]"
                                            szValue = st_PayInfo.szParam3
                                            szLogValue = "".PadLeft(szValue.Length, "X")
                                        Case "[orderdesc]"                  'Added  6 May 2015. FPXD
                                            szValue = objCommon.szReplaceSpecialChar(st_PayInfo.szMerchantOrdDesc) 'Modified  26 May 2016, FPXD does not allow certain special characters
                                    End Select

                                    szValue = objCommon.szFormatStringRestructure(aszValue, szValue)

                                ElseIf ("{ASCII" = Left(szConfig(iLoop).Trim().ToUpper(), 6)) Then  'Added  28 Nov 2013, Alliance CC; Combined  28 Apr 2014
                                    'e.g. PX_MERCHANT_ID|{ASCII15[GatewayTxnID]}
                                    szValue = objHash.szASCIICounter(st_PayInfo.szTxnID, st_HostInfo.szMID)

                                    Dim sb As StringBuilder = New StringBuilder
                                    Dim szConfigField As String = szConfig(iLoop)
                                    For i = 0 To szConfigField.Length - 1
                                        If IsNumeric(szConfigField(i)) Then
                                            sb.Append(szConfigField(i))
                                        End If
                                    Next

                                    If "" <> sb.ToString Then
                                        If Convert.ToInt32(sb.ToString()) > szValue.Length Then
                                            szValue = szValue.PadLeft(Convert.ToInt32(sb.ToString()), "0")
                                        End If
                                    End If

                                ElseIf (aszValue IsNot Nothing AndAlso "{REVMASK" = Left(aszValue(iCounter).Trim().ToUpper(), 8)) Then  'Added  9 April 2015. eBPG Maybank-CC. Modified 28 Nov 2017, added checking of aszValue IsNot Nothing AndAlso to avoid exception
                                    'Reverse masking field value with 'x'
                                    'Example: {REVMASK@R:2@[cvv2]}. Note: The sequance of R(right)/L(left)/M(Middle) detemine the output string format.
                                    '         [cvv2] = 123 - In template as {REVMASK@R:2@[cvv2]}. Result = 12x
                                    '         [cvv2] = 789 - In template as {REVMASK@L:2@[cvv2]}. Result = x89
                                    '         [param3] = 333344445555666 - In template set as {REVMASK@M:6:6@[param3]}. Result = xxxxxx445555xxx
                                    Dim aszSplit() As String = Nothing

                                    szValue = aszValue(iCounter).Trim().Replace("{REVMASK@", "").Replace("}", "")
                                    aszSplit = Split(szValue, "@")

                                    'take the last array's value
                                    Select Case aszSplit(aszSplit.Length - 1).ToLower()
                                        'Add case for new fields when needed
                                        Case "[cvv2]"
                                            szValue = st_PayInfo.szCVV2
                                            szLogValue = "".PadLeft(szValue.Length, "X")
                                    End Select

                                    szValue = objCommon.szRevMask(aszSplit, szValue)

                                    'ElseIf ("{IF@" = Left(szConfig(iLoop).Trim().ToUpper(), 4)) Then    'Added  27 Oct 2016. MasterPass
                                    '    '{IF@@EN@vi@VN@[LanguageCode]}
                                    '    Dim szDefaultValue As String = ""

                                    '    szValue = szConfig(iLoop).Trim().Replace("{", "").Replace("}", "")
                                    '    aszValue = Split(szValue, "@")

                                    '    'Source string to be IF ELSE
                                    '    Select Case aszValue(aszValue.GetUpperBound(0)).ToLower()
                                    '        Case "[languagecode]"
                                    '            szValue = st_PayInfo.szLanguageCode

                                    '        Case "[cardtype]"   'Added  29 Oct 2013, for HSBC Credit
                                    '            szValue = st_PayInfo.szCardType

                                    '            'Added, 29 Oct 2013
                                    '            If ("" = szValue) Then
                                    '                If (True = objTxnProc.bGetCardType(st_PayInfo)) Then
                                    '                    szValue = st_PayInfo.szCardType
                                    '                End If
                                    '            Else
                                    '                szValue = Left(st_PayInfo.szCardType, 1) 'Added, 20 May 2016, for MyDin Card Type
                                    '            End If
                                    '    End Select

                                    '    'Look for any default value, skips the last element which is the field
                                    '    For iCounter = 1 To (aszValue.GetUpperBound(0) - 1)
                                    '        If (0 <> (iCounter Mod 2)) Then
                                    '            If ("" = aszValue(iCounter).ToLower()) Then
                                    '                szDefaultValue = aszValue(iCounter + 1)
                                    '                Exit For
                                    '            End If
                                    '        End If
                                    '    Next

                                    '    'Look for value mapping from Merchant's request to Host's request, skips the last element which is the field
                                    '    bFound = False
                                    '    For iCounter = 1 To (aszValue.GetUpperBound(0) - 1)
                                    '        If (0 <> (iCounter Mod 2)) Then
                                    '            If (szValue.ToLower() = aszValue(iCounter).ToLower()) Then
                                    '                szValue = aszValue(iCounter + 1)
                                    '                bFound = True
                                    '                Exit For
                                    '            End If
                                    '        End If
                                    '    Next

                                    '    If (False = bFound) Then
                                    '        szValue = szDefaultValue
                                    '    End If

                                Else
                                    'szValue = System.DateTime.Now.ToString(szConfig(iLoop).Trim().Replace("{", "").Replace("}", ""))
                                    szValue = Convert.ToDateTime(st_PayInfo.szTxnDateTime).ToString(szConfig(iLoop).Trim().Replace("{", "").Replace("}", ""))
                                End If

                            ElseIf ("#" = Left(szConfig(iLoop).Trim(), 1)) Then ' Supports Hashing
                                ' Example: #[SendKey]|[-]|[=PAYONLY]|[-]|[]|[-]|[MID]|[-]|[GatewayTxnID]|[-]|[CurrencyCode]|[-]|[TxnAmount]|[-]|[]|[-]|{dd-MMM-yyyy}|[-]|[]|[-]|[]|[-]|[]|[-]|[=615]|[-]|[]#
                                szValue = szConfig(iLoop).Trim().Replace("#", "")
                                aszValue = Split(szValue, "!")

                                szValue = ""
                                szHashValue = ""
                                For iCounter = 0 To aszValue.GetUpperBound(0)
                                    If (0 = (iCounter Mod 2)) Then ' Field
                                        'Added on 17 June 2009, to get MerchantID, CurrencyCode, HostID in order to further bGetTerminal to get SendKey, ReturnKey, SecretKey
                                        Select Case aszValue(iCounter).Trim().ToLower()
                                            Case "[secretkey]", "[secretkeyhex]", "[hostpassword1]", "[hostpassword2]", "[hostpassword3]", "[hostpassword1hex]", "[hostpassword2hex]", "[hostpassword3hex]", "[sendkeypath]", "[returnkeypath]"
                                                If ("" <> st_PayInfo.szTxnID) Then
                                                    'NOTE: The following config template is NOT YET SUPPORTED!!!!!!
                                                    'GatewayTxnID is not encrypted in response, only certain fields are encrypted using 3DES, e.g. ConfNum and Amount
                                                    '*** Must put GatewayTxnID before/on the left of other encrypted fields ***
                                                    'e.g. BankRef1|[GatewayTxnID]|ConfNum|[Reserved]{*[3DES]!!![BankRefNo]*}|Amount|[Reserved]{*[3DES]!!![TxnAmount]*}
                                                    'bGetReqTxn() will be called again in bVerifyResTxn()
                                                    stPayInfo.szTxnID = st_PayInfo.szTxnID

                                                    'Modified 25 Nov 2013, use objCommon instead of objTxnProc, to cater for different DB connection by Payment Method
                                                    If (True <> objCommon.bGetReqTxn(stPayInfo, st_HostInfo)) Then
                                                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                                    stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > iGetQueryString() " + stPayInfo.szErrDesc)

                                                        Return False
                                                    End If
                                                Else
                                                    'For bank response which the whole content is encrypted, no GatewayTxnID is available like the above,
                                                    'DDGW will need to retrieve the respective merchant's SecretKey to decrypt the payment response.
                                                    'Since the whole content is encrypted, GW has no way to identify to which merchant that the response belongs and therefore
                                                    'the respective MerchantID identifying merchant, IssuingBank and CurrencyCode need to be
                                                    'defined in each response_[IssuingBank]_[MerchantID].aspx and pass to this bGetData().
                                                    '0|[Reserved]{*[3DES]!&!BillRef1![GatewayTxnID]!Amount![TxnAmount]*}
                                                    stPayInfo.szMerchantID = st_PayInfo.szMerchantID
                                                    stPayInfo.szCurrencyCode = szCurrencyCode   'Modified 11 Dec 2017, 
                                                End If

                                                'Added on 17 June 2009
                                                'Get Terminal and Channel information - MID, TID, SendKey, ReturnKey, SecretKey, InitVector, PayeeCode, PayURL, QueryURL, CfgFile, ReturnURL
                                                'Modified 25 Nov 2013, use objCommon instead of objTxnProc, to cater for different DB connection by Payment Method
                                                If True <> objCommon.bGetTerminal(st_HostInfo, stPayInfo) Then
                                                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                                    stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > iGetQueryString() " + stPayInfo.szErrDesc)
                                                    Return False
                                                End If
                                        End Select

                                        Select Case aszValue(iCounter).Trim().ToLower()
                                            Case "[]"               'Empty value
                                                szValue = ""
                                            Case "[sendkey]"
                                                'Modified on 25 May 2009
                                                'NOTE: Use szDecrypt3DES instead of szDecrypt3DEStoHex because original encrypted value(Hash Key) is not in hexadecimal value
                                                'szValue = st_HostInfo.szSendKey
                                                szValue = objHash.szDecrypt3DES(st_HostInfo.szSendKey, Common.C_3DESAPPKEY, Common.C_3DESAPPVECTOR)
                                                st_HostInfo.szSendKey = szValue

                                            Case "[sendkeyhex]"
                                                'Added on 3 Jun 2009
                                                'NOTE: Use szDecrypt3DES instead of szDecrypt3DES because original encrypted value(Hash Key) is in hexadecimal value
                                                szValue = objHash.szDecrypt3DEStoHex(st_HostInfo.szSendKey, Common.C_3DESAPPKEY, Common.C_3DESAPPVECTOR)
                                                st_HostInfo.szSendKey = szValue

                                                'CYBS-Masterpass
                                            Case "[sendkeypath]"
                                                szValue = st_PayInfo.szEncryptKeyPath
                                            Case "[languagecode]"
                                                szValue = st_PayInfo.szLanguageCode

                                            Case "[ordernumber]"
                                                szValue = st_PayInfo.szMerchantOrdID
                                            Case "[gatewaytxnid]"
                                                szValue = st_PayInfo.szTxnID
                                            Case "[txnamount]"      'Take the request's txn amount which is in 2 decimals. Modified 11 Dec 2017, removed st_PayInfo
                                                szValue = szTxnAmount
                                            Case "[txnamount_2]"    'Amount in cents whereby the last two digits are decimal points. Modified 11 Dec 2017, removed st_PayInfo
                                                szValue = szTxnAmount.Replace(".", "")
                                            Case "[txnamtexpn_2]"    'Amount without decimal point but the last two digits are Zero     'Added on 6  Jan 2016, MIGS exponent currency  -Masterpass-GP 14 Mar 2017. Modified 11 Dec 2017, removed st_PayInfo
                                                If ("JPY" = szCurrencyCode Or "KRW" = szCurrencyCode Or "VND" = szCurrencyCode Or "IDR" = szCurrencyCode) Then
                                                    szValue = Left(szTxnAmount, szTxnAmount.Length() - 3)
                                                Else
                                                    szValue = szTxnAmount.Replace(".", "")
                                                End If
                                            Case "[txnamount_02]"   'Added on 25 Oct 2009. 12 digits, leading zeroes, no decimal point, last 2 are decimal values. Modified 11 Dec 2017, removed st_PayInfo
                                                szValue = szTxnAmount.Replace(".", "")
                                                szValue = "".PadLeft(12 - szValue.Length(), "0") + szValue
                                            Case "[gatewayreturnurl]"
                                                'Added [languagecode] on 12 June 2009 to support guide_EN.htm, guide_*.htm for SCB's BackURL
                                                szValue = Replace(st_HostInfo.szReturnURL.ToLower(), "[languagecode]", st_PayInfo.szLanguageCode)
                                            Case "[gatewayreturnurl2]"  'Added on 22 Oct 2009
                                                szValue = Replace(st_HostInfo.szReturnURL2.ToLower(), "[languagecode]", st_PayInfo.szLanguageCode)
                                            Case "[gatewayreturnurlsvcid]"  'Added on 16 June 2009 to include MerchantID in Return URL
                                                'e.g. https://ddgwsg.airasia.com/directdebit/response_pbb.aspx to
                                                '     https://ddgwsg.airasia.com/directdebit/response_pbb_A08.aspx
                                                szValue = Replace(st_HostInfo.szReturnURL.Trim().ToLower(), "[languagecode]", stPayInfo.szLanguageCode)
                                                If (InStr(szValue, ".aspx") > 0) Then
                                                    szValue = szValue.Substring(0, szValue.Length() - 5) + "_" + stPayInfo.szMerchantID + ".aspx"
                                                End If
                                            Case "[gatewayreturnurl2svcid]"  'Added on 22 Oct 2009
                                                szValue = Replace(st_HostInfo.szReturnURL2.Trim().ToLower(), "[languagecode]", stPayInfo.szLanguageCode)
                                                If (InStr(szValue, ".aspx") > 0) Then
                                                    szValue = szValue.Substring(0, szValue.Length() - 5) + "_" + stPayInfo.szMerchantID + ".aspx"
                                                End If
                                            Case "[param1]"
                                                szValue = st_PayInfo.szParam1
                                            Case "[param2]"
                                                szValue = st_PayInfo.szParam2
                                            Case "[param3]"
                                                szValue = st_PayInfo.szParam3
                                            Case "[param4]"
                                                szValue = st_PayInfo.szParam4
                                            Case "[param5]"
                                                szValue = st_PayInfo.szParam5
                                            Case "[param6]"
                                                szValue = st_PayInfo.szParam6
                                            Case "[param7]"
                                                szValue = st_PayInfo.szParam7
                                            Case "[param8]"
                                                szValue = st_PayInfo.szParam8
                                            Case "[param9]"
                                                szValue = st_PayInfo.szParam9
                                            Case "[param10]"
                                                szValue = st_PayInfo.szParam10
                                            Case "[param11]"
                                                szValue = st_PayInfo.szParam11
                                            Case "[param12]"
                                                szValue = st_PayInfo.szParam12
                                            Case "[param13]"
                                                szValue = st_PayInfo.szParam13
                                            Case "[param14]"
                                                szValue = st_PayInfo.szParam14
                                            Case "[param15]"
                                                szValue = st_PayInfo.szParam15
                                            Case "[currencycode]"
                                                szValue = szCurrencyCode    'Modified 11 Dec 2017, removed st_PayInfo
                                            Case "[orderdesc]"
                                                szValue = st_PayInfo.szMerchantOrdDesc
                                            Case "[cardpan]"
                                                szValue = st_PayInfo.szCardPAN
                                            Case "[cvv2]"
                                                szValue = st_PayInfo.szCVV2
                                            Case "[maskcardpan]"                'Added  14 Oct. MasterPass
                                                If "" <> st_PayInfo.szCardPAN Then
                                                    If st_PayInfo.szCardPAN.Length > 10 Then
                                                        szValue = st_PayInfo.szCardPAN.Substring(0, 6) + "".PadRight(st_PayInfo.szCardPAN.Length - 6 - 4, "x") + st_PayInfo.szCardPAN.Substring(st_PayInfo.szCardPAN.Length - 4, 4)
                                                    Else
                                                        szValue = "".PadRight(st_PayInfo.szCardPAN.Length, "x")
                                                    End If
                                                End If
                                            Case "[sessionid]"
                                                szValue = st_PayInfo.szMerchantSessionID    'Modified 16 Jul 2014, SessionID to MerchantSessionID
                                            Case "[mid]"
                                                szValue = st_HostInfo.szMID
                                            Case "[tid]"
                                                szValue = st_HostInfo.szTID
                                            Case "[acquirerid]"                    'Added on 15 June 2009, to store e.g. Query UserID, Product ID which is different per merchant per bank
                                                szValue = st_HostInfo.szAcquirerID
                                            Case "[payeecode]"                     'Added on 15 June 2009, to store Payee Code which is different per merchant per bank
                                                szValue = st_HostInfo.szPayeeCode
                                            Case "[hostpassword1]"                  'Added on 17 June 2009, to retrieve value in Password field in Terminals table
                                                szValue = objHash.szDecrypt3DES(st_HostInfo.szSendKey, Common.C_3DESAPPKEY, Common.C_3DESAPPVECTOR)
                                            Case "[hostpassword2]"                  'Added on 17 June 2009, to retrieve value in ReturnKey field in Terminals table
                                                szValue = objHash.szDecrypt3DES(st_HostInfo.szReturnKey, Common.C_3DESAPPKEY, Common.C_3DESAPPVECTOR)
                                            Case "[hostpassword3]"                  'Added on 17 June 2009, to retrieve value in SecretKey field in Terminals table
                                                szValue = objHash.szDecrypt3DES(st_HostInfo.szSecretKey, Common.C_3DESAPPKEY, Common.C_3DESAPPVECTOR)
                                            Case "[hostpassword1hex]"               'Added on 17 June 2009, to retrieve hex-value in Password field in Terminals table
                                                szValue = objHash.szDecrypt3DEStoHex(st_HostInfo.szSendKey, Common.C_3DESAPPKEY, Common.C_3DESAPPVECTOR)
                                            Case "[hostpassword2hex]"               'Added on 17 June 2009, to retrieve hex-value in ReturnKey field in Terminals table
                                                szValue = objHash.szDecrypt3DEStoHex(st_HostInfo.szReturnKey, Common.C_3DESAPPKEY, Common.C_3DESAPPVECTOR)
                                            Case "[hostpassword3hex]"               'Added on 17 June 2009, to retrieve hex-value in SecretKey field in Terminals table
                                                szValue = objHash.szDecrypt3DEStoHex(st_HostInfo.szSecretKey, Common.C_3DESAPPKEY, Common.C_3DESAPPVECTOR)
                                            Case "[sendkeypath]"                    'Added on 5 June 2010
                                                szValue = stPayInfo.szEncryptKeyPath
                                            Case "[returnkeypath]"                  'Added on 5 June 2010
                                                szValue = stPayInfo.szDecryptKeyPath
                                            Case "[merchantname]"                   'Added  27 Oct 2016. MasterPass
                                                szValue = st_PayInfo.szMerchantName
                                            Case "[merchanttxnid]"                   'Added  27 Oct 2016. MasterPass
                                                szValue = st_PayInfo.szMerchantTxnID
                                            Case "[date_ddmmyyyy_hh:mm:ss]"         'Added  28 Nov 2013, Alliance CC; Combined  28 Apr 2014
                                                szValue = Convert.ToDateTime(st_PayInfo.szTxnDateTime).ToString("ddMMyyyy HH:mm:ss")
                                            Case "[date_yyyymmddthh:mm:ssz]"         'Added  1 July 2014, GlobalPay Cybersource
                                                'original date format is 2013-11-29 15:10:10
                                                szValue = Convert.ToDateTime(st_PayInfo.szTxnDateTime).ToString("yyyy-MM-dd'T'HH:mm:ss'Z'")
                                            Case "[utc_yyyymmddthh:mm:ssz]"         'Added  4 July 2014, GlobalPay Cybersource
                                                szValue = st_PayInfo.szUTCDateTime
                                            Case "[uuid]"                   'Added  1 July 2014. GlobalPay Cybersource
                                                szValue = st_PayInfo.szUUID
                                            Case "[custemail]"              'Added  16 Jun 2014. Firefly FFCIMB
                                                szValue = st_PayInfo.szCustEmail
                                            Case "[billaddr]"               'Added  16 Jun 2014. Firefly FFCIMB
                                                szValue = st_PayInfo.szBillAddr
                                                If ("" = szValue) Then
                                                    szValue = "1"
                                                    'If ("" = szValue) Then      'Added  16 Oct 2015, GlobaPay Cybersource (AVS)
                                                    '    szValue = "-"
                                                End If
                                            Case "[billcountry2]"            'Added  16 Jun 2014. Firefly FFCIMB
                                                szValue = st_PayInfo.szBillCountry

                                                If ("" = szValue) Then      'Added, 22 Jul 2014. GlobalPay CyberSource
                                                    szValue = st_PayInfo.szMerchantCountry
                                                End If

                                            Case "[unixtimestamp]"
                                                szValue = DateDiff("s", "01/01/1970 00:00:00", stPayInfo.szTxnDateTime).ToString()
                                            Case Else
                                                'Check if it is a default value, e.g. [=AIRASIA]
                                                If ("[=" = Left(aszValue(iCounter).Trim(), 2)) Then
                                                    szValue = Mid(aszValue(iCounter).Trim(), 3, aszValue(iCounter).Trim().Length - 3)

                                                ElseIf ("{" = Left(aszValue(iCounter).Trim(), 1)) Then   ' "{" indicates format
                                                    'Added on 14 Oct 2016  MasterPass
                                                    If ("{SR@" = Left(aszValue(iCounter).Trim().ToUpper(), 4)) Then
                                                        'Currently use for Card Expired Date different format require by bank. This only can be use on restructure 1 string format to another string format
                                                        'Example: {SR@R:2@L:2@M:2:4@[param3]}. Note: The sequance of R(right)/L(left)/M(Middle) will detemine of the output string format.
                                                        '         [param3] = 022013 / [param3] = abcdefghijk
                                                        '         CardExpireDate format require by bank is MMYY. In template as {SR@L:2@R:2@[param3]}. Result = 0213
                                                        '         CardExpireDate format require by bank is YYMM. In template as {SR@R:2@L:2@[param3]}. Result = 1302
                                                        '         Other string format require. In template set as {SR@R:1@M:3:3@[param3]}. Result = adef

                                                        'need to check for stpayinfo. field and get the value b4 pass in to restructure
                                                        Dim szField As String = ""
                                                        Dim aszSplit() As String

                                                        szValue = aszValue(iCounter).Trim().Replace("{SR@", "").Replace("}", "")
                                                        aszSplit = Split(szValue, "@")

                                                        'take the last array's value
                                                        Select Case aszSplit(aszSplit.Length - 1).ToLower()
                                                            'Add case for new fields when needed
                                                            Case "[param3]"
                                                                szValue = st_PayInfo.szParam3
                                                                'szHashLogValue = "".PadLeft(szValue.Length, "X")    'Added  10 Dec 2013, Alliance CC; Combined  28 Apr 2014
                                                            Case "[orderdesc]"                  'Added  6 May 2015. FPXD
                                                                szValue = objCommon.szReplaceSpecialChar(st_PayInfo.szMerchantOrdDesc) 'Modified  26 May 2016, FPXD does not allow certain special characters
                                                        End Select

                                                        szValue = objCommon.szFormatStringRestructure(aszSplit, szValue)

                                                    ElseIf ("{ASCII" = Left(aszValue(iCounter).Trim().ToUpper(), 6)) Then  'Added  28 Nov 2013, Alliance CC; Combined  28 Apr 2014
                                                        'e.g. PX_MERCHANT_ID|{ASCII15[GatewayTxnID]}
                                                        szValue = objHash.szASCIICounter(st_PayInfo.szTxnID, st_HostInfo.szMID)

                                                        Dim sb As StringBuilder = New StringBuilder
                                                        Dim szConfigField As String = aszValue(iCounter)
                                                        For i = 0 To szConfigField.Length - 1
                                                            If IsNumeric(szConfigField(i)) Then
                                                                sb.Append(szConfigField(i))
                                                            End If
                                                        Next

                                                        If "" <> sb.ToString Then
                                                            If Convert.ToInt32(sb.ToString()) > szValue.Length Then
                                                                szValue = szValue.PadLeft(Convert.ToInt32(sb.ToString()), "0")
                                                            End If
                                                        End If

                                                    ElseIf ("{REVMASK" = Left(aszValue(iCounter).Trim().ToUpper(), 8)) Then  'Added  9 April 2015. eBPG Maybank-CC
                                                        'Reverse masking field value with 'x'
                                                        'Example: {REVMASK@R:2@[cvv2]}. Note: The sequance of R(right)/L(left)/M(Middle) detemine the output string format.
                                                        '         [cvv2] = 123 - In template as {REVMASK@R:2@[cvv2]}. Result = 12x
                                                        '         [cvv2] = 789 - In template as {REVMASK@L:2@[cvv2]}. Result = x89
                                                        '         [param3] = 333344445555666 - In template set as {REVMASK@M:6:6@[param3]}. Result = xxxxxx445555xxx
                                                        Dim aszSplit() As String = Nothing

                                                        szValue = aszValue(iCounter).Trim().Replace("{REVMASK@", "").Replace("}", "")
                                                        aszSplit = Split(szValue, "@")

                                                        'take the last array's value
                                                        Select Case aszSplit(aszSplit.Length - 1).ToLower()
                                                            'Add case for new fields when needed
                                                            Case "[cvv2]"
                                                                szValue = st_PayInfo.szCVV2
                                                                szLogValue = "".PadLeft(szValue.Length, "X")
                                                        End Select

                                                        szValue = objCommon.szRevMask(aszSplit, szValue)

                                                    ElseIf ("{IF@" = Left(aszValue(iCounter).Trim().ToUpper(), 4)) Then  'Added, 3 Aug 2011, for Smartlink
                                                        '{IF@@EN@vi@VN@[LanguageCode]}
                                                        Dim szDefaultValue As String = ""
                                                        Dim iCount As Integer = 0
                                                        Dim aszSplit() As String = Nothing

                                                        szValue = aszValue(iCounter).Trim().Replace("{", "").Replace("}", "")
                                                        aszSplit = Split(szValue, "@")

                                                        'Source string to be IF ELSE
                                                        Select Case aszSplit(aszSplit.GetUpperBound(0)).ToLower()
                                                            Case "[languagecode]"
                                                                szValue = st_PayInfo.szLanguageCode

                                                            Case "[cardtype]"   'Added  29 Oct 2013, for HSBC Credit
                                                                szValue = st_PayInfo.szCardType

                                                                'Added, 29 Oct 2013
                                                                If ("" = szValue) Then
                                                                    If (True = objTxnProc.bGetCardType(st_PayInfo)) Then
                                                                        szValue = st_PayInfo.szCardType
                                                                    End If
                                                                Else
                                                                    szValue = Left(st_PayInfo.szCardType, 1) 'Added, 20 May 2016, for MyDin Card Type
                                                                End If
                                                        End Select

                                                        'Look for any default value, skips the last element which is the field
                                                        For iCount = 1 To (aszSplit.GetUpperBound(0) - 1)
                                                            If (0 <> (iCount Mod 2)) Then
                                                                If ("" = aszSplit(iCount).ToLower()) Then
                                                                    szDefaultValue = aszSplit(iCount + 1)
                                                                    Exit For
                                                                End If
                                                            End If
                                                        Next

                                                        'Look for value mapping from Merchant's request to Host's request, skips the last element which is the field
                                                        bFound = False
                                                        For iCount = 1 To (aszSplit.GetUpperBound(0) - 1)
                                                            If (0 <> (iCount Mod 2)) Then
                                                                If (szValue.ToLower() = aszSplit(iCount).ToLower()) Then
                                                                    szValue = aszSplit(iCount + 1)
                                                                    bFound = True
                                                                    Exit For
                                                                End If
                                                            End If
                                                        Next

                                                        If (False = bFound) Then
                                                            szValue = szDefaultValue
                                                        End If

                                                        '  CYBS Masterpass
                                                    ElseIf ("{RIGHT@" = Left(aszValue(iCounter).Trim().ToUpper(), 7) Or "{LEFT@" = Left(aszValue(iCounter).Trim().ToUpper(), 6)) Then
                                                        'Added, 6 Oct 2016, KTB
                                                        'Example: {RIGHT@6@[GatewayTxnID]} ==> Retrieve the right 6 characters from GatewayTxnID
                                                        'Format : (0):RIGHT;(1):<How many characters to take from right side>;(2):<Field to be retrieved>
                                                        Dim aszSplit() As String = Nothing
                                                        szValue = aszValue(iCounter).Trim().Replace("{", "").Replace("}", "")
                                                        aszSplit = Split(szValue, "@")

                                                        If ("{RIGHT@" = Left(aszValue(iCounter).Trim().ToUpper(), 7)) Then
                                                            aszSplit(0) = "R"
                                                        Else
                                                            aszSplit(0) = "L"
                                                        End If

                                                        'Source string to be retrieved from RIGHT/LEFT
                                                        Select Case aszSplit(2).ToLower()
                                                            Case "[custphone]"
                                                                szValue = st_PayInfo.szCustPhone 'Added  21 Oct 2015, for GP which allows a maximum number of digits for bill phone
                                                                'Remove to avoid conflicting with visibility of 'signature' field of masterpass
                                                                'If (szValue <> "") Then
                                                                '    szLogValue = "".PadRight(szValue.Length, "X")
                                                                'End If

                                                                ' CYBS Masterpass
                                                            Case "[merchantname]"
                                                                szValue = st_PayInfo.szMerchantName
                                                                'Remove to avoid conflicting with visibility of 'signature' field of masterpass
                                                                'If (szValue <> "") Then
                                                                '    szLogValue = "".PadRight(szValue.Length, "X")
                                                                'End If

                                                        End Select

                                                        If ("R" = aszSplit(0).ToUpper()) Then
                                                            szValue = Right(szValue, Integer.Parse(aszSplit(1)))
                                                            'szLogValue = "".PadLeft(szValue.Length, "X")
                                                        Else
                                                            szValue = Left(szValue, Integer.Parse(aszSplit(1)))
                                                            'szLogValue = "".PadLeft(szValue.Length, "X")
                                                        End If

                                                    Else 'ElseIf ("{" = Left(aszValue(iCounter).Trim(), 1)) Then   ' "{" indicates format
                                                        'Commented  18 Feb 2016, SCBTH encountered Query error "invalid referential date" because pymt request using szTxnDateTime but here using DateTime.Now
                                                        'szValue = System.DateTime.Now.ToString(aszValue(iCounter).Trim().Replace("{", "").Replace("}", ""))
                                                        'Added  18 Feb 2016, uses TxnDateTime
                                                        'Modified 2 Mar 2016, modified stPayInfo to st_PayInfo
                                                        szValue = Convert.ToDateTime(st_PayInfo.szTxnDateTime).ToString(aszValue(iCounter).Trim().Replace("{", "").Replace("}", ""))
                                                    End If
                                                End If

                                        End Select

                                        szHashValue = szHashValue + szValue
                                    Else
                                        ' Delimiter
                                        If (InStr(aszValue(iCounter).Trim().ToLower(), "\n") > 0) Then  'Added  29 Nov 2013, Alliance CC; Combined  28 Apr 2014
                                            szDelimiterEx = aszValue(iCounter).Trim().ToLower().Replace("[", "").Replace("]", "").Replace("\n", vbLf)
                                        Else
                                            szDelimiterEx = aszValue(iCounter).Trim().Replace("[", "").Replace("]", "")
                                        End If

                                        szHashValue = szHashValue + szDelimiterEx
                                        'szHashLog = szHashLog + szDelimiterEx

                                        'szHashValue = szHashValue + aszValue(iCounter).Trim().Replace("[", "").Replace("]", "")
                                    End If
                                Next

                                'Added on 4 Nov 2009 Public and Private Key file path assignment from st_PayInfo to stPayInfo
                                stPayInfo.szDecryptKeyPath = st_PayInfo.szDecryptKeyPath
                                stPayInfo.szEncryptKeyPath = st_PayInfo.szEncryptKeyPath

                                ' CYBS-Masterpass
                                'To handle the wierd occurence of having the issuing bank appended to the end of the Hashing Method
                                'This issue is currently present with Masterpass-enabled GPAY
                                'st_HostInfo.szHashMethod = st_HostInfo.szHashMethod.Split("|")(0)
                                If (InStr(st_HostInfo.szHashMethod, "|") > 0) Then
                                    st_HostInfo.szHashMethod = st_HostInfo.szHashMethod.Split("|")(0)
                                End If

                                szValue = objHash.szGetHash(szHashValue, st_HostInfo.szHashMethod, stPayInfo, st_HostInfo)

                            Else
                                'Unsupported value tag
                                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + "> Unsupported field value tag(" + szConfig(iLoop) + ") in Config Template for " + st_PayInfo.szIssuingBank)
                                GoTo CleanUp
                            End If

                    End Select

                    'Do not encode SOAP messages (web service)
                    'CYBS-Masterpass - Added a condition to not encode Gpay Masterpass Request
                    'Modified  9 March 2018. POST method redirect should not encode
                    If ("WS" <> st_PayInfo.szPostMethod And "GET&BYTES" <> st_PayInfo.szPostMethod And "POST" <> st_PayInfo.szPostMethod And "JSON" <> st_PayInfo.szPostMethod) Then ' And "GET" <> st_PayInfo.szPostMethod) Then
                        If (szValue <> "") Then
                            szValue = Server.UrlEncode(szValue)
                        End If
                    End If
                    'If ("WS" <> st_PayInfo.szPostMethod And "GET&BYTES" <> st_PayInfo.szPostMethod) Then 'And Not ("POST" = st_PayInfo.szPostMethod And 1 = st_PayInfo.iMasterPass And "GPay" = st_PayInfo.szIssuingBank)) Then

                    '    'Modified  Masterpass
                    '    If (szValue <> "") Then
                    '        'Modified by OM, 6 Jul 2017, added checking of [merchantname]. Modified 27 Aug 2017. added date & hash '#'
                    '        If ("[gatewayreturnurl]" <> szConfig(iLoop).Trim().ToLower() And "[merchantname]" <> szConfig(iLoop).Trim().ToLower() And "[date_ddmmyyyy_hh:mm:ss]" <> szConfig(iLoop).Trim().ToLower() And "#" <> Left(szConfig(iLoop).Trim(), 1)) Then
                    '            szValue = Server.UrlEncode(szValue)
                    '        Else
                    '            '[gatewayreturnurl]
                    '            If ("POST" <> st_PayInfo.szPostMethod) Then
                    '                szValue = Server.UrlEncode(szValue)
                    '            End If
                    '        End If
                    '    End If
                    'End If

                    'Modified on 20 Jul 2009, "GET" = st_PayInfo.szPostMethod TO Left(st_PayInfo.szPostMethod.ToUpper(), 3)
                    If (("GET" = Left(st_PayInfo.szPostMethod.ToUpper(), 3)) Or ("WS" = st_PayInfo.szPostMethod) Or ("TCP" = Left(st_PayInfo.szPostMethod.ToUpper(), 3)) Or ("JSON" = st_PayInfo.szPostMethod)) Then 'Added  9 Jul 2012 for FPX
                        szParams = szParams + szValue
                    ElseIf ("POST" = st_PayInfo.szPostMethod.ToUpper() Or "POSTONLY" = st_PayInfo.szPostMethod.ToUpper()) Then
                        'Added POSTONLY on 26 Jul 2010
                        If (szLogValue <> "") Then
                            sz_LogParams = sz_LogParams + szLogValue + "'>"
                        Else
                            sz_LogParams = sz_LogParams + szValue + "'>"
                        End If

                        szParams = szParams + szValue + "'>" + vbCrLf
                    End If
                End If  ' Field value tag
            Next iLoop

            sz_QueryString = szParams

            'Added  18 Oct 2016. Masterpass param string contain credit card details. Commented this log and added sz_LogParams and log at iHTTPSend
            'objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
            '                DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
            '                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Param string (" + szParams + ") for " + st_PayInfo.szIssuingBank)

            iReturn = 0

        Catch ex As Exception
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Exception caught: " + ex.ToString)
        End Try

CleanUp:
        If Not objStreamReader Is Nothing Then objStreamReader = Nothing

        Return iReturn
    End Function

    '********************************************************************************************
    ' Class Name:		Response
    ' Function Name:	bVerifyResTxn
    ' Function Type:	Boolean.  Return False if error.
    ' Parameter:		
    ' Description:		Verify response data with request data stored in database.
    '                   At the same time, retrieve some request data from database based on Gateway TxnID to return to client's server.
    ' History:			29 Oct 2008
    '********************************************************************************************
    'Modified , 25 Apr 2011, for ENETS, changed from Private to Public, so that response_enets.aspx.vb able to call this
    Public Function bVerifyResTxn(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost, ByRef sz_HTML As String) As Boolean

        Dim bReturn As Boolean = True
        Dim bRet As Boolean
        Dim bCompare As Boolean = False
        Dim stPayInfo As Common.STPayInfo   'Declare local PayInfo structure so that will not overwrite st_PayInfo which is storing response data

        Try
            stPayInfo.szTxnID = st_PayInfo.szTxnID
            stPayInfo.szPymtMethod = st_PayInfo.szPymtMethod    'Added, 25 Nov 2013

            st_PayInfo.iQueryStatus = 2 'Added on 29 Mar 2010, initialization of st_PayInfo.iQueryStatus

            'Modified 25 Nov 2013, use objCommon instead of objTxnProc, to cater for different DB connection by Payment Method
            bRet = objCommon.bGetReqTxn(stPayInfo, st_HostInfo)  'Added st_HostInfo on 16 June 2009

            st_PayInfo.iQueryStatus = stPayInfo.iQueryStatus    'Added on 29 Mar 2010
            If bRet <> True Then
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bVerifyResTxn() " + stPayInfo.szErrDesc)

                'Added iNeedReplyAcknowledgement checking on 1 Mar 2009, to cater for the the following scenario:
                'Response already returned by host that requires Gateway to send acknowledgement but due to communication
                'error to host, acknowledgement failed to be sent to host but request record already moved into Response
                'table. Host may resent reply due to failure in getting acknowledgement from Gateway.
                'When Gateway checks record already existed in Response table, Gateway should not treat this as an
                'invalid response if host requires an acknowledgement.
                If (st_HostInfo.iNeedReplyAcknowledgement <> 1 Or stPayInfo.iQueryStatus <> -1) Then
                    'Added on 28 Mar 2010 to support host response already existed in Response table, only for DDGW Aggregator Model
                    'To cater for more than one type or more than one time of response from host
                    If (ConfigurationManager.AppSettings("HostsListTemplate") <> "") Then
                        If (-1 = stPayInfo.iQueryStatus) Then
                            'Modified 25 Nov 2013, use objCommon instead of objTxnProc, to cater for different DB connection by Payment Method
                            bRet = objCommon.bGetResTxn(stPayInfo, st_HostInfo)

                            If bRet <> True Then
                                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bVerifyResTxn() " + stPayInfo.szErrDesc)

                                Return False
                            Else
                                'Added on 11 Apr 2010, verification of RespCode returned by Host against RespCode already received and stored in database
                                If ((stPayInfo.szRespCode.ToUpper() <> st_PayInfo.szRespCode.ToUpper()) And (stPayInfo.szRespCode.ToUpper() <> st_PayInfo.szHostTxnStatus.ToUpper())) Then
                                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bVerifyResTxn() RespCode Mismatched. Host(" + st_PayInfo.szHostTxnStatus + " or " + st_PayInfo.szRespCode + ") Gateway(" + stPayInfo.szRespCode + ")")
                                    'Add , 23 Sep 2017, Record late response
                                    If (True <> objCommon.bInsertLateResponse(st_PayInfo)) Then
                                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                        stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bVerifyResTxn() Error insert late response")
                                    End If
                                    Return False
                                End If

                                st_PayInfo.szMerchantName = stPayInfo.szMerchantName    'Added on 4 Apr 2010
                            End If
                        Else
                            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INVALID_HOST_REPLY_GATEWAYTXNID
                            st_PayInfo.szErrDesc = stPayInfo.szTxnMsg

                            Return False
                        End If
                    Else
                        'Added iTxnStatus, 27 Feb 2009, to cater for possibility of hackers send fake query string reply
                        'which contains GatewayTxnID already finalized in Gateway (in Response table) or
                        'GatewayTxnID which does not exist in both Request and Response tables.
                        'e.g. Payment rejected by bank and finalized in response table already but hacker sends fake APPROVED reply.
                        'This status will be detected to avoid returning error message to merchant and inserting into response table.
                        st_PayInfo.iTxnStatus = Common.TXN_STATUS.INVALID_HOST_REPLY_GATEWAYTXNID
                        st_PayInfo.szErrDesc = stPayInfo.szTxnMsg

                        Return False
                    End If
                Else
                    'Added on 28 Mar 2010
                    If (-1 = stPayInfo.iQueryStatus) Then
                        'Modified 25 Nov 2013, use objCommon instead of objTxnProc, to cater for different DB connection by Payment Method
                        bRet = objCommon.bGetResTxn(stPayInfo, st_HostInfo)

                        If bRet <> True Then
                            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                            stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bVerifyResTxn() " + stPayInfo.szErrDesc)

                            Return False
                        Else
                            'Added on 11 Apr 2010, verification of RespCode returned by Host against RespCode already received and stored in database
                            If ((stPayInfo.szRespCode.ToUpper() <> st_PayInfo.szRespCode.ToUpper()) And (stPayInfo.szRespCode.ToUpper() <> st_PayInfo.szHostTxnStatus.ToUpper())) Then
                                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bVerifyResTxn() RespCode Mismatched. Host(" + st_PayInfo.szHostTxnStatus + " or " + st_PayInfo.szRespCode + ") Gateway(" + stPayInfo.szRespCode + ")")
                                'Add , 23 Sep 2017, Record late response                                
                                If (True <> objCommon.bInsertLateResponse(st_PayInfo)) Then
                                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bVerifyResTxn() Error insert late response")
                                End If
                                Return False
                            End If

                            st_PayInfo.szMerchantName = stPayInfo.szMerchantName    'Added on 4 Apr 2010
                        End If
                    Else
                        st_PayInfo.iTxnStatus = Common.TXN_STATUS.INVALID_HOST_REPLY_GATEWAYTXNID
                        st_PayInfo.szErrDesc = stPayInfo.szTxnMsg

                        Return False
                    End If
                End If
            End If

            st_PayInfo.szLanguageCode = stPayInfo.szLanguageCode

            ' Assign stPayInfo data to st_PayInfo ONE BY ONE for only those data not received from Host and needed to be replied to client (e.g. MerchantTxnID)
            ' CAN NOT do st_PayInfo = stPayInfo because DO NOT want to overwrite Host's response data in st_PayInfo.
            ' Puts all these fields here so that if bCompareOriginalReq returns error, these fields will be returned to
            ' the merchant.
            st_PayInfo.szTxnType = stPayInfo.szTxnType
            st_PayInfo.szMerchantID = stPayInfo.szMerchantID
            st_PayInfo.szMerchantTxnID = stPayInfo.szMerchantTxnID
            st_PayInfo.szIssuingBank = stPayInfo.szIssuingBank
            st_PayInfo.iTxnStatus = stPayInfo.iTxnStatus
            st_PayInfo.iTxnState = stPayInfo.iTxnState
            st_PayInfo.szMerchantSessionID = stPayInfo.szMerchantSessionID  'Modified 16 Jul 2014, SessionID to MerchantSessionID
            'Commented on 18 May 2009 so that they won't be inserted again into TB_Pay_Params table in spInsRespTxn stored procedure
            'st_PayInfo.szParam1 = stPayInfo.szParam1
            'st_PayInfo.szParam2 = stPayInfo.szParam2
            'st_PayInfo.szParam3 = stPayInfo.szParam3
            'st_PayInfo.szParam4 = stPayInfo.szParam4
            'st_PayInfo.szParam5 = stPayInfo.szParam5
            st_PayInfo.szHashMethod = stPayInfo.szHashMethod
            st_PayInfo.szMerchantReturnURL = stPayInfo.szMerchantReturnURL
            st_PayInfo.szMerchantApprovalURL = stPayInfo.szMerchantApprovalURL      'Added, 8 Jul 2014
            st_PayInfo.szMerchantUnApprovalURL = stPayInfo.szMerchantUnApprovalURL  'Added, 8 Jul 2014
            st_PayInfo.szMerchantCallbackURL = stPayInfo.szMerchantCallbackURL      'Added  16 Oct 2014
            st_PayInfo.szGatewayID = stPayInfo.szGatewayID
            st_PayInfo.szMachineID = stPayInfo.szMachineID
            st_PayInfo.szCardPAN = stPayInfo.szCardPAN
            st_PayInfo.szMaskedCardPAN = stPayInfo.szMaskedCardPAN          ' Added on 31 July 2013  bInsertTxnResp not able to update CardPAN field bcoz it is not assign
            st_PayInfo.szCardExp = stPayInfo.szCardExp                      ' Added, 9 Oct 2015, for MyDin
            st_PayInfo.szCardHolder = stPayInfo.szCardHolder                ' Added, 9 Oct 2015, for MyDin
            st_PayInfo.szBaseTxnAmount = stPayInfo.szBaseTxnAmount          ' Added on 3 Aug 2009, base currency booking confirmation enhancement
            st_PayInfo.szBaseCurrencyCode = stPayInfo.szBaseCurrencyCode    ' Added on 3 Aug 2009, base currency booking confirmation enhancement
            st_PayInfo.szTxnDateTime = stPayInfo.szTxnDateTime              ' Added on 12 Oct 2009
            st_PayInfo.szMerchantOrdDesc = stPayInfo.szMerchantOrdDesc                  ' Added on 12 Oct 2009
            st_PayInfo.szCardHolder = stPayInfo.szCardHolder                ' Added, 14 Dec 2013
            st_PayInfo.szCustEmail = stPayInfo.szCustEmail                  ' Added, 16 Dec 2013
            st_PayInfo.szCustName = stPayInfo.szCustName                    ' Added, 18 Feb 2014
            st_PayInfo.szCustOCP = stPayInfo.szCustOCP                      ' Added, 12 Aug 2014
            st_PayInfo.szFXCurrencyCode = stPayInfo.szFXCurrencyCode        ' Added, 29 Aug 2014
            st_PayInfo.szFXTxnAmount = stPayInfo.szFXTxnAmount              ' Added, 29 Aug 2014
            st_PayInfo.szTokenType = stPayInfo.szTokenType                  ' Added, 26 Mar 2015, for BNB
            st_PayInfo.szPromoCode = stPayInfo.szPromoCode                  ' Added 30 Nov 2015,Promotion
            st_PayInfo.szPromoOriAmt = stPayInfo.szPromoOriAmt              ' Added 30 Nov 2015,Promotion
            st_PayInfo.szParam6 = stPayInfo.szParam6                        ' Added  6 Nov 2015, Param67
            st_PayInfo.szParam7 = stPayInfo.szParam7                        ' Added  6 Nov 2015, Param67
            st_PayInfo.szClearCardPAN = stPayInfo.szClearCardPAN            ' Added, 20 May 2016, for Token creation based on clear instead of masked CardPAN
            st_PayInfo.szCardType = stPayInfo.szCardType                    ' Added, 20 May 2016, for MyDin Card Type
            st_PayInfo.szParam12 = stPayInfo.szParam12                      ' Added  29 Aug 2017, for Masterpass enhancement, log txn to Masterpass
            st_PayInfo.szCustPhone = stPayInfo.szCustPhone                  ' Added, 12 Feb 2018, Tik FX

            'Compare certain Gateway's values with the corresponding Host's value
            'Added checking on 27 Feb 2009 to avoid hackers to send fake query string reply which contains valid
            'GatewayTxnID but invalid reply fields.
            'Do not compare fields if Host is using redirect method (without hashing) to reply because genuinity of the
            'reply can not be guaranteed, what if hackers/malicious people wanted to sabotage?
            'Fields comparison will be conducted after this function if host uses redirect reply method without hashing.
            bCompare = False
            If (st_HostInfo.iHostReplyMethod <> 1 And st_HostInfo.iHostReplyMethod <> 4) Then
                bCompare = True
            Else
                If (st_HostInfo.szReturnKey <> "") Then
                    bCompare = True
                End If
            End If

            If (True = bCompare) Then
                If bCompareOriginalReq(stPayInfo, st_PayInfo) <> True Then
                    st_PayInfo.szErrDesc = stPayInfo.szErrDesc

                    ' Put these 3 fields here so that st_PayInfo which is storing Host's invalid reply will be overwritten
                    ' by original request's values before being replied to merchant.
                    ' Modified 29 Aug 2014, for FX, added checking of szFXCurrencyCode
                    If ("" = stPayInfo.szFXCurrencyCode) Then
                        st_PayInfo.szTxnAmount = stPayInfo.szTxnAmount
                        st_PayInfo.szCurrencyCode = stPayInfo.szCurrencyCode
                    Else    'Added, 29 Aug 2014, for FX
                        st_PayInfo.szBaseCurrencyCode = stPayInfo.szCurrencyCode
                        st_PayInfo.szBaseTxnAmount = stPayInfo.szTxnAmount
                        st_PayInfo.szTxnAmount = stPayInfo.szFXTxnAmount        'For FX, stPayInfo.szTxnAmount is original TxnAmount
                        st_PayInfo.szCurrencyCode = stPayInfo.szFXCurrencyCode
                    End If

                    st_PayInfo.szMerchantOrdID = stPayInfo.szMerchantOrdID

                    st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.INVALID_HOST_REPLY
                    st_PayInfo.szTxnMsg = stPayInfo.szErrDesc
                    st_PayInfo.iTxnState = Common.TXN_STATE.ABORT

                    Return False
                End If
            End If

            ' Put these 3 fields here so that st_PayInfo is still storing these values from Host's reply
            ' when being passed into bCompareOriginalReq. After that, overwrites them with the Request's values or no need
            ' to overwrite also OK becoz they must be the same or else will fail in bCompareOriginalReq.
            ' Modified 29 Aug 2014, added checking of szFXCurrencyCode
            If ("" = stPayInfo.szFXCurrencyCode) Then
                st_PayInfo.szTxnAmount = stPayInfo.szTxnAmount          'Gateway's value - Verified against Host's value OK 
                st_PayInfo.szCurrencyCode = stPayInfo.szCurrencyCode    'Gateway's value - If returned by Host, verified against Host's value OK
            Else
                st_PayInfo.szBaseCurrencyCode = stPayInfo.szCurrencyCode    'Currency Code and Txn Amount retrieved from DB are original currency and amount
                st_PayInfo.szBaseTxnAmount = stPayInfo.szTxnAmount
                st_PayInfo.szTxnAmount = stPayInfo.szFXTxnAmount            'Overwrites szTxnAmount with FX Txn Amount
                st_PayInfo.szCurrencyCode = stPayInfo.szFXCurrencyCode
            End If

            st_PayInfo.szMerchantOrdID = stPayInfo.szMerchantOrdID      'Gateway's value - If returned by Host, verified against Host's value OK

        Catch ex As Exception
            bReturn = False

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > bVerifyResTxn() Exception: " + ex.Message)
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		Response
    ' Function Name:	bCompareOriginalReq
    ' Function Type:	Boolean.  Return False if Gateway's values mismatched with Host's values.
    ' Parameter:		
    ' Description:		Verify Gateway's values with Host's corresponding values
    ' History:			30 Oct 2008
    '********************************************************************************************
    Private Function bCompareOriginalReq(ByRef st_GatewayPayInfo As Common.STPayInfo, ByVal st_HostPayInfo As Common.STPayInfo) As Boolean
        Dim szTxnAmount As String = ""      'Added, 29 Aug 2014, for FX
        Dim szCurrencyCode As String = ""   'Added, 29 Aug 2014, for FX

        'Added, 29 Aug 2014, for FX
        If ("" = st_GatewayPayInfo.szFXCurrencyCode) Then
            szCurrencyCode = st_GatewayPayInfo.szCurrencyCode
            szTxnAmount = st_GatewayPayInfo.szTxnAmount
        Else
            szCurrencyCode = st_GatewayPayInfo.szFXCurrencyCode
            szTxnAmount = st_GatewayPayInfo.szFXTxnAmount
        End If

        If (st_HostPayInfo.szHostTxnAmount <> "") Then  'Modified 16 Jan 2018, added checking of HostTxnAmount to solve amt mismatched for host resp which got redirect n s2s n involved FX
            If Convert.ToDecimal(szTxnAmount) <> Convert.ToDecimal(st_HostPayInfo.szHostTxnAmount) Then
                st_GatewayPayInfo.szErrDesc = "Gateway(" + szTxnAmount + ") and " + st_GatewayPayInfo.szIssuingBank + " (" + st_HostPayInfo.szHostTxnAmount + ") TxnAmount mismatched."
                Return False
            End If
        ElseIf st_HostPayInfo.szTxnAmount <> "" Then
            'Modified 29 Aug 2014, use szTxnAmount instead of st_GatewayPayInfo.szTxnAmount, for FX
            If Convert.ToDecimal(szTxnAmount) <> Convert.ToDecimal(st_HostPayInfo.szTxnAmount) Then
                st_GatewayPayInfo.szErrDesc = "Gateway(" + szTxnAmount + ") and " + st_GatewayPayInfo.szIssuingBank + " (" + st_HostPayInfo.szTxnAmount + ") TxnAmount mismatched."
                Return False
            End If
        ElseIf st_HostPayInfo.szCurrencyCode <> "" Then
            'Modified 29 Aug 2014, use szCurrencyCode instead of st_GatewayPayInfo.szCurrencyCode, for FX
            If szCurrencyCode <> st_HostPayInfo.szCurrencyCode Then
                st_GatewayPayInfo.szErrDesc = "Gateway(" + szCurrencyCode + ") and " + st_GatewayPayInfo.szIssuingBank + " (" + st_HostPayInfo.szCurrencyCode + ") CurrencyCode mismatched."
                Return False
            End If
        End If

        Return True
    End Function

    '********************************************************************************************
    ' Class Name:		Response
    ' Function Name:	bCompareOriginalReq
    ' Function Type:	Boolean.  Return False if Gateway's values mismatched with Host's values.
    ' Parameter:		
    ' Description:		Verify Gateway's values with Host's corresponding values
    ' History:			27 Feb 2009
    '********************************************************************************************
    Private Function bCompareOriginalReq(ByRef st_PayInfo As Common.STPayInfo) As Boolean
        Dim szTxnAmount As String = ""      'Added, 29 Aug 2014, for FX
        Dim szCurrencyCode As String = ""   'Added, 29 Aug 2014, for FX

        'Added, 29 Aug 2014, for FX
        If ("" = st_PayInfo.szFXCurrencyCode) Then
            szCurrencyCode = st_PayInfo.szCurrencyCode
            szTxnAmount = st_PayInfo.szTxnAmount
        Else
            szCurrencyCode = st_PayInfo.szFXCurrencyCode
            szTxnAmount = st_PayInfo.szFXTxnAmount
        End If

        If st_PayInfo.szHostTxnAmount <> "" Then
            'Modified 29 Aug 2014, use szTxnAmount instead of st_PayInfo.szTxnAmount, for FX
            If Convert.ToDecimal(szTxnAmount) <> Convert.ToDecimal(st_PayInfo.szHostTxnAmount) Then
                st_PayInfo.szErrDesc = "Gateway(" + szTxnAmount + ") and " + st_PayInfo.szIssuingBank + " (" + st_PayInfo.szHostTxnAmount + ") TxnAmount mismatched."
                Return False
            End If
        ElseIf st_PayInfo.szHostGatewayTxnID <> "" Then
            If st_PayInfo.szTxnID <> st_PayInfo.szHostGatewayTxnID Then
                st_PayInfo.szErrDesc = "Gateway(" + st_PayInfo.szTxnID + ") and " + st_PayInfo.szIssuingBank + " (" + st_PayInfo.szHostGatewayTxnID + ") GatewayTxnID mismatched."
                Return False
            End If
        ElseIf st_PayInfo.szHostCurrencyCode <> "" Then
            'Modified 29 Aug 2014, use szCurrencyCode instead of st_PayInfo.szCurrencyCode, for FX
            If szCurrencyCode <> st_PayInfo.szHostCurrencyCode Then
                st_PayInfo.szErrDesc = "Gateway(" + szCurrencyCode + ") and " + st_PayInfo.szIssuingBank + " (" + st_PayInfo.szHostCurrencyCode + ") CurrencyCode mismatched."
                Return False
            End If
        ElseIf st_PayInfo.szHostOrderNumber <> "" Then
            If st_PayInfo.szMerchantOrdID <> st_PayInfo.szHostOrderNumber Then
                st_PayInfo.szErrDesc = "Gateway(" + st_PayInfo.szMerchantOrdID + ") and " + st_PayInfo.szIssuingBank + " (" + st_PayInfo.szHostOrderNumber + ") OrderNumber mismatched."
                Return False
            End If
        End If

        Return True
    End Function

    '********************************************************************************************
    ' Class Name:		Response
    ' Function Name:	bMerchantCallBackResp
    ' Function Type:	Boolean.  Return False if Gateway's failed to send callback responsee
    ' Parameter:		
    ' Description:		Return callback response to merchant
    ' History:			09 Feb 2015
    '********************************************************************************************
    Public Function bMerchantCallBackResp(ByRef st_PayInfo As Common.STPayInfo, ByVal st_HostInfo As Common.STHost) As Boolean
        Dim iReturn As Boolean = False
        Dim iRet As Integer = 0
        Dim szHTTPString As String = ""
        Dim szResp As String = ""
        Dim szCallbackStatus As String = ""
        Dim iCallBackStatus As Integer = -1
        Dim szLogString As String = ""          'Added, 22 May 2016, do not log clear Card Expiry

        Try
            'Added  6 Jun 2016, FPXD, to solve certain scenario iAllowCallBack is 0 even though Merchant table 1
            'If (0 = st_PayInfo.iAllowCallBack) Then
            '    objTxnProc.bGetMerchant(st_PayInfo)
            'End If

            'Added  20 Jun 2016
            'Generates response hash value
            If ("" = st_PayInfo.szHashValue Or "" = st_PayInfo.szHashValue2) Then
                'Added  27 Oct 2017, to prevent no merchant password and caused inaccurate hash value in callback response
                If ("" = st_PayInfo.szMerchantPassword) Then
                    objTxnProc.bGetMerchant(st_PayInfo)
                End If
                'st_PayInfo.szHashValue = objHash.szGetSaleResHash(st_PayInfo)
                st_PayInfo.szHashValue2 = objHash.szGetSaleResHash2(st_PayInfo)
            End If

            'If (1 = st_PayInfo.iAllowCallBack) Then    'Commented  9 Jun 2016, since FPXD S2S no call bGetMerchant and not able get iAllowCallback
            If ("" <> st_PayInfo.szMerchantCallbackURL) Then

                objTxnProc.bCheckCallBackStatus(st_PayInfo, iCallBackStatus)

                'Added  6 Jun 2016
                'Modified 13 Sept 2016, added logging of protocol
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > iCallBackStatus[" + iCallBackStatus.ToString() + "]")

                If (1 <> iCallBackStatus) Then   'Callback reponse not yet send/Fail to sent before
                    'ORIGINAL PLAN: Wait for "ok"/"OK" to stop sending response, maximum 3 times
                    szHTTPString = szGetReplyMerchantCallbackHTTPString(st_PayInfo, st_HostInfo, szLogString)

                    'Modified 24 Mar 2016, modified iHttpTimeoutSeconds to 10, to avoid thread aborted due to over 1.5 minutes
                    For t = 0 To 4 '2
                        'Modified 13 Sept 2016, added st_PayInfo.szProtocol to replace "", support callback resp protocol
                        'Modified 6 Jul 2017, modified 10s to 11s, to solve OpenCart 3.0 slow in returning ack for callback resp
                        iRet = objHTTP.iHTTPostWithRecv(st_PayInfo.szMerchantCallbackURL, szHTTPString, 11, szResp, "", st_PayInfo.szProtocol)

                        If (0 = iRet) Then
                            szCallbackStatus = "success"
                            iCallBackStatus = 1

                            If ("" <> szResp.Trim()) Then
                                szCallbackStatus = szCallbackStatus + " and recv acknowledgement (" + Left(szResp.Trim(), 10) + " ....)"
                                Exit For
                            End If
                        Else
                            szCallbackStatus = "fail"
                            iCallBackStatus = 0
                        End If

                        'Added by OM, 16 Apr 2019
                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "Callback count [" + (t + 1).ToString + "]")
                        Thread.Sleep(50)
                    Next

                    objTxnProc.bInsertCallBackStatus(st_PayInfo, iCallBackStatus)

                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "Callback response [" + szLogString + "], Status [" + szCallbackStatus + "] Protocol[" + st_PayInfo.szProtocol + "]") 'Modified 22 May 2016, szHTTPString to szLogString, do not log clear Card Expiry
                End If
            End If

            'End If

            iReturn = True
        Catch ex As Exception
            Return iReturn

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > bMerchantCallBackResp() Exception: " + ex.Message)
        End Try

    End Function

    'Added BTP, 26 Jan 2017.
    '********************************************************************************************
    ' Class Name:		Response
    ' Function Name:	szGetJsonTagValue
    ' Function Type:	String
    ' Parameter:		sz_JsonContent, sz_TagKey
    ' Description:		Return JsonTag value if matched else return empty
    ' History:			26 Jan 2017
    '********************************************************************************************
    Public Function szGetJsonTagValue(ByVal sz_JsonContent As String, ByVal sz_TagKey As String) As String

        Dim szValue As String = ""
        Dim objJson As JObject

        Try
            Dim iJsonElement As Integer = 1
            Dim aszJsonFields() As String = Split(sz_TagKey.Trim(), ":")

            'Added 711 to cater "ID:", for bitcoin is "ID:ID2"
            If aszJsonFields.Length() = 2 Then
                If aszJsonFields(1).Trim().Equals("") Then
                    aszJsonFields = Nothing
                    aszJsonFields = {sz_TagKey.Trim()}
                End If
            End If

            objJson = JObject.Parse(sz_JsonContent)

            For Each szJsonField As String In aszJsonFields
                If iJsonElement < aszJsonFields.Length() Then
                    If objJson(szJsonField) Is Nothing Then
                        Exit For
                    Else
                        objJson = JObject.Parse(Convert.ToString(objJson(szJsonField)))
                    End If
                Else
                    szValue = objJson(szJsonField)
                    Exit For
                End If
                iJsonElement = iJsonElement + 1
            Next
        Catch ex As Exception
            'Log here
        Finally
            objJson = Nothing
        End Try
        Return szValue
    End Function

    'Constructor
    Public Sub New(ByRef o_LoggerII As LoggerII.CLoggerII)
        Try
            objLoggerII = o_LoggerII

            objHTTP = New CHTTP(objLoggerII)

            objTxnProc = New TxnProc(objLoggerII)
            objTxnProcOB = New TxnProcOB(objLoggerII)

            objCommon = New Common(objTxnProc, objTxnProcOB, objLoggerII)   'Modfied  24 Nov 2013, added parameters objTxnProc and objTxnProcOB

            objHash = New Hash(objLoggerII, objCommon)                      'Modified 11 Apr 2014, moved from above to here
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Destructor - explicitly called
    Public Sub Dispose() Implements System.IDisposable.Dispose
        ' Calling GC.SuppressFinalize in Dispose is an important optimization to ensure resources released promptly.
        If Not objCommon Is Nothing Then objCommon = Nothing
        If Not objTxnProc Is Nothing Then objTxnProc = Nothing
        If Not objTxnProcOB Is Nothing Then objTxnProcOB = Nothing 'Added, 25 Nov 2013
        If Not objHash Is Nothing Then objHash = Nothing
        If Not objHTTP Is Nothing Then objHTTP = Nothing

        GC.SuppressFinalize(Me)
    End Sub
End Class
