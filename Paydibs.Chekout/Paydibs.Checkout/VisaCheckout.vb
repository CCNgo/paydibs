﻿Imports System
Imports System.IO
Imports System.Data
Imports System.Web
Imports LoggerII
Imports System.Security.Cryptography
Imports System.Net
Imports System.Threading

Public Class VisaCheckout
    Implements IDisposable

    Private objLoggerII As LoggerII.CLoggerII
    Private objHash As Hash
    Private szDBConnStr As String = ""
    Private objCommon As Common
    Private objTxnProc As TxnProc
    Private objTxnProcOB As TxnProcOB
    Private stHostInfo2 As Common.STHost
    Private stPayInfo2 As Common.STPayInfo
    Private Server As System.Web.HttpServerUtility = System.Web.HttpContext.Current.Server

    Public Function VCCheckout(ByRef st_HostInfo As Common.STHost, ByVal st_PayInfo As Common.STPayInfo, ByVal sz_ReqStr As String) As String
        Dim szVCStr As String = ""
        Dim szEncryptReqStr As String = ""

        Try
            st_PayInfo.szIssuingBank = "VisaCheckout"

            If True <> objCommon.bGetHostInfo(st_HostInfo, st_PayInfo) Then
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + "> Error Get Host: " + st_PayInfo.szErrDesc)
            End If

            If True <> objTxnProc.bGetTerminalEx2(st_HostInfo, st_PayInfo) Then 'If True <> objCommon.bGetTerminal(st_HostInfo, st_PayInfo) Then
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + "> Error Get Terminal: HostID(" + st_HostInfo.iHostID.ToString() + ") with CurrencyCode(" + st_PayInfo.szCurrencyCode + ") Not Support") '+ st_PayInfo.szErrDesc)

                Return szVCStr
            End If
            'APIKey
            st_HostInfo.szSendKey = objHash.szDecrypt3DES(st_HostInfo.szSendKey, Common.C_3DESAPPKEY, Common.C_3DESAPPVECTOR)
            szEncryptReqStr = objHash.szComputeSHA512Hash(Common.C_3DESAPPKEY + Server.UrlDecode(sz_ReqStr).Replace("+", " "))

            szVCStr = sz_ReqStr + "&VCID=" + szEncryptReqStr

        Catch ex As Exception
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
               DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "VCCheckout() exception: " + ex.ToString)
        End Try

        Return szVCStr
    End Function

    Public Function VCUpdatePayment(ByVal st_HostInfo As Common.STHost, ByVal st_PayInfo As Common.STPayInfo) As Boolean
        Dim bReturn As Boolean = False
        Dim szQueryString As String = ""
        Dim iRet As Integer = -1
        Dim szResStr As String = ""

        Try
            'To avoid overwrite origin st_HostInfo & st_PayInfo which process payment
            stHostInfo2 = st_HostInfo
            stPayInfo2 = st_PayInfo

            stPayInfo2.szIssuingBank = "VisaCheckout"

            If True <> objTxnProc.bGetHostInfo(stHostInfo2, stPayInfo2) Then
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                stPayInfo2.szMerchantID + stPayInfo2.szMerchantTxnID + " > VisaCheckout() > " + stPayInfo2.szErrDesc)
                Return False
            End If

            If True <> objTxnProc.bGetTerminalEx2(stHostInfo2, stPayInfo2) Then 'If True <> objTxnProc.bGetTerminal(stHostInfo2, stPayInfo2) Then
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                stPayInfo2.szMerchantID + stPayInfo2.szMerchantTxnID + " > VisaCheckout() > " + stPayInfo2.szErrDesc)
                Return False
            End If

            szQueryString = Server.UrlEncode("apikey=") + objHash.szDecrypt3DES(stHostInfo2.szSendKey, Common.C_3DESAPPKEY, Common.C_3DESAPPVECTOR)

            iRet = iHTTPutWithRecv(stHostInfo2.szAcknowledgementURL + stPayInfo2.szParam1, szQueryString, stPayInfo2.iHttpTimeoutSeconds, szResStr, "", objHash.szDecrypt3DES(stHostInfo2.szSecretKey, Common.C_3DESAPPKEY, Common.C_3DESAPPVECTOR), stHostInfo2.szProtocol)

            'If (0 = iRet) Then
            bReturn = True
            'End If

        Catch ex As Exception
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
               DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "VCUpdatePayment() exception: " + ex.ToString)
        End Try

        Return bReturn

    End Function

    Public Function iHTTPutWithRecv(ByVal sz_URL As String, ByVal sz_HTTPString As String, ByVal i_Timeout As Integer, ByRef sz_HTTPResponse As String, ByVal sz_LogString As String, ByVal sz_SharedKey As String, Optional ByVal sz_SecurityProtocol As String = "TLS") As Integer
        Dim iRet As Integer = -1
        Dim objHttpWebRequest As HttpWebRequest
        Dim objHTTPWebResponse As HttpWebResponse
        'Dim objStreamWriter As StreamWriter
        Dim objStreamReader As StreamReader
        'Dim sbInStream As System.Text.StringBuilder
        Dim iStartTickCount As Integer
        Dim iTimeout As Integer
        Dim iResCode As Integer

        Dim szUNIXUTC As String = ""
        Dim szResourcePath As String = ""
        Dim szReqBody As String = ""
        Dim szSHA256Hash As String = ""
        Dim szXPayToken As String = ""
        Dim btReqBody As Byte() = Nothing
        Dim objWriter

        Try
            '======================================== Sets HttpWebRequest properties ====================================

            szUNIXUTC = DateDiff("s", "01/01/1970 00:00:00", DateTime.UtcNow).ToString()
            szResourcePath = sz_URL.Substring(sz_URL.IndexOf("payment"), sz_URL.Length() - sz_URL.IndexOf("payment"))

            'update payment. Param1 = Callid
            szReqBody = "<?xml version=""1.0"" encoding=""UTF-8""?><p:updatePaymentInfoRequest xmlns:p=""http://www.visa.com/vme/walletservices/external/payment"" xmlns:p1=""http://www.visa.com/vme/walletservices/external/common"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:schemaLocation=""http://www.visa.com/vme/walletservices/external/payment payment_mgmt.xsd "">" +
                    "<p:payInfo><p:payTransId>" + stPayInfo2.szTxnID + "</p:payTransId><p:eventType>Capture</p:eventType><p:eventStatus>Success</p:eventStatus><p:currencyCode>" + stPayInfo2.szCurrencyCode + "</p:currencyCode>" +
                    "<p:total>" + stPayInfo2.szTxnAmount + "</p:total><p:reason>Payment Success</p:reason></p:payInfo></p:updatePaymentInfoRequest>"

            btReqBody = Encoding.UTF8.GetBytes(szReqBody)

            szSHA256Hash = objHash.szComputeSHA256Hash(sz_SharedKey + szUNIXUTC + szResourcePath + Server.UrlDecode(sz_HTTPString) + szReqBody)

            szXPayToken = "x:" + szUNIXUTC + ":" + szSHA256Hash
            'sbInStream = New System.Text.StringBuilder
            'sbInStream.Append(szReqBody) '(sz_HTTPString)
            iTimeout = i_Timeout * 1000 'converts input timeout param from second to milliseconds

            objHttpWebRequest = WebRequest.Create(sz_URL + "?" + Server.UrlDecode(sz_HTTPString)) 'Posting URL
            objHttpWebRequest.Method = "PUT"
            objHttpWebRequest.Accept = "application/xml"
            objHttpWebRequest.ContentType = "application/xml"
            objHttpWebRequest.Headers.Add("x-pay-token", szXPayToken)
            objHttpWebRequest.ContentLength = btReqBody.Length 'sbInStream.Length         ' Must be set b4 retrieving stream for sending data
            objHttpWebRequest.KeepAlive = False
            objHttpWebRequest.Timeout = iTimeout                        '(in milliseconds)

            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Timeout period(" + iTimeout.ToString() + " ms) Protocol(" + sz_SecurityProtocol + ") Posting HTTP data " + sz_LogString + " to " + sz_URL)

            'Ignore invalid server certificates or else may encounter WebException with e.Status = WebExceptionStatus.TrustFailure
            'When the CertificatePolicy property is set to an ICertificatePolicy interface instance, the ServicePointManager
            'uses the certificate policy defined in that instance instead of the default certificate policy.
            'The default certificate policy allows valid certificates, as well as valid certificates that have expired.

            'ServicePointManager.CertificatePolicy = New SecPolicy

            'Added, 19 Jun 2015
            If (sz_SecurityProtocol.Trim() <> "") Then
                Select Case sz_SecurityProtocol.Trim().ToUpper()
                    Case "SSL3"
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3
                    Case "TLS12"
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
                    Case "TLS11"
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11
                    Case "TLS"
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                    Case Else
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3
                End Select

                ' allows for validation of SSL conversations
                ServicePointManager.ServerCertificateValidationCallback = Function() True   'C#: delegate { return true }
            End If

            iStartTickCount = System.Environment.TickCount()


            '=========================================== Post request ==================================================
            '- StreamWriter constructor creates a StreamWriter with UTF-8 encoding whose GetPreamble method returns an
            'empty byte array. The BaseStream property is initialized using the stream parameter (i.e. objHttpWebRequest.GetRequestStream)
            '- GetRequestStream method returns a stream to use to send data for the HttpWebRequest. Once the Stream
            'instance has been returned, you can send data with the HttpWebRequest by using the Stream.Write method.
            'NOTE:    Must set the value of the ContentLength property before retrieving the stream.
            'CAUTION: Must call the Stream.Close method to close the stream and release the connection for reuse.
            '         Failure to close the stream will cause application to run out of connections.
            objWriter = objHttpWebRequest.GetRequestStream()
            objWriter.Write(btReqBody, 0, btReqBody.Length)   ' Sends data
            objWriter.Close()

            'objStreamWriter = New StreamWriter(objHttpWebRequest.GetRequestStream())
            'objStreamWriter.Write(btReqBody) '(sbInStream.ToString)  ' Sends data
            'objStreamWriter.Close() ' Closes the stream and release the connection for reuse to avoid running out of connections

            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Finished posting HTTP data")

            '============================================= Get response =================================================
            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Getting HTTP response....")

            '- GetResponse method returns a WebResponse instance containing the response from the Internet resource.
            'The actual instance returned is an instance of HttpWebResponse, and can be typecast to that class to access
            'HTTP-specific properties. When POST method is used, must get the request stream, write the data to be posted,
            'and close the stream. This method blocks waiting for content to post; If there is no time-out set and
            'content is not provided, application will block indefinitely.

            objHTTPWebResponse = objHttpWebRequest.GetResponse()

            If (objHttpWebRequest.HaveResponse And (objHTTPWebResponse.StatusCode = HttpStatusCode.OK Or objHTTPWebResponse.StatusCode = HttpStatusCode.Created)) Then 'Modified 711, 29 Mar 2016, 711 successful txn returns as created
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "HTTP response received.")

                objStreamReader = New StreamReader(objHTTPWebResponse.GetResponseStream)

                '================ Implements Timeout for getting HTTP reply from host <START> ====================
                Dim objRespReader As RespReader
                Dim oThreadStart As ThreadStart
                Dim oThread As Thread
                Dim iElapsedTime As Integer

                objRespReader = New RespReader
                objRespReader.SetLogger(objLoggerII)
                objRespReader.SetReader(objStreamReader)

                oThreadStart = New ThreadStart(AddressOf objRespReader.WaitForResp)
                oThread = New Thread(oThreadStart)
                oThread.Start()

                'iStartTickCount = System.Environment.TickCount()
                iElapsedTime = System.Environment.TickCount() - iStartTickCount

                While ((iElapsedTime < iTimeout) And Not objRespReader.IsCompleted())
                    System.Threading.Thread.Sleep(50)
                    iElapsedTime = System.Environment.TickCount() - iStartTickCount
                End While
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp Elapsed Time(ms): " & iElapsedTime.ToString())

                If (iElapsedTime >= iTimeout) Then
                    ' Timeout
                    iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                    objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp Timeout, iRet(" + iRet.ToString() + ")")
                Else
                    'Completed AND No Timeout. Could be got reply or exception
                    If (True = objRespReader.IsSucceeded()) Then
                        'Received reply within timeout period
                        sz_HTTPResponse = objRespReader.GetRespStream()

                        If ("" = sz_HTTPResponse) Then  'Added  4 jun 2014. Firefly FF
                            iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp Response received with empty string. Set to Timeout.")
                        End If
                    Else
                        'Exception within timeout period
                        iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp exception within timeout period, iRet(" + iRet.ToString() + ")")
                    End If
                End If

                oThread.Abort()
                oThreadStart = Nothing
                oThread = Nothing
                'objRespReader.Dispose()
                objRespReader = Nothing

                'Releases the resources of the response stream
                'NOTE: Failure to close the stream will cause application to run out of connections.
                objStreamReader.Close()
                If Not objStreamReader Is Nothing Then objStreamReader = Nothing 'Added on 1 Mar 2009
                '================== Implements Timeout for getting HTTP reply from host <END> =====================
            Else
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "ERROR: Response was not received.")
            End If

            'Releases the resources of the response
            objHTTPWebResponse.Close()

            If (-1 = iRet) Then 'Modified  4 Jun 2014, added checking of (-1=iRet), Firefly FF
                iRet = 0
            End If

        Catch webEx As WebException
            Try
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException: " + webEx.ToString)

                iRet = Common.ERROR_CODE.E_COM_URL_UNREACHABLE

                If webEx.Status = WebExceptionStatus.Timeout Then
                    iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                    objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException: Timeout")
                End If

                If webEx.Status = WebExceptionStatus.ProtocolError Then
                    objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Response Status Code :" + CStr(CType(webEx.Response, HttpWebResponse).StatusCode))

                    'Re: HttpStatusCode Enumeration
                    iResCode = CInt(CType(webEx.Response, HttpWebResponse).StatusCode)
                    If iResCode = 400 Then
                        iRet = Common.ERROR_CODE.E_COM_BAD_REQUEST
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Bad Request could not be understood by the server.")
                    ElseIf iResCode = 401 Then
                        iRet = Common.ERROR_CODE.E_COM_UNAUTHORIZED_URL
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Unauthorized, the requested resource requires authentication.")
                    ElseIf iResCode = 403 Then
                        iRet = Common.ERROR_CODE.E_COM_FORBIDDEN_URL
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Forbidden, the server refuses to fulfill the request.")
                    ElseIf iResCode = 404 Then
                        iRet = Common.ERROR_CODE.E_COM_URL_UNREACHABLE
                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Not Found, the requested resource does not exist on the server.")
                    End If
                End If
            Catch exp As Exception
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Exception: " + exp.ToString)
            End Try

        Catch ioEx As IOException
            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "IO Exception: " + ioEx.ToString)
            iRet = Common.ERROR_CODE.E_COM_FILE_ERROR

        Catch ex As Exception
            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Exception: " + ex.ToString)
            iRet = Common.ERROR_CODE.E_COMMON_UNKNOWN_ERROR

        Finally
            'Commented  15 Aug 2013
            'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "iRet: " + CStr(iRet))
            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Update VCO done for GatewayTxnID [" + stPayInfo2.szTxnID + "] with status " + sz_HTTPResponse)
        End Try

        Return iRet
    End Function

    Public Sub New(ByRef o_LoggerII As LoggerII.CLoggerII)
        Try
            objLoggerII = o_LoggerII
            objTxnProc = New TxnProc(objLoggerII)
            objTxnProcOB = New TxnProcOB(objLoggerII)
            objCommon = New Common(objTxnProc, objTxnProcOB, objLoggerII)
            objHash = New Hash(objLoggerII)

            'szDBConnStr = ConfigurationManager.AppSettings("DBString2") 'Modified 11 Oct 2017, use DBString2 encrypted by non card key
            szDBConnStr = ConfigurationManager.AppSettings("PGDBString") 'Modified 11 Oct 2017, use DBString2 encrypted by non card key

            'Added, 26 Jul 2013, cater for encrypted database string
            If (InStr(szDBConnStr.ToLower(), "uid=") <= 0) Then
                szDBConnStr = objHash.szDecrypt3DES(szDBConnStr, Common.C_3DESAPPKEY2, Common.C_3DESAPPVECTOR)
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#Region "IDisposable Support"
    Private disposedValue As Boolean ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: dispose managed state (managed objects).
            End If

            ' TODO: free unmanaged resources (unmanaged objects) and override Finalize() below.
            ' TODO: set large fields to null.
        End If
        Me.disposedValue = True
    End Sub

    ' TODO: override Finalize() only if Dispose(ByVal disposing As Boolean) above has code to free unmanaged resources.
    'Protected Overrides Sub Finalize()
    '    ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
    '    Dispose(False)
    '    MyBase.Finalize()
    'End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        If Not objCommon Is Nothing Then objCommon = Nothing
        If Not objTxnProc Is Nothing Then objTxnProc = Nothing
        If Not objTxnProcOB Is Nothing Then objTxnProcOB = Nothing 'Added, 25 Nov 2013
        If Not objHash Is Nothing Then objHash = Nothing

        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
