'Imports Common
Imports LoggerII
Imports System.IO
Imports System
Imports System.Data
Imports System.Security.Cryptography
Imports System.Threading
Imports System.Text.RegularExpressions
Imports System.Web      ' HttpContext for Current.Response.Redirect, Current.Server.MapPath; HttpServerUtility/HttpUtility for Server.UrlEncode
Imports System.Web.Mail ' MailMessage

'This class handles MCash responses for TxnType "PAY" only.

'***********************************************************************************************************************
''' THIS PAGE IS DATAFEED PAGE REQUEST FOR MCASH WALLET. IT'S RETURN WALLET TRANSACTION DETAILS
''' TO DDGW. NO REPLY TO MERCHANT NEED. REPLY MERCHANT WILL BE PERFORM BY ANOTHER PAGE RESPONSE_MCASH.ASPX.
'***********************************************************************************************************************
Public Class respmcash_s2s
    Inherits System.Web.UI.Page
    'This MCash response page is apply to Regional MCash
    Public Const C_ISSUING_BANK = "MCASH"
    Public Const C_TXN_TYPE = "PAY"
    Public Const C_PYMT_METHOD = "WA" 'MCASH WALLET
    Public g_szHTML As String = ""
    Public objLoggerII As LoggerII.CLoggerII

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private objCommon As Common
    Private objTxnProcOB As TxnProcOB
    Private objTxnProc As TxnProc
    Private objHash As Hash
    Private objResponse As Response

    Private stHostInfo As Common.STHost
    Private stPayInfo As Common.STPayInfo

    Private szLogPath As String = ""
    Private iLogLevel As String = ""
    Private szReplyDateTime As String = ""

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim bGetHostInfo As Boolean = False

        Try
            'Get Host's reply date and time
            szReplyDateTime = System.DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss.fff")

            'Set to No Cache to make this page expired when user clicks the "back" button on browser
            Response.Cache.SetNoStore()

            'Get database connection string from Web.config file
            'szDBConnStr = ConfigurationSettings.AppSettings("DBString") 'Commented 14 Aug 2014, 

            If Not Page.IsPostBack Then
                'Initialization of HostInfo and PayInfo structures
                objCommon.InitHostInfo(stHostInfo)
                objCommon.InitPayInfo(stPayInfo)

                stPayInfo.szIssuingBank = C_ISSUING_BANK
                stPayInfo.szTxnType = C_TXN_TYPE
                stPayInfo.szPymtMethod = C_PYMT_METHOD 'MCASH WALLET
                stPayInfo.iHttpTimeoutSeconds = Convert.ToInt32(ConfigurationManager.AppSettings("HttpTimeoutSeconds"))

                'Get Host information - HostID, HostTemplate, NeedReplyAcknowledgement, TxnStatusActionID, HostReplyMethod, OSPymtCode
                bGetHostInfo = objTxnProcOB.bGetHostInfo(stHostInfo, stPayInfo) 'MCASH WALLET
                If (bGetHostInfo) Then
                    'Load waiting page based on LanguageCode, that's why have to get GatewayTxnID from Host first,
                    'then, look for the original request's LanguageCode then only can load the respective waiting page
                    'in bVerifyResTxn() inside bResponseMain()

                    If True <> bResponseMain() Then
                        If (stPayInfo.iTxnStatus <> Common.TXN_STATUS.LATE_HOST_REPLY And stPayInfo.iTxnStatus <> Common.TXN_STATUS.INVALID_HOST_IP And stPayInfo.iTxnStatus <> Common.TXN_STATUS.INVALID_HOST_REPLY_GATEWAYTXNID) Then
                            'Return failed VALID response to merchant
                            'ResponseErrHandler()

                            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                   DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                   stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > Invalid response received!")
                        End If
                    End If

                    'Good/Failed/Pending response already returned to merchant in bResponseMain()
                    'Error response also returned to merchant as above
                    'Now, only inserts the respective response with the final TxnStatus into database.
                    'Good/Failed/Error responses are considered as finalized and can be deleted from Request table and inserted into Response table.
                    'Pending txns that need retry need to stay in Request table for Retry Service to process accordingly.

                    If (stPayInfo.iTxnStatus <> Common.TXN_STATUS.LATE_HOST_REPLY And stPayInfo.iTxnStatus <> Common.TXN_STATUS.INVALID_HOST_IP And stPayInfo.iTxnStatus <> Common.TXN_STATUS.INVALID_HOST_REPLY_GATEWAYTXNID) Then
                        'Action 3 (Retry query host); Action 4 (Retry confirm booking)
                        'Txns that belong to these two types of action do not need to be inserted into Response table yet,
                        'because once the spInsTxnResp stored procedure is called, the original request will be deleted from
                        'Request table. These txns that need retry should stay in Request table for Retry Service to process.

                        If ((stPayInfo.iAction <> 3) And (stPayInfo.iAction <> 4)) Then
                            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                            stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > Inserting TxnResp. GatewayTxnID[" + stPayInfo.szTxnID + "] MerchantTxnStatus[" + stPayInfo.iMerchantTxnStatus.ToString() + "] TxnStatus[" + stPayInfo.iTxnStatus.ToString() + "] TxnState[" + stPayInfo.iTxnState.ToString() + "] RespMesg[" + stPayInfo.szTxnMsg + "].")

                            If (stPayInfo.szTxnID <> "" And stPayInfo.szTxnAmount <> "" And (stPayInfo.szHostTxnStatus <> "" Or stPayInfo.szRespCode <> "")) Then
                                If True <> objTxnProcOB.bInsertTxnResp(stPayInfo, stHostInfo) Then
                                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                    stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > Error: " + stPayInfo.szErrDesc)
                                Else
                                    g_szHTML = "OK" 'print in html page to acknowledge MCash received datafeed

                                    ' update IPG..TB_PayTxnRef
                                    If True <> objTxnProc.bUpdatePayTxnRef(stPayInfo, stHostInfo) Then
                                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                        stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > Error: " + stPayInfo.szErrDesc)
                                    End If
                                End If

                                'S2Scallback
                                objResponse.bMerchantCallBackResp(stPayInfo, stHostInfo)
                            Else
                                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                " > GatewayTxnID(" + stPayInfo.szTxnID + ")/TxnAmount(" + stPayInfo.szTxnAmount + ")/HostTxnStatus(" + stPayInfo.szHostTxnStatus + ")/RespCode(" + stPayInfo.szRespCode + ")/OrderNumber(" + stPayInfo.szMerchantOrdID + ") are empty from response. Bypass InsertTxnResp.")
                            End If
                        End If
                    End If
                Else
                    'For this scenario, most probably Datebase error, the Retry Service will query the host for status
                    'once database is up.
                    stPayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_PENDING
                    stPayInfo.iTxnStatus = Common.TXN_STATUS.TXN_PENDING
                    stPayInfo.szTxnMsg = Common.C_INVALID_HOST_902
                    stPayInfo.szErrDesc = "> Failed to get Host Info for " + stPayInfo.szIssuingBank + " " + stPayInfo.szTxnType + " response"

                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    "Error: " + stPayInfo.szErrDesc)

                    'bResponseMain() is not run, therefore, unable to get other reply values.
                    'Merchant will only get TxnType, IssuingBank, TxnState, TxnStatus, RespMesg
                    'Without MerchantPymtID, the reply is meaningless for the merchant. Therefore, no need reply.
                    'ResponseErrHandler()
                End If
            End If
        Catch ex As Exception
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            " > " + C_ISSUING_BANK + " response Page_Load() Exception: " + ex.Message)
        End Try
    End Sub

    '********************************************************************************************
    ' Class Name:		Response_MCash_s2s
    ' Function Name:	ResponseErrHandler
    ' Function Type:	NONE
    ' Parameter:        
    ' Description:		Error handler
    ' History:			18 Nov 2008
    '********************************************************************************************
    Private Sub ResponseErrHandler()
        Dim szHTTPString As String = ""
        Dim iRet As Integer = -1

        Try
            'Generates response hash value
            'stPayInfo.szHashValue = objHash.szGetSaleResHash(stPayInfo)
            stPayInfo.szHashValue2 = objHash.szGetSaleResHash2(stPayInfo)

            'Generates HTTP string to be replied to merchant
            szHTTPString = objResponse.szGetReplyMerchantHTTPString(stPayInfo, stHostInfo)

            'Send HTTP string reply to merchant using the same method used by Host
            'added st_HostInfo, MCASH redirect resp query
            If (stPayInfo.szMerchantReturnURL <> "") Then
                iRet = objResponse.iHTTPSend(stHostInfo.iHostReplyMethod, szHTTPString, stPayInfo.szMerchantReturnURL, stPayInfo, stHostInfo, g_szHTML)
                If (iRet <> 0) Then
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > Failed to send error reply iRet(" + CStr(iRet) + ") " + szHTTPString + " to merchant URL " + stPayInfo.szMerchantReturnURL)
                Else
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > Error reply to merchant sent.")
                End If
            Else
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > MerchantReturnURL empty. Bypass sending error reply " + szHTTPString + " to merchant")
            End If

            'Another alternative - NOTE: If MerchantReturnURL contains "?" and param, can not use this alternative
            'coz it does not support additional params in the error template.
            'objResponse.iLoadErrUI(stPayInfo.szMerchantReturnURL, stPayInfo, g_szHTML)
        Catch ex As Exception
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            " > ResponseErrHandler() Exception: " + ex.Message)
        End Try
    End Sub

    '********************************************************************************************
    ' Class Name:		Response_MCash_s2s
    ' Function Name:	bResponseMain
    ' Function Type:	Boolean.  Return False if error.
    ' Parameter:		NONE
    ' Description:		A main function to get the response info and handle the process
    ' History:			28 Oct 2008
    '********************************************************************************************
    Private Function bResponseMain() As Boolean
        Dim bReturn = True

        Try
            'Log one empty line for easier reference for new response transaction
            'objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
            '                DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "") 

            'Log all form's response fields
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            C_ISSUING_BANK + " S2S Response: " + szGetAllHTTPVal())

            'Get Response data
            If True <> objResponse.bGetData(stPayInfo, stHostInfo) Then
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                "> Error1_1 GetData: " + stPayInfo.szErrDesc)

                ' stPayInfo.szTxnMsg specified in bGetData
                stPayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                stPayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                stPayInfo.iTxnState = Common.TXN_STATE.ABORT

                'NOTE: May not be able to be inserted into Response table if mandatory fields are missing due to failure in bGetData()
                Return False
            End If

            'Ensure GatewayTxnID is passed back by Host before proceed. GatewayTxnID is important for reconciliation.
            If (stPayInfo.szTxnID = "") Then
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                "> Error1_2 bResponseMain(): Missing GatewayTxnID from Host's reply.")

                stPayInfo.szTxnMsg = Common.C_INVALID_HOST_REPLY_2907
                stPayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                stPayInfo.iTxnStatus = Common.TXN_STATUS.INVALID_HOST_REPLY
                stPayInfo.iTxnState = Common.TXN_STATE.ABORT

                'NOTE: May not be able to be inserted into Response table since GatewayTxnID is missing.
                Return False
            End If

            If True <> bProcessSaleRes(stPayInfo, stHostInfo, szReplyDateTime, g_szHTML) Then
                Return False
            End If

        Catch ex As Exception
            bReturn = False

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bResponseMain() Exception: " + ex.Message)
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		Response_MCash_s2s
    ' Function Name:	GetAllHTTPVal
    ' Function Type:	Boolean.  Return False if error.
    ' Parameter:		NONE
    ' Description:		Get all form's or query string's values
    ' History:			28 Oct 2008
    '********************************************************************************************
    Public Function szGetAllHTTPVal() As String
        Dim szFormValues As String
        Dim szTempKey As String
        Dim szTempVal As String

        Dim iCount As Integer = 0
        Dim iLoop As Integer = 0

        szFormValues = "[User IP: " + Request.UserHostAddress + "] "

        iCount = Request.Form.Count

        If (iCount > 0) Then
            For iLoop = 1 To iCount
                szTempKey = ""
                szTempKey = Request.Form.GetKey(iLoop - 1)

                szFormValues = szFormValues + szTempKey + "="

                szTempVal = ""
                szTempVal = Server.UrlDecode(Request.Form.Get(iLoop - 1))

                'Mask CardPAN
                If ((szTempKey <> "")) Then
                    If (szTempKey.ToLower = "cardpan") Then
                        If szTempVal.Length > 10 Then
                            szFormValues = szFormValues + szTempVal.Substring(0, 6) + "".PadRight(szTempVal.Length - 6 - 4, "X") + szTempVal.Substring(szTempVal.Length - 4, 4) + "&"
                        Else
                            szFormValues = szFormValues + "".PadRight(szTempVal.Length, "X") + "&"
                        End If
                    Else
                        If (iLoop = iCount) Then
                            szFormValues = szFormValues + szTempVal
                        Else
                            szFormValues = szFormValues + szTempVal + "&"
                        End If
                    End If
                End If
            Next iLoop
        Else
            iCount = Request.QueryString.Count

            For iLoop = 1 To iCount
                szTempKey = ""
                szTempKey = Request.QueryString.GetKey(iLoop - 1)

                szFormValues = szFormValues + szTempKey + "="

                szTempVal = ""
                szTempVal = Server.UrlDecode(Request.QueryString.Get(iLoop - 1))

                'Mask CardPAN
                If ((szTempKey <> "")) Then
                    If (szTempKey.ToLower = "cardpan") Then
                        If szTempVal.Length > 10 Then
                            szFormValues = szFormValues + szTempVal.Substring(0, 6) + "".PadRight(szTempVal.Length - 6 - 4, "X") + szTempVal.Substring(szTempVal.Length - 4, 4) + "&"
                        Else
                            szFormValues = szFormValues + "".PadRight(szTempVal.Length, "X") + "&"
                        End If
                    Else
                        If (iLoop = iCount) Then
                            szFormValues = szFormValues + szTempVal
                        Else
                            szFormValues = szFormValues + szTempVal + "&"
                        End If
                    End If
                End If
            Next iLoop
        End If

        Return szFormValues
    End Function

    '********************************************************************************************
    ' Class Name:		Response
    ' Function Name:	bProcessSaleRes
    ' Function Type:	Boolean.  Return False if error.
    ' Parameter:		
    ' Description:		Process payment response
    ' History:			29 Oct 2008
    '********************************************************************************************
    Public Function bProcessSaleRes(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost, ByVal sz_ReplyDateTime As String, ByRef sz_HTML As String) As Boolean
        Dim bReturn As Boolean = True
        Dim szQueryString As String = ""
        Dim iMesgSet As Integer = -1
        Dim iMesgNum As Integer = -1
        Dim szMesg As String = ""           ' Added on 18 Apr 2010
        Dim iRet As Integer = -1

        Dim objHash As Hash
        Dim objMailMesg As New MailMessage
        Dim objMail As SmtpMail
        Dim szBody As String = ""

        objHash = New Hash(objLoggerII)
        st_PayInfo.szHashValue = ""

        Try
            '- Verify Host return IP addresses (optional)
            '- Check late response - exceeding 2 hours from request datetime since booking PO will be released after 2 hours
            '- Update Txn State
            '- Verify response data with request data
            '- Reply Acknowledgement to Host if needed
            '- Get merchant password
            '- Get HostTxnStatus's action
            If True <> bInitResponse(st_PayInfo, st_HostInfo, sz_ReplyDateTime, sz_HTML) Then 'MCASH WALLET
                Return False
            End If

            'Process Host reply based on action type configured in database according to Host TxnStatus
            If (1 = st_PayInfo.iAction) Then                'Payment approved by Host
                'Need to confirm booking for this merchant
                'If (1 = st_PayInfo.iNeedAddOSPymt) Then
                'OpenSkies Integration - (1) DisplayReservation based on PNR/Order Number (2) Confirm booking if necessary
                'The above operation will be performed by UASOAPClient.dll's ConfirmBooking interface function
                'DirectDebit Gateway will call it via a wrapper class library (managed code written in Visual C++ .NET), i.e. SOAPClient.dll
                'Input params : OrderNumber, MerchantPymtID, GatewayTxnID, HostTxnID, 
                '               IssuingBank (for Remarks in ChangeReservation), TxnAmount, CurrencyCode, OpenSkies Payment Code by Host
                'Output params: iRet, MesgSet and MesgNum will be available if iRet is 2 or 5
                'iRet:  - 0		Success (Payment approved and booking is successfully confirmed for Merchant ID that needs Gateway to confirm booking)
                '       - 1		(Failed - OS Amount mismatched with request's TxnAmount)
                '	    - 2		(Failed - OS returned error other than "Comm Error")
                '	    - 3		(Failed - OS pymt status "OK" but DistributionOption not "E")
                '	    - 4		(Pending - OS pymt status "PN" after a maximum of 2 retries ==> caller to retry ConfirmBooking)
                '	    - 5		(Unconfirmed - Comm error with Reservation System after a maximum of 2 retries ==> caller to retry ConfirmBooking)
                '	    - -1	(Failed - UASOAPClient.dll system error) - Exception, HTTP error, HTTP timeout, Exception,
                '               Invalid input params, Unknown OpenSkies Payment Status, XML parsing error
                '       - -2	(Failed - SOAPClient.dll system error)
                '- If "PN", retry DisplayReservation 2 times, then if still "PN" at the 2nd time, return error, DD GW should flag
                '- this in database, so that the Service will retry and if up to 1 hour 45 minutes still like this then exception report will be generated (similar to Mandiri Exception Report), show status
                '- the respective AirAsia team can check based on status.
                '- If "WA/ER" - warning/error, retry DisplayReservation until timeout or max number of times? exception report, show status
                '- If "OK", check DistributionOption, is it "E", if yes return success, if no, call ChangeReservation BUT put
                '- empty for <payments> tag, e.g. <payments />, AND put <DistributionOption> to "E" again, after that,
                '- Call DisplayReservation - maybe no need to handle this, just put in Exception Report, as suggested by Bill, 5 Nov 2008

                ' Added on 9 Apr 2009, to cater for certain host which does not return anything but just status
                ' to pass empty fields value checking by UASOAPClient.dll for field(5th), which is BankRefNo.
                'If ("" = st_PayInfo.szHostTxnID) Then
                'st_PayInfo.szHostTxnID = "-"
                'End If

                'ConfirmBooking Start
                '' Added on 3 Aug 2009, base currency booking confirmation enhancement
                ''Base Currency is DIFFERENT from Payment Currency, then, confirm booking using Base Currency and Base Amount
                'If (st_PayInfo.szBaseCurrencyCode <> "" And (st_PayInfo.szBaseCurrencyCode.ToUpper() <> st_PayInfo.szCurrencyCode.ToUpper())) Then
                '    'Added on 14 Apr 2010
                '    If ("NS" = st_PayInfo.szGatewayID.ToUpper()) Then
                '        Dim WS As New NSWebService.Service1 ' Added on 18 Apr 2010

                '        WS.Url = ConfigurationSettings.AppSettings("NSWebSvcURL")

                '        objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), _
                '                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
                '                        " > Confirming Booking")

                '        iRet = WS.ConfirmBooking(st_PayInfo.szOrderNumber, st_PayInfo.szTxnID, st_PayInfo.szMerchantTxnID, st_PayInfo.szHostTxnID, st_PayInfo.szBaseTxnAmount, st_PayInfo.szBaseCurrencyCode, st_HostInfo.szOSPymtCode, st_PayInfo.szIssuingBank, szMesg)

                '        objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), _
                '                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
                '                        " > Base Currency ConfirmBooking OrderNumber(" + st_PayInfo.szOrderNumber + ") GatewayTxnID(" + st_PayInfo.szTxnID + ") MerchantPymtID(" + st_PayInfo.szMerchantTxnID + ") HostTxnID(" + st_PayInfo.szHostTxnID + ") BaseTxnAmount(" + st_PayInfo.szBaseTxnAmount + ") BaseCurrencyCode(" + st_PayInfo.szBaseCurrencyCode + ") TxnAmount(" + st_PayInfo.szTxnAmount + ") CurrencyCode(" + st_PayInfo.szCurrencyCode + ") OSPymtCode(" + st_HostInfo.szOSPymtCode + ") IssuingBank(" + st_PayInfo.szIssuingBank + ")")
                '    Else
                '        iRet = objSOAP.ConfirmBooking(ConfigurationSettings.AppSettings("UASOAPClientPath"), st_PayInfo.szOrderNumber, st_PayInfo.szTxnID, st_PayInfo.szMerchantTxnID, st_PayInfo.szHostTxnID, st_PayInfo.szBaseTxnAmount, st_PayInfo.szBaseCurrencyCode, st_HostInfo.szOSPymtCode, st_PayInfo.szIssuingBank, iMesgSet, iMesgNum)

                '        objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), _
                '                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
                '                        " > Base Currency ConfirmBooking UASOAPClientDLL(" + ConfigurationSettings.AppSettings("UASOAPClientPath") + ") OrderNumber(" + st_PayInfo.szOrderNumber + ") GatewayTxnID(" + st_PayInfo.szTxnID + ") MerchantPymtID(" + st_PayInfo.szMerchantTxnID + ") HostTxnID(" + st_PayInfo.szHostTxnID + ") BaseTxnAmount(" + st_PayInfo.szBaseTxnAmount + ") BaseCurrencyCode(" + st_PayInfo.szBaseCurrencyCode + ") TxnAmount(" + st_PayInfo.szTxnAmount + ") CurrencyCode(" + st_PayInfo.szCurrencyCode + ") OSPymtCode(" + st_HostInfo.szOSPymtCode + ") IssuingBank(" + st_PayInfo.szIssuingBank + ")")
                '    End If
                'Else
                '    'Base Currency is EMPTY or Base Currency is SAME as Payment Currency, then, confirm booking using Payment Currency and Payment Amount
                '    'Added on 14 Apr 2010
                '    If ("NS" = st_PayInfo.szGatewayID.ToUpper()) Then
                '        Dim WS As New NSWebService.Service1 ' Added on 18 Apr 2010

                '        WS.Url = ConfigurationSettings.AppSettings("NSWebSvcURL")

                '        objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), _
                '                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
                '                        " > Confirming Booking")

                '        iRet = WS.ConfirmBooking(st_PayInfo.szOrderNumber, st_PayInfo.szTxnID, st_PayInfo.szMerchantTxnID, st_PayInfo.szHostTxnID, st_PayInfo.szTxnAmount, st_PayInfo.szCurrencyCode, st_HostInfo.szOSPymtCode, st_PayInfo.szIssuingBank, szMesg)

                '        objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), _
                '                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
                '                        " > ConfirmBooking OrderNumber(" + st_PayInfo.szOrderNumber + ") GatewayTxnID(" + st_PayInfo.szTxnID + ") MerchantPymtID(" + st_PayInfo.szMerchantTxnID + ") HostTxnID(" + st_PayInfo.szHostTxnID + ") TxnAmount(" + st_PayInfo.szTxnAmount + ") CurrencyCode(" + st_PayInfo.szCurrencyCode + ") OSPymtCode(" + st_HostInfo.szOSPymtCode + ") IssuingBank(" + st_PayInfo.szIssuingBank + ")")
                '    Else
                '        iRet = objSOAP.ConfirmBooking(ConfigurationSettings.AppSettings("UASOAPClientPath"), st_PayInfo.szOrderNumber, st_PayInfo.szTxnID, st_PayInfo.szMerchantTxnID, st_PayInfo.szHostTxnID, st_PayInfo.szTxnAmount, st_PayInfo.szCurrencyCode, st_HostInfo.szOSPymtCode, st_PayInfo.szIssuingBank, iMesgSet, iMesgNum)

                '        objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), _
                '                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
                '                        " > ConfirmBooking UASOAPClientDLL(" + ConfigurationSettings.AppSettings("UASOAPClientPath") + ") OrderNumber(" + st_PayInfo.szOrderNumber + ") GatewayTxnID(" + st_PayInfo.szTxnID + ") MerchantPymtID(" + st_PayInfo.szMerchantTxnID + ") HostTxnID(" + st_PayInfo.szHostTxnID + ") TxnAmount(" + st_PayInfo.szTxnAmount + ") CurrencyCode(" + st_PayInfo.szCurrencyCode + ") OSPymtCode(" + st_HostInfo.szOSPymtCode + ") IssuingBank(" + st_PayInfo.szIssuingBank + ")")
                '    End If
                'End If

                'objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), _
                '                DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
                '                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > ConfirmBooking iRet(" + iRet.ToString() + ") MesgSet(" + iMesgSet.ToString() + ") MesgNum(" + iMesgNum.ToString() + ") Mesg(" + szMesg + ")")

                'st_PayInfo.iOSRet = iRet
                'st_PayInfo.iErrSet = iMesgSet
                'st_PayInfo.iErrNum = iMesgNum

                ''Map iRet to TxnStatus returned to merchant.
                'Select Case iRet
                '    Case 0  'Success (Payment approved and booking is successfully confirmed for Service ID that needs Gateway to confirm booking)
                '        st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_SUCCESS
                '        st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_SUCCESS
                '        st_PayInfo.szTxnMsg = Common.C_TXN_SUCCESS_0
                '        st_PayInfo.iTxnState = Common.TXN_STATE.COMMIT

                '        ' Modified on 13 Apr 2009, changed status to 3 but no need retry
                '    Case 1  'Failed - Now: Booking unconfirmed due to partial payment not allowed; Previously: OS Amount mismatched with request's TxnAmount
                '        'st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                '        'st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_FAILED
                '        st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_PENDING_CONFIRM_BOOKING   'Merchant will extend booking based on this status
                '        st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_PENDING_CONFIRM_BOOKING
                '        st_PayInfo.szTxnMsg = "Failed: Payment approved but booking unconfirmed due to partial payment not allowed"
                '        st_PayInfo.iTxnState = Common.TXN_STATE.RECV_CONFIRM_OS
                '        'st_PayInfo.iTxnState = Common.TXN_STATE.COMMIT_FAILED

                '        ' Modified on 13 Apr 2009, changed status to 3 but no need retry
                '    Case 2  'Failed - OS returned error other than "Comm Error"
                '        'st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                '        'st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_FAILED
                '        st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_PENDING_CONFIRM_BOOKING   'Merchant will extend booking based on this status
                '        st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_PENDING_CONFIRM_BOOKING
                '        st_PayInfo.szTxnMsg = "Booking Extended: Payment approved but OS returned error MsgSet[" + iMesgSet.ToString() + "] MsgNum [" + iMesgNum.ToString() + "]"
                '        st_PayInfo.iTxnState = Common.TXN_STATE.RECV_CONFIRM_OS
                '        'st_PayInfo.iTxnState = Common.TXN_STATE.COMMIT_FAILED

                '        ' Modified on 13 Apr 2009, changed status to 0 (Success) but no need retry
                '    Case 3  'Failed - OS pymt status "OK" but DistributionOption not "E"
                '        'st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                '        'st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_FAILED
                '        st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_SUCCESS
                '        st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_SUCCESS
                '        st_PayInfo.szTxnMsg = "Partial Success: Payment approved, booking confirmed but DistributionOption not changed to E"
                '        st_PayInfo.iTxnState = Common.TXN_STATE.COMMIT
                '        'st_PayInfo.iTxnState = Common.TXN_STATE.COMMIT_FAILED

                '    Case 4  'Pending - OS pymt status "PN" after a maximum of 2 retries ==> Retry service to retry ConfirmBooking
                '        st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_PENDING_CONFIRM_BOOKING   'Merchant will extend booking based on this status
                '        st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_PENDING_CONFIRM_BOOKING
                '        st_PayInfo.szTxnMsg = "Pending: Payment approved but booking is in Pending status"
                '        st_PayInfo.iTxnState = Common.TXN_STATE.RECV_CONFIRM_OS

                '        st_PayInfo.iAction = 4  'Retry ConfirmBooking

                '    Case 5  'Unconfirmed - Comm error with Reservation System after a maximum of 2 retries ==> Retry service to retry ConfirmBooking
                '        st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_PENDING_CONFIRM_BOOKING   'Merchant will extend booking based on this status
                '        st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_PENDING_CONFIRM_BOOKING
                '        st_PayInfo.szTxnMsg = "Unknown: Payment approved but booking status is unknown due to communication error returned by the booking system"
                '        st_PayInfo.iTxnState = Common.TXN_STATE.RECV_CONFIRM_OS

                '        st_PayInfo.iAction = 4  'Retry ConfirmBooking

                '    Case -1 'Failed - UASOAPClient.dll's system error => Exception, HTTP error, HTTP timeout, Invalid input params, Unknown OpenSkies Payment Status, XML parsing error
                '        st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_PENDING_CONFIRM_BOOKING
                '        st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_PENDING_CONFIRM_BOOKING
                '        st_PayInfo.szTxnMsg = "Unknown: Payment approved but encountered internal system error 1, could be HTTP timeout Booking Engine"
                '        st_PayInfo.iTxnState = Common.TXN_STATE.SENT_CONFIRM_OS

                '        st_PayInfo.iAction = 4  'Retry ConfirmBooking

                '        ' Added -9 and Action 4, 13 Apr 2009, for SOAPClient.dll's exception, do not retry, will cause W3SVC die!!
                '    Case -2, -9 'Failed - SOAPClient.dll's system error
                '        'st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                '        'st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_FAILED
                '        st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_PENDING_CONFIRM_BOOKING
                '        st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_PENDING_CONFIRM_BOOKING
                '        st_PayInfo.szTxnMsg = "Unknown: Payment approved but encountered internal system error 2"
                '        st_PayInfo.iTxnState = Common.TXN_STATE.SENT_CONFIRM_OS
                '        'st_PayInfo.iTxnState = Common.TXN_STATE.COMMIT_FAILED

                '        'st_PayInfo.iAction = 4  'Retry ConfirmBooking

                '    Case Else   'Unknown iRet value
                '        st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                '        st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_FAILED
                '        st_PayInfo.szTxnMsg = "Failed: Payment approved but received unknown status from internal component"
                '        st_PayInfo.iTxnState = Common.TXN_STATE.COMMIT_FAILED
                'End Select
                'ConfirmBooking End

                'Else    'Success (Payment approved, no need to confirm booking for this merchant)
                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_SUCCESS
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_SUCCESS
                st_PayInfo.szTxnMsg = Common.C_TXN_SUCCESS_0
                st_PayInfo.iTxnState = Common.TXN_STATE.COMMIT
                'End If

            ElseIf (2 = st_PayInfo.iAction) Then    'Failed by Host
                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.szTxnMsg = Common.C_TXN_FAILED_1
                st_PayInfo.iTxnState = Common.TXN_STATE.COMMIT_FAILED

            ElseIf (3 = st_PayInfo.iAction) Then    'Unconfirmed status from Host, need to query Host until getting reply or timeout
                'Update Request table with Action 3 for Retry Service to pick up and query the respective Host
                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_PENDING
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_PENDING
                st_PayInfo.szTxnMsg = "Host returned not processed/unknown status, pending query Host"
                st_PayInfo.iTxnState = Common.TXN_STATE.RECV_FROM_HOST

            ElseIf (5 = st_PayInfo.iAction Or 6 = st_PayInfo.iAction) Then    'Due to query account invalid password, need to send email alert and stop querying Host by setting QueryFlag to OFF until QueryFlag is manually set to ON
                'Overwrite Action from 5 to 3 and then update Request table with Action 3 for Retry Service to pick up
                'and query the respective Host
                'Requested by AirAsia's Bill on 8 June 2009, added a setting to enable and disable the Query Flag OFF feature
                'This is due to Maybank's response code 20 is inaccurate, it not only indicates invalid query account's
                'password provided by merchant; It also indicates internal network error.
                If (5 = st_PayInfo.iAction) Then
                    st_HostInfo.iQueryFlag = 0
                    If True <> objTxnProcOB.bUpdateHostInfo(st_PayInfo, st_HostInfo) Then
                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                        st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " " + st_PayInfo.szErrDesc)
                    End If
                End If

                'Sends email alert (http://support.microsoft.com/kb/555287)
                objMailMesg.From = ConfigurationManager.AppSettings("EmailSendFrom") 'Modified by 14 Aug 2014, MCash
                objMailMesg.To = ConfigurationManager.AppSettings("EmailSendTo") 'Modified by 14 Aug 2014, MCash
                objMailMesg.Cc = ConfigurationManager.AppSettings("EmailSendCc") 'Modified by 14 Aug 2014, MCash
                objMailMesg.BodyFormat = MailFormat.Text
                objMailMesg.Priority = MailPriority.High

                If (5 = st_PayInfo.iAction) Then
                    objMailMesg.Subject = "Alert - " + st_PayInfo.szIssuingBank + " Query Flag OFF"

                    szBody = "Dear all," + vbNewLine + vbNewLine
                    szBody = szBody + "This is an auto generated email. Please be informed that Query Flag for " + st_PayInfo.szIssuingBank + " is currently set to OFF due to Query rejected with error Unauthorized Query UserName and Password." + vbNewLine + vbNewLine
                    szBody = szBody + "Please liaise with the bank to check this Query account accordingly. After that, kindly inform Support Team to re-configure the Query UserName and Password and then turn ON Query Flag." + vbNewLine + vbNewLine
                    szBody = szBody + "Thank you," + vbNewLine
                    szBody = szBody + "Payment Gateway" + vbNewLine
                Else
                    If ("" <> st_PayInfo.szHostTxnStatus) Then
                        objMailMesg.Subject = "Alert - " + st_PayInfo.szIssuingBank + " Response Code " + st_PayInfo.szHostTxnStatus
                    Else
                        objMailMesg.Subject = "Alert - " + st_PayInfo.szIssuingBank + " Response Code " + st_PayInfo.szRespCode
                    End If

                    szBody = "Dear all," + vbNewLine + vbNewLine
                    szBody = szBody + "This is an auto generated email. Please be informed that Response Code as stated in email title for " + st_PayInfo.szIssuingBank + " was received." + vbNewLine + vbNewLine
                    szBody = szBody + "Please liaise with the bank to check this accordingly. After that, kindly inform Support Team for further actions." + vbNewLine + vbNewLine
                    szBody = szBody + "Thank you," + vbNewLine
                    szBody = szBody + "Payment Gateway" + vbNewLine
                End If

                objMailMesg.Body = szBody

                objMailMesg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusing", 2) 'Send the message using the network (SMTP over the network).
                objMailMesg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpusessl", False)  'Use SSL for the connection (True or False)
                objMailMesg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout", 1200)
                objMailMesg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpserver", ConfigurationManager.AppSettings("EmailServer")) 'Modified by 14 Aug 2014, MCas
                objMailMesg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpserverport", ConfigurationManager.AppSettings("EmailPort")) 'Modified by 14 Aug 2014, MCash
                objMailMesg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", 1)    'Use basic clear-text authentication, have to provide user name and password through sendusername and sendpassword fields
                objMailMesg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", ConfigurationManager.AppSettings("EmailUserName")) 'Modified by 14 Aug 2014, MCash
                objMailMesg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", ConfigurationManager.AppSettings("EmailPassword")) 'Modified by 14 Aug 2014, MCash

                objMail.SmtpServer = ConfigurationManager.AppSettings("EmailServer") 'Modified by 14 Aug 2014, MCash
                objMail.Send(objMailMesg)

                st_PayInfo.iAction = 3

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_TIMEOUT_HOST_RETURN
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_PENDING
                st_PayInfo.szTxnMsg = "Pending query Host"
                st_PayInfo.iTxnState = Common.TXN_STATE.RECV_FROM_HOST

                'Added by Jazz, 20 Jun 2011, for Mandiri, to cater for Sale response status "Pending" received from Host, need to send Reversal to Host
            ElseIf (7 = st_PayInfo.iAction) Then
                'Update Request table with Action 7 for Retry Service to pick up and send Reversal to Host
                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_HOST_UNPROCESSED
                st_PayInfo.szTxnMsg = "Host returned Sale pending status, pending Reversal to Host"
                st_PayInfo.iTxnState = Common.TXN_STATE.RECV_FROM_HOST
            End If

            'Update Request table with the respective Txn State and Action so that Retry Service can process
            'accordingly for both Action 3 (retry query host), Action 4 (retry confirm booking) and Action 7 (retry Reversal)
            'At the same time, also update Request table with BankRefNo (st_PayInfo.szHostTxnID), OSPymtCode (st_HostInfo.szOSPymtCode),
            'OSRet (iRet), ErrSet (iMesgSet), ErrNum (iMesgNum)
            'Added (7 = st_PayInfo.iAction) by Jazz, 20 Jun 2011, for Mandiri, to cater for Sale response status "Pending" received from Host, need to send Reversal to Host
            If ((3 = st_PayInfo.iAction) Or (4 = st_PayInfo.iAction) Or (7 = st_PayInfo.iAction)) Then
                If True <> objTxnProcOB.bUpdateTxnStatus(st_PayInfo, st_HostInfo) Then 'Modified by 14 Aug 2014, MCash
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " " + st_PayInfo.szErrDesc)
                End If
            End If

            ''******************************************************************
            '' NO REPLY TO MERCHANT NEEDED
            ''******************************************************************
            'Send Gateway response to merchant using the same Host's reply method
            ''iSendReply2Merchant(st_PayInfo, st_HostInfo, sz_HTML)  
            'Even If (True = bRet), no need update DB for TxnState SENT_REPLY_CLIENT so that TxnState like COMMIT/COMMIT_FAILED won't be overwritten.

        Catch ex As Exception
            bReturn = False

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Exception: " + ex.Message)

            'Added on 12 Mar 2011, to solve "The operation has timed out" exception after calling NewSkies Web Service ConfirmBooking()
            'Without adding this, even though payment was approved but when encountered operation timeout during booking confirmation,
            'DDGW will return TxnStatus -1 and TxnState 2 to merchant that requires DDGW to confirm booking
            If (1 = st_PayInfo.iAction And 1 = st_PayInfo.iNeedAddOSPymt) Then  'Approved by bank and need booking confirmation
                bReturn = True

                If ("THE OPERATION HAS TIMED OUT" = ex.Message.ToUpper.Trim()) Then
                    st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_PENDING_CONFIRM_BOOKING   'Merchant will extend booking based on this status
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_PENDING_CONFIRM_BOOKING
                    st_PayInfo.szTxnMsg = "Pending: Payment approved but booking status is unknown due to communication error with booking system, retry in progress"
                    st_PayInfo.iTxnState = Common.TXN_STATE.SENT_CONFIRM_OS

                    st_PayInfo.iAction = 4  'Retry ConfirmBooking
                Else
                    st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_PENDING
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_PENDING
                    st_PayInfo.szTxnMsg = "Pending: Status unknown, query Host in progress"
                    st_PayInfo.iTxnState = Common.TXN_STATE.RECV_FROM_HOST

                    st_PayInfo.iAction = 3  'Retry query bank
                End If

                'Update Request table with the respective Txn State and Action so that Retry Service can retry confirm booking
                'At the same time, also update Request table with BankRefNo (st_PayInfo.szHostTxnID), OSPymtCode (st_HostInfo.szOSPymtCode)
                If True <> objTxnProcOB.bUpdateTxnStatus(st_PayInfo, st_HostInfo) Then 'Modified by 14 Aug 2014, MCash
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " " + st_PayInfo.szErrDesc)
                End If

                ''******************************************************************
                '' NO REPLY TO MERCHANT NEEDED
                ''******************************************************************
                'Send Gateway response to merchant using the same Host's reply method
                'iSendReply2Merchant(st_PayInfo, st_HostInfo, sz_HTML)
                'Even If (True = bRet), no need update DB for TxnState SENT_REPLY_CLIENT so that TxnState like COMMIT/COMMIT_FAILED won't be overwritten.
            End If
        End Try

        If Not objMailMesg Is Nothing Then objMailMesg = Nothing

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		Response
    ' Function Name:	bInitResponse
    ' Function Type:	Boolean.  Return False if error.
    ' Parameter:		
    ' Description:		Update Txn State, Verify response data with request data, Reply Acknowledgement to Host if needed,
    '                   Get merchant password, Get HostTxnStatus's action
    ' History:			31 Oct 2008
    '********************************************************************************************
    Private Function bInitResponse(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost, ByVal sz_ReplyDateTime As String, ByRef sz_HTML As String) As Boolean 'Modified by 14 Aug 2014, MCash 
        Dim bReturn As Boolean = True
        Dim aszReturnIPAddresses() As String
        Dim iLoop As Integer = 0
        Dim iRet As Integer = -1
        Dim bMatch As Boolean = False

        Try
            'Verify Host return IP addresses (optional)
            If (st_HostInfo.szReturnIPAddresses <> "") Then
                aszReturnIPAddresses = Split(st_HostInfo.szReturnIPAddresses, ";")
                bMatch = False
                For iLoop = 0 To aszReturnIPAddresses.GetUpperBound(0)
                    If (aszReturnIPAddresses(iLoop) = HttpContext.Current.Request.UserHostAddress) Then
                        bMatch = True

                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                        "GatewayTxnID(" + st_PayInfo.szTxnID + ") Host reply IP matched " + aszReturnIPAddresses(iLoop))
                    End If
                Next

                If (False = bMatch) Then
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.INVALID_HOST_IP

                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    "GatewayTxnID(" + st_PayInfo.szTxnID + ") Host reply IP (" + HttpContext.Current.Request.UserHostAddress + ") does not match IP list (" + st_HostInfo.szReturnIPAddresses + ")")
                    Return False
                End If
            End If

            'Added checking of DDGW Aggregator on 3 Apr 2010, no need check late Host response for DDGW Aggregator
            If ("" = ConfigurationManager.AppSettings("HostsListTemplate")) Then 'Modified by 14 Aug 2014, MCash 
                ' Check late reply
                ' (2 hours x 60 minutes x 60 seconds = 7200 seconds) - PO 'll be released after 2 hrs from its reserved time.
                ' So, host reply came back 2 hours after the request was registered in Gateway 'll be treated as late reply and 'll be discarded.
                If True <> objTxnProcOB.bCheckLateResp(st_PayInfo, sz_ReplyDateTime) Then
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    "GatewayTxnID(" + st_PayInfo.szTxnID + ") " + st_PayInfo.szErrDesc)

                    Return False
                End If
            End If

            'Commented on 14 Jan 2009, to prevent invalid host/hackers' reply from affecting existing TxnStatus,
            'especially for hosts that does not support hashing in host reply, e.g. CIMBClicks
            'st_PayInfo.iTxnStatus = Common.TXN_STATUS.PROCESSING_RESP
            'st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_PENDING
            'st_PayInfo.iTxnState = Common.TXN_STATE.RECV_FROM_HOST

            ''Update Txn State
            'If (st_PayInfo.szHostTxnID <> "" Or st_PayInfo.szAuthCode <> "") Then
            '    If True <> objTxnProc.bUpdateTxnStatus(st_PayInfo, st_HostInfo, sz_DBConnStr) Then
            '        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
            '                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
            '                        " > GatewayTxnID(" + st_PayInfo.szTxnID + ") " + st_PayInfo.szErrDesc)

            '        Return False
            '    End If
            'Else
            '    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
            '                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
            '                    " > GatewayTxnID(" + st_PayInfo.szTxnID + ") BankRefNo is emtpy, bypass UpdateTxnStatus.")
            'End If

            'Verify response data with request data stored in database.
            'At the same time, retrieve some request data from database based on Gateway TxnID to return to client's server.
            If True <> objResponse.bVerifyResTxn(st_PayInfo, st_HostInfo, sz_HTML) Then 'Modified by 14 Aug 2014, MCash 
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                "GatewayTxnID(" + st_PayInfo.szTxnID + ") > bVerifyResTxn failed: " + st_PayInfo.szErrDesc)

                Return False
            End If

            ' ''****************************************************************************************************************
            ' ''NO REPLY TO MERCHANT NEEDED - START 
            ' ''****************************************************************************************************************
            ' '' ''Get Merchant Password, NeedAddOSPymt flag to determine whether need to confirm booking with OpenSkies if pymt approved for txns received from this merchant
            '' ''If True <> objTxnProc.bGetMerchant(st_PayInfo, sz_DBConnStr) Then
            '' ''    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
            '' ''                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
            '' ''                    st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > bInitResponse() " + st_PayInfo.szErrDesc)
            '' ''    Return False
            '' ''End If

            ' '' ''Added on 11 Apr 2010, decrypt Merchant Password if its length > 8 characters
            '' ''If (st_PayInfo.szMerchantPassword.Length() > 8) Then
            '' ''    st_PayInfo.szMerchantPassword = objHash.szDecrypt3DES(st_PayInfo.szMerchantPassword, st_HostInfo.sz3DESAppKey, st_HostInfo.sz3DESAppVector)
            '' ''End If

            ' '' ''Moved bGetTerminal from below to here on 30 Oct 2009
            '' ''If True <> objTxnProc.bGetTerminal(st_HostInfo, st_PayInfo, sz_DBConnStr) Then
            '' ''    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
            '' ''                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
            '' ''                    st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > bInitResponse() " + st_PayInfo.szErrDesc)
            '' ''    Return False
            '' ''End If
            ' '' ''Added on 26 Feb 2009, to support query host if HostReplyMethod is 1 or 4 and hashing is not involved.
            ' '' ''HostReplyMethod 4 indicates host reply returned through pop-up bank payment window (Child Window) triggers
            ' '' ''merchant's window (Parent Window) to redirect to Gateway.
            ' '' ''HostReplyMethod 1 indicates non pop-up bank payment window (Single Window) redirects host reply
            ' '' ''to Gateway.
            ' '' ''If there is no hashing involved in the host reply, need to send query to host to get the actual status
            ' '' ''because host reply returned through user's browser redirection can not be trusted.
            ' '' ''========================================================================================================
            ' '' ''29 May 2009, NOTE: Must set ReturnKey to dummy key "1" or any value if HostReplyMethod is 1 or 4 but
            ' '' ''DOES NOT need to query Host because Host reply is secure enough although no hashing but got encryption
            ' '' ''========================================================================================================
            '' ''If (1 = st_HostInfo.iHostReplyMethod Or 4 = st_HostInfo.iHostReplyMethod) Then
            '' ''    If (st_HostInfo.szReturnKey = "") Then
            '' ''        If (1 = st_HostInfo.iQueryFlag) Then
            '' ''            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
            '' ''                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
            '' ''                            st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > " + st_PayInfo.szIssuingBank + " redirected reply (no hashing) to DDGW, Query Host required")

            '' ''            'Moved bGetTerminal above on 30 Oct 2009
            '' ''            'If True <> objTxnProc.bGetTerminal(st_HostInfo, st_PayInfo, sz_DBConnStr) Then
            '' ''            '    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
            '' ''            '                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
            '' ''            '                    st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > bInitResponse() " + st_PayInfo.szErrDesc)
            '' ''            '    Return False
            '' ''            'End If

            '' ''            ' Query host for payment status ONLY for host that does not support hash but uses redirect method
            '' ''            ' to reply Gateway
            '' ''            st_PayInfo.szHostTxnStatus = "" ' Overwrites Host Txn Status/Response Code with blank before getting it from Host Query reply
            '' ''            st_PayInfo.szRespCode = ""
            '' ''            ' Overwritting TxnType from "PAY" to "QUERY" is to ensure the respective action in bGetTxnStatusAction() can be retrieved
            '' ''            st_PayInfo.szTxnType = "QUERY"
            '' ''            iRet = iSendQueryHost(st_PayInfo, st_HostInfo)
            '' ''            If (iRet <> 0) Then 'Modified on 7 Mar 2011, HLB, changed < 0 to <> 0 becoz iRet=44(Failed to send);iRet=5(Timeout);<0:Exception
            '' ''                st_PayInfo.szTxnType = "PAY"

            '' ''                ' Sets Action to 3 so that response_[Host] class's Page_Load() will not insert this txn into
            '' ''                ' Response table in order for Retry Service to retry query Host.
            '' ''                st_PayInfo.iAction = 3

            '' ''                st_PayInfo.szTxnMsg = "Failed to send Query request/Timeout Query response from " + st_PayInfo.szIssuingBank
            '' ''                st_PayInfo.iMerchantTxnStatus = objCommon.TXN_STATUS.TXN_TIMEOUT_HOST_RETURN
            '' ''                st_PayInfo.iTxnStatus = objCommon.TXN_STATUS.TXN_PENDING
            '' ''                st_PayInfo.iTxnState = objCommon.TXN_STATE.SENT_QUERY_HOST

            '' ''                'Added on 5 Apr 2010
            '' ''                If True <> objTxnProc.bUpdateTxnStatus(st_PayInfo, st_HostInfo, sz_DBConnStr) Then
            '' ''                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
            '' ''                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
            '' ''                                    st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " " + st_PayInfo.szErrDesc)
            '' ''                End If

            '' ''                'Modified by 7 Mar 2011, changed False to True, so that DDGW will not return failed status
            '' ''                'if bank response is approved but failed to send query request/timeout query response to banks,
            '' ''                'for banks that do not support security in their payment response message
            '' ''                Return True
            '' ''                'Return False
            '' ''            End If
            '' ''        Else
            '' ''            'Sets Action to 5 if QueryFlag is OFF so that email alert can be sent and the necessary actions
            '' ''            'can be conducted. Once everything is settled, QueryFlag needs to be set to ON.
            '' ''            st_PayInfo.iAction = 5

            '' ''            Return True
            '' ''        End If
            '' ''    End If
            '' ''    'ElseIf (2 = st_HostInfo.iHostReplyMethod) Then  'For SCIB
            '' ''    '    If (False = bMatch) Then
            '' ''    '        st_PayInfo.iTxnStatus = Common.TXN_STATUS.INVALID_HOST_IP

            '' ''    '        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
            '' ''    '                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
            '' ''    '                        "GatewayTxnID(" + st_PayInfo.szTxnID + ") Host reply IP (" + HttpContext.Current.Request.UserHostAddress + ") does not match IP list (" + st_HostInfo.szReturnIPAddresses + ")")
            '' ''    '        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
            '' ''    '                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "QueryURL-> " + st_HostInfo.szQueryURL)
            '' ''    '        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
            '' ''    '                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "secretkey-> " + st_HostInfo.szSecretKey)
            '' ''    '        'if ("" = CheckQueryURL And "" = ) 
            '' ''    '        Return False
            '' ''    '        'ENd If
            '' ''    '    End If
            '' ''End If


            ' '' ''NOTE: bGetHostInfo has been called in Page_Load() b4 calling this function, therefore,
            ' '' ''st_HostInfo.iHostID needed for bGetTerminal() and bGetTxnStatusAction() already populated with the respective HostID value

            ' '' ''Check whether to send reply acknowledgement to Host
            '' ''If (1 = st_HostInfo.iNeedReplyAcknowledgement) Then
            '' ''    'Get Terminal and Channel information - CfgFile, ExtraURL
            '' ''    If (st_HostInfo.szAcknowledgementURL = "") Then
            '' ''        If True <> objTxnProc.bGetTerminal(st_HostInfo, st_PayInfo, sz_DBConnStr) Then
            '' ''            st_PayInfo.szTxnType = "PAY"

            '' ''            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
            '' ''                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
            '' ''                            st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > bInitResponse() " + st_PayInfo.szErrDesc)
            '' ''            Return False
            '' ''        End If
            '' ''    End If

            '' ''    iSendACK(st_PayInfo, st_HostInfo)
            '' ''End If
            ' ''****************************************************************************************************************
            ' ''NO REPLY TO MERCHANT NEEDED - END 
            ' ''****************************************************************************************************************

            'Get Host's TxnStatus's action
            If True <> objTxnProcOB.bGetTxnStatusAction(st_HostInfo, st_PayInfo) Then
                st_PayInfo.szTxnType = "PAY"

                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > bInitResponse() " + st_PayInfo.szErrDesc)
                Return False
            End If

            st_PayInfo.szTxnType = "PAY"

        Catch ex As Exception
            bReturn = False

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > bInitResponse() Exception: " + ex.Message)
        End Try

        Return bReturn
    End Function

    Public Sub New()
        szLogPath = ConfigurationManager.AppSettings("LogPath") ' MCash
        iLogLevel = Convert.ToInt32(ConfigurationManager.AppSettings("LogLevel")) ' MCash

        '2 Apr 2010, moved InitLogger() from down to up here, or else Hash will get empty objLoggerII and encountered exception
        InitLogger()

        objTxnProc = New TxnProc(objLoggerII)
        objCommon = New Common(objTxnProc, objLoggerII) 'MCash
        objHash = New Hash(objLoggerII)
        objTxnProcOB = New TxnProcOB(objLoggerII) 'MCash
        objResponse = New Response(objLoggerII)

    End Sub

    Public Sub InitLogger()
        If (objLoggerII Is Nothing) Then

            If (szLogPath Is Nothing) Then
                szLogPath = "C:\\OB.log"
            End If

            If (iLogLevel < 0 Or iLogLevel > 5) Then
                iLogLevel = 3
            End If

            'objLoggerII = New LoggerII.CLoggerII(
            objLoggerII = LoggerII.CLoggerII.GetInstanceX(szLogPath + "_" + C_ISSUING_BANK + ".log", iLogLevel, 255)

        End If
    End Sub

    Protected Overrides Sub Finalize()
        If Not objLoggerII Is Nothing Then
            objLoggerII.Dispose()
            objLoggerII = Nothing
        End If

        If Not objCommon Is Nothing Then objCommon = Nothing
        If Not objTxnProcOB Is Nothing Then objTxnProcOB = Nothing
        If Not objTxnProc Is Nothing Then objTxnProc = Nothing
        If Not objHash Is Nothing Then objHash = Nothing

        If Not objResponse Is Nothing Then
            objResponse.Dispose()
            objResponse = Nothing
        End If

        MyBase.Finalize()
    End Sub
End Class
