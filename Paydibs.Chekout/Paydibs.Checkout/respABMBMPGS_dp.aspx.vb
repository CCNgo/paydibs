'Imports Common
Imports LoggerII
Imports System.IO
Imports System
Imports System.Data
Imports System.Security.Cryptography
Imports System.Threading
Imports System.Text.RegularExpressions

'This class handles ambmb Credit Card responses for TxnType "PAY" only.
'Other transaction types will not be a response page like this that the bank needs to return the reply, it would be
'Credit Card Gateway sends the request and then waits for the bank reply within the same session.
'Therefore, a response page like this is not needed for other transaction types.
Public Class respABMBMPGS
    Inherits System.Web.UI.Page

    Public Const C_ISSUING_BANK = "ABMBMPGS"
    Public Const C_TXN_TYPE = "PAY"
    Public Const C_PYMT_METHOD = "CC"   'Added 24 Nov 2013
    Public g_szHTML As String = ""
    Public objLoggerII As LoggerII.CLoggerII

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private objCommon As Common
    Private objTxnProc As TxnProc
    Private objHash As Hash
    Private objResponse As Response
    Private objHTTP As CHTTP

    Private stHostInfo As Common.STHost
    Private stPayInfo As Common.STPayInfo

    Private szLogPath As String = ""
    Private iLogLevel As String = ""
    Private szReplyDateTime As String = ""

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim bGetHostInfo As Boolean = False

        Try
            'Get Host's reply date and time
            szReplyDateTime = System.DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss.fff")

            'Set to No Cache to make this page expired when user clicks the "back" button on browser
            Response.Cache.SetNoStore()

            If Not Page.IsPostBack Then
                'Initialization of HostInfo and PayInfo structures
                objCommon.InitHostInfo(stHostInfo)
                objCommon.InitPayInfo(stPayInfo)

                stPayInfo.szIssuingBank = C_ISSUING_BANK
                stPayInfo.szTxnType = C_TXN_TYPE
                stPayInfo.szPymtMethod = C_PYMT_METHOD  'Added 24 Nov 2013
                stPayInfo.iHttpTimeoutSeconds = Convert.ToInt32(ConfigurationManager.AppSettings("HttpTimeoutSeconds"))

                'Get Host information - HostID, HostTemplate, NeedReplyAcknowledgement, TxnStatusActionID, HostReplyMethod, OSPymtCode
                bGetHostInfo = objTxnProc.bGetHostInfo(stHostInfo, stPayInfo)
                If (bGetHostInfo) Then
                    'Load waiting page based on LanguageCode, that's why have to get GatewayTxnID from Host first,
                    'then, look for the original request's LanguageCode then only can load the respective waiting page
                    'in bVerifyResTxn() inside bResponseMain()

                    If True <> bResponseMain() Then
                        If (stPayInfo.iTxnStatus <> Common.TXN_STATUS.LATE_HOST_REPLY And stPayInfo.iTxnStatus <> Common.TXN_STATUS.INVALID_HOST_IP And stPayInfo.iTxnStatus <> Common.TXN_STATUS.INVALID_HOST_REPLY_GATEWAYTXNID) Then
                            'Return failed response to merchant
                            ResponseErrHandler()
                        End If
                    End If

                    'Good/Failed/Pending response already returned to merchant in bResponseMain()
                    'Error response also returned to merchant as above
                    'Now, only inserts the respective response with the final TxnStatus into database.
                    'Good/Failed/Error responses are considered as finalized and can be deleted from Request table and inserted into Response table.
                    'Pending txns that need retry need to stay in Request table for Retry Service to process accordingly.

                    If (stPayInfo.iTxnStatus <> Common.TXN_STATUS.LATE_HOST_REPLY And stPayInfo.iTxnStatus <> Common.TXN_STATUS.INVALID_HOST_IP And stPayInfo.iTxnStatus <> Common.TXN_STATUS.INVALID_HOST_REPLY_GATEWAYTXNID) Then
                        'Action 3 (Retry query host); Action 4 (Retry confirm booking)
                        'Txns that belong to these two types of action do not need to be inserted into Response table yet,
                        'because once the spInsTxnResp stored procedure is called, the original request will be deleted from
                        'Request table. These txns that need retry should stay in Request table for Retry Service to process.

                        If ((stPayInfo.iAction <> 3) And (stPayInfo.iAction <> 4)) Then
                            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                            stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > Insert TxnResp. GatewayTxnID[" + stPayInfo.szTxnID + "] MerchantTxnStatus[" + stPayInfo.iMerchantTxnStatus.ToString() + "] TxnStatus[" + stPayInfo.iTxnStatus.ToString() + "] TxnState[" + stPayInfo.iTxnState.ToString() + "] RespMesg[" + stPayInfo.szTxnMsg + "].")

                            If (stPayInfo.szTxnID <> "" And stPayInfo.szTxnAmount <> "" And (stPayInfo.szHostTxnStatus <> "" Or stPayInfo.szRespCode <> "")) Then 'And stPayInfo.szOrderNumber <> "") Then
                                If True <> objTxnProc.bInsertTxnResp(stPayInfo, stHostInfo) Then
                                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                    stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > Error: " + stPayInfo.szErrDesc)
                                End If
                            Else
                                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                " > GatewayTxnID(" + stPayInfo.szTxnID + ")/TxnAmount(" + stPayInfo.szTxnAmount + ")/HostTxnStatus(" + stPayInfo.szHostTxnStatus + ")/RespCode(" + stPayInfo.szRespCode + ")/OrderNumber(" + stPayInfo.szMerchantOrdID + ") are empty from response. Bypass InsertTxnResp.")
                            End If
                        End If
                    End If
                Else
                    'For this scenario, most probably Datebase error, the Retry Service will query the host for status
                    'once database is up.
                    stPayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_PENDING
                    stPayInfo.iTxnStatus = Common.TXN_STATUS.TXN_PENDING
                    stPayInfo.szTxnMsg = Common.C_INVALID_HOST_902
                    stPayInfo.szErrDesc = "> Failed to get Host Info for " + stPayInfo.szIssuingBank + " " + stPayInfo.szTxnType + " response"

                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "Error: " + stPayInfo.szErrDesc)

                    'bResponseMain() is not run, therefore, unable to get other reply values.
                    'Merchant will only get TxnType, IssuingBank, TxnState, TxnStatus, RespMesg
                    'Without MerchantPymtID, the reply is meaningless for the merchant. Therefore, no need reply.
                    'ResponseErrHandler()
                End If
            End If
        Catch ex As Exception
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            " > " + C_ISSUING_BANK + " response Page_Load() Exception: " + ex.Message)
        End Try
    End Sub

    '********************************************************************************************
    ' Class Name:		respABMBMPGS
    ' Function Name:	ResponseErrHandler
    ' Function Type:	NONE
    ' Parameter:        
    ' Description:		Error handler
    ' History:			18 Nov 2008
    '********************************************************************************************
    Private Sub ResponseErrHandler()
        Dim szHTTPString As String = ""
        Dim iRet As Integer = -1

        Try
            'Generates response hash value
            'stPayInfo.szHashValue = objHash.szGetSaleResHash(stPayInfo)
            stPayInfo.szHashValue2 = objHash.szGetSaleResHash2(stPayInfo)   'Added 2 May 2016
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "TRACE-err-" + stPayInfo.szHashValue2)
            'Generates HTTP string to be replied to merchant
            szHTTPString = objResponse.szGetReplyMerchantHTTPString(stPayInfo, stHostInfo)

            'Send HTTP string reply to merchant using the same method used by Host
            'Modified 4 Oct 2016, added st_HostInfo, KTB redirect resp query
            If (stPayInfo.szMerchantReturnURL <> "") Then
                iRet = objResponse.iHTTPSend(stHostInfo.iHostReplyMethod, szHTTPString, stPayInfo.szMerchantReturnURL, stPayInfo, stHostInfo, g_szHTML)
                If (iRet <> 0) Then
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > Failed to send error reply iRet(" + CStr(iRet) + ") " + szHTTPString + " to merchant URL " + stPayInfo.szMerchantReturnURL)
                Else
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > Error reply to merchant sent.")
                End If
            Else
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > MerchantReturnURL empty. Bypass sending error reply " + szHTTPString + " to merchant")
            End If

            If True <> objTxnProc.bGetMerchant(stPayInfo) Then
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bInitResponse() " + stPayInfo.szErrDesc)
            End If

            If (1 = stPayInfo.iAllowCallBack) Then
                If ("" <> stPayInfo.szMerchantCallbackURL) Then
                    Dim szResp As String = ""
                    Dim szCallbackStatus As String = "" 'Added, 23 Oct 2014

                    'ORIGINAL PLAN: Wait for "ok"/"OK" to stop sending response, maximum 3 times
                    'Added, 12 Nov 2014, callback response string
                    szHTTPString = objResponse.szGetReplyMerchantCallbackHTTPString(stPayInfo, stHostInfo)
                    'iRet = objHTTP.iHTTPCallbackPostWithRecv(st_PayInfo.szMerchantReturnURL, szHTTPString, st_PayInfo.iHttpTimeoutSeconds, szResp, "")

                    For t = 0 To 4 '2
                        'Modified 13 Sept 2016, support callback resp protocol
                        iRet = objHTTP.iHTTPostWithRecv(stPayInfo.szMerchantCallbackURL, szHTTPString, stPayInfo.iHttpTimeoutSeconds, szResp, "", stPayInfo.szProtocol)

                        If (0 = iRet) Then
                            szCallbackStatus = "success"

                            If ("" <> szResp.Trim()) Then
                                szCallbackStatus = szCallbackStatus + " and recv acknowledgement (" + Left(szResp.Trim(), 10) + " ....)"
                                Exit For
                            End If
                        Else
                            szCallbackStatus = "fail"
                        End If

                        'Added by OM, 16 Apr 2019
                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "Callback count [" + (t + 1).ToString + "]")
                        Thread.Sleep(50)
                    Next

                    'Modified 13 Sept 2016, added logging of protocol
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "Callback response [" + szHTTPString + "], Status [" + szCallbackStatus + "] Protocol[" + stPayInfo.szProtocol + "]")
                Else
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "MerchantCallbackURL is empty. No Callback sent.")
                End If
            End If

            'Another alternative - NOTE: If MerchantReturnURL contains "?" and param, can not use this alternative
            'coz it does not support additional params in the error template.
            'objResponse.iLoadErrUI(stPayInfo.szMerchantReturnURL, stPayInfo, g_szHTML)
        Catch ex As Exception
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(), " > ResponseErrHandler() Exception: " + ex.Message)
        End Try
    End Sub

    '********************************************************************************************
    ' Class Name:		respABMBMPGS
    ' Function Name:	bResponseMain
    ' Function Type:	Boolean.  Return False if error.
    ' Parameter:		NONE
    ' Description:		A main function to get the response info and handle the process
    ' History:			28 Oct 2008
    '********************************************************************************************
    Private Function bResponseMain() As Boolean
        Dim bReturn = True
        Dim szHashMethod As String = ""
        'Dim szPARes As String = ""
        Dim iReturn As Integer = -1
        Dim szQueryString As String = ""
        'Dim sz3DSecureId As String = ""
        Dim szHostRes As String = ""
        Dim szLogString As String = ""

        Try
            'Log all form's response fields
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(), C_ISSUING_BANK + " Resp: " + szGetAllHTTPVal())

            'Response string from processPares.php contains of [] which is not support in bParseQueryString.
            szHostRes = Server.UrlDecode(Request.Form("ACS")).Replace("[", ".").Replace("]", "")
            'RES2
            If (False = objResponse.bParseQueryString(szHostRes, stPayInfo, stHostInfo, "2")) Then
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                    stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > Failed to parse response." + " Error[" + stPayInfo.szErrDesc + "]")

                Return False
            End If

            'Assign 3DSecureID to szTxnID which is GatewayTxnID and retrive origin txn from Req table
            stPayInfo.szTxnID = stPayInfo.szHostGatewayTxnID
            If (True <> objCommon.bGetReqTxn(stPayInfo, stHostInfo)) Then
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                        stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bResponseMain() > " + stPayInfo.szErrDesc)

                stPayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                stPayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                stPayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                stPayInfo.iTxnState = Common.TXN_STATE.ABORT
                Return False
            End If

            'Verify Payer Authentication (PA)'s ECI to proceed or reject txn. 
            If (False = objTxnProc.bVerify3DSStatus(stPayInfo, "PA")) Then
                If "" = stPayInfo.szMerchantPassword Then
                    If True <> objTxnProc.bGetMerchant(stPayInfo) Then
                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bResponseMain() " + stPayInfo.szErrDesc)
                        Return False
                    End If
                End If

                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                        stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > Txn Failed at PaRes")

                stPayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                stPayInfo.iTxnStatus = Common.TXN_STATUS.DECLINED_BY_PA
                stPayInfo.szTxnMsg = stPayInfo.szIssuingBank + " " + stPayInfo.szTxnMsg
                stPayInfo.iTxnState = Common.TXN_STATE.FAILED
                stPayInfo.szHostTxnStatus = Common.TXN_STATUS.DECLINED_BY_PA    'Added for finalize txn to response table bcoz txn failed b4 payment request.
                Return False
            End If

            If True <> objTxnProc.bGetTerminal(stHostInfo, stPayInfo) Then
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bResponseMain() > " + stPayInfo.szErrDesc)

                stPayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                stPayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                stPayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                stPayInfo.iTxnState = Common.TXN_STATE.ABORT
                Return False
            End If

            'Proceed to PAY
            iReturn = objResponse.iGetQueryString(szQueryString, stHostInfo.szHostTemplate, "REQ3", "PAY", stPayInfo, stHostInfo)
            If (iReturn < 0) Then
                stPayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                stPayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                stPayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                stPayInfo.iTxnState = Common.TXN_STATE.ABORT
                Return False
            End If

            szLogString = szReplaceCardDetails(szQueryString)

            iReturn = objHTTP.iHTTPostWithRecvMPGS(stHostInfo.szURL, szQueryString, stPayInfo.iHttpTimeoutSeconds, szHostRes, szLogString, "x-www-form-urlencoded; charset=iso-8859-1",
                                                   "", stHostInfo.szMID, objHash.szDecrypt3DEStoHex(stHostInfo.szSecretKey, Common.C_3DESAPPKEY, Common.C_3DESAPPVECTOR).ToLower(), stHostInfo.szProtocol)
            If (iReturn <> 0) Then
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                    stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > Failed to send PAY request/Timeout response, iRet(" + CStr(iReturn) + ") ")

                stPayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                stPayInfo.iTxnStatus = Common.TXN_STATUS.INVALID_HOST_REPLY
                stPayInfo.szTxnMsg = stPayInfo.szIssuingBank + " Failed to send PAY request/Timeout response"
                stPayInfo.iTxnState = Common.TXN_STATE.ABORT
                Return False
            End If

            'RES3
            If (False = objResponse.bParseQueryString(szHostRes, stPayInfo, stHostInfo, "3")) Then
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                    stPayInfo.szTxnID + " > Failed to parse response." + " Error[" + stPayInfo.szErrDesc + "]")

                Return False
            End If

            'Insert/Update 3DS details
            objTxnProc.bInsUpdPayReq3DData(stPayInfo)

            'Get Response data
            'If True <> objResponse.bGetData(stPayInfo, stHostInfo, "3") Then
            '    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
            '                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "> Error1_1 GetData: " + stPayInfo.szErrDesc)

            '    ' stPayInfo.szTxnMsg specified in bGetData
            '    stPayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            '    stPayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            '    stPayInfo.iTxnState = Common.TXN_STATE.ABORT

            '    'NOTE: May not be able to be inserted into Response table if mandatory fields are missing due to failure in bGetData()
            '    Return False
            'End If

            'Ensure GatewayTxnID is passed back by Host before proceed. GatewayTxnID is important for reconciliation.
            'If (stPayInfo.szTxnID = "") Then
            '    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
            '                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "> Error1_2 bResponseMain(): Missing GatewayTxnID from Host's reply.")

            '    stPayInfo.szTxnMsg = Common.C_INVALID_HOST_REPLY_2907
            '    stPayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            '    stPayInfo.iTxnStatus = Common.TXN_STATUS.INVALID_HOST_REPLY
            '    stPayInfo.iTxnState = Common.TXN_STATE.ABORT

            '    'NOTE: May not be able to be inserted into Response table since GatewayTxnID is missing.
            '    Return False
            'End If

            'Param will be wipe off in bProcessSaleRes. Assign into temp variable and assign back after bProcessSaleRes
            Dim szParam1 As String = ""
            szParam1 = stPayInfo.szParam1

            If True <> objResponse.bProcessSaleRes(stPayInfo, stHostInfo, szReplyDateTime, g_szHTML) Then
                Return False
            End If

            stPayInfo.szParam1 = szParam1

        Catch ex As Exception
            bReturn = False

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bResponseMain() Exception: " + ex.Message)
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		respABMBMPGS
    ' Function Name:	GetAllHTTPVal
    ' Function Type:	Boolean.  Return False if error.
    ' Parameter:		NONE
    ' Description:		Get all form's or query string's values
    ' History:			28 Oct 2008
    '********************************************************************************************
    Public Function szGetAllHTTPVal() As String
        Dim szFormValues As String
        Dim szTempKey As String
        Dim szTempVal As String

        Dim iCount As Integer = 0
        Dim iLoop As Integer = 0

        'szFormValues = "[User IP: " + Request.UserHostAddress + "] "
        szFormValues = "[UserIP: " + Request.UserHostAddress + "] [UserHostName: " + Request.UserHostName + "] [UserBrowser:" + Request.UserAgent + "] [SessionID:" + HttpContext.Current.Session.SessionID.ToString() + "] "

        iCount = Request.Form.Count

        If (iCount > 0) Then
            For iLoop = 1 To iCount
                szTempKey = ""
                szTempKey = Request.Form.GetKey(iLoop - 1)

                szFormValues = szFormValues + szTempKey + "="

                szTempVal = ""
                szTempVal = Request.Form.Get(iLoop - 1)        ' szTempVal = Server.UrlDecode(Request.Form.Get(iLoop - 1))

                'Mask CardPAN
                If ((szTempKey <> "")) Then
                    If (szTempKey.ToLower = "cardpan") Then
                        If szTempVal.Length > 10 Then
                            szFormValues = szFormValues + szTempVal.Substring(0, 6) + "".PadRight(szTempVal.Length - 6 - 4, "X") + szTempVal.Substring(szTempVal.Length - 4, 4) + "&"
                        Else
                            szFormValues = szFormValues + "".PadRight(szTempVal.Length, "X") + "&"
                        End If
                    Else
                        If (iLoop = iCount) Then
                            szFormValues = szFormValues + szTempVal
                        Else
                            szFormValues = szFormValues + szTempVal + "&"
                        End If
                    End If
                End If
            Next iLoop
        Else
            iCount = Request.QueryString.Count

            For iLoop = 1 To iCount
                szTempKey = ""
                szTempKey = Request.QueryString.GetKey(iLoop - 1)

                szFormValues = szFormValues + szTempKey + "="

                szTempVal = ""
                szTempVal = Request.QueryString.Get(iLoop - 1) 'szTempVal = Server.UrlDecode(Request.QueryString.Get(iLoop - 1))

                'Mask CardPAN
                If ((szTempKey <> "")) Then
                    If (szTempKey.ToLower = "cardpan") Then
                        If szTempVal.Length > 10 Then
                            szFormValues = szFormValues + szTempVal.Substring(0, 6) + "".PadRight(szTempVal.Length - 6 - 4, "X") + szTempVal.Substring(szTempVal.Length - 4, 4) + "&"
                        Else
                            szFormValues = szFormValues + "".PadRight(szTempVal.Length, "X") + "&"
                        End If
                    Else
                        If (iLoop = iCount) Then
                            szFormValues = szFormValues + szTempVal
                        Else
                            szFormValues = szFormValues + szTempVal + "&"
                        End If
                    End If
                End If
            Next iLoop
        End If

        Return szFormValues

        'Dim szFormValues As String = ""
        'Dim szTempVal As String = ""
        'Dim resData As New Dictionary(Of String, String)()
        'Dim coll As NameValueCollection = Request.Form 'Request.QueryString
        'Dim requestItem As String() = coll.AllKeys
        'Dim treeMap As New SortedDictionary(Of String, String)(StringComparer.Ordinal)
        ''Dim szLogValues As String

        'Try
        '    For i As Integer = 0 To requestItem.Length - 1
        '        resData.Add(requestItem(i), Request.Form(requestItem(i)))
        '    Next

        '    For Each kvp As KeyValuePair(Of String, String) In resData
        '        treeMap.Add(kvp.Key, kvp.Value)
        '    Next

        '    szFormValues = "[UserIP: " + Request.UserHostAddress + "] [UserHostName: " + Request.UserHostName + "] [UserBrowser:" + Request.UserAgent + "] [SessionID:" + HttpContext.Current.Session.SessionID.ToString() + "] "

        '    Dim builder As New StringBuilder()

        '    ' order the keys
        '    For Each item As String In treeMap.Keys
        '        szFormValues = szFormValues + item + "="
        '        szTempVal = ""
        '        szTempVal = Server.UrlDecode(coll(item))

        '        'Mask CardPAN
        '        If ((item <> "")) Then
        '            Select Case item.ToLower
        '                Case "cardpan"
        '                    If szTempVal.Length > 10 Then
        '                        szFormValues = szFormValues + szTempVal.Substring(0, 6) + "".PadRight(szTempVal.Length - 6 - 4, "X") + szTempVal.Substring(szTempVal.Length - 4, 4) + "&"
        '                    Else
        '                        szFormValues = szFormValues + "".PadRight(szTempVal.Length, "X") + "&"
        '                    End If
        '                    builder.Append(item + "=" + coll(item) + "&")
        '                Case "vpc_securehash", "vpc_securehashtype"
        '                    szFormValues = szFormValues + szTempVal + "&"
        '                Case Else
        '                    szFormValues = szFormValues + szTempVal + "&"
        '                    builder.Append(item + "=" + coll(item) + "&")
        '            End Select
        '        End If
        '    Next

        '    If (szFormValues.Substring(szFormValues.Length - 1, 1) = "&") Then
        '        szFormValues = szFormValues.Substring(0, szFormValues.Length - 1)
        '    End If

        '    Dim szBuilder As String = builder.ToString()
        '    If (szBuilder.Substring(szBuilder.Length - 1, 1) = "&") Then
        '        szBuilder = szBuilder.Substring(0, szBuilder.Length - 1)
        '    End If

        '    If (szBuilder.Length > 0) Then
        '        stHostInfo.szReserved = szBuilder
        '    End If

        'Catch ex As Exception
        '    objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
        '                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
        '                    stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + "- szGetAllHTTPVal Exception: " + ex.Message)
        'End Try

        'Return szFormValues

    End Function

    Private Function szReplaceCardDetails(ByVal sz_LogString As String) As String
        Dim szSearch As String = ""
        Dim szNew As String = ""

        Try
            'Replace CardNumber
            szSearch = sz_LogString.Substring(sz_LogString.IndexOf("card.number=") + 12, sz_LogString.IndexOf(sz_LogString.IndexOf("card.number=") + 12) + 17)
            szNew = szSearch.Substring(0, 6) + "xxxxxx" + szSearch.Substring(12, 4)
            sz_LogString = sz_LogString.Replace(szSearch, szNew)

            'Replace expiry month
            szSearch = sz_LogString.Substring(sz_LogString.IndexOf("month="), sz_LogString.IndexOf(sz_LogString.IndexOf("month=")) + 9)
            'szNew = szSearch.Substring(0, 6) + "xx"
            sz_LogString = sz_LogString.Replace(szSearch, "month=xx") 'szNew)

            'Replace expire year
            szSearch = sz_LogString.Substring(sz_LogString.IndexOf("year="), sz_LogString.IndexOf(sz_LogString.IndexOf("year=")) + 8)
            'szNew = szSearch.Substring(0, 5) + "xx"
            sz_LogString = sz_LogString.Replace(szSearch, "year=xx") 'szNew)

        Catch ex As Exception
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(), stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + "- szReplaceCardDetails Exception: " + ex.Message)
        End Try

        Return sz_LogString
    End Function

    Public Sub New()
        szLogPath = ConfigurationManager.AppSettings("LogPath")
        iLogLevel = Convert.ToInt32(ConfigurationManager.AppSettings("LogLevel"))

        '2 Apr 2010, moved InitLogger() from down to up here, or else Hash will get empty objLoggerII and encountered exception
        InitLogger()

        objTxnProc = New TxnProc(objLoggerII)
        objCommon = New Common(objTxnProc, objLoggerII)
        objHash = New Hash(objLoggerII)
        objResponse = New Response(objLoggerII)
        objHTTP = New CHTTP(objLoggerII)

    End Sub

    Public Sub InitLogger()
        If (objLoggerII Is Nothing) Then

            If (szLogPath Is Nothing) Then
                szLogPath = "C:\\CC.log"
            End If

            If (iLogLevel < 0 Or iLogLevel > 5) Then
                iLogLevel = 3
            End If

            objLoggerII = LoggerII.CLoggerII.GetInstanceX(szLogPath + "_" + C_ISSUING_BANK + ".log", iLogLevel, 255)

        End If
    End Sub

    Protected Overrides Sub Finalize()
        If Not objLoggerII Is Nothing Then
            objLoggerII.Dispose()
            objLoggerII = Nothing
        End If

        If Not objCommon Is Nothing Then objCommon = Nothing
        If Not objTxnProc Is Nothing Then objTxnProc = Nothing
        If Not objHash Is Nothing Then objHash = Nothing

        If Not objResponse Is Nothing Then
            objResponse.Dispose()
            objResponse = Nothing
        End If

        If Not objHTTP Is Nothing Then objHTTP = Nothing

        MyBase.Finalize()
    End Sub
End Class
