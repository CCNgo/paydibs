﻿Imports LoggerII
Imports System.IO
Imports System
Imports System.Data
Imports System.Security.Cryptography
Imports System.Threading
Imports System.Text.RegularExpressions
Imports System.Web      ' HttpContext for Current.Response.Redirect, Current.Server.MapPath; HttpServerUtility/HttpUtility for Server.UrlEncode
Imports System.Web.Mail ' MailMessage

'This class handles EXPAY responses for TxnType "PAY" only.
'Other transaction types will not be a response page like this that the bank needs to return the reply, it would be
' OB Gateway sends the request and then waits for the bank reply within the same session.
'Therefore, a response page like this is not needed for other transaction types.

'***********************************************************************************************************************
''' THIS PAGE ONLY UPDATE TXN. NO REPLY TO MERCHANT NEED. REPLY MERCHANT WILL BE PERFORM BY ANOTHER PAGE respEXPAY_s2s.ASPX.
'***********************************************************************************************************************
Public Class respEXPAY
    Inherits System.Web.UI.Page

    Public Const C_ISSUING_BANK = "EXPAY"
    Public Const C_TXN_TYPE = "PAY2"
    Public Const C_PYMT_METHOD = "OB"
    Public g_szHTML As String = ""
    Public objLoggerII As LoggerII.CLoggerII

    Private objCommon As Common
    Private objTxnProcOB As TxnProcOB
    Private objTxnProc As TxnProc
    Private objHash As Hash
    Private objResponse As Response

    Private stHostInfo As Common.STHost
    Private stPayInfo As Common.STPayInfo

    Private szDBConnStr As String = ""
    Private szLogPath As String = ""
    Private iLogLevel As String = ""
    Private szReplyDateTime As String = ""
    Private bTimeOut As Boolean = False

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim bGetHostInfo As Boolean = False

        Try
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            " > " + C_ISSUING_BANK + "Redirect/S2s get nudge trigger")

            'Get Host's reply date and time
            szReplyDateTime = System.DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss.fff")

            'Set to No Cache to make this page expired when user clicks the "back" button on browser
            Response.Cache.SetNoStore()

            If Not Page.IsPostBack Then
                'Initialization of HostInfo and PayInfo structures
                objCommon.InitHostInfo(stHostInfo)
                objCommon.InitPayInfo(stPayInfo)

                stPayInfo.szIssuingBank = C_ISSUING_BANK
                stPayInfo.szTxnType = C_TXN_TYPE
                stPayInfo.szPymtMethod = C_PYMT_METHOD
                stPayInfo.iHttpTimeoutSeconds = Convert.ToInt32(ConfigurationManager.AppSettings("HttpTimeoutSeconds"))

                'Get Host information - HostID, HostTemplate, NeedReplyAcknowledgement, TxnStatusActionID, HostReplyMethod, OSPymtCode
                bGetHostInfo = objTxnProcOB.bGetHostInfo(stHostInfo, stPayInfo)

                If (bGetHostInfo) Then
                    If (True = bIsVerifyPayment()) Then
                        Response.ContentType = "application/json; charset=utf-8"
                        Dim JsonResp As String = bRequestMain()

                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                  DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                  stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + "  Json Response: " + JsonResp)

                        Response.Write(JsonResp)

                    Else
                        If True <> bResponseMain() Then
                            If (stPayInfo.iTxnStatus <> Common.TXN_STATUS.TXN_NOT_FOUND And stPayInfo.iTxnStatus <> Common.TXN_STATUS.LATE_HOST_REPLY And stPayInfo.iTxnStatus <> Common.TXN_STATUS.INVALID_HOST_IP And stPayInfo.iTxnStatus <> Common.TXN_STATUS.INVALID_HOST_REPLY_GATEWAYTXNID And stPayInfo.iTxnStatus <> Common.TXN_STATUS.INVALID_HOST_REPLY) Then
                                'log failed response
                                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                  DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                  stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + "  Invalid response received!")
                            End If
                        End If

                        'Good/Failed/Pending response already returned to merchant in bResponseMain()
                        'Error response also returned to merchant as above
                        'Now, only inserts the respective response with the final TxnStatus into database.
                        'Good/Failed/Error responses are considered as finalized and can be deleted from Request table and inserted into Response table.
                        'Pending txns that need retry need to stay in Request table for Retry Service to process accordingly.

                        If (stPayInfo.iTxnStatus <> Common.TXN_STATUS.TXN_NOT_FOUND And stPayInfo.iTxnStatus <> Common.TXN_STATUS.LATE_HOST_REPLY And stPayInfo.iTxnStatus <> Common.TXN_STATUS.INVALID_HOST_IP And stPayInfo.iTxnStatus <> Common.TXN_STATUS.INVALID_HOST_REPLY_GATEWAYTXNID And stPayInfo.iTxnStatus <> Common.TXN_STATUS.INVALID_HOST_REPLY) Then
                            'Action 3 (Retry query host); Action 4 (Retry confirm booking)
                            'Txns that belong to these two types of action do not need to be inserted into Response table yet,
                            'because once the spInsTxnResp stored procedure is called, the original request will be deleted from
                            'Request table. These txns that need retry should stay in Request table for Retry Service to process.

                            If ((stPayInfo.iAction <> 3) And (stPayInfo.iAction <> 4)) Then
                                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                            stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " Inserting TxnResp. GatewayTxnID[" + stPayInfo.szTxnID + "] MerchantTxnStatus[" + stPayInfo.iMerchantTxnStatus.ToString() + "] TxnStatus[" + stPayInfo.iTxnStatus.ToString() + "] TxnState[" + stPayInfo.iTxnState.ToString() + "] RespMesg[" + stPayInfo.szTxnMsg + "].")

                                If (stPayInfo.szTxnID <> "" And stPayInfo.szTxnAmount <> "" And (stPayInfo.szHostTxnStatus <> "" Or stPayInfo.szRespCode <> "") And stPayInfo.szMerchantOrdID <> "") Then
                                    If True <> objTxnProcOB.bInsertTxnResp(stPayInfo, stHostInfo) Then
                                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                    stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " Error: " + stPayInfo.szErrDesc)
                                    Else
                                        'Added, 21 Apr 2014, update PG..TB_PayTxnRef
                                        If True <> objTxnProc.bUpdatePayTxnRef(stPayInfo, stHostInfo) Then
                                            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                        stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " Error: " + stPayInfo.szErrDesc)
                                        End If
                                    End If
                                    'Added  12 Feb 2015 callback
                                    objResponse.bMerchantCallBackResp(stPayInfo, stHostInfo)
                                Else
                                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                " GatewayTxnID(" + stPayInfo.szTxnID + ")/TxnAmount(" + stPayInfo.szTxnAmount + ")/HostTxnStatus(" + stPayInfo.szHostTxnStatus + ")/RespCode(" + stPayInfo.szRespCode + ")/OrderNumber(" + stPayInfo.szMerchantOrdID + ") are empty from response. Bypass InsertTxnResp.")
                                End If
                            End If
                        End If
                    End If
                Else
                    'For this scenario, most probably Datebase error, the Retry Service will query the host for status
                    'once database is up.
                    stPayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_PENDING
                    stPayInfo.iTxnStatus = Common.TXN_STATUS.TXN_PENDING
                    stPayInfo.szTxnMsg = Common.C_INVALID_HOST_902
                    stPayInfo.szErrDesc = "> Failed to get Host Info for " + stPayInfo.szIssuingBank + " " + stPayInfo.szTxnType + " response"

                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    " Error: " + stPayInfo.szErrDesc)

                    'bResponseMain() is not run, therefore, unable to get other reply values.
                    'Merchant will only get TxnType, IssuingBank, TxnState, TxnStatus, RespMesg
                    'Without MerchantPymtID, the reply is meaningless for the merchant. Therefore, no need reply.
                End If
            End If
        Catch ex As Exception
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            " > " + C_ISSUING_BANK + " response Page_Load() Exception: " + ex.Message)
        End Try
    End Sub

    '********************************************************************************************
    ' Class Name:		respEXPAY
    ' Function Name:	bResponseMain
    ' Function Type:	Boolean.  Return False if error.
    ' Parameter:		NONE
    ' Description:		A main function to get the response info and handle the process
    ' History:			22 Aug 2009
    '********************************************************************************************
    Private Function bResponseMain() As Boolean
        Dim bReturn = True
        Dim iDBTimeOut As Integer = 0
        Dim iRet As Integer = -1
        Dim bGetMerchantInfo As Boolean = False
        Dim tempTxnID As String = ""
        Dim tempPymtID As String = ""

        Try
            'Log all form's response fields
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            C_ISSUING_BANK + " Response: " + szGetAllHTTPVal())

            If (Request.QueryString("order") Is Nothing And Request.QueryString("pid") Is Nothing) Then
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                "> Error1_2 bResponseMain(): Empty Response from Host's reply.")

                stPayInfo.szTxnMsg = Common.C_INVALID_HOST_REPLY_2907
                stPayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                stPayInfo.iTxnStatus = Common.TXN_STATUS.INVALID_HOST_REPLY
                stPayInfo.iTxnState = Common.TXN_STATE.ABORT

                'NOTE: May not be able to be inserted into Response table since GatewayTxnID is missing.
                Return False
            End If

            'Get Response Data
            tempTxnID = HttpContext.Current.Request.QueryString("order").ToString()
            tempPymtID = HttpContext.Current.Request.QueryString("pid").ToString()

            'Set Param
            stPayInfo.szTxnID = tempTxnID

            'Check Request table for stPayinfo's MerchantID, etc
            objTxnProcOB.bGetReqTxn(stPayInfo, stHostInfo)

            Dim iStartTickCount As Integer
            Dim iElapsedTime As Integer

            iElapsedTime = 0
            iStartTickCount = System.Environment.TickCount()
            iDBTimeOut = Convert.ToInt32(ConfigurationManager.AppSettings("DBQueryTimeoutSeconds"))

            While (iElapsedTime < iDBTimeOut * 1000)

                objTxnProcOB.bGetResTxn(stPayInfo, stHostInfo)

                If (-1 = stPayInfo.iQueryStatus) Then ' -1 indicate that record found in Res table
                    Exit While
                Else
                    Thread.Sleep(3000) 'sleep for 3 seconds
                End If

                iElapsedTime = System.Environment.TickCount() - iStartTickCount
            End While

            'If (False = bRet) Then 'To update iAction in Req Table
            If (-1 <> stPayInfo.iQueryStatus) Then
                bTimeOut = True
            End If

            'Added, 21 Jun 2015, if else to check s2s timeout due to bank not support send s2s resp using TLS1.2
            If (True = bTimeOut) Then
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "Get Response TimeOut = " + bTimeOut.ToString() + " for GatewayTxnID (" + stPayInfo.szTxnID + "),process Redirect resp")

                stPayInfo.szParam1 = tempPymtID

                'Added  7 Jan 2011, get MerchantID, CurrencyCode, GatewayTxnID, MerchantPymtID based on Param1
                If True <> objTxnProcOB.bGetTxnFromParam(stPayInfo, stHostInfo) Then
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bResponseMain() > " + stPayInfo.szErrDesc + ". Replace space with + and retry")
                    Return False
                End If

                'Added  7 Jan 2011, Get Terminal account details based on Currency Code
                If True <> objTxnProcOB.bGetTerminal(stHostInfo, stPayInfo) Then
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bResponseMain() > " + stPayInfo.szErrDesc)
                    Return False
                End If

                'Set latest unixtimestamp
                stPayInfo.szParam5 = DateDiff("s", "01/01/1970 00:00:00", Now()).ToString()

                If True <> objResponse.bProcessSaleRes(stPayInfo, stHostInfo, szReplyDateTime, g_szHTML) Then
                    Return False
                End If
            Else
                'Modified 21 Jun 2015, moved the following from below into this Else section, only run when recv s2s resp
                'Get merchant information
                'Modified 19 Dec 2013, objTxnProcOB to objTxnProc for bGetMerchant
                bGetMerchantInfo = objTxnProc.bGetMerchant(stPayInfo)

                If True <> bProcessSaleRes(stPayInfo, stHostInfo, szReplyDateTime, g_szHTML) Then
                    Return False
                End If
            End If

        Catch ex As Exception
            bReturn = False

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bResponseMain() Exception: " + ex.Message)
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		respEXPAY
    ' Function Name:	GetAllHTTPVal
    ' Function Type:	Boolean.  Return False if error.
    ' Parameter:		NONE
    ' Description:		Get all form's or query string's values 
    ' History:			22 Aug 2009 
    '********************************************************************************************
    Public Function szGetAllHTTPVal() As String
        Dim szFormValues As String
        Dim szTempKey As String
        Dim szTempVal As String

        Dim iCount As Integer = 0
        Dim iLoop As Integer = 0

        'Cater for EXPAY
        Dim reqHashKey As String = ""

        szFormValues = "[User IP: " + Request.UserHostAddress + "] "

        iCount = Request.Form.Count

        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                        "  > No. of Form Response Fields (" + iCount.ToString() + ")")

        If (iCount > 0) Then
            For iLoop = 1 To iCount
                szTempKey = ""
                szTempKey = Request.Form.GetKey(iLoop - 1)

                szFormValues = szFormValues + szTempKey + "="

                szTempVal = ""
                szTempVal = Server.UrlDecode(Request.Form.Get(iLoop - 1))

                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                " > Field(" + szTempKey + ") Value(" + szTempVal + ")")

                'Mask CardPAN
                If ((szTempKey <> "")) Then
                    If (szTempKey.ToLower = "cardpan") Then
                        If szTempVal.Length > 10 Then
                            szFormValues = szFormValues + szTempVal.Substring(0, 6) + "".PadRight(szTempVal.Length - 6 - 4, "X") + szTempVal.Substring(szTempVal.Length - 4, 4) + "&"
                        Else
                            szFormValues = szFormValues + "".PadRight(szTempVal.Length, "X") + "&"
                        End If
                    Else
                        If (iLoop = iCount) Then
                            szFormValues = szFormValues + szTempVal
                        Else
                            szFormValues = szFormValues + szTempVal + "&"
                        End If
                    End If
                End If
            Next iLoop
        Else
            iCount = Request.QueryString.Count

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            " > No. of QueryString Response Fields (" + iCount.ToString() + ")")

            For iLoop = 1 To iCount
                szTempKey = ""
                szTempKey = Request.QueryString.GetKey(iLoop - 1)

                szFormValues = szFormValues + szTempKey + "="

                szTempVal = ""
                szTempVal = Server.UrlDecode(Request.QueryString.Get(iLoop - 1))

                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                " > Field(" + szTempKey + ") Value(" + szTempVal + ")")

                'Mask CardPAN
                If ((szTempKey <> "")) Then
                    If (szTempKey.ToLower = "cardpan") Then
                        If szTempVal.Length > 10 Then
                            szFormValues = szFormValues + szTempVal.Substring(0, 6) + "".PadRight(szTempVal.Length - 6 - 4, "X") + szTempVal.Substring(szTempVal.Length - 4, 4) + "&"
                        Else
                            szFormValues = szFormValues + "".PadRight(szTempVal.Length, "X") + "&"
                        End If
                    Else
                        If (iLoop = iCount) Then
                            szFormValues = szFormValues + szTempVal
                        Else
                            szFormValues = szFormValues + szTempVal + "&"
                        End If
                    End If
                End If
            Next iLoop
        End If

        Return szFormValues
    End Function

    '********************************************************************************************
    ' Class Name:		Response
    ' Function Name:	bProcessSaleRes
    ' Function Type:	Boolean.  Return False if error.
    ' Parameter:		
    ' Description:		Process payment response
    ' History:			29 Oct 2008
    '********************************************************************************************
    Public Function bProcessSaleRes(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost, ByVal sz_ReplyDateTime As String, ByRef sz_HTML As String) As Boolean
        Dim bReturn As Boolean = True
        Dim szQueryString As String = ""
        Dim iMesgSet As Integer = -1
        Dim iMesgNum As Integer = -1
        Dim szMesg As String = ""           ' Added on 18 Apr 2010
        Dim iRet As Integer = -1

        Dim objHash As Hash
        Dim szBody As String = ""
        Dim szTxnMsg As String = ""         ' Added on 20 Apr 2017, Buyer Bank Name

        szTxnMsg = st_PayInfo.szTxnMsg      ' Added on 20 Apr 2017, Buyer Bank Name

        objHash = New Hash(objLoggerII, objCommon)      'Modified 15 Apr 2014, added objCommon
        st_PayInfo.szHashValue = ""

        Try
            '- Verify Host return IP addresses (optional)
            '- Check late response - exceeding 2 hours from request datetime since booking PO will be released after 2 hours
            '- Update Txn State
            '- Verify response data with request data
            '- Reply Acknowledgement to Host if needed
            '- Get merchant password
            '- Get HostTxnStatus's action
            'If True <> bInitResponse(st_PayInfo, st_HostInfo, sz_ReplyDateTime, sz_HTML) Then
            '    Return False
            'End If

            'Process Host reply based on action type configured in database according to Host TxnStatus
            If (Common.TXN_STATUS.TXN_SUCCESS = st_PayInfo.iTxnStatus) Then                'Payment approved by Host
                'Else    'Success (Payment approved, no need to confirm booking for this merchant)
                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_SUCCESS
                st_PayInfo.szTxnMsg = Common.C_TXN_SUCCESS_0
                st_PayInfo.iTxnState = Common.TXN_STATE.COMMIT
                'End If

            ElseIf (Common.TXN_STATUS.TXN_FAILED = st_PayInfo.iTxnStatus) Then    'Failed by Host
                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.szTxnMsg = Common.C_TXN_FAILED_1
                st_PayInfo.iTxnState = Common.TXN_STATE.COMMIT_FAILED

            ElseIf (Common.TXN_STATUS.TXN_PENDING = st_PayInfo.iTxnStatus) Then    'Unconfirmed status from Host, need to query Host until getting reply or timeout
                'Update Request table with Action 3 for Retry Service to pick up and query the respective Host
                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_PENDING
                st_PayInfo.szTxnMsg = "Host returned not processed/unknown status"
                st_PayInfo.iTxnState = Common.TXN_STATE.RECV_FROM_HOST
            End If

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "Get Response TimeOut = " + bTimeOut.ToString() + " for GatewayTxnID (" + stPayInfo.szTxnID + ")")

            If (True = bTimeOut) Then
                If (1 = st_HostInfo.iAllowReversal) Then
                    st_PayInfo.iAction = 7
                    st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.TIME_OUT_FAILED
                    st_PayInfo.szTxnMsg = "Timeout Host"
                    st_PayInfo.iTxnState = Common.TXN_STATE.ROLLBACK_PENDING
                Else
                    st_PayInfo.iAction = 3
                    st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_PENDING
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_PENDING
                    st_PayInfo.szTxnMsg = "Pending Host"
                    st_PayInfo.iTxnState = Common.TXN_STATE.SENT_QUERY_HOST
                End If

                'Update Request table with the respective Txn State and Action so that Retry Service can retry confirm booking
                'At the same time, also update Request table with BankRefNo (st_PayInfo.szHostTxnID), OSPymtCode (st_HostInfo.szOSPymtCode)
                If True <> objTxnProcOB.bUpdateTxnStatus(st_PayInfo, st_HostInfo) Then
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " Update req table failed [" + st_PayInfo.szErrDesc + "], could be removed by server-to-server resp, getting from resp table..")
                    'if update TxnStatus failed, try to check for Response table again. Because Req had been just move to Res by server 2 server. - CCGW
                    Dim bRet As Boolean = False
                    bRet = objTxnProcOB.bGetResTxn(stPayInfo, stHostInfo)
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " Got response [" + bRet.ToString() + "]")

                    If (True = bRet) Then
                        If (Common.TXN_STATUS.TXN_SUCCESS = stPayInfo.iTxnStatus) Then                'Payment approved by Host
                            'Else    'Success (Payment approved, no need to confirm booking for this merchant)
                            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_SUCCESS
                            st_PayInfo.szTxnMsg = Common.C_TXN_SUCCESS_0
                            st_PayInfo.iTxnState = Common.TXN_STATE.COMMIT
                            'End If

                        ElseIf (Common.TXN_STATUS.TXN_FAILED = stPayInfo.iTxnStatus) Then    'Failed by Host
                            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                            st_PayInfo.szTxnMsg = Common.C_TXN_FAILED_1
                            st_PayInfo.iTxnState = Common.TXN_STATE.COMMIT_FAILED

                        ElseIf (Common.TXN_STATUS.TXN_PENDING = stPayInfo.iTxnStatus) Then    'Unconfirmed status from Host, need to query Host until getting reply or timeout
                            'Update Request table with Action 3 for Retry Service to pick up and query the respective Host
                            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_PENDING
                            st_PayInfo.szTxnMsg = "Host returned not processed/unknown status"
                            st_PayInfo.iTxnState = Common.TXN_STATE.RECV_FROM_HOST
                        End If
                    End If
                End If
            End If

            If (szTxnMsg <> "") Then                'Added, 20 Apr 2017, Buyer Bank Name
                st_PayInfo.szTxnMsg = szTxnMsg
            End If

            ''Update Request table with the respective Txn State and Action so that Retry Service can retry confirm booking
            ''At the same time, also update Request table with BankRefNo (st_PayInfo.szHostTxnID), OSPymtCode (st_HostInfo.szOSPymtCode)
            'If True <> objTxnProcOB.bUpdateTxnStatus(st_PayInfo, st_HostInfo) Then
            '    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
            '                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
            '                    st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " " + st_PayInfo.szErrDesc)
            'End If

            'Send Gateway response to merchant using the same Host's reply method
            objResponse.iSendReply2Merchant(st_PayInfo, st_HostInfo, sz_HTML)
            'Even If (True = bRet), no need update DB for TxnState SENT_REPLY_CLIENT so that TxnState like COMMIT/COMMIT_FAILED won't be overwritten.

        Catch ex As Exception
            bReturn = False

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Exception: " + ex.Message)

            'Added on 12 Mar 2011, to solve "The operation has timed out" exception after calling NewSkies Web Service ConfirmBooking()
            'Without adding this, even though payment was approved but when encountered operation timeout during booking confirmation,
            'DDGW will return TxnStatus -1 and TxnState 2 to merchant that requires DDGW to confirm booking
            If (1 = st_PayInfo.iAction And 1 = st_PayInfo.iNeedAddOSPymt) Then  'Approved by bank and need booking confirmation
                bReturn = True

                If ("THE OPERATION HAS TIMED OUT" = ex.Message.ToUpper.Trim()) Then
                    st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_PENDING_CONFIRM_BOOKING   'Merchant will extend booking based on this status
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_PENDING_CONFIRM_BOOKING
                    st_PayInfo.szTxnMsg = "Pending: Payment approved but booking status is unknown due to communication error with booking system, retry in progress"
                    st_PayInfo.iTxnState = Common.TXN_STATE.SENT_CONFIRM_OS

                    st_PayInfo.iAction = 4  'Retry ConfirmBooking
                Else
                    st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_PENDING
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.TXN_PENDING
                    st_PayInfo.szTxnMsg = "Pending: Status unknown, query Host in progress"
                    st_PayInfo.iTxnState = Common.TXN_STATE.RECV_FROM_HOST

                    st_PayInfo.iAction = 3  'Retry query bank
                End If

                'Update Request table with the respective Txn State and Action so that Retry Service can retry confirm booking
                'At the same time, also update Request table with BankRefNo (st_PayInfo.szHostTxnID), OSPymtCode (st_HostInfo.szOSPymtCode)
                If True <> objTxnProcOB.bUpdateTxnStatus(st_PayInfo, st_HostInfo) Then
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " " + st_PayInfo.szErrDesc)
                End If

                st_HostInfo.iHostReplyMethod = 1
                'Send Gateway response to merchant using the same Host's reply method
                objResponse.iSendReply2Merchant(st_PayInfo, st_HostInfo, sz_HTML)
                'Even If (True = bRet), no need update DB for TxnState SENT_REPLY_CLIENT so that TxnState like COMMIT/COMMIT_FAILED won't be overwritten.
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		Response
    ' Function Name:	bInitResponse
    ' Function Type:	Boolean.  Return False if error.
    ' Parameter:		
    ' Description:		Update Txn State, Verify response data with request data, Reply Acknowledgement to Host if needed,
    '                   Get merchant password, Get HostTxnStatus's action
    ' History:			31 Oct 2008
    '********************************************************************************************
    Private Function bInitResponse(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost, ByVal sz_ReplyDateTime As String, ByRef sz_HTML As String) As Boolean
        Dim bReturn As Boolean = True
        Dim aszReturnIPAddresses() As String
        Dim iLoop As Integer = 0
        Dim iRet As Integer = -1
        Dim bMatch As Boolean = False

        Try
            'Verify Host return IP addresses (optional)
            If (st_HostInfo.szReturnIPAddresses <> "") Then
                aszReturnIPAddresses = Split(st_HostInfo.szReturnIPAddresses, ";")
                bMatch = False
                For iLoop = 0 To aszReturnIPAddresses.GetUpperBound(0)
                    If (aszReturnIPAddresses(iLoop) = HttpContext.Current.Request.UserHostAddress) Then
                        bMatch = True

                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                        "S2S-GatewayTxnID(" + st_PayInfo.szTxnID + ") Host reply IP matched " + aszReturnIPAddresses(iLoop))
                    End If
                Next

                If (False = bMatch) Then
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.INVALID_HOST_IP

                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    "S2S-GatewayTxnID(" + st_PayInfo.szTxnID + ") Host reply IP (" + HttpContext.Current.Request.UserHostAddress + ") does not match IP list (" + st_HostInfo.szReturnIPAddresses + ")")
                    Return False
                End If
            End If

            'Added checking of DDGW Aggregator on 3 Apr 2010, no need check late Host response for DDGW Aggregator
            If ("" = ConfigurationManager.AppSettings("HostsListTemplate")) Then
                ' Check late reply
                ' (2 hours x 60 minutes x 60 seconds = 7200 seconds) - PO 'll be released after 2 hrs from its reserved time.
                ' So, host reply came back 2 hours after the request was registered in Gateway 'll be treated as late reply and 'll be discarded.
                If True <> objTxnProcOB.bCheckLateResp(st_PayInfo, sz_ReplyDateTime) Then
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    "S2S-GatewayTxnID(" + st_PayInfo.szTxnID + ") " + st_PayInfo.szErrDesc)

                    Return False
                End If
            End If

            'Verify response data with request data stored in database.
            'At the same time, retrieve some request data from database based on Gateway TxnID to return to client's server.
            If True <> objResponse.bVerifyResTxn(st_PayInfo, st_HostInfo, sz_HTML) Then
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                "S2S-GatewayTxnID(" + st_PayInfo.szTxnID + ") > bVerifyResTxn failed: " + st_PayInfo.szErrDesc)

                Return False
            End If

            'Get Host's TxnStatus's action
            If True <> objTxnProcOB.bGetTxnStatusAction(st_HostInfo, st_PayInfo) Then
                st_PayInfo.szTxnType = "PAY"

                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > bInitResponse() " + st_PayInfo.szErrDesc)
                Return False
            End If

            st_PayInfo.szTxnType = "PAY"

        Catch ex As Exception
            bReturn = False

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > bInitResponse() Exception: " + ex.Message)
        End Try

        Return bReturn
    End Function

    Public Function szReqGetHashKey() As String
        Dim szFormValues As String = ""
        Dim szTempKey As String
        Dim szTempVal As String

        Dim iCount As Integer = 0
        Dim iLoop As Integer = 0

        'Cater for EXPAY
        Dim reqHashKey As String = ""

        iCount = Request.Form.Count

        If (iCount > 0) Then
            For iLoop = 1 To iCount
                szTempKey = ""
                szTempKey = Request.Form.GetKey(iLoop - 1)

                If (szTempKey.ToLower <> "hash") Then
                    szFormValues = szFormValues + szTempKey + "="

                    szTempVal = ""
                    szTempVal = Server.UrlDecode(Request.Form.Get(iLoop - 1))

                    If ((szTempKey <> "")) Then
                        If (iLoop = iCount) Then
                            szFormValues = szFormValues + szTempVal
                        Else
                            szFormValues = szFormValues + szTempVal + "&"
                        End If
                    End If
                End If
            Next iLoop
        End If
        If szFormValues.Substring(szFormValues.Length - 1) = "&" Then
            szFormValues = szFormValues.Substring(0, szFormValues.Length - 1)
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                 " >" + C_ISSUING_BANK + " szReqGetHashKey(): " + szFormValues)
        End If

        Return szFormValues
    End Function


    Public Function bIsVerifyPayment() As Boolean
        Dim bReturn As Boolean = True
        Dim szRequestMethod As String = Request.Form.Get("method")

        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                C_ISSUING_BANK + " Request: " + szGetAllHTTPVal())

        If szRequestMethod = "" Then
            bReturn = False
        ElseIf szRequestMethod.Equals("check") Or szRequestMethod.Equals("pay") Then
            bReturn = True
        End If

        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                C_ISSUING_BANK + " Request > bIsVerifyPayment() > " + bReturn.ToString)

        Return bReturn
    End Function

    Private Function bRequestMain() As String
        Dim bReturnMessage As String = ""
        Dim iRet As Integer = -1
        Dim reqTimestamp As String = DateDiff("s", "01/01/1970 00:00:00", Now()).ToString()

        Try
            stPayInfo.szTxnType = "PAY3"

            'Get param from request message
            Dim reqPymtID As String = Request.Form.Get("id").ToString
            Dim reqTxnAmount As Decimal = Request.Form.Get("amount").ToString
            Dim reqTxnID As String = Request.Form.Get("order").ToString
            Dim reqHashValue As String = Request.Form.Get("hash").ToString
            Dim reqMethod As String = Request.Form.Get("method").ToString
            Dim reqHashKey As String = ""

            reqHashKey = szReqGetHashKey()

            Dim reqHashValueVef As String = objHash.szGetHash(reqHashKey.Trim(), "HMACSHA1LH", stPayInfo, stHostInfo)
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                        stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bRequestMain() > " + reqHashKey + " Hash: " + reqHashValueVef)

            If (reqHashValueVef.Equals(reqHashValue)) Then
                stPayInfo.szTxnID = reqTxnID

                objTxnProcOB.bGetReqTxn(stPayInfo, stHostInfo)
                stPayInfo.szTxnType = "PAY3"

                If (stPayInfo.iQueryStatus = -3) Then
                    stPayInfo.szParam3 = bGetResponseStatusValue(99)
                    stPayInfo.szParam4 = stPayInfo.szTxnMsg
                    stPayInfo.szParam5 = reqTimestamp
                ElseIf (stPayInfo.iQueryStatus = 1) Then
                    If reqTxnID.ToLower().Equals(stPayInfo.szTxnID.ToLower()) And reqTxnAmount.ToString().Equals(stPayInfo.szFXTxnAmount) Then
                        If (reqMethod.ToLower.Equals("check")) Then
                            stPayInfo.szParam3 = bGetResponseStatusValue(stPayInfo.iTxnStatus)
                            stPayInfo.szParam4 = "Transaction Does Exist"
                            stPayInfo.szParam5 = reqTimestamp

                        ElseIf (reqMethod.ToLower.Equals("pay")) Then
                            stPayInfo.szParam3 = bGetResponseStatusValue(0) 'Default Return Success
                            stPayInfo.szParam4 = "Transaction Does Exist"
                            stPayInfo.szParam5 = reqTimestamp

                            'Payment approved by Host if request method is "pay"
                            stPayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_SUCCESS
                            stPayInfo.iTxnStatus = Common.TXN_STATUS.TXN_SUCCESS
                            stPayInfo.szTxnMsg = Common.C_TXN_SUCCESS_0
                            stPayInfo.iTxnState = Common.TXN_STATE.COMMIT
                            stPayInfo.szRespCode = stPayInfo.szParam3
                            stPayInfo.szHostTxnID = stPayInfo.szParam1

                            If True <> objTxnProcOB.bInsertTxnResp(stPayInfo, stHostInfo) Then
                                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                        stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " Error: " + stPayInfo.szErrDesc)
                            Else
                                'Added, 21 Apr 2014, update PG..TB_PayTxnRef
                                If True <> objTxnProc.bUpdatePayTxnRef(stPayInfo, stHostInfo) Then
                                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                            stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " Error: " + stPayInfo.szErrDesc)
                                End If
                            End If
                        End If
                    Else
                        stPayInfo.szParam3 = bGetResponseStatusValue(99)
                        stPayInfo.szParam4 = "Transaction mismatched"
                        stPayInfo.szParam5 = reqTimestamp
                    End If
                ElseIf (stPayInfo.iQueryStatus = -1) Then
                    stPayInfo.szTxnType = "PAY3"

                    If (reqMethod.ToLower.Equals("check")) Then
                        stPayInfo.szParam3 = bGetResponseStatusValue(30)
                        stPayInfo.szParam4 = "Transaction Does Exist"
                        stPayInfo.szParam5 = reqTimestamp
                    Else
                        If True <> objTxnProcOB.bGetResTxn(stPayInfo, stHostInfo) Then
                            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                       DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                       " > bRequestMain() > Check status of payment transaction: " + stPayInfo.szErrDesc)

                            stPayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                            stPayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                            stPayInfo.iTxnState = Common.TXN_STATE.ABORT
                        End If

                        If (stPayInfo.iTxnStatus = 0) Then
                            stPayInfo.szParam3 = bGetResponseStatusValue(stPayInfo.iTxnStatus)
                            stPayInfo.szParam4 = stPayInfo.szQueryMsg
                            stPayInfo.szParam5 = reqTimestamp
                        Else
                            stPayInfo.szParam3 = bGetResponseStatusValue(99)
                            stPayInfo.szParam4 = stPayInfo.szQueryMsg
                            stPayInfo.szParam5 = reqTimestamp
                        End If
                    End If
                End If
            Else
                stPayInfo.szParam3 = bGetResponseStatusValue(-99)
                stPayInfo.szParam4 = "Invalid Hash"
                stPayInfo.szParam5 = reqTimestamp
            End If

            iRet = objResponse.iGetQueryString(bReturnMessage, stHostInfo.szHostTemplate, "RES", stPayInfo.szTxnType, stPayInfo, stHostInfo)

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                    C_ISSUING_BANK + " > bRequestMain() > : (" + iRet.ToString + ") Transaction ID: " + stPayInfo.szTxnID +
                    "Status: " + stPayInfo.szParam3 + " Message: " + stPayInfo.szParam4)

        Catch ex As Exception

            stPayInfo.szParam3 = stPayInfo.szParam3 = bGetResponseStatusValue(99)
            stPayInfo.szParam4 = ex.Message
            stPayInfo.szParam5 = reqTimestamp

            objResponse.iGetQueryString(bReturnMessage, stHostInfo.szHostTemplate, "RES", stPayInfo.szTxnType, stPayInfo, stHostInfo)

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                C_ISSUING_BANK + " > bRequestMain() > Request " + ex.Message)
        End Try

        Return bReturnMessage
    End Function

    Public Function bGetResponseStatusValue(key As Integer) As Integer
        'Transform status value
        Dim dictStatus As New Dictionary(Of Integer, Integer)()
        dictStatus.Add(0, 205)  'Payment Success
        dictStatus.Add(30, 270) 'Transaction Exist
        dictStatus.Add(-99, 401) 'Invalid Hash; Custom Key code -99
        dictStatus.Add(99, 475) 'Invalid code; Custom Key code 99
        dictStatus.Add(500, 500)

        If (dictStatus.ContainsKey(key)) Then
            Return dictStatus.Item(key)
        Else
            Return 475
        End If
    End Function

    Public Sub New()
        szLogPath = ConfigurationManager.AppSettings("LogPath")
        iLogLevel = Convert.ToInt32(ConfigurationManager.AppSettings("LogLevel"))

        '2 Apr 2010, moved InitLogger() from down to up here, or else Hash will get empty objLoggerII and encountered exception
        InitLogger()

        objTxnProc = New TxnProc(objLoggerII)           'Modified 20 Oct 2017, moved above objCommon
        objCommon = New Common(objTxnProc, objLoggerII) 'Modified 20 Oct 2017, added objTxnProc
        objHash = New Hash(objLoggerII)
        objTxnProcOB = New TxnProcOB(objLoggerII)
        objResponse = New Response(objLoggerII)
    End Sub

    Public Sub InitLogger()
        If (objLoggerII Is Nothing) Then

            If (szLogPath Is Nothing) Then
                szLogPath = "C:\\OB.log"
            End If

            If (iLogLevel < 0 Or iLogLevel > 5) Then
                iLogLevel = 3
            End If

            objLoggerII = LoggerII.CLoggerII.GetInstanceX(szLogPath + "_" + C_ISSUING_BANK + ".log", iLogLevel, 255)

        End If
    End Sub

    Protected Overrides Sub Finalize()
        If Not objLoggerII Is Nothing Then
            objLoggerII.Dispose()
            objLoggerII = Nothing
        End If

        If Not objCommon Is Nothing Then objCommon = Nothing
        If Not objTxnProcOB Is Nothing Then objTxnProcOB = Nothing
        If Not objTxnProc Is Nothing Then objTxnProc = Nothing
        If Not objHash Is Nothing Then objHash = Nothing

        If Not objResponse Is Nothing Then
            objResponse.Dispose()
            objResponse = Nothing
        End If

        MyBase.Finalize()
    End Sub
End Class
