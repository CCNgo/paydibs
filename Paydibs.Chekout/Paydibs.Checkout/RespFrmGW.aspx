﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="RespFrmGW.aspx.vb" Inherits="Paydibs.Checkout.RespFrmGW" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Transactions Response Result</title>
</head>
<body>
    <form id="form1" runat="server">
    <div align="center">
        <b style="font-size: 30px; color: #f55a00">Transactions Response Result</b><br />
        <br />
        <asp:Label ID="lblResp" runat="server"></asp:Label>
    </div>
    </form>
</body>
</html>
