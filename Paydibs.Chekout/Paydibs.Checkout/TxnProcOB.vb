﻿Imports System.Data.SqlClient
Imports System.Threading
Imports LoggerII
Imports System.Security.Cryptography
Imports Crypto
Imports System.Security
Imports System.Net

Public Class TxnProcOB
    Private objLoggerII As LoggerII.CLoggerII
    Private objHash As Hash
    Private szDBConnStr As String = ""          'Added 23 Sept 2013

    '********************************************************************************************
    ' Class Name:		TxnProcOB
    ' Function Name:	bUpdateTxnStatusEx
    ' Function Type:	Boolean.  True if update successful else false
    ' Parameter:		st_PayInfo
    ' Description:		Updates transaction status in either Request/Response table
    ' History:			24 Nov 2013
    '********************************************************************************************
    Public Function bUpdateTxnStatusEx(ByRef st_PayInfo As Common.STPayInfo) As Boolean

        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRowsAffected As Integer = 0

        Try
            bReturn = False

            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spUpdateTxnStatusEx", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@i_TxnStatus", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_TxnState", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_ReqOrRes", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@sz_RespMesg", SqlDbType.VarChar, 255)
            objParam = objCommand.Parameters.Add("@i_PKID", SqlDbType.BigInt)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@i_TxnStatus").Value = st_PayInfo.iTxnStatus
            objCommand.Parameters("@i_TxnState").Value = st_PayInfo.iTxnState
            objCommand.Parameters("@i_ReqOrRes").Value = st_PayInfo.iQueryStatus
            objCommand.Parameters("@sz_RespMesg").Value = st_PayInfo.szTxnMsg
            objCommand.Parameters("@i_PKID").Value = st_PayInfo.iPKID

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()
            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True
            Else
                bReturn = False

                st_PayInfo.szErrDesc = "bUpdateTxnStatusEx() error: Failed to update TxnStatus(" + st_PayInfo.iTxnStatus.ToString() + ") TxnState(" + st_PayInfo.iTxnState.ToString() + ") ReqOrRes(" + st_PayInfo.iQueryStatus.ToString() + ") PKID(" + st_PayInfo.iPKID.ToString() + ") GatewayTxnID(" + st_PayInfo.szTxnID + ")"

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_INT_ERR
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
                st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bUpdateTxnStatusEx() exception: " + ex.ToString + " > " + "TxnStatus(" + st_PayInfo.iTxnStatus.ToString() + ") TxnState(" + st_PayInfo.iTxnState.ToString() + ") ReqOrRes(" + st_PayInfo.iQueryStatus.ToString() + ") PKID (" + st_PayInfo.iPKID.ToString() + ") GatewayTxnID(" + st_PayInfo.szTxnID + ")"

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_INT_ERR
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProcOB
    ' Function Name:	bInsertTxnResp
    ' Function Type:	Boolean.  True if insertion successful else false
    ' Parameter:		st_PayInfo, sz_DBConnStr
    ' Description:		Inserts transaction response into database
    ' History:			15 Oct 2008
    '********************************************************************************************
    Public Function bInsertTxnResp(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost) As Boolean

        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection
        Dim objCommand As SqlCommand
        Dim objParam As SqlParameter
        Dim dr As SqlDataReader
        Dim iRowsAffected As Integer = 0
        Dim szTxnAmount As String = ""      'Added, 1 Sept 2014, for FX
        Dim szCurrencyCode As String = ""   'Added, 1 Sept 2014, for FX

        Try
            bReturn = False
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spInsTxnResp", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_TxnType", SqlDbType.VarChar, 7)
            objParam = objCommand.Parameters.Add("@sz_GatewayTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_MerchantTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_TxnAmount", SqlDbType.VarChar, 18)
            objParam = objCommand.Parameters.Add("@d_TxnAmount", SqlDbType.Decimal, 18)   'Added on 24 Jun 2010
            objParam = objCommand.Parameters.Add("@sz_CurrencyCode", SqlDbType.VarChar, 3)
            objParam = objCommand.Parameters.Add("@sz_GatewayID", SqlDbType.VarChar, 4)
            objParam = objCommand.Parameters.Add("@sz_CardPAN", SqlDbType.VarChar, 20)
            objParam = objCommand.Parameters.Add("@i_HostID", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@sz_IssuingBank", SqlDbType.VarChar, 20)
            objParam = objCommand.Parameters.Add("@sz_RespMesg", SqlDbType.VarChar, 255)
            objParam = objCommand.Parameters.Add("@i_TxnState", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_MerchantTxnStatus", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_TxnStatus", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@sz_BankRespCode", SqlDbType.VarChar, 20)  'Modified on 27 Aug 2009, from varchar(4) to varchar(20)
            objParam = objCommand.Parameters.Add("@sz_BankRefNo", SqlDbType.VarChar, 40)    'Modified 19 Nov 2015, Bitnet, increase from 30 to 40 to store Invoice Number required for Refund
            objParam = objCommand.Parameters.Add("@sz_HashMethod", SqlDbType.VarChar, 6)
            objParam = objCommand.Parameters.Add("@sz_HashValue", SqlDbType.VarChar, 200)   'Modified 19 Jun 2014, increased from 40 to 100, for SHA256
            objParam = objCommand.Parameters.Add("@sz_OrderNumber", SqlDbType.VarChar, 20)
            objParam = objCommand.Parameters.Add("@sz_ErrDesc", SqlDbType.VarChar, 200)
            objParam = objCommand.Parameters.Add("@sz_Param1", SqlDbType.VarChar, 100)
            objParam = objCommand.Parameters.Add("@sz_Param2", SqlDbType.VarChar, 100)
            objParam = objCommand.Parameters.Add("@sz_Param3", SqlDbType.VarChar, 100)
            objParam = objCommand.Parameters.Add("@sz_Param4", SqlDbType.VarChar, 100)
            objParam = objCommand.Parameters.Add("@sz_Param5", SqlDbType.VarChar, 100)
            objParam = objCommand.Parameters.Add("@sz_Param6", SqlDbType.VarChar, 50)       'Added, 24 Nov 2015, Param67
            objParam = objCommand.Parameters.Add("@sz_Param7", SqlDbType.VarChar, 50)       'Added, 24 Nov 2015, Param67
            objParam = objCommand.Parameters.Add("@sz_MachineID", SqlDbType.VarChar, 10)
            objParam = objCommand.Parameters.Add("@i_OSRet", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_ErrSet", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_ErrNum", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            'Added on 9 Jan 2009
            If ("query" = st_PayInfo.szTxnType.ToLower()) Then
                objCommand.Parameters("@sz_TxnType").Value = "PAY"
            Else
                objCommand.Parameters("@sz_TxnType").Value = st_PayInfo.szTxnType
            End If
            objCommand.Parameters("@sz_GatewayTxnID").Value = st_PayInfo.szTxnID
            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@sz_MerchantTxnID").Value = st_PayInfo.szMerchantTxnID

            'Modified 1 Sept 2014, added checking of FXCurrencyCode, TB_PayResp table's TxnAmount should store base amount
            If ("" = st_PayInfo.szFXCurrencyCode) Then
                szTxnAmount = st_PayInfo.szTxnAmount
                szCurrencyCode = st_PayInfo.szCurrencyCode
            Else
                szTxnAmount = st_PayInfo.szBaseTxnAmount
                szCurrencyCode = st_PayInfo.szBaseCurrencyCode
            End If

            'Modified 1 Sept 2014, use szTxnAmount instead of st_PayInfo.szTxnAmount, and szCurrencyCode
            objCommand.Parameters("@sz_TxnAmount").Value = szTxnAmount
            objCommand.Parameters("@d_TxnAmount").Value = Convert.ToDecimal(szTxnAmount) 'Added on 24 Jun 2010
            objCommand.Parameters("@sz_CurrencyCode").Value = szCurrencyCode

            objCommand.Parameters("@sz_GatewayID").Value = st_PayInfo.szGatewayID
            objCommand.Parameters("@sz_CardPAN").Value = st_PayInfo.szMaskedCardPAN
            objCommand.Parameters("@i_HostID").Value = st_HostInfo.iHostID
            objCommand.Parameters("@sz_IssuingBank").Value = st_PayInfo.szIssuingBank

            'Modified 8 Dec 2013, if bank returned message, stores bank message or else stores gateway message
            If (st_PayInfo.szBankRespMsg <> "") Then
                objCommand.Parameters("@sz_RespMesg").Value = st_PayInfo.szBankRespMsg  'Response message returned from bank
            Else
                objCommand.Parameters("@sz_RespMesg").Value = st_PayInfo.szTxnMsg       'Response message generated by Gateway and returned to merchant, not storing Host's response message
            End If

            objCommand.Parameters("@i_TxnState").Value = st_PayInfo.iTxnState
            objCommand.Parameters("@i_MerchantTxnStatus").Value = st_PayInfo.iMerchantTxnStatus
            objCommand.Parameters("@i_TxnStatus").Value = st_PayInfo.iTxnStatus
            If (st_PayInfo.szRespCode <> "") Then
                objCommand.Parameters("@sz_BankRespCode").Value = st_PayInfo.szRespCode
            Else
                objCommand.Parameters("@sz_BankRespCode").Value = st_PayInfo.szHostTxnStatus
            End If
            If (st_PayInfo.szHostTxnID <> "") Then
                objCommand.Parameters("@sz_BankRefNo").Value = st_PayInfo.szHostTxnID
            Else
                objCommand.Parameters("@sz_BankRefNo").Value = st_PayInfo.szAuthCode
            End If
            objCommand.Parameters("@sz_HashMethod").Value = st_PayInfo.szHashMethod

            'Added on 26 May 2009, to store the maximum length
            If ("" <> st_PayInfo.szHashValue) Then
                If (st_PayInfo.szHashValue.Length > 200) Then                           'Modified 19 Jun 2014, 40 to 100, SHA256
                    st_PayInfo.szHashValue = st_PayInfo.szHashValue.Substring(0, 200)   'Modified 19 Jun 2014, 40 to 100, SHA256
                End If
            End If
            objCommand.Parameters("@sz_HashValue").Value = st_PayInfo.szHashValue

            objCommand.Parameters("@sz_OrderNumber").Value = st_PayInfo.szMerchantOrdID
            objCommand.Parameters("@sz_ErrDesc").Value = st_PayInfo.szErrDesc   'Error message generated by Gateway, not returned to merchant.
            objCommand.Parameters("@sz_Param1").Value = st_PayInfo.szParam1
            objCommand.Parameters("@sz_Param2").Value = st_PayInfo.szParam2
            objCommand.Parameters("@sz_Param3").Value = st_PayInfo.szParam3
            objCommand.Parameters("@sz_Param4").Value = st_PayInfo.szParam4
            objCommand.Parameters("@sz_Param5").Value = st_PayInfo.szParam5
            objCommand.Parameters("@sz_Param6").Value = st_PayInfo.szParam6
            objCommand.Parameters("@sz_Param7").Value = st_PayInfo.szParam7
            objCommand.Parameters("@sz_MachineID").Value = st_PayInfo.szMachineID
            objCommand.Parameters("@i_OSRet").Value = st_PayInfo.iOSRet
            objCommand.Parameters("@i_ErrSet").Value = st_PayInfo.iErrSet
            objCommand.Parameters("@i_ErrNum").Value = st_PayInfo.iErrNum

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()
            'Modified 19 Dec 2013, changed from .Value = 0, returned value now has become PKID inserted into response table
            'Value -2 indicates already exists in response table
            If objCommand.Parameters("@i_Return").Value > 0 Then
                st_PayInfo.iPKID = objCommand.Parameters("@i_Return").Value 'Added, 19 Dec 2013

                bReturn = True
            Else
                st_PayInfo.szErrDesc = "bInsertTxnResp() error: Failed to insert transaction response. GatewayTxnID(" + st_PayInfo.szTxnID + ")"

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.iTxnState = Common.TXN_STATE.FAILED
                st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            End If

            dr.Close()

        Catch ex As Exception
            st_PayInfo.szErrDesc = "bInsertTxnResp() exception: " + ex.ToString + " > " + "GatewayTxnID(" + st_PayInfo.szTxnID + ")"

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.FAILED
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProcOB
    ' Function Name:	bCheckUniqueTxn
    ' Function Type:	Boolean.  True if unique else false
    ' Parameter:		st_PayInfo, sz_DBConnStr
    ' Description:		Check the uniqueness of the transaction
    ' History:			17 Oct 2008
    '********************************************************************************************
    Public Function bCheckUniqueTxn(ByRef st_PayInfo As Common.STPayInfo) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection
        Dim objCommand As SqlCommand
        Dim objParam As SqlParameter

        Dim iRowsAffected As Integer = 0

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spCheckUniqueTxn", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_MerchantTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@sz_MerchantTxnID").Value = st_PayInfo.szMerchantTxnID

            objConn.Open()
            iRowsAffected = objCommand.ExecuteNonQuery()

            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True
            Else
                bReturn = False

                st_PayInfo.szErrDesc = "bCheckUniqueTxn() error: Duplicate MerchantPymtID."

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INVALID_INPUT 'Common.ERROR_CODE.E_INVALID_INPUT
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT           'Common.TXN_STATE.FAILED
                st_PayInfo.szTxnMsg = Common.C_INVALID_INPUT_2906
            End If

        Catch ex As Exception
            st_PayInfo.szErrDesc = "bCheckUniqueTxn() exception: " + ex.ToString

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.FAILED
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProcOB
    ' Function Name:	bInsertNewTxn
    ' Function Type:	Boolean.  True if inserted successfully else false
    ' Parameter:		st_PayInfo, sz_DBConnStr
    ' Description:		Insert new transaction in DB
    ' History:			20 Oct 2008
    '********************************************************************************************
    Public Function bInsertNewTxn(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection
        Dim objCommand As SqlCommand
        Dim objParam As SqlParameter
        Dim szTxnAmount As String = ""      'Added, 3 Sept 2014, for FX
        Dim szCurrencyCode As String = ""   'Added, 3 Sept 2014, for FX

        Dim iRowsAffected As Integer = 0

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spInsertNewTxn", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_GatewayTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_MerchantName", SqlDbType.VarChar, 25)
            objParam = objCommand.Parameters.Add("@sz_MerchantReturnURL", SqlDbType.VarChar, 255)
            objParam = objCommand.Parameters.Add("@sz_MerchantSupportURL", SqlDbType.VarChar, 255)
            objParam = objCommand.Parameters.Add("@sz_MerchantTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_TxnAmount", SqlDbType.VarChar, 18)
            objParam = objCommand.Parameters.Add("@d_TxnAmount", SqlDbType.Decimal, 18)         'Added on 24 Jun 2010
            objParam = objCommand.Parameters.Add("@sz_BaseTxnAmount", SqlDbType.VarChar, 18)    'Added on 3 Aug 2009, multi currency booking confirmation enhancement
            objParam = objCommand.Parameters.Add("@sz_CurrencyCode", SqlDbType.VarChar, 3)
            objParam = objCommand.Parameters.Add("@sz_BaseCurrencyCode", SqlDbType.VarChar, 3)  'Added on 3 Aug 2009, multi currency booking confirmation enhancement
            objParam = objCommand.Parameters.Add("@sz_GatewayID", SqlDbType.VarChar, 4)
            objParam = objCommand.Parameters.Add("@sz_TxnType", SqlDbType.VarChar, 7)
            objParam = objCommand.Parameters.Add("@sz_OrderDesc", SqlDbType.NVarChar, 100)      'Modified 17 Oct 2014, VarChar to NVarChar
            objParam = objCommand.Parameters.Add("@sz_CardPAN", SqlDbType.VarChar, 20)
            objParam = objCommand.Parameters.Add("@sz_MaskedCardPAN", SqlDbType.VarChar, 20)
            objParam = objCommand.Parameters.Add("@sz_OrderNumber", SqlDbType.VarChar, 20)
            objParam = objCommand.Parameters.Add("@sz_HashMethod", SqlDbType.VarChar, 6)
            objParam = objCommand.Parameters.Add("@sz_HashValue", SqlDbType.VarChar, 200)
            objParam = objCommand.Parameters.Add("@i_HostID", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@sz_IssuingBank", SqlDbType.VarChar, 20)
            objParam = objCommand.Parameters.Add("@sz_LanguageCode", SqlDbType.Char, 2)
            objParam = objCommand.Parameters.Add("@sz_CustEmail", SqlDbType.VarChar, 60)        'Added, 16 Dec 2013
            objParam = objCommand.Parameters.Add("@sz_CustName", SqlDbType.NVarChar, 50)        'Added, 18 Feb 2014
            objParam = objCommand.Parameters.Add("@sz_CustPhone", SqlDbType.VarChar, 25)        'Added, 17 Apr 2014
            objParam = objCommand.Parameters.Add("@sz_SessionID", SqlDbType.VarChar, 100)
            objParam = objCommand.Parameters.Add("@sz_Channel", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_Param1", SqlDbType.VarChar, 100)
            objParam = objCommand.Parameters.Add("@sz_Param2", SqlDbType.VarChar, 100)
            objParam = objCommand.Parameters.Add("@sz_Param3", SqlDbType.VarChar, 100)
            objParam = objCommand.Parameters.Add("@sz_Param4", SqlDbType.VarChar, 100)
            objParam = objCommand.Parameters.Add("@sz_Param5", SqlDbType.VarChar, 100)
            objParam = objCommand.Parameters.Add("@sz_MachineID", SqlDbType.VarChar, 10)
            objParam = objCommand.Parameters.Add("@sz_DateCreated", SqlDbType.VarChar, 23)
            objParam = objCommand.Parameters.Add("@i_DateCreated", SqlDbType.Int)               'Added on 24 Jun 2010
            objParam = objCommand.Parameters.Add("@i_TxnTimeOut", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@sz_MerchantApprovalURL", SqlDbType.VarChar, 255)     'Added, 4 Jul 2014, Firefly FF
            objParam = objCommand.Parameters.Add("@sz_MerchantUnApprovalURL", SqlDbType.VarChar, 255)   'Added, 4 Jul 2014, Firefly FF
            objParam = objCommand.Parameters.Add("@sz_MerchantCallbackURL", SqlDbType.VarChar, 255)     'Added  16 Oct 2014

            If (st_PayInfo.szFXCurrencyCode <> "") Then
                objParam = objCommand.Parameters.Add("@sz_FXCurrencyCode", SqlDbType.VarChar, 3)    'Added, 28 Aug 2014, for FX
                objParam = objCommand.Parameters.Add("@d_FXTxnAmt", SqlDbType.Decimal, 18)          'Added, 28 Aug 2014, for FX
                objParam = objCommand.Parameters.Add("@d_FXCurrOriRate", SqlDbType.Decimal, 18)     'Added, 28 Aug 2014, for FX
                objParam = objCommand.Parameters.Add("@d_FXCurrMarkUp", SqlDbType.Decimal, 18)      'Added, 28 Aug 2014, for FX
                objParam = objCommand.Parameters.Add("@d_FXMerchRate", SqlDbType.Decimal, 18)       'Added, 28 Aug 2014, for FX

                'Added, 3 Sept 2014
                szTxnAmount = st_PayInfo.szBaseTxnAmount        'Stores original TxnAmount in TxnAmount column while FXTxnAmount stored in TB_PayFX
                szCurrencyCode = st_PayInfo.szBaseCurrencyCode
            Else
                szTxnAmount = st_PayInfo.szTxnAmount
                szCurrencyCode = st_PayInfo.szCurrencyCode
            End If

            'Added, 8 Apr 2015, for BNB
            objParam = objCommand.Parameters.Add("@sz_TokenType", SqlDbType.VarChar, 3)
            objParam = objCommand.Parameters.Add("@sz_Token", SqlDbType.VarChar, 50)

            'Added  14 Apr 2015, For OTC
            'objParam = objCommand.Parameters.Add("@i_OTCSecureCode", SqlDbType.BigInt)         'Commented on 7 Oct 2016, 711
            objParam = objCommand.Parameters.Add("@sz_OTCSecureCode", SqlDbType.VarChar, 20)    '711, 8 Apr 2016, changed Data type from BIGINT to VARCHAR
            objParam = objCommand.Parameters.Add("@sz_ReqTime", SqlDbType.VarChar, 23)
            objParam = objCommand.Parameters.Add("@sz_DueTime", SqlDbType.VarChar, 23)

            'Added, 24 Nov 2015, Param67
            objParam = objCommand.Parameters.Add("@sz_Param6", SqlDbType.VarChar, 50)
            objParam = objCommand.Parameters.Add("@sz_Param7", SqlDbType.VarChar, 50)

            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_GatewayTxnID").Value = st_PayInfo.szTxnID
            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@sz_MerchantName").Value = st_PayInfo.szMerchantName
            objCommand.Parameters("@sz_MerchantReturnURL").Value = st_PayInfo.szMerchantReturnURL
            objCommand.Parameters("@sz_MerchantSupportURL").Value = st_PayInfo.szMerchantSupportURL
            objCommand.Parameters("@sz_MerchantTxnID").Value = st_PayInfo.szMerchantTxnID
            objCommand.Parameters("@sz_TxnAmount").Value = szTxnAmount                          'Modified 3 Sept 2014, use szTxnAmount instead of st_PayInfo.szTxnAmount
            objCommand.Parameters("@d_TxnAmount").Value = Convert.ToDecimal(szTxnAmount)        'Added on 24 Jun 2010
            objCommand.Parameters("@sz_BaseTxnAmount").Value = st_PayInfo.szBaseTxnAmount       'Added on 3 Aug 2009, multi currency booking confirmation enhancement
            objCommand.Parameters("@sz_CurrencyCode").Value = szCurrencyCode                    'Modified 3 Sept 2014, use szCurrencyCode instead of st_PayInfo.szCurrencyCode
            objCommand.Parameters("@sz_BaseCurrencyCode").Value = st_PayInfo.szBaseCurrencyCode 'Added on 3 Aug 2009, multi currency booking confirmation enhancement
            objCommand.Parameters("@sz_GatewayID").Value = st_PayInfo.szGatewayID
            objCommand.Parameters("@sz_TxnType").Value = st_PayInfo.szTxnType
            objCommand.Parameters("@sz_OrderDesc").Value = st_PayInfo.szMerchantOrdDesc
            objCommand.Parameters("@sz_CardPAN").Value = st_PayInfo.szCardPAN
            objCommand.Parameters("@sz_MaskedCardPAN").Value = st_PayInfo.szMaskedCardPAN
            objCommand.Parameters("@sz_OrderNumber").Value = st_PayInfo.szMerchantOrdID
            objCommand.Parameters("@sz_HashMethod").Value = st_PayInfo.szHashMethod

            'Added on 26 May 2009, to store the maximum length
            If (st_PayInfo.szHashValue.Length > 200) Then
                st_PayInfo.szHashValue = st_PayInfo.szHashValue.Substring(0, 200)
            End If
            objCommand.Parameters("@sz_HashValue").Value = st_PayInfo.szHashValue

            objCommand.Parameters("@i_HostID").Value = st_HostInfo.iHostID
            objCommand.Parameters("@sz_IssuingBank").Value = st_PayInfo.szIssuingBank
            objCommand.Parameters("@sz_LanguageCode").Value = st_PayInfo.szLanguageCode
            objCommand.Parameters("@sz_CustEmail").Value = st_PayInfo.szCustEmail           'Added, 16 Dec 2013
            objCommand.Parameters("@sz_CustName").Value = st_PayInfo.szCustName             'Added, 18 Feb 2014
            objCommand.Parameters("@sz_CustPhone").Value = st_PayInfo.szCustPhone           'Added, 17 Apr 2014

            If (st_PayInfo.szMerchantSessionID <> "") Then
                objCommand.Parameters("@sz_SessionID").Value = st_PayInfo.szMerchantSessionID   'Modified 15 Jul 2014, added MerchantSessionID
            Else
                objCommand.Parameters("@sz_SessionID").Value = st_PayInfo.szSessionID
            End If

            objCommand.Parameters("@sz_Channel").Value = ""
            objCommand.Parameters("@sz_Param1").Value = st_PayInfo.szParam1
            objCommand.Parameters("@sz_Param2").Value = st_PayInfo.szParam2
            objCommand.Parameters("@sz_Param3").Value = st_PayInfo.szParam3
            objCommand.Parameters("@sz_Param4").Value = st_PayInfo.szParam4
            objCommand.Parameters("@sz_Param5").Value = st_PayInfo.szParam5
            objCommand.Parameters("@sz_MachineID").Value = st_PayInfo.szMachineID

            'Assigns the system date of the App Server to szTxnDateTime so that the datetime value sent to the bank
            'in payment request will be the same as the datetime value (retrieved from database) sent to the bank in
            'query request because this system date is inserted into database and not the database system date (GETDATE()).
            st_PayInfo.szTxnDateTime = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
            objCommand.Parameters("@sz_DateCreated").Value = st_PayInfo.szTxnDateTime
            objCommand.Parameters("@i_DateCreated").Value = Convert.ToInt32(Left(st_PayInfo.szTxnDateTime, 10).Replace("-", ""))    'Added on 24 Jun 2010

            objCommand.Parameters("@i_TxnTimeOut").Value = st_HostInfo.iTimeOut 'seconds

            'Start - Added, 4 Jul 2014, Firefly FF
            objCommand.Parameters("@sz_MerchantApprovalURL").Value = st_PayInfo.szMerchantApprovalURL
            objCommand.Parameters("@sz_MerchantUnApprovalURL").Value = st_PayInfo.szMerchantUnApprovalURL
            'End
            'Added  16 Oct 2014
            objCommand.Parameters("@sz_MerchantCallbackURL").Value = st_PayInfo.szMerchantCallbackURL

            'Start - Added, 28 Aug 2014, for FX
            If (st_PayInfo.szFXCurrencyCode <> "") Then
                objCommand.Parameters("@sz_FXCurrencyCode").Value = st_PayInfo.szFXCurrencyCode
                objCommand.Parameters("@d_FXTxnAmt").Value = Convert.ToDecimal(st_PayInfo.szFXTxnAmount)
                objCommand.Parameters("@d_FXCurrOriRate").Value = Convert.ToDecimal(st_PayInfo.szFXCurrencyOriRate)
                objCommand.Parameters("@d_FXCurrMarkUp").Value = Convert.ToDecimal(st_PayInfo.szFXCurrencyMarkUp)
                objCommand.Parameters("@d_FXMerchRate").Value = st_PayInfo.dMerchFXRate
            End If
            'End

            'Added, 8 Apr 2015, for BNB
            objCommand.Parameters("@sz_TokenType").Value = st_PayInfo.szTokenType
            objCommand.Parameters("@sz_Token").Value = st_PayInfo.szToken

            'Added  14 Apr 2015, for OTC
            'Modified 15 Apr 2015, added checking of szOTCSecureCode
            If (st_PayInfo.szOTCSecureCode <> "") Then
                'objCommand.Parameters("@i_OTCSecureCode").Value = Convert.ToInt64(st_PayInfo.szOTCSecureCode)  'Commented on 7 Oct 2016, 711
                objCommand.Parameters("@sz_OTCSecureCode").Value = st_PayInfo.szOTCSecureCode   '711, 8 Apr 2016, changed Data type from BIGINT to VARCHAR
                objCommand.Parameters("@sz_ReqTime").Value = st_PayInfo.szReqTime
                objCommand.Parameters("@sz_DueTime").Value = st_PayInfo.szDueTime
            End If

            'Added, 24 Nov 2015, Param67
            objCommand.Parameters("@sz_Param6").Value = st_PayInfo.szParam6
            objCommand.Parameters("@sz_Param7").Value = st_PayInfo.szParam7

            objConn.Open()
            iRowsAffected = objCommand.ExecuteNonQuery()

            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True
            Else
                bReturn = False

                st_PayInfo.szErrDesc = "bInsertNewTxn() error: Failed to insert new transaction. GatewayTxnID(" + st_PayInfo.szTxnID + ")"

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.E_FAILED_STORE_TXN
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT                   'Common.TXN_STATE.FAILED
                st_PayInfo.szTxnMsg = Common.C_FAILED_STORE_TXN_906
            End If

        Catch ex As Exception
            st_PayInfo.szErrDesc = "bInsertNewTxn() exception: " + ex.ToString + " > " + "GatewayTxnID(" + st_PayInfo.szTxnID + ")"

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT                    'Common.TXN_STATE.FAILED
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProcOB
    ' Function Name:	bGetMerchant
    ' Function Type:	Boolean.  True if get merchant information successfully else false
    ' Parameter:		st_PayInfo, sz_DBConnStr
    ' Description:		Get merchant information
    ' History:			20 Oct 2008
    '********************************************************************************************
    Public Function bGetMerchant(ByRef st_PayInfo As Common.STPayInfo) As Boolean

        Dim bReturn As Boolean = True
        Dim objConn As SqlConnection
        Dim objCommand As SqlCommand
        Dim objParam As SqlParameter
        Dim dr As SqlDataReader
        Dim iRowsAffected As Integer = 0

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetMerchant", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True

                'Chg Start JOYCE 20190402, chg string to secure string
                'Dim szMerchantPassword As String = ""
                Dim szMerchantPassword As SecureString = New SecureString()
                'Chg E n d JOYCE 20190402, chg string to secure string

                While dr.Read()
                    'Modified Start JOYCE 20190402, chg string to secure string
                    'szMerchantPassword = dr("MerchantPassword").ToString()
                    szMerchantPassword = New NetworkCredential("", dr("MerchantPassword").ToString()).SecurePassword    ' MerchantPassword
                    'Modified E n d JOYCE 20190402, chg string to secure string
                    st_PayInfo.szPaymentTemplate = dr("PaymentTemplate").ToString()     ' PaymentTemplate
                    st_PayInfo.szErrorTemplate = dr("ErrorTemplate").ToString()         ' ErrorTemplate
                    st_PayInfo.iAllowReversal = Convert.ToInt32(dr("AllowReversal"))    ' AllowReversal
                    st_PayInfo.iNeedAddOSPymt = Convert.ToInt32(dr("NeedAddOSPymt"))    ' NeedAddOSPymt
                    st_PayInfo.szMerchantName = dr("MerchantName").ToString()           ' Merchant Name - Added on 4 Apr 2010
                    st_PayInfo.iTxnExpired_S = Convert.ToInt32(dr("TxnExpired_S"))      ' Added, 16 Dec 2013, after this time, if query still failed, txn will be finalized
                    dr.Read()
                End While

                'Modified Start JOYCE 20190402, as szMerchantPassword from string chg to secure string
                'If (szMerchantPassword <> "") Then
                If (New NetworkCredential("", szMerchantPassword).Password <> "") Then

                    'Commented on 11 Apr 2010, this DecryptString will return empty string!
                    'If szMerchantPassword.Length > 8 Then ' Merchant Password less than 8 characters in DB is clear
                    '    Dim objCrypto As NMXEncInterop.CryptoClass
                    '    Dim szKey As String
                    '    Dim iRet As Short = 0

                    '    Try
                    '        objCrypto = New NMXEncInterop.CryptoClass
                    '        szKey = "39333139623039623232323034353038"
                    '        st_PayInfo.szMerchantPassword = objCrypto.DecryptString(szMerchantPassword, szKey, 3, iRet)

                    '    Catch ex As Exception
                    '        bReturn = False

                    '        st_PayInfo.szErrDesc = "bGetMerchant() NMXEncInterop exception: " + ex.Message

                    '        st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                    '        st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                    '        st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
                    '        st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                    '    Finally
                    '        If Not IsNothing(objCrypto) Then objCrypto = Nothing
                    '    End Try
                    'End If

                    'If st_PayInfo.szMerchantPassword.Length <= 0 Then
                    '    st_PayInfo.szMerchantPassword = szMerchantPassword
                    'End If

                    'Modified the above  17 Apr 2014, will hit exception because st_PayInfo.szMerchantPassword is empty, when .length, will hit exception
                    'Modified Start JOYCE 04 Apr 2019, chg string to secure string
                    'st_PayInfo.szMerchantPassword = szMerchantPassword
                    st_PayInfo.szMerchantPassword = New NetworkCredential("", szMerchantPassword).Password
                    'Modified E n d JOYCE 04 Apr 2019, chg string to secure string
                Else
                    bReturn = False

                    st_PayInfo.szErrDesc = "bGetMerchant() error: Invalid MerchantID/Merchant."

                    st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.E_INVALID_MERCHANT
                    st_PayInfo.szTxnMsg = Common.C_INVALID_MID_2901
                    st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
                End If
            Else
                bReturn = False

                st_PayInfo.szErrDesc = "bGetMerchant() error: Invalid MerchantID/Merchant."

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.E_INVALID_MERCHANT
                st_PayInfo.szTxnMsg = Common.C_INVALID_MID_2901
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetMerchant() exception: " + ex.StackTrace + " " + ex.Message  ' .Message

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProcOB
    ' Function Name:	bGetHostInfo
    ' Function Type:	Boolean.  True if get host information successfully else false
    ' Parameter:		st_HostInfo, st_PayInfo, sz_DBConnStr
    ' Description:		Get host information
    ' History:			22 Oct 2008
    '********************************************************************************************
    Public Function bGetHostInfo(ByRef st_HostInfo As Common.STHost, ByRef st_PayInfo As Common.STPayInfo) As Boolean

        Dim bReturn As Boolean = True
        Dim objConn As SqlConnection
        Dim objCommand As SqlCommand
        Dim objParam As SqlParameter
        Dim dr As SqlDataReader
        Dim iRowsAffected As Integer = 0

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetHostInfo", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_HostName", SqlDbType.VarChar, 20)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_HostName").Value = st_PayInfo.szIssuingBank

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True

                'HostID, HostName, PaymentTemplate, NeedReplyAcknowledgement, Require2ndEntry, AllowReversal,
                'ChannelID, [Timeout], TxnStatusActionID, HostReplyMethod, OSPymtCode, DateActivated, DateDeactivated, RecStatus, [Desc], PayURL, CfgFile
                While dr.Read()
                    st_HostInfo.iHostID = Convert.ToInt32(dr("HostID"))                                     ' HostID
                    st_HostInfo.szPaymentTemplate = dr("PaymentTemplate").ToString()                        ' PaymentTemplate
                    st_HostInfo.szSecondEntryTemplate = dr("SecondEntryTemplate").ToString()                ' SecondEntryTemplate
                    st_HostInfo.szMesgTemplate = dr("MesgTemplate").ToString()                              ' Message Template - Added on 27 Oct 2009
                    st_HostInfo.iRequire2ndEntry = Convert.ToInt32(dr("Require2ndEntry"))                   ' Require2ndEntry
                    st_HostInfo.szHostTemplate = dr("CfgFile").ToString()                                   ' CfgFile
                    st_HostInfo.iNeedReplyAcknowledgement = Convert.ToInt32(dr("NeedReplyAcknowledgement")) ' NeedReplyAcknowledgement
                    st_HostInfo.iNeedRedirectOTP = Convert.ToInt32(dr("NeedRedirectOTP"))                   ' NeedRedirectOTP - Added on 21 Mar 2010
                    st_HostInfo.iTxnStatusActionID = Convert.ToInt32(dr("TxnStatusActionID"))               ' TxnStatusActionID
                    st_HostInfo.iHostReplyMethod = Convert.ToInt32(dr("HostReplyMethod"))                   ' HostReplyMethod
                    st_HostInfo.szOSPymtCode = dr("OSPymtCode").ToString()                                  ' OSPymtCode
                    st_HostInfo.iGatewayTxnIDFormat = Convert.ToInt32(dr("GatewayTxnIDFormat"))             ' GatewayTxnIDFormat
                    st_HostInfo.iTimeOut = Convert.ToInt32(dr("Timeout"))                                   ' Timeout
                    st_HostInfo.iChannelTimeOut = Convert.ToInt32(dr("ChannelTimeout"))                     ' Timeout - Added, 18 Nov 2015, MOTO, cater for receive timeout between PG and MPG
                    st_HostInfo.iOTCRevTimeOut = Convert.ToInt32(dr("OTCRevTimeout"))                       ' OTC Reversal Timeout - Added  24 Jul 2015
                    st_HostInfo.szHashMethod = dr("HashMethod").ToString()                                  ' Hash Method
                    st_HostInfo.szReturnURL = dr("ReturnURL").ToString()                                    ' Gateway Return URL - Added 15 Oct 2015, for BancNet
                    st_HostInfo.szReturnIPAddresses = dr("ReturnIPAddresses").ToString()                    ' Return IP Addresses
                    st_HostInfo.iQueryFlag = Convert.ToInt32(dr("QueryFlag"))
                    st_HostInfo.iIsActive = Convert.ToInt32(dr("IsActive"))
                    st_HostInfo.iAllowQuery = Convert.ToInt32(dr("AllowQuery"))                             ' AllowQuery - Added  11 Jun 2014. Firefly FFCTB
                    st_HostInfo.iAllowReversal = Convert.ToInt32(dr("AllowReversal"))                       ' AllowReversal - Added on 10 Apr 2010
                    st_HostInfo.szDesc = dr("Desc").ToString()                                              ' Host Description - Added on 9 Dec 2013
                    st_HostInfo.szProtocol = dr("Protocol").ToString()                                      ' Security Protocol, e.g. SSL3, TLS, TLS1.1, TLS1.2 - Added, 21 Jun 2015
                    st_HostInfo.szFixedCurrency = dr("FixedCurrency").ToString()                            ' Fixed Transaction Currency - Added, 25 Sept 2015, for Bitnet
                    st_HostInfo.szLogoPath = dr("LogoPath").ToString()                                      ' Bank logo name - Added 23 Oct 2015, for BancNet redirect_frame.htm
                    st_HostInfo.szMID = dr("MID").ToString()                'Added  17 June 2016. DOKU. Note*** szMID will be overwrite if bGetTerminal funtion call. No affect for current DOKU flow if szMID been overwrite because it is same value.
                    'Commented the following due to stored procedure updated on 15 June 2009 because the following fields should be
                    'host-merchant-based, Not host-based only, therefore, needs to be stored in Terminals table instead of Host table
                    'st_HostInfo.szSendKey = dr("MerchantPassword").ToString()                              ' Merchant Password (Send Key)
                    'st_HostInfo.szReturnKey = dr("ReturnKey").ToString()                                   ' Return Key
                    'st_HostInfo.szSecretKey = dr("SecretKey").ToString()                                   ' Encryption Key 
                    'st_HostInfo.szInitVector = dr("InitVector").ToString()                                 ' Initialization Vector

                    ''Reopen for Host base key using. Eg.UOBTH.  25 May 2012
                    If ("" <> dr("SecretKey").ToString()) Then  ' Added checking on 1 Oct 2010
                        If (True <> IsNumeric(dr("SecretKey").ToString())) Then
                            st_HostInfo.szSecretKey = dr("SecretKey").ToString()
                        Else
                            st_HostInfo.iRunningNoUniquePerDay = Convert.ToInt32(dr("SecretKey"))               ' Boolean for whether Trace Number/Running Number is unique per day, reuse SecretKey which had been moved from Host to Terminals table - Added on 18 Sept 2010
                        End If
                    End If
                    If ("" <> dr("InitVector").ToString()) Then  ' Added checking on 1 Oct 2010
                        st_HostInfo.iNoOfRunningNo = Convert.ToInt32(dr("InitVector"))                      ' Number of digits for Trace Number/Running Number, reuse InitVector which had been moved from Host to Terminals table - Added on 18 Sept 2010
                    End If

                    st_HostInfo.iLogRes = Convert.ToInt32(dr("LogRes"))     'Added  23 Oct 2014. Firefly FFAMBANK
                    st_HostInfo.szQueryURL = dr("QueryURL").ToString()      'Added19 Jan 2016, for BancNet

                    ' Added 711, 8 Apr 2016, To Specify Wether OTC Secure Code Generated by eGHL(1) or Host(2)
                    If Not dr("OTCGenMethod") Is DBNull.Value Then
                        st_HostInfo.iOTCGenMethod = Convert.ToInt32(dr("OTCGenMethod"))
                    End If

                    dr.Read()
                End While
            Else
                st_PayInfo.szErrDesc = "bGetHostInfo() error: Failed to get Host Info" + " > IssuingBank(" + st_PayInfo.szIssuingBank + ")"

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR 'Common.ERROR_CODE.E_INVALID_MID
                st_PayInfo.szTxnMsg = Common.C_INVALID_HOST_902
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT

                bReturn = False
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetHostInfo() exception: " + ex.StackTrace + " " + ex.Message  ' .Message

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR 'Common.ERROR_CODE.E_INVALID_MID
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProcOB
    ' Function Name:	bGetHostCountry
    ' Function Type:	Boolean.  True if get countries belong to Hosts supported by the merchant, else false
    ' Parameter:		st_HostInfo, st_PayInfo
    ' Description:		Get countries belong to the hosts supported by the merchant
    ' History:			6 Dec 2013
    '********************************************************************************************
    Public Function bGetHostCountry(ByVal st_HostInfo As Common.STHost, ByRef st_PayInfo As Common.STPayInfo) As Boolean

        Dim bReturn As Boolean = True
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRowsAffected As Integer = 0

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetHostCountry", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@i_HCProfileID", SqlDbType.Int)               'Added, 5 Dec 2013
            objParam = objCommand.Parameters.Add("@sz_HostName", SqlDbType.VarChar, 20)
            objParam = objCommand.Parameters.Add("@sz_CurrencyCode", SqlDbType.Char, 3)         'Uncommented  4 Apr 2014
            objParam = objCommand.Parameters.Add("@sz_PymtMethod", SqlDbType.VarChar, 3)           'Added  14 Apr 2015, for OTC
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@i_HCProfileID").Value = st_PayInfo.iHCProfileID             'Added, 5 Dec 2013
            objCommand.Parameters("@sz_HostName").Value = st_PayInfo.szIssuingBank              'Can be empty or not empty, depending on whether it is submitted by merchant in payment request

            'Added, 18 Aug 2014, added checking of AllowFX to load ob banks registered regardless of original currency received from merchant's payment request
            If (0 = st_PayInfo.iAllowFX) Then
                objCommand.Parameters("@sz_CurrencyCode").Value = st_PayInfo.szCurrencyCode     'Uncommented  4 Apr 2014
            Else
                objCommand.Parameters("@sz_CurrencyCode").Value = ""                            'Added, 18 Aug 2014
            End If

            objCommand.Parameters("@sz_PymtMethod").Value = st_PayInfo.szPymtMethod             'Added  14 Apr 2015, for OTC

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True

                'CountryName, CountryCodeA2
                st_PayInfo.szHTML = ""
                While dr.Read()
                    st_PayInfo.szHTML += dr("CountryCodeA2").ToString() + "|" + dr("CountryName").ToString() + ";"
                End While
                If (st_PayInfo.szHTML <> "") Then
                    st_PayInfo.szHTML = Left(st_PayInfo.szHTML, st_PayInfo.szHTML.Length() - 1)
                Else
                    'Modified 2 Jul 2015, added extra logging beside Merchant ID
                    st_PayInfo.szErrDesc = "bGetHostCountry() error: dr.Read() Failed" + " > MerchantID(" + st_PayInfo.szMerchantID + ") HCProfileID(" + st_PayInfo.iHCProfileID.ToString() + ") AllowFX(" + st_PayInfo.iAllowFX.ToString() + ") Method(" + st_PayInfo.szPymtMethod + ") AcqBank(" + st_PayInfo.szIssuingBank + ") CurrencyCode(" + st_PayInfo.szCurrencyCode + ")"

                    st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                    st_PayInfo.szTxnMsg = Common.C_INVALID_HOST_902
                    st_PayInfo.iTxnState = Common.TXN_STATE.ABORT

                    bReturn = False
                End If
            Else
                st_PayInfo.szErrDesc = "bGetHostCountry() error: Failed to get Host Country" + " > MerchantID(" + st_PayInfo.szMerchantID + ")"

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.szTxnMsg = Common.C_INVALID_HOST_902
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT

                bReturn = False
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetHostCountry() exception: " + ex.StackTrace + " " + ex.Message

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProcOB
    ' Function Name:	bGetHostLogo
    ' Function Type:	Boolean.  True if get logoes of hosts that support the currency successfully else false
    ' Parameter:		st_HostInfo, st_PayInfo
    ' Description:		Get logoes of hosts that support the currency
    ' History:			7 Dec 2013
    '                   Modified 14 Apr 2015, added optional param sz_CurrencyCode
    '********************************************************************************************
    Public Function bGetHostLogo(ByVal st_HostInfo As Common.STHost, ByRef st_PayInfo As Common.STPayInfo, Optional ByVal sz_CurrencyCode As String = "") As Boolean

        Dim bReturn As Boolean = True
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRowsAffected As Integer = 0
        Dim szHostName As String = ""

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetHostLogo", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@i_HCProfileID", SqlDbType.Int)           'Added, 7 Dec 2013
            objParam = objCommand.Parameters.Add("@sz_HostName", SqlDbType.VarChar, 20)
            objParam = objCommand.Parameters.Add("@sz_CurrencyCode", SqlDbType.Char, 3)
            objParam = objCommand.Parameters.Add("@sz_PymtMethod", SqlDbType.VarChar, 3)    'Added  14 Apr 2015, for OTC
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@i_HCProfileID").Value = st_PayInfo.iHCProfileID         'Added, 7 Dec 2013
            objCommand.Parameters("@sz_HostName").Value = st_PayInfo.szIssuingBank

            'Modified 14 Apr 2015, added checking of sz_CurrencyCode
            If (sz_CurrencyCode <> "") Then
                objCommand.Parameters("@sz_CurrencyCode").Value = sz_CurrencyCode           'Added, 14 Apr 2015
            ElseIf (0 = st_PayInfo.iAllowFX And st_PayInfo.szCurrencyCode <> "") Then       'Added, 25 Aug 2017, to support showing only the host that supports the processing currency
                objCommand.Parameters("@sz_CurrencyCode").Value = st_PayInfo.szCurrencyCode 'Added, 14 Apr 2015
            Else
                objCommand.Parameters("@sz_CurrencyCode").Value = ""                        'Modified 10 Nov 2013, since can be different currency then conversion is required
            End If

            objCommand.Parameters("@sz_PymtMethod").Value = st_PayInfo.szPymtMethod         'Added  14 Apr 2015, for OTC

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True

                'HostName, HostDesc, LogoPath, CurrencyCode, CountryName, CountryCodeA2
                st_PayInfo.szHTML = ""
                While dr.Read()
                    'Modified 15 Oct 2015, for AllDebit, modified dr("LogoPath").ToString() to Replace(dr("LogoPath").ToString(), "[CURRENCYCODE]", dr("CurrencyCode").ToString())
                    'Added  2 Aug 2016. HostGroup FPX2D
                    If (dr("BankID").ToString() <> "") Then
                        szHostName = dr("HostName").ToString() + "_" + dr("BankID").ToString()
                        st_PayInfo.szHTML += szHostName + "|" + dr("BankDesc").ToString() + "|" + Replace(dr("HGLogoPath").ToString(), "[CURRENCYCODE]", dr("CurrencyCode").ToString()) + "|" + dr("CurrencyCode").ToString() + "|" + dr("CountryName").ToString() + "|" + dr("CountryCodeA2").ToString() + "|" + dr("FloorAmt").ToString() + "|" + dr("CeilingAmt").ToString() + ";"
                    Else
                        st_PayInfo.szHTML += dr("HostName").ToString() + "|" + dr("HostDesc").ToString() + "|" + Replace(dr("LogoPath").ToString(), "[CURRENCYCODE]", dr("CurrencyCode").ToString()) + "|" + dr("CurrencyCode").ToString() + "|" + dr("CountryName").ToString() + "|" + dr("CountryCodeA2").ToString() + "|" + dr("FloorAmt").ToString() + "|" + dr("CeilingAmt").ToString() + ";"
                    End If
                End While
                If (st_PayInfo.szHTML <> "") Then
                    st_PayInfo.szHTML = Left(st_PayInfo.szHTML, st_PayInfo.szHTML.Length() - 1) 'Exclude the rightmost ";"
                Else
                    st_PayInfo.szErrDesc = "bGetHostLogo() error: dr.Read() Failed" + " > MerchantID(" + st_PayInfo.szMerchantID + ") HostName(" + st_PayInfo.szIssuingBank + ") CurrencyCode(" + st_PayInfo.szCurrencyCode + ")"

                    st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                    st_PayInfo.szTxnMsg = Common.C_INVALID_HOST_902
                    st_PayInfo.iTxnState = Common.TXN_STATE.ABORT

                    bReturn = False
                End If
            Else
                st_PayInfo.szErrDesc = "bGetHostLogo() error: Failed to get Host Logo" + " > MerchantID(" + st_PayInfo.szMerchantID + ") HostName(" + st_PayInfo.szIssuingBank + ") CurrencyCode(" + st_PayInfo.szCurrencyCode + ")"

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.szTxnMsg = Common.C_INVALID_HOST_902
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT

                bReturn = False
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetHostLogo() exception: " + ex.StackTrace + " " + ex.Message

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProcOB
    ' Function Name:	bGetHostCurrency
    ' Function Type:	Boolean.  True if get currency of host based on country successfully else false
    ' Parameter:		st_HostInfo, st_PayInfo
    ' Description:		Get currency of host
    ' History:			25 Aug 2014
    '********************************************************************************************
    Public Function bGetHostCurrency(ByRef st_PayInfo As Common.STPayInfo) As Boolean

        Dim bReturn As Boolean = True
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRowsAffected As Integer = 0

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetHostCurrency", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@i_HCProfileID", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@sz_HostName", SqlDbType.VarChar, 20)
            objParam = objCommand.Parameters.Add("@sz_CountryCodeA2", SqlDbType.Char, 2)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@i_HCProfileID").Value = st_PayInfo.iHCProfileID
            objCommand.Parameters("@sz_HostName").Value = st_PayInfo.szIssuingBank
            'Modified 19 Jul 2017, to cater for one host (e.g.POLi) supports more than 1 currency but obCountryList param emtpy
            'Modified 21 Jul 2017, added And st_PayInfo.szBaseCurrencyCode <> "" And (st_PayInfo.szBaseCurrencyCode <> st_PayInfo.szCurrencyCode)) or else will affect Firefly ALIPAYUSD txns
            If ("" = st_PayInfo.szOBCountryCode And st_PayInfo.szBaseCurrencyCode <> "" And (st_PayInfo.szBaseCurrencyCode <> st_PayInfo.szCurrencyCode)) Then
                objCommand.Parameters("@sz_CountryCodeA2").Value = Left(st_PayInfo.szCurrencyCode, 2)
            Else
                objCommand.Parameters("@sz_CountryCodeA2").Value = st_PayInfo.szOBCountryCode
            End If

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True

                If dr.Read() Then   'Added, 29 Aug 2014, dr.Read()
                    'HostName, HostDesc, LogoPath, CurrencyCode, CountryName, CountryCodeA2
                    st_PayInfo.szHostCurrencyCode = dr("CurrencyCode").ToString()
                    st_PayInfo.szHostCountryName = dr("CountryName").ToString()
                    st_PayInfo.iGMTDiffMins = Convert.ToInt32(dr("GMTDiffMins"))    ' Added OTC, 21 Feb 2017, to get host country local time
                End If
            Else
                st_PayInfo.szErrDesc = "bGetHostCurrency() error: Failed to get Host Currency" + " > MerchantID(" + st_PayInfo.szMerchantID + ") HostName(" + st_PayInfo.szIssuingBank + ") CountryCode(" + st_PayInfo.szOBCountryCode + ")"

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.szTxnMsg = Common.C_INVALID_HOST_902
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT

                bReturn = False
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetHostCurrency() exception: " + ex.StackTrace + " " + ex.Message

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProcOB
    ' Function Name:	bGetTerminal
    ' Function Type:	Boolean.  True if get terminal and channel information successfully else false
    ' Parameter:		st_HostInfo, st_PayInfo, sz_DBConnStr
    ' Description:		Get terminal information
    ' History:			27 Oct 2008
    '********************************************************************************************
    Public Function bGetTerminal(ByRef st_HostInfo As Common.STHost, ByRef st_PayInfo As Common.STPayInfo) As Boolean

        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection
        Dim objCommand As SqlCommand
        Dim objParam As SqlParameter
        Dim dr As SqlDataReader
        Dim iRowsAffected As Integer = 0
        Dim szCurrencyCode As String = ""       'Added, 26 Aug 2014, for FX

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetTerminal", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_CurrencyCode", SqlDbType.Char, 3)
            objParam = objCommand.Parameters.Add("@sz_AirlineCode", SqlDbType.VarChar, 3)
            objParam = objCommand.Parameters.Add("@i_HostID", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID

            'Added, 26 Sept 2014, just in case FXCurrencyCode got space
            st_PayInfo.szFXCurrencyCode = Trim(st_PayInfo.szFXCurrencyCode)

            'Modified 26 Aug 2014, for FX, added getting Terminal by FX Currency Code
            If ("" = st_PayInfo.szFXCurrencyCode) Then
                szCurrencyCode = st_PayInfo.szCurrencyCode
            Else
                szCurrencyCode = st_PayInfo.szFXCurrencyCode
            End If

            'Added, 18 Aug 2015, checking of WA
            If ("WA" = st_PayInfo.szPymtMethod) Then
                If ("BTC" = st_PayInfo.szIssuingBank.ToUpper()) Then
                    szCurrencyCode = "BTC"  'Modified 15 Sept 2015, for Bitnet
                End If
            End If

            objCommand.Parameters("@sz_CurrencyCode").Value = szCurrencyCode    'Modified 26 Aug 

            objCommand.Parameters("@sz_AirlineCode").Value = "" 'Empty for the moment
            objCommand.Parameters("@i_HostID").Value = st_HostInfo.iHostID  ' Retrieved in bGetHostInfo based on IssuingBank

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If objCommand.Parameters("@i_Return").Value = 0 Then
                '14 May 2009, ExecuteReader will not be able to get the exact Return value by stored procedure
                'will always get 0, that's why commented the following and put bReturn = True in while dr.Read()
                'bReturn = True

                'TerminalID, MID, TID, PayURL, QueryURL, ReversalURL, ExtraURL, CfgFile, ReturnURL, Timeout
                While dr.Read()
                    bReturn = True
                    'Modified 8 Aug 2016, added checking of MID to avoid MID, retrieved from Host table from bGetHostInfo which is called b4 bGetTerminal, got overwritten by MID from Terminal table, for Doku
                    If ("" = st_HostInfo.szMID) Then
                        st_HostInfo.szMID = dr("MID").ToString()                            ' MID
                    End If
                    st_HostInfo.szTID = dr("TID").ToString()                                ' TID
                    st_HostInfo.szURL = dr("PayURL").ToString()                             ' Pay URL
                    st_HostInfo.szQueryURL = dr("QueryURL").ToString()                      ' Query URL
                    st_HostInfo.szCancelURL = dr("CancelURL").ToString()                    ' Cancel URL
                    st_HostInfo.szReversalURL = dr("ReversalURL").ToString()                ' Reversal URL
                    st_HostInfo.szAcknowledgementURL = dr("ExtraURL").ToString()            ' ExtraURL - Reply Acknowledgement URL
                    st_HostInfo.iPortNumber = Convert.ToInt32(dr("PayPort"))                ' Pay Port
                    st_HostInfo.iQueryPortNumber = Convert.ToInt32(dr("QueryPort"))         ' Query Port
                    st_HostInfo.iCancelPortNumber = Convert.ToInt32(dr("CancelPort"))       ' Cancel Port
                    st_HostInfo.iReversalPortNumber = Convert.ToInt32(dr("ReversalPort"))   ' Reversal Port
                    st_HostInfo.iAcknowledgementPortNumber = Convert.ToInt32(dr("ExtraPort")) ' Extra Port - Reply Acknowledgement Port
                    st_HostInfo.szHostTemplate = dr("CfgFile").ToString()       ' CfgFile
                    st_HostInfo.szReturnURL = dr("ReturnURL").ToString()        ' ReturnURL
                    st_HostInfo.szReturnURL2 = dr("ReturnURL2").ToString()      ' ReturnURL2 - Added on 22 Oct 2009
                    st_HostInfo.szReturnIPAddresses = dr("ReturnIPAddresses").ToString()
                    st_HostInfo.szAcquirerID = dr("AcquirerID").ToString()
                    'Added the following fields on 15 June 2009 due to stored procedure updated on 15 June 2009 because the following fields should be
                    'host-merchant-based, Not host-based only, therefore, needs to be stored in Terminals table instead of Host table
                    st_HostInfo.szSendKey = dr("SendKey").ToString()                               ' Merchant Password (Send Key)
                    st_HostInfo.szReturnKey = dr("ReturnKey").ToString()                            ' Return Key
                    st_HostInfo.iMerchantHostTimeout = Convert.ToInt32(dr("MerchantHostTimeout"))   ' Added, 4 Jan 2018  

                    'Modified  30 Sept 2013, to support SecretKey using by host or merchant both level
                    'Host's secretkey is use by Paydibs G-Direct with bank
                    'Terminal (Merchant) secretkey is use by Bank Direct where merchant with Bank
                    'Eg. for the use of secretkey is Affin Bank (G-Direct)
                    If ("" <> dr("SecretKey").ToString()) Then 'check for terminal's secretkey is not empty
                        st_HostInfo.szSecretKey = dr("SecretKey").ToString()                                    ' Encryption Key
                    End If

                    st_HostInfo.szInitVector = dr("InitVector").ToString()                                  ' Initialization Vector
                    st_HostInfo.szPayeeCode = dr("PayeeCode").ToString()                                    ' Payee Code
                    st_HostInfo.szChannelCfg1 = dr("ChannelCfg1").ToString()                                ' Added, 1 Sept 2015, Bitnet
                    st_HostInfo.szChannelCfg2 = dr("ChannelCfg2").ToString()                                ' Added, 1 Sept 2015, Bitnet

                    'Added Public and Private key file path on 9 Nov 2009
                    st_PayInfo.szEncryptKeyPath = dr("SendKeyPath").ToString()                              ' Encrypt Key File Path for sending
                    st_PayInfo.szDecryptKeyPath = dr("ReturnKeyPath").ToString()                            ' Decrypt Key File Path for receiving
                    st_PayInfo.szAirlineCode = dr("AirlineCode").ToString()                                 ' Added 29 Jan 2016. Reuse AirlineCode as merAbbr for GPUPOP.
                    dr.Read()
                End While

                If (False = bReturn) Then
                    'Modified 24 Sept 2015, added logging of MerchantID and Method
                    st_PayInfo.szErrDesc = "bGetTerminal() error: Failed to get terminal" + " > HostID(" + st_HostInfo.iHostID.ToString() + ") CurrencyCode(" + szCurrencyCode + ") MerchantID(" + st_PayInfo.szMerchantID + ") Method(" + st_PayInfo.szPymtMethod + ")"

                    st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                    st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                    st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
                End If
            Else
                bReturn = False

                'Modified 24 Sept 2015, added logging of MerchantID and Method
                st_PayInfo.szErrDesc = "bGetTerminal() error: Failed to get terminal" + " > HostID(" + st_HostInfo.iHostID.ToString() + ") CurrencyCode(" + szCurrencyCode + ") MerchantID(" + st_PayInfo.szMerchantID + ") Method(" + st_PayInfo.szPymtMethod + ")"

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            'Modified 24 Sept 2015, added logging of MerchantID and Method
            st_PayInfo.szErrDesc = "bGetTerminal() exception: " + ex.StackTrace + " " + ex.Message + " > HostID(" + st_HostInfo.iHostID.ToString() + ") CurrencyCode(" + szCurrencyCode + ") MerchantID(" + st_PayInfo.szMerchantID + ") Method(" + st_PayInfo.szPymtMethod + ")"

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bGetTerminalEx
    ' Function Type:	Boolean.  True if get terminal and channel information successfully else false
    ' Parameter:		st_HostInfo, st_PayInfo
    ' Description:		Get terminal information by Bank MID
    ' History:			16 Apr 2014
    '********************************************************************************************
    Public Function bGetTerminalEx(ByRef st_HostInfo As Common.STHost, ByRef st_PayInfo As Common.STPayInfo) As Boolean

        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRowsAffected As Integer = 0

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetTerminalEx", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_CurrencyCode", SqlDbType.Char, 3)
            objParam = objCommand.Parameters.Add("@sz_MID", SqlDbType.VarChar, 20)
            objParam = objCommand.Parameters.Add("@i_HostID", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@sz_CurrencyCode").Value = st_PayInfo.szCurrencyCode
            objCommand.Parameters("@sz_MID").Value = st_HostInfo.szMID
            objCommand.Parameters("@i_HostID").Value = st_HostInfo.iHostID  ' Retrieved in bGetHostInfo based on IssuingBank

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If objCommand.Parameters("@i_Return").Value = 0 Then
                '14 May 2009, ExecuteReader will not be able to get the exact Return value by stored procedure
                'will always get 0, that's why commented the following and put bReturn = True in while dr.Read()
                'bReturn = True

                'TerminalID, MID, TID, PayURL, QueryURL, ReversalURL, ExtraURL, CfgFile, ReturnURL, Timeout
                While dr.Read()
                    bReturn = True
                    'st_HostInfo.szMID = dr("MID").ToString()                               ' MID
                    st_HostInfo.szTID = dr("TID").ToString()                                ' TID
                    st_HostInfo.szURL = dr("PayURL").ToString()                             ' Pay URL
                    st_HostInfo.szQueryURL = dr("QueryURL").ToString()                      ' Query URL
                    st_HostInfo.szCancelURL = dr("CancelURL").ToString()                    ' Cancel URL
                    st_HostInfo.szReversalURL = dr("ReversalURL").ToString()                ' Reversal URL
                    st_HostInfo.szAcknowledgementURL = dr("ExtraURL").ToString()            ' ExtraURL - Reply Acknowledgement URL
                    st_HostInfo.iPortNumber = Convert.ToInt32(dr("PayPort"))                ' Pay Port
                    st_HostInfo.iQueryPortNumber = Convert.ToInt32(dr("QueryPort"))         ' Query Port
                    st_HostInfo.iCancelPortNumber = Convert.ToInt32(dr("CancelPort"))       ' Cancel Port
                    st_HostInfo.iReversalPortNumber = Convert.ToInt32(dr("ReversalPort"))   ' Reversal Port
                    st_HostInfo.iAcknowledgementPortNumber = Convert.ToInt32(dr("ExtraPort")) ' Extra Port - Reply Acknowledgement Port
                    st_HostInfo.szHostTemplate = dr("CfgFile").ToString()       ' CfgFile
                    st_HostInfo.szReturnURL = dr("ReturnURL").ToString()        ' ReturnURL
                    st_HostInfo.szReturnURL2 = dr("ReturnURL2").ToString()      ' ReturnURL2 - Added on 22 Oct 2009
                    st_HostInfo.szReturnIPAddresses = dr("ReturnIPAddresses").ToString()
                    st_HostInfo.szAcquirerID = dr("AcquirerID").ToString()
                    'Added the following fields on 15 June 2009 due to stored procedure updated on 15 June 2009 because the following fields should be
                    'host-merchant-based, Not host-based only, therefore, needs to be stored in Terminals table instead of Host table
                    st_HostInfo.szSendKey = dr("SendKey").ToString()                               ' Merchant Password (Send Key)
                    st_HostInfo.szReturnKey = dr("ReturnKey").ToString()                                    ' Return Key
                    st_HostInfo.szSecretKey = dr("SecretKey").ToString()                                    ' Encryption Key
                    st_HostInfo.szInitVector = dr("InitVector").ToString()                                  ' Initialization Vector
                    st_HostInfo.szPayeeCode = dr("PayeeCode").ToString()                                    ' Payee Code
                    'Added Public and Private key file path on 9 Nov 2009
                    st_PayInfo.szEncryptKeyPath = dr("SendKeyPath").ToString()                              ' Encrypt Key File Path for sending
                    st_PayInfo.szDecryptKeyPath = dr("ReturnKeyPath").ToString()                            ' Decrypt Key File Path for receiving

                    dr.Read()
                End While

                If (False = bReturn) Then
                    st_PayInfo.szErrDesc = "bGetTerminalEx() error: Failed to get terminal" + " > HostID(" + st_HostInfo.iHostID.ToString() + ") CurrencyCode(" + st_PayInfo.szCurrencyCode + ") MID(" + st_HostInfo.szMID + ")"

                    st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                    st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                    st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
                End If
            Else
                bReturn = False

                st_PayInfo.szErrDesc = "bGetTerminalEx() error: Failed to get terminal" + " > HostID(" + st_HostInfo.iHostID.ToString() + ") CurrencyCode(" + st_PayInfo.szCurrencyCode + ") MID(" + st_HostInfo.szMID + ")"

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False
            st_PayInfo.szErrDesc = "bGetTerminalEx() exception: " + ex.StackTrace + " " + ex.Message + " > HostID(" + st_HostInfo.iHostID.ToString() + ") CurrencyCode(" + st_PayInfo.szCurrencyCode + ") MID(" + st_HostInfo.szMID + ")"

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProcOB
    ' Function Name:	bUpdateTxnStatus
    ' Function Type:	Boolean.  True if update successful else false
    ' Parameter:		st_PayInfo, st_HostInfo, sz_DBConnStr
    ' Description:		Updates transaction status into database
    ' History:			27 Oct 2008
    '********************************************************************************************
    Public Function bUpdateTxnStatus(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost) As Boolean

        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection
        Dim objCommand As SqlCommand
        Dim objParam As SqlParameter
        Dim dr As SqlDataReader
        Dim iRowsAffected As Integer = 0

        Try
            bReturn = False
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spUpdateTxnStatus", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@i_TxnStatus", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_MerchantTxnStatus", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_TxnState", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_Action", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@sz_BankRespCode", SqlDbType.VarChar, 20) 'Modified 4 to 20 on 12 Oct 2009
            objParam = objCommand.Parameters.Add("@sz_BankRefNo", SqlDbType.VarChar, 40)    'Modified 19 Nov 2015, Bitnet, increase from 30 to 40 to store Invoice Number required for Refund
            objParam = objCommand.Parameters.Add("@sz_OSPymtCode", SqlDbType.VarChar, 3)
            objParam = objCommand.Parameters.Add("@i_OSRet", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_ErrSet", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_ErrNum", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@sz_GatewayTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@i_TxnStatus").Value = st_PayInfo.iTxnStatus
            objCommand.Parameters("@i_MerchantTxnStatus").Value = st_PayInfo.iMerchantTxnStatus
            objCommand.Parameters("@i_TxnState").Value = st_PayInfo.iTxnState
            objCommand.Parameters("@i_Action").Value = st_PayInfo.iAction

            If (st_PayInfo.szRespCode <> "") Then
                objCommand.Parameters("@sz_BankRespCode").Value = st_PayInfo.szRespCode
            Else
                objCommand.Parameters("@sz_BankRespCode").Value = st_PayInfo.szHostTxnStatus
            End If

            If (st_PayInfo.szHostTxnID <> "") Then
                objCommand.Parameters("@sz_BankRefNo").Value = st_PayInfo.szHostTxnID
            Else
                objCommand.Parameters("@sz_BankRefNo").Value = st_PayInfo.szAuthCode
            End If

            objCommand.Parameters("@sz_OSPymtCode").Value = st_HostInfo.szOSPymtCode
            objCommand.Parameters("@i_OSRet").Value = st_PayInfo.iOSRet
            objCommand.Parameters("@i_ErrSet").Value = st_PayInfo.iErrSet
            objCommand.Parameters("@i_ErrNum").Value = st_PayInfo.iErrNum
            objCommand.Parameters("@sz_GatewayTxnID").Value = st_PayInfo.szTxnID

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()
            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True
            Else
                bReturn = False

                st_PayInfo.szErrDesc = "bUpdateTxnStatus() error: Failed to update TxnState(" + st_PayInfo.iTxnState.ToString() + "), TxnStatus(" + st_PayInfo.iTxnStatus.ToString() + "), MerchantTxnStatus(" + st_PayInfo.iMerchantTxnStatus.ToString() + "), Action(" + st_PayInfo.iAction.ToString() + "), BankRefNo(" + st_PayInfo.szHostTxnID + "), OSPymtCode(" + st_HostInfo.szOSPymtCode + "), OSRet(" + st_PayInfo.iOSRet.ToString() + "), ErrSet(" + st_PayInfo.iErrSet.ToString() + "), ErrNum(" + st_PayInfo.iErrNum.ToString() + ") for GatewayTxnID(" + st_PayInfo.szTxnID + ")"

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.iTxnState = Common.TXN_STATE.FAILED
                st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bUpdateTxnStatus() exception: " + ex.ToString + " > " + "TxnState(" + st_PayInfo.iTxnState.ToString() + "), TxnStatus(" + st_PayInfo.iTxnStatus.ToString() + "), MerchantTxnStatus(" + st_PayInfo.iMerchantTxnStatus.ToString() + "), Action(" + st_PayInfo.iAction.ToString() + "), BankRefNo(" + st_PayInfo.szHostTxnID + "), OSPymtCode(" + st_HostInfo.szOSPymtCode + "), OSRet(" + st_PayInfo.iOSRet.ToString() + "), ErrSet(" + st_PayInfo.iErrSet.ToString() + "), ErrNum(" + st_PayInfo.iErrNum.ToString() + ") for GatewayTxnID(" + st_PayInfo.szTxnID + ")"

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.FAILED
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProcOB
    ' Function Name:	bUpdateTxnStatusRes
    ' Function Type:	Boolean.  True if update successful else false
    ' Parameter:		st_PayInfo, st_HostInfo, sz_DBConnStr
    ' Description:		Updates transaction status in Response table
    ' History:			10 Apr 2010
    '********************************************************************************************
    Public Function bUpdateTxnStatusRes(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost) As Boolean

        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection
        Dim objCommand As SqlCommand
        Dim objParam As SqlParameter
        Dim dr As SqlDataReader
        Dim iRowsAffected As Integer = 0

        Try
            bReturn = False
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spUpdateTxnStatusRes", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@i_TxnStatus", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@sz_RespMesg", SqlDbType.VarChar, 255)    ' Added on 11 Apr 2010
            objParam = objCommand.Parameters.Add("@sz_BankRefNo", SqlDbType.VarChar, 40)    ' Added on 11 Apr 2010. 'Modified 19 Nov 2015, Bitnet, increase from 30 to 40 to store Invoice Number required for Refund
            objParam = objCommand.Parameters.Add("@sz_BankRespCode", SqlDbType.VarChar, 20) ' Added on 11 Apr 2010
            objParam = objCommand.Parameters.Add("@sz_GatewayTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@i_TxnStatus").Value = st_PayInfo.iTxnStatus
            objCommand.Parameters("@sz_RespMesg").Value = st_PayInfo.szTxnMsg
            If (st_PayInfo.szHostTxnID <> "") Then
                objCommand.Parameters("@sz_BankRefNo").Value = st_PayInfo.szHostTxnID
            Else
                objCommand.Parameters("@sz_BankRefNo").Value = st_PayInfo.szAuthCode
            End If
            If (st_PayInfo.szRespCode <> "") Then
                objCommand.Parameters("@sz_BankRespCode").Value = st_PayInfo.szRespCode
            Else
                objCommand.Parameters("@sz_BankRespCode").Value = st_PayInfo.szHostTxnStatus
            End If
            objCommand.Parameters("@sz_GatewayTxnID").Value = st_PayInfo.szTxnID

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()
            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True
            Else
                bReturn = False

                st_PayInfo.szErrDesc = "bUpdateTxnStatusRes() error: Failed to update TxnStatus(" + st_PayInfo.iTxnStatus.ToString() + ") for GatewayTxnID(" + st_PayInfo.szTxnID + ")"

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.iTxnState = Common.TXN_STATE.FAILED
                st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bUpdateTxnStatusRes() exception: " + ex.ToString + " > " + "TxnStatus(" + st_PayInfo.iTxnStatus.ToString() + ") for GatewayTxnID(" + st_PayInfo.szTxnID + ")"

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.FAILED
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProcOB
    ' Function Name:	bUpdateTxnParams
    ' Function Type:	Boolean.  True if update successful else false
    ' Parameter:		st_PayInfo, sz_DBConnStr
    ' Description:		Updates transaction details into database
    ' History:			18 Jan 2009
    '********************************************************************************************
    Public Function bUpdateTxnParams(ByRef st_PayInfo As Common.STPayInfo) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection
        Dim objCommand As SqlCommand
        Dim objParam As SqlParameter
        Dim dr As SqlDataReader
        Dim iRet As Integer = 0

        Try
            bReturn = False
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spUpdateTxnParams", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.Char, 30)
            objParam = objCommand.Parameters.Add("@sz_MerchantTxnID", SqlDbType.Char, 30)
            objParam = objCommand.Parameters.Add("@sz_Param1", SqlDbType.VarChar, 100)
            objParam = objCommand.Parameters.Add("@sz_Param2", SqlDbType.VarChar, 100)
            objParam = objCommand.Parameters.Add("@sz_Param3", SqlDbType.VarChar, 100)
            objParam = objCommand.Parameters.Add("@sz_Param4", SqlDbType.VarChar, 100)
            objParam = objCommand.Parameters.Add("@sz_Param5", SqlDbType.VarChar, 100)
            objParam = objCommand.Parameters.Add("@i_TxnState", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_ReqRes", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@sz_MerchantTxnID").Value = st_PayInfo.szMerchantTxnID
            objCommand.Parameters("@sz_Param1").Value = st_PayInfo.szParam1             'Modified 4 Oct 2011, for POLi, or else Param1 which contains space at front will be trimmed and response_POLi not able to GetTxnFromParam based on POLi response
            objCommand.Parameters("@sz_Param2").Value = st_PayInfo.szParam2             'Modified 17 Aug 2011, for BCA, do not trim, or else not able to update Param
            objCommand.Parameters("@sz_Param3").Value = st_PayInfo.szParam3.Trim()
            objCommand.Parameters("@sz_Param4").Value = st_PayInfo.szParam4.Trim()
            objCommand.Parameters("@sz_Param5").Value = st_PayInfo.szParam5.Trim()
            objCommand.Parameters("@i_TxnState").Value = st_PayInfo.iTxnState
            objCommand.Parameters("@i_ReqRes").Value = st_PayInfo.iReqRes

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            iRet = -1
            If dr.Read() Then
                iRet = Convert.ToInt32(dr("RetCode"))
                st_PayInfo.szTxnMsg = dr("RetDesc").ToString()
                st_PayInfo.szIssuingBank = dr("IssuingBank").ToString()
            End If

            If iRet = 0 Then
                bReturn = True
            Else
                bReturn = False

                st_PayInfo.szErrDesc = "bUpdateTxnParams() error: Failed to update Param1(" + st_PayInfo.szParam1 + "), Param2(" + st_PayInfo.szParam2 + "), Param3(" + st_PayInfo.szParam3 + "), Param4(" + st_PayInfo.szParam4 + "), Param5(" + st_PayInfo.szParam5 + ") due to " + st_PayInfo.szTxnMsg

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.iTxnState = Common.TXN_STATE.FAILED
                st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bUpdateTxnParams() exception: " + ex.ToString + " > " + "Param1(" + st_PayInfo.szParam1 + "), Param2(" + st_PayInfo.szParam2 + "), Param3(" + st_PayInfo.szParam3 + "), Param4(" + st_PayInfo.szParam4 + "), Param5(" + st_PayInfo.szParam5 + ")"

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.FAILED
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProcOB
    ' Function Name:	bUpdateTxnParamsRes
    ' Function Type:	Boolean.  True if update successful else false
    ' Parameter:		st_PayInfo, sz_DBConnStr
    ' Description:		Updates params details into database
    ' History:			26 Feb 2016
    '********************************************************************************************
    Public Function bUpdateTxnParamsRes(ByRef st_PayInfo As Common.STPayInfo) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection
        Dim objCommand As SqlCommand
        Dim objParam As SqlParameter
        Dim dr As SqlDataReader
        Dim iRet As Integer = 0

        Try
            bReturn = False
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spUpdateTxnParamsRes", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@i_PKID", SqlDbType.BigInt)
            objParam = objCommand.Parameters.Add("@sz_Param1", SqlDbType.VarChar, 100)
            objParam = objCommand.Parameters.Add("@sz_Param2", SqlDbType.VarChar, 100)
            objParam = objCommand.Parameters.Add("@sz_Param3", SqlDbType.VarChar, 100)
            objParam = objCommand.Parameters.Add("@sz_Param4", SqlDbType.VarChar, 100)
            objParam = objCommand.Parameters.Add("@sz_Param5", SqlDbType.VarChar, 100)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@i_PKID").Value = st_PayInfo.iPKID
            objCommand.Parameters("@sz_Param1").Value = st_PayInfo.szParam1             'Modified 4 Oct 2011, for POLi, or else Param1 which contains space at front will be trimmed and response_POLi not able to GetTxnFromParam based on POLi response
            objCommand.Parameters("@sz_Param2").Value = st_PayInfo.szParam2             'Modified 17 Aug 2011, for BCA, do not trim, or else not able to update Param
            objCommand.Parameters("@sz_Param3").Value = st_PayInfo.szParam3.Trim()
            objCommand.Parameters("@sz_Param4").Value = st_PayInfo.szParam4.Trim()
            objCommand.Parameters("@sz_Param5").Value = st_PayInfo.szParam5.Trim()

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            iRet = -1
            If dr.Read() Then
                iRet = Convert.ToInt32(dr("RetCode"))
            End If

            If iRet = 0 Then
                bReturn = True
            Else
                bReturn = False
                st_PayInfo.szErrDesc = "bUpdateTxnParamsRes() error: Failed to update Param1(" + st_PayInfo.szParam1 + "), Param2(" + st_PayInfo.szParam2 + "), Param3(" + st_PayInfo.szParam3 + "), Param4(" + st_PayInfo.szParam4 + "), Param5(" + st_PayInfo.szParam5 + ") due to " + dr("RetDesc").ToString()

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.iTxnState = Common.TXN_STATE.FAILED
                st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bUpdateTxnParamsRes() exception: " + ex.ToString + " > " + "Param1(" + st_PayInfo.szParam1 + "), Param2(" + st_PayInfo.szParam2 + "), Param3(" + st_PayInfo.szParam3 + "), Param4(" + st_PayInfo.szParam4 + "), Param5(" + st_PayInfo.szParam5 + ")"

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.FAILED
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProcOB
    ' Function Name:	bGetReqTxn
    ' Function Type:	Boolean.  True if unique else false
    ' Parameter:		
    ' Description:		Get request data from database
    ' History:			29 Oct 2008
    '********************************************************************************************
    Public Function bGetReqTxn(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection
        Dim objCommand As SqlCommand
        Dim objParam As SqlParameter
        Dim iRowsAffected As Integer = 0
        Dim dr As SqlDataReader

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetReqTxn", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_GatewayTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_GatewayTxnID").Value = st_PayInfo.szTxnID

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If dr.Read() Then
                st_PayInfo.iQueryStatus = Convert.ToInt32(dr("RetCode"))    'dr.GetInt32(1).ToString
                st_PayInfo.szTxnMsg = dr("RetDesc").ToString()

                'Assign initial value
                st_PayInfo.szTxnType = ""
                st_PayInfo.szMerchantID = ""
                st_PayInfo.szMerchantTxnID = ""
                st_PayInfo.szTxnAmount = ""
                st_PayInfo.szBaseTxnAmount = ""     ' Added on 3 Aug 2009, base currency booking confirmation enhancement
                st_PayInfo.szCurrencyCode = ""
                st_PayInfo.szBaseCurrencyCode = ""  ' Added on 3 Aug 2009, base currency booking confirmation enhancement
                st_PayInfo.szIssuingBank = ""
                st_PayInfo.szLanguageCode = ""
                st_PayInfo.iTxnStatus = 0
                st_PayInfo.iTxnState = 0
                st_PayInfo.szMerchantOrdID = ""
                st_PayInfo.szMerchantSessionID = "" 'Modified 16 Jul 2014, SessionID to MerchantSessionID
                st_PayInfo.szParam1 = ""
                st_PayInfo.szParam2 = ""
                st_PayInfo.szParam3 = ""
                st_PayInfo.szParam4 = ""
                st_PayInfo.szParam5 = ""
                st_PayInfo.szParam6 = ""            'Added, 24 Nov 2015, Param67
                st_PayInfo.szParam7 = ""            'Added, 24 Nov 2015, Param67
                st_PayInfo.szHashMethod = ""
                st_PayInfo.szMerchantReturnURL = ""
                st_PayInfo.szMerchantApprovalURL = ""       'Added, 30 Jun 2014
                st_PayInfo.szMerchantUnApprovalURL = ""     'Added, 30 Jun 2014
                st_PayInfo.szMerchantCallbackURL = ""       'Added, 28 Oct 2014
                st_PayInfo.szMerchantSupportURL = ""
                st_PayInfo.szGatewayID = ""
                st_PayInfo.szMachineID = ""
                st_PayInfo.szCardPAN = ""
                st_PayInfo.szMaskedCardPAN = ""
                st_PayInfo.szTxnDateTime = ""   'Added on 6 Oct 2009
                st_PayInfo.szMerchantOrdDesc = ""     'Added on 12 Oct 2009
                st_HostInfo.iHostID = 0         'Added on 16 June 2009
                st_PayInfo.szCustEmail = ""     'Added, 16 Dec 2013
                st_PayInfo.szCustName = ""      'Added, 18 Feb 2014
                st_PayInfo.szFXCurrencyCode = ""    'Added, 29 Aug 2014, for FX
                st_PayInfo.szFXTxnAmount = ""       'Added, 29 Aug 2014, for FX
                st_PayInfo.szTokenType = ""     'Added, 31 Mar 2015, for BNB

                If (1 = st_PayInfo.iQueryStatus) Then
                    st_PayInfo.szTxnType = dr("TxnType").ToString()
                    st_PayInfo.szMerchantID = dr("MerchantID").ToString()
                    st_PayInfo.szMerchantTxnID = dr("MerchantTxnID").ToString()
                    st_PayInfo.szTxnAmount = dr("TxnAmount").ToString()
                    st_PayInfo.szBaseTxnAmount = dr("BaseTxnAmount").ToString()                 ' Added on 3 Aug 2009, base currency booking confirmation enhancement
                    st_PayInfo.szCurrencyCode = dr("CurrencyCode").ToString()
                    st_PayInfo.szBaseCurrencyCode = Trim(dr("BaseCurrencyCode").ToString())     ' Added on 3 Aug 2009, base currency booking confirmation enhancement
                    st_PayInfo.szIssuingBank = dr("IssuingBank").ToString()
                    st_HostInfo.iHostID = Convert.ToInt32(dr("HostID")) 'Added on 16 June 2009
                    st_PayInfo.szLanguageCode = dr("LanguageCode").ToString()
                    st_PayInfo.iTxnStatus = Convert.ToInt32(dr("TxnStatus"))
                    st_PayInfo.iTxnState = Convert.ToInt32(dr("TxnState"))
                    st_PayInfo.szMerchantOrdID = dr("OrderNumber").ToString()
                    st_PayInfo.szMerchantSessionID = dr("SessionID").ToString()                 'Modified 16 Jul 2014, SessionID to MerchantSessionID
                    st_PayInfo.szParam1 = dr("Param1").ToString()
                    st_PayInfo.szParam2 = dr("Param2").ToString()
                    st_PayInfo.szParam3 = dr("Param3").ToString()
                    st_PayInfo.szParam4 = dr("Param4").ToString()
                    st_PayInfo.szParam5 = dr("Param5").ToString()
                    st_PayInfo.szParam6 = dr("Param6").ToString()                               'Added, 24 Nov 2015, Param67
                    st_PayInfo.szParam7 = dr("Param7").ToString()                               'Added, 24 Nov 2015, Param67
                    st_PayInfo.szHashMethod = dr("HashMethod").ToString()
                    st_PayInfo.szMerchantReturnURL = dr("MerchantReturnURL").ToString()
                    st_PayInfo.szMerchantApprovalURL = dr("MerchantApprovalURL").ToString()     'Added, 30 Jun 2014
                    st_PayInfo.szMerchantUnApprovalURL = dr("MerchantUnApprovalURL").ToString() 'Added, 30 Jun 2014
                    st_PayInfo.szMerchantCallbackURL = dr("MerchantCallbackURL").ToString()     'Added  16 Oct 2014
                    st_PayInfo.szMerchantSupportURL = dr("MerchantSupportURL").ToString()
                    st_PayInfo.szGatewayID = dr("GatewayID").ToString()
                    st_PayInfo.szMachineID = dr("MachineID").ToString()
                    st_PayInfo.szCardPAN = dr("CardPAN").ToString()
                    st_PayInfo.szMaskedCardPAN = dr("MaskedCardPAN").ToString()
                    st_PayInfo.szTxnDateTime = dr("DateCreated").ToString() 'Added on 6 Oct 2009
                    st_PayInfo.szMerchantOrdDesc = dr("OrderDesc").ToString() 'Added on 12 Oct 2009
                    st_PayInfo.szCustEmail = dr("CustEmail").ToString() 'Added, 16 Dec 2013
                    st_PayInfo.szCustName = dr("CustName").ToString()   'Added, 18 Feb 2014
                    st_PayInfo.szFXCurrencyCode = Trim(dr("FXCurrencyCode").ToString())     'Added, 29 Aug 2014, for FX. Modified 8 Sept 2014, somehow if '' will have '   ' (3 spaces)
                    st_PayInfo.szFXTxnAmount = dr("FXTxnAmt").ToString()                    'Added, 29 Aug 2014, for FX
                    st_PayInfo.szTokenType = dr("TokenType").ToString() 'Added, 8 Apr 2015, for BNB

                    ' Moved from outside of End If into here, 1 Mar 2009, should not be considered true if response
                    ' already existed in Response table OR no matching request in Request table, such response should be
                    ' treated as invalid response posted by hackers/malicious acts/host errors.
                    bReturn = True
                Else
                    bReturn = False
                    st_PayInfo.szErrDesc = "bGetReqTxn() error: " + st_PayInfo.szTxnMsg + " > GatewayTxnID(" + st_PayInfo.szTxnID + ")"

                    st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                    st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
                End If
                ' bReturn = True    ' Commented on 1 Mar 2009
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetReqTxn() exception: " + ex.StackTrace + ex.Message + " > GatewayTxnID(" + st_PayInfo.szTxnID + ")"

            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    'Added BTP, 26 Jan 2017.
    '********************************************************************************************
    ' Class Name:		TxnProcOB
    ' Function Name:	bGetTxnResultByParam1
    ' Function Type:	Boolean.  True if unique else false
    ' Parameter:		st_PayInfo, st_HostInfo
    ' Description:		Get request data from database
    ' History:			26 Jan 2017
    '********************************************************************************************
    Public Function bGetTxnResultByParam1(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost) As Boolean
        Dim bReturn As Boolean = True
        Dim objConn As SqlConnection
        Dim objCommand As SqlCommand
        Dim objParam As SqlParameter
        Dim iRowsAffected As Integer = 0
        Dim dr As SqlDataReader

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetTxnResultByParam1", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_Param1", SqlDbType.VarChar, 100)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_Param1").Value = st_PayInfo.szParam1

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If dr.Read() Then
                st_PayInfo.iQueryStatus = Convert.ToInt32(dr("RetCode"))    'dr.GetInt32(1).ToString
                st_PayInfo.szTxnMsg = dr("RetDesc").ToString()

                'Assign initial value
                st_PayInfo.szTxnType = ""
                st_PayInfo.szMerchantID = ""
                st_PayInfo.szMerchantTxnID = ""
                st_PayInfo.szTxnAmount = ""
                st_PayInfo.szBaseTxnAmount = ""     ' Added on 3 Aug 2009, base currency booking confirmation enhancement
                st_PayInfo.szCurrencyCode = ""
                st_PayInfo.szBaseCurrencyCode = ""  ' Added on 3 Aug 2009, base currency booking confirmation enhancement
                st_PayInfo.szIssuingBank = ""
                st_PayInfo.szLanguageCode = ""
                st_PayInfo.iTxnStatus = 0
                st_PayInfo.iTxnState = 0
                st_PayInfo.szMerchantOrdID = ""
                st_PayInfo.szMerchantSessionID = "" 'Modified 16 Jul 2014, SessionID to MerchantSessionID
                'st_PayInfo.szParam1 = ""
                st_PayInfo.szParam2 = ""
                st_PayInfo.szParam3 = ""
                st_PayInfo.szParam4 = ""
                st_PayInfo.szParam5 = ""
                st_PayInfo.szParam6 = ""            'Added, 24 Nov 2015, Param67
                st_PayInfo.szParam7 = ""            'Added, 24 Nov 2015, Param67
                st_PayInfo.szHashMethod = ""
                st_PayInfo.szMerchantReturnURL = ""
                st_PayInfo.szMerchantApprovalURL = ""       'Added, 30 Jun 2014
                st_PayInfo.szMerchantUnApprovalURL = ""     'Added, 30 Jun 2014
                st_PayInfo.szMerchantCallbackURL = ""       'Added, 28 Oct 2014
                st_PayInfo.szMerchantSupportURL = ""
                st_PayInfo.szGatewayID = ""
                st_PayInfo.szMachineID = ""
                st_PayInfo.szCardPAN = ""
                st_PayInfo.szMaskedCardPAN = ""
                st_PayInfo.szTxnDateTime = ""   'Added on 6 Oct 2009
                st_PayInfo.szMerchantOrdDesc = ""     'Added on 12 Oct 2009
                st_HostInfo.iHostID = 0         'Added on 16 June 2009
                st_PayInfo.szCustEmail = ""     'Added, 16 Dec 2013
                st_PayInfo.szCustName = ""      'Added, 18 Feb 2014
                st_PayInfo.szFXCurrencyCode = ""    'Added, 29 Aug 2014, for FX
                st_PayInfo.szFXTxnAmount = ""       'Added, 29 Aug 2014, for FX
                st_PayInfo.szTokenType = ""     'Added, 31 Mar 2015, for BNB
                st_PayInfo.szTxnID = ""

                If (-1 = st_PayInfo.iQueryStatus) Then
                    st_PayInfo.szTxnType = dr("TxnType").ToString()
                    st_PayInfo.szMerchantID = dr("MerchantID").ToString()
                    st_PayInfo.szMerchantTxnID = dr("MerchantTxnID").ToString()
                    st_PayInfo.szTxnAmount = dr("TxnAmount").ToString()
                    st_PayInfo.szBaseTxnAmount = dr("BaseTxnAmount").ToString()                 ' Added on 3 Aug 2009, base currency booking confirmation enhancement
                    st_PayInfo.szCurrencyCode = dr("CurrencyCode").ToString()
                    st_PayInfo.szBaseCurrencyCode = Trim(dr("BaseCurrencyCode").ToString())     ' Added on 3 Aug 2009, base currency booking confirmation enhancement
                    st_PayInfo.szIssuingBank = dr("IssuingBank").ToString()
                    st_HostInfo.iHostID = Convert.ToInt32(dr("HostID")) 'Added on 16 June 2009
                    st_PayInfo.szLanguageCode = dr("LanguageCode").ToString()
                    st_PayInfo.iTxnStatus = Convert.ToInt32(dr("TxnStatus"))
                    st_PayInfo.iTxnState = Convert.ToInt32(dr("TxnState"))
                    st_PayInfo.szMerchantOrdID = dr("OrderNumber").ToString()
                    st_PayInfo.szMerchantSessionID = dr("SessionID").ToString()                 'Modified 16 Jul 2014, SessionID to MerchantSessionID
                    'st_PayInfo.szParam1 = dr("Param1").ToString()
                    st_PayInfo.szParam2 = dr("Param2").ToString()
                    st_PayInfo.szParam3 = dr("Param3").ToString()
                    st_PayInfo.szParam4 = dr("Param4").ToString()
                    st_PayInfo.szParam5 = dr("Param5").ToString()
                    st_PayInfo.szParam6 = dr("Param6").ToString()                               'Added, 24 Nov 2015, Param67
                    st_PayInfo.szParam7 = dr("Param7").ToString()                               'Added, 24 Nov 2015, Param67
                    st_PayInfo.szHashMethod = dr("HashMethod").ToString()
                    st_PayInfo.szMerchantReturnURL = dr("MerchantReturnURL").ToString()
                    st_PayInfo.szMerchantApprovalURL = dr("MerchantApprovalURL").ToString()     'Added, 30 Jun 2014
                    st_PayInfo.szMerchantUnApprovalURL = dr("MerchantUnApprovalURL").ToString() 'Added, 30 Jun 2014
                    st_PayInfo.szMerchantCallbackURL = dr("MerchantCallbackURL").ToString()     'Added  16 Oct 2014
                    st_PayInfo.szMerchantSupportURL = dr("MerchantSupportURL").ToString()
                    st_PayInfo.szGatewayID = dr("GatewayID").ToString()
                    st_PayInfo.szMachineID = dr("MachineID").ToString()
                    st_PayInfo.szCardPAN = dr("CardPAN").ToString()
                    st_PayInfo.szMaskedCardPAN = dr("MaskedCardPAN").ToString()
                    st_PayInfo.szTxnDateTime = dr("DateCreated").ToString() 'Added on 6 Oct 2009
                    st_PayInfo.szMerchantOrdDesc = dr("OrderDesc").ToString() 'Added on 12 Oct 2009
                    st_PayInfo.szCustEmail = dr("CustEmail").ToString() 'Added, 16 Dec 2013
                    st_PayInfo.szCustName = dr("CustName").ToString()   'Added, 18 Feb 2014
                    st_PayInfo.szFXCurrencyCode = Trim(dr("FXCurrencyCode").ToString())     'Added, 29 Aug 2014, for FX. Modified 8 Sept 2014, somehow if '' will have '   ' (3 spaces)
                    st_PayInfo.szFXTxnAmount = dr("FXTxnAmt").ToString()                    'Added, 29 Aug 2014, for FX
                    st_PayInfo.szTokenType = dr("TokenType").ToString() 'Added, 8 Apr 2015, for BNB
                    st_PayInfo.szTxnID = dr("GatewayTxnID").ToString()
                    ' Moved from outside of End If into here, 1 Mar 2009, should not be considered true if response
                    ' already existed in Response table OR no matching request in Request table, such response should be
                    ' treated as invalid response posted by hackers/malicious acts/host errors.
                    bReturn = True
                ElseIf (1 = st_PayInfo.iQueryStatus) Then

                    'st_PayInfo.iQueryStatus = -1

                    st_PayInfo.szTxnType = dr("TxnType").ToString()
                    st_PayInfo.szMerchantID = dr("MerchantID").ToString()
                    st_PayInfo.szMerchantTxnID = dr("MerchantTxnID").ToString()
                    st_PayInfo.szMerchantName = dr("MerchantName").ToString()   'Added on 3 Apr 2010
                    st_PayInfo.szMerchantOrdID = dr("OrderNumber").ToString()
                    st_PayInfo.szTxnAmount = dr("TxnAmount").ToString()
                    st_PayInfo.szBaseTxnAmount = dr("BaseTxnAmount").ToString()
                    st_PayInfo.szCurrencyCode = dr("CurrencyCode").ToString()
                    st_PayInfo.szBaseCurrencyCode = Trim(dr("BaseCurrencyCode").ToString())
                    st_PayInfo.szLanguageCode = dr("LanguageCode").ToString()
                    st_PayInfo.szIssuingBank = dr("IssuingBank").ToString()
                    st_PayInfo.szMerchantSessionID = dr("SessionID").ToString() 'Modified 16 Jul 2014, SessionID to MerchantSessionID
                    st_PayInfo.szParam1 = dr("Param1").ToString()
                    st_PayInfo.szParam2 = dr("Param2").ToString()
                    st_PayInfo.szParam3 = dr("Param3").ToString()
                    st_PayInfo.szParam4 = dr("Param4").ToString()
                    st_PayInfo.szParam5 = dr("Param5").ToString()
                    st_PayInfo.szParam6 = dr("Param6").ToString()               'Added, 24 Nov 2015, Param67
                    st_PayInfo.szParam7 = dr("Param7").ToString()               'Added, 24 Nov 2015, Param67
                    st_PayInfo.szTxnID = dr("GatewayTxnID").ToString()
                    st_PayInfo.szTxnDateTime = dr("DateCreated").ToString()
                    st_PayInfo.szMerchantOrdDesc = dr("OrderDesc").ToString()

                    st_PayInfo.iTxnState = Convert.ToInt32(dr("TxnState"))
                    st_PayInfo.iTxnStatus = Convert.ToInt32(dr("TxnStatus"))
                    st_PayInfo.iMerchantTxnStatus = Convert.ToInt32(dr("MerchantTxnStatus"))
                    st_PayInfo.iAction = Convert.ToInt32(dr("Action"))
                    st_PayInfo.iDuration = Convert.ToInt32(dr("Duration"))
                    st_PayInfo.iOSRet = Convert.ToInt32(dr("OSRet"))
                    st_PayInfo.iErrSet = Convert.ToInt32(dr("ErrSet"))
                    st_PayInfo.iErrNum = Convert.ToInt32(dr("ErrNum"))

                    st_PayInfo.szRespCode = dr("BankRespCode").ToString()
                    st_PayInfo.szHostTxnID = dr("BankRefNo").ToString()
                    st_PayInfo.szTxnMsg = dr("RespMesg").ToString()
                    st_PayInfo.szHashMethod = dr("HashMethod").ToString()
                    st_PayInfo.szHashValue = dr("HashValue").ToString()
                    st_PayInfo.szGatewayID = dr("GatewayID").ToString()
                    st_PayInfo.szCardPAN = dr("CardPAN").ToString()
                    'If st_PayInfo.szCardPAN.Length > 10 Then
                    '    st_PayInfo.szMaskedCardPAN = st_PayInfo.szCardPAN.Substring(0, 6) + "".PadRight(st_PayInfo.szCardPAN.Length - 6 - 4, "X") + st_PayInfo.szCardPAN.Substring(st_PayInfo.szCardPAN.Length - 4, 4)
                    'Else
                    '    st_PayInfo.szMaskedCardPAN = "".PadRight(st_PayInfo.szCardPAN.Length, "X")
                    'End If
                    st_PayInfo.szMachineID = dr("MachineID").ToString()
                    st_PayInfo.szMerchantReturnURL = dr("MerchantReturnURL").ToString()
                    st_PayInfo.szMerchantSupportURL = dr("MerchantSupportURL").ToString()
                    st_PayInfo.szMerchantApprovalURL = dr("MerchantApprovalURL").ToString()     'Added, 8 Jul 2014
                    st_PayInfo.szMerchantUnApprovalURL = dr("MerchantUnApprovalURL").ToString() 'Added, 8 Jul 2014
                    st_PayInfo.szMerchantCallbackURL = dr("MerchantCallbackURL").ToString()     'Added  16 Oct 2014

                    st_HostInfo.szOSPymtCode = dr("OSPymtCode").ToString()
                    st_HostInfo.iHostID = Convert.ToInt32(dr("HostID"))

                    st_PayInfo.szCustEmail = dr("CustEmail").ToString()     'Added, 19 Dec 2013
                    st_PayInfo.szCustName = dr("CustName").ToString()       'Added, 18 Feb 2014

                    st_PayInfo.szFXCurrencyCode = Trim(dr("FXCurrencyCode").ToString())   'Added, 29 Aug 2014, for FX. Modified 26 Sept 2014, somehow if '' will become ' ' (space)
                    st_PayInfo.szFXTxnAmount = dr("FXTxnAmt").ToString()    'Added, 29 Aug 2014, for FX
                    st_PayInfo.szTokenType = dr("TokenType").ToString()     'Added, 8 Apr 2015, for BNB
                Else
                    bReturn = False
                    st_PayInfo.szErrDesc = "bGetReqTxnByBankRefNo() error: " + st_PayInfo.szTxnMsg + " > BankRefNo(" + st_PayInfo.szHostTxnID + ")"

                    st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                    st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
                End If
                ' bReturn = True    ' Commented on 1 Mar 2009
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetReqTxnByBankRefNo() exception: " + ex.StackTrace + ex.Message + " > BankRefNo(" + st_PayInfo.szHostTxnID + ")"

            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bGetTxnParam
    ' Function Type:	Boolean.  True if unique else false
    ' Parameter:		
    ' Description:		Get Param from database
    ' History:			16 Apr 2014
    '********************************************************************************************
    Public Function bGetTxnParam(ByRef st_PayInfo As Common.STPayInfo) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim iQueryStatus As Integer = 0
        Dim iRowsAffected As Integer = 0
        Dim dr As SqlDataReader = Nothing

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetTxnParam", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_MerchantTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@i_ReqRes", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@sz_MerchantTxnID").Value = st_PayInfo.szMerchantTxnID
            objCommand.Parameters("@i_ReqRes").Value = 1

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If dr.Read() Then
                st_PayInfo.szTxnMsg = dr("RetDesc").ToString()

                'Assign initial value
                st_PayInfo.szParam1 = ""

                If (1 = Convert.ToInt32(dr("RetCode"))) Then
                    st_PayInfo.szParam1 = dr("Param1").ToString()

                    bReturn = True
                Else
                    bReturn = False

                    st_PayInfo.szErrDesc = "bGetTxnParam() error: " + st_PayInfo.szTxnMsg + " > MerchantPymtID(" + st_PayInfo.szMerchantTxnID + ")"

                    st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                    st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
                End If
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetTxnParam() exception: " + ex.StackTrace + ex.Message + " > MerchantPymtID(" + st_PayInfo.szMerchantTxnID + ")"

            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProcOB
    ' Function Name:	bGetReqDetail
    ' Function Type:	Boolean.  True if unique else false
    ' Parameter:		
    ' Description:		Get request data from database
    ' History:			28 Sept 2011
    '********************************************************************************************
    Public Function bGetReqDetail(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection
        Dim objCommand As SqlCommand
        Dim objParam As SqlParameter
        Dim iRowsAffected As Integer = 0
        Dim dr As SqlDataReader

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetReqDetail", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_MerchantTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@sz_MerchantTxnID").Value = st_PayInfo.szMerchantTxnID

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If dr.Read() Then
                st_PayInfo.iQueryStatus = Convert.ToInt32(dr("RetCode"))    'dr.GetInt32(1).ToString
                st_PayInfo.szTxnMsg = dr("RetDesc").ToString()

                'Assign initial value
                st_PayInfo.szBaseTxnAmount = ""     ' Added on 3 Aug 2009, base currency booking confirmation enhancement
                st_PayInfo.szBaseCurrencyCode = ""  ' Added on 3 Aug 2009, base currency booking confirmation enhancement
                st_PayInfo.szMerchantOrdID = ""
                st_PayInfo.szMerchantSessionID = "" 'Modified 16 Jul 2014, SessionID to MerchantSessionID
                st_PayInfo.szHashMethod = ""
                st_PayInfo.szMerchantReturnURL = ""
                st_PayInfo.szMerchantSupportURL = ""
                st_PayInfo.szMaskedCardPAN = ""
                st_PayInfo.szMerchantOrdDesc = ""
                st_PayInfo.szIssuingBank = ""

                If (1 = st_PayInfo.iQueryStatus) Then
                    st_PayInfo.szBaseTxnAmount = dr("BaseTxnAmount").ToString()
                    st_PayInfo.szBaseCurrencyCode = Trim(dr("BaseCurrencyCode").ToString())
                    st_PayInfo.szMerchantOrdID = dr("OrderNumber").ToString()
                    st_PayInfo.szMerchantSessionID = dr("SessionID").ToString()             'Modified 16 Jul 2014, SessionID to MerchantSessionID
                    st_PayInfo.szHashMethod = dr("HashMethod").ToString()
                    st_PayInfo.szMerchantReturnURL = dr("MerchantReturnURL").ToString()
                    st_PayInfo.szMerchantSupportURL = dr("MerchantSupportURL").ToString()
                    st_PayInfo.szMaskedCardPAN = dr("MaskedCardPAN").ToString()
                    st_PayInfo.szMerchantOrdDesc = dr("OrderDesc").ToString()
                    st_PayInfo.szIssuingBank = dr("IssuingBank").ToString()

                    ' Moved from outside of End If into here, 1 Mar 2009, should not be considered true if response
                    ' already existed in Response table OR no matching request in Request table, such response should be
                    ' treated as invalid response posted by hackers/malicious acts/host errors.
                    bReturn = True
                Else
                    bReturn = False
                    st_PayInfo.szErrDesc = "bGetReqTxn() error: " + st_PayInfo.szTxnMsg + " > GatewayTxnID(" + st_PayInfo.szTxnID + ")"

                    st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                    st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
                End If
                ' bReturn = True    ' Commented on 1 Mar 2009
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetReqTxn() exception: " + ex.StackTrace + ex.Message + " > GatewayTxnID(" + st_PayInfo.szTxnID + ")"

            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProcOB
    ' Function Name:	bGetResTxn
    ' Function Type:	Boolean.  True if successfully retrieved record or else false
    ' Parameter:		
    ' Description:		Get transaction result from response table based on GatewayTxnID
    ' History:			28 Mar 2010
    '********************************************************************************************
    Public Function bGetResTxn(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection
        Dim objCommand As SqlCommand
        Dim objParam As SqlParameter
        Dim iRowsAffected As Integer = 0
        Dim dr As SqlDataReader
        Dim szHashMethod As String = ""

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetResTxn", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_GatewayTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_GatewayTxnID").Value = st_PayInfo.szTxnID

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If dr.Read() Then

                st_PayInfo.iQueryStatus = Convert.ToInt32(dr("RetCode"))
                st_PayInfo.szQueryMsg = dr("RetDesc").ToString()

                'Assign initial value
                'st_PayInfo.szTxnType = ""      'Commented  18 Dec 2012, to cater for NO server-to-server response received which causes TxnType, IssuingBank, HashMethod retrieved from bGetReqTxn() overwritten with default empty value by bGetResTxn()
                'st_PayInfo.szMerchantID = ""    'Commented  25 Sept 2012. To support if redirect come back faster then server 2 server
                'st_PayInfo.szMerchantTxnID = "" 'Commented  28 Sept 2012. To support if redirect come back faster then server 2 server
                st_PayInfo.szMerchantName = ""
                'st_PayInfo.szMerchantOrdID = "" 'Commented  28 Sept 2012. To support if redirect come back faster then server 2 server
                'st_PayInfo.szTxnAmount = "" 'Commented  28 Sept 2012. To support if redirect come back faster then server 2 server
                st_PayInfo.szBaseTxnAmount = ""
                'st_PayInfo.szCurrencyCode = "" 'Commented  28 Sept 2012. To support if redirect come back faster then server 2 server
                st_PayInfo.szBaseCurrencyCode = ""
                'st_PayInfo.szLanguageCode = "" 'Commented  28 Sept 2012. To support if redirect come back faster then server 2 server
                'st_PayInfo.szIssuingBank = ""  'Commented  18 Dec 2012, to cater for NO server-to-server response received which causes TxnType, IssuingBank, HashMethod retrieved from bGetReqTxn() overwritten with default empty value by bGetResTxn()
                st_PayInfo.szMerchantSessionID = "" 'Modified 16 Jul 2014, SessionID to MerchantSessionID
                st_PayInfo.szParam1 = ""
                st_PayInfo.szParam2 = ""
                st_PayInfo.szParam3 = ""
                st_PayInfo.szParam4 = ""
                st_PayInfo.szParam5 = ""
                'st_PayInfo.szTxnID = ""        'Commented  25 Sept 2012. To support if redirect come back faster then server 2 server
                st_PayInfo.iTxnState = -1
                st_PayInfo.iTxnStatus = -1
                st_PayInfo.iMerchantTxnStatus = -1
                st_PayInfo.iAction = -1
                st_PayInfo.iDuration = -1
                st_PayInfo.iOSRet = 99
                st_PayInfo.iErrSet = -1
                st_PayInfo.iErrNum = -1
                'st_PayInfo.szHostTxnID = ""    Commented  21 Jun 2015, for resp back by both redirect (while loop calling this function to look for status from res table) & s2s; if no s2s resp come back, do not empty HostTxnID, just take it
                st_PayInfo.szTxnMsg = ""
                'st_PayInfo.szHashMethod = ""   Commented  18 Dec 2012, to cater for NO server-to-server response received which causes TxnType, IssuingBank, HashMethod retrieved from bGetReqTxn() overwritten with default empty value by bGetResTxn()
                st_PayInfo.szHashValue = ""
                st_PayInfo.szGatewayID = ""
                st_PayInfo.szCardPAN = ""
                st_PayInfo.szMachineID = ""
                'st_PayInfo.szMerchantReturnURL = "" 'Commented  28 Sept 2012. To support if redirect come back faster then server 2 server
                'st_PayInfo.szMerchantSupportURL = "" 'Commented  28 Sept 2012. To support if redirect come back faster then server 2 server
                st_PayInfo.szTxnDateTime = ""
                'st_PayInfo.szMerchantOrdDesc = "" 'Commented  28 Sept 2012. To support if redirect come back faster then server 2 server
                st_HostInfo.szOSPymtCode = ""
                'st_HostInfo.iHostID = -1           'Commented  25 Jun 2015, will cause those supporting both redirect and s2s model failed, e.g. FPX redirect resp page while loop will call this and later failed to get terminal due to HostID -1
                st_PayInfo.szCustEmail = ""         'Added, 19 Dec 2013
                st_PayInfo.szFXCurrencyCode = ""    'Added, 29 Aug 2014
                st_PayInfo.szFXTxnAmount = ""       'Added, 29 Aug 2014
                st_PayInfo.szTokenType = ""         'Added, 8 Apr 2015, for BNB

                ' 1 - Transaction exists in Response table, has reached its final state
                If (1 = st_PayInfo.iQueryStatus) Then

                    st_PayInfo.iQueryStatus = -1

                    st_PayInfo.szTxnType = dr("TxnType").ToString()
                    st_PayInfo.szMerchantID = dr("MerchantID").ToString()
                    st_PayInfo.szMerchantTxnID = dr("MerchantTxnID").ToString()
                    st_PayInfo.szMerchantName = dr("MerchantName").ToString()   'Added on 3 Apr 2010
                    st_PayInfo.szMerchantOrdID = dr("OrderNumber").ToString()
                    st_PayInfo.szTxnAmount = dr("TxnAmount").ToString()
                    st_PayInfo.szBaseTxnAmount = dr("BaseTxnAmount").ToString()
                    st_PayInfo.szCurrencyCode = dr("CurrencyCode").ToString()
                    st_PayInfo.szBaseCurrencyCode = Trim(dr("BaseCurrencyCode").ToString())
                    st_PayInfo.szLanguageCode = dr("LanguageCode").ToString()
                    st_PayInfo.szIssuingBank = dr("IssuingBank").ToString()
                    st_PayInfo.szMerchantSessionID = dr("SessionID").ToString() 'Modified 16 Jul 2014, SessionID to MerchantSessionID
                    st_PayInfo.szParam1 = dr("Param1").ToString()
                    st_PayInfo.szParam2 = dr("Param2").ToString()
                    st_PayInfo.szParam3 = dr("Param3").ToString()
                    st_PayInfo.szParam4 = dr("Param4").ToString()
                    st_PayInfo.szParam5 = dr("Param5").ToString()
                    st_PayInfo.szParam6 = dr("Param6").ToString()               'Added, 24 Nov 2015, Param67
                    st_PayInfo.szParam7 = dr("Param7").ToString()               'Added, 24 Nov 2015, Param67
                    st_PayInfo.szTxnID = dr("GatewayTxnID").ToString()
                    st_PayInfo.szTxnDateTime = dr("DateCreated").ToString()
                    st_PayInfo.szMerchantOrdDesc = dr("OrderDesc").ToString()

                    st_PayInfo.iTxnState = Convert.ToInt32(dr("TxnState"))
                    st_PayInfo.iTxnStatus = Convert.ToInt32(dr("TxnStatus"))
                    st_PayInfo.iMerchantTxnStatus = Convert.ToInt32(dr("MerchantTxnStatus"))
                    st_PayInfo.iAction = Convert.ToInt32(dr("Action"))
                    st_PayInfo.iDuration = Convert.ToInt32(dr("Duration"))
                    st_PayInfo.iOSRet = Convert.ToInt32(dr("OSRet"))
                    st_PayInfo.iErrSet = Convert.ToInt32(dr("ErrSet"))
                    st_PayInfo.iErrNum = Convert.ToInt32(dr("ErrNum"))

                    st_PayInfo.szRespCode = dr("BankRespCode").ToString()
                    st_PayInfo.szHostTxnID = dr("BankRefNo").ToString()
                    st_PayInfo.szTxnMsg = dr("RespMesg").ToString()
                    st_PayInfo.szHashMethod = dr("HashMethod").ToString()
                    st_PayInfo.szHashValue = dr("HashValue").ToString()
                    st_PayInfo.szGatewayID = dr("GatewayID").ToString()
                    st_PayInfo.szCardPAN = dr("CardPAN").ToString()
                    'If st_PayInfo.szCardPAN.Length > 10 Then
                    '    st_PayInfo.szMaskedCardPAN = st_PayInfo.szCardPAN.Substring(0, 6) + "".PadRight(st_PayInfo.szCardPAN.Length - 6 - 4, "X") + st_PayInfo.szCardPAN.Substring(st_PayInfo.szCardPAN.Length - 4, 4)
                    'Else
                    '    st_PayInfo.szMaskedCardPAN = "".PadRight(st_PayInfo.szCardPAN.Length, "X")
                    'End If
                    st_PayInfo.szMachineID = dr("MachineID").ToString()
                    st_PayInfo.szMerchantReturnURL = dr("MerchantReturnURL").ToString()
                    st_PayInfo.szMerchantSupportURL = dr("MerchantSupportURL").ToString()
                    st_PayInfo.szMerchantApprovalURL = dr("MerchantApprovalURL").ToString()     'Added, 8 Jul 2014
                    st_PayInfo.szMerchantUnApprovalURL = dr("MerchantUnApprovalURL").ToString() 'Added, 8 Jul 2014
                    st_PayInfo.szMerchantCallbackURL = dr("MerchantCallbackURL").ToString()     'Added  16 Oct 2014

                    st_HostInfo.szOSPymtCode = dr("OSPymtCode").ToString()
                    st_HostInfo.iHostID = Convert.ToInt32(dr("HostID"))

                    st_PayInfo.szCustEmail = dr("CustEmail").ToString()     'Added, 19 Dec 2013
                    st_PayInfo.szCustName = dr("CustName").ToString()       'Added, 18 Feb 2014

                    st_PayInfo.szFXCurrencyCode = Trim(dr("FXCurrencyCode").ToString())   'Added, 29 Aug 2014, for FX. Modified 26 Sept 2014, somehow if '' will become ' ' (space)
                    st_PayInfo.szFXTxnAmount = dr("FXTxnAmt").ToString()    'Added, 29 Aug 2014, for FX
                    st_PayInfo.szTokenType = dr("TokenType").ToString()     'Added, 8 Apr 2015, for BNB
                End If

                bReturn = True
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetResTxn() exception: " + ex.StackTrace + ex.Message + " > GatewayTxnID(" + st_PayInfo.szTxnID + ")"

            st_PayInfo.iQueryStatus = 2
            st_PayInfo.szQueryMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProcOB
    ' Function Name:	bGetExtResTxn
    ' Function Type:	Boolean.  True if successfully retrieved record or else false
    ' Parameter:		
    ' Description:		Get transaction result from External's response table based on GatewayTxnID
    ' History:			28 Mar 2010
    '********************************************************************************************
    Public Function bGetExtResTxn(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost, ByVal sz_SPName As String) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection
        Dim objCommand As SqlCommand
        Dim objParam As SqlParameter
        'Dim iRowsAffected As Integer = 0
        Dim dr As SqlDataReader

        Try
            'If sz_SPName = "" Then
            '    Return False
            'End If
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand(sz_SPName, objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_GatewayTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_GatewayTxnID").Value = st_PayInfo.szTxnID

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If dr.Read() Then
                st_PayInfo.iQueryStatus = Convert.ToInt32(dr("RetCode"))
                st_PayInfo.szQueryMsg = dr("RetDesc").ToString()
                st_PayInfo.szTxnMsg = ""

                ' 1 - Transaction exists in Response table, has reached its final state
                If (1 = st_PayInfo.iQueryStatus) Then

                    st_PayInfo.iQueryStatus = -1
                    st_PayInfo.szTxnAmount = dr("TxnAmount").ToString()
                    st_PayInfo.szRespCode = dr("BankRespCode").ToString()
                    st_PayInfo.szHostTxnID = dr("BankRefNo").ToString()
                    st_PayInfo.szAuthCode = dr("AuthCode").ToString()
                    st_PayInfo.szTxnMsg = dr("RespMesg").ToString()
                Else
                    'Added 17 Nov 2017, Bancnet, query for -3 which is no response from Bancnet yet
                    st_PayInfo.szRespCode = st_PayInfo.iQueryStatus.ToString()
                End If

                bReturn = True
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetExtResTxn() exception: " + ex.StackTrace + ex.Message + " > GatewayTxnID(" + st_PayInfo.szTxnID + ")"

            st_PayInfo.iQueryStatus = 2
            st_PayInfo.szQueryMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProcOB
    ' Function Name:	bGetTxnFromParam
    ' Function Type:	Boolean.  True if successfully retrieved record or else false
    ' Parameter:		
    ' Description:		Get transaction details based on Param1
    ' History:			12 Jun 2010
    '********************************************************************************************
    Public Function bGetTxnFromParam(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection
        Dim objCommand As SqlCommand
        Dim objAdp As SqlDataAdapter
        Dim dt As DataTable
        Dim objParam As SqlParameter
        Dim iRowsAffected As Integer = 0
        Dim iRet As Integer = -1

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetTxnFromParam", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_Param", SqlDbType.VarChar, 100)
            objParam = objCommand.Parameters.Add("@i_HostID", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_Param").Value = st_PayInfo.szParam1
            objCommand.Parameters("@i_HostID").Value = st_HostInfo.iHostID

            objConn.Open()
            objCommand.Connection = objConn

            objAdp = New SqlDataAdapter(objCommand)
            dt = New DataTable
            objAdp.Fill(dt)

            iRowsAffected = dt.Rows.Count()
            If iRowsAffected > 0 Then
                For Each row As DataRow In dt.Rows
                    iRet = Convert.ToInt32(row("RetCode"))
                    st_PayInfo.szTxnMsg = row("RetDesc").ToString()

                    If (iRet <> 0) Then
                        bReturn = False
                        st_PayInfo.szErrDesc = "bGetTxnFromParam() Failed to get transaction info iRet(" + iRet.ToString() + ") Param1(" + st_PayInfo.szParam1 + ") HostID(" + st_HostInfo.iHostID.ToString() + ") due to " + st_PayInfo.szTxnMsg
                        GoTo CleanUp
                    End If
                    st_PayInfo.szTxnID = row("GatewayTxnID").ToString()
                    st_PayInfo.szTxnDateTime = row("DateCreated").ToString()
                    st_PayInfo.szCurrencyCode = row("CurrencyCode").ToString()
                    st_PayInfo.szTxnAmount = row("TxnAmount").ToString()
                    st_PayInfo.szMerchantOrdDesc = row("OrderDesc").ToString()
                    st_PayInfo.szMerchantOrdID = row("OrderNumber").ToString()

                    st_PayInfo.szMerchantID = row("MerchantID").ToString()
                    st_PayInfo.szMerchantTxnID = row("MerchantTxnID").ToString()

                    st_PayInfo.szFXCurrencyCode = Trim(row("FXCurrencyCode").ToString())     'Added, 9 Apr 2015, for FX
                    st_PayInfo.szFXTxnAmount = row("FXTxnAmt").ToString()                    'Added, 9 Apr 2015, for FX
                    st_PayInfo.iTxnStatus = Convert.ToInt32(row("TxnStatus"))                'Added 26 Feb 2016, for GPUPOP
                    st_PayInfo.iQueryStatus = Convert.ToInt32(row("QueryStatus"))            'Added 26 Feb 2016, for GPUPOP
                    st_PayInfo.iPKID = Convert.ToInt32(row("PKID"))                          'Added 26 Feb 2016, for GPUPOP
                    bReturn = True
                Next
            Else
                bReturn = False
                st_PayInfo.szTxnMsg = "Transaction info not found"
                st_PayInfo.szErrDesc = "bGetTxnFromParam() Failed to get transaction info" + " Param1(" + st_PayInfo.szParam1 + ") HostID(" + st_HostInfo.iHostID.ToString() + ")"
                GoTo CleanUp
            End If

CleanUp:
            If Not dt Is Nothing Then
                dt.Dispose()
                dt = Nothing
            End If

            If Not objAdp Is Nothing Then
                objAdp.Dispose()
                objAdp = Nothing
            End If

        Catch ex As Exception
            bReturn = False
            st_PayInfo.szTxnMsg = "Failed to retrieve transaction info"
            st_PayInfo.szErrDesc = "bGetTxnFromParam() Exception: " + ex.StackTrace + ex.Message + " > Failed to get transaction info based on Param1(" + st_PayInfo.szParam1 + ") HostID(" + st_HostInfo.iHostID.ToString() + ")"

        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProcOB
    ' Function Name:	bGetTxnResult
    ' Function Type:	Boolean.  True if successfully retrieved record or else false
    ' Parameter:		
    ' Description:		Get transaction result from database
    ' History:			13 Dec 2008
    '********************************************************************************************
    Public Function bGetTxnResult(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection
        Dim objCommand As SqlCommand
        Dim objParam As SqlParameter
        Dim iRowsAffected As Integer = 0
        Dim dr As SqlDataReader
        Dim szHashMethod As String = ""

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetTxnResult", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_MerchantTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@sz_MerchantTxnID").Value = st_PayInfo.szMerchantTxnID

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If dr.Read() Then

                st_PayInfo.iQueryStatus = Convert.ToInt32(dr("RetCode"))    'dr.GetInt32(1).ToString
                st_PayInfo.szQueryMsg = dr("RetDesc").ToString()            'dr.GetString(2)

                'Assign initial value
                st_PayInfo.szMerchantOrdID = ""
                st_PayInfo.szTxnAmount = ""
                st_PayInfo.szBaseTxnAmount = ""     ' Added on 3 Aug 2009, base currency booking confirmation enhancement
                st_PayInfo.szCurrencyCode = ""
                st_PayInfo.szBaseCurrencyCode = ""  ' Added on 3 Aug 2009, base currency booking confirmation enhancement
                st_PayInfo.szLanguageCode = ""
                st_PayInfo.szIssuingBank = ""
                'st_PayInfo.szSessionID = ""        ' Commented on 14 Apr 2010, do not overwrite Request Session ID
                st_PayInfo.szParam1 = ""
                st_PayInfo.szParam2 = ""
                st_PayInfo.szParam3 = ""
                st_PayInfo.szParam4 = ""
                st_PayInfo.szParam5 = ""
                st_PayInfo.szParam6 = ""            'Added, 24 Nov 2015, Param67
                st_PayInfo.szParam7 = ""            'Added, 24 Nov 2015, Param67
                st_PayInfo.szTxnID = ""
                st_PayInfo.iTxnState = -1
                st_PayInfo.iTxnStatus = -1
                st_PayInfo.iMerchantTxnStatus = -1
                st_PayInfo.iAction = -1
                st_PayInfo.iDuration = -1
                st_PayInfo.iOSRet = 99
                st_PayInfo.iErrSet = -1
                st_PayInfo.iErrNum = -1
                st_PayInfo.szHostTxnID = ""
                st_PayInfo.szTxnMsg = ""
                st_PayInfo.szHashValue = ""
                st_PayInfo.szGatewayID = ""
                st_PayInfo.szCardPAN = ""
                st_PayInfo.szMachineID = ""
                st_PayInfo.szMerchantReturnURL = ""
                st_PayInfo.szMerchantSupportURL = ""
                st_PayInfo.szMerchantCallbackURL = ""   'Added, 3 Dec 2014
                st_PayInfo.szTxnDateTime = ""
                st_HostInfo.szOSPymtCode = ""
                st_HostInfo.iHostID = -1
                st_PayInfo.szCustEmail = ""     'Added  25 Nov 2014 - ENS
                st_PayInfo.szMerchantOrdDesc = ""     'Added, 2 Dec 2014
                st_PayInfo.szCustName = ""      'Added, 3 Dec 2014

                ' 1 - Transaction exists in Response table, has reached its final state
                '-1 - Transaction only exists in Request table, still has not reached final state, maybe under processing
                If ((1 = st_PayInfo.iQueryStatus) Or (-1 = st_PayInfo.iQueryStatus)) Then
                    st_PayInfo.iPKID = Convert.ToInt32(dr("PKID"))                              'Refer to  code GPUPOP, added  for PBBUPOP 6 Apr 2016
                    st_PayInfo.szMerchantOrdID = dr("OrderNumber").ToString()
                    st_PayInfo.szTxnAmount = dr("TxnAmount").ToString()
                    st_PayInfo.szBaseTxnAmount = dr("BaseTxnAmount").ToString()                 ' Added on 3 Aug 2009, base currency booking confirmation enhancement
                    st_PayInfo.szCurrencyCode = dr("CurrencyCode").ToString()
                    st_PayInfo.szBaseCurrencyCode = Trim(dr("BaseCurrencyCode").ToString())     ' Added on 3 Aug 2009, base currency booking confirmation enhancement
                    st_PayInfo.szLanguageCode = dr("LanguageCode").ToString()
                    st_PayInfo.szIssuingBank = dr("IssuingBank").ToString()
                    'Added on 14 Apr 2010, if Request's SessionID is not empty, do not overwrite it.
                    If ("" = st_PayInfo.szMerchantSessionID) Then                               'Modified 16 Jul 2014, SessionID to MerchantSessionID
                        st_PayInfo.szMerchantSessionID = dr("SessionID").ToString()
                    End If
                    st_PayInfo.szParam1 = dr("Param1").ToString()
                    st_PayInfo.szParam2 = dr("Param2").ToString()
                    st_PayInfo.szParam3 = dr("Param3").ToString()
                    st_PayInfo.szParam4 = dr("Param4").ToString()
                    st_PayInfo.szParam5 = dr("Param5").ToString()
                    st_PayInfo.szParam6 = dr("Param6").ToString()           'Added, 24 Nov 2015, Param67
                    st_PayInfo.szParam7 = dr("Param7").ToString()           'Added, 24 Nov 2015, Param67
                    st_PayInfo.szTxnID = dr("GatewayTxnID").ToString()
                    st_PayInfo.szTxnDateTime = dr("DateCreated").ToString()

                    st_PayInfo.iTxnState = Convert.ToInt32(dr("TxnState"))
                    st_PayInfo.iTxnStatus = Convert.ToInt32(dr("TxnStatus"))
                    st_PayInfo.iMerchantTxnStatus = Convert.ToInt32(dr("MerchantTxnStatus"))
                    st_PayInfo.iAction = Convert.ToInt32(dr("Action"))
                    st_PayInfo.iDuration = Convert.ToInt32(dr("Duration"))
                    st_PayInfo.iOSRet = Convert.ToInt32(dr("OSRet"))
                    st_PayInfo.iErrSet = Convert.ToInt32(dr("ErrSet"))
                    st_PayInfo.iErrNum = Convert.ToInt32(dr("ErrNum"))

                    st_PayInfo.szRespCode = dr("BankRespCode").ToString()
                    st_PayInfo.szHostTxnID = dr("BankRefNo").ToString()
                    st_PayInfo.szTxnMsg = dr("RespMesg").ToString()

                    szHashMethod = dr("HashMethod").ToString()

                    st_PayInfo.szGatewayID = dr("GatewayID").ToString()
                    st_PayInfo.szCardPAN = dr("CardPAN").ToString()
                    If st_PayInfo.szCardPAN.Length > 10 Then
                        st_PayInfo.szMaskedCardPAN = st_PayInfo.szCardPAN.Substring(0, 6) + "".PadRight(st_PayInfo.szCardPAN.Length - 6 - 4, "X") + st_PayInfo.szCardPAN.Substring(st_PayInfo.szCardPAN.Length - 4, 4)
                    Else
                        st_PayInfo.szMaskedCardPAN = "".PadRight(st_PayInfo.szCardPAN.Length, "X")
                    End If
                    st_PayInfo.szMachineID = dr("MachineID").ToString()
                    st_PayInfo.szMerchantReturnURL = dr("MerchantReturnURL").ToString()
                    st_PayInfo.szMerchantSupportURL = dr("MerchantSupportURL").ToString()

                    st_HostInfo.szOSPymtCode = dr("OSPymtCode").ToString()
                    st_HostInfo.iHostID = Convert.ToInt32(dr("HostID"))

                    st_PayInfo.szCustEmail = dr("CustEmail").ToString()     'Added  25 Nov 2014 - ENS
                    st_PayInfo.szMerchantOrdDesc = dr("OrderDesc").ToString()     'Added, 2 Dec 2014, to show OrderDesc on email notification when Query
                    st_PayInfo.szCustName = dr("CustName").ToString()       'Added, 3 Dec 2014
                    st_PayInfo.szMerchantCallbackURL = dr("MerchantCallbackURL").ToString()       'Added, 3 Dec 2014

                    If ("" = st_PayInfo.szFXCurrencyCode) Then
                        If (Trim(dr("FXCurrencyCode").ToString()) <> "") Then               'Modified 1 Dec 2014, added checking of dr("FXCurrencyCode") which somehow will be "   " even though stored proc got ISNULL(FXCurrencyCode,'')
                            st_PayInfo.szFXCurrencyCode = dr("FXCurrencyCode").ToString()   'Added, 28 Nov 2014
                        End If

                        If (Trim(dr("FXTxnAmount").ToString()) <> "") Then               'Added, 10 Oct 2017, VTC
                            st_PayInfo.szFXTxnAmount = dr("FXTxnAmount").ToString()
                        End If
                    End If
                End If

                If (1 = st_PayInfo.iQueryStatus) Then
                    ' If Query request's Hash Method is the same as Hash Method for txn existed in Response table,
                    ' then just use the HashValue in Response table or else have to recalculate the response hash based
                    ' on Query request's Hash Method
                    If (st_PayInfo.szHashMethod.ToLower() = szHashMethod.ToLower()) Then
                        'Added checking of whether HashValue is empty or not on 3 Apr 2010
                        If (dr("HashValue").ToString() <> "") Then
                            st_PayInfo.szHashValue = dr("HashValue").ToString()
                        End If
                    End If
                ElseIf (-1 = st_PayInfo.iQueryStatus) Then
                    'Added on 20 Mar 2010 to cater for ability to retrieve the original request's hash method, especially after 2nd entry page
                    st_PayInfo.szHashMethod = dr("HashMethod").ToString()
                End If

                bReturn = True
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetTxnResult() exception: " + ex.StackTrace + ex.Message + " > GatewayTxnID(" + st_PayInfo.szTxnID + ")"

            st_PayInfo.iQueryStatus = 2
            st_PayInfo.szQueryMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bGetTxnResult2
    ' Function Type:	Boolean.  True if successfully retrieved record or else false
    ' Parameter:		
    ' Description:		Get transaction result from database without Amount condition
    ' History:			30 Apr 2015. Auth&Capture.      Modified  25 May 2015. Refund
    '********************************************************************************************
    Public Function bGetTxnResult2(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim iRowsAffected As Integer = 0
        Dim dr As SqlDataReader = Nothing
        Dim szHashMethod As String = ""

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetTxnResult2", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_MerchantTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_CurrencyCode", SqlDbType.Char, 3)     'Added, 18 Aug 2013
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@sz_MerchantTxnID").Value = st_PayInfo.szMerchantTxnID

            If (st_PayInfo.szCurrencyCode <> "") Then
                objCommand.Parameters("@sz_CurrencyCode").Value = st_PayInfo.szCurrencyCode             'Added, 18 Aug 2013
            Else
                objCommand.Parameters("@sz_CurrencyCode").Value = st_PayInfo.szRCurrencyCode            'Added, 5 Sept 2013
            End If

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If dr.Read() Then

                st_PayInfo.iQueryStatus = Convert.ToInt32(dr("RetCode"))    'dr.GetInt32(1).ToString
                st_PayInfo.szQueryMsg = dr("RetDesc").ToString()            'dr.GetString(2)

                'Assign initial value
                st_PayInfo.iPKID = -1               ' Added, 5 Sept 2013
                st_PayInfo.szTxnType = ""
                st_PayInfo.szMerchantOrdID = ""
                st_PayInfo.szTxnAmount = ""
                st_PayInfo.szBaseTxnAmount = ""     ' Added on 3 Aug 2009, base currency booking confirmation enhancement
                st_PayInfo.szCurrencyCode = ""
                st_PayInfo.szBaseCurrencyCode = ""  ' Added on 3 Aug 2009, base currency booking confirmation enhancement
                st_PayInfo.szLanguageCode = ""
                st_PayInfo.szIssuingBank = ""
                st_PayInfo.szParam1 = ""
                st_PayInfo.szParam2 = ""
                st_PayInfo.szParam3 = ""
                st_PayInfo.szParam4 = ""
                st_PayInfo.szParam5 = ""
                st_PayInfo.szParam6 = ""
                st_PayInfo.szParam7 = ""
                st_PayInfo.szParam8 = ""
                st_PayInfo.szParam9 = ""
                st_PayInfo.szParam10 = ""
                st_PayInfo.szParam11 = ""
                st_PayInfo.szParam12 = ""
                st_PayInfo.szParam13 = ""
                st_PayInfo.szParam14 = ""
                st_PayInfo.szParam15 = ""
                st_PayInfo.szTxnID = ""
                st_PayInfo.iTxnState = -1
                st_PayInfo.iTxnStatus = -1
                st_PayInfo.iMerchantTxnStatus = -1
                st_PayInfo.iAction = -1
                st_PayInfo.iDuration = -1
                st_PayInfo.iOSRet = 99
                st_PayInfo.iErrSet = -1
                st_PayInfo.iErrNum = -1
                st_PayInfo.szHostTxnID = ""
                st_PayInfo.szTxnMsg = ""
                st_PayInfo.szHashValue = ""
                st_PayInfo.szGatewayID = ""
                st_PayInfo.szCardPAN = ""
                st_PayInfo.szMachineID = ""
                st_PayInfo.szMerchantReturnURL = ""
                st_PayInfo.szMerchantSupportURL = ""
                st_PayInfo.szMerchantCallbackURL = ""   'Added, 3 Dec 2014
                st_PayInfo.szTxnDateTime = ""
                st_HostInfo.szOSPymtCode = ""
                st_HostInfo.iHostID = -1
                st_PayInfo.szCustEmail = ""     'Added  25 Nov 2014 - ENS
                st_PayInfo.szMerchantOrdDesc = ""     'Added, 2 Dec 2014
                st_PayInfo.szCustName = ""      'Added, 3 Dec 2014
                st_PayInfo.szTotRefundAmt = 0   'Added  15 Sept 2015. REFUND

                ' 1 - Transaction exists in Response table, has reached its final state
                '-1 - Transaction only exists in Request table, still has not reached final state, maybe under processing
                If ((1 = st_PayInfo.iQueryStatus) Or (-1 = st_PayInfo.iQueryStatus)) Then
                    st_PayInfo.iPKID = Convert.ToInt32(dr("PKID"))                              ' Added, 5 Sept 2013
                    st_PayInfo.szTxnType = dr("TxnType").ToString()
                    st_PayInfo.szMerchantOrdID = dr("OrderNumber").ToString()
                    st_PayInfo.szTxnAmount = dr("TxnAmount").ToString()
                    st_PayInfo.szBaseTxnAmount = dr("BaseTxnAmount").ToString()                 ' Added on 3 Aug 2009, base currency booking confirmation enhancement
                    st_PayInfo.szCurrencyCode = dr("CurrencyCode").ToString()
                    st_PayInfo.szBaseCurrencyCode = Trim(dr("BaseCurrencyCode").ToString())     ' Added on 3 Aug 2009, base currency booking confirmation enhancement
                    st_PayInfo.szLanguageCode = dr("LanguageCode").ToString()
                    st_PayInfo.szIssuingBank = dr("IssuingBank").ToString()
                    'Added on 14 Apr 2010, if Request's SessionID is not empty, do not overwrite it.
                    If ("" = st_PayInfo.szMerchantSessionID) Then                               'Modified 16 Jul 2014, SessionID to MerchantSessionID
                        st_PayInfo.szMerchantSessionID = dr("SessionID").ToString()
                    End If
                    st_PayInfo.szParam1 = dr("Param1").ToString()
                    st_PayInfo.szParam2 = dr("Param2").ToString()
                    st_PayInfo.szParam3 = dr("Param3").ToString()
                    st_PayInfo.szParam4 = dr("Param4").ToString()
                    st_PayInfo.szParam5 = dr("Param5").ToString()
                    st_PayInfo.szParam6 = dr("Param6").ToString()
                    st_PayInfo.szParam7 = dr("Param7").ToString()
                    st_PayInfo.szTxnID = dr("GatewayTxnID").ToString()
                    st_PayInfo.szTxnDateTime = dr("DateCreated").ToString()

                    st_PayInfo.iTxnState = Convert.ToInt32(dr("TxnState"))
                    st_PayInfo.iTxnStatus = Convert.ToInt32(dr("TxnStatus"))
                    st_PayInfo.iMerchantTxnStatus = Convert.ToInt32(dr("MerchantTxnStatus"))
                    st_PayInfo.iAction = Convert.ToInt32(dr("Action"))
                    st_PayInfo.iDuration = Convert.ToInt32(dr("Duration"))
                    st_PayInfo.iOSRet = Convert.ToInt32(dr("OSRet"))
                    st_PayInfo.iErrSet = Convert.ToInt32(dr("ErrSet"))
                    st_PayInfo.iErrNum = Convert.ToInt32(dr("ErrNum"))

                    st_PayInfo.szRespCode = dr("BankRespCode").ToString()
                    st_PayInfo.szHostTxnID = dr("BankRefNo").ToString()
                    st_PayInfo.szTxnMsg = dr("RespMesg").ToString()

                    szHashMethod = dr("HashMethod").ToString()

                    st_PayInfo.szGatewayID = dr("GatewayID").ToString()
                    st_PayInfo.szCardPAN = dr("CardPAN").ToString()
                    If st_PayInfo.szCardPAN.Length > 10 Then
                        st_PayInfo.szMaskedCardPAN = st_PayInfo.szCardPAN.Substring(0, 6) + "".PadRight(st_PayInfo.szCardPAN.Length - 6 - 4, "X") + st_PayInfo.szCardPAN.Substring(st_PayInfo.szCardPAN.Length - 4, 4)
                    Else
                        st_PayInfo.szMaskedCardPAN = "".PadRight(st_PayInfo.szCardPAN.Length, "X")
                    End If
                    st_PayInfo.szMachineID = dr("MachineID").ToString()
                    st_PayInfo.szMerchantReturnURL = dr("MerchantReturnURL").ToString()
                    st_PayInfo.szMerchantSupportURL = dr("MerchantSupportURL").ToString()

                    st_HostInfo.szOSPymtCode = dr("OSPymtCode").ToString()
                    st_HostInfo.iHostID = Convert.ToInt32(dr("HostID"))
                    st_PayInfo.szMerchantOrdDesc = dr("OrderDesc").ToString()     'Added, 2 Dec 2014, to show OrderDesc on email notification when Query
                    st_PayInfo.szMerchantCallbackURL = dr("MerchantCallbackURL").ToString() 'Added, 3 Dec 2014
                    st_PayInfo.szTotRefundAmt = dr("TotRefundAmt").ToString()               'Added  15 Sept 2015. REFUND
                End If

                If (1 = st_PayInfo.iQueryStatus) Then
                    ' If Query request's Hash Method is the same as Hash Method for txn existed in Response table,
                    ' then just use the HashValue in Response table or else have to recalculate the response hash based
                    ' on Query request's Hash Method
                    If (st_PayInfo.szHashMethod.ToLower() = szHashMethod.ToLower()) Then
                        'Added checking of whether HashValue is empty or not on 3 Apr 2010
                        If (dr("HashValue").ToString() <> "") Then
                            st_PayInfo.szHashValue = dr("HashValue").ToString()
                        End If
                    End If
                ElseIf (-1 = st_PayInfo.iQueryStatus) Then
                    'Added on 20 Mar 2010 to cater for ability to retrieve the original request's hash method, especially after 2nd entry page
                    st_PayInfo.szHashMethod = dr("HashMethod").ToString()
                End If

                bReturn = True
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "spGetTxnResult2() exception: " + ex.StackTrace + ex.Message + " > GatewayTxnID(" + st_PayInfo.szTxnID + ")"

            st_PayInfo.iQueryStatus = 2
            st_PayInfo.szQueryMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function
    '********************************************************************************************
    ' Class Name:		TxnProcOB
    ' Function Name:	bGetTxnResult3
    ' Function Type:	Boolean.  True if successfully retrieved record or else false
    ' Parameter:		
    ' Description:		Get transaction PKID from TB_PayReq table
    ' History:			2 Nov 2015
    '********************************************************************************************
    Public Function bGetTxnResult3(ByRef st_PayInfo As Common.STPayInfo) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim iRowsAffected As Integer = 0
        Dim dr As SqlDataReader = Nothing

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetTxnResult3", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_GatewayTxnID", SqlDbType.VarChar, 30)
            objCommand.Parameters("@sz_GatewayTxnID").Value = st_PayInfo.szTxnID

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If dr.Read() Then
                'Assign initial value
                st_PayInfo.iPKID = -1

                If (1 = Convert.ToInt32(dr("RetCode"))) Then
                    st_PayInfo.iPKID = Convert.ToInt32(dr("PKID"))
                    bReturn = True
                Else
                    st_PayInfo.szErrDesc = dr("RetDesc").ToString()
                    bReturn = False
                End If
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "spGetTxnResult3() exception: " + ex.StackTrace + ex.Message + " > GatewayTxnID(" + st_PayInfo.szTxnID + ")"
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProcOB
    ' Function Name:	bGetTxnResultOTC
    ' Function Type:	Boolean.  True if successfully retrieved record or else false
    ' Parameter:		
    ' Description:		Get transaction result based on OTC SecureCode
    ' History:			16 Apr 2015
    '********************************************************************************************
    Public Function bGetTxnResultOTC(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection
        Dim objCommand As SqlCommand
        Dim objParam As SqlParameter
        Dim iRowsAffected As Integer = 0
        Dim dr As SqlDataReader
        Dim szHashMethod As String = ""

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetTxnResultOTC", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_SecureCode", SqlDbType.VarChar, 20)   '711, 8 Apr 2016, changed Data type from BIGINT to VARCHAR
            objParam = objCommand.Parameters.Add("@sz_TxnAmt", SqlDbType.Decimal)
            objParam = objCommand.Parameters.Add("@sz_CurrencyCode", SqlDbType.Char, 3)
            objParam = objCommand.Parameters.Add("@i_HostID", SqlDbType.Int)                'Added, 27 Apr 2017
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            'objCommand.Parameters("@i_SecureCode").Value = Convert.ToInt64(st_PayInfo.szOTCSecureCode) 'Commented on 7 Oct 2016, 711
            objCommand.Parameters("@sz_SecureCode").Value = st_PayInfo.szOTCSecureCode   '711, 8 Apr 2016, changed Data type from BIGINT to VARCHAR

            'Modified  24 Jul 2015, added checking of TxnAmount <> ""
            If (st_PayInfo.szTxnAmount <> "") Then
                objCommand.Parameters("@sz_TxnAmt").Value = st_PayInfo.szTxnAmount
            Else
                objCommand.Parameters("@sz_TxnAmt").Value = 0
            End If

            objCommand.Parameters("@sz_CurrencyCode").Value = st_PayInfo.szCurrencyCode
            objCommand.Parameters("@i_HostID").Value = st_HostInfo.iHostID                  'Added, 27 Apr 2017

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If dr.Read() Then

                st_PayInfo.iQueryStatus = Convert.ToInt32(dr("RetCode"))    'dr.GetInt32(1).ToString
                st_PayInfo.szQueryMsg = dr("RetDesc").ToString()            'dr.GetString(2)

                'Assign initial value
                st_PayInfo.szMerchantOrdID = ""
                st_PayInfo.szMerchantOrdDesc = ""         ' Added, 17 Apr 2015
                'st_PayInfo.szTxnAmount = ""        ' Commented  17 Apr 2015, so that can return the same value in QuerySP response as per QuerySP request
                st_PayInfo.szBaseTxnAmount = ""     ' Added on 3 Aug 2009, base currency booking confirmation enhancement
                'st_PayInfo.szCurrencyCode = ""     ' Commented  17 Apr 2015, so that can return the same value in QuerySP response as per QuerySP request
                st_PayInfo.szBaseCurrencyCode = ""  ' Added on 3 Aug 2009, base currency booking confirmation enhancement
                st_PayInfo.szLanguageCode = ""
                'st_PayInfo.szIssuingBank = ""      ' Commented  19 Apr 2015, else will be empty in Payment Notification Response
                'st_PayInfo.szSessionID = ""        ' Commented on 14 Apr 2010, do not overwrite Request Session ID
                st_PayInfo.szParam1 = ""
                st_PayInfo.szParam2 = ""
                st_PayInfo.szParam3 = ""
                st_PayInfo.szParam4 = ""
                st_PayInfo.szParam5 = ""
                st_PayInfo.szMerchantTxnID = ""     'Added, 17 Apr 2015
                st_PayInfo.szTxnID = ""
                st_PayInfo.iTxnState = -1
                st_PayInfo.iTxnStatus = -1
                st_PayInfo.iMerchantTxnStatus = -1
                st_PayInfo.iAction = -1
                st_PayInfo.iDuration = -1
                st_PayInfo.iOSRet = 99
                st_PayInfo.iErrSet = -1
                st_PayInfo.iErrNum = -1
                'st_PayInfo.szHostTxnID = ""        ' Commented  19 Apr 2015, else will be empty in Payment Notification Response
                st_PayInfo.szTxnMsg = ""
                st_PayInfo.szHashValue = ""
                st_PayInfo.szGatewayID = ""
                st_PayInfo.szCardPAN = ""
                st_PayInfo.szMachineID = ""
                st_PayInfo.szMerchantReturnURL = ""
                st_PayInfo.szMerchantSupportURL = ""
                st_PayInfo.szTxnDateTime = ""
                st_PayInfo.szCustName = ""          'Added, 21 Jul 2015
                st_HostInfo.szOSPymtCode = ""
                st_HostInfo.iHostID = -1

                ' 1 - Transaction exists in Response table, has reached its final state
                '-1 - Transaction only exists in Request table, still has not reached final state, maybe under processing
                If ((1 = st_PayInfo.iQueryStatus) Or (-1 = st_PayInfo.iQueryStatus)) Then
                    st_PayInfo.szMerchantID = dr("MerchantID").ToString()                         ' Added, 19 Apr 2015
                    st_PayInfo.szMerchantOrdID = dr("OrderNumber").ToString()
                    st_PayInfo.szMerchantOrdDesc = dr("OrderDesc").ToString()                         ' Added, 17 Apr 2015
                    st_PayInfo.szTxnAmount = dr("TxnAmount").ToString()
                    st_PayInfo.szBaseTxnAmount = dr("BaseTxnAmount").ToString()                 ' Added on 3 Aug 2009, base currency booking confirmation enhancement
                    st_PayInfo.szCurrencyCode = dr("CurrencyCode").ToString()
                    st_PayInfo.szBaseCurrencyCode = Trim(dr("BaseCurrencyCode").ToString())     ' Added on 3 Aug 2009, base currency booking confirmation enhancement
                    st_PayInfo.szLanguageCode = dr("LanguageCode").ToString()
                    st_PayInfo.szIssuingBank = dr("IssuingBank").ToString()
                    'Added on 14 Apr 2010, if Request's SessionID is not empty, do not overwrite it.
                    If ("" = st_PayInfo.szMerchantSessionID) Then                               'Modified 16 Jul 2014, SessionID to MerchantSessionID
                        st_PayInfo.szMerchantSessionID = dr("SessionID").ToString()
                    End If
                    st_PayInfo.szParam1 = dr("Param1").ToString()
                    st_PayInfo.szParam2 = dr("Param2").ToString()
                    st_PayInfo.szParam3 = dr("Param3").ToString()
                    st_PayInfo.szParam4 = dr("Param4").ToString()
                    st_PayInfo.szParam5 = dr("Param5").ToString()
                    st_PayInfo.szMerchantTxnID = dr("MerchantTxnID").ToString() 'Added, 17 Apr 2015
                    st_PayInfo.szTxnID = dr("GatewayTxnID").ToString()
                    st_PayInfo.szTxnDateTime = dr("DateCreated").ToString()

                    st_PayInfo.iTxnState = Convert.ToInt32(dr("TxnState"))
                    st_PayInfo.iTxnStatus = Convert.ToInt32(dr("TxnStatus"))
                    st_PayInfo.iMerchantTxnStatus = Convert.ToInt32(dr("MerchantTxnStatus"))
                    st_PayInfo.iAction = Convert.ToInt32(dr("Action"))
                    st_PayInfo.iDuration = Convert.ToInt32(dr("Duration"))
                    st_PayInfo.iOSRet = Convert.ToInt32(dr("OSRet"))
                    st_PayInfo.iErrSet = Convert.ToInt32(dr("ErrSet"))
                    st_PayInfo.iErrNum = Convert.ToInt32(dr("ErrNum"))

                    st_PayInfo.szRespCode = dr("BankRespCode").ToString()
                    'Modified 19 Apr 2015, avoid when first payment notification comes back, BankRefNo is empty in DB and overwrite Host returned BankRefNo stored in st_PayInfo.szHostTxnID from bGetData()
                    If (dr("BankRefNo").ToString() <> "") Then
                        st_PayInfo.szHostTxnID = dr("BankRefNo").ToString()
                    End If
                    st_PayInfo.szTxnMsg = dr("RespMesg").ToString()

                    szHashMethod = dr("HashMethod").ToString()

                    st_PayInfo.szGatewayID = dr("GatewayID").ToString()
                    st_PayInfo.szCardPAN = dr("CardPAN").ToString()
                    If st_PayInfo.szCardPAN.Length > 10 Then
                        st_PayInfo.szMaskedCardPAN = st_PayInfo.szCardPAN.Substring(0, 6) + "".PadRight(st_PayInfo.szCardPAN.Length - 6 - 4, "X") + st_PayInfo.szCardPAN.Substring(st_PayInfo.szCardPAN.Length - 4, 4)
                    Else
                        st_PayInfo.szMaskedCardPAN = "".PadRight(st_PayInfo.szCardPAN.Length, "X")
                    End If
                    st_PayInfo.szMachineID = dr("MachineID").ToString()
                    st_PayInfo.szMerchantReturnURL = dr("MerchantReturnURL").ToString()
                    st_PayInfo.szMerchantSupportURL = dr("MerchantSupportURL").ToString()
                    st_PayInfo.szCustName = dr("CustName").ToString()                       'Added, 21 Jul 2015

                    st_HostInfo.szOSPymtCode = dr("OSPymtCode").ToString()
                    st_HostInfo.iHostID = Convert.ToInt32(dr("HostID"))
                End If

                If (1 = st_PayInfo.iQueryStatus) Then
                    ' If Query request's Hash Method is the same as Hash Method for txn existed in Response table,
                    ' then just use the HashValue in Response table or else have to recalculate the response hash based
                    ' on Query request's Hash Method
                    If (st_PayInfo.szHashMethod.ToLower() = szHashMethod.ToLower()) Then
                        'Added checking of whether HashValue is empty or not on 3 Apr 2010
                        If (dr("HashValue").ToString() <> "") Then
                            st_PayInfo.szHashValue = dr("HashValue").ToString()
                        End If
                    End If
                ElseIf (-1 = st_PayInfo.iQueryStatus) Then
                    'Added on 20 Mar 2010 to cater for ability to retrieve the original request's hash method, especially after 2nd entry page
                    st_PayInfo.szHashMethod = dr("HashMethod").ToString()
                End If

                bReturn = True
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetTxnResultOTC() exception: " + ex.StackTrace + ex.Message + " > SecureCode(" + st_PayInfo.szOTCSecureCode + ") TxnAmt(" + st_PayInfo.szTxnAmount + ") CurrencyCode(" + st_PayInfo.szCurrencyCode + ")"

            st_PayInfo.iQueryStatus = 2
            st_PayInfo.szQueryMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProcOB
    ' Function Name:	bGetTxnStatusAction
    ' Function Type:	Boolean.  True if get txn status's action successfully else false
    ' Parameter:		st_HostInfo, st_PayInfo, sz_DBConnStr
    ' Description:		Get transaction status's action
    ' History:			31 Oct 2008
    '********************************************************************************************
    Public Function bGetTxnStatusAction(ByRef st_HostInfo As Common.STHost, ByRef st_PayInfo As Common.STPayInfo) As Boolean

        Dim bReturn As Boolean = True
        Dim objConn As SqlConnection
        Dim objCommand As SqlCommand
        Dim objParam As SqlParameter
        Dim dr As SqlDataReader
        Dim iRet As Integer = 0
        Dim szStatus As String = "" 'Added on 6 Apr 2010 for logging purposes

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetTxnStatusAction", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@i_HostID", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_ActionID", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@sz_TxnStatus", SqlDbType.VarChar, 60)    'Modified 4 to 60 on 24 Aug 2009
            objParam = objCommand.Parameters.Add("@sz_TxnType", SqlDbType.VarChar, 7)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@i_HostID").Value = st_HostInfo.iHostID
            objCommand.Parameters("@i_ActionID").Value = st_HostInfo.iTxnStatusActionID
            If (st_PayInfo.szHostTxnStatus.Length > 0) Then
                objCommand.Parameters("@sz_TxnStatus").Value = st_PayInfo.szHostTxnStatus
                szStatus = st_PayInfo.szHostTxnStatus   'Added on 6 Apr 2010
            Else
                objCommand.Parameters("@sz_TxnStatus").Value = st_PayInfo.szRespCode
                szStatus = st_PayInfo.szRespCode        'Added on 6 Apr 2010
            End If
            objCommand.Parameters("@sz_TxnType").Value = st_PayInfo.szTxnType

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            iRet = -1
            If dr.Read() Then
                iRet = Convert.ToInt32(dr("RetCode"))
            End If

            If iRet = 0 Then
                bReturn = True
                '[Action], [Desc]
                st_PayInfo.iAction = Convert.ToInt32(dr("Action"))  ' Action
            Else
                st_PayInfo.szErrDesc = "bGetTxnStatusAction() error: Failed to get Txn Status Action" + " > HostID(" + st_HostInfo.iHostID.ToString() + ") ActionID(" + st_HostInfo.iTxnStatusActionID.ToString() + ") TxnStatus(" + szStatus + ") TxnType(" + st_PayInfo.szTxnType + ")"

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT

                bReturn = False
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetTxnStatusAction() exception: " + ex.StackTrace + " " + ex.Message + " > HostID(" + st_HostInfo.iHostID.ToString() + ") ActionID(" + st_HostInfo.iTxnStatusActionID.ToString() + ") TxnStatus(" + szStatus + ") TxnType(" + st_PayInfo.szTxnType + ")"

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProcOB
    ' Function Name:	bGetTxnStatusEx
    ' Function Type:	Boolean.  True if get any mapping of response fields and response status successfully else false
    ' Parameter:		st_HostInfo, st_PayInfo
    ' Description:		Get mapping of response fields and response status
    ' History:			6 Dec 2013
    '********************************************************************************************
    Public Function bGetTxnStatusActionEx(ByRef sz_TxnField As String, ByRef sz_TxnStatus As String, ByRef sz_TxnAction As String, ByRef st_HostInfo As Common.STHost, ByRef st_PayInfo As Common.STPayInfo) As Boolean

        Dim bReturn As Boolean = True
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetTxnStatusActionEx", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@i_HostID", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_ActionID", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@sz_TxnType", SqlDbType.VarChar, 7)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@i_HostID").Value = st_HostInfo.iHostID
            objCommand.Parameters("@i_ActionID").Value = st_HostInfo.iTxnStatusActionID
            objCommand.Parameters("@sz_TxnType").Value = st_PayInfo.szTxnType

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            sz_TxnField = ""
            sz_TxnStatus = ""
            sz_TxnAction = ""

            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True

                While dr.Read()
                    'Example:
                    'TxnField                           TxnStatus                       Action  Desc        Priority
                    'IsFullyCaptured	                true	                            1	Success	    1
                    'IsFullyCaptured;IsReversed;sError	false;false;Success	                2	Failed	    1
                    'sError	                            TrxnSearchbyOrderIDOrderNotFound	3	Pending	    1
                    'IsReversed	                        true	                            9	Reversed	1
                    sz_TxnField = sz_TxnField + dr("TxnField").ToString() + "|"
                    sz_TxnStatus = sz_TxnStatus + dr("TxnStatus").ToString() + "|"
                    sz_TxnAction = sz_TxnAction + Convert.ToInt32(dr("Action")).ToString() + "|"
                End While
                'Removes the rightmost "|"
                sz_TxnField = Left(sz_TxnField, sz_TxnField.Length() - 1)
                sz_TxnStatus = Left(sz_TxnStatus, sz_TxnStatus.Length() - 1)
                sz_TxnAction = Left(sz_TxnAction, sz_TxnAction.Length() - 1)
            Else
                st_PayInfo.szErrDesc = "bGetTxnStatusActionEx() error: Failed to get results" + " > HostID(" + st_HostInfo.iHostID.ToString() + ") TxnStatusActionID(" + st_HostInfo.iTxnStatusActionID.ToString() + ") TxnType(" + st_PayInfo.szTxnType + ")"

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT

                bReturn = False
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetTxnStatusActionEx() exception: " + ex.StackTrace + " " + ex.Message

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProcOB
    ' Function Name:	bGetCurrency
    ' Function Type:	Boolean.  True if get currency information successfully else false
    ' Parameter:		st_HostInfo, st_PayInfo, sz_DBConnStr
    ' Description:		Maps currency from ISO4217 to ISO3166 format
    ' History:			23 Mar 2009
    '********************************************************************************************
    Public Function bGetCurrency(ByVal sz_CurrencyCode As String, ByRef st_PayInfo As Common.STPayInfo) As String

        Dim szReturn As String = ""
        Dim objConn As SqlConnection
        Dim objCommand As SqlCommand
        Dim objParam As SqlParameter

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetCurrency", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_CurrencyCode", SqlDbType.VarChar, 5)
            objCommand.Parameters("@sz_CurrencyCode").Value = sz_CurrencyCode
            'Modified the following to sz_Currency on 6 Oct 2009
            'objCommand.Parameters("@sz_CurrencyCode").Value = st_PayInfo.szCurrencyCode

            objParam = objCommand.Parameters.Add("@sz_Currency", SqlDbType.VarChar, 5)
            objParam.Direction = ParameterDirection.Output

            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objConn.Open()
            objCommand.Connection = objConn

            objCommand.ExecuteScalar()

            If objCommand.Parameters("@i_Return").Value = 0 Then
                szReturn = objCommand.Parameters("@sz_Currency").Value
            Else
                st_PayInfo.szErrDesc = "bGetCurrency() error: Failed to get currency" + " > CurrencyCode(" + sz_CurrencyCode + ")"

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
            End If

        Catch ex As Exception
            st_PayInfo.szErrDesc = "bGetCurrency() exception: " + ex.StackTrace + " " + ex.Message + " > CurrencyCode(" + sz_CurrencyCode + ")"

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT

        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return szReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProcOB
    ' Function Name:	bCheckLateResp
    ' Function Type:	Boolean.  True if not late response else false if (late response or request not found)
    ' Parameter:		st_PayInfo, sz_DBConnStr
    ' Description:		Check whether Host reply is a late response
    ' History:			31 Oct 2008
    '********************************************************************************************
    Public Function bCheckLateResp(ByRef st_PayInfo As Common.STPayInfo, ByVal sz_ReplyDateTime As String) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader
        Dim iRowsAffected As Integer = 0
        Dim iRet As Integer = 0

        Dim szRetDesc As String = ""
        Dim iLateRespTimeSec As Integer = Convert.ToInt32(ConfigurationSettings.AppSettings("LateRespTimeSec"))

        'Add , 21 Sep 2017, FPXDB2B payment will expiry aft 1 Day, convert to second
        If ("02" = st_PayInfo.szToken And InStr(st_PayInfo.szIssuingBank, "FPX") > 0) Then
            iLateRespTimeSec = 24 * 60 * 60
        End If

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spCheckLateResp", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_GatewayTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_ReplyDateTime", SqlDbType.VarChar, 23)
            objParam = objCommand.Parameters.Add("@i_LateRespTimeSec", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_GatewayTxnID").Value = st_PayInfo.szTxnID
            objCommand.Parameters("@sz_ReplyDateTime").Value = sz_ReplyDateTime
            objCommand.Parameters("@i_LateRespTimeSec").Value = iLateRespTimeSec

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If objCommand.Parameters("@i_Return").Value = 0 Then        'Non late response
                'Added on 30 May 2009, because if stored procedure got return recordset and got return value then
                'ExecuteReader will not be able to get the return value as expected but only able to get recordset
                iRet = -1
                If dr.Read() Then
                    iRet = Convert.ToInt32(dr("RetCode"))
                    szRetDesc = dr("RetDesc").ToString()
                End If

                If (0 = iRet) Then      'Non-late response
                    bReturn = True

                ElseIf (-1 = iRet) Then 'Late response
                    st_PayInfo.szErrDesc = "bCheckLateResp() error: Late Host response discarded - HostTxnStatus(" + st_PayInfo.szHostTxnStatus + ") > Host Reply Date Time(" + sz_ReplyDateTime + ") DateCreated(" + szRetDesc + ") (>" + iLateRespTimeSec.ToString() + " seconds)"

                    st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.LATE_HOST_REPLY
                    st_PayInfo.szTxnMsg = Common.C_LATE_HOST_REPLY_2908
                    st_PayInfo.iTxnState = Common.TXN_STATE.ABORT

                    bReturn = False

                Else                    'Invalid/Late response
                    st_PayInfo.szErrDesc = "bCheckLateResp() error: Invalid/Late Host response discarded - HostTxnStatus(" + st_PayInfo.szHostTxnStatus + ") " + szRetDesc

                    st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.LATE_HOST_REPLY
                    st_PayInfo.szTxnMsg = Common.C_LATE_HOST_REPLY_2908
                    st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
                    'Add , 23 Sep 2017, Record late response
                    If (iRet = -2) Then
                        If (True <> bInsertLateResponse(st_PayInfo)) Then
                            st_PayInfo.szErrDesc = st_PayInfo.szErrDesc + "error during to insert into late response record."
                        End If
                    End If

                    bReturn = False
                End If

            ElseIf objCommand.Parameters("@i_Return").Value = -1 Then   'Late response
                While dr.Read()
                    szRetDesc = dr("RetDesc").ToString()
                    dr.Read()
                End While

                st_PayInfo.szErrDesc = "bCheckLateResp() error: Late Host response discarded - HostTxnStatus(" + st_PayInfo.szHostTxnStatus + ") > Host Reply Date Time(" + sz_ReplyDateTime + ") DateCreated(" + szRetDesc + ") (>" + iLateRespTimeSec.ToString() + " seconds)"

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.LATE_HOST_REPLY
                st_PayInfo.szTxnMsg = Common.C_LATE_HOST_REPLY_2908
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT

                bReturn = False
            Else                                                        'Invalid/late response
                While dr.Read()
                    iRet = Convert.ToInt32(dr("RetCode"))       'Add , 23 Sep 2017, Get RetCode, record late response
                    szRetDesc = dr("RetDesc").ToString()
                    dr.Read()
                End While

                st_PayInfo.szErrDesc = "bCheckLateResp() error: Invalid/Late Host response discarded - HostTxnStatus(" + st_PayInfo.szHostTxnStatus + ") " + szRetDesc

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.LATE_HOST_REPLY
                st_PayInfo.szTxnMsg = Common.C_LATE_HOST_REPLY_2908
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
                'Add , 23 Sep 2017, Record late response
                If (iRet = -2) Then
                    If (True <> bInsertLateResponse(st_PayInfo)) Then
                        st_PayInfo.szErrDesc = st_PayInfo.szErrDesc + "error during to insert into late response record."
                    End If
                End If
                bReturn = False
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bCheckLateResp() exception: " + ex.StackTrace + " " + ex.Message + " > Host Reply Date Time(" + sz_ReplyDateTime + ") LateRespTimeSec(" + iLateRespTimeSec.ToString() + ")"

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Function Name:	bCheckLateResp
    ' Parameter:		st_PayInfo
    ' Return:       	Boolean
    ' Description:		True if not late response else false if (late response or request not found)
    ' History:			23 May 2016
    ' Auther:           -711
    '********************************************************************************************
    Public Function bCheckLateResp(ByRef st_PayInfo As Common.STPayInfo) As Boolean

        Dim bReturn As Boolean = False
        Dim iRet As Integer = 0
        Dim szRetDesc As String = Nothing
        Dim szReplyDateTime As String = System.DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss.fff")
        Dim iLateRespTimeSec As Integer = st_PayInfo.iOTCExpiryHour * 60 * 60 'To convert Hour to Seconds

        Dim objConn As SqlConnection
        Dim objCommand As SqlCommand
        Dim objParam As SqlParameter
        Dim dr As SqlDataReader

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spCheckLateResp", objConn)
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.Parameters.Add("@sz_GatewayTxnID", SqlDbType.VarChar, 30).Value = st_PayInfo.szTxnID
            objCommand.Parameters.Add("@sz_ReplyDateTime", SqlDbType.VarChar, 23).Value = szReplyDateTime
            objCommand.Parameters.Add("@i_LateRespTimeSec", SqlDbType.Int).Value = iLateRespTimeSec
            objCommand.Parameters.Add("@i_Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue

            objConn.Open()
            objCommand.Connection = objConn
            dr = objCommand.ExecuteReader()

            If dr.Read() Then
                iRet = Convert.ToInt32(dr("RetCode"))
                szRetDesc = dr("RetDesc").ToString()
                If objCommand.Parameters("@i_Return").Value = 0 And 0 = iRet Then
                    bReturn = True
                End If
            End If
            dr.Close()
            objConn.Close()
            If Not bReturn Then
                If (-1 = iRet) Then 'Late response
                    st_PayInfo.szErrDesc = "bCheckLateResp() error: Late Host response discarded - HostTxnStatus(" + st_PayInfo.szHostTxnStatus + ") > Host Reply Date Time(" + szReplyDateTime + ") DateCreated(" + szRetDesc + ") (>" + iLateRespTimeSec.ToString() + " seconds)"
                Else                'Invalid/Late response
                    st_PayInfo.szErrDesc = "bCheckLateResp() error: Invalid/Late Host response discarded - HostTxnStatus(" + st_PayInfo.szHostTxnStatus + ") " + szRetDesc
                End If
                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.LATE_HOST_REPLY
                st_PayInfo.szTxnMsg = Common.C_LATE_HOST_REPLY_2908
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
            End If

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bCheckLateResp() exception: " + ex.StackTrace + " " + ex.Message  ' .Message

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999

        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProcOB
    ' Function Name:	bGetMerchantRunningNo
    ' Function Type:	Boolean.  True if get merchant running number successfully else false
    ' Parameter:		st_PayInfo, sz_DBConnStr
    ' Description:		Get merchant running number based on Merchant ID
    ' History:			14 Nov 2008
    '********************************************************************************************
    Public Function bGetMerchantRunningNo(ByRef st_PayInfo As Common.STPayInfo) As Boolean

        Dim bReturn As Boolean = True
        Dim objConn As SqlConnection
        Dim objCommand As SqlCommand
        Dim objParam As SqlParameter
        Dim dr As SqlDataReader
        Dim iRowsAffected As Integer = 0

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetMerchantRunningNo", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True

                While dr.Read()
                    st_PayInfo.szRunningNo = dr("RunningNo").ToString()
                    dr.Read()
                End While
            Else
                bReturn = False

                st_PayInfo.szErrDesc = "bGetMerchantRunningNo() error: Invalid MerchantID/Merchant."

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.E_INVALID_MERCHANT
                st_PayInfo.szTxnMsg = Common.C_INVALID_MID_2901
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetMerchantRunningNo() exception: " + ex.StackTrace + " " + ex.Message  ' .Message

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProcOB
    ' Function Name:	bGetMerchantHostRunningNo
    ' Function Type:	Boolean.  True if get merchant-host running number/trace number successfully else false
    ' Parameter:		st_PayInfo, st_HostInfo, sz_DBConnStr
    ' Description:		Get merchant-host running number/trace number based on Merchant ID and Host ID
    ' History:			18 Sept 2010
    '********************************************************************************************
    Public Function bGetMerchantHostRunningNo(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost) As Boolean

        Dim bReturn As Boolean = True
        Dim objConn As SqlConnection
        Dim objCommand As SqlCommand
        Dim objParam As SqlParameter
        Dim dr As SqlDataReader
        Dim iRowsAffected As Integer = 0

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetMerchantHostRunningNo", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@i_HostID", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_NoOfRunningNo", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_UniquePerDay", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_Today", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@i_HostID").Value = st_HostInfo.iHostID
            objCommand.Parameters("@i_NoOfRunningNo").Value = st_HostInfo.iNoOfRunningNo
            objCommand.Parameters("@i_UniquePerDay").Value = st_HostInfo.iRunningNoUniquePerDay
            objCommand.Parameters("@i_Today").Value = Convert.ToInt32(Left(st_PayInfo.szTxnDateTime.Replace("-", "").Replace(":", "").Replace(" ", ""), 8))

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True

                While dr.Read()
                    st_HostInfo.szRunningNo = dr("RunningNo").ToString()
                    dr.Read()
                End While
            Else
                bReturn = False

                st_PayInfo.szErrDesc = "bGetMerchantHostRunningNo() error: Invalid MerchantID/HostID"

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.E_INVALID_MERCHANT
                st_PayInfo.szTxnMsg = Common.C_INVALID_MID_2901
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetMerchantHostRunningNo() exception: " + ex.StackTrace + " " + ex.Message  ' .Message

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProcOB
    ' Function Name:	bGetTxnIDRunningNo
    ' Function Type:	Boolean.  True if get host running number successfully else false
    ' Parameter:		st_PayInfo, st_HostInfo, sz_DBConnStr
    ' Description:		Get host running number based on GatewayTxnIDFormat
    ' History:			30 Oct 2009
    '********************************************************************************************
    Public Function bGetTxnIDRunningNo(ByRef st_PayInfo As Common.STPayInfo, ByVal st_HostInfo As Common.STHost) As Boolean

        Dim bReturn As Boolean = True
        Dim objConn As SqlConnection
        Dim objCommand As SqlCommand
        Dim objParam As SqlParameter
        Dim dr As SqlDataReader
        Dim iRowsAffected As Integer = 0

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetTxnIDRunningNo", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@i_GatewayTxnIDFormat", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@i_GatewayTxnIDFormat").Value = st_HostInfo.iGatewayTxnIDFormat

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True

                While dr.Read()
                    st_PayInfo.szRunningNo = dr("RunningNo").ToString()
                    st_PayInfo.iTxnIDMaxLen = Convert.ToInt32(dr("MaxLen"))
                    dr.Read()
                End While
            Else
                bReturn = False

                st_PayInfo.szErrDesc = "bGetTxnIDRunningNo() error: Invalid GatewayTxnIDFormat."

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INVALID_INPUT
                st_PayInfo.szTxnMsg = Common.C_INVALID_INPUT_2906
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetTxnIDRunningNo() exception: " + ex.StackTrace + " " + ex.Message  ' .Message

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProcOB
    ' Function Name:	bUpdateHostInfo
    ' Function Type:	Boolean.  True if update successful else false
    ' Parameter:		st_PayInfo, st_HostInfo, sz_DBConnStr
    ' Description:		Updates Host table
    ' History:			13 Mar 2009
    '********************************************************************************************
    Public Function bUpdateHostInfo(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost) As Boolean

        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection
        Dim objCommand As SqlCommand
        Dim objParam As SqlParameter
        Dim dr As SqlDataReader
        Dim iRowsAffected As Integer = 0

        Try
            bReturn = False
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spUpdateHostInfo", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@i_HostID", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_QueryFlag", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@i_HostID").Value = st_HostInfo.iHostID
            objCommand.Parameters("@i_QueryFlag").Value = st_HostInfo.iQueryFlag

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()
            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True
            Else
                bReturn = False

                st_PayInfo.szErrDesc = "bUpdateHostInfo() error: Failed to update QueryFlag(" + st_HostInfo.iQueryFlag.ToString() + ") for GatewayTxnID(" + st_PayInfo.szTxnID + ")"

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.iTxnState = Common.TXN_STATE.FAILED
                st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bUpdateHostInfo() exception: " + ex.ToString + " > QueryFlag(" + st_HostInfo.iQueryFlag.ToString() + ") for GatewayTxnID(" + st_PayInfo.szTxnID + ")"

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.FAILED
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProcOB
    ' Function Name:	bInsertMCCReqTxn
    ' Function Type:	Boolean.  True if inserted successfully else false
    ' Parameter:		st_PayInfo, sz_DBConnStr
    ' Description:		Insert transaction into MCCReq table
    ' History:			9 Feb 2012
    '********************************************************************************************
    Public Function bInsertMCCReqTxn(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost, ByRef szRetDesc As String) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection
        Dim objCommand As SqlCommand
        Dim objParam As SqlParameter
        Dim dr As SqlDataReader
        Dim iRet As Integer = 2

        Dim iRowsAffected As Integer = 0

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spInsertMCCReqTxn", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@i_HostID", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@sz_MerchantTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_GatewayTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_OrderNumber", SqlDbType.VarChar, 20)
            objParam = objCommand.Parameters.Add("@sz_CurrencyCode", SqlDbType.VarChar, 3)
            objParam = objCommand.Parameters.Add("@sz_BaseCurrencyCode", SqlDbType.VarChar, 3)
            objParam = objCommand.Parameters.Add("@d_TxnAmt", SqlDbType.Decimal, 18)
            objParam = objCommand.Parameters.Add("@d_BaseTxnAmt", SqlDbType.Decimal, 18)
            'objParam = objCommand.Parameters.Add("@sz_Param5", SqlDbType.VarChar, 100)
            objParam = objCommand.Parameters.Add("@sz_DateCreated", SqlDbType.VarChar, 23)
            objParam = objCommand.Parameters.Add("@i_TxnDay", SqlDbType.Int)

            'objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            'objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@i_HostID").Value = st_HostInfo.iHostID
            objCommand.Parameters("@sz_MerchantTxnID").Value = st_PayInfo.szMerchantTxnID
            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@sz_GatewayTxnID").Value = st_PayInfo.szTxnID
            objCommand.Parameters("@sz_OrderNumber").Value = st_PayInfo.szMerchantOrdID
            objCommand.Parameters("@sz_CurrencyCode").Value = st_PayInfo.szCurrencyCode
            objCommand.Parameters("@sz_BaseCurrencyCode").Value = st_PayInfo.szBaseCurrencyCode
            objCommand.Parameters("@d_TxnAmt").Value = Convert.ToDecimal(st_PayInfo.szTxnAmount)
            objCommand.Parameters("@d_BaseTxnAmt").Value = Convert.ToDecimal(st_PayInfo.szBaseTxnAmount)
            'objCommand.Parameters("@sz_Param5").Value = st_PayInfo.szParam5

            'Assigns the system date of the App Server to szTxnDateTime so that the datetime value sent to the bank
            'in payment request will be the same as the datetime value (retrieved from database) sent to the bank in
            'query request because this system date is inserted into database and not the database system date (GETDATE()).
            st_PayInfo.szTxnDateTime = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
            objCommand.Parameters("@sz_DateCreated").Value = st_PayInfo.szTxnDateTime
            objCommand.Parameters("@i_TxnDay").Value = Convert.ToInt32(Left(st_PayInfo.szTxnDateTime, 10).Replace("-", ""))

            objConn.Open()
            dr = objCommand.ExecuteReader()

            While dr.Read()
                iRet = Convert.ToInt32(dr("RetCode"))
                szRetDesc = dr("RetDesc").ToString() '+ "-" + iRet.ToString()
            End While
            'End If

            If (0 = iRet) Then
                bReturn = True
            End If

            dr.Close()
            objConn.Close()

        Catch ex As Exception
            bReturn = False
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProcOB
    ' Function Name:	bGetMerchantIDbyTxnID
    ' Function Type:	Boolean.  True if get merchant information successfully else false
    ' Parameter:		st_PayInfo, sz_DBConnStr
    ' Description:		get MerchantID from TB_PayReq table
    ' History:			25 May 2012 Add  for integration of Regional DD - UOB Thailand
    '********************************************************************************************
    Public Function bGetMerchantIDbyTxnID(ByRef st_PayInfo As Common.STPayInfo) As Boolean

        Dim bReturn As Boolean = True
        Dim objConn As SqlConnection
        Dim objCommand As SqlCommand
        Dim objParam As SqlParameter
        Dim dr As SqlDataReader
        Dim iRowsAffected As Integer = 0

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetMerchantIDbyTxnID", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_GatewayTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_GatewayTxnID").Value = st_PayInfo.szTxnID

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True

                While dr.Read()
                    st_PayInfo.szMerchantID = dr("MerchantID").ToString()
                End While

            Else
                bReturn = False

                st_PayInfo.szErrDesc = "bGetMerchantIDbyTxnID() error: MerchantID not found."

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.E_INVALID_MERCHANT
                st_PayInfo.szTxnMsg = Common.C_INVALID_MID_2901
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetMerchantIDbyTxnID() exception: " + ex.StackTrace + " " + ex.Message  ' .Message

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProcOB
    ' Function Name:	bGetMerchantID
    ' Function Type:	Boolean.  True if get merchant information successfully else false
    ' Parameter:		st_PayInfo, sz_DBConnStr
    ' Description:		get MerchantID from Terminal table
    ' History:			24 Apr 2012 Add  for integration of Regional DD - SCB Thailand
    '********************************************************************************************
    Public Function bGetMerchantID(ByRef st_PayInfo As Common.STPayInfo) As Boolean

        Dim bReturn As Boolean = True
        Dim objConn As SqlConnection
        Dim objCommand As SqlCommand
        Dim objParam As SqlParameter
        Dim dr As SqlDataReader
        Dim iRowsAffected As Integer = 0

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetMerchantID", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            'Can add payeecode in this store proc when needed. But need to pass in st_HostInfo because payeecode is under HostInfo stucture.
            'objParam = objCommand.Parameters.Add("@sz_PayeeCode", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_HostMID", SqlDbType.VarChar, 20)
            objParam = objCommand.Parameters.Add("@sz_HostTID", SqlDbType.VarChar, 20)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            'objCommand.Parameters("@sz_PayeeCode").Value = 
            objCommand.Parameters("@sz_HostMID").Value = st_PayInfo.szHostMID
            objCommand.Parameters("@sz_HostTID").Value = st_PayInfo.szHostTID

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If objCommand.Parameters("@i_Return").Value = 0 Then
                While dr.Read()
                    st_PayInfo.szMerchantID = dr("MerchantID").ToString()
                    st_PayInfo.szEncryptKeyPath = dr("SendKeyPath").ToString() 'Added 15 March 2017, SCBTH
                End While

                bReturn = True
            Else
                st_PayInfo.szErrDesc = "bGetMerchantID() error: Invalid MerchantID/Merchant."

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.E_INVALID_MERCHANT
                st_PayInfo.szTxnMsg = Common.C_INVALID_MID_2901
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT

                bReturn = False
            End If

            dr.Close()

        Catch ex As Exception
            st_PayInfo.szErrDesc = "bGetMerchantID() exception: " + ex.StackTrace + " " + ex.Message  ' .Message

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999

            bReturn = False
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProcOB
    ' Function Name:	bGetTxnID
    ' Function Type:	Boolean.  True if successfully retrieved record or else false
    ' Parameter:		
    ' Description:		Get transaction ID based on specific field. This function is to retrieve the ACTUAL GatewayTxnID.
    ' History:			8 Aug 2012 (Currently for KTB Thailand redirect response.)
    '********************************************************************************************
    Public Function bGetTxnID(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection
        Dim objCommand As SqlCommand
        'Dim objAdp As SqlDataAdapter
        'Dim dt As DataTable
        Dim objParam As SqlParameter
        Dim dr As SqlDataReader
        'Dim iRowsAffected As Integer = 0
        Dim iRet As Integer = -1

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetTxnID", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@i_HostID", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@sz_HostTxnID", SqlDbType.VarChar, 30) 'This HostTxnID is not the actual GatewayTxnID DDGW pass to Host
            objParam = objCommand.Parameters.Add("@sz_HostDate", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@d_Amt", SqlDbType.Decimal, 18)
            'objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            'objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@i_HostID").Value = st_HostInfo.iHostID
            objCommand.Parameters("@sz_HostTxnID").Value = st_HostInfo.szReserved
            objCommand.Parameters("@sz_HostDate").Value = st_PayInfo.szHostDate 'Convert.ToDateTime(st_PayInfo.szHostDate.Substring(0, 4) + "-" + st_PayInfo.szHostDate.Substring(4, 2) + "-" + st_PayInfo.szHostDate.Substring(6, 2))
            objCommand.Parameters("@d_Amt").Value = Convert.ToDecimal(st_PayInfo.szTxnAmount)

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            'If objCommand.Parameters("@i_Return").Value = 0 Then
            If (dr.HasRows) Then
                While dr.Read()
                    st_PayInfo.szTxnID = dr("GatewayTxnID").ToString()
                End While

                bReturn = True
            Else
                st_PayInfo.szErrDesc = "bGetTxnID() Failed to get transaction info iRet(" + iRet.ToString() + ") HostTxnID(" + st_HostInfo.szReserved + ") HostID(" + st_HostInfo.iHostID.ToString() +
                                        ") HostDate(" + st_PayInfo.szHostDate + ") Amount(" + st_PayInfo.szTxnAmount + ")"

                bReturn = False
            End If

            dr.Close()

        Catch ex As Exception
            st_PayInfo.szTxnMsg = "Failed to retrieve transaction info"
            st_PayInfo.szErrDesc = "bGetTxnID() Exception: " + ex.StackTrace + ex.Message + " > Failed to get transaction info based on HostTxnID(" + st_HostInfo.szReserved + ") HostID(" + st_HostInfo.iHostID.ToString() + ")"

            bReturn = False
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProcOB
    ' Function Name:	bGetOTCSecureCode
    ' Function Type:	Boolean.  True if get OTC Secure Code successfully
    ' Parameter:		st_PayInfo
    ' Description:		Get OTC Secure Code
    ' History:			30 Jun 2015 
    '********************************************************************************************
    Public Function bGetOTCSecureCode(ByRef st_PayInfo As Common.STPayInfo) As Boolean
        Dim bReturn As Boolean = True
        Dim objConn As SqlConnection
        Dim objCommand As SqlCommand
        Dim objParam As SqlParameter
        Dim dr As SqlDataReader
        Dim iRet As Integer = 0

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetOTCSecureCode", objConn)
            objCommand.CommandType = CommandType.StoredProcedure
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            iRet = Convert.ToInt32(objCommand.Parameters("@i_Return").Value)

            If iRet = 0 Then
                bReturn = True

                While dr.Read()
                    st_PayInfo.szOTCSecureCode = dr("OTCSecureCode").ToString()
                    dr.Read()
                End While
            Else
                bReturn = False

                st_PayInfo.szErrDesc = "bGetOTCSecureCode() error: Failed to generate Secure Code."

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
                st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetOTCSecureCode() exception: " + ex.StackTrace + " " + ex.Message  ' .Message

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999

        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProcOB
    ' Function Name:	bUpdateOriTxnStatus
    ' Function Type:	Boolean.  True if update successful else false
    ' Parameter:		st_PayInfo
    ' Description:		Updates reversal pending transaction status to original status in either Request/Response table
    ' History:			10 Sept 2013
    '********************************************************************************************
    Public Function bUpdateOriTxnStatus(ByRef st_PayInfo As Common.STPayInfo) As Boolean

        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRowsAffected As Integer = 0

        Try
            bReturn = False

            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spUpdateOriTxnStatus", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@i_ReqOrRes", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_PKID", SqlDbType.BigInt)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@i_ReqOrRes").Value = st_PayInfo.iQueryStatus
            objCommand.Parameters("@i_PKID").Value = st_PayInfo.iPKID

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()
            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True
            Else
                bReturn = False

                st_PayInfo.szErrDesc = "bUpdateOriTxnStatus() error: Failed to update back to original status," + " ReqOrRes(" + st_PayInfo.iQueryStatus.ToString() + ") PKID(" + st_PayInfo.iPKID.ToString() + ") GatewayTxnID(" + st_PayInfo.szTxnID + ")"

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_INT_ERR
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
                st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bUpdateOriTxnStatus() exception: " + ex.ToString + " > " + " ReqOrRes(" + st_PayInfo.iQueryStatus.ToString() + ") PKID (" + st_PayInfo.iPKID.ToString() + ") GatewayTxnID(" + st_PayInfo.szTxnID + ")"

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_INT_ERR
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function
    '********************************************************************************************
    ' Class Name:		TxnProcOB
    ' Function Name:	bCheckNInsRefundTxn
    ' Function Type:	Boolean.  True if successfully retrieved record or else false
    ' Parameter:		
    ' Description:		Check transaction details meet refund requirement before send to bank host
    ' History:			13 July 2015. REFUND
    ' Modified:         22 Sept 2015. REFUND
    '********************************************************************************************
    Public Function bCheckNInsRefundTxn(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost) As Integer
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRet As Integer = -1

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spCheckNInsRefundTxn", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@i_HostID", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_MTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@d_RefundAmt", SqlDbType.Decimal, 18)
            objParam = objCommand.Parameters.Add("@d_OriAmt", SqlDbType.Decimal, 18)

            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@i_HostID").Value = st_HostInfo.iHostID
            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@sz_MTxnID").Value = st_PayInfo.szMerchantTxnID
            objCommand.Parameters("@d_RefundAmt").Value = Convert.ToDecimal(st_PayInfo.szRTxnAmount)
            objCommand.Parameters("@d_OriAmt").Value = Convert.ToDecimal(st_PayInfo.szTxnAmountOri)

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            'Return value can be -1 (refund failed), 0 (new refund), 1 (retry refund query needed)
            While dr.Read()
                iRet = Convert.ToInt32(dr("RetCode"))
                If (-1 = iRet) Then
                    st_PayInfo.szErrDesc = dr("RetDesc").ToString()
                ElseIf (0 = iRet Or 1 = iRet) Then
                    st_PayInfo.szHostRefundTxnID = dr("GatewayRefundTxnID").ToString()
                End If
            End While

            dr.Close()

        Catch ex As Exception
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "bCheckNInsRefundTxn() exception: " + ex.ToString)

        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return iRet

    End Function
    '********************************************************************************************
    ' Class Name:		TxnProcOB
    ' Function Name:	bUpdateRefundTxnStatus
    ' Function Type:	Boolean.  True if update successful else false
    ' Parameter:		st_PayInfo, st_HostInfo
    ' Description:		Updates TB_PayRes_Multi table
    ' History:			29 Feb 2016
    '********************************************************************************************
    Public Function bUpdateRefundTxnStatus(ByRef st_PayInfo As Common.STPayInfo) As Boolean

        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRowsAffected As Integer = 0

        Dim iShopperEmailNotifyStatus As Integer = -1       'Added  28 Nov 2014 - ENS
        Dim iMerchantEmailNotifyStatus As Integer = -1      'Added  28 Nov 2014 - ENS

        Try
            bReturn = False
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spUpdateRefundTxnStatus", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@i_PayResID", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_MerchantTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@d_RefundAmt", SqlDbType.Decimal, 18)
            objParam = objCommand.Parameters.Add("@d_OriAmt", SqlDbType.Decimal, 18)
            objParam = objCommand.Parameters.Add("@i_TxnStatus", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@sz_BankRefNo", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@i_PayResID").Value = st_PayInfo.iPKID
            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@sz_MerchantTxnID").Value = st_PayInfo.szMerchantTxnID
            objCommand.Parameters("@d_RefundAmt").Value = st_PayInfo.szRTxnAmount
            objCommand.Parameters("@d_OriAmt").Value = st_PayInfo.szTxnAmountOri
            objCommand.Parameters("@i_TxnStatus").Value = st_PayInfo.iTxnStatus
            objCommand.Parameters("@sz_BankRefNo").Value = st_PayInfo.szHostTxnID

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()
            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True
            Else
                bReturn = False

                st_PayInfo.szErrDesc = "bUpdateRefundTxnStatus() error: TxnStatus(" + st_PayInfo.iTxnStatus.ToString() + ") TxnPKID(" + st_PayInfo.iPKID.ToString() + ") for MerchantPymtID(" + st_PayInfo.szMerchantTxnID + ")"

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.iTxnState = Common.TXN_STATE.FAILED
                st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bUpdateRefundTxnStatus() exception: " + ex.ToString + " > TxnStatus(" + st_PayInfo.iTxnStatus.ToString() + ") TxnPKID(" + st_PayInfo.iPKID.ToString() + ") for GatewayTxnID(" + st_PayInfo.szMerchantTxnID + ")"

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.FAILED
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProcOB
    ' Function Name:	bGetRefundResTxn
    ' Function Type:	Boolean.  True if successfully retrieved record or else false
    ' Parameter:		
    ' Description:		Get refund result from TB_PayRes_Multi table
    ' History:			2 March 2016
    '********************************************************************************************
    Public Function bGetRefundResTxn(ByRef st_PayInfo As Common.STPayInfo) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection
        Dim objCommand As SqlCommand
        Dim objParam As SqlParameter
        Dim iRowsAffected As Integer = 0
        Dim dr As SqlDataReader

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetRefundResTxn", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@i_PayResID", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@sz_GatewayRefundTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@d_RefundAmt", SqlDbType.Decimal, 18)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@i_PayResID").Value = st_PayInfo.iPKID
            objCommand.Parameters("@sz_GatewayRefundTxnID").Value = st_PayInfo.szHostRefundTxnID
            objCommand.Parameters("@d_RefundAmt").Value = st_PayInfo.szRTxnAmount

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If dr.Read() Then
                st_PayInfo.iQueryStatus = Convert.ToInt32(dr("RetCode"))
                st_PayInfo.szQueryMsg = dr("RetDesc").ToString()
                st_PayInfo.iTxnStatus = Convert.ToInt32(dr("TxnStatus"))
                st_PayInfo.szHostTxnID = dr("BankRefNo").ToString()
                ' 1 - Transaction exists in Response table
                If (1 = st_PayInfo.iQueryStatus) Then
                    st_PayInfo.iQueryStatus = -1
                End If

                bReturn = True
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetRefundResTxn() exception: " + ex.StackTrace + ex.Message + " > GatewayTxnID(" + st_PayInfo.szTxnID + ")"

            st_PayInfo.iQueryStatus = 2
            st_PayInfo.szQueryMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Function Name:	bGetOTCOrderDetails
    ' Parameter:		st_PayInfo
    ' Return:       	Boolean
    ' Description:		--
    ' History:			01 July 2015
    ' Auther:           -711
    '********************************************************************************************
    Public Function bGetOTCOrderDetails(ByRef st_PayInfo As Common.STPayInfo) As Boolean

        Dim bReturn As Boolean = False

        Dim objConn As SqlConnection
        Dim objCommand As SqlCommand
        Dim dr As SqlDataReader

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetOTCOrderDetails", objConn)
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.Parameters.Add("@sz_SecureCode", SqlDbType.VarChar, 20).Value = st_PayInfo.szOTCSecureCode
            objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.Char, 30).Value = st_PayInfo.szMerchantID

            objConn.Open()
            objCommand.Connection = objConn
            dr = objCommand.ExecuteReader()
            While dr.Read()
                st_PayInfo.szTxnID = dr("GatewayTxnID").ToString()
                st_PayInfo.szMerchantTxnID = dr("GatewayTxnID").ToString()
                st_PayInfo.szDetailID = dr("DetailID").ToString()
                st_PayInfo.szTxnAmount = dr("TxnAmt").ToString()
                st_PayInfo.szCurrencyCode = dr("CurrencyCode").ToString()
                bReturn = True
                dr.Read()
            End While
            dr.Close()
            objConn.Close()

            If Not bReturn Then
                st_PayInfo.szErrDesc = "bGetOTCOrderDetails() error: Failed to get OTC Order Details> GatewayTxnID(" + st_PayInfo.szTxnID + ")"
                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.szTxnMsg = Common.C_TXN_NOT_FOUND_21
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
            End If

        Catch ex As Exception
            Throw ex
        Finally

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn

    End Function
    '********************************************************************************************
    ' Class Name:		TxnProcOB
    ' Function Name:	bGetTxnSecureCode
    ' Function Type:	Boolean.  True if get OTC Secure Code successfully
    ' Parameter:		st_PayInfo
    ' Description:		Get OTC Secure Code
    ' History:			
    '********************************************************************************************
    Public Function bGetTxnSecureCode(ByRef st_PayInfo As Common.STPayInfo) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetTxnSecureCode", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.Char, 30)
            objParam = objCommand.Parameters.Add("@sz_GatewayTxnID", SqlDbType.VarChar, 30)

            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@sz_GatewayTxnID").Value = st_PayInfo.szTxnID

            objConn.Open()
            dr = objCommand.ExecuteReader()

            If (dr.Read()) Then 'record found in table, return value from table
                st_PayInfo.szOTCSecureCode = dr("SecureCode").ToString()
                st_PayInfo.szReqTime = dr("ReqTime").ToString()
                bReturn = True
            End If
            If Not bReturn Then
                st_PayInfo.szErrDesc = "bGetTxnSecureCode() error: Failed to get OTC Secure code> GatewayTxnID(" + st_PayInfo.szTxnID + ")"
                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.szTxnMsg = Common.C_TXN_NOT_FOUND_21
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
            End If
            dr.Close()

        Catch ex As Exception
            bReturn = False

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
            DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "bGetTxnSecureCode() exception: " + ex.ToString)

        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProcOB
    ' Function Name:	bCheckExpCurrency
    ' Function Type:	Boolean.  True if successfully retrieved record or else false
    ' Parameter:		
    ' Description:		Get currency exponent from CL_CurrencyCode table based on Currency Code
    ' History:			15 Feb 2017
    '********************************************************************************************
    Public Function bCheckExpCurrency(ByVal sz_CurrencyCode As String, ByVal i_HostID As Integer) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing

        Try

            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spCheckCurrencyIsExponent", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_CurrencyCode", SqlDbType.VarChar, 3)
            objParam = objCommand.Parameters.Add("@i_HostID", SqlDbType.Int)

            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_CurrencyCode").Value = sz_CurrencyCode
            objCommand.Parameters("@i_HostID").Value = i_HostID

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True
            Else
                bReturn = False
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function
    '********************************************************************************************
    ' Class Name:		TxnProcOB
    ' Function Name:	bInsertLateResponse
    ' Function Type:	Boolean.  True if successfully insert record or else false
    ' Parameter:		
    ' Description:		Insert Late Response
    ' History:			22 Sep 2017
    '********************************************************************************************
    Public Function bInsertLateResponse(ByRef st_PayInfo As Common.STPayInfo) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing

        Try

            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spInsertLateResp", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_GatewayTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_BankRespCode", SqlDbType.VarChar, 20)
            objParam = objCommand.Parameters.Add("@sz_BankRefNo", SqlDbType.VarChar, 40)

            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_GatewayTxnID").Value = st_PayInfo.szTxnID
            If (st_PayInfo.szRespCode <> "") Then
                objCommand.Parameters("@sz_BankRespCode").Value = st_PayInfo.szRespCode
            Else
                objCommand.Parameters("@sz_BankRespCode").Value = st_PayInfo.szHostTxnStatus
            End If
            If (st_PayInfo.szHostTxnID <> "") Then
                objCommand.Parameters("@sz_BankRefNo").Value = st_PayInfo.szHostTxnID
            Else
                objCommand.Parameters("@sz_BankRefNo").Value = st_PayInfo.szAuthCode
            End If

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If objCommand.Parameters("@i_Return").Value >= 0 Then
                bReturn = True
            Else
                bReturn = False
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProcOB
    ' Function Name:	bCheckExistHostGroup
    ' Function Type:	Boolean.  True if exist else false
    ' Parameter:		st_HostInfo, sz_DBConnStr
    ' Description:		Check the uniqueness of the transaction
    ' History:			17 Apr 2019
    '********************************************************************************************
    Public Function bCheckExistHostGroup(ByRef stBankID As String) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection
        Dim objCommand As SqlCommand
        Dim objParam As SqlParameter

        Dim iRowsAffected As Integer = 0

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spCheckExistHostGroup", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_BankID", SqlDbType.VarChar, 16)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_BankID").Value = stBankID

            objConn.Open()
            iRowsAffected = objCommand.ExecuteNonQuery()

            If objCommand.Parameters("@i_Return").Value = 1 Then
                'Host ID exist in Host Group
                bReturn = True
            Else
                'Host ID does not exist in Host Group
                bReturn = False
            End If

        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    Public Sub New(ByRef o_LoggerII As LoggerII.CLoggerII)
        Try
            objLoggerII = o_LoggerII

            objHash = New Hash(objLoggerII)      'Modified 11 Apr 2014, moved from above to here

            'szDBConnStr = ConfigurationManager.AppSettings("DBString_DD2")   'Modified 11 Oct 2017, use DBString_DD2 encrypted by non card key
            szDBConnStr = ConfigurationManager.AppSettings("OBDBString")   'Modified 11 Oct 2017, use DBString_DD2 encrypted by non card key

            'Added, 24 Oct 2013, OB DB connection string
            If (szDBConnStr <> "") Then
                If (InStr(szDBConnStr.ToLower(), "uid=") <= 0) Then
                    'szDBConnStr = objHash.szEncrypt3DES("Server=(LocalDB)\MSSQLLocalDB;database=OB", Common.C_3DESAPPKEY2, Common.C_3DESAPPVECTOR)
                    szDBConnStr = objHash.szDecrypt3DES(szDBConnStr, Common.C_3DESAPPKEY2, Common.C_3DESAPPVECTOR)
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class
