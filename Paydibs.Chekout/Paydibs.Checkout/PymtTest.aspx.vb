﻿Public Class PymtTest
    Inherits System.Web.UI.Page
    Public g_szHTML As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        '        Try
        Response.Cache.SetNoStore()
        g_szHTML = "<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.01 Transitional//EN"" ""http://www.w3.org/TR/html4/loose.dtd""><html><head><title>Process Secure Payment</title><meta http-equiv=""content-type"" content=""text/html;charset=UTF-8""><meta name=""description"" content=""Process Secure Payment""><meta name=""robots"" content=""noindex""><style type=""text/css"">body {font-family:""Trebuchet MS"",sans-serif; background-color: #FFFFFF; }#msg {border:5px solid #666; background-color:#fff; margin:20px; padding:25px; max-width:40em; -webkit-border-radius: 10px; -khtml-border-radius: 10px; -moz-border-radius: 10px; border-radius: 10px;}#submitButton { text-align: center ; }#footnote {font-size:0.8em;}</style></head><body onload=""return window.document.echoForm.submit()""><form name=""echoForm"" method=""POST"" action=""https://bne-stripe2.ap.gateway.mastercard.com/acs/MastercardACS/2162235c-a4fc-4c53-aae5-645b4aae2ff7"" accept-charset=""UTF-8""><input type=""hidden"" name=""PaReq"" value=""eAFVUctuwjAQvFfiH6Lcix8kkKLFKDRCUAmEeFSCm5sYSEscyKMKf981JKX1yTPrHc/OwrBKTta3yvI41QObtaltKR2mUawPA3uzHj979lC0nmB9zJQKViosMyVgpvJcHpQVRwObUzwO491O12W2gIW/VBcBtaZAyTYH0kBszcKj1IUAGV5G07lwHY8xB0gNIVHZNBCUshfmuajcY0DuHGiZKLHwt8F0tLJWwdwaTQIgNxbCtNRFdhWO6wFpAJTZSRyL4twnRCXydGqf5TWKP/J2mCYEiCkDeVhalMZcjhNWcSR2G/lZ7fTYr45f232q33f8TeW83G/8ARDzAiJZKMEp82iPOhblfd7rOy6QGw8yMabEbLu0ljMMguKUdwrO5if/Dpgp/CUAU85wDc00DQJVnVOtUBIT/b0Dedh+nZhcwwITdBnvOBhgfTCVumBUYkyKM4pOawDEtJJ6eRjJbcHI/Ft86+kHAHuzfQ==""><input type=""hidden"" name=""TermUrl"" value=""https://dev.paydibs.com/ppgsg/resprhb.aspx""><input type=""hidden"" name=""MD"" value=""""><noscript><div id=""msg""><div id=""submitButton""><input type=""submit"" value=""Click here to continue"" class=""button""></div></div></noscript></form></body></html>"

        '            If Not Page.IsPostBack Then
        '                'Dim sz_HTTPString As String = "apiOperation=PAY&order.id=Test201805300002&sourceOfFunds.type=CARD&sourceOfFunds.provided.card.number=5123450000000008&sourceOfFunds.provided.card.expiry.month=05&sourceOfFunds.provided.card.expiry.year=21&sourceOfFunds.provided.card.securityCode=100&order.amount=10.00&order.currency=MYR&merchant=TEST001918500471&apiUsername=merchant.TEST001918500471&apiPassword=d253e202f17dc6b0212f5a9fb88cd95e&order.reference=Test201805300002&transaction.reference=OM00000000Test201805300002"
        '                Dim sz_HTTPString As String = "apiOperation=CHECK_3DS_ENROLLMENT&3DSecureId=201806181601&3DSecure.authenticationRedirect.responseUrl=https://dev.paydibs.com/ppgsg/respRHB.aspx&order.amount=10.00&order.currency=MYR&sourceOfFunds.provided.card.number=5123450000000008&sourceOfFunds.provided.card.expiry.month=05&sourceOfFunds.provided.card.expiry.year=21&merchant=TEST001918500471&apiUsername=merchant.TEST001918500471&apiPassword=d253e202f17dc6b0212f5a9fb88cd95e"
        '                'Dim sz_HTTPString As String = "apiOperation=PROCESS_ACS_RESULT&3DSecureId=201806181601&3DSecure.paRes=&merchant=TEST001918500471&apiUsername=merchant.TEST001918500471&apiPassword=d253e202f17dc6b0212f5a9fb88cd95e"
        '                Dim sz_URL As String = "https://ap-gateway.mastercard.com/api/nvp/version/47"

        '                Dim i_Timeout As Integer = 3000
        '                Dim sz_HTTPResponse As String = ""
        '                Dim sz_ContentType As String = "x-www-form-urlencoded; charset=iso-8859-1"
        '                Dim sz_Authenticate As String = ""
        '                Dim sz_UserName As String = "TEST001918500471"
        '                Dim sz_Password As String = "d253e202f17dc6b0212f5a9fb88cd95e"
        '            End If

        '        Catch ex As Exception

        '        End Try

    End Sub

    '    Public Function iHTTPostWithRecvMPGS(ByVal sz_URL As String, ByVal sz_HTTPString As String, ByVal i_Timeout As Integer, ByRef sz_HTTPResponse As String, ByVal sz_LogString As String, ByVal sz_ContentType As String, ByVal sz_Authenticate As String, Optional ByVal sz_UserName As String = "", Optional ByVal sz_Password As String = "", Optional ByVal sz_SecurityProtocol As String = "TLS") As Integer
    '        Dim iRet As Integer = -1
    '        Dim objHttpWebRequest As HttpWebRequest
    '        Dim objHTTPWebResponse As HttpWebResponse
    '        Dim objStreamWriter As StreamWriter
    '        Dim objStreamReader As StreamReader
    '        Dim sbInStream As System.Text.StringBuilder
    '        Dim iStartTickCount As Integer
    '        Dim iTimeout As Integer
    '        Dim iResCode As Integer
    '        Dim byteToSend() As Byte = Nothing
    '        Dim byteISO8859() As Byte = Nothing
    '        Dim objWriter
    '        Dim bInStream = False

    '        Try
    '            '======================================== Sets HttpWebRequest properties ====================================
    '            If (sz_HTTPString <> "") Then
    '                byteToSend = Encoding.UTF8.GetBytes(sz_HTTPString)
    '                byteISO8859 = Encoding.Convert(Encoding.UTF8, Encoding.GetEncoding("iso-8859-1"), byteToSend)
    '            End If

    '            iTimeout = i_Timeout * 1000
    '            objHttpWebRequest = WebRequest.Create(sz_URL)                       'Posting URL

    '            If ("" = sz_ContentType) Then
    '                sz_ContentType = "x-www-form-urlencoded"    'default value
    '            End If

    '            objHttpWebRequest.ContentType = "application/" + sz_ContentType
    '            objHttpWebRequest.Method = "POST"                                   'default value
    '            objHttpWebRequest.ContentLength = byteISO8859.Length
    '            objHttpWebRequest.KeepAlive = False
    '            objHttpWebRequest.Timeout = iTimeout                                '(in milliseconds)

    '            If (sz_UserName <> "" And sz_Password <> "") Then
    '                Dim C As String
    '                C = String.Format("{0}:{1}", sz_UserName, sz_Password)

    '                objHttpWebRequest.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes(C)))
    '                objHttpWebRequest.Credentials = New NetworkCredential(sz_UserName, sz_Password)
    '            End If

    '            'ServicePointManager.CertificatePolicy = New SecPolicy

    '            'Added, 19 Jun 2015
    '            If (sz_SecurityProtocol.Trim() <> "") Then
    '                Select Case sz_SecurityProtocol.Trim().ToUpper()
    '                    Case "SSL3"
    '                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3
    '                    Case "TLS12"
    '                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
    '                    Case "TLS11"
    '                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11
    '                    Case "TLS"
    '                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
    '                    Case Else
    '                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3
    '                End Select

    '                ' allows for validation of SSL conversations
    '                ServicePointManager.ServerCertificateValidationCallback = Function() True
    '            End If

    '            iStartTickCount = System.Environment.TickCount()

    '            '=========================================== Post request ==================================================

    '            If (sz_HTTPString <> "") Then
    '                objWriter = objHttpWebRequest.GetRequestStream()
    '                objWriter.Write(byteToSend, 0, byteToSend.Length)   ' Sends data
    '                objWriter.Close()
    '            End If

    '            '============================================= Get response =================================================

    '            objHTTPWebResponse = objHttpWebRequest.GetResponse()

    '            'Modified 24 Aug 2015, added checking of StatusCode Created 201, for Bitnet
    '            If (objHttpWebRequest.HaveResponse And (objHTTPWebResponse.StatusCode = HttpStatusCode.OK Or objHTTPWebResponse.StatusCode = HttpStatusCode.Created)) Then

    '                objStreamReader = New StreamReader(objHTTPWebResponse.GetResponseStream)

    '                '================ Implements Timeout for getting HTTP reply from host <START> ====================
    '                Dim objRespReader As RespReader
    '                Dim oThreadStart As ThreadStart
    '                Dim oThread As Thread
    '                Dim iElapsedTime As Integer

    '                objRespReader = New RespReader
    '                objRespReader.SetLogger(objLoggerII)
    '                objRespReader.SetReader(objStreamReader)

    '                oThreadStart = New ThreadStart(AddressOf objRespReader.WaitForResp)
    '                oThread = New Thread(oThreadStart)
    '                oThread.Start()

    '                'iStartTickCount = System.Environment.TickCount()
    '                iElapsedTime = System.Environment.TickCount() - iStartTickCount

    '                While ((iElapsedTime < iTimeout) And Not objRespReader.IsCompleted())
    '                    System.Threading.Thread.Sleep(50)
    '                    iElapsedTime = System.Environment.TickCount() - iStartTickCount
    '                End While

    '                If (iElapsedTime >= iTimeout) Then
    '                    ' Timeout
    '                    iRet = Common.ERROR_CODE.E_COM_TIMEOUT
    '                Else
    '                    'Completed AND No Timeout. Could be got reply or exception
    '                    If (True = objRespReader.IsSucceeded()) Then
    '                        'Received reply within timeout period
    '                        sz_HTTPResponse = objRespReader.GetRespStream()

    '                        If ("" = sz_HTTPResponse) Then  'Added  4 jun 2014. Firefly FF
    '                            iRet = Common.ERROR_CODE.E_COM_TIMEOUT
    '                            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp Response received with empty string. Set to Timeout.")
    '                        End If
    '                    Else
    '                        'Exception within timeout period
    '                        iRet = Common.ERROR_CODE.E_COM_TIMEOUT
    '                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp exception within timeout period, iRet(" + iRet.ToString() + ")")
    '                    End If
    '                End If

    '                oThread.Abort()
    '                oThreadStart = Nothing
    '                oThread = Nothing
    '                'objRespReader.Dispose()
    '                objRespReader = Nothing

    '                'Releases the resources of the response stream
    '                'NOTE: Failure to close the stream will cause application to run out of connections.
    '                objStreamReader.Close()
    '                If Not objStreamReader Is Nothing Then objStreamReader = Nothing 'Added on 1 Mar 2009
    '                '================== Implements Timeout for getting HTTP reply from host <END> =====================
    '            Else
    '                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "ERROR: Response was not received.")
    '            End If

    '            'Releases the resources of the response
    '            objHTTPWebResponse.Close()

    '            If (-1 = iRet) Then 'Added  4 Jun 2014. Firefly FF
    '                iRet = 0
    '            End If

    '        Catch webEx As WebException
    '            Try
    '                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException: " + webEx.ToString)

    '                iRet = Common.ERROR_CODE.E_COM_URL_UNREACHABLE

    '                If webEx.Status = WebExceptionStatus.Timeout Then
    '                    iRet = Common.ERROR_CODE.E_COM_TIMEOUT
    '                    objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException: Timeout")
    '                End If

    '                If webEx.Status = WebExceptionStatus.ProtocolError Then
    '                    objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Response Status Code :" + CStr(CType(webEx.Response, HttpWebResponse).StatusCode))

    '                    'Re: HttpStatusCode Enumeration
    '                    iResCode = CInt(CType(webEx.Response, HttpWebResponse).StatusCode)
    '                    If iResCode = 400 Then
    '                        iRet = Common.ERROR_CODE.E_COM_BAD_REQUEST
    '                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Bad Request could not be understood by the server.")
    '                    ElseIf iResCode = 401 Then
    '                        iRet = Common.ERROR_CODE.E_COM_UNAUTHORIZED_URL
    '                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Unauthorized, the requested resource requires authentication.")
    '                    ElseIf iResCode = 403 Then
    '                        iRet = Common.ERROR_CODE.E_COM_FORBIDDEN_URL
    '                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Forbidden, the server refuses to fulfill the request.")
    '                    ElseIf iResCode = 404 Then
    '                        iRet = Common.ERROR_CODE.E_COM_URL_UNREACHABLE
    '                        objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Not Found, the requested resource does not exist on the server.")
    '                    End If
    '                End If
    '            Catch exp As Exception
    '                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Exception: " + exp.ToString)
    '            End Try

    '        Catch ioEx As IOException
    '            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "IO Exception: " + ioEx.ToString)
    '            iRet = Common.ERROR_CODE.E_COM_FILE_ERROR

    '        Catch ex As Exception
    '            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Exception: " + ex.ToString)
    '            iRet = Common.ERROR_CODE.E_COMMON_UNKNOWN_ERROR

    '        Finally
    '            'Commented  15 Aug 2013
    '            'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "iRet: " + CStr(iRet))
    '        End Try

    '        Return iRet
    '    End Function
End Class