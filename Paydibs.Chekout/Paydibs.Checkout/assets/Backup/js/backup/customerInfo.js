﻿var customerInfoCollector = new function () {
    var collection = {
        CustName: {
            value:"",
            isValid: false,
            DOM: new Object(),
            Dummy: new Object(),
            field_grp: new Object(),
            err: new Object(),
            validator: 'ValidateName',
            validatorResp: ''
        },
        CustEmail: {
            value:"",
            isValid: false,
            DOM: new Object(),
            Dummy: new Object(),
            field_grp: new Object(),
            err: new Object(),
            validator: 'ValidateEmail',
            validatorResp: ''
        },
        CustPhone: {
            value:"",
            isValid: false,
            DOM: new Object(),
            Dummy: new Object(),
            field_grp: new Object(),
            err: new Object(),
            validator: 'ValidatePhone',
            validatorResp: ''
        }
    };

    var container = new Object();

    this.init = function (container_id) {
        if (container_id == '') {
            alert('[customerInfoCollector] Please provide container id.');
        }
        else if ($("#" + container_id).length == 0) {
            alert('[customerInfoCollector] Container not found.');
        }
        else {
            container = $("#" + container_id);
            $.each(collection, function (index, obj) {
                obj.DOM = $('input[name="' + index + '"]');
                obj.Dummy = $('input[name="ent_' + index + '"]');
                obj.Dummy.blur(function () {
                    if (obj.Dummy.val() != "") {
                        eval('obj.validatorResp = ' + obj.validator + '();');
                        if ($.type(obj.validatorResp) == "string") {
                            obj.isValid = false;
                            showError(index, obj.validatorResp,true);
                        }
                        else if (obj.validatorResp == true) {
                            obj.isValid = true;
                            hideError(index);
                        }
                        obj.DOM.val(obj.Dummy.val());
                    }
                    else {
                        obj.isValid = false;
                        obj.DOM.val('-');
                    }
                    
                });
                if (obj.Dummy.val() == "-") {
                    obj.Dummy.val('');
                }
                obj.field_grp = $('#fld_' + index);
                obj.err = obj.field_grp.find('.error_msg');
                obj.value = obj.DOM.val();
                hideError(index);
                eval('obj.validatorResp = ' + obj.validator + '();');
                if ($.type(obj.validatorResp) == "string") {
                    obj.isValid = false;
                    obj.field_grp.show();
                }
                else if (obj.validatorResp == true) {
                    obj.isValid = true;
                    obj.field_grp.hide();
                }
            });
            if (IsCustomerInfoRequired()) {
                container.slideDown();
            }
        }
    };

    var ReverseObject = function (Obj) {
        var TempArr = [];
        var NewObj = new Object();
        for (var Key in Obj) {
            TempArr.push(Key);
        }
        for (var i = TempArr.length - 1; i >= 0; i--) {
            NewObj[TempArr[i]] = Obj[TempArr[i]];
        }
        return NewObj;
    };

    var ValidateEmail = function () {
        var email = collection.CustEmail.Dummy.val();
        collection.CustEmail.isValid = false;
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(email)) {
            if (email.length > 60) {
                return "Max length of email must be 60 characters.";
            }
            else {
                collection.CustEmail.isValid = true;
                return true;
            }
        }
        return "You have entered an invalid email address.";
    };

    var ValidatePhone = function () {
        var phone = collection.CustPhone.Dummy.val();
        collection.CustPhone.isValid = false;        
        if (/^\+?([0-9]+)$/.test(phone)) {
            if (phone.length > 25) {
                return "Max length of Phone must be 25 characters.";
            }
            else {
                collection.CustPhone.isValid = true;
                return true;
            }
        }
        return "You have entered an invalid Phone Number.";
    };

    var ValidateName = function () {
        var Name = collection.CustName.Dummy.val();
        collection.CustName.isValid = false;
        if (Name.length > 50) {
            return "Max length of Name must be 50 characters.";
        }
        else if (Name.length == 0) {
            return "Name is Required";
        }
        else {
            collection.CustName.isValid = true;
            return true;
        }
    };

    this.beforeSubmitVerify = function () {
        $.each(collection, function (index, obj) {
            eval('var validatorResp = ' + obj.validator + '();');
            obj.validatorResp = validatorResp;
        });
        $.each(ReverseObject(collection), function (index, obj) {
            if (obj.Dummy.val()=='') {
                showError(index, 'This is required field');
            }
            else if ($.type(obj.validatorResp) == "string") {
                showError(index, obj.validatorResp);
            }
            else {
                hideError(index);
            }
        });
        if (IsCustomerInfoRequired()) {
            return false;
        }
        else {
            return true;
        }
    };

    this.getCCollection = function () {
        return collection;
    };

    var showError = function (fld, msg, noFocus) {
        collection[fld].err.text(msg);
        collection[fld].err.fadeIn();
        collection[fld].Dummy.css('border-color', 'red');
        if (noFocus != true) {
            collection[fld].Dummy.focus();
        }
    };

    var hideError = function (fld) {
        collection[fld].err.fadeOut();
        collection[fld].err.text('');
        collection[fld].Dummy.css('border-color', '#ccc');
    };

    var IsCustomerInfoRequired = function () {
        var bReturn = false;
        $.each(collection, function (index, obj) {
            if (obj.isValid == false) {
                bReturn = true;
            }
        });
        return bReturn;
    };
};