function populateOtherPaymentByCountryOB(initBankListOB, initBankListOTC, elementPanelCountry, forexData, $cc, $price) {
    for (var key in initBankListOB) {
        var templateCountryDIV = "<div class=\"panel panel-default\" style=\"display:block;\">"
                                + "<div class=\"panel-heading price-container\">"
                                + "    <h4 class=\"panel-title\">"
                                + "       <a data-toggle=\"collapse\" [SETCLASS1] data-parent=\"#payment-by-country\" href=\"#c" + key + "\" style=\"text-decoration:none;\">"
                                + "	        <img src=\"assets3/img/payments/flag-" + key + ".png\" width=\"40\"> " + key + " (<label id=\"ccurr" + key + "\">[CURCODE]</label>&nbsp;<label id=\"amt" + key + "\">[AMT]</label>)"
                                + "       </a>"
                                + "    </h4>"
                                + "<label id=\"forex" + key + "\" style=\"display:none;\">[isforexvalue]</label>"
                                + "</div>"
                                + "<div id=\"c" + key + "\" [SETCLASS2]>"
                                + "    <div class=\"panel-body\">"
                                + "       <div class=\"accordion-v2 plus-toggle\">"
                                + "	        <div class=\"panel-group\" id=\"" + key + "\">"
                                + "            <!-- Online Banking -->"
                                + "		       <div class=\"panel panel-default\">"
                                + "			      <div class=\"panel-heading price-container\">"
                                + "				      <h4 class=\"panel-title\">"
                                + "					      <a data-toggle=\"collapse\" data-parent=\"#" + key + "\" href=\"#" + key + "-ob\" style=\"text-decoration:none;\">"
                                + "						     Online Banking"
                                + "					      </a>"
                                + "				      </h4>"
                                + "			      </div>"
                                + "			      <div id=\"" + key + "-ob\" class=\"panel-collapse collapse in\">"
                                + "			          <div class=\"panel-body\" id=\"divPanelBodyOB" + key + "\">"
                                + "                     [oblist]"
                                + "				      </div>"
                                + "			      </div>"
                                + "		       </div>"
                                + "		       <!-- End Online Banking -->"
                                + "            <!-- Over the Counter -->"
                                + " 		   <div class=\"panel panel-default\" style=\"display:[dispstyle]\">"
                                + "			      <div class=\"panel-heading price-container\">"
                                + "				      <h4 class=\"panel-title\">"
                                + "					      <a data-toggle=\"collapse\" class=\"collapsed\" data-parent=\"#" + key + "\" href=\"#" + key + "-otc\" style=\"text-decoration:none;\">"
                                + "						     Over the Counter"
                                + "					      </a>"
                                + "				      </h4>"
                                + "			      </div>"
                                + "			      <div id=\"" + key + "-otc\" class=\"panel-collapse collapse\">"
                                + "				      <div class=\"panel-body\" id=\"divPanelBodyOTC" + key + "\">"
                                + "					    [otclist]"
                                + "				      </div>"
                                + "			      </div>"
                                + "		       </div>"
                                + "		       <!-- End Over the Counter -->"
                                + "            </div>"
                                + "        </div>"
                                + "    </div>"
                                + "</div>"
                                + "</div>";


        var banksOB = JSON.parse(JSON.stringify(initBankListOB[key]));
        var bankspropOB = JSON.parse(JSON.stringify(banksOB["banks"]));
        var strObList = "";
        for (i = 0; i < bankspropOB.length; i++) {
            if (bankspropOB[i]["imgName"] == "meps-fpx.png") {
                strObList = strObList + "<a href=\"#\" OnClick=\"VerifyDataOthers('DD','" + bankspropOB[i]["issuingBank"] + "','" + key + "')\"><img id=\"imgmepsfpx\" src=\"assets3/img/" + bankspropOB[i]["imgName"] + "\" alt=\"" + bankspropOB[i]["dispName"] + "\"/></a>";
            }
            else {
                strObList = strObList + "<a href=\"#\" OnClick=\"VerifyDataOthers('DD','" + bankspropOB[i]["issuingBank"] + "','" + key + "')\"><img src=\"assets3/img/" + bankspropOB[i]["imgName"] + "\" width=\"80\" alt=\"" + bankspropOB[i]["dispName"] + "\"/></a>";
            }
        }
        templateCountryDIV = templateCountryDIV.replace("[oblist]", strObList)

        if (initBankListOTC != "") {
            if (initBankListOTC.hasOwnProperty(key)) {
                var banksOTC = JSON.parse(JSON.stringify(initBankListOTC[key]));
                var bankspropOTC = JSON.parse(JSON.stringify(banksOTC["banks"]));
                var strOTCList = "";
                for (i = 0; i < bankspropOTC.length; i++) {
                    strOTCList = strOTCList + "<a href=\"#\" OnClick=\"VerifyDataOthers('OTC','" + bankspropOTC[i]["issuingBank"] + "','" + key + "')\"><img src=\"assets3/img/" + bankspropOTC[i]["imgName"] + "\" width=\"80\" alt=\"" + bankspropOTC[i]["dispName"] + "\"></a>";
                }
                templateCountryDIV = templateCountryDIV.replace("[dispstyle]", "block");
                templateCountryDIV = templateCountryDIV.replace("[otclist]", strOTCList)
            }
            else {
                templateCountryDIV = templateCountryDIV.replace("[dispstyle]", "none");
            }
        }
        else {
            templateCountryDIV = templateCountryDIV.replace("[dispstyle]", "none");
        }

        
        var newCurr = "";
        var newPrice = "";
        var isForexData = false;

        var basecur = $cc.text();
        var baseamt = $price.text();

        // checking whether Forex or not               
        if (forex.hasOwnProperty(key)) {
            isForexData = true;
        }
        //
        var temp = templateCountryDIV;

        if (isForexData) {
            if (forex.hasOwnProperty(key)) {
                newCurr = getCurr(key, forexData);				
				if (basecur == newCurr) {
					newPrice = formatCurrency(getPrice(key, baseamt, forexData));
				}
				else{
					newPrice = formatCurrency(getCPrice(key, baseamt, forexData));
				}
                temp = temp.replace("[CURCODE]", newCurr);
                temp = temp.replace("[AMT]", newPrice);
                temp = temp.replace("[isforexvalue]", 1);
				
                if (newCurr == basecur) {
                    temp = temp.replace("[SETCLASS1]", "aria-expanded=\"true\"");
                    temp = temp.replace("[SETCLASS2]", "class=\"panel-collapse collapse in\" aria-expanded=\"true\"");
                    elementPanelCountry.innerHTML = temp + elementPanelCountry.innerHTML;
                }
                else {
                    temp = temp.replace("[SETCLASS1]", "class=\"collapsed\" aria-expanded=\"false\"");
                    temp = temp.replace("[SETCLASS2]", "class=\"panel-collapse collapse\" aria-expanded=\"false\"");
                    elementPanelCountry.innerHTML = elementPanelCountry.innerHTML + temp;
                }
            }
            
        } else {
            newCurr = basecur;
            newPrice = baseamt;

            temp = temp.replace("[CURCODE]", newCurr);
            temp = temp.replace("[AMT]", newPrice);
            temp = temp.replace("[isforexvalue]", 0);

            if (newCurr == basecur) {
                temp = temp.replace("[SETCLASS1]", "aria-expanded=\"true\"");
                temp = temp.replace("[SETCLASS2]", "class=\"panel-collapse collapse in\" aria-expanded=\"true\"");
                elementPanelCountry.innerHTML = temp + elementPanelCountry.innerHTML;
            }
            else {
                temp = temp.replace("[SETCLASS1]", "class=\"collapsed\" aria-expanded=\"false\"");
                temp = temp.replace("[SETCLASS2]", "class=\"panel-collapse collapse\" aria-expanded=\"false\"");
                elementPanelCountry.innerHTML = elementPanelCountry.innerHTML + temp;
            }
        }
    }
}
function populateOtherPaymentByCountryOTC(initBankListOTC, elementPanelCountry, forexData, $cc, $price) {
    for (var key in initBankListOTC) {
        var templateCountryDIV = "<div class=\"panel panel-default\" style=\"display:block;\">"
                                + "<div class=\"panel-heading price-container\">"
                                + "    <h4 class=\"panel-title\">"
                                + "       <a data-toggle=\"collapse\" [SETCLASS1] data-parent=\"#payment-by-country\" href=\"#c" + key + "\">"
                                + "	        <img src=\"assets3/img/payments/flag-" + key + ".png\" width=\"40\"> " + key + " (<label id=\"ccurr" + key + "\">[CURCODE]</label>&nbsp;<label id=\"amt" + key + "\">[AMT]</label>)"
                                + "       </a>"
                                + "    </h4>"
                                + "<label id=\"forex" + key + "\" style=\"display:none;\">[isforexvalue]</label>"
                                + "</div>"
                                + "<div id=\"c" + key + "\" [SETCLASS2]>"
                                + "    <div class=\"panel-body\">"
                                + "       <div class=\"accordion-v2 plus-toggle\">"
                                + "	        <div class=\"panel-group\" id=\"" + key + "\">"
                                + "            <!-- Over the Counter -->"
                                + " 		   <div class=\"panel panel-default\" style=\"display:[dispstyle]\">"
                                + "			      <div class=\"panel-heading price-container\">"
                                + "				      <h4 class=\"panel-title\">"
                                + "					      <a data-toggle=\"collapse\" class=\"collapsed\" data-parent=\"#" + key + "\" href=\"#" + key + "-otc\">"
                                + "						     Over the Counter"
                                + "					      </a>"
                                + "				      </h4>"
                                + "			      </div>"
                                + "			      <div id=\"" + key + "-otc\" class=\"panel-collapse collapse\">"
                                + "				      <div class=\"panel-body\" id=\"divPanelBodyOTC" + key + "\">"
                                + "					    [otclist]"
                                + "				      </div>"
                                + "			      </div>"
                                + "		       </div>"
                                + "		       <!-- End Over the Counter -->"
                                + "            </div>"
                                + "        </div>"
                                + "    </div>"
                                + "</div>"
                                + "</div>";

        var banksOTC = JSON.parse(JSON.stringify(initBankListOTC[key]));
        var bankspropOTC = JSON.parse(JSON.stringify(banksOTC["banks"]));
        var strOTCList = "";
        for (i = 0; i < bankspropOTC.length; i++) {
            strOTCList = strOTCList + "<a href=\"#\" OnClick=\"VerifyDataOthers('OTC','" + bankspropOTC[i]["issuingBank"] + "','" + key + "')\"><img src=\"assets3/img/" + bankspropOTC[i]["imgName"] + "\" width=\"80\" alt=\"" + bankspropOTC[i]["dispName"] + "\"></a>";
        }
        templateCountryDIV = templateCountryDIV.replace("[dispstyle]", "block");
        templateCountryDIV = templateCountryDIV.replace("[otclist]", strOTCList);

        var newCurr = "";
        var newPrice = "";
        var isForexData = false;

        var basecur = $cc.text();
        var baseamt = $price.text();

        // checking whether Forex or not               
        if (forex.hasOwnProperty(key)) {
            isForexData = true;
        }
        //

        var temp = templateCountryDIV;

        if (isForexData) {
            if (forex.hasOwnProperty(key)) {
				
                newCurr = getCurr(key, forexData);
                newPrice = formatCurrency(getPrice(key, baseamt, forexData));

                temp = temp.replace("[CURCODE]", newCurr);
                temp = temp.replace("[AMT]", newPrice);
                temp = temp.replace("[isforexvalue]", 1);

                if (newCurr == basecur) {
                    temp = temp.replace("[SETCLASS1]", "aria-expanded=\"true\"");
                    temp = temp.replace("[SETCLASS2]", "class=\"panel-collapse collapse in\" aria-expanded=\"true\"");
                    elementPanelCountry.innerHTML = temp + elementPanelCountry.innerHTML;
                }
                else {
                    temp = temp.replace("[SETCLASS1]", "class=\"collapsed\" aria-expanded=\"false\"");
                    temp = temp.replace("[SETCLASS2]", "class=\"panel-collapse collapse\" aria-expanded=\"false\"");
                    elementPanelCountry.innerHTML = elementPanelCountry.innerHTML + temp;
                }
            }

        } else {
            newCurr = basecur;
            newPrice = baseamt;

            temp = temp.replace("[CURCODE]", newCurr);
            temp = temp.replace("[AMT]", newPrice);
            temp = temp.replace("[isforexvalue]", 0);

            if (newCurr == basecur) {
                temp = temp.replace("[SETCLASS1]", "aria-expanded=\"true\"");
                temp = temp.replace("[SETCLASS2]", "class=\"panel-collapse collapse in\" aria-expanded=\"true\"");
                elementPanelCountry.innerHTML = temp + elementPanelCountry.innerHTML;
            }
            else {
                temp = temp.replace("[SETCLASS1]", "class=\"collapsed\" aria-expanded=\"false\"");
                temp = temp.replace("[SETCLASS2]", "class=\"panel-collapse collapse\" aria-expanded=\"false\"");
                elementPanelCountry.innerHTML = elementPanelCountry.innerHTML + temp;
            }
        } 
    }
}

function populateOtherPaymentByWallet(initBankListWA, elementPanelWA)
{
    for (var key in initBankListWA)
    {
        var templateWalletDIV = "<div class=\"panel panel-default\"  id=\"divWA\" >"
								+ "<div class=\"panel-heading price-container\">"
								+ "<h4 class=\"panel-title\">"
								+ "<a data-toggle=\"collapse\" class=\"collapsed\" data-parent=\"#payment-wallet\" href=\"#wallet\" style=\"text-decoration:none;\">"
								+ "eWallet"
								+ "</a>"
								+ "</h4>"
								+ "</div>"
								+ "<div id=\"wallet\" class=\"panel-collapse collapse\">"
								+ "<div class=\"panel-body\">"
                                + "[WAList]"
								+ "</div>"
								+ "</div>"
							    + "</div>";

        var banksWA = JSON.parse(JSON.stringify(initBankListWA[key]));
        var bankspropWA = JSON.parse(JSON.stringify(banksWA["banks"]));
        var strWAList = "";
        for (i = 0; i < bankspropWA.length; i++) {
            strWAList = strWAList + "<a href=\"#\" OnClick=\"VerifyDataOthersWA('" + bankspropOB[i]["issuingBank"] + "')\"><img src=\"assets3/img/" + bankspropOB[i]["imgName"] + "\" width=\"80\" alt=\"" + bankspropOB[i]["dispName"] + "\"/></a>";
        }
        templateCountryDIV = templateCountryDIV.replace("[WAList]", strWAList)
    }
}

function getCurr(cval, forex) { 
    return forex[cval].curr;
}
function getPrice(cval, baseamt, forex) {
	return toFixed(baseamt * forex[cval].rate, 2).toFixed(2);
}
function getCPrice(cval, baseamt, forex) {
	if (forex[cval].curr.toUpperCase()==='IDR' || forex[cval].curr.toUpperCase()==='JPY' || forex[cval].curr.toUpperCase()==='KRW' || forex[cval].curr.toUpperCase()==='VND') {
		return toCeil(baseamt * forex[cval].rate, 2).toFixed(2);}
	else {
		return toFixed(baseamt * forex[cval].rate, 2).toFixed(2);}
}
function toFixed(number, precision) {
    var multiplier = Math.pow(10, precision + 1),
    wholeNumber = Math.floor(number * multiplier);
    return Math.round(wholeNumber / 10) * 10 / multiplier;
}
function toCeil(number, precision) { 
    var multiplier = Math.pow(10, precision + 1),
    wholeNumber = Math.floor(number * multiplier),
    finalNumber = Math.round(wholeNumber / 10) * 10 / multiplier;
	return Math.ceil(finalNumber);
}

function formatCurrency(number) { 
    var n = number.split('').reverse().join("");
    var n2 = n.replace(/\d\d\d(?!$)/g, "$&,");
    return n2.split('').reverse().join('');
}