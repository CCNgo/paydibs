function populateOtherPaymentByCountryOB(initBankListOB, initBankListOTC, elementPanelCountry, forexData, $cc, $price) {
    for (var key in initBankListOB) {
        var templateCountryDIV = "<div class=\"panel panel-default\" style=\"display:block;\">"
                                + "<div class=\"panel-heading\">"
                                + "    <h4 class=\"panel-title\">"
                                + "       <a data-toggle=\"collapse\" class=\"collapsed\" data-parent=\"#payment-by-country\" href=\"#c" + key + "\" style=\"text-decoration:none;\">"
                                + "	        <img src=\"assets/X30/img/payments/flag-" + key + ".png\" width=\"40\"> " + key + " ([CURCODE] [AMT])"
                                + "       </a>"
                                + "    </h4>"
                                + "</div>"
                                + "<div id=\"c" + key + "\" class=\"panel-collapse collapse\">"
                                + "    <div class=\"panel-body\">"
                                + "       <div class=\"accordion-v2 plus-toggle\">"
                                + "	        <div class=\"panel-group\" id=\"" + key + "\"></div>"
                                + "            <!-- Online Banking -->"
                                + "		       <div class=\"panel panel-default\">"
                                + "			      <div class=\"panel-heading\">"
                                + "				      <h4 class=\"panel-title\">"
                                + "					      <a data-toggle=\"collapse\" data-parent=\"#" + key + "\" href=\"#" + key + "-ob\" style=\"text-decoration:none;\">"
                                + "						     Online Banking"
                                + "					      </a>"
                                + "				      </h4>"
                                + "			      </div>"
                                + "			      <div id=\"" + key + "-ob\" class=\"panel-collapse collapse in\">"
                                + "			          <div class=\"panel-body\" id=\"divPanelBodyOB" + key + "\">"
                                + "                     [oblist]"
                                + "				      </div>"
                                + "			      </div>"
                                + "		       </div>"
                                + "		       <!-- End Online Banking -->"
                                + "            <!-- Over the Counter -->"
                                + " 		   <div class=\"panel panel-default\" style=\"display:[dispstyle]\">"
                                + "			      <div class=\"panel-heading\">"
                                + "				      <h4 class=\"panel-title\">"
                                + "					      <a data-toggle=\"collapse\" class=\"collapsed\" data-parent=\"#" + key + "\" href=\"#" + key + "-otc\" style=\"text-decoration:none;\">"
                                + "						     Over the Counter"
                                + "					      </a>"
                                + "				      </h4>"
                                + "			      </div>"
                                + "			      <div id=\"" + key + "-otc\" class=\"panel-collapse collapse\">"
                                + "				      <div class=\"panel-body\" id=\"divPanelBodyOTC" + key + "\">"
                                + "					    [otclist]"
                                + "				      </div>"
                                + "			      </div>"
                                + "		       </div>"
                                + "		       <!-- End Over the Counter -->"
                                + "        </div>"
                                + "    </div>"
                                + "</div>"
                                + "</div>";


        var banksOB = JSON.parse(JSON.stringify(initBankListOB[key]));
        var bankspropOB = JSON.parse(JSON.stringify(banksOB["banks"]));
        var strObList = "";
        for (i = 0; i < bankspropOB.length; i++) {
            strObList = strObList + "<a href=\"#\" OnClick=\"VerifyDataOthers('DD','" + bankspropOB[i]["issuingBank"] + "','[CURCODE]','[AMT]')\"><img src=\"assets2/img/" + bankspropOB[i]["imgName"] + "\" width=\"80\" alt=\"" + bankspropOB[i]["dispName"] + "\"/></a>";
        }
        templateCountryDIV = templateCountryDIV.replace("[oblist]", strObList)

        if (initBankListOTC != "") {
            if (initBankListOTC.hasOwnProperty(key)) {
                var banksOTC = JSON.parse(JSON.stringify(initBankListOTC[key]));
                var bankspropOTC = JSON.parse(JSON.stringify(banksOTC["banks"]));
                var strOTCList = "";
                for (i = 0; i < bankspropOTC.length; i++) {
                    strOTCList = strOTCList + "<a href=\"#\" OnClick=\"VerifyDataOthers('OTC','" + bankspropOTC[i]["issuingBank"] + "','[CURCODE]','[AMT]')\"><img src=\"assets2/img/" + bankspropOTC[i]["imgName"] + "\" width=\"80\" alt=\"" + bankspropOTC[i]["dispName"] + "\"></a>";
                }
                templateCountryDIV = templateCountryDIV.replace("[dispstyle]", "block");
                templateCountryDIV = templateCountryDIV.replace("[otclist]", strOTCList)
            }
            else {
                templateCountryDIV = templateCountryDIV.replace("[dispstyle]", "none");
            }
        }

        //elementPanelCountry.innerHTML = elementPanelCountry.innerHTML + templateCountryDIV;

        
        var newCurr = "";
        var newPrice = "";
        var isForexData = false;

        var basecur = $cc.text();
        var baseamt = $price.text();

        // checking whether Forex or not               
        if (forex.hasOwnProperty(key)) {
            isForexData = true;
        }
        //

        var temp = templateCountryDIV;

        if (isForexData) {
            if (forex.hasOwnProperty(key)) {
                newCurr = getCurr(key, forexData);
                newPrice = formatCurrency(getPrice(key, baseamt, forexData));

                temp = temp.replace("[CURCODE]", newCurr);
                temp = temp.replace("[AMT]", newPrice);

                if (newCurr == basecur) {
                    elementPanelCountry.innerHTML = temp + elementPanelCountry.innerHTML;
                }
                else {
                    elementPanelCountry.innerHTML = elementPanelCountry.innerHTML + temp;
                }

                //elementPanelCountry.innerHTML = elementPanelCountry.innerHTML.replace("[CURCODE]", newCurr);
                //elementPanelCountry.innerHTML = elementPanelCountry.innerHTML.replace("[AMT]", newPrice);
            }
            
        } else {
            newCurr = basecur;
            newPrice = baseamt;
            elementPanelCountry.innerHTML = elementPanelCountry.innerHTML.replace("[CURCODE]", newCurr);
            elementPanelCountry.innerHTML = elementPanelCountry.innerHTML.replace("[AMT]", newPrice);
        }
    }
}
function populateOtherPaymentByCountryOTC(initBankListOTC, elementPanelCountry, forexData, $cc, $price) {
    for (var key in initBankListOTC) {
        var templateCountryDIV = "<div class=\"panel panel-default\" style=\"display:block;\">"
                                + "<div class=\"panel-heading\">"
                                + "    <h4 class=\"panel-title\">"
                                + "       <a data-toggle=\"collapse\" class=\"collapsed\" data-parent=\"#payment-by-country\" href=\"#c" + key + "\">"
                                + "	        <img src=\"assets/X30/img/payments/flag-" + key + ".png\" width=\"40\"> " + key + " ([CURCODE] [AMT])"
                                + "       </a>"
                                + "    </h4>"
                                + "</div>"
                                + "<div id=\"c" + key + "\" class=\"panel-collapse collapse\">"
                                + "    <div class=\"panel-body\">"
                                + "       <div class=\"accordion-v2 plus-toggle\">"
                                + "	        <div class=\"panel-group\" id=\"" + key + "\"></div>"
                                + "            <!-- Over the Counter -->"
                                + " 		   <div class=\"panel panel-default\" style=\"display:[dispstyle]\">"
                                + "			      <div class=\"panel-heading\">"
                                + "				      <h4 class=\"panel-title\">"
                                + "					      <a data-toggle=\"collapse\" class=\"collapsed\" data-parent=\"#" + key + "\" href=\"#" + key + "-otc\">"
                                + "						     Over the Counter"
                                + "					      </a>"
                                + "				      </h4>"
                                + "			      </div>"
                                + "			      <div id=\"" + key + "-otc\" class=\"panel-collapse collapse\">"
                                + "				      <div class=\"panel-body\" id=\"divPanelBodyOTC" + key + "\">"
                                + "					    [otclist]"
                                + "				      </div>"
                                + "			      </div>"
                                + "		       </div>"
                                + "		       <!-- End Over the Counter -->"
                                + "        </div>"
                                + "    </div>"
                                + "</div>"
                                + "</div>";

        var banksOTC = JSON.parse(JSON.stringify(initBankListOTC[key]));
        var bankspropOTC = JSON.parse(JSON.stringify(banksOTC["banks"]));
        var strOTCList = "";
        for (i = 0; i < bankspropOTC.length; i++) {
            strOTCList = strOTCList + "<a href=\"#\" OnClick=\"VerifyDataOthers('OTC','" + bankspropOTC[i]["issuingBank"] + "','[CURCODE]','[AMT]')\"><img src=\"assets2/img/" + bankspropOTC[i]["imgName"] + "\" width=\"80\" alt=\"" + bankspropOTC[i]["dispName"] + "\"></a>";
        }
        templateCountryDIV = templateCountryDIV.replace("[dispstyle]", "block");
        templateCountryDIV = templateCountryDIV.replace("[otclist]", strOTCList);

        //elementPanelCountry.innerHTML = elementPanelCountry.innerHTML + templateCountryDIV;

        var newCurr = "";
        var newPrice = "";
        var isForexData = false;

        var basecur = $cc.text();
        var baseamt = $price.text();

        // checking whether Forex or not               
        if (forex.hasOwnProperty(key)) {
            isForexData = true;
        }
        //

        var temp = templateCountryDIV;

        if (isForexData) {
            if (forex.hasOwnProperty(key)) {
                newCurr = getCurr(key, forexData);
                newPrice = formatCurrency(getPrice(key, baseamt, forexData));

                temp = temp.replace("[CURCODE]", newCurr);
                temp = temp.replace("[AMT]", newPrice);

                if (newCurr == basecur) {
                    elementPanelCountry.innerHTML = temp + elementPanelCountry.innerHTML;
                }
                else {
                    elementPanelCountry.innerHTML = elementPanelCountry.innerHTML + temp;
                }

                //elementPanelCountry.innerHTML = elementPanelCountry.innerHTML.replace("[CURCODE]", newCurr);
                //elementPanelCountry.innerHTML = elementPanelCountry.innerHTML.replace("[AMT]", newPrice);
            }

        } else {
            newCurr = basecur;
            newPrice = baseamt;
            elementPanelCountry.innerHTML = elementPanelCountry.innerHTML.replace("[CURCODE]", newCurr);
            elementPanelCountry.innerHTML = elementPanelCountry.innerHTML.replace("[AMT]", newPrice);
        }
    }
}

function getCurr(cval, forex) {
    return forex[cval].curr;
}
function getPrice(cval, baseamt, forex) {
    return toFixed(baseamt * forex[cval].rate, 2).toFixed(2);
}
function toFixed(number, precision) {
    var multiplier = Math.pow(10, precision + 1),
    wholeNumber = Math.floor(number * multiplier);
    return Math.round(wholeNumber / 10) * 10 / multiplier;
}

function formatCurrency(number) {
    var n = number.split('').reverse().join("");
    var n2 = n.replace(/\d\d\d(?!$)/g, "$&,");
    return n2.split('').reverse().join('');
}