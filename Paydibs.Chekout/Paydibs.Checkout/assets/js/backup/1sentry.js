/*  METHOD B2
    v1.01
    To use, put: <script src="[Scriptpath]/B2.js"></script> on PG page, and define client_id
*/
function a(merchant_id){
    var now = new Date()

    qs = window.location.search.substr(1).split("&")
    for (var i=0;i<qs.length;i++){
        var p=qs[i].split('=', 2);
            if (p.length == 1){
                continue;
            } else if (p[0]== "merchant_id"){
                merchant_id = decodeURIComponent(p[1].replace(/\+/g, " "))
                break;
            }
    }

    var info = {"a": document.referrer,
            "b": location.origin,
            "c": navigator.userAgent,
            "d": now.getTime(),
            "e": -now.getTimezoneOffset()/60,
            "f": now.toString(),
            "g": merchant_id,
            "h": 5723866542374912,
            }

    var sinfo = [];
    for(var p in info){
        if (info.hasOwnProperty(p)) {
            sinfo.push(encodeURIComponent(p) + "=" + encodeURIComponent(info[p]));
        }
    }
    sinfo = sinfo.join("&");

    var xhr = new XMLHttpRequest();
    xhr.open("post", "https://beta-ama-onesentry.appspot.com/check", true);
    xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	// console.log(sinfo); //debug	
    xhr.send(sinfo);
}