function OB_OTC_merger(ListOB, ListOTC, ListWA) {
    var output = new Object();

    /*** Countries initialization ***/
    $.each(ListOB, function (index, val) {
        output[index] = new Object();
        output[index]['OB'] = val.banks;
    });
    $.each(ListOTC, function (index, val) {
        if (!(index in output)) { /* Don't Add index if already exist */
            output[index] = new Object();
        }
        output[index]['OTC'] = val.banks;
    });
	/*Added by OoiMei, 8 Jun 2017. Wallet */
	$.each(ListWA, function (index, val) {
        if (!(index in output)) { /* Don't Add index if already exist */
            output[index] = new Object();
        }
        output[index]['WA'] = val.banks;
    });
    /********************************/
    return output;
}
//START-----Modify for FPX B2B
function fpx_splitter(ListOB) {
    var output = new Object();

    /*** Countries initialization ***/
    $.each(ListOB, function (country, val) {
        output[country] = new Object();
		output[country]['B2B'] = new Object();
		output[country]['B2C'] = new Object();
		$.each(val.banks, function (bidx, bank) {
			if(bank.issuingBank.indexOf("FPXD")>=0){
				if(bank.issuingBank.indexOf("FPXDB2B")>=0){
					output[country]['B2B'][bidx] = bank;
				}
				else{
					output[country]['B2C'][bidx] = bank;
				}
			}
		});
    });
    /********************************/
    return output;
}
//END-----Modify for FPX B2B
function isEmpty(a) {
    var res = false;
    if ($.type(a) == 'string' && a == '') {
        res = true;
    }
    else if ($.type(a) == 'object' && $.isEmptyObject(a)) {
        res = true;
    }
    else if (a==null) {
        res = true;
    }
    return res;
}
//Added by OoiMei, 8 Jun 2017. - Wallet lists
function populateOtherPaymentByCountryOB(initBankListOB, initBankListOTC, initBankListWA, elementPanelCountry, forexData, $cc, $price) {    
    var BankList = OB_OTC_merger(initBankListOB, initBankListOTC, initBankListWA);
	var fpxBanks = fpx_splitter(initBankListOB);//Modify for FPX B2B
	var onlyOB = false;
    var onlyOTC = false;
	var onlyWA = false;
	    
    if (isEmpty(initBankListOTC) && isEmpty(initBankListOB) && isEmpty(initBankListWA)) {
        $('#pm_heading').text('Payment Methods');
        elementPanelCountry.innerHTML = '<center><b><h3 style="color:red;">No Payment Methods Found!</h3></b></center>';
        return false;
    }
    else {
		if (isEmpty(initBankListOTC) && isEmpty(initBankListWA)) {
            $('#pm_heading').text('Online Banking');
            onlyOB = true;
        }
        if (isEmpty(initBankListOB) && isEmpty(initBankListWA)) {
            $('#pm_heading').text('Over the Counter');
            onlyOTC = true;
        }
		if (isEmpty(initBankListOTC) && isEmpty(initBankListOB)) {
            $('#pm_heading').text('Wallet');
            onlyWA = true;
        }
    }
    for (var key in BankList) {
        var templateCountryDIV = "<div class=\"panel panel-default\" style=\"display:block;\">";
        templateCountryDIV += "<div class=\"panel-heading price-container\">";
        templateCountryDIV += "    <h4 class=\"panel-title\">";
        templateCountryDIV += "       <a data-toggle=\"collapse\" [SETCLASS1] data-parent=\"#payment-by-country\" href=\"#c" + key + "\" style=\"text-decoration:none;\">";
        templateCountryDIV += "	        <img src=\"assets3/img/payments/flag-" + key + ".png\" width=\"40\"> " + key + " (<label id=\"ccurr" + key + "\">[CURCODE]</label>&nbsp;<label id=\"amt" + key + "\">[AMT]</label>)";
        templateCountryDIV += "       </a>";
        templateCountryDIV += "    </h4>";
        templateCountryDIV += "<label id=\"forex" + key + "\" style=\"display:none;\">[isforexvalue]</label>";
        templateCountryDIV += "</div>";
        templateCountryDIV += "<div id=\"c" + key + "\" [SETCLASS2]>";
        templateCountryDIV += "    <div class=\"panel-body\">";
        templateCountryDIV += "       <div class=\"accordion-v2 plus-toggle\">";
        templateCountryDIV += "	        <div class=\"panel-group\" id=\"" + key + "\">";

        if ("OB" in BankList[key]) {
            if (!onlyOB) {
                templateCountryDIV += "            <!-- Online Banking -->";
                templateCountryDIV += "		       <div class=\"panel panel-default\">";
                templateCountryDIV += "			      <div class=\"panel-heading price-container\">";
                templateCountryDIV += "				      <h4 class=\"panel-title\">";
                templateCountryDIV += "					      <a data-toggle=\"collapse\" data-parent=\"#" + key + "\" href=\"#" + key + "-ob\" style=\"text-decoration:none;\">";
                templateCountryDIV += "						     Online Banking";
                templateCountryDIV += "					      </a>";
                templateCountryDIV += "				      </h4>";
                templateCountryDIV += "			      </div>";
                templateCountryDIV += "			      <div id=\"" + key + "-ob\" class=\"panel-collapse collapse in\">";
                templateCountryDIV += "			          <div class=\"panel-body\" id=\"divPanelBodyOB" + key + "\">";
            }
                templateCountryDIV += "                     [oblist]";
            if (!onlyOB) {
                templateCountryDIV += "				      </div>";
                templateCountryDIV += "			      </div>";
                templateCountryDIV += "		       </div>";
                templateCountryDIV += "		       <!-- End Online Banking -->";
            }
            
        }

        if ("OTC" in BankList[key]) {
            if (!onlyOTC) {
                templateCountryDIV += "            <!-- Over the Counter -->";
                templateCountryDIV += " 		   <div class=\"panel panel-default\" style=\"display:[dispstyle]\">";
                templateCountryDIV += "			      <div class=\"panel-heading price-container\">";
                templateCountryDIV += "				      <h4 class=\"panel-title\">";

                if ('OB' in BankList[key]) {
                    templateCountryDIV += "					  <a data-toggle=\"collapse\" class=\"collapsed\" data-parent=\"#" + key + "\" href=\"#" + key + "-otc\" style=\"text-decoration:none;\">";
                }
                else {
                    templateCountryDIV += "					  <a aria-expanded=\"true\" data-toggle=\"collapse\" data-parent=\"#" + key + "\" href=\"#" + key + "-otc\" style=\"text-decoration:none;\">";
                }
                
                templateCountryDIV += "						     Over the Counter";
                templateCountryDIV += "					      </a>";
                templateCountryDIV += "				      </h4>";
                templateCountryDIV += "			      </div>";

                if ('OB' in BankList[key]) {
                    templateCountryDIV += "			  <div id=\"" + key + "-otc\" class=\"panel-collapse collapse\">";
                }
                else {
                    templateCountryDIV += "			  <div aria-expanded=\"true\" id=\"" + key + "-otc\" class=\"panel-collapse collapse in\">";
                }
                
                templateCountryDIV += "				      <div class=\"panel-body\" id=\"divPanelBodyOTC" + key + "\">";
            }
                templateCountryDIV += "					    [otclist]";
            if (!onlyOTC) {
                templateCountryDIV += "				      </div>";
                templateCountryDIV += "			      </div>";
                templateCountryDIV += "		       </div>";
                templateCountryDIV += "		       <!-- End Over the Counter -->";
            }
        }
		
		if ("WA" in BankList[key]) {
            if (!onlyWA) {
                templateCountryDIV += "            <!-- Wallet -->";
                templateCountryDIV += "		       <div class=\"panel panel-default\">";
                templateCountryDIV += "			      <div class=\"panel-heading price-container\">";
                templateCountryDIV += "				      <h4 class=\"panel-title\">";
				
				if ('OB' in BankList[key] || 'OTC' in BankList[key]) {
					// close/collapse
					templateCountryDIV += "					<a data-toggle=\"collapse\" class=\"collapsed\" data-parent=\"#" + key + "\" href=\"#" + key + "-wa\" style=\"text-decoration:none;\">";
				}
				else{
					// open
					templateCountryDIV += "					<a aria-expanded=\"true\" data-toggle=\"collapse\" data-parent=\"#" + key + "\" href=\"#" + key + "-wa\" style=\"text-decoration:none;\">";
				}
				
                templateCountryDIV += "						     Wallet";
                templateCountryDIV += "					      </a>";
                templateCountryDIV += "				      </h4>";
                templateCountryDIV += "			      </div>";
				
				if ('OB' in BankList[key] || 'OTC' in BankList[key]) {
					// close/collapse
					templateCountryDIV += "			      <div id=\"" + key + "-wa\" class=\"panel-collapse collapse\">";
				}
				else{
					// open
					templateCountryDIV += "			      <div aria-expanded=\"true\" id=\"" + key + "-wa\" class=\"panel-collapse collapse in\">";
				}
                
                templateCountryDIV += "			          <div class=\"panel-body\" id=\"divPanelBodyWA" + key + "\">";
            }
                templateCountryDIV += "                     [walist]";
            if (!onlyWA) {
                templateCountryDIV += "				      </div>";
                templateCountryDIV += "			      </div>";
                templateCountryDIV += "		       </div>";
                templateCountryDIV += "		       <!-- End Wallet -->";
            }
            
        }

        templateCountryDIV += "            </div>";
        templateCountryDIV += "        </div>";
        templateCountryDIV += "    </div>";
        templateCountryDIV += "</div>";
        templateCountryDIV += "</div>";
		
        if ('OB' in BankList[key]) { var bankspropOB = BankList[key]['OB']; } else { var bankspropOB = new Object(); }
        var strObList = "";
		//START-----Modify for FPX B2B
		var strFpxList="";
		var gotB2B = false;
		var gotB2C = false;
		var fpx_radio = "";
		var fpx_radio2 = "";
		
		if(typeof fpxBanks[key] != 'undefined'){			
			if(Object.keys(fpxBanks[key]['B2B']).length){
				var gotB2B = true;
			}	
			if(Object.keys(fpxBanks[key]['B2C']).length){
				var gotB2C = true;
			}	
		}
		
		for (i = 0; i < bankspropOB.length; i++) {			
				// if (bankspropOB[i]["imgName"] == "meps-fpx.png") {
					// strObList = strObList + "<a href=\"#\" OnClick=\"VerifyDataOthers('OB','" + bankspropOB[i]["issuingBank"] + "','" + key + "')\"><img id=\"imgmepsfpx\" src=\"assets3/img/" + bankspropOB[i]["imgName"] + "\" alt=\"" + bankspropOB[i]["dispName"] + "\"/></a>";
				// }
				// else if(!gotFPXddl || (gotFPXddl&&!bankspropOB[i]["issuingBank"].startsWith("FPXD"))){					
				if (!(bankspropOB[i]["issuingBank"].indexOf("FPXD")>=0)){					
					strObList = strObList + "<a href=\"#\" OnClick=\"VerifyDataOthers('OB','" + bankspropOB[i]["issuingBank"] + "','" + key + "')\"><img src=\"assets3/img/" + bankspropOB[i]["imgName"] + "\" width=\"105\" alt=\"" + bankspropOB[i]["dispName"] + "\"/></a>";
				}
				// }			
			}	
		
		if (gotB2B || gotB2C){
				
			
			$.each(fpxBanks[key]['B2C'],function(i,meta){
					var display_name = (meta["dispName"] == "") ? meta["issuingBank"] : meta["dispName"];
					fpx_radio += "<a href=\"#\" OnClick=\"VerifyDataOthers('OB','" + meta["issuingBank"] + "','" + key + "')\"><img src=\"assets3/img/" + meta["imgName"] + "\" height=\"44\" width=\"105\" alt=\"" + meta["dispName"] + "\"/></a>"
				});
			
			$.each(fpxBanks[key]['B2B'],function(i,meta){
					var display_name = (meta["dispName"] == "") ? meta["issuingBank"] : meta["dispName"];
					fpx_radio2 += "<a href=\"#\" OnClick=\"VerifyDataOthers('OB','" + meta["issuingBank"] + "','" + key + "')\"><img src=\"assets3/img/" + meta["imgName"] + "\" height=\"44\" width=\"105\" alt=\"" + meta["dispName"] + "\"/></a>"
				});
			
			if(gotB2C && gotB2B){
				// B2C list
				strFpxList += '<div class"accordion-v2 plus-toggle">'
				strFpxList += '<div class="panel-group" id="MY">'
				strFpxList += '<div class="panel panel-default">'
				strFpxList += '<div class="panel-heading price-container">'
				strFpxList += '<h4 class="panel-title">'
				strFpxList += '<a data-toggle="collapse" data-parent="#B2C" href="#B2C-B2C" style="text-decoration: none;" aria-expanded="true" class>'
				strFpxList += 'Retail Internet Banking'
				strFpxList += '</a>'
				strFpxList += '</h4>'
				strFpxList += '</div>'
				strFpxList += '<div id="B2C-B2C" class="panel-collapse collapse in" aria-expanded="true" style>'
				strFpxList += '<div class="panel-body" id="B2C">'
				strFpxList += fpx_radio
				strFpxList += '</div>'
				strFpxList += '</div>'
				strFpxList += '</div>'
				strFpxList += '</div>'
				strFpxList += '</div>'
				
				strFpxList += '<br>'
			
				// B2B list
				strFpxList += '<div class"accordion-v2 plus-toggle">'
				strFpxList += '<div class="panel-group" id="MY">'
				strFpxList += '<div class="panel panel-default">'
				strFpxList += '<div class="panel-heading price-container">'
				strFpxList += '<h4 class="panel-title">'
				strFpxList += '<a data-toggle="collapse" data-parent="#B2B" href="#B2B-B2B" style="text-decoration: none;" aria-expanded="true" class>'
				strFpxList += 'Corporate Internet Banking'
				strFpxList += '</a>'
				strFpxList += '</h4>'
				strFpxList += '</div>'
				strFpxList += '<div id="B2B-B2B" class="panel-collapse collapse in" aria-expanded="true" style>'
				strFpxList += '<div class="panel-body" id="B2B">'
				strFpxList += fpx_radio2
				strFpxList += '</div>'
				strFpxList += '</div>'
				strFpxList += '</div>'
				strFpxList += '</div>'
				strFpxList += '</div>'
			}
			else {
				strFpxList = "<div>" + fpx_radio + fpx_radio2 + "</div>"
			}
			
			strObList = strFpxList + strObList ;
		}			
		//END-----Modify for FPX B2B
        templateCountryDIV = templateCountryDIV.replace("[oblist]", strObList)
		
		if ('WA' in BankList[key]) { var bankspropWA = BankList[key]['WA']; } else { var bankspropWA = new Object(); }
        var strWAList = "";
        for (i = 0; i < bankspropWA.length; i++) {
			strWAList = strWAList + "<a href=\"#\" OnClick=\"VerifyDataOthers('WA','" + bankspropWA[i]["issuingBank"] + "','" + key + "')\"><img src=\"assets3/img/" + bankspropWA[i]["imgName"] + "\" width=\"80\" alt=\"" + bankspropWA[i]["dispName"] + "\"/></a>";         
        }
        templateCountryDIV = templateCountryDIV.replace("[walist]", strWAList)

        if (initBankListOTC != "") {
            if (initBankListOTC.hasOwnProperty(key)) {
                
                if ('OTC' in BankList[key]) { var bankspropOTC = BankList[key]['OTC']; } else { var bankspropOTC = new Object(); }
                
                var strOTCList = "";
                for (i = 0; i < bankspropOTC.length; i++) {
                    strOTCList = strOTCList + "<a href=\"#\" OnClick=\"VerifyDataOthers('OTC','" + bankspropOTC[i]["issuingBank"] + "','" + key + "')\"><img src=\"assets3/img/" + bankspropOTC[i]["imgName"] + "\" width=\"80\" alt=\"" + bankspropOTC[i]["dispName"] + "\"></a>";
                }
                templateCountryDIV = templateCountryDIV.replace("[dispstyle]", "block");
                templateCountryDIV = templateCountryDIV.replace("[otclist]", strOTCList)
            }
            else {
                templateCountryDIV = templateCountryDIV.replace("[dispstyle]", "none");
            }
        }
        else {
            templateCountryDIV = templateCountryDIV.replace("[dispstyle]", "none");
        }

        
        var newCurr = "";
        var newPrice = "";
        var isForexData = false;

        var basecur = $cc.text();
        var baseamt = $price.text();

        // checking whether Forex or not               
        if (forex.hasOwnProperty(key)) {
            isForexData = true;
        }
        //

        var temp = templateCountryDIV;

        if (isForexData) {
            if (forex.hasOwnProperty(key)) {
                newCurr = getCurr(key, forexData);				
				if (basecur == newCurr) {
					newPrice = formatCurrency(getPrice(key, baseamt, forexData));
				}
				else{
					newPrice = formatCurrency(getCPrice(key, baseamt, forexData));
				}
                temp = temp.replace("[CURCODE]", newCurr);
                temp = temp.replace("[AMT]", newPrice);
                temp = temp.replace("[isforexvalue]", 1);

                if (newCurr == basecur) {
                    temp = temp.replace("[SETCLASS1]", "aria-expanded=\"true\"");
                    temp = temp.replace("[SETCLASS2]", "class=\"panel-collapse collapse in\" aria-expanded=\"true\"");
                    elementPanelCountry.innerHTML = temp + elementPanelCountry.innerHTML;
                }
                else {
                    temp = temp.replace("[SETCLASS1]", "class=\"collapsed\" aria-expanded=\"false\"");
                    temp = temp.replace("[SETCLASS2]", "class=\"panel-collapse collapse\" aria-expanded=\"false\"");
                    elementPanelCountry.innerHTML = elementPanelCountry.innerHTML + temp;
                }
            }
            
        } else {
            newCurr = basecur;
            newPrice = baseamt;

            temp = temp.replace("[CURCODE]", newCurr);
            temp = temp.replace("[AMT]", newPrice);
            temp = temp.replace("[isforexvalue]", 0);

            if (newCurr == basecur) {
                temp = temp.replace("[SETCLASS1]", "aria-expanded=\"true\"");
                temp = temp.replace("[SETCLASS2]", "class=\"panel-collapse collapse in\" aria-expanded=\"true\"");
                elementPanelCountry.innerHTML = temp + elementPanelCountry.innerHTML;
            }
            else {
                temp = temp.replace("[SETCLASS1]", "class=\"collapsed\" aria-expanded=\"false\"");
                temp = temp.replace("[SETCLASS2]", "class=\"panel-collapse collapse\" aria-expanded=\"false\"");
                elementPanelCountry.innerHTML = elementPanelCountry.innerHTML + temp;
            }
        }
		
		if(key=="MY") //if(key=="MY" && parseFloat($('#amtMY').text())<2)
		{			
			var checkamt=$('#amtMY').text().replace(',', '');
			if(parseFloat(checkamt)<2)
			{
				$("input[type=radio][value=B2B]").attr("disabled",true);
				jQuery('#' + key + '_ddlfpx_B2B_err').html("Minimum RM2.00 for B2B");
			}			
		}
		
		
		//START-----Modify for FPX B2B
		jQuery(document.body).on('click','#'+key+'_ddlfpx_Pay_OB', function () {				
			var key = jQuery(this).attr('id').replace('_ddlfpx_Pay_OB','');
            //jQuery('#'+key+'_ddlfpx_Pay_OB').prop('disabled',true);
            if (validateOBPayment(key)) {
                VerifyDataOthers('OB', jQuery('#'+key+'_ddlfpx').val(), key);
            }
            else {
                jQuery('#'+key+'_ddlfpx_Pay_OB').prop('disabled', false);
            }
        });
		jQuery(document.body).on("change", 'input[name="'+key+'_fpxType"]', function(){
			var opt = jQuery(this).val();
			var options="<option value=\"\">Select Bank</option>";
			var key = jQuery(this).attr('name').replace('_fpxType','');
			
			$.each(fpxBanks[key][opt],function(i,meta){
				var display_name = (meta["dispName"] == "") ? meta["issuingBank"] : meta["dispName"];
				if (display_name.indexOf("Offline") >= 0) {
					var option = '<option value="' + meta["issuingBank"] + '" disabled>' + display_name + '</option>';
				}
				else{
					var option = '<option value="' + meta["issuingBank"] + '">' + display_name + '</option>';
				}
				options = options+option;
			});
			jQuery('#'+key+'_ddlfpx').html(options);
		});
		//END-----Modify for FPX B2B		
    }
}
//START-----Modify for FPX B2B
function validateOBPayment(key) {
    var bReturn = true;
    var err_msg = '';
    jQuery('#' + key + '_ddlfpx_OB_err').html('');	
    if (jQuery('#' + key + '_ddlfpx').val() == "") {
        bReturn = false;
        err_msg += "Please select the bank.";
        jQuery('#' + key + '_ddlfpx').focus();
    }
    jQuery('#' + key + '_ddlfpx_OB_err').html(err_msg);
    return bReturn;
}
//END-----Modify for FPX B2B
function populateOtherPaymentByCountryOTC(initBankListOTC, elementPanelCountry, forexData, $cc, $price) {
    $('#pm_heading').text('Over the Counter');
	console.log(forexData);
    for (var key in initBankListOTC) {
        var templateCountryDIV = "<div class=\"panel panel-default\" style=\"display:block;\">"
                                + "<div class=\"panel-heading price-container\">"
                                + "    <h4 class=\"panel-title\">"
                                + "       <a data-toggle=\"collapse\" [SETCLASS1] data-parent=\"#payment-by-country\" href=\"#c" + key + "\">"
                                + "	        <img src=\"assets3/img/payments/flag-" + key + ".png\" width=\"40\"> " + key + " (<label id=\"ccurr" + key + "\">[CURCODE]</label>&nbsp;<label id=\"amt" + key + "\">[AMT]</label>)"
                                + "       </a>"
                                + "    </h4>"
                                + "<label id=\"forex" + key + "\" style=\"display:none;\">[isforexvalue]</label>"
                                + "</div>"
                                + "<div id=\"c" + key + "\" [SETCLASS2]>"
                                + "    <div class=\"panel-body\">"
                                + "			[otclist]"
                                //+ "       <div class=\"accordion-v2 plus-toggle\">"
                                //+ "	        <div class=\"panel-group\" id=\"" + key + "\">"
                                //+ "            <!-- Over the Counter -->"
                                //+ " 		   <div class=\"panel panel-default\" style=\"display:[dispstyle]\">"
                                //+ "			      <div class=\"panel-heading price-container\">"
                                //+ "				      <h4 class=\"panel-title\">"
                                //+ "					      <a data-toggle=\"collapse\" class=\"collapsed\" data-parent=\"#" + key + "\" href=\"#" + key + "-otc\">"
                                //+ "						     Over the Counter"
                                //+ "					      </a>"
                                //+ "				      </h4>"
                                //+ "			      </div>"
                                //+ "			      <div id=\"" + key + "-otc\" class=\"panel-collapse collapse\">"
                                //+ "				      <div class=\"panel-body\" id=\"divPanelBodyOTC" + key + "\">"
                                //+ "					    [otclist]"
                                //+ "				      </div>"
                                //+ "			      </div>"
                                //+ "		       </div>"
                                //+ "		       <!-- End Over the Counter -->"
                                //+ "            </div>"
                                //+ "        </div>"
                                + "    </div>"
                                + "</div>"
                                + "</div>";

        var banksOTC = JSON.parse(JSON.stringify(initBankListOTC[key]));
        var bankspropOTC = JSON.parse(JSON.stringify(banksOTC["banks"]));
        var strOTCList = "";
        for (i = 0; i < bankspropOTC.length; i++) {
            strOTCList = strOTCList + "<a href=\"#\" OnClick=\"VerifyDataOthers('OTC','" + bankspropOTC[i]["issuingBank"] + "','" + key + "')\"><img src=\"assets3/img/" + bankspropOTC[i]["imgName"] + "\" width=\"80\" alt=\"" + bankspropOTC[i]["dispName"] + "\"></a>";
        }
        templateCountryDIV = templateCountryDIV.replace("[dispstyle]", "block");
        templateCountryDIV = templateCountryDIV.replace("[otclist]", strOTCList);

        var newCurr = "";
        var newPrice = "";
        var isForexData = false;

        var basecur = $cc.text();
        var baseamt = $price.text();

        // checking whether Forex or not               
        if (forex.hasOwnProperty(key)) {
            isForexData = true;
        }
        //

        var temp = templateCountryDIV;

        if (isForexData) {
            if (forex.hasOwnProperty(key)) {
                newCurr = getCurr(key, forexData);
                newPrice = formatCurrency(getPrice(key, baseamt, forexData));

                temp = temp.replace("[CURCODE]", newCurr);
                temp = temp.replace("[AMT]", newPrice);
                temp = temp.replace("[isforexvalue]", 1);

                if (newCurr == basecur) {
                    temp = temp.replace("[SETCLASS1]", "aria-expanded=\"true\"");
                    temp = temp.replace("[SETCLASS2]", "class=\"panel-collapse collapse in\" aria-expanded=\"true\"");
                    elementPanelCountry.innerHTML = temp + elementPanelCountry.innerHTML;
                }
                else {
                    temp = temp.replace("[SETCLASS1]", "class=\"collapsed\" aria-expanded=\"false\"");
                    temp = temp.replace("[SETCLASS2]", "class=\"panel-collapse collapse\" aria-expanded=\"false\"");
                    elementPanelCountry.innerHTML = elementPanelCountry.innerHTML + temp;
                }
            }

        } else {
            newCurr = basecur;
            newPrice = baseamt;

            temp = temp.replace("[CURCODE]", newCurr);
            temp = temp.replace("[AMT]", newPrice);
            temp = temp.replace("[isforexvalue]", 0);

            if (newCurr == basecur) {
                temp = temp.replace("[SETCLASS1]", "aria-expanded=\"true\"");
                temp = temp.replace("[SETCLASS2]", "class=\"panel-collapse collapse in\" aria-expanded=\"true\"");
                elementPanelCountry.innerHTML = temp + elementPanelCountry.innerHTML;
            }
            else {
                temp = temp.replace("[SETCLASS1]", "class=\"collapsed\" aria-expanded=\"false\"");
                temp = temp.replace("[SETCLASS2]", "class=\"panel-collapse collapse\" aria-expanded=\"false\"");
                elementPanelCountry.innerHTML = elementPanelCountry.innerHTML + temp;
            }
        } 
    }
}

function populateOtherPaymentByWallet(initBankListWA, elementPanelWA, isExpandPane)
{
    if (isExpandPane) {
        $('#pm_heading').text('eWallet');
    }
    
    for (var key in initBankListWA)
    {
        var templateWalletDIV = "<div class=\"panel panel-default\"  id=\"divWA\" >";
        templateWalletDIV += "      <div class=\"panel-heading price-container\">";
        templateWalletDIV += "          <h4 class=\"panel-title\">";
        
        if (isExpandPane) {
            templateWalletDIV += "              <p style=\"padding:10px;font-size:16px;\">eWallet Payment Methods</p>";
        }
        else {
            templateWalletDIV += "              <a data-toggle=\"collapse\" aria-expanded=\"true\" data-parent=\"#payment-wallet\" href=\"#wa\" style=\"text-decoration:none;\">"
            templateWalletDIV += "              eWallet";
            templateWalletDIV += "              </a>";
        }
        
        templateWalletDIV += "          </h4>";
        templateWalletDIV += "      </div>";
        templateWalletDIV += "      <div id=\"wa\" class=\"collapse in\" aria-expanded=\"true\" >";
        templateWalletDIV += "          <div class=\"panel-body\">";
        templateWalletDIV += "          [WAList]";
        templateWalletDIV += "          </div>";
        templateWalletDIV += "      </div>";
        templateWalletDIV += "   </div>";

        var banksWA = JSON.parse(JSON.stringify(initBankListWA[key]));
        var bankspropWA = JSON.parse(JSON.stringify(banksWA["banks"]));
        var strWAList = "";
        for (i = 0; i < bankspropWA.length; i++) {
            strWAList = strWAList + "<a href=\"#\" OnClick=\"VerifyDataOthersWA('" + bankspropWA[i]["issuingBank"] + "')\"><img src=\"assets3/img/" + bankspropWA[i]["imgName"] + "\" width=\"80\" alt=\"" + bankspropWA[i]["dispName"] + "\"/></a>";
        }
        templateWalletDIV = templateWalletDIV.replace("[WAList]", strWAList)
    }

    if (!isExpandPane) {
        templateWalletDIV = templateWalletDIV.replace("[SETCLASS1]", "class=\"collapsed\" aria-expanded=\"false\"");
    }
    else {
        templateWalletDIV = templateWalletDIV.replace("[SETCLASS1]", "aria-expanded=\"true\"");
    }

    elementPanelWA.innerHTML = templateWalletDIV;
}

function getCurr(cval, forex) {
    return forex[cval].curr;
}
function getPrice(cval, baseamt, forex) {
    return toFixed(baseamt * forex[cval].rate, 2).toFixed(2);
}
function getCPrice(cval, baseamt, forex) {
	if (forex[cval].curr.toUpperCase()==='IDR' || forex[cval].curr.toUpperCase()==='JPY' || forex[cval].curr.toUpperCase()==='KRW' || forex[cval].curr.toUpperCase()==='VND') {
		return toCeil(baseamt * forex[cval].rate, 2).toFixed(2);}
	else {
		return toFixed(baseamt * forex[cval].rate, 2).toFixed(2);}
}
function toFixed(number, precision) {
    var multiplier = Math.pow(10, precision + 1),
    wholeNumber = Math.floor(number * multiplier);
    return Math.round(wholeNumber / 10) * 10 / multiplier;
}
function toCeil(number, precision) { 
    var multiplier = Math.pow(10, precision + 1),
    wholeNumber = Math.floor(number * multiplier),
    finalNumber = Math.round(wholeNumber / 10) * 10 / multiplier;
	return Math.ceil(finalNumber);
}

function formatCurrency(number) {
    var n = number.split('').reverse().join("");
    var n2 = n.replace(/\d\d\d(?!$)/g, "$&,");
    return n2.split('').reverse().join('');
}