function OB_OTC_merger(ListOB, ListOTC) {
    var output = new Object();

    /*** Countries initialization ***/
    $.each(ListOB, function (index, val) {
        output[index] = new Object();
        output[index]['OB'] = val.banks;
    });
    $.each(ListOTC, function (index, val) {
        if (!(index in output)) { /* Don't Add index if already exist */
            output[index] = new Object();
        }
        output[index]['OTC'] = val.banks;
    });
    /********************************/
    return output;
}

function populateOtherPaymentByCountryOB(initBankListOB, initBankListOTC, elementPanelCountry, forexData, $cc, $price) {    
    var BankList = OB_OTC_merger(initBankListOB, initBankListOTC);

    for (var key in BankList) {
        var templateCountryDIV = "<div class=\"panel panel-default\" style=\"display:block;\">";
        templateCountryDIV += "<div class=\"panel-heading price-container\">";
        templateCountryDIV += "    <h4 class=\"panel-title\">";
        templateCountryDIV += "       <a data-toggle=\"collapse\" [SETCLASS1] data-parent=\"#payment-by-country\" href=\"#c" + key + "\" style=\"text-decoration:none;\">";
        templateCountryDIV += "	        <img src=\"assets3/img/payments/flag-" + key + ".png\" width=\"40\"> " + key + " (<label id=\"ccurr" + key + "\">[CURCODE]</label>&nbsp;<label id=\"amt" + key + "\">[AMT]</label>)";
        templateCountryDIV += "       </a>";
        templateCountryDIV += "    </h4>";
        templateCountryDIV += "<label id=\"forex" + key + "\" style=\"display:none;\">[isforexvalue]</label>";
        templateCountryDIV += "</div>";
        templateCountryDIV += "<div id=\"c" + key + "\" [SETCLASS2]>";
        templateCountryDIV += "    <div class=\"panel-body\">";
        templateCountryDIV += "       <div class=\"accordion-v2 plus-toggle\">";
        templateCountryDIV += "	        <div class=\"panel-group\" id=\"" + key + "\">";

        if ("OB" in BankList[key]) {
            templateCountryDIV += "            <!-- Online Banking -->";
            templateCountryDIV += "		       <div class=\"panel panel-default\">";
            templateCountryDIV += "			      <div class=\"panel-heading price-container\">";
            templateCountryDIV += "				      <h4 class=\"panel-title\">";
            templateCountryDIV += "					      <a data-toggle=\"collapse\" data-parent=\"#" + key + "\" href=\"#" + key + "-ob\" style=\"text-decoration:none;\">";
            templateCountryDIV += "						     Online Banking";
            templateCountryDIV += "					      </a>";
            templateCountryDIV += "				      </h4>";
            templateCountryDIV += "			      </div>";
            templateCountryDIV += "			      <div id=\"" + key + "-ob\" class=\"panel-collapse collapse in\">";
            templateCountryDIV += "			          <div class=\"panel-body\" id=\"divPanelBodyOB" + key + "\">";
            templateCountryDIV += "                     [oblist]";
            templateCountryDIV += "				      </div>";
            templateCountryDIV += "			      </div>";
            templateCountryDIV += "		       </div>";
            templateCountryDIV += "		       <!-- End Online Banking -->";
        }

        if ("OTC" in BankList[key]) {
            templateCountryDIV += "            <!-- Over the Counter -->";
            templateCountryDIV += " 		   <div class=\"panel panel-default\" style=\"display:[dispstyle]\">";
            templateCountryDIV += "			      <div class=\"panel-heading price-container\">";
            templateCountryDIV += "				      <h4 class=\"panel-title\">";
            templateCountryDIV += "					      <a data-toggle=\"collapse\" class=\"collapsed\" data-parent=\"#" + key + "\" href=\"#" + key + "-otc\" style=\"text-decoration:none;\">"
            templateCountryDIV += "						     Over the Counter";
            templateCountryDIV += "					      </a>";
            templateCountryDIV += "				      </h4>";
            templateCountryDIV += "			      </div>";
            templateCountryDIV += "			      <div id=\"" + key + "-otc\" class=\"panel-collapse collapse\">";
            templateCountryDIV += "				      <div class=\"panel-body\" id=\"divPanelBodyOTC" + key + "\">";
            templateCountryDIV += "					    [otclist]";
            templateCountryDIV += "				      </div>";
            templateCountryDIV += "			      </div>";
            templateCountryDIV += "		       </div>";
            templateCountryDIV += "		       <!-- End Over the Counter -->";
        }

        templateCountryDIV += "            </div>";
        templateCountryDIV += "        </div>";
        templateCountryDIV += "    </div>";
        templateCountryDIV += "</div>";
        templateCountryDIV += "</div>";


        if ('OB' in BankList[key]) { var bankspropOB = BankList[key]['OB']; } else { var bankspropOB = new Object(); }
        var strObList = "";
        for (i = 0; i < bankspropOB.length; i++) {
            if (bankspropOB[i]["imgName"] == "meps-fpx.png") {
                strObList = strObList + "<a href=\"#\" OnClick=\"VerifyDataOthers('DD','" + bankspropOB[i]["issuingBank"] + "','" + key + "')\"><img id=\"imgmepsfpx\" src=\"assets3/img/" + bankspropOB[i]["imgName"] + "\" alt=\"" + bankspropOB[i]["dispName"] + "\"/></a>";
            }
            else {
                strObList = strObList + "<a href=\"#\" OnClick=\"VerifyDataOthers('DD','" + bankspropOB[i]["issuingBank"] + "','" + key + "')\"><img src=\"assets3/img/" + bankspropOB[i]["imgName"] + "\" width=\"80\" alt=\"" + bankspropOB[i]["dispName"] + "\"/></a>";
            }
        }
        templateCountryDIV = templateCountryDIV.replace("[oblist]", strObList)

        if (initBankListOTC != "") {
            if (initBankListOTC.hasOwnProperty(key)) {
                
                if ('OTC' in BankList[key]) { var bankspropOTC = BankList[key]['OTC']; } else { var bankspropOTC = new Object(); }
                
                var strOTCList = "";
                for (i = 0; i < bankspropOTC.length; i++) {
                    strOTCList = strOTCList + "<a href=\"#\" OnClick=\"VerifyDataOthers('OTC','" + bankspropOTC[i]["issuingBank"] + "','" + key + "')\"><img src=\"assets3/img/" + bankspropOTC[i]["imgName"] + "\" width=\"80\" alt=\"" + bankspropOTC[i]["dispName"] + "\"></a>";
                }
                templateCountryDIV = templateCountryDIV.replace("[dispstyle]", "block");
                templateCountryDIV = templateCountryDIV.replace("[otclist]", strOTCList)
            }
            else {
                templateCountryDIV = templateCountryDIV.replace("[dispstyle]", "none");
            }
        }
        else {
            templateCountryDIV = templateCountryDIV.replace("[dispstyle]", "none");
        }

        
        var newCurr = "";
        var newPrice = "";
        var isForexData = false;

        var basecur = $cc.text();
        var baseamt = $price.text();

        // checking whether Forex or not               
        if (forex.hasOwnProperty(key)) {
            isForexData = true;
        }
        //

        var temp = templateCountryDIV;

        if (isForexData) {
            if (forex.hasOwnProperty(key)) {
                newCurr = getCurr(key, forexData);
                newPrice = formatCurrency(getPrice(key, baseamt, forexData));

                temp = temp.replace("[CURCODE]", newCurr);
                temp = temp.replace("[AMT]", newPrice);
                temp = temp.replace("[isforexvalue]", 1);

                if (newCurr == basecur) {
                    temp = temp.replace("[SETCLASS1]", "aria-expanded=\"true\"");
                    temp = temp.replace("[SETCLASS2]", "class=\"panel-collapse collapse in\" aria-expanded=\"true\"");
                    elementPanelCountry.innerHTML = temp + elementPanelCountry.innerHTML;
                }
                else {
                    temp = temp.replace("[SETCLASS1]", "class=\"collapsed\" aria-expanded=\"false\"");
                    temp = temp.replace("[SETCLASS2]", "class=\"panel-collapse collapse\" aria-expanded=\"false\"");
                    elementPanelCountry.innerHTML = elementPanelCountry.innerHTML + temp;
                }
            }
            
        } else {
            newCurr = basecur;
            newPrice = baseamt;

            temp = temp.replace("[CURCODE]", newCurr);
            temp = temp.replace("[AMT]", newPrice);
            temp = temp.replace("[isforexvalue]", 0);

            if (newCurr == basecur) {
                temp = temp.replace("[SETCLASS1]", "aria-expanded=\"true\"");
                temp = temp.replace("[SETCLASS2]", "class=\"panel-collapse collapse in\" aria-expanded=\"true\"");
                elementPanelCountry.innerHTML = temp + elementPanelCountry.innerHTML;
            }
            else {
                temp = temp.replace("[SETCLASS1]", "class=\"collapsed\" aria-expanded=\"false\"");
                temp = temp.replace("[SETCLASS2]", "class=\"panel-collapse collapse\" aria-expanded=\"false\"");
                elementPanelCountry.innerHTML = elementPanelCountry.innerHTML + temp;
            }
        }
    }
}
function populateOtherPaymentByCountryOTC(initBankListOTC, elementPanelCountry, forexData, $cc, $price) {

    for (var key in initBankListOTC) {
        var templateCountryDIV = "<div class=\"panel panel-default\" style=\"display:block;\">"
                                + "<div class=\"panel-heading price-container\">"
                                + "    <h4 class=\"panel-title\">"
                                + "       <a data-toggle=\"collapse\" [SETCLASS1] data-parent=\"#payment-by-country\" href=\"#c" + key + "\">"
                                + "	        <img src=\"assets3/img/payments/flag-" + key + ".png\" width=\"40\"> " + key + " (<label id=\"ccurr" + key + "\">[CURCODE]</label>&nbsp;<label id=\"amt" + key + "\">[AMT]</label>)"
                                + "       </a>"
                                + "    </h4>"
                                + "<label id=\"forex" + key + "\" style=\"display:none;\">[isforexvalue]</label>"
                                + "</div>"
                                + "<div id=\"c" + key + "\" [SETCLASS2]>"
                                + "    <div class=\"panel-body\">"
                                + "       <div class=\"accordion-v2 plus-toggle\">"
                                + "	        <div class=\"panel-group\" id=\"" + key + "\">"
                                + "            <!-- Over the Counter -->"
                                + " 		   <div class=\"panel panel-default\" style=\"display:[dispstyle]\">"
                                + "			      <div class=\"panel-heading price-container\">"
                                + "				      <h4 class=\"panel-title\">"
                                + "					      <a data-toggle=\"collapse\" class=\"collapsed\" data-parent=\"#" + key + "\" href=\"#" + key + "-otc\">"
                                + "						     Over the Counter"
                                + "					      </a>"
                                + "				      </h4>"
                                + "			      </div>"
                                + "			      <div id=\"" + key + "-otc\" class=\"panel-collapse collapse\">"
                                + "				      <div class=\"panel-body\" id=\"divPanelBodyOTC" + key + "\">"
                                + "					    [otclist]"
                                + "				      </div>"
                                + "			      </div>"
                                + "		       </div>"
                                + "		       <!-- End Over the Counter -->"
                                + "            </div>"
                                + "        </div>"
                                + "    </div>"
                                + "</div>"
                                + "</div>";

        var banksOTC = JSON.parse(JSON.stringify(initBankListOTC[key]));
        var bankspropOTC = JSON.parse(JSON.stringify(banksOTC["banks"]));
        var strOTCList = "";
        for (i = 0; i < bankspropOTC.length; i++) {
            strOTCList = strOTCList + "<a href=\"#\" OnClick=\"VerifyDataOthers('OTC','" + bankspropOTC[i]["issuingBank"] + "','" + key + "')\"><img src=\"assets3/img/" + bankspropOTC[i]["imgName"] + "\" width=\"80\" alt=\"" + bankspropOTC[i]["dispName"] + "\"></a>";
        }
        templateCountryDIV = templateCountryDIV.replace("[dispstyle]", "block");
        templateCountryDIV = templateCountryDIV.replace("[otclist]", strOTCList);

        var newCurr = "";
        var newPrice = "";
        var isForexData = false;

        var basecur = $cc.text();
        var baseamt = $price.text();

        // checking whether Forex or not               
        if (forex.hasOwnProperty(key)) {
            isForexData = true;
        }
        //

        var temp = templateCountryDIV;

        if (isForexData) {
            if (forex.hasOwnProperty(key)) {
                newCurr = getCurr(key, forexData);
                newPrice = formatCurrency(getPrice(key, baseamt, forexData));

                temp = temp.replace("[CURCODE]", newCurr);
                temp = temp.replace("[AMT]", newPrice);
                temp = temp.replace("[isforexvalue]", 1);

                if (newCurr == basecur) {
                    temp = temp.replace("[SETCLASS1]", "aria-expanded=\"true\"");
                    temp = temp.replace("[SETCLASS2]", "class=\"panel-collapse collapse in\" aria-expanded=\"true\"");
                    elementPanelCountry.innerHTML = temp + elementPanelCountry.innerHTML;
                }
                else {
                    temp = temp.replace("[SETCLASS1]", "class=\"collapsed\" aria-expanded=\"false\"");
                    temp = temp.replace("[SETCLASS2]", "class=\"panel-collapse collapse\" aria-expanded=\"false\"");
                    elementPanelCountry.innerHTML = elementPanelCountry.innerHTML + temp;
                }
            }

        } else {
            newCurr = basecur;
            newPrice = baseamt;

            temp = temp.replace("[CURCODE]", newCurr);
            temp = temp.replace("[AMT]", newPrice);
            temp = temp.replace("[isforexvalue]", 0);

            if (newCurr == basecur) {
                temp = temp.replace("[SETCLASS1]", "aria-expanded=\"true\"");
                temp = temp.replace("[SETCLASS2]", "class=\"panel-collapse collapse in\" aria-expanded=\"true\"");
                elementPanelCountry.innerHTML = temp + elementPanelCountry.innerHTML;
            }
            else {
                temp = temp.replace("[SETCLASS1]", "class=\"collapsed\" aria-expanded=\"false\"");
                temp = temp.replace("[SETCLASS2]", "class=\"panel-collapse collapse\" aria-expanded=\"false\"");
                elementPanelCountry.innerHTML = elementPanelCountry.innerHTML + temp;
            }
        } 
    }
}

function populateOtherPaymentByWallet(initBankListWA, elementPanelWA, isExpandPane)
{

    for (var key in initBankListWA)
    {
        var templateWalletDIV = "<div class=\"panel panel-default\"  id=\"divWA\" >"
								+ "<div class=\"panel-heading price-container\">"
								+	"<h4 class=\"panel-title\">"
								+ "<a data-toggle=\"collapse\" [SETCLASS1] data-parent=\"#payment-wallet\" href=\"#wa\" style=\"text-decoration:none;\">"
								+			"eWallet"
								+		"</a>"
								+	"</h4>"
								+"</div>"
								+ "<div id=\"wa\">"
								+	"<div class=\"panel-body\">"
                                +   "[WAList]"
								+	"</div>"
								+"</div>"
							    + "</div>"

        var banksWA = JSON.parse(JSON.stringify(initBankListWA[key]));
        var bankspropWA = JSON.parse(JSON.stringify(banksWA["banks"]));
        var strWAList = "";
        for (i = 0; i < bankspropWA.length; i++) {
            strWAList = strWAList + "<a href=\"#\" OnClick=\"VerifyDataOthersWA('" + bankspropWA[i]["issuingBank"] + "')\"><img src=\"assets3/img/" + bankspropWA[i]["imgName"] + "\" width=\"80\" alt=\"" + bankspropWA[i]["dispName"] + "\"/></a>";
        }
        templateWalletDIV = templateWalletDIV.replace("[WAList]", strWAList)
    }

    if (!isExpandPane) {
        templateWalletDIV = templateWalletDIV.replace("[SETCLASS1]", "class=\"collapsed\" aria-expanded=\"false\"");
    }
    else {
        templateWalletDIV = templateWalletDIV.replace("[SETCLASS1]", "aria-expanded=\"true\"");
    }

    elementPanelWA.innerHTML = templateWalletDIV;
}

function getCurr(cval, forex) {
    return forex[cval].curr;
}
function getPrice(cval, baseamt, forex) {
    return toFixed(baseamt * forex[cval].rate, 2).toFixed(2);
}
function toFixed(number, precision) {
    var multiplier = Math.pow(10, precision + 1),
    wholeNumber = Math.floor(number * multiplier);
    return Math.round(wholeNumber / 10) * 10 / multiplier;
}

function formatCurrency(number) {
    var n = number.split('').reverse().join("");
    var n2 = n.replace(/\d\d\d(?!$)/g, "$&,");
    return n2.split('').reverse().join('');
}