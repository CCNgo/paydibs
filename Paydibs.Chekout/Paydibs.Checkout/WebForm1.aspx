﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="WebForm1.aspx.vb" Inherits="Paydibs.Checkout.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
        </div>
        <asp:Panel ID="Panel1" runat="server" Height="130px" HorizontalAlign="Center" Width="1400px">
            <asp:Image ID="Image1" ImageUrl="~/assets/img/pdbs-logo.png" runat="server" Height="60px" Width="200px" /><br /><br />
            <asp:Label ID="Label1" runat="server" Text="Paydibs Sdn Bhd" Font-Bold="True" Font-Names="Arial Black"></asp:Label>
        </asp:Panel>
        <asp:Panel ID="Panel3" runat="server" Height="40px" HorizontalAlign="Center" Width="200px" Style="left: 50%; margin-left: 610px;">
            <asp:Table ID="Table3" runat="server" Width="200px" Height="40px">
                <asp:TableRow runat="server">
                    <asp:TableCell runat="server" Width="40px"><asp:Image ID="Image3" ImageUrl="~/assets3/img/clock_go.png" runat="server" Height="20px" Width="20px"/></asp:TableCell>
                    <asp:TableCell runat="server" Font-Names="Arial" Font-Size="9pt" ForeColor="Silver" HorizontalAlign="Left" Width="60px">Timout in</asp:TableCell>
                    <asp:TableCell runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="9pt" ForeColor="#CC3300" HorizontalAlign="Left" Width="50px">04 : 13</asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:Panel>

        <br /><br />
        <asp:Panel ID="Panel2" runat="server" Height="45px" Width="1100px" Style="left: 50%; margin-left: 150px;" BorderColor="#CCCCCC" BorderWidth="1px">
            <div style="margin: 10px 0px 10px 20px">
                <asp:Label ID="Label3" runat="server" Font-Names="Arial" Text="Payment Details" Font-Bold="True"></asp:Label>
            </div>
        </asp:Panel>
        <asp:Panel ID="Panel4" runat="server" Height="280px" Width="1100px" Style="left: 50%; margin-left: 150px;" BorderColor="#CCCCCC" BorderWidth="1px">
            <div style="margin: 10px 0px 10px 40px">
                <asp:Table ID="Table1" runat="server">
                    <asp:TableRow runat="server">
                        <asp:TableCell runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="11pt" Width="200px">Payment ID</asp:TableCell>
                        <asp:TableCell runat="server" Font-Names="Arial" Font-Size="11pt">TestUAT2018062502</asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow runat="server">
                        <asp:TableCell runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="11pt" Width="200px">Order ID</asp:TableCell>
                        <asp:TableCell runat="server" Font-Names="Arial" Font-Size="11pt">UAT 0014</asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow runat="server">
                        <asp:TableCell runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="11pt" Width="200px">Order Description</asp:TableCell>
                        <asp:TableCell runat="server" Font-Names="Arial" Font-Size="11pt">Paynet UAT Testing</asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow runat="server">
                        <asp:TableCell runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="11pt" Width="200px">Total Amount</asp:TableCell>
                        <asp:TableCell runat="server" Font-Names="Arial" Font-Size="11pt" Font-Bold="True">MYR 10.00</asp:TableCell>
                    </asp:TableRow>
                     <asp:TableRow runat="server">
                        <asp:TableCell runat="server" Height="10px"></asp:TableCell>
                        <asp:TableCell runat="server"></asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow runat="server">
                        <asp:TableCell runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="11pt" Width="200px">Pay With</asp:TableCell>
                        <asp:TableCell runat="server"><img id="logo-fpx" style="width:100px;" border="0" alt="fpx" src="assets3/img/fpx.png"></asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow runat="server">
                        <asp:TableCell runat="server"></asp:TableCell>
                        <asp:TableCell runat="server"><asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="SqlDataSource1" DataTextField="BankDesc" DataValueField="BankDesc" appenddatabounditems="True">
                            <asp:listitem text="Select Bank" value="-1" />
        </asp:DropDownList>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:OBConnectionString2 %>" SelectCommand="SELECT CASE WHEN BankStatus = 0 THEN BankDesc + '  (Offline)' ELSE BankDesc END AS [BankDesc] FROM [HostGroup] ORDER BY BankDesc ASC"></asp:SqlDataSource>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <br />
                <asp:Button ID="Button1" runat="server" Text="Proceed" BackColor="#FACC30" Font-Bold="True" Width="100px" Style="left: 50%; margin-left: 450px; margin-bottom: 10px;"/><br />
                <asp:Label ID="Label2" runat="server" Font-Names="Time" Font-size="12px" Text="By clicking the Proceed button, you agree to the " Style="left: 50%;margin-left: 335px; margin-bottom: 8px;"></asp:Label>
                <asp:HyperLink ID="HyperLink1" runat="server" Font-Size="10pt" NavigateUrl="https://www.mepsfpx.com.my/FPXMain/termsAndConditions.jsp" ToolTip="FPX Terms and Conditions" Target="_blank">FPX’s Terms and Conditions</asp:HyperLink>
            </div>
           
        </asp:Panel>
        <asp:Panel ID="Panel5" runat="server" Height="45px" Width="1100px" Style="left: 50%; margin-left: 150px;">
            <div style="margin: 10px 0px 10px 20px">
                <asp:Label ID="Label4" runat="server" Font-Names="Arial" Text="Cancel and Back to Paydibs Testing" Font-Bold="False" ForeColor="#339966" Style="left: 50%; margin-left: 380px;"></asp:Label>
            </div>
            </asp:Panel>
        <asp:Panel ID="Panel6" runat="server" Height="200px" Width="1100px" Style="left: 50%; margin-left: 150px;">
            <div style="margin: 70px 0px 10px 400px; width: 300px;">
            <asp:Table ID="Table2" runat="server" Width="300px">
                <asp:TableRow runat="server">
                    <asp:TableCell runat="server" Font-Names="Arial" ForeColor="#999999" Font-Size="10pt">Powered By :
                    </asp:TableCell>
                    <asp:TableCell runat="server"><img id="logo-header" src="assets3/img/paydibs.png?v=20180416" alt="Merchant" width="80" height="17"></asp:TableCell>
                    <asp:TableCell runat="server"><img id="imgTrustwave" style="width:50px;" border="0" alt="trustwave" src="assets3/img/trustwave.png"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
            </div>
            <div style="margin-top: 20px; width: 1100px;">
                <asp:Label ID="Label5" runat="server" Font-Names="Arial" Text="Paydibs Careline: +603 2242 0289, 9am - 6pm (Monday - Friday)&nbsp;&nbsp;&nbsp;&nbsp;Email: support@paydibs.com" Font-Bold="False" ForeColor="#999999" Style="left: 50%; margin-left: 330px;" Font-Size="X-Small"></asp:Label><br />
                <asp:Label ID="Label6" runat="server" Font-Names="Arial" Text="Copyright © 2018 Paydibs Sdn Bhd. All rights reserved" Font-Bold="False" ForeColor="#999999" Style="left: 50%; margin-left: 420px;" Font-Size="X-Small"></asp:Label>
            </div>
        </asp:Panel>
        
    </form>
</body>
</html>
