Imports LoggerII
Imports System.Security.Cryptography
Imports System.Text 'UTF8Encoding
Imports System.IO   'MemoryStream
Imports System.Web  'HttpContext for Current.Response.Redirect, Current.Server.MapPath; HttpServerUtility/HttpUtility for Server.UrlEncode
Imports Crypto
Imports BoAPrj                                  'Added 15 Dec 2010  for UOB Thai Dll to encrypt and decrypt message
Imports com.wiz.enets2.transaction.umapi        'Added 20 Dec 2010  for ENETS
Imports com.wiz.enets2.transaction.umapi.data   'Added 20 Dec 2010  for ENETS
Imports JavaCrypto                              'Added  4 Dec 2013 Alliance CC
'Imports DDGWLib                                'Added 22 Jun 2011  for BCA2 (KlikPay)
Imports System.Globalization
Imports System.Numerics
Imports System.Security.Cryptography.X509Certificates
Imports CryptokiNet


Public Class Hash

    Private objLoggerII As LoggerII.CLoggerII
    Private objCommon As Common                 'Added, 11 Apr 2014
    Private Server As System.Web.HttpServerUtility = System.Web.HttpContext.Current.Server 'Server.UrlEncode, Server.MapPath; If HttpUtility just Server.UrlEncode

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	bCheckReqHash
    ' Function Type:	Boolean. Return False if hash values not matched
    ' Parameter:		st_PayInfo
    ' Description:		Validate hash values
    ' History:			20 Oct 2008
    '********************************************************************************************
    Public Function bCheckReqHash(ByRef st_PayInfo As Common.STPayInfo) As Boolean
        Dim bReturn As Boolean = False
        Dim szHash As String = ""

        szHash = szGetReqHash(st_PayInfo)

        'Added case-insensitive hash value comparison, 12 May 2009
        If (st_PayInfo.szHashValue <> "" And st_PayInfo.szHashValue.ToLower() = szHash.ToLower()) Then
            bReturn = True
        Else
            st_PayInfo.szErrDesc = "bCheckReqHash() error: Request hash value mismatched. Request(" + st_PayInfo.szHashValue + ") Gateway(" + szHash + ")"
            st_PayInfo.szTxnMsg = "Hash value mismatched."
        End If

        If (False = bReturn) Then
            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INVALID_INPUT
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
        End If

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szGetReqHash
    ' Function Type:	String. Pay request hash value
    ' Parameter:		st_PayInfo
    ' Description:		Get Pay request's hash value
    ' History:			20 Oct 2008
    '********************************************************************************************
    Public Function szGetReqHash(ByVal st_PayInfo As Common.STPayInfo) As String
        Dim szHashValue As String
        szHashValue = ""

        szHashValue = st_PayInfo.szMerchantPassword
        szHashValue = szHashValue + st_PayInfo.szHashKey

        If (st_PayInfo.szHashMethod.ToUpper() = "SHA1") Then
            szHashValue = szComputeSHA1Hash(szHashValue)
        ElseIf (st_PayInfo.szHashMethod.ToUpper() = "SHA1_2") Then  'Added -711, 24 May 2016
            szHashValue = szComputeSHA1Hash_2(szHashValue)
        ElseIf (st_PayInfo.szHashMethod.ToUpper() = "SHA2") Then    'Added SHA256 on 26 May 2009
            szHashValue = szComputeSHA256Hash(szHashValue)
        ElseIf (st_PayInfo.szHashMethod.ToUpper() = "MD5") Then
            szHashValue = szComputeMD5Hash(szHashValue, "O")
        ElseIf (st_PayInfo.szHashMethod.ToUpper() = "SHA512") Then
            szHashValue = szComputeSHA512Hash(szHashValue)
        Else
            szHashValue = ""
            'Log error 
        End If

        Return szHashValue
    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szGetSaleResHash
    ' Function Type:	String. Pay response hash value
    ' Parameter:		st_PayInfo
    ' Description:		Get Pay response's hash value
    ' History:			17 Nov 2008
    '********************************************************************************************
    Public Function szGetSaleResHash(ByVal st_PayInfo As Common.STPayInfo) As String
        Dim szHashValue As String = ""
        Dim szTxnAmount As String = ""      'Added, 2 Sept 2014, for FX
        Dim szCurrencyCode As String = ""   'Added, 2 Sept 2014, for FX

        szHashValue = ""

        'Added, 2 Sept 2014, checking of FXCurrency and BaseTxnAmount
        If ("" = st_PayInfo.szFXCurrencyCode) Then
            szTxnAmount = st_PayInfo.szTxnAmount
            szCurrencyCode = st_PayInfo.szCurrencyCode
        Else
            szTxnAmount = st_PayInfo.szBaseTxnAmount            'Hashing TxnAmount is the original amount
            szCurrencyCode = st_PayInfo.szBaseCurrencyCode      'If FX, st_PayInfo.szTxnAmount is storing FXTxnAmount assigned in bVerifyResTxn()
        End If

        szHashValue = st_PayInfo.szMerchantPassword
        szHashValue = szHashValue + st_PayInfo.szTxnID
        szHashValue = szHashValue + st_PayInfo.szMerchantID
        szHashValue = szHashValue + st_PayInfo.szMerchantTxnID
        szHashValue = szHashValue + st_PayInfo.iMerchantTxnStatus.ToString()
        'Modified 2 Sept 2014, use szTxnAmount and szCurrencyCode instead of st_PayInfo.szTxnAmount
        szHashValue = szHashValue + szTxnAmount
        szHashValue = szHashValue + szCurrencyCode
        'szHashValue = szHashValue + st_PayInfo.szBaseTxnAmount      'Added on 8 Aug 2009, to cater for base currency booking confirmation enhancement
        'szHashValue = szHashValue + st_PayInfo.szBaseCurrencyCode   'Added on 8 Aug 2009, to cater for base currency booking confirmation enhancement
        szHashValue = szHashValue + st_PayInfo.szAuthCode

        'Added on 10 Mar 2010
        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                        st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > (" + st_PayInfo.szHashMethod + ") (Merchant Password + " + Right(szHashValue, szHashValue.Length() - st_PayInfo.szMerchantPassword.Length()) + ")")

        If (st_PayInfo.szHashMethod.ToUpper() = "SHA1") Then
            szHashValue = szComputeSHA1Hash(szHashValue)
        ElseIf (st_PayInfo.szHashMethod.ToUpper() = "SHA1_2") Then  'Added -711, 24 May 2016
            szHashValue = szComputeSHA1Hash_2(szHashValue)
        ElseIf (st_PayInfo.szHashMethod.ToUpper() = "SHA2" Or st_PayInfo.szHashMethod.ToUpper() = "SHA256") Then    'Added SHA256 on 26 May 2009; Modified 19 Apr 2015, added checking of "SHA256", for OTC
            szHashValue = szComputeSHA256Hash(szHashValue)
        ElseIf (st_PayInfo.szHashMethod.ToUpper() = "MD5") Then
            szHashValue = szComputeMD5Hash(szHashValue, "O")
        ElseIf (st_PayInfo.szHashMethod.ToUpper() = "SHA512") Then
            szHashValue = szComputeSHA512Hash(szHashValue)
        Else
            szHashValue = ""
            'Log error 
        End If

        Return szHashValue
    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szGetSaleResHash2
    ' Function Type:	String. Pay response hash value
    ' Parameter:		st_PayInfo
    ' Description:		Get Pay response's hash value
    ' History:			28 Apr 2016
    '********************************************************************************************
    Public Function szGetSaleResHash2(ByVal st_PayInfo As Common.STPayInfo) As String
        Dim szHashValue As String = ""
        Dim szTxnAmount As String = ""      'Added, 2 Sept 2014, for FX
        Dim szCurrencyCode As String = ""   'Added, 2 Sept 2014, for FX

        szHashValue = ""

        'Added, 2 Sept 2014, checking of FXCurrency and BaseTxnAmount
        If ("" = st_PayInfo.szFXCurrencyCode) Then
            szTxnAmount = st_PayInfo.szTxnAmount
            szCurrencyCode = st_PayInfo.szCurrencyCode
        Else
            szTxnAmount = st_PayInfo.szBaseTxnAmount            'Hashing TxnAmount is the original amount
            szCurrencyCode = st_PayInfo.szBaseCurrencyCode      'If FX, st_PayInfo.szTxnAmount is storing FXTxnAmount assigned in bVerifyResTxn()
        End If

        szHashValue = st_PayInfo.szMerchantPassword
        szHashValue = szHashValue + st_PayInfo.szMerchantID
        szHashValue = szHashValue + st_PayInfo.szMerchantTxnID
        szHashValue = szHashValue + st_PayInfo.szTxnID
        szHashValue = szHashValue + st_PayInfo.szMerchantOrdID
        'Modified 2 Sept 2014, use szTxnAmount and szCurrencyCode instead of st_PayInfo.szTxnAmount
        szHashValue = szHashValue + szTxnAmount
        szHashValue = szHashValue + szCurrencyCode
        szHashValue = szHashValue + st_PayInfo.iMerchantTxnStatus.ToString()
        'szHashValue = szHashValue + st_PayInfo.szBaseTxnAmount      'Added on 8 Aug 2009, to cater for base currency booking confirmation enhancement
        'szHashValue = szHashValue + st_PayInfo.szBaseCurrencyCode   'Added on 8 Aug 2009, to cater for base currency booking confirmation enhancement
        szHashValue = szHashValue + st_PayInfo.szAuthCode

        szHashValue = szHashValue + st_PayInfo.szParam6             'Added, 28 Apr 2016
        szHashValue = szHashValue + st_PayInfo.szParam7             'Added, 2 May 2016

        'Added on 10 Mar 2010
        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                        st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > (" + st_PayInfo.szHashMethod + ") (Merchant Password + " + Right(szHashValue, szHashValue.Length() - st_PayInfo.szMerchantPassword.Length()) + ")")

        If (st_PayInfo.szHashMethod.ToUpper() = "SHA1") Then
            szHashValue = szComputeSHA1Hash(szHashValue)
        ElseIf (st_PayInfo.szHashMethod.ToUpper() = "SHA2" Or st_PayInfo.szHashMethod.ToUpper() = "SHA256") Then    'Added SHA256 on 26 May 2009; Modified 19 Apr 2015, added checking of "SHA256", for OTC
            szHashValue = szComputeSHA256Hash(szHashValue)
        ElseIf (st_PayInfo.szHashMethod.ToUpper() = "MD5") Then
            szHashValue = szComputeMD5Hash(szHashValue, "O")
        ElseIf (st_PayInfo.szHashMethod.ToUpper() = "SHA512") Then
            szHashValue = szComputeSHA512Hash(szHashValue)
        Else
            szHashValue = ""
            'Log error 
        End If
        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "TRACE-hash-" + szHashValue)
        Return szHashValue
    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szGetSettleResHash
    ' Function Type:	String. Settle response hash value
    ' Parameter:		st_PayInfo
    ' Description:		Get Settle response's hash value
    ' History:			7 May 2016
    '********************************************************************************************
    Public Function szGetSettleResHash(ByVal st_PayInfo As Common.STPayInfo) As String
        Dim szHashValue As String = ""

        szHashValue = ""

        szHashValue = st_PayInfo.szMerchantPassword
        szHashValue = szHashValue + st_PayInfo.szMerchantID
        szHashValue = szHashValue + st_PayInfo.szSettleTAID
        szHashValue = szHashValue + st_PayInfo.szSettleAmount
        szHashValue = szHashValue + st_PayInfo.szSettleCount
        szHashValue = szHashValue + st_PayInfo.iMerchantTxnStatus.ToString()

        'Added on 10 Mar 2010
        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                        st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > (" + st_PayInfo.szHashMethod + ") (Merchant Password + " + Right(szHashValue, szHashValue.Length() - st_PayInfo.szMerchantPassword.Length()) + ")")

        If (st_PayInfo.szHashMethod.ToUpper() = "SHA1") Then
            szHashValue = szComputeSHA1Hash(szHashValue)
        ElseIf (st_PayInfo.szHashMethod.ToUpper() = "SHA2" Or st_PayInfo.szHashMethod.ToUpper() = "SHA256") Then    'Added SHA256 on 26 May 2009; Modified 19 Apr 2015, added checking of "SHA256", for OTC
            szHashValue = szComputeSHA256Hash(szHashValue)
        ElseIf (st_PayInfo.szHashMethod.ToUpper() = "MD5") Then
            szHashValue = szComputeMD5Hash(szHashValue, "O")
        ElseIf (st_PayInfo.szHashMethod.ToUpper() = "SHA512") Then
            szHashValue = szComputeSHA512Hash(szHashValue)
        Else
            szHashValue = ""
            'Log error 
        End If

        Return szHashValue
    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	bTokenReqSOPHashMatch
    ' Function Type:	Boolean
    ' Parameter:		st_PayInfo
    ' Description:		Check if the hash matches for the token request
    ' History:			Added , 24 May 2017, SOP
    '********************************************************************************************
    Public Function bTokenReqSOPHashMatch(ByVal st_PayInfo As Common.STPayInfo) As String
        Dim bMatch As Boolean = False
        Dim szHashValue As String = ""
        Dim szClearString As String = ""

        szClearString = st_PayInfo.szMerchantPassword
        szClearString = szClearString + st_PayInfo.szMerchantID
        szClearString = szClearString + st_PayInfo.szMerchantTxnID
        szClearString = szClearString + st_PayInfo.szCustIP

        If (st_PayInfo.szHashMethod.ToUpper() = "SHA1") Then
            szHashValue = szComputeSHA1Hash(szClearString)
        ElseIf (st_PayInfo.szHashMethod.ToUpper() = "SHA1_2") Then
            szHashValue = szComputeSHA1Hash_2(szClearString)
        ElseIf (st_PayInfo.szHashMethod.ToUpper() = "SHA2") Then    'SHA256
            szHashValue = szComputeSHA256Hash(szClearString)
        ElseIf (st_PayInfo.szHashMethod.ToUpper() = "MD5") Then
            szHashValue = szComputeMD5Hash(szClearString, "O")
        ElseIf (st_PayInfo.szHashMethod.ToUpper() = "SHA512") Then
            szHashValue = szComputeSHA512Hash(szClearString)
        Else
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                        st_PayInfo.szMerchantID + "bTokenReqSOPHashMatch() > Invalid HashMethod:" + st_PayInfo.szHashMethod.ToUpper())
        End If

        If (szHashValue.ToLower() = st_PayInfo.szHashValue.ToLower()) Then
            bMatch = True
        Else
            ' Hash Mismatched
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                        st_PayInfo.szMerchantID + "bTokenReqSOPHashMatch() Hash Mismatched (Gateway: " + szHashValue.ToLower() + " <> Recieved: " + st_PayInfo.szHashValue.ToLower() + ")")
        End If

        Return bMatch
    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szGetHash
    ' Function Type:	String. Gets hash value
    ' Parameter:		st_PayInfo
    ' Description:		Get hash value
    ' History:			22 Dec 2010 Added st_HostInfo input param 
    '********************************************************************************************
    Public Function szGetHash(ByVal sz_HashKey As String, ByVal sz_HashMethod As String, ByVal st_PayInfo As Common.STPayInfo, ByVal st_HostInfo As Common.STHost) As String
        Dim szHashValue As String
        szHashValue = ""

        If (sz_HashMethod.ToUpper() = "SHA1") Then
            szHashValue = szComputeSHA1Hash(sz_HashKey)
        ElseIf (sz_HashMethod.ToUpper() = "SHA1_2") Then                'Added -711, 24 May 2016
            szHashValue = szComputeSHA1Hash_2(sz_HashKey)
        ElseIf (sz_HashMethod.ToUpper() = "SHA1_3") Then            'Added  MCash
            szHashValue = szComputeSHA1Hash_3(sz_HashKey)
        ElseIf (sz_HashMethod.ToUpper() = "SHA1HEX") Then               'Added  06 Jan 2011, for DragonPay
            szHashValue = szComputeSHA1HexHash(sz_HashKey, "O")
        ElseIf (sz_HashMethod.ToUpper() = "SHA1HEXL") Then              'Added  06 Jan 2011, for DragonPay
            szHashValue = szComputeSHA1HexHash(sz_HashKey, "L")
        ElseIf (sz_HashMethod.ToUpper() = "SHA1HEXU") Then              'Added  06 Jan 2011, for DragonPay
            szHashValue = szComputeSHA1HexHash(sz_HashKey, "U")
        ElseIf (sz_HashMethod.ToUpper() = "HMACSHA1") Then              'Added  06 Jan 2011, for Barclay
            szHashValue = szComputeHMACSHA1Hash(sz_HashKey, st_HostInfo, "O")
        ElseIf (sz_HashMethod.ToUpper() = "HMACSHA1L") Then             'Added  06 Jan 2011, for Barclay
            szHashValue = szComputeHMACSHA1Hash(sz_HashKey, st_HostInfo, "L")
        ElseIf (sz_HashMethod.ToUpper() = "HMACSHA1U") Then             'Added  06 Jan 2011, for Barclay
            szHashValue = szComputeHMACSHA1Hash(sz_HashKey, st_HostInfo, "U")
        ElseIf (sz_HashMethod.ToUpper() = "HMACSHA1LH") Then              'Added by HS on 12 April 2019, for EXPAY
            szHashValue = szComputeHMACSHA1Hash(sz_HashKey, st_HostInfo, "L", "H")
        ElseIf (sz_HashMethod.ToUpper() = "HMACSHA1HEX") Then           'Added  06 Jan 2011, for Barclay
            szHashValue = szComputeHMACSHA1HEXHash(sz_HashKey, st_HostInfo, "O")
        ElseIf (sz_HashMethod.ToUpper() = "HMACSHA1HEXL") Then          'Added  06 Jan 2011, for Barclay
            szHashValue = szComputeHMACSHA1HEXHash(sz_HashKey, st_HostInfo, "L")
        ElseIf (sz_HashMethod.ToUpper() = "HMACSHA1HEXU") Then          'Added  06 Jan 2011, for Barclay
            szHashValue = szComputeHMACSHA1HEXHash(sz_HashKey, st_HostInfo, "U")
        ElseIf (sz_HashMethod.ToUpper() = "MD5") Then                   'MD5 in base64
            szHashValue = szComputeMD5Hash(sz_HashKey, "O")
        ElseIf (sz_HashMethod.ToUpper() = "MD5U") Then                  'MD5 in base64 - Upper Case
            szHashValue = szComputeMD5Hash(sz_HashKey, "U")
        ElseIf (sz_HashMethod.ToUpper() = "MD5L") Then                  'MD5 in base64 - Lower Case
            szHashValue = szComputeMD5Hash(sz_HashKey, "L")
        ElseIf (sz_HashMethod.ToUpper() = "MD5HEX") Then
            szHashValue = szComputeMD5HexHash(sz_HashKey, "O")          'MD5 in HEX - Modified on 25 Oct 2010, support case
        ElseIf (sz_HashMethod.ToUpper() = "MD5HEXU") Then
            szHashValue = szComputeMD5HexHash(sz_HashKey, "U")          'MD5 in HEX - Upper Case
        ElseIf (sz_HashMethod.ToUpper() = "MD5HEXL") Then
            szHashValue = szComputeMD5HexHash(sz_HashKey, "L")          'MD5 in HEX - Lower Case
        ElseIf (sz_HashMethod.ToUpper() = "SHA160") Then                'SHA1 160bit in Hex
            szHashValue = szComputeSHA160Hash(sz_HashKey)
        ElseIf (sz_HashMethod.ToUpper() = "SHA256") Then
            szHashValue = szComputeSHA256Hash(sz_HashKey)
        ElseIf (sz_HashMethod.ToUpper() = "SHA1BC") Then                'BitConverter SHA1 in .NET default value - Upper Case
            szHashValue = szComputeSHA1BCHash(sz_HashKey, "U")
        ElseIf (sz_HashMethod.ToUpper() = "SHA1BCL") Then
            szHashValue = szComputeSHA1BCHash(sz_HashKey, "L")          'BitConverter SHA1 in lower case
        ElseIf (sz_HashMethod.ToUpper() = "ADLER32") Then               'Adler-32 checksum
            szHashValue = szComputeAdler32Hash(sz_HashKey)              'Added on 17 Dec 2009
        ElseIf (sz_HashMethod.ToUpper() = "RHB") Then
            szHashValue = szComputeRHBHash(sz_HashKey)
        ElseIf (sz_HashMethod.ToUpper() = "RHB2") Then
            szHashValue = szComputeRHB2Hash(sz_HashKey, st_HostInfo)     'Modify , 05 Aug 2016, add to cater new hashing method V1.5
        ElseIf (sz_HashMethod.ToUpper() = "SCB") Then
            szHashValue = szComputeSCBHash(sz_HashKey, st_PayInfo)      'Added st_PayInfo on 17 June 2009
        ElseIf (sz_HashMethod.ToUpper() = "ALIPAY") Then
            szHashValue = szComputeAliPayHash(sz_HashKey, st_PayInfo)   'Added on 21 Aug 2009
        ElseIf (sz_HashMethod.ToUpper() = "CHINAPAY") Then
            szHashValue = szComputeChinaPayHash(sz_HashKey, st_PayInfo) 'Added on 25 Oct 2009
        ElseIf (sz_HashMethod.ToUpper() = "99BILL") Then
            szHashValue = szCompute99BillHash(sz_HashKey, st_PayInfo)   'Added on 25 Oct 2010
            'ElseIf (sz_HashMethod.ToUpper() = "CUP") Then
            '    szHashValue = szComputeCUPHash(sz_HashKey, st_PayInfo, st_HostInfo) 'Added on 11 Nov 2010 
        ElseIf (sz_HashMethod.ToUpper() = "UOBTHAI") Then               'Added on 14 Dec 2010 
            szHashValue = szComputeUOBThaiHash(sz_HashKey, st_HostInfo.szSecretKey)
        ElseIf (sz_HashMethod.ToUpper() = "ENETS") Then         'Added on 17 Dec 2010 
            szHashValue = szComputeEnetsHash(sz_HashKey, st_HostInfo.szMesgTemplate, st_PayInfo)
        ElseIf (sz_HashMethod.ToUpper() = "MD5HEXENC") Then
            szHashValue = szComputeMD5HexEnc(sz_HashKey, "O")   'MD5 in HEX + EncodeBase64 - Added on 15 Mar 2011  for BBL
        ElseIf (sz_HashMethod.ToUpper() = "MD5HEXUENC") Then
            szHashValue = szComputeMD5HexEnc(sz_HashKey, "U")   'MD5 in HEX UpperCase + EncodeBase64 - Added on 15 Mar 2011  for BBL
        ElseIf (sz_HashMethod.ToUpper() = "MD5HEXLENC") Then
            szHashValue = szComputeMD5HexEnc(sz_HashKey, "L")   'MD5 in HEX LowerCase + EncodeBase64 - Added on 15 Mar 2011  for BBL
        ElseIf (sz_HashMethod.ToUpper() = "EPBEMD5DES") Then     'Added  4 Dec 2013, for Alliance bank CC
            szHashValue = szEncryptPBEMD5Des(sz_HashKey, st_PayInfo, st_HostInfo)
        ElseIf (sz_HashMethod.ToUpper() = "DPBEMD5DES") Then    'Added  4 Dec 2013, for Alliance bank CC
            szHashValue = szDecryptPBEMD5Des(sz_HashKey, st_PayInfo, st_HostInfo)
        ElseIf (sz_HashMethod.ToUpper() = "ALBEAES") Then       'Added  15 Apr 2014, for Alliance CC
            szHashValue = szALBEncryptAES(sz_HashKey, st_PayInfo, st_HostInfo)
        ElseIf (sz_HashMethod.ToUpper() = "ALBDAES") Then       'Added  15 Apr 2014, for Alliance CC
            szHashValue = szALBDecryptAES(sz_HashKey, st_PayInfo, st_HostInfo)
        ElseIf (sz_HashMethod.ToUpper() = "HMSHA256") Then      'Added  1 July 2014, GlobalPay Cybersource
            szHashValue = szComputeHMACSHA256Hash(sz_HashKey, st_HostInfo, "O")
        ElseIf (sz_HashMethod.ToUpper() = "HMSHA256L") Then     'Added  1 July 2014, GlobalPay Cybersource
            szHashValue = szComputeHMACSHA256Hash(sz_HashKey, st_HostInfo, "L")
        ElseIf (sz_HashMethod.ToUpper() = "HMSHA256U") Then     'Added  1 July 2014, GlobalPay Cybersource
            szHashValue = szComputeHMACSHA256Hash(sz_HashKey, st_HostInfo, "U")

        ElseIf (sz_HashMethod.ToUpper() = "HMSHA256HOL") Then     'Added  1 July 2014, GlobalPay Cybersource
            szHashValue = szComputeHMACSHA256HEXHashKeyCase(sz_HashKey, st_HostInfo, "O", "L")

        ElseIf (sz_HashMethod.ToUpper() = "HMSHA256HL") Then    'Added  1 July 2014, GlobalPay Cybersource
            szHashValue = szComputeHMACSHA256HEXHash(sz_HashKey, st_HostInfo, "L")
        ElseIf (sz_HashMethod.ToUpper() = "HMSHA256HU") Then    'Added  1 July 2014, GlobalPay Cybersource
            szHashValue = szComputeHMACSHA256HEXHash(sz_HashKey, st_HostInfo, "U")
        ElseIf (sz_HashMethod.ToUpper() = "GPAY") Then          'Added  7 July 2014, GlobalPay Cybersource
            szHashValue = szComputeGPAYHash(sz_HashKey, st_HostInfo, "O")
        ElseIf (sz_HashMethod.ToUpper() = "MBBEAES") Then       'Added  1 Dec 2014, for Maybank2U-FFM2U
            szHashValue = szMBBEncryptAES(sz_HashKey, st_PayInfo, st_HostInfo)
        ElseIf (sz_HashMethod.ToUpper() = "SHA512") Then        'Added  5 March 2015, eBPG Maybank-CC
            szHashValue = szComputeSHA512Hash(sz_HashKey)
        ElseIf (sz_HashMethod.ToUpper() = "SHA512U") Then       'Added  24 Aug 2015, eBPG Maybank-CC
            szHashValue = szComputeSHA512UHash(sz_HashKey)      'sz_HashKey and szHashValue is uppercase - REFUND
        ElseIf (sz_HashMethod.ToUpper = "FPXSIGN") Then         'Added  5 May 2015, FPXD
            'Modified 1 Mar 2017, added checking of szMesgTemplate to cater for FPX2DBD to set exchange key path on terminal instead of host table
            If (st_HostInfo.szMesgTemplate <> "") Then
                szHashValue = szComputeFPXCheckSum(sz_HashKey, st_HostInfo.szMesgTemplate)
            Else
                szHashValue = szComputeFPXCheckSum(sz_HashKey, st_PayInfo.szEncryptKeyPath) 'cater for FPX2DBD exchange key path stored in terminal table
            End If
        ElseIf (sz_HashMethod.ToUpper = "FPXVERIFY") Then
            'Modified 1 Mar 2017, added checking of szSecretKey to cater for FPX2DBD to set FPX cert path on terminal instead of host table
            If (st_HostInfo.szSecretKey <> "") Then
                szHashValue = szVerifyFPXCheckSum(sz_HashKey, st_HostInfo.szHashValue, st_HostInfo.szSecretKey)
            Else
                szHashValue = szVerifyFPXCheckSum(sz_HashKey, st_HostInfo.szHashValue, st_PayInfo.szDecryptKeyPath) 'cater for FPX2DBD FPX cert path stored in terminal table
            End If
        ElseIf (sz_HashMethod.ToUpper() = "SHA2B64") Then       'Added  19 Aug 2015, PBB
            szHashValue = szComputeSHA2B64Hash(sz_HashKey)
        ElseIf (sz_HashMethod.ToUpper() = "SHA2B64U") Then      'Added  19 Aug 2015, PBB. Modified  added upper case
            szHashValue = szComputeSHA2B64Hash(sz_HashKey, "U")
            'ElseIf (sz_HashMethod.ToUpper() = "BCAKP") Then         'Added on 22 Jun 2011, for BCA2 (KlikPay), 
            '    szHashValue = szComputeBCAKPHash(sz_HashKey)
        ElseIf (sz_HashMethod.ToUpper() = "GPUPOPSIGN") Then
            szHashValue = szComputeGPUPOPSignature(st_HostInfo.szMesgTemplate, st_HostInfo.szSendKey, sz_HashKey)
        ElseIf (sz_HashMethod.ToUpper() = "GPUPOPVALIDATE") Then
            szHashValue = szComputeGPUPOPValidate(st_HostInfo.szSecretKey, st_HostInfo.szCertId, sz_HashKey, st_HostInfo.szHashValue)
        ElseIf (sz_HashMethod.ToUpper() = "TBANK") Then
            szHashValue = szEncryptDecryptTBank(sz_HashKey, st_PayInfo)      'Added 07 April 2017, TBank
        ElseIf (sz_HashMethod.Substring(0, 5) = "BOOST") Then
            szHashValue = szComputeBoostHash(sz_HashKey, st_PayInfo, st_HostInfo)    'Added  16 Nov 2017, Boost, Triple DES encryption for Boost    
        Else
            szHashValue = ""
            'Log error 
        End If

        Return szHashValue
    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szWriteHex
    ' Function Type:	String. Hex string value converted from byte array.
    ' Parameter:		bt_Array. Byte values to be converted to hex.
    ' Description:		Get hex string value converted from byte array.
    ' History:			04 March 2009
    '********************************************************************************************
    Public Function szWriteHex(ByVal bt_Array() As Byte) As String
        Dim szHex As String = ""
        Dim iCounter As Integer

        For iCounter = 0 To (bt_Array.Length() - 1)
            szHex += bt_Array(iCounter).ToString("X2")
        Next

        Return szHex
    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szDecode
    ' Function Type:	String. Decoded string.
    ' Parameter:		sz_Data. Encoded string to be decoded.
    ' Description:		Get Base64 decoded string.
    ' History:			15 Sept 2009
    '********************************************************************************************
    Function szDecode(ByVal sz_Data As String) As String
        Dim bt() As Byte
        Dim szOutput As String

        bt = System.Convert.FromBase64String(sz_Data)
        szOutput = System.Text.Encoding.ASCII.GetString(bt)

        Return szOutput
    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szEncode
    ' Function Type:	String. Encoded string.
    ' Parameter:		sz_Data. String to be encoded.
    ' Description:		Get Base64 encoded string.
    ' History:			15 Sept 2009
    '********************************************************************************************
    Function szEncode(ByVal sz_Data As String) As String
        Dim bt() As Byte
        Dim szOutput As String

        bt = System.Text.Encoding.ASCII.GetBytes(sz_Data.ToCharArray)
        szOutput = System.Convert.ToBase64String(bt)

        Return szOutput
    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szComputeSHA1Hash
    ' Function Type:	String. Hash value generated using SHA-1 algorithm.
    ' Parameter:		sz_Key. Values to be hash.
    ' Description:		Get hash value generated using SHA-1 algorithm
    ' History:			20 Oct 2008
    '********************************************************************************************
    Public Function szComputeSHA1Hash(ByVal sz_Key As String) As String
        Dim szHashValue As String = ""
        Dim objSHA1 As SHA1CryptoServiceProvider = Nothing
        Dim btHashbuffer() As Byte = Nothing

        Try
            'UTF-8 (8-bit UCS/Unicode Transformation Format) is a variable-length character encoding for Unicode.
            'It is able to represent any character in the Unicode standard, yet the initial encoding of byte codes and
            'character assignments for UTF-8 is backwards compatible with ASCII.
            'For these reasons, it is steadily becoming the preferred encoding for e-mail, web pages, and other places
            'where characters are stored or streamed.
            'String.ToCharArray - Copies the characters in this instance to a Unicode character array.
            'System.Text.Encoding.UTF8 - This class encodes Unicode characters using UCS Transformation Format, 8-bit form (UTF-8).
            '.GetBytes - This method encodes characters into an array of bytes.
            'System.Convert.ToBase64String - Converts the value of an array of 8-bit unsigned integers to its equivalent
            'String representation consisting of base 64 digits.

            objSHA1 = New SHA1CryptoServiceProvider
            objSHA1.ComputeHash(System.Text.Encoding.UTF8.GetBytes(sz_Key.ToCharArray))
            btHashbuffer = objSHA1.Hash
            szHashValue = System.Convert.ToBase64String(btHashbuffer)
        Catch ex As Exception
            'oLogger.Log(Logger.CLogger.LOG_SEVERITY.WARNING, __FILE__, __METHOD__, __LINE__, "Err: " + ex.ToString)
            Throw ex
        Finally
            If Not objSHA1 Is Nothing Then objSHA1 = Nothing
        End Try

        Return szHashValue
    End Function

    'Added 711, 29 Mar 2016
    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szComputeSHA1Hash_2
    ' Function Type:	String. Hash value generated using SHA-1 algorithm.
    ' Parameter:		sz_Key. Values to be hash.
    ' Description:		Get hash value generated using SHA-1 algorithm
    ' History:			24 May 2016
    '********************************************************************************************
    Public Function szComputeSHA1Hash_2(ByVal sz_Key As String) As String
        Dim szHashValue As String = ""
        Dim objSHA1 As SHA1CryptoServiceProvider = Nothing
        Dim btHashbuffer() As Byte = Nothing

        Try
            objSHA1 = New SHA1CryptoServiceProvider
            btHashbuffer = objSHA1.ComputeHash(System.Text.Encoding.ASCII.GetBytes(sz_Key))

            For Each b As Byte In btHashbuffer
                szHashValue += b.ToString("x2")
            Next

        Catch ex As Exception
            Throw ex
        Finally
            If Not objSHA1 Is Nothing Then objSHA1 = Nothing
        End Try

        Return szHashValue
    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szComputeSHA1Hash_3
    ' Function Type:	String. Hash value generated using SHA-1 algorithm.
    ' Parameter:		sz_Key. Values to be hash.
    ' Description:		Get hash value generated using SHA-1 algorithm. replace + sign become space character
    ' History:			14 June 2017
    '********************************************************************************************
    Public Function szComputeSHA1Hash_3(ByVal sz_Key As String) As String   'Added  MCash
        Dim szHashValue As String = ""
        Dim objSHA1 As SHA1CryptoServiceProvider = Nothing
        Dim btHashbuffer() As Byte = Nothing

        Try
            'UTF-8 (8-bit UCS/Unicode Transformation Format) is a variable-length character encoding for Unicode.
            'It is able to represent any character in the Unicode standard, yet the initial encoding of byte codes and
            'character assignments for UTF-8 is backwards compatible with ASCII.
            'For these reasons, it is steadily becoming the preferred encoding for e-mail, web pages, and other places
            'where characters are stored or streamed.
            'String.ToCharArray - Copies the characters in this instance to a Unicode character array.
            'System.Text.Encoding.UTF8 - This class encodes Unicode characters using UCS Transformation Format, 8-bit form (UTF-8).
            '.GetBytes - This method encodes characters into an array of bytes.
            'System.Convert.ToBase64String - Converts the value of an array of 8-bit unsigned integers to its equivalent
            'String representation consisting of base 64 digits.

            objSHA1 = New SHA1CryptoServiceProvider
            objSHA1.ComputeHash(System.Text.Encoding.UTF8.GetBytes(sz_Key.ToCharArray))
            btHashbuffer = objSHA1.Hash
            szHashValue = Server.UrlEncode(System.Convert.ToBase64String(btHashbuffer)) '.Replace("+", " ")

        Catch ex As Exception
            'oLogger.Log(Logger.CLogger.LOG_SEVERITY.WARNING, __FILE__, __METHOD__, __LINE__, "Err: " + ex.ToString)
            Throw ex
        Finally
            If Not objSHA1 Is Nothing Then objSHA1 = Nothing
        End Try

        Return szHashValue
    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szComputeSHA1HashHEX
    ' Function Type:	String. Hash value generated using SHA-1 algorithm with HEX.
    ' Parameter:		sz_Key. Values to be hash.
    ' Description:		Get hash value generated using SHA-1 algorithm with HEX
    ' History:			Added  for DragonPay on 20 Oct 2011
    '********************************************************************************************
    Public Function szComputeSHA1HexHash(ByVal sz_Key As String, ByVal sz_Case As String) As String
        Dim szHashValue As String = ""
        Dim objSHA1 As SHA1CryptoServiceProvider = Nothing
        Dim btHashbuffer() As Byte = Nothing

        Try
            'UTF-8 (8-bit UCS/Unicode Transformation Format) is a variable-length character encoding for Unicode.
            'It is able to represent any character in the Unicode standard, yet the initial encoding of byte codes and
            'character assignments for UTF-8 is backwards compatible with ASCII.
            'For these reasons, it is steadily becoming the preferred encoding for e-mail, web pages, and other places
            'where characters are stored or streamed.
            'String.ToCharArray - Copies the characters in this instance to a Unicode character array.
            'System.Text.Encoding.UTF8 - This class encodes Unicode characters using UCS Transformation Format, 8-bit form (UTF-8).
            '.GetBytes - This method encodes characters into an array of bytes.
            'System.Convert.ToBase64String - Converts the value of an array of 8-bit unsigned integers to its equivalent
            'String representation consisting of base 64 digits.

            objSHA1 = New SHA1CryptoServiceProvider
            objSHA1.ComputeHash(System.Text.Encoding.UTF8.GetBytes(sz_Key.ToCharArray))
            btHashbuffer = objSHA1.Hash

            szHashValue = szBytesToHex(btHashbuffer)
            If ("L" = sz_Case.ToUpper()) Then
                szHashValue = szHashValue.ToLower()
            ElseIf ("U" = sz_Case.ToUpper()) Then
                szHashValue = szHashValue.ToUpper()
            End If

        Catch ex As Exception
            Throw ex
        Finally
            If Not objSHA1 Is Nothing Then objSHA1 = Nothing
        End Try

        Return szHashValue
    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szComputeHMACSHA1Hash
    ' Function Type:	String. Hash value generated using HMAC SHA-1 algorithm.
    ' Parameter:		sz_Key. Values to be hash.
    ' Description:		Get hash value generated using HMAC SHA-1 algorithm
    ' History:			Added  for Barclay on 20 Oct 2011
    '********************************************************************************************
    Public Function szComputeHMACSHA1Hash(ByVal sz_Key As String, ByVal st_HostInfo As Common.STHost, ByVal sz_Case As String, Optional ByVal sz_Convert As String = "S") As String
        Dim szHashValue As String = ""
        Dim szSecretKey As String = ""
        Dim encoding As New System.Text.UTF8Encoding

        Try
            szSecretKey = szDecrypt3DES(st_HostInfo.szSecretKey, Common.C_3DESAPPKEY, Common.C_3DESAPPVECTOR)

            ' Calculate the HMAC
            Dim objHMACSHA1 As New HMACSHA1(encoding.GetBytes(szSecretKey))
            If ("H" = sz_Convert) Then
                szHashValue = szWriteHex(objHMACSHA1.ComputeHash(encoding.GetBytes(sz_Key)))
            Else
                szHashValue = System.Convert.ToBase64String(objHMACSHA1.ComputeHash(encoding.GetBytes(sz_Key)))
            End If
            If ("L" = sz_Case.ToUpper()) Then
                szHashValue = szHashValue.ToLower()
            ElseIf ("U" = sz_Case.ToUpper()) Then
                szHashValue = szHashValue.ToUpper()
            End If
            objHMACSHA1.Clear()

        Catch ex As Exception
            Throw ex
        End Try

        Return szHashValue
    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szComputeHMACSHA1HEXHash
    ' Function Type:	String. Hash value generated using HMAC SHA-1 algorithm with HEX.
    ' Parameter:		sz_Key. Values to be hash.
    ' Description:		Get hash value generated using HMAC SHA-1 algorithm with HEX
    ' History:			Added  for Barclay on 20 Oct 2011
    '********************************************************************************************
    Public Function szComputeHMACSHA1HEXHash(ByVal sz_Key As String, ByVal st_HostInfo As Common.STHost, ByVal sz_Case As String) As String
        Dim szHashValue As String = ""
        Dim objHMACSHA1 As New HMACSHA1
        Dim btHashbuffer() As Byte
        Dim szSecretKey As String = ""

        Try
            szSecretKey = szDecrypt3DEStoHex(st_HostInfo.szSecretKey, Common.C_3DESAPPKEY, Common.C_3DESAPPVECTOR)
            objHMACSHA1 = New HMACSHA1(System.Text.Encoding.UTF8.GetBytes(szSecretKey))
            btHashbuffer = objHMACSHA1.Hash
            szHashValue = szBytesToHex(btHashbuffer)

            If ("L" = sz_Case.ToUpper()) Then
                szHashValue = szHashValue.ToLower()
            ElseIf ("U" = sz_Case.ToUpper()) Then
                szHashValue = szHashValue.ToUpper()
            End If

        Catch ex As Exception
            Throw ex
        Finally
            If Not objHMACSHA1 Is Nothing Then objHMACSHA1 = Nothing
        End Try

        Return szHashValue
    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szComputeSHA160Hash
    ' Function Type:	String. Hash value generated using SHA-160Bit algorithm.
    ' Parameter:		sz_Key. Values to be hash.
    ' Description:		Get hash value generated using SHA-160Bit algorithm
    ' History:			11 May 2009
    '********************************************************************************************
    Public Function szComputeSHA160Hash(ByVal sz_Key As String) As String
        Dim szHashValue As String = ""
        Dim objSHA160 As New SHA1CryptoServiceProvider
        Dim btHashbuffer() As Byte = System.Text.Encoding.ASCII.GetBytes(sz_Key)

        Try
            btHashbuffer = objSHA160.ComputeHash(btHashbuffer)
            szHashValue = szWriteHex(btHashbuffer)
        Catch ex As Exception
            Throw ex
        Finally
            If Not objSHA160 Is Nothing Then objSHA160 = Nothing
        End Try

        Return szHashValue
    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szComputeSHA256Hash
    ' Function Type:	String. Hash value generated using SHA256 algorithm.
    ' Parameter:		sz_Key. Values to be hash.
    ' Description:		Get hash value generated using SHA256 algorithm
    ' History:			04 March 2009
    '********************************************************************************************
    Public Function szComputeSHA256Hash(ByVal sz_Key As String) As String
        Dim szHashValue As String = ""
        Dim objSHA256 As SHA256 = Nothing
        Dim btHashbuffer() As Byte = Nothing

        Try
            objSHA256 = New SHA256Managed
            objSHA256.ComputeHash(System.Text.Encoding.ASCII.GetBytes(sz_Key))
            btHashbuffer = objSHA256.Hash
            szHashValue = szWriteHex(btHashbuffer).ToLower()

        Catch ex As Exception
            Throw ex
        Finally
            If Not objSHA256 Is Nothing Then objSHA256 = Nothing
        End Try

        Return szHashValue
    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szComputeSHA2B64Hash
    ' Function Type:	String. Hash value generated using SHA256 algorithm.
    ' Parameter:		sz_Key. Values to be hash.
    ' Description:		Get hash value generated using SHA256 algorithm
    ' History:			19 Aug 2015     'Added  PBB
    ' Modified:         4 Dec 2015  PBB hash string need to be upper case
    '********************************************************************************************
    Public Function szComputeSHA2B64Hash(ByVal sz_Key As String, Optional ByVal sz_Case As String = "") As String
        Dim szHashValue As String = ""
        Dim objSHA256 As SHA256 = Nothing
        Dim btHashbuffer() As Byte = Nothing

        Try
            If ("U" = sz_Case) Then
                sz_Key = sz_Key.ToUpper()
            End If

            objSHA256 = New SHA256Managed
            objSHA256.ComputeHash(System.Text.Encoding.UTF8.GetBytes(sz_Key))
            btHashbuffer = objSHA256.Hash
            szHashValue = Convert.ToBase64String(btHashbuffer)

        Catch ex As Exception
            Throw ex
        Finally
            If Not objSHA256 Is Nothing Then objSHA256 = Nothing
        End Try

        Return szHashValue
    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szComputeSHA1BCHash
    ' Function Type:	String. Hash value generated using SHA1(Bit converter) algorithm.
    ' Parameter:		sz_Key. Values to be hash.
    ' Description:		Get hash value generated using SHA1(Bit converter) algorithm
    ' History:			04 June 2009
    '********************************************************************************************
    Public Function szComputeSHA1BCHash(ByVal sz_Key As String, ByVal sz_Case As String) As String
        Dim szHashValue As String = ""
        Dim objSHA1 As SHA1CryptoServiceProvider = Nothing
        Dim btHashbuffer() As Byte = Nothing

        Try
            objSHA1 = New SHA1CryptoServiceProvider
            objSHA1.ComputeHash(System.Text.Encoding.UTF8.GetBytes(sz_Key.ToCharArray))
            btHashbuffer = objSHA1.Hash
            szHashValue = BitConverter.ToString(btHashbuffer)
            szHashValue = szHashValue.Replace("-", "")
            If ("L" = sz_Case.ToUpper()) Then
                szHashValue = szHashValue.ToLower()
            End If

        Catch ex As Exception
            Throw ex
        Finally
            If Not objSHA1 Is Nothing Then objSHA1 = Nothing
        End Try

        Return szHashValue
    End Function
    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szComputeMD5Hash - Base64
    ' Function Type:	String. Hash value generated using MD5 algorithm.
    ' Parameter:		sz_Key. Values to be hash.
    ' Description:		Get hash value generated using MD5 algorithm
    ' History:			20 Oct 2008
    '********************************************************************************************
    Public Function szComputeMD5Hash(ByVal sz_Key As String, ByVal sz_Case As String) As String
        Dim szHashValue As String = ""
        Dim objMD5 As MD5CryptoServiceProvider = Nothing
        Dim btHashbuffer() As Byte

        Try
            objMD5 = New MD5CryptoServiceProvider
            objMD5.ComputeHash(System.Text.Encoding.UTF8.GetBytes(sz_Key.ToCharArray))
            btHashbuffer = objMD5.Hash
            szHashValue = System.Convert.ToBase64String(btHashbuffer)
            'Added on 15 Oct 2009
            If ("L" = sz_Case.ToUpper()) Then
                szHashValue = szHashValue.ToLower()
            ElseIf ("U" = sz_Case.ToUpper()) Then
                szHashValue = szHashValue.ToUpper()
            End If

        Catch ex As Exception
            Throw ex
        Finally
            If Not objMD5 Is Nothing Then objMD5 = Nothing
        End Try

        Return szHashValue
    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szComputeMD5HexHash - Hex
    ' Function Type:	String. Hash value generated using MD5 algorithm.
    ' Parameter:		sz_Key. Values to be hash. Added sz_Case on 25 Oct 2010
    ' Description:		Get hash value generated using MD5 algorithm
    ' History:			17 Apr 2009
    '********************************************************************************************
    Public Function szComputeMD5HexHash(ByVal sz_Key As String, ByVal sz_Case As String) As String
        Dim szHashValue As String = Nothing
        Dim objMD5 As MD5CryptoServiceProvider = Nothing
        Dim btHashbuffer() As Byte = Nothing

        Try
            objMD5 = New MD5CryptoServiceProvider
            objMD5.ComputeHash(System.Text.Encoding.UTF8.GetBytes(sz_Key.ToCharArray))
            btHashbuffer = objMD5.Hash

            szHashValue = szBytesToHex(btHashbuffer)

            'Added on 25 Oct 2010
            If ("L" = sz_Case.ToUpper()) Then
                szHashValue = szHashValue.ToLower()
            ElseIf ("U" = sz_Case.ToUpper()) Then
                szHashValue = szHashValue.ToUpper()
            End If

        Catch ex As Exception
            Throw ex
        Finally
            If Not objMD5 Is Nothing Then objMD5 = Nothing
        End Try

        Return szHashValue
    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szComputeMD5HexEnc - Hex + Encode
    ' Function Type:	String. Hash value generated using MD5 algorithm.
    ' Parameter:		sz_Key. Values to be hashed
    ' Description:		Get hash value generated using MD5 algorithm
    ' History:			15 Mar 2011 
    '********************************************************************************************
    Public Function szComputeMD5HexEnc(ByVal sz_Key As String, ByVal sz_Case As String) As String
        Dim szHashValue As String = Nothing
        Dim objMD5 As MD5CryptoServiceProvider = Nothing
        Dim btHashbuffer() As Byte = Nothing
        Dim btHashHexbuffer() As Byte = Nothing

        Try
            objMD5 = New MD5CryptoServiceProvider
            objMD5.ComputeHash(System.Text.Encoding.UTF8.GetBytes(sz_Key.ToCharArray))
            btHashbuffer = objMD5.Hash

            szHashValue = szBytesToHex(btHashbuffer)

            If ("L" = sz_Case.ToUpper()) Then
                szHashValue = szHashValue.ToLower()
            ElseIf ("U" = sz_Case.ToUpper()) Then
                szHashValue = szHashValue.ToUpper()
            End If

            btHashHexbuffer = System.Text.Encoding.UTF8.GetBytes(szHashValue)
            szHashValue = Convert.ToBase64String(btHashHexbuffer)

        Catch ex As Exception
            Throw ex
        Finally
            If Not objMD5 Is Nothing Then objMD5 = Nothing
        End Try

        Return szHashValue
    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szComputeAdler32Hash
    ' Function Type:	String. Hash value generated using Adler32 algorithm.
    ' Parameter:		sz_Key. Values to be hash.
    ' Description:		Get hash value generated using Adler32 algorithm
    ' History:			17 Dec 2009
    '********************************************************************************************
    Public Function szComputeAdler32Hash(ByVal sz_Key As String) As String
        Dim lAdler As Long = 0

        Try
            lAdler = 1
            lAdler = lAdler32(lAdler, sz_Key)
        Catch ex As Exception
            Throw ex
        End Try

        Return CStr(lAdler)
    End Function
    '********************************************************************************************
    ' Class Name:		Hash  (RHB)
    ' Function Name:	szComputeRHBHash
    ' Function Type:	String. string value split to array and perform hash count.
    ' Parameter:		sz_Key. String values to be converted to HashCount.
    ' Description:		Get HashCount string value converted from String.
    ' History:			31 March 2009
    '********************************************************************************************
    Public Function szComputeRHBHash(ByVal sz_Key As String) As String
        Dim szHashValue As String = ""
        Dim aszParams() As String
        Dim szReturnCode As String = ""
        Dim szTotalHash As String = ""
        Dim szOrderNo As String = ""    ' Order Number to RHB - GatewayTxnID
        Dim iMID As Integer
        Dim iAmt As Decimal
        Dim dHashA As Decimal
        Dim dHashB As Decimal
        Dim dHashC As Decimal
        Dim iLoop As Integer = 0
        Dim szCompare As String = ""

        If ("" = sz_Key) Then
            Return szHashValue
        End If

        Try
            aszParams = sz_Key.Split("-")

            For iLoop = 0 To aszParams.GetUpperBound(0)
                Select Case iLoop.ToString()
                    Case "0"
                        iMID = Convert.ToInt32(aszParams(iLoop).Trim())
                    Case "1"
                        szOrderNo = aszParams(iLoop).Trim()
                    Case "2"
                        iAmt = Convert.ToDecimal(aszParams(iLoop).Trim())
                    Case "3"
                        szReturnCode = aszParams(iLoop).Trim()
                End Select
            Next

            ' Extracts numeric digits from Order Number(GatewayTxnID) if it contains alpha characters
            szTotalHash = ""
            If (False = IsNumeric(szOrderNo)) Then
                For iLoop = 1 To Len(szOrderNo)
                    szCompare = Mid(szOrderNo, iLoop, 1)
                    If (IsNumeric(szCompare)) Then
                        szTotalHash = szTotalHash + szCompare
                    End If
                Next

                szOrderNo = szTotalHash
                ' Assigns szOrderNo to "0" if Order Number contains all alpha characters
                If ("" = szOrderNo) Then
                    szOrderNo = "0"
                End If
            End If

            dHashA = Convert.ToDecimal(szOrderNo) * iAmt
            dHashB = iMID * iAmt
            dHashC = iMID * Convert.ToDecimal(szOrderNo)

            If ("" = szReturnCode) Then
                szReturnCode = "0"
            End If

            ' e.g. 7090982.12345678
            szTotalHash = ((dHashA + dHashB + dHashC) / (iMID + Convert.ToInt32(szReturnCode))).ToString()
            ' e.g. IndexOf is zero based, giving 7, then 7+7=14, Substring(0, 14) giving 7090982.123456
            '35030000425.33
            szTotalHash = szTotalHash.Substring(0, szTotalHash.IndexOf(".") + 7)    ' 6 decimal places precision

            szHashValue = szTotalHash

        Catch ex As Exception
            Throw ex
        End Try

        Return szHashValue
    End Function
    '********************************************************************************************
    ' Class Name:		Hash  (RHB)
    ' Function Name:	szComputeRHB2Hash
    ' Function Type:	String. string value split to array and perform hash count.
    ' Parameter:		sz_Key. String values to be converted to HashCount.
    ' Description:		Get HashCount string value converted from String.
    ' History:			31 March 2009
    '********************************************************************************************
    Public Function szComputeRHB2Hash(ByVal sz_Key As String, ByVal st_HostInfo As Common.STHost) As String
        Dim szHashValue As String = ""
        Dim aszParams() As String
        Dim szReturnCode As String = ""
        Dim szTotalHash As String = ""
        Dim szOrderNo As String = ""    ' Order Number to RHB - GatewayTxnID
        Dim iMID As Integer
        Dim iAmt As Decimal
        Dim dHashA As Decimal
        Dim dHashB As Decimal
        Dim dHashC As Decimal
        Dim iLoop As Integer = 0
        Dim szCompare As String = ""

        If ("" = sz_Key) Then
            Return szHashValue
        End If

        Try
            aszParams = sz_Key.Split("-")

            For iLoop = 0 To aszParams.GetUpperBound(0)
                Select Case iLoop.ToString()
                    Case "0"
                        iMID = Convert.ToInt32(aszParams(iLoop).Trim())
                    Case "1"
                        szOrderNo = aszParams(iLoop).Trim()
                    Case "2"
                        iAmt = Convert.ToDecimal(aszParams(iLoop).Trim())
                    Case "3"
                        szReturnCode = aszParams(iLoop).Trim()
                End Select
            Next

            ' Extracts numeric digits from Order Number(GatewayTxnID) if it contains alpha characters
            szTotalHash = ""
            If (False = IsNumeric(szOrderNo)) Then
                For iLoop = 1 To Len(szOrderNo)
                    szCompare = Mid(szOrderNo, iLoop, 1)
                    If (IsNumeric(szCompare)) Then
                        szTotalHash = szTotalHash + szCompare
                    End If
                Next

                szOrderNo = szTotalHash
                ' Assigns szOrderNo to "0" if Order Number contains all alpha characters
                If ("" = szOrderNo) Then
                    szOrderNo = "0"
                End If
            End If

            dHashA = Convert.ToDecimal(szOrderNo) * iAmt
            dHashB = iMID * iAmt
            dHashC = iMID * Convert.ToDecimal(szOrderNo)

            If ("" = szReturnCode) Then
                szReturnCode = "0"
            End If

            ' e.g. 7090982.12345678
            szTotalHash = ((dHashA + dHashB + dHashC) / (iMID + Convert.ToInt32(szReturnCode))).ToString()
            ' e.g. IndexOf is zero based, giving 7, then 7+7=14, Substring(0, 14) giving 7090982.123456
            '35030000425.33
            szTotalHash = szTotalHash.Substring(0, szTotalHash.IndexOf(".") + 7)    ' 6 decimal places precision

            'Added 05 Aug 2016, New add in SHA hashing, in V1.5
            szHashValue = szComputeHMACSHA1Hash(szTotalHash, st_HostInfo, "L", "H")
            'szHashValue = szTotalHash

        Catch ex As Exception
            Throw ex
        End Try

        Return szHashValue
    End Function

    '********************************************************************************************
    ' Class Name:		Hash  (SCB)
    ' Function Name:	szComputeSCBHash
    ' Function Type:	String. Encrypted or decrypted value.
    ' Parameter:		sz_Key. String values to be encrypted or decrypted.
    ' Description:		Encrypt and decrypt data for SCB.
    ' History:			20 May 2009
    '********************************************************************************************
    Public Function szComputeSCBHash(ByVal sz_Data As String, ByVal st_PayInfo As Common.STPayInfo) As String
        Dim szHashValue As String = ""
        Dim szMethod As String = "" 'encrypt/descrypt
        Dim szFileName As String = ""
        Dim szCommand As String = ""
        Dim szFilePath As String = ""
        'Added st_PayInfo.szMerchantID on 17 June 2009 '"D:\AA\OB\Templates\Hashtool\Win_Exe\"
        'Added 15 March 2017, SCBTH_MY
        If ("" <> st_PayInfo.szEncryptKeyPath) Then
            szFilePath = st_PayInfo.szEncryptKeyPath
        Else
            szFilePath = ConfigurationManager.AppSettings("HashPath") '+ st_PayInfo.szMerchantID + "\" 'Modified 30 Jan 2015, removed MerchantID because all merchants are sharing the same public key and not using private key
        End If

        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
               DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
               st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > SCB file path(" + szFilePath + ")")

        'Dim szFilePath As String = ConfigurationManager.AppSettings("HashPath") '+ st_PayInfo.szMerchantID + "\" 'Modified 30 Jan 2015, removed MerchantID because all merchants are sharing the same public key and not using private key
        Dim szEXEFile As String = ConfigurationManager.AppSettings("Hashexe")

        Dim objStreamReader As StreamReader
        Dim objStreamWriter As StreamWriter
        Dim oFile As File

        Try
            'encrypt^payee_id=01&cust_id=xxxx&ref_no=xxxx&ref_date=20010820080000&amount=300.00&currency=THB&description=&usrdat=usrxxx

            Dim szSplitDt As String() = sz_Data.Split("^")

            If (szSplitDt.Length > 1) Then
                szMethod = szSplitDt(0)
                sz_Data = szSplitDt(1)
            End If

            Dim iStartIndex As Integer = sz_Data.IndexOf("&ref_no", 0)
            Dim iEndIndex As Integer = sz_Data.IndexOf("&ref_date", 0)

            If (iStartIndex > 0) Then   'For payment request and query request's encryption which should be fine
                'because there won't be payment and query request for the same GatewayTxnID happen at the same time
                'Takes ref_no which is GatewayTxnID as the crypto output file name
                'Limitation of this is tight down to fixed SCB payment and query request format: &ref_no=.....&ref_date
                'NOTE: If SCB.txt message format configuration file changed, this won't work
                szFileName = sz_Data.Substring((iStartIndex + 8), (iEndIndex - (iStartIndex + 8)))
            ElseIf (st_PayInfo.szTxnID <> "") Then    'Added on 13 Nov 2009 - for query response's decryption
                'Cannot just take "yyyyMMddHHmmssfff" as the output file name because there may be possibility when
                'at the same millisecond the filename will be the same for query response and payment response
                szFileName = st_PayInfo.szTxnID + System.DateTime.Now.ToString("yyyyMMddHHmmssfff")
            Else
                'NOTE: Random number may not be 100% random on the same milliseconds!
                'For payment response's decryption, should be safe because this will be only used by payment response
                Dim RandomNumGen = New System.Random
                Dim szRandomNumber As String = RandomNumGen.Next(100, 999)
                szFileName = st_PayInfo.szMerchantID + System.DateTime.Now.ToString("yyyyMMddHHmmssfff") + szRandomNumber 'Modified 30 Jan 2015, added MerchantID because bat files sharing same folder
            End If

            'Runtime generates bat file which includes the following command line in order to call SCB crypto application(SecureHost.exe)
            'for encrypting or decrypting a string and then output the cipher text or clear text into a file named after GatewayTxnID

            'TESTING
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > SCB (" + sz_Data + ")")

            szCommand = "cd " + vbNewLine + "cd /D " + szFilePath + vbNewLine + szEXEFile + " " + szMethod + " -o " + szFileName + ".txt """ + sz_Data + """"

            'Call Shell(szCommand, 0)

            objStreamWriter = oFile.CreateText(szFilePath + szFileName + ".bat")
            objStreamWriter.WriteLine(szCommand)
            objStreamWriter.Close()

            'Run bat file which has been included command line to execute SCB crypto application (SecureHost.exe)
            Dim MyProcess As System.Diagnostics.Process = New System.Diagnostics.Process

            MyProcess.StartInfo.FileName = szFilePath + szFileName + ".bat"
            MyProcess.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden
            MyProcess.StartInfo.CreateNoWindow = True
            MyProcess.Start()
            MyProcess.WaitForExit()
            MyProcess.Close()

            'Retrieves cipher text or clear text from the output file
            objStreamReader = File.OpenText(szFilePath + szFileName + ".txt")
            szHashValue = objStreamReader.ReadToEnd().Trim()

        Catch ex As Exception
            Throw ex
        Finally
            objStreamReader.Close()

            'Deletes both bat file and output file upon finished processing but remains bat file for troubleshooting
            'if no output file is generated due to crypto process error - invalid parameter, invalid key size, etc
            If (System.IO.File.Exists(szFilePath + szFileName + ".txt") = True And System.IO.File.Exists(szFilePath + szFileName + ".bat") = True) Then
                System.IO.File.Delete(szFilePath + szFileName + ".txt")
                System.IO.File.Delete(szFilePath + szFileName + ".bat")
            End If

        End Try

        Return szHashValue
    End Function

    '********************************************************************************************
    ' Class Name:		Hash  (AliPay)
    ' Function Name:	szComputeAliPayHash
    ' Function Type:	String. Alipay hash value.
    ' Parameter:		sz_Data - String from which data to be hashed can be extracted
    ' Description:		Generate AliPay hash value
    ' History:			21 Aug 2009
    '********************************************************************************************
    Public Function szComputeAliPayHash(ByVal sz_Data As String, ByVal st_PayInfo As Common.STPayInfo) As String
        Dim szHashValue As String = ""
        Dim szKey As String = ""
        Dim szString() As String
        Dim szSortedStr() As String
        Dim sbHashStr As New StringBuilder
        Dim i As Integer

        Try
            'Request hash
            If (1 = st_PayInfo.iReqRes) Then
                'SendKeyABC123-&_input_charset=utf-8&out_trade_no=A07DBDHJK00000000001...
                szString = Split(sz_Data, "&")
                szKey = szString(0)

                Dim szStringEx() As String
                ReDim szStringEx(szString.GetUpperBound(0) - 1)

                'Excludes the 1st element(SendKey) from string array to form another new string array 
                For i = 1 To szString.GetUpperBound(0)
                    szStringEx(i - 1) = szString(i)
                Next

                szSortedStr = szBubbleSort(szStringEx)

                For i = 0 To szSortedStr.Length - 1
                    If i = szSortedStr.Length - 1 Then
                        sbHashStr.Append(szSortedStr(i))
                    Else
                        sbHashStr.Append(szSortedStr(i) & "&")
                    End If
                Next
            Else
                'Response hash
                szKey = sz_Data

                Dim coll As System.Collections.Specialized.NameValueCollection
                i = HttpContext.Current.Request.QueryString.Count
                If i > 0 Then
                    coll = HttpContext.Current.Request.QueryString()
                Else
                    i = HttpContext.Current.Request.Form.Count
                    coll = HttpContext.Current.Request.Form
                End If

                szString = coll.AllKeys
                szSortedStr = szBubbleSort(szString)

                For i = 0 To szSortedStr.Length - 1
                    If (Server.UrlDecode(HttpContext.Current.Request(szSortedStr(i))) <> "" And szSortedStr(i).ToLower() <> "sign" And szSortedStr(i).ToLower() <> "sign_type") Then
                        If i = szSortedStr.Length - 1 Then
                            'AliPay spec specifies must decode BUT actually need to ENCODE field "notify_id",
                            'as per AliPay's Vincent's email reply on 26 Aug 2009
                            If (szSortedStr(i).ToLower() = "notify_id") Then
                                sbHashStr.Append(szSortedStr(i) & "=" & Server.UrlEncode(HttpContext.Current.Request(szSortedStr(i))))
                                'As confirmed by AliPay's Vincent on 27 Aug 2009, have to remove "25" from encoded sign character
                                'For "+", "%252F" to "%2F"; For "/", "%252B" to "%2B"
                                sbHashStr.Replace("%25", "%")
                            Else
                                sbHashStr.Append(szSortedStr(i) & "=" & Server.UrlDecode(HttpContext.Current.Request(szSortedStr(i))))
                            End If

                        Else
                            If (szSortedStr(i).ToLower() = "notify_id") Then
                                sbHashStr.Append(szSortedStr(i) & "=" & Server.UrlEncode(HttpContext.Current.Request(szSortedStr(i))) & "&")
                                'As confirmed by AliPay's Vincent on 27 Aug 2009, have to remove "25" from encoded sign character
                                'For "+", "%252F" to "%2F"; For "/", "%252B" to "%2B"
                                sbHashStr.Replace("%25", "%")
                            Else
                                sbHashStr.Append(szSortedStr(i) & "=" & Server.UrlDecode(HttpContext.Current.Request(szSortedStr(i))) & "&")
                            End If

                        End If
                    End If
                Next
            End If

            'Appends hash send key or return key
            If ("&" = sbHashStr.ToString().Substring(sbHashStr.Length - 1, 1)) Then
                sbHashStr.Remove(sbHashStr.Length - 1, 1)
            End If

            sbHashStr.Append(szKey)

            szHashValue = szComputeMD5HexHash(sbHashStr.ToString(), "O")    ' Added "O" on 25 Oct 2010

        Catch ex As Exception
            Throw ex
        End Try

        Return szHashValue.ToLower()
    End Function

    '********************************************************************************************
    ' Class Name:		Hash  (ChinaPay)
    ' Function Name:	szComputeChinaPayHash
    ' Function Type:	String. Checksum value.
    ' Parameter:		sz_Key. String values for checksum creation.
    ' Description:		Create checksum value for ChinaPay.
    ' History:			25 Oct 2009
    '********************************************************************************************
    Public Function szComputeChinaPayHash(ByVal sz_Data As String, ByVal st_PayInfo As Common.STPayInfo) As String
        Dim szHashValue As String = ""
        Dim objCP
        Dim szSplitDt As String()
        Dim szString As String = ""
        Dim iLoop As Integer = 0

        Try
            'Pre-requisite: CNPNC.dll provided by ChinaPay must be registered using regsvr32
            objCP = Server.CreateObject("CPNPC.NPC")

            'SIGN^[MID]^[GatewayTxnID]^[TxnAmount_02]^{ISO3166}^{yyyyMMdd}^[=0001]
            'CHECK^merid^orderno^amount^currencycode^transdate^transtype^status^checkvalue
            szSplitDt = sz_Data.Split("^")

            If (szSplitDt.Length() > 0) Then
                If ("SIGN" = szSplitDt(0).ToUpper()) Then
                    If (7 = szSplitDt.Length()) Then
                        objCP.setMerKeyFile(st_PayInfo.szEncryptKeyPath)

                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                        "Encrypt key file path (" + st_PayInfo.szEncryptKeyPath + ")")

                        'Version: 20040916 - Sign([in] BSTR MerId, [in] BSTR OrdId, [in] BSTR TransAmt, [in] BSTR CuryId, [in] BSTR TransDate, [in] BSTR TransType, [out, retval] BSTR* RetValue))
                        szHashValue = objCP.Sign(szSplitDt(1), szSplitDt(2), szSplitDt(3), szSplitDt(4), szSplitDt(5), szSplitDt(6))

                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                        szHashValue + " = CP.Sign(" + szSplitDt(1) + "," + szSplitDt(2) + "," + szSplitDt(3) + "," + szSplitDt(4) + "," + szSplitDt(5) + "," + szSplitDt(6) + ")")
                    End If

                ElseIf ("SIGNDATA" = szSplitDt(0).ToUpper()) Then
                    szString = ""
                    For iLoop = 2 To szSplitDt.GetUpperBound(0)
                        szString = szString + szSplitDt(iLoop)
                    Next

                    objCP.setMerKeyFile(st_PayInfo.szEncryptKeyPath)

                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    "Encrypt key file path (" + st_PayInfo.szEncryptKeyPath + ")")

                    'Version: 20070129 - SignData([in] BSTR MerId, [in] BSTR SignData, [out, retval] BSTR* RetValue)
                    szHashValue = objCP.SignData(szSplitDt(1), szString)

                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    szHashValue + " = CP.SignData(" + szSplitDt(1) + "," + szString + ")")

                ElseIf ("CHECK" = szSplitDt(0).ToUpper()) Then
                    If (9 = szSplitDt.Length()) Then
                        objCP.setPubKeyFile(st_PayInfo.szDecryptKeyPath)

                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                        "Decrypt key file path (" + st_PayInfo.szDecryptKeyPath + ")")

                        'Check([in] BSTR MerId, [in] BSTR OrdId, [in] BSTR TransAmt, [in] BSTR CuryId, [in] BSTR TransDate, [in] BSTR TransType, [in] BSTR OrdStat, [in] BSTR ChkValue, [out, retval] BSTR* RetValue))
                        szHashValue = objCP.Check(szSplitDt(1), szSplitDt(2), szSplitDt(3), szSplitDt(4), szSplitDt(5), szSplitDt(6), szSplitDt(7), szSplitDt(8))

                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                        szHashValue + " = CP.Check(" + szSplitDt(1) + "," + szSplitDt(2) + "," + szSplitDt(3) + "," + szSplitDt(4) + "," + szSplitDt(5) + "," + szSplitDt(6) + "," + szSplitDt(7) + "," + szSplitDt(8) + ")")
                    End If
                End If
            End If

        Catch ex As Exception
            Throw ex
        Finally
            objCP = Nothing
        End Try

        Return szHashValue
    End Function

    '********************************************************************************************
    ' Class Name:		Hash  (99Bill)
    ' Function Name:	szCompute99BillHash
    ' Function Type:	String. Hash value.
    ' Parameter:		sz_Key. String values for hash creation.
    ' Description:		Create to cater 99Bill hash on null value.
    ' History:			25 Oct 2010
    '********************************************************************************************
    Public Function szCompute99BillHash(ByVal sz_Key As String, ByVal st_PayInfo As Common.STPayInfo) As String
        Dim szTempKey() As String
        Dim szInnerKey() As String
        Dim szHashString As String = ""
        Dim szHashValue As String = ""
        Dim iLoop As Integer

        Try
            szTempKey = Split(sz_Key, "&")
            For iLoop = 0 To szTempKey.GetUpperBound(0)
                szInnerKey = Split(szTempKey(iLoop), "=")
                If ("" <> szInnerKey(1)) Then
                    If ("" = szHashString) Then
                        szHashString = szTempKey(iLoop)
                    Else
                        szHashString = szHashString + "&" + szTempKey(iLoop)
                    End If
                End If
            Next

            szHashValue = szComputeMD5HexHash(szHashString, "U")

        Catch ex As Exception
            Throw ex
        End Try

        Return szHashValue
    End Function

    ''********************************************************************************************
    '' Class Name:		Hash
    '' Function Name:	szComputeCUPHash - Hex
    '' Function Type:	String. Sign message base on pfx private key with RSA, then hash value generated using MD5 algorithm.
    '' Parameter:		sz_Key. Values to be hash. Added sz_Case on 25 Oct 2010
    '' Description:		Sign message base on pfx private key with RSA, then get hash value generated using MD5 algorithm
    '' History:			15 Nov 2010 
    ''********************************************************************************************
    'Public Function szComputeCUPHash(ByVal sz_Key As String, ByVal st_PayInfo As Common.STPayInfo, ByVal st_HostInfo As Common.STHost) As String
    '    Dim szHashValue As String
    '    Dim szReturnKey As String
    '    Dim DDGWCOM As New DDGWComponent.Component

    '    Try
    '        'Decrypt to retrieve clear password to open pfx file in order to get private key
    '        szReturnKey = szDecrypt3DEStoHex(st_HostInfo.szSendKey, Common.C_3DESAPPKEY, Common.C_3DESAPPVECTOR)

    '        'Call windows service DDGWComponent's function RSAwithMD5 to perform RSA encryption and MD5 hashing, with lower case
    '        'EncryptKeyPath stores the pfx file path for the MerchantID/Merchant
    '        DDGWCOM.Url = ConfigurationManager.AppSettings("DDCompWebSvcURL")  'Added on 7 Jan 2011

    '        szHashValue = DDGWCOM.RSAwithMD5(sz_Key, st_PayInfo.szEncryptKeyPath, szReturnKey, "L")

    '        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
    '        DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
    '        " szHashValue[" + szHashValue + "]")

    '    Catch ex As Exception
    '        Throw ex
    '    End Try

    '    Return szHashValue
    'End Function

    '********************************************************************************************
    ' Class Name:		Hash  (UOBThai)
    ' Function Name:	szComputeUOBThaiHash
    ' Function Type:	String. Encrypted value.
    ' Parameter:		sz_Key. String values to be encrypted.
    ' Description:		Encrypt data for UOBThai.
    ' History:			14 Dec 2010, added 
    '********************************************************************************************
    Public Function szComputeUOBThaiHash(ByVal sz_Data As String, ByVal sz_Key As String) As String
        'Currently UOB never encrypt and decrypt request and response message, so this function is unused at this moment.

        Dim szOutput As String
        Dim CryptoObject As Object
        Dim szTempArray() As String

        Try
            szTempArray = Split(sz_Data, "^")
            CryptoObject = New BoAPrj.EncCls

            If ("encrypt" = szTempArray(0).Trim().ToLower()) Then
                szOutput = CryptoObject.BoAEncrypt(szTempArray(1), sz_Key) 'encrypt
            Else
                szOutput = CryptoObject.BoaDecrypt(szTempArray(1).ToString(), sz_Key.ToString()) 'decrypt
            End If

        Catch ex As Exception
            Throw ex
        End Try

        Return szOutput
    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szComputeEnetsHash
    ' Function Type:	String. Encrypted value.
    ' Parameter:		sz_Key. String values to be encrypted.
    ' Description:		Encrypt data for Enets.
    ' History:			17 Dec 2010, added 
    '********************************************************************************************
    Public Function szComputeEnetsHash(ByVal sz_Data As String, ByVal sz_FilePath As String, ByVal st_PayInfo As Common.STPayInfo) As String
        Dim szString As String = ""
        Dim szMessage() As String
        Dim szFieldNameArray() As String
        Dim szFieldValueArray() As String
        Dim szTempArray() As String
        Dim szMethodArray() As String
        Dim iCount As Integer
        Dim iStart As Integer               'Added, 27 Apr 2011
        Dim myDebitMerchant As Object
        Dim myTxnReq As Object
        Dim myDebitTxnReq As Object
        Dim myTxnNotify As Object
        Dim myDebitNotify As Object
        Dim myTxnAck As Object
        Dim myDebitAck As Object
        Dim myTxnRes As Object
        Dim myDebitTxnRes As Object
        Dim szPath As String

        myDebitMerchant = New DebitMerchant
        myTxnReq = New TxnReq
        myDebitTxnReq = New DebitTxnReq
        myTxnNotify = New TxnNotify
        myDebitNotify = New DebitNotify
        myTxnAck = New TxnAck
        myDebitAck = New DebitAck
        myTxnRes = New TxnRes
        myDebitTxnRes = New DebitTxnRes

        'Path by MerchantID for ENETS Merchant Private Key File, ENETS Public Key File storage and ENETS settings file
        'Modified 31 Mar 2014, to cater for G-Direct and Bank-Direct eNETS plugin path
        'e.g. D:\IIS\IPG\ENETS\GHL\etc (G-Direct) or D:\IIS\IPG\ENETS\[MERCHANTID]\etc (Bank-Direct)
        'Commented  31 Mar 2014
        'szPath = sz_FilePath + "\" + st_PayInfo.szMerchantID + "\etc"
        szPath = sz_FilePath.Replace("[MERCHANTID]", st_PayInfo.szMerchantID)

        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                        st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Private Key Path [" + szPath + "]")

        Try
            myDebitMerchant = Merchant.getInstance(Merchant.MERCHANT_TYPE_DEBIT, szPath)

            'Added logging  20 Jun 2011, as suggested by ENETS
            If (myDebitMerchant Is Nothing) Then
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > ENETS myDebitMerchant [NULL], Merchant.MERCHANT_TYPE_DEBIT[" + Merchant.MERCHANT_TYPE_DEBIT.ToString() + "], Path[" + szPath + "]")
            End If

            'Format:[encrypt^NetsMid=]![mid]![&SubmissionMode=]![=B]![&Tid=]![]![&TxnAmount=]![TxnAmount_2]![&MerTxnRef=]![gatewaytxnid]![&MerCertId=]![acquirerid]![&PaymentMode=]![=DD]![&CurrenyCode=]![CurrencyCode]![&SuccessURL=]![gatewayreturnurl]![&SuccessURLParams=]![]![&FailureURL=]![gatewayreturnurl]![&FailureURLParams=]![]![&TxnDtm=]![]![&MerTimeZone=]![=+8:00]![&MerTxnDate=]!{dd/MM/yyyy}![&MerTxnTime=]!{HH:mm:ss}![&Param1=]![=0]![&Param2=]![=0]![&Param3=]![=0]![&Param4=]![=0]![&Param5=]![=0]#
            'sz_Data:encrypt^NetsMid=123&SubmissionMode=B&Tid=&TxnAmount=123.12&....
            szMethodArray = Split(sz_Data, "^")

            'Calls ENETS UMAPI.dll to form encrypted request message to be sent to ENETS later
            If ("encrypt" = szMethodArray(0).ToLower()) Then
                szMessage = Split(szMethodArray(1), "&")        'Split NetsMid=123456&SubmissionMode=B&TxnAmount=12300

                ReDim szFieldNameArray(szMessage.GetUpperBound(0))
                ReDim szFieldValueArray(szMessage.GetUpperBound(0))

                'For each FieldName=FieldValue set
                For iCount = 0 To szMessage.GetUpperBound(0)
                    szTempArray = Split(szMessage(iCount), "=") 'Split NetsMid=123456

                    'Populate field name array: NetsMid, SubmissionMode...
                    '[&SuccessURLParams>MerchantID=]![MerchantID]
                    szFieldNameArray(iCount) = szTempArray(0)
                    'Populate field value array: 123456, B...
                    If ("=" = Right(szMessage(iCount).Trim(), 1)) Then
                        szFieldValueArray(iCount) = ""
                    Else
                        'Added checking of szFieldNameArray  27 Apr 2011, to cater for [&SuccessURLParams>MerchantID=]![MerchantID]
                        'to be formed as FieldName to be SuccessURLParams and FieldValue to be MerchantID=A07, note: A07 is an example of [MerchantID]
                        'to cater for ENETS to return "MerchantID=A07" as an additional response param on top of "message" param which contains encrypted response data
                        iStart = InStr(szFieldNameArray(iCount), ">")
                        If (iStart > 0) Then
                            'Take the remaining characters after ">" from FieldName "SuccessURLParams>MerchantID" + "=" + FieldValue to become "MerchantID=A07"
                            szFieldValueArray(iCount) = Mid(szFieldNameArray(iCount), iStart + 1) + "=" + szTempArray(1)
                            'Remove ">" together with characters after ">" from FieldName
                            szFieldNameArray(iCount) = Replace(szFieldNameArray(iCount), Mid(szFieldNameArray(iCount), iStart), "")
                        Else
                            szFieldValueArray(iCount) = szTempArray(1)
                        End If
                    End If
                Next iCount

                'Based on FieldName defined in message template, calls ENETS UMAPI.dll to set the respective fields to the corresponding value
                For iCount = 0 To szFieldNameArray.GetUpperBound(0)
                    Select Case szFieldNameArray(iCount).Trim().ToLower()
                        'Populate myTxnReq
                        Case "netsmid"
                            myTxnReq.setNetsMid(szFieldValueArray(iCount))
                        Case "submissionmode"
                            myTxnReq.setSubmissionMode(szFieldValueArray(iCount))
                        Case "txnamount"
                            myTxnReq.setTxnAmount(szFieldValueArray(iCount))
                        Case "mertxnref"
                            myTxnReq.setMerchantTxnRef(szFieldValueArray(iCount))
                        Case "mercertid"
                            myTxnReq.setMerchantCertId(szFieldValueArray(iCount))
                        Case "paymentmode"
                            myTxnReq.setPaymentMode(szFieldValueArray(iCount))
                        Case "currencycode"
                            myTxnReq.setCurrencyCode(szFieldValueArray(iCount))
                        Case "successurl"
                            myTxnReq.setSuccessUrl(szFieldValueArray(iCount))
                        Case "successurlparams"
                            myTxnReq.setSuccessUrlParams(szFieldValueArray(iCount))
                        Case "failureurl"
                            myTxnReq.setFailureUrl(szFieldValueArray(iCount))
                        Case "failureurlparams"
                            myTxnReq.setFailureUrlParams(szFieldValueArray(iCount))
                        Case "tid"
                            myTxnReq.setTid(szFieldValueArray(iCount))
                        Case "txndtm"
                            myTxnReq.setMerchantTxnDtm(szFieldValueArray(iCount))
                            'Populate myDebitTxnReq
                        Case "mertimezone"
                            myDebitTxnReq.setMerchantTimeZone(szFieldValueArray(iCount))
                        Case "mertxndate"
                            myDebitTxnReq.setMerchantTxnDate(szFieldValueArray(iCount))
                        Case "mertxntime"
                            myDebitTxnReq.setMerchantTxnTime(szFieldValueArray(iCount))
                        Case "param1"
                            myDebitTxnReq.setParam1(szFieldValueArray(iCount))
                        Case "param2"
                            myDebitTxnReq.setParam2(szFieldValueArray(iCount))
                        Case "param3"
                            myDebitTxnReq.setParam3(szFieldValueArray(iCount))
                        Case "param4"
                            myDebitTxnReq.setParam4(szFieldValueArray(iCount))
                        Case "param5"
                            myDebitTxnReq.setParam5(szFieldValueArray(iCount))
                    End Select
                Next iCount

                myTxnReq.setDebitTxnReq(myDebitTxnReq)

                'Added logging  20 Jun 2011, as suggested by ENETS
                If (myDebitTxnReq Is Nothing) Then
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                    st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > ENETS myDebitTxnReq [NULL]")
                End If

                'Added logging  20 Jun 2011, as suggested by ENETS
                If (myTxnReq Is Nothing) Then
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                    st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > ENETS myTxnReq [NULL]")
                End If

                'Added logging  9 Jun 2011
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > ENETS DLL forming request...")

                szString = myDebitMerchant.formPayReq(myTxnReq)

                'Added logging  9 Jun 2011
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > ENETS DLL request forming done")

                'Notification has been disabled
            ElseIf ("decryptnotification" = szMethodArray(0).ToLower()) Then 'Decrypt notification message from Enets
                myTxnNotify = myDebitMerchant.unpackNotification(szMethodArray(1))
                myDebitNotify = myTxnNotify.getDebitNotify()

                szString = ""
                szString += "MerchantTxnRef=" + myTxnNotify.getMerchantTxnRef()
                szString += "&NetsMid=" + myTxnNotify.getNetsMid()
                szString += "&Mid=" + myTxnNotify.getMid().ToString()
                szString += "&MerchantCertId=" + myTxnNotify.getMerchantCertId()
                szString += "&netsTxnRef=" + myTxnNotify.getNetsTxnRef()
                szString += "&Param1=" + myDebitNotify.getParam1()
                szString += "&Param2=" + myDebitNotify.getParam2()
                szString += "&Param3=" + myDebitNotify.getParam3()
                szString += "&Param4=" + myDebitNotify.getParam4()
                szString += "&Param5=" + myDebitNotify.getParam5()
                szString += "&Retry=" + myDebitNotify.getRetry()
                szString += "&Version=" + myDebitNotify.getVersion()

                'Notification has been disabled
            ElseIf ("encryptack" = szMethodArray(0).ToLower()) Then 'Form and encrypt ACK message to send to ENETS

                szMessage = Split(szMethodArray(1), "&") 'Split NetsMid=123456&MerchantCertId=111&MerchantTxnRef=12300
                ReDim szFieldNameArray(szMessage.GetUpperBound(0))
                ReDim szFieldValueArray(szMessage.GetUpperBound(0))

                For iCount = 0 To szMessage.GetUpperBound(0)
                    szTempArray = Split(szMessage(iCount), "=") 'Split NetsMid=123456
                    szFieldNameArray(iCount) = szTempArray(0)
                    szFieldValueArray(iCount) = szTempArray(1)
                Next iCount

                For iCount = 0 To szFieldNameArray.GetUpperBound(0)
                    Select Case szFieldNameArray(iCount).Trim().ToLower()
                        Case "merchantcertid"
                            myDebitAck.setCertId(szFieldValueArray(iCount))
                        Case "param1"
                            myDebitAck.setParam1(szFieldValueArray(iCount))
                        Case "param2"
                            myDebitAck.setParam2(szFieldValueArray(iCount))
                        Case "param3"
                            myDebitAck.setParam3(szFieldValueArray(iCount))
                        Case "param4"
                            myDebitAck.setParam4(szFieldValueArray(iCount))
                        Case "param5"
                            myDebitAck.setParam5(szFieldValueArray(iCount))
                        Case "retry"
                            myDebitAck.setRetry(szFieldValueArray(iCount))
                        Case "version"
                            myDebitAck.setVersion(szFieldValueArray(iCount))
                    End Select
                Next iCount

                myTxnAck.setDebitAck(myDebitAck)

                For iCount = 0 To szFieldNameArray.GetUpperBound(0)
                    Select Case szFieldNameArray(iCount).Trim().ToLower()
                        Case "merchantcertid"
                            myTxnAck.setMerchantCertId(szFieldValueArray(iCount))
                        Case "merchanttxnref"
                            myTxnAck.setMerchantTxnRef(szFieldValueArray(iCount))
                        Case "merchanttxnstatus"
                            myTxnAck.setMerchantTxnStatus(szFieldValueArray(iCount))
                        Case "mid"
                            myTxnAck.setMid(szFieldValueArray(iCount))
                        Case "netsmid"
                            myTxnAck.setNetsMid(szFieldValueArray(iCount))
                        Case "netstxnref"
                            myTxnAck.setNetsTxnRef(szFieldValueArray(iCount))
                    End Select
                Next iCount

                szString = myDebitMerchant.formAck(myTxnAck)

            ElseIf ("decrypt" = szMethodArray(0).ToLower()) Then            'decrypt^ENETS response
                myTxnRes = myDebitMerchant.unpackResponse(szMethodArray(1)) 'Decrypt response message from Enets
                myDebitTxnRes = myTxnRes.getDebitTxnRes()

                szString = "netsMid=" + myTxnRes.getNetsMid()
                szString += "&merchantTxnRef=" + myTxnRes.getMerchantTxnRef()
                szString += "&netsTxnStatus=" + myTxnRes.getNetsTxnStatus()
                szString += "&netsTxnRespCode=" + myTxnRes.getNetsTxnRespCode()
                szString += "&netsTxnMsg=" + myTxnRes.getNetsTxnMsg()
                szString += "&BANK_Ref_Code=" + myDebitTxnRes.getBankRefCode()
                szString += "&BANK_Txn_Date=" + myDebitTxnRes.getBankTxnDate()
                szString += "&BANK_Txn_Time=" + myDebitTxnRes.getBankTxnTime()
                szString += "&TxnAmount=" + myDebitTxnRes.getTxnDestAmt()
                szString += "&Source_Curr_Code=" + myDebitTxnRes.getTxnSrcCurrencyCode()
            End If

        Catch ex As Exception
            Throw ex
        End Try

        Return szString
    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szComputeFPXCheckSum
    ' Function Type:	String. Hashing value.
    ' Parameter:		sz_HashStr and sz_CertKey. String values to be sign with key.
    ' Description:		Checksum for Direct FPX
    ' History:			5 May 2015, added  FPXD
    '********************************************************************************************
    Public Function szComputeFPXCheckSum(ByVal sz_HashStr As String, ByVal sz_CertKey As String) As String
        Dim rsaCsp As RSACryptoServiceProvider = Nothing
        Dim dataBytes As Byte() = Nothing
        Dim signatureBytes As Byte() = Nothing

        Try
            rsaCsp = LoadCertificateFile(sz_CertKey)
            dataBytes = Encoding.Default.GetBytes(sz_HashStr)
            signatureBytes = rsaCsp.SignData(dataBytes, "SHA1")

            Return BitConverter.ToString(signatureBytes).Replace("-", Nothing)
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szComputeGPUPOPSignature
    ' Function Type:	String. Hashing value.
    ' Parameter:		sz_HashStr and sz_CertKey. String values to be sign with key.
    ' Description:		Signature for GP UPOP
    ' History:			2 feb 2016, GPUPOP
    '********************************************************************************************
    Public Function szComputeGPUPOPSignature(ByVal szCertPath As String, ByVal szCertPassword As String, ByVal szHasKey As String) As String
        Dim szReturn As String
        Try
            If (szHasKey = "SIGNDATA") Then
                Dim x509_2 As New X509Certificates.X509Certificate2(szCertPath, szCertPassword, X509KeyStorageFlags.MachineKeySet Or X509KeyStorageFlags.Exportable Or X509KeyStorageFlags.PersistKeySet)
                szReturn = BigInteger.Parse(x509_2.SerialNumber, NumberStyles.HexNumber).ToString()
            Else
                Dim szSignDigest As String
                szSignDigest = szComputeSHA1BCHash(szHasKey, "L")
                Dim x509_2 As New X509Certificates.X509Certificate2(szCertPath, szCertPassword, X509KeyStorageFlags.MachineKeySet Or X509KeyStorageFlags.Exportable Or X509KeyStorageFlags.PersistKeySet)
                'Debug
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                       DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                       "Start RSACryptoServiceProvider PrivateKey> + " + szHasKey)

                Dim rsa As RSACryptoServiceProvider = x509_2.PrivateKey
                'Debug
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                       DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                       "Stop RSACryptoServiceProvider PrivateKey> + " + szHasKey)

                Dim hashalg As HashAlgorithm = New SHA1CryptoServiceProvider()
                Dim res As Byte() = Nothing
                res = rsa.SignData(System.Text.Encoding.UTF8.GetBytes(szSignDigest), hashalg)
                szReturn = Convert.ToBase64String(res)
            End If
        Catch ex As Exception
            Throw ex
        End Try
        Return szReturn
    End Function
    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szComputeGPUPOPValidate
    ' Function Type:	String. Hashing value.
    ' Parameter:		sz_HashStr and sz_CertKey. String values to be sign with key.
    ' Description:		Validate for GP UPOP
    ' History:			16 feb 2016, GPUPOP
    '********************************************************************************************
    Public Function szComputeGPUPOPValidate(ByVal szCertPath As String, ByVal szCertPassword As String, ByVal szHasKey As String, ByVal szHostHashValue As String) As String
        Dim szReturn As String
        Dim bValid As Boolean
        Try
            Dim szSignDigest As String
            szSignDigest = szComputeSHA1BCHash(szHasKey, "L")

            Dim x509_2 As New X509Certificates.X509Certificate2(szCertPath)
            Dim szId As String = BigInteger.Parse(x509_2.SerialNumber, NumberStyles.HexNumber).ToString()
            Dim rsa As RSACryptoServiceProvider = Nothing
            If (szId.Equals(szCertPassword)) Then
                rsa = DirectCast(x509_2.PublicKey.Key, RSACryptoServiceProvider)
            Else
                Return Nothing
            End If

            Dim hashalg As HashAlgorithm = New SHA1CryptoServiceProvider()
            Dim res As Byte() = Convert.FromBase64String(szHostHashValue)
            bValid = rsa.VerifyData(Encoding.UTF8.GetBytes(szSignDigest), hashalg, res)
            If (bValid) Then
                szReturn = szHostHashValue
            End If
        Catch ex As Exception
            Throw ex
        End Try
        Return szReturn
    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	LoadCertificateFile
    ' Function Type:	RSACryptoServiceProvider
    ' Parameter:		sz_CertKey. Certificate key path
    ' Description:		Checksum for Direct FPX 
    ' History:			5 May 2015, added  FPXD
    '********************************************************************************************
    Private Shared Function LoadCertificateFile(ByVal sz_CertKey As String) As RSACryptoServiceProvider
        Dim rsaCsp As RSACryptoServiceProvider = Nothing
        Dim btData As Byte() = Nothing
        Dim btRes As Byte() = Nothing

        Using fs As FileStream = File.OpenRead(sz_CertKey)
            btData = New Byte(fs.Length - 1) {}
            fs.Read(btData, 0, btData.Length)

            If btData(0) <> &H30 Then
                btRes = btGetPem("RSA PRIVATE KEY", btData)
            End If

            Try
                rsaCsp = DecodeRSAPrivateKey(btRes)
                Return rsaCsp
            Catch ex As Exception
                Throw ex
            End Try

            Return Nothing
        End Using

    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	btGetPem
    ' Function Type:	Bytes.
    ' Parameter:		sz_Type and sz_Data. 
    ' Description:		Checksum for Direct FPX 
    ' History:			5 May 2015, added  FPXD
    '********************************************************************************************
    Private Shared Function btGetPem(ByVal sz_Type As String, ByVal sz_Data As Byte()) As Byte()
        Dim szPEM As String = ""
        Dim szHeader As String = ""
        Dim szFooter As String = ""
        Dim iStart As Integer = 0
        Dim iEnd As Integer = 0
        Dim szBase64 As String = ""

        szPEM = Encoding.UTF8.GetString(sz_Data)
        szHeader = String.Format("-----BEGIN {0}-----\n", sz_Type)
        szFooter = String.Format("-----END {0}-----", sz_Type)
        iStart = szPEM.IndexOf(szHeader) + szHeader.Length
        iEnd = szPEM.IndexOf(szFooter, iStart)
        szBase64 = szPEM.Substring(iStart, (iEnd - iStart))

        Return Convert.FromBase64String(szBase64)

    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	DecodeRSAPrivateKey
    ' Function Type:	RSACryptoServiceProvider
    ' Parameter:		bt_PrivKey
    ' Description:		Checksum for Direct FPX 
    ' History:			5 May 2015, added  FPXD
    '********************************************************************************************
    Public Shared Function DecodeRSAPrivateKey(ByVal bt_PrivKey As Byte()) As RSACryptoServiceProvider
        Dim MODULUS As Byte() = Nothing
        Dim E As Byte() = Nothing
        Dim D As Byte() = Nothing
        Dim P As Byte() = Nothing
        Dim Q As Byte() = Nothing
        Dim DP As Byte() = Nothing
        Dim DQ As Byte() = Nothing
        Dim IQ As Byte() = Nothing

        ' --------- Set up stream to decode the asn.1 encoded RSA private key ------
        Dim mem As New MemoryStream(bt_PrivKey)
        Dim binr As New BinaryReader(mem)
        'wrap Memory Stream with BinaryReader for easy reading
        Dim bt As Byte = 0
        Dim twobytes As UShort = 0
        Dim elems As Integer = 0

        Try
            twobytes = binr.ReadUInt16()
            If twobytes = &H8130 Then
                'data read as little endian order (actual data order for Sequence is 30 81)
                binr.ReadByte()
                'advance 1 byte
            ElseIf twobytes = &H8230 Then
                binr.ReadInt16()
            Else
                'advance 2 bytes
                Return Nothing
            End If

            twobytes = binr.ReadUInt16()
            If twobytes <> &H102 Then
                'version number
                Return Nothing
            End If
            bt = binr.ReadByte()
            If bt <> &H0 Then
                Return Nothing
            End If


            '------ all private key components are Integer sequences ----
            elems = iGetIntegerSize(binr)
            MODULUS = binr.ReadBytes(elems)

            elems = iGetIntegerSize(binr)
            E = binr.ReadBytes(elems)

            elems = iGetIntegerSize(binr)
            D = binr.ReadBytes(elems)

            elems = iGetIntegerSize(binr)
            P = binr.ReadBytes(elems)

            elems = iGetIntegerSize(binr)
            Q = binr.ReadBytes(elems)

            elems = iGetIntegerSize(binr)
            DP = binr.ReadBytes(elems)

            elems = iGetIntegerSize(binr)
            DQ = binr.ReadBytes(elems)

            elems = iGetIntegerSize(binr)
            IQ = binr.ReadBytes(elems)

            'Console.WriteLine("showing components ..")
            'If verbose Then
            '    showBytes(vbLf & "Modulus", MODULUS)
            '    showBytes(vbLf & "Exponent", E)
            '    showBytes(vbLf & "D", D)
            '    showBytes(vbLf & "P", P)
            '    showBytes(vbLf & "Q", Q)
            '    showBytes(vbLf & "DP", DP)
            '    showBytes(vbLf & "DQ", DQ)
            '    showBytes(vbLf & "IQ", IQ)
            'End If

            ' ------- create RSACryptoServiceProvider instance and initialize with public key -----
            Dim CspParameters As New CspParameters()
            CspParameters.Flags = CspProviderFlags.UseMachineKeyStore

            Dim RSA As New RSACryptoServiceProvider(1024, CspParameters)
            Dim RSAparams As New RSAParameters()

            RSAparams.Modulus = MODULUS
            RSAparams.Exponent = E
            RSAparams.D = D
            RSAparams.P = P
            RSAparams.Q = Q
            RSAparams.DP = DP
            RSAparams.DQ = DQ
            RSAparams.InverseQ = IQ
            RSA.ImportParameters(RSAparams)

            Return RSA

        Catch ex As Exception
            Throw ex
            Return Nothing
        Finally
            binr.Close()
        End Try

    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	iGetIntegerSize
    ' Function Type:	Integer
    ' Parameter:		br_binr
    ' Description:		Checksum for Direct FPX 
    ' History:			5 May 2015, added  FPXD
    '********************************************************************************************
    Private Shared Function iGetIntegerSize(ByVal br_binr As BinaryReader) As Integer
        Dim bt As Byte = 0
        Dim lowbyte As Byte = &H0
        Dim highbyte As Byte = &H0
        Dim count As Integer = 0

        'Modified 19 Apr 2019 Joyce, as PCI requestes to add try catch to handle exception
        Try
            bt = br_binr.ReadByte()
            If bt <> &H2 Then
                'expect integer
                Return 0
            End If
            bt = br_binr.ReadByte()

            If bt = &H81 Then
                count = br_binr.ReadByte()
                ' data size in next byte
            ElseIf bt = &H82 Then
                highbyte = br_binr.ReadByte()
                ' data size in next 2 bytes
                lowbyte = br_binr.ReadByte()
                Dim modint As Byte() = {lowbyte, highbyte, &H0, &H0}
                count = BitConverter.ToInt32(modint, 0)
            Else
                ' we already have the data size
                count = bt
            End If

            While br_binr.ReadByte() = &H0
                'remove high order zeros in data
                count -= 1
            End While

            br_binr.BaseStream.Seek(-1, SeekOrigin.Current)
            'last ReadByte wasn't a removed zero, so back up a byte
        Catch ex As Exception
            Throw ex
        End Try

        Return count

    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szVerifyFPXCheckSum
    ' Function Type:	String. Hashing value.
    ' Parameter:		sz_CheckSum and sz_CertKey. String values to be verify with key.
    ' Description:		Verify checksum from Direct FPX
    ' History:			8 May 2015, added  FPXD
    '********************************************************************************************
    Public Function szVerifyFPXCheckSum(ByVal sz_RespMsg As String, ByVal sz_CheckSum As String, ByVal sz_CertKey As String) As String
        Dim szCert As String = ""
        Dim szRetMsg As String = ""
        Dim btData As Byte() = Nothing
        Dim btSignData As Byte() = Nothing
        Dim rsaEncryptor As RSACryptoServiceProvider = Nothing

        Try
            szCert = sz_CertKey

            Dim x509_2 As New X509Certificates.X509Certificate2(szCert)

            btData = Encoding.Default.GetBytes(sz_RespMsg)
            btSignData = btHexToByte(sz_CheckSum)
            rsaEncryptor = DirectCast(x509_2.PublicKey.Key, RSACryptoServiceProvider)

            If rsaEncryptor.VerifyData(btData, "SHA1", btSignData) Then
                szRetMsg = "0"  'Modified 20 May 2015, return hash verification result instead of hash value
            Else
                szRetMsg = "1"  'Modified 20 May 2015, return hash verification result instead of hash value
            End If

        Catch ex As Exception
            'szRetMsg = "ERROR :" + e.Message
            Throw ex
        End Try

        Return szRetMsg

    End Function

    ''********************************************************************************************
    '' Class Name:		Hash
    '' Function Name:	szComputeBCAKPHash
    '' Function Type:	String. Hashing value.
    '' Parameter:		sz_Key. String values to be hash.
    '' Description:		Hash value for BCA KlikPay
    '' History:			15 June 2011, added 
    ''********************************************************************************************
    'Public Function szComputeBCAKPHash(ByVal sz_Key As String) As String
    '    Dim szHashValue As String = ""
    '    Dim aszParams() As String

    '    Dim szClearKey As String = ""    'Provided by BCA (stored as password in database)
    '    Dim szKlikPayCode As String = "" 'PayeeCode
    '    Dim szTxnID As String = ""       'GatewayTxnID
    '    Dim szCurrency As String = ""    'CurrencyCode
    '    Dim iDate As Integer = 0
    '    Dim iTxnAmt As Integer = 0

    '    Dim szKeyId As String = ""
    '    Dim szHash1 As String = ""
    '    Dim szHash2 As String = ""
    '    Dim szHash3 As String = ""
    '    Dim szHash4 As String = ""

    '    Dim iLoop As Integer = 0

    '    If ("" = sz_Key) Then
    '        Return szHashValue
    '    End If

    '    Try
    '        'sz_Key = KlikPayToAirAsia-UATAIR-A07PZ3JEN000000756-IDR-15062011-1.00
    '        aszParams = sz_Key.Split("-")

    '        For iLoop = 0 To aszParams.GetUpperBound(0)
    '            Select Case iLoop.ToString()
    '                Case "0"
    '                    szClearKey = aszParams(iLoop).Trim()
    '                Case "1"
    '                    szKlikPayCode = aszParams(iLoop).Trim()
    '                Case "2"
    '                    szTxnID = aszParams(iLoop).Trim()
    '                Case "3"
    '                    szCurrency = aszParams(iLoop).Trim()
    '                Case "4"
    '                    iDate = Convert.ToInt32(aszParams(iLoop).Trim())
    '                Case "5"
    '                    'BCA's requirement is to have Amount to be integer, no decimal, for hash calculation
    '                    iTxnAmt = CType(aszParams(iLoop).Trim(), Integer)
    '            End Select
    '        Next

    '        'Generate KeyId for hashing
    '        szKeyId = szStringToHex(szClearKey)

    '        'Generate Hash1 - Combine KlikPayCode, transactionNumber, currency, keyid
    '        szHash1 = szKlikPayCode + szTxnID + szCurrency + szKeyId

    '        'Generate Hash2 - Adding transactionTime(Date) + totalAmount
    '        szHash2 = iDate + iTxnAmt

    '        'Generate Hash3 & Hash4 - Loop Hash1 & Hash2; VB.net will have overflow, therefore, use DDGWlib.dll written in C#
    '        szHash3 = Arithmetic.BCAalgorithm(szHash1)

    '        szHash4 = Arithmetic.BCAalgorithm(szHash2)

    '        'Final hash result
    '        szHashValue = Arithmetic.Add_CS(szHash3, szHash4) 'use Arithmetic to do add function to avoid overflow error in VB.Net

    '    Catch ex As Exception
    '        Throw ex
    '    End Try

    '    Return szHashValue
    'End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szBubbleSort
    ' Function Type:	String
    ' Parameter:		sz_Str - String to be sorted
    ' Description:		Generate bubble-sorted string
    ' History:			21 Aug 2009
    '********************************************************************************************
    Public Function szBubbleSort(ByVal sz_Str() As String) As String()
        Dim i As Integer, j As Integer
        Dim szTemp As String

        Dim bExchanged As Boolean

        For i = 0 To sz_Str.Length - 1
            bExchanged = False

            For j = sz_Str.Length - 2 To i Step -1
                If String.CompareOrdinal(sz_Str(j + 1), sz_Str(j)) < 0 Then
                    szTemp = sz_Str(j + 1)
                    sz_Str(j + 1) = sz_Str(j)
                    sz_Str(j) = szTemp

                    bExchanged = True
                End If
            Next

            If Not bExchanged Then
                Exit For
            End If
        Next

        Return sz_Str
    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szEncrypt
    ' Function Type:	Return encrypted text in HEX
    ' Parameter:		Clear Text, Key, Initialization Vector
    ' Description:		Encrypt a clear text into cipher text using 3DES (PKCS5, PKCS7 padding and CBC cipher mode)
    ' History:			16 Mar 2009
    '********************************************************************************************
    Public Function szEncrypt3DES(ByVal sz_ClearText As String, ByVal sz_Key As String, ByVal sz_InitVector As String) As String
        Dim obj3DES As New TripleDESCryptoServiceProvider
        Dim btInput() As Byte
        Dim btOutput() As Byte

        Try
            szEncrypt3DES = ""

            obj3DES.Padding = PaddingMode.PKCS7 ' Default value
            obj3DES.Mode = CipherMode.CBC       ' Default value
            obj3DES.Key = btHexToByte(sz_Key)

            If (False = bValidHex(sz_InitVector)) Then
                sz_InitVector = szStringToHex(sz_InitVector)
            End If
            obj3DES.IV = btHexToByte(sz_InitVector)

            'Added bValidHex  29 Mar 2009
            If (False = bValidHex(sz_ClearText)) Then
                sz_ClearText = szStringToHex(sz_ClearText)
            End If
            btInput = btHexToByte(sz_ClearText)

            btOutput = btTransform(btInput, obj3DES.CreateEncryptor())

        Catch ex As Exception
            Throw ex
        Finally
            If Not obj3DES Is Nothing Then obj3DES = Nothing
        End Try

        Return szWriteHex(btOutput)
    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szDecrypt
    ' Function Type:	Return clear text in HEX
    ' Parameter:		Cipher Text, Key, Initialization Vector
    ' Description:		Decrypt a cipher text into clear text using 3DES (PKCS5, PKCS7 padding and CBC cipher mode)
    ' History:			16 Mar 2009
    '********************************************************************************************
    Public Function szDecrypt3DES(ByVal sz_CipherText As String, ByVal sz_Key As String, ByVal sz_InitVector As String) As String
        Dim obj3DES As New TripleDESCryptoServiceProvider
        Dim objUTF8 As New UTF8Encoding
        Dim btInput() As Byte
        Dim btOutput() As Byte

        Try
            szDecrypt3DES = ""

            obj3DES.Padding = PaddingMode.PKCS7 ' Default value
            obj3DES.Mode = CipherMode.CBC       ' Default value
            obj3DES.Key = btHexToByte(sz_Key)

            If (False = bValidHex(sz_InitVector)) Then
                sz_InitVector = szStringToHex(sz_InitVector)
            End If
            obj3DES.IV = btHexToByte(sz_InitVector)

            'Added on 4 May 2009, to remove LF or CRLF
            'or else will encounter exception "Index was outside the bounds of the array" in btHextoByte
            'e.g. PBB Query reply contains CRLF or LF, causing its length become not genap but ganjil
            sz_CipherText = Replace(sz_CipherText, vbCrLf, "")
            sz_CipherText = Replace(sz_CipherText, vbCr, "")
            sz_CipherText = Replace(sz_CipherText, vbLf, "")

            If (False = bValidHex(sz_CipherText)) Then
                sz_CipherText = szStringToHex(sz_CipherText)
            End If
            btInput = btHexToByte(sz_CipherText)

            btOutput = btTransform(btInput, obj3DES.CreateDecryptor())

            'Comment on 25 May 2009, use UTF GetString if encrypted value's clear texts are not hexadecimal value,
            'e.g. BillRef1=ABC&Amt=123.99
            szDecrypt3DES = objUTF8.GetString(btOutput)

        Catch ex As Exception
            Throw ex
        Finally
            If Not obj3DES Is Nothing Then obj3DES = Nothing
            If Not objUTF8 Is Nothing Then objUTF8 = Nothing
        End Try
    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szDecrypt
    ' Function Type:	Return clear text in HEX
    ' Parameter:		Cipher Text, Key, Initialization Vector
    ' Description:		Decrypt a cipher text into clear text using 3DES (PKCS5, PKCS7 padding and CBC cipher mode)
    ' History:			25 May 2009
    '********************************************************************************************
    Public Function szDecrypt3DEStoHex(ByVal sz_CipherText As String, ByVal sz_Key As String, ByVal sz_InitVector As String) As String
        Dim obj3DES As New TripleDESCryptoServiceProvider
        Dim objUTF8 As New UTF8Encoding
        Dim btInput() As Byte
        Dim btOutput() As Byte

        Try
            szDecrypt3DEStoHex = ""

            obj3DES.Padding = PaddingMode.PKCS7 ' Default value
            obj3DES.Mode = CipherMode.CBC       ' Default value
            obj3DES.Key = btHexToByte(sz_Key)

            If (False = bValidHex(sz_InitVector)) Then
                sz_InitVector = szStringToHex(sz_InitVector)
            End If
            obj3DES.IV = btHexToByte(sz_InitVector)

            'Added on 4 May 2009, to remove LF or CRLF
            'or else will encounter exception "Index was outside the bounds of the array" in btHextoByte
            'e.g. PBB Query reply contains CRLF or LF, causing its length become not genap but ganjil
            sz_CipherText = Replace(sz_CipherText, vbCrLf, "")
            sz_CipherText = Replace(sz_CipherText, vbCr, "")
            sz_CipherText = Replace(sz_CipherText, vbLf, "")

            If (False = bValidHex(sz_CipherText)) Then
                sz_CipherText = szStringToHex(sz_CipherText)
            End If
            btInput = btHexToByte(sz_CipherText)

            btOutput = btTransform(btInput, obj3DES.CreateDecryptor())

            'Comment on 25 May 2009, convert bytes to hex if encrypted value's clear texts are hexadecimal value
            szDecrypt3DEStoHex = szWriteHex(btOutput)

        Catch ex As Exception
            Throw ex
        Finally
            If Not obj3DES Is Nothing Then obj3DES = Nothing
            If Not objUTF8 Is Nothing Then objUTF8 = Nothing
        End Try
    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szDecrypt3DEStoHexCase
    ' Function Type:	Return clear text in HEX and in the specified case
    ' Parameter:		Cipher Text, Key, Initialization Vector
    ' Description:		Decrypt a cipher text into clear text using 3DES (PKCS5, PKCS7 padding and CBC cipher mode)
    ' History:			Added  18 Jul 2014. GlobalPay CyberSource
    '********************************************************************************************
    Public Function szDecrypt3DEStoHexCase(ByVal sz_CipherText As String, ByVal sz_Key As String, ByVal sz_InitVector As String, ByVal sz_Case As String) As String
        Dim obj3DES As New TripleDESCryptoServiceProvider
        Dim objUTF8 As New UTF8Encoding
        Dim btInput() As Byte
        Dim btOutput() As Byte

        Try
            szDecrypt3DEStoHexCase = ""

            obj3DES.Padding = PaddingMode.PKCS7 ' Default value
            obj3DES.Mode = CipherMode.CBC       ' Default value
            obj3DES.Key = btHexToByte(sz_Key)

            If (False = bValidHex(sz_InitVector)) Then
                sz_InitVector = szStringToHex(sz_InitVector)
            End If
            obj3DES.IV = btHexToByte(sz_InitVector)

            'Added on 4 May 2009, to remove LF or CRLF
            'or else will encounter exception "Index was outside the bounds of the array" in btHextoByte
            'e.g. PBB Query reply contains CRLF or LF, causing its length become not genap but ganjil
            sz_CipherText = Replace(sz_CipherText, vbCrLf, "")
            sz_CipherText = Replace(sz_CipherText, vbCr, "")
            sz_CipherText = Replace(sz_CipherText, vbLf, "")

            If (False = bValidHex(sz_CipherText)) Then
                sz_CipherText = szStringToHex(sz_CipherText)
            End If
            btInput = btHexToByte(sz_CipherText)

            btOutput = btTransform(btInput, obj3DES.CreateDecryptor())

            'Comment on 25 May 2009, convert bytes to hex if encrypted value's clear texts are hexadecimal value
            If ("L" = sz_Case.ToUpper()) Then
                szDecrypt3DEStoHexCase = szWriteHex(btOutput).ToLower()
            ElseIf ("U" = sz_Case.ToUpper) Then
                szDecrypt3DEStoHexCase = szWriteHex(btOutput).ToUpper()
            End If

        Catch ex As Exception
            Throw ex
        Finally
            If Not obj3DES Is Nothing Then obj3DES = Nothing
            If Not objUTF8 Is Nothing Then objUTF8 = Nothing
        End Try
    End Function

    Public Function szEncryptAES(ByVal sz_ClearText As String, ByVal sz_Key As String) As String
        Dim szRet As String = ""

        Try
            Dim szMethod As String = ConfigurationManager.AppSettings("encryptmethod")
            If szMethod = "1" Then
                szRet = szEncryptAESHSM(sz_ClearText, sz_Key)
                szRet = "#$~" + szRet
            Else
                szRet = szEncryptAESRijndael(sz_ClearText, sz_Key)
            End If
        Catch ex As Exception
            Throw ex
        Finally

        End Try
        Return szRet
    End Function

    Public Function szDecryptAES(ByVal sz_ClearText As String, ByVal sz_Key As String) As String
        Dim szRet As String = ""

        Try
            If (sz_ClearText.StartsWith("#$~")) Then
                sz_ClearText = sz_ClearText.Substring(3, sz_ClearText.Length - 3)
                szRet = szDecryptAESHSM(sz_ClearText, sz_Key)
            Else
                szRet = szDecryptAESRijndael(sz_ClearText, sz_Key)
            End If

        Catch ex As Exception
            Throw ex
        Finally

        End Try

        Return szRet
    End Function

    Public Function szEncryptAESRijndael(ByVal sz_ClearText As String, ByVal sz_Key As String) As String
        Dim szRet As String = ""
        Dim objRijndael As New Crypto.Rijndael(sz_Key)

        Try
            szRet = objRijndael.EncryptStringAES(sz_ClearText)
        Catch ex As Exception
            Throw ex
        Finally
            objRijndael = Nothing
        End Try

        Return szRet
    End Function

    Public Function szDecryptAESRijndael(ByVal sz_ClearText As String, ByVal sz_Key As String) As String
        Dim szRet As String = ""
        Dim objRijndael As New Crypto.Rijndael(sz_Key)

        Try
            szRet = objRijndael.DecryptStringAES(sz_ClearText)
        Catch ex As Exception
            Throw ex
        Finally
            objRijndael = Nothing
        End Try

        Return szRet
    End Function

    'HSM Encry/Decry 
    Public Function szEncryptAESHSM(ByVal sz_ClearText As String, ByVal sz_Key As String) As String
        Dim szRet As String = ""

        Try
            'objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
            '            DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
            '            " EncryptAESHSM > (" + sz_ClearText + ")")

            If String.IsNullOrEmpty(Settings.NativeLibPath) Then
                Settings.NativeLibPath = ConfigurationManager.AppSettings("hsmpath")
                Settings.SlotId = Int32.Parse(ConfigurationManager.AppSettings("hsmslotid"))
                Try
                    Settings.UserPin = szDecryptAESRijndael(ConfigurationManager.AppSettings("hsmuserpin"), Common.C_3DESAPPKEY)
                Catch
                    Settings.UserPin = ConfigurationManager.AppSettings("hsmuserpin")
                End Try
                Try
                    Settings.KeyLabel = szDecryptAESRijndael(ConfigurationManager.AppSettings("hsmkeylabel"), Common.C_3DESAPPKEY)
                Catch
                    Settings.KeyLabel = ConfigurationManager.AppSettings("hsmkeylabel")
                End Try
            End If
            szRet = NetAPI.EncryptWithKey(sz_ClearText, Settings.KeyLabel, Settings.SlotId)

            'objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
            '            DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
            '            " EncryptAESHSM > (" + sz_ClearText + ") get (" + szRet + ") with DLL (" + Settings.NativeLibPath + ")slot (" + Settings.SlotId.ToString() + ")label (" + Settings.KeyLabel + ")")
        Catch ex As Exception
            Throw ex
        Finally
        End Try
        Return szRet
    End Function

    Public Function szDecryptAESHSM(ByVal sz_ClearText As String, ByVal sz_Key As String) As String
        Dim szRet As String = ""

        Try
            'objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
            '            DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
            '            " DecryptAESHSM > (" + sz_ClearText + ")")

            If String.IsNullOrEmpty(Settings.NativeLibPath) Then
                Settings.NativeLibPath = ConfigurationManager.AppSettings("hsmpath")
                Settings.SlotId = Int32.Parse(ConfigurationManager.AppSettings("hsmslotid"))
                Try
                    Settings.UserPin = szDecryptAESRijndael(ConfigurationManager.AppSettings("hsmuserpin"), Common.C_3DESAPPKEY)
                Catch
                    Settings.UserPin = ConfigurationManager.AppSettings("hsmuserpin")
                End Try
                Try
                    Settings.KeyLabel = szDecryptAESRijndael(ConfigurationManager.AppSettings("hsmkeylabel"), Common.C_3DESAPPKEY)
                Catch
                    Settings.KeyLabel = ConfigurationManager.AppSettings("hsmkeylabel")
                End Try
            End If
            szRet = NetAPI.DecryptWithKey(sz_ClearText, Settings.KeyLabel, Settings.SlotId)

            'objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
            '            DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
            '            " DecryptAESHSM > (" + sz_ClearText + ") get (" + szRet + ") with DLL (" + Settings.NativeLibPath + ")slot (" + Settings.SlotId.ToString() + ")label (" + Settings.KeyLabel + ")")
        Catch ex As Exception
            Throw ex
        Finally

        End Try

        Return szRet
    End Function

    '********************************************************************************************
    ' Class Name:		Hash  (PBEMD5Des)
    ' Function Name:	szEncryptPBEMD5Des
    ' Function Type:	String. Encrypted value.
    ' Parameter:		sz_Data. String values to be encrypted.
    ' Description:		Encrypt data for Alliance Bank. ALB.
    ' History:			4 Dec 2013, added 
    '********************************************************************************************
    Public Function szEncryptPBEMD5Des(ByVal sz_Data As String, ByVal st_PayInfo As Common.STPayInfo, ByVal st_HostInfo As Common.STHost) As String
        Dim szOutput As String = ""
        Dim szMD5Output As String = ""
        'Dim szPwd As String = ""       'Modified JOYCE 04 Apr 2019, rename szPwd to szHostMID
        Dim szHostMID As String = ""
        Dim szHostKey As String = ""
        Dim aPwd As Char()
        Dim szAsciiMID As String = ""

        Try
            'Need to check is st_HostInfo.szSendKey is empty or not
            'This is because Sales request SendKey is included in hashvalue string
            'Where Sales response SendKey is not included and need to be retrieved using bGetTerminal
            If ("" = st_PayInfo.szEncryptKeyPath) Then 'Commented  24 Jul 2014, "" = st_HostInfo.szSendKey Then
                If True <> objCommon.bGetTerminal(st_HostInfo, st_PayInfo) Then
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(), " > szEncryptPBEMD5Des() Get SendKey Failed")
                    Return False
                End If
            End If

            'Commented  24 Jul 2014, due to certain px_ref key assigned by Alliance Bank to merchant can be only decrypted using Decrypt3DESHEX
            'szHostKey = szDecrypt3DES(st_HostInfo.szSendKey, Common.C_3DESAPPKEY, Common.C_3DESAPPVECTOR) 'PX_Ref field is password for merchant verification

            szHostKey = st_PayInfo.szEncryptKeyPath 'Terminal's send key path
            szHostMID = st_HostInfo.szMID + szHostKey   'szPwd = st_HostInfo.szMID + szHostKey      'Modified JOYCE 04 Apr 2019, rename szPwd to szHostMID

            aPwd = szHostMID.ToCharArray()
            szOutput = PBE.EncryptPBEMD5DES(aPwd, sz_Data, "PBEWithMD5AndDES")
            szOutput = szOutput.Replace(vbCrLf, "").Replace(vbLf, "")

            Return szOutput

        Catch ex As Exception
            Throw ex
        End Try

    End Function

    '********************************************************************************************
    ' Class Name:		Hash  (PBEMD5Des)
    ' Function Name:	szDecryptPBEMD5Des
    ' Function Type:	String. Decrypted value.
    ' Parameter:		sz_Data. String values to be decrypted.
    ' Description:		Decrypt data for Alliance Bank. ALB.
    ' History:			4 Dec 2013, added 
    '********************************************************************************************
    Public Function szDecryptPBEMD5Des(ByVal sz_Data As String, ByVal st_PayInfo As Common.STPayInfo, ByVal st_HostInfo As Common.STHost) As String
        Dim szOutput As String = ""
        Dim szMD5Output As String = ""
        'Dim szPwd As String = ""   'Modified JOYCE 04 Apr 2019, rename szPwd to szHostMID
        Dim szHostMID As String = ""
        Dim szHostKey As String = ""
        Dim aPwd As Char()
        Dim szAsciiMID As String = ""

        Try
            'Commented  24 Jul 2014, due to px_ref key assigned by Alliance to some merchants can be only decrypted using Decrypt3DESHEX
            'szHostKey = szDecrypt3DES(st_HostInfo.szSendKey, Common.C_3DESAPPKEY, Common.C_3DESAPPVECTOR) 'PX_Ref field is password for merchant verification

            'Added, 24 Jul 2014
            szHostKey = st_PayInfo.szEncryptKeyPath
            'Decrypt use clear(original from db) mid
            szHostMID = st_HostInfo.szMID + szHostKey   'szPwd = st_HostInfo.szMID + szHostKey      'Modified JOYCE 04 Apr 2019, rename szPwd to szHostMID

            aPwd = szHostMID.ToCharArray()
            szOutput = PBE.DecryptPBEMD5DES(aPwd, sz_Data, "PBEWithMD5AndDES")

            Return szOutput
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    '********************************************************************************************
    ' Class Name:		Hash  (ALB)
    ' Function Name:	szALBEncryptAES
    ' Function Type:	String. Encrypt string
    ' Parameter:		sz_Data. String to encrypt. 
    ' Description:		Get Encrypt string.
    ' History:			18 April 2014
    '********************************************************************************************
    Public Function szALBEncryptAES(ByVal sz_Data As String, ByVal st_PayInfo As Common.STPayInfo, ByVal st_HostInfo As Common.STHost) As String
        Dim szOutput As String = ""
        Dim szMD5Output As String = ""
        'Dim szPwd As String = ""       'Modified JOYCE 04 Apr 2019, rename szPwd to szHostMID
        Dim szHostMID As String = ""
        Dim szHostKey As String = ""
        Dim aPwd() As Byte
        Dim szAsciiMID As String = ""

        Try
            If ("" = st_PayInfo.szEncryptKeyPath) Then
                If True <> objCommon.bGetTerminal(st_HostInfo, st_PayInfo) Then
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(), " > szALBEncryptAES() Get SendKey Failed")
                    Return False
                End If
            End If

            szHostKey = st_PayInfo.szEncryptKeyPath 'Terminal's send key path

            'Added  27 Aug 2017. Masterpass
            If ("M" = st_PayInfo.szGatewayID Or "V" = st_PayInfo.szGatewayID) Then
                'Regenerate st_HostInfo.szMID by ASCIIconter instead of original MID from terminals table.
                'Response iGetQueryString. szMID generated had overwriten by original MID in [sendkeypath]'s bGetTerminal
                szAsciiMID = szASCIICounter(st_PayInfo.szTxnID, st_HostInfo.szMID)
                st_HostInfo.szMID = szAsciiMID.PadLeft(13, "0") 'Currently follow ALB template ASCII13
            End If

            'Commented  24 Jul 2014, due to certain px_ref key assigned by Alliance Bank to merchant can be only decrypted using Decrypt3DESHEX
            'szHostKey = szDecrypt3DES(st_HostInfo.szSendKey, Common.C_3DESAPPKEY, Common.C_3DESAPPVECTOR) 'PX_Ref field is password for merchant verification

            szHostMID = st_HostInfo.szMID + szHostKey       'szPwd = st_HostInfo.szMID + szHostKey      'Modified JOYCE 04 Apr 2019, rename szPwd to szHostMID

            aPwd = Encoding.ASCII.GetBytes(szHostMID.ToCharArray)
            szOutput = JavaCrypto.AES.ALBEncrypt(sz_Data, aPwd)         'Modified 17 Jun 2015, added JavaCrypto else will have AES is ambiguous error in VS 2012
            szOutput = szOutput.Replace(vbCrLf, "").Replace(vbLf, "")

            Return szOutput

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    '********************************************************************************************
    ' Class Name:		Hash  (ALB)
    ' Function Name:	szALBDecryptAES
    ' Function Type:	String. Decrypt string
    ' Parameter:		sz_Data. String to decrypt. 
    ' Description:		Get Decrypt string.
    ' History:			18 April 2014
    '********************************************************************************************
    Public Function szALBDecryptAES(ByVal sz_Data As String, ByVal st_PayInfo As Common.STPayInfo, ByVal st_HostInfo As Common.STHost) As String
        Dim szOutput As String = ""
        Dim szMD5Output As String = ""
        'Dim szPwd As String = ""       'Modified JOYCE 04 Apr 2019, rename szPwd to szHostMID
        Dim szHostMID As String = ""
        Dim szHostKey As String = ""
        Dim aPwd() As Byte
        Dim szAsciiMID As String = ""

        Try
            'szHostKey = szDecrypt3DES(st_HostInfo.szReturnKey, Common.C_3DESAPPKEY, Common.C_3DESAPPVECTOR) 'PX_Ref field is password for merchant verification
            szHostKey = st_PayInfo.szEncryptKeyPath

            'Decrypt use clear(original from db) mid
            szHostMID = st_HostInfo.szMID + szHostKey       'szPwd = st_HostInfo.szMID + szHostKey      'Modified JOYCE 04 Apr 2019, rename szPwd to szHostMID

            aPwd = Encoding.ASCII.GetBytes(szHostMID.ToCharArray())

            szOutput = JavaCrypto.AES.ALBDecrypt(sz_Data, aPwd) 'Modified 17 Jun 2015, added JavaCrypto else will have AES is ambiguous error in VS 2012

            Return szOutput
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szComputeHMACSHA256Hash
    ' Function Type:	String. Hash value generated using HMAC SHA-256 algorithm.
    ' Parameter:		sz_Key. Values to be hash.
    ' Description:		Get hash value generated using HMAC SHA-256 algorithm
    ' History:			Added  on 1 July 2014. MIGS. Modified  6 Oct 2016
    '********************************************************************************************
    Public Function szComputeHMACSHA256Hash(ByVal sz_Key As String, ByVal st_HostInfo As Common.STHost, ByVal sz_Case As String) As String
        Dim szHashValue As String = ""
        Dim szSecretKey As String = ""
        Dim aSecretKey As Byte()
        Dim aUTF8Bytes As Byte()
        Dim aISO8859Bytes As Byte()
        Dim aHashBytes As Byte()

        Try
            'If ("" <> st_HostInfo.szSecretKey) Then
            szSecretKey = szDecrypt3DEStoHex(st_HostInfo.szSendKey, Common.C_3DESAPPKEY, Common.C_3DESAPPVECTOR)
            'End If

            aSecretKey = New Byte(szSecretKey.Length / 2 - 1) {}
            For i As Integer = 0 To szSecretKey.Length / 2 - 1
                aSecretKey(i) = CByte(Int32.Parse(szSecretKey.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber))
            Next

            Dim objHMACSHA256 As New HMACSHA256(aSecretKey)

            aUTF8Bytes = Encoding.UTF8.GetBytes(sz_Key)
            aISO8859Bytes = Encoding.Convert(Encoding.UTF8, Encoding.GetEncoding("iso-8859-1"), aUTF8Bytes)
            aHashBytes = objHMACSHA256.ComputeHash(aISO8859Bytes)
            szHashValue = szWriteHex(aHashBytes)

            If ("L" = sz_Case.ToUpper()) Then
                szHashValue = szHashValue.ToLower()
            ElseIf ("U" = sz_Case.ToUpper()) Then
                szHashValue = szHashValue.ToUpper()
            End If

            objHMACSHA256.Clear()

        Catch ex As Exception
            Throw ex
        End Try

        Return szHashValue
    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szComputeHMACSHA256HEXHash
    ' Function Type:	String. Hash value generated using HMAC SHA-256 algorithm with HEX.
    ' Parameter:		sz_Key. Values to be hash.
    ' Description:		Get hash value generated using HMAC SHA-256 algorithm with HEX
    ' History:			Added  on 1 July 2014. GlobalPay Cybersource
    '********************************************************************************************
    Public Function szComputeHMACSHA256HEXHash(ByVal sz_Key As String, ByVal st_HostInfo As Common.STHost, ByVal sz_Case As String) As String
        Dim szHashValue As String = ""
        Dim szSecretKey As String = ""
        Dim encoding As New System.Text.UTF8Encoding

        Try
            szSecretKey = szDecrypt3DEStoHexCase(st_HostInfo.szSecretKey, Common.C_3DESAPPKEY, Common.C_3DESAPPVECTOR, "L")

            Dim objHMACSHA256 As New HMACSHA256(encoding.GetBytes(szSecretKey))
            szHashValue = System.Convert.ToBase64String(objHMACSHA256.ComputeHash(encoding.GetBytes(sz_Key)))

            If ("L" = sz_Case.ToUpper()) Then
                szHashValue = szHashValue.ToLower()
            ElseIf ("U" = sz_Case.ToUpper()) Then
                szHashValue = szHashValue.ToUpper()
            End If

            objHMACSHA256.Clear()

        Catch ex As Exception
            Throw ex
        End Try

        Return szHashValue
    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szComputeHMACSHA256HEXHash
    ' Function Type:	String. Hash value generated using HMAC SHA-256 algorithm with HEX.
    ' Parameter:		sz_Key. Values to be hash.
    ' Description:		Get hash value generated using HMAC SHA-256 algorithm with HEX
    ' History:			Added  on 1 July 2014. GlobalPay Cybersource
    '********************************************************************************************
    Public Function szComputeHMACSHA256HEXHashKeyCase(ByVal sz_Key As String, ByVal st_HostInfo As Common.STHost, ByVal sz_Case As String, ByVal sz_KeyCase As String) As String
        Dim szHashValue As String = ""
        Dim szSecretKey As String = ""
        Dim encoding As New System.Text.UTF8Encoding

        Try
            szSecretKey = szDecrypt3DEStoHexCase(st_HostInfo.szSecretKey, Common.C_3DESAPPKEY, Common.C_3DESAPPVECTOR, sz_KeyCase)

            'Added, 4 Mar 2015, for testing, Modified 24 Sept 2015, removed logging of SecretKey
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
            DebugInfo.__METHOD__(), DebugInfo.__LINE__(), " > GP Hash[" + sz_Key + "]")

            Dim objHMACSHA256 As New HMACSHA256(encoding.GetBytes(szSecretKey))
            szHashValue = System.Convert.ToBase64String(objHMACSHA256.ComputeHash(encoding.GetBytes(sz_Key)))

            If ("L" = sz_Case.ToUpper()) Then
                szHashValue = szHashValue.ToLower()
            ElseIf ("U" = sz_Case.ToUpper()) Then
                szHashValue = szHashValue.ToUpper()
            End If

            objHMACSHA256.Clear()

        Catch ex As Exception
            Throw ex
        End Try

        Return szHashValue
    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szComputeGPAYHash
    ' Function Type:	String. Hash value generated using HMAC SHA-256 algorithm with HEX.
    ' Parameter:		sz_Key. Values to be hash.
    ' Description:		Get hash value generated using HMAC SHA-256 algorithm with HEX
    ' History:			Added  on 7 July 2014. GlobalPay Cybersource
    '********************************************************************************************
    Public Function szComputeGPAYHash(ByVal sz_Key As String, ByVal st_HostInfo As Common.STHost, ByVal sz_Case As String) As String
        Dim aszHostFieldName() As String = Nothing
        Dim szHostFieldValue As String = ""
        Dim szHashValue As String = ""
        Dim szSecretKey As String = ""
        Dim szHashString As String = ""
        Dim szValue As String = ""  'Added, 10 Feb 2015
        Dim szTemp As String = ""   'Added, 6 Mar 2015

        Try
            aszHostFieldName = Split(st_HostInfo.szReserved, ",")

            For j = 0 To aszHostFieldName.Length - 1
                'Added, 10 Feb 2015, because HTMLDecode and URLDecode are not able to decode " " to "+"
                'Modified 16 Apr 2015, added checking of "request_token"
                'Modified 20 May 2015, added checking of "payer_authentication_cavv"
                If ("payer_authentication_xid" = aszHostFieldName(j).ToLower() Or "payer_authentication_uad" = aszHostFieldName(j).ToLower() Or "request_token" = aszHostFieldName(j).ToLower() Or "payer_authentication_cavv" = aszHostFieldName(j).ToLower()) Then
                    szValue = Replace(Server.UrlDecode(HttpContext.Current.Request(aszHostFieldName(j))), " ", "+")
                ElseIf ("payer_authentication_proof_xml" = aszHostFieldName(j).ToLower()) Then 'Added, 6 Mar 2015, to solve possibility to have acctid value containing " "
                    'Stores original full XML before being HTML Decoded, e.g. &lt;acctID&gt; WIrBKz3Rzi5HTaowv EvQgDAgk=&lt;/acctID&gt;
                    szValue = HttpContext.Current.Request(aszHostFieldName(j))
                    'Finds the value of acctID tag
                    szTemp = objCommon.szGetTagValue(Server.HtmlDecode(HttpContext.Current.Request(aszHostFieldName(j))), "acctID")
                    szValue = Replace(szValue, szTemp, Replace(szTemp, " ", "+"))
                Else
                    szValue = Server.UrlDecode(HttpContext.Current.Request(aszHostFieldName(j)))
                End If

                If (j = aszHostFieldName.Length - 1) Then
                    'Replaced Server.UrlDecode(HttpContext.Current.Request(aszHostFieldName(j))) with szValue
                    szHashString = szHashString + aszHostFieldName(j) + "=" + szValue
                Else
                    szHashString = szHashString + aszHostFieldName(j) + "=" + szValue + ","
                End If
            Next

            szHashValue = szComputeHMACSHA256HEXHashKeyCase(szHashString, st_HostInfo, "O", "L")

        Catch ex As Exception
            Throw ex
        End Try

        Return szHashValue
    End Function

    '********************************************************************************************
    ' Class Name:		Hash  (MBB)
    ' Function Name:	szMBBEncryptAES
    ' Function Type:	String. Encrypt string
    ' Parameter:		sz_Data. String to encrypt. 
    ' Description:		Get Encrypt string.
    ' History:			28 Nov 2014. Added 
    '********************************************************************************************
    Public Function szMBBEncryptAES(ByVal sz_Data As String, ByVal st_PayInfo As Common.STPayInfo, ByVal st_HostInfo As Common.STHost) As String
        Dim szOutput As String = ""
        Dim szSalt As String = ""

        Try
            szSalt = szDecrypt3DES(st_HostInfo.szSendKey, Common.C_3DESAPPKEY, Common.C_3DESAPPVECTOR)
            szOutput = Server.UrlEncode(JavaCrypto.AES.MBBEncrypt(sz_Data, szSalt)) 'Modified 17 Jun 2015, added JavaCrypto else will have AES is ambiguous error in VS 2012

            Return szOutput

        Catch ex As Exception
            Throw ex
        End Try

    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szComputeSHA512Hash
    ' Function Type:	String. Hash value generated using SHA512 algorithm.
    ' Parameter:		sz_Key. Values to be hash.
    ' Description:		Get hash value generated using SHA512 algorithm
    ' History:			05 March 2015. Added  - eBPG Maybank-CC
    '********************************************************************************************
    Public Function szComputeSHA512Hash(ByVal sz_Key As String) As String
        Dim szHashValue As String = ""
        Dim objSHA512 As SHA512 = Nothing
        Dim btHashbuffer() As Byte = Nothing

        Try
            objSHA512 = New SHA512Managed
            objSHA512.ComputeHash(System.Text.Encoding.ASCII.GetBytes(sz_Key))
            btHashbuffer = objSHA512.Hash
            szHashValue = szWriteHex(btHashbuffer).ToLower()

        Catch ex As Exception
            Throw ex
        Finally
            If Not objSHA512 Is Nothing Then objSHA512 = Nothing
        End Try

        Return szHashValue
    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szComputeSHA512UHash
    ' Function Type:	String. Hash value generated using SHA512 algorithm.
    ' Parameter:		sz_Key. Values to be hash.
    ' Description:		Get hash value generated using SHA512 algorithm
    ' History:			24 Aug 2015. Added  - eBPG Maybank-CC
    '********************************************************************************************
    Public Function szComputeSHA512UHash(ByVal sz_Key As String) As String
        Dim szHashValue As String = ""
        Dim objSHA512 As SHA512 = Nothing
        Dim btHashbuffer() As Byte = Nothing

        Try
            objSHA512 = New SHA512Managed
            objSHA512.ComputeHash(System.Text.Encoding.ASCII.GetBytes(sz_Key.ToUpper()))
            btHashbuffer = objSHA512.Hash
            szHashValue = szWriteHex(btHashbuffer)

        Catch ex As Exception
            Throw ex
        Finally
            If Not objSHA512 Is Nothing Then objSHA512 = Nothing
        End Try

        Return szHashValue
    End Function
    '********************************************************************************************
    ' Class Name:		Hash  (TBank)
    ' Function Name:	szEncryptDecryptTBank
    ' Function Type:	String. Encrypt Decrypt string
    ' Parameter:		sz_Data. String to encrypt/decrypt. 
    ' Description:		Get Encrypt string.
    ' History:			7 April 2017
    '********************************************************************************************
    Public Function szEncryptDecryptTBank(ByVal sz_Data As String, ByVal st_PayInfo As Common.STPayInfo) As String
        Dim szOutput As String = ""
        Dim szMethod As String = ""
        Dim sntq3Des As SinaptIQ3DES.SinaptIQ3DES

        Try

            sntq3Des = New SinaptIQ3DES.SinaptIQ3DES()
            Dim szSplitDt As String() = sz_Data.Split("^")

            If (szSplitDt.Length > 1) Then
                szMethod = szSplitDt(0)
                sz_Data = szSplitDt(1)
            End If
            'Return sz_Data
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
                        st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > " + szMethod + " Data [" + sz_Data + "]")

            If (szMethod = "encrypt") Then
                szOutput = sntq3Des.Encrypt(sz_Data)
            Else
                szOutput = sntq3Des.Decrypt(sz_Data)
            End If

            Return szOutput

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    '********************************************************************************************
    ' Class Name:		Hash  (VisaCheckout)
    ' Function Name:	szDecryptVCPayLoad
    ' Function Type:	String. Decrypt VC encPaymentData string
    ' Parameter:		sz_Data. String to decrypt. 
    ' Description:		Get Decrypt string.
    ' History:			20 June 2017
    '********************************************************************************************
    Public Function szDecryptVCPayLoad(szSecretKey As String, szVCKey As String, szVCPayLoad As String) As String
        Return Encoding.UTF8.GetString(DecryptVC(DecryptVC(Encoding.UTF8.GetBytes(szSecretKey), Convert.FromBase64String(szVCKey)), Convert.FromBase64String(szVCPayLoad)))
    End Function

    '********************************************************************************************
    ' Class Name:		Hash  (VisaCheckout)
    ' Function Name:	szDecryptVCPayLoad
    ' Function Type:	String. Decrypt VC encPaymentData string
    ' Parameter:		sz_Data. String to decrypt. 
    ' Description:		Get Decrypt string.
    ' History:			20 June 2017
    '********************************************************************************************
    Public Function DecryptVC(key As Byte(), data As Byte()) As Byte()
        Dim HMAC_LENGTH As Integer = 32
        Dim IV_LENGTH As Integer = 16

        Try
            If data Is Nothing OrElse data.Length <= IV_LENGTH + HMAC_LENGTH Then
                Throw New ArgumentException("Bad input data", "data")
            End If

            Dim hmac As Byte() = New Byte(HMAC_LENGTH - 1) {}
            Array.Copy(data, 0, hmac, 0, HMAC_LENGTH)

            Dim iv As Byte() = New Byte(IV_LENGTH - 1) {}
            Array.Copy(data, HMAC_LENGTH, iv, 0, IV_LENGTH)

            Dim payload As Byte() = New Byte(data.Length - HMAC_LENGTH - IV_LENGTH - 1) {}
            Array.Copy(data, HMAC_LENGTH + IV_LENGTH, payload, 0, payload.Length)
            'if (byteArrayEquals(hmac, dohmac(key, byteArrayConcat(iv, payload)))) { // TODO: Handle HMAC validation failure //} 

            Dim aes As New RijndaelManaged 'AES
            aes.BlockSize = 128
            aes.KeySize = 256
            aes.Key = hash(key)
            aes.IV = iv
            aes.Mode = CipherMode.CBC
            aes.Padding = PaddingMode.PKCS7

            Dim ms As New MemoryStream()
            Dim cs As New CryptoStream(ms, aes.CreateDecryptor(), CryptoStreamMode.Write)
            cs.Write(payload, 0, payload.Length)
            cs.FlushFinalBlock()

            Return ms.ToArray()

        Catch ex As Exception
            Throw ex
        End Try

    End Function

    '********************************************************************************************
    ' Class Name:		Hash  (Boost)
    ' Function Name:	szComputeBoostHash
    ' Function Type:	String. Boost hash value.
    ' Parameter:		sz_Data - String from which data to be hashed can be extracted
    ' Description:		Generate Boost hash value
    ' History:			15 Nov 2017
    '********************************************************************************************
    Public Function szComputeBoostHash(ByVal sz_Key As String, ByVal st_PayInfo As Common.STPayInfo, ByVal st_HostInfo As Common.STHost) As String
        Dim szHashValue As String = ""
        Dim DES As New TripleDESCryptoServiceProvider()
        Dim aszValue() As String
        Dim szKey As String = sz_Key    'Added 17 Jan 2018

        aszValue = sz_Key.Split("@")
        If (aszValue.Length() > 1) Then
            sz_Key = aszValue(1)
            DES.Key = UTF8Encoding.UTF8.GetBytes(st_HostInfo.szReturnKey.Substring(0, 24))
        Else
            DES.Key = UTF8Encoding.UTF8.GetBytes(szDecrypt3DES(st_HostInfo.szSendKey, Common.C_3DESAPPKEY, Common.C_3DESAPPVECTOR).Substring(0, 24))
        End If
        DES.Mode = CipherMode.ECB
        DES.Padding = PaddingMode.PKCS7
        Dim DESEncrypt As ICryptoTransform = DES.CreateEncryptor()
        Dim Buffer As Byte() = UTF8Encoding.UTF8.GetBytes(sz_Key)
        szHashValue = Convert.ToBase64String(DESEncrypt.TransformFinalBlock(Buffer, 0, Buffer.Length))

        'Added 17 Jan 2018
        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                        st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > HashString(" + szKey + ") Hash(" + szHashValue + ")")

        Return szHashValue
    End Function

    '********************************************************************************************
    ' Class Name:		Hash  (VisaCheckout)
    ' Function Name:	szDecryptVCPayLoad
    ' Function Type:	String. Decrypt VC encPaymentData string
    ' Parameter:		sz_Data. String to decrypt. 
    ' Description:		Get Decrypt string.
    ' History:			20 June 2017
    '********************************************************************************************
    Public Function hash(key As Byte()) As Byte()
        Return New SHA256Managed().ComputeHash(key)
    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	bValidHex
    ' Function Type:	Boolean. True: Valid HEX False: Invalid HEX
    ' Parameter:		Input string for HEX validation
    ' Description:		Check if the string contains hexadecimal values
    ' History:			26 Mar 2009
    '********************************************************************************************
    Public Function bValidHex(ByVal sz_Txt As String) As Boolean
        Dim szch As String
        Dim i As Integer

        sz_Txt = sz_Txt.ToUpper()
        For i = 1 To Len(sz_Txt)

            szch = Mid(sz_Txt, i, 1)
            ' See if the next character is a non-digit.
            If (False = IsNumeric(szch)) Then
                If szch >= "G" Then
                    ' This is not a letter.
                    Return False
                End If
            End If
        Next i

        Return True
    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szStringToHex
    ' Function Type:	String. Hex string value converted from String.
    ' Parameter:		sz_text. String values to be converted to hex.
    ' Description:		Get hex string value converted from String.
    ' History:			26 March 2009
    '********************************************************************************************
    Public Function szStringToHex(ByVal sz_Text As String) As String
        Dim szHex As String = ""

        For i As Integer = 0 To sz_Text.Length - 1
            szHex &= Asc(sz_Text.Substring(i, 1)).ToString("x").ToUpper
        Next

        Return szHex
    End Function

    Public Function btHexToByte(ByVal c_Hex As Char()) As Byte()
        Dim szHex As String = ""

        Dim btOutput(c_Hex.Length() / 2 - 1) As Byte 'Byte() = New Byte(c_Hex.Length / 2 - 1) {}
        Dim iOffset As Integer = 0
        Dim iCounter As Integer = 0

        iCounter = 0
        While iCounter < c_Hex.Length()
            szHex = Convert.ToString(c_Hex(iCounter)) + Convert.ToString(c_Hex(iCounter + 1))
            btOutput(iOffset) = Byte.Parse(szHex, System.Globalization.NumberStyles.HexNumber)
            iOffset += 1
            iCounter = iCounter + 2
        End While

        Return btOutput
    End Function

    Public Function szBytesToHex(ByVal bt_bytes As Byte()) As String
        Dim sbHexStr As New StringBuilder(bt_bytes.Length)  ' System.Text
        Dim szHex As String = ""
        Dim iCounter As Integer = 0

        For iCounter = 0 To bt_bytes.Length - 1
            sbHexStr.Append(bt_bytes(iCounter).ToString("X2"))
        Next

        szHex = sbHexStr.ToString()

        If Not sbHexStr Is Nothing Then
            sbHexStr = Nothing
        End If

        Return szHex
    End Function

    Public Function btTransform(ByVal bt_Input() As Byte, ByVal I_CryptoTransform As ICryptoTransform) As Byte()
        ' Create the necessary streams
        Dim objMemStream As MemoryStream = New MemoryStream
        Dim objCryptStream As CryptoStream = New CryptoStream(objMemStream, I_CryptoTransform, CryptoStreamMode.Write)
        Dim btResult() As Byte

        Try
            ' Transform the bytes as requested
            objCryptStream.Write(bt_Input, 0, bt_Input.Length())
            objCryptStream.FlushFinalBlock()

            ' Read the memory stream and convert it back into byte array
            objMemStream.Position = 0

            ReDim btResult(CType(objMemStream.Length() - 1, System.Int32))
            objMemStream.Read(btResult, 0, CType(btResult.Length(), System.Int32))

        Catch ex As Exception
            Throw ex
        Finally
            ' close and release the streams
            objMemStream.Close()
            objCryptStream.Close()

            If Not objMemStream Is Nothing Then objMemStream = Nothing
            If Not objCryptStream Is Nothing Then objCryptStream = Nothing
        End Try

        ' hand back the result buffer
        Return btResult
    End Function

    'Adler-32 is a checksum algorithm which was invented by Mark Adler.
    'Compared to a cyclic redundancy check of the same length, it trades reliability for speed.
    'Adler-32 is more reliable than Fletcher-16, and slightly less reliable than Fletcher-32.

    'An Adler-32 checksum is obtained by calculating two 16-bit checksums A and B and concatenating their bits into a 32-bit integer. A is the sum of all bytes in the string plus one, and B is the sum of the individual values of A from each step.
    'At the beginning of an Adler-32 run, A is initialized to 1, B to 0. The sums are done modulo 65521 (the largest prime number smaller than 216). The bytes are stored in network order (big endian), B occupying the two most significant bytes.
    'A = 1 + D1 + D2 + ... + Dn (mod 65521)
    'B = (1 + D1) + (1 + D1 + D2) + ... + (1 + D1 + D2 + ... + Dn) (mod 65521)
    '  = n�D1 + (n-1)�D2 + (n-2)�D3 + ... + Dn + n (mod 65521)
    'Adler-32(D) = B � 65536 + A

    'The Adler-32 sum of the ASCII string "Wikipedia" would be calculated as follows
    '   ASCII code          A                   B
    '   (shown as base 10)
    '   W: 87           1 +  87 =  88        0 +  88 =   88
    '   i: 105         88 + 105 = 193       88 + 193 =  281
    '   k: 107        193 + 107 = 300      281 + 300 =  581
    '   i: 105        300 + 105 = 405      581 + 405 =  986
    '   p: 112        405 + 112 = 517      986 + 517 = 1503
    '   e: 101        517 + 101 = 618     1503 + 618 = 2121
    '   d: 100        618 + 100 = 718     2121 + 718 = 2839
    '   i: 105        718 + 105 = 823     2839 + 823 = 3662
    '   a: 97         823 +  97 = 920     3662 + 920 = 4582
    '   A = 920  =  398 hex (base 16)
    '   B = 4582 = 11E6 hex
    '   Output = 300286872 = 11E60398 hex
    Public Function lAdler32(ByVal i_Adler As Integer, ByVal sz_Buf As String) As Long
        Dim iBASE As Integer = 0
        Dim iLoop As Integer = 0
        Dim iS1 As Integer = 0
        Dim iS2 As Integer = 0

        Try
            iBASE = 65521
            iS1 = lAndop(i_Adler, 65535)
            iS2 = lAndop(lCDec(szRightShift(szCBin(i_Adler), 16)), 65535)

            For iLoop = 1 To Len(sz_Buf)
                iS1 = (iS1 + Asc(Mid(sz_Buf, iLoop, 1))) Mod iBASE
                iS2 = (iS2 + iS1) Mod iBASE
            Next
        Catch ex As Exception
            Throw ex
        End Try

        Return lCDec(szLeftShift(szCBin(iS2), 16)) + iS1
    End Function

    Public Function lAndop(ByVal l_Op1 As Long, ByVal l_Op2 As Long) As Long
        Dim szOp3 As String = ""
        Dim szOp4 As String = ""
        Dim szOp As String = ""
        Dim iLoop As Integer = 0

        Try
            szOp3 = szCBin(l_Op1)
            szOp4 = szCBin(l_Op2)

            For iLoop = 1 To 32
                szOp = szOp & "" & (CInt(Mid(szOp3, iLoop, 1)) And CInt(Mid(szOp4, iLoop, 1)))
            Next
        Catch ex As Exception
            Throw ex
        End Try

        Return lCDec(szOp)
    End Function

    'Function used for bitwise right shifting
    Public Function szRightShift(ByVal sz_Str As String, ByVal i_Num As Integer) As String
        Dim iLoop As Integer = 0

        Try
            For iLoop = 1 To i_Num
                sz_Str = "0" & sz_Str
                sz_Str = Left(sz_Str, Len(sz_Str) - 1)
            Next
        Catch ex As Exception
            Throw ex
        End Try

        Return sz_Str
    End Function

    'Function used for bitwise left shifting
    Public Function szLeftShift(ByVal sz_Str As String, ByVal i_Num As Integer) As String
        Dim iLoop As Integer = 0

        Try
            For iLoop = 1 To 32 - Len(sz_Str)
                sz_Str = "0" & sz_Str
            Next

            For iLoop = 1 To i_Num
                sz_Str = sz_Str & "0"
                sz_Str = Right(sz_Str, Len(sz_Str) - 1)
            Next
        Catch ex As Exception
            Throw ex
        End Try

        Return sz_Str
    End Function

    'Function used for decimal to binary conversion
    Public Function szCBin(ByVal l_Num As Long) As String
        Dim szBin As String = ""
        Dim iLoop As Integer = 0

        Try
            Do
                szBin = CStr((l_Num Mod 2)) & szBin
                l_Num = Fix(l_Num / 2)
            Loop While Not ((l_Num = 0))

            For iLoop = 1 To 32 - Len(szBin)
                szBin = "0" & szBin
            Next
        Catch ex As Exception
            Throw ex
        End Try

        Return szBin
    End Function

    'Function used for  binary to decimal conversion
    Public Function lCDec(ByVal sz_Num As String) As Long
        Dim iLoop As Integer = 0
        Dim lDec As Long = 0

        Try
            For iLoop = 1 To Len(sz_Num)
                lDec = lDec + CInt(Mid(sz_Num, iLoop, 1)) * lPower(Len(sz_Num) - iLoop)
            Next
        Catch ex As Exception
            Throw ex
        End Try

        Return lDec
    End Function

    'Function used for calculate value of 2^n
    Public Function lPower(ByVal l_Num As Long) As Long
        Dim lResult As Long = 1
        Dim iLoop As Integer = 0

        Try
            For iLoop = 1 To l_Num
                lResult = lResult * 2
            Next
        Catch ex As Exception
            Throw ex
        End Try

        Return lResult
    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szASCIICounter
    ' Function Type:	String. 
    ' Parameter:		sz_Check, sz_ValueToSum. Sum up string using ASCII.
    ' Description:		Sum up 2 string with ASCII value.
    ' History:			4 Dec 2013. Alliance ALB
    '********************************************************************************************
    Public Function szASCIICounter(ByVal sz_Check As String, ByVal sz_ValueToSum As String) As String
        Dim iCheckSum As Integer
        Dim szResult As String

        'szTestMID = "111112222233333"
        'szCheck = "111222" / "GHLSIT20131127000001"
        For Each c As Char In sz_Check
            iCheckSum += Asc(c)
        Next

        If (1000 <= iCheckSum) Then
            iCheckSum = iCheckSum Mod 1000
        End If

        szResult = Convert.ToString(Convert.ToInt64(sz_ValueToSum) + iCheckSum)

        Return szResult
    End Function

    'Modified 11 Apr 2014, added new param o_Common
    Public Sub New(ByRef o_LoggerII As LoggerII.CLoggerII, ByVal o_Common As Common)
        Try
            objLoggerII = o_LoggerII
            objCommon = o_Common        'Added, 11 Apr 2014
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub New(ByRef o_LoggerII As LoggerII.CLoggerII)
        Try
            objLoggerII = o_LoggerII
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class
