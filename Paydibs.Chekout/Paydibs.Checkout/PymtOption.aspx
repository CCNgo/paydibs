﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PymtOption.aspx.vb" Inherits="Paydibs.Checkout.PymtOption" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Paydibs Checkout</title>

    <!-- Meta -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

    <!-- Favicon -->
	<link rel="shortcut icon" href="assets3/img/favicon.ico?v1">

	<!-- Web Fonts -->
	<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin">

	<!-- CSS Global Compulsory -->
	<link rel="stylesheet" href="assets3/plugins/bootstrap/css/bootstrap.min.css?v1.1">
	<link rel="stylesheet" href="assets3/css/shop.style.css?v=1.0">
    <link rel="stylesheet" href="assets3/css/Custom.css?v=1.7">

	<!-- CSS Header and Footer -->
	<link rel="stylesheet" href="assets3/css/headers/header-v1.css?v=1.1">
	<link rel="stylesheet" href="assets3/css/footers/footer-v4.css">

	<!-- CSS Implementing Plugins -->
	<link rel="stylesheet" href="assets3/plugins/animate.css">
	<link rel="stylesheet" href="assets3/plugins/line-icons/line-icons.css">
	<link rel="stylesheet" href="assets3/plugins/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets3/plugins/jquery-steps/css/custom-jquery.steps.css">
	<link rel="stylesheet" href="assets3/plugins/scrollbar/css/jquery.mCustomScrollbar.css">
	<link rel="stylesheet" href="assets3/plugins/sky-forms-pro/skyforms/css/sky-forms.css">
	<link rel="stylesheet" href="assets3/plugins/sky-forms-pro/skyforms/custom/custom-sky-forms.css">

	<!-- Style Switcher -->
	<link rel="stylesheet" href="assets3/css/plugins/style-switcher.css">

	<!-- CSS Theme -->
	<link rel="stylesheet" href="assets3/css/theme-colors/default.css" id="style_color">

	<!-- CSS Customization -->
	<link rel="stylesheet" href="assets3/css/custom1.css">
    
    <style type="text/css">
        #padd br {
            display:none;
        }
        a :visited :active :hover{
            text-decoration:none;
        }
    </style>
</head>
<body class="header-fixed">
    <div class="wrapper">
        <div class="header-v1">
			<div class="topbar-v1">
				<div class="row">
					<div class="hide"><!--div class="[HIDEMERCHANTLOGO]"-->
						<img class="logo-img" src="assets/img/om-logo.png" alt="Merchant" style="max-width:150px;max-height:50;">
						<nav><h4 style="font-family:sans-serif;color:black;padding-top:5px;">Om Testing Sdn Bhd</h4>
						<h6 style="font-family:sans-serif;color:black;padding-bottom:5px;margin-top:-12px;font-size:10px">
						<i class="fa fa-map-marker"></i><span id="padd">123, Jalan 123, Taman Industry, 50000 Wilayah Persekutuaan, Kuala Lupur</span>&nbsp;&nbsp;
						<i class="fa fa-envelope-o"></i>testing@test.com&nbsp;&nbsp;<i class="fa fa-phone"></i>+603 1234 5678&nbsp;&nbsp;
						<i class="fa fa-at"></i><a href="javascript:openWindow('www.abc.com',800,800,'');">www.abc.com</a>
						</h6>
						</nav>
					</div>
				</div>
			</div>
		</div>

        <div class="container">
            <div class="row">
                <div id="wrapper-TimeOut" class="alert alert-m"><!--alert-danger-->
                    <center>
                        <img class="clock-error" style="display:none;" src="assets3/img/clock_stop.png" />
                        <img class="clock-play" src="assets3/img/clock_go.png">
                        <span id="TimeOut"></span>
                    </center>
                </div>
            </div>
            <div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading price-container">
							<h3 class="panel-title"><strong> Payment Details</strong></h3>
                            <ul id="paymentType" style="display: none;">
			                    [PYMTMETHOD]
		                    </ul>
						</div>						
						<div class="panel-body">
							<div class="col-md-6">
								<dl class="dl-horizontal">
									<dt><strong>Payment ID</strong></dt>
									<dd>[MERCHANTTXNID]</dd>
								</dl>
								<dl class="dl-horizontal">
									<dt><strong>Order ID</strong></dt>
									<dd>[ORDERNUMBER]</dd>
								</dl>
								<dl class="dl-horizontal">
									<dt><strong>Order Description</strong></dt>
									<dd>[ORDERDESC]</dd>
								</dl>
								<dl class="dl-horizontal">
									<dt><strong>Total Amount</strong></dt>
									<dd><strong><span id="currency-code">[CURRENCYCODE]</span>&nbsp;<span class="price" id="price" >[TXNAMOUNT]</span></strong></dd>
								</dl>
							</div>
							<div class="col-md-6">
								<div id="divPromolist" class="panel panel-default" style="display:block;">
                                    [PROMOLIST]
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

            <div class="row">
                    <form id="frmProcessPayment" name="frmProcessPayment"  class="form-horizontal" role="form" autocomplete="off" method="POST">
                        </form>
                </div>

            <%--<button id="btnCC" class="tablink" onclick="openPage('divCC', this, '#F4F4F4')">Credit Card</button>
            <button id="btnOB" class="tablink" onclick="openPage('divOB', this, '#F9F9F9')">Online Banking</button>
            <div id="divCC" class="tabcontent">
                <div class="row">
                    <form id="frmProcessPayment" name="frmProcessPayment"  class="form-horizontal" role="form" autocomplete="off" method="POST">
                        </form>
                </div>
            </div>

            <div id="divOB" class="tabcontent">

            </div>--%>
        </div> <!--container-->
    </div> <!--wrapper-->

    <div class="row"> <!--Cancel-->
        <div class="col-md-12 text-center">
            <hr />
        <p id="pcancel" class="text-center" style="padding-top:5px">
		    <a id="acancel" href="[CANCELURL]" style="color:green;">[CANCELURLDESC]</a>
	    </p>
	    <br><br>
        </div>
    </div>

    <div class="footer-v4">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<p><strong>Powered by:</strong> <img id="logo-header" src="assets3/img/paydibs.png?v=20180416" alt="Merchant" width="80" height="17">
					&nbsp;&nbsp;&nbsp;&nbsp;
                    <a style="pointer-events:none; cursor:default;"><img id="imgTrustwave" style="width:50px;" border="0" alt="trustwave" src="assets3/img/trustwave.png"></a>
					</p>
					<p style="margin:0;"><font size="1">Paydibs Careline: +603 XXXXXXXX, 9am - 6pm (Monday - Friday)&nbsp;&nbsp;&nbsp;&nbsp;Email: support@paydibs.com</Font></p>
					<p style="margin:0;"><font size="1">Copyright © 2018 Paydibs Sdn Bhd. All rights reserved</font></p>
				</div>
			</div>
		</div>
	</div>

    <div id="modal-timeout" style="display:none;" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel modal-body" aria-hidden="true">
        <div class="modal-header text-center">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h3 style="color:white;" id="myModalLabel">Timed Out</h3>
        </div>
        <div class="modal-body text-center">
            <p style="color:white;">Sorry, your payment session has timed out. Please return to [MERCHANTNAME] to make a new payment.</p>
        </div>
    </div>

    <!-- JS Global Compulsory -->
	<script src="assets3/plugins/jquery/jquery.min.js"></script>
	<script src="assets3/plugins/jquery/jquery-migrate.min.js"></script>
    <script src="assets3/plugins/bootstrap/js/bootstrap.min.js"></script>
    
	<!-- JS Implementing Plugins -->
	<script src="assets3/plugins/back-to-top.js"></script>
	<script src="assets3/plugins/smoothScroll.js"></script>
	<script src="assets3/plugins/jquery-steps/build/jquery.steps.js"></script>
	<script src="assets3/plugins/scrollbar/js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script src="assets3/plugins/sky-forms-pro/skyforms/js/jquery.validate.min.js"></script>
	<!-- JS Customization -->
	<script src="assets3/js/custom.js"></script>
	<!-- JS Page Level -->
	<script src="assets3/js/shop.app.js"></script>
	<script src="assets3/js/forms/page_login.js"></script>
	<script src="assets3/js/plugins/stepWizard.js"></script>
	<script src="assets3/js/forms/product-quantity.js"></script>
	<script src="assets3/js/plugins/style-switcher.js"></script>
    <script src="assets3/js/pymttemplate2.js?v=1.1"></script>
    <script src="assets3/js/knockout.min.js"></script>
    <script src="assets3/js/customerInfo.js?v=1.3"></script>

    <script type="text/javascript">
        jQuery(document).ready(function() {
			App.init();
			Login.initLogin();
			App.initScrollBar();
			StepWizard.initStepWizard();
			StyleSwitcher.initStyleSwitcher();
            //customerInfoCollector.init('customer_info');
            //a('[MERCHANTID]');
		});
		
		document.cookie = "currentTxnID=[MERCHANTID][MERCHANTTXNID];365;path=/";
        //function openPage(pageName, elmnt, color) {
        //    // Hide all elements with class="tabcontent" by default */
        //    var i, tabcontent, tablinks;
        //    tabcontent = document.getElementsByClassName("tabcontent");
        //    for (i = 0; i < tabcontent.length; i++) {
        //        tabcontent[i].style.display = "none";
        //    }
        //    // Remove the background color of all tablinks/buttons
        //    tablinks = document.getElementsByClassName("tablink");
        //    for (i = 0; i < tablinks.length; i++) {
        //        tablinks[i].style.backgroundColor = "";
        //    }
        //    // Show the specific tab content
        //    document.getElementById(pageName).style.display = "block";
        //    // Add the specific color to the button used to open the tab content
        //    elmnt.style.backgroundColor = color;
        //}
        // Get the element with id="defaultOpen" and click on it
        //document.getElementById("defaultOpen").click();

        var divPromo = document.getElementById("divPromolist");
		if(divPromo.innerHTML.trim() == "") {
		    divPromo.style.display = "none";
		}
		else {
		    divPromo.style.display = "block";
		}

        if ("[MERCHANTID][MERCHANTTXNID]" == getCookie("pymttxnid")) {
		    iTimeout = getCookie("pymttxntimeout");
		}
        else {
            iTimeout = 3;//[PAGETIMEOUT];	// In seconds
		}

		TargetDate = new Date().valueOf() + iTimeout * 1000;
		CountActive = true;
		CountStepper = -1;
		LeadingZero = true;
		//DisplayFormat = "Time Remaining : %%M%% Minutes %%S%% Seconds";
		DisplayFormat = "Timeout in %%M%% : %%S%% ";
        
		if (typeof (BackColor) == "undefined")
		    BackColor = "white";
		if (typeof (ForeColor) == "undefined")
		    ForeColor = "black";
		if (typeof (TargetDate) == "undefined")
		    TargetDate = "12/31/2020 5:00 AM";
		if (typeof (DisplayFormat) == "undefined")
		    DisplayFormat = "%%D%% Days, %%H%% Hours, %%M%% Minutes, %%S%% Seconds.";
		if (typeof (CountActive) == "undefined")
		    CountActive = true;
		if (typeof (FinishMessage) == "undefined")
		    FinishMessage = "";
		if (typeof (CountStepper) != "number")
		    CountStepper = -1;
		if (typeof (LeadingZero) == "undefined")
		    LeadingZero = true;

		CountStepper = Math.ceil(CountStepper);
		if (CountStepper == 0)
		    CountActive = false;
		var SetTimeOutPeriod = (Math.abs(CountStepper) - 1) * 1000 + 990;

		//putspan(BackColor, ForeColor);
		var dthen = new Date(TargetDate);
		var dnow = new Date();
		if (CountStepper > 0)
		    ddiff = new Date(dnow - dthen);
		else
		    ddiff = new Date(dthen - dnow);
		gsecs = Math.floor(ddiff.valueOf() / 1000);
		CountBack(gsecs);

        function calcage(secs, num1, num2) {
		    s = ((Math.floor(secs / num1)) % num2).toString();
		    if (LeadingZero && s.length < 2)
		        s = "0" + s;
		    return "<b><font color=red>" + s + "</font></b>";
		}

		function CountBack(secs) {
		    document.cookie = "pymttxntimeout=" + secs;
		    document.cookie = "pymttxnid=" + "[MERCHANTID][MERCHANTTXNID]";
		    document.cookie = "currentTimeOut="+secs+";365;path=/";

		    if (secs < 0) {

		        var form = document.getElementById("frmProcessPayment");
		        var elements = form.elements;
		        for (var i = 0, len = elements.length; i < len; ++i) {
		            elements[i].readOnly = true;
		        }
		        //window.frmProcessPayment.postNow.disabled = true;
		        //var atag = document.getElementsByTagName("a");
		        //for (var i = 0; i < atag.length; i++)
		        //{
		        //    if (atag[i].id == "acancel") {
		        //        atag[i].disabled = false;
		        //    }
		        //    else {
		        //        atag[i].disabled = true;
		        //        atag[i].style.pointerEvents = "none";
		        //    }
		        //}
		        //document.getElementById("rdSinglePymt").disabled = true;
		        //document.getElementById("rdInstPymt").disabled = true;
		        //document.getElementById("instLists").disabled = true;
		        //document.getElementById("epMonth2").disabled = true;
		        //document.getElementById("epYear2").disabled = true;
		        //document.getElementById("cvv2").disabled = true;
		        //document.getElementById("custOCP").disabled = true;
				
				//MasterPass & VisaCheckout
				/*document.getElementById('MP').style.display = 'none';
				document.getElementById('VC').style.display = 'none';*/
				document.getElementById("pcancel").style.display = "none"; // Added on 7 May 2018
				
		        document.getElementById('TimeOut').innerHTML = "<div>Sorry, your payment session has timed out.</div>"
                                                                + "<div>Please return to [MERCHANTNAME] to make a new payment.</div>"
                                                                + "<div><a id=\"acancel\" href=\"[CANCELURL]\">[CANCELURLDESC]</a></div>";
		        $('#wrapper-TimeOut').addClass("alert-danger");
		        $(".clock-error").show();
		        $(".clock-play").hide();
		        $('#modal-timeout').modal({ keyboard: false });

		        return;
		    }
		    DisplayStr = DisplayFormat.replace(/%%D%%/g, calcage(secs, 86400, 100000));
		    DisplayStr = DisplayStr.replace(/%%H%%/g, calcage(secs, 3600, 24));
		    DisplayStr = DisplayStr.replace(/%%M%%/g, calcage(secs, 60, 60));
		    DisplayStr = DisplayStr.replace(/%%S%%/g, calcage(secs, 1, 60));

		    if (document.getElementById('TimeOut') != null)
		        document.getElementById('TimeOut').innerHTML = DisplayStr;

		    if (CountActive)
		        setTimeout("CountBack(" + (secs + CountStepper) + ")", SetTimeOutPeriod);
		}

		function getCookie(c_name) {
		    var i, x, y, ARRcookies = document.cookie.split(";");
		    for (i = 0; i < ARRcookies.length; i++) {
		        x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
		        y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
		        x = x.replace(/^\s+|\s+$/g, "");
		        if (x == c_name) {
		            return unescape(y);
		        }
		    }
        }

  //      $("#paymentType").each(function() {
		//    var ccside = 0;
		//    var othersside = 0;
		//    $(this).find("a").each(function() {
		//        var paymentMethod = $(this).attr("href");
		//        var unionPymtMethod = "";	//Added  on 6 Dec 2017
				
		//        if(paymentMethod == "#cc" || paymentMethod == "#mo") {
		//            $("#payment-method-cc").css("display","block");
		//            if(paymentMethod == "#mo") {
		//                $("#divInstallment").css("display","none");
		//                $("#divCardHolderName").css("display", "none");
		//                $("#divCardType").css("display", "none");
		//                $("#divCVV").css("display", "none");
		//                $("#postNow").css("display","none");
		//                $("#postNowMO").css("display","inline");
		//            }
		//            ccside = 1;
		//        }	   
		//        if(paymentMethod == "#wallet" || paymentMethod == "#ob" || paymentMethod == "#otc") {
		//            $("#payment-method-others").css("display","block");
		//            /*if(paymentMethod == "#wallet")
		//            {
		//                $("#payment-bitcoin").css("display","block");
		//            }*/
		//			if(paymentMethod == "#wallet") {
		//				unionPymtMethod = "[UNIONPYMTMETHOD]";

		//				if(unionPymtMethod != "")	// Added on 6 Dec 2017
		//				{
		//					if(unionPymtMethod.toUpperCase().indexOf("BITPAY") > -1)
		//					{
		//						$("#payment-bitcoin").css("display","block");
		//					}
		//					if(unionPymtMethod.toUpperCase().indexOf("PAYPAL") > -1)
		//					{
		//						$("#payment-paypal").css("display","block");
		//					}
		//				}
		//			}
		//            othersside = 1;
		//        }
		        

		//    });
		//    if(ccside == 1 && othersside == 1)
		//    {
		//        /*$("#left_side").addClass("col-md-6");
		//        $("#payment-method-others").addClass("col-md-6");*/
		//    }
		//    if(ccside == 1 && othersside == 0)
  //          {
  //              $("#divDD").style.display = false
		//        /*$("#left_side").removeClass("col-md-6");
		//        $("#left_side").addClass("col-md-12");*/

		//        $("#payment-method-cc").css('padding','0');
		//        $("#customer_info").css('padding','0');
		//    }
		//    if(ccside == 0 && othersside == 1)
  //          {
  //              $("#divCC").style.display = false
		//        /*$("#left_side").removeClass("col-md-6");
		//        $("#left_side").addClass("col-md-12");*/
		//        $("#customer_info").css('padding','0');

		//        $("#payment-method-others").removeClass("col-md-6");
		//        $("#payment-method-others").addClass("col-md-12");
		//    }
  //      });

  //      //OoiMei-Installment//
		//function onSelectedIndexChanged_InstList() {
		//    if(document.getElementById("rdInstPymt").checked==true) {
		//        document.getElementById("instLists").disabled=false;
		//    }
		//    else if(document.getElementById("rdSinglePymt").checked==true) {
		//        document.getElementById("instLists").disabled=true;
		//        document.getElementById("instLists").value='';
		//    }
		//}

		//onSelectedIndexChanged_InstList();
		//$(document).ready(function(){
		//    $('[data-toggle="popover"]').popover();
		//});
		
		//document.getElementById("padd").innerText = document.getElementById("padd").innerText.replace(',,',',');

    </script>
</body>
</html>
