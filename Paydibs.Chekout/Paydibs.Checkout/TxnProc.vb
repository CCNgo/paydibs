Imports System.Data.SqlClient
Imports System.Threading
Imports LoggerII
Imports System.Security.Cryptography
Imports Crypto
Imports System.Security
Imports System.Net

Public Class TxnProc

    Private objLoggerII As LoggerII.CLoggerII
    Private objHash As Hash
    Private szDBConnStr As String = ""
    Private resp As Response            'Added 11 Apr 2019, Joyce

    'Private objLog4Net As ILogger = LoggerFactory.GetLogger(GetType(Configuration.SlfConfigurationSection))
    'Private Shared ReadOnly objLog4Net As ILog = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

    'Dim logFilePath As String = AppDomain.CurrentDomain.BaseDirectory + "\\web.config"
    ''FileInfo finfo = New FileInfo(logFilePath)
    'Dim finfo As FileInfo = New FileInfo(logFilePath)
    '        log4net.Config.XmlConfigurator.ConfigureAndWatch(finfo)



    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bInsertTxnResp
    ' Function Type:	Boolean.  True if insertion successful else false
    ' Parameter:		st_PayInfo
    ' Description:		Inserts transaction response into database
    ' History:			15 Oct 2008
    '********************************************************************************************
    Public Function bInsertTxnResp(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost) As Boolean

        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRowsAffected As Integer = 0
        Dim szTxnAmount As String = ""      'Added 20 Oct 2017, Conversion Currerncy
        Dim szCurrencyCode As String = ""   'Added 20 Oct 2017, Conversion Currerncy

        Try
            bReturn = False
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spInsTxnResp", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_TxnType", SqlDbType.VarChar, 7)
            objParam = objCommand.Parameters.Add("@sz_GatewayTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_MerchantTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_TxnAmount", SqlDbType.VarChar, 18)
            objParam = objCommand.Parameters.Add("@d_TxnAmount", SqlDbType.Decimal, 18)   'Added on 24 Jun 2010
            objParam = objCommand.Parameters.Add("@sz_CurrencyCode", SqlDbType.VarChar, 3)
            objParam = objCommand.Parameters.Add("@sz_GatewayID", SqlDbType.VarChar, 4)
            objParam = objCommand.Parameters.Add("@sz_CardPAN", SqlDbType.VarChar, 20)
            objParam = objCommand.Parameters.Add("@i_HostID", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@sz_IssuingBank", SqlDbType.VarChar, 30)  'Modified 16 Aug 2013, increased from 20 to 30
            objParam = objCommand.Parameters.Add("@sz_RespMesg", SqlDbType.VarChar, 255)
            objParam = objCommand.Parameters.Add("@i_TxnState", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_MerchantTxnStatus", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_TxnStatus", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@sz_BankRespCode", SqlDbType.VarChar, 20) 'Modified on 27 Aug 2009, from varchar(4) to varchar(20)
            objParam = objCommand.Parameters.Add("@sz_AuthCode", SqlDbType.VarChar, 20)     'Added  7 Oct 2011 - CCGW
            objParam = objCommand.Parameters.Add("@sz_ECI", SqlDbType.VarChar, 20)          'Added  6 Oct 2011 - CCGW
            objParam = objCommand.Parameters.Add("@sz_PayerAuth", SqlDbType.VarChar, 20)    'Added  6 Oct 2011 - CCGW
            objParam = objCommand.Parameters.Add("@sz_AVS", SqlDbType.Char, 1)              'Added  17 July 2013 - PG
            objParam = objCommand.Parameters.Add("@sz_BankRefNo", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_HashMethod", SqlDbType.VarChar, 6)
            objParam = objCommand.Parameters.Add("@sz_HashValue", SqlDbType.VarChar, 200)    'Modified 16 Aug 2013, increased from 40 to 100 due to SHA256
            objParam = objCommand.Parameters.Add("@sz_OrderNumber", SqlDbType.VarChar, 20)
            objParam = objCommand.Parameters.Add("@sz_ErrDesc", SqlDbType.VarChar, 200)
            objParam = objCommand.Parameters.Add("@sz_Param1", SqlDbType.VarChar, 100)
            objParam = objCommand.Parameters.Add("@sz_Param2", SqlDbType.VarChar, 100)
            objParam = objCommand.Parameters.Add("@sz_Param3", SqlDbType.VarChar, 100)
            objParam = objCommand.Parameters.Add("@sz_Param4", SqlDbType.VarChar, 100)
            objParam = objCommand.Parameters.Add("@sz_Param5", SqlDbType.VarChar, 100)
            objParam = objCommand.Parameters.Add("@sz_Param6", SqlDbType.VarChar, 50)
            objParam = objCommand.Parameters.Add("@sz_Param7", SqlDbType.VarChar, 50)
            objParam = objCommand.Parameters.Add("@sz_Param8", SqlDbType.VarChar, 50)
            objParam = objCommand.Parameters.Add("@sz_Param9", SqlDbType.VarChar, 50)
            objParam = objCommand.Parameters.Add("@sz_Param10", SqlDbType.VarChar, 50)
            objParam = objCommand.Parameters.Add("@sz_Param11", SqlDbType.VarChar, 255)
            objParam = objCommand.Parameters.Add("@sz_Param12", SqlDbType.VarChar, 255)
            objParam = objCommand.Parameters.Add("@sz_Param13", SqlDbType.VarChar, 255)
            objParam = objCommand.Parameters.Add("@sz_Param14", SqlDbType.VarChar, 255)
            objParam = objCommand.Parameters.Add("@sz_Param15", SqlDbType.VarChar, 255)
            objParam = objCommand.Parameters.Add("@sz_MachineID", SqlDbType.VarChar, 10)
            objParam = objCommand.Parameters.Add("@i_OSRet", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_ErrSet", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_ErrNum", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@sz_Token", SqlDbType.VarChar, 50)        'Added, 1 Aug 2014, One-Click Payment
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            'Added on 9 Jan 2009
            If ("query" = st_PayInfo.szTxnType.ToLower()) Then
                objCommand.Parameters("@sz_TxnType").Value = "PAY"
            Else
                objCommand.Parameters("@sz_TxnType").Value = st_PayInfo.szTxnType
            End If

            objCommand.Parameters("@sz_GatewayTxnID").Value = st_PayInfo.szTxnID
            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@sz_MerchantTxnID").Value = st_PayInfo.szMerchantTxnID
            'Added 20 Oct 2017, Conversion Currency
            If ("" = st_PayInfo.szFXCurrencyCode) Then
                szTxnAmount = st_PayInfo.szTxnAmount
                szCurrencyCode = st_PayInfo.szCurrencyCode
            Else
                szTxnAmount = st_PayInfo.szBaseTxnAmount
                szCurrencyCode = st_PayInfo.szBaseCurrencyCode
            End If

            objCommand.Parameters("@sz_TxnAmount").Value = szTxnAmount
            objCommand.Parameters("@d_TxnAmount").Value = Convert.ToDecimal(szTxnAmount) 'Added on 24 Jun 2010
            objCommand.Parameters("@sz_CurrencyCode").Value = szCurrencyCode

            objCommand.Parameters("@sz_GatewayID").Value = st_PayInfo.szGatewayID
            objCommand.Parameters("@sz_CardPAN").Value = st_PayInfo.szMaskedCardPAN
            objCommand.Parameters("@i_HostID").Value = st_HostInfo.iHostID
            objCommand.Parameters("@sz_IssuingBank").Value = st_PayInfo.szIssuingBank

            'Modified 14 Nov 2013, if bank returned message, stores bank message or else stores gateway message
            If (st_PayInfo.szBankRespMsg <> "") Then
                objCommand.Parameters("@sz_RespMesg").Value = st_PayInfo.szBankRespMsg  'Response message returned from bank
            Else
                objCommand.Parameters("@sz_RespMesg").Value = st_PayInfo.szTxnMsg       'Response message generated by Gateway and returned to merchant, not storing Host's response message
            End If

            objCommand.Parameters("@i_TxnState").Value = st_PayInfo.iTxnState
            objCommand.Parameters("@i_MerchantTxnStatus").Value = st_PayInfo.iMerchantTxnStatus
            objCommand.Parameters("@i_TxnStatus").Value = st_PayInfo.iTxnStatus

            'Added, 28 Oct 2013, "-" which could be retrieved from bGetTxnResult() for txn not able to get resp code from Host
            If (st_PayInfo.szRespCode <> "" And st_PayInfo.szRespCode <> "-") Then
                objCommand.Parameters("@sz_BankRespCode").Value = st_PayInfo.szRespCode
            Else
                objCommand.Parameters("@sz_BankRespCode").Value = st_PayInfo.szHostTxnStatus
            End If
            'Added  20 April 2012 - if AuthCode is nothing then have to set to "" (empty) to avoid insert error in store proc
            If (st_PayInfo.szAuthCode = "") Then
                st_PayInfo.szAuthCode = ""
            End If
            objCommand.Parameters("@sz_AuthCode").Value = st_PayInfo.szAuthCode

            If (st_PayInfo.szECI = "") Then
                st_PayInfo.szECI = ""
            End If
            objCommand.Parameters("@sz_ECI").Value = st_PayInfo.szECI

            If (st_PayInfo.szPayerAuth = "") Then
                st_PayInfo.szPayerAuth = ""
            End If
            objCommand.Parameters("@sz_PayerAuth").Value = st_PayInfo.szPayerAuth

            If (st_PayInfo.szAVS = "") Then
                st_PayInfo.szAVS = ""
            End If
            objCommand.Parameters("@sz_AVS").Value = st_PayInfo.szAVS

            'Commented  27 Sept 2013
            If (st_PayInfo.szHostTxnID <> "") Then
                objCommand.Parameters("@sz_BankRefNo").Value = st_PayInfo.szHostTxnID
            Else
                objCommand.Parameters("@sz_BankRefNo").Value = "-"                      'Commented  30 Sept 2013, st_PayInfo.szAuthCode
            End If

            objCommand.Parameters("@sz_HashMethod").Value = st_PayInfo.szHashMethod

            'Added on 26 May 2009, to store the maximum length
            If ("" <> st_PayInfo.szHashValue) Then
                If (st_PayInfo.szHashValue.Length > 200) Then   'Modified  19 Jun 2014, increased from 40 to 100 due to SHA256. Firefly FFCIMB
                    st_PayInfo.szHashValue = st_PayInfo.szHashValue.Substring(0, 200)
                End If
            End If

            objCommand.Parameters("@sz_HashValue").Value = st_PayInfo.szHashValue
            objCommand.Parameters("@sz_OrderNumber").Value = st_PayInfo.szMerchantOrdID
            objCommand.Parameters("@sz_ErrDesc").Value = st_PayInfo.szErrDesc   'Error message generated by Gateway, not returned to merchant.
            objCommand.Parameters("@sz_Param1").Value = st_PayInfo.szParam1

            If (st_PayInfo.szParam2 <> "") Then
                objCommand.Parameters("@sz_Param2").Value = objHash.szEncryptAES(st_PayInfo.szParam2, Common.C_3DESAPPKEY)
            Else
                objCommand.Parameters("@sz_Param2").Value = st_PayInfo.szParam2
            End If

            If (st_PayInfo.szParam3 <> "") Then
                objCommand.Parameters("@sz_Param3").Value = objHash.szEncryptAES(st_PayInfo.szParam3, Common.C_3DESAPPKEY)
            Else
                objCommand.Parameters("@sz_Param3").Value = st_PayInfo.szParam3
            End If

            objCommand.Parameters("@sz_Param4").Value = st_PayInfo.szParam4
            objCommand.Parameters("@sz_Param5").Value = st_PayInfo.szParam5
            objCommand.Parameters("@sz_Param6").Value = st_PayInfo.szParam6
            objCommand.Parameters("@sz_Param7").Value = st_PayInfo.szParam7
            objCommand.Parameters("@sz_Param8").Value = st_PayInfo.szParam8
            objCommand.Parameters("@sz_Param9").Value = st_PayInfo.szParam9
            objCommand.Parameters("@sz_Param10").Value = st_PayInfo.szParam10
            objCommand.Parameters("@sz_Param11").Value = st_PayInfo.szParam11
            objCommand.Parameters("@sz_Param12").Value = st_PayInfo.szParam12
            objCommand.Parameters("@sz_Param13").Value = st_PayInfo.szParam13
            objCommand.Parameters("@sz_Param14").Value = st_PayInfo.szParam14
            objCommand.Parameters("@sz_Param15").Value = st_PayInfo.szParam15
            objCommand.Parameters("@sz_MachineID").Value = st_PayInfo.szMachineID
            objCommand.Parameters("@i_OSRet").Value = st_PayInfo.iOSRet
            objCommand.Parameters("@i_ErrSet").Value = st_PayInfo.iErrSet
            objCommand.Parameters("@i_ErrNum").Value = st_PayInfo.iErrNum
            objCommand.Parameters("@sz_Token").Value = st_PayInfo.szToken           'Added, 1 Aug 2014, One-Click Payment

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()
            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True
            Else
                st_PayInfo.szErrDesc = "bInsertTxnResp() error: Failed to insert transaction response. GatewayTxnID(" + st_PayInfo.szTxnID + ")"

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.iTxnState = Common.TXN_STATE.FAILED
                st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            End If

            dr.Close()

            'Added  13 jul 2017. update payment into VC & MP
            If ("V" = st_PayInfo.szGatewayID) Then
                Dim objVC As VisaCheckout = Nothing
                objVC = New VisaCheckout(objLoggerII)
                objVC.VCUpdatePayment(st_HostInfo, st_PayInfo)
                objVC.Dispose()
            ElseIf ("M" = st_PayInfo.szGatewayID) Then
                If (Common.TXN_STATUS.TXN_SUCCESS = st_PayInfo.iTxnStatus) Then
                    Dim objMP As MasterPass = Nothing
                    objMP = New MasterPass(objLoggerII)
                    objMP.MPPostbackMerchantTxn(st_HostInfo, st_PayInfo)
                    objMP.Dispose()
                End If
            End If
        Catch ex As Exception
            'Modified 1 Apr 2015, for GP, added [Could be ...]
            st_PayInfo.szErrDesc = "bInsertTxnResp() exception [Could be another bank resp process already inserted] [" + ex.ToString + "] > " + "GatewayTxnID(" + st_PayInfo.szTxnID + ")"

            'Commented  1 Apr 2015, for GP, do not overwrite the iMerchantTxnStatus in the passed in st_PayInfo ByRef param
            'st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            'st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            'st_PayInfo.iTxnState = Common.TXN_STATE.FAILED
            'st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bCheckUniqueTxn
    ' Function Type:	Boolean.  True if unique else false
    ' Parameter:		st_PayInfo
    ' Description:		Check the uniqueness of the transaction
    ' History:			17 Oct 2008
    '********************************************************************************************
    Public Function bCheckUniqueTxn(ByRef st_PayInfo As Common.STPayInfo) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing

        Dim iRowsAffected As Integer = 0

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spCheckUniqueTxn", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_MerchantTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@sz_MerchantTxnID").Value = st_PayInfo.szMerchantTxnID

            objConn.Open()
            iRowsAffected = objCommand.ExecuteNonQuery()

            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True
            Else
                bReturn = False

                st_PayInfo.szErrDesc = "bCheckUniqueTxn() error: Duplicate MerchantPymtID."

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INVALID_INPUT 'Common.ERROR_CODE.E_INVALID_INPUT
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT           'Common.TXN_STATE.FAILED
                st_PayInfo.szTxnMsg = Common.C_INVALID_INPUT_2906
            End If

        Catch ex As Exception
            st_PayInfo.szErrDesc = "bCheckUniqueTxn() exception: " + ex.ToString

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.FAILED
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bCheckNInsUniqueTxn
    ' Function Type:	Boolean.  True if unique and insertion successful else false
    ' Parameter:		st_PayInfo
    ' Description:		Check the uniqueness of the transaction and then inserts it into DB
    '                   If not unique but original Method is 'ANY' and txnamt matched, update actual payment method
    ' History:			18 Aug 2013
    '********************************************************************************************
    Public Function bCheckNInsUniqueTxn(ByRef b_UpdatedPymtMethod As Boolean, ByRef st_PayInfo As Common.STPayInfo) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim szTxnDay As String = ""
        Dim iDBRet As Integer = 0
        Dim szTxnAmount As String = ""      'Added, 3 Sept 2014, for FX
        Dim szCurrencyCode As String = ""   'Added, 3 Sept 2014, for FX

        Dim iRowsAffected As Integer = 0

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spCheckNInsUniqueTxn", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_MerchantTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@d_TxnAmount", SqlDbType.Decimal, 18)
            objParam = objCommand.Parameters.Add("@sz_CurrencyCode", SqlDbType.Char, 3)
            objParam = objCommand.Parameters.Add("@i_TxnDay", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@sz_PymtMethod", SqlDbType.VarChar, 3)
            objParam = objCommand.Parameters.Add("@sz_PMEntry", SqlDbType.Char, 1)          'Added, 23 Aug 2013
            objParam = objCommand.Parameters.Add("@sz_OrderNumber", SqlDbType.VarChar, 20)  'Added, 10 Sept 2013
            objParam = objCommand.Parameters.Add("@sz_SessionID", SqlDbType.VarChar, 30)    'Added, 11 Oct 2013
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            'Added, 3 Sept 2014, for FX
            If (st_PayInfo.szBaseCurrencyCode <> "" And st_PayInfo.szBaseCurrencyCode <> st_PayInfo.szCurrencyCode) Then
                szCurrencyCode = st_PayInfo.szBaseCurrencyCode
                szTxnAmount = st_PayInfo.szBaseTxnAmount
            Else
                szCurrencyCode = st_PayInfo.szCurrencyCode
                szTxnAmount = st_PayInfo.szTxnAmount
            End If

            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@sz_MerchantTxnID").Value = st_PayInfo.szMerchantTxnID

            'Modified 3 Sept 2014, for FX, use szTxnAmount and szCurrencyCode instead of st_PayInfo.szTxnAmount
            objCommand.Parameters("@d_TxnAmount").Value = Convert.ToDecimal(szTxnAmount)
            objCommand.Parameters("@sz_CurrencyCode").Value = szCurrencyCode

            szTxnDay = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
            objCommand.Parameters("@i_TxnDay").Value = Convert.ToInt32(Left(szTxnDay, 10).Replace("-", ""))
            objCommand.Parameters("@sz_PymtMethod").Value = st_PayInfo.szPymtMethod
            objCommand.Parameters("@sz_PMEntry").Value = st_PayInfo.szPMEntry               'Added, 23 Aug 2013
            objCommand.Parameters("@sz_OrderNumber").Value = st_PayInfo.szMerchantOrdID       'Added, 10 Sept 2013
            objCommand.Parameters("@sz_SessionID").Value = st_PayInfo.szSessionID       'Added, 11 Oct 2013
            If (st_PayInfo.szPromoOriAmt <> Nothing And st_PayInfo.iPromoID <> -1) Then
                objParam = objCommand.Parameters.Add("@d_PromoOriAmount", SqlDbType.Decimal, 18)
                objParam = objCommand.Parameters.Add("@i_PromoID", SqlDbType.Int)
                objCommand.Parameters("@d_PromoOriAmount").Value = st_PayInfo.szPromoOriAmt
                objCommand.Parameters("@i_PromoID").Value = st_PayInfo.iPromoID
            End If
            objConn.Open()
            iRowsAffected = objCommand.ExecuteNonQuery()

            iDBRet = objCommand.Parameters("@i_Return").Value

            st_PayInfo.bMultiEntries = False  'Added, 11 Oct 2013

            If iDBRet = 0 Then      'Unique txn and DB insertion is successful
                bReturn = True
            ElseIf iDBRet = 3 Then  'Added, 11 Oct 2013, same MerchantPymtID came in more than once within same session and 2 seconds (cater for IE 9)
                st_PayInfo.bMultiEntries = True
                b_UpdatedPymtMethod = True
                bReturn = True
            ElseIf iDBRet > 0 Then  'Unique txn but came in 2nd time and updated Payment Method 'ANY' to actual Payment Method successfully
                b_UpdatedPymtMethod = True
                bReturn = True
            Else
                bReturn = False

                st_PayInfo.szErrDesc = "[" + iDBRet.ToString() + "] bCheckNInsUniqueTxn() Duplicate MerchantPymtID/DB operation fail."

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INVALID_INPUT 'Common.ERROR_CODE.E_INVALID_INPUT
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT           'Common.TXN_STATE.FAILED
                st_PayInfo.szTxnMsg = Common.C_INVALID_INPUT_2906
            End If

        Catch ex As Exception
            st_PayInfo.szErrDesc = "bCheckNInsUniqueTxn() exception: " + ex.ToString

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.FAILED
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bGetCountryList
    ' Function Type:	Boolean.  True if get country list information successfully else false
    ' Parameter:		st_PayInfo
    ' Description:		Retrieves country list
    ' History:			9 Aug 2013
    '********************************************************************************************
    Public Function bGetCountryList(ByRef st_PayInfo As Common.STPayInfo, Optional ByVal sz_CountryCode As String = "") As Boolean

        Dim bReturn As Boolean = True
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRowsAffected As Integer = 0

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetCLCountry", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_CountryCode", SqlDbType.Char, 2)
            objParam = objCommand.Parameters.Add("@i_CountryID", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)    'Added, 25 May 2016
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_CountryCode").Value = sz_CountryCode
            objCommand.Parameters("@i_CountryID").Value = -1
            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID  'Added, 25 May 2016, for Watsons' payment page Country List 1st country to be Malaysia

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True

                'CountryName, CountryCode
                st_PayInfo.szHTML = ""  'Modified 10 Nov 2013
                While dr.Read()
                    'sz_CountryList = sz_CountryList + "<option value=""" + dr("CountryCodeA2").ToString() + """>" + dr("CountryName").ToString() + "</option>"
                    st_PayInfo.szHTML += dr("CountryCodeA2").ToString() + "|" + dr("CountryName").ToString() + ";"
                End While
                If (st_PayInfo.szHTML <> "") Then
                    st_PayInfo.szHTML = Left(st_PayInfo.szHTML, st_PayInfo.szHTML.Length() - 1) 'Exclude the rightmost ";"
                Else
                    st_PayInfo.szErrDesc = "bGetCountryList() dr.Read() error: Failed to get Country List"

                    st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                    st_PayInfo.szTxnMsg = Common.TXN_STATUS.INTERNAL_ERROR
                    st_PayInfo.iTxnState = Common.TXN_STATE.ABORT

                    bReturn = False
                End If
            Else
                st_PayInfo.szErrDesc = "bGetCountryList() error: Failed to get Country List"

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.szTxnMsg = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT

                bReturn = False
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetCountryList() exception: " + ex.StackTrace + " " + ex.Message

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bGetCardType
    ' Function Type:	Boolean.  True if get card type successfully else false
    ' Parameter:		st_PayInfo
    ' Description:		Retrieves card type based on Card PAN
    ' History:			29 Oct 2013
    '********************************************************************************************
    Public Function bGetCardType(ByRef st_PayInfo As Common.STPayInfo) As Boolean

        Dim bReturn As Boolean = True
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRowsAffected As Integer = 0

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetCLCardType", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_Card", SqlDbType.VarChar, 19)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_Card").Value = st_PayInfo.szCardPAN

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True

                While dr.Read()
                    st_PayInfo.szCardType = dr("CardTypeCode").ToString()
                    st_PayInfo.szCardTypeDesc = dr("CardType").ToString()   'Added, 9 Dec 2013
                    dr.Read()
                End While
            Else
                st_PayInfo.szErrDesc = "bGetCardType() error: Failed to get Card Type for Card[" + st_PayInfo.szCardPAN + "]"

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.szTxnMsg = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT

                bReturn = False
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetCardType() exception: " + ex.StackTrace + " " + ex.Message

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bGetFIList
    ' Function Type:	Boolean.  True if get Financial Institution list information successfully else false
    ' Parameter:		st_PayInfo
    ' Description:		Retrieves Financial Institution list
    ' History:			30 Sept 2013
    '********************************************************************************************
    Public Function bGetFIList(ByRef st_PayInfo As Common.STPayInfo, Optional ByVal iAction As Integer = 0) As Boolean

        Dim bReturn As Boolean = True
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRowsAffected As Integer = 0

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetCLFI", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@i_CountryID", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_Action", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@sz_BINName", SqlDbType.VarChar, 20)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)


            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@i_CountryID").Value = 0
            objCommand.Parameters("@i_Action").Value = iAction
            objCommand.Parameters("@sz_BINName").Value = st_PayInfo.szBINName

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True

                'FIShortName, FIName
                st_PayInfo.szHTML = ""  'Modified 10 Nov 2013
                While dr.Read()
                    'sz_FIList = sz_FIList + "<option value=""" + dr("FIShortName").ToString() + """>" + dr("FIName").ToString() + "</option>"
                    st_PayInfo.szHTML += dr("FIShortName").ToString() + "|" + dr("FIName").ToString() + ";"
                End While
                If (st_PayInfo.szHTML <> "") Then
                    st_PayInfo.szHTML = Left(st_PayInfo.szHTML, st_PayInfo.szHTML.Length() - 1)
                Else
                    st_PayInfo.szErrDesc = "bGetFIList() dr.Read() error: Failed to get Financial Institution List"

                    st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                    st_PayInfo.szTxnMsg = Common.TXN_STATUS.INTERNAL_ERROR
                    st_PayInfo.iTxnState = Common.TXN_STATE.ABORT

                    bReturn = False
                End If
            Else
                st_PayInfo.szErrDesc = "bGetFIList() error: Failed to get Financial Institution List"

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.szTxnMsg = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT

                bReturn = False
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetFIList() exception: " + ex.StackTrace + " " + ex.Message

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bGetInstallmentList
    ' Function Type:	Boolean.  True if get Installment list information successfully else false
    ' Parameter:		st_PayInfo
    ' Description:		Retrieves merchant host installment list
    ' History:			17 Nov 2015
    '********************************************************************************************
    Public Function bGetInstallmentList(ByRef st_PayInfo As Common.STPayInfo) As Boolean

        Dim bReturn As Boolean = True
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRowsAffected As Integer = 0

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetInstallmentList", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True

                'HIID, InstLists
                st_PayInfo.szHTML = ""
                While dr.Read()
                    st_PayInfo.szHTML += dr("HIID").ToString() + "|" + dr("InstLists").ToString() + ";"
                End While
                If (st_PayInfo.szHTML <> "") Then
                    st_PayInfo.szHTML = Left(st_PayInfo.szHTML, st_PayInfo.szHTML.Length() - 1)
                End If
            Else
                st_PayInfo.szErrDesc = "bGetInstallmentList() error: Failed to get Installment List"

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.szTxnMsg = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT

                bReturn = False
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetInstallmentList() exception: " + ex.StackTrace + " " + ex.Message

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bGetInstallmentDetails
    ' Function Type:	Boolean.  True if get Installment information successfully else false
    ' Parameter:		st_PayInfo
    ' Description:		Retrieves merchant host installment details
    ' History:			23 May 2016
    '********************************************************************************************
    Public Function bGetInstallmentDetails(ByRef st_PayInfo As Common.STPayInfo) As Boolean

        Dim bReturn As Boolean = True
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRowsAffected As Integer = 0

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetInstallmentDetails", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@i_HIID", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@i_HIID").Value = st_PayInfo.iInstallment

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True

                While dr.Read()
                    st_PayInfo.szInstallMthTerm = dr("TermMonth").ToString()
                    st_PayInfo.szInstallPlan = dr("PlanCode").ToString()
                End While

            Else
                st_PayInfo.szErrDesc = "bGetInstallmentDetails() error: Failed to get Installment Details"

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.szTxnMsg = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT

                bReturn = False
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetInstallmentDetails() exception: " + ex.StackTrace + " " + ex.Message

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bInsertNewTxn
    ' Function Type:	Boolean.  True if inserted successfully else false
    ' Parameter:		st_PayInfo
    ' Description:		Insert new transaction in DB
    ' History:			20 Oct 2008
    '********************************************************************************************
    Public Function bInsertNewTxn(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim szTxnAmount As String = ""      'Added 19 Oct 2017, Conversion Currency
        Dim szCurrencyCode As String = ""   'Added 19 Oct 2017, Conversion Currency
        Dim iRowsAffected As Integer = 0

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spInsertNewTxn", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_GatewayTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_MerchantName", SqlDbType.VarChar, 25)
            objParam = objCommand.Parameters.Add("@sz_MerchantReturnURL", SqlDbType.VarChar, 255)
            objParam = objCommand.Parameters.Add("@sz_MerchantSupportURL", SqlDbType.VarChar, 255)
            objParam = objCommand.Parameters.Add("@sz_MerchantTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_TxnAmount", SqlDbType.VarChar, 18)
            objParam = objCommand.Parameters.Add("@d_TxnAmount", SqlDbType.Decimal, 18)         'Added on 24 Jun 2010
            objParam = objCommand.Parameters.Add("@sz_BaseTxnAmount", SqlDbType.VarChar, 18)    'Added on 3 Aug 2009, multi currency booking confirmation enhancement
            objParam = objCommand.Parameters.Add("@sz_CurrencyCode", SqlDbType.VarChar, 3)
            objParam = objCommand.Parameters.Add("@sz_BaseCurrencyCode", SqlDbType.VarChar, 3)  'Added on 3 Aug 2009, multi currency booking confirmation enhancement
            objParam = objCommand.Parameters.Add("@sz_GatewayID", SqlDbType.VarChar, 4)
            objParam = objCommand.Parameters.Add("@sz_TxnType", SqlDbType.VarChar, 7)
            objParam = objCommand.Parameters.Add("@sz_OrderDesc", SqlDbType.NVarChar, 100)      'Modified 18 Oct 2014, VarChar to NVarChar
            objParam = objCommand.Parameters.Add("@sz_CardPAN", SqlDbType.VarChar, 20)
            objParam = objCommand.Parameters.Add("@sz_MaskedCardPAN", SqlDbType.VarChar, 20)
            objParam = objCommand.Parameters.Add("@sz_OrderNumber", SqlDbType.VarChar, 20)
            objParam = objCommand.Parameters.Add("@sz_HashMethod", SqlDbType.VarChar, 6)
            objParam = objCommand.Parameters.Add("@sz_HashValue", SqlDbType.VarChar, 200)       'Modified 16 Aug 2013, increased from 40 to 100 due to SHA256
            objParam = objCommand.Parameters.Add("@i_HostID", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@sz_IssuingBank", SqlDbType.VarChar, 30)      'Modified 16 Aug 2013, increased from 20 to 30
            objParam = objCommand.Parameters.Add("@sz_LanguageCode", SqlDbType.Char, 2)
            objParam = objCommand.Parameters.Add("@sz_SessionID", SqlDbType.VarChar, 100)
            objParam = objCommand.Parameters.Add("@sz_Channel", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_Param1", SqlDbType.VarChar, 100)
            objParam = objCommand.Parameters.Add("@sz_Param2", SqlDbType.VarChar, 100)
            objParam = objCommand.Parameters.Add("@sz_Param3", SqlDbType.VarChar, 100)
            objParam = objCommand.Parameters.Add("@sz_Param4", SqlDbType.VarChar, 100)
            objParam = objCommand.Parameters.Add("@sz_Param5", SqlDbType.VarChar, 100)
            objParam = objCommand.Parameters.Add("@sz_Param6", SqlDbType.VarChar, 50)
            objParam = objCommand.Parameters.Add("@sz_Param7", SqlDbType.VarChar, 50)
            objParam = objCommand.Parameters.Add("@sz_Param8", SqlDbType.VarChar, 50)
            objParam = objCommand.Parameters.Add("@sz_Param9", SqlDbType.VarChar, 50)
            objParam = objCommand.Parameters.Add("@sz_Param10", SqlDbType.VarChar, 50)
            objParam = objCommand.Parameters.Add("@sz_Param11", SqlDbType.VarChar, 255)
            objParam = objCommand.Parameters.Add("@sz_Param12", SqlDbType.VarChar, 255)
            objParam = objCommand.Parameters.Add("@sz_Param13", SqlDbType.VarChar, 255)
            objParam = objCommand.Parameters.Add("@sz_Param14", SqlDbType.VarChar, 255)
            objParam = objCommand.Parameters.Add("@sz_Param15", SqlDbType.VarChar, 255)
            objParam = objCommand.Parameters.Add("@sz_MachineID", SqlDbType.VarChar, 10)
            objParam = objCommand.Parameters.Add("@sz_DateCreated", SqlDbType.VarChar, 23)
            objParam = objCommand.Parameters.Add("@i_DateCreated", SqlDbType.Int)               'Added on 24 Jun 2010
            objParam = objCommand.Parameters.Add("@i_TxnTimeOut", SqlDbType.Int)

            ''Start - Added on 25 Jul 2013 more field for PG
            objParam = objCommand.Parameters.Add("@sz_CustIP", SqlDbType.VarChar, 20)
            objParam = objCommand.Parameters.Add("@sz_CustName", SqlDbType.NVarChar, 50)        'Added, 22 Aug 2013, modified on 26 Aug 2013, varchar to nvarchar(50)
            objParam = objCommand.Parameters.Add("@sz_MerchantIP", SqlDbType.VarChar, 20)       'Added, 6 Aug 2013
            objParam = objCommand.Parameters.Add("@sz_BillAddr", SqlDbType.NVarChar, 100)       'Modified 14 Aug 2013, increased from 50 to 100. 16 Aug 2013, varchar to nvarchar
            objParam = objCommand.Parameters.Add("@sz_BillCity", SqlDbType.NVarChar, 30)        'Modified 20 Aug 2013, varchar to nvarchar
            objParam = objCommand.Parameters.Add("@sz_BillRegion", SqlDbType.NVarChar, 30)      'Modified 20 Aug 2013, varchar to nvarchar
            objParam = objCommand.Parameters.Add("@sz_BillPostal", SqlDbType.VarChar, 15)
            objParam = objCommand.Parameters.Add("@sz_BillCountryID", SqlDbType.Char, 2)        'Modified 6 Aug 2013
            objParam = objCommand.Parameters.Add("@sz_ShipAddr", SqlDbType.NVarChar, 100)       'Modified 14 Aug 2013, increased from 50 to 100. 16 Aug 2013, varchar to nvarchar
            objParam = objCommand.Parameters.Add("@sz_ShipCity", SqlDbType.NVarChar, 30)        'Modified 20 Aug 2013, varchar to nvarchar
            objParam = objCommand.Parameters.Add("@sz_ShipRegion", SqlDbType.NVarChar, 30)      'Modified 20 Aug 2013, varchar to nvarchar
            objParam = objCommand.Parameters.Add("@sz_ShipPostal", SqlDbType.VarChar, 15)
            objParam = objCommand.Parameters.Add("@sz_ShipCountryID", SqlDbType.Char, 2)        'Modified 6 Aug 2013
            objParam = objCommand.Parameters.Add("@sz_CustEmail", SqlDbType.VarChar, 60)
            objParam = objCommand.Parameters.Add("@sz_CustPhone", SqlDbType.VarChar, 25)
            objParam = objCommand.Parameters.Add("@sz_BINCountry", SqlDbType.Char, 2)           'Added, 3 Oct 2013
            objParam = objCommand.Parameters.Add("@sz_BINName", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_BINPhone", SqlDbType.VarChar, 25)
            objParam = objCommand.Parameters.Add("@sz_CustMAC", SqlDbType.VarChar, 50)          'Added, 22 Aug 2013
            ''End

            ''Start - Added  21 May 2014, Firefly FF
            objParam = objCommand.Parameters.Add("@sz_3DFlag", SqlDbType.VarChar, 5)
            objParam = objCommand.Parameters.Add("@sz_ECI", SqlDbType.VarChar, 2)
            objParam = objCommand.Parameters.Add("@sz_CAVV", SqlDbType.VarChar, 40)
            objParam = objCommand.Parameters.Add("@sz_XID", SqlDbType.VarChar, 40)
            ''End

            ''Start - Added, 4 Jul 2014, Firefly FF
            objParam = objCommand.Parameters.Add("@sz_MerchantApprovalURL", SqlDbType.VarChar, 255)
            objParam = objCommand.Parameters.Add("@sz_MerchantUnApprovalURL", SqlDbType.VarChar, 255)
            ''End
            'Added  16 Oct 2014, Firefly FF SkySales
            objParam = objCommand.Parameters.Add("@sz_MerchantCallbackURL", SqlDbType.VarChar, 255)

            'Added, 12 Aug 2014, One-Click Payment
            objParam = objCommand.Parameters.Add("@i_CustOCP", SqlDbType.Int)

            'Added, 26 Mar 2015, for BNB
            objParam = objCommand.Parameters.Add("@sz_TokenType", SqlDbType.VarChar, 3)
            objParam = objCommand.Parameters.Add("@sz_Token", SqlDbType.VarChar, 50)

            'Added, 20 May 2016, for MyDin Card Type
            objParam = objCommand.Parameters.Add("@sz_CardType", SqlDbType.VarChar, 2)

            'Added 19 Oct 2017, Conversion Currency
            If (st_PayInfo.szFXCurrencyCode <> "") Then
                objParam = objCommand.Parameters.Add("@sz_FXCurrencyCode", SqlDbType.VarChar, 3)    'Added 19 Oct 2017, Conversion Currency
                objParam = objCommand.Parameters.Add("@d_FXTxnAmt", SqlDbType.Decimal, 18)          'Added 19 Oct 2017, Conversion Currency
                objParam = objCommand.Parameters.Add("@d_FXCurrOriRate", SqlDbType.Decimal, 18)     'Added 19 Oct 2017, Conversion Currency
                objParam = objCommand.Parameters.Add("@d_FXCurrMarkUp", SqlDbType.Decimal, 18)      'Added 19 Oct 2017, Conversion Currency
                objParam = objCommand.Parameters.Add("@d_FXMerchRate", SqlDbType.Decimal, 18)       'Added 19 Oct 2017, Conversion Currency

                szTxnAmount = st_PayInfo.szBaseTxnAmount        'Stores original TxnAmount in TxnAmount column while FXTxnAmount stored in TB_PayFX
                szCurrencyCode = st_PayInfo.szBaseCurrencyCode
            Else
                szTxnAmount = st_PayInfo.szTxnAmount
                szCurrencyCode = st_PayInfo.szCurrencyCode
            End If

            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_GatewayTxnID").Value = st_PayInfo.szTxnID
            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@sz_MerchantName").Value = st_PayInfo.szMerchantName
            objCommand.Parameters("@sz_MerchantReturnURL").Value = st_PayInfo.szMerchantReturnURL
            objCommand.Parameters("@sz_MerchantSupportURL").Value = st_PayInfo.szMerchantSupportURL
            objCommand.Parameters("@sz_MerchantTxnID").Value = st_PayInfo.szMerchantTxnID
            objCommand.Parameters("@sz_TxnAmount").Value = szTxnAmount                          'Modify  , 19 Oct 2017, Conversion Currency and comment st_PayInfo.szTxnAmount
            objCommand.Parameters("@d_TxnAmount").Value = Convert.ToDecimal(szTxnAmount)               'Added on 24 Jun 2010
            objCommand.Parameters("@sz_BaseTxnAmount").Value = st_PayInfo.szBaseTxnAmount       'Added on 3 Aug 2009, multi currency booking confirmation enhancement
            objCommand.Parameters("@sz_CurrencyCode").Value = szCurrencyCode                    'Modify  , 19 Oct 2017, Conversion Currency and comment st_PayInfo.szCurrencyCode
            objCommand.Parameters("@sz_BaseCurrencyCode").Value = st_PayInfo.szBaseCurrencyCode 'Added on 3 Aug 2009, multi currency booking confirmation enhancement
            objCommand.Parameters("@sz_GatewayID").Value = st_PayInfo.szGatewayID
            objCommand.Parameters("@sz_TxnType").Value = st_PayInfo.szTxnType
            objCommand.Parameters("@sz_OrderDesc").Value = st_PayInfo.szMerchantOrdDesc
            objCommand.Parameters("@sz_CardPAN").Value = st_PayInfo.szCardPAN
            objCommand.Parameters("@sz_MaskedCardPAN").Value = st_PayInfo.szMaskedCardPAN
            objCommand.Parameters("@sz_OrderNumber").Value = st_PayInfo.szMerchantOrdID
            objCommand.Parameters("@sz_HashMethod").Value = st_PayInfo.szHashMethod

            'Added on 26 May 2009, to store the maximum length
            'Modified 1 Sept 2013, changed 40 to 100
            If (st_PayInfo.szHashValue.Length > 200) Then
                st_PayInfo.szHashValue = st_PayInfo.szHashValue.Substring(0, 200)
            End If
            objCommand.Parameters("@sz_HashValue").Value = st_PayInfo.szHashValue

            objCommand.Parameters("@i_HostID").Value = st_HostInfo.iHostID
            objCommand.Parameters("@sz_IssuingBank").Value = st_PayInfo.szIssuingBank

            If ("" = st_PayInfo.szLanguageCode) Then
                objCommand.Parameters("@sz_LanguageCode").Value = "EN"
            Else
                objCommand.Parameters("@sz_LanguageCode").Value = st_PayInfo.szLanguageCode
            End If

            If (st_PayInfo.szMerchantSessionID <> "") Then
                objCommand.Parameters("@sz_SessionID").Value = st_PayInfo.szMerchantSessionID   'Modified 15 Jul 2014, added MerchantSessionID
            Else
                objCommand.Parameters("@sz_SessionID").Value = st_PayInfo.szSessionID
            End If

            objCommand.Parameters("@sz_Channel").Value = ""
            objCommand.Parameters("@sz_Param1").Value = st_PayInfo.szParam1
            If (st_PayInfo.szParam2 <> "") Then
                objCommand.Parameters("@sz_Param2").Value = objHash.szEncryptAES(st_PayInfo.szParam2, Common.C_3DESAPPKEY)
            Else
                objCommand.Parameters("@sz_Param2").Value = st_PayInfo.szParam2
            End If

            If (st_PayInfo.szParam3 <> "") Then
                objCommand.Parameters("@sz_Param3").Value = objHash.szEncryptAES(st_PayInfo.szParam3, Common.C_3DESAPPKEY)
            Else
                objCommand.Parameters("@sz_Param3").Value = st_PayInfo.szParam3
            End If

            objCommand.Parameters("@sz_Param4").Value = st_PayInfo.szParam4
            objCommand.Parameters("@sz_Param5").Value = st_PayInfo.szParam5
            objCommand.Parameters("@sz_Param6").Value = st_PayInfo.szParam6
            objCommand.Parameters("@sz_Param7").Value = st_PayInfo.szParam7
            objCommand.Parameters("@sz_Param8").Value = st_PayInfo.szParam8
            objCommand.Parameters("@sz_Param9").Value = st_PayInfo.szParam9
            'objCommand.Parameters("@sz_Param10").Value = st_PayInfo.szParam10
            'Added  23 May 2016. Installment. Store HIID
            If (0 <> st_PayInfo.iInstallment) Then
                objCommand.Parameters("@sz_Param10").Value = st_PayInfo.iInstallment
            Else
                objCommand.Parameters("@sz_Param10").Value = st_PayInfo.szParam10
            End If
            objCommand.Parameters("@sz_Param11").Value = st_PayInfo.szParam11
            objCommand.Parameters("@sz_Param12").Value = st_PayInfo.szParam12
            objCommand.Parameters("@sz_Param13").Value = st_PayInfo.szParam13
            objCommand.Parameters("@sz_Param14").Value = st_PayInfo.szParam14
            objCommand.Parameters("@sz_Param15").Value = st_PayInfo.szParam15
            objCommand.Parameters("@sz_MachineID").Value = st_PayInfo.szMachineID

            'Assigns the system date of the App Server to szTxnDateTime so that the datetime value sent to the bank
            'in payment request will be the same as the datetime value (retrieved from database) sent to the bank in
            'query request because this system date is inserted into database and not the database system date (GETDATE()).
            st_PayInfo.szTxnDateTime = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
            objCommand.Parameters("@sz_DateCreated").Value = st_PayInfo.szTxnDateTime
            objCommand.Parameters("@i_DateCreated").Value = Convert.ToInt32(Left(st_PayInfo.szTxnDateTime, 10).Replace("-", ""))    'Added on 24 Jun 2010

            objCommand.Parameters("@i_TxnTimeOut").Value = st_HostInfo.iTimeOut 'seconds

            ''Start - Added on 25 Jul 2013 more field for PG
            objCommand.Parameters("@sz_CustIP").Value = st_PayInfo.szCustIP
            objCommand.Parameters("@sz_CustName").Value = st_PayInfo.szCustName             'Added, 22 Aug 2013
            objCommand.Parameters("@sz_MerchantIP").Value = st_PayInfo.szMerchantIP         'Added, 6 Aug 2013
            objCommand.Parameters("@sz_BillAddr").Value = st_PayInfo.szBillAddr
            objCommand.Parameters("@sz_BillCity").Value = st_PayInfo.szBillCity
            objCommand.Parameters("@sz_BillRegion").Value = st_PayInfo.szBillRegion
            objCommand.Parameters("@sz_BillPostal").Value = st_PayInfo.szBillPostal
            objCommand.Parameters("@sz_BillCountryID").Value = st_PayInfo.szBillCountry   'Modified 6 Aug 2013
            objCommand.Parameters("@sz_ShipAddr").Value = st_PayInfo.szShipAddr
            objCommand.Parameters("@sz_ShipCity").Value = st_PayInfo.szShipCity
            objCommand.Parameters("@sz_ShipRegion").Value = st_PayInfo.szShipRegion
            objCommand.Parameters("@sz_ShipPostal").Value = st_PayInfo.szShipPostal
            objCommand.Parameters("@sz_ShipCountryID").Value = st_PayInfo.szShipCountry   'Modified 6 Aug 2013
            objCommand.Parameters("@sz_CustEmail").Value = st_PayInfo.szCustEmail
            objCommand.Parameters("@sz_CustPhone").Value = st_PayInfo.szCustPhone
            objCommand.Parameters("@sz_BINCountry").Value = st_PayInfo.szBINCountry     'Added, 3 Oct 2013
            objCommand.Parameters("@sz_BINName").Value = st_PayInfo.szBINName
            objCommand.Parameters("@sz_BINPhone").Value = st_PayInfo.szBINPhone
            objCommand.Parameters("@sz_CustMAC").Value = st_PayInfo.szCustMAC           'Added, 22 Aug 2013
            ''End

            ''Start - Added  21 May 2014, Firefly FF
            objCommand.Parameters("@sz_3DFlag").Value = st_PayInfo.sz3DFlag
            objCommand.Parameters("@sz_ECI").Value = st_PayInfo.szECI
            objCommand.Parameters("@sz_CAVV").Value = st_PayInfo.szCAVV
            objCommand.Parameters("@sz_XID").Value = st_PayInfo.sz3dsXID
            ''End

            ''Start - Added, 4 Jul 2014, Firefly FF
            objCommand.Parameters("@sz_MerchantApprovalURL").Value = st_PayInfo.szMerchantApprovalURL
            objCommand.Parameters("@sz_MerchantUnApprovalURL").Value = st_PayInfo.szMerchantUnApprovalURL
            ''End
            'Added  16 Oct 2014
            objCommand.Parameters("@sz_MerchantCallbackURL").Value = st_PayInfo.szMerchantCallbackURL

            'Added, 12 Aug 2014, One-Click Payment. Modified 12 Feb 2018, Tik FX
            If ("on" = st_PayInfo.szCustOCP.ToLower()) Then
                objCommand.Parameters("@i_CustOCP").Value = 1
            Else
                objCommand.Parameters("@i_CustOCP").Value = 0
            End If

            'Added, 26 Mar 2015, for BNB
            objCommand.Parameters("@sz_TokenType").Value = st_PayInfo.szTokenType
            objCommand.Parameters("@sz_Token").Value = st_PayInfo.szToken

            'Added 30 Nov 2015, Promotion
            If (st_PayInfo.iPromoID <> -1) Then
                objParam = objCommand.Parameters.Add("@d_PromoOriAmount", SqlDbType.Decimal, 18)
                objParam = objCommand.Parameters.Add("@i_PromoID", SqlDbType.Int)
                objCommand.Parameters("@d_PromoOriAmount").Value = st_PayInfo.szPromoOriAmt
                objCommand.Parameters("@i_PromoID").Value = st_PayInfo.iPromoID

            End If

            'Added, 20 May 2016, for MyDin Card Type
            objCommand.Parameters("@sz_CardType").Value = st_PayInfo.szCardType

            'Added 19 Oct 2017, Conversion Currency
            If (st_PayInfo.szFXCurrencyCode <> "") Then
                objCommand.Parameters("@sz_FXCurrencyCode").Value = st_PayInfo.szFXCurrencyCode
                objCommand.Parameters("@d_FXTxnAmt").Value = Convert.ToDecimal(st_PayInfo.szFXTxnAmount)
                objCommand.Parameters("@d_FXCurrOriRate").Value = Convert.ToDecimal(st_PayInfo.szFXCurrencyOriRate)
                objCommand.Parameters("@d_FXCurrMarkUp").Value = Convert.ToDecimal(st_PayInfo.szFXCurrencyMarkUp)
                objCommand.Parameters("@d_FXMerchRate").Value = st_PayInfo.dMerchFXRate
            End If

            objConn.Open()
            iRowsAffected = objCommand.ExecuteNonQuery()

            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True
            Else
                bReturn = False

                st_PayInfo.szErrDesc = "bInsertNewTxn() error: Failed to insert new transaction. GatewayTxnID(" + st_PayInfo.szTxnID + ")"

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.E_FAILED_STORE_TXN
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT                   'Common.TXN_STATE.FAILED
                st_PayInfo.szTxnMsg = Common.C_FAILED_STORE_TXN_906
            End If

        Catch ex As Exception
            st_PayInfo.szErrDesc = "bInsertNewTxn() exception: " + ex.ToString + " > " + "GatewayTxnID(" + st_PayInfo.szTxnID + ")"

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT                    'Common.TXN_STATE.FAILED
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bGetMerchant
    ' Function Type:	Boolean.  True if get merchant information successfully else false
    ' Parameter:		st_PayInfo
    ' Description:		Get merchant information
    ' History:			20 Oct 2008
    '********************************************************************************************
    Public Function bGetMerchant(ByRef st_PayInfo As Common.STPayInfo) As Boolean
        Dim bReturn As Boolean = True
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRowsAffected As Integer = 0

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetMerchant", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID

            If Not String.IsNullOrEmpty(st_PayInfo.szPromoCode) Then
                objParam = objCommand.Parameters.Add("@sz_PromoCode", SqlDbType.VarChar, 10)    'Added 30 June 2016, Product Promotion
                objCommand.Parameters("@sz_PromoCode").Value = st_PayInfo.szPromoCode   'Added 30 June 2016, Product Promotion
            End If

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True

                'Chg Start JOYCE 20190402, chg string to secure string
                'Dim szMerchantPassword As String = ""
                Dim szMerchantPassword As SecureString = New SecureString()
                'Chg E n d JOYCE 20190402, chg string to secure string

                While dr.Read()
                    'Chg Start JOYCE 20190402, chg string to secure string
                    'szMerchantPassword = dr("MerchantPassword").ToString()
                    szMerchantPassword = New NetworkCredential("", dr("MerchantPassword").ToString()).SecurePassword    ' MerchantPassword
                    'Chg E n d JOYCE 20190402, chg string to secure string
                    st_PayInfo.szPaymentTemplate = dr("PaymentTemplate").ToString()     ' PaymentTemplate
                    st_PayInfo.szErrorTemplate = dr("ErrorTemplate").ToString()         ' ErrorTemplate
                    st_PayInfo.iAllowReversal = Convert.ToInt32(dr("AllowReversal"))    ' Allow Reversal
                    st_PayInfo.iAllowFDS = Convert.ToInt32(dr("AllowFDS"))              ' Allow basic FDS       - Added  Jul 2013
                    st_PayInfo.iCollectShipAddr = Convert.ToInt32(dr("CollectShipAddr"))  ' Ship Address Required - Added, 26 Jul 2013; Modified 29 Sept 2013, AllowShipAddr to CollectShipAddr
                    st_PayInfo.iCollectBillAddr = Convert.ToInt32(dr("CollectBillAddr"))  ' Bill Address Required - Added, 25 Oct 2013
                    st_PayInfo.iAllowMaxMind = Convert.ToInt32(dr("AllowMaxMind"))      ' MaxMind subscribed    - Added, 6 Aug 2013
                    st_PayInfo.szFraudByAmt = dr("FraudByAmt").ToString()               ' If AllowMaxMind is ON (1) then optional to set Amt to send to MaxMind for fraud check.   'Added on 23 Jul 2013  IPG
                    st_PayInfo.iSvcType = Convert.ToInt32(dr("SvcTypeID"))              ' Service Type: 1->Bank-Direct; 2->G-Direct - Added, 23 Aug 2013
                    st_PayInfo.iAllowPayment = Convert.ToInt32(dr("AllowPayment"))      ' Allow Payment         - Added, 7 Aug 2013
                    st_PayInfo.iAllowQuery = Convert.ToInt32(dr("AllowQuery"))          ' Allow Query           - Added, 7 Aug 2013
                    st_PayInfo.iAllowOB = Convert.ToInt32(dr("AllowOB"))                ' Allow Online Banking  - Added, 7 Aug 2013
                    st_PayInfo.iAllowOTC = Convert.ToInt32(dr("AllowOTC"))              ' Allow Over The Counter- Added, 2 Sept 2014
                    st_PayInfo.iAllowWallet = Convert.ToInt32(dr("AllowWallet"))        ' Allow Wallet          - Added, 7 Aug 2013
                    st_PayInfo.iAllowOCP = Convert.ToInt32(dr("AllowOCP"))              ' Allow One-Click Pymt  - Added, 17 Jul 2014
                    st_PayInfo.iAllowCallBack = Convert.ToInt32(dr("AllowCallBack"))    ' Allow Call Back       - Added, 17 Jul 2014
                    st_PayInfo.iAllowFX = Convert.ToInt32(dr("AllowFX"))                ' Allow Foreign Exchange- Added, 24 Jul 2014
                    st_PayInfo.iRouteByParam1 = Convert.ToInt32(dr("RouteByParam1"))    ' Route By Param1       - Added, 11 Apr 2014
                    st_PayInfo.iPymtPageTimeout_S = Convert.ToInt32(dr("PymtPageTimeout_S"))  ' Payment page timeout - Added, 26 Jul 2013; Modified 29 Sept 2013, to CardEntryTimeout_S to PymtPageTimeout_S
                    st_PayInfo.i3DAccept = Convert.ToInt32(dr("ThreeDAccept"))          ' 3D acceptance     - Added, 28 Jul 2013
                    st_PayInfo.iSelfMPI = Convert.ToInt32(dr("SelfMPI"))                ' Merchant has own MPI - Added  22 May 2014, Firefly FF
                    st_PayInfo.iAutoReversal = Convert.ToInt32(dr("AutoReversal"))      ' Auto Reversal     - Added, 3 Oct 2013
                    st_PayInfo.iRespMethod = Convert.ToInt32(dr("RespMethod"))          ' Response Method   - Added, 3 Oct 2013
                    st_PayInfo.iHCProfileID = Convert.ToInt32(dr("HCProfileID"))        ' OB Host Country Profile ID - Added, 5 Dec 2013
                    st_PayInfo.dPerTxnAmtLimit = Convert.ToDecimal(dr("PerTxnAmtLimit")) ' Transaction Amount Limit - Added, 16 Jan 2014
                    st_PayInfo.iHitLimitAct = Convert.ToInt32(dr("HitLimitAct"))        ' Added 08 April 2019, Joyce

                    st_PayInfo.szMerchantCity = dr("City").ToString()                   'Added  6 Jun 2014. Firefly FFCTB
                    st_PayInfo.szMerchantCountry = dr("Country2").ToString()            'Added  6 Jun 2014. Firefly FFCTB

                    ' Merchant address - Added, 29 Jul 2013 - multiple line address
                    'st_PayInfo.szMerchantAddress = dr("Addr1").ToString().Trim() + ",<br>"
                    'If (dr("Addr2").ToString().Trim() <> "") Then
                    '    st_PayInfo.szMerchantAddress = st_PayInfo.szMerchantAddress + dr("Addr2").ToString().Trim() + ",<br>"
                    'End If
                    'If (dr("Addr3").ToString().Trim() <> "") Then
                    '    st_PayInfo.szMerchantAddress = st_PayInfo.szMerchantAddress + dr("Addr3").ToString().Trim() + ",<br>"
                    'End If
                    'If ("PH" = st_PayInfo.szMerchantCountry) Then
                    '    st_PayInfo.szMerchantAddress = st_PayInfo.szMerchantAddress + dr("City").ToString() + " " + dr("PostCode").ToString() + ",<br>"
                    'Else
                    '    st_PayInfo.szMerchantAddress = st_PayInfo.szMerchantAddress + dr("PostCode").ToString() + " " + dr("City").ToString() + ",<br>"
                    'End If

                    ' Merchant address - single line address
                    st_PayInfo.szMerchantAddress = dr("Addr1").ToString().Trim() + ",&nbsp;"
                    If (dr("Addr2").ToString().Trim() <> "") Then
                        st_PayInfo.szMerchantAddress = st_PayInfo.szMerchantAddress + dr("Addr2").ToString().Trim() + ",&nbsp;"
                    End If
                    If (dr("Addr3").ToString().Trim() <> "") Then
                        st_PayInfo.szMerchantAddress = st_PayInfo.szMerchantAddress + dr("Addr3").ToString().Trim() + ",&nbsp;"
                    End If
                    If ("PH" = st_PayInfo.szMerchantCountry) Then
                        st_PayInfo.szMerchantAddress = st_PayInfo.szMerchantAddress + dr("City").ToString() + " " + dr("PostCode").ToString() + ",&nbsp;"
                    Else
                        st_PayInfo.szMerchantAddress = st_PayInfo.szMerchantAddress + dr("PostCode").ToString() + " " + dr("City").ToString() + ",&nbsp;"
                    End If

                    'Modified  6 Nov 2017, for PH without State
                    If (dr("State").ToString().Trim() <> "") Then
                        st_PayInfo.szMerchantAddress = st_PayInfo.szMerchantAddress + dr("State").ToString() + "," + dr("Country").ToString()
                    Else
                        st_PayInfo.szMerchantAddress = st_PayInfo.szMerchantAddress + dr("Country").ToString()
                    End If
                    'st_PayInfo.szMerchantAddress = st_PayInfo.szMerchantAddress + dr("State").ToString() + "," + dr("Country").ToString()
                    st_PayInfo.szMerchantAddress = Replace(st_PayInfo.szMerchantAddress, ",,", ",")

                    'st_PayInfo.szMerchantAddress = Replace(st_PayInfo.szMerchantAddress, "&nbsp;", "") 'Added, 18 Aug 2017, requested by LNS
                    'st_PayInfo.szMerchantAddress = Replace(st_PayInfo.szMerchantAddress, " ,", ",") 'Added, 18 Aug 2017, requested by LNS
                    'st_PayInfo.szMerchantAddress = Replace(st_PayInfo.szMerchantAddress, ",", ", ") 'Added, 18 Aug 2017, requested by LNS
                    'st_PayInfo.szMerchantAddress = Replace(st_PayInfo.szMerchantAddress, ", ,", ",") 'Added, 18 Aug 2017, requested by LNS

                    st_PayInfo.szMerchantContactNo = dr("ContactNo").ToString()         ' Merchant Contact Number - Added, 30 Jul 2013
                    st_PayInfo.szMerchantEmailAddr = dr("EmailAddr").ToString()                 ' Merchant Customer Service Email Address - Added, 30 Jul 2013
                    st_PayInfo.szMerchantNotifyEmailAddr = dr("NotifyEmailAddr").ToString()     ' Merchant Notification Email Address - Added, 8 Apr 2014
                    st_PayInfo.szMerchantWebSiteURL = dr("WebSiteURL").ToString()       ' Merchant Website URL - Added, 30 Jul 2013
                    st_PayInfo.szMerchantValidDomain = dr("ValidDomain").ToString()     ' Merchant Valid Domain - Added, 11 Oct 2013

                    st_PayInfo.iVISA = Convert.ToInt32(dr("VISA"))                      ' Support VISA       - Added, 31 Jul 2013
                    st_PayInfo.iMasterCard = Convert.ToInt32(dr("MasterCard"))          ' Support MasterCard - Added, 31 Jul 2013
                    st_PayInfo.iAMEX = Convert.ToInt32(dr("AMEX"))                      ' Support AMEX   - Added, 31 Jul 2013
                    st_PayInfo.iJCB = Convert.ToInt32(dr("JCB"))                        ' Support JCB    - Added, 6 Aug 2013
                    st_PayInfo.iDiners = Convert.ToInt32(dr("Diners"))                  ' Support Diners - Added, 6 Aug 2013
                    st_PayInfo.iCUP = Convert.ToInt32(dr("CUP"))                        ' Support CUP    - Added, 6 Aug 2013
                    st_PayInfo.iMasterPass = Convert.ToInt32(dr("MasterPass"))          ' Added  20 Sept 2016. MasterPass
                    st_PayInfo.iVisaCheckout = Convert.ToInt32(dr("VisaCheckout"))      ' Added  3 Apr 2017. VisaCheckout
                    st_PayInfo.iSamsungPay = Convert.ToInt32(dr("SamsungPay"))          ' Added SSP, 15 Oct 2017. SamsungPay

                    st_PayInfo.iNeedAddOSPymt = Convert.ToInt32(dr("NeedAddOSPymt"))    ' NeedAddOSPymt
                    st_PayInfo.iTxnExpired_S = Convert.ToInt32(dr("TxnExpired_S"))      ' Added, 22 Sept 2013, after this time, if query still failed, txn will be finalized

                    'Added, 23 Aug 2013, if MerchantName is not provided, take the registered Merchant Name from DB
                    If ("" = st_PayInfo.szMerchantName) Then
                        st_PayInfo.szMerchantName = dr("MerchantName").ToString()       ' Merchant Name - Added on 4 Apr 2010
                    End If

                    st_PayInfo.szMerchantName2 = dr("MerchantName").ToString()                          ' Added  5 Oct 2017, field for merchant name from db even there's a merchant name on payment request.
                    st_PayInfo.iReceipt = Convert.ToInt32(dr("Receipt"))                                ' Added, 29 Sept 2013, shows receipt page only if it is set to required
                    st_PayInfo.iPymtNotificationEmail = Convert.ToInt32(dr("PymtNotificationEmail"))    ' Added, 29 Sept 2013
                    st_PayInfo.iPymtNotificationSMS = Convert.ToInt32(dr("PymtNotificationSMS"))        ' Added, 29 Sept 2013
                    st_PayInfo.dMerchFXRate = Convert.ToDecimal(dr("FXRate"))                           ' Added, 20 Aug 2014
                    st_PayInfo.iShowMerchantAddr = Convert.ToDecimal(dr("ShowMerchantAddr"))            ' Added, 2 Oct 2014, to show or hide merchant address on payment page
                    st_PayInfo.iShowMerchantLogo = Convert.ToDecimal(dr("ShowMerchantLogo"))            ' Added, 13 Oct 2014, to show or hide merchant logo on payment page
                    st_PayInfo.szExtraCSS = dr("ExtraCSS").ToString()                                   ' Added, 14 Oct 2014, to have extra CSS supported for payment page, e.g. different colour scheme than the default blue
                    st_PayInfo.iOTCExpiryHour = Convert.ToInt32(dr("OTCExpiryHour"))                    ' Added, 15 Apr 2015, order expiry for OTC transaction
                    st_PayInfo.iReturnCardData = Convert.ToInt32(dr("ReturnCardData"))                  ' Added, 7 Oct 2015, flag whether want to return card data in payment and query response, requested by MyDin

                    st_PayInfo.iPromoExist = Convert.ToInt32(dr("PROMO"))                               ' Added 20 Nov 2015, promotion
                    st_PayInfo.iAllowInstallment = Convert.ToInt32(dr("AllowInstallment"))              ' Added  15 Jan 2016, Installment

                    st_PayInfo.iMCCCode = Convert.ToInt32(dr("MCCCode"))                                ' Added  Fraudwall
                    st_PayInfo.iAllowExtFDS = Convert.ToInt32(dr("AllowExtFDS"))                        ' Added  Fraudwall 13 Apr 2016
                    st_PayInfo.szFDSCustomerID = dr("FDSCustomerID").ToString()                         ' Added  Fraudwall
                    st_PayInfo.szFDSAuthCode = dr("FDSAuthCode").ToString()                             ' Added  Fraudwall

                    st_PayInfo.szProtocol = dr("Protocol").ToString()                                   ' Added, 13 Sept 2016, support callback resp protocol
                    st_PayInfo.szAcqCountryCode = dr("AcqCountryCode").ToString()                       ' Added, 24 Oct 2016, support fraud score email template by country

                    st_PayInfo.szPostCode = dr("PostCode").ToString()                                   ' Added  29 Mar 2017, for SMI/Paymaya
                    st_PayInfo.iCVVRequire = Convert.ToInt32(dr("CVVRequire"))                          ' Added 12 Feb 2018, Tik FX

                    dr.Read()
                End While

                'Chg Start JOYCE 20190402, as szMerchantPassword from string chg to secure string
                'If (szMerchantPassword <> "") Then
                If (New NetworkCredential("", szMerchantPassword).Password <> "") Then

                    'Commented on 11 Apr 2010, this DecryptString will return empty string!
                    'If szMerchantPassword.Length > 8 Then ' Merchant Password less than 8 characters in DB is clear
                    '    Dim objCrypto As NMXEncInterop.CryptoClass
                    '    Dim szKey As String
                    '    Dim iRet As Short = 0

                    '    Try
                    '        objCrypto = New NMXEncInterop.CryptoClass
                    '        szKey = "39333139623039623232323034353038"
                    '        st_PayInfo.szMerchantPassword = objCrypto.DecryptString(szMerchantPassword, szKey, 3, iRet)

                    '    Catch ex As Exception
                    '        bReturn = False

                    '        st_PayInfo.szErrDesc = "bGetMerchant() NMXEncInterop exception: " + ex.Message

                    '        st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                    '        st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                    '        st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
                    '        st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                    '    Finally
                    '        If Not IsNothing(objCrypto) Then objCrypto = Nothing
                    '    End Try
                    'End If

                    'If st_PayInfo.szMerchantPassword.Length <= 0 Then
                    '    st_PayInfo.szMerchantPassword = szMerchantPassword
                    'End If

                    'Modified the above  17 Apr 2014, will hit exception because st_PayInfo.szMerchantPassword is empty, when .length, will hit exception
                    'Chg Start JOYCE 20190402, chg string to secure string
                    'st_PayInfo.szMerchantPassword = szMerchantPassword
                    st_PayInfo.szMerchantPassword = New NetworkCredential("", szMerchantPassword).Password
                    'Chg E n d JOYCE 20190402, chg string to secure string
                Else
                    bReturn = False

                    st_PayInfo.szErrDesc = "bGetMerchant() error: Invalid MerchantID/Merchant."

                    st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.E_INVALID_MERCHANT
                    st_PayInfo.szTxnMsg = Common.C_INVALID_MID_2901
                    st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
                End If
            Else
                bReturn = False

                st_PayInfo.szErrDesc = "bGetMerchant() error: Invalid MerchantID/Merchant."

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.E_INVALID_MERCHANT
                st_PayInfo.szTxnMsg = Common.C_INVALID_MID_2901
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetMerchant() exception: " + ex.StackTrace + " " + ex.Message  ' .Message

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bGetHostInfo
    ' Function Type:	Boolean.  True if get host information successfully else false
    ' Parameter:		st_HostInfo, st_PayInfo
    ' Description:		Get host information
    ' History:			22 Oct 2008
    '********************************************************************************************
    Public Function bGetHostInfo(ByRef st_HostInfo As Common.STHost, ByRef st_PayInfo As Common.STPayInfo) As Boolean
        Dim bReturn As Boolean = True
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRowsAffected As Integer = 0

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetHostInfo", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_HostName", SqlDbType.VarChar, 20)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_HostName").Value = st_PayInfo.szIssuingBank

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True

                'HostID, HostName, PaymentTemplate, NeedReplyAcknowledgement, Require2ndEntry, AllowReversal,
                'ChannelID, [Timeout], TxnStatusActionID, HostReplyMethod, OSPymtCode, DateActivated, DateDeactivated, RecStatus, [Desc], PayURL, CfgFile
                While dr.Read()
                    st_HostInfo.iHostID = Convert.ToInt32(dr("HostID"))                                     ' HostID
                    st_HostInfo.szPaymentTemplate = dr("PaymentTemplate").ToString()                        ' PaymentTemplate
                    st_HostInfo.szSecondEntryTemplate = dr("SecondEntryTemplate").ToString()                ' SecondEntryTemplate
                    st_HostInfo.szMesgTemplate = dr("MesgTemplate").ToString()                              ' Message Template - Added on 27 Oct 2009
                    st_HostInfo.iRequire2ndEntry = Convert.ToInt32(dr("Require2ndEntry"))                   ' Require2ndEntry
                    st_HostInfo.szHostTemplate = dr("CfgFile").ToString()                                   ' CfgFile
                    st_HostInfo.iNeedReplyAcknowledgement = Convert.ToInt32(dr("NeedReplyAcknowledgement")) ' NeedReplyAcknowledgement
                    st_HostInfo.iNeedRedirectOTP = Convert.ToInt32(dr("NeedRedirectOTP"))                   ' NeedRedirectOTP - Added on 21 Mar 2010
                    st_HostInfo.iTxnStatusActionID = Convert.ToInt32(dr("TxnStatusActionID"))               ' TxnStatusActionID
                    st_HostInfo.iHostReplyMethod = Convert.ToInt32(dr("HostReplyMethod"))                   ' HostReplyMethod
                    st_HostInfo.szOSPymtCode = dr("OSPymtCode").ToString()                                  ' OSPymtCode
                    st_HostInfo.iGatewayTxnIDFormat = Convert.ToInt32(dr("GatewayTxnIDFormat"))             ' GatewayTxnIDFormat
                    st_HostInfo.iTimeOut = Convert.ToInt32(dr("Timeout"))                                   ' Timeout - if txn more than this timeout but host's query resp still returned pending, then fail the txn
                    st_HostInfo.iChannelTimeOut = Convert.ToInt32(dr("ChannelTimeout"))                     ' Timeout - Added, 18 Nov 2015, MOTO, cater for receive timeout between PG and MPG
                    st_HostInfo.szHashMethod = dr("HashMethod").ToString()                                  ' Hash Method
                    st_HostInfo.szReturnIPAddresses = dr("ReturnIPAddresses").ToString()                    ' Return IP Addresses
                    st_HostInfo.iQueryFlag = Convert.ToInt32(dr("QueryFlag"))
                    st_HostInfo.iIsActive = Convert.ToInt32(dr("IsActive"))
                    st_HostInfo.iAllowQuery = Convert.ToInt32(dr("AllowQuery"))                             ' AllowQuery - Added  11 Jun 2014. Firefly FFCTB
                    st_HostInfo.iAllowReversal = Convert.ToInt32(dr("AllowReversal"))                       ' AllowReversal - Added on 10 Apr 2010
                    st_HostInfo.szDesc = dr("Desc").ToString()                                              ' Host Description - Added on 9 Dec 2013
                    st_HostInfo.szProtocol = dr("Protocol").ToString()                                      ' Security Protocol, e.g. SSL3, TLS, TLS1.1, TLS1.2 - Added, 21 Jun 2015
                    'Commented the following due to stored procedure updated on 15 June 2009 because the following fields should be
                    'host-merchant-based, Not host-based only, therefore, needs to be stored in Terminals table instead of Host table
                    'st_HostInfo.szSendKey = dr("MerchantPassword").ToString()                              ' Merchant Password (Send Key)
                    'st_HostInfo.szReturnKey = dr("ReturnKey").ToString()                                   ' Return Key
                    'st_HostInfo.szSecretKey = dr("SecretKey").ToString()                                   ' Encryption Key
                    'st_HostInfo.szInitVector = dr("InitVector").ToString()                                 ' Initialization Vector
                    If ("" <> dr("SecretKey").ToString()) Then  ' Added checking on 1 Oct 2010
                        If (IsNumeric(dr("SecretKey").ToString())) Then 'Modified 9 Feb 2018, SPay, added checking of IsNumeric
                            st_HostInfo.iRunningNoUniquePerDay = Convert.ToInt32(dr("SecretKey"))           ' Boolean for whether Trace Number/Running Number is unique per day, reuse SecretKey which had been moved from Host to Terminals table - Added on 18 Sept 2010
                        Else
                            st_HostInfo.szSecretKey = dr("SecretKey").ToString()    'Added, 9 Feb 2018, SPay
                        End If
                    End If

                    If ("" <> dr("InitVector").ToString()) Then  ' Added checking on 1 Oct 2010
                        st_HostInfo.iNoOfRunningNo = Convert.ToInt32(dr("InitVector"))                      ' Number of digits for Trace Number/Running Number, reuse InitVector which had been moved from Host to Terminals table - Added on 18 Sept 2010
                    End If

                    st_HostInfo.iLogRes = Convert.ToInt32(dr("LogRes"))     'Added  23 Oct 2014. Firefly FFAMBANK
                    st_HostInfo.szQueryURL = dr("QueryURL").ToString()      'Added19 Jan 2016, for BancNet
                    st_HostInfo.szSelectedCurrExpnt = dr("SelectedCurrExpnt").ToString()       'Added  13 Sept 2017. FYAMEX
                    st_HostInfo.iHostMPI = Convert.ToInt32(dr("MPI"))       'MPGS (RHB), 11 Jun 2018
                    dr.Read()
                End While
            Else
                st_PayInfo.szErrDesc = "bGetHostInfo() error: Failed to get Host Info" + " > IssuingBank(" + st_PayInfo.szIssuingBank + ")"

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR 'Common.ERROR_CODE.E_INVALID_MID
                st_PayInfo.szTxnMsg = Common.C_INVALID_HOST_902
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT

                bReturn = False
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetHostInfo() exception: " + ex.StackTrace + " " + ex.Message  ' .Message

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR 'Common.ERROR_CODE.E_INVALID_MID
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bGetHostLogo
    ' Function Type:	Boolean.  True if get logoes of hosts that support the currency successfully else false
    ' Parameter:		st_HostInfo, st_PayInfo
    ' Description:		Get logoes of hosts that support the currency
    ' History:			16 Mar 2010
    '********************************************************************************************
    Public Function bGetHostLogo(ByVal st_HostInfo As Common.STHost, ByRef st_PayInfo As Common.STPayInfo, Optional ByVal sz_CurrencyCode As String = "") As Boolean

        Dim bReturn As Boolean = True
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRowsAffected As Integer = 0

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetHostLogo", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@i_HCProfileID", SqlDbType.Int)           'Added, 7 Dec 2013
            objParam = objCommand.Parameters.Add("@sz_HostName", SqlDbType.VarChar, 20)
            objParam = objCommand.Parameters.Add("@sz_CurrencyCode", SqlDbType.Char, 3)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@i_HCProfileID").Value = st_PayInfo.iHCProfileID         'Added, 7 Dec 2013
            objCommand.Parameters("@sz_HostName").Value = st_PayInfo.szIssuingBank

            'Modified 14 Apr 2015, added checking of sz_CurrencyCode
            If (sz_CurrencyCode <> "") Then
                objCommand.Parameters("@sz_CurrencyCode").Value = sz_CurrencyCode           'Added, 14 Apr 2015
            ElseIf (0 = st_PayInfo.iAllowFX And st_PayInfo.szCurrencyCode <> "") Then       'Added, 25 Aug 2017, to support showing only the host that supports the processing currency
                objCommand.Parameters("@sz_CurrencyCode").Value = st_PayInfo.szCurrencyCode 'Added, 14 Apr 2015
            Else
                objCommand.Parameters("@sz_CurrencyCode").Value = ""                        'Modified 10 Nov 2013, since can be different currency then conversion is required
            End If

            'objCommand.Parameters("@sz_CurrencyCode").Value = ""    'Modified 10 Nov 2013, since can be different currency then conversion is required

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True

                'HostName, HostDesc, LogoPath, CurrencyCode, CountryName, CountryCodeA2
                st_PayInfo.szHTML = ""
                While dr.Read()
                    st_PayInfo.szHTML += dr("HostName").ToString() + "|" + dr("HostDesc").ToString() + "|" + dr("LogoPath").ToString() + "|" + dr("CurrencyCode").ToString() + "|" + dr("CountryName").ToString() + "|" + dr("CountryCodeA2").ToString() + ";"
                End While
                If (st_PayInfo.szHTML <> "") Then
                    st_PayInfo.szHTML = Left(st_PayInfo.szHTML, st_PayInfo.szHTML.Length() - 1) 'Exclude the rightmost ";"
                Else
                    st_PayInfo.szErrDesc = "bGetHostLogo() error: dr.Read() Failed" + " > MerchantID(" + st_PayInfo.szMerchantID + ") HostName(" + st_PayInfo.szIssuingBank + ") CurrencyCode(" + st_PayInfo.szCurrencyCode + ")"

                    st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                    st_PayInfo.szTxnMsg = Common.C_INVALID_HOST_902
                    st_PayInfo.iTxnState = Common.TXN_STATE.ABORT

                    bReturn = False
                End If
            Else
                st_PayInfo.szErrDesc = "bGetHostLogo() error: Failed to get Host Logo" + " > MerchantID(" + st_PayInfo.szMerchantID + ") HostName(" + st_PayInfo.szIssuingBank + ") CurrencyCode(" + st_PayInfo.szCurrencyCode + ")"

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.szTxnMsg = Common.C_INVALID_HOST_902
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT

                bReturn = False
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetHostLogo() exception: " + ex.StackTrace + " " + ex.Message

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bGetCurrencyRate
    ' Function Type:	Boolean.  True if get currency rate successfully else false
    ' Parameter:		st_PayInfo
    ' Description:		Get currency rate
    ' History:			19 Aug 2014
    '                   Modified 25 Aug 2014, added parameter sz_ToCurrency
    '********************************************************************************************
    Public Function bGetCurrencyRate(ByRef st_PayInfo As Common.STPayInfo, ByVal sz_ToCurrency As String) As Boolean

        Dim bReturn As Boolean = True
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRowsAffected As Integer = 0
        Dim szCurrencyCode As String = ""           'Added, 4 Sept 2014, for FX

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetCurrencyRate", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)    'Added, 21 Aug 2014
            objParam = objCommand.Parameters.Add("@sz_FromCurr", SqlDbType.VarChar, 3)
            objParam = objCommand.Parameters.Add("@sz_ToCurr", SqlDbType.VarChar, 3)
            objParam = objCommand.Parameters.Add("@sz_PymtMethod", SqlDbType.VarChar, 3)    'Added 19 Oct 2017, Conversion Currency
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            'Added, 4 Sept 2014, for FX
            If ("" = st_PayInfo.szBaseCurrencyCode) Then
                szCurrencyCode = st_PayInfo.szCurrencyCode
            Else
                szCurrencyCode = st_PayInfo.szBaseCurrencyCode
            End If

            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID          'Added, 21 Aug 2014
            objCommand.Parameters("@sz_FromCurr").Value = szCurrencyCode
            objCommand.Parameters("@sz_ToCurr").Value = sz_ToCurrency                       'Modified 21 Aug 2014, added "OB" to retrieve registered ob banks' currency
            'Modified 25 Aug 2014, changed "OB" to sz_ToCurrency
            objCommand.Parameters("@sz_PymtMethod").Value = st_PayInfo.szPymtMethod         'Added 19 Oct 2017, Conversion Currency
            'Added, 14 Oct 2016
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > FromCurrency(" + szCurrencyCode + ") ToCurrency(" + sz_ToCurrency + ")")

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True

                'ToCurrencyCountry, ToCurrency, Rate, Currency Mark Up Rate
                st_PayInfo.szHTML = ""
                While dr.Read()
                    st_PayInfo.szHTML += dr("ToCurrencyCountry").ToString() + "|" + dr("ToCurrency").ToString() + "|" + dr("Rate").ToString() + "|" + dr("CurrMarkUpRate").ToString() + "|" + dr("OriRate").ToString() + ";" 'Modified 25 Aug 2014, added Currency Mark Up Rate
                End While
                If (st_PayInfo.szHTML <> "") Then
                    st_PayInfo.szHTML = Left(st_PayInfo.szHTML, st_PayInfo.szHTML.Length() - 1) 'Exclude the rightmost ";"
                Else
                    st_PayInfo.szErrDesc = "bGetCurrencyRate() error: dr.Read() Failed" + " > FromCurrency(" + st_PayInfo.szCurrencyCode + ")"

                    st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                    st_PayInfo.szTxnMsg = Common.C_INVALID_HOST_902
                    st_PayInfo.iTxnState = Common.TXN_STATE.ABORT

                    bReturn = False
                End If
            Else
                st_PayInfo.szErrDesc = "bGetCurrencyRate() error: Failed to get Currency Rate" + " > FromCurrency(" + st_PayInfo.szCurrencyCode + ")"

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.szTxnMsg = Common.C_INVALID_HOST_902
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT

                bReturn = False
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetCurrencyRate() exception: " + ex.StackTrace + " " + ex.Message

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bGetHostCountry
    ' Function Type:	Boolean.  True if get countries belong to Hosts supported by the merchant, else false
    ' Parameter:		st_HostInfo, st_PayInfo
    ' Description:		Get countries belong to the hosts supported by the merchant
    ' History:			10 Nov 2013
    '********************************************************************************************
    Public Function bGetHostCountry(ByVal st_HostInfo As Common.STHost, ByRef st_PayInfo As Common.STPayInfo) As Boolean

        Dim bReturn As Boolean = True
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRowsAffected As Integer = 0

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetHostCountry", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@i_HCProfileID", SqlDbType.Int)               'Added, 5 Dec 2013
            objParam = objCommand.Parameters.Add("@sz_HostName", SqlDbType.VarChar, 20)
            'objParam = objCommand.Parameters.Add("@sz_CurrencyCode", SqlDbType.Char, 3)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@i_HCProfileID").Value = st_PayInfo.iHCProfileID             'Added, 5 Dec 2013
            objCommand.Parameters("@sz_HostName").Value = st_PayInfo.szIssuingBank  'Can be empty or not empty, depending on whether it is submitted by merchant in payment request
            'objCommand.Parameters("@sz_CurrencyCode").Value = ""

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True

                'CountryName, CountryCodeA2
                st_PayInfo.szHTML = ""
                While dr.Read()
                    st_PayInfo.szHTML += dr("CountryCodeA2").ToString() + "|" + dr("CountryName").ToString() + ";"
                End While
                If (st_PayInfo.szHTML <> "") Then
                    st_PayInfo.szHTML = Left(st_PayInfo.szHTML, st_PayInfo.szHTML.Length() - 1)
                Else
                    st_PayInfo.szErrDesc = "bGetHostCountry() error: dr.Read() Failed" + " > MerchantID(" + st_PayInfo.szMerchantID + ")"

                    st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                    st_PayInfo.szTxnMsg = Common.C_INVALID_HOST_902
                    st_PayInfo.iTxnState = Common.TXN_STATE.ABORT

                    bReturn = False
                End If
            Else
                st_PayInfo.szErrDesc = "bGetHostCountry() error: Failed to get Host Country" + " > MerchantID(" + st_PayInfo.szMerchantID + ")"

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.szTxnMsg = Common.C_INVALID_HOST_902
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT

                bReturn = False
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetHostCountry() exception: " + ex.StackTrace + " " + ex.Message

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bGetTerminal
    ' Function Type:	Boolean.  True if get terminal and channel information successfully else false
    ' Parameter:		st_HostInfo, st_PayInfo
    ' Description:		Get terminal information
    ' History:			27 Oct 2008
    '********************************************************************************************
    Public Function bGetTerminal(ByRef st_HostInfo As Common.STHost, ByRef st_PayInfo As Common.STPayInfo) As Boolean

        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRowsAffected As Integer = 0
        Dim szCurrencyCode As String = ""       'Added 19 Oct 2017, COnversion Currency
        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetTerminal", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_CurrencyCode", SqlDbType.Char, 3)
            objParam = objCommand.Parameters.Add("@sz_AirlineCode", SqlDbType.VarChar, 3)
            objParam = objCommand.Parameters.Add("@i_HostID", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_HIID", SqlDbType.Int)      'Added  2 Dec 2015. Installment.

            If ("A" = st_PayInfo.szCardType) Then   'Added  14 Dec 2017. To support same host but different account number specific for Masterpass/VCO
                If ("M" = st_PayInfo.szGatewayID Or "V" = st_PayInfo.szGatewayID) Then
                    objParam = objCommand.Parameters.Add("@i_MPVCO", SqlDbType.Int)     'Added  14 Dec 2017. Masterpass
                End If
            End If

            'Added  27 Dec 2017. Same merchant has 2 accounts from same bank. Eg normal and auth+capture account
            'Re-use RouteByParam1=2 to select correct terminal records.
            If ("AUTH" = st_PayInfo.szTxnType Or "CAPTURE" = st_PayInfo.szTxnType) Then
                If (2 = st_PayInfo.iRouteByParam1) Then
                    objParam = objCommand.Parameters.Add("@i_MPVCO", SqlDbType.Int)
                End If
            End If

            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            'Modified 26 Aug 2014, for FX, added getting Terminal by FX Currency Code
            If ("" = st_PayInfo.szFXCurrencyCode) Then
                szCurrencyCode = st_PayInfo.szCurrencyCode
            Else
                szCurrencyCode = st_PayInfo.szBaseCurrencyCode
            End If
            objCommand.Parameters("@sz_CurrencyCode").Value = szCurrencyCode
            objCommand.Parameters("@sz_AirlineCode").Value = "" 'Empty for the moment
            objCommand.Parameters("@i_HostID").Value = st_HostInfo.iHostID  ' Retrieved in bGetHostInfo based on IssuingBank
            objCommand.Parameters("@i_HIID").Value = st_PayInfo.iInstallment   'Added  2 Dec 2015. Installment.

            If ("A" = st_PayInfo.szCardType) Then   'Added  14 Dec 2017. To support same host but different account number specific for Masterpass/VCO
                If ("M" = st_PayInfo.szGatewayID Or "V" = st_PayInfo.szGatewayID) Then
                    objCommand.Parameters("@i_MPVCO").Value = 1
                End If
            End If

            'Added  27 Dec 2017. Same merchant has 2 accounts from same bank. Eg normal and auth+capture account
            'Re-use RouteByParam1=2 to select correct terminal records.
            If ("AUTH" = st_PayInfo.szTxnType Or "CAPTURE" = st_PayInfo.szTxnType) Then
                If (2 = st_PayInfo.iRouteByParam1) Then
                    objCommand.Parameters("@i_MPVCO").Value = 1
                End If
            End If

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If objCommand.Parameters("@i_Return").Value = 0 Then
                '14 May 2009, ExecuteReader will not be able to get the exact Return value by stored procedure
                'will always get 0, that's why commented the following and put bReturn = True in while dr.Read()
                'bReturn = True

                'TerminalID, MID, TID, PayURL, QueryURL, ReversalURL, ExtraURL, CfgFile, ReturnURL, Timeout
                While dr.Read()
                    bReturn = True
                    st_HostInfo.szMID = dr("MID").ToString()                                ' MID
                    st_HostInfo.szTID = dr("TID").ToString()                                ' TID
                    st_HostInfo.szURL = dr("PayURL").ToString()                             ' Pay URL
                    st_HostInfo.szQueryURL = dr("QueryURL").ToString()                      ' Query URL
                    st_HostInfo.szCancelURL = dr("CancelURL").ToString()                    ' Cancel URL
                    st_HostInfo.szReversalURL = dr("ReversalURL").ToString()                ' Reversal URL
                    st_HostInfo.szAcknowledgementURL = dr("ExtraURL").ToString()            ' ExtraURL - Reply Acknowledgement URL
                    st_HostInfo.iPortNumber = Convert.ToInt32(dr("PayPort"))                ' Pay Port
                    st_HostInfo.iQueryPortNumber = Convert.ToInt32(dr("QueryPort"))         ' Query Port
                    st_HostInfo.iCancelPortNumber = Convert.ToInt32(dr("CancelPort"))       ' Cancel Port
                    st_HostInfo.iReversalPortNumber = Convert.ToInt32(dr("ReversalPort"))   ' Reversal Port
                    st_HostInfo.iAcknowledgementPortNumber = Convert.ToInt32(dr("ExtraPort")) ' Extra Port - Reply Acknowledgement Port
                    st_HostInfo.szHostTemplate = dr("CfgFile").ToString()       ' CfgFile
                    st_HostInfo.szReturnURL = dr("ReturnURL").ToString()        ' ReturnURL
                    st_HostInfo.szReturnURL2 = dr("ReturnURL2").ToString()      ' ReturnURL2 - Added on 22 Oct 2009
                    st_HostInfo.szReturnIPAddresses = dr("ReturnIPAddresses").ToString()
                    st_HostInfo.szAcquirerID = dr("AcquirerID").ToString()
                    'Added the following fields on 15 June 2009 due to stored procedure updated on 15 June 2009 because the following fields should be
                    'host-merchant-based, Not host-based only, therefore, needs to be stored in Terminals table instead of Host table
                    st_HostInfo.szSendKey = dr("SendKey").ToString()                               ' Merchant Password (Send Key)
                    st_HostInfo.szReturnKey = dr("ReturnKey").ToString()                                    ' Return Key

                    '-SPay 'Added condition because szSecretKey will have value from Host table for SamsungPay to avoid replacing with empty from terminal 
                    If dr("SecretKey").ToString().Trim() <> "" Then
                        st_HostInfo.szSecretKey = dr("SecretKey").ToString()                                ' Encryption Key
                    End If

                    st_HostInfo.szInitVector = dr("InitVector").ToString()                                  ' Initialization Vector
                    st_HostInfo.szPayeeCode = dr("PayeeCode").ToString()                                    ' Payee Code
                    'Added Public and Private key file path on 9 Nov 2009
                    st_PayInfo.szEncryptKeyPath = dr("SendKeyPath").ToString()                              ' Encrypt Key File Path for sending
                    st_PayInfo.szDecryptKeyPath = dr("ReturnKeyPath").ToString()                            ' Decrypt Key File Path for receiving
                    st_PayInfo.szAirlineCode = dr("AirlineCode").ToString()                                 ' Added  3 March 2015. Reuse AirlineCode as Trxn Security Code for GPay Reversal.
                    st_HostInfo.szFixedCurrency = dr("FXCurrencyCode").ToString()                           ' Added 19 Oct 2017, Currency Conversion
                    st_HostInfo.iMerchantHostTimeout = Convert.ToInt32(dr("MerchantHostTimeout"))           ' Added, 9 Jan 2018
                    dr.Read()
                End While

                If (False = bReturn) Then
                    st_PayInfo.szErrDesc = "bGetTerminal() error: Failed to get terminal" + " > HostID(" + st_HostInfo.iHostID.ToString() + ") CurrencyCode(" + st_PayInfo.szCurrencyCode + ")"

                    st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                    st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                    st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
                End If
            Else
                bReturn = False

                st_PayInfo.szErrDesc = "bGetTerminal() error: Failed to get terminal" + " > HostID(" + st_HostInfo.iHostID.ToString() + ") CurrencyCode(" + st_PayInfo.szCurrencyCode + ")"

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False
            st_PayInfo.szErrDesc = "bGetTerminal() exception: " + ex.StackTrace + " " + ex.Message + " > HostID(" + st_HostInfo.iHostID.ToString() + ") CurrencyCode(" + st_PayInfo.szCurrencyCode + ")"

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bGetTerminalEx
    ' Function Type:	Boolean.  True if get terminal and channel information successfully else false
    ' Parameter:		st_HostInfo, st_PayInfo
    ' Description:		Get terminal information by Bank MID
    ' History:			16 Apr 2014
    '********************************************************************************************
    Public Function bGetTerminalEx(ByRef st_HostInfo As Common.STHost, ByRef st_PayInfo As Common.STPayInfo) As Boolean

        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRowsAffected As Integer = 0

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetTerminalEx", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_CurrencyCode", SqlDbType.Char, 3)
            objParam = objCommand.Parameters.Add("@sz_MID", SqlDbType.VarChar, 30)  'Modified 14 Nov 2016, increased from 20 to 30, Ambank
            objParam = objCommand.Parameters.Add("@i_HostID", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@sz_CurrencyCode").Value = st_PayInfo.szCurrencyCode
            objCommand.Parameters("@sz_MID").Value = st_HostInfo.szMID
            objCommand.Parameters("@i_HostID").Value = st_HostInfo.iHostID  ' Retrieved in bGetHostInfo based on IssuingBank

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If objCommand.Parameters("@i_Return").Value = 0 Then
                '14 May 2009, ExecuteReader will not be able to get the exact Return value by stored procedure
                'will always get 0, that's why commented the following and put bReturn = True in while dr.Read()
                'bReturn = True

                'TerminalID, MID, TID, PayURL, QueryURL, ReversalURL, ExtraURL, CfgFile, ReturnURL, Timeout
                While dr.Read()
                    bReturn = True
                    'st_HostInfo.szMID = dr("MID").ToString()                               ' MID
                    st_HostInfo.szTID = dr("TID").ToString()                                ' TID
                    st_HostInfo.szURL = dr("PayURL").ToString()                             ' Pay URL
                    st_HostInfo.szQueryURL = dr("QueryURL").ToString()                      ' Query URL
                    st_HostInfo.szCancelURL = dr("CancelURL").ToString()                    ' Cancel URL
                    st_HostInfo.szReversalURL = dr("ReversalURL").ToString()                ' Reversal URL
                    st_HostInfo.szAcknowledgementURL = dr("ExtraURL").ToString()            ' ExtraURL - Reply Acknowledgement URL
                    st_HostInfo.iPortNumber = Convert.ToInt32(dr("PayPort"))                ' Pay Port
                    st_HostInfo.iQueryPortNumber = Convert.ToInt32(dr("QueryPort"))         ' Query Port
                    st_HostInfo.iCancelPortNumber = Convert.ToInt32(dr("CancelPort"))       ' Cancel Port
                    st_HostInfo.iReversalPortNumber = Convert.ToInt32(dr("ReversalPort"))   ' Reversal Port
                    st_HostInfo.iAcknowledgementPortNumber = Convert.ToInt32(dr("ExtraPort")) ' Extra Port - Reply Acknowledgement Port
                    st_HostInfo.szHostTemplate = dr("CfgFile").ToString()       ' CfgFile
                    st_HostInfo.szReturnURL = dr("ReturnURL").ToString()        ' ReturnURL
                    st_HostInfo.szReturnURL2 = dr("ReturnURL2").ToString()      ' ReturnURL2 - Added on 22 Oct 2009
                    st_HostInfo.szReturnIPAddresses = dr("ReturnIPAddresses").ToString()
                    st_HostInfo.szAcquirerID = dr("AcquirerID").ToString()
                    'Added the following fields on 15 June 2009 due to stored procedure updated on 15 June 2009 because the following fields should be
                    'host-merchant-based, Not host-based only, therefore, needs to be stored in Terminals table instead of Host table
                    st_HostInfo.szSendKey = dr("SendKey").ToString()                               ' Merchant Password (Send Key)
                    st_HostInfo.szReturnKey = dr("ReturnKey").ToString()                                    ' Return Key
                    st_HostInfo.szSecretKey = dr("SecretKey").ToString()                                    ' Encryption Key
                    st_HostInfo.szInitVector = dr("InitVector").ToString()                                  ' Initialization Vector
                    st_HostInfo.szPayeeCode = dr("PayeeCode").ToString()                                    ' Payee Code
                    'Added Public and Private key file path on 9 Nov 2009
                    st_PayInfo.szEncryptKeyPath = dr("SendKeyPath").ToString()                              ' Encrypt Key File Path for sending
                    st_PayInfo.szDecryptKeyPath = dr("ReturnKeyPath").ToString()                            ' Decrypt Key File Path for receiving
                    st_HostInfo.szFixedCurrency = dr("FXCurrencyCode").ToString()                           ' Added 19 Oct 2017, Currency Conversion 
                    dr.Read()
                End While

                If (False = bReturn) Then
                    st_PayInfo.szErrDesc = "bGetTerminalEx() error: Failed to get terminal" + " > HostID(" + st_HostInfo.iHostID.ToString() + ") CurrencyCode(" + st_PayInfo.szCurrencyCode + ") MID(" + st_HostInfo.szMID + ")"

                    st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                    st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                    st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
                End If
            Else
                bReturn = False

                st_PayInfo.szErrDesc = "bGetTerminalEx() error: Failed to get terminal" + " > HostID(" + st_HostInfo.iHostID.ToString() + ") CurrencyCode(" + st_PayInfo.szCurrencyCode + ") MID(" + st_HostInfo.szMID + ")"

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False
            st_PayInfo.szErrDesc = "bGetTerminalEx() exception: " + ex.StackTrace + " " + ex.Message + " > HostID(" + st_HostInfo.iHostID.ToString() + ") CurrencyCode(" + st_PayInfo.szCurrencyCode + ") MID(" + st_HostInfo.szMID + ")"

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bGetTerminalEx2
    ' Function Type:	Boolean.  True if get terminal and channel information successfully else false
    ' Parameter:		st_HostInfo, st_PayInfo
    ' Description:		Get terminal information
    ' History:			14 Dec 2017 - Added  for Masterpass/VCO
    '********************************************************************************************
    Public Function bGetTerminalEx2(ByRef st_HostInfo As Common.STHost, ByRef st_PayInfo As Common.STPayInfo) As Boolean

        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRowsAffected As Integer = 0

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetTerminalEx2", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_AirlineCode", SqlDbType.VarChar, 3)
            objParam = objCommand.Parameters.Add("@i_HostID", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_HIID", SqlDbType.Int)      'Added  2 Dec 2015. Installment.
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@sz_AirlineCode").Value = "" 'Empty for the moment
            objCommand.Parameters("@i_HostID").Value = st_HostInfo.iHostID  ' Retrieved in bGetHostInfo based on IssuingBank
            objCommand.Parameters("@i_HIID").Value = st_PayInfo.iInstallment   'Added  2 Dec 2015. Installment.

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If objCommand.Parameters("@i_Return").Value = 0 Then
                '14 May 2009, ExecuteReader will not be able to get the exact Return value by stored procedure
                'will always get 0, that's why commented the following and put bReturn = True in while dr.Read()
                'bReturn = True

                'TerminalID, MID, TID, PayURL, QueryURL, ReversalURL, ExtraURL, CfgFile, ReturnURL, Timeout
                While dr.Read()
                    bReturn = True
                    st_HostInfo.szMID = dr("MID").ToString()                                ' MID
                    st_HostInfo.szTID = dr("TID").ToString()                                ' TID
                    st_HostInfo.szURL = dr("PayURL").ToString()                             ' Pay URL
                    st_HostInfo.szQueryURL = dr("QueryURL").ToString()                      ' Query URL
                    st_HostInfo.szCancelURL = dr("CancelURL").ToString()                    ' Cancel URL
                    st_HostInfo.szReversalURL = dr("ReversalURL").ToString()                ' Reversal URL
                    st_HostInfo.szAcknowledgementURL = dr("ExtraURL").ToString()            ' ExtraURL - Reply Acknowledgement URL
                    st_HostInfo.iPortNumber = Convert.ToInt32(dr("PayPort"))                ' Pay Port
                    st_HostInfo.iQueryPortNumber = Convert.ToInt32(dr("QueryPort"))         ' Query Port
                    st_HostInfo.iCancelPortNumber = Convert.ToInt32(dr("CancelPort"))       ' Cancel Port
                    st_HostInfo.iReversalPortNumber = Convert.ToInt32(dr("ReversalPort"))   ' Reversal Port
                    st_HostInfo.iAcknowledgementPortNumber = Convert.ToInt32(dr("ExtraPort")) ' Extra Port - Reply Acknowledgement Port
                    st_HostInfo.szHostTemplate = dr("CfgFile").ToString()       ' CfgFile
                    st_HostInfo.szReturnURL = dr("ReturnURL").ToString()        ' ReturnURL
                    st_HostInfo.szReturnURL2 = dr("ReturnURL2").ToString()      ' ReturnURL2 - Added on 22 Oct 2009
                    st_HostInfo.szReturnIPAddresses = dr("ReturnIPAddresses").ToString()
                    st_HostInfo.szAcquirerID = dr("AcquirerID").ToString()
                    'Added the following fields on 15 June 2009 due to stored procedure updated on 15 June 2009 because the following fields should be
                    'host-merchant-based, Not host-based only, therefore, needs to be stored in Terminals table instead of Host table
                    st_HostInfo.szSendKey = dr("SendKey").ToString()                               ' Merchant Password (Send Key)
                    st_HostInfo.szReturnKey = dr("ReturnKey").ToString()                                    ' Return Key
                    st_HostInfo.szSecretKey = dr("SecretKey").ToString()                                    ' Encryption Key
                    st_HostInfo.szInitVector = dr("InitVector").ToString()                                  ' Initialization Vector
                    st_HostInfo.szPayeeCode = dr("PayeeCode").ToString()                                    ' Payee Code
                    'Added Public and Private key file path on 9 Nov 2009
                    st_PayInfo.szEncryptKeyPath = dr("SendKeyPath").ToString()                              ' Encrypt Key File Path for sending
                    st_PayInfo.szDecryptKeyPath = dr("ReturnKeyPath").ToString()                            ' Decrypt Key File Path for receiving
                    st_PayInfo.szAirlineCode = dr("AirlineCode").ToString()                                 ' Added  3 March 2015. Reuse AirlineCode as Trxn Security Code for GPay Reversal.
                    st_HostInfo.szFixedCurrency = dr("FXCurrencyCode").ToString()                           ' Added 19 Oct 2017, Currency Conversion 
                    dr.Read()
                End While

                If (False = bReturn) Then
                    st_PayInfo.szErrDesc = "bGetTerminalEx2() error: Failed to get terminal" + " > HostID(" + st_HostInfo.iHostID.ToString() + ") CurrencyCode(-)"

                    st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                    st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                    st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
                End If
            Else
                bReturn = False

                st_PayInfo.szErrDesc = "bGetTerminalEx2() error: Failed to get terminal" + " > HostID(" + st_HostInfo.iHostID.ToString() + ") CurrencyCode(-)"

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False
            st_PayInfo.szErrDesc = "bGetTerminalEx2() exception: " + ex.StackTrace + " " + ex.Message + " > HostID(" + st_HostInfo.iHostID.ToString() + ") CurrencyCode(-)"

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bUpdateTxnStatus
    ' Function Type:	Boolean.  True if update successful else false
    ' Parameter:		st_PayInfo, st_HostInfo
    ' Description:		Updates transaction status into database
    ' History:			27 Oct 2008
    '********************************************************************************************
    Public Function bUpdateTxnStatus(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost) As Boolean

        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRowsAffected As Integer = 0

        Try
            bReturn = False
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spUpdateTxnStatus", objConn)
            objCommand.CommandType = CommandType.StoredProcedure
            'check is there able to add hostid for search as param
            objParam = objCommand.Parameters.Add("@i_TxnStatus", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_MerchantTxnStatus", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_TxnState", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_Action", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@sz_BankRespCode", SqlDbType.VarChar, 20) 'Modified 4 to 20 on 12 Oct 2009
            objParam = objCommand.Parameters.Add("@sz_BankRefNo", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_OSPymtCode", SqlDbType.VarChar, 3)
            objParam = objCommand.Parameters.Add("@i_OSRet", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_ErrSet", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_ErrNum", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@sz_GatewayTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@i_TxnStatus").Value = st_PayInfo.iTxnStatus
            objCommand.Parameters("@i_MerchantTxnStatus").Value = st_PayInfo.iMerchantTxnStatus
            objCommand.Parameters("@i_TxnState").Value = st_PayInfo.iTxnState
            objCommand.Parameters("@i_Action").Value = st_PayInfo.iAction

            If (st_PayInfo.szRespCode <> "") Then
                objCommand.Parameters("@sz_BankRespCode").Value = st_PayInfo.szRespCode
            Else
                objCommand.Parameters("@sz_BankRespCode").Value = st_PayInfo.szHostTxnStatus
            End If

            If (st_PayInfo.szHostTxnID <> "") Then
                objCommand.Parameters("@sz_BankRefNo").Value = st_PayInfo.szHostTxnID
            Else
                objCommand.Parameters("@sz_BankRefNo").Value = st_PayInfo.szAuthCode
            End If

            objCommand.Parameters("@sz_OSPymtCode").Value = st_HostInfo.szOSPymtCode
            objCommand.Parameters("@i_OSRet").Value = st_PayInfo.iOSRet
            objCommand.Parameters("@i_ErrSet").Value = st_PayInfo.iErrSet
            objCommand.Parameters("@i_ErrNum").Value = st_PayInfo.iErrNum
            objCommand.Parameters("@sz_GatewayTxnID").Value = st_PayInfo.szTxnID

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()
            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True
            Else
                bReturn = False

                st_PayInfo.szErrDesc = "bUpdateTxnStatus() error: Failed to update TxnState(" + st_PayInfo.iTxnState.ToString() + "), TxnStatus(" + st_PayInfo.iTxnStatus.ToString() + "), MerchantTxnStatus(" + st_PayInfo.iMerchantTxnStatus.ToString() + "), Action(" + st_PayInfo.iAction.ToString() + "), BankRefNo(" + st_PayInfo.szHostTxnID + "), OSPymtCode(" + st_HostInfo.szOSPymtCode + "), OSRet(" + st_PayInfo.iOSRet.ToString() + "), ErrSet(" + st_PayInfo.iErrSet.ToString() + "), ErrNum(" + st_PayInfo.iErrNum.ToString() + ") for GatewayTxnID(" + st_PayInfo.szTxnID + "), could be already finalized"

                'Commented  28 Apr 2015, for GP
                'Do not overwrite merhant txn status, could be already finalized in resp table due to same milliseconds of redirect and s2s bank response
                'st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                'st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                'st_PayInfo.iTxnState = Common.TXN_STATE.FAILED
                'st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bUpdateTxnStatus() exception: " + ex.ToString + " > " + "TxnState(" + st_PayInfo.iTxnState.ToString() + "), TxnStatus(" + st_PayInfo.iTxnStatus.ToString() + "), MerchantTxnStatus(" + st_PayInfo.iMerchantTxnStatus.ToString() + "), Action(" + st_PayInfo.iAction.ToString() + "), BankRefNo(" + st_PayInfo.szHostTxnID + "), OSPymtCode(" + st_HostInfo.szOSPymtCode + "), OSRet(" + st_PayInfo.iOSRet.ToString() + "), ErrSet(" + st_PayInfo.iErrSet.ToString() + "), ErrNum(" + st_PayInfo.iErrNum.ToString() + ") for GatewayTxnID(" + st_PayInfo.szTxnID + ")"

            'Commented  28 Apr 2015, for GP
            'Do not overwrite merhant txn status, could be already finalized in resp table due to same milliseconds of redirect and s2s bank response
            'st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            'st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            'st_PayInfo.iTxnState = Common.TXN_STATE.FAILED
            'st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bUpdateTxnStatusRes
    ' Function Type:	Boolean.  True if update successful else false
    ' Parameter:		st_PayInfo, st_HostInfo
    ' Description:		Updates transaction status in Response table
    ' History:			10 Apr 2010
    '********************************************************************************************
    Public Function bUpdateTxnStatusRes(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost) As Boolean

        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRowsAffected As Integer = 0

        Try
            bReturn = False
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spUpdateTxnStatusRes", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@i_MerchantTxnStatus", SqlDbType.Int)     'Added on 19 Jul 2013
            objParam = objCommand.Parameters.Add("@i_TxnStatus", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@sz_RespMesg", SqlDbType.VarChar, 255)    ' Added on 11 Apr 2010
            objParam = objCommand.Parameters.Add("@sz_BankRefNo", SqlDbType.VarChar, 30)    ' Added on 11 Apr 2010
            objParam = objCommand.Parameters.Add("@sz_BankRespCode", SqlDbType.VarChar, 20) ' Added on 11 Apr 2010
            objParam = objCommand.Parameters.Add("@sz_GatewayTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@i_MerchantTxnStatus").Value = st_PayInfo.iTxnStatus 'Added on 19 Jul 2013
            objCommand.Parameters("@i_TxnStatus").Value = st_PayInfo.iTxnStatus
            objCommand.Parameters("@sz_RespMesg").Value = st_PayInfo.szTxnMsg
            If (st_PayInfo.szHostTxnID <> "") Then
                objCommand.Parameters("@sz_BankRefNo").Value = st_PayInfo.szHostTxnID
            Else
                objCommand.Parameters("@sz_BankRefNo").Value = st_PayInfo.szAuthCode
            End If
            If (st_PayInfo.szRespCode <> "") Then
                objCommand.Parameters("@sz_BankRespCode").Value = st_PayInfo.szRespCode
            Else
                objCommand.Parameters("@sz_BankRespCode").Value = st_PayInfo.szHostTxnStatus
            End If
            objCommand.Parameters("@sz_GatewayTxnID").Value = st_PayInfo.szTxnID

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()
            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True
            Else
                bReturn = False

                st_PayInfo.szErrDesc = "bUpdateTxnStatusRes() error: Failed to update TxnStatus(" + st_PayInfo.iTxnStatus.ToString() + ") for GatewayTxnID(" + st_PayInfo.szTxnID + ")"

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.iTxnState = Common.TXN_STATE.FAILED
                st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bUpdateTxnStatusRes() exception: " + ex.ToString + " > " + "TxnStatus(" + st_PayInfo.iTxnStatus.ToString() + ") for GatewayTxnID(" + st_PayInfo.szTxnID + ")"

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.FAILED
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bUpdatePayTxnRef
    ' Function Type:	Boolean.  True if update successful else false
    ' Parameter:		st_PayInfo, st_HostInfo
    ' Description:		Updates TB_PayTxnRef table
    ' History:			19 Dec 2013
    '********************************************************************************************
    Public Function bUpdatePayTxnRef(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost) As Boolean

        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRowsAffected As Integer = 0

        Dim iShopperEmailNotifyStatus As Integer = -1       'Added  28 Nov 2014 - ENS
        Dim iMerchantEmailNotifyStatus As Integer = -1      'Added  28 Nov 2014 - ENS

        Dim bSendFraudEmail As Boolean                      'Added 11 Apr 2019

        Try
            bReturn = False
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spUpdatePayTxnRef", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@i_TxnStatus", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_HostID", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@sz_IssuingBank", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@i_PKID", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@sz_GatewayTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_MerchantTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_PymtMethod", SqlDbType.VarChar, 3)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@i_TxnStatus").Value = st_PayInfo.iTxnStatus
            objCommand.Parameters("@i_HostID").Value = st_HostInfo.iHostID
            objCommand.Parameters("@sz_IssuingBank").Value = st_PayInfo.szIssuingBank
            objCommand.Parameters("@i_PKID").Value = st_PayInfo.iPKID
            objCommand.Parameters("@sz_GatewayTxnID").Value = st_PayInfo.szTxnID
            objCommand.Parameters("@sz_MerchantTxnID").Value = st_PayInfo.szMerchantTxnID
            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@sz_PymtMethod").Value = st_PayInfo.szPymtMethod

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()
            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True
            Else
                bReturn = False

                st_PayInfo.szErrDesc = "bUpdatePayTxnRef() error: TxnStatus(" + st_PayInfo.iTxnStatus.ToString() + ") HostID(" + st_HostInfo.iHostID.ToString() + ") AcqBank(" + st_PayInfo.szIssuingBank + ") TxnPKID(" + st_PayInfo.iPKID.ToString() + ") Method(" + st_PayInfo.szPymtMethod + ") for GatewayTxnID(" + st_PayInfo.szTxnID + ")"

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.iTxnState = Common.TXN_STATE.FAILED
                st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999

                'Added, 27 Sept 2017
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Return False: TxnStatus becomes(" + st_PayInfo.iTxnStatus.ToString() + ") TxnMsg becomes(" + st_PayInfo.szTxnMsg + ") Error(" + st_PayInfo.szErrDesc + ")")
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bUpdatePayTxnRef() exception: " + ex.ToString + " > TxnStatus(" + st_PayInfo.iTxnStatus.ToString() + ") HostID(" + st_HostInfo.iHostID.ToString() + ") AcqBank(" + st_PayInfo.szIssuingBank + ") TxnPKID(" + st_PayInfo.iPKID.ToString() + ") Method(" + st_PayInfo.szPymtMethod + ") for GatewayTxnID(" + st_PayInfo.szTxnID + ")"

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.FAILED
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999

            'Added, 27 Sept 2017
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Exception: TxnStatus becomes(" + st_PayInfo.iTxnStatus.ToString() + ") TxnMsg becomes(" + st_PayInfo.szTxnMsg + ") Error(" + st_PayInfo.szErrDesc + ") Exception(" + ex.ToString() + ")")

        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        'Added  24 Nov 2014, ENS-Email Notification
        If (Common.TXN_STATUS.TXN_SUCCESS = st_PayInfo.iMerchantTxnStatus And st_PayInfo.iPymtNotificationEmail > 0) Then
            bCheckNotifyStatus(st_PayInfo, iShopperEmailNotifyStatus, iMerchantEmailNotifyStatus)

            iSendEmailNotification(st_PayInfo, st_HostInfo, iShopperEmailNotifyStatus, iMerchantEmailNotifyStatus)
        End If

        'Added 11 Apr 2019, Joyce. 
        'To check only success trx and over per trx amount limit only send fraud email applicable for iHitLimitAct = 0.
        If (Common.TXN_STATUS.TXN_SUCCESS = st_PayInfo.iMerchantTxnStatus And ("PAY" = st_PayInfo.szTxnType.ToUpper() Or "CAPTURE" = st_PayInfo.szTxnType.ToUpper()) And st_PayInfo.iHitLimitAct = 0) Then
            If ((0 = st_PayInfo.dPerTxnAmtLimit And True <> bCheckTxnAmountMinMaxLimit(st_PayInfo)) Or (("IDR" <> st_PayInfo.szCurrencyCode And "VND" <> st_PayInfo.szCurrencyCode) And (st_PayInfo.dPerTxnAmtLimit > 0) And (Convert.ToDecimal(st_PayInfo.szTxnAmount) > st_PayInfo.dPerTxnAmtLimit))) Then
                bSendFraudEmail = resp.iSendFraudEmailNotification(st_PayInfo, st_HostInfo)

                If (True <> bSendFraudEmail) Then
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > iSendReply2Merchant > Error: Send Fraud Email Fail")
                Else
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > iSendReply2Merchant > Send Fraud Email Success")
                End If
            End If
        End If
        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		Payment
    ' Function Name:	iSendEmailNotification
    ' Function Type:	NONE
    ' Parameter:        i_ShopperEmailNotifyStatus, i_MerchantEmailNotifyStatus
    ' Description:		Send Email Notification
    ' History:			21 Nov 2014 - ENS
    '********************************************************************************************
    Private Function iSendEmailNotification(ByVal st_PayInfo As Common.STPayInfo, ByVal st_HostInfo As Common.STHost, ByVal i_ShopperEmailNotifyStatus As Integer, ByVal i_MerchantEmailNotifyStatus As Integer) As Integer
        Dim iRet As Integer = -1
        Dim szTo() As String = Nothing          'Added, 24 Jan 2014
        Dim szCc() As String = Nothing          'Added, 24 Jan 2014, not used
        Dim szBcc() As String = Nothing         'Added, 24 Jan 2014, not used
        Dim szToAddress() As String = Nothing   'Added, 15 Jan 2015

        Dim szReceiptEmailBody As String = ""   'Added, 29 Jan 2014

        'Dim PGMailer As New MailService.Mailer                'Added, 24 Jan 2014
        'Dim PGMailerStatus As New MailService.MailerStatus    'Added, 24 Jan 2014
        Dim objResp As New Response(objLoggerII)
        'Dim objeMail As New MailService.eMail
        Dim objMail As New Mail(objLoggerII)
        Dim iMailStatus As Integer = -1
        Dim szStatusMsg As String = ""

        Try
            ReDim szCc(0)                           'Added, 10 Feb 2014
            ReDim szBcc(0)                          'Added, 10 Feb 2014
            ReDim szTo(0)                           'Added, 15 Jan 2015

            szCc(0) = ""                            'Added, 10 Feb 2014
            szBcc(0) = ""                           'Added, 10 Feb 2014

            'Modified 18 Feb 2014, combined checking of payment notification option, set szTo(0) later
            'Commented  14 Jan 2015
            'If (1 = st_PayInfo.iPymtNotificationEmail Or 2 = st_PayInfo.iPymtNotificationEmail Or 3 = st_PayInfo.iPymtNotificationEmail) Then
            '    ReDim szTo(0)
            '    szTo(0) = ""
            'End If

            If (Not szTo Is Nothing) Then
                'IPGMailer.Url = ConfigurationManager.AppSettings("EmailServer")    'Added, 7 Feb 2014
                'IPGMailer.Timeout = 60000 'Milliseconds, 60s

                'Added, 17 Feb 2014, differentiate shopper and merchant's payment notification
                '1 - Customer only; 2 - Merchant only; 3 - Customer & Merchant
                'Customer Email Notification
                If ((1 = st_PayInfo.iPymtNotificationEmail Or 3 = st_PayInfo.iPymtNotificationEmail) And i_ShopperEmailNotifyStatus <> 1) Then
                    szTo(0) = st_PayInfo.szCustEmail

                    If (objResp.iLoadReceiptEmailUI_Shopper(szReceiptEmailBody, st_PayInfo, st_HostInfo) <> 0) Then
                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                        st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Failed to load shopper receipt email body for (" + st_PayInfo.iPymtNotificationEmail.ToString() + ":" + szTo(0) + ")")
                    Else
                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                        st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Loaded shopper receipt email body for (" + st_PayInfo.iPymtNotificationEmail.ToString() + ":" + szTo(0) + ")")
                    End If

                    'objeMail.To = szTo(0)
                    'objeMail.Cc = szCc(0)
                    'objeMail.Bcc = szBcc(0)
                    'objeMail.Subject = ConfigurationManager.AppSettings("EmailSubject")
                    'objeMail.FromName = ConfigurationManager.AppSettings("EmailFromName")
                    'objeMail.Body = szReceiptEmailBody
                    iMailStatus = objMail.SendMail(szTo(0), szReceiptEmailBody)
                    szStatusMsg = objMail.StatusMessage(iMailStatus)

                    st_PayInfo.iOSRet = iMailStatus
                    st_PayInfo.szErrDesc = szStatusMsg

                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                        st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Shopper email notification [" + szTo(0) + "] [" + iMailStatus.ToString() + ":" + szStatusMsg + "]")

                    i_ShopperEmailNotifyStatus = iMailStatus
                    'PGMailerStatus = PGMailer.SendMail(objeMail)
                    'PGMailerStatus = PGMailer.SendMail(ConfigurationManager.AppSettings("EmailFromAddr"), ConfigurationManager.AppSettings("EmailFromName"), szTo, szCc, szBcc, ConfigurationManager.AppSettings("EmailSubject"), True, szReceiptEmailBody)
                    'If (Not PGMailerStatus Is Nothing) Then
                    '    st_PayInfo.iOSRet = iMailStatus 'PGMailerStatus.StatusCode
                    '    st_PayInfo.szErrDesc = 'PGMailerStatus.StatusMessage

                    '    'Added, 11 Feb 2014
                    '    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                    '    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                    '    st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Shopper email notification [" + szTo(0) + "] [" + PGMailerStatus.StatusCode.ToString() + ":" + PGMailerStatus.StatusMessage + "]")

                    '    i_ShopperEmailNotifyStatus = PGMailerStatus.StatusCode  'Added  21 Nov 2014 - ENS
                    'End If
                End If

                'Merchant Email Notification
                If ((2 = st_PayInfo.iPymtNotificationEmail Or 3 = st_PayInfo.iPymtNotificationEmail) And i_MerchantEmailNotifyStatus <> 1) Then
                    'szTo(0) = st_PayInfo.szMerchantNotifyEmailAddr  'Commented  14 Jan 2015
                    szToAddress = Split(st_PayInfo.szMerchantNotifyEmailAddr, ",")  'Modified 14 Jan 2015, support multiple merchant payment notification email addresses

                    If (objResp.iLoadReceiptEmailUI_Merchant(szReceiptEmailBody, st_PayInfo, st_HostInfo) <> 0) Then
                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                        st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Failed to load merchant receipt email body for (" + st_PayInfo.iPymtNotificationEmail.ToString() + ":" + st_PayInfo.szMerchantNotifyEmailAddr + ")")
                    Else
                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                        st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Loaded merchant receipt email body for (" + st_PayInfo.iPymtNotificationEmail.ToString() + ":" + st_PayInfo.szMerchantNotifyEmailAddr + ")")
                    End If

                    'Added, 15 Jan 2015, for loop
                    For iCount = 0 To szToAddress.GetUpperBound(0)

                        iMailStatus = objMail.SendMail(szToAddress(iCount), szReceiptEmailBody)
                        szStatusMsg = objMail.StatusMessage(iMailStatus)

                        st_PayInfo.iOSRet = iMailStatus
                        st_PayInfo.szErrDesc = szStatusMsg

                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Merchant email notification " + CStr(iCount + 1) + " [" + szToAddress(iCount) + "] [" + iMailStatus.ToString() + ":" + szStatusMsg + "]")

                        i_MerchantEmailNotifyStatus = iMailStatus

                        ''''Modified 15 Jan 2015, added szToAddress
                        ''''szTo(0) = szToAddress(iCount)
                        ''objeMail.To = szToAddress(iCount)
                        ''objeMail.Cc = szCc(0)
                        ''objeMail.Bcc = szBcc(0)
                        ''objeMail.Subject = ConfigurationManager.AppSettings("EmailSubject")
                        ''objeMail.FromName = ConfigurationManager.AppSettings("EmailFromName")
                        ''objeMail.Body = szReceiptEmailBody

                        ''PGMailerStatus = PGMailer.SendMail(objeMail)
                        ''''PGMailerStatus = PGMailer.SendMail(ConfigurationManager.AppSettings("EmailFromAddr"), ConfigurationManager.AppSettings("EmailFromName"), szTo, szCc, szBcc, ConfigurationManager.AppSettings("EmailSubject"), True, szReceiptEmailBody)
                        ''If (Not PGMailerStatus Is Nothing) Then
                        ''    'Added, 11 Feb 2014
                        ''    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                        ''    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                        ''    st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > Merchant email notification " + CStr(iCount + 1) + " [" + szTo(0) + "] [" + PGMailerStatus.StatusCode.ToString() + ":" + PGMailerStatus.StatusMessage + "]")

                        ''    i_MerchantEmailNotifyStatus = PGMailerStatus.StatusCode  'Added  21 Nov 2014 - ENS
                        ''End If
                    Next
                End If

                'Added  21 Nov 2014. - ENS
                bInsertNotifyStatus(st_PayInfo, i_ShopperEmailNotifyStatus, i_MerchantEmailNotifyStatus)
            End If

        Catch ex As Exception
            'iRet = -1
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            st_PayInfo.szMerchantID + st_PayInfo.szMerchantTxnID + " > iSendEmailNotification() Exception: " + ex.Message)
        Finally
            If Not objResp Is Nothing Then
                objResp.Dispose()
                objResp = Nothing
            End If

            If Not objMail Is Nothing Then
                objMail = Nothing
            End If
        End Try

        Return iRet
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bUpdateTxnStatusEx
    ' Function Type:	Boolean.  True if update successful else false
    ' Parameter:		st_PayInfo
    ' Description:		Updates transaction status in either Request/Response table
    ' History:			4 Sept 2013
    '********************************************************************************************
    Public Function bUpdateTxnStatusEx(ByRef st_PayInfo As Common.STPayInfo) As Boolean

        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRowsAffected As Integer = 0

        Try
            bReturn = False

            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spUpdateTxnStatusEx", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@i_TxnStatus", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_TxnState", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_ReqOrRes", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@sz_RespMesg", SqlDbType.VarChar, 255)
            objParam = objCommand.Parameters.Add("@i_PKID", SqlDbType.BigInt)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@i_TxnStatus").Value = st_PayInfo.iTxnStatus
            objCommand.Parameters("@i_TxnState").Value = st_PayInfo.iTxnState
            objCommand.Parameters("@i_ReqOrRes").Value = st_PayInfo.iQueryStatus
            objCommand.Parameters("@sz_RespMesg").Value = st_PayInfo.szTxnMsg
            objCommand.Parameters("@i_PKID").Value = st_PayInfo.iPKID

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()
            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True
            Else
                bReturn = False

                st_PayInfo.szErrDesc = "bUpdateTxnStatusEx() error: Failed to update TxnStatus(" + st_PayInfo.iTxnStatus.ToString() + ") TxnState(" + st_PayInfo.iTxnState.ToString() + ") ReqOrRes(" + st_PayInfo.iQueryStatus.ToString() + ") PKID(" + st_PayInfo.iPKID.ToString() + ") GatewayTxnID(" + st_PayInfo.szTxnID + ")"

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_INT_ERR
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
                st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bUpdateTxnStatusEx() exception: " + ex.ToString + " > " + "TxnStatus(" + st_PayInfo.iTxnStatus.ToString() + ") TxnState(" + st_PayInfo.iTxnState.ToString() + ") ReqOrRes(" + st_PayInfo.iQueryStatus.ToString() + ") PKID (" + st_PayInfo.iPKID.ToString() + ") GatewayTxnID(" + st_PayInfo.szTxnID + ")"

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_INT_ERR
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bUpdateOriTxnStatus
    ' Function Type:	Boolean.  True if update successful else false
    ' Parameter:		st_PayInfo
    ' Description:		Updates reversal pending transaction status to original status in either Request/Response table
    ' History:			10 Sept 2013
    '********************************************************************************************
    Public Function bUpdateOriTxnStatus(ByRef st_PayInfo As Common.STPayInfo) As Boolean

        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRowsAffected As Integer = 0

        Try
            bReturn = False

            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spUpdateOriTxnStatus", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@i_ReqOrRes", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_PKID", SqlDbType.BigInt)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@i_ReqOrRes").Value = st_PayInfo.iQueryStatus
            objCommand.Parameters("@i_PKID").Value = st_PayInfo.iPKID

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()
            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True
            Else
                bReturn = False

                st_PayInfo.szErrDesc = "bUpdateOriTxnStatus() error: Failed to update back to original status," + " ReqOrRes(" + st_PayInfo.iQueryStatus.ToString() + ") PKID(" + st_PayInfo.iPKID.ToString() + ") GatewayTxnID(" + st_PayInfo.szTxnID + ")"

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_INT_ERR
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
                st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bUpdateOriTxnStatus() exception: " + ex.ToString + " > " + " ReqOrRes(" + st_PayInfo.iQueryStatus.ToString() + ") PKID (" + st_PayInfo.iPKID.ToString() + ") GatewayTxnID(" + st_PayInfo.szTxnID + ")"

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_INT_ERR
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bUpdateTxnParams
    ' Function Type:	Boolean.  True if update successful else false
    ' Parameter:		st_PayInfo
    ' Description:		Updates transaction details into database
    ' History:			18 Jan 2009
    '********************************************************************************************
    Public Function bUpdateTxnParams(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost) As Boolean

        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRet As Integer = 0

        Try
            bReturn = False
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spUpdateTxnParams", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.Char, 30)
            objParam = objCommand.Parameters.Add("@sz_MerchantTxnID", SqlDbType.Char, 30)
            objParam = objCommand.Parameters.Add("@sz_Param1", SqlDbType.VarChar, 100)
            objParam = objCommand.Parameters.Add("@sz_Param2", SqlDbType.VarChar, 100)
            objParam = objCommand.Parameters.Add("@sz_Param3", SqlDbType.VarChar, 100)
            objParam = objCommand.Parameters.Add("@sz_Param4", SqlDbType.VarChar, 100)
            objParam = objCommand.Parameters.Add("@sz_Param5", SqlDbType.VarChar, 100)
            objParam = objCommand.Parameters.Add("@sz_Param6", SqlDbType.VarChar, 50)
            objParam = objCommand.Parameters.Add("@sz_Param7", SqlDbType.VarChar, 50)
            objParam = objCommand.Parameters.Add("@sz_Param8", SqlDbType.VarChar, 50)
            objParam = objCommand.Parameters.Add("@sz_Param9", SqlDbType.VarChar, 50)
            objParam = objCommand.Parameters.Add("@sz_Param10", SqlDbType.VarChar, 50)
            objParam = objCommand.Parameters.Add("@sz_Param11", SqlDbType.VarChar, 255)
            objParam = objCommand.Parameters.Add("@sz_Param12", SqlDbType.VarChar, 255)
            objParam = objCommand.Parameters.Add("@sz_Param13", SqlDbType.VarChar, 255)
            objParam = objCommand.Parameters.Add("@sz_Param14", SqlDbType.VarChar, 255)
            objParam = objCommand.Parameters.Add("@sz_Param15", SqlDbType.VarChar, 255)
            objParam = objCommand.Parameters.Add("@i_TxnState", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_ReqRes", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@sz_MerchantTxnID").Value = st_PayInfo.szMerchantTxnID
            objCommand.Parameters("@sz_Param1").Value = st_PayInfo.szParam1.Trim()
            If (st_PayInfo.szParam2 <> "") Then
                Dim szTempValue As String = ""
                objCommand.Parameters("@sz_Param2").Value = objHash.szEncryptAES(st_PayInfo.szParam2, Common.C_3DESAPPKEY)
            Else
                objCommand.Parameters("@sz_Param2").Value = st_PayInfo.szParam2
            End If

            If (st_PayInfo.szParam3 <> "") Then
                objCommand.Parameters("@sz_Param3").Value = objHash.szEncryptAES(st_PayInfo.szParam3, Common.C_3DESAPPKEY)
            Else
                objCommand.Parameters("@sz_Param3").Value = st_PayInfo.szParam3
            End If

            objCommand.Parameters("@sz_Param4").Value = st_PayInfo.szParam4.Trim()
            objCommand.Parameters("@sz_Param5").Value = st_PayInfo.szParam5.Trim()
            objCommand.Parameters("@sz_Param6").Value = st_PayInfo.szParam6.Trim()
            objCommand.Parameters("@sz_Param7").Value = st_PayInfo.szParam7.Trim()
            objCommand.Parameters("@sz_Param8").Value = st_PayInfo.szParam8.Trim()
            objCommand.Parameters("@sz_Param9").Value = st_PayInfo.szParam9.Trim()
            objCommand.Parameters("@sz_Param10").Value = st_PayInfo.szParam10.Trim()
            objCommand.Parameters("@sz_Param11").Value = st_PayInfo.szParam11.Trim()
            objCommand.Parameters("@sz_Param12").Value = st_PayInfo.szParam12.Trim()
            objCommand.Parameters("@sz_Param13").Value = st_PayInfo.szParam13.Trim()
            objCommand.Parameters("@sz_Param14").Value = st_PayInfo.szParam14.Trim()
            objCommand.Parameters("@sz_Param15").Value = st_PayInfo.szParam15.Trim()
            objCommand.Parameters("@i_TxnState").Value = st_PayInfo.iTxnState
            objCommand.Parameters("@i_ReqRes").Value = st_PayInfo.iReqRes

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            iRet = -1
            If dr.Read() Then
                iRet = Convert.ToInt32(dr("RetCode"))
                st_PayInfo.szTxnMsg = dr("RetDesc").ToString()
                st_PayInfo.szIssuingBank = dr("IssuingBank").ToString()
            End If

            If iRet = 0 Then
                bReturn = True
            Else
                bReturn = False

                st_PayInfo.szErrDesc = "bUpdateTxnParams() error: Failed to update Param1(" + st_PayInfo.szParam1 + "), Param2(" + st_PayInfo.szMaskedParam2 + "), Param3(" + st_PayInfo.szMaskedParam3 + "), Param4(" + st_PayInfo.szParam4 + "), Param5(" + st_PayInfo.szParam5 _
                                        + "), Param6(" + st_PayInfo.szParam6 + "), Param7(" + st_PayInfo.szParam7 + "), Param8(" + st_PayInfo.szParam8 + "), Param9(" + st_PayInfo.szParam9 + "), Param10(" + st_PayInfo.szParam10 + "), Param11(" + st_PayInfo.szParam11 _
                                        + "), Param12(" + st_PayInfo.szParam12 + "), Param13(" + st_PayInfo.szParam13 + "), Param14(" + st_PayInfo.szParam14 + "), Param15(" + st_PayInfo.szParam15 + ") due to " + st_PayInfo.szTxnMsg

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.iTxnState = Common.TXN_STATE.FAILED
                st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bUpdateTxnParams() exception: " + ex.ToString + " > " + "Param1(" + st_PayInfo.szParam1 + "), Param2(" + st_PayInfo.szMaskedParam2 + "), Param3(" + st_PayInfo.szMaskedParam3 + "), Param4(" + st_PayInfo.szParam4 + "), Param5(" + st_PayInfo.szParam5 _
                                    + "), Param6(" + st_PayInfo.szParam6 + "), Param7(" + st_PayInfo.szParam7 + "), Param8(" + st_PayInfo.szParam8 + "), Param9(" + st_PayInfo.szParam9 + "), Param10(" + st_PayInfo.szParam10 + +"), Param11(" + st_PayInfo.szParam11 _
                                    + "), Param12(" + st_PayInfo.szParam12 + "), Param13(" + st_PayInfo.szParam13 + "), Param14(" + st_PayInfo.szParam14 + "), Param15(" + st_PayInfo.szParam15 + ")"

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.FAILED
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bGetReqTxn
    ' Function Type:	Boolean.  True if unique else false
    ' Parameter:		
    ' Description:		Get request data from database
    ' History:			29 Oct 2008
    '********************************************************************************************
    Public Function bGetReqTxn(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim iRowsAffected As Integer = 0
        Dim dr As SqlDataReader = Nothing

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetReqTxn", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_GatewayTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_GatewayTxnID").Value = st_PayInfo.szTxnID

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If dr.Read() Then
                st_PayInfo.iQueryStatus = Convert.ToInt32(dr("RetCode"))    'dr.GetInt32(1).ToString
                st_PayInfo.szTxnMsg = dr("RetDesc").ToString()

                'Assign initial value
                st_PayInfo.szTxnType = ""
                st_PayInfo.szMerchantID = ""
                st_PayInfo.szMerchantTxnID = ""
                st_PayInfo.szTxnAmount = ""
                st_PayInfo.szBaseTxnAmount = ""     ' Added on 3 Aug 2009, base currency booking confirmation enhancement
                st_PayInfo.szCurrencyCode = ""
                st_PayInfo.szBaseCurrencyCode = ""  ' Added on 3 Aug 2009, base currency booking confirmation enhancement
                st_PayInfo.szIssuingBank = ""
                st_PayInfo.szLanguageCode = ""
                st_PayInfo.iTxnStatus = 0
                st_PayInfo.iTxnState = 0
                st_PayInfo.szMerchantOrdID = ""
                st_PayInfo.szMerchantSessionID = "" 'Modified 15 Jul 2014, SessionID to MerchantSessionID
                st_PayInfo.szParam1 = ""
                st_PayInfo.szParam2 = ""
                st_PayInfo.szParam3 = ""
                st_PayInfo.szParam4 = ""
                st_PayInfo.szParam5 = ""
                st_PayInfo.szParam6 = ""
                st_PayInfo.szParam7 = ""
                st_PayInfo.szParam8 = ""
                st_PayInfo.szParam9 = ""
                st_PayInfo.szParam10 = ""
                st_PayInfo.szParam11 = ""
                st_PayInfo.szParam12 = ""
                st_PayInfo.szParam13 = ""
                st_PayInfo.szParam14 = ""
                st_PayInfo.szParam15 = ""
                st_PayInfo.szHashMethod = ""
                st_PayInfo.szMerchantReturnURL = ""
                st_PayInfo.szMerchantApprovalURL = ""       'Added, 30 Jun 2014
                st_PayInfo.szMerchantUnApprovalURL = ""     'Added, 30 Jun 2014
                st_PayInfo.szMerchantCallbackURL = ""       'Added  16 Oct 2014
                st_PayInfo.szMerchantSupportURL = ""
                st_PayInfo.szGatewayID = ""
                st_PayInfo.szMachineID = ""
                st_PayInfo.szCardPAN = ""
                st_PayInfo.szMaskedCardPAN = ""
                st_PayInfo.szTxnDateTime = ""   'Added on 6 Oct 2009
                st_PayInfo.szMerchantOrdDesc = ""     'Added on 12 Oct 2009
                st_HostInfo.iHostID = 0         'Added on 16 June 2009
                st_PayInfo.szCardHolder = ""    'Added, 14 Dec 2013
                st_PayInfo.szCustEmail = ""     'Added, 16 Dec 2013
                st_PayInfo.szCustName = ""      'Added, 18 Feb 2014
                st_PayInfo.szCustOCP = ""       'Added, 12 Aug 2014
                st_PayInfo.szTokenType = ""     'Added, 26 Mar 2015, for BNB
                st_PayInfo.szPromoCode = ""     'Added 30 Nov 2015, Promotion
                st_PayInfo.szPromoOriAmt = ""   'Added 30 Nov 2015, Promotion
                st_PayInfo.szCardType = ""      'Added, 20 May 2016, for MyDin Card Type
                st_PayInfo.iInstallment = 0     'Added 14 Dec 2016, CIMB Installment
                st_PayInfo.szFXCurrencyCode = "" 'Added 19 Oct 2017, Conversion Currency
                st_PayInfo.szFXTxnAmount = ""    'Added 19 Oct 2017, Conversion Currency
                st_PayInfo.szCustPhone = ""     'Added, 12 Feb 2018, Tik FX

                If (1 = st_PayInfo.iQueryStatus) Then
                    st_PayInfo.szTxnType = dr("TxnType").ToString()
                    st_PayInfo.szMerchantID = dr("MerchantID").ToString()
                    st_PayInfo.szMerchantTxnID = dr("MerchantTxnID").ToString()
                    st_PayInfo.szTxnAmount = dr("TxnAmount").ToString()
                    st_PayInfo.szBaseTxnAmount = dr("BaseTxnAmount").ToString()                 ' Added on 3 Aug 2009, base currency booking confirmation enhancement
                    st_PayInfo.szCurrencyCode = dr("CurrencyCode").ToString()
                    st_PayInfo.szBaseCurrencyCode = Trim(dr("BaseCurrencyCode").ToString())     ' Added on 3 Aug 2009, base currency booking confirmation enhancement
                    st_PayInfo.szIssuingBank = dr("IssuingBank").ToString()
                    st_HostInfo.iHostID = Convert.ToInt32(dr("HostID")) 'Added on 16 June 2009
                    st_PayInfo.szLanguageCode = dr("LanguageCode").ToString()
                    st_PayInfo.iTxnStatus = Convert.ToInt32(dr("TxnStatus"))
                    st_PayInfo.iTxnState = Convert.ToInt32(dr("TxnState"))
                    st_PayInfo.szMerchantOrdID = dr("OrderNumber").ToString()
                    st_PayInfo.szMerchantSessionID = dr("SessionID").ToString()                 'Modified 15 Jul 2014, SessionID to MerchantSessionID
                    st_PayInfo.szParam1 = dr("Param1").ToString()
                    st_PayInfo.szCardHolder = dr("CardHolder").ToString()                       'Added, 14 Dec 2013
                    st_PayInfo.szCustEmail = dr("CustEmail").ToString()                         'Added, 16 Dec 2013
                    st_PayInfo.szCustName = dr("CustName").ToString()                           'Added, 18 Feb 2014

                    If (dr("Param2").ToString() <> "") Then
                        st_PayInfo.szParam2 = objHash.szDecryptAES(dr("Param2").ToString(), Common.C_3DESAPPKEY)
                        st_PayInfo.szClearCardPAN = st_PayInfo.szParam2 'Added, 17 May 2016, for OCP token creation
                    Else
                        st_PayInfo.szParam2 = dr("Param2").ToString()
                    End If

                    If (dr("Param3").ToString() <> "") Then
                        st_PayInfo.szParam3 = objHash.szDecryptAES(dr("Param3").ToString(), Common.C_3DESAPPKEY)
                        st_PayInfo.szCardExp = st_PayInfo.szParam3      'Added, 9 Oct 2015, for MyDin
                    Else
                        st_PayInfo.szParam3 = dr("Param3").ToString()
                    End If

                    st_PayInfo.szParam4 = dr("Param4").ToString()
                    st_PayInfo.szParam5 = dr("Param5").ToString()
                    st_PayInfo.szParam6 = dr("Param6").ToString()
                    st_PayInfo.szParam7 = dr("Param7").ToString()
                    st_PayInfo.szParam8 = dr("Param8").ToString()
                    st_PayInfo.szParam9 = dr("Param9").ToString()
                    st_PayInfo.szParam10 = dr("Param10").ToString()
                    If ("" <> st_PayInfo.szParam10) Then
                        st_PayInfo.iInstallment = Convert.ToInt16(st_PayInfo.szParam10)     'Added 14 Dec 2016, CIMB Installment
                    End If
                    st_PayInfo.szParam11 = dr("Param11").ToString()
                    st_PayInfo.szParam12 = dr("Param12").ToString()
                    st_PayInfo.szParam13 = dr("Param13").ToString()
                    st_PayInfo.szParam14 = dr("Param14").ToString()
                    st_PayInfo.szParam15 = dr("Param15").ToString()
                    st_PayInfo.szHashMethod = dr("HashMethod").ToString()
                    st_PayInfo.szMerchantReturnURL = dr("MerchantReturnURL").ToString()
                    st_PayInfo.szMerchantApprovalURL = dr("MerchantApprovalURL").ToString()     'Added, 30 Jun 2014
                    st_PayInfo.szMerchantUnApprovalURL = dr("MerchantUnApprovalURL").ToString() 'Added, 30 Jun 2014
                    st_PayInfo.szMerchantCallbackURL = dr("MerchantCallbackURL").ToString()     'Added  16 Oct 2014
                    st_PayInfo.szMerchantSupportURL = dr("MerchantSupportURL").ToString()
                    st_PayInfo.szGatewayID = dr("GatewayID").ToString()
                    st_PayInfo.szMachineID = dr("MachineID").ToString()
                    st_PayInfo.szCardPAN = dr("CardPAN").ToString()
                    st_PayInfo.szMaskedCardPAN = dr("MaskedCardPAN").ToString()
                    st_PayInfo.szTxnDateTime = dr("DateCreated").ToString() 'Added on 6 Oct 2009
                    st_PayInfo.szMerchantOrdDesc = dr("OrderDesc").ToString()     'Added on 12 Oct 2009
                    st_PayInfo.szCustOCP = dr("CustOCP").ToString()         'Added, 12 Aug 2014
                    st_PayInfo.szTokenType = dr("TokenType").ToString()     'Added, 26 Mar 2015, for BNB
                    st_PayInfo.szPromoCode = dr("PromoCode").ToString()     'Added 30 Nov 2015, Promotion
                    st_PayInfo.szPromoOriAmt = dr("PromoOriAmt").ToString() 'Added 30 Nov 2015, Promotion
                    st_PayInfo.szCardType = dr("CardType").ToString()       'Added, 20 May 2016, for MyDin Card Type
                    st_PayInfo.szFXCurrencyCode = Trim(dr("FXCurrencyCode").ToString())     'Added 19 Oct 2017, Conversion Currency
                    st_PayInfo.szFXTxnAmount = dr("FXTxnAmt").ToString()                    'Added 19 Oct 2017, Conversion Currency
                    st_PayInfo.szCustPhone = dr("CustPhone").ToString()     'Added, 12 Feb 2018, Tik FX

                    ' Moved from outside of End If into here, 1 Mar 2009, should not be considered true if response
                    ' already existed in Response table OR no matching request in Request table, such response should be
                    ' treated as invalid response posted by hackers/malicious acts/host errors.
                    bReturn = True
                Else
                    bReturn = False
                    st_PayInfo.szErrDesc = "bGetReqTxn() error: " + st_PayInfo.szTxnMsg + " > GatewayTxnID(" + st_PayInfo.szTxnID + ")"

                    st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                    st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
                End If
                ' bReturn = True    ' Commented on 1 Mar 2009
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetReqTxn() exception: " + ex.StackTrace + ex.Message + " > GatewayTxnID(" + st_PayInfo.szTxnID + ")"

            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bGetTxnParam
    ' Function Type:	Boolean.  True if unique else false
    ' Parameter:		
    ' Description:		Get Param from database
    ' History:			16 Apr 2014
    '********************************************************************************************
    Public Function bGetTxnParam(ByRef st_PayInfo As Common.STPayInfo) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim iQueryStatus As Integer = 0
        Dim iRowsAffected As Integer = 0
        Dim dr As SqlDataReader = Nothing

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetTxnParam", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_MerchantTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@i_ReqRes", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@sz_MerchantTxnID").Value = st_PayInfo.szMerchantTxnID
            objCommand.Parameters("@i_ReqRes").Value = 1

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If dr.Read() Then
                'Added  27 May 2016. If it is Installment then should not overwrite TxnMsg as 'Transaction Exist"
                'Modified  15 Nov 2017. VCO Txn need to avoid TxnMsg to be overwrite
                If (1 <> st_PayInfo.iAllowInstallment And "V" <> st_PayInfo.szGatewayID) Then
                    st_PayInfo.szTxnMsg = dr("RetDesc").ToString()
                End If

                'Assign initial value
                st_PayInfo.szParam1 = """ > MerchantPymtID("
                st_PayInfo.szParam10 = ""   'Added  26 May 2016. Installment

                If (1 = Convert.ToInt32(dr("RetCode"))) Then
                    st_PayInfo.szParam1 = dr("Param1").ToString()
                    st_PayInfo.szParam10 = dr("Param10").ToString() 'Added  26 May 2016. Installment

                    bReturn = True
                Else
                    bReturn = False

                    st_PayInfo.szErrDesc = "bGetTxnParam() error: " + st_PayInfo.szTxnMsg + " > MerchantPymtID(" + st_PayInfo.szMerchantTxnID + ")"

                    st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                    st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
                End If
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetTxnParam() exception: " + ex.StackTrace + ex.Message + " > MerchantPymtID(" + st_PayInfo.szMerchantTxnID + ")"

            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bGetReqDetail
    ' Function Type:	Boolean.  True if unique else false
    ' Parameter:		
    ' Description:		Get request data from database
    ' History:			28 Sept 2011
    '********************************************************************************************
    Public Function bGetReqDetail(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim iRowsAffected As Integer = 0
        Dim dr As SqlDataReader = Nothing

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetReqDetail", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_MerchantTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@sz_MerchantTxnID").Value = st_PayInfo.szMerchantTxnID

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If dr.Read() Then
                st_PayInfo.iQueryStatus = Convert.ToInt32(dr("RetCode"))    'dr.GetInt32(1).ToString
                st_PayInfo.szTxnMsg = dr("RetDesc").ToString()

                'Assign initial value
                st_PayInfo.szBaseTxnAmount = ""     ' Added on 3 Aug 2009, base currency booking confirmation enhancement
                st_PayInfo.szBaseCurrencyCode = ""  ' Added on 3 Aug 2009, base currency booking confirmation enhancement
                st_PayInfo.szMerchantOrdID = ""
                st_PayInfo.szMerchantSessionID = "" 'Modified 15 Jul 2014, SessionID to MerchantSessionID
                st_PayInfo.szHashMethod = ""
                st_PayInfo.szMerchantReturnURL = ""
                st_PayInfo.szMerchantSupportURL = ""
                st_PayInfo.szMaskedCardPAN = ""
                st_PayInfo.szMerchantOrdDesc = ""
                st_PayInfo.szIssuingBank = ""

                If (1 = st_PayInfo.iQueryStatus) Then
                    st_PayInfo.szBaseTxnAmount = dr("BaseTxnAmount").ToString()
                    st_PayInfo.szBaseCurrencyCode = Trim(dr("BaseCurrencyCode").ToString())
                    st_PayInfo.szMerchantOrdID = dr("OrderNumber").ToString()
                    st_PayInfo.szMerchantSessionID = dr("SessionID").ToString() 'Modified 15 Jul 2014, SessionID to MerchantSessionID
                    st_PayInfo.szHashMethod = dr("HashMethod").ToString()
                    st_PayInfo.szMerchantReturnURL = dr("MerchantReturnURL").ToString()
                    st_PayInfo.szMerchantSupportURL = dr("MerchantSupportURL").ToString()
                    st_PayInfo.szMaskedCardPAN = dr("MaskedCardPAN").ToString()
                    st_PayInfo.szMerchantOrdDesc = dr("OrderDesc").ToString()
                    st_PayInfo.szIssuingBank = dr("IssuingBank").ToString()

                    ' Moved from outside of End If into here, 1 Mar 2009, should not be considered true if response
                    ' already existed in Response table OR no matching request in Request table, such response should be
                    ' treated as invalid response posted by hackers/malicious acts/host errors.
                    bReturn = True
                Else
                    bReturn = False
                    st_PayInfo.szErrDesc = "bGetReqDetail() error: " + st_PayInfo.szTxnMsg + " > GatewayTxnID(" + st_PayInfo.szTxnID + ")"

                    st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                    st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
                End If
                ' bReturn = True    ' Commented on 1 Mar 2009
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetReqDetail() exception: " + ex.StackTrace + ex.Message + " > GatewayTxnID(" + st_PayInfo.szTxnID + ")"

            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bGetResTxn
    ' Function Type:	Boolean.  True if successfully retrieved record or else false
    ' Parameter:		
    ' Description:		Get transaction result from response table based on GatewayTxnID
    ' History:			28 Mar 2010
    '********************************************************************************************
    Public Function bGetResTxn(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim iRowsAffected As Integer = 0
        Dim dr As SqlDataReader = Nothing
        Dim szHashMethod As String = ""

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetResTxn", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_GatewayTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue
            ' add host id for search
            objCommand.Parameters("@sz_GatewayTxnID").Value = st_PayInfo.szTxnID

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If dr.Read() Then

                st_PayInfo.iQueryStatus = Convert.ToInt32(dr("RetCode"))
                st_PayInfo.szQueryMsg = dr("RetDesc").ToString()

                'This will overwrite Get Req data - CCGW
                ''Assign initial value
                'st_PayInfo.szTxnType = ""
                'st_PayInfo.szMerchantID = ""
                'st_PayInfo.szMerchantTxnID = ""
                'st_PayInfo.szMerchantName = ""
                'st_PayInfo.szMerchantOrdID = ""
                'st_PayInfo.szTxnAmount = ""
                'st_PayInfo.szBaseTxnAmount = ""
                'st_PayInfo.szCurrencyCode = ""
                'st_PayInfo.szBaseCurrencyCode = ""
                'st_PayInfo.szLanguageCode = ""
                'st_PayInfo.szIssuingBank = ""
                'st_PayInfo.szSessionID = ""
                'st_PayInfo.szParam1 = ""
                'st_PayInfo.szParam2 = ""
                'st_PayInfo.szParam3 = ""
                'st_PayInfo.szParam4 = ""
                'st_PayInfo.szParam5 = ""
                'st_PayInfo.szTxnID = ""
                'st_PayInfo.iTxnState = -1
                'st_PayInfo.iTxnStatus = -1
                'st_PayInfo.iMerchantTxnStatus = -1
                'st_PayInfo.iAction = -1
                'st_PayInfo.iDuration = -1
                'st_PayInfo.iOSRet = 99
                'st_PayInfo.iErrSet = -1
                'st_PayInfo.iErrNum = -1
                'st_PayInfo.szHostTxnID = ""
                'st_PayInfo.szTxnMsg = ""
                'st_PayInfo.szHashMethod = ""
                'st_PayInfo.szHashValue = ""
                'st_PayInfo.szGatewayID = ""
                'st_PayInfo.szCardPAN = ""
                'st_PayInfo.szMachineID = ""
                'st_PayInfo.szMerchantReturnURL = ""
                'st_PayInfo.szMerchantSupportURL = ""
                'st_PayInfo.szTxnDateTime = ""
                'st_PayInfo.szMerchantOrdDesc = ""
                'st_PayInfo.szECI = ""
                'st_PayInfo.szPayerAuth = ""
                'st_HostInfo.szOSPymtCode = ""
                'st_HostInfo.iHostID = -1

                ' 1 - Transaction exists in Response table, has reached its final state
                If (1 = st_PayInfo.iQueryStatus) Then

                    st_PayInfo.iQueryStatus = -1

                    st_PayInfo.szTxnType = dr("TxnType").ToString()
                    st_PayInfo.szMerchantID = dr("MerchantID").ToString()
                    st_PayInfo.szMerchantTxnID = dr("MerchantTxnID").ToString()
                    st_PayInfo.szMerchantName = dr("MerchantName").ToString()   'Added on 3 Apr 2010
                    st_PayInfo.szMerchantOrdID = dr("OrderNumber").ToString()
                    st_PayInfo.szTxnAmount = dr("TxnAmount").ToString()
                    st_PayInfo.szBaseTxnAmount = dr("BaseTxnAmount").ToString()
                    st_PayInfo.szCurrencyCode = dr("CurrencyCode").ToString()
                    st_PayInfo.szBaseCurrencyCode = Trim(dr("BaseCurrencyCode").ToString())
                    st_PayInfo.szLanguageCode = dr("LanguageCode").ToString()
                    st_PayInfo.szIssuingBank = dr("IssuingBank").ToString()
                    st_PayInfo.szMerchantSessionID = dr("SessionID").ToString() 'Modified 15 Jul 2014, SessionID to MerchantSessionID
                    st_PayInfo.szParam1 = dr("Param1").ToString()

                    If (dr("Param2").ToString() <> "") Then
                        st_PayInfo.szParam2 = objHash.szDecryptAES(dr("Param2").ToString(), Common.C_3DESAPPKEY)
                        st_PayInfo.szClearCardPAN = st_PayInfo.szParam2 'Added, 17 May 2016, for OCP token creation
                    Else
                        st_PayInfo.szParam2 = dr("Param2").ToString()
                    End If

                    If (dr("Param3").ToString() <> "") Then
                        st_PayInfo.szParam3 = objHash.szDecryptAES(dr("Param3").ToString(), Common.C_3DESAPPKEY)
                    Else
                        st_PayInfo.szParam3 = dr("Param3").ToString()
                    End If

                    'Added, 9 Oct 2015, for MyDin
                    If (dr("CardExp").ToString() <> "") Then
                        st_PayInfo.szCardExp = objHash.szDecryptAES(dr("CardExp").ToString(), Common.C_3DESAPPKEY)
                    End If

                    st_PayInfo.szParam4 = dr("Param4").ToString()
                    st_PayInfo.szParam5 = dr("Param5").ToString()
                    st_PayInfo.szParam6 = dr("Param6").ToString()
                    st_PayInfo.szParam7 = dr("Param7").ToString()
                    st_PayInfo.szParam8 = dr("Param8").ToString()
                    st_PayInfo.szParam9 = dr("Param9").ToString()
                    st_PayInfo.szParam10 = dr("Param10").ToString()
                    st_PayInfo.szParam11 = dr("Param11").ToString()
                    st_PayInfo.szParam12 = dr("Param12").ToString()
                    st_PayInfo.szParam13 = dr("Param13").ToString()
                    st_PayInfo.szParam14 = dr("Param14").ToString()
                    st_PayInfo.szParam15 = dr("Param15").ToString()
                    st_PayInfo.szTxnID = dr("GatewayTxnID").ToString()
                    st_PayInfo.szTxnDateTime = dr("DateCreated").ToString()
                    st_PayInfo.szMerchantOrdDesc = dr("OrderDesc").ToString()

                    st_PayInfo.iTxnState = Convert.ToInt32(dr("TxnState"))
                    st_PayInfo.iTxnStatus = Convert.ToInt32(dr("TxnStatus"))
                    st_PayInfo.iMerchantTxnStatus = Convert.ToInt32(dr("MerchantTxnStatus"))
                    st_PayInfo.iAction = Convert.ToInt32(dr("Action"))
                    st_PayInfo.iDuration = Convert.ToInt32(dr("Duration"))
                    st_PayInfo.iOSRet = Convert.ToInt32(dr("OSRet"))
                    st_PayInfo.iErrSet = Convert.ToInt32(dr("ErrSet"))
                    st_PayInfo.iErrNum = Convert.ToInt32(dr("ErrNum"))

                    st_PayInfo.szRespCode = dr("BankRespCode").ToString()
                    st_PayInfo.szHostTxnID = dr("BankRefNo").ToString()
                    st_PayInfo.szTxnMsg = dr("RespMesg").ToString()
                    st_PayInfo.szHashMethod = dr("HashMethod").ToString()
                    st_PayInfo.szHashValue = dr("HashValue").ToString()
                    st_PayInfo.szGatewayID = dr("GatewayID").ToString()
                    st_PayInfo.szCardPAN = dr("CardPAN").ToString()             'Masked CardPAN
                    st_PayInfo.szMaskedCardPAN = dr("CardPAN").ToString()       'Added, 9 Oct 2015, for MyDin, "CardPAN" is actually masked CardPAN

                    'If st_PayInfo.szCardPAN.Length > 10 Then
                    '    st_PayInfo.szMaskedCardPAN = st_PayInfo.szCardPAN.Substring(0, 6) + "".PadRight(st_PayInfo.szCardPAN.Length - 6 - 4, "X") + st_PayInfo.szCardPAN.Substring(st_PayInfo.szCardPAN.Length - 4, 4)
                    'Else
                    '    st_PayInfo.szMaskedCardPAN = "".PadRight(st_PayInfo.szCardPAN.Length, "X")
                    'End If
                    st_PayInfo.szMachineID = dr("MachineID").ToString()
                    st_PayInfo.szMerchantReturnURL = dr("MerchantReturnURL").ToString()
                    st_PayInfo.szMerchantSupportURL = dr("MerchantSupportURL").ToString()
                    st_PayInfo.szMerchantApprovalURL = dr("MerchantApprovalURL").ToString()     'Added, 8 Jul 2014
                    st_PayInfo.szMerchantUnApprovalURL = dr("MerchantUnApprovalURL").ToString() 'Added, 8 Jul 2014
                    st_PayInfo.szMerchantCallbackURL = dr("MerchantCallbackURL").ToString()     'Added  16 Oct 2014
                    st_PayInfo.szAuthCode = dr("AuthCode").ToString()   'Added on 7 Oct 2011  for CCGW
                    st_PayInfo.szECI = dr("ECI").ToString()             'Added on 5 Oct 2011  for CCGW
                    st_PayInfo.szPayerAuth = dr("PayerAuth").ToString() 'Added on 5 Oct 2011  for CCGW
                    st_PayInfo.szAVS = dr("AVS").ToString()             'Added on 17 Jul 2013  for PG

                    st_HostInfo.szOSPymtCode = dr("OSPymtCode").ToString()
                    st_HostInfo.iHostID = Convert.ToInt32(dr("HostID"))

                    st_PayInfo.szCardHolder = dr("CardHolder").ToString()       'Added, 19 Dec 2013
                    st_PayInfo.szCustEmail = dr("CustEmail").ToString()         'Added, 19 Dec 2013
                    st_PayInfo.szCustName = dr("CustName").ToString()           'Added, 18 Feb 2014
                    st_PayInfo.szTokenType = dr("TokenType").ToString()         'Added, 26 Mar 2015, for BNB
                    st_PayInfo.szCardType = dr("CardType").ToString()           'Added, 20 May 2016, for MyDin Card Type
                    st_PayInfo.szCustPhone = dr("CustPhone").ToString()         'Added, 12 Feb 2018, Tik FX

                    bReturn = True
                Else
                    bReturn = False
                End If

            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetResTxn() exception: " + ex.StackTrace + ex.Message + " > GatewayTxnID(" + st_PayInfo.szTxnID + ")"

            st_PayInfo.iQueryStatus = 2
            st_PayInfo.szQueryMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bGetMerchantIDbyTxnID
    ' Function Type:	Boolean.  True if get merchant information successfully else false
    ' Parameter:		st_PayInfo, sz_DBConnStr
    ' Description:		get MerchantID from TB_PayReq table
    ' History:			25 May 2012 Add  for integration of Regional DD - UOB Thailand
    '********************************************************************************************
    Public Function bGetMerchantIDbyTxnID(ByRef st_PayInfo As Common.STPayInfo) As Boolean

        Dim bReturn As Boolean = True
        Dim objConn As SqlConnection
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader
        Dim iRowsAffected As Integer = 0

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetMerchantIDbyTxnID", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_GatewayTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_GatewayTxnID").Value = st_PayInfo.szTxnID

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True

                While dr.Read()
                    st_PayInfo.szMerchantID = dr("MerchantID").ToString()
                End While

            Else
                bReturn = False

                st_PayInfo.szErrDesc = "bGetMerchantIDbyTxnID() error: MerchantID not found."

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.E_INVALID_MERCHANT
                st_PayInfo.szTxnMsg = Common.C_INVALID_MID_2901
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetMerchantIDbyTxnID() exception: " + ex.StackTrace + " " + ex.Message  ' .Message

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bGetTxnFromParam
    ' Function Type:	Boolean.  True if successfully retrieved record or else false
    ' Parameter:		
    ' Description:		Get transaction details based on Param1
    ' History:			12 Jun 2010
    '********************************************************************************************
    Public Function bGetTxnFromParam(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objAdp As SqlDataAdapter = Nothing
        Dim dt As DataTable = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim iRowsAffected As Integer = 0
        Dim iRet As Integer = -1

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetTxnFromParam", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_Param", SqlDbType.VarChar, 100)
            objParam = objCommand.Parameters.Add("@i_HostID", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_Param").Value = st_PayInfo.szParam1
            objCommand.Parameters("@i_HostID").Value = st_HostInfo.iHostID

            objConn.Open()
            objCommand.Connection = objConn

            objAdp = New SqlDataAdapter(objCommand)
            dt = New DataTable
            objAdp.Fill(dt)

            iRowsAffected = dt.Rows.Count()
            If iRowsAffected > 0 Then
                For Each row As DataRow In dt.Rows
                    iRet = Convert.ToInt32(row("RetCode"))
                    st_PayInfo.szTxnMsg = row("RetDesc").ToString()

                    If (iRet <> 0) Then
                        bReturn = False
                        st_PayInfo.szErrDesc = "bGetTxnFromParam() Failed to get transaction info iRet(" + iRet.ToString() + ") Param1(" + st_PayInfo.szParam1 + ") HostID(" + st_HostInfo.iHostID.ToString() + ") due to " + st_PayInfo.szTxnMsg
                        GoTo CleanUp
                    End If

                    st_PayInfo.szTxnID = row("GatewayTxnID").ToString()
                    st_PayInfo.szTxnDateTime = row("DateCreated").ToString()
                    st_PayInfo.szCurrencyCode = row("CurrencyCode").ToString()
                    st_PayInfo.szTxnAmount = row("TxnAmount").ToString()
                    st_PayInfo.szMerchantOrdDesc = row("OrderDesc").ToString()
                    st_PayInfo.szMerchantOrdID = row("OrderNumber").ToString()

                    st_PayInfo.szMerchantID = row("MerchantID").ToString()
                    st_PayInfo.szMerchantTxnID = row("MerchantTxnID").ToString()

                    bReturn = True
                Next
            Else
                bReturn = False
                st_PayInfo.szTxnMsg = "Transaction info not found"
                st_PayInfo.szErrDesc = "bGetTxnFromParam() Failed to get transaction info" + " Param1(" + st_PayInfo.szParam1 + ") HostID(" + st_HostInfo.iHostID.ToString() + ")"
                GoTo CleanUp
            End If

CleanUp:
            If Not dt Is Nothing Then
                dt.Dispose()
                dt = Nothing
            End If

            If Not objAdp Is Nothing Then
                objAdp.Dispose()
                objAdp = Nothing
            End If

        Catch ex As Exception
            bReturn = False
            st_PayInfo.szTxnMsg = "Failed to retrieve transaction info"
            st_PayInfo.szErrDesc = "bGetTxnFromParam() Exception: " + ex.StackTrace + ex.Message + " > Failed to get transaction info based on Param1(" + st_PayInfo.szParam1 + ") HostID(" + st_HostInfo.iHostID.ToString() + ")"

        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bGetTxnResult
    ' Function Type:	Boolean.  True if successfully retrieved record or else false
    ' Parameter:		
    ' Description:		Get transaction result from database
    ' History:			13 Dec 2008
    '********************************************************************************************
    Public Function bGetTxnResult(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim iRowsAffected As Integer = 0
        Dim dr As SqlDataReader = Nothing
        Dim szHashMethod As String = ""

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetTxnResult", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_MerchantTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@d_TxnAmount", SqlDbType.Decimal, 18)     'Added, 18 Aug 2013
            objParam = objCommand.Parameters.Add("@sz_CurrencyCode", SqlDbType.Char, 3)     'Added, 18 Aug 2013
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@sz_MerchantTxnID").Value = st_PayInfo.szMerchantTxnID

            'Added, 5 Sept 2013, checking of TxnAmount, to cater for Reversal using RTxnAmount
            If (st_PayInfo.szTxnAmount <> "") Then
                objCommand.Parameters("@d_TxnAmount").Value = Convert.ToDecimal(st_PayInfo.szTxnAmount) 'Added, 18 Aug 2013
            Else
                objCommand.Parameters("@d_TxnAmount").Value = Convert.ToDecimal(st_PayInfo.szRTxnAmount) 'Added, 5 Sept 2013
            End If

            If (st_PayInfo.szCurrencyCode <> "") Then
                objCommand.Parameters("@sz_CurrencyCode").Value = st_PayInfo.szCurrencyCode             'Added, 18 Aug 2013
            Else
                objCommand.Parameters("@sz_CurrencyCode").Value = st_PayInfo.szRCurrencyCode            'Added, 5 Sept 2013
            End If

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If dr.Read() Then

                st_PayInfo.iQueryStatus = Convert.ToInt32(dr("RetCode"))    'dr.GetInt32(1).ToString
                st_PayInfo.szQueryMsg = dr("RetDesc").ToString()            'dr.GetString(2)

                'Assign initial value
                st_PayInfo.iPKID = -1               ' Added, 5 Sept 2013
                st_PayInfo.szMerchantOrdID = ""
                st_PayInfo.szTxnAmount = ""
                st_PayInfo.szBaseTxnAmount = ""     ' Added on 3 Aug 2009, base currency booking confirmation enhancement
                st_PayInfo.szCurrencyCode = ""
                st_PayInfo.szBaseCurrencyCode = ""  ' Added on 3 Aug 2009, base currency booking confirmation enhancement
                st_PayInfo.szLanguageCode = ""
                st_PayInfo.szIssuingBank = ""
                'st_PayInfo.szSessionID = ""        ' Commented on 14 Apr 2010, do not overwrite Request Session ID
                st_PayInfo.szParam1 = ""
                st_PayInfo.szParam2 = ""
                st_PayInfo.szParam3 = ""
                st_PayInfo.szParam4 = ""
                st_PayInfo.szParam5 = ""
                st_PayInfo.szParam6 = ""
                st_PayInfo.szParam7 = ""
                st_PayInfo.szParam8 = ""
                st_PayInfo.szParam9 = ""
                st_PayInfo.szParam10 = ""
                st_PayInfo.szParam11 = ""
                st_PayInfo.szParam12 = ""
                st_PayInfo.szParam13 = ""
                st_PayInfo.szParam14 = ""
                st_PayInfo.szParam15 = ""
                st_PayInfo.szTxnID = ""
                st_PayInfo.iTxnState = -1
                st_PayInfo.iTxnStatus = -1
                st_PayInfo.iMerchantTxnStatus = -1
                st_PayInfo.iAction = -1
                st_PayInfo.iDuration = -1
                st_PayInfo.iOSRet = 99
                st_PayInfo.iErrSet = -1
                st_PayInfo.iErrNum = -1
                st_PayInfo.szHostTxnID = ""
                st_PayInfo.szTxnMsg = ""
                st_PayInfo.szHashValue = ""
                st_PayInfo.szGatewayID = ""
                st_PayInfo.szCardPAN = ""
                st_PayInfo.szMachineID = ""
                st_PayInfo.szMerchantReturnURL = ""
                st_PayInfo.szMerchantSupportURL = ""
                st_PayInfo.szMerchantCallbackURL = ""   'Added, 3 Dec 2014
                st_PayInfo.szTxnDateTime = ""
                st_HostInfo.szOSPymtCode = ""
                st_HostInfo.iHostID = -1
                st_PayInfo.szCustEmail = ""     'Added  25 Nov 2014 - ENS
                st_PayInfo.szMerchantOrdDesc = ""     'Added, 2 Dec 2014
                st_PayInfo.szCustName = ""      'Added, 3 Dec 2014
                st_PayInfo.szTotRefundAmt = 0   'Added  9 Sept 2015. REFUND
                st_PayInfo.szCardType = ""      'Added, 20 May 2016, for MyDin Card Type
                st_PayInfo.szTokenType = ""     'Added 15 Aug 2016, for token type OCP
                st_PayInfo.szToken = ""         'Added 15 Aug 2016, for token value OCP
                st_PayInfo.szCustOCP = ""       'Added 22 Aug 2016, for tick OCP indicator
                st_PayInfo.szCustPhone = ""     'Added, 12 Feb 2018, Tik FX

                ' 1 - Transaction exists in Response table, has reached its final state
                '-1 - Transaction only exists in Request table, still has not reached final state, maybe under processing
                If ((1 = st_PayInfo.iQueryStatus) Or (-1 = st_PayInfo.iQueryStatus)) Then
                    st_PayInfo.iPKID = Convert.ToInt32(dr("PKID"))                              ' Added, 5 Sept 2013
                    st_PayInfo.szMerchantOrdID = dr("OrderNumber").ToString()
                    st_PayInfo.szTxnAmount = dr("TxnAmount").ToString()
                    st_PayInfo.szBaseTxnAmount = dr("BaseTxnAmount").ToString()                 ' Added on 3 Aug 2009, base currency booking confirmation enhancement
                    st_PayInfo.szCurrencyCode = dr("CurrencyCode").ToString()
                    st_PayInfo.szBaseCurrencyCode = Trim(dr("BaseCurrencyCode").ToString())     ' Added on 3 Aug 2009, base currency booking confirmation enhancement
                    st_PayInfo.szLanguageCode = dr("LanguageCode").ToString()
                    st_PayInfo.szIssuingBank = dr("IssuingBank").ToString()
                    'Added on 14 Apr 2010, if Request's SessionID is not empty, do not overwrite it.
                    If ("" = st_PayInfo.szMerchantSessionID) Then                               'Modified 16 Jul 2014, SessionID to MerchantSessionID
                        st_PayInfo.szMerchantSessionID = dr("SessionID").ToString()
                    End If
                    st_PayInfo.szParam1 = dr("Param1").ToString()

                    If (dr("Param2").ToString() <> "") Then
                        st_PayInfo.szParam2 = objHash.szDecryptAES(dr("Param2").ToString(), Common.C_3DESAPPKEY)
                        st_PayInfo.szClearCardPAN = st_PayInfo.szParam2 'Added, 17 May 2016, for OCP token creation
                    Else
                        st_PayInfo.szParam2 = dr("Param2").ToString()
                    End If

                    If (dr("Param3").ToString() <> "") Then
                        st_PayInfo.szParam3 = objHash.szDecryptAES(dr("Param3").ToString(), Common.C_3DESAPPKEY)
                    Else
                        st_PayInfo.szParam3 = dr("Param3").ToString()
                    End If

                    'Added, 9 Oct 2015, for MyDin
                    If (dr("CardExp").ToString() <> "") Then
                        st_PayInfo.szCardExp = objHash.szDecryptAES(dr("CardExp").ToString(), Common.C_3DESAPPKEY)
                    End If

                    st_PayInfo.szParam4 = dr("Param4").ToString()
                    st_PayInfo.szParam5 = dr("Param5").ToString()
                    st_PayInfo.szParam6 = dr("Param6").ToString()
                    st_PayInfo.szParam7 = dr("Param7").ToString()
                    st_PayInfo.szParam8 = dr("Param8").ToString()
                    st_PayInfo.szParam9 = dr("Param9").ToString()
                    st_PayInfo.szParam10 = dr("Param10").ToString()

                    If ("" <> st_PayInfo.szParam10) Then    'Added by FM, 24 May 2017
                        st_PayInfo.iInstallment = Convert.ToInt16(st_PayInfo.szParam10)
                    End If

                    st_PayInfo.szParam11 = dr("Param11").ToString()
                    st_PayInfo.szParam12 = dr("Param12").ToString()
                    st_PayInfo.szParam13 = dr("Param13").ToString()
                    st_PayInfo.szParam14 = dr("Param14").ToString()
                    st_PayInfo.szParam15 = dr("Param15").ToString()
                    st_PayInfo.szTxnID = dr("GatewayTxnID").ToString()
                    st_PayInfo.szTxnDateTime = dr("DateCreated").ToString()

                    st_PayInfo.iTxnState = Convert.ToInt32(dr("TxnState"))
                    st_PayInfo.iTxnStatus = Convert.ToInt32(dr("TxnStatus"))
                    st_PayInfo.iMerchantTxnStatus = Convert.ToInt32(dr("MerchantTxnStatus"))
                    st_PayInfo.iAction = Convert.ToInt32(dr("Action"))
                    st_PayInfo.iDuration = Convert.ToInt32(dr("Duration"))
                    st_PayInfo.iOSRet = Convert.ToInt32(dr("OSRet"))
                    st_PayInfo.iErrSet = Convert.ToInt32(dr("ErrSet"))
                    st_PayInfo.iErrNum = Convert.ToInt32(dr("ErrNum"))

                    st_PayInfo.szRespCode = dr("BankRespCode").ToString()
                    st_PayInfo.szHostTxnID = dr("BankRefNo").ToString()
                    st_PayInfo.szAuthCode = dr("AuthCode").ToString()           'Added, 5 Sept 2013
                    st_PayInfo.szTxnMsg = dr("RespMesg").ToString()

                    szHashMethod = dr("HashMethod").ToString()

                    st_PayInfo.szGatewayID = dr("GatewayID").ToString()
                    st_PayInfo.szCardPAN = dr("CardPAN").ToString()
                    If st_PayInfo.szCardPAN.Length > 10 Then
                        st_PayInfo.szMaskedCardPAN = st_PayInfo.szCardPAN.Substring(0, 6) + "".PadRight(st_PayInfo.szCardPAN.Length - 6 - 4, "X") + st_PayInfo.szCardPAN.Substring(st_PayInfo.szCardPAN.Length - 4, 4)
                    Else
                        st_PayInfo.szMaskedCardPAN = "".PadRight(st_PayInfo.szCardPAN.Length, "X")
                    End If
                    st_PayInfo.szCardHolder = dr("CardHolder").ToString()   'Added, 9 Oct 2015, for MyDin
                    st_PayInfo.szMachineID = dr("MachineID").ToString()
                    st_PayInfo.szMerchantReturnURL = dr("MerchantReturnURL").ToString()
                    st_PayInfo.szMerchantSupportURL = dr("MerchantSupportURL").ToString()

                    st_HostInfo.szOSPymtCode = dr("OSPymtCode").ToString()
                    st_HostInfo.iHostID = Convert.ToInt32(dr("HostID"))
                    st_PayInfo.szCustEmail = dr("CustEmail").ToString()     'Added  25 Nov 2014 - ENS
                    st_PayInfo.szMerchantOrdDesc = dr("OrderDesc").ToString()     'Added, 2 Dec 2014, to show OrderDesc on email notification when Query
                    st_PayInfo.szCustName = dr("CustName").ToString()       'Added, 3 Dec 2014
                    st_PayInfo.szMerchantCallbackURL = dr("MerchantCallbackURL").ToString()       'Added, 3 Dec 2014
                    st_PayInfo.szTotRefundAmt = dr("TotRefundAmt").ToString()                     'Added  9 Sept 2015. REFUND
                    st_PayInfo.szCardType = dr("CardType").ToString()                             'Added, 20 May 2016, for MyDin Card Type
                    st_PayInfo.szTokenType = dr("TokenType").ToString()                           'Added 15 Aug 2016, for token type OCP
                    st_PayInfo.szToken = dr("Token").ToString()                                   'Added 15 Aug 2016, for token value OCP
                    st_PayInfo.szCustOCP = dr("CustOCP").ToString()                               'Added 22 Aug 2016, for tick OCP indicator
                    'Added 20 Oct 2017, Conversion Currency
                    If ("" = st_PayInfo.szFXCurrencyCode) Then
                        If (Trim(dr("FXCurrencyCode").ToString()) <> "") Then
                            st_PayInfo.szFXCurrencyCode = dr("FXCurrencyCode").ToString()
                        End If

                        If (Trim(dr("FXTxnAmount").ToString()) <> "") Then
                            st_PayInfo.szFXTxnAmount = dr("FXTxnAmount").ToString()
                        End If
                    End If
                    st_PayInfo.szCustPhone = dr("CustPhone").ToString()                         'Added, 12 Feb 2018, Tik FX
                End If

                If (1 = st_PayInfo.iQueryStatus) Then
                    ' If Query request's Hash Method is the same as Hash Method for txn existed in Response table,
                    ' then just use the HashValue in Response table or else have to recalculate the response hash based
                    ' on Query request's Hash Method
                    If (st_PayInfo.szHashMethod.ToLower() = szHashMethod.ToLower()) Then
                        'Added checking of whether HashValue is empty or not on 3 Apr 2010
                        If (dr("HashValue").ToString() <> "") Then
                            st_PayInfo.szHashValue = dr("HashValue").ToString()
                        End If
                    End If
                ElseIf (-1 = st_PayInfo.iQueryStatus) Then
                    'Added on 20 Mar 2010 to cater for ability to retrieve the original request's hash method, especially after 2nd entry page
                    st_PayInfo.szHashMethod = dr("HashMethod").ToString()
                End If

                bReturn = True
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetTxnResult() exception: " + ex.StackTrace + ex.Message + " > GatewayTxnID(" + st_PayInfo.szTxnID + ")"

            st_PayInfo.iQueryStatus = 2
            st_PayInfo.szQueryMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bGetTxnPymtMethod
    ' Function Type:	Boolean.  True if get successfully else false
    ' Parameter:		sz_PymtMethod
    ' Description:		Get payment method of the transaction
    ' History:			18 Aug 2013
    '********************************************************************************************
    Public Function bGetTxnPymtMethod(ByRef sz_PymtMethod As String, ByRef i_Exist As Integer, ByRef st_PayInfo As Common.STPayInfo) As Boolean

        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRowsAffected As Integer = 0

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetTxnRef", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_MerchantTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@d_TxnAmount", SqlDbType.Decimal, 18)
            objParam = objCommand.Parameters.Add("@sz_CurrencyCode", SqlDbType.Char, 3)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@sz_MerchantTxnID").Value = st_PayInfo.szMerchantTxnID

            ''''''''''''Modified  2 Feb 2018, solve Internal Error PTxnMsg for FX'''''''''''''
            If (st_PayInfo.szBaseCurrencyCode <> "" And String.Compare(st_PayInfo.szCurrencyCode, st_PayInfo.szBaseCurrencyCode, True) <> 0) Then
                objCommand.Parameters("@d_TxnAmount").Value = Convert.ToDecimal(st_PayInfo.szBaseTxnAmount)
                objCommand.Parameters("@sz_CurrencyCode").Value = st_PayInfo.szBaseCurrencyCode
            Else
                objCommand.Parameters("@d_TxnAmount").Value = Convert.ToDecimal(st_PayInfo.szTxnAmount)
                objCommand.Parameters("@sz_CurrencyCode").Value = st_PayInfo.szCurrencyCode
            End If
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Commented  2 Feb 2018
            'objCommand.Parameters("@d_TxnAmount").Value = Convert.ToDecimal(st_PayInfo.szTxnAmount)
            'objCommand.Parameters("@sz_CurrencyCode").Value = st_PayInfo.szCurrencyCode

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If objCommand.Parameters("@i_Return").Value = 0 Then

                While dr.Read()
                    bReturn = True
                    i_Exist = 1     'Added, 6 Sept 2013
                    sz_PymtMethod = dr("PymtMethod").ToString()
                    st_PayInfo.szMerchantOrdID = dr("OrderNumber").ToString() 'Added, 10 Sept 2013
                    st_PayInfo.iDuration = Convert.ToInt32(dr("Duration"))  'Added, 15 Oct 2014
                    st_PayInfo.iTxnStatus = Convert.ToInt32(dr("TxnStatus"))  'Added 02 Nov 2017, response to merchant. 'Added  1 Nov 2017. To avoid proceed PMEntry 1 txn which had expired in pymt page
                    st_PayInfo.iTxnState = Convert.ToInt32(dr("IsFinalized"))  'Added 02 Nov 2017, here we will finalized and set the TxnState=12(commit failed) when the payment's TxnStatus>=2900 OR TxnStatus=-3
                    dr.Read()
                End While

                If (False = bReturn) Then
                    i_Exist = 0     'Added, 7 Oct 2013, if not exists, somehow still getting @i_Return as 0

                    bReturn = True  'Modified 7 Oct 2013

                    st_PayInfo.szErrDesc = "bGetTxnPymtMethod(): Failed to get Payment Method 1"

                    st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                    st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                    st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
                End If
            Else
                i_Exist = 0     'Added, 6 Sept 2013

                bReturn = True  'Modified 6 Sept 2013

                st_PayInfo.szErrDesc = "bGetTxnPymtMethod(): Failed to get Payment Method 2"

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False
            st_PayInfo.szErrDesc = "bGetTxnPymtMethod() exception: " + ex.StackTrace + " " + ex.Message

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bGetTxnStatusAction
    ' Function Type:	Boolean.  True if get txn status's action successfully else false
    ' Parameter:		st_HostInfo, st_PayInfo
    ' Description:		Get transaction status's action
    ' History:			31 Oct 2008
    '********************************************************************************************
    Public Function bGetTxnStatusAction(ByRef st_HostInfo As Common.STHost, ByRef st_PayInfo As Common.STPayInfo) As Boolean

        Dim bReturn As Boolean = True
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRet As Integer = 0
        Dim szStatus As String = "" 'Added on 6 Apr 2010 for logging purposes

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetTxnStatusAction", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@i_HostID", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_ActionID", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@sz_TxnStatus", SqlDbType.VarChar, 60)    'Modified 4 to 60 on 24 Aug 2009
            objParam = objCommand.Parameters.Add("@sz_TxnType", SqlDbType.VarChar, 7)

            If (True = st_PayInfo.bRespCodeCS) Then                                         'Added  25 Aug 2016. Auth&Capture. to cater for case sensitive respcode
                objParam = objCommand.Parameters.Add("@i_CaseSensitive", SqlDbType.Int)
            End If

            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@i_HostID").Value = st_HostInfo.iHostID
            objCommand.Parameters("@i_ActionID").Value = st_HostInfo.iTxnStatusActionID
            If (st_PayInfo.szHostTxnStatus.Length > 0) Then
                objCommand.Parameters("@sz_TxnStatus").Value = st_PayInfo.szHostTxnStatus
                szStatus = st_PayInfo.szHostTxnStatus   'Added on 6 Apr 2010
            Else
                objCommand.Parameters("@sz_TxnStatus").Value = st_PayInfo.szRespCode
                szStatus = st_PayInfo.szRespCode        'Added on 6 Apr 2010
            End If
            objCommand.Parameters("@sz_TxnType").Value = st_PayInfo.szTxnType

            If (True = st_PayInfo.bRespCodeCS) Then                                          'Added  25 Aug 2016. Auth&Capture. to cater for case sensitive respcode
                objCommand.Parameters("@i_CaseSensitive").Value = 1
            End If

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            iRet = -1
            If dr.Read() Then
                iRet = Convert.ToInt32(dr("RetCode"))
            End If

            If iRet = 0 Then
                bReturn = True
                '[Action], [Desc]
                st_PayInfo.iAction = Convert.ToInt32(dr("Action"))  ' Action
            Else
                st_PayInfo.szErrDesc = "bGetTxnStatusAction() error: Failed to get Txn Status Action" + " > HostID(" + st_HostInfo.iHostID.ToString() + ") ActionID(" + st_HostInfo.iTxnStatusActionID.ToString() + ") TxnStatus(" + szStatus + ") TxnType(" + st_PayInfo.szTxnType + ")"

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT

                bReturn = False
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetTxnStatusAction() exception: " + ex.StackTrace + " " + ex.Message + " > HostID(" + st_HostInfo.iHostID.ToString() + ") ActionID(" + st_HostInfo.iTxnStatusActionID.ToString() + ") TxnStatus(" + szStatus + ") TxnType(" + st_PayInfo.szTxnType + ")"

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bGetTxnStatusEx
    ' Function Type:	Boolean.  True if get any mapping of response fields and response status successfully else false
    ' Parameter:		st_HostInfo, st_PayInfo
    ' Description:		Get mapping of response fields and response status
    ' History:			22 Sept 2013
    '********************************************************************************************
    Public Function bGetTxnStatusActionEx(ByRef sz_TxnField As String, ByRef sz_TxnStatus As String, ByRef sz_TxnAction As String, ByRef st_HostInfo As Common.STHost, ByRef st_PayInfo As Common.STPayInfo) As Boolean

        Dim bReturn As Boolean = True
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetTxnStatusActionEx", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@i_HostID", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_ActionID", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@sz_TxnType", SqlDbType.VarChar, 7)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@i_HostID").Value = st_HostInfo.iHostID
            objCommand.Parameters("@i_ActionID").Value = st_HostInfo.iTxnStatusActionID
            objCommand.Parameters("@sz_TxnType").Value = st_PayInfo.szTxnType

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            sz_TxnField = ""
            sz_TxnStatus = ""
            sz_TxnAction = ""

            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True

                While dr.Read()
                    'Example:
                    'TxnField                           TxnStatus                       Action  Desc        Priority
                    'IsFullyCaptured	                true	                            1	Success	    1
                    'IsFullyCaptured;IsReversed;sError	false;false;Success	                2	Failed	    1
                    'sError	                            TrxnSearchbyOrderIDOrderNotFound	3	Pending	    1
                    'IsReversed	                        true	                            9	Reversed	1
                    sz_TxnField = sz_TxnField + dr("TxnField").ToString() + "|"
                    sz_TxnStatus = sz_TxnStatus + dr("TxnStatus").ToString() + "|"
                    sz_TxnAction = sz_TxnAction + Convert.ToInt32(dr("Action")).ToString() + "|"
                End While
                'Removes the rightmost "|"
                sz_TxnField = Left(sz_TxnField, sz_TxnField.Length() - 1)
                sz_TxnStatus = Left(sz_TxnStatus, sz_TxnStatus.Length() - 1)
                sz_TxnAction = Left(sz_TxnAction, sz_TxnAction.Length() - 1)
            Else
                st_PayInfo.szErrDesc = "bGetTxnStatusActionEx() error: Failed to get results" + " > HostID(" + st_HostInfo.iHostID.ToString() + ") TxnStatusActionID(" + st_HostInfo.iTxnStatusActionID.ToString() + ") TxnType(" + st_PayInfo.szTxnType + ")"

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT

                bReturn = False
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetTxnStatusActionEx() exception: " + ex.StackTrace + " " + ex.Message

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bGetCurrency
    ' Function Type:	Boolean.  True if get currency information successfully else false
    ' Parameter:		st_HostInfo, st_PayInfo
    ' Description:		Maps currency from ISO4217 to ISO3166 format
    ' History:			23 Mar 2009
    '********************************************************************************************
    Public Function bGetCurrency(ByVal sz_CurrencyCode As String, ByRef st_PayInfo As Common.STPayInfo) As String

        Dim szReturn As String = ""
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetCurrency", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_CurrencyCode", SqlDbType.VarChar, 5)
            objCommand.Parameters("@sz_CurrencyCode").Value = sz_CurrencyCode
            'Modified the following to sz_Currency on 6 Oct 2009
            'objCommand.Parameters("@sz_CurrencyCode").Value = st_PayInfo.szCurrencyCode

            objParam = objCommand.Parameters.Add("@sz_Currency", SqlDbType.VarChar, 5)
            objParam.Direction = ParameterDirection.Output

            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objConn.Open()
            objCommand.Connection = objConn

            objCommand.ExecuteScalar()

            If objCommand.Parameters("@i_Return").Value = 0 Then
                szReturn = objCommand.Parameters("@sz_Currency").Value
            Else
                st_PayInfo.szErrDesc = "bGetCurrency() error: Failed to get currency" + " > CurrencyCode(" + sz_CurrencyCode + ")"

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
            End If

        Catch ex As Exception
            st_PayInfo.szErrDesc = "bGetCurrency() exception: " + ex.StackTrace + " " + ex.Message + " > CurrencyCode(" + sz_CurrencyCode + ")"

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT

        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return szReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bCheckLateResp
    ' Function Type:	Boolean.  True if not late response else false if (late response or request not found)
    ' Parameter:		st_PayInfo
    ' Description:		Check whether Host reply is a late response
    ' History:			31 Oct 2008
    '********************************************************************************************
    Public Function bCheckLateResp(ByRef st_PayInfo As Common.STPayInfo, ByVal sz_ReplyDateTime As String) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRowsAffected As Integer = 0
        Dim iRet As Integer = 0

        Dim szRetDesc As String = ""
        Dim iLateRespTimeSec As Integer = Convert.ToInt32(ConfigurationManager.AppSettings("LateRespTimeSec"))

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spCheckLateResp", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_GatewayTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_ReplyDateTime", SqlDbType.VarChar, 23)
            objParam = objCommand.Parameters.Add("@i_LateRespTimeSec", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_GatewayTxnID").Value = st_PayInfo.szTxnID
            objCommand.Parameters("@sz_ReplyDateTime").Value = sz_ReplyDateTime
            objCommand.Parameters("@i_LateRespTimeSec").Value = iLateRespTimeSec

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If objCommand.Parameters("@i_Return").Value = 0 Then        'Non late response
                'Added on 30 May 2009, because if stored procedure got return recordset and got return value then
                'ExecuteReader will not be able to get the return value as expected but only able to get recordset
                iRet = -1
                If dr.Read() Then
                    iRet = Convert.ToInt32(dr("RetCode"))
                    szRetDesc = dr("RetDesc").ToString()
                End If

                If (0 = iRet) Then      'Non-late response
                    bReturn = True

                ElseIf (-1 = iRet) Then 'Late response
                    st_PayInfo.szErrDesc = "bCheckLateResp() error: Late Host response discarded - HostTxnStatus(" + st_PayInfo.szHostTxnStatus + ") > Host Reply Date Time(" + sz_ReplyDateTime + ") DateCreated(" + szRetDesc + ") (>" + iLateRespTimeSec.ToString() + " seconds)"

                    st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.LATE_HOST_REPLY
                    st_PayInfo.szTxnMsg = Common.C_LATE_HOST_REPLY_2908
                    st_PayInfo.iTxnState = Common.TXN_STATE.ABORT

                    bReturn = False

                Else                    'Invalid/Late response
                    st_PayInfo.szErrDesc = "bCheckLateResp() error: Invalid/Late Host response discarded - HostTxnStatus(" + st_PayInfo.szHostTxnStatus + ") " + szRetDesc

                    st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.LATE_HOST_REPLY
                    st_PayInfo.szTxnMsg = Common.C_LATE_HOST_REPLY_2908
                    st_PayInfo.iTxnState = Common.TXN_STATE.ABORT

                    bReturn = False
                End If

            ElseIf objCommand.Parameters("@i_Return").Value = -1 Then   'Late response
                While dr.Read()
                    szRetDesc = dr("RetDesc").ToString()
                    dr.Read()
                End While

                st_PayInfo.szErrDesc = "bCheckLateResp() error: Late Host response discarded - HostTxnStatus(" + st_PayInfo.szHostTxnStatus + ") > Host Reply Date Time(" + sz_ReplyDateTime + ") DateCreated(" + szRetDesc + ") (>" + iLateRespTimeSec.ToString() + " seconds)"

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.LATE_HOST_REPLY
                st_PayInfo.szTxnMsg = Common.C_LATE_HOST_REPLY_2908
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT

                bReturn = False
            Else                                                        'Invalid/late response
                While dr.Read()
                    szRetDesc = dr("RetDesc").ToString()
                    dr.Read()
                End While

                st_PayInfo.szErrDesc = "bCheckLateResp() error: Invalid/Late Host response discarded - HostTxnStatus(" + st_PayInfo.szHostTxnStatus + ") " + szRetDesc

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.LATE_HOST_REPLY
                st_PayInfo.szTxnMsg = Common.C_LATE_HOST_REPLY_2908
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT

                bReturn = False
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bCheckLateResp() exception: " + ex.StackTrace + " " + ex.Message + " > Host Reply Date Time(" + sz_ReplyDateTime + ") LateRespTimeSec(" + iLateRespTimeSec.ToString() + ")"

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bGetMerchantRunningNo
    ' Function Type:	Boolean.  True if get merchant running number successfully else false
    ' Parameter:		st_PayInfo
    ' Description:		Get merchant running number based on Merchant ID
    ' History:			14 Nov 2008 Created
    '                   Combined  31 Mar 2014
    '                   4 Dec 2013, added TxnIDFormat
    '********************************************************************************************
    Public Function bGetMerchantRunningNo(ByRef st_PayInfo As Common.STPayInfo, ByVal i_TxnIDFormat As Integer) As Boolean

        Dim bReturn As Boolean = True
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRowsAffected As Integer = 0

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetMerchantRunningNo", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@i_Today", SqlDbType.Int)                 'Added  3 Dec 2013. Alliance bank Credit, CIMBClicks, eNETS
            objParam = objCommand.Parameters.Add("@i_GatewayTxnIDFormat", SqlDbType.Int)    'Added  3 Dec 2013. Alliance bank Credit, CIMBClicks, eNETS
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID

            'Modified 31 Mar 2014, added checking of szTxnDateTime
            If (st_PayInfo.szTxnDateTime <> "" And True = IsNumeric(st_PayInfo.szTxnDateTime)) Then
                objCommand.Parameters("@i_Today").Value = Convert.ToInt32(st_PayInfo.szTxnDateTime) 'Added  3 Dec 2013. Alliance bank Credit, CIMBClicks, eNETS
            Else
                objCommand.Parameters("@i_Today").Value = 0 'Added, 31 Mar 2014
            End If

            objCommand.Parameters("@i_GatewayTxnIDFormat").Value = i_TxnIDFormat

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True

                While dr.Read()
                    st_PayInfo.szRunningNo = dr("RunningNo").ToString()
                    dr.Read()
                End While
            Else
                bReturn = False

                st_PayInfo.szErrDesc = "bGetMerchantRunningNo() error: Invalid MerchantID/Merchant."

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.E_INVALID_MERCHANT
                st_PayInfo.szTxnMsg = Common.C_INVALID_MID_2901
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetMerchantRunningNo() exception: " + ex.StackTrace + " " + ex.Message  ' .Message

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bGetMerchantHostRunningNo
    ' Function Type:	Boolean.  True if get merchant-host running number/trace number successfully else false
    ' Parameter:		st_PayInfo, st_HostInfo
    ' Description:		Get merchant-host running number/trace number based on Merchant ID and Host ID
    ' History:			18 Sept 2010
    '********************************************************************************************
    Public Function bGetMerchantHostRunningNo(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost) As Boolean

        Dim bReturn As Boolean = True
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRowsAffected As Integer = 0

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetMerchantHostRunningNo", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@i_HostID", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_NoOfRunningNo", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_UniquePerDay", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_Today", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@i_HostID").Value = st_HostInfo.iHostID
            objCommand.Parameters("@i_NoOfRunningNo").Value = st_HostInfo.iNoOfRunningNo
            objCommand.Parameters("@i_UniquePerDay").Value = st_HostInfo.iRunningNoUniquePerDay
            objCommand.Parameters("@i_Today").Value = Convert.ToInt32(Left(st_PayInfo.szTxnDateTime.Replace("-", "").Replace(":", "").Replace(" ", ""), 8))

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True

                While dr.Read()
                    st_HostInfo.szRunningNo = dr("RunningNo").ToString()
                    dr.Read()
                End While
            Else
                bReturn = False

                st_PayInfo.szErrDesc = "bGetMerchantHostRunningNo() error: Invalid MerchantID/HostID"

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.E_INVALID_MERCHANT
                st_PayInfo.szTxnMsg = Common.C_INVALID_MID_2901
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetMerchantHostRunningNo() exception: " + ex.StackTrace + " " + ex.Message  ' .Message

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bGetTxnIDRunningNo
    ' Function Type:	Boolean.  True if get host running number successfully else false
    ' Parameter:		st_PayInfo, st_HostInfo
    ' Description:		Get host running number based on GatewayTxnIDFormat
    ' History:			30 Oct 2009
    '********************************************************************************************
    Public Function bGetTxnIDRunningNo(ByRef st_PayInfo As Common.STPayInfo, ByVal st_HostInfo As Common.STHost) As Boolean

        Dim bReturn As Boolean = True
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRowsAffected As Integer = 0

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetTxnIDRunningNo", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@i_GatewayTxnIDFormat", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@i_GatewayTxnIDFormat").Value = st_HostInfo.iGatewayTxnIDFormat

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True

                While dr.Read()
                    st_PayInfo.szRunningNo = dr("RunningNo").ToString()
                    st_PayInfo.iTxnIDMaxLen = Convert.ToInt32(dr("MaxLen"))
                    dr.Read()
                End While
            Else
                bReturn = False

                st_PayInfo.szErrDesc = "bGetTxnIDRunningNo() error: Invalid GatewayTxnIDFormat."

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INVALID_INPUT
                st_PayInfo.szTxnMsg = Common.C_INVALID_INPUT_2906
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetTxnIDRunningNo() exception: " + ex.StackTrace + " " + ex.Message  ' .Message

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bUpdateHostInfo
    ' Function Type:	Boolean.  True if update successful else false
    ' Parameter:		st_PayInfo, st_HostInfo
    ' Description:		Updates Host table
    ' History:			13 Mar 2009
    '********************************************************************************************
    Public Function bUpdateHostInfo(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost) As Boolean

        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRowsAffected As Integer = 0

        Try
            bReturn = False
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spUpdateHostInfo", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@i_HostID", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_QueryFlag", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@i_HostID").Value = st_HostInfo.iHostID
            objCommand.Parameters("@i_QueryFlag").Value = st_HostInfo.iQueryFlag

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()
            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True
            Else
                bReturn = False

                st_PayInfo.szErrDesc = "bUpdateHostInfo() error: Failed to update QueryFlag(" + st_HostInfo.iQueryFlag.ToString() + ") for GatewayTxnID(" + st_PayInfo.szTxnID + ")"

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.iTxnState = Common.TXN_STATE.FAILED
                st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bUpdateHostInfo() exception: " + ex.ToString + " > QueryFlag(" + st_HostInfo.iQueryFlag.ToString() + ") for GatewayTxnID(" + st_PayInfo.szTxnID + ")"

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.FAILED
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bGetHostRouting
    ' Function Type:	Boolean.  True if get routing information successfully else false
    ' Parameter:		st_PayInfo
    ' Description:		Get Routing bank information
    ' History:			29 Nov 2011
    '********************************************************************************************
    Public Function bGetHostRouting(ByRef st_PayInfo As Common.STPayInfo) As Boolean

        Dim bReturn As Boolean = True
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetHostRouting", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_CardPAN", SqlDbType.VarChar, 20)
            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_CurrencyID", SqlDbType.VarChar, 3)
            objParam = objCommand.Parameters.Add("@i_3DFlag", SqlDbType.Int)                'Added  21 May 2014, Firefly FF

            objCommand.Parameters("@sz_CardPAN").Value = st_PayInfo.szParam2
            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@sz_CurrencyID").Value = st_PayInfo.szCurrencyCode

            'Added  21 May 2014, Firefly FF
            If (1 = st_PayInfo.iSelfMPI) Then
                If (st_PayInfo.sz3DFlag = "" Or st_PayInfo.szECI = "00" Or st_PayInfo.szECI = "07") Then
                    objCommand.Parameters("@i_3DFlag").Value = 0 'N3D
                End If
            Else
                If (2 = st_PayInfo.i3DAccept Or st_PayInfo.sz3DFlag = "N3DNP") Then  'Non-3D only
                    objCommand.Parameters("@i_3DFlag").Value = 0 'N3D
                End If
            End If

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If dr.HasRows Then

                While dr.Read()
                    st_PayInfo.szIssuingBank = dr("HostName").ToString()
                End While

                If "" <> st_PayInfo.szIssuingBank Then
                    bReturn = True
                Else
                    bReturn = False

                    st_PayInfo.szErrDesc = "bGetHostRouting() error: Failed to get bank host."

                    st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.INVALID_HOST
                    st_PayInfo.szTxnMsg = Common.C_INVALID_HOST_902
                    st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
                End If
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetHostRouting() exception: [" + ex.StackTrace + "][" + ex.Message + "][" + Left(st_PayInfo.szParam2, 6) + "][" + st_PayInfo.szMerchantID + "][" + st_PayInfo.szCurrencyCode + "][" + st_PayInfo.sz3DFlag + "]"

            'TESTING
            'st_PayInfo.szErrDesc = "bGetHostRouting() Param2[" + st_PayInfo.szParam2 + "] SvcID[" + st_PayInfo.szMerchantID + "] CurrencyCode[" + st_PayInfo.szCurrencyCode + "] exception: " + ex.StackTrace + " " + ex.Message

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bGetHostRoutingByParam
    ' Function Type:	Boolean.  True if get routing information successfully else false
    ' Parameter:		st_PayInfo
    ' Description:		Get Routing bank information by param to determine which bank to get
    ' History:			17 Jan 2018
    '********************************************************************************************
    Public Function bGetHostRoutingByParam(ByRef st_PayInfo As Common.STPayInfo) As Boolean

        Dim bReturn As Boolean = True
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetHostRoutingByParam", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_CardPAN", SqlDbType.VarChar, 20)
            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_CurrencyID", SqlDbType.VarChar, 3)
            objParam = objCommand.Parameters.Add("@i_3DFlag", SqlDbType.Int)                'Added  21 May 2014, Firefly FF
            objParam = objCommand.Parameters.Add("@i_RouteSetting", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@sz_RouteValue", SqlDbType.VarChar, 40)

            objCommand.Parameters("@sz_CardPAN").Value = st_PayInfo.szParam2
            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@sz_CurrencyID").Value = st_PayInfo.szCurrencyCode

            'Added  21 May 2014, Firefly FF
            If (1 = st_PayInfo.iSelfMPI) Then
                If (st_PayInfo.sz3DFlag = "" Or st_PayInfo.szECI = "00" Or st_PayInfo.szECI = "07") Then
                    objCommand.Parameters("@i_3DFlag").Value = 0 'N3D
                End If
            Else
                If (2 = st_PayInfo.i3DAccept Or st_PayInfo.sz3DFlag = "N3DNP") Then  'Non-3D only
                    objCommand.Parameters("@i_3DFlag").Value = 0 'N3D
                End If
            End If
            objCommand.Parameters("@i_RouteSetting").Value = st_PayInfo.iRouteByParam1
            If (st_PayInfo.iRouteByParam1 = 10) Then
                objCommand.Parameters("@sz_RouteValue").Value = st_PayInfo.szMerchantTxnID
            End If

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If dr.HasRows Then

                While dr.Read()
                    st_PayInfo.szIssuingBank = dr("HostName").ToString()
                End While

                If "" <> st_PayInfo.szIssuingBank Then
                    bReturn = True
                Else
                    bReturn = False

                    st_PayInfo.szErrDesc = "bGetHostRoutingByParam() error: Failed to get bank host."

                    st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.INVALID_HOST
                    st_PayInfo.szTxnMsg = Common.C_INVALID_HOST_902
                    st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
                End If
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetHostRoutingByParam() exception: [" + ex.StackTrace + "][" + ex.Message + "][" + Left(st_PayInfo.szParam2, 6) + "][" + st_PayInfo.szMerchantID + "][" + st_PayInfo.szCurrencyCode + "][" + st_PayInfo.sz3DFlag + "]"

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bGetOCPToken
    ' Function Type:	Boolean.  True if get one-click payment token successfully else false
    ' Parameter:		st_PayInfo
    ' Description:		Get One-Click Payment Token
    ' History:			1 Aug 2014
    '********************************************************************************************
    Public Function bGetOCPToken(ByRef st_PayInfo As Common.STPayInfo, Optional i_Action As Integer = 0) As Boolean

        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRowsAffected As Integer = 0
        Dim iRetCode As Integer = 0         'Added, 14 Aug 2014
        Dim szRetDesc As String = ""        'Added, 14 Aug 2014

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("Token_GetOCP", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_Token", SqlDbType.VarChar, 50)
            objParam = objCommand.Parameters.Add("@sz_CustEmail", SqlDbType.VarChar, 60)    'Added, 11 Aug 2014
            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)    'Added 18 Aug 2016
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            If 1 = i_Action Then 'Added 8 Aug 2017
                objParam = objCommand.Parameters.Add("@sz_EncyCardPan", SqlDbType.VarChar, 100)
                objCommand.Parameters("@sz_Token").Value = ""
                objCommand.Parameters("@sz_CustEmail").Value = ""
                objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
                If (st_PayInfo.szClearCardPAN <> "") Then
                    objCommand.Parameters("@sz_EncyCardPan").Value = objHash.szEncryptAES(st_PayInfo.szClearCardPAN, Common.C_3DESAPPKEY)
                End If
            Else
                objCommand.Parameters("@sz_Token").Value = st_PayInfo.szToken

                If (3 = st_PayInfo.iAllowOCP) Then 'Added 12 Feb 2018, TIK FX
                    objCommand.Parameters("@sz_CustEmail").Value = st_PayInfo.szCustPhone
                Else
                    objCommand.Parameters("@sz_CustEmail").Value = st_PayInfo.szCustEmail           'Added, 11 Aug 2014
                End If

                objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID          'Added 18 Aug 2016
            End If

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If dr.Read() Then
                iRetCode = Convert.ToInt32(dr("RetCode"))
                szRetDesc = dr("RetDesc").ToString()

                If iRetCode = 1 Then
                    bReturn = True
                    st_PayInfo.szToken = dr("Token").ToString()         'Added 18 Aug 2016
                    st_PayInfo.szParam3 = dr("CardExp").ToString()
                    If 1 <> i_Action Then
                        '[CardPAN],[CardExp],[CardHolder]
                        st_PayInfo.szParam2 = dr("CardPAN").ToString()
                        st_PayInfo.szCardPAN = st_PayInfo.szParam2
                        st_PayInfo.szParam4 = dr("CardHolder").ToString()
                    End If
                Else
                    bReturn = False

                    If (3 = st_PayInfo.iAllowOCP) Then  'Modified 12 Feb 2018, Tik FX
                        st_PayInfo.szErrDesc = "bGetOCPToken() error:Unable retrieve card info, Token[" + st_PayInfo.szToken + "] CustPhone[" + st_PayInfo.szCustPhone + "]"
                    Else
                        st_PayInfo.szErrDesc = "bGetOCPToken() error:Unable retrieve card info, Token[" + st_PayInfo.szToken + "] CustEmail[" + st_PayInfo.szCustEmail + "]"
                    End If

                    'st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                    'st_PayInfo.iTxnStatus = Common.TXN_STATUS.INVALID_INPUT
                    'st_PayInfo.szTxnMsg = Common.C_INVALID_INPUT_2906
                    'st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
                End If
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetOCPToken() exception: " + ex.StackTrace + " " + ex.Message  ' .Message

            'st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            'st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            'st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
            'st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bUpdateOCPToken
    ' Function Type:	Boolean.  True if update successful else false
    ' Parameter:		st_PayInfo, st_HostInfo
    ' Description:		Updates OCP Token table
    ' History:			15 Aug 2014
    '********************************************************************************************
    Public Function bUpdateOCPToken(ByVal st_PayInfo As Common.STPayInfo, ByVal sz_EncCardExp As String, Optional ByVal i_Action As Integer = 0) As Boolean

        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRowsAffected As Integer = 0

        Try
            bReturn = False
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("Token_UpdateOCP", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_Token", SqlDbType.VarChar, 50)
            objParam = objCommand.Parameters.Add("@sz_CustEmail", SqlDbType.VarChar, 60)
            objParam = objCommand.Parameters.Add("@sz_EncCardExp", SqlDbType.VarChar, 100)
            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@i_Action", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_Token").Value = st_PayInfo.szToken

            If (3 = st_PayInfo.iAllowOCP) Then 'Added 12 Feb 2018, Tik FX
                objCommand.Parameters("@sz_CustEmail").Value = st_PayInfo.szCustPhone
            Else
                objCommand.Parameters("@sz_CustEmail").Value = st_PayInfo.szCustEmail
            End If

            objCommand.Parameters("@sz_EncCardExp").Value = sz_EncCardExp
            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@i_Action").Value = i_Action

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()
            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True
            Else
                bReturn = False

                st_PayInfo.szErrDesc = "bUpdateOCPToken() error: Failed to update CardExp"

                'st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                'st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                'st_PayInfo.iTxnState = Common.TXN_STATE.FAILED
                'st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bUpdateOCPToken() exception: " + ex.ToString

            'st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            'st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            'st_PayInfo.iTxnState = Common.TXN_STATE.FAILED
            'st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bVerifyFraud
    ' Function Type:	Boolean.  True if get verify fraud successfully else false
    ' Parameter:		st_PayInfo, st_HostInfo
    ' Description:		Verify credit card fraud
    ' History:			14 Dec 2011
    '********************************************************************************************
    Public Function bVerifyFraud(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost) As Boolean

        Dim bReturn As Boolean = True
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRetCode As Integer
        Dim szRetDesc As String = ""

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spVerifyFraud", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_GatewayTxnID", SqlDbType.VarChar, 40)
            objParam = objCommand.Parameters.Add("@sz_CardPan", SqlDbType.VarChar, 20)
            objParam = objCommand.Parameters.Add("@sz_CardEnc", SqlDbType.VarChar, 50)  'Added, 11 Oct 2013

            objCommand.Parameters("@sz_GatewayTxnID").Value = st_PayInfo.szTxnID
            objCommand.Parameters("@sz_CardPan").Value = st_PayInfo.szParam2 'Modified 11 Oct 2013, removed SHA1, to be compared against white card and hot card list containing clear CardPAN
            objCommand.Parameters("@sz_CardEnc").Value = objHash.szComputeSHA1Hash(st_PayInfo.szParam2) 'Added, 11 Oct 2013, to be added into TB_CardList table

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If dr.HasRows Then
                If dr.Read() Then
                    iRetCode = Convert.ToInt32(dr("RetCode"))
                    szRetDesc = dr("RetDesc").ToString()
                End If

                If (0 = iRetCode) Then
                    bReturn = True
                Else
                    st_PayInfo.szErrDesc = szRetDesc

                    st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                    st_PayInfo.iTxnStatus = iRetCode 'Commented  12 Oct 2013, Common.TXN_STATUS.INVALID_INPUT
                    st_PayInfo.iTxnState = Common.TXN_STATE.DECLINE_FDS 'Commented  12 Oct 2013, Common.TXN_STATE.ABORT
                    st_PayInfo.szTxnMsg = szRetDesc 'Commented  12 Oct 2013, Common.C_FAILED_HOTCARD_9

                    bReturn = False
                End If
            End If
            dr.Close()

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "bVerifyFraud() - Verify Fraud Process Done")

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bVerifyFraud() exception: " + ex.StackTrace + " " + ex.Message

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bInsertNotifyStatus
    ' Function Type:	Boolean.  True if insertion successful else false
    ' Parameter:		stPayInfo, iShopperEmailNotifyStatus, iMerchantEmailNotifyStatus
    ' Description:		Inserts transaction notification status
    ' History:			21 Nov 2014 - ENS
    '********************************************************************************************
    Public Function bInsertNotifyStatus(ByVal st_PayInfo As Common.STPayInfo, ByVal i_ShopperEmailNotifyStatus As Integer, ByVal i_MerchantEmailNotifyStatus As Integer) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim iRowsAffected As Integer = 0

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spInsNotifyStatus", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_GatewayTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@i_ShopperEmailNotifyStatus", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_MerchantEmailNotifyStatus", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_GatewayTxnID").Value = st_PayInfo.szTxnID
            objCommand.Parameters("@i_ShopperEmailNotifyStatus").Value = i_ShopperEmailNotifyStatus
            objCommand.Parameters("@i_MerchantEmailNotifyStatus").Value = i_MerchantEmailNotifyStatus

            objConn.Open()
            iRowsAffected = objCommand.ExecuteNonQuery()

            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True
            Else
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "bInsertNotifyStatus() error: Failed to insert transaction notification status. GatewayTxnID(" + st_PayInfo.szTxnID + ")")
            End If

        Catch ex As Exception
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
            DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "bInsertNotifyStatus() exception: " + ex.ToString + " > " + "GatewayTxnID(" + st_PayInfo.szTxnID + ")")

        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bCheckNotifyStatus
    ' Function Type:	Boolean.  True if unique else false
    ' Parameter:		st_PayInfo
    ' Description:		Check status of transaction email notification
    ' History:			24 Nov 2014
    '********************************************************************************************
    Public Function bCheckNotifyStatus(ByVal st_PayInfo As Common.STPayInfo, ByRef i_ShopperENS As Integer, ByRef i_MerchantENS As Integer) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spCheckNotifyStatus", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_GatewayTxnID", SqlDbType.VarChar, 30)

            objCommand.Parameters("@sz_GatewayTxnID").Value = st_PayInfo.szTxnID

            objConn.Open()
            dr = objCommand.ExecuteReader()

            If (dr.Read()) Then 'record found in Email Notification Status table, return value from table
                i_ShopperENS = Convert.ToInt32(dr("ShopperEmailNotifyStatus"))
                i_MerchantENS = Convert.ToInt32(dr("MerchantEmailNotifyStatus"))
            End If

            dr.Close()

        Catch ex As Exception
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
            DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "bCheckNotifyStatus() exception: " + ex.ToString)

        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bInsertCallBackStatus
    ' Function Type:	Boolean.  True if insertion successful else false
    ' Parameter:		stPayInfo, iCallBackStatus
    ' Description:		Inserts callback status - S2Scallback
    ' History:			12 Feb 2015
    '********************************************************************************************
    Public Function bInsertCallBackStatus(ByVal st_PayInfo As Common.STPayInfo, ByVal i_CallBackStatus As Integer) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim iRowsAffected As Integer = 0

        Try
            If (Common.TXN_STATUS.TXN_PENDING = st_PayInfo.iMerchantTxnStatus) Then 'Added 15 Dec 2017, FPX B2B
                Return True
            End If

            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spInsNotifyStatus", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_GatewayTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@i_CallBackStatus", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_GatewayTxnID").Value = st_PayInfo.szTxnID
            objCommand.Parameters("@i_CallBackStatus").Value = i_CallBackStatus

            objConn.Open()
            iRowsAffected = objCommand.ExecuteNonQuery()

            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True
            Else
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "bInsertCallBackStatus() error: Failed to insert transaction notification status. GatewayTxnID(" + st_PayInfo.szTxnID + ")")
            End If

        Catch ex As Exception
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
            DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "bInsertCallBackStatus() exception: " + ex.ToString + " > " + "GatewayTxnID(" + st_PayInfo.szTxnID + ")")

        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bCheckCallBackStatus
    ' Function Type:	Boolean.  True if unique else false
    ' Parameter:		st_PayInfo
    ' Description:		Check status of transaction callback - S2Scallback
    ' History:			9 Feb 2015
    '********************************************************************************************
    Public Function bCheckCallBackStatus(ByVal st_PayInfo As Common.STPayInfo, ByRef i_CallBackStatus As Integer) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spCheckNotifyStatus", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_GatewayTxnID", SqlDbType.VarChar, 30)

            objCommand.Parameters("@sz_GatewayTxnID").Value = st_PayInfo.szTxnID

            objConn.Open()
            dr = objCommand.ExecuteReader()

            If (dr.Read()) Then 'record found in table, return value from table
                i_CallBackStatus = Convert.ToInt32(dr("CallBackStatus"))
            End If

            dr.Close()

        Catch ex As Exception
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
            DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "bCheckCallBackStatus() exception: " + ex.ToString)

        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	iUpdatePendingTxnReq
    ' Function Type:	Integer.  0 if get res status 30 and update to 3 else 1, exception -1
    ' Parameter:		st_PayInfo, st_HostInfo
    ' Description:		Get txn status = 30 update it to 3
    ' History:			28 Feb 2011
    '********************************************************************************************
    Public Function iUpdatePendingTxnReq(ByRef st_PayInfo As Common.STPayInfo, ByVal st_HostInfo As Common.STHost) As Integer

        'Dim iReturn As Integer = 0
        'Dim objConn As SqlConnection = Nothing
        'Dim objCommand As SqlCommand = Nothing
        'Dim objParam As SqlParameter = Nothing
        'Dim dr As SqlDataReader = Nothing
        'Dim iRetCode As Integer = -1

        'Try
        '    objConn = New SqlConnection(szDBConnStr)
        '    objCommand = New SqlCommand("spUpdatePendingTxnReq", objConn)
        '    objCommand.CommandType = CommandType.StoredProcedure

        '    objParam = objCommand.Parameters.Add("@sz_GatewayTxnID", SqlDbType.VarChar)

        '    objCommand.Parameters("@sz_GatewayTxnID").Value = st_PayInfo.szTxnID

        '    objConn.Open()
        '    objCommand.Connection = objConn

        '    dr = objCommand.ExecuteReader()

        '    While dr.Read()
        '        iRetCode = Convert.ToInt32(dr("RetCode"))
        '    End While

        '    If iRetCode = 0 Then
        '        objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), _
        '                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
        '                                        " > Get TxnStatus 30 and mod to 3")
        '        iReturn = 0 'Get TxnStatus 30 and mod to 3
        '    ElseIf iRetCode = 1 Then
        '        objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), _
        '                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
        '                                        " > Failed to get TxnStatus 30")
        '        iReturn = 1 'Failed to get TxnStatus 30
        '    ElseIf iRetCode = 2 Then
        '        objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), _
        '                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
        '                                        " > Failed to get TxnStatus 30")
        '        iReturn = 2 'Finalized Txn
        '    End If

        '    dr.Close()

        'Catch ex As Exception
        '    iReturn = -1

        '    objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), _
        '                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
        '                                        "iUpdatePendingTxnReq() exception: " + ex.StackTrace + " " + ex.Message)

        '    st_PayInfo.szErrDesc = "iUpdatePendingTxnReq() exception: " + ex.StackTrace + " " + ex.Message  ' .Message

        '    st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
        '    st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
        '    st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
        '    st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        'Finally
        '    If Not objParam Is Nothing Then objParam = Nothing

        '    If Not objCommand Is Nothing Then
        '        objCommand.Dispose()
        '        objCommand = Nothing
        '    End If

        '    If Not objConn Is Nothing Then
        '        objConn.Close()
        '        objConn.Dispose()
        '        objConn = Nothing
        '    End If
        'End Try

        'Return iReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProcOB
    ' Function Name:	bGetBNBSaleReqData
    ' Function Type:	Boolean.  True if successfully retrieved record or else false
    ' Parameter:		
    ' Description:		Get info based on Buy Now Button Token
    ' History:			23 Mar 2015 Created by 
    '********************************************************************************************
    Public Function bGetBNBSaleReqData(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim iRowsAffected As Integer = 0
        Dim dr As SqlDataReader
        Dim szHashMethod As String = ""

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("Token_GetBNBbyToken", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_Token", SqlDbType.VarChar, 50)
            objParam = objCommand.Parameters.Add("@sz_CustEmail", SqlDbType.VarChar, 50)    'Added, 15 Jun 2016

            objCommand.Parameters("@sz_Token").Value = st_PayInfo.szToken
            objCommand.Parameters("@sz_CustEmail").Value = st_PayInfo.szCustEmail       'Added, 15 Jun 2016

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If dr.Read() Then
                st_PayInfo.szMachineID = dr("MerchantID").ToString()
                st_PayInfo.szMerchantID = st_PayInfo.szMachineID
                st_PayInfo.szMerchantOrdDesc = dr("OrderDesc").ToString()
                st_PayInfo.szMerchantOrdID = dr("OrderNumber").ToString()
                st_PayInfo.szCurrencyCode = dr("CurrCode").ToString()
                st_PayInfo.szTxnAmount = Trim(dr("Amount").ToString())
                st_PayInfo.szTxnDateTime = dr("DateCreated").ToString()
                st_PayInfo.szCustName = dr("CustName").ToString()    ' Added 8 Feb 2017, to display CustName in notification email

                bReturn = True

                'Added, 9 Apr 2015
                'RecStatus - Token_BNB
                'ExpiryDate, EmailPymtStatus - Token_EmailPayment
                If ("0" = Trim(dr("RecStatus").ToString())) Then
                    st_PayInfo.szErrDesc = "Buy Now Button/Email Payment order disabled"
                    bReturn = False
                ElseIf (Trim(dr("ExpiryDate").ToString()) <> "-1") Then
                    If (Convert.ToInt32(System.DateTime.Now.ToString("yyyyMMdd")) > Convert.ToInt32(Trim(dr("ExpiryDate").ToString()))) Then
                        st_PayInfo.szErrDesc = "Email payment expired"
                        bReturn = False
                    ElseIf ("0" = Trim(dr("EmailPymtStatus").ToString())) Then
                        st_PayInfo.szErrDesc = "Email payment disabled"
                        bReturn = False
                    End If
                End If
            Else
                'Added, 9 Apr 2015
                st_PayInfo.szErrDesc = "Buy Now Button/Email Payment order not found"
                bReturn = False
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetBNBSaleReqData() exception: " + ex.StackTrace + ex.Message + " > GatewayTxnID(" + st_PayInfo.szTxnID + ")"

            st_PayInfo.iQueryStatus = 2
            st_PayInfo.szQueryMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bGetPymtMethod
    ' Function Type:	
    ' Parameter:		
    ' Description:		
    ' History:			23 Mar 2015 Created by 
    '********************************************************************************************
    Public Function bGetPymtMethod(ByRef st_PayInfo As Common.STPayInfo) As String
        Dim bReturn As Boolean = True
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim iRowsAffected As Integer = 0
        Dim obj As Object = Nothing

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetPymtMethod", objConn)
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.Parameters.Add("@PymtMethodCode", SqlDbType.Char, 3).Value = st_PayInfo.szPymtMethod
            objCommand.Connection = objConn

            objConn.Open()
            obj = objCommand.ExecuteScalar()

        Catch ex As Exception
            bReturn = False
            st_PayInfo.szErrDesc = "bGetPymtMethod() exception: " + ex.StackTrace + " " + ex.Message

            'Commented  23 Mar 2015
            'st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            'st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            'st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            'st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
        Finally

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return obj.ToString()
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bUpdateCaptureTxnStatus
    ' Function Type:	Boolean.  True if update successful else false
    ' Parameter:		st_PayInfo
    ' Description:		Updates transaction status in either Request/Response table
    ' History:			23 Apr 2015. Auth&Capture
    '********************************************************************************************
    Public Function bUpdateCaptureTxnStatus(ByRef st_PayInfo As Common.STPayInfo) As Boolean

        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRowsAffected As Integer = 0

        Try
            bReturn = False

            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spUpdateCaptTxnStatus", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@i_PKID", SqlDbType.BigInt)
            'objParam = objCommand.Parameters.Add("@i_ReqOrRes", SqlDbType.Int)   'Modified  8 May 2015. Auth&Capture 2. Capture always will be in Response table
            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_MerchantTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_TxnType", SqlDbType.VarChar, 7)
            objParam = objCommand.Parameters.Add("@i_TxnStatus", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_TxnState", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@d_TxnAmount", SqlDbType.Decimal, 18)
            objParam = objCommand.Parameters.Add("@i_MerchantTxnStatus", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@sz_RespMesg", SqlDbType.VarChar, 255)
            objParam = objCommand.Parameters.Add("@sz_BankRefNo", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_Param1", SqlDbType.VarChar, 100)    'Added  4 Aug 2016. Auth&Capture - GPay2.
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@i_PKID").Value = st_PayInfo.iPKID
            'objCommand.Parameters("@i_ReqOrRes").Value = st_PayInfo.iQueryStatus   'Modified  8 May 2015. Auth&Capture 2
            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@sz_MerchantTxnID").Value = st_PayInfo.szMerchantTxnID
            objCommand.Parameters("@sz_TxnType").Value = st_PayInfo.szTxnType
            objCommand.Parameters("@i_TxnStatus").Value = st_PayInfo.iTxnStatus
            objCommand.Parameters("@i_TxnState").Value = st_PayInfo.iTxnState
            objCommand.Parameters("@d_TxnAmount").Value = st_PayInfo.szTxnAmount
            objCommand.Parameters("@i_MerchantTxnStatus").Value = st_PayInfo.iMerchantTxnStatus
            objCommand.Parameters("@sz_RespMesg").Value = st_PayInfo.szTxnMsg

            If (st_PayInfo.szHostTxnID <> "" And st_PayInfo.szHostTxnID <> "-") Then
                objCommand.Parameters("@sz_BankRefNo").Value = st_PayInfo.szHostTxnID
            Else
                objCommand.Parameters("@sz_BankRefNo").Value = st_PayInfo.szAuthCode
            End If
            'Added  4 Aug 2016. Auth&Capture - GPay2. Store Capture vpc_TransactionNo
            objCommand.Parameters("@sz_Param1").Value = st_PayInfo.szHostTxnID

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()
            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True
            Else
                bReturn = False

                st_PayInfo.szErrDesc = "bUpdateCaptureTxnStatus() error: Failed to update TxnStatus(" + st_PayInfo.iTxnStatus.ToString() + ") TxnState(" + st_PayInfo.iTxnState.ToString() + ") ReqOrRes(" + st_PayInfo.iQueryStatus.ToString() + ") PKID(" + st_PayInfo.iPKID.ToString() + ") GatewayTxnID(" + st_PayInfo.szTxnID + ")"

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_INT_ERR
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
                st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bUpdateCaptureTxnStatus() exception: " + ex.ToString + " > " + "TxnStatus(" + st_PayInfo.iTxnStatus.ToString() + ") TxnState(" + st_PayInfo.iTxnState.ToString() + ") ReqOrRes(" + st_PayInfo.iQueryStatus.ToString() + ") PKID (" + st_PayInfo.iPKID.ToString() + ") GatewayTxnID(" + st_PayInfo.szTxnID + ")"

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_INT_ERR
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bGetReqTxnType
    ' Function Type:	Boolean.  True if unique else false
    ' Parameter:		
    ' Description:		Get transaction type of the transaction
    ' History:			29 Apr 2015
    '********************************************************************************************
    Public Function bGetReqTxnType(ByVal st_PayInfo As Common.STPayInfo, ByRef sz_ReqTxnType As String) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim iRowsAffected As Integer = 0
        Dim dr As SqlDataReader = Nothing

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetReqTxnType", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_GatewayTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_GatewayTxnID").Value = st_PayInfo.szTxnID

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If dr.Read() Then
                sz_ReqTxnType = dr("TxnType").ToString()
            End If

            bReturn = True

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetReqTxnType() exception: " + ex.StackTrace + ex.Message + " > GatewayTxnID(" + st_PayInfo.szTxnID + ")"

            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bGetTxnRefPymtMethod --Change function name from bGetCaptureTxnRef to bGetTxnRefPymtMethod
    ' Function Type:	Boolean.  True if get successfully else false
    ' Parameter:		sz_PymtMethod
    ' Description:		Get payment method of transaction (Capture and Refund)
    ' History:			30 Apr 2015. Auth&Capture.      Modified  25 May 2015. Refund.
    '********************************************************************************************
    Public Function bGetTxnRefPymtMethod(ByRef sz_PymtMethod As String, ByRef i_Exist As Integer, ByRef st_PayInfo As Common.STPayInfo) As Boolean

        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRowsAffected As Integer = 0

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetTxnRefPymtMethod", objConn)     'Modify  25 May 2015. Change store proc name from spGetCaptureTxnRef to spGetTxnRefPymtMethod
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_MerchantTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_CurrencyCode", SqlDbType.Char, 3)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@sz_MerchantTxnID").Value = st_PayInfo.szMerchantTxnID
            objCommand.Parameters("@sz_CurrencyCode").Value = st_PayInfo.szCurrencyCode

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If objCommand.Parameters("@i_Return").Value = 0 Then

                While dr.Read()
                    bReturn = True
                    i_Exist = 1     'Added, 6 Sept 2013
                    sz_PymtMethod = dr("PymtMethod").ToString()
                    st_PayInfo.szMerchantOrdID = dr("OrderNumber").ToString() 'Added, 10 Sept 2013
                    st_PayInfo.iDuration = Convert.ToInt32(dr("Duration"))  'Added, 15 Oct 2014
                    dr.Read()
                End While

                If (False = bReturn) Then
                    i_Exist = 0     'Added, 7 Oct 2013, if not exists, somehow still getting @i_Return as 0

                    bReturn = True  'Modified 7 Oct 2013

                    st_PayInfo.szErrDesc = "bGetTxnRefPymtMethod(): Failed to get Payment Method 1"

                    st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                    st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                    st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
                End If
            Else
                i_Exist = 0     'Added, 6 Sept 2013

                bReturn = True  'Modified 6 Sept 2013

                st_PayInfo.szErrDesc = "bGetTxnRefPymtMethod(): Failed to get Payment Method 2"

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False
            st_PayInfo.szErrDesc = "bGetTxnRefPymtMethod() exception: " + ex.StackTrace + " " + ex.Message

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bGetTxnResult2
    ' Function Type:	Boolean.  True if successfully retrieved record or else false
    ' Parameter:		
    ' Description:		Get transaction result from database without Amount condition
    ' History:			30 Apr 2015. Auth&Capture.      Modified  25 May 2015. Refund
    '********************************************************************************************
    Public Function bGetTxnResult2(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim iRowsAffected As Integer = 0
        Dim dr As SqlDataReader = Nothing
        Dim szHashMethod As String = ""

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetTxnResult2", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_MerchantTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_CurrencyCode", SqlDbType.Char, 3)     'Added, 18 Aug 2013
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@sz_MerchantTxnID").Value = st_PayInfo.szMerchantTxnID

            If (st_PayInfo.szCurrencyCode <> "") Then
                objCommand.Parameters("@sz_CurrencyCode").Value = st_PayInfo.szCurrencyCode             'Added, 18 Aug 2013
            Else
                objCommand.Parameters("@sz_CurrencyCode").Value = st_PayInfo.szRCurrencyCode            'Added, 5 Sept 2013
            End If

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If dr.Read() Then

                st_PayInfo.iQueryStatus = Convert.ToInt32(dr("RetCode"))    'dr.GetInt32(1).ToString
                st_PayInfo.szQueryMsg = dr("RetDesc").ToString()            'dr.GetString(2)

                'Assign initial value
                st_PayInfo.iPKID = -1               ' Added, 5 Sept 2013
                st_PayInfo.szTxnType = ""
                st_PayInfo.szMerchantOrdID = ""
                st_PayInfo.szTxnAmount = ""
                st_PayInfo.szBaseTxnAmount = ""     ' Added on 3 Aug 2009, base currency booking confirmation enhancement
                st_PayInfo.szCurrencyCode = ""
                st_PayInfo.szBaseCurrencyCode = ""  ' Added on 3 Aug 2009, base currency booking confirmation enhancement
                st_PayInfo.szLanguageCode = ""
                st_PayInfo.szIssuingBank = ""
                'st_PayInfo.szSessionID = ""        ' Commented on 14 Apr 2010, do not overwrite Request Session ID
                st_PayInfo.szParam1 = ""
                st_PayInfo.szParam2 = ""
                st_PayInfo.szParam3 = ""
                st_PayInfo.szParam4 = ""
                st_PayInfo.szParam5 = ""
                st_PayInfo.szParam6 = ""
                st_PayInfo.szParam7 = ""
                st_PayInfo.szParam8 = ""
                st_PayInfo.szParam9 = ""
                st_PayInfo.szParam10 = ""
                st_PayInfo.szParam11 = ""
                st_PayInfo.szParam12 = ""
                st_PayInfo.szParam13 = ""
                st_PayInfo.szParam14 = ""
                st_PayInfo.szParam15 = ""
                st_PayInfo.szTxnID = ""
                st_PayInfo.iTxnState = -1
                st_PayInfo.iTxnStatus = -1
                st_PayInfo.iMerchantTxnStatus = -1
                st_PayInfo.iAction = -1
                st_PayInfo.iDuration = -1
                st_PayInfo.iOSRet = 99
                st_PayInfo.iErrSet = -1
                st_PayInfo.iErrNum = -1
                st_PayInfo.szHostTxnID = ""
                st_PayInfo.szTxnMsg = ""
                st_PayInfo.szHashValue = ""
                st_PayInfo.szGatewayID = ""
                st_PayInfo.szCardPAN = ""
                st_PayInfo.szMachineID = ""
                st_PayInfo.szMerchantReturnURL = ""
                st_PayInfo.szMerchantSupportURL = ""
                st_PayInfo.szMerchantCallbackURL = ""   'Added, 3 Dec 2014
                st_PayInfo.szTxnDateTime = ""
                st_HostInfo.szOSPymtCode = ""
                st_HostInfo.iHostID = -1
                st_PayInfo.szCustEmail = ""     'Added  25 Nov 2014 - ENS
                st_PayInfo.szMerchantOrdDesc = ""     'Added, 2 Dec 2014
                st_PayInfo.szCustName = ""      'Added, 3 Dec 2014
                st_PayInfo.szTotRefundAmt = 0   'Added  15 Sept 2015. REFUND
                st_PayInfo.szCardType = ""      'Added  22 Feb 2018. MBBAmex has 2 acct number, CardType is one of the criteria to search for correct acct number - Masterpass

                ' 1 - Transaction exists in Response table, has reached its final state
                '-1 - Transaction only exists in Request table, still has not reached final state, maybe under processing
                If ((1 = st_PayInfo.iQueryStatus) Or (-1 = st_PayInfo.iQueryStatus)) Then
                    st_PayInfo.iPKID = Convert.ToInt32(dr("PKID"))                              ' Added, 5 Sept 2013
                    st_PayInfo.szTxnType = dr("TxnType").ToString()
                    st_PayInfo.szMerchantOrdID = dr("OrderNumber").ToString()
                    st_PayInfo.szTxnAmount = dr("TxnAmount").ToString()
                    st_PayInfo.szBaseTxnAmount = dr("BaseTxnAmount").ToString()                 ' Added on 3 Aug 2009, base currency booking confirmation enhancement
                    st_PayInfo.szCurrencyCode = dr("CurrencyCode").ToString()
                    st_PayInfo.szBaseCurrencyCode = Trim(dr("BaseCurrencyCode").ToString())     ' Added on 3 Aug 2009, base currency booking confirmation enhancement
                    st_PayInfo.szLanguageCode = dr("LanguageCode").ToString()
                    st_PayInfo.szIssuingBank = dr("IssuingBank").ToString()
                    'Added on 14 Apr 2010, if Request's SessionID is not empty, do not overwrite it.
                    If ("" = st_PayInfo.szMerchantSessionID) Then                               'Modified 16 Jul 2014, SessionID to MerchantSessionID
                        st_PayInfo.szMerchantSessionID = dr("SessionID").ToString()
                    End If
                    st_PayInfo.szParam1 = dr("Param1").ToString()

                    If (dr("Param2").ToString() <> "") Then
                        st_PayInfo.szParam2 = objHash.szDecryptAES(dr("Param2").ToString(), Common.C_3DESAPPKEY)
                        st_PayInfo.szClearCardPAN = st_PayInfo.szParam2 'Added, 17 May 2016, for OCP token creation
                    Else
                        st_PayInfo.szParam2 = dr("Param2").ToString()
                    End If

                    If (dr("Param3").ToString() <> "") Then
                        st_PayInfo.szParam3 = objHash.szDecryptAES(dr("Param3").ToString(), Common.C_3DESAPPKEY)
                    Else
                        st_PayInfo.szParam3 = dr("Param3").ToString()
                    End If

                    st_PayInfo.szParam4 = dr("Param4").ToString()
                    st_PayInfo.szParam5 = dr("Param5").ToString()
                    st_PayInfo.szParam6 = dr("Param6").ToString()
                    st_PayInfo.szParam7 = dr("Param7").ToString()
                    st_PayInfo.szParam8 = dr("Param8").ToString()
                    st_PayInfo.szParam9 = dr("Param9").ToString()
                    st_PayInfo.szParam10 = dr("Param10").ToString()
                    st_PayInfo.szParam11 = dr("Param11").ToString()
                    st_PayInfo.szParam12 = dr("Param12").ToString()
                    st_PayInfo.szParam13 = dr("Param13").ToString()
                    st_PayInfo.szParam14 = dr("Param14").ToString()
                    st_PayInfo.szParam15 = dr("Param15").ToString()
                    st_PayInfo.szTxnID = dr("GatewayTxnID").ToString()
                    st_PayInfo.szTxnDateTime = dr("DateCreated").ToString()

                    st_PayInfo.iTxnState = Convert.ToInt32(dr("TxnState"))
                    st_PayInfo.iTxnStatus = Convert.ToInt32(dr("TxnStatus"))
                    st_PayInfo.iMerchantTxnStatus = Convert.ToInt32(dr("MerchantTxnStatus"))
                    st_PayInfo.iAction = Convert.ToInt32(dr("Action"))
                    st_PayInfo.iDuration = Convert.ToInt32(dr("Duration"))
                    st_PayInfo.iOSRet = Convert.ToInt32(dr("OSRet"))
                    st_PayInfo.iErrSet = Convert.ToInt32(dr("ErrSet"))
                    st_PayInfo.iErrNum = Convert.ToInt32(dr("ErrNum"))

                    st_PayInfo.szRespCode = dr("BankRespCode").ToString()
                    st_PayInfo.szHostTxnID = dr("BankRefNo").ToString()
                    st_PayInfo.szAuthCode = dr("AuthCode").ToString()           'Added, 5 Sept 2013
                    st_PayInfo.szTxnMsg = dr("RespMesg").ToString()

                    szHashMethod = dr("HashMethod").ToString()

                    st_PayInfo.szGatewayID = dr("GatewayID").ToString()
                    st_PayInfo.szCardPAN = dr("CardPAN").ToString()
                    If st_PayInfo.szCardPAN.Length > 10 Then
                        st_PayInfo.szMaskedCardPAN = st_PayInfo.szCardPAN.Substring(0, 6) + "".PadRight(st_PayInfo.szCardPAN.Length - 6 - 4, "X") + st_PayInfo.szCardPAN.Substring(st_PayInfo.szCardPAN.Length - 4, 4)
                    Else
                        st_PayInfo.szMaskedCardPAN = "".PadRight(st_PayInfo.szCardPAN.Length, "X")
                    End If
                    st_PayInfo.szCardType = dr("CardType").ToString()       'Added  22 Feb 2018.
                    st_PayInfo.szMachineID = dr("MachineID").ToString()
                    st_PayInfo.szMerchantReturnURL = dr("MerchantReturnURL").ToString()
                    st_PayInfo.szMerchantSupportURL = dr("MerchantSupportURL").ToString()

                    st_HostInfo.szOSPymtCode = dr("OSPymtCode").ToString()
                    st_HostInfo.iHostID = Convert.ToInt32(dr("HostID"))
                    st_PayInfo.szCustEmail = dr("CustEmail").ToString()     'Added  25 Nov 2014 - ENS
                    st_PayInfo.szMerchantOrdDesc = dr("OrderDesc").ToString()     'Added, 2 Dec 2014, to show OrderDesc on email notification when Query
                    st_PayInfo.szCustName = dr("CustName").ToString()       'Added, 3 Dec 2014
                    st_PayInfo.szMerchantCallbackURL = dr("MerchantCallbackURL").ToString() 'Added, 3 Dec 2014
                    st_PayInfo.szTotRefundAmt = dr("TotRefundAmt").ToString()               'Added  15 Sept 2015. REFUND
                    'Added 20 Oct 2017, Conversion Currency
                    If ("" = st_PayInfo.szFXCurrencyCode) Then
                        If (Trim(dr("FXCurrencyCode").ToString()) <> "") Then
                            st_PayInfo.szFXCurrencyCode = dr("FXCurrencyCode").ToString()
                        End If

                        If (Trim(dr("FXTxnAmount").ToString()) <> "") Then
                            st_PayInfo.szFXTxnAmount = dr("FXTxnAmount").ToString()
                        End If

                        If (Trim(dr("FXCurrOriRate").ToString()) <> "") Then
                            st_PayInfo.szFXCurrencyOriRate = dr("FXCurrOriRate").ToString()
                        End If
                    End If
                End If

                If (1 = st_PayInfo.iQueryStatus) Then
                    ' If Query request's Hash Method is the same as Hash Method for txn existed in Response table,
                    ' then just use the HashValue in Response table or else have to recalculate the response hash based
                    ' on Query request's Hash Method
                    If (st_PayInfo.szHashMethod.ToLower() = szHashMethod.ToLower()) Then
                        'Added checking of whether HashValue is empty or not on 3 Apr 2010
                        If (dr("HashValue").ToString() <> "") Then
                            st_PayInfo.szHashValue = dr("HashValue").ToString()
                        End If
                    End If
                ElseIf (-1 = st_PayInfo.iQueryStatus) Then
                    'Added on 20 Mar 2010 to cater for ability to retrieve the original request's hash method, especially after 2nd entry page
                    st_PayInfo.szHashMethod = dr("HashMethod").ToString()
                End If

                bReturn = True
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "spGetTxnResult2() exception: " + ex.StackTrace + ex.Message + " > GatewayTxnID(" + st_PayInfo.szTxnID + ")"

            st_PayInfo.iQueryStatus = 2
            st_PayInfo.szQueryMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bGetTxnResult3
    ' Function Type:	Boolean.  True if successfully retrieved record or else false
    ' Parameter:		
    ' Description:		Get transaction PKID from TB_PayReq table
    ' History:			2 Nov 2015. UOBMOTO
    '********************************************************************************************
    Public Function bGetTxnResult3(ByRef st_PayInfo As Common.STPayInfo) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim iRowsAffected As Integer = 0
        Dim dr As SqlDataReader = Nothing

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetTxnResult3", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_GatewayTxnID", SqlDbType.VarChar, 30)
            objCommand.Parameters("@sz_GatewayTxnID").Value = st_PayInfo.szTxnID

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If dr.Read() Then
                'Assign initial value
                st_PayInfo.iPKID = -1

                If (1 = Convert.ToInt32(dr("RetCode"))) Then
                    st_PayInfo.iPKID = Convert.ToInt32(dr("PKID"))
                    bReturn = True
                Else
                    st_PayInfo.szErrDesc = dr("RetDesc").ToString()
                    bReturn = False
                End If
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "spGetTxnResult3() exception: " + ex.StackTrace + ex.Message + " > GatewayTxnID(" + st_PayInfo.szTxnID + ")"
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function
    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bGetPromotion
    ' Function Type:	Boolean.  True if get promotion information successfully else false
    ' Parameter:		st_PayInfo
    ' Description:		Get merchant promotion information
    ' History:			20 Nov 2015
    '********************************************************************************************
    Public Function bGetPromotion(ByRef st_PayInfo As Common.STPayInfo) As Boolean
        Dim bReturn As Boolean = True
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("uspGetPromotion", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID

            'Added 29 June 2016, cater special product promotion
            If Not String.IsNullOrEmpty(st_PayInfo.szPromoCode) Then
                objParam = objCommand.Parameters.Add("@sz_PromoCode", SqlDbType.VarChar, 10)
                objCommand.Parameters("@sz_PromoCode").Value = st_PayInfo.szPromoCode
            End If

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()
            If (dr.HasRows) Then
                st_PayInfo.lPromoList = New List(Of String)
                While dr.Read()
                    st_PayInfo.lPromoList.Add(dr("PromoDesc").ToString())
                End While
            End If

            dr.Close()
        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetPromotion() exception: " + ex.StackTrace + ex.Message + " > MerchantID(" + st_PayInfo.szMerchantID + ")"
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try
        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bCheckNInsRefundTxn
    ' Function Type:	Boolean.  True if successfully retrieved record or else false
    ' Parameter:		
    ' Description:		Check transaction details meet refund requirement before send to bank host
    ' History:			13 July 2015. REFUND
    ' Modified:         22 Sept 2015. REFUND
    '********************************************************************************************
    Public Function bCheckNInsRefundTxn(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost) As Integer
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRet As Integer = -1

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spCheckNInsRefundTxn", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@i_HostID", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_MTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@d_RefundAmt", SqlDbType.Decimal, 18)
            objParam = objCommand.Parameters.Add("@d_OriAmt", SqlDbType.Decimal, 18)

            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@i_HostID").Value = st_HostInfo.iHostID
            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@sz_MTxnID").Value = st_PayInfo.szMerchantTxnID
            objCommand.Parameters("@d_RefundAmt").Value = Convert.ToDecimal(st_PayInfo.szRTxnAmount)
            objCommand.Parameters("@d_OriAmt").Value = Convert.ToDecimal(st_PayInfo.szTxnAmountOri)

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            'Return value can be -1 (refund failed), 0 (new refund), 1 (retry refund query needed)
            While dr.Read()
                iRet = Convert.ToInt32(dr("RetCode"))
                If (-1 = iRet) Then
                    st_PayInfo.szErrDesc = dr("RetDesc").ToString()
                End If
            End While

            dr.Close()

        Catch ex As Exception
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "bCheckNInsRefundTxn() exception: " + ex.ToString)

        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return iRet

    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bCalculatePromotion
    ' Function Type:	Boolean.  True if successfully else false
    ' Parameter:		st_PayInfo
    ' Description:		Calcualte promotion trx amount
    ' History:			23 Nov 2015
    '********************************************************************************************
    Public Function bCalculatePromotion(ByRef st_PayInfo As Common.STPayInfo) As Boolean
        Dim bReturn As Boolean = True
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim bGetPromo As Boolean = False
        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("uspGetPromotion", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@i_Action", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@sz_CardType", SqlDbType.VarChar, 2)
            objParam = objCommand.Parameters.Add("@sz_CardPAN", SqlDbType.VarChar, 20)
            objParam = objCommand.Parameters.Add("@sz_CurrencyID", SqlDbType.VarChar, 20)
            objParam = objCommand.Parameters.Add("@sz_TxnAmount", SqlDbType.VarChar, 18)

            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@i_Action").Value = 1
            objCommand.Parameters("@sz_CardType").Value = st_PayInfo.szCardType
            objCommand.Parameters("@sz_CardPAN").Value = st_PayInfo.szCardPAN
            objCommand.Parameters("@sz_CurrencyID").Value = st_PayInfo.szCurrencyCode
            objCommand.Parameters("@sz_TxnAmount").Value = st_PayInfo.szTxnAmount

            If Not String.IsNullOrEmpty(st_PayInfo.szPromoCode) Then
                objParam = objCommand.Parameters.Add("@sz_PromoCode", SqlDbType.VarChar, 10)

                objCommand.Parameters("@sz_PromoCode").Value = st_PayInfo.szPromoCode
            End If

            objConn.Open()
            objCommand.Connection = objConn

            Using adapter As New SqlDataAdapter(objCommand)
                Using resultSet As New DataSet("uspGetPromotion")
                    adapter.Fill(resultSet)
                    If (resultSet.Tables.Count > 0) Then
                        For Each row As DataRow In resultSet.Tables(0).Rows
                            Dim szConditionCheck As String = Nothing
                            szConditionCheck = row("ConditionCheck").ToString()
                            If szConditionCheck = "" Then
                                Select Case st_PayInfo.szPMEntry
                                    Case "1"
                                        st_PayInfo.iPromoID = Convert.ToInt32(row("PROMOID"))
                                        st_PayInfo.szPromoOriAmt = row("AFTPROMOAMT").ToString()
                                        st_PayInfo.szPromotion = row("PROMODESC").ToString()
                                    Case "2"
                                        st_PayInfo.szPromoOriAmt = st_PayInfo.szTxnAmount
                                        st_PayInfo.iPromoID = Convert.ToInt32(row("PROMOID"))
                                        st_PayInfo.szPromoCode = row("PROMOCODE").ToString()
                                        st_PayInfo.szTxnAmount = row("AFTPROMOAMT").ToString()
                                End Select
                                st_PayInfo.iPromoActionID = Convert.ToInt32(row("ACTIONID"))
                                st_PayInfo.iPromoConfirmID = Convert.ToInt32(row("CONFIRMID"))
                                bGetPromo = True
                                Exit For
                            Else
                                Dim table As New System.Data.DataTable()
                                table = resultSet.Tables(0).Clone
                                table.Columns.Add("ExpressionCheck", GetType(Boolean))
                                table.Columns("ExpressionCheck").Expression = szConditionCheck
                                table.ImportRow(row)
                                Dim result As [Boolean] = DirectCast(table.Rows(0)("ExpressionCheck"), [Boolean])
                                table.Clear()
                                If result Then
                                    Select Case st_PayInfo.szPMEntry
                                        Case "1"
                                            st_PayInfo.iPromoID = Convert.ToInt32(row("PROMOID"))
                                            st_PayInfo.szPromoOriAmt = row("AFTPROMOAMT").ToString()
                                            st_PayInfo.szPromotion = row("PROMODESC").ToString()
                                        Case "2"
                                            st_PayInfo.szPromoOriAmt = st_PayInfo.szTxnAmount
                                            st_PayInfo.iPromoID = Convert.ToInt32(row("PROMOID"))
                                            st_PayInfo.szPromoCode = row("PROMOCODE").ToString()
                                            st_PayInfo.szTxnAmount = row("AFTPROMOAMT").ToString()
                                    End Select
                                    st_PayInfo.iPromoActionID = Convert.ToInt32(row("ACTIONID"))
                                    st_PayInfo.iPromoConfirmID = Convert.ToInt32(row("CONFIRMID"))
                                    bGetPromo = True
                                    Exit For
                                End If
                                st_PayInfo.iPromoActionID = Convert.ToInt32(row("ACTIONID"))
                                st_PayInfo.iPromoConfirmID = Convert.ToInt32(row("CONFIRMID"))
                            End If
                        Next row

                        If Not bGetPromo Then
                            st_PayInfo.szPromoOriAmt = ""
                            st_PayInfo.iPromoID = -1
                            st_PayInfo.szPromotion = ""
                        End If
                    Else
                        st_PayInfo.szPromoOriAmt = ""
                        st_PayInfo.iPromoID = -1
                        st_PayInfo.szPromotion = ""
                    End If
                End Using
            End Using
        Catch ex As Exception
            bReturn = False
            st_PayInfo.szErrDesc = "bCalculatePromotion() exception: " + ex.StackTrace + ex.Message + " > MerchantID(" + st_PayInfo.szMerchantID + ")"
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try
        Return bReturn
    End Function
    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bUpdateRefundTxnStatus
    ' Function Type:	Boolean.  True if update successful else false
    ' Parameter:		st_PayInfo, st_HostInfo
    ' Description:		Updates TB_PayRes_Multi table
    ' History:			15 Jul 2015
    '********************************************************************************************
    Public Function bUpdateRefundTxnStatus(ByRef st_PayInfo As Common.STPayInfo) As Boolean

        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRowsAffected As Integer = 0

        Dim iShopperEmailNotifyStatus As Integer = -1       'Added  28 Nov 2014 - ENS
        Dim iMerchantEmailNotifyStatus As Integer = -1      'Added  28 Nov 2014 - ENS

        Try
            bReturn = False
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spUpdateRefundTxnStatus", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@i_PayResID", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_MerchantTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@d_RefundAmt", SqlDbType.Decimal, 18)
            objParam = objCommand.Parameters.Add("@d_OriAmt", SqlDbType.Decimal, 18)
            objParam = objCommand.Parameters.Add("@i_TxnStatus", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@sz_BankRefNo", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@i_PayResID").Value = st_PayInfo.iPKID
            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@sz_MerchantTxnID").Value = st_PayInfo.szMerchantTxnID
            objCommand.Parameters("@d_RefundAmt").Value = st_PayInfo.szRTxnAmount
            objCommand.Parameters("@d_OriAmt").Value = st_PayInfo.szTxnAmountOri
            objCommand.Parameters("@i_TxnStatus").Value = st_PayInfo.iTxnStatus
            objCommand.Parameters("@sz_BankRefNo").Value = st_PayInfo.szHostTxnID

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()
            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True
            Else
                bReturn = False

                st_PayInfo.szErrDesc = "bUpdateRefundTxnStatus() error: TxnStatus(" + st_PayInfo.iTxnStatus.ToString() + ") TxnPKID(" + st_PayInfo.iPKID.ToString() + ") for MerchantPymtID(" + st_PayInfo.szMerchantTxnID + ")"

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.iTxnState = Common.TXN_STATE.FAILED
                st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bUpdateRefundTxnStatus() exception: " + ex.ToString + " > TxnStatus(" + st_PayInfo.iTxnStatus.ToString() + ") TxnPKID(" + st_PayInfo.iPKID.ToString() + ") for GatewayTxnID(" + st_PayInfo.szMerchantTxnID + ")"

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.FAILED
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        ''Added  24 Nov 2014, ENS-Email Notification
        'If (Common.TXN_STATUS.TXN_SUCCESS = st_PayInfo.iMerchantTxnStatus And st_PayInfo.iPymtNotificationEmail > 0) Then
        '    bCheckNotifyStatus(st_PayInfo, iShopperEmailNotifyStatus, iMerchantEmailNotifyStatus)

        '    iSendEmailNotification(st_PayInfo, st_HostInfo, iShopperEmailNotifyStatus, iMerchantEmailNotifyStatus)
        'End If

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bGetInstlRouting
    ' Function Type:	Boolean.  True if get routing information successfully else false
    ' Parameter:		st_PayInfo
    ' Description:		Get installment routing bank information
    ' History:			25 Nov 2015
    '********************************************************************************************
    Public Function bGetInstlRouting(ByRef st_PayInfo As Common.STPayInfo) As Boolean

        Dim bReturn As Boolean = True
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetInstlRouting", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_CardPAN", SqlDbType.VarChar, 20)
            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_CurrencyID", SqlDbType.VarChar, 3)
            objParam = objCommand.Parameters.Add("@i_3DFlag", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_HIID", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@sz_TermMonth", SqlDbType.VarChar, 10)

            objCommand.Parameters("@sz_CardPAN").Value = st_PayInfo.szParam2
            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@sz_CurrencyID").Value = st_PayInfo.szCurrencyCode

            If (1 = st_PayInfo.iSelfMPI) Then
                If (st_PayInfo.sz3DFlag = "" Or st_PayInfo.szECI = "00" Or st_PayInfo.szECI = "07") Then
                    objCommand.Parameters("@i_3DFlag").Value = 0 'N3D
                End If
            Else
                If (2 = st_PayInfo.i3DAccept) Then  'Non-3D only
                    objCommand.Parameters("@i_3DFlag").Value = 0 'N3D
                End If
            End If

            objCommand.Parameters("@i_HIID").Value = st_PayInfo.iInstallment
            objCommand.Parameters("@sz_TermMonth").Value = st_PayInfo.szInstallMthTerm

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If dr.HasRows Then

                While dr.Read()
                    st_PayInfo.szIssuingBank = dr("HostName").ToString()
                    st_PayInfo.szInstallPlan = dr("PlanCode").ToString()
                    If ("" = st_PayInfo.szInstallMthTerm) Then
                        st_PayInfo.szInstallMthTerm = dr("MonthTerms").ToString()
                    End If
                    If (0 = st_PayInfo.iInstallment) Then
                        st_PayInfo.iInstallment = Convert.ToInt32(dr("HIID"))
                    End If

                End While

                If "" <> st_PayInfo.szIssuingBank Then
                    bReturn = True
                Else
                    bReturn = False

                    st_PayInfo.szErrDesc = "bGetInstlRouting() error: Failed to get installment bank host, could be not OnUs Card"

                    st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.INVALID_HOST
                    st_PayInfo.szTxnMsg = "Failed to get installment bank host, could be not OnUs Card"  'Common.C_INVALID_HOST_902
                    st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
                End If
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetInstlRouting() exception: " + ex.StackTrace + " " + ex.Message

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bGetExtResTxn
    ' Function Type:	Boolean.  True if successfully retrieved record or else false
    ' Parameter:		
    ' Description:		Get transaction result from External's response table based on GatewayTxnID
    ' History:			28 Mar 2010
    '********************************************************************************************
    Public Function bGetExtResTxn(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost, ByVal sz_SPName As String) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing

        Try

            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand(sz_SPName, objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_GatewayTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_GatewayTxnID").Value = st_PayInfo.szTxnID

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If dr.Read() Then
                st_PayInfo.iQueryStatus = Convert.ToInt32(dr("RetCode"))
                st_PayInfo.szQueryMsg = dr("RetDesc").ToString()
                st_PayInfo.szTxnMsg = ""

                ' 1 - Transaction exists in Response table, has reached its final state
                If (1 = st_PayInfo.iQueryStatus) Then

                    st_PayInfo.iQueryStatus = -1
                    st_PayInfo.szTxnAmount = dr("TxnAmount").ToString()
                    st_PayInfo.szRespCode = dr("BankRespCode").ToString()
                    st_PayInfo.szHostTxnID = dr("BankRefNo").ToString()
                    st_PayInfo.szAuthCode = dr("AuthCode").ToString()
                    st_PayInfo.szTxnMsg = dr("RespMesg").ToString()
                End If

                bReturn = True
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetExtResTxn() exception: " + ex.StackTrace + ex.Message + " > GatewayTxnID(" + st_PayInfo.szTxnID + ")"

            st_PayInfo.iQueryStatus = 2
            st_PayInfo.szQueryMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function


    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bMapBankRespCode
    ' Function Type:	Boolean.  True if successfully retrieved record or else false
    ' Parameter:		
    ' Description:		Get response desc from bank response table based on HostID and bank response code
    ' History:			05 May 2016
    '********************************************************************************************

    Public Function bMapBankRespCode(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost, ByVal sz_BankRespCode As String) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing

        Try

            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("uspGetRespDescVRespCode", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@i_HostID", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@sz_BankRespCode", SqlDbType.VarChar, 10)

            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@i_HostID").Value = st_HostInfo.iHostID
            objCommand.Parameters("@sz_BankRespCode").Value = sz_BankRespCode


            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If dr.Read() Then
                If objCommand.Parameters("@i_Return").Value = 0 Then
                    bReturn = True
                    st_PayInfo.szBankRespMsg = dr("BankRespMsg").ToString()
                Else
                    bReturn = False

                    st_PayInfo.szErrDesc = "bMapBankRespCode() error: TxnStatus(" + st_PayInfo.iTxnStatus.ToString() + ") TxnPKID(" + st_PayInfo.iPKID.ToString() + ") for MerchantPymtID(" + st_PayInfo.szMerchantTxnID + ") BankRespCode(" + sz_BankRespCode + ")"
                    st_PayInfo.szBankRespMsg = sz_BankRespCode
                End If
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bMapBankRespCode() exception: " + ex.StackTrace + ex.Message + " > GatewayTxnID(" + st_PayInfo.szTxnID + ")"

            st_PayInfo.iQueryStatus = 2
            st_PayInfo.szQueryMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bSettle
    ' Function Type:	Boolean.  True if successfully retrieved record or else false
    ' Parameter:		
    ' Description:		Check again DB wheather settle amount and txn count is correct.
    ' History:			05 May 2016
    '********************************************************************************************

    Public Function bSettle(ByRef st_PayInfo As Common.STPayInfo) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing

        Try

            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("uspCheckCanSettle", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@i_TAID", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@d_SettleAmt", SqlDbType.Decimal, 18)
            objParam = objCommand.Parameters.Add("@i_SettleTxnCount", SqlDbType.Int)

            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@i_TAID").Value = st_PayInfo.szSettleTAID
            objCommand.Parameters("@d_SettleAmt").Value = st_PayInfo.szSettleAmount
            objCommand.Parameters("@i_SettleTxnCount").Value = st_PayInfo.szSettleCount

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If dr.Read() Then
                If Convert.ToInt32(dr("RetCode")) = 0 Then      'Modified 10 May 2016, use dr("RetCode") instead of checking the return value which is not accurate (=NOTHING)
                    bReturn = True

                ElseIf Convert.ToInt32(dr("RetCode")) = 2 Then  'Settlement pending
                    bReturn = False

                    st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_PENDING
                    st_PayInfo.szErrDesc = dr("RetDesc").ToString()
                Else
                    bReturn = False

                    st_PayInfo.szErrDesc = dr("RetDesc").ToString()
                End If

                'Overwrites merchant settle amount and settle txn count with Gateway's
                st_PayInfo.szSettleAmount = dr("GatewaySettleAmt").ToString()
                st_PayInfo.szSettleCount = dr("GatewaySettleTxnCount").ToString()
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bSettle() exception: " + ex.StackTrace + ex.Message + " > MerchantID(" + st_PayInfo.szMerchantID + ") SettleTAID(" + st_PayInfo.szSettleTAID + ")"

            st_PayInfo.iQueryStatus = 2
            st_PayInfo.szQueryMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function
    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bGetSettleHost
    ' Function Type:	Boolean.  True if successfully retrieved record or else false
    ' Parameter:		
    ' Description:		Get Settlement host info from MPG.
    ' History:			05 May 2016
    '********************************************************************************************

    Public Function bGetSettleHost(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost) As Boolean
        Dim bReturn As Boolean = True
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRowsAffected As Integer = 0

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("uspGetSettleHostInfo", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@i_TAID", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@i_TAID").Value = st_PayInfo.szSettleTAID

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True

                'HostID, HostName, PaymentTemplate, NeedReplyAcknowledgement, Require2ndEntry, AllowReversal,
                'ChannelID, [Timeout], TxnStatusActionID, HostReplyMethod, OSPymtCode, DateActivated, DateDeactivated, RecStatus, [Desc], PayURL, CfgFile
                While dr.Read()
                    st_HostInfo.iHostID = Convert.ToInt32(dr("HostID"))                                     ' HostID
                    st_HostInfo.szPaymentTemplate = dr("PaymentTemplate").ToString()                        ' PaymentTemplate
                    st_HostInfo.szSecondEntryTemplate = dr("SecondEntryTemplate").ToString()                ' SecondEntryTemplate
                    st_HostInfo.szMesgTemplate = dr("MesgTemplate").ToString()                              ' Message Template - Added on 27 Oct 2009
                    st_HostInfo.iRequire2ndEntry = Convert.ToInt32(dr("Require2ndEntry"))                   ' Require2ndEntry
                    st_HostInfo.szHostTemplate = dr("CfgFile").ToString()                                   ' CfgFile
                    st_HostInfo.iNeedReplyAcknowledgement = Convert.ToInt32(dr("NeedReplyAcknowledgement")) ' NeedReplyAcknowledgement
                    st_HostInfo.iNeedRedirectOTP = Convert.ToInt32(dr("NeedRedirectOTP"))                   ' NeedRedirectOTP - Added on 21 Mar 2010
                    st_HostInfo.iTxnStatusActionID = Convert.ToInt32(dr("TxnStatusActionID"))               ' TxnStatusActionID
                    st_HostInfo.iHostReplyMethod = Convert.ToInt32(dr("HostReplyMethod"))                   ' HostReplyMethod
                    st_HostInfo.szOSPymtCode = dr("OSPymtCode").ToString()                                  ' OSPymtCode
                    st_HostInfo.iGatewayTxnIDFormat = Convert.ToInt32(dr("GatewayTxnIDFormat"))             ' GatewayTxnIDFormat
                    st_HostInfo.iTimeOut = Convert.ToInt32(dr("Timeout"))                                   ' Timeout - if txn more than this timeout but host's query resp still returned pending, then fail the txn
                    st_HostInfo.iChannelTimeOut = Convert.ToInt32(dr("ChannelTimeout"))                     ' Timeout - Added, 18 Nov 2015, MOTO, cater for receive timeout between PG and MPG
                    st_HostInfo.szHashMethod = dr("HashMethod").ToString()                                  ' Hash Method
                    st_HostInfo.szReturnIPAddresses = dr("ReturnIPAddresses").ToString()                    ' Return IP Addresses
                    st_HostInfo.iQueryFlag = Convert.ToInt32(dr("QueryFlag"))
                    st_HostInfo.iIsActive = Convert.ToInt32(dr("IsActive"))
                    st_HostInfo.iAllowQuery = Convert.ToInt32(dr("AllowQuery"))                             ' AllowQuery - Added  11 Jun 2014. Firefly FFCTB
                    st_HostInfo.iAllowReversal = Convert.ToInt32(dr("AllowReversal"))                       ' AllowReversal - Added on 10 Apr 2010
                    st_HostInfo.szDesc = dr("Desc").ToString()                                              ' Host Description - Added on 9 Dec 2013
                    st_HostInfo.szProtocol = dr("Protocol").ToString()                                      ' Security Protocol, e.g. SSL3, TLS, TLS1.1, TLS1.2 - Added, 21 Jun 2015
                    st_HostInfo.szURL = dr("PayURL").ToString()                             ' Pay URL                    
                    st_HostInfo.iPortNumber = Convert.ToInt32(dr("PayPort"))                ' Pay Port
                    If ("" <> dr("SecretKey").ToString()) Then  ' Added checking on 1 Oct 2010
                        st_HostInfo.iRunningNoUniquePerDay = Convert.ToInt32(dr("SecretKey"))               ' Boolean for whether Trace Number/Running Number is unique per day, reuse SecretKey which had been moved from Host to Terminals table - Added on 18 Sept 2010
                    End If
                    If ("" <> dr("InitVector").ToString()) Then  ' Added checking on 1 Oct 2010
                        st_HostInfo.iNoOfRunningNo = Convert.ToInt32(dr("InitVector"))                      ' Number of digits for Trace Number/Running Number, reuse InitVector which had been moved from Host to Terminals table - Added on 18 Sept 2010
                    End If

                    st_HostInfo.iLogRes = Convert.ToInt32(dr("LogRes"))     'Added  23 Oct 2014. Firefly FFAMBANK
                    st_HostInfo.szQueryURL = dr("QueryURL").ToString()      'Added19 Jan 2016, for BancNet
                    st_PayInfo.szIssuingBank = dr("HostName").ToString()    'For settlement use
                    dr.Read()
                End While
            Else
                st_PayInfo.szErrDesc = "bGetSettleHost() error: Failed to get Host Info" + " > IssuingBank(" + st_PayInfo.szIssuingBank + ")"

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR 'Common.ERROR_CODE.E_INVALID_MID
                st_PayInfo.szTxnMsg = Common.C_INVALID_HOST_902
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT

                bReturn = False
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetSettleHost() exception: " + ex.StackTrace + " " + ex.Message  ' .Message

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR 'Common.ERROR_CODE.E_INVALID_MID
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bGetFDS_FraudScoreAction
    ' Function Type:	Boolean.  True if get FraudScoreAction information successfully else false
    ' Parameter:		st_PayInfo
    ' Description:		Get FraudScoreAction
    ' History:			20 Apr 2016  fraudwall
    '********************************************************************************************
    Public Function bGetFDS_FraudScoreAction(ByRef st_PayInfo As Common.STPayInfo) As Boolean
        Dim bReturn As Boolean = True
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRowsAffected As Integer = 0

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("fds_GetFraudScoreAction", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.Char, 30)
            objParam.Value = st_PayInfo.szMerchantID
            objParam = objCommand.Parameters.Add("@i_RespCode", SqlDbType.Int)
            objParam.Value = st_PayInfo.iFDSRespCode

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If dr.Read() Then
                st_PayInfo.iFDSActionID = Convert.ToInt32(dr("ActionID"))              ' ActionID     
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetFDS_FraudScoreAction() exception: " + ex.StackTrace + " " + ex.Message  ' .Message

            'st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bInsExtFraudResults
    ' Function Type:	Boolean.  True if insertion successful else false
    ' Parameter:		stPayInfo
    ' Description:		Inserts External Fraud Results
    ' History:			25 Apr 2016,  fraudwall
    '********************************************************************************************
    Public Function bInsExtFraudResults(ByVal st_PayInfo As Common.STPayInfo) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim iRowsAffected As Integer = 0
        Dim szTxnDay As String = ""

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("fds_InsExtFraudResults", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.Char)
            objParam = objCommand.Parameters.Add("@sz_MerchantTxnID", SqlDbType.VarChar)
            objParam = objCommand.Parameters.Add("@sz_GatewayTxnID", SqlDbType.VarChar)
            objParam = objCommand.Parameters.Add("@i_ResponseCode", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_ActionID", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_TxnDay", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@sz_DateCreated", SqlDbType.VarChar)
            objParam = objCommand.Parameters.Add("@sz_ResponseMsg", SqlDbType.VarChar)        'Added  23 Sep 2016
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@sz_MerchantTxnID").Value = st_PayInfo.szMerchantTxnID
            objCommand.Parameters("@sz_GatewayTxnID").Value = st_PayInfo.szTxnID
            objCommand.Parameters("@i_ResponseCode").Value = st_PayInfo.iFDSRespCode
            objCommand.Parameters("@i_ActionID").Value = st_PayInfo.iFDSActionID
            objCommand.Parameters("@sz_ResponseMsg").Value = st_PayInfo.szFDSRespMsg            'Added  23 Sep 2016
            szTxnDay = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
            objCommand.Parameters("@i_TxnDay").Value = Convert.ToInt32(Left(szTxnDay, 10).Replace("-", ""))

            st_PayInfo.szTxnDateTime = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
            objCommand.Parameters("@sz_DateCreated").Value = st_PayInfo.szTxnDateTime

            objConn.Open()
            iRowsAffected = objCommand.ExecuteNonQuery()

            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True
            Else
                st_PayInfo.szErrDesc = "bInsExtFraudResults() error: Failed to insert External Fraud Results."
            End If

        Catch ex As Exception

            st_PayInfo.szErrDesc = "bInsExtFraudResults() exception: " + ex.Message + " " + ex.StackTrace  ' .Message

        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bCheckNInsCaptureTxn
    ' Function Type:	Boolean.  True if successfully retrieved record or else false
    ' Parameter:		
    ' Description:		Check transaction details meet capture requirement before send to bank host
    ' History:			22 Aut 2016. Auth&Capture
    '********************************************************************************************
    Public Function bCheckNInsCaptureTxn(ByRef st_PayInfo As Common.STPayInfo, ByRef st_HostInfo As Common.STHost) As Integer
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRet As Integer = -1

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spCheckNInsCaptureTxn", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@i_HostID", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_MTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@d_CaptureAmt", SqlDbType.Decimal, 18)

            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@i_HostID").Value = st_HostInfo.iHostID
            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@sz_MTxnID").Value = st_PayInfo.szMerchantTxnID
            objCommand.Parameters("@d_CaptureAmt").Value = Convert.ToDecimal(st_PayInfo.szCTxnAmount)

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            'Return value can be -1 (capture failed), 0 (new capture), 1 (Query before Capture)
            While dr.Read()
                iRet = Convert.ToInt32(dr("RetCode"))
                If (-1 = iRet) Then
                    st_PayInfo.szErrDesc = dr("RetDesc").ToString()
                End If
            End While

            dr.Close()

        Catch ex As Exception
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "bCheckNInsCaptureTxn() exception: " + ex.ToString)

        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return iRet

    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bGetExtFDSEmailTemplate
    ' Function Type:	Boolean.
    ' Parameter:		st_PayInfo
    ' Description:		Get ExtFDSEmailTemplate
    ' History:			29 Sept 2016 FW
    '********************************************************************************************
    Public Function bGetExtFDSEmailTemplate(ByRef st_PayInfo As Common.STPayInfo) As Boolean
        Dim bReturn As Boolean = True
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRowsAffected As Integer = 0

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("fds_GetExtFDSEmailTemplate", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_EmailTemplateID", SqlDbType.VarChar, 50)
            objParam.Value = st_PayInfo.szFDSDeclineRule

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If dr.Read() Then
                st_PayInfo.szFDSEmailTemplate = Convert.ToString(dr("EmailTemplate"))
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetExtFDSEmailTemplate() exception: " + ex.StackTrace + " " + ex.Message

            'st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bCheckExpCurrency
    ' Function Type:	Boolean.  True if successfully retrieved record or else false
    ' Parameter:		
    ' Description:		Get currency exponent from CL_CurrencyCode table based on Currency Code
    ' History:			09 Jan 2017
    '********************************************************************************************

    Public Function bCheckExpCurrency(ByVal sz_CurrencyCode As String, ByVal i_HostID As Integer) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing

        Try

            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spCheckCurrencyIsExponent", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_CurrencyCode", SqlDbType.VarChar, 3)
            objParam = objCommand.Parameters.Add("@i_HostID", SqlDbType.Int)

            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_CurrencyCode").Value = sz_CurrencyCode
            objCommand.Parameters("@i_HostID").Value = i_HostID

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True
            Else
                bReturn = False
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bInsertUpdateMPToken
    ' Function Type:	Boolean.  True if insertion successful else false
    ' Parameter:		stPayInfo, 
    ' Description:		Inserts or Update MasterPass Long Access Token
    ' History:			9 Nov 2016
    '********************************************************************************************
    Public Function bInsertUpdateMPToken(ByVal st_PayInfo As Common.STPayInfo, Optional ByVal iRemove As Integer = 0) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim iRowsAffected As Integer = 0

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("Token_InsertUpdateMP", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_ConsumerUserID", SqlDbType.VarChar, 50)
            objParam = objCommand.Parameters.Add("@sz_LongAccessToken", SqlDbType.VarChar, 60)
            objParam = objCommand.Parameters.Add("@i_Remove", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@sz_ConsumerUserID").Value = st_PayInfo.szToken
            objCommand.Parameters("@sz_LongAccessToken").Value = st_PayInfo.szLongAccessToken
            objCommand.Parameters("@i_Remove").Value = iRemove

            objConn.Open()
            iRowsAffected = objCommand.ExecuteNonQuery()

            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True
            Else
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "bInsertUpdateMPToken() error: Failed to insert/update MasterPass LAT token. GatewayTxnID(" + st_PayInfo.szTxnID + ")")
            End If

        Catch ex As Exception
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
            DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "bInsertUpdateMPToken() exception: " + ex.ToString + " > " + "GatewayTxnID(" + st_PayInfo.szTxnID + ")")

        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bGetMPEToken
    ' Function Type:	Boolean.  True if get token successfully else false
    ' Parameter:		st_PayInfo
    ' Description:		Get MPE Token
    ' History:			11 Nov 2016
    '********************************************************************************************
    Public Function bGetMPEToken(ByRef st_PayInfo As Common.STPayInfo) As Boolean

        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRowsAffected As Integer = 0
        Dim iRetCode As Integer = 0
        Dim szRetDesc As String = ""

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("Token_GetMPE", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_Token", SqlDbType.VarChar, 60)    'ConsumerUserID
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@sz_Token").Value = st_PayInfo.szToken

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If dr.Read() Then
                iRetCode = Convert.ToInt32(dr("RetCode"))
                szRetDesc = dr("RetDesc").ToString()

                If iRetCode = 1 Then    'Found long access token / ReqToken and PairingToken
                    bReturn = True
                    st_PayInfo.szLongAccessToken = dr("LongAccessToken").ToString()
                    'Modified  20 Feb 2017 - Masterpass-2
                    'st_PayInfo.szReqToken = dr("ReqToken").ToString()
                    'st_PayInfo.szPairingReqToken = dr("PairingReqToken").ToString()
                Else
                    bReturn = False
                End If
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False
            st_PayInfo.szErrDesc = "bGetMEPToken() exception: " + ex.StackTrace + " " + ex.Message

        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bGetPromotionAction
    ' Function Type:	Boolean.  True if get promotion information successfully else false
    ' Parameter:		st_PayInfo
    ' Description:		Get merchant promotion action
    ' History:			21 Mar 2017
    '********************************************************************************************
    Public Function bGetPromotionAction(ByRef st_PayInfo As Common.STPayInfo) As Boolean
        Dim bReturn As Boolean = True
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("uspGetPromotion", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objParam = objCommand.Parameters.Add("@sz_PromoCode", SqlDbType.VarChar, 10)
            objCommand.Parameters("@sz_PromoCode").Value = st_PayInfo.szPromoCode
            objParam = objCommand.Parameters.Add("@i_Action", SqlDbType.Int)
            objCommand.Parameters("@i_Action").Value = 2

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()
            If (dr.HasRows) Then
                While dr.Read()
                    st_PayInfo.iPromoActionID = Convert.ToInt32(dr("ACTIONID"))
                End While
            End If

            dr.Close()
        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetPromotionAction() exception: " + ex.StackTrace + ex.Message + " > MerchantID(" + st_PayInfo.szMerchantID + ")"
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try
        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bGetTokenRunningNo
    ' Function Type:	Boolean.  True if get running number successfully else false
    ' Parameter:		st_PayInfo, int
    ' Description:		Get running number based on GatewayTxnIDFormat
    ' History:			30 Oct 2009
    '********************************************************************************************
    Public Function bGetTokenRunningNo(ByRef st_PayInfo As Common.STPayInfo, ByVal i_TokenFormat As Integer) As Boolean

        Dim bReturn As Boolean = True
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRowsAffected As Integer = 0

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetTxnIDRunningNo", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@i_GatewayTxnIDFormat", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@i_GatewayTxnIDFormat").Value = i_TokenFormat

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True

                While dr.Read()
                    st_PayInfo.szRunningNo = dr("RunningNo").ToString()
                    st_PayInfo.iTxnIDMaxLen = Convert.ToInt32(dr("MaxLen"))
                    dr.Read()
                End While
            Else
                bReturn = False

                st_PayInfo.szErrDesc = "bGetTokenRunningNo() error: Invalid GatewayTxnIDFormat."

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INVALID_INPUT
                st_PayInfo.szTxnMsg = Common.C_INVALID_INPUT_2906
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetTokenRunningNo() exception: " + ex.StackTrace + " " + ex.Message  ' .Message

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bCheckFWCurrency
    ' Function Type:	Boolean.  True if get currency is successfully else false
    ' Parameter:		sz_CurrencyCode, st_PayInfo
    ' Description:		To check the currency supported by FrauWall_v2 or not in ISO4217 format
    ' History:			Added  13 Jul 2017
    '********************************************************************************************
    Public Function bCheckFWCurrency(ByVal sz_CurrencyCode As String, ByRef st_PayInfo As Common.STPayInfo) As String
        Dim iReturn As Integer = 0
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spCheckFWCurrency", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_CurrencyCode", SqlDbType.VarChar, 5)
            objCommand.Parameters("@sz_CurrencyCode").Value = sz_CurrencyCode

            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objConn.Open()
            objCommand.Connection = objConn

            objCommand.ExecuteScalar()

            If objCommand.Parameters("@i_Return").Value = 0 Then
                iReturn = 1
            Else
                st_PayInfo.szErrDesc = "bCheckFWCurrency() error: unsupported currency by Fraudwall" + " > CurrencyCode(" + sz_CurrencyCode + ")"
                iReturn = 0
            End If

        Catch ex As Exception
            st_PayInfo.szErrDesc = "bCheckFWCurrency() exception: " + ex.StackTrace + " " + ex.Message + " > CurrencyCode(" + sz_CurrencyCode + ")"
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return iReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	iSOPNewToken
    ' Function Type:	String
    ' Parameter:		st_PayInfo
    ' Description:		returns PKID if new record is added successfully and st_PayInfo.iSOPTokenID = iReturn
    ' History:			Added , 24 May 2017
    '********************************************************************************************
    Public Function iSOPNewToken(ByRef st_PayInfo As Common.STPayInfo) As String
        Dim iReturn As Integer = 0
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRowsAffected As Integer = 0
        Dim preturn As System.Data.SqlClient.SqlParameter = Nothing
        Dim poutput As System.Data.SqlClient.SqlParameter = Nothing

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("sop_newToken", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_Token", SqlDbType.VarChar, 64)
            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_CustIP", SqlDbType.VarChar, 20)
            objParam = objCommand.Parameters.Add("@sz_CardPan", SqlDbType.VarChar, 100)
            objParam = objCommand.Parameters.Add("@sz_CardHolder", SqlDbType.VarChar, 50)
            objParam = objCommand.Parameters.Add("@sz_CardExp", SqlDbType.VarChar, 100)
            objParam = objCommand.Parameters.Add("@sz_CVV", SqlDbType.VarChar, 100)

            poutput = objCommand.CreateParameter()
            poutput.ParameterName = "@i_PKID"
            poutput.DbType = SqlDbType.BigInt
            poutput.Size = 19
            poutput.Direction = ParameterDirection.Output
            objCommand.Parameters.Add(poutput)

            preturn = objCommand.CreateParameter()
            preturn.ParameterName = "@return_value"
            preturn.DbType = SqlDbType.Int
            preturn.Direction = ParameterDirection.ReturnValue
            objCommand.Parameters.Add(preturn)

            objCommand.Parameters("@sz_Token").Value = Convert.ToString(st_PayInfo.szToken)
            objCommand.Parameters("@sz_MerchantID").Value = Convert.ToString(st_PayInfo.szMerchantID)
            objCommand.Parameters("@sz_CustIP").Value = Convert.ToString(st_PayInfo.szCustIP)
            objCommand.Parameters("@sz_CardPan").Value = Convert.ToString(objHash.szEncryptAES(st_PayInfo.szCardPAN, Common.C_3DESAPPKEY))
            objCommand.Parameters("@sz_CardHolder").Value = Convert.ToString(st_PayInfo.szCardHolder)
            objCommand.Parameters("@sz_CardExp").Value = Convert.ToString(objHash.szEncryptAES(st_PayInfo.szCardExp, Common.C_3DESAPPKEY))
            objCommand.Parameters("@sz_CVV").Value = Convert.ToString(objHash.szEncryptAES(st_PayInfo.szCVV2, Common.C_3DESAPPKEY))

            objConn.Open()
            objCommand.Connection = objConn
            objCommand.ExecuteNonQuery() ' To get return value of stored procedure

            If preturn.Value = 0 Then
                iReturn = poutput.Value
            End If

        Catch ex As Exception
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
            "iSOPNewToken() exception: " + ex.StackTrace + " " + ex.Message)

        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return iReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bSOPUpdateTxnStatus
    ' Function Type:	Boolean
    ' Parameter:		st_PayInfo, Common.SOP_STATUS
    ' Description:		returns True if update is successful
    ' History:			Added , 30 May 2017
    '********************************************************************************************
    Public Function bSOPUpdateTxnStatus(ByRef st_PayInfo As Common.STPayInfo, ByVal status As Common.SOP_STATUS) As String
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRowsAffected As Integer = 0
        Dim preturn As System.Data.SqlClient.SqlParameter = Nothing

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("sop_updateTxnStatus", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_Token", SqlDbType.VarChar, 64)
            objParam = objCommand.Parameters.Add("@i_SOP_Status", SqlDbType.Int)

            preturn = objCommand.CreateParameter()
            preturn.ParameterName = "@return_value"
            preturn.DbType = SqlDbType.Int
            preturn.Direction = ParameterDirection.ReturnValue
            objCommand.Parameters.Add(preturn)

            objCommand.Parameters("@sz_Token").Value = Convert.ToString(st_PayInfo.szToken)
            objCommand.Parameters("@i_SOP_Status").Value = Convert.ToInt32(status)

            objConn.Open()
            objCommand.Connection = objConn
            objCommand.ExecuteNonQuery() ' To get return value of stored procedure

            If preturn.Value = 0 Then
                bReturn = True
            End If

        Catch ex As Exception
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
            "bSOPUpdateTxnStatus() exception: " + ex.StackTrace + " " + ex.Message)

        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bSOPlinkTxn
    ' Function Type:	Boolean
    ' Parameter:		st_PayInfo, Common.SOP_STATUS
    ' Description:		returns True if update is successful
    ' History:			Added , 14 June 2017
    '********************************************************************************************
    Public Function bSOPlinkTxn(ByRef st_PayInfo As Common.STPayInfo, ByVal status As Common.SOP_STATUS) As String
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRowsAffected As Integer = 0
        Dim preturn As System.Data.SqlClient.SqlParameter = Nothing

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("sop_linkTxn", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_Token", SqlDbType.VarChar, 64)
            objParam = objCommand.Parameters.Add("@sz_MerchantTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@i_SOP_Status", SqlDbType.Int)

            preturn = objCommand.CreateParameter()
            preturn.ParameterName = "@return_value"
            preturn.DbType = SqlDbType.Int
            preturn.Direction = ParameterDirection.ReturnValue
            objCommand.Parameters.Add(preturn)

            objCommand.Parameters("@sz_Token").Value = Convert.ToString(st_PayInfo.szToken)
            objCommand.Parameters("@sz_MerchantTxnID").Value = Convert.ToString(st_PayInfo.szMerchantTxnID)
            objCommand.Parameters("@i_SOP_Status").Value = Convert.ToInt32(status)

            objConn.Open()
            objCommand.Connection = objConn
            objCommand.ExecuteNonQuery() ' To get return value of stored procedure

            If preturn.Value = 0 Then
                bReturn = True
            End If

        Catch ex As Exception
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
            "bSOPlinkTxn() exception: " + ex.StackTrace + " " + ex.Message)

        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	iSOPIsTokenValid
    ' Function Type:	Boolean
    ' Parameter:		st_PayInfo
    ' Description:		returns 0 if Token not found in DB
    '                   returns 1 if Token is Valid and not Expired
    '                   returns 2 if Token is Valid but Expired
    ' History:			Added , 30 May 2017
    '********************************************************************************************
    Public Function iSOPIsTokenValid(ByRef st_PayInfo As Common.STPayInfo) As String
        Dim iReturn As Integer = 0
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRowsAffected As Integer = 0
        Dim preturn As System.Data.SqlClient.SqlParameter = Nothing

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("sop_isTokenValid", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_Token", SqlDbType.VarChar, 64)
            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_CustIP", SqlDbType.VarChar, 20)

            preturn = objCommand.CreateParameter()
            preturn.ParameterName = "@return_value"
            preturn.DbType = SqlDbType.Int
            preturn.Direction = ParameterDirection.ReturnValue
            objCommand.Parameters.Add(preturn)

            objCommand.Parameters("@sz_Token").Value = st_PayInfo.szToken
            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@sz_CustIP").Value = st_PayInfo.szCustIP

            objConn.Open()
            objCommand.Connection = objConn
            objCommand.ExecuteNonQuery() ' To get return value of stored procedure
            dr = objCommand.ExecuteReader()

            If preturn.Value = 0 Then
                Dim iExpiredSec As Integer = 0
                Dim szCardPan As String = ""
                Dim szCardExp As String = ""
                Dim szCVV As String = ""
                Dim CardType = New CardTypeIdentifier()

                iReturn = 2
                If (dr.Read()) Then
                    iExpiredSec = Convert.ToInt32(dr("ExpiredSec"))
                    szCardPan = Convert.ToString(dr("CardPan"))
                    szCardExp = Convert.ToString(dr("CardExp"))
                    szCVV = Convert.ToString(dr("CVV"))
                    st_PayInfo.szCardHolder = Convert.ToString(dr("CardHolder"))

                    st_PayInfo.szCardPAN = objHash.szDecryptAES(szCardPan, Common.C_3DESAPPKEY)
                    st_PayInfo.szCardExp = objHash.szDecryptAES(szCardExp, Common.C_3DESAPPKEY)
                    st_PayInfo.szCVV2 = objHash.szDecryptAES(szCVV, Common.C_3DESAPPKEY)
                    st_PayInfo.szCardType = CardType.szCardTypeIdentifier(st_PayInfo.szCardPAN)

                    st_PayInfo.szParam2 = st_PayInfo.szCardPAN
                    st_PayInfo.szParam3 = st_PayInfo.szCardExp
                    st_PayInfo.szParam4 = st_PayInfo.szCardHolder

                    'Masked CardPAN
                    If st_PayInfo.szCardPAN.Length > 10 Then
                        st_PayInfo.szMaskedCardPAN = st_PayInfo.szCardPAN.Substring(0, 6) + "".PadRight(st_PayInfo.szCardPAN.Length - 6 - 4, "X") + st_PayInfo.szCardPAN.Substring(st_PayInfo.szCardPAN.Length - 4, 4)
                    Else
                        st_PayInfo.szMaskedCardPAN = "".PadRight(st_PayInfo.szCardPAN.Length, "X")
                    End If
                    st_PayInfo.szMaskedParam2 = st_PayInfo.szMaskedCardPAN

                    'Masked Card Expiry
                    If st_PayInfo.szParam3.Length > 10 Then
                        st_PayInfo.szMaskedParam3 = st_PayInfo.szParam3.Substring(0, 6) + "".PadRight(st_PayInfo.szParam3.Length - 6 - 4, "X") + st_PayInfo.szParam3.Substring(st_PayInfo.szParam3.Length - 4, 4)
                    Else
                        st_PayInfo.szMaskedParam3 = "".PadRight(st_PayInfo.szParam3.Length, "X")
                    End If

                    If (iExpiredSec < 0) Then
                        iReturn = 1
                    Else
                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                        "iSOPIsTokenValid() Token: " + st_PayInfo.szToken + " > Token is Expired")
                    End If

                End If

                dr.Close()
            Else
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                        "iSOPIsTokenValid() Token: " + st_PayInfo.szToken + " > Token not found in DB")
            End If

        Catch ex As Exception
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
            "iSOPIsTokenValid() exception: " + ex.StackTrace + " " + ex.Message)

        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return iReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bGetKEK
    ' Function Type:	Boolean.  True if get KEK successfully else false
    ' Parameter:		sz_KEK
    ' Description:		Retrieves Key Encryption Key (KEK)
    ' History:			3 Oct 2017 Created 
    '********************************************************************************************
    Public Function bGetKEK(ByRef st_PayInfo As Common.STPayInfo) As Boolean

        Dim bReturn As Boolean = True
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRowsAffected As Integer = 0
        Dim bSandBox As Boolean = False

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spGetKEK", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True

                While dr.Read()
                    Common.szKEK = dr("KEK").ToString()
                    dr.Read()
                End While

                'Added, 11 Oct 2017
                If (Common.szKEK <> "") Then
                    bSandBox = ConfigurationManager.AppSettings("SandboxMode")  'Wrong key will affect CC txns
                    If (True = bSandBox) Then
                        Common.C_3DESAPPKEY = objHash.szDecrypt3DEStoHex(Common.C_3DESAPPKEYENCS, Common.szKEK, Common.C_3DESAPPVECTOR)
                    Else
                        Common.C_3DESAPPKEY = objHash.szDecrypt3DEStoHex(Common.C_3DESAPPKEYENCP, Common.szKEK, Common.C_3DESAPPVECTOR)
                    End If
                End If
            Else
                st_PayInfo.szErrDesc = "bGetKEK() error: Failed to get KEK"

                st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.szTxnMsg = Common.TXN_STATUS.INTERNAL_ERROR
                st_PayInfo.iTxnState = Common.TXN_STATE.ABORT

                bReturn = False
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bGetKEK() exception: " + ex.StackTrace + " " + ex.Message

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bCheckTxnAmountMinMaxLimit
    ' Function Type:	Boolean.  True if TxnAmt between MinMaxLimit else false
    ' Parameter:		st_PayInfo
    ' Description:		Check if the transaction amount is within a certain amount of payment, varies based on the currency.
    ' History:			Added  26 Jan 2018
    '********************************************************************************************
    Public Function bCheckTxnAmountMinMaxLimit(ByRef st_PayInfo As Common.STPayInfo) As Boolean
        Dim bReturn As Boolean = True
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spDetermineMinMaxTxnAmt", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_TxnAmt", SqlDbType.Decimal, 18)
            objParam = objCommand.Parameters.Add("@sz_CurrencyCode", SqlDbType.VarChar, 3)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@sz_TxnAmt").Value = st_PayInfo.szTxnAmount
            objCommand.Parameters("@sz_CurrencyCode").Value = st_PayInfo.szCurrencyCode

            objConn.Open()
            objCommand.Connection = objConn

            objCommand.ExecuteScalar()

            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True
            Else
                st_PayInfo.szErrDesc = "bCheckTxnAmountMinMaxLimit()"
                bReturn = False
            End If

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "bCheckTxnAmountMinMaxLimit() exception: " + ex.StackTrace + " " + ex.Message

            st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bInsUpdPayReq3DData
    ' Function Type:	Boolean.  True if successful else false
    ' Parameter:		stPayInfo
    ' Description:		Inserts or update 3DS data
    ' History:			27 Jun 2018
    '********************************************************************************************
    Public Function bInsUpdPayReq3DData(ByVal st_PayInfo As Common.STPayInfo) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim iRowsAffected As Integer = 0

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spInsUpdPayReq3DData", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_MerchantTxnID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_XID", SqlDbType.VarChar, 40)
            objParam = objCommand.Parameters.Add("@sz_VERes", SqlDbType.Char, 1)
            objParam = objCommand.Parameters.Add("@sz_PARes", SqlDbType.Char, 1)
            objParam = objCommand.Parameters.Add("@sz_3DFlag", SqlDbType.VarChar, 5)
            objParam = objCommand.Parameters.Add("@sz_ECI", SqlDbType.VarChar, 2)
            objParam = objCommand.Parameters.Add("@sz_CAVV", SqlDbType.VarChar, 40)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@sz_MerchantTxnID").Value = st_PayInfo.szMerchantTxnID
            objCommand.Parameters("@sz_XID").Value = System.Web.HttpUtility.UrlDecode(st_PayInfo.sz3dsXID)
            objCommand.Parameters("@sz_VERes").Value = st_PayInfo.sz3dsEnrolled
            objCommand.Parameters("@sz_PARes").Value = st_PayInfo.szPayerAuth
            objCommand.Parameters("@sz_3DFlag").Value = st_PayInfo.sz3DFlag
            objCommand.Parameters("@sz_ECI").Value = st_PayInfo.szECI
            objCommand.Parameters("@sz_CAVV").Value = st_PayInfo.szCAVV

            objConn.Open()
            iRowsAffected = objCommand.ExecuteNonQuery()

            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True
            Else
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "bInsUpdPayReq3DData() error: Failed to insert or update 3DS Data. MerchantPymtID(" + st_PayInfo.szMerchantTxnID + ")")
            End If

        Catch ex As Exception
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
            DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "bInsUpdPayReq3DData() exception: " + ex.ToString + " > " + "MerchantTxnID(" + st_PayInfo.szMerchantTxnID + ")")

        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bVerify3DSStatus
    ' Function Type:	Boolean.  True if unique else false
    ' Parameter:		st_PayInfo
    ' Description:		Check status of 3DS
    ' History:			28 Jun 2018
    '********************************************************************************************
    Public Function bVerify3DSStatus(ByRef st_PayInfo As Common.STPayInfo, ByVal sz_Type As String) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader = Nothing
        Dim iRetCode As Integer
        Dim szRetDesc As String = ""

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spVerify3DSStatus", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_Res", SqlDbType.VarChar, 20)
            objParam = objCommand.Parameters.Add("@sz_Type", SqlDbType.Char, 2)

            If ("VE" = sz_Type) Then
                objCommand.Parameters("@sz_Res").Value = st_PayInfo.sz3dsEnrolled
            Else
                objCommand.Parameters("@sz_Res").Value = st_PayInfo.szECI 'st_PayInfo.szPayerAuth
            End If

            objCommand.Parameters("@sz_Type").Value = sz_Type

            objConn.Open()
            dr = objCommand.ExecuteReader()

            If (dr.Read()) Then
                iRetCode = Convert.ToInt32(dr("RetCode"))

                If (0 = iRetCode) Then
                    bReturn = True
                End If

                If (-1 = iRetCode) Then 'Failed
                    st_PayInfo.szTxnMsg = dr("RetDesc")
                End If
            End If

            dr.Close()

        Catch ex As Exception
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
            DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "spVerify3DSStatus() exception: " + ex.ToString)

        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bCheckBlockForeignCard
    ' Function Type:	Boolean.  True if block else false
    ' Parameter:		
    ' Description:		Check Block Foreign Card
    ' History:			2 May 2019
    '********************************************************************************************
    Public Function bCheckBlockForeignCard(ByRef st_PayInfo As Common.STPayInfo) As Boolean
        Dim bReturn As Boolean = False
        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        'Dim iQueryStatus As Integer = 0
        'Dim iRowsAffected As Integer = 0
        Dim dr As SqlDataReader = Nothing

        Try
            objConn = New SqlConnection(szDBConnStr)
            objCommand = New SqlCommand("spCheckBlockForeignCard", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
            objParam = objCommand.Parameters.Add("@sz_CardPAN", SqlDbType.VarChar, 20)

            objCommand.Parameters("@sz_MerchantID").Value = st_PayInfo.szMerchantID
            objCommand.Parameters("@sz_CardPAN").Value = st_PayInfo.szParam2

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If dr.Read() Then
                If (1 = Convert.ToInt32(dr("RetCode"))) Then

                    st_PayInfo.szErrDesc = Common.C_TXN_DECLINED_BY_FDS + " - " + dr("RetDesc").ToString
                    st_PayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                    st_PayInfo.iTxnStatus = Common.TXN_STATUS.FDS_BlOCKCARD
                    st_PayInfo.iTxnState = Common.TXN_STATE.DECLINE_FDS
                    st_PayInfo.szTxnMsg = Common.C_TXN_DECLINED_BY_FDS + " - " + dr("RetDesc").ToString

                    bReturn = True
                End If
            End If

            dr.Close()

        Catch ex As Exception
            bReturn = False

            st_PayInfo.szErrDesc = "spCheckBlockForeignCard() exception: " + ex.StackTrace + ex.Message + " > MerchantPymtID(" + st_PayInfo.szMerchantTxnID + ")"

            st_PayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
            st_PayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
            st_PayInfo.iTxnState = Common.TXN_STATE.ABORT
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Close()
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    Public Sub New(ByRef o_LoggerII As LoggerII.CLoggerII)
        Try
            objLoggerII = o_LoggerII

            objHash = New Hash(objLoggerII)          'Modified 11 Apr 2014, moved from above to here

            szDBConnStr = ConfigurationManager.AppSettings("PGDBString") 'Modified 11 Oct 2017, use DBString2 encrypted by non card key

            'Added, 26 Jul 2013, cater for encrypted database string
            If (InStr(szDBConnStr.ToLower(), "uid=") <= 0) Then
                szDBConnStr = objHash.szDecrypt3DES(szDBConnStr, Common.C_3DESAPPKEY2, Common.C_3DESAPPVECTOR)
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class
