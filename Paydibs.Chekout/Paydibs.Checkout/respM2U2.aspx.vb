'Imports Common
Imports LoggerII
Imports System.IO
Imports System
Imports System.Data
Imports System.Security.Cryptography
Imports System.Threading
Imports System.Text.RegularExpressions

'This class handles M2U2 responses for TxnType "PAY" only.
'Other transaction types will not be a response page like this that the bank needs to return the reply, it would be
' OB Gateway sends the request and then waits for the bank reply within the same session.
'Therefore, a response page like this is not needed for other transaction types.
Public Class respM2U2
    Inherits System.Web.UI.Page

    Public Const C_ISSUING_BANK = "M2U2"
    Public Const C_TXN_TYPE = "PAY"
    Public Const C_PYMT_METHOD = "OB"
    Public g_szHTML As String = ""
    Public objLoggerII As LoggerII.CLoggerII

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private objCommon As Common
    Private objTxnProcOB As TxnProcOB
    Private objTxnProc As TxnProc
    Private objHash As Hash
    Private objResponse As Response

    Private stHostInfo As Common.STHost
    Private stPayInfo As Common.STPayInfo

    Private szLogPath As String = ""
    Private iLogLevel As String = ""
    Private szReplyDateTime As String = ""

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim bGetHostInfo As Boolean = False

        Try
            'Get Host's reply date and time
            szReplyDateTime = System.DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss.fff")

            'Set to No Cache to make this page expired when user clicks the "back" button on browser
            Response.Cache.SetNoStore()

            If Not Page.IsPostBack Then
                'Initialization of HostInfo and PayInfo structures
                objCommon.InitHostInfo(stHostInfo)
                objCommon.InitPayInfo(stPayInfo)

                stPayInfo.szIssuingBank = C_ISSUING_BANK
                stPayInfo.szTxnType = C_TXN_TYPE
                stPayInfo.szPymtMethod = C_PYMT_METHOD
                stPayInfo.iHttpTimeoutSeconds = Convert.ToInt32(ConfigurationManager.AppSettings("HttpTimeoutSeconds"))

                'Get Host information - HostID, HostTemplate, NeedReplyAcknowledgement, TxnStatusActionID, HostReplyMethod, OSPymtCode
                bGetHostInfo = objTxnProcOB.bGetHostInfo(stHostInfo, stPayInfo)
                If (bGetHostInfo) Then
                    'Load waiting page based on LanguageCode, that's why have to get GatewayTxnID from Host first,
                    'then, look for the original request's LanguageCode then only can load the respective waiting page
                    'in bVerifyResTxn() inside bResponseMain()

                    If True <> bResponseMain() Then
                        If (stPayInfo.iTxnStatus <> Common.TXN_STATUS.LATE_HOST_REPLY And stPayInfo.iTxnStatus <> Common.TXN_STATUS.INVALID_HOST_IP And stPayInfo.iTxnStatus <> Common.TXN_STATUS.INVALID_HOST_REPLY_GATEWAYTXNID) Then
                            'Return failed response to merchant
                            ResponseErrHandler()
                        End If
                    End If

                    'Good/Failed/Pending response already returned to merchant in bResponseMain()
                    'Error response also returned to merchant as above
                    'Now, only inserts the respective response with the final TxnStatus into database.
                    'Good/Failed/Error responses are considered as finalized and can be deleted from Request table and inserted into Response table.
                    'Pending txns that need retry need to stay in Request table for Retry Service to process accordingly.

                    If (stPayInfo.iTxnStatus <> Common.TXN_STATUS.LATE_HOST_REPLY And stPayInfo.iTxnStatus <> Common.TXN_STATUS.INVALID_HOST_IP And stPayInfo.iTxnStatus <> Common.TXN_STATUS.INVALID_HOST_REPLY_GATEWAYTXNID) Then
                        'Action 3 (Retry query host); Action 4 (Retry confirm booking)
                        'Txns that belong to these two types of action do not need to be inserted into Response table yet,
                        'because once the spInsTxnResp stored procedure is called, the original request will be deleted from
                        'Request table. These txns that need retry should stay in Request table for Retry Service to process.

                        If ((stPayInfo.iAction <> 3) And (stPayInfo.iAction <> 4)) Then
                            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                            stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > Inserting TxnResp. GatewayTxnID[" + stPayInfo.szTxnID + "] MerchantTxnStatus[" + stPayInfo.iMerchantTxnStatus.ToString() + "] TxnStatus[" + stPayInfo.iTxnStatus.ToString() + "] TxnState[" + stPayInfo.iTxnState.ToString() + "] RespMesg[" + stPayInfo.szTxnMsg + "].")

                            If (stPayInfo.szTxnID <> "" And stPayInfo.szTxnAmount <> "" And (stPayInfo.szHostTxnStatus <> "" Or stPayInfo.szRespCode <> "") And stPayInfo.szMerchantOrdID <> "") Then
                                If True <> objTxnProcOB.bInsertTxnResp(stPayInfo, stHostInfo) Then
                                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                    stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > Error: " + stPayInfo.szErrDesc)
                                Else
                                    'Added, 28 Mar 2014, update PG..TB_PayTxnRef
                                    If True <> objTxnProc.bUpdatePayTxnRef(stPayInfo, stHostInfo) Then
                                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                        stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > Error: " + stPayInfo.szErrDesc)
                                    End If
                                End If
                            Else
                                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                                " > GatewayTxnID(" + stPayInfo.szTxnID + ")/TxnAmount(" + stPayInfo.szTxnAmount + ")/HostTxnStatus(" + stPayInfo.szHostTxnStatus + ")/RespCode(" + stPayInfo.szRespCode + ") are empty from response. Bypass InsertTxnResp.")
                            End If
                        End If
                    End If
                Else
                    'For this scenario, most probably Datebase error, the Retry Service will query the host for status
                    'once database is up.
                    stPayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_PENDING
                    stPayInfo.iTxnStatus = Common.TXN_STATUS.TXN_PENDING
                    stPayInfo.szTxnMsg = Common.C_INVALID_HOST_902
                    stPayInfo.szErrDesc = "> Failed to get Host Info for " + stPayInfo.szIssuingBank + " " + stPayInfo.szTxnType + " response"

                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    "Error: " + stPayInfo.szErrDesc)

                    'bResponseMain() is not run, therefore, unable to get other reply values.
                    'Merchant will only get TxnType, IssuingBank, TxnState, TxnStatus, RespMesg
                    'Without MerchantPymtID, the reply is meaningless for the merchant. Therefore, no need reply.
                    'ResponseErrHandler()
                End If
            End If
        Catch ex As Exception
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            " > " + C_ISSUING_BANK + " response Page_Load() Exception: " + ex.Message)
        End Try
    End Sub

    '********************************************************************************************
    ' Class Name:		Response_m2u2
    ' Function Name:	ResponseErrHandler
    ' Function Type:	NONE
    ' Parameter:        
    ' Description:		Error handler
    ' History:			18 Nov 2008
    '********************************************************************************************
    Private Sub ResponseErrHandler()
        Dim szHTTPString As String = ""
        Dim iRet As Integer = -1

        Try
            'Generates response hash value
            'stPayInfo.szHashValue = objHash.szGetSaleResHash(stPayInfo)
            stPayInfo.szHashValue2 = objHash.szGetSaleResHash2(stPayInfo)   'Added, 2 May 2016

            'Generates HTTP string to be replied to merchant
            szHTTPString = objResponse.szGetReplyMerchantHTTPString(stPayInfo, stHostInfo)

            'Send HTTP string reply to merchant using the same method used by Host
            'Modified 4 Oct 2016, added st_HostInfo, KTB redirect resp query
            If (stPayInfo.szMerchantReturnURL <> "") Then
                iRet = objResponse.iHTTPSend(stHostInfo.iHostReplyMethod, szHTTPString, stPayInfo.szMerchantReturnURL, stPayInfo, stHostInfo, g_szHTML)
                If (iRet <> 0) Then
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > Failed to send error reply iRet(" + CStr(iRet) + ") " + szHTTPString + " to merchant URL " + stPayInfo.szMerchantReturnURL)
                Else
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                    DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > Error reply to merchant sent.")
                End If
            Else
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > MerchantRURL empty. Bypass sending error reply " + szHTTPString + " to merchant")
            End If

            'Another alternative - NOTE: If MerchantRURL contains "?" and param, can not use this alternative
            'coz it does not support additional params in the error template.
            'objResponse.iLoadErrUI(stPayInfo.szMerchantReturnURL, stPayInfo, g_szHTML)
        Catch ex As Exception
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            " > ResponseErrHandler() Exception: " + ex.Message)
        End Try
    End Sub

    '********************************************************************************************
    ' Class Name:		Response_m2u2
    ' Function Name:	bResponseMain
    ' Function Type:	Boolean.  Return False if error.
    ' Parameter:		NONE
    ' Description:		A main function to get the response info and handle the process
    ' History:			28 Oct 2008
    '********************************************************************************************
    Private Function bResponseMain() As Boolean
        Dim bReturn = True

        Try
            'Log one empty line for easier reference for new response transaction
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "")

            'Log all form's response fields
            'Modified 9 Mar 2015, changed wording of Redirect to S2S
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            C_ISSUING_BANK + " S2S Response: " + szGetAllHTTPVal())

            'Get Response data
            If True <> objResponse.bGetData(stPayInfo, stHostInfo) Then
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                "> Error1_1 GetData: " + stPayInfo.szErrDesc)

                ' stPayInfo.szTxnMsg specified in bGetData
                stPayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                stPayInfo.iTxnStatus = Common.TXN_STATUS.INTERNAL_ERROR
                stPayInfo.iTxnState = Common.TXN_STATE.ABORT

                'NOTE: May not be able to be inserted into Response table if mandatory fields are missing due to failure in bGetData()
                Return False
            End If

            'Ensure GatewayTxnID is passed back by Host before proceed. GatewayTxnID is important for reconciliation.
            If (stPayInfo.szTxnID = "") Then
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                "> Error1_2 bResponseMain(): Missing GatewayTxnID from Host's reply.")

                stPayInfo.szTxnMsg = Common.C_INVALID_HOST_REPLY_2907
                stPayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                stPayInfo.iTxnStatus = Common.TXN_STATUS.INVALID_HOST_REPLY
                stPayInfo.iTxnState = Common.TXN_STATE.ABORT

                'NOTE: May not be able to be inserted into Response table since GatewayTxnID is missing.
                Return False
            End If

            'Modified 23 Jun 2015, added True for b_VerifyHostIP
            If True <> objResponse.bProcessSaleRes(stPayInfo, stHostInfo, szReplyDateTime, g_szHTML, True) Then
                Return False
            End If

        Catch ex As Exception
            bReturn = False

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            stPayInfo.szMerchantID + stPayInfo.szMerchantTxnID + " > bResponseMain() Exception: " + ex.Message)
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		Response_m2u2
    ' Function Name:	GetAllHTTPVal
    ' Function Type:	Boolean.  Return False if error.
    ' Parameter:		NONE
    ' Description:		Get all form's or query string's values
    ' History:			28 Oct 2008
    '********************************************************************************************
    Public Function szGetAllHTTPVal() As String
        Dim szFormValues As String
        Dim szTempKey As String
        Dim szTempVal As String

        Dim iCount As Integer = 0
        Dim iLoop As Integer = 0

        szFormValues = "[User IP: " + Request.UserHostAddress + "] "

        iCount = Request.Form.Count

        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                        " > No. of Form Response Fields (" + iCount.ToString() + ")")

        If (iCount > 0) Then
            For iLoop = 1 To iCount
                szTempKey = ""
                szTempKey = Request.Form.GetKey(iLoop - 1)

                szFormValues = szFormValues + szTempKey + "="

                szTempVal = ""
                szTempVal = Server.UrlDecode(Request.Form.Get(iLoop - 1))

                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                " > Field(" + szTempKey + ") Value(" + szTempVal + ")")

                'Mask CardPAN
                If ((szTempKey <> "")) Then
                    If (szTempKey.ToLower = "cardpan") Then
                        If szTempVal.Length > 10 Then
                            szFormValues = szFormValues + szTempVal.Substring(0, 6) + "".PadRight(szTempVal.Length - 6 - 4, "X") + szTempVal.Substring(szTempVal.Length - 4, 4) + "&"
                        Else
                            szFormValues = szFormValues + "".PadRight(szTempVal.Length, "X") + "&"
                        End If
                    Else
                        If (iLoop = iCount) Then
                            szFormValues = szFormValues + szTempVal
                        Else
                            szFormValues = szFormValues + szTempVal + "&"
                        End If
                    End If
                End If
            Next iLoop
        Else
            iCount = Request.QueryString.Count

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            " > No. of QueryString Response Fields (" + iCount.ToString() + ")")

            For iLoop = 1 To iCount
                szTempKey = ""
                szTempKey = Request.QueryString.GetKey(iLoop - 1)

                szFormValues = szFormValues + szTempKey + "="

                szTempVal = ""
                szTempVal = Server.UrlDecode(Request.QueryString.Get(iLoop - 1))

                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                " > Field(" + szTempKey + ") Value(" + szTempVal + ")")

                'Mask CardPAN
                If ((szTempKey <> "")) Then
                    If (szTempKey.ToLower = "cardpan") Then
                        If szTempVal.Length > 10 Then
                            szFormValues = szFormValues + szTempVal.Substring(0, 6) + "".PadRight(szTempVal.Length - 6 - 4, "X") + szTempVal.Substring(szTempVal.Length - 4, 4) + "&"
                        Else
                            szFormValues = szFormValues + "".PadRight(szTempVal.Length, "X") + "&"
                        End If
                    Else
                        If (iLoop = iCount) Then
                            szFormValues = szFormValues + szTempVal
                        Else
                            szFormValues = szFormValues + szTempVal + "&"
                        End If
                    End If
                End If
            Next iLoop
        End If

        Return szFormValues
    End Function

    Public Sub New()
        szLogPath = ConfigurationManager.AppSettings("LogPath")
        iLogLevel = Convert.ToInt32(ConfigurationManager.AppSettings("LogLevel"))

        '2 Apr 2010, moved InitLogger() from down to up here, or else Hash will get empty objLoggerII and encountered exception
        InitLogger()

        objTxnProc = New TxnProc(objLoggerII)
        objCommon = New Common(objLoggerII)
        objHash = New Hash(objLoggerII)
        objTxnProcOB = New TxnProcOB(objLoggerII)
        objResponse = New Response(objLoggerII)

    End Sub

    Public Sub InitLogger()
        If (objLoggerII Is Nothing) Then

            If (szLogPath Is Nothing) Then
                szLogPath = "C:\\PG.log"
            End If

            If (iLogLevel < 0 Or iLogLevel > 5) Then
                iLogLevel = 3
            End If

            'objLoggerII = New LoggerII.CLoggerII(
            objLoggerII = LoggerII.CLoggerII.GetInstanceX(szLogPath + "_" + C_ISSUING_BANK + ".log", iLogLevel, 255)

        End If
    End Sub

    Protected Overrides Sub Finalize()
        If Not objLoggerII Is Nothing Then
            objLoggerII.Dispose()
            objLoggerII = Nothing
        End If

        If Not objCommon Is Nothing Then objCommon = Nothing
        If Not objTxnProcOB Is Nothing Then objTxnProcOB = Nothing
        If Not objTxnProc Is Nothing Then objTxnProc = Nothing
        If Not objHash Is Nothing Then objHash = Nothing

        If Not objResponse Is Nothing Then
            objResponse.Dispose()
            objResponse = Nothing
        End If

        MyBase.Finalize()
    End Sub
End Class
