using Scheduler.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduler
{
    static class Program
    {
        static void Main(string[] args)
        {
            TransactionAutomatedService transactionTypeService = new TransactionAutomatedService();
            transactionTypeService.Process();

            //Console.WriteLine("Press any key to close.");
            //Console.ReadLine();

            //Console.ReadKey();

            // Go to http://aka.ms/dotnet-get-started-console to continue learning how to build a console app! 
        }
    }
}
