﻿using Scheduler.Core.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduler.Core.Providers
{
    public class TransactionProvider
    {
        private string PGconnStrName = "PGAdmin_conn";
        private string OBconnStrName = "OB_conn";

        public List<RepTBLateRes> GetAllRepTBLateRes(DateTime dateFrom, DateTime dateTo, string FPXbankRespCode, string DragonPayRespCode)
        {
            Database.DB database = new Database.DB();
            var connStr = database.connstring(PGconnStrName);

            using (SqlConnection sqlConnection = new SqlConnection(connStr.StrConn))
            {
                List<RepTBLateRes> details = new List<RepTBLateRes>();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "PGAdmin.dbo.spRepTBLateRes_By_BankRespCode";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = sqlConnection;
                    cmd.CommandTimeout = 90;
                    cmd.Parameters.Add("@PGatewayTxnID", SqlDbType.VarChar, 30).Value = DBNull.Value;
                    cmd.Parameters.Add("@PMerchantID", SqlDbType.VarChar, 30).Value = DBNull.Value;
                    cmd.Parameters.Add("@PSince", SqlDbType.DateTime).Value = dateFrom;
                    cmd.Parameters.Add("@PTo", SqlDbType.DateTime).Value = dateTo;
                    cmd.Parameters.Add("@PFPXbankRespCode", SqlDbType.VarChar, 2).Value = FPXbankRespCode;
                    cmd.Parameters.Add("@PDragonPayRespCode", SqlDbType.VarChar, 1).Value = DragonPayRespCode;
                    cmd.Parameters.Add("@PCurrentStatus", SqlDbType.VarChar, 1).Value ="0";

                    sqlConnection.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    foreach (DataRow dr in dt.Rows)
                    {
                        RepTBLateRes repTBLateRes = new RepTBLateRes();

                        repTBLateRes.GatewayTxnId = dr["GatewayTxnID"].ToString();
                        repTBLateRes.MerchantID = dr["MerchantID"].ToString();
                        repTBLateRes.BankRespCode = dr["BankRespCode"].ToString();
                        repTBLateRes.BankRefNo = dr["BankRefNo"].ToString();
                        repTBLateRes.LastUpdated = (DateTime)(dr["LastUpdated"]);
                        repTBLateRes.CurrentStatus = dr["CurrentStatus"].ToString();
                        repTBLateRes.RespMesg = dr["RespMesg"].ToString();
                        details.Add(repTBLateRes);
                    }
                }
                return details;
            }
        }

        public int UpdatePayResTxnRateStatus(string PGatewayTxnID, string PPymtMethod, int PStatus, int PState)
        {
            Database.DB database = new Database.DB();
            var connStr = database.connstring(PGconnStrName);

            using (SqlConnection sqlConnection = new SqlConnection(connStr.StrConn))
            {
                int affectedRecord = 0;
                List<RepTBLateRes> details = new List<RepTBLateRes>();

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = Database.PGAdmin.StoredProcedures.spUpdate_PayRes_TxnRate_Status;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = sqlConnection;

                    cmd.Parameters.Add("@PGatewayTxnID", SqlDbType.VarChar, 30).Value = PGatewayTxnID;
                    cmd.Parameters.Add("@PPymtMethod", SqlDbType.VarChar, 30).Value = PPymtMethod;
                    cmd.Parameters.Add("@PStatus", SqlDbType.VarChar, 30).Value = PStatus;
                    cmd.Parameters.Add("@PState", SqlDbType.VarChar, 30).Value = PState;

                    sqlConnection.Open();

                    affectedRecord = cmd.ExecuteNonQuery();
                    return affectedRecord;

                }
            }
        }


        public List<MissingTransactionInTB_TxnRate> GetAllMissingTransactionInTB_TxnRateForOB(DateTime dateFrom, DateTime dateTo)
        {
            Database.DB database = new Database.DB();
            var connStr = database.connstring(PGconnStrName);

            using (SqlConnection sqlConnection = new SqlConnection(connStr.StrConn))
            {
                List<MissingTransactionInTB_TxnRate> details = new List<MissingTransactionInTB_TxnRate>();

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = Database.PGAdmin.StoredProcedures.spRptGetAllMissingTransactionInTB_TxnRateForOB;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = sqlConnection;

                    cmd.Parameters.Add("@PSince", SqlDbType.DateTime).Value = dateFrom;
                    cmd.Parameters.Add("@PTo", SqlDbType.DateTime).Value = dateTo;

                    sqlConnection.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);

                    foreach (DataRow dr in dt.Rows)
                    {
                        MissingTransactionInTB_TxnRate missingTransactionInTB_TxnRate = new MissingTransactionInTB_TxnRate();

                        missingTransactionInTB_TxnRate.GatewayTxnId = dr["GatewayTxnID"].ToString();
                        missingTransactionInTB_TxnRate.MerchantID = dr["MerchantID"].ToString();
                        missingTransactionInTB_TxnRate.TxnStatus = dr["TxnStatus"].ToString();
                        missingTransactionInTB_TxnRate.DateCompleted = (DateTime)(dr["DateCompleted"]);
                        missingTransactionInTB_TxnRate.BankRefNo = dr["BankRefNo"].ToString();
                        details.Add(missingTransactionInTB_TxnRate);
                    }
                }
                return details;
            }
        }

        public int AutoJobTxnRateCountOB(string PGatewayTxnID, DateTime dateFrom, DateTime dateTo)
        {
            Database.DB database = new Database.DB();
            var connStr = database.connstring(OBconnStrName);

            using (SqlConnection sqlConnection = new SqlConnection(connStr.StrConn))
            {
                int affectedRecord = 0;
       
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = Database.OB.StoredProcedures.spJobTxnRateCountOB;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = sqlConnection;

                    cmd.Parameters.Add("@PGatewayTxnID", SqlDbType.VarChar, 30).Value = PGatewayTxnID;
                    cmd.Parameters.Add("@PSince", SqlDbType.DateTime).Value = dateFrom;
                    cmd.Parameters.Add("@PTo", SqlDbType.DateTime).Value = dateTo;

                    sqlConnection.Open();

                    affectedRecord = cmd.ExecuteNonQuery();
                    return affectedRecord;

                }
            }
        }

        public List<string> GetPendingGatewayTxn(string IssuingBank, int minutes)
        {
            Database.DB database = new Database.DB();
            var connStr = database.connstring(OBconnStrName);

            using (SqlConnection sqlConnection = new SqlConnection(connStr.StrConn))
            {
                List<string> affectedRecords = new List<string>();

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = Database.OB.StoredProcedures.spGetAllPendingTransactionsGatewayTxnID;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = sqlConnection;

                    cmd.Parameters.Add("@IssuingBank", SqlDbType.VarChar, 30).Value = IssuingBank;
                    cmd.Parameters.Add("@DurationMinute", SqlDbType.Int).Value = minutes;

                    sqlConnection.Open();

                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                   
                    foreach (DataRow dr in dt.Rows)
                    {
                        affectedRecords.Add(dr["GatewayTxnID"].ToString());
                    }

                }
                return affectedRecords;
            }
        }

    }
}
