﻿using Scheduler.Core.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduler.Core.Providers
{
    public class HostProvider
    {
        private string connStr = Convert.ToString(ConfigurationManager.AppSettings["OB_conn"]);

        public List<Host> GetAllHost()
        {
            using (SqlConnection sqlConnection = new SqlConnection(connStr))
            {
                List<Host> details = new List<Host>();

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = Database.OB.StoredProcedures.spGetHosts;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = sqlConnection;

                    sqlConnection.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);

                    foreach (DataRow dr in dt.Rows)
                    {
                        Host host = new Host();

                        host.HostName = dr["HostName"].ToString();
                        host.PaymentTemplate = dr["PaymentTemplate"].ToString();

                        details.Add(host);
                    }
                }
                return details;
            }
        }
    }
}
