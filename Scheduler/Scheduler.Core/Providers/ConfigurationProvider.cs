﻿using Scheduler.Core.Configuration;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduler.Core.Providers
{
    internal class ConfigurationProvider
    {
        private string connStrName = "PGAdmin_conn";
      
        public SystemConfiguration GetConfig(string configName)
        {
            Database.DB database = new Database.DB();
            var connStr = database.connstring(connStrName);

            using (SqlConnection sqlConnection = new SqlConnection(connStr.StrConn))
            {
                List<SystemConfiguration> systemConfig = new List<SystemConfiguration>();

                var config = new SystemConfiguration();

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = Database.PGAdmin.StoredProcedures.GetConfigDetails;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = sqlConnection;

                    cmd.Parameters.Add("@ConfigName", SqlDbType.VarChar, 30).Value = configName;

                    sqlConnection.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);

                    foreach (DataRow dr in dt.Rows)
                    {

                        switch (dr["KeyName"].ToString())
                        {
                            case SystemConfiguration.TransactionsPeriodHoursKeyName:
                                config.TransactionsPeriodHours = int.Parse(dr["KeyValue"].ToString());
                                break;
                        }
                    }
                }
                return config;
            }
        }

        public SystemConfiguration GetConfigPaymentGatewayAPI(string configName)
        {
            Database.DB database = new Database.DB();
            var connStr = database.connstring(connStrName);

            using (SqlConnection sqlConnection = new SqlConnection(connStr.StrConn))
            {
                List<SystemConfiguration> systemConfig = new List<SystemConfiguration>();

                var config = new SystemConfiguration();

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = Database.PGAdmin.StoredProcedures.GetConfigDetails;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = sqlConnection;

                    cmd.Parameters.Add("@ConfigName", SqlDbType.VarChar, 30).Value = configName;

                    sqlConnection.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);

                    foreach (DataRow dr in dt.Rows)
                    {

                        switch (dr["KeyName"].ToString())
                        {
                            case SystemConfiguration.TNG_clientIdKeyName:
                                config.TNG_clientId = (dr["KeyValue"]).ToString();
                                break;
                            case SystemConfiguration.TNG_merchantIdKeyName:
                                config.TNG_merchantId = (dr["KeyValue"]).ToString();
                                break;
                            case SystemConfiguration.TNG_versionKeyName:
                                config.TNG_version = (dr["KeyValue"]).ToString();
                                break;
                            case SystemConfiguration.TNG_URLPAY_RETURNKeyName:
                                config.TNG_URLPAY_RETURN = (dr["KeyValue"]).ToString();
                                break;
                            case SystemConfiguration.TNG_PendingMinutesKeyName:
                                config.TNG_PendingMinutes = (dr["KeyValue"]).ToString();
                                break;
                            case SystemConfiguration.TNG_URLAUTO_RETURNKeyName:
                                config.TNG_URLAUTO_RETURN = (dr["KeyValue"]).ToString();
                                break;
                        }
                    }
                }
                return config;
            }
        }
    }
}
