﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduler.Core.Configuration
{
    public class SystemConfiguration
    {
        public const string TransactionsPeriodHoursKeyName = "Paydibs.Scheduler#TransactionsPeriodHours";

        public int TransactionsPeriodHours { get; set; }


        //TNG
        public const string TNG_clientIdKeyName = "Paydibs.PaymentGatewayAPI.TNG#clientId";
        public const string TNG_merchantIdKeyName = "Paydibs.PaymentGatewayAPI.TNG#merchantId";
        public const string TNG_versionKeyName = "Paydibs.PaymentGatewayAPI.TNG#version";
        public const string TNG_URLPAY_RETURNKeyName = "Paydibs.PaymentGatewayAPI.TNG#URLPAY_RETURN";
        public const string TNG_PendingMinutesKeyName = "Paydibs.PaymentGatewayAPI.TNG#PendingMinutes";
        public const string TNG_URLAUTO_RETURNKeyName = "Paydibs.PaymentGatewayAPI.TNG#URLAUTO_RETURN";


        public string TNG_clientId { get; set; }
        public string TNG_merchantId { get; set; }
        public string TNG_version { get; set; }
        public string TNG_URLPAY_RETURN { get; set; }
        public string TNG_PendingMinutes { get; set; }
        public string TNG_URLAUTO_RETURN { get; set; }
    }
}
