﻿
namespace Scheduler.Core.Services.Interfaces
{
    public interface ITransactionAutomatedService
    {
        void Process();
    }
}
