﻿using LoggerII;
using Scheduler.Core.Configuration;
using Scheduler.Core.Providers;
using Scheduler.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;

namespace Scheduler.Core.Services
{
    public class TnGAutomatedService
    {
        #region Declaration

        const string configName = "Paydibs.Scheduler";
        const string TnGName = "TNG";
        const int pendingDurationMinutes = 5;
        private SystemConfiguration Config = null;
        private static readonly HttpClient client = new HttpClient();

        private ConfigurationProvider _configurationProvider;
        private ConfigurationProvider ConfigurationProvider
        {
            get { return _configurationProvider ?? (_configurationProvider = new ConfigurationProvider()); }
        }

        //private HostProvider _hostProvider;
        //private HostProvider HostProvider
        //{
        //    get { return _hostProvider ?? (_hostProvider = new HostProvider()); }
        //}

        private TransactionProvider _transactionProvider;
        private TransactionProvider TransactionProvider
        {
            get { return _transactionProvider ?? (_transactionProvider = new TransactionProvider()); }
        }

        #endregion

        #region Constructor

        #endregion

        public TnGAutomatedService()
        {
            //Config = ConfigurationProvider.GetConfig();
        }
        public void TnGRequery()
        {
            Config = ConfigurationProvider.GetConfigPaymentGatewayAPI("Paydibs.PaymentGatewayAPI");

            string msg = string.Empty;
            Logger logger = new Logger();
            CLoggerII objLoggerII = new CLoggerII();
            objLoggerII = logger.InitLogger();

            try
            {
                //Get TnG transaction which the results results still unknown and finish succesful results
                var pendingTransactions = TransactionProvider.GetPendingGatewayTxn(TnGName, int.Parse(Config.TNG_PendingMinutes));

                msg = "[TnG 1][Paydibs Console Start]" + DateTime.Now + ": [Total of Pending Transactions Found: " + pendingTransactions.Count() + "]";

                Console.WriteLine(msg);
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                    msg);
                foreach (var pendingTransactionGatewayTxnID in pendingTransactions)
                {
                    msg = "[TnG 1][Gateway Transaction ID: " + pendingTransactionGatewayTxnID + "]";
                    Console.WriteLine(msg);
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                        msg);
                    //Console.WriteLine(Config.TNG_URLPAY_RETURN + "?clientId=" + Config.TNG_clientId + "&merchantId=" + Config.TNG_merchantId + "&merchantTransId=" + pendingTransactionGatewayTxnID + "&version=" + Config.TNG_version);
                    
                    //Send API to PaymentGatewayAPI to initiate requery
                    var responseString = HttpHelper.HttpGet(Config.TNG_URLAUTO_RETURN + "?clientId=" + Config.TNG_clientId + "&merchantId=" + Config.TNG_merchantId + "&merchantTransId=" + pendingTransactionGatewayTxnID + "&version=" + Config.TNG_version);
                    //msg = "[TnG 1][Respond: " + responseString + "]";
                    //Console.WriteLine(responseString);
                    if (responseString == "SUCCESS")
                    {
                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
    "Requery for transaction " + pendingTransactionGatewayTxnID + " is success");
                    }
                    else if (responseString == "INIT")
                    {
                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
    "Transaction " + pendingTransactionGatewayTxnID + " has not been completed yet");
                    }
                    else
                    {
                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
    "Requery for transaction " + pendingTransactionGatewayTxnID + " is failed");
                    }

                }



            }
            catch (Exception ex)
            {
                msg = "[TnG 1][Exception]" + DateTime.Now + ex.ToString();
                Console.WriteLine(msg);
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                      msg);
            }


        }
        }
    }
