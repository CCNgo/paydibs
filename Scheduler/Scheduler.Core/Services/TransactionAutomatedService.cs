﻿using Scheduler.Core.Providers;
using Scheduler.Core.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LoggerII;
using Scheduler.Core.Configuration;

namespace Scheduler.Core.Services
{
    public class TransactionAutomatedService : ITransactionAutomatedService
    {

        #region Declaration

        const string configName = "Paydibs.Scheduler";
        const string FPXbankRespCode = "00";
        const string DragonPayRespCode = "S";
        private SystemConfiguration Config = null;

        private ConfigurationProvider _configurationProvider;
        private ConfigurationProvider ConfigurationProvider
        {
            get { return _configurationProvider ?? (_configurationProvider = new ConfigurationProvider()); }
        }

        //private HostProvider _hostProvider;
        //private HostProvider HostProvider
        //{
        //    get { return _hostProvider ?? (_hostProvider = new HostProvider()); }
        //}

        private TransactionProvider _transactionProvider;
        private TransactionProvider TransactionProvider
        {
            get { return _transactionProvider ?? (_transactionProvider = new TransactionProvider()); }
        }

        #endregion

        #region Constructor

        #endregion

        public TransactionAutomatedService()
        {
            //Config = ConfigurationProvider.GetConfig();
        }

        public void Process()
        {
            Config = ConfigurationProvider.GetConfig("Paydibs.Scheduler");

            DateTime dateFrom = DateTime.Now.AddHours(-Config.TransactionsPeriodHours);
            DateTime dateTo = DateTime.Now.AddMinutes(30);

            string msg = string.Empty;
            Logger logger = new Logger();
            CLoggerII objLoggerII = new CLoggerII();
            objLoggerII = logger.InitLogger();

            try
            {
                //Get Order that not 00 bank response code and 0 current status
                var orders = TransactionProvider.GetAllRepTBLateRes(dateFrom, dateTo, FPXbankRespCode, DragonPayRespCode);

                msg = "[1][Paydibs Console Start]" + DateTime.Now + ": [Total of Orders Required to be Auto-Success for View Late Response in the system: " + orders.Count() + "]";
                Console.WriteLine(msg);
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                    msg);

                foreach (var o in orders)
                {
                    msg = "[1][Gateway Transaction ID: " + o.GatewayTxnId + "][MerchantId: " + o.MerchantID + "][Bank Reference Number: "
                        + o.BankRefNo + "][Response Message: " + o.RespMesg + "][Current Status: " + o.CurrentStatus + "][Last Updated: " + o.LastUpdated + "]";
                    Console.WriteLine(msg);
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                        msg);

                    //Update Current Status
                    if ((o.BankRespCode == "00" || o.BankRespCode == "S") && o.CurrentStatus != "Success")
                    {
                        //41 = Transaction modified from failed status
                        int affected = TransactionProvider.UpdatePayResTxnRateStatus(o.GatewayTxnId, "OB", 0, 41);
                        if (affected != 0)
                        {
                            msg = "[1]Transaction Updated [" + o.GatewayTxnId + " ] Transaction modified from pending status to success status";
                            Console.WriteLine(msg);
                            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                       msg);
                        }
                        else
                        {
                            msg = "[1]No Transaction is Updated";
                            Console.WriteLine(msg);
                            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                       msg);
                        }
                    }
                }
                msg = "[1][Paydibs Console End]" + DateTime.Now;
                Console.WriteLine(msg);
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                       msg);


                //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                //Get Missing Transaction Orders
                dateFrom = DateTime.Today.AddDays(-1);
                dateTo = DateTime.Today;

                var missingTransactionOrders = TransactionProvider.GetAllMissingTransactionInTB_TxnRateForOB(dateFrom, dateTo);

                msg = "[2][Paydibs Console Start]" + DateTime.Now + ": [Total of Orders Missing to be Auto Re-adding to Finance Report on Yesterday: " + missingTransactionOrders.Count() + "]";
                Console.WriteLine(msg);
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                    msg);

                foreach (var o in missingTransactionOrders)
                {
                    msg = "[2][Gateway Transaction ID: " + o.GatewayTxnId + "][MerchantId: " + o.MerchantID + "][Bank Reference Number: " + o.BankRefNo + "]";
                    Console.WriteLine(msg);
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                        msg);

                    int affected = TransactionProvider.AutoJobTxnRateCountOB(o.GatewayTxnId, dateFrom, dateTo);
                    if (affected != 0)
                    {
                        msg = "[2]Transaction Updated [" + o.GatewayTxnId + " ] Transaction being added successfully to Finance Report for Yesterday";
                        Console.WriteLine(msg);
                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                    msg);
                    }
                    else
                    {
                        msg = "[2]No Transaction is Updated to Finance Report on Yesterday";
                        Console.WriteLine(msg);
                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                    msg);
                    }
        
                }
                msg = "[2][Paydibs Console End]" + DateTime.Now;
                Console.WriteLine(msg);
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                       msg);
            }
            catch (Exception ex)
            {
                msg = "[Exception]" + DateTime.Now + ex.ToString();
                Console.WriteLine(msg);
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                      msg);
            }
        }
    
    }
}

