﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Kenji.Apps;
using NeoApp;
using PaydibsPortal.DAL;

namespace Scheduler.Core
{
    public class Database
    {
        public class DB
        {
            MsSqlClient mssqlc = null;
            SqlBuilder sqlb = new SqlBuilder();
            string connStr = "";
            LinqTool linq = new LinqTool();
            public string errMsg = "";
            Encryption enc = new Encryption();
            public string system_err = string.Empty;
            string decryptKey = string.Empty;

            public MsSqlClient connstring(string conn)
            {
                string Ecnrypted_connStr = ConfigurationManager.AppSettings[conn];
                connStr = Decrypt_ConnStr(Ecnrypted_connStr);
                mssqlc = new MsSqlClient(connStr);

                return mssqlc;
            }

            string Decrypt_ConnStr(string PEncrypted_ConnStr)
            {
                string KEY = "e26b5be2-867a-4400-8079-612b4a2332f6";
                decryptKey = enc.AESDecrypt(PEncrypted_ConnStr, KEY, KEY);

                return decryptKey;
            }
        }

        public class OB
        {
            private static string _alias;
            public static string Alias
            {
                get
                {
                    if (String.IsNullOrEmpty(_alias))
                    {
                        _alias = ConfigurationManager.AppSettings["OB_DBAlias"];
                        if (String.IsNullOrEmpty(_alias))
                        {
                            _alias = "OB";
                        }
                    }

                    return _alias;
                }
            }

            public class StoredProcedures
            {
                public const string spGetHosts = "OB.dbo.spGetHosts";
                public const string spJobTxnRateCountOB = "OB.dbo.spJobTxnRateCountOB";
                public const string spGetAllPendingTransactionsGatewayTxnID = "OB.dbo.spGetAllPendingTransactionsGatewayTxnID";

            }
        }

        public class PGAdmin
        {
            private static string _alias;
            public static string Alias
            {
                get
                {
                    if (String.IsNullOrEmpty(_alias))
                    {
                        _alias = ConfigurationManager.AppSettings["PGAdmin_DBAlias"];
                        if (String.IsNullOrEmpty(_alias))
                        {
                            _alias = "PGAdmin";
                        }
                    }

                    return _alias;
                }
            }

            public class StoredProcedures
            {
                public const string spRepTBLateRes_By_BankRespCode = "PGAdmin.dbo.spRepTBLateRes_By_BankRespCode";

                public const string GetConfigDetails  = "PGAdmin.dbo.GetConfigDetails";

                public const string spUpdate_PayRes_TxnRate_Status = "PGAdmin.dbo.spUpdate_PayRes_TxnRate_Status";

                public const string spRptGetAllMissingTransactionInTB_TxnRateForOB = "PGAdmin.dbo.spRptGetAllMissingTransactionInTB_TxnRateForOB";
            }
        }
    }
}
