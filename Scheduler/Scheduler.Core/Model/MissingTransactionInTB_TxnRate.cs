﻿
using System;

namespace Scheduler.Core.Model
{
    public class MissingTransactionInTB_TxnRate
    {
        public string GatewayTxnId { get; set; }
        public string TxnStatus { get; set; }
        public DateTime DateCompleted { get; set; }
        public string MerchantID { get; set; }
        public string BankRefNo { get; set; }
    }
}
