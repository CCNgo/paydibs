﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduler.Core.Model
{
    public class ConfigurationDatabase
    {
        public int Id { get; set; }
        public string DBName { get; set; }
        public string ConnectionString { get; set; }
    }
}
