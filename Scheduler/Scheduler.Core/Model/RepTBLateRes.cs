﻿
using System;

namespace Scheduler.Core.Model
{
    public class RepTBLateRes
    {
        public string GatewayTxnId { get; set; }
        public string MerchantID { get; set; }
        public string BankRespCode { get; set; }
        public string BankRefNo { get; set; }
        public DateTime LastUpdated { get; set; }
        public string CurrentStatus { get; set; }
        public string RespMesg { get; set; }
    }
}
