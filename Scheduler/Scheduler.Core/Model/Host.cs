﻿
namespace Scheduler.Core.Model
{
    public class Host
    {
        public string HostName { get; set; }
        public string PaymentTemplate { get; set; }
    }
}
