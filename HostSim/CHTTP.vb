﻿'Imports LoggerII
Imports System.IO   ' StreamWriter
Imports System
Imports System.Threading
Imports System.Net  ' WebRequest

Public Class CHTTP

    Private objLoggerII As LoggerII.CLoggerII

    '********************************************************************************************
    ' Class Name:		CHTTP
    ' Function Name:	iHTTPost
    ' Function Type:	boolean
    ' Parameter:        sz_URL [in, string]         - Posting URL
    '                   sz_HTTPString [in, string]  - HTTP string to be posted
    '                   i_Timeout [in, integer]     - Connection timeout(in second)
    ' Description:		Perform HTTP posting
    ' History:			19 Nov 2008
    '********************************************************************************************
    Public Function iHTTPostOnly(ByVal sz_URL As String, ByVal sz_HTTPString As String, ByVal i_Timeout As Integer) As Integer
        Dim iRet As Integer = -1
        Dim objHttpWebRequest As HttpWebRequest
        Dim objStreamWriter As StreamWriter
        Dim sbInStream As System.Text.StringBuilder
        Dim iTimeout As Integer
        Dim iResCode As Integer
        Dim iLoop As Integer
        Dim szHTTPStrArray() As String
        Dim szFieldArray() As String

        Try
            '======================================== Sets HttpWebRequest properties ====================================
            sbInStream = New System.Text.StringBuilder
            sbInStream.Append(sz_HTTPString)
            iTimeout = i_Timeout * 1000 'converts input timeout param from second to milliseconds

            objHttpWebRequest = WebRequest.Create(sz_URL)                       'Posting URL
            objHttpWebRequest.ContentType = "application/x-www-form-urlencoded" 'default value
            objHttpWebRequest.Method = "POST"                                   'default value
            objHttpWebRequest.ContentLength = sbInStream.Length ' Must be set b4 retrieving stream for sending data
            objHttpWebRequest.KeepAlive = False
            objHttpWebRequest.Timeout = iTimeout                                '(in milliseconds)

            'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Timeout period(" + iTimeout.ToString() + " ms) Posting HTTP data " + sz_HTTPString + " to " + sz_URL)

            'Ignore invalid server certificates or else may encounter WebException with e.Status = WebExceptionStatus.TrustFailure
            'When the CertificatePolicy property is set to an ICertificatePolicy interface instance, the ServicePointManager
            'uses the certificate policy defined in that instance instead of the default certificate policy.
            'The default certificate policy allows valid certificates, as well as valid certificates that have expired.
            ServicePointManager.CertificatePolicy = New SecPolicy

            If (InStr(sz_HTTPString, "&") > 0) Then
                szHTTPStrArray = Split(sz_HTTPString, "&")
            ElseIf (InStr(sz_HTTPString, "$") > 0) Then
                szHTTPStrArray = Split(sz_HTTPString, "$")
            Else
                ReDim szHTTPStrArray(0)             'Added by Jazz on 5 Jan 2011 for ENETS
                szHTTPStrArray(0) = sz_HTTPString   'Only one value, no need split
            End If

            For iLoop = 0 To szHTTPStrArray.GetUpperBound(0)
                'Added on 19 Aug 2009
                szHTTPStrArray(iLoop) = Trim(Replace(Replace(Replace(szHTTPStrArray(iLoop), vbCrLf, ""), vbCr, ""), vbLf, ""))
                If ("==" = Right(szHTTPStrArray(iLoop), 2)) Then
                    'Differentiate between "field==" from "field=h==" 
                    'Replace "field==" to "field=@" and "field=h==" to "field=h@@"
                    If ((szHTTPStrArray(iLoop).IndexOf("=") + 1) = (szHTTPStrArray(iLoop).Length() - 1)) Then
                        szHTTPStrArray(iLoop) = Left(szHTTPStrArray(iLoop), Len(szHTTPStrArray(iLoop)) - 1) + "@"
                    Else
                        szHTTPStrArray(iLoop) = Left(szHTTPStrArray(iLoop), Len(szHTTPStrArray(iLoop)) - 2) + "@@"
                    End If
                ElseIf ("=" = Right(szHTTPStrArray(iLoop), 1)) Then
                    'Differentiate between "field=" (empty value) from "field=value="
                    'Checks if "=" is the character at the last position of the field value OR, e.g. "field=value="
                    'if "=" is the field delimiter, e.g. "field="
                    'Make sure "=" does not exist at the last character of szHTTPStrArray or else
                    'the array result from the next Split by "=" will encounter out of bound exception
                    If (szHTTPStrArray(iLoop).IndexOf("=") <> szHTTPStrArray(iLoop).Length() - 1) Then
                        szHTTPStrArray(iLoop) = Left(szHTTPStrArray(iLoop), Len(szHTTPStrArray(iLoop)) - 1) + "@"
                    End If
                End If

                szFieldArray = Split(szHTTPStrArray(iLoop), "=")

                'Added on 19 Aug 2009
                If ("@@" = Right(szFieldArray(1), 2)) Then
                    szFieldArray(1) = Left(szFieldArray(1), Len(szFieldArray(1)) - 2) + "=="
                ElseIf ("@" = Right(szFieldArray(1), 1)) Then
                    szFieldArray(1) = Left(szFieldArray(1), Len(szFieldArray(1)) - 1) + "="
                End If

                'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), " Field(" + szFieldArray(0) + ") Value(" + szFieldArray(1) + ")")
            Next iLoop

            '=========================================== Post request ==================================================
            '- StreamWriter constructor creates a StreamWriter with UTF-8 encoding whose GetPreamble method returns an
            'empty byte array. The BaseStream property is initialized using the stream parameter (i.e. objHttpWebRequest.GetRequestStream)
            '- GetRequestStream method returns a stream to use to send data for the HttpWebRequest. Once the Stream
            'instance has been returned, you can send data with the HttpWebRequest by using the Stream.Write method.
            'NOTE:  Must set the value of the ContentLength property before retrieving the stream.
            '       When POST method is used, must get the request stream, write the data to be posted,
            '       and close the stream. This method blocks waiting for content to post; If there is no time-out set and
            '       content is not provided, application will block indefinitely.
            'CAUTION: Must call the Stream.Close method to close the stream and release the connection for reuse.
            '         Failure to close the stream will cause application to run out of connections.

            objStreamWriter = New StreamWriter(objHttpWebRequest.GetRequestStream())
            objStreamWriter.Write(sbInStream.ToString)  ' Sends data
            objStreamWriter.Close() ' Closes the stream and release the connection for reuse to avoid running out of connections

            'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Finished posting HTTP data")

            iRet = 0

        Catch webEx As WebException
            Try
                'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException: " + webEx.ToString)

                iRet = Common.ERROR_CODE.E_COM_URL_UNREACHABLE

                If webEx.Status = WebExceptionStatus.Timeout Then
                    iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                    'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException: Timeout")
                End If

                If webEx.Status = WebExceptionStatus.ProtocolError Then
                    'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Response Status Code :" + CStr(CType(webEx.Response, HttpWebResponse).StatusCode))

                    'Re: HttpStatusCode Enumeration
                    iResCode = CInt(CType(webEx.Response, HttpWebResponse).StatusCode)
                    If iResCode = 400 Then
                        iRet = Common.ERROR_CODE.E_COM_BAD_REQUEST
                        'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Bad Request could not be understood by the server.")
                    ElseIf iResCode = 401 Then
                        iRet = Common.ERROR_CODE.E_COM_UNAUTHORIZED_URL
                        'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Unauthorized, the requested resource requires authentication.")
                    ElseIf iResCode = 403 Then
                        iRet = Common.ERROR_CODE.E_COM_FORBIDDEN_URL
                        'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Forbidden, the server refuses to fulfill the request.")
                    ElseIf iResCode = 404 Then
                        iRet = Common.ERROR_CODE.E_COM_URL_UNREACHABLE
                        'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Not Found, the requested resource does not exist on the server.")
                    End If
                End If
            Catch exp As Exception
                'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Exception: " + exp.ToString)
            End Try

        Catch ioEx As IOException
            'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "IO Exception: " + ioEx.ToString)
            iRet = Common.ERROR_CODE.E_COM_FILE_ERROR

        Catch ex As Exception
            'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Exception: " + ex.ToString)
            iRet = Common.ERROR_CODE.E_COMMON_UNKNOWN_ERROR

        Finally
            'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "iRet: " + CStr(iRet))
        End Try

        Return iRet
    End Function

    '********************************************************************************************
    ' Class Name:		CHTTP
    ' Function Name:	iHTTPostWithRecv
    ' Function Type:	boolean
    ' Parameter:        sz_URL [in, string]             - Posting URL
    '                   sz_HTTPString [in, string]      - HTTP string to be posted
    '                   i_Timeout [in, integer]         - Connection timeout(in second)
    '                   sz_HTTPResponse [out, string]   - HTTP string received
    ' Description:		Perform HTTP posting
    ' History:			19 Nov 2008
    '********************************************************************************************
    Public Function iHTTPostWithRecv(ByVal sz_URL As String, ByVal sz_HTTPString As String, ByVal i_Timeout As Integer, ByRef sz_HTTPResponse As String, ByVal sz_LogString As String) As Integer
        Dim iRet As Integer = -1
        Dim objHttpWebRequest As HttpWebRequest
        Dim objHTTPWebResponse As HttpWebResponse
        Dim objStreamWriter As StreamWriter
        Dim objStreamReader As StreamReader
        Dim sbInStream As System.Text.StringBuilder
        Dim iStartTickCount As Integer
        Dim iTimeout As Integer
        Dim iResCode As Integer
        Dim iLoop As Integer
        Dim szHTTPStrArray() As String
        Dim szFieldArray() As String
        Dim szLogValue As String

        Try
            '======================================== Sets HttpWebRequest properties ====================================
            sbInStream = New System.Text.StringBuilder
            sbInStream.Append(sz_HTTPString)
            iTimeout = i_Timeout * 1000 'converts input timeout param from second to milliseconds

            objHttpWebRequest = WebRequest.Create(sz_URL)                       'Posting URL
            objHttpWebRequest.ContentType = "application/x-www-form-urlencoded" 'default value
            objHttpWebRequest.Method = "POST"                                   'default value
            objHttpWebRequest.ContentLength = sbInStream.Length ' Must be set b4 retrieving stream for sending data
            objHttpWebRequest.KeepAlive = False
            objHttpWebRequest.Timeout = iTimeout                                '(in milliseconds)

            'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Timeout period(" + iTimeout.ToString() + " ms) Posting HTTP data " + sz_LogString + " to " + sz_URL)

            'Ignore invalid server certificates or else may encounter WebException with e.Status = WebExceptionStatus.TrustFailure
            'When the CertificatePolicy property is set to an ICertificatePolicy interface instance, the ServicePointManager
            'uses the certificate policy defined in that instance instead of the default certificate policy.
            'The default certificate policy allows valid certificates, as well as valid certificates that have expired.
            ServicePointManager.CertificatePolicy = New SecPolicy

            iStartTickCount = System.Environment.TickCount()
            'Modified because Credit Card can not log clear value
            If ("" = sz_LogString) Then
                If (InStr(sz_HTTPString, "&") > 0) Then
                    szHTTPStrArray = Split(sz_HTTPString, "&")
                ElseIf (InStr(sz_HTTPString, "$") > 0) Then
                    szHTTPStrArray = Split(sz_HTTPString, "$")
                End If
            Else
                If (InStr(sz_LogString, "&") > 0) Then
                    szHTTPStrArray = Split(sz_LogString, "&")
                ElseIf (InStr(sz_LogString, "$") > 0) Then
                    szHTTPStrArray = Split(sz_LogString, "$")
                End If
            End If

            For iLoop = 0 To szHTTPStrArray.GetUpperBound(0)
                'Added on 19 Aug 2009
                szHTTPStrArray(iLoop) = Trim(Replace(Replace(Replace(szHTTPStrArray(iLoop), vbCrLf, ""), vbCr, ""), vbLf, ""))
                If ("==" = Right(szHTTPStrArray(iLoop), 2)) Then
                    'Differentiate between "field==" from "field=h==" 
                    'Replace "field==" to "field=@" and "field=h==" to "field=h@@"
                    If ((szHTTPStrArray(iLoop).IndexOf("=") + 1) = (szHTTPStrArray(iLoop).Length() - 1)) Then
                        szHTTPStrArray(iLoop) = Left(szHTTPStrArray(iLoop), Len(szHTTPStrArray(iLoop)) - 1) + "@"
                    Else
                        szHTTPStrArray(iLoop) = Left(szHTTPStrArray(iLoop), Len(szHTTPStrArray(iLoop)) - 2) + "@@"
                    End If
                ElseIf ("=" = Right(szHTTPStrArray(iLoop), 1)) Then
                    'Differentiate between "field=" (empty value) from "field=value="
                    'Checks if "=" is the character at the last position of the field value OR, e.g. "field=value="
                    'if "=" is the field delimiter, e.g. "field="
                    'Make sure "=" does not exist at the last character of szHTTPStrArray or else
                    'the array result from the next Split by "=" will encounter out of bound exception
                    If (szHTTPStrArray(iLoop).IndexOf("=") <> szHTTPStrArray(iLoop).Length() - 1) Then
                        szHTTPStrArray(iLoop) = Left(szHTTPStrArray(iLoop), Len(szHTTPStrArray(iLoop)) - 1) + "@"
                    End If
                End If

                szFieldArray = Split(szHTTPStrArray(iLoop), "=")

                'Added on 19 Aug 2009
                If ("@@" = Right(szFieldArray(1), 2)) Then
                    szFieldArray(1) = Left(szFieldArray(1), Len(szFieldArray(1)) - 2) + "=="
                ElseIf ("@" = Right(szFieldArray(1), 1)) Then
                    szFieldArray(1) = Left(szFieldArray(1), Len(szFieldArray(1)) - 1) + "="
                End If

                'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), " Field(" + szFieldArray(0) + ") Value(" + szFieldArray(1) + ")")
            Next iLoop

            '=========================================== Post request ==================================================
            '- StreamWriter constructor creates a StreamWriter with UTF-8 encoding whose GetPreamble method returns an
            'empty byte array. The BaseStream property is initialized using the stream parameter (i.e. objHttpWebRequest.GetRequestStream)
            '- GetRequestStream method returns a stream to use to send data for the HttpWebRequest. Once the Stream
            'instance has been returned, you can send data with the HttpWebRequest by using the Stream.Write method.
            'NOTE:    Must set the value of the ContentLength property before retrieving the stream.
            'CAUTION: Must call the Stream.Close method to close the stream and release the connection for reuse.
            '         Failure to close the stream will cause application to run out of connections.

            objStreamWriter = New StreamWriter(objHttpWebRequest.GetRequestStream())
            objStreamWriter.Write(sbInStream.ToString)  ' Sends data
            objStreamWriter.Close() ' Closes the stream and release the connection for reuse to avoid running out of connections

            'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Finished posting HTTP data")

            '============================================= Get response =================================================
            'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Getting HTTP response....")

            '- GetResponse method returns a WebResponse instance containing the response from the Internet resource.
            'The actual instance returned is an instance of HttpWebResponse, and can be typecast to that class to access
            'HTTP-specific properties. When POST method is used, must get the request stream, write the data to be posted,
            'and close the stream. This method blocks waiting for content to post; If there is no time-out set and
            'content is not provided, application will block indefinitely.

            objHTTPWebResponse = objHttpWebRequest.GetResponse()

            If (objHttpWebRequest.HaveResponse And objHTTPWebResponse.StatusCode = HttpStatusCode.OK) Then
                'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "HTTP response received.")

                objStreamReader = New StreamReader(objHTTPWebResponse.GetResponseStream)

                '================ Implements Timeout for getting HTTP reply from host <START> ====================
                Dim objRespReader As RespReader
                Dim oThreadStart As ThreadStart
                Dim oThread As Thread
                Dim iElapsedTime As Integer

                objRespReader = New RespReader
                'objRespReader.SetLogger(objLoggerII)
                objRespReader.SetReader(objStreamReader)

                oThreadStart = New ThreadStart(AddressOf objRespReader.WaitForResp)
                oThread = New Thread(oThreadStart)
                oThread.Start()

                'iStartTickCount = System.Environment.TickCount()
                iElapsedTime = System.Environment.TickCount() - iStartTickCount

                While ((iElapsedTime < iTimeout) And Not objRespReader.IsCompleted())
                    System.Threading.Thread.Sleep(50)
                    iElapsedTime = System.Environment.TickCount() - iStartTickCount
                End While
                'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp Elapsed Time(ms): " & iElapsedTime.ToString())

                If (iElapsedTime >= iTimeout) Then
                    ' Timeout
                    iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                    'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp Timeout, iRet(" + iRet.ToString() + ")")
                Else
                    'Completed AND No Timeout. Could be got reply or exception
                    If (True = objRespReader.IsSucceeded()) Then
                        'Received reply within timeout period
                        sz_HTTPResponse = objRespReader.GetRespStream()
                    Else
                        'Exception within timeout period
                        iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                        'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp exception within timeout period, iRet(" + iRet.ToString() + ")")
                    End If
                End If

                oThread.Abort()
                oThreadStart = Nothing
                oThread = Nothing
                'objRespReader.Dispose()
                objRespReader = Nothing

                'Releases the resources of the response stream
                'NOTE: Failure to close the stream will cause application to run out of connections.
                objStreamReader.Close()
                If Not objStreamReader Is Nothing Then objStreamReader = Nothing 'Added on 1 Mar 2009
                '================== Implements Timeout for getting HTTP reply from host <END> =====================
            Else
                'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "ERROR: Response was not received.")
            End If

            'Releases the resources of the response
            objHTTPWebResponse.Close()

            iRet = 0

        Catch webEx As WebException
            Try
                'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException: " + webEx.ToString)

                iRet = Common.ERROR_CODE.E_COM_URL_UNREACHABLE

                If webEx.Status = WebExceptionStatus.Timeout Then
                    iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                    'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException: Timeout")
                End If

                If webEx.Status = WebExceptionStatus.ProtocolError Then
                    'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Response Status Code :" + CStr(CType(webEx.Response, HttpWebResponse).StatusCode))

                    'Re: HttpStatusCode Enumeration
                    iResCode = CInt(CType(webEx.Response, HttpWebResponse).StatusCode)
                    If iResCode = 400 Then
                        iRet = Common.ERROR_CODE.E_COM_BAD_REQUEST
                        'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Bad Request could not be understood by the server.")
                    ElseIf iResCode = 401 Then
                        iRet = Common.ERROR_CODE.E_COM_UNAUTHORIZED_URL
                        'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Unauthorized, the requested resource requires authentication.")
                    ElseIf iResCode = 403 Then
                        iRet = Common.ERROR_CODE.E_COM_FORBIDDEN_URL
                        'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Forbidden, the server refuses to fulfill the request.")
                    ElseIf iResCode = 404 Then
                        iRet = Common.ERROR_CODE.E_COM_URL_UNREACHABLE
                        'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Not Found, the requested resource does not exist on the server.")
                    End If
                End If
            Catch exp As Exception
                'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Exception: " + exp.ToString)
            End Try

        Catch ioEx As IOException
            'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "IO Exception: " + ioEx.ToString)
            iRet = Common.ERROR_CODE.E_COM_FILE_ERROR

        Catch ex As Exception
            'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Exception: " + ex.ToString)
            iRet = Common.ERROR_CODE.E_COMMON_UNKNOWN_ERROR

        Finally
            'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "iRet: " + CStr(iRet))
        End Try

        Return iRet
    End Function

    '********************************************************************************************
    ' Class Name:		CHTTP
    ' Function Name:	iHTTPostWithRecvEx
    ' Function Type:	boolean
    ' Parameter:        sz_URL [in, string]             - Posting URL
    '                   sz_HTTPString [in, string]      - HTTP string to be posted
    '                   i_Timeout [in, integer]         - Connection timeout(in second)
    '                   sz_HTTPResponse [out, string]   - HTTP string received
    ' Description:		Perform HTTP posting
    ' History:			16 Jun 2010
    '********************************************************************************************
    Public Function iHTTPostWithRecvEx(ByVal sz_URL As String, ByVal sz_HTTPString As String, ByVal i_Timeout As Integer, ByVal sz_ContentType As String, ByRef sz_HTTPResponse As String) As Integer
        Dim iRet As Integer = -1
        Dim objHttpWebRequest As HttpWebRequest
        Dim objHTTPWebResponse As HttpWebResponse
        Dim objStreamWriter As StreamWriter
        Dim objStreamReader As StreamReader
        Dim sbInStream As System.Text.StringBuilder
        Dim iStartTickCount As Integer
        Dim iTimeout As Integer
        Dim iResCode As Integer
        Dim iLoop As Integer
        Dim szHTTPStrArray() As String
        Dim szFieldArray() As String

        Try
            '======================================== Sets HttpWebRequest properties ====================================
            sbInStream = New System.Text.StringBuilder
            sbInStream.Append(sz_HTTPString)
            iTimeout = i_Timeout * 1000 'converts input timeout param from second to milliseconds

            objHttpWebRequest = WebRequest.Create(sz_URL)                       'Posting URL
            objHttpWebRequest.ContentType = sz_ContentType
            objHttpWebRequest.Method = "POST"                                   'default value
            objHttpWebRequest.ContentLength = sbInStream.Length ' Must be set b4 retrieving stream for sending data
            objHttpWebRequest.KeepAlive = False
            objHttpWebRequest.Timeout = iTimeout                                '(in milliseconds)

            'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Timeout period(" + iTimeout.ToString() + " ms) Posting HTTP data " + sz_HTTPString + " to " + sz_URL)

            'Ignore invalid server certificates or else may encounter WebException with e.Status = WebExceptionStatus.TrustFailure
            'When the CertificatePolicy property is set to an ICertificatePolicy interface instance, the ServicePointManager
            'uses the certificate policy defined in that instance instead of the default certificate policy.
            'The default certificate policy allows valid certificates, as well as valid certificates that have expired.
            ServicePointManager.CertificatePolicy = New SecPolicy

            iStartTickCount = System.Environment.TickCount()

            If (InStr(sz_HTTPString, "&") > 0) Then
                szHTTPStrArray = Split(sz_HTTPString, "&")
            ElseIf (InStr(sz_HTTPString, "$") > 0) Then
                szHTTPStrArray = Split(sz_HTTPString, "$")
            Else 'Added by Jazz, 17 Feb 2011, for Mandiri, only one string which does not contain "&"/"$"
                ReDim szHTTPStrArray(-1)
            End If

            For iLoop = 0 To szHTTPStrArray.GetUpperBound(0)
                'Added on 19 Aug 2009
                szHTTPStrArray(iLoop) = Trim(Replace(Replace(Replace(szHTTPStrArray(iLoop), vbCrLf, ""), vbCr, ""), vbLf, ""))
                If ("==" = Right(szHTTPStrArray(iLoop), 2)) Then
                    'Differentiate between "field==" from "field=h==" 
                    'Replace "field==" to "field=@" and "field=h==" to "field=h@@"
                    If ((szHTTPStrArray(iLoop).IndexOf("=") + 1) = (szHTTPStrArray(iLoop).Length() - 1)) Then
                        szHTTPStrArray(iLoop) = Left(szHTTPStrArray(iLoop), Len(szHTTPStrArray(iLoop)) - 1) + "@"
                    Else
                        szHTTPStrArray(iLoop) = Left(szHTTPStrArray(iLoop), Len(szHTTPStrArray(iLoop)) - 2) + "@@"
                    End If
                ElseIf ("=" = Right(szHTTPStrArray(iLoop), 1)) Then
                    'Differentiate between "field=" (empty value) from "field=value="
                    'Checks if "=" is the character at the last position of the field value OR, e.g. "field=value="
                    'if "=" is the field delimiter, e.g. "field="
                    'Make sure "=" does not exist at the last character of szHTTPStrArray or else
                    'the array result from the next Split by "=" will encounter out of bound exception
                    If (szHTTPStrArray(iLoop).IndexOf("=") <> szHTTPStrArray(iLoop).Length() - 1) Then
                        szHTTPStrArray(iLoop) = Left(szHTTPStrArray(iLoop), Len(szHTTPStrArray(iLoop)) - 1) + "@"
                    End If
                End If

                szFieldArray = Split(szHTTPStrArray(iLoop), "=")

                'Added on 19 Aug 2009
                If ("@@" = Right(szFieldArray(1), 2)) Then
                    szFieldArray(1) = Left(szFieldArray(1), Len(szFieldArray(1)) - 2) + "=="
                ElseIf ("@" = Right(szFieldArray(1), 1)) Then
                    szFieldArray(1) = Left(szFieldArray(1), Len(szFieldArray(1)) - 1) + "="
                End If

                'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), " Field(" + szFieldArray(0) + ") Value(" + szFieldArray(1) + ")")
            Next iLoop

            '=========================================== Post request ==================================================
            '- StreamWriter constructor creates a StreamWriter with UTF-8 encoding whose GetPreamble method returns an
            'empty byte array. The BaseStream property is initialized using the stream parameter (i.e. objHttpWebRequest.GetRequestStream)
            '- GetRequestStream method returns a stream to use to send data for the HttpWebRequest. Once the Stream
            'instance has been returned, you can send data with the HttpWebRequest by using the Stream.Write method.
            'NOTE:    Must set the value of the ContentLength property before retrieving the stream.
            'CAUTION: Must call the Stream.Close method to close the stream and release the connection for reuse.
            '         Failure to close the stream will cause application to run out of connections.

            objStreamWriter = New StreamWriter(objHttpWebRequest.GetRequestStream())
            objStreamWriter.Write(sbInStream.ToString)  ' Sends data
            objStreamWriter.Close() ' Closes the stream and release the connection for reuse to avoid running out of connections

            'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Finished posting HTTP data")

            '============================================= Get response =================================================
            'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Getting HTTP response....")

            '- GetResponse method returns a WebResponse instance containing the response from the Internet resource.
            'The actual instance returned is an instance of HttpWebResponse, and can be typecast to that class to access
            'HTTP-specific properties. When POST method is used, must get the request stream, write the data to be posted,
            'and close the stream. This method blocks waiting for content to post; If there is no time-out set and
            'content is not provided, application will block indefinitely.

            objHTTPWebResponse = objHttpWebRequest.GetResponse()

            If (objHttpWebRequest.HaveResponse And objHTTPWebResponse.StatusCode = HttpStatusCode.OK) Then
                'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "HTTP response received.")

                objStreamReader = New StreamReader(objHTTPWebResponse.GetResponseStream)

                '================ Implements Timeout for getting HTTP reply from host <START> ====================
                Dim objRespReader As RespReader
                Dim oThreadStart As ThreadStart
                Dim oThread As Thread
                Dim iElapsedTime As Integer

                objRespReader = New RespReader
                'objRespReader.SetLogger(objLoggerII)
                objRespReader.SetReader(objStreamReader)

                oThreadStart = New ThreadStart(AddressOf objRespReader.WaitForResp)
                oThread = New Thread(oThreadStart)
                oThread.Start()

                'iStartTickCount = System.Environment.TickCount()
                iElapsedTime = System.Environment.TickCount() - iStartTickCount

                While ((iElapsedTime < iTimeout) And Not objRespReader.IsCompleted())
                    System.Threading.Thread.Sleep(50)
                    iElapsedTime = System.Environment.TickCount() - iStartTickCount
                End While
                'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp Elapsed Time(ms): " & iElapsedTime.ToString())

                If (iElapsedTime >= iTimeout) Then
                    ' Timeout
                    iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                    'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp Timeout, iRet(" + iRet.ToString() + ")")
                Else
                    'Completed AND No Timeout. Could be got reply or exception
                    If (True = objRespReader.IsSucceeded()) Then
                        'Received reply within timeout period
                        sz_HTTPResponse = objRespReader.GetRespStream()
                    Else
                        'Exception within timeout period
                        iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                        'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp exception within timeout period, iRet(" + iRet.ToString() + ")")
                    End If
                End If

                oThread.Abort()
                oThreadStart = Nothing
                oThread = Nothing
                'objRespReader.Dispose()
                objRespReader = Nothing

                'Releases the resources of the response stream
                'NOTE: Failure to close the stream will cause application to run out of connections.
                objStreamReader.Close()
                If Not objStreamReader Is Nothing Then objStreamReader = Nothing 'Added on 1 Mar 2009
                '================== Implements Timeout for getting HTTP reply from host <END> =====================
            Else
                'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "ERROR: Response was not received.")
            End If

            'Releases the resources of the response
            objHTTPWebResponse.Close()

            iRet = 0

        Catch webEx As WebException
            Try
                'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException: " + webEx.ToString)

                iRet = Common.ERROR_CODE.E_COM_URL_UNREACHABLE

                If webEx.Status = WebExceptionStatus.Timeout Then
                    iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                    'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException: Timeout")
                End If

                If webEx.Status = WebExceptionStatus.ProtocolError Then
                    'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Response Status Code :" + CStr(CType(webEx.Response, HttpWebResponse).StatusCode))

                    'Re: HttpStatusCode Enumeration
                    iResCode = CInt(CType(webEx.Response, HttpWebResponse).StatusCode)
                    If iResCode = 400 Then
                        iRet = Common.ERROR_CODE.E_COM_BAD_REQUEST
                        'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Bad Request could not be understood by the server.")
                    ElseIf iResCode = 401 Then
                        iRet = Common.ERROR_CODE.E_COM_UNAUTHORIZED_URL
                        'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Unauthorized, the requested resource requires authentication.")
                    ElseIf iResCode = 403 Then
                        iRet = Common.ERROR_CODE.E_COM_FORBIDDEN_URL
                        'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Forbidden, the server refuses to fulfill the request.")
                    ElseIf iResCode = 404 Then
                        iRet = Common.ERROR_CODE.E_COM_URL_UNREACHABLE
                        'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Not Found, the requested resource does not exist on the server.")
                    End If
                End If
            Catch exp As Exception
                'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Exception: " + exp.ToString)
            End Try

        Catch ioEx As IOException
            'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "IO Exception: " + ioEx.ToString)
            iRet = Common.ERROR_CODE.E_COM_FILE_ERROR

        Catch ex As Exception
            'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Exception: " + ex.ToString)
            iRet = Common.ERROR_CODE.E_COMMON_UNKNOWN_ERROR

        Finally
            'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "iRet: " + CStr(iRet))
        End Try

        Return iRet
    End Function

    '********************************************************************************************
    ' Class Name:		CHTTP
    ' Function Name:	iHTTPGetWithRecv
    ' Function Type:	boolean
    ' Parameter:        sz_URL [in, string]             - Posting URL
    '                   sz_HTTPString [in, string]      - HTTP string to be posted
    '                   i_Timeout [in, integer]         - Connection timeout(in second)
    '                   sz_HTTPResponse [out, string]   - HTTP string received
    ' Description:		Perform HTTP Get posting
    ' History:			22 April 2009   
    '********************************************************************************************
    Public Function iHTTPGetWithRecv(ByVal sz_URL As String, ByVal sz_HTTPString As String, ByVal i_Timeout As Integer, ByRef sz_HTTPResponse As String) As Integer
        Dim iRet As Integer = -1
        Dim iTimeout As Integer
        Dim objHttpWebRequest As HttpWebRequest
        Dim objHTTPWebResponse As HttpWebResponse = Nothing
        Dim objStreamReader As StreamReader
        Dim szStr As String = ""
        Dim szHTTPStrArray() As String
        Dim szFieldArray() As String
        Dim iStartTickCount As Integer
        Dim iLoop As Integer
        Dim iResCode As Integer

        Try
            '======================================== Sets HttpWebRequest properties ====================================
            iTimeout = i_Timeout * 1000 'converts input timeout param from second to milliseconds

            'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Timeout period(" + iTimeout.ToString() + " ms) Posting HTTP data " + sz_HTTPString + " to " + sz_URL)

            ServicePointManager.CertificatePolicy = New SecPolicy

            iStartTickCount = System.Environment.TickCount()

            If (InStr(sz_HTTPString, "&") > 0) Then
                szHTTPStrArray = Split(sz_HTTPString, "&")
            ElseIf (InStr(sz_HTTPString, "$") > 0) Then
                szHTTPStrArray = Split(sz_HTTPString, "$")
            End If

            For iLoop = 0 To szHTTPStrArray.GetUpperBound(0)
                'Added on 19 Aug 2009
                szHTTPStrArray(iLoop) = Trim(Replace(Replace(Replace(szHTTPStrArray(iLoop), vbCrLf, ""), vbCr, ""), vbLf, ""))
                If ("==" = Right(szHTTPStrArray(iLoop), 2)) Then
                    'Differentiate between "field==" from "field=h==" 
                    'Replace "field==" to "field=@" and "field=h==" to "field=h@@"
                    If ((szHTTPStrArray(iLoop).IndexOf("=") + 1) = (szHTTPStrArray(iLoop).Length() - 1)) Then
                        szHTTPStrArray(iLoop) = Left(szHTTPStrArray(iLoop), Len(szHTTPStrArray(iLoop)) - 1) + "@"
                    Else
                        szHTTPStrArray(iLoop) = Left(szHTTPStrArray(iLoop), Len(szHTTPStrArray(iLoop)) - 2) + "@@"
                    End If
                ElseIf ("=" = Right(szHTTPStrArray(iLoop), 1)) Then
                    'Differentiate between "field=" (empty value) from "field=value="
                    'Checks if "=" is the character at the last position of the field value OR, e.g. "field=value="
                    'if "=" is the field delimiter, e.g. "field="
                    'Make sure "=" does not exist at the last character of szHTTPStrArray or else
                    'the array result from the next Split by "=" will encounter out of bound exception
                    If (szHTTPStrArray(iLoop).IndexOf("=") <> szHTTPStrArray(iLoop).Length() - 1) Then
                        szHTTPStrArray(iLoop) = Left(szHTTPStrArray(iLoop), Len(szHTTPStrArray(iLoop)) - 1) + "@"
                    End If
                End If

                szFieldArray = Split(szHTTPStrArray(iLoop), "=")

                'Added on 19 Aug 2009
                If ("@@" = Right(szFieldArray(1), 2)) Then
                    szFieldArray(1) = Left(szFieldArray(1), Len(szFieldArray(1)) - 2) + "=="
                ElseIf ("@" = Right(szFieldArray(1), 1)) Then
                    szFieldArray(1) = Left(szFieldArray(1), Len(szFieldArray(1)) - 1) + "="
                End If

                'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), " Field(" + szFieldArray(0) + ") Value(" + szFieldArray(1) + ")")
            Next iLoop

            '=========================================== Post(GET) request ==================================================
            ' Create the web request   
            objHttpWebRequest = DirectCast(WebRequest.Create(sz_URL + "?" + sz_HTTPString), HttpWebRequest)  ' Sends data

            objHttpWebRequest.KeepAlive = False
            objHttpWebRequest.Timeout = iTimeout    '(in milliseconds)

            'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Finished posting HTTP data")

            '============================================= Get response =====================================================
            'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Getting HTTP response....")

            ' Get response   
            objHTTPWebResponse = DirectCast(objHttpWebRequest.GetResponse(), HttpWebResponse)

            If (objHttpWebRequest.HaveResponse And objHTTPWebResponse.StatusCode = HttpStatusCode.OK) Then
                'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "HTTP response received.")

                ' Get the response stream into a reader   
                objStreamReader = New StreamReader(objHTTPWebResponse.GetResponseStream())

                szStr = objStreamReader.ReadToEnd()

                '================ Implements Timeout for getting HTTP reply from host <START> ====================
                Dim objRespReader As RespReader
                Dim oThreadStart As ThreadStart
                Dim oThread As Thread
                Dim iElapsedTime As Integer

                objRespReader = New RespReader
                'objRespReader.SetLogger(objLoggerII)
                objRespReader.SetReader(objStreamReader)

                oThreadStart = New ThreadStart(AddressOf objRespReader.WaitForResp)
                oThread = New Thread(oThreadStart)
                oThread.Start()

                'iStartTickCount = System.Environment.TickCount()
                iElapsedTime = System.Environment.TickCount() - iStartTickCount

                While ((iElapsedTime < iTimeout) And Not objRespReader.IsCompleted())
                    System.Threading.Thread.Sleep(50)
                    iElapsedTime = System.Environment.TickCount() - iStartTickCount
                End While
                'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp Elapsed Time(ms): " & iElapsedTime.ToString())

                If (iElapsedTime >= iTimeout) Then
                    ' Timeout
                    iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                    'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp Timeout, iRet(" + iRet.ToString() + ")")
                Else
                    'Completed AND No Timeout. Could be got reply or exception
                    If (True = objRespReader.IsSucceeded()) Then
                        ' Console application output   
                        sz_HTTPResponse = szStr
                    Else
                        'Exception within timeout period
                        iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                        'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp exception within timeout period, iRet(" + iRet.ToString() + ")")
                    End If
                End If

                oThread.Abort()
                oThreadStart = Nothing
                oThread = Nothing
                'objRespReader.Dispose()
                objRespReader = Nothing

                'Releases the resources of the response stream
                'NOTE: Failure to close the stream will cause application to run out of connections.
                objStreamReader.Close()
                If Not objStreamReader Is Nothing Then objStreamReader = Nothing
                '================== Implements Timeout for getting HTTP reply from host <END> =====================
            Else
                'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "ERROR: Response was not received.")
            End If

            'Releases the resources of the response
            objHTTPWebResponse.Close()

            iRet = 0

        Catch webEx As WebException
            Try
                'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException: " + webEx.ToString)

                iRet = Common.ERROR_CODE.E_COM_URL_UNREACHABLE

                If webEx.Status = WebExceptionStatus.Timeout Then
                    iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                    'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException: Timeout")
                End If

                If webEx.Status = WebExceptionStatus.ProtocolError Then
                    'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Response Status Code :" + CStr(CType(webEx.Response, HttpWebResponse).StatusCode))

                    'Re: HttpStatusCode Enumeration
                    iResCode = CInt(CType(webEx.Response, HttpWebResponse).StatusCode)
                    If iResCode = 400 Then
                        iRet = Common.ERROR_CODE.E_COM_BAD_REQUEST
                        'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Bad Request could not be understood by the server.")
                    ElseIf iResCode = 401 Then
                        iRet = Common.ERROR_CODE.E_COM_UNAUTHORIZED_URL
                        'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Unauthorized, the requested resource requires authentication.")
                    ElseIf iResCode = 403 Then
                        iRet = Common.ERROR_CODE.E_COM_FORBIDDEN_URL
                        'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Forbidden, the server refuses to fulfill the request.")
                    ElseIf iResCode = 404 Then
                        iRet = Common.ERROR_CODE.E_COM_URL_UNREACHABLE
                        'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WebException - Not Found, the requested resource does not exist on the server.")
                    End If
                End If
            Catch exp As Exception
                'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Exception: " + exp.ToString)
            End Try

        Catch ioEx As IOException
            'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "IO Exception: " + ioEx.ToString)
            iRet = Common.ERROR_CODE.E_COM_FILE_ERROR

        Catch ex As Exception
            'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "Exception: " + ex.ToString)
            iRet = Common.ERROR_CODE.E_COMMON_UNKNOWN_ERROR

        Finally
            'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "iRet: " + CStr(iRet))
        End Try

        Return iRet
    End Function

    Public Sub New(ByRef o_LoggerII As LoggerII.CLoggerII)
        Try
            objLoggerII = o_LoggerII
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class
