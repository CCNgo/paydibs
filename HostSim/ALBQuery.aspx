﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ALBQuery.aspx.vb" Inherits="HostSim.ALBQuery" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="frmQuery" runat="server" style="text-align:center">
        <br /><br />
        <h3>Alliance Bank Transaction Inquiry</h3>
        <asp:Panel ID="Panel1" runat="server">
            <div>
                <asp:Label ID="Label2" runat="server" Text="Merchant : " style="margin-bottom:10px"></asp:Label>
                <asp:DropDownList ID="DropDownList1" runat="server" style="margin-bottom:10px">
                    <asp:ListItem Value="TwoSpicy"></asp:ListItem>
                    <asp:ListItem Value="ACS Ent."></asp:ListItem>
                    <asp:ListItem Value="Zcova"></asp:ListItem>
                </asp:DropDownList>
                <br />
                <asp:Label ID="Label1" runat="server" Text="TranRunNo : "></asp:Label>
                <asp:TextBox ID="TxnRunNo" runat="server"></asp:TextBox><br />
                <asp:Button ID="Button1" runat="server" Text="Submit" BackColor="Khaki" style="margin-top:20px"/>
            </div>
        </asp:Panel><br /><br />
        <asp:Panel ID="Panel2" runat="server">
            <div>
                <asp:Label ID="Label3" runat="server" Text="" Visible="false"></asp:Label>
            </div>
        </asp:Panel>
    </form>
</body>
</html>
