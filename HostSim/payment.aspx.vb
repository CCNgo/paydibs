﻿Imports LoggerII
Imports System.IO
Imports System
Imports System.Data
Imports System.Security.Cryptography
Imports System.Threading
Imports System.Text.RegularExpressions

Partial Public Class payment
    Inherits System.Web.UI.Page

    Public g_szHTML As String = ""

    Public objLoggerII As LoggerII.CLoggerII
    Private objCommon As Common
    Private objHTTP As CHTTP
    Private objHash As Hash

    Private szLogPath As String = ""
    Private iLogLevel As String = ""

    Private stHostInfo As Common.STHost
    Private stPayInfo As Common.STPayInfo

    Private szReqStr As String = ""
    Private szReqStatus As String = ""
    Private szURL As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Set to No Cache to make this page expired when user clicks the "back" button on browser
            Response.Cache.SetNoStore()

            If Not Page.IsPostBack Then
                'Initialization of HostInfo and PayInfo structures
                objCommon.InitHostInfo(stHostInfo)
                objCommon.InitPayInfo(stPayInfo)

                If True <> bPaymentMain() Then
                    PaymentErrHandler()
                End If
            End If

        Catch ex As Exception

        End Try
    End Sub

    '********************************************************************************************
    ' Class Name:		Payment
    ' Function Name:	bPaymentMain
    ' Function Type:	Boolean.  Return False if error.
    ' Parameter:		NONE
    ' Description:		A main function to get the request info and handle the process
    ' History:			4 Oct 2008
    '********************************************************************************************
    Private Function bPaymentMain() As Boolean
        Dim bReturn = True

        Try
            'Log all form's request fields
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                           DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "Req: " + szGetAllHTTPVal())

            'Get Request data
            If True <> bGetData() Then
                Return False
            End If

            'Check TxnType
            If stPayInfo.szTxnType.ToLower = "pay" Then
                If (stPayInfo.szPymtMethod.ToLower = "ob") Then
                    If (szReqStatus = "") Then
                        If True <> bProcessOB(szURL, szReqStr) Then
                            Return False
                        End If
                    Else
                        If True <> bProcessSale() Then
                            Return False
                        End If
                    End If
                Else
                    If True <> bProcessSale() Then
                        Return False
                    End If
                End If

            ElseIf stPayInfo.szTxnType.ToLower = "query" Or stPayInfo.szTxnType.ToLower = "void" Then
                If True <> bProcessQuery() Then
                    Return False
                End If

                'ElseIf stPayInfo.szTxnType.ToLower = "rsale" Then
                '    If True <> bProcessRSale() Then  'Uncommented on 10 Apr 2010
                '        Return False
                '    End If
            Else
                'Error: Invalid TxnType
                stPayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                stPayInfo.iTxnStatus = Common.TXN_STATUS.INVALID_INPUT 'Common.ERROR_CODE.E_COMMON_MISSING_ELEMENT
                stPayInfo.szTxnMsg = Common.C_COMMON_MISSING_ELEMENT_2903 '"Internal error: Missing element"
                stPayInfo.iTxnState = Common.TXN_STATE.ABORT
                stPayInfo.szErrDesc = "Error: Invalid TxnType"

                Return False
            End If
        Catch ex As Exception
            bReturn = False
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		Payment
    ' Function Name:	bGetData
    ' Function Type:	NONE
    ' Parameter:		NONE
    ' Description:		Data collection from the HTTP POST and GET collection.
    ' History:			4 Oct 2008
    '********************************************************************************************
    Private Function bGetData() As Boolean
        Dim bReturn As Boolean = False

        Try
            'Request Object (Re: Microsoft Visual Studio.NET 2003 Documentation - Request Object object[IIS] described)
            'The Request object retrieves the values that the client browser passed to the server during an HTTP request.
            'If the specified variable is not in one of the preceding five collections, the Request object returns EMPTY.
            'All variables can be accessed directly by calling Request(variable) without the collection name. In this case, the Web server searches the collections in the following order:
            '1.QueryString
            '2.Form
            '3.Cookies
            '4.ClientCertificate
            '5.ServerVariables
            'If a variable with the same name exists in more than one collection, the Request object returns the first instance that the object encounters.
            'It is strongly recommended that when referring to members of a collection the full name be used. For example, rather than Request.("AUTH_USER") use Request.ServerVariables("AUTH_USER"). This allows the server to locate the item more quickly.

            'Checks whether it is 2nd entry
            stPayInfo.szRecv2ndEntry = Request("SecondEntry")
            If ("" = stPayInfo.szRecv2ndEntry) Then
                If Trim(Request("TxnType")) <> "" Or Trim(Request("TransactionType")) <> "" Then
                    If Trim(Request("TxnType")) <> "" Then
                        stPayInfo.szTxnType = Trim(Request("TxnType"))
                    ElseIf Trim(Request("TransactionType")) <> "" Then
                        stPayInfo.szTxnType = Trim(Request("TransactionType"))
                    End If

                    If (stPayInfo.szTxnType.Length > 7) Then
                        stPayInfo.szTxnType = stPayInfo.szTxnType.Substring(0, 7)
                    End If
                Else
                    stPayInfo.szErrDesc = "Required field is missing: TxnType."
                    stPayInfo.szTxnMsg = "Required field(s) is missing."
                    GoTo Cleanup
                End If

                If ("" = Trim(Request("Param2"))) Then
                    If ("PAY" = stPayInfo.szTxnType.ToUpper()) Then
                        stPayInfo.szPymtMethod = "OB"
                        If ("" <> Trim(Request("Status"))) Then '2nd
                            szReqStatus = Trim(Request("Status"))
                            If (False = bGetSaleReqData()) Then
                                GoTo Cleanup
                            End If
                        Else '1st
                            szURL = ConfigurationManager.AppSettings("PymtURL")
                        End If
                    ElseIf ("QUERY" = stPayInfo.szTxnType.ToUpper()) Then
                        If (False = bGetQueryReqData()) Then
                            GoTo Cleanup
                        End If
                    ElseIf ("VOID" = stPayInfo.szTxnType.ToUpper()) Then
                        If (False = bGetRSaleReqData()) Then
                            GoTo Cleanup
                        End If
                    Else
                        stPayInfo.szErrDesc = "Error: Invalid TxnType"
                        stPayInfo.szTxnMsg = "Invalid TxnType"
                        GoTo Cleanup
                    End If
                    
                Else  'CC
                    If (False = bGetSaleReqData()) Then
                        GoTo Cleanup
                    End If
                End If


                'If ("PAY" = stPayInfo.szTxnType.ToUpper()) Then
                '    If (False = bGetSaleReqData()) Then
                '        GoTo Cleanup
                '    End If
                'ElseIf ("QUERY" = stPayInfo.szTxnType.ToUpper()) Then
                '    If (False = bGetQueryReqData()) Then
                '        GoTo Cleanup
                '    End If
                'ElseIf ("VOID" = stPayInfo.szTxnType.ToUpper()) Then
                '    If (False = bGetRSaleReqData()) Then
                '        GoTo Cleanup
                '    End If
                'Else
                '    stPayInfo.szErrDesc = "Error: Invalid TxnType"
                '    stPayInfo.szTxnMsg = "Invalid TxnType"
                '    GoTo Cleanup
                'End If
            Else
                'Put logic here for 2nd entry, e.g. UserID(BCA); Tokens info(Mandiri); Customer ID(SCB)
                'Get payment details for the 1st entry from database based on GatewayTxnID, can refer to AASPS's Pay.aspx.vb
                'If (False = bGet2ndEntryReqData()) Then
                '    GoTo Cleanup
                'End If
            End If

            bReturn = True

        Catch ex As Exception
            stPayInfo.szErrDesc = "Error: Fail to get request data."
            stPayInfo.szTxnMsg = "System Error: Fail to get request data."
            bReturn = False
        End Try

Cleanup:
        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		Payment
    ' Function Name:	bGetSaleReqData
    ' Function Type:	Boolean True if success else false
    ' Parameter:		
    ' Description:		Gets Sale request data
    ' History:			8 Dec 2008
    '********************************************************************************************
    Private Function bGetSaleReqData() As Boolean
        Dim bReturn As Boolean = False
        Dim szServiceID As String = ""
        Dim szMerchantTxnID As String = ""
        Dim szMerchantReturnURL As String = ""
        Dim szTxnAmount As String = ""
        Dim szCurrencyCode As String = ""
        Dim szBaseTxnAmount As String = ""      'Added on 8 Aug 2009, to cater for base currency booking confirmation enhancement
        Dim szBaseCurrencyCode As String = ""   'Added on 8 Aug 2009, to cater for base currency booking confirmation enhancement
        Try
            stPayInfo.szHashKey = ""

            If Trim(Request("MerchantReturnURL")) <> "" Then
                ' Modified on 26 Nov 2008, replaces ";" with "&" to cater for MerchantReturnURL that contains extra
                ' param, e.g. &MerchantReturnURL=https://airasia.com?page=1&event=response. "&event" is the extra
                ' param for the above MerchantReturnURL. Request("MerchantReturnURL") will be able to get
                ' https://airasia.com?page=1 without getting the extra param "event=response", "&event" will be
                ' treated as another param that does not belong to "MerchantReturnURL" field.
                ' Therefore, if merchant would still like to have extra field in "MerchantReturnURL" field,
                ' they have to use ";" instead of "&". Gateway will replace ";" to "&".

                ' Gets the MerchantReturnURL before reformatting
                szMerchantReturnURL = Server.UrlDecode(Request("MerchantReturnURL"))
                ' Reformats MerchantReturnURL
                stPayInfo.szMerchantReturnURL = Trim(szMerchantReturnURL).Replace(";", "&")

                If (stPayInfo.szMerchantReturnURL.Length > 255) Then
                    stPayInfo.szMerchantReturnURL = stPayInfo.szMerchantReturnURL.Substring(0, 255)
                End If
                'Else
                '    stPayInfo.szErrDesc = "Required field is missing: MerchantReturnURL."
                '    stPayInfo.szTxnMsg = "Required field(s) is missing."
                '    GoTo Cleanup
            End If

            If Trim(Request("MerchantSupportURL")) <> "" Then
                stPayInfo.szMerchantSupportURL = Server.UrlDecode(Trim(Request("MerchantSupportURL"))).Replace(";", "&")

                If (stPayInfo.szMerchantSupportURL.Length > 255) Then
                    stPayInfo.szMerchantSupportURL = stPayInfo.szMerchantSupportURL.Substring(0, 255)
                End If
            End If

            If Trim(Request("ServiceID")) <> "" Then
                ' Gets the ServiceID before reformatting
                szServiceID = Server.UrlDecode(Request("ServiceID"))
                stPayInfo.szServiceID = Trim(szServiceID)

                'If (stPayInfo.szServiceID.Length > 3) Then
                '    stPayInfo.szServiceID = stPayInfo.szServiceID.Substring(0, 3)
                'End If
                'Else
                '    stPayInfo.szErrDesc = "Required field is missing: ServiceID."
                '    stPayInfo.szTxnMsg = "Required field(s) is missing."
                '    GoTo Cleanup
            End If

            If Trim(Request("MerchantName")) <> "" Then
                stPayInfo.szMerchantName = Server.UrlDecode(Trim(Request("MerchantName")))

                If (stPayInfo.szMerchantName.Length > 25) Then
                    stPayInfo.szMerchantName = stPayInfo.szMerchantName.Substring(0, 25)
                End If
                'Else
                '    stPayInfo.szErrDesc = "Required field is missing: MerchantName."
                '    stPayInfo.szTxnMsg = "Required field(s) is missing."
                '    GoTo Cleanup
            End If

            If Trim(Request("GatewayID")) <> "" Then
                stPayInfo.szGatewayID = Trim(Request("GatewayID"))

                If (stPayInfo.szGatewayID.Length > 4) Then
                    stPayInfo.szGatewayID = stPayInfo.szGatewayID.Substring(0, 4)
                End If
            Else
                stPayInfo.szGatewayID = "1" ' Default value, modified from "AA" to "1" on 10 Mar 2010
            End If

            If Trim(Request("MerchantTxnID")) <> "" Or Trim(Request("PaymentID")) <> "" Then
                ' Gets MerchantTxnID before reformatting
                If Trim(Request("MerchantTxnID")) <> "" Then
                    szMerchantTxnID = Server.UrlDecode(Request("MerchantTxnID"))
                    stPayInfo.szMerchantTxnID = Trim(szMerchantTxnID)

                    If (stPayInfo.szMerchantTxnID.Length > 30) Then
                        stPayInfo.szMerchantTxnID = stPayInfo.szMerchantTxnID.Substring(0, 30)
                    End If
                ElseIf Trim(Request("PaymentID")) <> "" Then
                    szMerchantTxnID = Server.UrlDecode(Request("PaymentID"))
                    stPayInfo.szMerchantTxnID = Trim(szMerchantTxnID)

                    If (stPayInfo.szMerchantTxnID.Length > 30) Then
                        stPayInfo.szMerchantTxnID = stPayInfo.szMerchantTxnID.Substring(0, 30)
                    End If
                End If
                'Else
                '    stPayInfo.szErrDesc = "Required field is missing: MerchantTxnID/PaymentID."
                '    stPayInfo.szTxnMsg = "Required field(s) is missing."
                '    GoTo Cleanup
            End If

            If Trim(Request("OrderNumber")) <> "" Then
                stPayInfo.szOrderNumber = Server.UrlDecode(Trim(Request("OrderNumber")))

                If (stPayInfo.szOrderNumber.Length > 20) Then
                    stPayInfo.szOrderNumber = stPayInfo.szOrderNumber.Substring(0, 20)
                End If
            Else
                stPayInfo.szOrderNumber = ""
            End If

            'PaymentDesc
            If Trim(Request("OrderDesc")) <> "" Or Trim(Request("PaymentDesc")) <> "" Then
                If Trim(Request("OrderDesc")) <> "" Then
                    stPayInfo.szOrderDesc = Server.UrlDecode(Request("OrderDesc"))

                    If (stPayInfo.szOrderDesc.Length > 100) Then
                        stPayInfo.szOrderDesc = stPayInfo.szOrderDesc.Substring(0, 100)
                    End If
                ElseIf Trim(Request("PaymentDesc")) <> "" Then
                    stPayInfo.szOrderDesc = Server.UrlDecode(Request("PaymentDesc"))

                    If (stPayInfo.szOrderDesc.Length > 100) Then
                        stPayInfo.szOrderDesc = stPayInfo.szOrderDesc.Substring(0, 100)
                    End If
                End If
            End If

            If Trim(Request("TxnAmount")) <> "" Or Trim(Request("Amount")) <> "" Then
                If Trim(Request("TxnAmount")) <> "" Then
                    szTxnAmount = Request("TxnAmount")
                    stPayInfo.szTxnAmount = Trim(szTxnAmount)
                ElseIf Trim(Request("Amount")) <> "" Then
                    szTxnAmount = Request("Amount")
                    stPayInfo.szTxnAmount = Trim(szTxnAmount)
                End If

                If False = bValidateAmount(stPayInfo.szTxnAmount) Then
                    stPayInfo.szErrDesc = "Required field is invalid: TxnAmount."
                    stPayInfo.szTxnMsg = "Required field(s) is invalid."
                    GoTo Cleanup
                End If
                'Else
                '    stPayInfo.szErrDesc = "Required field is missing: TxnAmount."
                '    stPayInfo.szTxnMsg = "Required field(s) is missing."
                '    GoTo Cleanup
            End If

            If Trim(Request("CurrencyCode")) <> "" Then
                ' Gets CurrencyCode before reformatting
                szCurrencyCode = Request("CurrencyCode")
                stPayInfo.szCurrencyCode = Trim(szCurrencyCode).ToUpper()                           'Added ToUpper() on 29 Sept 2009

                If (stPayInfo.szCurrencyCode.Length > 3) Then
                    stPayInfo.szCurrencyCode = stPayInfo.szCurrencyCode.Substring(0, 3).ToUpper()   'Added ToUpper() on 29 Sept 2009
                End If

                'Else
                '    stPayInfo.szErrDesc = "Required field is missing: CurrencyCode."
                '    stPayInfo.szTxnMsg = "Required field(s) is missing."
                '    GoTo Cleanup
            End If

            'If Trim(Request("IssuingBank")) <> "" Then
            '    stPayInfo.szIssuingBank = Trim(Request("IssuingBank"))

            '    If (stPayInfo.szIssuingBank.Length > 20) Then
            '        stPayInfo.szIssuingBank = stPayInfo.szIssuingBank.Substring(0, 20)
            '    End If
            'Else
            '    stPayInfo.szIssuingBank = ""
            'End If

            If Trim(Request("LanguageCode")) <> "" Then
                stPayInfo.szLanguageCode = Trim(Request("LanguageCode"))

                If (stPayInfo.szLanguageCode.Length > 2) Then
                    stPayInfo.szLanguageCode = stPayInfo.szLanguageCode.Substring(0, 2)
                End If
            Else
                stPayInfo.szLanguageCode = "EN" ' Default value - English
            End If

            If Trim(Request("CardPAN")) <> "" Then
                stPayInfo.szCardPAN = Server.UrlDecode(Trim(Request("CardPAN")))

                If (stPayInfo.szCardPAN.Length > 20) Then
                    stPayInfo.szCardPAN = stPayInfo.szCardPAN.Substring(0, 20)
                End If

                If stPayInfo.szCardPAN.Length > 10 Then
                    stPayInfo.szMaskedCardPAN = stPayInfo.szCardPAN.Substring(0, 6) + "".PadRight(stPayInfo.szCardPAN.Length - 6 - 4, "X") + stPayInfo.szCardPAN.Substring(stPayInfo.szCardPAN.Length - 4, 4)
                Else
                    stPayInfo.szMaskedCardPAN = "".PadRight(stPayInfo.szCardPAN.Length, "X")
                End If
            End If

            If Trim(Request("CVV2")) <> "" Or Trim(Request("CardCVV2")) <> "" Then
                If Trim(Request("CVV2")) <> "" Then
                    stPayInfo.szCVV2 = Server.UrlDecode(Trim(Request("CVV2")))
                Else
                    stPayInfo.szCVV2 = Server.UrlDecode(Trim(Request("CardCVV2")))
                End If

                If (stPayInfo.szCVV2.Length > 20) Then
                    stPayInfo.szCVV2 = stPayInfo.szCVV2.Substring(0, 20)
                End If

            End If

            If Trim(Request("CardType")) <> "" Then
                stPayInfo.szCardType = Server.UrlDecode(Trim(Request("CardType")))

                If (stPayInfo.szCardType.Length > 10) Then
                    stPayInfo.szCardType = stPayInfo.szCardType.Substring(0, 25)
                End If
            Else
                stPayInfo.szCardType = ""
            End If

            If Trim(Request("SessionID")) <> "" Then
                stPayInfo.szSessionID = Server.UrlDecode(Trim(Request("SessionID")))

                If (stPayInfo.szSessionID.Length > 100) Then
                    stPayInfo.szSessionID = stPayInfo.szSessionID.Substring(0, 100)
                End If
            End If

            If Trim(Request("Param1")) <> "" Then
                stPayInfo.szParam1 = Server.UrlDecode(Trim(Request("Param1")))

                If (stPayInfo.szParam1.Length > 100) Then
                    stPayInfo.szParam1 = stPayInfo.szParam1.Substring(0, 100)
                End If
            End If

            If Trim(Request("Param2")) <> "" Or Trim(Request("CardNo")) <> "" Then
                If Trim(Request("Param2")) <> "" Then
                    stPayInfo.szParam2 = Server.UrlDecode(Trim(Request("Param2")))
                Else
                    stPayInfo.szParam2 = Server.UrlDecode(Trim(Request("CardNo")))
                End If

                'Added by Jeff, 27 Feb 2014
                stPayInfo.szCardPAN = stPayInfo.szParam2

                If (stPayInfo.szParam2.Length > 100) Then
                    stPayInfo.szParam2 = stPayInfo.szParam2.Substring(0, 100)
                End If

                If stPayInfo.szParam2.Length > 10 Then
                    stPayInfo.szMaskedParam2 = stPayInfo.szParam2.Substring(0, 6) + "".PadRight(stPayInfo.szParam2.Length - 6 - 4, "X") + stPayInfo.szParam2.Substring(stPayInfo.szParam2.Length - 4, 4)
                Else
                    stPayInfo.szMaskedParam2 = "".PadRight(stPayInfo.szParam2.Length, "X")
                End If

                stPayInfo.szMaskedCardPAN = stPayInfo.szMaskedParam2
            End If

            If Trim(Request("Param3")) <> "" Or Trim(Request("CardExp")) <> "" Then
                If Trim(Request("Param3")) <> "" Then
                    stPayInfo.szParam3 = Server.UrlDecode(Trim(Request("Param3")))
                Else
                    stPayInfo.szParam3 = Server.UrlDecode(Trim(Request("CardExp")))
                End If

                If (stPayInfo.szParam3.Length > 100) Then
                    stPayInfo.szParam3 = stPayInfo.szParam3.Substring(0, 100)
                End If

                If stPayInfo.szParam3.Length > 10 Then
                    stPayInfo.szMaskedParam3 = stPayInfo.szParam3.Substring(0, 6) + "".PadRight(stPayInfo.szParam3.Length - 6 - 4, "X") + stPayInfo.szParam3.Substring(stPayInfo.szParam3.Length - 4, 4)
                Else
                    stPayInfo.szMaskedParam3 = "".PadRight(stPayInfo.szParam3.Length, "X")
                End If
            End If

            If Trim(Request("Param4")) <> "" Then
                stPayInfo.szParam4 = Server.UrlDecode(Trim(Request("Param4")))

                If (stPayInfo.szParam4.Length > 100) Then
                    stPayInfo.szParam4 = stPayInfo.szParam4.Substring(0, 100)
                End If
            End If

            If Trim(Request("Param5")) <> "" Then
                stPayInfo.szParam5 = Server.UrlDecode(Trim(Request("Param5")))

                If (stPayInfo.szParam5.Length > 100) Then
                    stPayInfo.szParam5 = stPayInfo.szParam5.Substring(0, 100)
                End If
            End If
            'Added on 16 Aug 2009, to cater for add seats
            If Trim(Request("Param6")) <> "" Then
                stPayInfo.szParam6 = Server.UrlDecode(Trim(Request("Param6")))

                If (stPayInfo.szParam6.Length > 50) Then
                    stPayInfo.szParam6 = stPayInfo.szParam6.Substring(0, 50)
                End If
            End If

            If Trim(Request("Param7")) <> "" Then
                stPayInfo.szParam7 = Server.UrlDecode(Trim(Request("Param7")))

                If (stPayInfo.szParam7.Length > 50) Then
                    stPayInfo.szParam7 = stPayInfo.szParam7.Substring(0, 50)
                End If
            End If

            If Trim(Request("Param8")) <> "" Then
                stPayInfo.szParam8 = Server.UrlDecode(Trim(Request("Param8")))

                If (stPayInfo.szParam8.Length > 50) Then
                    stPayInfo.szParam8 = stPayInfo.szParam8.Substring(0, 50)
                End If
            End If

            If Trim(Request("Param9")) <> "" Then
                stPayInfo.szParam9 = Server.UrlDecode(Trim(Request("Param9")))

                If (stPayInfo.szParam9.Length > 50) Then
                    stPayInfo.szParam9 = stPayInfo.szParam9.Substring(0, 50)
                End If
            End If

            If Trim(Request("Param10")) <> "" Then
                stPayInfo.szParam10 = Server.UrlDecode(Trim(Request("Param10")))

                If (stPayInfo.szParam10.Length > 50) Then
                    stPayInfo.szParam10 = stPayInfo.szParam10.Substring(0, 50)
                End If
            End If

            If Trim(Request("Param11")) <> "" Then
                stPayInfo.szParam11 = Server.UrlDecode(Trim(Request("Param11")))

                If (stPayInfo.szParam11.Length > 255) Then
                    stPayInfo.szParam11 = stPayInfo.szParam11.Substring(0, 255)
                End If
            End If

            If Trim(Request("Param12")) <> "" Then
                stPayInfo.szParam12 = Server.UrlDecode(Trim(Request("Param12")))

                If (stPayInfo.szParam12.Length > 255) Then
                    stPayInfo.szParam12 = stPayInfo.szParam12.Substring(0, 255)
                End If
            End If

            If Trim(Request("Param13")) <> "" Then
                stPayInfo.szParam13 = Server.UrlDecode(Trim(Request("Param13")))

                If (stPayInfo.szParam13.Length > 255) Then
                    stPayInfo.szParam13 = stPayInfo.szParam13.Substring(0, 255)
                End If
            End If

            If Trim(Request("Param14")) <> "" Then
                stPayInfo.szParam14 = Server.UrlDecode(Trim(Request("Param14")))

                If (stPayInfo.szParam14.Length > 255) Then
                    stPayInfo.szParam14 = stPayInfo.szParam14.Substring(0, 255)
                End If
            End If

            If Trim(Request("Param15")) <> "" Then
                stPayInfo.szParam15 = Server.UrlDecode(Trim(Request("Param15")))

                If (stPayInfo.szParam15.Length > 255) Then
                    stPayInfo.szParam15 = stPayInfo.szParam15.Substring(0, 255)
                End If
            End If

            If Trim(Request("HashMethod")) <> "" Then
                stPayInfo.szHashMethod = Trim(Request("HashMethod"))

                If (stPayInfo.szHashMethod.Length > 4) Then
                    stPayInfo.szHashMethod = stPayInfo.szHashMethod.Substring(0, 4)
                End If

                'Added SHA256 checking on 26 May 2009
                If ((stPayInfo.szHashMethod.ToLower <> "md5") And (stPayInfo.szHashMethod.ToLower <> "sha1") And (stPayInfo.szHashMethod.ToLower <> "sha2")) Then
                    stPayInfo.szErrDesc = "Required field is invalid: HashMethod."
                    stPayInfo.szTxnMsg = "Required field(s) is invalid."
                    GoTo CleanUp
                End If
                'Else
                '    stPayInfo.szErrDesc = "Required field is missing: HashMethod."
                '    stPayInfo.szTxnMsg = "Required field(s) is missing."
                '    GoTo Cleanup
            End If

            If Trim(Request("HashValue")) <> "" Then
                'Added on 21 Nov 2008 because "+" value will be treated as " ", may resulting in Hash value mismatched.
                stPayInfo.szHashValue = Server.UrlDecode(Request("HashValue")).Replace(" ", "+")

                'Moved the following into TxnProc bInsertNewTxn on 26 May 2009
                'If (stPayInfo.szHashValue.Length > 40) Then
                '    stPayInfo.szHashValue = stPayInfo.szHashValue.Substring(0, 40)
                'End If
                'Else
                '    stPayInfo.szErrDesc = "Required field is missing: HashValue."
                '    stPayInfo.szTxnMsg = "Required field(s) is missing."
                '    GoTo Cleanup
            End If

            stPayInfo.szMachineID = ConfigurationManager.AppSettings("MachineID")

            ' Gets Hash key for request
            ' Added BaseCurrencyCode and BaseTxnAmount on 8 Aug 2009 to cater for base currency booking confirmation enhancement
            stPayInfo.szHashKey = szServiceID + szMerchantTxnID + szMerchantReturnURL + szTxnAmount + szCurrencyCode

            bReturn = True

        Catch ex As Exception
            stPayInfo.szErrDesc = "Error: Fail to get PAY request data."
            stPayInfo.szTxnMsg = "System Error: Fail to get PAY request data."
            bReturn = False
        End Try

CleanUp:
        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		Payment
    ' Function Name:	bGetRSaleReqData
    ' Function Type:	Boolean True if success else false
    ' Parameter:		
    ' Description:		Gets Reversal Sale or Refund request data
    ' History:			10 Apr 2010
    '********************************************************************************************
    Private Function bGetRSaleReqData() As Boolean
        Dim bReturn As Boolean = False

        Try
            bGetQueryReqData()

            bReturn = True

        Catch ex As Exception
            stPayInfo.szErrDesc = "Error: Fail to get Reversal request data."
            stPayInfo.szTxnMsg = "System Error: Fail to get Reversal request data."
            bReturn = False
        End Try

CleanUp:
        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		Payment
    ' Function Name:	bGetQueryReqData
    ' Function Type:	Boolean True if success else false
    ' Parameter:		
    ' Description:		Gets Query request data
    ' History:			8 Dec 2008
    '********************************************************************************************
    Private Function bGetQueryReqData() As Boolean
        Dim bReturn As Boolean = False
        Dim szServiceID As String = ""
        Dim szMerchantTxnID As String = ""
        Dim szTxnAmount As String = ""

        Try
            stPayInfo.szHashKey = ""

            If Trim(Request("ServiceID")) <> "" Then
                ' Gets the ServiceID before reformatting
                szServiceID = Server.UrlDecode(Request("ServiceID"))
                stPayInfo.szServiceID = Trim(szServiceID)

                'If (stPayInfo.szServiceID.Length > 3) Then
                '    stPayInfo.szServiceID = stPayInfo.szServiceID.Substring(0, 3)
                'End If
            Else
                stPayInfo.szErrDesc = "Required field is missing: ServiceID."
                stPayInfo.szQueryMsg = "Required field(s) is missing."
                GoTo Cleanup
            End If

            If Trim(Request("MerchantTxnID")) <> "" Or Trim(Request("PaymentID")) <> "" Then
                If Trim(Request("MerchantTxnID")) <> "" Then
                    ' Gets MerchantTxnID before reformatting
                    szMerchantTxnID = Server.UrlDecode(Request("MerchantTxnID"))
                    stPayInfo.szMerchantTxnID = Trim(szMerchantTxnID)

                    If (stPayInfo.szMerchantTxnID.Length > 30) Then
                        stPayInfo.szMerchantTxnID = stPayInfo.szMerchantTxnID.Substring(0, 30)
                    End If
                ElseIf Trim(Request("PaymentID")) <> "" Then
                    szMerchantTxnID = Server.UrlDecode(Request("PaymentID"))
                    stPayInfo.szMerchantTxnID = Trim(szMerchantTxnID)

                    If (stPayInfo.szMerchantTxnID.Length > 30) Then
                        stPayInfo.szMerchantTxnID = stPayInfo.szMerchantTxnID.Substring(0, 30)
                    End If

                End If

            Else
                stPayInfo.szErrDesc = "Required field is missing: MerchantTxnID/PaymentID."
                stPayInfo.szQueryMsg = "Required field(s) is missing."
                GoTo Cleanup
            End If

            If Trim(Request("TxnAmount")) <> "" Or Trim(Request("Amount")) <> "" Then
                If Trim(Request("TxnAmount")) <> "" Then
                    szTxnAmount = Request("TxnAmount")
                    stPayInfo.szTxnAmount = Trim(szTxnAmount)
                ElseIf Trim(Request("Amount")) <> "" Then
                    szTxnAmount = Request("Amount")
                    stPayInfo.szTxnAmount = Trim(szTxnAmount)
                End If

                If False = bValidateAmount(stPayInfo.szTxnAmount) Then
                    stPayInfo.szErrDesc = "Required field is invalid: TxnAmount."
                    stPayInfo.szTxnMsg = "Required field(s) is invalid."
                    GoTo Cleanup
                End If
                'Else
                '    stPayInfo.szErrDesc = "Required field is missing: TxnAmount."
                '    stPayInfo.szTxnMsg = "Required field(s) is missing."
                '    GoTo Cleanup
            End If

            If Trim(Request("HashMethod")) <> "" Then
                stPayInfo.szHashMethod = Trim(Request("HashMethod"))

                If (stPayInfo.szHashMethod.Length > 4) Then
                    stPayInfo.szHashMethod = stPayInfo.szHashMethod.Substring(0, 4)
                End If

                'Added on 26 May 2009, to cater for SHA256
                If ((stPayInfo.szHashMethod.ToLower <> "md5") And (stPayInfo.szHashMethod.ToLower <> "sha1") And (stPayInfo.szHashMethod.ToLower <> "sha2")) Then
                    stPayInfo.szErrDesc = "Required field is invalid: HashMethod."
                    stPayInfo.szQueryMsg = "Required field(s) is invalid."
                    GoTo Cleanup
                End If
            Else
                stPayInfo.szErrDesc = "Required field is missing: HashMethod."
                stPayInfo.szQueryMsg = "Required field(s) is missing."
                GoTo Cleanup
            End If

            If Trim(Request("HashValue")) <> "" Then
                'Added on 21 Nov 2008 because "+" value will be treated as " ", may resulting in Hash value mismatched.
                stPayInfo.szHashValue = Server.UrlDecode(Request("HashValue")).Replace(" ", "+")

                'Commented the following on 26 May 2009
                'If (stPayInfo.szHashValue.Length > 40) Then
                '    stPayInfo.szHashValue = stPayInfo.szHashValue.Substring(0, 40)
                'End If
                'Else
                '    stPayInfo.szErrDesc = "Required field is missing: HashValue."
                '    stPayInfo.szQueryMsg = "Required field(s) is missing."
                '    GoTo Cleanup
            End If

            'Gets Hash key for request
            stPayInfo.szHashKey = szServiceID + szMerchantTxnID
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(),
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            stPayInfo.szServiceID + stPayInfo.szMerchantTxnID + " > Query Request Hash Key (Merchant Password + " + stPayInfo.szHashKey + ")")

            bReturn = True

        Catch ex As Exception
            stPayInfo.szErrDesc = "Error: Fail to get QUERY request data."
            stPayInfo.szQueryMsg = "System Error: Fail to get QUERY request data."
            bReturn = False
        End Try

Cleanup:
        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		Payment
    ' Function Name:	bValidateAmount
    ' Function Type:	Boolean  True if success else false
    ' Parameter:		sz_Amount [in, string]  -   String representation of the amount
    ' Description:		Validate the amount payable submitted by the user
    ' History:			20 Oct 2008
    '********************************************************************************************
    Private Function bValidateAmount(ByVal sz_Amount As String) As Boolean
        Dim bReturn As Boolean = True
        Dim regexAmount As Regex

        Try
            ' Check for the length of the sz_Amount string
            If sz_Amount.Length > 16 Or Not IsNumeric(sz_Amount) Then
                bReturn = False
            Else
                If (Convert.ToDecimal(sz_Amount) <= 0) Then
                    bReturn = False
                Else
                    ' Check for the money pattern
                    regexAmount = New Regex("^\d+(\.(\d{2}))$")
                    Dim MAmt As Match = regexAmount.Match(sz_Amount)

                    ' Assign the result to the return value
                    bReturn = MAmt.Success
                End If
            End If
        Catch ex As Exception
            bReturn = False
        Finally
            If Not regexAmount Is Nothing Then regexAmount = Nothing
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		Payment
    ' Function Name:	bProcessSale
    ' Function Type:	Boolean.  Return False if error.
    ' Parameter:		NONE
    ' Description:		Process payment request
    ' History:			17 Oct 2008
    '********************************************************************************************
    Private Function bProcessSale() As Boolean
        Dim szHTTPString As String = ""
        'Success
        '4111 1111 1111 1111
        '5500 0000 0000 0012
        '5555 5555 5555 4444
        'Failed
        '4111 1111 1111 1194
        '5500 0000 0000 0004
        'Pending
        '4111 1111 1111 1145
        '5500 0000 0000 0020

        'Added by Jeff, 27 Feb 2014
        If (stPayInfo.szCardPAN = "4111111111111194" Or stPayInfo.szCardPAN = "5500000000000004" Or stPayInfo.szCardPAN = "4111111111111145" Or stPayInfo.szCardPAN = "5500000000000020") Then
            If (stPayInfo.szCardPAN = "4111111111111194" Or stPayInfo.szCardPAN = "5500000000000004") Then
                stPayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                stPayInfo.szTxnMsg = "Declined"
                stPayInfo.szRespCode = "01"
            ElseIf (stPayInfo.szCardPAN = "4111111111111145" Or stPayInfo.szCardPAN = "5500000000000020") Then
                Response.End()  'Discard, simulate no response
            Else
                stPayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_SUCCESS
                stPayInfo.szAuthCode = "123456" 'stPayInfo.szMerchantTxnID.Substring(0, 6)
                stPayInfo.szRespCode = "00"
                stPayInfo.szTxnMsg = "Approved"
            End If
        Else
            If (szReqStatus = "Failed") Then
                stPayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                stPayInfo.szTxnMsg = "Declined"
                stPayInfo.szRespCode = "01"
            Else
                stPayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_SUCCESS
                stPayInfo.szRespCode = "00"
                stPayInfo.szTxnMsg = "Approved"
            End If
            'Else
            '    If (stPayInfo.szTxnAmount = "0.14") Then
            '        stPayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
            '        stPayInfo.szTxnMsg = "Declined"
            '        stPayInfo.szRespCode = "05"
            '    ElseIf (stPayInfo.szTxnAmount = "0.53" Or stPayInfo.szTxnAmount = "0.44") Then
            '        Response.End()  'Discard, simulate no response
            '    Else
            '        stPayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_SUCCESS
            '        stPayInfo.szAuthCode = stPayInfo.szMerchantTxnID.Substring(0, 6)
            '        stPayInfo.szRespCode = "00"
            '        stPayInfo.szTxnMsg = "Approved"
            '    End If
        End If

        If (stPayInfo.szGatewayID = "1") Then
            stHostInfo.iHostReplyMethod = 1 'Redirect
        Else
            stHostInfo.iHostReplyMethod = 2 'Server-to-server
        End If

        stPayInfo.szTxnID = stPayInfo.szMerchantTxnID

        iSendReply2Merchant(stPayInfo, stHostInfo, g_szHTML)

        'Added by Jeff, 26 May 2014
        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
            DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
            stPayInfo.szServiceID + stPayInfo.szMerchantTxnID + ">Resp[" + g_szHTML + "]")

        Return True
    End Function

    Private Function bProcessOB(ByVal sz_URL As String, ByVal sz_HTTPString As String) As Boolean
        Dim bRet As Boolean = True
        Dim szTemplatePath As String = ""
        Dim szHTMLContent As String = ""

        szTemplatePath = ConfigurationManager.AppSettings("HostSimLogin")

        If szTemplatePath <> "" Then
            GetTemplate(szTemplatePath, szHTMLContent)

            If szHTMLContent <> "" Then
                objCommon.szFormatPlaceHolder(szHTMLContent, "[HOSTPARAMS]", sz_HTTPString)
                objCommon.szFormatPlaceHolder(szHTMLContent, "[HOSTURL]", sz_URL)

                g_szHTML = szHTMLContent
            Else
                bRet = False
            End If
        Else
            bRet = False
        End If

        Return bRet

    End Function

    '********************************************************************************************
    ' Class Name:		Payment
    ' Function Name:	bProcessSale
    ' Function Type:	Boolean.  Return False if error.
    ' Parameter:		NONE
    ' Description:		Process payment request
    ' History:			17 Oct 2008
    '********************************************************************************************
    Private Function bProcessQuery() As Boolean
        Dim szHTTPString As String = ""

        'Success
        '4111 1111 1111 1111
        '5500 0000 0000 0012
        '5555 5555 5555 4444
        'Failed
        '4111 1111 1111 1194
        '5500 0000 0000 0004
        'Pending
        '4111 1111 1111 1145
        '5500 0000 0000 0020

        If (stPayInfo.szCardPAN = "4111111111111194" Or stPayInfo.szCardPAN = "5500000000000004" Or stPayInfo.szCardPAN = "4111111111111145" Or stPayInfo.szCardPAN = "5500000000000020") Then
            If (stPayInfo.szCardPAN = "4111111111111194" Or stPayInfo.szCardPAN = "5500000000000004") Then
                stPayInfo.iQueryStatus = 0
                stPayInfo.szQueryMsg = "Exists"
                stPayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                stPayInfo.szTxnMsg = "Declined"
                stPayInfo.szRespCode = "01"
            ElseIf (stPayInfo.szCardPAN = "4111111111111145" Or stPayInfo.szCardPAN = "5500000000000020") Then
                stPayInfo.iQueryStatus = 0
                stPayInfo.szQueryMsg = "Exists"
                stPayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_PENDING
                stPayInfo.szTxnMsg = "Pending"
                stPayInfo.szRespCode = "99"
            End If
        Else
            If (stPayInfo.szTxnAmount = "0.14") Then
                stPayInfo.iQueryStatus = 0
                stPayInfo.szQueryMsg = "Exists"
                stPayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_FAILED
                stPayInfo.szTxnMsg = "Declined"
                stPayInfo.szRespCode = "01"
            ElseIf (stPayInfo.szTxnAmount = "0.53") Then
                stPayInfo.iQueryStatus = 0
                stPayInfo.szQueryMsg = "Exists"
                stPayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_PENDING
                stPayInfo.szTxnMsg = "Pending"
                stPayInfo.szRespCode = "99"
            ElseIf (stPayInfo.szTxnAmount = "0.44") Then
                stPayInfo.iQueryStatus = 1  'Not exists
                stPayInfo.szQueryMsg = "Not exists"
                stPayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_TIMEOUT_HOST_UNPROCESSED
                stPayInfo.szTxnMsg = "Not exists"
                stPayInfo.szRespCode = "98"
            Else
                stPayInfo.iQueryStatus = 0
                stPayInfo.szQueryMsg = "Exists"
                stPayInfo.iMerchantTxnStatus = Common.TXN_STATUS.TXN_SUCCESS
                stPayInfo.szAuthCode = stPayInfo.szMerchantTxnID.Substring(0, 6)
                stPayInfo.szRespCode = "00"
                stPayInfo.szTxnMsg = "Approved"
            End If
        End If

        If (stPayInfo.szGatewayID = "1") Then
            stHostInfo.iHostReplyMethod = 1 'Redirect
        Else
            stHostInfo.iHostReplyMethod = 2 'Server-to-server
        End If

        stPayInfo.szTxnID = stPayInfo.szMerchantTxnID

        iLoadQueryUI()

        Return True
    End Function

    '********************************************************************************************
    ' Class Name:		Payment
    ' Function Name:	bProcessRSale
    ' Function Type:	Boolean.  Return False if error.
    ' Parameter:		NONE
    ' Description:		Process reversal payment request
    ' History:			17 Oct 2008
    '********************************************************************************************
    Private Function bProcessRSale() As Boolean
        bProcessQuery()

        Return True
    End Function

    '********************************************************************************************
    ' Class Name:		Payment
    ' Function Name:	iLoadQueryUI
    ' Function Type:	Integer
    ' Parameter:		
    ' Description:		Load the template for query page
    ' History:			18 Dec 2008
    '********************************************************************************************
    Private Function iLoadQueryUI() As Integer
        Dim iReturn As Integer = 0
        Dim szTemplatePath As String = ""
        Dim szHTMLContent As String = ""

        szTemplatePath = ConfigurationManager.AppSettings("QueryTemplate")

        If szTemplatePath <> "" Then
            GetTemplate(szTemplatePath, szHTMLContent)
            If szHTMLContent <> "" Then
                iReturn = iCustomizeQueryPage(szHTMLContent)
                If iReturn <> 0 Then
                    iReturn = -1
                    Return iReturn
                End If

                g_szHTML = szHTMLContent
            Else
                iReturn = -1
            End If
        Else
            iReturn = -1
        End If

        Return iReturn
    End Function

    '********************************************************************************************
    ' Class Name:		Payment
    ' Function Name:	iCustomizeQueryPage
    ' Function Type:	
    ' Parameter:		NONE
    ' Description:		Customize query page
    ' History:			18 Dec 2008
    '********************************************************************************************

    Private Function iCustomizeQueryPage(ByRef sz_HTMLContent As String) As Integer
        Dim iReturn As Integer = -1

        'TxnExists=[QUERYSTATUS]&QueryDesc=[QUERYMESG]&TransactionType=[TXNTYPE]&ServiceID=[SERVICEID]&PaymentID=[MERCHANTTXNID]&Amount=[TXNAMOUNT]&TxnID=[GATEWAYTXNID]&RespCode=[RESPCODE]&AuthCode=[AUTHCODE]&TxnMessage=[RESPMESG]
        objCommon.szFormatPlaceHolder(sz_HTMLContent, "[QUERYSTATUS]", stPayInfo.iQueryStatus.ToString())
        objCommon.szFormatPlaceHolder(sz_HTMLContent, "[QUERYMESG]", stPayInfo.szQueryMsg)
        objCommon.szFormatPlaceHolder(sz_HTMLContent, "[TXNTYPE]", "QUERY")
        objCommon.szFormatPlaceHolder(sz_HTMLContent, "[SERVICEID]", stPayInfo.szServiceID)
        objCommon.szFormatPlaceHolder(sz_HTMLContent, "[MERCHANTTXNID]", stPayInfo.szMerchantTxnID)
        objCommon.szFormatPlaceHolder(sz_HTMLContent, "[TXNAMOUNT]", stPayInfo.szTxnAmount)
        objCommon.szFormatPlaceHolder(sz_HTMLContent, "[GATEWAYTXNID]", stPayInfo.szTxnID)
        'objCommon.szFormatPlaceHolder(sz_HTMLContent, "[TXNSTATUS]", stPayInfo.iMerchantTxnStatus.ToString())
        'objCommon.szFormatPlaceHolder(sz_HTMLContent, "[BANKREFNO]", stPayInfo.szAuthCode)
        objCommon.szFormatPlaceHolder(sz_HTMLContent, "[RESPMESG]", stPayInfo.szTxnMsg)

        'Added for CCGW AuthCode and BankRespNo
        objCommon.szFormatPlaceHolder(sz_HTMLContent, "[AUTHCODE]", stPayInfo.szAuthCode)

        If (stPayInfo.szRespCode <> "") Then
            objCommon.szFormatPlaceHolder(sz_HTMLContent, "[RESPCODE]", stPayInfo.szRespCode)
        Else
            objCommon.szFormatPlaceHolder(sz_HTMLContent, "[RESPCODE]", stPayInfo.szHostTxnStatus)
        End If

        iReturn = 0

        Return iReturn
    End Function

    '********************************************************************************************
    ' Class Name:		Response
    ' Function Name:	bSendReply2Merchant
    ' Function Type:	NONE
    ' Parameter:        
    ' Description:		Error handler
    ' History:			18 Nov 2008
    '********************************************************************************************
    Public Function iSendReply2Merchant(ByRef st_PayInfo As Common.STPayInfo, ByVal st_HostInfo As Common.STHost, ByRef sz_HTML As String) As Integer
        Dim szHTTPString As String = ""
        Dim iRet As Integer = -1

        Try
            st_PayInfo.szMerchantPassword = "hostsim12345"

            'Generates response hash value
            st_PayInfo.szHashValue = objHash.szGetSaleResHash(st_PayInfo)

            'Generates HTTP string to be replied to merchant
            szHTTPString = szGetReplyMerchantHTTPString(st_PayInfo, st_HostInfo)

            'Added checking of DDGW Aggregator Model on 28 Mar 2010
            'If (ConfigurationSettings.AppSettings("HostsListTemplate") <> "") Then
            '    If (1 = st_HostInfo.iHostReplyMethod Or 4 = st_HostInfo.iHostReplyMethod) Then
            '        iRet = iLoadReceiptUI(st_PayInfo.szMerchantReturnURL, szHTTPString, sz_HTML, st_PayInfo)

            '        If (iRet <> 0) Then
            '            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
            '                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
            '                            st_PayInfo.szServiceID + st_PayInfo.szMerchantTxnID + " > Failed to load receipt for MerchantReturnURL(" + st_PayInfo.szMerchantReturnURL + ") Params(" + szHTTPString + ")")
            '        Else
            '            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
            '                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
            '                            st_PayInfo.szServiceID + st_PayInfo.szMerchantTxnID + " > Loaded receipt for MerchantReturnURL(" + st_PayInfo.szMerchantReturnURL + ") Params(" + szHTTPString + ")")
            '        End If
            '    Else
            '        'Send HTTP string reply to merchant using the same method used by Host
            '        iRet = iHTTPSend(st_HostInfo.iHostReplyMethod, szHTTPString, st_PayInfo.szMerchantReturnURL, st_PayInfo, sz_HTML)
            '        If (iRet <> 0) Then
            '            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
            '                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
            '                            st_PayInfo.szServiceID + st_PayInfo.szMerchantTxnID + " > Failed to send reply iRet(" + CStr(iRet) + ") " + szHTTPString + " to merchant URL " + st_PayInfo.szMerchantReturnURL)
            '        Else
            '            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
            '                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
            '                            st_PayInfo.szServiceID + st_PayInfo.szMerchantTxnID + " > Reply to merchant sent.")
            '        End If
            '    End If
            'Else
            'Send HTTP string reply to merchant using the same method used by Host
            iRet = iHTTPSend(st_HostInfo.iHostReplyMethod, szHTTPString, st_PayInfo.szMerchantReturnURL, st_PayInfo, sz_HTML)
            'End If

        Catch ex As Exception
            iRet = -1
        End Try

        Return iRet
    End Function

    '********************************************************************************************
    ' Class Name:		Response
    ' Function Name:	szGetReplyMerchantHTTPString
    ' Function Type:	String
    ' Parameter:		None
    ' Description:		Form HTTP string to reply merchant
    ' History:			18 Nov 2008
    '********************************************************************************************
    Public Function szGetReplyMerchantHTTPString(ByVal st_PayInfo As Common.STPayInfo, ByVal st_HostInfo As Common.STHost) As String
        Dim szHTTPString As String = ""

        'If host replies using Redirect/Page-to-page method, Gateway reply to merchant also needs to be redirected to merchant
        'by returning a page which will auto submit these hidden fields to merchant server, better than using Response.Redirect
        'which will cause these fields to be visible on client's browser during redirection.
        If (1 = st_HostInfo.iHostReplyMethod Or 4 = st_HostInfo.iHostReplyMethod) Then
            szHTTPString = "<INPUT type='hidden' name='TransactionType' value='" + Server.UrlEncode(st_PayInfo.szTxnType) + "'>" + vbCrLf
            szHTTPString = szHTTPString + "<INPUT type='hidden' name='ServiceID' value='" + st_PayInfo.szServiceID + "'>" + vbCrLf
            szHTTPString = szHTTPString + "<INPUT type='hidden' name='PaymentID' value='" + st_PayInfo.szMerchantTxnID + "'>" + vbCrLf
            szHTTPString = szHTTPString + "<INPUT type='hidden' name='Amount' value='" + st_PayInfo.szTxnAmount + "'>" + vbCrLf
            szHTTPString = szHTTPString + "<INPUT type='hidden' name='CurrencyCode' value='" + st_PayInfo.szCurrencyCode + "'>" + vbCrLf
            'szHTTPString = szHTTPString + "<INPUT type='hidden' name='SessionID' value='" + Server.UrlEncode(st_PayInfo.szSessionID) + "'>" + vbCrLf
            szHTTPString = szHTTPString + "<INPUT type='hidden' name='TxnID' value='" + st_PayInfo.szTxnID + "'>" + vbCrLf
            szHTTPString = szHTTPString + "<INPUT type='hidden' name='TxnStatus' value='" + st_PayInfo.iMerchantTxnStatus.ToString() + "'>" + vbCrLf
            szHTTPString = szHTTPString + "<INPUT type='hidden' name='AuthCode' value='" + Server.UrlEncode(st_PayInfo.szAuthCode) + "'>" + vbCrLf
            szHTTPString = szHTTPString + "<INPUT type='hidden' name='TxnMessage' value='" + Server.UrlEncode(st_PayInfo.szTxnMsg) + "'>" + vbCrLf
            szHTTPString = szHTTPString + "<INPUT type='hidden' name='HashMethod' value='" + Server.UrlEncode(st_PayInfo.szHashMethod) + "'>" + vbCrLf
            szHTTPString = szHTTPString + "<INPUT type='hidden' name='HashValue' value='" + st_PayInfo.szHashValue + "'>" + vbCrLf
            If (st_PayInfo.szHostTxnStatus <> "") Then
                szHTTPString = szHTTPString + "<INPUT type='hidden' name='RespCode' value='" + Server.UrlEncode(st_PayInfo.szHostTxnStatus) + "'>" + vbCrLf
            Else
                szHTTPString = szHTTPString + "<INPUT type='hidden' name='RespCode' value='" + Server.UrlEncode(st_PayInfo.szRespCode) + "'>" + vbCrLf
            End If

        Else
            szHTTPString = "TransactionType=" + Server.UrlEncode(st_PayInfo.szTxnType) + "&"
            szHTTPString = szHTTPString + "ServiceID=" + st_PayInfo.szServiceID + "&"
            szHTTPString = szHTTPString + "PaymentID=" + st_PayInfo.szMerchantTxnID + "&"
            szHTTPString = szHTTPString + "Amount=" + st_PayInfo.szTxnAmount + "&"
            szHTTPString = szHTTPString + "CurrencyCode=" + Server.UrlEncode(st_PayInfo.szCurrencyCode) + "&"
            szHTTPString = szHTTPString + "TxnID=" + st_PayInfo.szTxnID + "&"
            szHTTPString = szHTTPString + "TxnStatus=" + st_PayInfo.iMerchantTxnStatus.ToString() + "&"
            szHTTPString = szHTTPString + "AuthCode=" + Server.UrlEncode(st_PayInfo.szAuthCode) + "&"
            szHTTPString = szHTTPString + "TxnMessage=" + Server.UrlEncode(st_PayInfo.szTxnMsg) + "&"
            szHTTPString = szHTTPString + "HashMethod=" + Server.UrlEncode(st_PayInfo.szHashMethod) + "&"
            szHTTPString = szHTTPString + "HashValue=" + st_PayInfo.szHashValue + "&"
            If (st_PayInfo.szHostTxnStatus <> "") Then
                szHTTPString = szHTTPString + "RespCode=" + Server.UrlEncode(st_PayInfo.szHostTxnStatus)
            Else
                szHTTPString = szHTTPString + "RespCode=" + Server.UrlEncode(st_PayInfo.szRespCode)
            End If

        End If

        Return szHTTPString
    End Function

    '********************************************************************************************
    ' Class Name:		Response
    ' Function Name:	iHTTPSend
    ' Function Type:	NONE
    ' Parameter:        i_SendMethod [in, integer] - Reply method:
    '                   - 1 (page-to-page, redirect);
    '                   - 2 (server-to-server, send ONLY);
    '                   - 3 (server-to-server, send and wait for reply)
    '                   - 4 (pop-up bank page-to-merchant parent page, redirect)
    '                   sz_HTTPString [in, string]  - HTTP string to be sent
    '                   sz_URL [in, string]         - Sending URL
    '                   st_PayInfo [in, structure]  - Payment info structure
    ' Description:		Error handler
    ' History:			18 Nov 2008
    '********************************************************************************************
    Public Function iHTTPSend(ByVal i_SendMethod As Integer, ByRef sz_HTTPString As String, ByVal sz_URL As String, ByVal st_PayInfo As Common.STPayInfo, ByRef sz_HTML As String) As Integer
        Dim iRet As Integer = 0
        Dim szURL() As String
        Dim iPosition As Integer = 0
        Dim szHTTPResponse As String = ""

        Try
            If (i_SendMethod <> 1 And i_SendMethod <> 4) Then
                'Cater for MerchantReturnURL that contains "?" or "$", e.g. "http://MerchantReturnURL?page=1&event=response" or https://149.122.26.33:6443/skylights/cgi-bin/skylights?page=DEBITRETURN&event=response
                'For merchants that use Get method(Query string), they can use "$" to replace "?" if they need to include
                '"?" in MerchantReturnURL param, e.g. Response.Redirect("http://url/page?MerchantReturnURL$page=1").
                iPosition = InStr(sz_URL, "?")  'e.g. 25 which is 1 base of the location
                If (iPosition > 0) Then
                    szURL = Split(sz_URL, "?")
                    If ((InStr(sz_URL, "?=") <= 0) And (InStr(sz_URL, "=") > iPosition)) Then
                        If (sz_HTTPString <> "") Then
                            sz_HTTPString = szURL(1) + "&" + sz_HTTPString
                        Else
                            sz_HTTPString = szURL(1)
                        End If
                    End If
                    sz_URL = szURL(0)
                Else
                    iPosition = InStr(sz_URL, "$")  'e.g. 25 which is 1 base of the location
                    If (iPosition > 0) Then
                        szURL = Split(sz_URL, "$")
                        If ((InStr(sz_URL, "$=") <= 0) And (InStr(sz_URL, "=") > iPosition)) Then
                            If (sz_HTTPString <> "") Then
                                sz_HTTPString = szURL(1) + "&" + sz_HTTPString
                            Else
                                sz_HTTPString = szURL(1)
                            End If
                        End If
                        sz_URL = szURL(0)
                    End If
                End If
            End If

            If (1 = i_SendMethod Or 4 = i_SendMethod) Then
                iLoadRedirectUI(sz_URL, sz_HTTPString, sz_HTML)

                'Host uses page-to-page method to redirect reply via client's browser to Direct Debit Gateway's reply page
                'Direct Debit Gateway also should redirect reply to merchant so that user can view the final result.
                'HttpContext.Current.Response.Redirect(sz_URL + "?" + sz_HTTPString)

            ElseIf (2 = i_SendMethod) Then
                'Host uses server-to-server method to reply Direct Debit Gateway
                'User is unable to see Direct Debit Gateway's reply page, Direct Debit Gateway should also do a
                'server-to-server reply to merchant without having to wait for merchant's acknowledgement.
                'Added 26 May 2014, added checking of MerchantReturnURL to determine the type of S2S response
                If (sz_URL <> "") Then
                    iRet = objHTTP.iHTTPostOnly(sz_URL, sz_HTTPString, st_PayInfo.iHttpTimeoutSeconds)
                Else
                    sz_HTML = sz_HTTPString
                End If

                If (iRet <> 0) Then
                    Return iRet
                End If

                'Modified on 20 Jul 2009, from Else to ElseIf (3 = i_SendMethod) Then
            ElseIf (3 = i_SendMethod) Then
                'HTTP post and wait for reply until timeout
                iRet = objHTTP.iHTTPostWithRecv(sz_URL, sz_HTTPString, st_PayInfo.iHttpTimeoutSeconds, szHTTPResponse, "")
                If (iRet <> 0) Then
                    Return iRet
                End If

                sz_HTTPString = szHTTPResponse

                'Added on 20 Jul 2009
            ElseIf (5 = i_SendMethod) Then
                'HTTP GET and wait for reply until timeout
                iRet = objHTTP.iHTTPGetWithRecv(sz_URL, sz_HTTPString, st_PayInfo.iHttpTimeoutSeconds, szHTTPResponse)
                If (iRet <> 0) Then
                    Return iRet
                End If

                sz_HTTPString = szHTTPResponse
            End If

        Catch ex As Exception
            iRet = -1

            st_PayInfo.szErrDesc = "iHTTPSend() exception: " + ex.StackTrace + " " + ex.Message  ' .Message
        End Try

        Return iRet
    End Function

    '********************************************************************************************
    ' Class Name:		Response
    ' Function Name:	iLoadRedirectUI
    ' Function Type:	Integer
    ' Parameter:		sz_HTML [out, string]   -   Customized content of the web page
    ' Description:		Load the redirect (auto submit hidden fields) template
    ' History:			25 Feb 2009
    '********************************************************************************************
    Public Function iLoadRedirectUI(ByVal sz_URL As String, ByVal sz_HTTPString As String, ByRef sz_HTML As String) As Integer
        Dim iReturn As Integer = 0
        Dim szTemplatePath As String = ""
        Dim szHTMLContent As String = ""

        szTemplatePath = ConfigurationManager.AppSettings("RedirectTemplate")

        If szTemplatePath <> "" Then
            GetTemplate(szTemplatePath, szHTMLContent)

            If szHTMLContent <> "" Then
                objCommon.szFormatPlaceHolder(szHTMLContent, "[HOSTPARAMS]", sz_HTTPString)
                objCommon.szFormatPlaceHolder(szHTMLContent, "[HOSTURL]", sz_URL)

                sz_HTML = szHTMLContent
            Else
                iReturn = -1
            End If
        Else
            iReturn = -1
        End If

        Return iReturn
    End Function

    '********************************************************************************************
    ' Class Name:		Response
    ' Function Name:	GetTemplate
    ' Function Type:	NONE
    ' Parameter:		sz_FilePath [in, string]        -   File name of the file to be read
    '                   sz_HTMLContent [out, string]    -   Content of the display file
    ' Description:		Get the corresponding template in order to form reply HTML page
    ' History:			17 Oct 2008
    '********************************************************************************************
    Private Function GetTemplate(ByVal sz_FilePath As String, ByRef sz_HTMLContent As String) As Boolean
        Dim objStreamReader As StreamReader
        Dim szTemplateFile As String = ""
        Dim bReturn As Boolean = True

        Try
            szTemplateFile = Server.MapPath(sz_FilePath)    'HttpContext.Current.Server.MapPath
            objStreamReader = System.IO.File.OpenText(szTemplateFile)
            sz_HTMLContent = objStreamReader.ReadToEnd()
            objStreamReader.Close()

        Catch ex As Exception
            bReturn = False
        Finally
            If Not objStreamReader Is Nothing Then objStreamReader = Nothing
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		Payment
    ' Function Name:	PaymentErrHandler
    ' Function Type:	NONE
    ' Description:		Error handler
    ' History:			15 Oct 2008
    '********************************************************************************************
    Private Sub PaymentErrHandler()

        Try
            If stPayInfo.szTxnType.ToLower = "pay" Then

                stPayInfo.szParam1 = ""
                stPayInfo.szParam2 = ""
                stPayInfo.szParam3 = ""
                stPayInfo.szParam4 = ""
                stPayInfo.szParam5 = ""
                stPayInfo.szParam6 = ""
                stPayInfo.szParam7 = ""
                stPayInfo.szParam8 = ""
                stPayInfo.szParam9 = ""
                stPayInfo.szParam10 = ""
                stPayInfo.szParam11 = ""
                stPayInfo.szParam12 = ""
                stPayInfo.szParam13 = ""
                stPayInfo.szParam14 = ""
                stPayInfo.szParam15 = ""

                iLoadErrUI()

            ElseIf stPayInfo.szTxnType.ToLower = "query" Then   'Query Result
                stPayInfo.szTxnMsg = ""

                'iLoadQueryUI()

            ElseIf stPayInfo.szTxnType.ToLower = "void" Then   'Reversal Sale - Added on 10 Apr 2010
                'iSendRSaleReply2Merchant()
            End If

        Catch ex As Exception

        End Try
    End Sub

    '********************************************************************************************
    ' Class Name:		Payment
    ' Function Name:	iLoadErrUI
    ' Function Type:	Integer
    ' Parameter:		None
    ' Description:		Load the error template for the payment page from the database
    ' History:			16 Oct 2008	Created
    '********************************************************************************************
    Private Function iLoadErrUI() As Integer
        Dim iReturn As Integer = 0
        Dim szTemplatePath As String = ""
        Dim szHTMLContent As String = ""

        szTemplatePath = ConfigurationManager.AppSettings("ErrTemplate")

        If szTemplatePath <> "" Then
            GetTemplate(szTemplatePath, szHTMLContent)
            If szHTMLContent <> "" Then
                If stPayInfo.szMerchantReturnURL <> "" Then
                    objCommon.szFormatPlaceHolder(szHTMLContent, "[REDIRECTURL]", stPayInfo.szMerchantReturnURL)
                Else
                    objCommon.szFormatPlaceHolder(szHTMLContent, "[REDIRECTURL]", "error.htm")
                End If

                iReturn = iCustomizeErrPage(szHTMLContent)
                If iReturn <> 0 Then
                    iReturn = -1
                    Return iReturn
                End If

                g_szHTML = szHTMLContent
            Else
                iReturn = -1
            End If
        Else
            iReturn = -1
        End If

        Return iReturn
    End Function

    '********************************************************************************************
    ' Class Name:		Payment
    ' Function Name:	iCustomizeErrPage
    ' Function Type:	Integer   
    ' Parameter:		sz_HTMLContent
    ' Description:		Customize content/return fields on error reply page
    ' History:			17 Oct 2008
    '********************************************************************************************
    Private Function iCustomizeErrPage(ByRef sz_HTMLContent As String) As Integer
        Dim iReturn As Integer = -1

        objCommon.szFormatPlaceHolder(sz_HTMLContent, "[TXNTYPE]", stPayInfo.szTxnType)
        objCommon.szFormatPlaceHolder(sz_HTMLContent, "[SERVICEID]", stPayInfo.szServiceID)
        objCommon.szFormatPlaceHolder(sz_HTMLContent, "[MERCHANTTXNID]", stPayInfo.szMerchantTxnID)
        objCommon.szFormatPlaceHolder(sz_HTMLContent, "[ORDERNUMBER]", stPayInfo.szOrderNumber)
        objCommon.szFormatPlaceHolder(sz_HTMLContent, "[TXNAMOUNT]", String.Format("{0:0.00}", stPayInfo.szTxnAmount))
        objCommon.szFormatPlaceHolder(sz_HTMLContent, "[CURRENCYCODE]", stPayInfo.szCurrencyCode)
        'objCommon.szFormatPlaceHolder(sz_HTMLContent, "[BASETXNAMOUNT]", stPayInfo.szBaseTxnAmount)         'Added on 8 Aug 2009, to cater for base currency booking confirmation enhancement
        'objCommon.szFormatPlaceHolder(sz_HTMLContent, "[BASECURRENCYCODE]", stPayInfo.szBaseCurrencyCode)   'Added on 8 Aug 2009, to cater for base currency booking confirmation enhancement
        'objCommon.szFormatPlaceHolder(sz_HTMLContent, "[ISSUINGBANK]", stPayInfo.szIssuingBank)
        objCommon.szFormatPlaceHolder(sz_HTMLContent, "[SESSIONID]", stPayInfo.szSessionID)
        objCommon.szFormatPlaceHolder(sz_HTMLContent, "[GATEWAYTXNID]", stPayInfo.szTxnID)
        objCommon.szFormatPlaceHolder(sz_HTMLContent, "[TXNSTATE]", stPayInfo.iTxnState.ToString)
        objCommon.szFormatPlaceHolder(sz_HTMLContent, "[TXNSTATUS]", stPayInfo.iMerchantTxnStatus.ToString)
        objCommon.szFormatPlaceHolder(sz_HTMLContent, "[BANKREFNO]", stPayInfo.szHostTxnID)
        objCommon.szFormatPlaceHolder(sz_HTMLContent, "[RESPMESG]", stPayInfo.szTxnMsg)
        ' objCommon.szFormatPlaceHolder(sz_HTMLContent, "[HASHMETHOD]", stPayInfo.szHashMethod)
        'objCommon.szFormatPlaceHolder(sz_HTMLContent, "[HASHVALUE]", stPayInfo.szHashValue)

        iReturn = 0

        Return iReturn
    End Function

    '********************************************************************************************
    ' Class Name:		Payment
    ' Function Name:	GetAllHTTPVal
    ' Function Type:	Boolean.  Return False if error.
    ' Parameter:		NONE
    ' Description:		Get all form's or query string's values
    ' History:			4 Oct 2008
    '********************************************************************************************
    Public Function szGetAllHTTPVal() As String
        Dim szFormValues As String
        Dim szTempKey As String
        Dim szTempVal As String

        Dim iCount As Integer = 0
        Dim iLoop As Integer = 0

        szFormValues = "[User IP: " + Request.UserHostAddress + "] "

        iCount = Request.Form.Count

        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
                        DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
                        " > No. of Form Request Fields (" + iCount.ToString() + ")")

        If (iCount > 0) Then
            For iLoop = 1 To iCount
                szTempKey = ""
                szTempKey = Request.Form.GetKey(iLoop - 1)

                szFormValues = szFormValues + szTempKey + "="

                szTempVal = ""
                szTempVal = Server.UrlDecode(Request.Form.Get(iLoop - 1))

                'Mask CardPAN
                If ((szTempKey <> "")) Then 'Added by Jeff, 20 Dec 2012, checking of CardNo, CardExp, CardCVV2
                    If (szTempKey.ToLower = "cardno") Or (szTempKey.ToLower = "cardexp") Or (szTempKey.ToLower = "cardcvv2") Or (szTempKey.ToLower = "cardpan") Or (szTempKey.ToLower = "cardno_1") Or (szTempKey.ToLower = "cardno_2") Or (szTempKey.ToLower = "cardno_3") Or (szTempKey.ToLower = "cardno_4") Or (szTempKey.ToLower = "epmonth2") Or (szTempKey.ToLower = "epyear2") Or (szTempKey.ToLower = "cvv2") Or (szTempKey.ToLower = "param2") Or (szTempKey.ToLower = "param3") Then
                        If szTempVal.Length > 10 Then
                            szTempVal = szTempVal.Substring(0, 6) + "".PadRight(szTempVal.Length - 6 - 4, "X") + szTempVal.Substring(szTempVal.Length - 4, 4)
                            'szFormValues = szFormValues + szTempVal.Substring(0, 6) + "".PadRight(szTempVal.Length - 6 - 4, "X") + szTempVal.Substring(szTempVal.Length - 4, 4) + "&"
                        Else
                            szTempVal = "".PadRight(szTempVal.Length, "X")
                            'szFormValues = szFormValues + "".PadRight(szTempVal.Length, "X") + "&"
                        End If
                    End If

                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
                                " > Field(" + szTempKey + ") Value(" + szTempVal + ")")

                    If (iLoop = iCount) Then
                        szFormValues = szFormValues + szTempVal
                    Else
                        szFormValues = szFormValues + szTempVal + "&"
                    End If
                End If
            Next iLoop
        Else
            iCount = Request.QueryString.Count

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
                            DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
                            " > No. of QueryString Request Fields (" + iCount.ToString() + ")")

            For iLoop = 1 To iCount
                szTempKey = ""
                szTempKey = Request.QueryString.GetKey(iLoop - 1)

                szFormValues = szFormValues + szTempKey + "="

                szTempVal = ""
                szTempVal = Server.UrlDecode(Request.QueryString.Get(iLoop - 1))

                'Mask CardPAN
                If ((szTempKey <> "")) Then 'Added by Jeff, 20 Dec 2012, checking of CardNo, CardExp, CardCVV2
                    If (szTempKey.ToLower = "cardno") Or (szTempKey.ToLower = "cardexp") Or (szTempKey.ToLower = "cardcvv2") Or (szTempKey.ToLower = "cardpan") Or (szTempKey.ToLower = "cardno_1") Or (szTempKey.ToLower = "cardno_2") Or (szTempKey.ToLower = "cardno_3") Or (szTempKey.ToLower = "cardno_4") Or (szTempKey.ToLower = "epmonth2") Or (szTempKey.ToLower = "epyear2") Or (szTempKey.ToLower = "cvv2") Or (szTempKey.ToLower = "param2") Or (szTempKey.ToLower = "param3") Then
                        If szTempVal.Length > 10 Then
                            szTempVal = szTempVal.Substring(0, 6) + "".PadRight(szTempVal.Length - 6 - 4, "X") + szTempVal.Substring(szTempVal.Length - 4, 4)
                            'szFormValues = szFormValues + szTempVal.Substring(0, 6) + "".PadRight(szTempVal.Length - 6 - 4, "X") + szTempVal.Substring(szTempVal.Length - 4, 4) + "&"
                        Else
                            szTempVal = "".PadRight(szTempVal.Length, "X")
                            'szFormValues = szFormValues + "".PadRight(szTempVal.Length, "X") + "&"
                        End If
                    End If

                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), _
                                DebugInfo.__METHOD__(), DebugInfo.__LINE__(), _
                                " > Field(" + szTempKey + ") Value(" + szTempVal + ")")

                    If (iLoop = iCount) Then
                        szFormValues = szFormValues + szTempVal
                    Else
                        szFormValues = szFormValues + szTempVal + "&"
                    End If
                End If
            Next iLoop
        End If

        szReqStr = szFormValues.Substring(szFormValues.IndexOf("]") + 2)
        Return szFormValues
    End Function

    Public Sub New()
        szLogPath = ConfigurationManager.AppSettings("LogPath")
        iLogLevel = Convert.ToInt32(ConfigurationManager.AppSettings("LogLevel"))

        objCommon = New Common
        InitLogger()

        objHash = New Hash(objLoggerII)
        objHTTP = New CHTTP(objLoggerII)
    End Sub

    Public Sub InitLogger()
        If (objLoggerII Is Nothing) Then

            If (szLogPath Is Nothing) Then
                szLogPath = "C:\\HostSim.log"
            End If

            If (iLogLevel < 0 Or iLogLevel > 5) Then
                iLogLevel = 3
            End If

            'oLoggerII = New LoggerII.CLoggerII(
            objLoggerII = LoggerII.CLoggerII.GetInstanceX(szLogPath + ".log", iLogLevel, 255)

        End If
    End Sub

    Protected Overrides Sub Finalize()
        If Not objLoggerII Is Nothing Then
            objLoggerII.Dispose()
            objLoggerII = Nothing
        End If

        If Not objCommon Is Nothing Then objCommon = Nothing
        If Not objHash Is Nothing Then objHash = Nothing

        MyBase.Finalize()
    End Sub
End Class