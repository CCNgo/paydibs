﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="OBPayment.aspx.vb" Inherits="HostSim.OBPayment" %>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Bank Simulation Page</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>
<body>
	
	<div id="Login" class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-t-50 p-b-90">
				<form class="login100-form validate-form flex-sb flex-w">
                    <h3 style="margin-bottom:10px">Paydibs Bank Host Simulator</h3>
					<span class="login100-form-title p-b-51">
						Account Login
					</span>

					<div class="wrap-input100 m-b-16" data-validate = "Username is required">
						<input class="input100" type="text" name="username" placeholder="Username">
						<span class="focus-input100"></span>
					</div>
					
					<div class="wrap-input100 validate-input m-b-16" data-validate = "Password is required">
						<input class="input100" type="password" name="pass" placeholder="Password">
						<span class="focus-input100"></span>
					</div>
					
					<div class="flex-sb-m w-full p-t-3 p-b-24">
						<div class="contact100-form-checkbox">
							<input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
							<label class="label-checkbox100" for="ckb1">
								Remember me
							</label>
						</div>

						<div>
							<a href="#" class="txt1">
								Forgot?
							</a>
						</div>
					</div>

					<div class="container-login100-form-btn m-t-17">
						<button id="AcctPage" class="login100-form-btn" style="background-color:coral">
							Login
						</button>
					</div>

				</form>
			</div>
		</div>
	</div>

    <div id="Acct" style="display:none;" align="center">
        <div class="panel panel-primary" style="border: 2px; max-width:400px; align-content:center">
            <div class="panel-header" style="margin-top:50px;">
                <h4>Transaction Status</h4>
            </div>
            <div class="panel-body" style="margin-top:50px;">
                <p>This is Bank Simulation Page for Paydibs transaction testing</p>
                <p>Select transaction status for testing</p><br />
                
                <input type="radio" name="status" value="Success" checked> Success<br />
                <input type="radio" name="status" value="Failed"> Failed 
            </div>
            <div class="container-login100-form-btn m-t-17">
				<button id="Confirm" class="login100-form-btn" style="background-color:coral">
					Confirm
				</button>
			</div>
        </div>
    </div>
	

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

    <script type="text/javascript">
        document.getElementById("AcctPage").onclick = function () {
            document.getElementById("Login").style.display = 'none';
            document.getElementById("Acct").style.display = 'Block';
        };

        document.getElementById("Confirm").onclick = function () {
            location.href = "[HOSTURL]?[HOSTPARAMS]&Status=" + document.getElementsByName("status").values;
        };
        
</script>

</body>
</html>


