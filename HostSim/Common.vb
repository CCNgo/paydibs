﻿Imports System.Text.RegularExpressions

'Module Common
Public Class Common

    Public Const C_TXN_SUCCESS_0 = "Transaction Successful"
    Public Const C_TXN_FAILED_1 = "Transaction Failed"
    Public Const C_COM_TIMEOUT_5 = "Timeout Out Failed"
    Public Const C_FAILED_HOTCARD_9 = "Hot Card / Hot Name"
    Public Const C_PENDING_RESP_30 = "Transaction Pending For Response"
    Public Const C_QUERY_HOST_33 = "Querying Host"
    Public Const C_FAILED_TO_SEND_46 = "Transaction Failed To Send"
    Public Const C_TXN_NOT_FOUND_21 = "Transaction Not Found"
    Public Const C_DB_UNKNOWN_ERROR_901 = "Failed to Connect DB"
    Public Const C_INVALID_HOST_902 = "Failed to Get Host Information"
    Public Const C_FAILED_SET_TXN_STATUS_905 = "Failed to Set Transaction Status"
    Public Const C_FAILED_STORE_TXN_906 = "Failed to Store Transaction" ' Failed bInsertNewTxn
    Public Const C_FAILED_UPDATE_TXN_RESP_907 = "Failed to Update Transaction Response"
    Public Const C_FAILED_FORM_REQ_914 = "Failed to Form Message"
    Public Const C_FAILED_COM_INIT_917 = "Failed to Initiate Communications"
    Public Const C_INVALID_MID_2901 = "Invalid Service ID"                  ' Failed bGetMerchant in bInitPayment
    Public Const C_COMMON_INVALID_FIELD_2902 = "Invalid Field"
    Public Const C_COMMON_MISSING_ELEMENT_2903 = "Missing Required Field"   ' Failed bPaymentMain
    Public Const C_INVALID_INPUT_2906 = "Invalid Input"         ' Failed bCheckUniqueTxn, bInitPayment
    Public Const C_INVALID_HOST_REPLY_2907 = "Invalid Host Reply"
    Public Const C_INVALID_SIGNATURE_2909 = "Invalid Hash Value"
    Public Const C_LATE_HOST_REPLY_2908 = "Late Host Reply"
    Public Const C_COMMON_UNKNOWN_ERROR_2999 = "Internal Error" 'Failed to GenTxnID, bInsertTxnResp, bCheckUniqueTxn, bInsertNewTxn, bProcessSale, bInitPayment

    Public Enum TXN_STATUS
        TXN_SUCCESS = 0                     '1
        TXN_FAILED = 1                      '4
        TXN_PENDING = 2                     'Processing
        TXN_PENDING_CONFIRM_BOOKING = 3     'Host returned approved for the payment, pending OpenSkies AddPymt reply OR trying to send AddPymt request to OpenSkies, need extend booking
        TXN_TIMEOUT_HOST_RETURN = 4         'Host no response for 1 hour 45 minutes, need extend booking, stop and generate exception report
        TXN_TIMEOUT_HOST_UNPROCESSED = 5    'Host's reply status is "not processed" for 1 hour 45 minutes, no need extend booking, no need exception report
        TIME_OUT_FAILED = 6                 'Txn existed in Request table for more than 2 hours
        TXN_HOST_UNPROCESSED = 7            'Host's reply status is "not processed"
        TXN_HOST_RETURN_TIMEOUT = 8         'Host no response/timeout
        TXN_REVERSED = 9                    'Txn reversed successfully

        PENDING_RESP = 30
        PENDING_REVERSAL = 31
        PROCESSING_RESP = 32
        QUERYING_HOST = 33
        E_FAILED_STORE_TXN = 906    ' Failed bInsertNewTxn
        E_INVALID_MERCHANT = 2901
        INVALID_INPUT = 2906        ' Failed GetData, bInitPayment, bPaymentMain
        INVALID_HOST_REPLY = 2907
        LATE_HOST_REPLY = 2908      ' Late reply from Host
        INVALID_HOST_IP = 2909
        INVALID_HOST_REPLY_GATEWAYTXNID = 2910
        INVALID_HOST = 2911
        INTERNAL_ERROR = 2999       ' Failed bInitPayment, bProcessSale
    End Enum

    Public Enum TXN_STATE
        INITIAL = 1             'Received request for a particular txn for the 1st time from AA Web or other merchants

        SENT_TO_HOST = 2        'Can be redirect through client's browser to Host,e.g.CIMB(popup) OR sent through WebComm to Host  'PENDING_PROCESS = 2
        SENT_ADD_SEATS = 3      'Sent add seats request to OpenSkies/NewSkies   - Added on 16 Aug 2009, to cater for add seats

        SUCCESS = 4
        FAILED = 5              ' Failed spInsTxnResp, bCheckUniqueTxn, bInsertNewTxn
        ABORT = 6               ' Failed GenTxnID, GetData, bPaymentMain
        ROLLBACK_PENDING = 7
        ROLLBACK_SUCCESS = 8
        COMMIT_PENDING = 9
        COMMIT = 10
        ROLLBACK_FAILED = 11
        COMMIT_FAILED = 12

        RECV_ADD_SEATS = 13     'Received add seats reply from OpenSkies/NewSkies   - Added on 16 Aug 2009, to cater for add seats

        SENT_2ND_ENTRY = 15     'Replied an entry page to client, e.g. UserID entry(BCA); Tokens info entry(Mandiri)
        RECV_2ND_ENTRY = 16     'e.g. Received UserID (BCA);Tokens info(Mandiri)                  '    'PROCESSING = 3
        RECV_FROM_HOST = 17     'Received reply from Host
        SENT_QUERY_HOST = 18    'Sent query request to Host
        RECV_QUERY_HOST = 19    'Received query reply from Host
        SENT_QUERY_OS = 20      'Sent query request to OpenSkies/NewSkies
        RECV_QUERY_OS = 21      'Received query reply from OpenSkies/NewSkies
        SENT_CONFIRM_OS = 22    'Sent Add/Confirm Payment request to OpenSkies/NewSkies
        RECV_CONFIRM_OS = 23    'Received Add/Confirm Payment reply from OpenSkies/NewSkies
        RECV_HOST_QUERY = 24    'Received Host's Query request, e.g. BCA
        SENT_HOST_QUERY = 25    'Sent Host's Query reply, e.g. BCA
        SENT_REPLY_CLIENT = 26  'Sent reply to client/merchant
        SENT_RSALE_HOST = 27    'Sent Reversal Sale request to Host     - Added on 10 Apr 2010
        RECV_RSALE_HOST = 28    'Received Reversal Sale reply from Host - Added on 10 Apr 2010
    End Enum

    Public Enum TXN_Type
        SALE = 3
        REVERSAL = 13
    End Enum

    Public Enum ERROR_CODE
        E_SUCCESS = 0 '

        E_TXN_SUCCESSFUL = 1 'From NA--
        E_TXN_INITIATED = 2 'From NA--
        E_TXN_AUTHORIZED = 3 'From NA--
        E_TXN_FAILED = 4 'From NA--
        E_COM_TIMEOUT = 5 'From NA--
        E_TXN_REVERSED = 7 'From NA--
        E_TXN_VOIDED = 8 'From NA
        E_TXN_HOTCARD = 9

        E_TXN_PENDING_SALE_RESP = 30 'From NA
        E_TXN_PENDING_REVERSAL_RESP = 31 'From NA
        E_TXN_PENDING_VOID_RESP = 32 'From NA
        E_TXN_PENDING_AUTH_RESP = 33 'From NA

        E_INVALID_PAN = 101 'From NA
        E_INVALID_PAN_LENGTH = 102 'From NA
        E_INVALID_PAN_UNKNOWN = 103 'From NA
        E_INVALID_TRACK2_FORMAT = 104 'From NA

        E_DB_UNKNOWN_ERROR = 901 ' From NA--
        E_INVALID_HOST = 902 ' From NA--
        E_FAILED_GET_REV_RESP = 903 'From NA--
        E_FAILED_REVERSAL = 904 ' From NA--
        E_FAILED_SET_TXN_STATUS = 905 'From NA--
        E_FAILED_STORE_TXN = 906            ' Failed bInsertNewTxn
        E_FAILED_UPDATE_TXN_RESP = 907 ' From NA--
        E_FAILED_FORM_REQ = 914 ' From NA--
        E_FAILED_COM_INIT = 917 ' From NA--

        E_COMMON_INVALID_FIELD = 2902 '
        E_COMMON_MISSING_ELEMENT = 2903

        E_INVALID_INPUT = 2906              ' Failed bCheckUniqueTxn
        E_INVALID_HOST_REPLY = 2907
        E_INVALID_MID = 2901 '
        E_INVALID_SIGNATURE = 2909 ''

        E_COMMON_UNKNOWN_ERROR = 2999       ' Failed GenTxnID, bInsertTxnResp, bCheckUniqueTxn, bInsertNewTxn


        'E_PAN_AUTH_NOT_AVAILABLE = 16
        'E_PAN_NOT_PARTICIPATING = 17

        'E_DB_INVALID_FIELD = 101
        'E_DB_INVALID_TYPE = 102
        'E_DB_MISSING_FIELD = 106
        'E_DB_FAIL_INSERT = 111
        'E_DB_FAIL_UPDATE = 112
        'E_DB_FAIL_DELETE = 113
        'E_DB_FAIL_QUERY = 114

        'E_CERT_INVALID_FILE = 141
        'E_CERT_FILE_EXISTED = 142
        'E_CERT_SIGNATURE_FAILED = 143
        'E_CERT_CHAIN_INCOMPLETE = 144
        E_CERT_ERROR = 51 '

        E_COM_BAD_REQUEST = 41 '
        E_COM_UNAUTHORIZED_URL = 42
        E_COM_FORBIDDEN_URL = 43
        E_COM_URL_UNREACHABLE = 44 '
        E_COM_FILE_ERROR = 45

        'E_MERCHANT_DOES_NOT_EXIST = 241
        'E_MERCHANT_ALREADY_EXIST = 242
        'E_MERCHANT_INACTIVE = 243
        'E_MERCHANT_PWD_MISMATCH = 244
        'E_MERCHANT_PWD_FORMAT = 245
        'E_MERCHANT_ACQ_INVALID = 246
        'E_MERCHANT_INVALID_CURRENCY = 247

        E_CRYPTO_INIT_KEY = 51
        E_CRYPTO_FAIL = 52 ''
        E_CRYPTO_UNKNOWN_ERROR = 53

        'E_CRANGE_NOT_IN_RANGE = 331
        'E_CRANGE_INVALID_BIN = 332

        E_TXN_FAIL_GEN_ID = 61
        E_TXN_INVALID_STATE = 62 '
        E_TXN_NOT_FOUND = 63 '
        E_TXN_INVALID_AMOUNT = 64 '

        E_CAPTURE_FAILED = 500

    End Enum

    '************************************
    'Transaction Member variables
    '************************************
    Public Structure STPayInfo

        'Request fields
        Public szTxnType As String
        Public szPymtMethod As String
        Public szServiceID As String
        Public szMerchantName As String
        Public szGatewayID As String
        Public szMerchantTxnID As String
        Public szOrderNumber As String
        Public szOrderDesc As String
        Public szTxnAmount As String
        Public szBaseTxnAmount As String    '3 Aug 2009, base currency booking confirmation enhancement
        Public szRTxnAmount As String       '11 Apr 2010, Reversal Sale transaction amount
        Public szCurrencyCode As String
        Public szBaseCurrencyCode As String '3 Aug 2009, base currency booking confirmation enhancement
        Public szRCurrencyCode As String    '11 Apr 2010, Reversal Sale currency code
        Public szIssuingBank As String
        Public szLanguageCode As String
        Public szCardPAN As String
        Public szCVV2 As String
        Public szCardType As String
        Public szSessionID As String
        Public szMerchantReturnURL As String
        Public szMerchantSupportURL As String
        Public szRMerchantReturnURL As String   '11 Apr 2010, Reversal Sale Merchant Return URL
        Public szParam1 As String               'Index (token) - length 100
        Public szParam2 As String               'CardPAN - length 100
        Public szMaskedParam2 As String
        Public szParam3 As String               'Expired date - length 100
        Public szMaskedParam3 As String
        Public szParam4 As String               'Card holder name - length 100
        Public szParam5 As String               'Card issueing bank - length 100
        Public szParam6 As String               'Added on 16 Aug 2009, to cater for add seats - change to misc - length 50
        Public szParam7 As String               'misc - length 50
        Public szParam8 As String               'misc - length 50
        Public szParam9 As String               'misc - length 50
        Public szParam10 As String              'misc - length 50
        Public szParam11 As String              'misc - length 255
        Public szParam12 As String              'misc - length 255
        Public szParam13 As String              'misc - length 255
        Public szParam14 As String              'misc - length 255
        Public szParam15 As String              'misc - length 255
        Public szHashMethod As String
        Public szHashValue As String
        Public szHashKey As String

        Public szMaskedCardPAN As String
        Public szTxnID As String
        Public szMachineID As String
        Public szRecv2ndEntry As String     'e.g. BCA (UserID entry), Mandiri (Tokens info entry)
        Public szHTML As String             'Added on 16 Mar 2010, to cater for Hosts Listing Page

        Public iPayAmount As Double
        Public iTxnType As Integer
        Public szPostMethod As String
        Public szTxnDateTime As String      'Date Created
        Public iReqRes As Integer
        Public iTxnIDMaxLen As Integer      'Added on 30 Oct 2009
        Public bReplyMerchant As Boolean    'Added by Jeff, 21 Apr 2011, for KTB, to cater for send query to Host which will reply in another session to DDGW query return page, so, no need reply merchant on same query session and just let query return page to reply merchant instead

        'Merchant 
        Public szMerchantPassword As String
        Public szPaymentTemplate As String
        Public szErrorTemplate As String
        Public szRunningNo As String
        Public iAllowReversal As Integer
        Public iAllowFDS As Integer
        Public iNeedAddOSPymt As Integer

        'Res to client
        Public iTxnStatus As Integer
        Public iTxnState As Integer
        Public iMerchantTxnStatus As Integer
        Public iQueryStatus As Integer
        Public iDuration As Integer
        'Res from Host
        Public szHostTxnID As String
        Public szRespCode As String
        Public szAuthCode As String
        Public szBankRespMsg As String
        Public szHostDate As String
        Public szHostTime As String
        Public szHostMID As String
        Public szHostTID As String
        Public szHostTxnStatus As String
        Public szHostTxnAmount As String
        Public szHostOrderNumber As String
        Public szHostGatewayTxnID As String
        Public szHostCurrencyCode As String
        Public szHostTxnType As String      'Added on 2 Oct 2009
        Public szEncryptKeyPath As String   'Added on 4 Nov 2009, Encrypt key file path
        Public szDecryptKeyPath As String   'Added on 4 Nov 2009, Decrypt key file path
        Public szRedirectURL As String      'Added on 21 Mar 2010
        Public szECI As String              'Added on 5 Oct 2011 by OoiMei for CCGW
        Public szPayerAuth As String        'Added on 5 Oct 2011 by OoiMei for CCGW

        'Res from Host - Action
        Public iAction As Integer
        Public bVerified As Boolean
        'Res from OpenSkies
        Public iOSRet As Integer
        Public iErrSet As Integer
        Public iErrNum As Integer
        'Http connection details
        Public iHttpTimeoutSeconds As Integer

        'Error
        Public szTxnMsg As String   ' RespMesg
        Public szQueryMsg As String
        Public szErrDesc As String
        Public szDate As String
        Public szTime As String

    End Structure

    '************************************
    'Transaction Member variables
    '************************************
    Public Structure STHost
        Public szTID As String
        Public szMID As String
        Public szServerCertName As String
        Public szHostTemplate As String
        Public szPaymentTemplate As String
        Public szSecondEntryTemplate As String
        Public szMesgTemplate As String 'Added on 27 Oct 2009
        Public szURL As String
        Public szQueryURL As String
        Public szReversalURL As String
        Public szAcknowledgementURL As String
        Public szCancelURL As String
        Public szReturnURL As String
        Public szReturnURL2 As String   'Added on 22 Oct 2009
        Public szReturnIPAddresses As String
        Public szDBConnStr As String
        Public szOSPymtCode As String
        Public szSendKey As String      'Secret key to form hash value for request to Host
        Public szReturnKey As String    'Secret key to form hash value for response from Host
        Public szHashMethod As String
        Public szHashValue As String
        Public szSecretKey As String
        Public szInitVector As String
        Public sz3DESAppKey As String
        Public sz3DESAppVector As String
        Public szReserved As String     'Reserved field to store Host reply, especially for the whole encrypted reply
        Public szAcquirerID As String
        Public szPayeeCode As String
        Public szRunningNo As String    'Trace Number/Running Number - Added on 19 Sept 2010

        Public iChannelID As Integer
        Public iHostID As Integer
        Public iPortNumber As Integer
        Public iQueryPortNumber As Integer
        Public iReversalPortNumber As Integer
        Public iAcknowledgementPortNumber As Integer
        Public iCancelPortNumber As Integer
        Public iTimeOut As Long
        Public iRequire2ndEntry As Integer
        Public iNeedReplyAcknowledgement As Integer
        Public iNeedRedirectOTP As Integer 'Added on 21 Mar 2010
        Public iTxnStatusActionID As Integer
        Public iHostReplyMethod As Integer
        Public iGatewayTxnIDFormat As Integer
        Public iQueryFlag As Integer    ' Query flag to determine whether to query the host
        Public iIsActive As Integer     ' Flag to indicate whether Host is enabled or disabled
        Public iAllowReversal As Integer ' Flag to indicate whether Host supports Reversal Sale or Refund - Added on 10 Apr 2010
        Public iNoOfRunningNo As Integer 'Number of digits for Trace Number or Running Number - Added on 18 Sept 2010
        Public iRunningNoUniquePerDay As Integer 'Boolean to indicate whether Trace Number or Running Number is unique per day - Added on 18 Sept 2010
    End Structure
    'End Module

    Public Sub InitHostInfo(ByRef st_HostInfo As STHost)
        st_HostInfo.szTID = ""
        st_HostInfo.szMID = ""
        st_HostInfo.szServerCertName = ""
        st_HostInfo.szHostTemplate = ""
        st_HostInfo.szPaymentTemplate = ""
        st_HostInfo.szSecondEntryTemplate = ""
        st_HostInfo.szMesgTemplate = ""             'Added on 27 Oct 2009
        st_HostInfo.szURL = ""
        st_HostInfo.szQueryURL = ""
        st_HostInfo.szReversalURL = ""
        st_HostInfo.szAcknowledgementURL = ""
        st_HostInfo.szCancelURL = ""
        st_HostInfo.szReturnURL = ""
        st_HostInfo.szReturnURL2 = ""               'Added on 22 Oct 2009
        st_HostInfo.szReturnIPAddresses = ""
        st_HostInfo.szDBConnStr = ""
        st_HostInfo.szOSPymtCode = ""
        st_HostInfo.szSendKey = ""
        st_HostInfo.szReturnKey = ""
        st_HostInfo.szHashMethod = ""
        st_HostInfo.szHashValue = ""
        st_HostInfo.szSecretKey = ""
        st_HostInfo.szInitVector = ""
        st_HostInfo.sz3DESAppKey = "3537146182309002FEB7EBCE55ED3BBA71BED7CEB1D18CAA" '"3537146182309002yeKterceSSED3ppAtibeDtceriDLHGAA"
        st_HostInfo.sz3DESAppVector = "0000000000000000"
        st_HostInfo.szReserved = ""
        st_HostInfo.szAcquirerID = ""
        st_HostInfo.szPayeeCode = ""
        st_HostInfo.szRunningNo = ""                'Added on 19 Sept 2010

        st_HostInfo.iChannelID = 0
        st_HostInfo.iHostID = 0
        st_HostInfo.iPortNumber = 0
        st_HostInfo.iQueryPortNumber = 0
        st_HostInfo.iReversalPortNumber = 0
        st_HostInfo.iAcknowledgementPortNumber = 0
        st_HostInfo.iCancelPortNumber = 0
        st_HostInfo.iTimeOut = 900  'seconds
        st_HostInfo.iRequire2ndEntry = 0
        st_HostInfo.iNeedReplyAcknowledgement = 0
        st_HostInfo.iNeedRedirectOTP = 0            'Added on 21 Mar 2010
        st_HostInfo.iTxnStatusActionID = 0
        st_HostInfo.iHostReplyMethod = 0
        st_HostInfo.iGatewayTxnIDFormat = 0
        st_HostInfo.iQueryFlag = 1
        st_HostInfo.iIsActive = 1
        st_HostInfo.iAllowReversal = 0              ' Added on 10 Apr 2010
        st_HostInfo.iNoOfRunningNo = 0              ' Added on 18 Sept 2010
        st_HostInfo.iRunningNoUniquePerDay = 0      ' Added on 18 Sept 2010
    End Sub

    Public Sub InitPayInfo(ByRef st_PayInfo As STPayInfo)

        st_PayInfo.szTxnType = ""
        st_PayInfo.szPymtMethod = ""
        st_PayInfo.szServiceID = ""
        st_PayInfo.szMerchantName = ""
        st_PayInfo.szGatewayID = ""
        st_PayInfo.szMerchantTxnID = ""
        st_PayInfo.szOrderNumber = ""
        st_PayInfo.szOrderDesc = ""
        st_PayInfo.szTxnAmount = ""
        st_PayInfo.szBaseTxnAmount = ""     '3 Aug 2009, base currency booking confirmation enhancement
        st_PayInfo.szRTxnAmount = ""        '11 Apr 2010, Reversal Sale transaction amount
        st_PayInfo.szCurrencyCode = ""
        st_PayInfo.szBaseCurrencyCode = ""  '3 Aug 2009, base currency booking confirmation enhancement
        st_PayInfo.szRCurrencyCode = ""     '11 Apr 2010, Reversal Sale currency code
        st_PayInfo.szIssuingBank = ""
        st_PayInfo.szLanguageCode = ""
        st_PayInfo.szCardPAN = ""
        st_PayInfo.szCVV2 = ""
        st_PayInfo.szCardType = ""
        st_PayInfo.szSessionID = ""
        st_PayInfo.szMerchantReturnURL = ""
        st_PayInfo.szMerchantSupportURL = ""
        st_PayInfo.szRMerchantReturnURL = ""    '11 Apr 2010, Reversal Sale Merchant Return URL
        st_PayInfo.szParam1 = ""
        st_PayInfo.szParam2 = ""
        st_PayInfo.szMaskedParam2 = ""
        st_PayInfo.szParam3 = ""
        st_PayInfo.szMaskedParam3 = ""
        st_PayInfo.szParam4 = ""
        st_PayInfo.szParam5 = ""
        st_PayInfo.szParam6 = ""            'Added on 16 Aug 2009, to cater for add seats
        st_PayInfo.szParam7 = ""
        st_PayInfo.szParam8 = ""
        st_PayInfo.szParam9 = ""
        st_PayInfo.szParam10 = ""
        st_PayInfo.szParam11 = ""
        st_PayInfo.szParam12 = ""
        st_PayInfo.szParam13 = ""
        st_PayInfo.szParam14 = ""
        st_PayInfo.szParam15 = ""
        st_PayInfo.szHashMethod = ""
        st_PayInfo.szHashValue = ""
        st_PayInfo.szHashKey = ""
        st_PayInfo.szPostMethod = ""
        st_PayInfo.szTxnDateTime = ""
        st_PayInfo.iReqRes = 1
        st_PayInfo.iTxnIDMaxLen = 0         'Added on 30 Oct 2009
        st_PayInfo.bReplyMerchant = True    'Added by Jeff, 20 Apr 2011

        st_PayInfo.szMaskedCardPAN = ""
        st_PayInfo.szTxnID = ""
        st_PayInfo.szMachineID = ""
        st_PayInfo.szRecv2ndEntry = ""
        st_PayInfo.szHTML = ""              'Added on 16 Mar 2010

        st_PayInfo.iTxnStatus = -1
        st_PayInfo.iTxnState = -1
        st_PayInfo.iMerchantTxnStatus = -1

        st_PayInfo.szHostTxnID = ""
        st_PayInfo.szRespCode = ""
        st_PayInfo.szAuthCode = ""
        st_PayInfo.szBankRespMsg = ""
        st_PayInfo.szHostDate = ""
        st_PayInfo.szHostTime = ""
        st_PayInfo.szHostMID = ""
        st_PayInfo.szHostTID = ""
        st_PayInfo.szHostTxnStatus = ""
        st_PayInfo.szHostTxnAmount = ""
        st_PayInfo.szHostOrderNumber = ""
        st_PayInfo.szHostGatewayTxnID = ""
        st_PayInfo.szHostCurrencyCode = ""
        st_PayInfo.szHostTxnType = ""       ' Added on 2 Oct 2009
        st_PayInfo.szEncryptKeyPath = ""
        st_PayInfo.szDecryptKeyPath = ""
        st_PayInfo.szRedirectURL = ""   ' Added on 21 Mar 2010
        st_PayInfo.szECI = ""           'Added on 5 Oct 2011 by OoiMei for CCGW
        st_PayInfo.szPayerAuth = ""     'Added on 5 Oct 2011 by OoiMei for CCGW

        st_PayInfo.iAction = 0
        st_PayInfo.bVerified = True    ' Added on 22 Apr 2009 for verification of host's replied fields

        st_PayInfo.iOSRet = 99
        st_PayInfo.iErrSet = -1
        st_PayInfo.iErrNum = -1
        st_PayInfo.iHttpTimeoutSeconds = 0

        st_PayInfo.iQueryStatus = 2    ' Query error
        st_PayInfo.iDuration = -1
        st_PayInfo.szTxnMsg = ""
        st_PayInfo.szQueryMsg = ""
        st_PayInfo.szErrDesc = ""

        st_PayInfo.szMerchantPassword = ""
        st_PayInfo.szPaymentTemplate = ""
        st_PayInfo.szErrorTemplate = ""
        st_PayInfo.szRunningNo = ""
        st_PayInfo.iAllowReversal = 0
        st_PayInfo.iAllowFDS = 0
        st_PayInfo.iNeedAddOSPymt = 0

        st_PayInfo.szDate = ""
        st_PayInfo.szTime = ""
    End Sub

    Public Function szFormatPlaceHolder(ByRef szOriText As String, ByVal szTextToFind As String, ByVal szTextToReplace As String)

        If InStr(szOriText, szTextToFind) Then
            If (szTextToReplace.Trim.Length > 0) Then
                szOriText = szOriText.Replace(szTextToFind, szTextToReplace)
            Else
                szOriText = szOriText.Replace(szTextToFind, "")
            End If
        End If

    End Function

    '********************************************************************************************
    ' Class Name:		Common
    ' Function Name:	szPad
    ' Function Type:	String
    ' Parameter:		sz_Src     - Source string to be padded
    '                   i_Length   - Length of output string
    '                   sz_PadSide - Which side to pad
    '                   sz_PadChar - What character to pad
    ' Description:		Perform padding
    ' History:			16 Nov 2010 by Sean
    '********************************************************************************************
    Public Function szPad(ByVal sz_Src As String, ByVal i_Length As Integer, ByVal sz_PadSide As String, ByVal sz_PadChar As String) As String
        If ("R" = sz_PadSide.ToUpper) Then
            sz_Src = sz_Src.PadRight(i_Length, sz_PadChar)
        ElseIf ("L" = sz_PadSide.ToUpper) Then
            sz_Src = sz_Src.PadLeft(i_Length, sz_PadChar)
        End If

        Return sz_Src
    End Function

    '********************************************************************************************
    ' Class Name:		Common
    ' Function Name:	szReplaceSpecialChar
    ' Function Type:	String
    ' Parameter:		sz_Src     - Source string to be replace
    '                   sz_Type    - C for convert special character to tag, R is convert tag back to special character
    ' Description:		Replace special char to specific tag or vice versa
    ' History:			23 Sept 2011 by OoiMei
    '********************************************************************************************
    Public Function szReplaceSpecialChar(ByVal sz_Src As String, ByVal sz_Type As String) As String
        'Dim SpecialChars As Char() = "!@#$%^&*(){}[]""_+<>?/".ToCharArray()
        'Dim SpecialChars As Char() = "!#?".ToCharArray()
        'Dim regexTag As Regex

        Try
            If sz_Type = "C" Then
                'For Each ch As Char In sz_Src
                '    If Array.IndexOf(SpecialChars, ch) = 0 Then ' temporary will replace for 3 special character as below 
                '        If ch = "!" Then
                '            sz_Src = sz_Src.Replace("!", "[EXC]")
                '        ElseIf ch = "?" Then
                '            sz_Src = sz_Src.Replace("?", "[QUE]")
                '        ElseIf ch = "#" Then
                '            sz_Src = sz_Src.Replace("#", "[HASH]")
                '        End If
                '    End If
                'Next
                sz_Src = sz_Src.Replace("!", "[EXC]") '.Replace("?", "[QUE]").Replace("#", "[HASH]")
            Else 'Type R will replace tag to special character. Current Regex is not function in proper way because of "[]" this had been use as pattern define

                'If True = regexTag.IsMatch(sz_Src, "[[EXC]]") Then
                '    regexTag.Replace(sz_Src, "[[EXC]]", "!")
                'End If

                'If True = regexTag.IsMatch(sz_Src, "[[QUE]]") Then
                '    regexTag.Replace(sz_Src, "[[QUE]]", "?")
                'End If

                'If True = regexTag.IsMatch(sz_Src, "[[HASH]]") Then
                '    regexTag.Replace(sz_Src, "[[HASH]]", "#")
                'End If

                sz_Src = sz_Src.Replace("[EXC]", "!") '.Replace("[QUE]", "?").Replace("[HASH]", "#")
            End If

        Catch ex As Exception
            sz_Src = ""
        End Try

        Return sz_Src
    End Function

    '********************************************************************************************
    ' Class Name:		Common
    ' Function Name:	szFormatStringRestructure
    ' Function Type:	String
    ' Parameter:		asz_Src, sz_Value    - Source string to be restructure
    ' Description:		Restruture the string with subtring function and return a new string output
    ' History:			8 Dec 2011 by OoiMei
    '********************************************************************************************
    Public Function szFormatStringRestructure(ByVal asz_Src() As String, ByVal sz_Value As String) As String
        Dim szOutput As String = ""
        Dim szSubStr As String = ""
        Dim aszIndicator() As String
        Dim i As Integer

        Try
            'input asz_Src example = R:2 / L:2 / M:2:2 / [param3]
            For i = 0 To asz_Src.GetUpperBound(0) - 1
                aszIndicator = Split(asz_Src(i), ":")
                Select Case aszIndicator(0).ToUpper()
                    Case "R"
                        szSubStr = Right(sz_Value, aszIndicator(1))
                    Case "L"
                        szSubStr = Left(sz_Value, aszIndicator(1))
                    Case "M"
                        szSubStr = sz_Value.Substring(aszIndicator(1), aszIndicator(2))
                End Select
                szOutput += szSubStr
            Next

        Catch ex As Exception
            szOutput = ""
        End Try

        Return szOutput
    End Function

End Class

