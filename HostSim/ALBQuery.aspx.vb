﻿Imports System
Imports System.Reflection   'Assembly
Imports System.Text 'UTF8Encoding
Imports System.IO
Imports System.Data
Imports System.Security.Cryptography
Imports System.Threading
Imports System.Text.RegularExpressions
Imports System.Web
Imports System.Xml
Imports Microsoft.VisualBasic.Strings
Imports System.Net
Imports System.Net.Security

Public Class ALBQuery
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetNoStore()

        If Not Page.IsPostBack Then

        End If
    End Sub

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim iRet As Integer = -1
        Dim objHttpWebRequest As HttpWebRequest
        Dim objHTTPWebResponse As HttpWebResponse
        Dim objStreamWriter As StreamWriter
        Dim objStreamReader As StreamReader
        Dim sbInStream As System.Text.StringBuilder
        Dim iStartTickCount As Integer
        Dim iTimeout As Integer

        Dim pxMID As String = ""
        Dim pxRef As String = ""
        Dim pxTxnID As String = "" '"883312"
        Dim pxHashMID As String = ""
        Dim iCheckSum As Integer

        pxTxnID = Request.Form("TxnRunNo")

        Select Case DropDownList1.SelectedIndex
            Case 0
                pxMID = "0000010212025"
                pxRef = "SPIC2025"
            Case 1
                pxMID = "0000010208379"
                pxRef = "ACSE8379"
            Case 2
                pxMID = "0000010214591"
                pxRef = "ZCOV4591"
        End Select

        For Each c As Char In pxTxnID
            iCheckSum += Asc(c)
        Next

        If (1000 <= iCheckSum) Then
            iCheckSum = iCheckSum Mod 1000
        End If

        pxHashMID = Convert.ToString(Convert.ToInt64(pxMID) + iCheckSum)

        Dim sz_HTTPString As String = "PX_VERSION=1.1&PX_PURCHASE_ID=" + pxTxnID + "&PX_MERCHANT_ID=" + pxHashMID + "&PX_REF=" + pxRef + "&vcc_action=110"
        Dim sz_URL As String = "https://www.abmb.com.my/threed/SVLMPIInquiry"

        Dim i_Timeout As Integer = 3000
        Dim sz_SecurityProtocol As String = "TLS12"
        Dim sz_HTTPResponse As String = ""

        Try
            '======================================== Sets HttpWebRequest properties ====================================
            sbInStream = New System.Text.StringBuilder
            sbInStream.Append(sz_HTTPString)
            iTimeout = i_Timeout * 1000 'converts input timeout param from second to milliseconds

            objHttpWebRequest = WebRequest.Create(sz_URL)                       'Posting URL
            objHttpWebRequest.ContentType = "application/x-www-form-urlencoded" 'default value
            objHttpWebRequest.Method = "POST"                                   'default value
            objHttpWebRequest.ContentLength = sbInStream.Length ' Must be set b4 retrieving stream for sending data
            objHttpWebRequest.KeepAlive = False
            objHttpWebRequest.Timeout = iTimeout                                '(in milliseconds)

            'Ignore invalid server certificates or else may encounter WebException with e.Status = WebExceptionStatus.TrustFailure
            'When the CertificatePolicy property is set to an ICertificatePolicy interface instance, the ServicePointManager
            'uses the certificate policy defined in that instance instead of the default certificate policy.
            'The default certificate policy allows valid certificates, as well as valid certificates that have expired.
            ServicePointManager.CertificatePolicy = New SecPolicy

            'Added by Jeff, 19 Jun 2015
            If (sz_SecurityProtocol.Trim() <> "") Then
                Select Case sz_SecurityProtocol.Trim().ToUpper()
                    Case "SSL3"
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3
                    Case "TLS12"
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
                    Case "TLS11"
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11
                    Case "TLS"
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                    Case Else
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3
                End Select

                ' allows for validation of SSL conversations
                ServicePointManager.ServerCertificateValidationCallback = Nothing 'Function() True   'C#: delegate { return true }
            End If

            iStartTickCount = System.Environment.TickCount()

            '=========================================== Post request ==================================================
            '- StreamWriter constructor creates a StreamWriter with UTF-8 encoding whose GetPreamble method returns an
            'empty byte array. The BaseStream property is initialized using the stream parameter (i.e. objHttpWebRequest.GetRequestStream)
            '- GetRequestStream method returns a stream to use to send data for the HttpWebRequest. Once the Stream
            'instance has been returned, you can send data with the HttpWebRequest by using the Stream.Write method.
            'NOTE:    Must set the value of the ContentLength property before retrieving the stream.
            'CAUTION: Must call the Stream.Close method to close the stream and release the connection for reuse.
            '         Failure to close the stream will cause application to run out of connections.

            objStreamWriter = New StreamWriter(objHttpWebRequest.GetRequestStream())
            objStreamWriter.Write(sbInStream.ToString)  ' Sends data
            objStreamWriter.Close() ' Closes the stream and release the connection for reuse to avoid running out of connections

            '============================================= Get response =================================================
            '- GetResponse method returns a WebResponse instance containing the response from the Internet resource.
            'The actual instance returned is an instance of HttpWebResponse, and can be typecast to that class to access
            'HTTP-specific properties. When POST method is used, must get the request stream, write the data to be posted,
            'and close the stream. This method blocks waiting for content to post; If there is no time-out set and
            'content is not provided, application will block indefinitely.

            objHTTPWebResponse = objHttpWebRequest.GetResponse()

            If (objHttpWebRequest.HaveResponse And objHTTPWebResponse.StatusCode = HttpStatusCode.OK) Then

                objStreamReader = New StreamReader(objHTTPWebResponse.GetResponseStream)

                '================ Implements Timeout for getting HTTP reply from host <START> ====================
                Dim objRespReader As RespReader
                Dim oThreadStart As ThreadStart
                Dim oThread As Thread
                Dim iElapsedTime As Integer

                objRespReader = New RespReader
                'objRespReader.SetLogger(objLoggerII)
                objRespReader.SetReader(objStreamReader)

                oThreadStart = New ThreadStart(AddressOf objRespReader.WaitForResp)
                oThread = New Thread(oThreadStart)
                oThread.Start()

                'iStartTickCount = System.Environment.TickCount()
                iElapsedTime = System.Environment.TickCount() - iStartTickCount

                While ((iElapsedTime < iTimeout) And Not objRespReader.IsCompleted())
                    System.Threading.Thread.Sleep(50)
                    iElapsedTime = System.Environment.TickCount() - iStartTickCount
                End While

                If (iElapsedTime >= iTimeout) Then
                    ' Timeout
                    iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                Else
                    'Completed AND No Timeout. Could be got reply or exception
                    If (True = objRespReader.IsSucceeded()) Then
                        'Received reply within timeout period
                        sz_HTTPResponse = objRespReader.GetRespStream()

                        If ("" = sz_HTTPResponse) Then  'Added by OoiMei 4 jun 2014. Firefly FF
                            iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                        End If
                    Else
                        'Exception within timeout period
                        iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                    End If
                End If

                oThread.Abort()
                oThreadStart = Nothing
                oThread = Nothing
                'objRespReader.Dispose()
                objRespReader = Nothing

                'Releases the resources of the response stream
                'NOTE: Failure to close the stream will cause application to run out of connections.
                objStreamReader.Close()
                If Not objStreamReader Is Nothing Then objStreamReader = Nothing 'Added on 1 Mar 2009
                '================== Implements Timeout for getting HTTP reply from host <END> =====================
            Else

            End If

            'Releases the resources of the response
            objHTTPWebResponse.Close()
            Dim szRes As String = sz_HTTPResponse.Trim()
            Dim s As String = ""
            Dim t As String = ""

            Using reader As XmlReader = XmlReader.Create(New StringReader(szRes))     'XMLReader read the original message
                While reader.Read()
                    Select Case reader.Name.ToUpper()
                        Case "PX_PURCHASE_ID"
                            s = s + "TranRunNo = " + reader.ReadElementContentAsString() + "<br>"
                        Case "PX_PAN"
                            t = reader.ReadElementContentAsString()
                            If ("null" <> t) Then
                                s = s + "CardPAN = " + t.Substring(0, 6) + "XXXXXX" + t.Substring(12, 4) + "<br>"
                            Else
                                s = s + "CardPAN = " + t + "<br>"
                            End If
                        Case "PX_PURCHASE_AMOUNT"
                            t = reader.ReadElementContentAsString()
                            If ("null" <> t) Then
                                s = s + "Amount = " + t.Substring(0, t.Length - 2) + "." + t.Substring(t.Length - 2, 2) + "<br>"
                            Else
                                s = s + "Amount = " + t + "<br>"
                            End If
                        Case "PX_HOST_DATE"
                            s = s + "Bank DateTime = " + reader.ReadElementContentAsString() + "<br>"
                        Case "PX_ERROR_DESCRIPTION"
                            s = s + "Status = " + reader.ReadElementContentAsString() + "<br>"
                        Case "PX_ERROR_CODE"
                            s = s + "Status Code = " + reader.ReadElementContentAsString() + "<br>"
                        Case "PX_APPROVAL_CODE"
                            s = s + "Approval Code = " + reader.ReadElementContentAsString() + "<br>"
                    End Select
                End While
            End Using

            Label3.Visible = True
            Label3.Text = "Result <br>" + "******** <br>" + s
            'MsgBox(s, vbOKOnly, "Result")

        Catch ex As Exception
            'MsgBox(ex.Message)
        Finally

        End Try

    End Sub
End Class