using System;
using System.IO;
using System.Security.Cryptography; // Added by Jeff, 24 Dec 2013
using System.Text;                  // Added by Jeff, 24 Dec 2013

class Util
{
    internal static void LogWriter(string header)
    {
        StreamWriter streamWriter = null;
        string sProcessLog = Global.ProcessLog.Replace("[YYYYMMDD]", System.DateTime.Today.ToString("yyyyMMdd"));
        try
        {
            if (!File.Exists(sProcessLog))
            {
                FileStream fs = null;
                using (fs = File.Create(sProcessLog))
                {
                    fs.Close();
                }
            }
            using (streamWriter = new StreamWriter(sProcessLog, true))
            {
                string headerLine = string.Empty;
                headerLine = headerLine.PadLeft(header.Length, '=');
                streamWriter.WriteLine(headerLine);
                streamWriter.WriteLine(header);
                streamWriter.WriteLine(headerLine);
                streamWriter.Close();
            }
        }
        catch (Exception ex) { ex.Message.ToString(); }
        finally { if (streamWriter != null) streamWriter.Dispose(); }
    }

    internal static void LogWriter(string processID, string message)
    {
        StreamWriter streamWriter = null;
        string sProcessLog = Global.ProcessLog.Replace("[YYYYMMDD]", System.DateTime.Today.ToString("yyyyMMdd"));
        try
        {
            if (!File.Exists(sProcessLog))
            {
                FileStream fs = null;
                using (fs = File.Create(sProcessLog))
                {
                    fs.Close();
                }
            }
            using (streamWriter = new StreamWriter(sProcessLog, true))
            {
                string seperator = " | ";
                string data = "\n";
                data += DateTime.Now.ToString("HH:mm:ss.fff").PadRight(12);
                data += seperator;
                data += processID.PadRight(10);
                data += seperator;
                data += message;
                streamWriter.WriteLine(data);
                streamWriter.Close();
            }
        }
        catch (Exception ex) { LogWriter("Exception", ex.Message.ToString()); }
        finally { if (streamWriter != null) streamWriter.Dispose(); }
    }

    internal static void ServiceLogWriter(string message)
    {
        StreamWriter streamWriter = null;
        string sServiceLog = Global.ServiceLog.Replace("[YYYYMMDD]", System.DateTime.Today.ToString("yyyyMMdd"));
        try
        {
            if (!File.Exists(sServiceLog))
            {
                FileStream fs = null;
                using (fs = File.Create(sServiceLog))
                {
                    fs.Close();
                }
            }
            using (streamWriter = new StreamWriter(sServiceLog, true))
            {
                streamWriter.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff").PadRight(15) + message);
                streamWriter.Close();
            }
        }
        catch (Exception ex) { ex.Message.ToString(); }
        finally { if (streamWriter != null) streamWriter.Dispose(); }
    }

    internal static bool MoveFile(string fileSource, string fileTarget)
    {
        try
        {
            if (File.Exists(fileTarget)) File.Delete(fileTarget);
            File.Move(fileSource, fileTarget);
        }
        catch (Exception ex)
        { throw ex; }
        return true;
    }

    // Added by Jeff, 25 Dec 2013
    internal static string strDecrypt3DES(string str_CipherText, string str_Key, string str_InitVector)
    {
        TripleDESCryptoServiceProvider obj3DES = new TripleDESCryptoServiceProvider();
        UTF8Encoding objUTF8 = new UTF8Encoding();
        byte[] btInput;
        byte[] btOutput;
        string strClear = null;

        try
        {
            strClear = "";

            obj3DES.Padding = PaddingMode.PKCS7; // Default value
            obj3DES.Mode = CipherMode.CBC;       // Default value
            obj3DES.Key = btHexToByte(str_Key);
            obj3DES.IV = btHexToByte(str_InitVector);
            btInput = btHexToByte(str_CipherText);

            btOutput = btTransform(btInput, obj3DES.CreateDecryptor());

            strClear = objUTF8.GetString(btOutput);
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            if (obj3DES != null) obj3DES = null;
            if (objUTF8 != null) objUTF8 = null;
        }

        return strClear;
    }

    // Added by Jeff, 25 Dec 2013
    internal static byte[] btHexToByte(string str_Hex)
    {
        string strHex = "";

        byte[] btOutput = new byte[str_Hex.Length / 2];
        int iOffset = 0;
        int iCounter = 0;

        iCounter = 0;
        while (iCounter < str_Hex.Length)
        {
            strHex = (str_Hex[iCounter]).ToString() + (str_Hex[iCounter + 1]).ToString();
            btOutput[iOffset] = Byte.Parse(strHex, System.Globalization.NumberStyles.HexNumber);
            iOffset += 1;
            iCounter = iCounter + 2;
        }

        return btOutput;
    }

    // Added by Jeff, 25 Dec 2013
    internal static byte[] btTransform(byte[] bt_Input, ICryptoTransform cryptoTransform)
    {
        MemoryStream objMemStream = null;
        CryptoStream objCryptStream = null;
        byte[] btResult = null;

        try
        {
            // Create the necessary streams
            using (objMemStream = new MemoryStream())
            {
                using (objCryptStream = new CryptoStream(objMemStream, cryptoTransform, CryptoStreamMode.Write))
                {
                    // Transform the bytes as requested
                    objCryptStream.Write(bt_Input, 0, bt_Input.Length);
                    objCryptStream.FlushFinalBlock();

                    // Read the memory stream and convert it back into byte array
                    objMemStream.Position = 0;
                    btResult = new byte[objMemStream.Length];
                    objMemStream.Read(btResult, 0, btResult.Length);
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            objMemStream.Close();
            objCryptStream.Close();

            if (objMemStream != null) objMemStream = null;
            if (objCryptStream != null) objCryptStream = null;
        }

        // Return result
        return btResult;
    }
}
