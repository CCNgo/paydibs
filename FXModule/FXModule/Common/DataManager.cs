using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

class DataManager
{
    internal static string GetConnectionString(string conString)
    {
		try
		{
			if (!conString.ToLower().Contains("uid="))
				conString = Util.strDecrypt3DES(conString, "3537146182309002FEB7EBCE55ED3BBA71BED7CEB1D18CAA", "0000000000000000"); //3537146182309002FEB7EBCE55ED3BBA71BED7CEB1D18CAA
        }
		catch (Exception ex)
		{ 
			conString = null; 
		}
        return conString;
    }
    internal static bool VerifyDBConnection(string connectionString)
    {
        bool bReturn = false;
        SqlConnection sqlConnection = null;
        try
        {
            using (sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                bReturn = true;
                sqlConnection.Close();
            }
        }
        catch (Exception ex) 
		{ 
			throw ex; 
		}
        finally 
		{
			if (sqlConnection.State == ConnectionState.Open)
			{
				sqlConnection.Close();
			}
		}
        return bReturn;
    }
	internal static SqlDataReader TransactionMethod(string storedProcName, SqlConnection sqlConnection, ArrayList paramName, ArrayList paramType, ArrayList paramValue, ref string errMessage)
	{
		SqlCommand sqlCommand = null;
		SqlDataReader sqlDataReader = null;
		try
		{
			using (sqlCommand = new SqlCommand(storedProcName, sqlConnection))
			{
				sqlCommand.CommandType = CommandType.StoredProcedure;
				for (int i = 0; i < paramName.Count; i++)
				{
					SqlParameter sqlParameter = new SqlParameter();
					sqlParameter.DbType = (DbType)paramType[i];
					sqlParameter.Direction = ParameterDirection.Input;
					sqlParameter.ParameterName = paramName[i].ToString();
					sqlParameter.Value = paramValue[i].ToString();

					sqlCommand.Parameters.Add(sqlParameter);
				}
				sqlDataReader = sqlCommand.ExecuteReader();
			}
		}
		catch (SqlException ex)
		{
			errMessage = ex.Message;
			throw ex;
		}
		return sqlDataReader;
	}
}