using FXModule.Properties;

class Global
{
	public static string ConString = DataManager.GetConnectionString(Settings.Default.ConString);
	public static string UserID = Settings.Default.UserID;
	//public static string PathInput = Settings.Default.PathInput;
	//public static string PathExpired = Settings.Default.PathExpired;
	public static string PathLogger = Settings.Default.PathLogger;
	//public static string PathRetry = Settings.Default.PathRetry;
	//public static string PathHistory = Settings.Default.PathHistory;
	public static int RetryMaximum = Settings.Default.RetryMaximum;
	public static int ServiceInterval = Settings.Default.ServiceInterval;
	//public static string SshHostKeyFingerprint = Settings.Default.WinSCP_SshHostKeyFingerprint;
	//public static string SshPrivateKeyPath = Settings.Default.WinSCP_SshPrivateKeyPath;
	public static string URLRoot = Settings.Default.URLRoot;
	public static string BaseCurrency = Settings.Default.BaseCurrency;
	public static string ProcessLog = System.IO.Path.Combine(PathLogger, Settings.Default.ProcessLog);
	public static string ServiceLog = System.IO.Path.Combine(PathLogger, Settings.Default.ServiceLog);
}