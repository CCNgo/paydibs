﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;
using System.Timers;

namespace FXModule
{
	public partial class FXModule : ServiceBase
	{
		private Timer serviceTimer;

		public FXModule()
		{
			InitializeComponent();
		}

		protected override void OnStart(string[] args)
		{
			bool isDoProcess = false;
			bool isEnableSvcLog = Properties.Settings.Default.EnableSvcLog;
			try
			{
				if (isEnableSvcLog) Util.ServiceLogWriter("Service Started");
				if (Global.ConString != null)
					isDoProcess = DataManager.VerifyDBConnection(Global.ConString);
				else if (isEnableSvcLog)
				{
					Util.ServiceLogWriter("Invalid DB Connection String.");
				}
			}
			catch (Exception ex)
			{
				if (isEnableSvcLog)
				{
					Util.ServiceLogWriter(ex.Message.ToString());
					Util.ServiceLogWriter("DB Connection Failed.");
				}
			}
			try
			{
				if (isDoProcess)
				{
					if (isEnableSvcLog)
					{
						Util.ServiceLogWriter("Database Connection(s) Validated Successfully");
						Util.ServiceLogWriter("Service is Running...");
					}

					serviceTimer = new System.Timers.Timer();
					serviceTimer.Elapsed += new System.Timers.ElapsedEventHandler(DoWork);
					serviceTimer.Interval = Global.ServiceInterval * 60 * 1000; //minutes
					serviceTimer.Enabled = true;
					serviceTimer.AutoReset = true;
					serviceTimer.Start();
				}
			}
			catch (Exception ex)
			{ 
				Util.ServiceLogWriter(ex.Message.Trim()); 
			}
		}

		protected override void OnStop()
		{
			try
			{
				if (Properties.Settings.Default.EnableSvcLog)
				{ 
					Util.ServiceLogWriter("Service Stopped."); 
				}
				serviceTimer.Stop();
			}
			catch (Exception ex)
			{ 
				Util.ServiceLogWriter(ex.Message.Trim()); 
			}
		}
		private void DoWork(object sender, ElapsedEventArgs args)
		{
			ClassProcess cp = new ClassProcess();
			int iRetryIndex = 0;
			bool bRes = false;
			try
			{
				string strURLRoot = Properties.Settings.Default.URLRoot;
				string[] strArrBaseCurrency = Global.BaseCurrency.Split('|');
				for (int i = 0; i < strArrBaseCurrency.Length; i++)
				{
					string strBaseCurrency = strArrBaseCurrency[i].ToString();
					while (bRes == false && iRetryIndex <= Global.RetryMaximum)
					{
						if (iRetryIndex > 0 && iRetryIndex <= Global.RetryMaximum)
						{
							Util.ServiceLogWriter("Retry Count : " + iRetryIndex.ToString());
						}
						bRes = cp.ExtractCurrencyExchange(strURLRoot, strBaseCurrency);
						if (bRes)
						{
							Util.ServiceLogWriter(strBaseCurrency + " Update/Insert Success");
							break;
						}
						else
						{
							Util.ServiceLogWriter(strBaseCurrency + " Update/Insert Error");
						}
						iRetryIndex++;
						System.Threading.Thread.Sleep(2000);
					}
					if (iRetryIndex > Global.RetryMaximum)
					{
						Util.ServiceLogWriter("Max Retry - Current Process Stopped");
					}
					iRetryIndex = 0;
					bRes = false;
				}
			}
			catch (Exception ex)
			{
				Util.ServiceLogWriter(ex.Message);
			}
		}
	}
}
