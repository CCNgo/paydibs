﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using FXModule;

namespace TestFXModule
{
	public partial class frmMain : Form
	{
		public frmMain()
		{
			InitializeComponent();
		}

		private void btnExtract_Click(object sender, EventArgs e)
		{
			ClassProcess cp = new ClassProcess();

			textBox1.Text = "MYR to Any";
			textBox1.Text = textBox1.Text + Environment.NewLine;
            bool testMYR = cp.ExtractCurrencyExchange("https://themoneyconverter.com/", "MYR");
            DataSet dsMYR = cp.TestExtractCurrencyExchange("https://themoneyconverter.com/", "MYR");
			for (int i = 0; i < dsMYR.Tables[0].Rows.Count; i++)
			{
				for (int j = 0; j < dsMYR.Tables[0].Columns.Count; j++)
				{
					textBox1.Text = textBox1.Text + dsMYR.Tables[0].Rows[i][j].ToString() + " | ";
				}
				textBox1.Text = textBox1.Text + Environment.NewLine;
				//textBox1.Text = textBox1.Text + "Tables : " + cp.tablesss.ToString() + Environment.NewLine;
			}

			textBox1.Text = textBox1.Text + Environment.NewLine;
			textBox1.Text = textBox1.Text + "THB to Any";
			textBox1.Text = textBox1.Text + Environment.NewLine;
			DataSet dsTHB = cp.TestExtractCurrencyExchange("https://themoneyconverter.com/", "THB");
			for (int i = 0; i < dsTHB.Tables[0].Rows.Count; i++)
			{
				for (int j = 0; j < dsTHB.Tables[0].Columns.Count; j++)
				{
					textBox1.Text = textBox1.Text + dsTHB.Tables[0].Rows[i][j].ToString() + " | ";
				}
				textBox1.Text = textBox1.Text + Environment.NewLine;
			}

			textBox1.Text = textBox1.Text + Environment.NewLine;
			textBox1.Text = textBox1.Text + "PHP to Any";
			textBox1.Text = textBox1.Text + Environment.NewLine;
			DataSet dsPHP = cp.TestExtractCurrencyExchange("https://themoneyconverter.com/", "PHP");
			for (int i = 0; i < dsPHP.Tables[0].Rows.Count; i++)
			{
				for (int j = 0; j < dsPHP.Tables[0].Columns.Count; j++)
				{
					textBox1.Text = textBox1.Text + dsPHP.Tables[0].Rows[i][j].ToString() + " | ";
				}
				textBox1.Text = textBox1.Text + Environment.NewLine;
			}

            textBox1.Text = textBox1.Text + Environment.NewLine;
            textBox1.Text = textBox1.Text + "USD to Any";
            textBox1.Text = textBox1.Text + Environment.NewLine;
            DataSet dsUSD = cp.TestExtractCurrencyExchange("https://themoneyconverter.com/", "USD");
            for (int i = 0; i < dsUSD.Tables[0].Rows.Count; i++)
            {
                for (int j = 0; j < dsUSD.Tables[0].Columns.Count; j++)
                {
                    textBox1.Text = textBox1.Text + dsUSD.Tables[0].Rows[i][j].ToString() + " | ";
                }
                textBox1.Text = textBox1.Text + Environment.NewLine;
            }

            textBox1.Text = textBox1.Text + Environment.NewLine;
            textBox1.Text = textBox1.Text + "CNY to Any";
            textBox1.Text = textBox1.Text + Environment.NewLine;
            DataSet dsCNY = cp.TestExtractCurrencyExchange("https://themoneyconverter.com/", "CNY");
            for (int i = 0; i < dsCNY.Tables[0].Rows.Count; i++)
            {
                for (int j = 0; j < dsCNY.Tables[0].Columns.Count; j++)
                {
                    textBox1.Text = textBox1.Text + dsCNY.Tables[0].Rows[i][j].ToString() + " | ";
                }
                textBox1.Text = textBox1.Text + Environment.NewLine;
            }

            textBox1.Text = textBox1.Text + Environment.NewLine;
            textBox1.Text = textBox1.Text + "VND to Any";
            textBox1.Text = textBox1.Text + Environment.NewLine;
            DataSet dsVND = cp.TestExtractCurrencyExchange("https://themoneyconverter.com/", "VND");
            for (int i = 0; i < dsVND.Tables[0].Rows.Count; i++)
            {
                for (int j = 0; j < dsVND.Tables[0].Columns.Count; j++)
                {
                    textBox1.Text = textBox1.Text + dsVND.Tables[0].Rows[i][j].ToString() + " | ";
                }
                textBox1.Text = textBox1.Text + Environment.NewLine;
            }

            textBox1.Text = textBox1.Text + Environment.NewLine;
            textBox1.Text = textBox1.Text + "AUD to Any";
            textBox1.Text = textBox1.Text + Environment.NewLine;
            DataSet dsAUD = cp.TestExtractCurrencyExchange("https://themoneyconverter.com/", "AUD");
            for (int i = 0; i < dsAUD.Tables[0].Rows.Count; i++)
            {
                for (int j = 0; j < dsAUD.Tables[0].Columns.Count; j++)
                {
                    textBox1.Text = textBox1.Text + dsAUD.Tables[0].Rows[i][j].ToString() + " | ";
                }
                textBox1.Text = textBox1.Text + Environment.NewLine;
            }

            textBox1.Text = textBox1.Text + Environment.NewLine;
            textBox1.Text = textBox1.Text + "NZD to Any";
            textBox1.Text = textBox1.Text + Environment.NewLine;
            DataSet dsNZD = cp.TestExtractCurrencyExchange("https://themoneyconverter.com/", "NZD");
            for (int i = 0; i < dsNZD.Tables[0].Rows.Count; i++)
            {
                for (int j = 0; j < dsNZD.Tables[0].Columns.Count; j++)
                {
                    textBox1.Text = textBox1.Text + dsNZD.Tables[0].Rows[i][j].ToString() + " | ";
                }
                textBox1.Text = textBox1.Text + Environment.NewLine;
            }

            textBox1.Text = textBox1.Text + Environment.NewLine;
            textBox1.Text = textBox1.Text + "IDR to Any";
            textBox1.Text = textBox1.Text + Environment.NewLine;
            DataSet dsIDR = cp.TestExtractCurrencyExchange("https://themoneyconverter.com/", "IDR");
            for (int i = 0; i < dsIDR.Tables[0].Rows.Count; i++)
            {
                for (int j = 0; j < dsIDR.Tables[0].Columns.Count; j++)
                {
                    textBox1.Text = textBox1.Text + dsIDR.Tables[0].Rows[i][j].ToString() + " | ";
                }
                textBox1.Text = textBox1.Text + Environment.NewLine;
            }

            textBox1.Text = textBox1.Text + Environment.NewLine;
            textBox1.Text = textBox1.Text + "TWD to Any";
            textBox1.Text = textBox1.Text + Environment.NewLine;
            DataSet dsTWD = cp.TestExtractCurrencyExchange("https://themoneyconverter.com/", "TWD");
            for (int i = 0; i < dsTWD.Tables[0].Rows.Count; i++)
            {
                for (int j = 0; j < dsTWD.Tables[0].Columns.Count; j++)
                {
                    textBox1.Text = textBox1.Text + dsTWD.Tables[0].Rows[i][j].ToString() + " | ";
                }
                textBox1.Text = textBox1.Text + Environment.NewLine;
            }

            textBox1.Text = textBox1.Text + Environment.NewLine;
            textBox1.Text = textBox1.Text + "SGD to Any";
            textBox1.Text = textBox1.Text + Environment.NewLine;
            DataSet dsSGD = cp.TestExtractCurrencyExchange("https://themoneyconverter.com/", "SGD");
            for (int i = 0; i < dsSGD.Tables[0].Rows.Count; i++)
            {
                for (int j = 0; j < dsSGD.Tables[0].Columns.Count; j++)
                {
                    textBox1.Text = textBox1.Text + dsSGD.Tables[0].Rows[i][j].ToString() + " | ";
                }
                textBox1.Text = textBox1.Text + Environment.NewLine;
            }
            
        }

		private void btnTestConn_Click(object sender, EventArgs e)
		{
			ClassProcess cp = new ClassProcess();
			MessageBox.Show( cp.TestDBConnection());
		}

		private void btnExtractAndInsert_Click(object sender, EventArgs e)
		{
			ClassProcess cp = new ClassProcess();
			try
			{
				cp.ExtractCurrencyExchange("https://themoneyconverter.com/", "MYR");
				MessageBox.Show("done1");
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
			try
			{
				cp.ExtractCurrencyExchange("https://themoneyconverter.com/", "THB");
				MessageBox.Show("done2");
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
			try
			{
				cp.ExtractCurrencyExchange("https://themoneyconverter.com/", "PHP");
				MessageBox.Show("done3");
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
            try
            {
                cp.ExtractCurrencyExchange("https://themoneyconverter.com/", "USD");
                MessageBox.Show("done3");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            try
            {
                cp.ExtractCurrencyExchange("https://themoneyconverter.com/", "CNY");
                MessageBox.Show("done3");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            try
            {
                cp.ExtractCurrencyExchange("https://themoneyconverter.com/", "VND");
                MessageBox.Show("done3");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            try
            {
                cp.ExtractCurrencyExchange("https://themoneyconverter.com/", "AUD");
                MessageBox.Show("done3");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            try
            {
                cp.ExtractCurrencyExchange("https://themoneyconverter.com/", "NZD");
                MessageBox.Show("done3");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            try
            {
                cp.ExtractCurrencyExchange("https://themoneyconverter.com/", "IDR");
                MessageBox.Show("done3");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            try
            {
                cp.ExtractCurrencyExchange("https://themoneyconverter.com/", "TWD");
                MessageBox.Show("done3");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            try
            {
                cp.ExtractCurrencyExchange("https://themoneyconverter.com/", "SGD");
                MessageBox.Show("done3");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
	}
}
