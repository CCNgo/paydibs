﻿namespace TestFXModule
{
	partial class frmMain
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnExtract = new System.Windows.Forms.Button();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.btnTestConn = new System.Windows.Forms.Button();
			this.btnExtractAndInsert = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// btnExtract
			// 
			this.btnExtract.Location = new System.Drawing.Point(12, 34);
			this.btnExtract.Name = "btnExtract";
			this.btnExtract.Size = new System.Drawing.Size(104, 26);
			this.btnExtract.TabIndex = 1;
			this.btnExtract.Text = "Extract";
			this.btnExtract.UseVisualStyleBackColor = true;
			this.btnExtract.Click += new System.EventHandler(this.btnExtract_Click);
			// 
			// textBox1
			// 
			this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.textBox1.Location = new System.Drawing.Point(12, 66);
			this.textBox1.Multiline = true;
			this.textBox1.Name = "textBox1";
			this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.textBox1.Size = new System.Drawing.Size(578, 310);
			this.textBox1.TabIndex = 2;
			// 
			// btnTestConn
			// 
			this.btnTestConn.Location = new System.Drawing.Point(122, 34);
			this.btnTestConn.Name = "btnTestConn";
			this.btnTestConn.Size = new System.Drawing.Size(170, 26);
			this.btnTestConn.TabIndex = 3;
			this.btnTestConn.Text = "Test Connection";
			this.btnTestConn.UseVisualStyleBackColor = true;
			this.btnTestConn.Click += new System.EventHandler(this.btnTestConn_Click);
			// 
			// btnExtractAndInsert
			// 
			this.btnExtractAndInsert.Location = new System.Drawing.Point(298, 34);
			this.btnExtractAndInsert.Name = "btnExtractAndInsert";
			this.btnExtractAndInsert.Size = new System.Drawing.Size(138, 26);
			this.btnExtractAndInsert.TabIndex = 4;
			this.btnExtractAndInsert.Text = "Extract and Insert";
			this.btnExtractAndInsert.UseVisualStyleBackColor = true;
			this.btnExtractAndInsert.Click += new System.EventHandler(this.btnExtractAndInsert_Click);
			// 
			// frmMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(913, 388);
			this.Controls.Add(this.btnExtractAndInsert);
			this.Controls.Add(this.btnTestConn);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.btnExtract);
			this.Name = "frmMain";
			this.Text = "Extract Currency Exchange";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button btnExtract;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Button btnTestConn;
		private System.Windows.Forms.Button btnExtractAndInsert;
	}
}

