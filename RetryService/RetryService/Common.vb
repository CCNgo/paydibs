Public Class Common
    Public Structure STQueryInfo
        'Request fields to PPG
        Public szQueryURL As String
        Public szTxnType As String
        Public szMerchantID As String
        Public szMerchantTxnID As String
        Public szTxnAmount As String
        Public szCurrencyCode As String
        Public szHashMethod As String
        Public szHashValue As String
        Public szMerchantPassword As String

        'Response fields from PPG
        Public szGatewayTxnID As String
        Public iTxnState As Integer
        Public iMerchantTxnStatus As Integer
        Public szHostTxnID As String
    End Structure

    Public Sub InitQueryInfo(ByRef st_QueryInfo As STQueryInfo)
        'Request to PPG
        st_QueryInfo.szQueryURL = ""
        st_QueryInfo.szTxnType = ""
        st_QueryInfo.szMerchantID = ""
        st_QueryInfo.szMerchantTxnID = ""
        st_QueryInfo.szTxnAmount = ""
        st_QueryInfo.szCurrencyCode = ""
        st_QueryInfo.szHashMethod = ""
        st_QueryInfo.szHashValue = ""
        st_QueryInfo.szMerchantPassword = ""

        'Response from PPG
        st_QueryInfo.szGatewayTxnID = ""
        st_QueryInfo.iTxnState = -1
        st_QueryInfo.iMerchantTxnStatus = -1
        st_QueryInfo.szHostTxnID = ""
    End Sub
End Class
