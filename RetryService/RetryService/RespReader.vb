Imports System.IO

Public Class RespReader
    'Implements IDisposable

    Private objStreamReader As StreamReader
    Private bCompleted As Boolean
    Private bSucceeded As Boolean
    Private szOutStream As String

    Public Sub New()
        objStreamReader = Nothing
        bCompleted = False
        bSucceeded = False
        szOutStream = ""
    End Sub

    'Public Sub Dispose() Implements System.IDisposable.Dispose
    ' Calling GC.SuppressFinalize in Dispose is an important optimization to ensure
    ' resources released promptly.
    '    GC.SuppressFinalize(Me)
    'End Sub

    Public Sub SetReader(ByRef o_Reader As StreamReader)
        objStreamReader = o_Reader
    End Sub

    Public Function IsCompleted()
        IsCompleted = bCompleted
    End Function

    Public Function IsSucceeded()
        IsSucceeded = bSucceeded
    End Function

    Public Function GetRespStream()
        GetRespStream = szOutStream
    End Function

    Public Sub WaitForResp()
        bCompleted = False
        bSucceeded = False
        szOutStream = ""

        Try
            ' This function will wait forever until terminated abruptly or exception
            szOutStream = objStreamReader.ReadToEnd
            bSucceeded = True
        Catch ex As Exception
            bSucceeded = False
        Finally
            bCompleted = True
        End Try
    End Sub
End Class
