Imports System.Reflection       'Assembly
Imports System.Data.SqlClient
Imports System.Threading
Imports System.Security.Cryptography
Imports System.IO
Imports System.Net      ' NetworkCredential
Imports System.Net.Mail ' MailMessage

Public Class TxnProc

    Public m_bStart As Boolean

    Public m_szDBConnStr As String
    Public m_szLogPath As String
    Public m_szQueryURL As String
    Public m_szPymtMethod As String             'Added  3 Oct 2013
    Public m_szHostName As String

    Public m_iAction As Integer
    Public m_iStartTickCount As Integer
    Public m_iCheckIntervalSeconds As Integer
    Public m_iHostID As Integer
    Public m_iSvcEnable As Integer

    Private m_stQueryInfo As Common.STQueryInfo
    Private m_objHTTP As CHTTP
    Private m_objHash As Hash
    Private m_objCommon As Common

    Private m_bCompleted As Boolean
    Private m_bSucceeded As Boolean

    Public Sub New()
        m_objHTTP = New CHTTP
        m_objHash = New Hash
        m_objCommon = New Common

        m_bCompleted = False
        m_bSucceeded = False

        m_iAction = 0
        m_iStartTickCount = 0
        m_iCheckIntervalSeconds = 0
        m_iHostID = 0
        m_szDBConnStr = ""
        m_szLogPath = ""
        m_szQueryURL = ""
        m_szHostName = ""
        m_bStart = False
        m_iSvcEnable = -1

        ' Initializes query structure
        m_objCommon.InitQueryInfo(m_stQueryInfo)
    End Sub

    Public Function IsCompleted()
        IsCompleted = m_bCompleted
    End Function

    Public Function IsSucceeded()
        IsSucceeded = m_bSucceeded
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bRetryTxns
    ' Function Type:	Boolean.  True if retry successfully else false
    ' Parameter:		m_stQueryInfo, m_szDBConnStr
    ' Description:		Retry query Direct Debit Gateway to trigger Query Host or Confirm Booking
    ' History:			2 Jan 2009
    '********************************************************************************************
    Public Sub bRetryTxns()
        m_bCompleted = False
        m_bSucceeded = False

        Dim objConn As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objParam As SqlParameter = Nothing
        Dim dr As SqlDataReader
        Dim iRet As Integer
        Dim szQueryHTTPString As String = ""
        Dim szHTTPResponse As String = ""
        Dim szWatchDog_W3Svc As String = "" ' Added on 14 Apr 2009

        Log(m_szLogPath, "bRetryTxns() Started")

        Try
            ' Added on 14 Apr 2009
            szWatchDog_W3Svc = [Assembly].GetExecutingAssembly().Location.Replace([Assembly].GetExecutingAssembly().GetModules()(0).Name, "WatchDog_W3SVC.bat") '"(SC query W3SVC | FIND ""RUNNING"") || SC START W3SVC"

            objConn = New SqlConnection(m_szDBConnStr)
            objCommand = New SqlCommand("spGetRetryTxns", objConn)
            objCommand.CommandType = CommandType.StoredProcedure

            objParam = objCommand.Parameters.Add("@i_HostID", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_Action", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_MaxRow", SqlDbType.Int)
            objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
            objParam.Direction = ParameterDirection.ReturnValue

            If (3 = m_iAction) Then
                objCommand.Parameters("@i_HostID").Value = m_iHostID
                objCommand.Parameters("@i_Action").Value = -1   'Retry Query ONLY
                objCommand.Parameters("@i_MaxRow").Value = 20

            ElseIf (4 = m_iAction) Then
                objCommand.Parameters("@i_HostID").Value = -1   'Regardless of what banks
                objCommand.Parameters("@i_Action").Value = 4    'Retry booking confirmation ONLY
                objCommand.Parameters("@i_MaxRow").Value = 20

                'Added on 13 June 2011, to cater for reversal
            ElseIf (7 = m_iAction) Then
                objCommand.Parameters("@i_HostID").Value = m_iHostID
                objCommand.Parameters("@i_Action").Value = m_iAction   'Retry Reversal ONLY ***********************
                objCommand.Parameters("@i_MaxRow").Value = 20
            Else
                objCommand.Parameters("@i_HostID").Value = -1   'Regardless of what banks
                objCommand.Parameters("@i_Action").Value = -1   'Retry Query ONLY
                objCommand.Parameters("@i_MaxRow").Value = 20
            End If

        Catch ex As Exception
            Log(m_szLogPath, "bRetryTxns() Exception: " + ex.Message)
            m_bSucceeded = False
        End Try

        While (m_bStart)
            Try
                Log(m_szLogPath, "Connecting DB...")

                objConn.Open()
                objCommand.Connection = objConn

                Log(m_szLogPath, "DB connected")

                While (m_bStart)
                    dr = objCommand.ExecuteReader()

                    While dr.Read()
                        m_stQueryInfo.szMerchantID = dr("MerchantID").ToString()
                        m_stQueryInfo.szMerchantTxnID = dr("MerchantTxnID").ToString()
                        m_stQueryInfo.szTxnAmount = dr("TxnAmount").ToString() 'Added on 13 June 2011, to cater for reversal
                        m_stQueryInfo.szCurrencyCode = dr("CurrencyCode").ToString() 'Added on 13 June 2011, to cater for reversal
                        m_stQueryInfo.szHashMethod = "SHA512" '"SHA2"     'Modified  3 Oct 2013, SHA1 to SHA2

                        'Get Merchant Password
                        If (bGetMerchant()) Then

                            'Added log on 22 June 2009
                            'Log(m_szLogPath, m_stQueryInfo.szMerchantPassword + m_stQueryInfo.szServiceID + m_stQueryInfo.szMerchantTxnID + " " + m_stQueryInfo.szHashMethod + " " + m_stQueryInfo.szHashValue)

                            If (7 = m_iAction) Then 'Added on 13 June 2011, to cater for reversal
                                m_stQueryInfo.szHashValue = m_objHash.szGetReversalReqHash(m_stQueryInfo)
                                szQueryHTTPString = szGetReversalHTTPString()
                            Else
                                m_stQueryInfo.szHashValue = m_objHash.szGetQueryReqHash(m_stQueryInfo) 'Moved here from above on 13 June 2011, to cater for reversal
                                szQueryHTTPString = szGetQueryHTTPString()
                            End If

                            Log(m_szLogPath, "Request: " + szQueryHTTPString)

                            iRet = m_objHTTP.iHTTPostWithRecv(m_szQueryURL, szQueryHTTPString, 600, szHTTPResponse)
                            If (iRet <> 0) Then
                                Log(m_szLogPath, "iRet(" + iRet.ToString() + ") Failed to HTTP post to " + m_szQueryURL)

                                ' Added on 14 Apr 2009, to restart W3SVC and send email alert whenever encounter connection problem to Direct Debit Gateway
                                If (44 = iRet) Then
                                    If (3 = m_iAction And 3 = m_iHostID) Then
                                        Call Shell(szWatchDog_W3Svc, 0)
                                        SendMail()
                                        Log(m_szLogPath, "Restarted World Wide Web Service and sent email alert by calling " + szWatchDog_W3Svc)
                                        System.Threading.Thread.Sleep(3 * 1000)
                                    End If
                                End If
                            Else
                                Log(m_szLogPath, "Received HTTP response: " + szHTTPResponse)
                            End If
                        End If

                        'Added  3 Oct 2013
                        System.Threading.Thread.Sleep(2000)
                    End While

                    dr.Close()

                    System.Threading.Thread.Sleep(m_iCheckIntervalSeconds * 1000)
                End While

                m_bSucceeded = True

            Catch ex As Exception
                objConn.Close()

                Log(m_szLogPath, "bRetryTxns() Exception: " + ex.Message)
                Log(m_szLogPath, "DB connection closed")

                m_bSucceeded = False
            End Try

            If (m_bStart) Then
                System.Threading.Thread.Sleep(40 * 1000)
            End If
        End While

        If Not objParam Is Nothing Then objParam = Nothing

        If Not objCommand Is Nothing Then
            objCommand.Dispose()
            objCommand = Nothing
        End If

        If Not objConn Is Nothing Then
            objConn.Close()
            Log(m_szLogPath, "DB connection closed")
            objConn.Dispose()
            objConn = Nothing
        End If

        Log(m_szLogPath, "bRetryTxns() Completed")
        m_bCompleted = True
    End Sub

    Private Function SendMail()
        Dim mail As MailMessage
        Dim smtpMail As SmtpClient

        Try
            mail = New MailMessage
            mail.From = New MailAddress("RetrySvc@Paydibs.com")
            mail.To.Add("test@paydibs.com")
            mail.To.Add("om.yong@paydibs.com")
            mail.Subject = "PG Retry Service Alert"
            mail.Body = "Restarted World Wide Web Service"

            smtpMail = New SmtpClient
            smtpMail.Host = ""
            smtpMail.Credentials = New NetworkCredential("", "")
            smtpMail.Port = 25
            smtpMail.DeliveryMethod = SmtpDeliveryMethod.Network
            smtpMail.Send(mail)

            Return True

        Catch ex As Exception

            Log(m_szLogPath, "Email exception " + ex.Message)
            Return False
        End Try

        smtpMail = Nothing
        mail = Nothing

    End Function

    Private Sub Log(ByVal sz_LogFile As String, ByVal sz_Text As String)
        Dim objStreamWriter As StreamWriter
        Dim szLogFile(2) As String
        Dim szLogPath As String = ""
        Dim szTxnType As String = ""    'Added  3 Oct 2013

        'Added  3 Oct 2013
        If (7 = m_iAction) Then
            szTxnType = "Void"
        Else
            szTxnType = "Query"
        End If

        If InStr(sz_LogFile, ".") > 0 Then
            szLogFile = Split(sz_LogFile, ".")
            If (m_szHostName <> "") Then
                szLogPath = szLogFile(0) + "_" + m_szHostName + "_" + m_szPymtMethod + "_" + szTxnType + "_" + DateTime.Now.ToString("yyyyMMdd") + "." + szLogFile(1)
            Else
                szLogPath = szLogFile(0) + "_" + "Booking" + "_" + DateTime.Now.ToString("yyyyMMdd") + "." + szLogFile(1)
            End If
        Else
            If (m_szHostName <> "") Then
                szLogPath = sz_LogFile + "_" + m_szHostName + "_" + m_szPymtMethod + "_" + szTxnType + "_" + DateTime.Now.ToString("yyyyMMdd")
            Else
                szLogPath = sz_LogFile + "_" + "Booking" + "_" + DateTime.Now.ToString("yyyyMMdd")
            End If
        End If

        objStreamWriter = New StreamWriter(szLogPath, True)
        objStreamWriter.WriteLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + " >>> " + sz_Text)
        objStreamWriter.Close()
    End Sub

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	bGetMerchant
    ' Function Type:	Boolean.  True if get merchant information successfully else false
    ' Parameter:		m_stQueryInfo, m_szDBConnStr
    ' Description:		Get merchant information
    ' History:			2 Jan 2009
    '********************************************************************************************
    Public Function bGetMerchant() As Boolean

        Dim bReturn As Boolean = True
        Dim objConn As SqlConnection
        Dim objCommand As SqlCommand
        Dim objParam As SqlParameter
        Dim dr As SqlDataReader
        Dim iRowsAffected As Integer = 0

        objConn = New SqlConnection(m_szDBConnStr)
        objCommand = New SqlCommand("spGetMerchant", objConn)
        objCommand.CommandType = CommandType.StoredProcedure

        objParam = objCommand.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30)
        objParam = objCommand.Parameters.Add("@i_Return", SqlDbType.Int)
        objParam.Direction = ParameterDirection.ReturnValue

        Try
            objCommand.Parameters("@sz_MerchantID").Value = m_stQueryInfo.szMerchantID

            objConn.Open()
            objCommand.Connection = objConn

            dr = objCommand.ExecuteReader()

            If objCommand.Parameters("@i_Return").Value = 0 Then
                bReturn = True

                Dim szMerchantPassword As String = ""

                While dr.Read()
                    szMerchantPassword = dr("MerchantPassword").ToString()
                    dr.Read()
                End While

                If (szMerchantPassword <> "") Then
                    'Modified the following on 22 June 2009
                    'If m_stQueryInfo.szMerchantPassword.Length <= 0 Then
                    '    m_stQueryInfo.szMerchantPassword = szMerchantPassword
                    'End If
                    m_stQueryInfo.szMerchantPassword = szMerchantPassword
                Else
                    bReturn = False
                End If
            Else
                bReturn = False
            End If

            dr.Close()
            objConn.Close()

        Catch ex As Exception
            bReturn = False
        Finally
            If Not objParam Is Nothing Then objParam = Nothing

            If Not objCommand Is Nothing Then
                objCommand.Dispose()
                objCommand = Nothing
            End If

            If Not objConn Is Nothing Then
                objConn.Dispose()
                objConn = Nothing
            End If
        End Try

        Return bReturn
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	szGetQueryHTTPString
    ' Function Type:	String
    ' Parameter:		None
    ' Description:		Form HTTP string to query Direct Debit Gateway
    ' History:			1 Jan 2009
    '********************************************************************************************
    Public Function szGetQueryHTTPString() As String
        Dim szHTTPString As String

        szHTTPString = "TxnType=QUERY" + "&"
        szHTTPString = szHTTPString + "Method=" + m_szPymtMethod + "&"                  'Added  3 Oct 2013
        szHTTPString = szHTTPString + "MerchantID=" + m_stQueryInfo.szMerchantID + "&"
        szHTTPString = szHTTPString + "MerchantPymtID=" + m_stQueryInfo.szMerchantTxnID + "&"
        szHTTPString = szHTTPString + "MerchantTxnAmt=" + m_stQueryInfo.szTxnAmount + "&"        'Added 3 Oct 2013
        szHTTPString = szHTTPString + "MerchantCurrCode=" + m_stQueryInfo.szCurrencyCode + "&"  'Added 3 Oct 2013
        szHTTPString = szHTTPString + "RS=1" + "&"                                          'Added 3 Oct 2013, RS = Retry Service, to indicate the request is initiated from Retry Service instead of merchant
        'szHTTPString = szHTTPString + "HashMethod=" + m_stQueryInfo.szHashMethod + "&"      'Not needed for gateway but remained so that this service can still be used by other existing gateways
        szHTTPString = szHTTPString + "Sign=" + m_stQueryInfo.szHashValue

        Return szHTTPString
    End Function

    '********************************************************************************************
    ' Class Name:		TxnProc
    ' Function Name:	szGetReversalHTTPString
    ' Function Type:	String
    ' Parameter:		None
    ' Description:		Form HTTP string to send reversal to Direct Debit Gateway
    ' History:			13 June 2011
    '********************************************************************************************
    'Added on 13 June 2011, to cater for reversal
    Public Function szGetReversalHTTPString() As String
        Dim szHTTPString As String

        szHTTPString = "TxnType=VOID" + "&"
        szHTTPString = szHTTPString + "Method=" + m_szPymtMethod + "&"                  'Added  3 Oct 2013
        szHTTPString = szHTTPString + "MerchantID=" + m_stQueryInfo.szMerchantID + "&"
        szHTTPString = szHTTPString + "MerchantPymtID=" + m_stQueryInfo.szMerchantTxnID + "&"
        szHTTPString = szHTTPString + "MerchantTxnAmt=" + m_stQueryInfo.szTxnAmount + "&"
        szHTTPString = szHTTPString + "MerchantCurrCode=" + m_stQueryInfo.szCurrencyCode + "&"
        szHTTPString = szHTTPString + "RS=1" + "&"                                          'Added  3 Oct 2013, RS = Retry Service, to indicate the request is initiated from Retry Service instead of merchant
        'szHTTPString = szHTTPString + "HashMethod=" + m_stQueryInfo.szHashMethod + "&"      'Not needed for gateway but remained so that this service can still be used by other existing gateways
        szHTTPString = szHTTPString + "Sign=" + m_stQueryInfo.szHashValue
        'szHTTPString = szHTTPString + "GatewayID=NS&"                                      'Commented  3 Oct 2013

        Return szHTTPString
    End Function

End Class
