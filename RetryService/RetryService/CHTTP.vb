Imports System.IO   ' StreamWriter
Imports System
Imports System.Threading
Imports System.Net  ' WebRequest

Public Enum ERROR_CODE
    E_SUCCESS = 0 '

    E_TXN_SUCCESSFUL = 1 'From NA--
    E_TXN_INITIATED = 2 'From NA--
    E_TXN_AUTHORIZED = 3 'From NA--
    E_TXN_FAILED = 4 'From NA--
    E_COM_TIMEOUT = 5 'From NA--
    E_TXN_REVERSED = 7 'From NA--
    E_TXN_VOIDED = 8 'From NA
    E_TXN_HOTCARD = 9

    E_TXN_PENDING_SALE_RESP = 30 'From NA
    E_TXN_PENDING_REVERSAL_RESP = 31 'From NA
    E_TXN_PENDING_VOID_RESP = 32 'From NA
    E_TXN_PENDING_AUTH_RESP = 33 'From NA

    E_INVALID_PAN = 101 'From NA
    E_INVALID_PAN_LENGTH = 102 'From NA
    E_INVALID_PAN_UNKNOWN = 103 'From NA
    E_INVALID_TRACK2_FORMAT = 104 'From NA

    E_DB_UNKNOWN_ERROR = 901 ' From NA--
    E_INVALID_HOST = 902 ' From NA--
    E_FAILED_GET_REV_RESP = 903 'From NA--
    E_FAILED_REVERSAL = 904 ' From NA--
    E_FAILED_SET_TXN_STATUS = 905 'From NA--
    E_FAILED_STORE_TXN = 906            ' Failed bInsertNewTxn
    E_FAILED_UPDATE_TXN_RESP = 907 ' From NA--
    E_FAILED_FORM_REQ = 914 ' From NA--
    E_FAILED_COM_INIT = 917 ' From NA--

    E_COMMON_INVALID_FIELD = 2902 '
    E_COMMON_MISSING_ELEMENT = 2903

    E_INVALID_INPUT = 2906              ' Failed bCheckUniqueTxn
    E_INVALID_HOST_REPLY = 2907
    E_INVALID_MID = 2901 '
    E_INVALID_SIGNATURE = 2909 ''

    E_COMMON_UNKNOWN_ERROR = 2999       ' Failed GenTxnID, bInsertTxnResp, bCheckUniqueTxn, bInsertNewTxn

    E_CERT_ERROR = 51 '

    E_COM_BAD_REQUEST = 41 '
    E_COM_UNAUTHORIZED_URL = 42
    E_COM_FORBIDDEN_URL = 43
    E_COM_URL_UNREACHABLE = 44 '
    E_COM_FILE_ERROR = 45

    E_CRYPTO_INIT_KEY = 51
    E_CRYPTO_FAIL = 52 ''
    E_CRYPTO_UNKNOWN_ERROR = 53

    E_TXN_FAIL_GEN_ID = 61
    E_TXN_INVALID_STATE = 62 '
    E_TXN_NOT_FOUND = 63 '
    E_TXN_INVALID_AMOUNT = 64 '

    E_CAPTURE_FAILED = 500

End Enum

Public Class CHTTP

    '********************************************************************************************
    ' Class Name:		CHTTP
    ' Function Name:	iHTTPost
    ' Function Type:	boolean
    ' Parameter:        sz_URL [in, string]         - Posting URL
    '                   sz_HTTPString [in, string]  - HTTP string to be posted
    '                   i_Timeout [in, integer]     - Connection timeout(in second)
    ' Description:		Perform HTTP posting
    ' History:			19 Nov 2008
    '********************************************************************************************
    Public Function iHTTPostOnly(ByVal sz_URL As String, ByVal sz_HTTPString As String, ByVal i_Timeout As Integer) As Integer
        Dim iRet As Integer = -1
        Dim objHttpWebRequest As HttpWebRequest
        Dim objStreamWriter As StreamWriter
        Dim sbInStream As System.Text.StringBuilder
        Dim iTimeout As Integer
        Dim iResCode As Integer
        Dim iLoop As Integer
        Dim szHTTPStrArray() As String
        Dim szFieldArray() As String

        Try
            '======================================== Sets HttpWebRequest properties ====================================
            sbInStream = New System.Text.StringBuilder
            sbInStream.Append(sz_HTTPString)
            iTimeout = i_Timeout * 1000 'converts input timeout param from second to milliseconds

            objHttpWebRequest = WebRequest.Create(sz_URL)                       'Posting URL
            objHttpWebRequest.ContentType = "application/x-www-form-urlencoded" 'default value
            objHttpWebRequest.Method = "POST"                                   'default value
            objHttpWebRequest.ContentLength = sbInStream.Length ' Must be set b4 retrieving stream for sending data
            objHttpWebRequest.KeepAlive = False
            objHttpWebRequest.Timeout = iTimeout                                '(in milliseconds)

            'Ignore invalid server certificates or else may encounter WebException with e.Status = WebExceptionStatus.TrustFailure
            'When the CertificatePolicy property is set to an ICertificatePolicy interface instance, the ServicePointManager
            'uses the certificate policy defined in that instance instead of the default certificate policy.
            'The default certificate policy allows valid certificates, as well as valid certificates that have expired.
            ServicePointManager.CertificatePolicy = New SecPolicy

            szHTTPStrArray = Split(sz_HTTPString, "&")
            For iLoop = 0 To szHTTPStrArray.GetUpperBound(0)
                szFieldArray = Split(szHTTPStrArray(iLoop), "=")
            Next iLoop

            '=========================================== Post request ==================================================
            '- StreamWriter constructor creates a StreamWriter with UTF-8 encoding whose GetPreamble method returns an
            'empty byte array. The BaseStream property is initialized using the stream parameter (i.e. objHttpWebRequest.GetRequestStream)
            '- GetRequestStream method returns a stream to use to send data for the HttpWebRequest. Once the Stream
            'instance has been returned, you can send data with the HttpWebRequest by using the Stream.Write method.
            'NOTE:  Must set the value of the ContentLength property before retrieving the stream.
            '       When POST method is used, must get the request stream, write the data to be posted,
            '       and close the stream. This method blocks waiting for content to post; If there is no time-out set and
            '       content is not provided, application will block indefinitely.
            'CAUTION: Must call the Stream.Close method to close the stream and release the connection for reuse.
            '         Failure to close the stream will cause application to run out of connections.

            objStreamWriter = New StreamWriter(objHttpWebRequest.GetRequestStream())
            objStreamWriter.Write(sbInStream.ToString)  ' Sends data
            objStreamWriter.Close() ' Closes the stream and release the connection for reuse to avoid running out of connections

            iRet = 0

        Catch webEx As WebException
            Try
                iRet = ERROR_CODE.E_COM_URL_UNREACHABLE

                If webEx.Status = WebExceptionStatus.Timeout Then
                    iRet = ERROR_CODE.E_COM_TIMEOUT
                End If

                If webEx.Status = WebExceptionStatus.ProtocolError Then
                    'Re: HttpStatusCode Enumeration
                    iResCode = CInt(CType(webEx.Response, HttpWebResponse).StatusCode)
                    If iResCode = 400 Then
                        iRet = ERROR_CODE.E_COM_BAD_REQUEST
                    ElseIf iResCode = 401 Then
                        iRet = ERROR_CODE.E_COM_UNAUTHORIZED_URL
                    ElseIf iResCode = 403 Then
                        iRet = ERROR_CODE.E_COM_FORBIDDEN_URL
                    ElseIf iResCode = 404 Then
                        iRet = ERROR_CODE.E_COM_URL_UNREACHABLE
                    End If
                End If
            Catch exp As Exception
            End Try

        Catch ioEx As IOException
            iRet = ERROR_CODE.E_COM_FILE_ERROR

        Catch ex As Exception
            iRet = ERROR_CODE.E_COMMON_UNKNOWN_ERROR
        Finally
        End Try

        Return iRet
    End Function

    '********************************************************************************************
    ' Class Name:		CHTTP
    ' Function Name:	iHTTPostWithRecv
    ' Function Type:	boolean
    ' Parameter:        sz_URL [in, string]             - Posting URL
    '                   sz_HTTPString [in, string]      - HTTP string to be posted
    '                   i_Timeout [in, integer]         - Connection timeout(in second)
    '                   sz_HTTPResponse [out, string]   - HTTP string received
    ' Description:		Perform HTTP posting
    ' History:			19 Nov 2008
    '********************************************************************************************
    Public Function iHTTPostWithRecv(ByVal sz_URL As String, ByVal sz_HTTPString As String, ByVal i_Timeout As Integer, ByRef sz_HTTPResponse As String) As Integer
        Dim iRet As Integer = -1
        Dim objHttpWebRequest As HttpWebRequest
        Dim objHTTPWebResponse As HttpWebResponse
        Dim objStreamWriter As StreamWriter
        Dim objStreamReader As StreamReader
        Dim sbInStream As System.Text.StringBuilder
        Dim iStartTickCount As Integer
        Dim iTimeout As Integer
        Dim iResCode As Integer
        Dim iLoop As Integer
        Dim szHTTPStrArray() As String
        Dim szFieldArray() As String

        Try
            '======================================== Sets HttpWebRequest properties ====================================
            sbInStream = New System.Text.StringBuilder
            sbInStream.Append(sz_HTTPString)
            iTimeout = i_Timeout * 1000 'converts input timeout param from second to milliseconds

            objHttpWebRequest = WebRequest.Create(sz_URL)                       'Posting URL
            objHttpWebRequest.ContentType = "application/x-www-form-urlencoded" 'default value
            objHttpWebRequest.Method = "POST"                                   'default value
            objHttpWebRequest.ContentLength = sbInStream.Length ' Must be set b4 retrieving stream for sending data
            objHttpWebRequest.KeepAlive = False
            objHttpWebRequest.Timeout = iTimeout                                '(in milliseconds)

            'Ignore invalid server certificates or else may encounter WebException with e.Status = WebExceptionStatus.TrustFailure
            'When the CertificatePolicy property is set to an ICertificatePolicy interface instance, the ServicePointManager
            'uses the certificate policy defined in that instance instead of the default certificate policy.
            'The default certificate policy allows valid certificates, as well as valid certificates that have expired.
            ServicePointManager.CertificatePolicy = New SecPolicy

            iStartTickCount = System.Environment.TickCount()

            szHTTPStrArray = Split(sz_HTTPString, "&")
            For iLoop = 0 To szHTTPStrArray.GetUpperBound(0)
                szFieldArray = Split(szHTTPStrArray(iLoop), "=")
            Next iLoop

            '=========================================== Post request ==================================================
            '- StreamWriter constructor creates a StreamWriter with UTF-8 encoding whose GetPreamble method returns an
            'empty byte array. The BaseStream property is initialized using the stream parameter (i.e. objHttpWebRequest.GetRequestStream)
            '- GetRequestStream method returns a stream to use to send data for the HttpWebRequest. Once the Stream
            'instance has been returned, you can send data with the HttpWebRequest by using the Stream.Write method.
            'NOTE:    Must set the value of the ContentLength property before retrieving the stream.
            'CAUTION: Must call the Stream.Close method to close the stream and release the connection for reuse.
            '         Failure to close the stream will cause application to run out of connections.

            objStreamWriter = New StreamWriter(objHttpWebRequest.GetRequestStream())
            objStreamWriter.Write(sbInStream.ToString)  ' Sends data
            objStreamWriter.Close() ' Closes the stream and release the connection for reuse to avoid running out of connections

            '============================================= Get response =================================================

            '- GetResponse method returns a WebResponse instance containing the response from the Internet resource.
            'The actual instance returned is an instance of HttpWebResponse, and can be typecast to that class to access
            'HTTP-specific properties. When POST method is used, must get the request stream, write the data to be posted,
            'and close the stream. This method blocks waiting for content to post; If there is no time-out set and
            'content is not provided, application will block indefinitely.

            objHTTPWebResponse = objHttpWebRequest.GetResponse()

            If (objHttpWebRequest.HaveResponse And objHTTPWebResponse.StatusCode = HttpStatusCode.OK) Then
                objStreamReader = New StreamReader(objHTTPWebResponse.GetResponseStream)

                '================ Implements Timeout for getting HTTP reply from host <START> ====================
                Dim objRespReader As RespReader
                Dim oThreadStart As ThreadStart
                Dim oThread As Thread
                Dim iElapsedTime As Integer

                objRespReader = New RespReader
                objRespReader.SetReader(objStreamReader)

                oThreadStart = New ThreadStart(AddressOf objRespReader.WaitForResp)
                oThread = New Thread(oThreadStart)
                oThread.Start()

                'iStartTickCount = System.Environment.TickCount()
                iElapsedTime = System.Environment.TickCount() - iStartTickCount

                While ((iElapsedTime < iTimeout) And Not objRespReader.IsCompleted())
                    System.Threading.Thread.Sleep(50)
                    iElapsedTime = System.Environment.TickCount() - iStartTickCount
                End While

                If (iElapsedTime >= iTimeout) Then
                    ' Timeout
                    iRet = ERROR_CODE.E_COM_TIMEOUT
                Else
                    'Completed AND No Timeout. Could be got reply or exception
                    If (True = objRespReader.IsSucceeded()) Then
                        'Received reply within timeout period
                        sz_HTTPResponse = objRespReader.GetRespStream()
                    Else
                        'Exception within timeout period
                        iRet = ERROR_CODE.E_COM_TIMEOUT
                    End If
                End If

                oThread.Abort()
                oThreadStart = Nothing
                oThread = Nothing
                'objRespReader.Dispose()
                objRespReader = Nothing

                'Releases the resources of the response stream
                'NOTE: Failure to close the stream will cause application to run out of connections.
                objStreamReader.Close()
                '================== Implements Timeout for getting HTTP reply from host <END> =====================
            Else
            End If

            'Releases the resources of the response
            objHTTPWebResponse.Close()

            iRet = 0

        Catch webEx As WebException
            Try
                iRet = ERROR_CODE.E_COM_URL_UNREACHABLE

                If webEx.Status = WebExceptionStatus.Timeout Then
                    iRet = ERROR_CODE.E_COM_TIMEOUT
                End If

                If webEx.Status = WebExceptionStatus.ProtocolError Then
                    'Re: HttpStatusCode Enumeration
                    iResCode = CInt(CType(webEx.Response, HttpWebResponse).StatusCode)
                    If iResCode = 400 Then
                        iRet = ERROR_CODE.E_COM_BAD_REQUEST
                    ElseIf iResCode = 401 Then
                        iRet = ERROR_CODE.E_COM_UNAUTHORIZED_URL
                    ElseIf iResCode = 403 Then
                        iRet = ERROR_CODE.E_COM_FORBIDDEN_URL
                    ElseIf iResCode = 404 Then
                        iRet = ERROR_CODE.E_COM_URL_UNREACHABLE
                    End If
                End If
            Catch exp As Exception
            End Try

        Catch ioEx As IOException
            iRet = ERROR_CODE.E_COM_FILE_ERROR

        Catch ex As Exception
            iRet = ERROR_CODE.E_COMMON_UNKNOWN_ERROR
        Finally
        End Try

        Return iRet
    End Function
End Class
