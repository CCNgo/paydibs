Imports System.Security.Cryptography
Imports System.Text 'UTF8Encoding
Imports System.IO   'MemoryStream

Public Class Hash

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szGetQueryReqHash
    ' Function Type:	String. Query request hash value
    ' Parameter:		st_PayInfo
    ' Description:		Get Query request's hash value
    ' History:			23 Nov 2008
    '********************************************************************************************
    Public Function szGetQueryReqHash(ByVal st_QueryInfo As Common.STQueryInfo) As String
        Dim szHashValue As String
        szHashValue = ""

        szHashValue = st_QueryInfo.szMerchantPassword
        szHashValue = szHashValue + st_QueryInfo.szMerchantID
        szHashValue = szHashValue + st_QueryInfo.szMerchantTxnID
        szHashValue = szHashValue + st_QueryInfo.szTxnAmount            'Added  3 Oct 2013
        szHashValue = szHashValue + st_QueryInfo.szCurrencyCode         'Added  3 Oct 2013

        If (st_QueryInfo.szHashMethod.ToUpper() = "SHA2") Then          'Added  3 Oct 2013
            szHashValue = szComputeSHA256Hash(szHashValue)
        ElseIf (st_QueryInfo.szHashMethod.ToUpper() = "SHA1") Then
            szHashValue = szComputeSHA1Hash(szHashValue)
        ElseIf (st_QueryInfo.szHashMethod.ToUpper() = "MD5") Then
            szHashValue = szComputeMD5Hash(szHashValue)
        ElseIf (st_QueryInfo.szHashMethod.ToUpper() = "SHA512") Then
            szHashValue = szComputeSHA512Hash(szHashValue)
        Else
            szHashValue = ""
        End If

        Return szHashValue
    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szGetReversalReqHash
    ' Function Type:	String. Reversal request hash value
    ' Parameter:		st_PayInfo
    ' Description:		Get Reversal request's hash value
    ' History:			17 June 2011
    '********************************************************************************************
    Public Function szGetReversalReqHash(ByVal st_QueryInfo As Common.STQueryInfo) As String
        Dim szHashValue As String
        szHashValue = ""

        szHashValue = st_QueryInfo.szMerchantPassword
        szHashValue = szHashValue + st_QueryInfo.szMerchantID
        szHashValue = szHashValue + st_QueryInfo.szMerchantTxnID
        szHashValue = szHashValue + st_QueryInfo.szTxnAmount
        szHashValue = szHashValue + st_QueryInfo.szCurrencyCode

        If (st_QueryInfo.szHashMethod.ToUpper() = "SHA2") Then      'Added  3 Oct 2013
            szHashValue = szComputeSHA256Hash(szHashValue)
        ElseIf (st_QueryInfo.szHashMethod.ToUpper() = "SHA1") Then
            szHashValue = szComputeSHA1Hash(szHashValue)
        ElseIf (st_QueryInfo.szHashMethod.ToUpper() = "MD5") Then
            szHashValue = szComputeMD5Hash(szHashValue)
        ElseIf (st_QueryInfo.szHashMethod.ToUpper() = "SHA512") Then
            szHashValue = szComputeSHA512Hash(szHashValue)
        Else
            szHashValue = ""
        End If

        Return szHashValue
    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szGetQueryResHash
    ' Function Type:	String. Query response hash value
    ' Parameter:		st_PayInfo
    ' Description:		Get Query response's hash value
    ' History:			23 Nov 2008
    '********************************************************************************************
    Public Function szGetQueryResHash(ByVal st_QueryInfo As Common.STQueryInfo, ByVal sz_MerchantPassword As String) As String
        Dim szHashValue As String
        szHashValue = ""

        szHashValue = sz_MerchantPassword
        szHashValue = szHashValue + st_QueryInfo.szGatewayTxnID
        szHashValue = szHashValue + st_QueryInfo.szMerchantID
        szHashValue = szHashValue + st_QueryInfo.szMerchantTxnID
        szHashValue = szHashValue + st_QueryInfo.iMerchantTxnStatus.ToString()
        szHashValue = szHashValue + st_QueryInfo.szTxnAmount
        szHashValue = szHashValue + st_QueryInfo.szCurrencyCode
        szHashValue = szHashValue + st_QueryInfo.szHostTxnID

        If (st_QueryInfo.szHashMethod.ToUpper() = "SHA2") Then      'Added  3 Oct 2013
            szHashValue = szComputeSHA256Hash(szHashValue)
        ElseIf (st_QueryInfo.szHashMethod.ToUpper() = "SHA1") Then
            szHashValue = szComputeSHA1Hash(szHashValue)
        ElseIf (st_QueryInfo.szHashMethod.ToUpper() = "MD5") Then
            szHashValue = szComputeMD5Hash(szHashValue)
        ElseIf (st_QueryInfo.szHashMethod.ToUpper() = "SHA512") Then
            szHashValue = szComputeSHA512Hash(szHashValue)
        Else
            szHashValue = ""
            'Log error 
        End If

        Return szHashValue
    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szComputeSHA1Hash
    ' Function Type:	String. Hash value generated using SHA-1 algorithm.
    ' Parameter:		sz_Key. Values to be hash.
    ' Description:		Get hash value generated using SHA-1 algorithm
    ' History:			20 Oct 2008
    '********************************************************************************************
    Public Function szComputeSHA1Hash(ByVal sz_Key As String) As String
        Dim szHashValue As String
        Dim objSHA1 As SHA1CryptoServiceProvider
        Dim btHashbuffer() As Byte

        objSHA1 = New SHA1CryptoServiceProvider

        Try
            'UTF-8 (8-bit UCS/Unicode Transformation Format) is a variable-length character encoding for Unicode.
            'It is able to represent any character in the Unicode standard, yet the initial encoding of byte codes and
            'character assignments for UTF-8 is backwards compatible with ASCII.
            'For these reasons, it is steadily becoming the preferred encoding for e-mail, web pages, and other places
            'where characters are stored or streamed.
            'String.ToCharArray - Copies the characters in this instance to a Unicode character array.
            'System.Text.Encoding.UTF8 - This class encodes Unicode characters using UCS Transformation Format, 8-bit form (UTF-8).
            '.GetBytes - This method encodes characters into an array of bytes.
            'System.Convert.ToBase64String - Converts the value of an array of 8-bit unsigned integers to its equivalent
            'String representation consisting of base 64 digits.

            objSHA1.ComputeHash(System.Text.Encoding.UTF8.GetBytes(sz_Key.ToCharArray))
            btHashbuffer = objSHA1.Hash
            szHashValue = System.Convert.ToBase64String(btHashbuffer)

        Catch ex As Exception
            'oLogger.Log(Logger.CLogger.LOG_SEVERITY.WARNING, __FILE__, __METHOD__, __LINE__, "Err: " + ex.ToString)
            Throw ex
        Finally
            If Not objSHA1 Is Nothing Then objSHA1 = Nothing
        End Try

        Return szHashValue
    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szComputeMD5Hash
    ' Function Type:	String. Hash value generated using MD5 algorithm.
    ' Parameter:		sz_Key. Values to be hash.
    ' Description:		Get hash value generated using MD5 algorithm
    ' History:			20 Oct 2008
    '********************************************************************************************
    Public Function szComputeMD5Hash(ByVal sz_Key As String) As String
        Dim szHashValue As String
        Dim objMD5 As MD5CryptoServiceProvider
        Dim btHashbuffer() As Byte

        objMD5 = New MD5CryptoServiceProvider

        Try
            objMD5.ComputeHash(System.Text.Encoding.UTF8.GetBytes(sz_Key.ToCharArray))
            btHashbuffer = objMD5.Hash
            szHashValue = System.Convert.ToBase64String(btHashbuffer)

        Catch ex As Exception
            'oLogger.Log(Logger.CLogger.LOG_SEVERITY.WARNING, __FILE__, __METHOD__, __LINE__, "[" + g_szLogID + "]ERROR: " + ex.ToString)
            Throw ex
        Finally
            If Not objMD5 Is Nothing Then objMD5 = Nothing
        End Try

        Return szHashValue
    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szComputeSHA256Hash
    ' Function Type:	String. Hash value generated using SHA256 algorithm.
    ' Parameter:		sz_Key. Values to be hash.
    ' Description:		Get hash value generated using SHA256 algorithm
    ' History:			03 Oct 2013
    '********************************************************************************************
    Public Function szComputeSHA256Hash(ByVal sz_Key As String) As String
        Dim szHashValue As String = ""
        Dim objSHA256 As SHA256 = Nothing
        Dim btHashbuffer() As Byte = Nothing

        Try
            objSHA256 = New SHA256Managed
            objSHA256.ComputeHash(System.Text.Encoding.ASCII.GetBytes(sz_Key))
            btHashbuffer = objSHA256.Hash
            szHashValue = szWriteHex(btHashbuffer).ToLower()

        Catch ex As Exception
            Throw ex
        Finally
            If Not objSHA256 Is Nothing Then objSHA256 = Nothing
        End Try

        Return szHashValue
    End Function

    Public Function szComputeSHA512Hash(ByVal sz_Key As String) As String
        Dim szHashValue As String = ""
        Dim objSHA512 As SHA512 = Nothing
        Dim btHashbuffer() As Byte = Nothing

        Try
            objSHA512 = New SHA512Managed
            objSHA512.ComputeHash(System.Text.Encoding.ASCII.GetBytes(sz_Key))
            btHashbuffer = objSHA512.Hash
            szHashValue = szWriteHex(btHashbuffer).ToLower()

        Catch ex As Exception
            Throw ex
        Finally
            If Not objSHA512 Is Nothing Then objSHA512 = Nothing
        End Try

        Return szHashValue
    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szWriteHex
    ' Function Type:	String. Hex string value converted from byte array.
    ' Parameter:		bt_Array. Byte values to be converted to hex.
    ' Description:		Get hex string value converted from byte array.
    ' History:			03 Oct 2013
    '********************************************************************************************
    Public Function szWriteHex(ByVal bt_Array() As Byte) As String
        Dim szHex As String = ""
        Dim iCounter As Integer

        For iCounter = 0 To (bt_Array.Length() - 1)
            szHex += bt_Array(iCounter).ToString("X2")
        Next

        Return szHex
    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szDecrypt
    ' Function Type:	Return clear text in HEX
    ' Parameter:		Cipher Text, Key, Initialization Vector
    ' Description:		Decrypt a cipher text into clear text using 3DES (PKCS5, PKCS7 padding and CBC cipher mode)
    ' History:			3 Oct 2013
    '********************************************************************************************
    Public Function szDecrypt3DES(ByVal sz_CipherText As String, ByVal sz_Key As String, ByVal sz_InitVector As String) As String
        Dim obj3DES As New TripleDESCryptoServiceProvider
        Dim objUTF8 As New UTF8Encoding
        Dim btInput() As Byte
        Dim btOutput() As Byte

        Try
            szDecrypt3DES = ""

            obj3DES.Padding = PaddingMode.PKCS7 ' Default value
            obj3DES.Mode = CipherMode.CBC       ' Default value
            obj3DES.Key = btHexToByte(sz_Key)

            If (False = bValidHex(sz_InitVector)) Then
                sz_InitVector = szStringToHex(sz_InitVector)
            End If
            obj3DES.IV = btHexToByte(sz_InitVector)

            'Added on 4 May 2009, to remove LF or CRLF
            'or else will encounter exception "Index was outside the bounds of the array" in btHextoByte
            'e.g. PBB Query reply contains CRLF or LF, causing its length become not genap but ganjil
            sz_CipherText = Replace(sz_CipherText, vbCrLf, "")
            sz_CipherText = Replace(sz_CipherText, vbCr, "")
            sz_CipherText = Replace(sz_CipherText, vbLf, "")

            If (False = bValidHex(sz_CipherText)) Then
                sz_CipherText = szStringToHex(sz_CipherText)
            End If
            btInput = btHexToByte(sz_CipherText)

            btOutput = btTransform(btInput, obj3DES.CreateDecryptor())

            'Comment on 25 May 2009, use UTF GetString if encrypted value's clear texts are not hexadecimal value,
            'e.g. BillRef1=ABC&Amt=123.99
            szDecrypt3DES = objUTF8.GetString(btOutput)

        Catch ex As Exception
            Throw ex
        Finally
            If Not obj3DES Is Nothing Then obj3DES = Nothing
            If Not objUTF8 Is Nothing Then objUTF8 = Nothing
        End Try
    End Function

    'Added  3 Oct 2013
    Public Function btHexToByte(ByVal c_Hex As Char()) As Byte()
        Dim szHex As String = ""

        Dim btOutput(c_Hex.Length() / 2 - 1) As Byte 'Byte() = New Byte(c_Hex.Length / 2 - 1) {}
        Dim iOffset As Integer = 0
        Dim iCounter As Integer = 0

        iCounter = 0
        While iCounter < c_Hex.Length()
            szHex = Convert.ToString(c_Hex(iCounter)) + Convert.ToString(c_Hex(iCounter + 1))
            btOutput(iOffset) = Byte.Parse(szHex, System.Globalization.NumberStyles.HexNumber)
            iOffset += 1
            iCounter = iCounter + 2
        End While

        Return btOutput
    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	bValidHex
    ' Function Type:	Boolean. True: Valid HEX False: Invalid HEX
    ' Parameter:		Input string for HEX validation
    ' Description:		Check if the string contains hexadecimal values
    ' History:			3 Oct 2013
    '********************************************************************************************
    Public Function bValidHex(ByVal sz_Txt As String) As Boolean
        Dim szch As String
        Dim i As Integer

        sz_Txt = sz_Txt.ToUpper()
        For i = 1 To Len(sz_Txt)

            szch = Mid(sz_Txt, i, 1)
            ' See if the next character is a non-digit.
            If (False = IsNumeric(szch)) Then
                If szch >= "G" Then
                    ' This is not a letter.
                    Return False
                End If
            End If
        Next i

        Return True
    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szStringToHex
    ' Function Type:	String. Hex string value converted from String.
    ' Parameter:		sz_text. String values to be converted to hex.
    ' Description:		Get hex string value converted from String.
    ' History:			3 Oct 2013
    '********************************************************************************************
    Public Function szStringToHex(ByVal sz_Text As String) As String
        Dim szHex As String = ""

        For i As Integer = 0 To sz_Text.Length - 1
            szHex &= Asc(sz_Text.Substring(i, 1)).ToString("x").ToUpper
        Next

        Return szHex
    End Function

    'Added  3 Oct 2013
    Public Function btTransform(ByVal bt_Input() As Byte, ByVal I_CryptoTransform As ICryptoTransform) As Byte()
        ' Create the necessary streams
        Dim objMemStream As MemoryStream = New MemoryStream
        Dim objCryptStream As CryptoStream = New CryptoStream(objMemStream, I_CryptoTransform, CryptoStreamMode.Write)
        Dim btResult() As Byte

        Try
            ' Transform the bytes as requested
            objCryptStream.Write(bt_Input, 0, bt_Input.Length())
            objCryptStream.FlushFinalBlock()

            ' Read the memory stream and convert it back into byte array
            objMemStream.Position = 0

            ReDim btResult(CType(objMemStream.Length() - 1, System.Int32))
            objMemStream.Read(btResult, 0, CType(btResult.Length(), System.Int32))

        Catch ex As Exception
            Throw ex
        Finally
            ' close and release the streams
            objMemStream.Close()
            objCryptStream.Close()

            If Not objMemStream Is Nothing Then objMemStream = Nothing
            If Not objCryptStream Is Nothing Then objCryptStream = Nothing
        End Try

        ' hand back the result buffer
        Return btResult
    End Function
End Class
