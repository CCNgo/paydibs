Windows Service Installation
1) Add installer to service - Click on design view, click "Add Installer", a ProjectInstaller.vb ll be created.
2) Double click ProjectInstaller.vb and you can see a ServiceProcessInstaller and ServiceInstaller component.
3) Click ServiceInstaller and ensure the ServiceName value matched. Can specify StartType.
4) Click ServiceProcessInstaller to specify Account to be "LocalSystem" instead of "User".
5) Add setup project to the Windows Service project
6) Runs command line utility InstallUtil.exe project.exe; To uninstall InstallUtil.exe project.exe -u