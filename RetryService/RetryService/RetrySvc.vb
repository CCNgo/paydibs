﻿Imports System.Reflection       'Assembly
Imports System.Xml              'XMLTextReader
Imports System.IO               'StreamWriter
Imports System.Threading        'Timer, TimerCallback delegate, WaitOne
Imports System.ServiceProcess

Public Class RetrySvc
    Private m_objRetryQuery() As TxnProc
    Private m_objRetryReversal() As TxnProc 'Added on 10 June 2011, cater for reversal
    Private m_objRetryBooking As TxnProc
    Private m_objThreadStart() As ThreadStart
    Private m_objThread() As Thread
    Private m_objHash As Hash               'Added on 3 Oct 2013, cater for db string decryption

    Private m_bStart As Boolean = False
    Private m_szConfigPath As String = ""
    Private m_iTimeout As Integer = 0
    Private m_iElapsedTime As Integer = 0

    'Configuration file
    Private m_szDBConnStr As String = ""
    Private m_szLogPath As String = ""
    Private m_szQueryURL As String = ""
    Private m_szPymtMethod As String = ""
    Private m_iCheckIntervalSeconds As Integer = 0
    Private m_iHostID() As Integer
    Private m_szHostName() As String
    Private m_iRevHostID() As Integer       'Added on 10 June 2011, cater for reversal
    Private m_szRevHostName() As String     'Added on 10 June 2011, cater for reversal
    Private m_iSvcEnable As Integer = -1    'Added by OoiMei on 24 July 2012, to turn off booking/other services in future

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Dim iCount As Integer = 0

        Try
            m_objHash = New Hash

            ' Loads configuration settings
            'Get XML Path   => Replace(App path with project.exe name, "") + "Settings.xml"
            '               => [Assembly].GetExecutingAssembly().Location.Replace("project.exe", "Settings.xml")
            m_szConfigPath = [Assembly].GetExecutingAssembly().Location.Replace([Assembly].GetExecutingAssembly().GetModules()(0).Name, "Settings.xml")
            LoadConfig()

            ReDim m_objRetryQuery(m_iHostID.GetUpperBound(0))
            ReDim m_objRetryReversal(m_iRevHostID.GetUpperBound(0)) 'Added on 10 June 2011, cater for reversal

            For iCount = 0 To (m_iHostID.GetUpperBound(0) - 1)
                m_objRetryQuery(iCount) = New TxnProc()
            Next

            'Added on 10 June 2011, cater for reversal
            For iCount = 0 To (m_iRevHostID.GetUpperBound(0) - 1)
                m_objRetryReversal(iCount) = New TxnProc()
            Next

            If (1 = m_iSvcEnable) Then
                m_objRetryBooking = New TxnProc()
            End If

        Catch ex As Exception
            Log("C:\\RetrySvc.log", "RetryApp_Load() Exception: " + ex.Message)
        End Try

    End Sub

    Private Sub LoadConfig()
        Dim szTemp As String = ""
        Dim iCount As Integer = 0               'Added on 10 June 2011, cater for reversal
        Dim iNum As Integer = 0
        'Dim iSvcCount As Integer = 0
        Dim objXMLReader As XmlTextReader
        Dim objXMLDoc As XmlDocument
        Dim objXMLRoot As XmlElement
        Dim objXMLNodeList As XmlNodeList
        Dim objXMLRevNodeList As XmlNodeList    'Added on 10 June 2011, cater for reversal
        'Dim objXMLSvcNodeList As XmlNodeList    'Added by OoiMei on 24 July 2012, to turn off booking/other services in future

        objXMLReader = New XmlTextReader(m_szConfigPath)
        objXMLDoc = New XmlDocument()

        Try
            objXMLDoc.Load(m_szConfigPath)
            objXMLRoot = objXMLDoc.DocumentElement
            objXMLNodeList = objXMLRoot.GetElementsByTagName("Host")
            'objXMLRevNodeList = objXMLRoot.GetElementsByTagName("ReversalHosts")   'Added on 10 June 2011, cater for reversal
            objXMLRevNodeList = objXMLRoot.GetElementsByTagName("RevHost")          'Modified by OoiMei on  25 July 2012, should use RevHost to get the element and not ReversalHosts
            'objXMLSvcNodeList = objXMLRoot.GetElementsByTagName("Services")        'Added by OoiMei on 24 July 2012, to turn off booking/other services in future
            iCount = objXMLNodeList.Count
            iNum = objXMLRevNodeList.Count  'Added on 10 June 2011, to cater for reversal

            ReDim m_iHostID(iCount)
            ReDim m_szHostName(iCount)
            ReDim m_iRevHostID(iNum)        'Added on 10 June 2011, cater for reversal
            ReDim m_szRevHostName(iNum)     'Added on 10 June 2011, cater for reversal

            iCount = 0
            iNum = 0                        'Added on 10 June 2011, cater for reversal
            While (Not objXMLReader.EOF)
                objXMLReader.Read()
                szTemp = objXMLReader.Name

                If (objXMLReader.NodeType <> XmlNodeType.EndElement) Then

                    Select Case szTemp.ToLower()
                        Case "logpath"
                            m_szLogPath = objXMLReader.ReadInnerXml()
                        Case "dbstring"
                            m_szDBConnStr = objXMLReader.ReadInnerXml()
                            'Added 3 Oct 2013, cater for encrypted database string
                            If (InStr(m_szDBConnStr.ToLower(), "uid=") <= 0) Then
                                m_szDBConnStr = m_objHash.szDecrypt3DES(m_szDBConnStr, "3537146182309002FEB7EBCE55ED3BBA71BED7CEB1D18CAA", "0000000000000000")
                            End If
                        Case "queryurl"
                            m_szQueryURL = objXMLReader.ReadInnerXml()
                        Case "checkintervalseconds"
                            m_iCheckIntervalSeconds = Convert.ToInt32(objXMLReader.ReadInnerXml())
                        Case "pymtmethod"
                            m_szPymtMethod = objXMLReader.ReadInnerXml()
                        Case "host"
                            m_iHostID(iCount) = objXMLReader.GetAttribute("HostID")
                            m_szHostName(iCount) = objXMLReader.GetAttribute("Name")
                            iCount = iCount + 1
                        Case "revhost" 'Added on 10 June 2011, to cater for reversal
                            m_iRevHostID(iNum) = objXMLReader.GetAttribute("RevHostID")
                            m_szRevHostName(iNum) = objXMLReader.GetAttribute("RevName")
                            iNum = iNum + 1
                        Case "productfullfillment"
                            m_iSvcEnable = Convert.ToInt32(objXMLReader.ReadInnerXml())
                    End Select
                End If
            End While

        Catch ex As Exception
            Log("C:\\RetrySvc.log", "LoadConfig() exception: " + ex.Message)
        Finally
            objXMLReader.Close()
            objXMLReader = Nothing

            objXMLNodeList = Nothing
            objXMLRoot = Nothing
            objXMLDoc = Nothing
        End Try

    End Sub

    Private Sub Log(ByVal sz_LogFile As String, ByVal sz_Text As String)
        Dim objStreamWriter As StreamWriter
        Dim szLogFile(2) As String
        Dim szLogPath As String = ""

        If InStr(sz_LogFile, ".") > 0 Then
            szLogFile = Split(sz_LogFile, ".")
            szLogPath = szLogFile(0) + "_" + DateTime.Now.ToString("yyyyMMdd") + "." + szLogFile(1)
        Else
            szLogPath = sz_LogFile + "_" + DateTime.Now.ToString("yyyyMMdd")
        End If

        objStreamWriter = New StreamWriter(szLogPath, True)
        objStreamWriter.WriteLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + " >>> " + sz_Text)
        objStreamWriter.Close()
    End Sub

    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.
        Dim iLoop As Integer = 0
        Dim iNum As Integer = 0

        m_bStart = True

        'Sets Retry Query, Retry Reversal and Retry Booking object's member variables     'Added on 10 June 2011, to cater for reversal
        For iLoop = 0 To (m_iHostID.GetUpperBound(0) - 1)
            m_objRetryQuery(iLoop).m_bStart = m_bStart
        Next

        For iLoop = 0 To (m_iRevHostID.GetUpperBound(0) - 1) 'Added on 10 June 2011, to cater for reversal
            m_objRetryReversal(iLoop).m_bStart = m_bStart
        Next

        If (1 = m_iSvcEnable) Then
            m_objRetryBooking.m_bStart = m_bStart
        End If

        Try
            Log(m_szLogPath, "Starting service...")

            If (1 = m_iSvcEnable) Then
                ReDim m_objThreadStart(m_iHostID.GetUpperBound(0) + m_iRevHostID.GetUpperBound(0) + 1) 'Query thread for each Host + Reversal thread for each Host + 1 Booking thread    'Added on 13 June 2011, to cater for reversal
                ReDim m_objThread(m_iHostID.GetUpperBound(0) + m_iRevHostID.GetUpperBound(0) + 1) 'Added on 13 June 2011, to cater for reversal
            Else
                ReDim m_objThreadStart(m_iHostID.GetUpperBound(0) + m_iRevHostID.GetUpperBound(0))
                ReDim m_objThread(m_iHostID.GetUpperBound(0) + m_iRevHostID.GetUpperBound(0))
            End If

            iLoop = 0 'Added on 13 June 2011, to cater for reversal
            While (iLoop < m_iHostID.GetUpperBound(0)) 'Added on 13 June 2011, to cater for reversal. Change from For loop to while loop, so that iLoop value can be continue used for reversal loop below
                'Starts Retry Query thread for getting txns that need to query Direct Debit Gateway to trigger Query Host
                Log(m_szLogPath, "Retry Query thread started for HostID(" + m_iHostID(iLoop).ToString() + ") HostName(" + m_szHostName(iLoop) + ")")

                m_objRetryQuery(iLoop).m_iAction = 3
                m_objRetryQuery(iLoop).m_szDBConnStr = m_szDBConnStr
                m_objRetryQuery(iLoop).m_szQueryURL = m_szQueryURL
                m_objRetryQuery(iLoop).m_szPymtMethod = m_szPymtMethod      'Added  3 Oct 2013
                m_objRetryQuery(iLoop).m_szLogPath = m_szLogPath
                m_objRetryQuery(iLoop).m_iCheckIntervalSeconds = m_iCheckIntervalSeconds
                m_objRetryQuery(iLoop).m_iStartTickCount = System.Environment.TickCount()

                m_objRetryQuery(iLoop).m_iHostID = m_iHostID(iLoop)
                m_objRetryQuery(iLoop).m_szHostName = m_szHostName(iLoop)

                m_objThreadStart(iLoop) = New ThreadStart(AddressOf m_objRetryQuery(iLoop).bRetryTxns)
                m_objThread(iLoop) = New Thread(m_objThreadStart(iLoop))
                m_objThread(iLoop).Start()

                System.Threading.Thread.Sleep(1000) 'Sleeps one second b4 proceed or else will encounter error
                iLoop = iLoop + 1 'Added on 13 June 2011, to cater for reversal
            End While

            'Added on 13 June 2011, to cater for reversal
            iNum = 0
            While (iNum < m_iRevHostID.GetUpperBound(0)) 'Added on 13 June 2011, to cater for reversal.
                'Starts Retry Reversal thread for getting txns that need to send reversal to Direct Debit Gateway to trigger Reversal Host
                Log(m_szLogPath, "Retry Reversal thread started for HostID(" + m_iRevHostID(iNum).ToString() + ") HostName(" + m_szRevHostName(iNum) + ")")

                m_objRetryReversal(iNum).m_iAction = 7
                m_objRetryReversal(iNum).m_szDBConnStr = m_szDBConnStr
                m_objRetryReversal(iNum).m_szQueryURL = m_szQueryURL
                m_objRetryReversal(iNum).m_szPymtMethod = m_szPymtMethod    'Added  3 Oct 2013
                m_objRetryReversal(iNum).m_szLogPath = m_szLogPath
                m_objRetryReversal(iNum).m_iCheckIntervalSeconds = m_iCheckIntervalSeconds
                m_objRetryReversal(iNum).m_iStartTickCount = System.Environment.TickCount()

                m_objRetryReversal(iNum).m_iHostID = m_iRevHostID(iNum)
                m_objRetryReversal(iNum).m_szHostName = m_szRevHostName(iNum)

                m_objThreadStart(iLoop) = New ThreadStart(AddressOf m_objRetryReversal(iNum).bRetryTxns)
                m_objThread(iLoop) = New Thread(m_objThreadStart(iLoop))
                m_objThread(iLoop).Start()

                System.Threading.Thread.Sleep(1000) 'Sleeps one second b4 proceed or else will encounter error
                iNum = iNum + 1
                iLoop = iLoop + 1
            End While

            'Starts Retry Booking thread for getting txns that need to query Direct Debit Gateway to trigger Confirm Booking
            If (1 = m_iSvcEnable) Then
                Log(m_szLogPath, "Retry Booking thread started")

                m_objRetryBooking.m_iAction = 4
                m_objRetryBooking.m_szDBConnStr = m_szDBConnStr
                m_objRetryBooking.m_szQueryURL = m_szQueryURL
                m_objRetryBooking.m_szLogPath = m_szLogPath
                m_objRetryBooking.m_iCheckIntervalSeconds = m_iCheckIntervalSeconds
                m_objRetryBooking.m_iStartTickCount = System.Environment.TickCount()

                m_objThreadStart(iLoop) = New ThreadStart(AddressOf m_objRetryBooking.bRetryTxns)
                m_objThread(iLoop) = New Thread(m_objThreadStart(iLoop))
                m_objThread(iLoop).Start()
            End If

        Catch ex As Exception
            Log(m_szLogPath, "OnStart() Exception " + ex.Message)
        Finally
            Log(m_szLogPath, "Service started")
        End Try
    End Sub

    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
        Dim iLoop As Integer = 0

        m_bStart = False

        'Sets Retry Query, Retry Reversal and Retry Booking object's member variables      'Added on 13 June 2011, to cater for reversal.
        For iLoop = 0 To (m_iHostID.GetUpperBound(0) - 1)
            m_objRetryQuery(iLoop).m_bStart = m_bStart
        Next

        'Added on 13 June 2011, to cater for reversal.
        For iLoop = 0 To (m_iRevHostID.GetUpperBound(0) - 1)
            m_objRetryReversal(iLoop).m_bStart = m_bStart
        Next

        If (1 = m_iSvcEnable) Then
            m_objRetryBooking.m_bStart = m_bStart
        End If

        Try
            Log(m_szLogPath, "Stopping service...")

            For iLoop = 0 To (m_iHostID.GetUpperBound(0) - 1)
                While (Not m_objRetryQuery(iLoop).IsCompleted())
                    System.Threading.Thread.Sleep(50)
                End While
            Next

            'Added on 13 June 2011, to cater for reversal.
            For iLoop = 0 To (m_iRevHostID.GetUpperBound(0) - 1)
                While (Not m_objRetryReversal(iLoop).IsCompleted())
                    System.Threading.Thread.Sleep(50)
                End While
            Next

            If (1 = m_iSvcEnable) Then
                While (Not m_objRetryBooking.IsCompleted())
                    System.Threading.Thread.Sleep(50)
                End While
            End If
            'For iLoop = 0 To (m_iHostID.GetUpperBound(0) - 1)
            '    m_iElapsedTime = System.Environment.TickCount() - m_objRetryQuery(iLoop).m_iStartTickCount
            '    While ((m_iElapsedTime < m_iTimeout) And Not m_objRetryQuery(iLoop).IsCompleted())
            '        System.Threading.Thread.Sleep(50)
            '        m_iElapsedTime = System.Environment.TickCount() - m_objRetryQuery(iLoop).m_iStartTickCount
            '    End While
            'Next

            'm_iElapsedTime = System.Environment.TickCount() - m_objRetryBooking.m_iStartTickCount
            'While ((m_iElapsedTime < m_iTimeout) And Not m_objRetryBooking.IsCompleted())
            '   System.Threading.Thread.Sleep(50)
            '   m_iElapsedTime = System.Environment.TickCount() - m_objRetryBooking.m_iStartTickCount
            'End While

            'Terminates threads
            For iLoop = 0 To (m_objThread.GetUpperBound(0) - 1)
                m_objThread(iLoop).Abort()
                m_objThreadStart(iLoop) = Nothing
                m_objThread(iLoop) = Nothing

                Log(m_szLogPath, "Thread " + iLoop.ToString() + " terminated")
            Next

        Catch ex As Exception
            Log(m_szLogPath, "OnStop() Exception " + ex.Message)
        Finally
            Log(m_szLogPath, "Service stopped")
        End Try
    End Sub

    Protected Overrides Sub OnShutdown()
        Dim iLoop As Integer = 0

        m_bStart = False

        'Sets Retry Query, Retry Reversal and Retry Booking object's member variables    'Added on 13 June 2011, to cater for reversal.
        For iLoop = 0 To (m_iHostID.GetUpperBound(0) - 1)
            m_objRetryQuery(iLoop).m_bStart = m_bStart
        Next

        'Added on 13 June 2011, to cater for reversal.
        For iLoop = 0 To (m_iRevHostID.GetUpperBound(0) - 1)
            m_objRetryReversal(iLoop).m_bStart = m_bStart
        Next

        If (1 = m_iSvcEnable) Then
            m_objRetryBooking.m_bStart = m_bStart
        End If

        Try
            Log(m_szLogPath, "Shutting down service...")

            For iLoop = 0 To (m_iHostID.GetUpperBound(0) - 1)
                While (Not m_objRetryQuery(iLoop).IsCompleted())
                    System.Threading.Thread.Sleep(50)
                End While
            Next

            'Added on 13 June 2011, to cater for reversal.
            For iLoop = 0 To (m_iRevHostID.GetUpperBound(0) - 1)
                While (Not m_objRetryReversal(iLoop).IsCompleted())
                    System.Threading.Thread.Sleep(50)
                End While
            Next

            If (1 = m_iSvcEnable) Then
                While (Not m_objRetryBooking.IsCompleted())
                    System.Threading.Thread.Sleep(50)
                End While
            End If

            'For iLoop = 0 To (m_iHostID.GetUpperBound(0) - 1)
            '    m_iElapsedTime = System.Environment.TickCount() - m_objRetryQuery(iLoop).m_iStartTickCount
            '    While ((m_iElapsedTime < m_iTimeout) And Not m_objRetryQuery(iLoop).IsCompleted())
            '        System.Threading.Thread.Sleep(50)
            '        m_iElapsedTime = System.Environment.TickCount() - m_objRetryQuery(iLoop).m_iStartTickCount
            '    End While
            'Next

            'm_iElapsedTime = System.Environment.TickCount() - m_objRetryBooking.m_iStartTickCount
            'While ((m_iElapsedTime < m_iTimeout) And Not m_objRetryBooking.IsCompleted())
            '   System.Threading.Thread.Sleep(50)
            '   m_iElapsedTime = System.Environment.TickCount() - m_objRetryBooking.m_iStartTickCount
            'End While

            'Terminates threads
            For iLoop = 0 To (m_objThread.GetUpperBound(0) - 1)
                m_objThread(iLoop).Abort()
                m_objThreadStart(iLoop) = Nothing
                m_objThread(iLoop) = Nothing

                Log(m_szLogPath, "Thread " + iLoop.ToString() + " terminated")
            Next

        Catch ex As Exception
            Log(m_szLogPath, "OnShutdown() Exception " + ex.Message)
        Finally
            Log(m_szLogPath, "Service shutdown")
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        Dim iLoop As Integer = 0

        For iLoop = 0 To (m_iHostID.GetUpperBound(0) - 1)
            m_objRetryQuery(iLoop) = Nothing
        Next

        'Added on 13 June 2011, to cater for reversal.
        For iLoop = 0 To (m_iRevHostID.GetUpperBound(0) - 1)
            m_objRetryReversal(iLoop) = Nothing
        Next

        m_objRetryBooking = Nothing

        MyBase.Finalize()
    End Sub
End Class
