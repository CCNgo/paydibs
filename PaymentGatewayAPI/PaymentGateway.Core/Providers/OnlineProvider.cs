﻿using LoggerII;
using PaymentGatewayAPI.Core.Models;
using PaymentGatewayAPI.Core.UPPPaymentQueryRequestMessage0780_TestURL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewayAPI.Core.Providers
{
    internal class OnlineProvider
    {
        private const string LoggingPaymentProvider = "[DB PaymentGatewayAPI]";

        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();
        public string msg = string.Empty;

        private string OBconnStrName = "OB_conn"; //ncctemp
        private string PGAdminconnStrName = "PGAdmin_conn";

        public TxnParam GetPaymentTransaction(TxnParam param)
        {
            Database.DB database = new Database.DB();
            var connStr = database.connstring(OBconnStrName);
             
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(connStr.StrConn))
                {
                    List<TxnParam> systemConfig = new List<TxnParam>();

                    var config = new TxnParam();

                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = Database.OB.StoredProcedures.spGetTxnParam;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = sqlConnection;

                        cmd.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30).Value = param.MerchantID;
                        cmd.Parameters.Add("@sz_MerchantTxnID", SqlDbType.VarChar, 30).Value = param.MerchantTxnID;
                        cmd.Parameters.Add("@i_ReqRes", SqlDbType.Int).Value = param.ReqRes;

                        sqlConnection.Open();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);

                        foreach (DataRow dr in dt.Rows)
                        {
                            switch (dr["KeyName"].ToString())
                            {

                            }
                        }
                    }
                    return config;
                }
            }
            catch (Exception ex)
            {
                msg = LoggingPaymentProvider + "[GetPaymentTransaction]" + DateTime.Now + ": [Configuration Exception: " + ex.ToString() + "]";
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                    msg);

                return null;
            }
        }

        public Merchant GetMerchant(string merchantId)
        {
            Database.DB database = new Database.DB();
          
            var connStr = database.connstring("PG_conn");
           
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(connStr.StrConn))
                {
                    List<Merchant> results = new List<Merchant>();

                    using (SqlCommand cmd = new SqlCommand())
                    {
                        Logger logger = new Logger();
                        CLoggerII objLoggerII = new CLoggerII();
                        objLoggerII = logger.InitLogger();
                        cmd.CommandText = Database.PG.StoredProcedures.spGetMerchant;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = sqlConnection;
                        cmd.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30).Value = merchantId;

                        sqlConnection.Open();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);

                        foreach (DataRow dr in dt.Rows)
                        {
                            Merchant merchant = new Merchant();
                           
                            merchant.PaymentTemplate = dr["PaymentTemplate"].ToString();
                            merchant.ErrorTemplate = dr["ErrorTemplate"].ToString();
                            merchant.AllowReversal = dr["AllowReversal"].ToString();
                            merchant.AllowFDS = dr["AllowFDS"].ToString();
                            merchant.CollectShipAddr = dr["CollectShipAddr"].ToString();
                            merchant.CollectBillAddr = dr["CollectBillAddr"].ToString();
                            merchant.AllowMaxMind = dr["AllowMaxMind"].ToString();
                            merchant.FraudByAmt = dr["FraudByAmt"].ToString();
                            merchant.SvcTypeID = dr["SvcTypeID"].ToString();
                            merchant.AllowPayment = dr["AllowPayment"].ToString();
                            merchant.AllowQuery = dr["AllowQuery"].ToString();
                            merchant.AllowOB = dr["AllowOB"].ToString();
                            //merchant.AllowOTC = dr["AllowOTC"].ToString();
                            merchant.AllowWallet = dr["AllowWallet"].ToString();
                            //merchant.AllowOCP = dr["AllowOCP"].ToString();
                            //merchant.AllowCallBack = dr["AllowCallBack"].ToString();
                            //merchant.AllowFX = dr["AllowFX"].ToString();
                            //merchant.RouteByParam1 = dr["RouteByParam1"].ToString();
                            merchant.PymtPageTimeout_S = dr["PymtPageTimeout_S"].ToString();
                            merchant.ThreeDAccept = dr["ThreeDAccept"].ToString();
                            //merchant.SelfMPI = dr["SelfMPI"].ToString();
                            merchant.AutoReversal = dr["AutoReversal"].ToString();
                            merchant.RespMethod = dr["RespMethod"].ToString();
                            merchant.HCProfileID = dr["HCProfileID"].ToString();
                            merchant.PerTxnAmtLimit = dr["PerTxnAmtLimit"].ToString();
                            merchant.HitLimitAct = dr["HitLimitAct"].ToString();
                            merchant.City = dr["City"].ToString();
                            //merchant.Country2 = dr["Country2"].ToString();
                            merchant.Addr1 = dr["Addr1"].ToString();
                            merchant.Addr2 = dr["Addr2"].ToString();
                            merchant.Addr3 = dr["Addr3"].ToString();
                            merchant.City = dr["City"].ToString();
                            merchant.PostCode = dr["PostCode"].ToString();
                            merchant.State = dr["State"].ToString();
                            merchant.Country = dr["Country"].ToString();
                            merchant.ContactNo = dr["ContactNo"].ToString();
                            merchant.EmailAddr = dr["EmailAddr"].ToString();
                            merchant.NotifyEmailAddr = dr["NotifyEmailAddr"].ToString();
                            merchant.WebSiteURL = dr["WebSiteURL"].ToString();
                            merchant.ValidDomain = dr["ValidDomain"].ToString();
                            merchant.VISA = dr["VISA"].ToString();
                            merchant.MasterCard = dr["MasterCard"].ToString();
                            merchant.AMEX = dr["AMEX"].ToString();
                            merchant.JCB = dr["JCB"].ToString();
                            merchant.Diners = dr["Diners"].ToString();
                            merchant.CUP = dr["CUP"].ToString();
                            //merchant.MasterPass = dr["MasterPass"].ToString();
                            //merchant.VisaCheckout = dr["VisaCheckout"].ToString();
                            //merchant.SamsungPay = dr["SamsungPay"].ToString();
                            merchant.NeedAddOSPymt = dr["NeedAddOSPymt"].ToString();
                            merchant.TxnExpired_S = dr["TxnExpired_S"].ToString();
                            merchant.MerchantName = dr["MerchantName"].ToString();
                            merchant.Receipt = dr["Receipt"].ToString();
                            merchant.PymtNotificationEmail = dr["PymtNotificationEmail"].ToString();
                            merchant.PymtNotificationSMS = dr["PymtNotificationSMS"].ToString();
                            //merchant.FXRate = dr["FXRate"].ToString();
                            //merchant.ShowMerchantAddr = dr["ShowMerchantAddr"].ToString();
                            //merchant.ShowMerchantLogo = dr["ShowMerchantLogo"].ToString();
                            //merchant.ExtraCSS = dr["ExtraCSS"].ToString();
                            merchant.OTCExpiryHour = dr["OTCExpiryHour"].ToString();
                            //merchant.ReturnCardData = dr["ReturnCardData"].ToString();
                            //merchant.PROMO = dr["PROMO"].ToString();
                            merchant.MCCCode = dr["MCCCode"].ToString();
                            //merchant.AllowExtFDS = dr["AllowExtFDS"].ToString();
                            //merchant.FDSCustomerID = dr["FDSCustomerID"].ToString();
                            //merchant.FDSAuthCode = dr["FDSAuthCode"].ToString();
                            //merchant.Protocol = dr["Protocol"].ToString();
                            //merchant.AcqCountryCode = dr["AcqCountryCode"].ToString();
                            merchant.PostCode = dr["PostCode"].ToString();
                            //merchant.CVVRequire = dr["CVVRequire"].ToString();
                            merchant.MerchantPassword = dr["MerchantPassword"].ToString();

                            results.Add(merchant);
                        }
                    }
                    return results.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                msg = LoggingPaymentProvider + "[GetMerchant]" + DateTime.Now + ": [Configuration Exception: " + ex.ToString() + "]";
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                    msg);

                return null;
            }
        }

        public OBResTxn GetResTxn(string gatewayTxnID)
        {
            Database.DB database = new Database.DB();
            var connStr = database.connstring(OBconnStrName);
           
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(connStr.StrConn))
                {
                    List<OBResTxn> results = new List<OBResTxn>();

                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = Database.OB.StoredProcedures.spGetResTxn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = sqlConnection;

                        cmd.Parameters.Add("@sz_GatewayTxnID", SqlDbType.VarChar, 30).Value = gatewayTxnID;

                        sqlConnection.Open();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);

                        foreach (DataRow dr in dt.Rows)
                        {
                            OBResTxn resTxn = new OBResTxn();

                            //resTxn.TxnType = dr["TxnType"].ToString();
                            resTxn.MerchantID = dr["MerchantID"].ToString();
                            resTxn.MerchantTxnID = dr["MerchantTxnID"].ToString();
                            resTxn.MerchantName = dr["MerchantName"].ToString();
                            resTxn.OrderNumber = dr["OrderNumber"].ToString();
                            resTxn.TxnAmount = dr["TxnAmount"].ToString();
                            resTxn.BaseTxnAmount = dr["BaseTxnAmount"].ToString();
                            resTxn.CurrencyCode = dr["CurrencyCode"].ToString();
                            resTxn.BaseCurrencyCode = dr["BaseCurrencyCode"].ToString();
                            resTxn.LanguageCode = dr["LanguageCode"].ToString();
                            resTxn.IssuingBank = dr["IssuingBank"].ToString();
                            resTxn.SessionID = dr["SessionID"].ToString();
                            resTxn.Param1 = dr["Param1"].ToString();
                            resTxn.Param2 = dr["Param2"].ToString();
                            resTxn.Param3 = dr["Param3"].ToString();
                            resTxn.Param4 = dr["Param4"].ToString();
                            resTxn.Param5 = dr["Param5"].ToString();
                            resTxn.Param6 = dr["Param6"].ToString();
                            resTxn.Param7 = dr["Param7"].ToString();
                            resTxn.GatewayTxnID = dr["GatewayTxnID"].ToString();
                            resTxn.DateCreated = dr["DateCreated"].ToString();
                            resTxn.OrderDesc = dr["OrderDesc"].ToString();
                            resTxn.TxnState = dr["TxnState"].ToString();
                            resTxn.TxnStatus = (int)(dr["TxnStatus"]);
                            resTxn.MerchantTxnStatus = dr["MerchantTxnStatus"].ToString();
                            resTxn.Action = dr["Action"].ToString();
                            resTxn.Duration = dr["Duration"].ToString();
                            resTxn.OSRet = dr["OSRet"].ToString();
                            resTxn.ErrSet = dr["ErrSet"].ToString();
                            resTxn.ErrNum = dr["ErrNum"].ToString();
                            resTxn.BankRespCode = dr["BankRespCode"].ToString();
                            resTxn.BankRefNo = dr["BankRefNo"].ToString();
                            resTxn.HashMethod = dr["HashMethod"].ToString();
                            resTxn.HashValue = dr["HashValue"].ToString();
                            resTxn.GatewayID = dr["GatewayID"].ToString();
                            resTxn.CardPAN = dr["CardPAN"].ToString();
                            resTxn.MachineID = dr["MachineID"].ToString();
                            resTxn.MerchantReturnURL = dr["MerchantReturnURL"].ToString();
                            resTxn.MerchantSupportURL = dr["MerchantSupportURL"].ToString();
                            resTxn.MerchantApprovalURL = dr["MerchantApprovalURL"].ToString();
                            resTxn.MerchantUnApprovalURL = dr["MerchantUnApprovalURL"].ToString();
                            resTxn.MerchantCallbackURL = dr["MerchantCallbackURL"].ToString();
                            resTxn.OSPymtCode = dr["OSPymtCode"].ToString();
                            resTxn.HostID = dr["HostID"].ToString();
                            resTxn.CustEmail = dr["CustEmail"].ToString();
                            resTxn.CustName = dr["CustName"].ToString();
                            //resTxn.FXCurrencyCode = dr["FXCurrencyCode"].ToString();
                            //resTxn.FXTxnAmt = dr["FXTxnAmt"].ToString();
                            resTxn.TokenType = dr["TokenType"].ToString();
                            resTxn.RetCode = dr["RetCode"].ToString();

                            results.Add(resTxn);
                        }
                    }
                    return results.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                msg = LoggingPaymentProvider + "[GetResTxn]" + DateTime.Now + ": [Configuration Exception: " + ex.ToString() + "]";
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                    msg);

                return null;
            }
        }

        public NotifyStatus CheckCallBackStatus(string gatewayTxnID)
        {
            Database.DB database = new Database.DB();
            var connStr = database.connstring(OBconnStrName);

            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(connStr.StrConn))
                {
                    List<NotifyStatus> results = new List<NotifyStatus>();

                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = Database.OB.StoredProcedures.spCheckNotifyStatus;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = sqlConnection;

                        cmd.Parameters.Add("@sz_GatewayTxnID", SqlDbType.VarChar, 30).Value = gatewayTxnID;

                        sqlConnection.Open();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);

                        foreach (DataRow dr in dt.Rows)
                        {
                            NotifyStatus notifyStatus = new NotifyStatus();

                            notifyStatus.ShopperEmailNotifyStatus = dr["ShopperEmailNotifyStatus"].ToString();
                            notifyStatus.MerchantEmailNotifyStatus = dr["MerchantEmailNotifyStatus"].ToString();
                            notifyStatus.CallBackStatus = dr["CallBackStatus"].ToString();

                            results.Add(notifyStatus);
                        }
                    }
                    return results.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                msg = LoggingPaymentProvider + "[CheckCallBackStatus]" + DateTime.Now + ": [Configuration Exception: " + ex.ToString() + "]";
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                    msg);

                return null;
            }
        }

        public OBReqTxn GetReqTxn(string gatewayTxnID)
        {
            Database.DB database = new Database.DB();
            var connStr = database.connstring(OBconnStrName);
             

            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(connStr.StrConn))
                {
                    List<OBReqTxn> results = new List<OBReqTxn>();

                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = Database.OB.StoredProcedures.spGetReqTxn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = sqlConnection;

                        cmd.Parameters.Add("@sz_GatewayTxnID", SqlDbType.VarChar, 30).Value = gatewayTxnID;

                        sqlConnection.Open();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);

                        foreach (DataRow dr in dt.Rows)
                        {
                            OBReqTxn reqTxn = new OBReqTxn();

                            reqTxn.RetCode = dr["RetCode"].ToString();
                            reqTxn.RetDesc = dr["RetDesc"].ToString();
                            reqTxn.MerchantID = dr["MerchantID"].ToString();
                            reqTxn.MerchantTxnID = dr["MerchantTxnID"].ToString();
                            reqTxn.TxnAmount = dr["TxnAmount"].ToString();
                            reqTxn.CurrencyCode = dr["CurrencyCode"].ToString();
                            reqTxn.IssuingBank = dr["IssuingBank"].ToString();
                            reqTxn.HostID = dr["HostID"].ToString();
                            reqTxn.DateCreated = dr["DateCreated"].ToString();
                            reqTxn.LanguageCode = dr["LanguageCode"].ToString();
                            reqTxn.TxnStatus = dr["TxnStatus"].ToString();
                            reqTxn.TxnState = dr["TxnState"].ToString();
                            reqTxn.GatewayID = dr["GatewayID"].ToString();
                            reqTxn.MachineID = dr["MachineID"].ToString();
                            reqTxn.CardPAN = dr["CardPAN"].ToString();
                            reqTxn.OrderNumber = dr["OrderNumber"].ToString();
                            reqTxn.SessionID = dr["SessionID"].ToString();
                            reqTxn.MerchantReturnURL = dr["MerchantReturnURL"].ToString();
                            reqTxn.MerchantSupportURL = dr["MerchantSupportURL"].ToString();
                            reqTxn.MerchantApprovalURL = dr["MerchantApprovalURL"].ToString();
                            reqTxn.MerchantCallbackURL = dr["MerchantCallbackURL"].ToString();
                            reqTxn.Param1 = dr["Param1"].ToString();
                            reqTxn.Param2 = dr["Param2"].ToString();
                            reqTxn.MaskedCardPAN = dr["MaskedCardPAN"].ToString();
                            reqTxn.Param3 = dr["Param3"].ToString();
                            reqTxn.Param4 = dr["Param4"].ToString();
                            reqTxn.Param5 = dr["Param5"].ToString();
                            reqTxn.HashMethod = dr["HashMethod"].ToString();
                            reqTxn.Param6 = dr["Param6"].ToString();
                            reqTxn.Param7 = dr["Param7"].ToString();
                            reqTxn.BaseCurrencyCode = dr["BaseCurrencyCode"].ToString();
                            reqTxn.BaseTxnAmount = dr["BaseTxnAmount"].ToString();
                            reqTxn.OrderDesc = dr["OrderDesc"].ToString();
                            reqTxn.CustEmail = dr["CustEmail"].ToString();
                            reqTxn.CustName = dr["CustName"].ToString();
                            reqTxn.TokenType = dr["TokenType"].ToString();                    
                            reqTxn.HashMethod = dr["HashMethod"].ToString();                           
                            results.Add(reqTxn);
                        }
                    }
                    return results.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                msg = LoggingPaymentProvider + "[GetReqTxn]" + DateTime.Now + ": [Configuration Exception: " + ex.ToString() + "]";
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                    msg);

                return null;
            }
        }

        public ReqTxn GetReqTxnByMerchantTxnId(string merchantTxnId)
        {
            Database.DB database = new Database.DB();
            var connStr = database.connstring(OBconnStrName);

            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(connStr.StrConn))
                {
                    List<ReqTxn> results = new List<ReqTxn>();

                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = Database.OB.StoredProcedures.spGetReqTxn_By_MerchantTxnID;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = sqlConnection;

                        cmd.Parameters.Add("@szMerchantTxnID", SqlDbType.VarChar, 30).Value = merchantTxnId;

                        sqlConnection.Open();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);

                        foreach (DataRow dr in dt.Rows)
                        {
                            ReqTxn reqTxn = new ReqTxn();

                            reqTxn.RetCode = dr["RetCode"].ToString();
                            reqTxn.RetDesc = dr["RetDesc"].ToString();
                            reqTxn.MerchantID = dr["MerchantID"].ToString();
                            reqTxn.MerchantTxnID = dr["MerchantTxnID"].ToString();
                            reqTxn.TxnAmount = dr["TxnAmount"].ToString();
                            reqTxn.CurrencyCode = dr["CurrencyCode"].ToString();
                            reqTxn.IssuingBank = dr["IssuingBank"].ToString();
                            reqTxn.HostID = dr["HostID"].ToString();
                            reqTxn.DateCreated = dr["DateCreated"].ToString();
                            reqTxn.LanguageCode = dr["LanguageCode"].ToString();
                            reqTxn.TxnStatus = dr["TxnStatus"].ToString();
                            reqTxn.TxnState = dr["TxnState"].ToString();
                            reqTxn.GatewayID = dr["GatewayID"].ToString();
                            reqTxn.MachineID = dr["MachineID"].ToString();
                            reqTxn.CardPAN = dr["CardPAN"].ToString();
                            reqTxn.OrderNumber = dr["OrderNumber"].ToString();
                            reqTxn.SessionID = dr["SessionID"].ToString();
                            reqTxn.MerchantReturnURL = dr["MerchantReturnURL"].ToString();
                            reqTxn.MerchantSupportURL = dr["MerchantSupportURL"].ToString();
                            reqTxn.MerchantApprovalURL = dr["MerchantApprovalURL"].ToString();
                            reqTxn.MerchantCallbackURL = dr["MerchantCallbackURL"].ToString();
                            reqTxn.Param1 = dr["Param1"].ToString();
                            reqTxn.Param2 = dr["Param2"].ToString();
                            reqTxn.MaskedCardPAN = dr["MaskedCardPAN"].ToString();
                            reqTxn.Param3 = dr["Param3"].ToString();
                            reqTxn.Param4 = dr["Param4"].ToString();
                            reqTxn.Param5 = dr["Param5"].ToString();
                            reqTxn.HashMethod = dr["HashMethod"].ToString();
                            reqTxn.Param6 = dr["Param6"].ToString();
                            reqTxn.Param7 = dr["Param7"].ToString();
                            reqTxn.Param8 = dr["Param8"].ToString();
                            reqTxn.Param9 = dr["Param9"].ToString();
                            reqTxn.Param10 = dr["Param10"].ToString();
                            reqTxn.Param11 = dr["Param11"].ToString();
                            reqTxn.Param12 = dr["Param12"].ToString();
                            reqTxn.Param13 = dr["Param13"].ToString();
                            reqTxn.Param14 = dr["Param14"].ToString();
                            reqTxn.Param15 = dr["Param15"].ToString();
                            reqTxn.BaseCurrencyCode = dr["BaseCurrencyCode"].ToString();
                            reqTxn.BaseTxnAmount = dr["BaseTxnAmount"].ToString();
                            reqTxn.OrderDesc = dr["OrderDesc"].ToString();
                            reqTxn.CardHolder = dr["CardHolder"].ToString();
                            reqTxn.CustEmail = dr["CustEmail"].ToString();
                            reqTxn.CustName = dr["CustName"].ToString();
                            reqTxn.CustOCP = dr["CustOCP"].ToString();
                            reqTxn.TokenType = dr["TokenType"].ToString();
                            reqTxn.PromoCode = dr["PromoCode"].ToString();
                            reqTxn.PromoOriAmt = dr["PromoOriAmt"].ToString();
                            reqTxn.CardType = dr["CardType"].ToString();
                            reqTxn.CardHolder = dr["CardHolder"].ToString();
                            reqTxn.CardHolder = dr["CardHolder"].ToString();
                            reqTxn.CardHolder = dr["CardHolder"].ToString();
                            reqTxn.HashMethod = dr["HashMethod"].ToString();
                            reqTxn.CustPhone = dr["CustPhone"].ToString();
                            results.Add(reqTxn);
                        }
                    }
                    return results.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                msg = LoggingPaymentProvider + "[GetReqTxnByMerchantTxnId]" + DateTime.Now + ": [Configuration Exception: " + ex.ToString() + "]";
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                    msg);

                return null;
            }
        }

        public Host GetHost(string hostName)
        {
            Database.DB database = new Database.DB();
            var connStr = database.connstring(OBconnStrName);
             

            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(connStr.StrConn))
                {
                    List<Host> results = new List<Host>();

                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = Database.OB.StoredProcedures.spGetHostInfo;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = sqlConnection;

                        cmd.Parameters.Add("@sz_HostName", SqlDbType.VarChar, 20).Value = hostName;

                        sqlConnection.Open();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);

                        foreach (DataRow dr in dt.Rows)
                        {
                            Host host = new Host();

                            host.AllowQuery = dr["AllowQuery"].ToString();
                            host.AllowReversal = dr["AllowReversal"].ToString();
                            host.CfgFile = dr["CfgFile"].ToString();
                            host.ChannelTimeout = dr["ChannelTimeout"].ToString();
                            host.Desc = dr["Desc"].ToString();
                            //host.FixedCurrency = dr["FixedCurrency"].ToString();
                            host.GatewayTxnIDFormat = dr["GatewayTxnIDFormat"].ToString();
                            host.HashMethod = dr["HashMethod"].ToString();
                            host.HostID = dr["HostID"].ToString();
                            host.HostReplyMethod = dr["HostReplyMethod"].ToString();
                            host.InitVector = dr["InitVector"].ToString();
                            host.IsActive = dr["IsActive"].ToString();
                            //host.LogoPath = dr["LogoPath"].ToString();
                            host.LogRes = dr["LogRes"].ToString();
                            host.MesgTemplate = dr["MesgTemplate"].ToString();
                            //host.MID = dr["MID"].ToString();
                            host.NeedRedirectOTP = dr["NeedRedirectOTP"].ToString();
                            host.NeedReplyAcknowledgement = dr["NeedReplyAcknowledgement"].ToString();
                            host.OSPymtCode = dr["OSPymtCode"].ToString();
                            //host.OTCGenMethod = dr["OTCGenMethod"].ToString();
                            //host.OTCRevTimeout = dr["OTCRevTimeout"].ToString();
                            host.PaymentTemplate = dr["PaymentTemplate"].ToString();
                            host.Protocol = dr["Protocol"].ToString();
                            host.QueryFlag = dr["QueryFlag"].ToString();
                            host.QueryURL = dr["QueryURL"].ToString();
                            host.Require2ndEntry = dr["Require2ndEntry"].ToString();
                            host.ReturnIPAddresses = dr["ReturnIPAddresses"].ToString();
                            host.ReturnURL = dr["ReturnURL"].ToString();
                            host.SecondEntryTemplate = dr["SecondEntryTemplate"].ToString();
                            host.SecretKey = dr["SecretKey"].ToString();
                            host.Timeout = dr["Timeout"].ToString();
                            host.TxnStatusActionID = dr["TxnStatusActionID"].ToString();

                            results.Add(host);
                        }
                    }
                    return results.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                msg = LoggingPaymentProvider + "[GetHost]" + DateTime.Now + ": [Configuration Exception: " + ex.ToString() + "]";
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                    msg);

                return null;
            }
        }

        public CCMerchantCredential GetCCMerchantCredential(string paydibsMID)
        {
            Database.DB database = new Database.DB();
            var connStr = database.connstring(PGAdminconnStrName);

            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(connStr.StrConn))
                {
                    List<CCMerchantCredential> results = new List<CCMerchantCredential>();

                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = Database.PGAdmin.StoredProcedures.spGettblCCMerchantCredential_By_PaydibsMID;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = sqlConnection;

                        cmd.Parameters.Add("@PaydibsMID", SqlDbType.VarChar, 30).Value = paydibsMID;

                        sqlConnection.Open();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);

                        foreach (DataRow dr in dt.Rows)
                        {
                            CCMerchantCredential CCMerchantCredential = new CCMerchantCredential();

                            CCMerchantCredential.HostID = dr["HostID"].ToString();
                            CCMerchantCredential.MerchantID = dr["MerchantID"].ToString();
                            CCMerchantCredential.TerminalID = dr["TerminalID"].ToString();
                            CCMerchantCredential.HASHKEY = dr["HASHKEY"].ToString();

                            results.Add(CCMerchantCredential);
                        }
                    }
                    return results.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                msg = LoggingPaymentProvider + "[GetCCMerchantCredential]" + DateTime.Now + ": [Configuration Exception: " + ex.ToString() + "]";
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                    msg);

                return null;
            }
        }

        internal int UpdatePayTxnRef(ReqTxn reqTxn, string txnType)
        {
            Database.DB database = new Database.DB();
            var connStr = database.connstring(OBconnStrName);

            using (SqlConnection sqlConnection = new SqlConnection(connStr.StrConn))
            {
                int affectedRecord = 0;
                List<ReqTxn> details = new List<ReqTxn>();

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = Database.OB.StoredProcedures.spUpdatePayTxnRef;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = sqlConnection;

                    cmd.Parameters.Add("@i_TxnStatus", SqlDbType.Int).Value = reqTxn.TxnStatus;
                    cmd.Parameters.Add("@i_HostID", SqlDbType.Int).Value = reqTxn.HostID;
                    cmd.Parameters.Add("@sz_IssuingBank", SqlDbType.VarChar, 30).Value = reqTxn.IssuingBank;
                    cmd.Parameters.Add("@i_PKID", SqlDbType.Int, 30).Value = reqTxn.GatewayID;//gatewaytxnid
                    cmd.Parameters.Add("@sz_GatewayTxnID", SqlDbType.VarChar, 30).Value = reqTxn.GatewayTxnID;
                    cmd.Parameters.Add("@sz_MerchantTxnID", SqlDbType.VarChar, 30).Value = reqTxn.MerchantTxnID;
                    cmd.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30).Value = reqTxn.MerchantID;
                    cmd.Parameters.Add("@sz_PymtMethod", SqlDbType.VarChar, 3).Value = txnType;

                    sqlConnection.Open();

                    affectedRecord = cmd.ExecuteNonQuery();

                    return affectedRecord;
                }
            }
        }

        internal int InsertTxnResp(OBReqTxn reqTxn, string txnType)
        {
            Database.DB database = new Database.DB();
            var connStr = database.connstring(OBconnStrName);
            Logger logger = new Logger();
            CLoggerII objLoggerII = new CLoggerII();
            objLoggerII = logger.InitLogger();
           
            using (SqlConnection sqlConnection = new SqlConnection(connStr.StrConn))
            {
                int affectedRecord = 0;
                List<OBReqTxn> details = new List<OBReqTxn>();

                using (SqlCommand cmd = new SqlCommand())
                {
                    try
                    {
                        cmd.CommandText = Database.OB.StoredProcedures.spInsTxnResp;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = sqlConnection;
                       
                        cmd.Parameters.Add("@sz_TxnType", SqlDbType.VarChar, 7).Value = txnType;

                        cmd.Parameters.Add("@sz_GatewayTxnID", SqlDbType.VarChar, 30).Value = reqTxn.GatewayTxnID;
                        cmd.Parameters.Add("@sz_MerchantID", SqlDbType.VarChar, 30).Value = reqTxn.MerchantID;
                        cmd.Parameters.Add("@sz_MerchantTxnID", SqlDbType.VarChar, 30).Value = reqTxn.MerchantTxnID;

                        cmd.Parameters.Add("@sz_TxnAmount", SqlDbType.VarChar, 18).Value = reqTxn.TxnAmount;
                        cmd.Parameters.Add("@d_TxnAmount", SqlDbType.Decimal, 18).Value = Convert.ToDecimal(reqTxn.TxnAmount);
                        cmd.Parameters.Add("@sz_CurrencyCode", SqlDbType.VarChar, 3).Value = reqTxn.CurrencyCode;
                        cmd.Parameters.Add("@sz_GatewayID", SqlDbType.VarChar, 4).Value = reqTxn.GatewayID;
                        cmd.Parameters.Add("@sz_CardPAN", SqlDbType.VarChar, 20).Value = reqTxn.CardPAN; //Credit card number
                        cmd.Parameters.Add("@i_HostID", SqlDbType.Int).Value = Int32.Parse(reqTxn.HostID);
                        cmd.Parameters.Add("@sz_IssuingBank", SqlDbType.VarChar, 20).Value = reqTxn.IssuingBank;
                        cmd.Parameters.Add("@sz_RespMesg", SqlDbType.VarChar, 255).Value = reqTxn.TxnMsg; //TODO:
                        cmd.Parameters.Add("@i_TxnState", SqlDbType.Int).Value = reqTxn.TxnState;
                        cmd.Parameters.Add("@i_MerchantTxnStatus", SqlDbType.VarChar, 30).Value = reqTxn.TxnStatus; //TODO:
                        cmd.Parameters.Add("@i_TxnStatus", SqlDbType.Int).Value = Int32.Parse(reqTxn.TxnStatus);
                        cmd.Parameters.Add("@sz_BankRespCode", SqlDbType.VarChar, 20).Value = reqTxn.IssuingBank; //TODO:
                        cmd.Parameters.Add("@sz_BankRefNo", SqlDbType.VarChar, 40).Value = reqTxn.BankRefNo; //TODO: bank ref no
                        cmd.Parameters.Add("@sz_HashMethod", SqlDbType.VarChar, 6).Value = reqTxn.HashMethod; //TODO:
                        cmd.Parameters.Add("@sz_HashValue", SqlDbType.VarChar, 200).Value = ""; //TODO:
                        cmd.Parameters.Add("@sz_OrderNumber", SqlDbType.VarChar, 20).Value = reqTxn.OrderNumber;
                        cmd.Parameters.Add("@sz_ErrDesc", SqlDbType.VarChar, 200).Value = "";//TODO:
                        cmd.Parameters.Add("@sz_Param1", SqlDbType.VarChar, 100).Value = reqTxn.Param1;
                        cmd.Parameters.Add("@sz_Param2", SqlDbType.VarChar, 100).Value = reqTxn.Param2;
                        cmd.Parameters.Add("@sz_Param3", SqlDbType.VarChar, 100).Value = reqTxn.Param3;
                        cmd.Parameters.Add("@sz_Param4", SqlDbType.VarChar, 100).Value = reqTxn.Param4;
                        cmd.Parameters.Add("@sz_Param5", SqlDbType.VarChar, 100).Value = reqTxn.Param5;
                        cmd.Parameters.Add("@sz_Param6", SqlDbType.VarChar, 50).Value = reqTxn.Param6;
                        cmd.Parameters.Add("@sz_Param7", SqlDbType.VarChar, 50).Value = reqTxn.Param7;
                        cmd.Parameters.Add("@sz_MachineID", SqlDbType.VarChar, 10).Value = reqTxn.MachineID;
                        cmd.Parameters.Add("@i_OSRet", SqlDbType.Int).Value = Int32.Parse(reqTxn.RetCode);
                        cmd.Parameters.Add("@i_ErrSet", SqlDbType.Int).Value = -1;//TODO:
                        cmd.Parameters.Add("@i_ErrNum", SqlDbType.Int).Value = -1;//TODO:
                        cmd.Parameters.Add("@retValue", System.Data.SqlDbType.Int).Direction = System.Data.ParameterDirection.ReturnValue;
                        sqlConnection.Open();

                        affectedRecord = cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex){
                        
                        objLoggerII = logger.InitLogger();
                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                ex.Message);
                    }

                    return affectedRecord;
                }
            }
        }

        internal int InsertCallBackStatus(OBReqTxn reqTxn, int callBackStatus)
        {
            Database.DB database = new Database.DB();
            var connStr = database.connstring(OBconnStrName);

            using (SqlConnection sqlConnection = new SqlConnection(connStr.StrConn))
            {
                int affectedRecord = 0;
                List<ReqTxn> details = new List<ReqTxn>();

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = Database.OB.StoredProcedures.spInsNotifyStatus;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = sqlConnection;

                    cmd.Parameters.Add("@sz_GatewayTxnID", SqlDbType.VarChar, 30).Value = reqTxn.GatewayTxnID;
                    cmd.Parameters.Add("@i_CallBackStatus", SqlDbType.VarChar, 30).Value = callBackStatus;

                    sqlConnection.Open();

                    affectedRecord = cmd.ExecuteNonQuery();

                    return affectedRecord;
                }
            }
        }
    }
}
