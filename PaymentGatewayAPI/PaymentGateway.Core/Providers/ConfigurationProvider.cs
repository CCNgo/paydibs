﻿using LoggerII;
using PaymentGatewayAPI.Core.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewayAPI.Core.Providers
{
    internal class ConfigurationProvider
    {
        private string connStrName = "PGAdmin_conn";

        public SystemConfiguration GetConfig(string configName)
        {
            Logger logger = new Logger();
            CLoggerII objLoggerII = new CLoggerII();
            objLoggerII = logger.InitLogger();

            Database.DB database = new Database.DB();
            var connStr = database.connstring(connStrName);

            //string msg = "[Paydibs PaymentGatewayAPI PurchaseTNGService.CommitPurchase]" + DateTime.Now + ": [Conncection String: " + connStr.StrConn + "]";
            //objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
            //    msg);

            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(connStr.StrConn))
                {
                    List<SystemConfiguration> systemConfig = new List<SystemConfiguration>();

                    var config = new SystemConfiguration();

                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = Database.PGAdmin.StoredProcedures.GetConfigDetails;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = sqlConnection;

                        cmd.Parameters.Add("@ConfigName", SqlDbType.VarChar, 30).Value = configName;

                        sqlConnection.Open();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);

                        foreach (DataRow dr in dt.Rows)
                        {

                            switch (dr["KeyName"].ToString())
                            {
                                case SystemConfiguration.TNG_clientIdKeyName:
                                    config.TNG_clientId = (dr["KeyValue"]).ToString();
                                    break;
                                case SystemConfiguration.TNG_clientSecretKeyName:
                                    config.TNG_clientSecret = (dr["KeyValue"]).ToString();
                                    break;
                                case SystemConfiguration.TNG_merchantIdKeyName:
                                    config.TNG_merchantId = (dr["KeyValue"]).ToString();
                                    break;
                                case SystemConfiguration.TNG_POSTURLKeyName:
                                    config.TNG_POSTURL = (dr["KeyValue"]).ToString();
                                    break;
                                case SystemConfiguration.TNG_productCodeKeyName:
                                    config.TNG_productCode = (dr["KeyValue"]).ToString();
                                    break;
                                case SystemConfiguration.TNG_URLNOTIFICATIONKeyName:
                                    config.TNG_URLNOTIFICATION = (dr["KeyValue"]).ToString();
                                    break;
                                case SystemConfiguration.TNG_URLPAY_RETURNKeyName:
                                    config.TNG_URLPAY_RETURN = (dr["KeyValue"]).ToString();
                                    break;
                                case SystemConfiguration.TNG_privatekeyKeyName:
                                    config.TNG_privatekey = (dr["KeyValue"]).ToString();
                                    break;
                                case SystemConfiguration.TNG_versionKeyName:
                                    config.TNG_version = (dr["KeyValue"]).ToString();
                                    break;
                                case SystemConfiguration.TNG_timeoutPeriod:
                                    config.TNG_timeout = (dr["KeyValue"]).ToString();
                                    break;
                                case SystemConfiguration.TNG_QueryUrlKeyName:
                                    config.TNG_QueryUrl = (dr["KeyValue"]).ToString();
                                    break;
                                case SystemConfiguration.TNG_RefundUrlKeyName:
                                    config.TNG_RefundUrl = (dr["KeyValue"]).ToString();
                                    break;
                                //case SystemConfiguration.PayMaster_MIDKeyName:
                                //    config.PayMaster_MID = (dr["KeyValue"]).ToString();
                                //    break;
                                case SystemConfiguration.PayMaster_RURL_UPPPCKeyName:
                                    config.PayMaster_RURL_UPPPC = (dr["KeyValue"]).ToString();
                                    break;
                                case SystemConfiguration.PayMaster_RURL_UPPPSKeyName:
                                    config.PayMaster_RURL_UPPPS = (dr["KeyValue"]).ToString();
                                    break;
                                case SystemConfiguration.PayMaster_RURL_UPPPUKeyName:
                                    config.PayMaster_RURL_UPPPU = (dr["KeyValue"]).ToString();
                                    break;
                                //case SystemConfiguration.PayMaster_TIDKeyName:
                                //    config.PayMaster_TID = (dr["KeyValue"]).ToString();
                                //    break;
                                case SystemConfiguration.PayMaster_VERSIONKeyName:
                                    config.PayMaster_VERSION = (dr["KeyValue"]).ToString();
                                    break;
                                ////case SystemConfiguration.PayMaster_HASHKEYKeyName:
                                ////    config.PayMaster_HASHKEY = (dr["KeyValue"]).ToString();
                                ////    break;
                                case SystemConfiguration.PayMaster_POSTURLKeyName:
                                    config.PayMaster_POSTURL = (dr["KeyValue"]).ToString();
                                    break;
                                case SystemConfiguration.PayMaster_ServIDKeyName:
                                    config.PayMaster_ServID = (dr["KeyValue"]).ToString();
                                    break;
                                case SystemConfiguration.PayMaster_POSTURL0200KeyName:
                                    config.PayMaster_POSTURL0200 = (dr["KeyValue"]).ToString();
                                    break;
                                case SystemConfiguration.PayMaster_PayMaster_AES256KeyName:
                                    config.PayMaster_AES256Key = (dr["KeyValue"]).ToString();
                                    break;
                            }
                        }
                    }
                    return config;
                }
            }
            catch(Exception ex)
            {
                string msg = "[Paydibs PaymentGatewayAPI ConfigurationProvider.GetConfig]" + DateTime.Now + ": [Configuration Exception: " + ex.ToString() + "]";
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                    msg);

                return null;
            }
        }
    }
}
