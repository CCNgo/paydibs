﻿using LoggerII;
using Newtonsoft.Json;
using PaymentGatewayAPI.Core.Configuration;
using PaymentGatewayAPI.Core.Models;
using PaymentGatewayAPI.Core.Providers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewayAPI.Core.Utilities
{
    public class VGS
    {
        private const string PaymentGatewayConfigurations = "Paydibs.PaymentGatewayAPI";
      
        public static String InboundConvert(string CardPAN)
        {
            SystemConfiguration Config = null;
            Logger logger = new Logger();
            CLoggerII objLoggerII = new CLoggerII();
            string msg = string.Empty;
            objLoggerII = logger.InitLogger();
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                   "inbound enter");
            VGSInbound inBoundResponse = new VGSInbound();
            var configProv = new ConfigurationProvider();
            Config = configProv.GetConfig(PaymentGatewayConfigurations);
            try
            {
                using (var httpClient = new HttpClient())
                {
                    using (var request = new HttpRequestMessage(new HttpMethod("POST"), ConfigurationManager.AppSettings["VGSInboundRoute"]))
                    {
                        request.Content = new StringContent("{\"card_number\": \"" + CardPAN + "\"}");
                        request.Content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");
                        var responseString = "";
                        var response = httpClient.SendAsync(request);
                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                   "checkpoit");
                        responseString = response.Result.Content.ReadAsStringAsync().Result;

                        inBoundResponse = JsonConvert.DeserializeObject<VGSInbound>(responseString);
                        //return Aes256CbcEncrypter.Encrypt(inBoundResponse.json.cardNumber, Config.PayMaster_AES256Key);
                        byte[] bytesToBeEncrypted = Encoding.UTF8.GetBytes(inBoundResponse.json.cardNumber);
                        byte[] passwordBytes = Encoding.UTF8.GetBytes(Config.PayMaster_AES256Key);

                        // Hash the password with SHA256
                        passwordBytes = SHA256.Create().ComputeHash(passwordBytes);
                        var encryptedByte = EncryptString(inBoundResponse.json.cardNumber, Config.PayMaster_AES256Key);
                        return encryptedByte;
                    }
                }
            }
            catch (Exception ex)
            {
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                          "error inbound: " + ex.Message);
            }
            return "";
        }

        public static String OutboundConvert(string encryptedToken)
        {
            SystemConfiguration Config = null;
            Logger logger = new Logger();
            CLoggerII objLoggerII = new CLoggerII();
            string msg = string.Empty;
            objLoggerII = logger.InitLogger();
            var configProv = new ConfigurationProvider();
            Config = configProv.GetConfig(PaymentGatewayConfigurations);
            //string token = Aes256CbcEncrypter.Decrypt(encryptedToken, Config.PayMaster_AES256Key);

            byte[] bytesToBeDecrypted = Convert.FromBase64String(encryptedToken);
            byte[] passwordBytes = Encoding.UTF8.GetBytes(Config.PayMaster_AES256Key);
            passwordBytes = SHA256.Create().ComputeHash(passwordBytes);

            //byte[] bytesDecrypted = AES_Decrypt(bytesToBeDecrypted, passwordBytes);

            //string token = Encoding.UTF8.GetString(bytesDecrypted);

            string token = DecryptString(encryptedToken, Config.PayMaster_AES256Key);
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                      "outbound token: " + token);
            string aliasData = "{\"card_number\":\"" + token + "\"}";
            //string aliasData = "{\"card_number\":\"tok_sandbox_tvmv2AcKNVQ9NtDFAChfyf\"}";
                      
            NetworkCredential credentials = new NetworkCredential(ConfigurationManager.AppSettings["VGSProxyName"], ConfigurationManager.AppSettings["VGSProxyPass"]);
            //NetworkCredential credentials = new NetworkCredential("US2ergWeupKZM7gj32snEVUM", "3962c9b1-7cbc-42e3-84ca-b56bbc7cb0ce");
            WebProxy proxy = new WebProxy(ConfigurationManager.AppSettings["VGSInboundRouteForOutbound"], false)
            {
                UseDefaultCredentials = false,
                Credentials = credentials,
            };
            //WebProxy proxy = new WebProxy("http://tntcdm4iz0p.sandbox.verygoodproxy.com:8080", false)
            //{
            //    UseDefaultCredentials = false,
            //    Credentials = credentials,
            //};
            var handler = new HttpClientHandler()
            {
                Proxy = proxy,
                PreAuthenticate = true,
                UseDefaultCredentials = false,
            };

            try
            {
                System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                HttpClient httpClient = new HttpClient(handler);
                StringContent content = new StringContent(aliasData, Encoding.UTF8, "application/json");
                var respond = httpClient.PostAsync(ConfigurationManager.AppSettings["VGSUpstreamHost"], content);
                //var respond = httpClient.PostAsync("https://echo.apps.verygood.systems/post", content);

                HttpResponseMessage response = respond.Result;
                var responseBody = response.Content.ReadAsStringAsync();

                var outBoundResponse = JsonConvert.DeserializeObject<VGSOutbound>(responseBody.Result);

                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                       "outbound resp: " + outBoundResponse.json.cardNumber);

               
                return outBoundResponse.json.cardNumber;
                        
            }
            catch (HttpRequestException e)
            {
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
     "error outbound: " + e.InnerException.Message);
            }           
           
            return "";
              

        }


        public static string EncryptString(string text, string password)
        {
            byte[] baPwd = Encoding.UTF8.GetBytes(password);

            // Hash the password with SHA256
            byte[] baPwdHash = SHA256Managed.Create().ComputeHash(baPwd);

            byte[] baText = Encoding.UTF8.GetBytes(text);

            byte[] baSalt = GetRandomBytes();
            byte[] baEncrypted = new byte[baSalt.Length + baText.Length];

            // Combine Salt + Text
            for (int i = 0; i < baSalt.Length; i++)
                baEncrypted[i] = baSalt[i];
            for (int i = 0; i < baText.Length; i++)
                baEncrypted[i + baSalt.Length] = baText[i];

            baEncrypted = AES_Encrypt(baEncrypted, baPwdHash);

            string result = Convert.ToBase64String(baEncrypted);
            return result;
        }
        public static byte[] AES_Encrypt(byte[] bytesToBeEncrypted, byte[] passwordBytes)
        {
            byte[] encryptedBytes = null;

            // Set your salt here, change it to meet your flavor:
            // The salt bytes must be at least 8 bytes.
            byte[] saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    AES.KeySize = 256;
                    AES.BlockSize = 128;

                    var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);

                    AES.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, AES.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeEncrypted, 0, bytesToBeEncrypted.Length);
                        cs.Close();
                    }
                    encryptedBytes = ms.ToArray();
                }
            }

            return encryptedBytes;
        }

        public static string DecryptString(string text, string password)
        {
            byte[] baPwd = Encoding.UTF8.GetBytes(password);

            // Hash the password with SHA256
            byte[] baPwdHash = SHA256Managed.Create().ComputeHash(baPwd);

            byte[] baText = Convert.FromBase64String(text);

            byte[] baDecrypted = AES_Decrypt(baText, baPwdHash);

            // Remove salt
            int saltLength = GetSaltLength();
            byte[] baResult = new byte[baDecrypted.Length - saltLength];
            for (int i = 0; i < baResult.Length; i++)
                baResult[i] = baDecrypted[i + saltLength];

            string result = Encoding.UTF8.GetString(baResult);
            return result;
        }

        public static byte[] AES_Decrypt(byte[] bytesToBeDecrypted, byte[] passwordBytes)
        {
            byte[] decryptedBytes = null;

            // Set your salt here, change it to meet your flavor:
            // The salt bytes must be at least 8 bytes.
            byte[] saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    AES.KeySize = 256;
                    AES.BlockSize = 128;

                    var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);

                    AES.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, AES.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeDecrypted, 0, bytesToBeDecrypted.Length);
                        cs.Close();
                    }
                    decryptedBytes = ms.ToArray();
                }
            }

            return decryptedBytes;
        }

        public static byte[] GetRandomBytes()
        {
            int saltLength = GetSaltLength();
            byte[] ba = new byte[saltLength];
            RNGCryptoServiceProvider.Create().GetBytes(ba);
            return ba;
        }

        public static int GetSaltLength()
        {
            return 8;
        }
    }
}
