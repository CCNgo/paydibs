﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Net.Mail;
using System.Security;
using System.Net;
using LoggerII;
using PaymentGatewayAPI.Core.Models;
using System.Globalization;

namespace PaymentGatewayAPI.Core.Utilities
{
    public class Mail
    {
        private LoggerII.CLoggerII objLoggerII = null;
        private SmtpClient objSMTPClient = new SmtpClient();
        private MailMessage objMailMessage = new MailMessage();

        public string GetTemplate(Common.EmailTemplate emailTemplate)
        {
            if (emailTemplate == Common.EmailTemplate.EMAILTEMPLATESHOPPER)
            { 
                return ConfigurationManager.AppSettings["EmailBodyTemplate_Shopper"];
            }
            else
            {
                return ConfigurationManager.AppSettings["EmailBodyTemplate_Merchant"];
            }
        }
        //private Hash objHash;

        public int SendMail(string szTo, string szMsg, string szSubject = "")
        {
            int iRet = -1;

            try
            {
                Logger logger = new Logger();
                CLoggerII objLoggerII = new CLoggerII();
                objLoggerII = logger.InitLogger();               
                SMTPConfig();              
                objMailMessage.From = new MailAddress(ConfigurationManager.AppSettings["EmailFromAddr"], ConfigurationManager.AppSettings["EmailFromName"]);
                objMailMessage.To.Clear();
                foreach (var curr_address in szTo.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                {
                    objMailMessage.To.Add(curr_address);
                }
                if (String.IsNullOrEmpty(szSubject))
                {
                    objMailMessage.Subject = ConfigurationManager.AppSettings["EmailSubject"];
                }
                else
                {
                    objMailMessage.Subject = szSubject;
                }
                objMailMessage.IsBodyHtml = true;
                objMailMessage.Body = szMsg;

                objSMTPClient.Send(objMailMessage);               
                iRet = (int)MailStatus.SUCCESS;
            }
            catch(Exception ex)
            {
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "SendMail Error: " + ex.Message);
                iRet = (int)MailStatus.FAIL;
            }

            return iRet;
        }

        private void SMTPConfig()
        {
            SecureString szEmailPwd = new SecureString();

            try
            {
                objSMTPClient.UseDefaultCredentials = false;
                objSMTPClient.Port = Int32.Parse(ConfigurationManager.AppSettings["EmailPort"]);
                objSMTPClient.EnableSsl = true;
                objSMTPClient.Host = ConfigurationManager.AppSettings["EmailServer"];

                szEmailPwd = new NetworkCredential("", "Z3_7GpB*6s").SecurePassword;
                objSMTPClient.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailUserName"], new NetworkCredential("", szEmailPwd).Password);
            }
            catch(Exception ex)
            {
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "SMTPConfig Error: " + ex.Message);
            }
        }

        private enum MailStatus
        {
            SUCCESS = 1,
            FAIL = 0
        }

        public string LoadHtmlContent(ReqTxn reqTxn, ResTxn resTxn, Host host, Merchant merchant, string szHTMLContent)
        {
            string sz_HTML = string.Empty;
            string C_PYMT_METHOD = "CC"; //TODO: differentiate payment channel method

            szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[CUSTNAME]", resTxn.CustName.ToUpper());                             // Modified 18 Feb 2014, added ToUpper
            szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[CURRENCYCODE]", resTxn.CurrencyCode);
            szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[TXNAMOUNT]", resTxn.TxnAmount);
            szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[MERCHANTNAME]", resTxn.MerchantName);
            szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[MERCHANTWEBSITEURL]", merchant.WebSiteURL);                   // Added, 14 Dec 2013
            szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[TXNDATETIME]", System.DateTime.Now.ToString("dd MMM yyyy hh:mm:ss tt", CultureInfo.CreateSpecificCulture("en-us")));  // Added, 9 Dec 2013
            szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[MERCHANTTXNID]", resTxn.MerchantTxnID);
            szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[ORDERNUMBER]", resTxn.OrderNumber);                                // Added, 14 Dec 2013
            szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[ORDERDESC]", resTxn.OrderDesc);
            szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[PYMTMETHOD]", C_PYMT_METHOD);                                              // Added, 9 Dec 2013
            szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[CCDETAILS]", "");                                                // Added, 9 Dec 2013
            szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[GATEWAYTXNID]", resTxn.GatewayTxnID);
            szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[MERCHANTCONTACTNO]", merchant.ContactNo);
            szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[MERCHANTEMAILADDR]", merchant.EmailAddr);
            szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[CARDTYPEURL]", ConfigurationManager.AppSettings["CardTypeURL"]);          // Added, 26 Aug 2015
            szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[MERCHANTID]", resTxn.MerchantID);                                     // Added, 26 Aug 2015
                                                                                                                                             // Added -OTC, 23 Nov 2016, for OTC Receipt
            szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[PYMTDUETIME]", "");
            szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[SECURECODE]", "");//string.Format("{0}{1}", st_HostInfo.szOSPymtCode, st_PayInfo.szOTCSecureCode));   // Added 7 Mar 2017, Artajasa, display Prefix infront secure code
            szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[HOSTNAME]", host.Desc);
            // Added, 17 May 2017, for OTC email reference number
            szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[PYMTCOUNTRY]", merchant.Country);
            szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[ISSUINGBANK]", resTxn.IssuingBank);

            szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[CUSTNAME]", resTxn.CustName.ToUpper());
        
            sz_HTML = szHTMLContent;
            return sz_HTML;
        }

        public string LoadHtmlContent(OBReqTxn reqTxn, OBResTxn resTxn, Host host, Merchant merchant, string szHTMLContent)
        {
            string sz_HTML = string.Empty;
            string C_PYMT_METHOD = "WA"; //TODO: differentiate payment channel method
            Logger logger = new Logger();
            CLoggerII objLoggerII = new CLoggerII();
            objLoggerII = logger.InitLogger();
            try
            {
                szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[CUSTNAME]", resTxn.CustName.ToUpper());                             // Modified 18 Feb 2014, added ToUpper
                szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[CURRENCYCODE]", resTxn.CurrencyCode);
                szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[TXNAMOUNT]", resTxn.TxnAmount);
                szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[MERCHANTNAME]", resTxn.MerchantName);
                szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[MERCHANTWEBSITEURL]", merchant.WebSiteURL);                   // Added, 14 Dec 2013
                szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[TXNDATETIME]", System.DateTime.Now.ToString("dd MMM yyyy hh:mm:ss tt", CultureInfo.CreateSpecificCulture("en-us")));  // Added, 9 Dec 2013
                szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[MERCHANTTXNID]", resTxn.MerchantTxnID);
                szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[ORDERNUMBER]", resTxn.OrderNumber);                                // Added, 14 Dec 2013
                szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[ORDERDESC]", resTxn.OrderDesc);
                szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[PYMTMETHOD]", C_PYMT_METHOD);                                              // Added, 9 Dec 2013
                szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[CCDETAILS]", "");                                                // Added, 9 Dec 2013
                szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[GATEWAYTXNID]", resTxn.GatewayTxnID);
                szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[MERCHANTCONTACTNO]", merchant.ContactNo);
                szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[MERCHANTEMAILADDR]", merchant.EmailAddr);
                szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[CARDTYPEURL]", ConfigurationManager.AppSettings["CardTypeURL"]);          // Added, 26 Aug 2015
                szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[MERCHANTID]", resTxn.MerchantID);                                     // Added, 26 Aug 2015                                                                                                                                         // Added -OTC, 23 Nov 2016, for OTC Receipt
                szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[PYMTDUETIME]", "");
                szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[SECURECODE]", "");//string.Format("{0}{1}", st_HostInfo.szOSPymtCode, st_PayInfo.szOTCSecureCode));   // Added 7 Mar 2017, Artajasa, display Prefix infront secure code
                szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[HOSTNAME]", host.Desc);
                // Added, 17 May 2017, for OTC email reference number
                szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[PYMTCOUNTRY]", merchant.Country);
                szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[ISSUINGBANK]", resTxn.IssuingBank);
                szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[CUSTNAME]", resTxn.CustName.ToUpper());
                sz_HTML = szHTMLContent;
            }
            catch (Exception ex)
            {

                objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
        ex.Message);
            }

            return sz_HTML;
        }

        public string szFormatPlaceHolder(string szOriText, string szTextToFind, string szTextToReplace)
        {
            if (szOriText.Contains(szTextToFind))
            {
                if ((szTextToReplace.Trim().Length > 0))
                {
                    szOriText = szOriText.Replace(szTextToFind, szTextToReplace);
                    return szOriText;
                }
                else
                {
                    szOriText = szOriText.Replace(szTextToFind, "");
                    return szOriText;
                }
            }
            return szOriText;
        }


    }
}
