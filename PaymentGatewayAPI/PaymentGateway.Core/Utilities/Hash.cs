﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using LoggerII;
using PaymentGatewayAPI.Core.Models;

namespace PaymentGatewayAPI.Core.Utilities
{
    public class Hash
    {
        private LoggerII.CLoggerII objLoggerII;
        private Common objCommon;                 // Added, 11 Apr 2014
        //private System.Web.HttpServerUtility Server = System.Web.HttpContext.Current.Server; // Server.UrlEncode, Server.MapPath; If HttpUtility just Server.UrlEncode

        //public string szDecrypt3DES(string sz_CipherText, string sz_Key, string sz_InitVector)
        //{
        //    TripleDESCryptoServiceProvider obj3DES = new TripleDESCryptoServiceProvider();
        //    UTF8Encoding objUTF8 = new UTF8Encoding();
        //    byte[] btInput;
        //    byte[] btOutput;

        //    string szDecrypt3DES = "";

        //    obj3DES.Padding = PaddingMode.PKCS7; // Default value
        //    obj3DES.Mode = CipherMode.CBC;       // Default value
        //    obj3DES.Key = btHexToByte(sz_Key);

        //    if ((false == bValidHex(sz_InitVector)))
        //    {
        //        sz_InitVector = szStringToHex(sz_InitVector);
        //    }
        //    obj3DES.IV = btHexToByte(sz_InitVector);

        //    sz_CipherText = Replace(sz_CipherText, Constants.vbCrLf, "");
        //    sz_CipherText = Replace(sz_CipherText, Constants.vbCr, "");
        //    sz_CipherText = Replace(sz_CipherText, Constants.vbLf, "");

        //    if ((false == bValidHex(sz_CipherText)))
        //    {
        //        sz_CipherText = szStringToHex(sz_CipherText);
        //    }
        //    btInput = btHexToByte(sz_CipherText);

        //    btOutput = btTransform(btInput, obj3DES.CreateDecryptor());

        //    szDecrypt3DES = objUTF8.GetString(btOutput);

        //    return szDecrypt3DES;
         

        //}

        //public bool bValidHex(string sz_Txt)
        //{
        //    string szch;
        //    int i;

        //    sz_Txt = sz_Txt.ToUpper();

        //    for (i = 1; i <= Len(sz_Txt); i++)
        //    {
        //        if ((false == IsNumeric(szch)))
        //        {
        //            if (szch >= "G")
        //            {
        //                return false;
        //            }
        //        }
        //    }
        //}

        //public byte[] btHexToByte(char[] c_Hex)
        //{
        //    string szHex = "";

        //    byte[] btOutput = new byte[c_Hex.Length() / (double)2 - 1 + 1]; // Byte() = New Byte(c_Hex.Length / 2 - 1) {}
        //    int iOffset = 0;
        //    int iCounter = 0;

        //    iCounter = 0;

        //    while (iCounter < c_Hex.Length)
        //    {
        //        szHex = Convert.ToString(c_Hex(iCounter)) + Convert.ToString(c_Hex(iCounter + 1));
        //        btOutput(iOffset) = byte.Parse(szHex, System.Globalization.NumberStyles.HexNumber);
        //        iOffset += 1;
        //        iCounter = iCounter + 2;
        //    }

        //    return btOutput;
        //}

        public string GetSaleResHash2(ResTxn resTxn, Merchant merchant)
        {
            Logger logger = new Logger();
            CLoggerII objLoggerII = new CLoggerII();
            objLoggerII = logger.InitLogger();

            string szHashValue = "";
            string szTxnAmount = "";      // Added, 2 Sept 2014, for FX
            string szCurrencyCode = "";   // Added, 2 Sept 2014, for FX
            string result = string.Empty;

            szHashValue = "";

            if ((string.IsNullOrEmpty(resTxn.FXCurrencyCode)))
            {
                szTxnAmount = resTxn.TxnAmount;
                szCurrencyCode = resTxn.CurrencyCode;
            }
            else
            {
                szTxnAmount = resTxn.BaseTxnAmount;            // Hashing TxnAmount is the original amount
                szCurrencyCode = resTxn.BaseCurrencyCode;      // If FX, st_PayInfo.szTxnAmount is storing FXTxnAmount assigned in bVerifyResTxn()
            }

            //MerchantPassword + MerchantID + MerchantPymtID + PTxnID + MerchantOrdID + MerchantTxnAmt + MerchantCurrCode + PTxnStatus + AuthCode
            szHashValue = merchant.MerchantPassword;//pdbs12345 MerchantPassword 
            szHashValue = szHashValue + resTxn.MerchantID; //PDBS MerchantID 
            szHashValue = szHashValue + HttpUtility.UrlEncode(resTxn.MerchantTxnID); //2020092200198 MerchantPymtID 
            szHashValue = szHashValue + resTxn.GatewayTxnID;//PDBS0000000002020092200198 PTxnID 
            szHashValue = szHashValue + HttpUtility.UrlEncode(resTxn.OrderNumber); //OMTest MerchantOrdID 
            // Modified 2 Sept 2014, use szTxnAmount and szCurrencyCode instead of st_PayInfo.szTxnAmount
            szHashValue = szHashValue + szTxnAmount; //100.00 MerchantTxnAmt 
            szHashValue = szHashValue + szCurrencyCode; //MYR MerchantCurrCode 
            szHashValue = szHashValue + resTxn.TxnStatus.ToString(); //0 PTxnStatus 
            if (!String.IsNullOrEmpty(resTxn.AuthCode))
            { 
                szHashValue = szHashValue + resTxn.AuthCode.ToString(); //AuthCode
            }

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                resTxn.MerchantID + resTxn.MerchantTxnID + " > (SHA512) " +
                "(Merchant Password + " + szHashValue.Length.ToString() + merchant.MerchantPassword.Length.ToString() +  ": "+ 
                resTxn.MerchantID  + resTxn.MerchantTxnID + resTxn.MerchantID + resTxn.MerchantTxnID + HttpUtility.UrlEncode(resTxn.OrderNumber) +
                szTxnAmount  + szCurrencyCode  + resTxn.TxnStatus.ToString() + resTxn.AuthCode.ToString() + ")");

            var data = Encoding.UTF8.GetBytes(szHashValue);
            using (SHA512 shaM = new SHA512Managed())
            {
                byte[] resultHash = shaM.ComputeHash(data);
                result = BitConverter.ToString(resultHash).Replace("-", "");
            }
            return result.ToLower();
        }

        public string GetSaleResHash2OB(OBResTxn resTxn, Merchant merchant)
        {
            Logger logger = new Logger();
            CLoggerII objLoggerII = new CLoggerII();
            objLoggerII = logger.InitLogger();

            string szHashValue = "";
            string szTxnAmount = "";      // Added, 2 Sept 2014, for FX
            string szCurrencyCode = "";   // Added, 2 Sept 2014, for FX
            string result = string.Empty;

            szHashValue = "";

            if ((string.IsNullOrEmpty(resTxn.FXCurrencyCode)))
            {
                szTxnAmount = resTxn.TxnAmount;
                szCurrencyCode = resTxn.CurrencyCode;
            }
            else
            {
                szTxnAmount = resTxn.BaseTxnAmount;            // Hashing TxnAmount is the original amount
                szCurrencyCode = resTxn.BaseCurrencyCode;      // If FX, st_PayInfo.szTxnAmount is storing FXTxnAmount assigned in bVerifyResTxn()
            }

            //MerchantPassword + MerchantID + MerchantPymtID + PTxnID + MerchantOrdID + MerchantTxnAmt + MerchantCurrCode + PTxnStatus + AuthCode
            szHashValue = merchant.MerchantPassword;//pdbs12345 MerchantPassword 
            szHashValue = szHashValue + resTxn.MerchantID; //PDBS MerchantID 
            szHashValue = szHashValue + resTxn.MerchantTxnID; //2020092200198 MerchantPymtID 
            szHashValue = szHashValue + resTxn.GatewayTxnID;//PDBS0000000002020092200198 PTxnID 
            szHashValue = szHashValue + HttpUtility.UrlEncode(resTxn.OrderNumber); //OMTest MerchantOrdID 
            // Modified 2 Sept 2014, use szTxnAmount and szCurrencyCode instead of st_PayInfo.szTxnAmount
            szHashValue = szHashValue + szTxnAmount; //100.00 MerchantTxnAmt 
            szHashValue = szHashValue + szCurrencyCode; //MYR MerchantCurrCode 
            szHashValue = szHashValue + resTxn.TxnStatus.ToString(); //0 PTxnStatus 
            //if (!String.IsNullOrEmpty(resTxn.AuthCode))
            //{
            //    szHashValue = szHashValue + resTxn.AuthCode.ToString(); //AuthCode
            //}

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                resTxn.MerchantID + resTxn.MerchantTxnID + " > (SHA512) " +
                "(Merchant Password + " + szHashValue.Length.ToString() + merchant.MerchantPassword.Length.ToString() + ": " +
                resTxn.MerchantID + resTxn.MerchantTxnID + resTxn.MerchantID + resTxn.MerchantTxnID + HttpUtility.UrlEncode(resTxn.OrderNumber) +
                szTxnAmount + szCurrencyCode + resTxn.TxnStatus.ToString() /*+ resTxn.AuthCode.ToString()*/ + ")");

            var data = Encoding.UTF8.GetBytes(szHashValue);
            using (SHA512 shaM = new SHA512Managed())
            {
                byte[] resultHash = shaM.ComputeHash(data);
                result = BitConverter.ToString(resultHash).Replace("-", "");
            }
            return result.ToLower();
        }

        public string GetSaleResHash2Cancel(ReqTxn reqTxn, Merchant merchant)
        {
            Logger logger = new Logger();
            CLoggerII objLoggerII = new CLoggerII();
            objLoggerII = logger.InitLogger();

            string szHashValue = "";
            string szTxnAmount = "";      // Added, 2 Sept 2014, for FX
            string szCurrencyCode = "";   // Added, 2 Sept 2014, for FX
            string result = string.Empty;

            szHashValue = "";

            if ((string.IsNullOrEmpty(reqTxn.FXCurrencyCode)))
            {
                szTxnAmount = reqTxn.TxnAmount;
                szCurrencyCode = reqTxn.CurrencyCode;
            }
            else
            {
                szTxnAmount = reqTxn.BaseTxnAmount;            // Hashing TxnAmount is the original amount
                szCurrencyCode = reqTxn.BaseCurrencyCode;      // If FX, st_PayInfo.szTxnAmount is storing FXTxnAmount assigned in bVerifyResTxn()
            }

            szHashValue = merchant.MerchantPassword;//pdbs12345 MerchantPassword 
            szHashValue = szHashValue + "PAY";
            szHashValue = szHashValue + reqTxn.MerchantID; //PDBS MerchantID 
            szHashValue = szHashValue + HttpUtility.UrlEncode(reqTxn.MerchantTxnID);
            szHashValue = szHashValue + HttpUtility.UrlEncode(reqTxn.OrderNumber); //OMTest MerchantOrdID 
            //szHashValue = szHashValue + reqTxn.Pagetimeout; //timeout
            szHashValue = szHashValue + reqTxn.MerchantReturnURL;
            szHashValue = szHashValue + szTxnAmount; //100.00 MerchantTxnAmt 
            szHashValue = szHashValue + szCurrencyCode; //MYR MerchantCurrCode 
            szHashValue = szHashValue + reqTxn.MerchantCallbackURL;

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                reqTxn.MerchantID + reqTxn.MerchantTxnID + " > (SHA512) " +
                szHashValue.Length.ToString());
                //": " +
                //reqTxn.MerchantID + reqTxn.MerchantTxnID + reqTxn.MerchantID + reqTxn.MerchantTxnID + HttpUtility.UrlEncode(reqTxn.OrderNumber) +
                //szTxnAmount + szCurrencyCode + reqTxn.TxnStatus.ToString() + reqTxn.AuthCode.ToString() + ")");

            var data = Encoding.UTF8.GetBytes(szHashValue);
            using (SHA512 shaM = new SHA512Managed())
            {
                byte[] resultHash = shaM.ComputeHash(data);
                result = BitConverter.ToString(resultHash).Replace("-", "");
            }
            return result.ToLower();
        }

    }

}
