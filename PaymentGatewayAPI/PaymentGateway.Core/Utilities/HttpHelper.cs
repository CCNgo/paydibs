﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using System.Text;
using System.Runtime.Serialization;
using System.Reflection;
using System.Globalization;
using LoggerII;
using System.Threading;

namespace PaymentGatewayAPI.Core.Utilities
{
    public static class HttpHelper
    {
        
        private const string HttpGetMethod = "GET";

        private const string HttpPostMethod = "POST";

        public static string HttpGet(string url)
        {
            var result = string.Empty;
            var request = WebRequest.Create(url) as HttpWebRequest;
            request.Method = HttpGetMethod;
            request.KeepAlive = false;

            using (var response = request.GetResponse())
            {
                var responseStream = response.GetResponseStream();
                try
                {
                    if (responseStream != null)
                    {
                        using (var sr = new StreamReader(responseStream))
                        {
                            result = sr.ReadToEnd();
                            responseStream = null;
                        }
                    }
                }
                finally
                {
                    if (responseStream != null) responseStream.Close();
                }
            }
            return result;
        }

        public static string HttpGet(string url, string mime, string Accept)
        {
            var result = string.Empty;
            var request = WebRequest.Create(url) as HttpWebRequest;
            request.Method = HttpGetMethod;
            request.ContentType = mime;
            request.KeepAlive = false;
            request.Accept = Accept;

            using (var response = request.GetResponse())
            {
                var responseStream = response.GetResponseStream();
                try
                {
                    if (responseStream != null)
                    {
                        using (var sr = new StreamReader(responseStream))
                        {
                            result = sr.ReadToEnd();
                            responseStream = null;
                        }
                    }
                }
                finally
                {
                    if (responseStream != null) responseStream.Close();
                }
            }
            return result;
        }

        public static string HttpPost(string url, string postBody, string mime, string sz_SecurityProtocol = "TLS")
        {
            //support merchant protocol
            if (sz_SecurityProtocol != string.Empty)
            {
                switch (sz_SecurityProtocol)
                {
                    case "SSL3":
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
                        break;
                    case "TLS12":
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                        break;
                    case "TLS11":
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11;
                        break;
                    case "TLS":
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;
                        break;
                    default:
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
                        break;
                }
            }

            Logger logger = new Logger();
            CLoggerII objLoggerII = new CLoggerII();
            objLoggerII = logger.InitLogger();
            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(),
                           LoggerII.DebugInfo.__LINE__(), "[Posting: " + url + "][Security Protocol: ]" + sz_SecurityProtocol);

            var result = string.Empty;
            var request = WebRequest.Create(url) as HttpWebRequest;
            request.Method = HttpPostMethod;
            request.ContentType = mime;
            request.KeepAlive = false;

            var body = Encoding.UTF8.GetBytes(postBody);

            request.ContentLength = body.Length;

            using (var requestStream = request.GetRequestStream())
            {
                requestStream.Write(body, 0, body.Length);
            }

            Stream responseStream = null;
            try
            {
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    responseStream = response.GetResponseStream();

                    if (responseStream != null)
                    {
                        if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Created)
                        {
                            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(),
                                LoggerII.DebugInfo.__LINE__(), "HTTP response received OK/Created: " + url);

                            using (var sr = new StreamReader(responseStream))
                            {
                                result = sr.ReadToEnd();
                                responseStream = null;
                            }
                            return result;
                        }
                    }
                }
            }

            catch (WebException webEx)
            {
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(),
                    "WebException: " + webEx.ToString() + webEx.Status.ToString());
            }
            finally
            {
                if (responseStream != null) responseStream.Close();
            }
            return result;
        }

        public static string HttpPostReadWithData(string url, string postBody, string mime, string sz_SecurityProtocol = "TLS")
        {
            Logger logger = new Logger();
            CLoggerII objLoggerII = new CLoggerII();
            objLoggerII = logger.InitLogger();
            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(),
                           LoggerII.DebugInfo.__LINE__(), "Posting " + url);

            //support merchant protocol
            if (sz_SecurityProtocol != string.Empty)
            {
                switch (sz_SecurityProtocol)
                {
                    case "SSL3":
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
                        break;
                    case "TLS12":
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                        break;
                    case "TLS11":
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11;
                        break;
                    case "TLS":
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;
                        break;
                    default:
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
                        break;
                }
            }

            var result = string.Empty;
            var request = WebRequest.Create(url) as HttpWebRequest;
            request.Method = HttpPostMethod;
            request.ContentType = mime;
            request.KeepAlive = false;

            var body = Encoding.UTF8.GetBytes(postBody);

            request.ContentLength = body.Length;

            using (var requestStream = request.GetRequestStream())
            {
                requestStream.Write(body, 0, body.Length);
            }

            Stream responseStream = null;
            try
            {
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    responseStream = response.GetResponseStream();
            
                    if (responseStream != null)
                    {
                        if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Created)
                        {
                            Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                            StreamReader readStream = new StreamReader(responseStream, encode);

                            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(),
                             LoggerII.DebugInfo.__LINE__(), "Response stream received" );

                            Char[] read = new Char[256];

                            // Read 256 charcters at a time.    
                            int count = readStream.Read(read, 0, 256);
                            string readValue = string.Empty;

                            while (count > 0)
                            {
                                // Dump the 256 characters on a string and display the string onto the log.
                                readValue = new String(read, 0, count);
                                count = readStream.Read(read, 0, 256);
                            }

                            objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(),
                                LoggerII.DebugInfo.__LINE__(), "HTTP response received OK/Created: " + url);

                            using (var sr = new StreamReader(responseStream))
                            {
                                result = sr.ReadToEnd();
                                responseStream = null;
                            }

                            if(!String.IsNullOrEmpty(readValue))
                            {
                                return readValue;
                            }
                        }
                    }
                }
            }

            catch (WebException webEx)
            {
                objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.WARNING, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(),
                    "WebException: " + webEx.ToString() + webEx.Status.ToString());
            }
            finally
            {
                if (responseStream != null) responseStream.Close();
            }
            return result;
        }

        public static string XmlHttpPostSerialize(Object obj, string posturl)
        {
            XmlSerializerNamespaces XSN = new XmlSerializerNamespaces();
            XSN.Add("", "");
            XmlWriterSettings XWS = new XmlWriterSettings();
            XWS.OmitXmlDeclaration = true;
            StringBuilder XmlData = new StringBuilder();
            XmlSerializer postData = new XmlSerializer(obj.GetType());
            postData.Serialize(XmlTextWriter.Create(XmlData, XWS), obj, XSN);

            string ResponseXML = string.Empty;
            ResponseXML = HttpHelper.HttpPost(Convert.ToString(posturl), XmlData.ToString(), MIMEType.URL_ENCODED);

            return ResponseXML;
        }
        public static string RemoveBracket(string param)
        {
            if (param.Substring(0, 1) == "[")
            {
                param = param.Substring(1, param.Length - 1);
            }

            if (param.Substring(param.Length - 1) == "]")
            {
                param = param.Substring(0, param.Length - 1);
            }

            return param;
        }

        public static string GetIP()
        {
            string strHostName = System.Net.Dns.GetHostName();
            IPHostEntry ipEntry = System.Net.Dns.GetHostEntry(strHostName);
            IPAddress[] addr = ipEntry.AddressList;
            return addr[addr.Length - 1].ToString();
        }
    }

    public struct MIMEType
    {
        public const string APP_XML = "application/xml";
        public const string APP_XML_UTF8 = "application/xml; charset=UTF-8";
        public const string APP_JSON = "application/json";
        public const string APP_JSON_UTF8 = "application/json; charset=UTF-8";
        public const string TEXT_JSON = "text/json";
        public const string TEXT_PLAIN = "text/plain";
        public const string TEXT_XML = "text/xml";
        public const string TEXT_XML_UTF8 = "text/xml; charset=UTF-8";
        public const string URL_ENCODED = "application/x-www-form-urlencoded";
    }

    
}