﻿using Kenji.Apps;
using NeoApp;
using PaydibsPortal.DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewayAPI.Core
{
    public class Database
    {
        public class DB
        {
            MsSqlClient mssqlc = null;
            SqlBuilder sqlb = new SqlBuilder();
            string connStr = "";
            LinqTool linq = new LinqTool();
            public string errMsg = "";
            Encryption enc = new Encryption();
            public string system_err = string.Empty;
            string decryptKey = string.Empty;

            public MsSqlClient connstring(string conn)
            {
                string Ecnrypted_connStr = ConfigurationManager.AppSettings[conn];
                connStr = Decrypt_ConnStr(Ecnrypted_connStr);
                mssqlc = new MsSqlClient(connStr);

                return mssqlc;
            }

            string Decrypt_ConnStr(string PEncrypted_ConnStr)
            {
                string KEY = "e26b5be2-867a-4400-8079-612b4a2332f6";
                decryptKey = enc.AESDecrypt(PEncrypted_ConnStr, KEY, KEY);

                return decryptKey;
            }
        }

        public class OB
        {
            private static string _alias;
            public static string Alias
            {
                get
                {
                    if (String.IsNullOrEmpty(_alias))
                    {
                        _alias = ConfigurationManager.AppSettings["OB_DBAlias"];
                        if (String.IsNullOrEmpty(_alias))
                        {
                            _alias = "OB";
                        }
                    }

                    return _alias;
                }
            }

            public class StoredProcedures
            {
                public const string spGetHosts = "OB.dbo.spGetHosts";
                public const string spGetTxnParam = "OB.dbo.spGetTxnParam";
                public const string spGetMerchant = "OB.dbo.spGetMerchant";
                public const string spGetResTxn = "OB.dbo.spGetResTxn";
                public const string spCheckNotifyStatus = "OB.dbo.spCheckNotifyStatus";
                public const string spGetReqTxn = "OB.dbo.spGetReqTxn";
                public const string spGetReqTxn_By_MerchantTxnID = "OB.dbo.spGetReqTxn_By_MerchantTxnID";
                public const string spGetHostInfo = "OB.dbo.spGetHostInfo";
                public const string spUpdatePayTxnRef = "OB.dbo.spUpdatePayTxnRef";
                public const string spInsTxnResp = "OB.dbo.spInsTxnResp";
                public const string spInsNotifyStatus = "OB.dbo.spInsNotifyStatus";

            }
        }

        public class PG
        {
            private static string _alias;
            public static string Alias
            {
                get
                {
                    if (String.IsNullOrEmpty(_alias))
                    {
                        _alias = ConfigurationManager.AppSettings["PG_DBAlias"];
                        if (String.IsNullOrEmpty(_alias))
                        {
                            _alias = "PG";
                        }
                    }

                    return _alias;
                }
            }

            public class StoredProcedures
            {
                public const string spGetHostInfo = "PG.dbo.spGetHostInfo";
                public const string spGetTxnParam = "PG.dbo.spGetTxnParam";
                public const string spGetReqTxn = "PG.dbo.spGetReqTxn";
                public const string spGetReqTxn_By_MerchantTxnID = "PG.dbo.spGetReqTxn_By_MerchantTxnID";
                public const string spGetResTxn = "PG.dbo.spGetResTxn";
                public const string spInsTxnResp = "PG.dbo.spInsTxnResp";
                public const string spUpdatePayTxnRef = "PG.dbo.spUpdatePayTxnRef";
                public const string spGetMerchant = "PG.dbo.spGetMerchant";
                public const string spCheckNotifyStatus = "PG.dbo.spCheckNotifyStatus";
                public const string spInsNotifyStatus = "PG.dbo.spInsNotifyStatus";
                public const string spInsVGSData = "PG.dbo.spInsVGSData";
                public const string spSelVGSDataByGatewayTxnId = "PG.dbo.spSelVGSDataByGatewayTxnId";
            }
        }

        public class PGAdmin
        {
            private static string _alias;
            public static string Alias
            {
                get
                {
                    if (String.IsNullOrEmpty(_alias))
                    {
                        _alias = ConfigurationManager.AppSettings["PGAdmin_DBAlias"];
                        if (String.IsNullOrEmpty(_alias))
                        {
                            _alias = "PGAdmin";
                        }
                    }

                    return _alias;
                }
            }

            public class StoredProcedures
            {
                public const string spRepTBLateRes_By_BankRespCode = "PGAdmin.dbo.spRepTBLateRes_By_BankRespCode";
                public const string GetConfigDetails = "PGAdmin.dbo.GetConfigDetails";
                public const string spUpdate_PayRes_TxnRate_Status = "PGAdmin.dbo.spUpdate_PayRes_TxnRate_Status";
                public const string spGettblCCMerchantCredential_By_PaydibsMID = "PGAdmin.dbo.GettblCCMerchantCredential_By_PaydibsMID";
                //local
                public const string spRep_ProcessCardPAN_Sel = "PGAdmin.dbo.Rep_ProcessCardPAN_Sel";
                public const string spRep_ProcessCardPAN_Upd = "PGAdmin.dbo.Rep_ProcessCardPAN_Upd";
            }
        }


    }
}
