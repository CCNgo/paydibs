﻿using LoggerII;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace PaymentGatewayAPI.Core
{
    public class Logger
    {
        public CLoggerII InitLogger()
        {
            //var objLoggerII = new CLoggerII();

            string szFI = string.Empty;
            string szPymtMethod = string.Empty;
            string szTxnType = string.Empty;
            string szLogFile = string.Empty;
            string szLogPath = string.Empty;
            int iLogLevel = 0;

            try
            {
                szLogPath = ConfigurationManager.AppSettings["LogPath"];
                iLogLevel = int.Parse(ConfigurationManager.AppSettings["LogLevel"]);

                if (string.IsNullOrEmpty(szLogPath))
                    szLogPath = Environment.CurrentDirectory + Path.DirectorySeparatorChar + "Logs\\";

                if (iLogLevel < 0 || iLogLevel > 5)
                    iLogLevel = 3;

                szLogFile = szLogPath;
                szLogFile = szLogFile + ".log";

                return (CLoggerII)CLoggerII.get_GetInstanceX(szLogFile, ((CLoggerII.LOG_SEVERITY)System.Enum.Parse(typeof(CLoggerII.LOG_SEVERITY), iLogLevel.ToString())), 255);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
