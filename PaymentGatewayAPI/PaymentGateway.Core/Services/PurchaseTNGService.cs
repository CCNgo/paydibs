﻿using LoggerII;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using PaymentGatewayAPI.Core.Configuration;
using PaymentGatewayAPI.Core.Models;
using PaymentGatewayAPI.Core.Providers;
using PaymentGatewayAPI.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace PaymentGatewayAPI.Core.Services
{
    public class PurchaseTNGService
    {
        private SystemConfiguration Config = null;
        private ConfigurationProvider _configurationProvider;
        private ConfigurationProvider ConfigurationProvider
        {
            get { return _configurationProvider ?? (_configurationProvider = new ConfigurationProvider()); }
        }
        private HttpResponseMessage OQResult = new HttpResponseMessage();
        public SalesOrderResponse CommitPurchase(SalesOrderRequest request)
        {
            Logger logger = new Logger();
            CLoggerII objLoggerII = new CLoggerII();
            objLoggerII = logger.InitLogger();
            string msg = "[Paydibs PaymentGatewayAPI PurchaseTNGService.CommitPurchase][" + request.function + "]" + DateTime.Now + ": [TNG Init: " + request.merchantId + "| " 
                + request.onlineRefNum + "|" + request.amount + "|"
                + request.orderTitle + "|" + request.merchantinfo.id + "|" + request.merchantinfo.merchantName + "]";
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                msg);

            string POSTResponse = string.Empty;
            string checkoutURL = string.Empty;
            string TNGeWalletReferenceNo = string.Empty;
  
            try
            {
                Config = ConfigurationProvider.GetConfig("Paydibs.PaymentGatewayAPI");

                request.reqTime = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz");
                request.expiryTime = DateTime.Now.AddSeconds(Convert.ToDouble(Config.TNG_timeout)).ToString("yyyy-MM-ddTHH:mm:sszzz");
                request.reqMsgId = request.onlineRefNum;
                request.version = Config.TNG_version;
                request.function = "alipayplus.acquiring.order.create";
                request.clientId = Config.TNG_clientId;
                request.clientSecret = Config.TNG_clientSecret;
                request.merchantId = Config.TNG_merchantId;
                request.productCode = Config.TNG_productCode;
                request.currency = "MYR";
                request.orderTitle = "(" + request.onlineRefNum + ") " + request.orderTitle;

              
                if (request.orderTitle.Length > 256)
                    request.orderTitle = request.orderTitle.Substring(0, 256);

                List<NotificationUrls> notificationUrls = new List<NotificationUrls>();

                notificationUrls.Add(new NotificationUrls() { url = Config.TNG_URLPAY_RETURN + "?clientId=" + request.clientId + "&merchantId=" + request.merchantId + "&merchantTransId=" + request.onlineRefNum + "&version=" + request.version, type = "PAY_RETURN" });
                //notificationUrls.Add(new NotificationUrls() { url = Config.TNG_URLNOTIFICATION, type = "NOTIFICATION" });
                notificationUrls.Add(new NotificationUrls() { url = Config.TNG_URLNOTIFICATION, type = "NOTIFICATION" });
                request.notificationurl = Config.TNG_URLNOTIFICATION;
                var signature = generateSignature(request);

                string POSTURL = Config.TNG_POSTURL;
                TNGRequest req = new TNGRequest()
                {
                    Request = new Request
                    {
                        Head = new RequestHead
                        {
                            version = request.version,
                            function = request.function,
                            clientId = request.clientId,
                            clientSecret = request.clientSecret,
                            reqTime = request.reqTime,
                            reqMsgId = request.reqMsgId
                        },
                        Body = new RequestBody
                        {
                            order = new Order
                            {
                                orderTitle = request.orderTitle,
                                orderAmount = new OrderAmount()
                                {
                                    currency = request.currency,
                                    value = request.amount.ToString().Replace(".", string.Empty)
                                },
                                merchantTransId = request.onlineRefNum,
                                expiryTime = request.expiryTime
                            },
                            merchantId = request.merchantId,
                            productCode = request.productCode,
                            signAgreementPay = false,
                            envInfo = new EnvInfo
                            {
                                terminalType = "SYSTEM",
                                orderTerminalType = "APP"
                            },
                            notificationUrls = notificationUrls,

                            //extendInfo = "{\"PARTNER_TRANSACTION_ID\":\"10207635486872290000\"}",
                            //subMerchantId = "TestingMerchantId",
                            //subMerchantName = "PAYDIBS"
                            extendInfo = "",
                            subMerchantId = "",
                            subMerchantName = ""
                        }
                    },
                    Signature = signature
                };

                //Regenerate signature
                string input = JsonConvert.SerializeObject(req.Request);
                req.Signature = generateSignatureTnG(input);

                string postBody = JsonConvert.SerializeObject(req, new JsonSerializerSettings() { ContractResolver = new CamelCasePropertyNamesContractResolver() });

                msg = "[Paydibs PaymentGatewayAPI PurchaseTNGService.CommitPurchase][" + request.function + "]" + DateTime.Now + ": [TNG Request: " + postBody + "]";
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                    msg);

                POSTResponse = HttpHelper.HttpPost(POSTURL, postBody, MIMEType.TEXT_JSON, "TLS12");

                msg = "[Paydibs PaymentGatewayAPI PurchaseTNGService.CommitPurchase][" + request.function + "]" + DateTime.Now + ": [TNG Response: " + POSTResponse + "]";
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                    msg);

                TNGResponse TNGresponse = JsonConvert.DeserializeObject<TNGResponse>(POSTResponse);

                if (TNGresponse.Response.Body.resultInfo.resultStatus == "S"
                    && TNGresponse.Response.Body.merchantTransId == request.onlineRefNum)
                {
                    checkoutURL = TNGresponse.Response.Body.checkoutUrl;
                    TNGeWalletReferenceNo = TNGresponse.Response.Body.acquirementId;
                }
                else
                {
                    throw (new Exception(
                                            "Result is not successful"
                                            ));
                }
            }
            catch (Exception ex)
            {
                msg = "[Paydibs PaymentGatewayAPI PurchaseTNGService.CommitPurchase][" + request.function + "]" + DateTime.Now + ": [TNG Exception: " + ex.ToString() + "]";
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                    msg);

                Config = ConfigurationProvider.GetConfig("Paydibs.PaymentGatewayAPI");

                var responseb = SalesOrderResponse.Pending;
                responseb.RedirectURL = Config.TNG_URLPAY_RETURN + "?clientId=" + Config.TNG_clientId + "&merchantId=" + Config.TNG_merchantId + "&merchantTransId=" + request.onlineRefNum + "&version=" + Config.TNG_version;
                responseb.merchantTransId = request.onlineRefNum;
                responseb.value = request.amount;
                responseb.bankrefno = " ";
                responseb.acquirementStatus = "2";
                //return new SalesOrderResponse();
                return responseb;
            }

            var response = SalesOrderResponse.Pending;
            if (POSTResponse != null)
            {
                response.RedirectURL = checkoutURL;
                response.merchantTransId = request.onlineRefNum;
                response.value = request.amount;
                response.bankrefno = " ";
                response.acquirementStatus = "6";
            }
            return response;
        }

        public TnGOrderFinishFinalResponse CommitOrderFinish(TnGOrderFinishRequest request)
        {
            Logger logger = new Logger();
            CLoggerII objLoggerII = new CLoggerII();
            objLoggerII = logger.InitLogger();
            string msg = "[Paydibs PaymentGatewayAPI PurchaseTNGService.OrderFinish]" + " " + request.TnGOFRequest.Head.reqMsgId;
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                msg);
            
            string POSTResponse = string.Empty;
            string checkoutURL = string.Empty;
            string TNGeWalletReferenceNo = string.Empty;
            TnGOrderFinishFinalResponse finalResponse = new TnGOrderFinishFinalResponse();
            OQResult = new HttpResponseMessage();
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
"transid" + request.TnGOFRequest.Body.merchantTransId);
            OnlineProvider TnGOnlineProvider = new OnlineProvider();
            OBResTxn TnGRes = TnGOnlineProvider.GetResTxn(request.TnGOFRequest.Body.merchantTransId);
          
            if (TnGRes == null)
            {
                try
                {
                    HttpResponseMessage OQResultTemp = new HttpResponseMessage();

                    Config = ConfigurationProvider.GetConfig("Paydibs.PaymentGatewayAPI");
                    OQResult = OQResultTemp;
                   
                }
                catch (Exception ex)
                {
                    msg = "[Paydibs PaymentGatewayAPI PurchaseTNGService.CommitOrderFinish][" + request.TnGOFRequest.Head.function + "]" + DateTime.Now + ": [TNG Exception: " + ex.ToString() + "]";
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                        msg);

                    return new TnGOrderFinishFinalResponse();
                }
            }
            
            var response = new TnGOrderFinishResponse();
            if (POSTResponse != null)
            {
                //response.RedirectURL = checkoutURL;
                //response.merchantTransId = request.onlineRefNum;
                //response.value = request.amount;
                //response.bankrefno = TNGeWalletReferenceNo;
                //response.acquirementStatus = "2";
            }
            
            response.TnGOFResponse = new OrderFinishResponse
            {
                Head = new OrderFinishResponseHead
                {
                    clientId = request.TnGOFRequest.Head.clientId,
                    function = request.TnGOFRequest.Head.function,
                    reqMsgId = request.TnGOFRequest.Head.reqMsgId,
                    respTime = request.TnGOFRequest.Head.reqTime,
                    version = request.TnGOFRequest.Head.version
                },
                Body = new OrderFinishResponseBody
                {
                    resultInfo = new ResultInfo
                    {
                        resultStatus = "S",
                        resultCodeId = "00000000",
                        resultCode = "SUCCESS",
                        resultMsg = "success"
                    }
                }
            };
           
           
            string input = JsonConvert.SerializeObject(response.TnGOFResponse);

            response.Signature = generateSignatureTnG(input);           

            finalResponse.OFResponse = response;
            if (OQResult != null && OQResult.Content != null)
            {
              
                finalResponse.QueryResult = OQResult;
            }
            return finalResponse;
        }

        public HttpResponseMessage CommitOrderQuery(TNGPayReturn request)
        {
            Logger logger = new Logger();
            CLoggerII objLoggerII = new CLoggerII();
            objLoggerII = logger.InitLogger();
            const string ISSUING_BANK = "TNG";
            const string TXN_TYPE = "PAY";
            var returnMsg = new HttpResponseMessage();

            var TnGFunction = "alipayplus.acquiring.order.query";

            var Config = new ConfigurationProvider().GetConfig("Paydibs.PaymentGatewayAPI");
            
            TnGOrderQueryRequest OQRequest = new TnGOrderQueryRequest
            {
                TnGOQRequest = new OrderQueryRequest
                {
                    Head = new OrderQueryRequestHead
                    {
                        clientId = request.clientId,
                        clientSecret = Config.TNG_clientSecret,
                        function = TnGFunction,
                        reqTime = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz"),
                        reqMsgId = request.merchantId + request.merchantTransId,
                        version = request.version,
                        reserve = "{ \"attr1\":\"val1\" }"
                    },
                    Body = new OrderQueryRequestBody
                    {
                        //acquirementId = request.TnGOFRequest.Body.acquirementId,
                        merchantId = request.merchantId,
                        merchantTransId = request.merchantTransId
                    }
                }
            };
            //string input = "{\"head\":{\"version\":\"" + request.TnGOFRequest.Head.version + "\",\"function\":\"" + request.TnGOFRequest.Head.function + "\",\"clientId\":\"" + request.TnGOFRequest.Head.clientId +
            //  "\",\"reqTime\":\"" + request.TnGOFRequest.Head.reqTime + "\",\"reqMsgId\":\"" + request.TnGOFRequest.Head.reqMsgId + "\"}," +
            //  "\"body\":{\"resultInfo\":{\"resultStatus\":\"" + request.TnGOFRequest.Body.resultInfo.resultStatus + "\",\"resultCodeId\":\"" + request.TnGOFRequest.Body.resultInfo.resultCodeId + "\",\"resultCode\":\"" + request.TnGOFRequest.Body.resultInfo.resultStatus +
            //   "\",\"resultMsg\":\"" + request.TnGOFRequest.Body.extendInfo + "\"}}";
            string input = JsonConvert.SerializeObject(OQRequest.TnGOQRequest);

            OQRequest.Signature = generateSignatureTnG(input);

            string postBody = JsonConvert.SerializeObject(OQRequest, new JsonSerializerSettings() { ContractResolver = new CamelCasePropertyNamesContractResolver() });

            string msg = "[Paydibs PaymentGatewayAPI PurchaseTNGService.CommitOrderQuery][" + TnGFunction + "]" + DateTime.Now + ": [TNG Request: " + postBody + "]";
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                msg);

            string POSTURL = Config.TNG_QueryUrl;
            var POSTResponse = HttpHelper.HttpPost(POSTURL, postBody, MIMEType.TEXT_JSON, "TLS12");

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                 "POST Response: " + POSTResponse);
          

            TnGOrderQueryResponse TNGresponse = new TnGOrderQueryResponse();
            OrderQueryResponse oFResponse = new OrderQueryResponse();
            OnlineProvider onlineProvider = new OnlineProvider(); 
            try
            {
                TNGresponse = JsonConvert.DeserializeObject<TnGOrderQueryResponse>(POSTResponse);

                oFResponse = TNGresponse.TnGOQResponse;
                //if (generateSignatureTnG(JsonConvert.SerializeObject(oFResponse)) != TNGresponse.Signature)
                //{
                //    return new HttpRequestMessage().CreateResponse(HttpStatusCode.BadRequest, oFResponse.Body.merchantTransId + "Invalid Signature");
                //}
            }
            catch (Exception ex)
            {
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
            ex.Message);
            }
            if (String.IsNullOrEmpty(oFResponse.Body.merchantTransId))
            {
                oFResponse.Body.merchantTransId = request.merchantTransId;
                oFResponse.Body.statusDetail = new StatusDetail
                {
                    acquirementStatus = StatusDetailEnum.CLOSED
                };

                returnMsg.Content = new StringContent("An error occured, please refer to the support for details");
                return returnMsg;

            }

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
           "transid" + oFResponse.Body.merchantTransId);
            OBReqTxn reqTxn = onlineProvider.GetReqTxn(oFResponse.Body.merchantTransId);

            PurchaseTNGService tNGService = new PurchaseTNGService();

            if (reqTxn != null)
            {              
                Host hostInfo = onlineProvider.GetHost(ISSUING_BANK);

                if (oFResponse.Body.statusDetail.acquirementStatus.ToString() == "SUCCESS")
                {
                    reqTxn.TxnStatus = (Convert.ToInt32(Common.TXN_STATUS.TXN_SUCCESS)).ToString();
                    reqTxn.TxnState = (Convert.ToInt32(Common.TXN_STATE.COMMIT)).ToString();
                    reqTxn.TxnMsg = Common.C_TXN_SUCCESS_0;
                }
                else if (oFResponse.Body.statusDetail.acquirementStatus.ToString() == "INIT")
                {
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                        "[Response TnGOrderQuery][" + oFResponse.Head.function + "]" + " Transaction " + oFResponse.Body.merchantTransId + " is still incomplete");
                    returnMsg.Content = new StringContent("An error occured, please refer to the support for details");
                    return returnMsg;
                }
                else
                {
                    reqTxn.TxnStatus = (Convert.ToInt32(Common.TXN_STATUS.TXN_FAILED)).ToString();
                    reqTxn.TxnState = (Convert.ToInt32(Common.TXN_STATE.COMMIT_FAILED)).ToString();
                    reqTxn.TxnMsg = Common.C_TXN_FAILED_1;
                }
                if (hostInfo == null)
                {
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                       "[Response TnGOrderQuery][" + oFResponse.Head.function + "][DB Host is Empty]" + Newtonsoft.Json.JsonConvert.SerializeObject(oFResponse));


                    return new HttpRequestMessage().CreateResponse(HttpStatusCode.BadRequest, oFResponse.Body.merchantTransId + "Bad Request");

                }
                else
                {
                   
                    reqTxn.CardPAN = "";
                    reqTxn.BankRefNo = oFResponse.Head.reqMsgId;
                    reqTxn.TxnMsg = oFResponse.Body.resultInfo.resultMsg;
                    reqTxn.GatewayTxnID = oFResponse.Body.merchantTransId;
                    reqTxn.GatewayID = "";

                    if (2 == onlineProvider.InsertTxnResp(reqTxn, TXN_TYPE))
                    {
                        OBResTxn resTxn = onlineProvider.GetResTxn(oFResponse.Body.merchantTransId);
                        if (resTxn != null)
                        {
                           
                            resTxn.RespMesg = oFResponse.Body.statusDetail.acquirementStatus.ToString();
                            Merchant merchant = onlineProvider.GetMerchant(resTxn.MerchantID);
                            Host host = onlineProvider.GetHost(ISSUING_BANK);
                            //if (merchant.NotifyEmailAddr == null)
                            //{
                            //    merchant.NotifyEmailAddr = merchant.EmailAddr;                              
                            //}
                            if (oFResponse.Body.statusDetail.acquirementStatus.ToString() == "SUCCESS")
                            {
                                tNGService.SendEmailNotification(reqTxn, resTxn, host, merchant, Common.EmailTemplate.EMAILTEMPLATESHOPPER);
                                tNGService.SendEmailNotification(reqTxn, resTxn, host, merchant, Common.EmailTemplate.EMAILTEMPLATEMERCHANT);
                            }
                           
                            string htmlContent = tNGService.TnGMerchantPayResp(host, resTxn, reqTxn);
                            if (!String.IsNullOrEmpty(resTxn.MerchantCallbackURL))
                            {
                                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "[Response TnGResp][Posting to Merchant CallbackURL][" + resTxn.MerchantCallbackURL + "]");

                                MerchantCallback merchantCallback = tNGService.TnGRetrieveMerchantParameters(host, resTxn, reqTxn);
                                string httpString = tNGService.TnGGetReplyMerchantCallbackHTTPString(merchantCallback);
                                int callBackStatus = 0;
                               
                                for (int t = 0; t <= 4; t++) //Callback 5 times if failed get response from callback url
                                {
                                    string result = HttpHelper.HttpPostReadWithData(resTxn.MerchantCallbackURL, httpString, MIMEType.URL_ENCODED, "TLS12");

                                    if (result != null)
                                    {
                                        callBackStatus = 1;

                                        t = 4;
                                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                            "[Response TnGResp][Callback response]["
                                        + Newtonsoft.Json.JsonConvert.SerializeObject(merchantCallback) + "], Status [success and recv acknowledgement (" + result + ")" + "  ....)]" +
                                        "Protocol[" + merchant.Protocol + "]");
                                    }
                                    else
                                    {
                                        callBackStatus = 0;
                                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                         "[Response TnGResp][Callback response]["
                                        + Newtonsoft.Json.JsonConvert.SerializeObject(merchantCallback) + "], Status [fail][Running: " + t.ToString() +
                                        "Protocol[" + merchant.Protocol + "]");
                                        //Thread.Sleep(50);

                                    }

                                }

                                //onlineProvider.InsertCallBackStatus(reqTxn, callBackStatus);

                                var webResponse = new HttpResponseMessage();
                                webResponse.Content = new StringContent(htmlContent);
                                webResponse.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
                                //objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                //            "htmlcontent:   " + htmlContent);
                                
                                return webResponse;
                            }

                        }
                    }
                    returnMsg.Content = new StringContent("An error occured, please refer to the support for details");
                    return returnMsg;
                }

            }
            else
            {
                OBResTxn resTxn = onlineProvider.GetResTxn(oFResponse.Body.merchantTransId);
                if (resTxn == null)
                {
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION,
                            DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "Failed to find req and res");
                }
                else
                {
                  
                    Merchant merchant = onlineProvider.GetMerchant(resTxn.MerchantID);
                  
                    Host host = onlineProvider.GetHost(ISSUING_BANK);
                    if (resTxn.RespMesg == null || resTxn.RespMesg == "")
                    {
                        resTxn.RespMesg = oFResponse.Body.statusDetail.acquirementStatus.ToString();
                    }
                    string htmlContent = tNGService.TnGMerchantPayResp(host, resTxn, reqTxn);

                    if (!String.IsNullOrEmpty(resTxn.MerchantCallbackURL))
                    {
                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                       "[Response TnGResp][Posting to Merchant CallbackURL][" + resTxn.MerchantCallbackURL + "]");

                        MerchantCallback merchantCallback = tNGService.TnGRetrieveMerchantParameters(host, resTxn, reqTxn);
                        string httpString = tNGService.TnGGetReplyMerchantCallbackHTTPString(merchantCallback);
                        int callBackStatus = 0;
                        
                        for (int t = 0; t <= 4; t++) //Callback 5 times if failed get response from callback url
                        {
                            string result = HttpHelper.HttpPostReadWithData(resTxn.MerchantCallbackURL, httpString, MIMEType.URL_ENCODED, "TLS12");

                            if (result != null)
                            {
                                callBackStatus = 1;

                                t = 4;
                                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    "[Response TnGResp][Callback response]["
                                + Newtonsoft.Json.JsonConvert.SerializeObject(merchantCallback) + "], Status [success and recv acknowledgement (" + result + ")" + "  ....)]" +
                                "Protocol[" + merchant.Protocol + "]");
                            }
                            else
                            {
                                callBackStatus = 0;
                                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                 "[Response TnGResp][Callback response]["
                                + Newtonsoft.Json.JsonConvert.SerializeObject(merchantCallback) + "], Status [fail][Running: " + t.ToString() +
                                "Protocol[" + merchant.Protocol + "]");
                                //Thread.Sleep(50);

                            }

                        }

                        //onlineProvider.InsertCallBackStatus(reqTxn, callBackStatus);

                        var webResponse = new HttpResponseMessage();
                        webResponse.Content = new StringContent(htmlContent);
                        webResponse.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
                        //objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                        //            "htmlcontent:   " + htmlContent);
                       
                        return webResponse;
                    }
                }

                
            }
            returnMsg.Content = new StringContent("An error occured, please refer to the support for details");
            return returnMsg;
        }

        public HttpResponseMessage CommitOrderQueryScheduler(TNGPayReturn request)
        {
            Logger logger = new Logger();
            CLoggerII objLoggerII = new CLoggerII();
            objLoggerII = logger.InitLogger();
            const string ISSUING_BANK = "TNG";
            const string TXN_TYPE = "PAY";
            var returnMsg = new HttpResponseMessage();

            var TnGFunction = "alipayplus.acquiring.order.query";

            var Config = new ConfigurationProvider().GetConfig("Paydibs.PaymentGatewayAPI");

            TnGOrderQueryRequest OQRequest = new TnGOrderQueryRequest
            {
                TnGOQRequest = new OrderQueryRequest
                {
                    Head = new OrderQueryRequestHead
                    {
                        clientId = request.clientId,
                        clientSecret = Config.TNG_clientSecret,
                        function = TnGFunction,
                        reqTime = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz"),
                        reqMsgId = request.merchantId + request.merchantTransId,
                        version = request.version,
                        reserve = "{ \"attr1\":\"val1\" }"
                    },
                    Body = new OrderQueryRequestBody
                    {
                        //acquirementId = request.TnGOFRequest.Body.acquirementId,
                        merchantId = request.merchantId,
                        merchantTransId = request.merchantTransId
                    }
                }
            };
            //string input = "{\"head\":{\"version\":\"" + request.TnGOFRequest.Head.version + "\",\"function\":\"" + request.TnGOFRequest.Head.function + "\",\"clientId\":\"" + request.TnGOFRequest.Head.clientId +
            //  "\",\"reqTime\":\"" + request.TnGOFRequest.Head.reqTime + "\",\"reqMsgId\":\"" + request.TnGOFRequest.Head.reqMsgId + "\"}," +
            //  "\"body\":{\"resultInfo\":{\"resultStatus\":\"" + request.TnGOFRequest.Body.resultInfo.resultStatus + "\",\"resultCodeId\":\"" + request.TnGOFRequest.Body.resultInfo.resultCodeId + "\",\"resultCode\":\"" + request.TnGOFRequest.Body.resultInfo.resultStatus +
            //   "\",\"resultMsg\":\"" + request.TnGOFRequest.Body.extendInfo + "\"}}";
            string input = JsonConvert.SerializeObject(OQRequest.TnGOQRequest);

            OQRequest.Signature = generateSignatureTnG(input);

            string postBody = JsonConvert.SerializeObject(OQRequest, new JsonSerializerSettings() { ContractResolver = new CamelCasePropertyNamesContractResolver() });

            string msg = "[Paydibs PaymentGatewayAPI PurchaseTNGService.CommitOrderQuery][" + TnGFunction + "]" + DateTime.Now + ": [TNG Request: " + postBody + "]";
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                msg);

            string POSTURL = Config.TNG_QueryUrl;
            var POSTResponse = HttpHelper.HttpPost(POSTURL, postBody, MIMEType.TEXT_JSON, "TLS12");

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                 "POST Response: " + POSTResponse);


            TnGOrderQueryResponse TNGresponse = new TnGOrderQueryResponse();
            OrderQueryResponse oFResponse = new OrderQueryResponse();
            OnlineProvider onlineProvider = new OnlineProvider();
            try
            {
                TNGresponse = JsonConvert.DeserializeObject<TnGOrderQueryResponse>(POSTResponse);

                oFResponse = TNGresponse.TnGOQResponse;
                //if (generateSignatureTnG(JsonConvert.SerializeObject(oFResponse)) != TNGresponse.Signature)
                //{
                //    return new HttpRequestMessage().CreateResponse(HttpStatusCode.BadRequest, oFResponse.Body.merchantTransId + "Invalid Signature");
                //}
            }
            catch (Exception ex)
            {
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
            ex.Message);
            }
            if (String.IsNullOrEmpty(oFResponse.Body.merchantTransId))
            {
                oFResponse.Body.merchantTransId = request.merchantTransId;
                oFResponse.Body.statusDetail = new StatusDetail
                {
                    acquirementStatus = StatusDetailEnum.CLOSED
                };

                returnMsg.Content = new StringContent("An error occured, please refer to the support for details");
                return returnMsg;

            }

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
           "transid" + oFResponse.Body.merchantTransId);
            OBReqTxn reqTxn = onlineProvider.GetReqTxn(oFResponse.Body.merchantTransId);

            PurchaseTNGService tNGService = new PurchaseTNGService();

            if (reqTxn != null)
            {
                Host hostInfo = onlineProvider.GetHost(ISSUING_BANK);

                if (oFResponse.Body.statusDetail.acquirementStatus.ToString() == "SUCCESS")
                {
                    reqTxn.TxnStatus = (Convert.ToInt32(Common.TXN_STATUS.TXN_SUCCESS)).ToString();
                    reqTxn.TxnState = (Convert.ToInt32(Common.TXN_STATE.COMMIT)).ToString();
                    reqTxn.TxnMsg = Common.C_TXN_SUCCESS_0;
                }
                else if (oFResponse.Body.statusDetail.acquirementStatus.ToString() == "INIT")
                {
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                        "[Response TnGOrderQuery][" + oFResponse.Head.function + "]" + " Transaction " + oFResponse.Body.merchantTransId + " is still incomplete");
                    returnMsg.Content = new StringContent(oFResponse.Body.statusDetail.acquirementStatus.ToString());
                    return returnMsg;
                }
                else
                {
                    reqTxn.TxnStatus = (Convert.ToInt32(Common.TXN_STATUS.TXN_FAILED)).ToString();
                    reqTxn.TxnState = (Convert.ToInt32(Common.TXN_STATE.COMMIT_FAILED)).ToString();
                    reqTxn.TxnMsg = Common.C_TXN_FAILED_1;
                }
                if (hostInfo == null)
                {
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                       "[Response TnGOrderQuery][" + oFResponse.Head.function + "][DB Host is Empty]" + Newtonsoft.Json.JsonConvert.SerializeObject(oFResponse));


                    returnMsg.Content = new StringContent("Bad request");
                    return returnMsg;
                }
                else
                {

                    reqTxn.CardPAN = "";
                    reqTxn.BankRefNo = oFResponse.Head.reqMsgId;
                    reqTxn.TxnMsg = oFResponse.Body.resultInfo.resultMsg;
                    reqTxn.GatewayTxnID = oFResponse.Body.merchantTransId;
                    reqTxn.GatewayID = "";

                    if (2 == onlineProvider.InsertTxnResp(reqTxn, TXN_TYPE))
                    {
                        OBResTxn resTxn = onlineProvider.GetResTxn(oFResponse.Body.merchantTransId);
                        if (resTxn != null)
                        {

                            resTxn.RespMesg = oFResponse.Body.statusDetail.acquirementStatus.ToString();
                            Merchant merchant = onlineProvider.GetMerchant(resTxn.MerchantID);
                            Host host = onlineProvider.GetHost(ISSUING_BANK);
                            //if (merchant.NotifyEmailAddr == null)
                            //{
                            //    merchant.NotifyEmailAddr = merchant.EmailAddr;                              
                            //}
                            if (oFResponse.Body.statusDetail.acquirementStatus.ToString() == "SUCCESS")
                            {
                                tNGService.SendEmailNotification(reqTxn, resTxn, host, merchant, Common.EmailTemplate.EMAILTEMPLATESHOPPER);
                                tNGService.SendEmailNotification(reqTxn, resTxn, host, merchant, Common.EmailTemplate.EMAILTEMPLATEMERCHANT);
                            }

                            string htmlContent = tNGService.TnGMerchantPayResp(host, resTxn, reqTxn);
                            if (!String.IsNullOrEmpty(resTxn.MerchantCallbackURL))
                            {
                                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "[Response TnGResp][Posting to Merchant CallbackURL][" + resTxn.MerchantCallbackURL + "]");

                                MerchantCallback merchantCallback = tNGService.TnGRetrieveMerchantParameters(host, resTxn, reqTxn);
                                string httpString = tNGService.TnGGetReplyMerchantCallbackHTTPString(merchantCallback);
                                int callBackStatus = 0;

                                for (int t = 0; t <= 4; t++) //Callback 5 times if failed get response from callback url
                                {
                                    string result = HttpHelper.HttpPostReadWithData(resTxn.MerchantCallbackURL, httpString, MIMEType.URL_ENCODED, "TLS12");

                                    if (result != null)
                                    {
                                        callBackStatus = 1;

                                        t = 4;
                                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                            "[Response TnGResp][Callback response]["
                                        + Newtonsoft.Json.JsonConvert.SerializeObject(merchantCallback) + "], Status [success and recv acknowledgement (" + result + ")" + "  ....)]" +
                                        "Protocol[" + merchant.Protocol + "]");
                                    }
                                    else
                                    {
                                        callBackStatus = 0;
                                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                         "[Response TnGResp][Callback response]["
                                        + Newtonsoft.Json.JsonConvert.SerializeObject(merchantCallback) + "], Status [fail][Running: " + t.ToString() +
                                        "Protocol[" + merchant.Protocol + "]");
                                        //Thread.Sleep(50);

                                    }

                                }

                                //onlineProvider.InsertCallBackStatus(reqTxn, callBackStatus);

                                var webResponse = new HttpResponseMessage();
                                webResponse.Content = new StringContent(oFResponse.Body.statusDetail.acquirementStatus.ToString());
                                webResponse.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
                                //objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                //            "htmlcontent:   " + htmlContent);

                                return webResponse;
                            }

                        }
                    }
                    returnMsg.Content = new StringContent("An error occured, please refer to the support for details");
                    return returnMsg;
                }

            }
            else
            {
                OBResTxn resTxn = onlineProvider.GetResTxn(oFResponse.Body.merchantTransId);
                if (resTxn == null)
                {
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION,
                            DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "Failed to find req and res");
                }
                else
                {

                    Merchant merchant = onlineProvider.GetMerchant(resTxn.MerchantID);

                    Host host = onlineProvider.GetHost(ISSUING_BANK);
                    if (resTxn.RespMesg == null || resTxn.RespMesg == "")
                    {
                        resTxn.RespMesg = oFResponse.Body.statusDetail.acquirementStatus.ToString();
                    }
                    string htmlContent = tNGService.TnGMerchantPayResp(host, resTxn, reqTxn);

                    if (!String.IsNullOrEmpty(resTxn.MerchantCallbackURL))
                    {
                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                       "[Response TnGResp][Posting to Merchant CallbackURL][" + resTxn.MerchantCallbackURL + "]");

                        MerchantCallback merchantCallback = tNGService.TnGRetrieveMerchantParameters(host, resTxn, reqTxn);
                        string httpString = tNGService.TnGGetReplyMerchantCallbackHTTPString(merchantCallback);
                        int callBackStatus = 0;

                        for (int t = 0; t <= 4; t++) //Callback 5 times if failed get response from callback url
                        {
                            string result = HttpHelper.HttpPostReadWithData(resTxn.MerchantCallbackURL, httpString, MIMEType.URL_ENCODED, "TLS12");

                            if (result != null)
                            {
                                callBackStatus = 1;

                                t = 4;
                                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                    "[Response TnGResp][Callback response]["
                                + Newtonsoft.Json.JsonConvert.SerializeObject(merchantCallback) + "], Status [success and recv acknowledgement (" + result + ")" + "  ....)]" +
                                "Protocol[" + merchant.Protocol + "]");
                            }
                            else
                            {
                                callBackStatus = 0;
                                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                 "[Response TnGResp][Callback response]["
                                + Newtonsoft.Json.JsonConvert.SerializeObject(merchantCallback) + "], Status [fail][Running: " + t.ToString() +
                                "Protocol[" + merchant.Protocol + "]");
                                //Thread.Sleep(50);

                            }

                        }

                        //onlineProvider.InsertCallBackStatus(reqTxn, callBackStatus);

                        var webResponse = new HttpResponseMessage();
                        webResponse.Content = new StringContent(htmlContent);
                        webResponse.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
                        //objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                        //            "htmlcontent:   " + htmlContent);

                        return webResponse;
                    }
                }


            }
            returnMsg.Content = new StringContent("An error occured, please refer to the support for details");
            return returnMsg;
        }

        public HttpResponseMessage CommitOrderRefund(TnGOrderRefundRequest request)
        {
            var webResponse = new HttpResponseMessage();
            var Config = new ConfigurationProvider().GetConfig("Paydibs.PaymentGatewayAPI");
            Logger logger = new Logger();
            CLoggerII objLoggerII = new CLoggerII();
            objLoggerII = logger.InitLogger();
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
    "Refund Request: " + JsonConvert.SerializeObject(request.TnGORRequest));
            string input = JsonConvert.SerializeObject(request.TnGORRequest);
            
            request.Signature = generateSignatureTnG(input);

            string postBody = JsonConvert.SerializeObject(request, new JsonSerializerSettings() { ContractResolver = new CamelCasePropertyNamesContractResolver() });

            string POSTURL = Config.TNG_RefundUrl;

            var POSTResponse = "";
            try {
                POSTResponse = HttpHelper.HttpPost(POSTURL, postBody, MIMEType.TEXT_JSON, "TLS12");
            }

            catch (Exception ex)
            {
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
    ex.Message);
                webResponse.Content = new StringContent("Failed refund, error unidentified, possibly caused by unresponsive server or invalid input");
                return webResponse;
            }

            webResponse.Content = new StringContent(JValue.Parse(POSTResponse).ToString(Formatting.Indented));

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
    "Refund Response: " + JsonConvert.SerializeObject(POSTResponse));
            //webResponse.Content.Headers.ContentType = new MediaTypeHeaderValue("text/json");
            return webResponse;
        }

        public HttpResponseMessage CommitOrderQueryInstant(TNGPayReturn request)
        {
            var webResponse = new HttpResponseMessage();
            Logger logger = new Logger();
            CLoggerII objLoggerII = new CLoggerII();
            objLoggerII = logger.InitLogger();
            const string ISSUING_BANK = "TNG";
            const string TXN_TYPE = "PAY";

            var TnGFunction = "alipayplus.acquiring.order.query";

            var Config = new ConfigurationProvider().GetConfig("Paydibs.PaymentGatewayAPI");

            TnGOrderQueryRequest OQRequest = new TnGOrderQueryRequest
            {
                TnGOQRequest = new OrderQueryRequest
                {
                    Head = new OrderQueryRequestHead
                    {
                        clientId = request.clientId,
                        clientSecret = Config.TNG_clientSecret,
                        function = TnGFunction,
                        reqTime = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz"),
                        reqMsgId = request.merchantId + request.merchantTransId,
                        version = request.version,
                        reserve = "{ \"attr1\":\"val1\" }"
                    },
                    Body = new OrderQueryRequestBody
                    {
                        //acquirementId = request.TnGOFRequest.Body.acquirementId,
                        merchantId = request.merchantId,
                        merchantTransId = request.merchantTransId
                    }
                }
            };
          
            string input = JsonConvert.SerializeObject(OQRequest.TnGOQRequest);

            OQRequest.Signature = generateSignatureTnG(input);

            string postBody = JsonConvert.SerializeObject(OQRequest, new JsonSerializerSettings() { ContractResolver = new CamelCasePropertyNamesContractResolver() });

            string msg = "[Paydibs PaymentGatewayAPI PurchaseTNGService.CommitOrderQuery][" + TnGFunction + "]" + DateTime.Now + ": [TNG Request: " + postBody + "]";
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                msg);

            string POSTURL = Config.TNG_QueryUrl;
            var POSTResponse = HttpHelper.HttpPost(POSTURL, postBody, MIMEType.TEXT_JSON, "TLS12");
            var postedResponse = JsonConvert.DeserializeObject<TnGOrderQueryResponse>(POSTResponse);
            //webResponse.Content = new StringContent(JValue.Parse(POSTResponse).ToString(Formatting.Indented));
            msg = "[Paydibs PaymentGatewayAPI PurchaseTNGService.CommitOrderQuery][" + TnGFunction + "]" + DateTime.Now + ": [TNG Response: " + POSTResponse + "]";
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                msg);
            var response = new TnGOrderCheckingResponse
            {
                acquirementId = postedResponse.TnGOQResponse.Body.acquirementId,
                amountDetail = postedResponse.TnGOQResponse.Body.amountDetail,
                createdTime = postedResponse.TnGOQResponse.Body.timeDetail.createdTime,
                merchantTransId = postedResponse.TnGOQResponse.Body.merchantTransId,
                resultCode = postedResponse.TnGOQResponse.Body.resultInfo.resultCode,
                resultStatus = postedResponse.TnGOQResponse.Body.statusDetail.acquirementStatus.ToString()
            };
            webResponse.Content = new StringContent(JValue.Parse(JsonConvert.SerializeObject(response)).ToString(Formatting.Indented));
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
    "Query Response: " + JsonConvert.SerializeObject(POSTResponse));
            //webResponse.Content.Headers.ContentType = new MediaTypeHeaderValue("text/json");
            return webResponse;
        }

            private int SendEmailNotification(OBReqTxn reqTxn, OBResTxn resTxn, Host host, Merchant merchant, Common.EmailTemplate emailTemplate)
        {
            int iMailStatus = -1;
            string szHTMLContent = "";
            string[] szReceiptTemplate = null;

            string szReceiptEmailBody = string.Empty;

            Logger logger = new Logger();
            CLoggerII objLoggerII = new CLoggerII();
            objLoggerII = logger.InitLogger();

            Mail objMail = new Mail();
            string szTemplatePath = objMail.GetTemplate(emailTemplate);
            szReceiptTemplate = szTemplatePath.Split('.');
            szTemplatePath = szReceiptTemplate[0] + "_" + Common.C_DEFAULT_LANGUAGE_CODE + "." + szReceiptTemplate[1];
                        string htmlContent = string.Empty;

            if (szTemplatePath != string.Empty)
            {
                htmlContent = GetTemplate(szReceiptTemplate[0] + "_" + Common.C_DEFAULT_LANGUAGE_CODE + "." + szReceiptTemplate[1], szHTMLContent);
                if (string.IsNullOrEmpty(htmlContent))
                {
                    return iMailStatus;
                }
            }
            htmlContent = objMail.LoadHtmlContent(reqTxn, resTxn, host, merchant, htmlContent);
            try
            {

                string recipientEmail = string.Empty;
                if (emailTemplate == Common.EmailTemplate.EMAILTEMPLATESHOPPER)
                {
                    iMailStatus = objMail.SendMail(resTxn.CustEmail, htmlContent, "Payment Confirmation to [" + merchant.MerchantName + "]");
                    if (iMailStatus == 1)
                    {
                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                        resTxn.GatewayTxnID + " Succesfully send notification email to shopper");
                    }
                    else
                    {
                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                        resTxn.GatewayTxnID + " Failed to send notification email to shopper");
                    }
                }
                else if (emailTemplate == Common.EmailTemplate.EMAILTEMPLATEMERCHANT)
                {
                    iMailStatus = objMail.SendMail(merchant.NotifyEmailAddr, htmlContent, "Payment Confirmation to [" + resTxn.MerchantTxnID + "]");
                    if (iMailStatus == 1)
                    {
                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                        resTxn.GatewayTxnID + " Succesfully send notification email to merchant");
                    }
                    else
                    {
                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                        resTxn.GatewayTxnID + " Failed to send notification email to merchant");
                    }
                }
                return iMailStatus;
            }
            catch (Exception ex)
            {
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                    "[SendEmailNotification Exception: " + ex.ToString() + "]");

                return iMailStatus;
            }
        }

        private string GetTemplate(string sz_FilePath, string szHTMLContent)
        {
            StreamReader objStreamReader = null;
            string szTemplateFile = "";

            try
            {
                szTemplateFile = sz_FilePath;
                objStreamReader = System.IO.File.OpenText(szTemplateFile);
                szHTMLContent = objStreamReader.ReadToEnd();
                objStreamReader.Close();
            }
            catch (Exception ex)
            {
                Logger logger = new Logger();
                CLoggerII objLoggerII = new CLoggerII();
                objLoggerII = logger.InitLogger();
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                 "[Response GetTemplate() exception: " + ex.ToString() + "]");

                return szHTMLContent;
            }
            //finally
            //{
            //    if (objStreamReader != null)
            //        objStreamReader = null;
            //}
            return szHTMLContent;
        }

        private string TnGMerchantPayResp(Host host, OBResTxn resTxn, OBReqTxn reqTxn)
        {
            var onlineProvider = new OnlineProvider();
            string szHTMLContent = string.Empty;
            try
            {
                Merchant merchant = onlineProvider.GetMerchant(resTxn.MerchantID);
                Logger logger = new Logger();
                CLoggerII objLoggerII = new CLoggerII();
                objLoggerII = logger.InitLogger();
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                "[TNGSales Response][" + resTxn.GatewayTxnID + "][[Paydibs PaymentGatewayAPI PurchaseTNGService-MerchantPayResp][" + Newtonsoft.Json.JsonConvert.SerializeObject(merchant) + "]");
                
                //iSendReply2Merchant();
                //st_PayInfo.szHashValue2 = objHash.szGetSaleResHash2(st_PayInfo)

                //notify status?
                //NotifyStatus notifyStatus = PaymentProvider.CheckCallBackStatus(resTxn.GatewayTxnID);

                //if (notifyStatus.CallBackStatus != "1")
                //{
                int HttpTimeoutSeconds = Convert.ToInt32(ConfigurationManager.AppSettings["HttpTimeoutSeconds"]);

                //Hash Value for pay response
                Hash hash = new Hash();
                resTxn.HashValue2 = hash.GetSaleResHash2OB(resTxn, merchant);

                string HTTPString = TnGGetReplyMerchantCallbackHTTPString(host, resTxn, reqTxn);
               


                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                "[TNGSales Response][" + resTxn.GatewayTxnID + "][PurchaseTNGService-MerchantPayResp-szGetReplyMerchantCallbackHTTPString][" + HTTPString + "]");

                if ((resTxn.MerchantReturnURL != ""))
                {
                    string sz_HTML = string.Empty;

                    //post to merchant page
                    MerchantCallback merchantCallback = TnGRetrieveMerchantParameters(host, resTxn, reqTxn);
                    string postRequest = merchantCallback.ToUrlEncoded();
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                    "[TNGSales Response][" + resTxn.GatewayTxnID + "][PurchaseTNGService-RetrieveMerchantParameters-POSTINGTOMerchantReturnURL][" + resTxn.MerchantReturnURL + ": " + postRequest + "]");
                    HttpHelper.HttpPost(resTxn.MerchantReturnURL, postRequest, MIMEType.URL_ENCODED, "TLS12");
                    //Display paydibs receipt
                    szHTMLContent = TnGLoadReceiptUI(resTxn.MerchantReturnURL, HTTPString, sz_HTML, host, resTxn, reqTxn, merchant);
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                    "[TNGSales Response][" + resTxn.GatewayTxnID + "][LoadReceiptUI][" + resTxn.MerchantID + resTxn.MerchantTxnID + " > Redirected to [" + resTxn.MerchantReturnURL + "] & Load HTML Content");

                    return szHTMLContent;
                }
                return szHTMLContent;
            }
            catch (Exception ex)
            {
                Logger logger = new Logger();
                CLoggerII objLoggerII = new CLoggerII();
                objLoggerII = logger.InitLogger();
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                 "[Paydibs PaymentGatewayAPI PurchaseTNGService-MerchantPayResp][Exception: " + ex.ToString() + "]");

                return szHTMLContent;
            }
        }

        private string TnGGetReplyMerchantCallbackHTTPString(Host host, OBResTxn resTxn, OBReqTxn reqTxn)
        {
            string szHTTPString = string.Empty;
            const string TXN_TYPE = "PAY";
            const string PYMT_METHOD = "WA";
            //What is 1 and 4? Refer on Legacy Checkout
            if (("1" == host.HostReplyMethod | "4" == host.HostReplyMethod))
            {
                szHTTPString = "<INPUT type='hidden' name='TxnType' value='" + HttpUtility.UrlEncode(TXN_TYPE) + "'>" + System.Environment.NewLine;
                szHTTPString = szHTTPString + "<INPUT type='hidden' name='Method' value='" + PYMT_METHOD + "'>" + System.Environment.NewLine;     // Added, 16 Aug 2013; Modified 9 Dec 2013, add Method as variable instead of 'CC'
                szHTTPString = szHTTPString + "<INPUT type='hidden' name='MerchantID' value='" + resTxn.MerchantID + "'>" + System.Environment.NewLine;
                szHTTPString = szHTTPString + "<INPUT type='hidden' name='MerchantPymtID' value='" + resTxn.MerchantTxnID + "'>" + System.Environment.NewLine;
                szHTTPString = szHTTPString + "<INPUT type='hidden' name='MerchantOrdID' value='" + HttpUtility.UrlEncode(resTxn.OrderNumber) + "'>" + System.Environment.NewLine;    // Added, 16 Aug 2013

                // Modified 2 Sept 2014, use szTxnAmount and szCurrencyCode instead of st_PayInfo.szTxnAmount, for FX
                szHTTPString = szHTTPString + "<INPUT type='hidden' name='MerchantTxnAmt' value='" + resTxn.TxnAmount + "'>" + System.Environment.NewLine;
                szHTTPString = szHTTPString + "<INPUT type='hidden' name='MerchantCurrCode' value='" + resTxn.CurrencyCode + "'>" + System.Environment.NewLine;

                szHTTPString = szHTTPString + "<INPUT type='hidden' name='Sign' value='" + (resTxn.HashValue2) + "'>" + System.Environment.NewLine;                          // Added, 28 Apr 2016, response hash value 2 which includes OrderNumber and Param6 because some plugins like Magento update order based on these 2 fields
                szHTTPString = szHTTPString + "<INPUT type='hidden' name='PTxnID' value='" + resTxn.GatewayTxnID + "'>" + System.Environment.NewLine;

                //if (("CC" == reqTxn.))
                //{
                //szHTTPString = szHTTPString + "<INPUT type='hidden' name='AcqBank' value='" + Server.UrlEncode(Common.C_TXN_CC_ACQBANK) + "'>" + System.Environment.NewLine;    // Added, 16 Aug 2013
                //szHTTPString = szHTTPString + "<INPUT type='hidden' name='AcqBank' value='" + Server.UrlEncode(st_PayInfo.szIssuingBank) + "'>" + System.Environment.NewLine;    // Added, 16 Aug 2013
                //}
                szHTTPString = szHTTPString + "<INPUT type='hidden' name='PTxnStatus' value='" + resTxn.TxnStatus + "'>" + System.Environment.NewLine;

                //if (("" != resTxn.AuthCode))
                //{
                //    szHTTPString = szHTTPString + "<INPUT type='hidden' name='AuthCode' value='" + HttpUtility.UrlEncode(resTxn.AuthCode) + "'>" + System.Environment.NewLine;
                //}

                szHTTPString = szHTTPString + "<INPUT type='hidden' name='BankRefNo' value='" + HttpUtility.UrlEncode(resTxn.BankRefNo) + "'>" + System.Environment.NewLine;    // Added, 10 Sept 2013

                //Added, 7 Oct 2015, returning card data, requested by MyDin
                //Added, 1 Aug 2014, One-Click Payment
                //Added 30 Nov 2015, Promotion
                //Added  6 Nov 2015, Param67
                //Added  24 May 2016, Installment

                szHTTPString = szHTTPString + "<INPUT type='hidden' name='PTxnMsg' value='" + resTxn.RespMesg + "'>" + System.Environment.NewLine;       // Modified 22 May 2017, removed EscapeDataString
                //sz_LogString = szHTTPString;
            }
            return szHTTPString;
            //if (("CC" == szPymtMethod.ToUpper() & 1 <= st_PayInfo.iAllowOCP & "1" == st_PayInfo.szCustOCP & 
            //    Common.TXN_STATUS.TXN_SUCCESS = st_PayInfo.iTxnStatus | 
            //    Common.TXN_STATUS.TXN_AUTH_SUCCESS == st_PayInfo.iTxnStatus;/* TODO ERROR: Skipped SkippedTokensTrivia *//* TODO ERROR: Skipped SkippedTokensTrivia */))
            //{

            //}

        }

        private MerchantCallback TnGRetrieveMerchantParameters(Host host, OBResTxn resTxn, OBReqTxn reqTxn)
        {
            const string TXN_TYPE = "PAY";
            const string PYMT_METHOD = "WA";

            MerchantCallback merchantCallback = new MerchantCallback();
            merchantCallback.TxnType = TXN_TYPE;
            merchantCallback.Method = PYMT_METHOD;
            merchantCallback.MerchantID = resTxn.MerchantID;
            merchantCallback.MerchantPymtID = HttpUtility.UrlEncode(resTxn.MerchantTxnID);
            merchantCallback.MerchantOrdID = HttpUtility.UrlEncode(resTxn.OrderNumber);
            merchantCallback.MerchantTxnAmt = resTxn.TxnAmount;
            merchantCallback.MerchantCurrCode = resTxn.CurrencyCode;
        
            merchantCallback.Sign = resTxn.HashValue2;
            merchantCallback.PTxnID = resTxn.GatewayTxnID;
            merchantCallback.PTxnStatus = resTxn.TxnStatus.ToString();
            merchantCallback.BankRefNo = HttpUtility.UrlEncode(resTxn.BankRefNo);
            merchantCallback.PTxnMsg = resTxn.RespMesg;

            return merchantCallback;
        }

        private string TnGLoadReceiptUI(string merchantReturnURL, string hTTPString, string sz_HTML, Host host, OBResTxn resTxn, OBReqTxn reqTxn, Merchant merchant)
        {
            string szTemplatePath = "";
            string szHTMLContent = "";
            string szTxnStatus = "";
            string[] szReceiptTemplate;
            string szPymtMethod = "WA";         // Added, 9 Dec 2013
            string szCCDetails = "";          // Added, 9 Dec 2013
            string szHideReceipt = "true";    // Added, 15 Dec 2013
            string szHideMerchantLogo = "";
            string szHideMerchantAddr = "";
            const string ISSUING_BANK = "TNG";

            //TODO: put in web config
            szTemplatePath = ConfigurationManager.AppSettings["TemplateFilePath"];
            Logger logger = new Logger();
            CLoggerII objLoggerII = new CLoggerII();
            objLoggerII = logger.InitLogger();
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                "[TNGSales Response][" + resTxn.GatewayTxnID + "][LoadReceiptUI][" + resTxn.MerchantID + resTxn.MerchantTxnID + " > [PurchaseTNGService][LoadReceiptUI]Load Template File to [" + resTxn.MerchantReturnURL + "] " + szHTMLContent);

            if (szTemplatePath != "")
            {
                szReceiptTemplate = szTemplatePath.Split('.');
                szTemplatePath = szReceiptTemplate[0] + "_" + resTxn.LanguageCode + "." + szReceiptTemplate[1];
                szHTMLContent = TnGGetTemplate(szTemplatePath, szHTMLContent, resTxn, reqTxn, merchant);

                if ((string.IsNullOrEmpty(szHTMLContent)))
                {
                    //if ((string.IsNullOrEmpty(GetTemplate(szReceiptTemplate[0] + "_" + Common.C_DEFAULT_LANGUAGE_CODE + "." + szReceiptTemplate[1], szHTMLContent, resTxn, reqTxn, merchant))))
                    //{
                    //iReturn = -1;
                    return szHTMLContent;
                    //}
                }
            }

            if (szHTMLContent != "")
            {
                if ((merchantReturnURL != ""))
                {
                    if ((int)Common.TXN_STATUS.TXN_SUCCESS == resTxn.TxnStatus)
                    {
                        szTxnStatus = "Successful"; // 'Modified 28 Jan 2014, Success to Successful
                        szHideReceipt = "false"; //'Added, 15 Dec 2013
                    }
                    else if ((int)Common.TXN_STATUS.TXN_AUTH_SUCCESS == resTxn.TxnStatus)
                    {
                        szTxnStatus = "Pre-Auth Successful";
                        szHideReceipt = "false";
                    }
                    else if ((int)Common.TXN_STATUS.TXN_FAILED == resTxn.TxnStatus ||
                        (int)Common.TXN_STATUS.INVALID_HOST_REPLY == resTxn.TxnStatus ||
                        (int)Common.TXN_STATUS.DECLINED_BY_VEENROLL == resTxn.TxnStatus)
                    {
                        szTxnStatus = "Failed. Please Try Again.";   //'Modified 25 Jul 2014, added "Please Try Again."
                    }
                    else if ((int)Common.TXN_STATUS.DECLINED_BY_EXT_FDS == resTxn.TxnStatus)       //  ' 25 May 2017
                    {
                        szTxnStatus = "Declined";
                    }
                    else
                    {
                        szTxnStatus = "Pending";
                    }

                    if (("0" == merchant.ShowMerchantLogo))
                    {
                        szHideMerchantLogo = "hide";           // 'Bootstrap hide class
                    }

                    if (("0" == merchant.ShowMerchantAddr))
                    {
                        szHideMerchantAddr = "hide";
                    }

                    szHTMLContent = TnGszFormatPlaceHolder(szHTMLContent, "[MERCHANTADDR]", merchant.Addr1 + " " + merchant.Addr2 + " " + merchant.Addr3);            // Added, 14 Dec 2013
                    szHTMLContent = TnGszFormatPlaceHolder(szHTMLContent, "[MERCHANTCONTACTNO]", merchant.ContactNo);     // Added, 14 Dec 2013
                    szHTMLContent = TnGszFormatPlaceHolder(szHTMLContent, "[MERCHANTEMAILADDR]", merchant.EmailAddr);     // Added, 14 Dec 2013
                    szHTMLContent = TnGszFormatPlaceHolder(szHTMLContent, "[MERCHANTWEBSITEURL]", merchant.WebSiteURL);   // Added, 14 Dec 2013

                    // st_PayInfo.szHostDate ---> resTxn.DateCreated?
                    if (("" != resTxn.DateCreated & ("FPX" == TnGLeft(ISSUING_BANK.Trim(), 3).ToUpper() | ("FFFPX" == ISSUING_BANK.Trim().ToUpper()))))
                    {
                        DateTime dt;
                        dt = DateTime.ParseExact(resTxn.DateCreated, "yyyyMMddHHmmss", CultureInfo.CreateSpecificCulture("en-us"));
                        szHTMLContent = TnGszFormatPlaceHolder(szHTMLContent, "[TXNDATETIME]", dt.ToString("dd MMM yyyy hh:mm:ss tt", CultureInfo.CreateSpecificCulture("en-us")));  // Added 6 Nov 2017, cater FPX B2B Receipt Date Time
                    }
                    else
                    {
                        szHTMLContent = TnGszFormatPlaceHolder(szHTMLContent, "[TXNDATETIME]", System.DateTime.Now.ToString("dd MMM yyyy hh:mm:ss tt", CultureInfo.CreateSpecificCulture("en-us")));  // Added, 9 Dec 2013; Modified 17 Feb 2014, added CultureInfo or else some machine not able to display AM/PM
                    }
                    szHTMLContent = TnGszFormatPlaceHolder(szHTMLContent, "[PYMTMETHOD]", szPymtMethod);                                          // Added, 9 Dec 2013
                    szHTMLContent = TnGszFormatPlaceHolder(szHTMLContent, "[CCDETAILS]", szCCDetails);                                            // Added, 9 Dec 2013
                    szHTMLContent = TnGszFormatPlaceHolder(szHTMLContent, "[MERCHANTNAME]", merchant.MerchantName);
                    szHTMLContent = TnGszFormatPlaceHolder(szHTMLContent, "[ORDERDESC]", resTxn.OrderDesc);
                    szHTMLContent = TnGszFormatPlaceHolder(szHTMLContent, "[TXNAMOUNT]", resTxn.TxnAmount);
                    szHTMLContent = TnGszFormatPlaceHolder(szHTMLContent, "[CURRENCYCODE]", resTxn.CurrencyCode);
                    szHTMLContent = TnGszFormatPlaceHolder(szHTMLContent, "[MERCHANTTXNID]", resTxn.MerchantTxnID);
                    szHTMLContent = TnGszFormatPlaceHolder(szHTMLContent, "[ORDERNUMBER]", resTxn.OrderNumber);                 // Added, 14 Dec 2013
                    szHTMLContent = TnGszFormatPlaceHolder(szHTMLContent, "[GATEWAYTXNID]", resTxn.GatewayTxnID);
                    szHTMLContent = TnGszFormatPlaceHolder(szHTMLContent, "[BANKREFNO]", resTxn.BankRefNo);                     // Added, 1 Jun 2015
                    szHTMLContent = TnGszFormatPlaceHolder(szHTMLContent, "[ISSUINGBANK]", ISSUING_BANK);
                    szHTMLContent = TnGszFormatPlaceHolder(szHTMLContent, "[HOSTTXNID]", resTxn.BankRefNo);
                    szHTMLContent = TnGszFormatPlaceHolder(szHTMLContent, "[TXNSTATUS]", szTxnStatus);
                    szHTMLContent = TnGszFormatPlaceHolder(szHTMLContent, "[HOSTPARAMS]", hTTPString);
                    szHTMLContent = TnGszFormatPlaceHolder(szHTMLContent, "[HOSTURL]", resTxn.MerchantReturnURL);// to do: double check sz_URL); 
                    szHTMLContent = TnGszFormatPlaceHolder(szHTMLContent, "[REDIRECTMS]", ConfigurationManager.AppSettings["ReceiptRedirectMS"]);
                    szHTMLContent = TnGszFormatPlaceHolder(szHTMLContent, "[REDIRECTSEC]", TnGLeft(ConfigurationManager.AppSettings["ReceiptRedirectMS"], ConfigurationManager.AppSettings["ReceiptRedirectMS"].Length - 3));
                    szHTMLContent = TnGszFormatPlaceHolder(szHTMLContent, "[ISHIDERECEIPT]", szHideReceipt);                          // Added, 15 Dec 2013
                    szHTMLContent = TnGszFormatPlaceHolder(szHTMLContent, "[MERCHANTID]", resTxn.MerchantID);                     // Added, 24 Jun 2014, to show Merchant logo on receipt page
                    szHTMLContent = TnGszFormatPlaceHolder(szHTMLContent, "[HIDEMERCHANTLOGO]", szHideMerchantLogo);                  // 04 may 2017
                    szHTMLContent = TnGszFormatPlaceHolder(szHTMLContent, "[HIDEMERCHANTADDR]", szHideMerchantAddr);                  // 04 may 2017

                }
                else
                {
                    return szHTMLContent;
                }

                sz_HTML = szHTMLContent;
            }
            else
            {
                return szHTMLContent;
            }
            return szHTMLContent;
        }

        private string TnGGetTemplate(string szTemplatePath, string szHTMLContent, OBResTxn resTxn, OBReqTxn reqTxn, Merchant merchant)
        {
            StreamReader objStreamReader = null;       // Modified Joyce 4 Apr 2019,  add "= Nothing" as requested by PCI
            string szTemplateFile = "";
            bool bReturn = true;

            try
            {
                szTemplateFile = szTemplatePath;

                // Load page
                Logger logger = new Logger();
                CLoggerII objLoggerII = new CLoggerII();
                objLoggerII = logger.InitLogger();
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(), resTxn.MerchantID + resTxn.MerchantTxnID + " > Load (" + szTemplateFile + ")");

                objStreamReader = System.IO.File.OpenText(szTemplateFile);
                szHTMLContent = objStreamReader.ReadToEnd();
                objStreamReader.Close();

                return szHTMLContent;
            }
            catch (Exception ex)
            {
                Logger logger = new Logger();
                CLoggerII objLoggerII = new CLoggerII();
                objLoggerII = logger.InitLogger();
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                 " > Invalid Template File Path Exception  (" + ex.ToString() + ")");

                return string.Empty;
            }
        }

        public string TnGszFormatPlaceHolder(string szOriText, string szTextToFind, string szTextToReplace)
        {
            if (szOriText.Contains(szTextToFind))
            {
                if ((szTextToReplace.Trim().Length > 0))
                {
                    szOriText = szOriText.Replace(szTextToFind, szTextToReplace);
                    return szOriText;
                }
                else
                {
                    szOriText = szOriText.Replace(szTextToFind, "");
                    return szOriText;
                }
            }
            return szOriText;
        }

        public string TnGLeft(string value, int maxLength)
        {
            if (string.IsNullOrEmpty(value)) return value;
            maxLength = Math.Abs(maxLength);

            return (value.Length <= maxLength
                   ? value
                   : value.Substring(0, maxLength)
                   );
        }

        private string TnGGetReplyMerchantCallbackHTTPString(MerchantCallback merchantCallback)
        {
            const string TXN_TYPE = "PAY";
            const string PYMT_METHOD = "WA";
            Dictionary<string, string> g_JSONResp = new Dictionary<string, string>();
            g_JSONResp.Add("TxnType", TXN_TYPE); // Server.UrlEncode(st_PayInfo.szTxnType));  
            g_JSONResp.Add("Method", PYMT_METHOD);
            g_JSONResp.Add("MerchantID", merchantCallback.MerchantID);
            g_JSONResp.Add("MerchantPymtID", HttpUtility.UrlEncode(merchantCallback.MerchantPymtID));
            g_JSONResp.Add("MerchantOrdID", HttpUtility.UrlEncode(merchantCallback.MerchantOrdID));
            g_JSONResp.Add("MerchantTxnAmt", merchantCallback.MerchantTxnAmt);
            g_JSONResp.Add("MerchantCurrCode", HttpUtility.UrlEncode(merchantCallback.MerchantCurrCode));

            g_JSONResp.Add("PTxnID",/* merchantCallback.MerchantID +*/ merchantCallback.PTxnID);
            g_JSONResp.Add("PTxnStatus", merchantCallback.PTxnStatus.ToString());
            g_JSONResp.Add("PTxnMsg", System.Uri.EscapeDataString(merchantCallback.PTxnMsg));
            //g_JSONResp.Add("AcqBank", st_PayInfo.szIssuingBank);
            g_JSONResp.Add("BankRefNo", merchantCallback.BankRefNo); // Server.UrlEncode(st_PayInfo.szHostTxnID))
            g_JSONResp.Add("Sign", merchantCallback.Sign);
            if (("" != merchantCallback.AuthCode))
            {
                g_JSONResp.Add("AuthCode", HttpUtility.UrlEncode(merchantCallback.AuthCode));
            }

            System.Web.Script.Serialization.JavaScriptSerializer objJS = new System.Web.Script.Serialization.JavaScriptSerializer();
            string sz_LogString = objJS.Serialize(g_JSONResp);
            string szHTTPString = HttpUtility.UrlEncode(objJS.Serialize(g_JSONResp));
            return szHTTPString;

        }

        public string generateSignature(SalesOrderRequest request)
        {
            string input = "{\"head\":{\"version\":\""+ request.version +"\",\"function\":\""+ request.function +"\",\"clientId\":\"" + request.clientId +
                "\",\"clientSecret\":\"" + request.clientSecret + "\",\"reqTime\":\"" + request.reqTime + "\",\"reqMsgId\":\"" + request.reqMsgId +"\"}," +
                "\"body\":{\"order\":{\"orderTitle\":\""+ request.orderTitle +"\",\"orderAmount\":{\"currency\":\""+ request.currency +"\",\"value\":\""+ request.amount.ToString().Replace(".", string.Empty) + "\"}," +
                "\"merchantTransId\":\"" + request.onlineRefNum + "\",\"expiryTime\":\"" + request.expiryTime + "\"},\"merchantId\":\"" + request.merchantId + "\"," +
                "\"productCode\":\"" + request.productCode + "\",\"signAgreementPay\":false,\"envInfo\":{\"terminalType\":\"SYSTEM\",\"orderTerminalType\":\"APP\"}," +
                "\"notificationUrls\":[{\"url\":\""+ Config.TNG_URLPAY_RETURN +"\",\"type\":\"PAY_RETURN\"},{\"url\":\"" + request.notificationurl + "\",\"type\":\"NOTIFICATION\"}],\"extendInfo\":\"{\\\"PARTNER_TRANSACTION_ID\\\":\\\"10207635486872290000\\\"}\",\"subMerchantId\":\"TestingMerchantId\",\"subMerchantName\":\"PAYDIBS\"}}";

            string privateKey = Config.TNG_privatekey;
            ASCIIEncoding ByteConverter = new ASCIIEncoding();
            byte[] inputBytes = ByteConverter.GetBytes(input);

            byte[] inputHash = new SHA256CryptoServiceProvider().ComputeHash(inputBytes);

            byte[] privateKeyBytes = Convert.FromBase64String(privateKey
                .Replace("-----BEGIN RSA PRIVATE KEY-----", string.Empty)
                .Replace("-----END RSA PRIVATE KEY-----", string.Empty)
                .Replace("\n", string.Empty));

            RSACryptoServiceProvider rsa = RSAUtils.DecodeRSAPrivateKey(privateKeyBytes);
            byte[] output = rsa.SignHash(inputHash, "SHA256");
            return Convert.ToBase64String(output);
        }

        public string generateSignatureTnG(string input)
        {
            var Config = new ConfigurationProvider().GetConfig("Paydibs.PaymentGatewayAPI");

            string privateKey = Config.TNG_privatekey;

            ASCIIEncoding ByteConverter = new ASCIIEncoding();
            byte[] inputBytes = ByteConverter.GetBytes(input);

            byte[] inputHash = new SHA256CryptoServiceProvider().ComputeHash(inputBytes);

            byte[] privateKeyBytes = Convert.FromBase64String(privateKey
                .Replace("-----BEGIN RSA PRIVATE KEY-----", string.Empty)
                .Replace("-----END RSA PRIVATE KEY-----", string.Empty)
                .Replace("\n", string.Empty));

            RSACryptoServiceProvider rsa = RSAUtils.DecodeRSAPrivateKey(privateKeyBytes);
            byte[] output = rsa.SignHash(inputHash, "SHA256");

            return Convert.ToBase64String(output);
        }
    }
}
 
