﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PaymentGatewayAPI.Core.Models;
using System.Net;
using System.Net.Http.Formatting;
using System.Web;
using PaymentGatewayAPI.Core.Utilities;
using System.Reflection;
using Newtonsoft.Json;
using System.Security.Cryptography;
using System.IO;
using LoggerII;
using PaymentGatewayAPI.Core.Enum;
using PaymentGatewayAPI.Core.Configuration;
using PaymentGatewayAPI.Core.Providers;
using PaymentGatewayAPI.Core.UPPPaymentQueryRequestMessage0780_TestURL;
using PaymentGatewayAPI.Core.Utilities;
using System.Configuration;
using System.Globalization;
using System.Runtime.Serialization;
using System.Net.Http;
using System.Net.Http.Headers;

namespace PaymentGatewayAPI.Core.Services
{
    public class PurchasePayMasterPCIService
    {
        private const string ISSUING_BANK = "GOBIZPCI";
        private const string TXN_TYPE = "PAY";
        private const string PYMT_METHOD = "CC";
        private const string PaymentGatewayConfigurations = "Paydibs.PaymentGatewayAPI";
        private const string LoggingPaymentGatewayAPIPurchasePayMasterService = "[Paydibs PaymentGatewayAPI PurchasePayMasterPCIService]";
        private const string C_DEFAULT_LANGUAGE_CODE = "EN";

        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        private SystemConfiguration Config = null;
        private ConfigurationProvider _configurationProvider;
        private ConfigurationProvider ConfigurationProvider
        {
            get { return _configurationProvider ?? (_configurationProvider = new ConfigurationProvider()); }
        }

        private PaymentProvider _paymentProvider;
        private PaymentProvider PaymentProvider
        {
            get { return _paymentProvider ?? (_paymentProvider = new PaymentProvider()); }
        }

        public SalesOrderResponse CommitPurchase(SalesOrderRequest request, CCInfo ccInfo)
        {
            string msg = string.Empty;
            objLoggerII = logger.InitLogger();
            var response = SalesOrderResponse.Pending;

            try
            {
                Config = ConfigurationProvider.GetConfig(PaymentGatewayConfigurations);

                CCMerchantCredential ccMerchantCredential = PaymentProvider.GetCCMerchantCredential(request.merchantId);

                PayMasterHeaderRequest payMasterHeaderRequest = InitializePayMasterHeader("CCPaymentAuthorizationRequestMessage");
                PayMasterParametersRequest payMasterParametersRequest = InitializePayMasterPCIParameters(request, ccMerchantCredential, ccInfo);
                PayMasterTrailerRequest payMasterTrailerRequest = InitializePayMasterTrailer();

                string vgsData = VGS.InboundConvert(ccInfo.Param2);
                string vgsOutbound = VGS.OutboundConvert(vgsData);
                if(!string.IsNullOrEmpty(vgsOutbound))
                { 
                    InsertVGS(vgsData, request.onlineRefNum); //ID null inside DB
                }
                else
                {
                    //return error message, not proceed to next line
                }

                string POSTURL = GeneratePOSTURL(payMasterHeaderRequest, payMasterParametersRequest, payMasterTrailerRequest, ccMerchantCredential);
                if (POSTURL != null)
                {
                    response.RedirectURL = POSTURL;
                    logMsg(CLoggerII.LOG_SEVERITY.INFORMATION,
                    "[PayMasterSales Request][" + request.onlineRefNum + "][Redirect URL: " + POSTURL + "]");
                }
            }
            catch (Exception ex)
            {
                logMsg(CLoggerII.LOG_SEVERITY.CRITICAL,
                  "[PayMasterSales Request][" + request.onlineRefNum + "][Exception: " + ex.ToString() + "]");
            }
            return response;
        }

        public SalesOrderResponse ReversalPurchase(SalesOrderRequest request)
        {
            objLoggerII = logger.InitLogger();

            Config = ConfigurationProvider.GetConfig(PaymentGatewayConfigurations);

            PayMasterHeaderRequest payMasterHeaderRequest = InitializePayMasterHeader("CCPaymentReversalRequestMessage");
            CCMerchantCredential ccMerchantCredential = PaymentProvider.GetCCMerchantCredential(request.merchantId);

            PayMasterParametersRequest payMasterParametersRequest = InitializePayMasterReversalParameters(request, ccMerchantCredential);
            PayMasterTrailerRequest payMasterTrailerRequest = InitializePayMasterTrailer();

            var response = SalesOrderResponse.Pending;

            string POSTURL = GenerateReversalPOSTURL(payMasterHeaderRequest, payMasterParametersRequest, payMasterTrailerRequest, ccMerchantCredential);
            if (POSTURL != null)
            {
                response.RedirectURL = POSTURL;
                logMsg(CLoggerII.LOG_SEVERITY.INFORMATION,
                "[PayMasterSales Manual Reversal][" + request.onlineRefNum + "][Reversal URL(0400): " + POSTURL + "]");
                string httpReversalResponse = HttpHelper.HttpGet(response.RedirectURL);
                logMsg(CLoggerII.LOG_SEVERITY.INFORMATION,
             "[PayMasterSales Manual Reversal][" + request.onlineRefNum + "][Reversal Response(0410): " + httpReversalResponse + "]");

                response = SalesOrderResponse.OK;
                return response;
            }

            return response;
        }

        public UPPPaymentQueryRequestMessage0780_TestURL.Query0790Resp RequeryPurchase(PayMasterResponse request, ReqTxn reqTxn)
        {
            string msg = string.Empty;
            objLoggerII = logger.InitLogger();

            try
            {
                Config = ConfigurationProvider.GetConfig("Paydibs.PaymentGatewayAPI");
                CCMerchantCredential ccMerchantCredential = PaymentProvider.GetCCMerchantCredential(reqTxn.MerchantID);

                UPPPaymentQueryRequestMessage0780_TestURL.GatewayClient wsPayMaster = new UPPPaymentQueryRequestMessage0780_TestURL.GatewayClient();
                var queryRequest = new UPPPaymentQueryRequestMessage0780_TestURL.Query0780Req()
                {
                    h001_MTI = h001_MTI.UPPPaymentQueryRequestMessage,
                    h002_VNO = Config.PayMaster_VERSION,
                    h003_TDT = DateTime.Now.ToString("yyyyMMdd"),
                    h004_TTM = DateTime.Now.ToString("HHmmssff"),
                    f001_MID = ccMerchantCredential.MerchantID,
                    f260_ServID = Config.PayMaster_ServID,
                    f263_MRN = request.f263_MRN,
                    f284_RURL_UPPPQ = "", //UPP Payment Query Return URL
                    t001_SHT = t001_SHT.SH2
                };

                string headerfield = "h001_MTI=" + queryRequest.h001_MTI +
                     "&h002_VNO=" + queryRequest.h002_VNO +
                     "&h003_TDT=" + queryRequest.h003_TDT +
                     "&h004_TTM=" + queryRequest.h004_TTM +

                    "&f001_MID=" + queryRequest.f001_MID +
                    "&f260_ServID=" + queryRequest.f260_ServID +
                    "&f263_MRN=" + queryRequest.f263_MRN +
                    "&f284_RURL_UPPPQ=" + queryRequest.f284_RURL_UPPPQ;

                string t002_SHV = generateSignature(headerfield, ccMerchantCredential);
                queryRequest.t002_SHV = t002_SHV;

                logMsg(CLoggerII.LOG_SEVERITY.INFORMATION,
                    "[PayMasterSales Response][" + request.f263_MRN + "][CommitPurchase][PurchasePayMasterService.RequeryPurchase 0780 Request: " + JsonConvert.SerializeObject(queryRequest) + "]");

                logMsg(CLoggerII.LOG_SEVERITY.INFORMATION,
                 "[PayMasterSales Response][" + request.f263_MRN + "][CommitPurchase][PurchasePayMasterService.RequeryPurchase Protocol TLS: " + ServicePointManager.SecurityProtocol.ToString() + "]");

                UPPPaymentQueryRequestMessage0780_TestURL.Query0790Resp Query0790Resp = wsPayMaster.UPPPaymentQuery(queryRequest);

                logMsg(CLoggerII.LOG_SEVERITY.INFORMATION,
                "[PayMasterSales Response][" + request.f263_MRN + "][CommitPurchase][PurchasePayMasterService.RequeryPurchase 0790 Response: " + JsonConvert.SerializeObject(Query0790Resp) + "]");

                return Query0790Resp;

            }
            catch (Exception ex)
            {
                logMsg(CLoggerII.LOG_SEVERITY.INFORMATION,
                "[PayMasterSales Response][" + request.f263_MRN + "][CommitPurchase][PurchasePayMasterService.RequeryPurchase Exception: " + ex.ToString() + "]");

                return null;
            }
        }

        private PayMasterHeaderRequest InitializePayMasterHeader(string function)
        {
            string h001 = string.Empty;
            if (function == "CCPaymentAuthorizationRequestMessage")
            {
                h001 = h001_MTI.CCPaymentAuthorizationRequestMessage;
            }
            if (function == "PaymentSalesCompletionRequestMessage")
            {
                h001 = h001_MTI.PaymentSalesCompletionRequestMessage;
            }
            else if(function == "CCPaymentReversalRequestMessage")
            {
                h001 = h001_MTI.CCPaymentReversalRequestMessage;
            }
            var payMasterHeaderRequest = new PayMasterHeaderRequest()
            {
                h001_MTI = h001,
                h002_VNO = Config.PayMaster_VERSION,
                h003_TDT = DateTime.Now.ToString("yyyyMMdd"),
                h004_TTM = DateTime.Now.ToString("HHmmssff")
            };
           
            return payMasterHeaderRequest;
        }

        private PayMasterParametersRequest InitializePayMasterPCIParameters(SalesOrderRequest request, CCMerchantCredential ccMerchantCredential, CCInfo ccInfo)
        {
            var payMasterParametersRequest = new PayMasterParametersRequest() //page95
            {
                //0200 page 86
                f001_MID = ccMerchantCredential.MerchantID,
                f003_ProcCode = "003000", //page 46
                f004_PAN = ccInfo.Param2, //testing card
                f005_ExpDate = "2812", //testing card
                f006_TxnDtTm = DateTime.Now.ToString("yyyyMMdd"),
                f007_TxnAmt = request.amount.ToString().Replace(".", "").Replace(",", "").PadLeft(12, '0'),//,"000000058030", //convert to correct format amount
                f008_POSCond = "59",
                f010_CurrCode = ((int)Field010CurrencyCode.MalaysiaRinggit).ToString(),
                f012_CVV2 = ccInfo.CVV2,
                f014_3DXID = "",
                f015_3DARC = "N",
                f016_3DCAVVLen = "",
                f017_3DCAVV = "",
                f019_ExpTxnAmt = "2", //TODO: if treat other currency code need to be cater
                f022_ECI = "00",
                f247_OrgTxnAmt = request.amount.ToString().Replace(".", "").Replace(",", "").PadLeft(12, '0'), //convert to correct format amount
                f248_OrgCurrCode = ((int)Field248OriginalCurrencyCode.MalaysiaRinggit).ToString(),
                f249_TxnCh = f249_TxnCh.WEB,
                f260_ServID = Config.PayMaster_ServID,
                f261_HostID = "",
                f262_SessID = "",
                f263_MRN = request.onlineRefNum,
                f264_Locale = "en",
                f265_RURL_CCPS = Config.PayMaster_RURL_UPPPS,
                f266_RURL_CCPU = Config.PayMaster_RURL_UPPPU,
                f267_RURL_CCPC = Config.PayMaster_RURL_UPPPC,
                f268_CHName = request.custName, //maybe another field
                f269_IssName = ccInfo.BankIssuer, //Iss Bank Name how?
                f270_ORN = request.ordernumber,
                f271_ODesc = request.orderTitle + "[" + request.ordernumber + "]",
                f278_EMailAddr = request.custEmail,
                f279_HP = request.custPhone,
                f285_IPAddr = HttpHelper.GetIP(), //get from IP Code
                f287_ExpOrgTxnAmt = "2", //TODO: if treat other currency code need to be cater
                f288_IssCntrCde = "MY",
                f350_CrdTyp = "",
                f354_TID = ccMerchantCredential.TerminalID,
                f352_AcqBank = "",
                f353_IPPTenure = "",
                f325_ECommMercInd = ((int)Field325_ECommMercInd.ECommerce3DSecure).ToString(), //bank s
                f339_TokenFlg = f339_TokenFlg.NO, //flag Phase 2
                f340_MercPromoCde = "",
                f341_MercPromoAmt = "",
                f342_PromoAmtAcqCst = "",
                f343_PromoAmtMercCst = "",
                f344_MercCustId = "",
                f347_TokenShrtNm = "",
                f363_InvNum = "",
                f364_Fee = ""
                
                //0220
                //f001_MID = ccMerchantCredential.MerchantID,
                //f003_ProcCode = "003000", //page 46
                //f004_PAN = "5453010000095323", //testing card
                //f005_ExpDate = "2812", //testing card
                //f006_TxnDtTm = DateTime.Now.ToString("yyyyMMdd"),
                //f007_TxnAmt = request.amount.ToString().Replace(".", "").Replace(",", "").PadLeft(12, '0'),//,"000000058030", //convert to correct format amount
                //f008_POSCond = "59",
                //f010_CurrCode = ((int)Field010CurrencyCode.MalaysiaRinggit).ToString(),
                //f012_CVV2 = "123",
                //f014_3DXID = "",
                //f015_3DARC = "N",
                //f016_3DCAVVLen = "",
                //f017_3DCAVV = "",
                //f019_ExpTxnAmt = "2", //TODO: if treat other currency code need to be cater
                //f022_ECI = "00",
                //f247_OrgTxnAmt = request.amount.ToString().Replace(".", "").Replace(",", "").PadLeft(12, '0'), //convert to correct format amount
                //f248_OrgCurrCode = ((int)Field248OriginalCurrencyCode.MalaysiaRinggit).ToString(),
                //f249_TxnCh = f249_TxnCh.WEB,
                //f260_ServID = Config.PayMaster_ServID,
                //f261_HostID = "",
                //f262_SessID = "",
                //f263_MRN = request.onlineRefNum,
                //f264_Locale = "en",
                //f265_RURL_CCPS = Config.PayMaster_RURL_UPPPS,
                //f266_RURL_CCPU = Config.PayMaster_RURL_UPPPU,
                //f267_RURL_CCPC = Config.PayMaster_RURL_UPPPC,
                //f268_CHName = request.custName, //maybe another field
                //f269_IssName = "CITIBANK",
                //f270_ORN = request.ordernumber,
                //f271_ODesc = request.orderTitle + "[" + request.ordernumber + "]",
                //f278_EMailAddr = request.custEmail,
                //f279_HP = request.custPhone,
                //f285_IPAddr = HttpHelper.GetIP(),
                //f287_ExpOrgTxnAmt = "2", //TODO: if treat other currency code need to be cater
                //f288_IssCntrCde = "MY",
                //f339_TokenFlg = f339_TokenFlg.NO //flag Phase 2
            };
            return payMasterParametersRequest;
        }

        private PayMasterTrailerRequest InitializePayMasterTrailer()
        {
            var payMasterTrailerRequest = new PayMasterTrailerRequest()
            {
                t001_SHT = t001_SHT.SH2,
            };
            return payMasterTrailerRequest;
        }

        private PayMasterParametersRequest InitializePayMasterReversalParameters(SalesOrderRequest request, CCMerchantCredential ccMerchantCredential)
        {
            var payMasterParametersRequest = new PayMasterParametersRequest()
            {
                f001_MID = ccMerchantCredential.MerchantID,
                f003_ProcCode = "220000",
                f004_PAN = "",
                f006_TxnDtTm = DateTime.Now.ToString("yyyyMMddHHmmss"),
                f007_TxnAmt = request.txnamount.ToString().Replace(".", "").Replace(",", "").PadLeft(12, '0'),
                f008_POSCond = "59",
                f010_CurrCode = ((int)Field010CurrencyCode.MalaysiaRinggit).ToString(),
                f011_AuthIDResp = "",
                f019_ExpTxnAmt = "2",
                f023_RRN = "",
                f257_PGRN = "",
                f260_ServID = Config.PayMaster_ServID,
                f261_HostID = "",
                f262_SessID = "",
                f263_MRN = request.onlineRefNum,
                f264_Locale = "en",
                f270_ORN = "",
                f272_RURL_CCR = ""
            };
            return payMasterParametersRequest;
        }

        private string GeneratePOSTURL(PayMasterHeaderRequest payMasterHeaderRequest, PayMasterParametersRequest payMasterParametersRequest, PayMasterTrailerRequest payMasterTrailerRequest, CCMerchantCredential ccMerchantCredential)
        {
            string headerfield = "h001_MTI=" + payMasterHeaderRequest.h001_MTI +
                    "&h002_VNO=" + payMasterHeaderRequest.h002_VNO +
                    "&h003_TDT=" + payMasterHeaderRequest.h003_TDT +
                    "&h004_TTM=" + payMasterHeaderRequest.h004_TTM +

                   //0200
                   "&f001_MID=" + payMasterParametersRequest.f001_MID +
                   "&f003_ProcCode=" + payMasterParametersRequest.f003_ProcCode +
                   "&f004_PAN=" + payMasterParametersRequest.f004_PAN +
                   "&f005_ExpDate=" + payMasterParametersRequest.f005_ExpDate +
                   "&f006_TxnDtTm=" + payMasterParametersRequest.f006_TxnDtTm +
                   "&f007_TxnAmt=" + payMasterParametersRequest.f007_TxnAmt +
                   "&f008_POSCond=" + payMasterParametersRequest.f008_POSCond +
                   "&f010_CurrCode=" + payMasterParametersRequest.f010_CurrCode +
                   "&f012_CVV2=" + payMasterParametersRequest.f012_CVV2 +
                   "&f014_3DXID=" + payMasterParametersRequest.f014_3DXID +
                   "&f015_3DARC=" + payMasterParametersRequest.f015_3DARC +
                   "&f016_3DCAVVLen=" + payMasterParametersRequest.f016_3DCAVVLen +
                   "&f017_3DCAVV=" + payMasterParametersRequest.f017_3DCAVV +
                   "&f019_ExpTxnAmt=" + payMasterParametersRequest.f019_ExpTxnAmt +
                   "&f022_ECI=" + payMasterParametersRequest.f022_ECI +
                   "&f247_OrgTxnAmt=" + payMasterParametersRequest.f247_OrgTxnAmt +
                   "&f248_OrgCurrCode=" + payMasterParametersRequest.f248_OrgCurrCode +
                   "&f249_TxnCh=" + payMasterParametersRequest.f249_TxnCh +
                   "&f260_ServID=" + payMasterParametersRequest.f260_ServID +
                   "&f261_HostID=" + payMasterParametersRequest.f261_HostID +
                   "&f262_SessID=" + payMasterParametersRequest.f262_SessID +
                   "&f263_MRN=" + payMasterParametersRequest.f263_MRN +
                   "&f264_Locale=" + payMasterParametersRequest.f264_Locale +
                   "&f265_RURL_CCPS=" + payMasterParametersRequest.f265_RURL_CCPS +
                   "&f266_RURL_CCPU=" + payMasterParametersRequest.f266_RURL_CCPU +
                   "&f267_RURL_CCPC=" + payMasterParametersRequest.f267_RURL_CCPC +
                   "&f268_CHName=" + payMasterParametersRequest.f268_CHName +
                   "&f269_IssName=" + payMasterParametersRequest.f269_IssName +
                   "&f270_ORN=" + payMasterParametersRequest.f270_ORN +
                   "&f271_ODesc=" + payMasterParametersRequest.f271_ODesc +
                   "&f278_EMailAddr=" + payMasterParametersRequest.f278_EMailAddr +
                   "&f279_HP=" + payMasterParametersRequest.f279_HP +
                   "&f285_IPAddr=" + payMasterParametersRequest.f285_IPAddr +
                   "&f287_ExpOrgTxnAmt=" + payMasterParametersRequest.f287_ExpOrgTxnAmt +
                   "&f288_IssCntrCde=" + payMasterParametersRequest.f288_IssCntrCde +
                   "&f350_CrdTyp=" + payMasterParametersRequest.f350_CrdTyp +
                   "&f354_TID=" + payMasterParametersRequest.f354_TID +
                   "&f352_AcqBank=" + payMasterParametersRequest.f352_AcqBank +
                   "&f353_IPPTenure=" + payMasterParametersRequest.f353_IPPTenure +
                   "&f325_ECommMercInd=" + payMasterParametersRequest.f325_ECommMercInd +
                   "&f339_TokenFlg=" + payMasterParametersRequest.f339_TokenFlg +
                   "&f340_MercPromoCde=" + payMasterParametersRequest.f340_MercPromoCde +
                   "&f341_MercPromoAmt=" + payMasterParametersRequest.f341_MercPromoAmt +
                   "&f342_PromoAmtAcqCst=" + payMasterParametersRequest.f342_PromoAmtAcqCst +
                   "&f343_PromoAmtMercCst=" + payMasterParametersRequest.f343_PromoAmtMercCst +
                   "&f344_MercCustId=" + payMasterParametersRequest.f344_MercCustId +
                   "&f347_TokenShrtNm=" + payMasterParametersRequest.f347_TokenShrtNm +
                   "&f363_InvNum=" + payMasterParametersRequest.f363_InvNum +
                   "&f364_Fee=" + payMasterParametersRequest.f364_Fee;
            
            ////0220
            //"&f001_MID=" + payMasterParametersRequest.f001_MID +
            //"&f003_ProcCode=" + payMasterParametersRequest.f003_ProcCode +
            //"&f004_PAN=" + payMasterParametersRequest.f004_PAN +
            //"&f005_ExpDate=" + payMasterParametersRequest.f005_ExpDate +
            //"&f006_TxnDtTm=" + payMasterParametersRequest.f006_TxnDtTm +
            //"&f007_TxnAmt=" + payMasterParametersRequest.f007_TxnAmt +
            //"&f008_POSCond=" + payMasterParametersRequest.f008_POSCond +
            //"&f010_CurrCode=" + payMasterParametersRequest.f010_CurrCode +
            //"&f012_CVV2=" + payMasterParametersRequest.f012_CVV2 +
            //"&f014_3DXID=" + payMasterParametersRequest.f014_3DXID +
            //"&f015_3DARC=" + payMasterParametersRequest.f015_3DARC +
            //"&f016_3DCAVVLen=" + payMasterParametersRequest.f016_3DCAVVLen +
            //"&f017_3DCAVV=" + payMasterParametersRequest.f017_3DCAVV +
            //"&f019_ExpTxnAmt=" + payMasterParametersRequest.f019_ExpTxnAmt +
            //"&f022_ECI=" + payMasterParametersRequest.f022_ECI +
            //"&f247_OrgTxnAmt=" + payMasterParametersRequest.f247_OrgTxnAmt +
            //"&f248_OrgCurrCode=" + payMasterParametersRequest.f248_OrgCurrCode +
            //"&f249_TxnCh=" + payMasterParametersRequest.f249_TxnCh +
            //"&f260_ServID=" + payMasterParametersRequest.f260_ServID +
            //"&f261_HostID=" + payMasterParametersRequest.f261_HostID +
            //"&f262_SessID=" + payMasterParametersRequest.f262_SessID +
            //"&f263_MRN=" + payMasterParametersRequest.f263_MRN +
            //"&f264_Locale=" + payMasterParametersRequest.f264_Locale +
            //"&f265_RURL_CCPS=" + payMasterParametersRequest.f265_RURL_CCPS +
            //"&f266_RURL_CCPU=" + payMasterParametersRequest.f266_RURL_CCPU +
            //"&f267_RURL_CCPC=" + payMasterParametersRequest.f267_RURL_CCPC +
            //"&f268_CHName=" + payMasterParametersRequest.f268_CHName +
            //"&f269_IssName=" + payMasterParametersRequest.f269_IssName +
            //"&f270_ORN=" + payMasterParametersRequest.f270_ORN +
            //"&f271_ODesc=" + payMasterParametersRequest.f271_ODesc +
            //"&f278_EMailAddr=" + payMasterParametersRequest.f278_EMailAddr +
            //"&f279_HP=" + payMasterParametersRequest.f279_HP +
            //"&f285_IPAddr=" + payMasterParametersRequest.f285_IPAddr +
            //"&f287_ExpOrgTxnAmt=" + payMasterParametersRequest.f287_ExpOrgTxnAmt +
            //"&f288_IssCntrCde=" + payMasterParametersRequest.f288_IssCntrCde;


            string parametersURL = headerfield +
                "&t001_SHT=" + payMasterTrailerRequest.t001_SHT +
                "&t002_SHV=" + generateSignature(headerfield, ccMerchantCredential);

            string POSTURL = Config.PayMaster_POSTURL0200 + parametersURL;

            return POSTURL;
        }

        private string GenerateReversalPOSTURL(PayMasterHeaderRequest payMasterHeaderRequest, PayMasterParametersRequest payMasterParametersRequest, PayMasterTrailerRequest payMasterTrailerRequest, CCMerchantCredential ccMerchantCredential)
        {
            string headerfield = "h001_MTI=" + payMasterHeaderRequest.h001_MTI +
                    "&h002_VNO=" + payMasterHeaderRequest.h002_VNO +
                    "&h003_TDT=" + payMasterHeaderRequest.h003_TDT +
                    "&h004_TTM=" + payMasterHeaderRequest.h004_TTM +

                   "&f001_MID=" + payMasterParametersRequest.f001_MID +
                   "&f003_ProcCode=" + payMasterParametersRequest.f003_ProcCode +
                   "&f004_PAN=" + payMasterParametersRequest.f004_PAN +
                   "&f006_TxnDtTm=" + payMasterParametersRequest.f006_TxnDtTm +
                   "&f007_TxnAmt=" + payMasterParametersRequest.f007_TxnAmt +
                   "&f008_POSCond=" + payMasterParametersRequest.f008_POSCond +
                   "&f010_CurrCode=" + payMasterParametersRequest.f010_CurrCode +
                   "&f011_AuthIDResp=" + payMasterParametersRequest.f011_AuthIDResp +
                   "&f019_ExpTxnAmt=" + payMasterParametersRequest.f019_ExpTxnAmt +
                   "&f023_RRN=" + payMasterParametersRequest.f023_RRN +
                   "&f257_PGRN=" + payMasterParametersRequest.f257_PGRN +
                   "&f260_ServID=" + payMasterParametersRequest.f260_ServID +
                   "&f261_HostID=" + payMasterParametersRequest.f261_HostID +
                   "&f262_SessID=" + payMasterParametersRequest.f262_SessID +
                   "&f263_MRN=" + payMasterParametersRequest.f263_MRN +
                   "&f264_Locale=" + payMasterParametersRequest.f264_Locale +
                   "&f270_ORN=" + payMasterParametersRequest.f270_ORN +
                   "&f272_RURL_CCR=" + payMasterParametersRequest.f272_RURL_CCR;

            string parametersURL = headerfield +
                "&t001_SHT=" + payMasterTrailerRequest.t001_SHT +
                "&t002_SHV=" + generateSignature(headerfield, ccMerchantCredential);

            string POSTURL = "https://payment.gobiz.com.my/upp/faces/reversal0400.xhtml?" + parametersURL;

            return POSTURL;
        }

        public string MerchantPayResp(Host host, ResTxn resTxn, ReqTxn reqTxn)
        {
            var purchasePayMasterService = new PurchasePayMasterService();
            string szHTMLContent = string.Empty;
            try
            {
                Merchant merchant = purchasePayMasterService.GetMerchant(resTxn.MerchantID);

                logMsg(CLoggerII.LOG_SEVERITY.INFORMATION,
                "[PayMasterSales Response][" + resTxn.GatewayTxnID + "][[Paydibs PaymentGatewayAPI PurchasePayMasterService-MerchantPayResp][" + Newtonsoft.Json.JsonConvert.SerializeObject(merchant) + "]");

                //iSendReply2Merchant();
                //st_PayInfo.szHashValue2 = objHash.szGetSaleResHash2(st_PayInfo)

                //notify status?
                //NotifyStatus notifyStatus = PaymentProvider.CheckCallBackStatus(resTxn.GatewayTxnID);

                //if (notifyStatus.CallBackStatus != "1")
                //{
                int HttpTimeoutSeconds = Convert.ToInt32(ConfigurationManager.AppSettings["HttpTimeoutSeconds"]);

                //Hash Value for pay response
                Hash hash = new Hash();
                resTxn.HashValue2 = hash.GetSaleResHash2(resTxn, merchant);

                string HTTPString = szGetReplyMerchantCallbackHTTPString(host, resTxn, reqTxn);


                logMsg(CLoggerII.LOG_SEVERITY.INFORMATION,
                "[PayMasterSales Response][" + resTxn.GatewayTxnID + "][PurchasePayMasterService-MerchantPayResp-szGetReplyMerchantCallbackHTTPString][" + HTTPString + "]");

                if ((resTxn.MerchantReturnURL != ""))
                {
                    string sz_HTML = string.Empty;

                    //post to merchant page
                    MerchantCallback merchantCallback = purchasePayMasterService.RetrieveMerchantParameters(host, resTxn, reqTxn);
                    string postRequest = merchantCallback.ToUrlEncoded();

                    logMsg(CLoggerII.LOG_SEVERITY.INFORMATION,
                    "[PayMasterSales Response][" + resTxn.GatewayTxnID + "][PurchasePayMasterService-RetrieveMerchantParameters-POSTINGTOMerchantReturnURL][" + resTxn.MerchantReturnURL + ": " + postRequest + "]");

                    HttpHelper.HttpPost(resTxn.MerchantReturnURL, postRequest, MIMEType.URL_ENCODED, "TLS12");
                    string szResp = string.Empty;

                    //Display paydibs receipt
                    szHTMLContent = LoadReceiptUI(resTxn.MerchantReturnURL, HTTPString, sz_HTML, host, resTxn, reqTxn, merchant);

                    logMsg(CLoggerII.LOG_SEVERITY.INFORMATION,
                    "[PayMasterSales Response][" + resTxn.GatewayTxnID + "][LoadReceiptUI][" + resTxn.MerchantID + resTxn.MerchantTxnID + " > Redirected to [" + resTxn.MerchantReturnURL + "] & Load HTML Content");

                    return szHTMLContent;
                }
                return szHTMLContent;
            }
            catch(Exception ex)
            {
                logMsg(CLoggerII.LOG_SEVERITY.CRITICAL,
                 "[Paydibs PaymentGatewayAPI PurchasePayMasterService-MerchantPayResp][Exception: " + ex.ToString() + "]");

                return szHTMLContent;
            }
        }

        public string GetReplyMerchantCallbackHTTPString(MerchantCallback merchantCallback)
        {
            //Dictionary<string, string> g_JSONResp = new Dictionary<string, string>();
            //g_JSONResp.Add("TxnType", TXN_TYPE); // Server.UrlEncode(st_PayInfo.szTxnType));  
            //g_JSONResp.Add("Method", PYMT_METHOD);
            //g_JSONResp.Add("MerchantID", resTxn.MerchantID);
            //g_JSONResp.Add("MerchantPymtID", HttpUtility.UrlEncode(resTxn.OrderNumber));
            //g_JSONResp.Add("MerchantOrdID", HttpUtility.UrlEncode(resTxn.MerchantTxnID));
            //g_JSONResp.Add("MerchantTxnAmt", resTxn.TxnAmount);
            //g_JSONResp.Add("MerchantCurrCode", HttpUtility.UrlEncode(resTxn.CurrencyCode));

            //g_JSONResp.Add("PTxnID", resTxn.MerchantID + resTxn.MerchantTxnID);
            //g_JSONResp.Add("PTxnStatus", resTxn.TxnStatus.ToString());
            //g_JSONResp.Add("PTxnMsg", System.Uri.EscapeDataString(resTxn.RespMesg));
            ////g_JSONResp.Add("AcqBank", st_PayInfo.szIssuingBank);
            //g_JSONResp.Add("BankRefNo", resTxn.BankRefNo); // Server.UrlEncode(st_PayInfo.szHostTxnID))
            //g_JSONResp.Add("Sign", resTxn.HashValue2);
            //if (("" != resTxn.AuthCode))
            //{
            //    g_JSONResp.Add("AuthCode", HttpUtility.UrlEncode(resTxn.AuthCode));
            //}

            //string sz_LogString = objJS.Serialize(g_JSONResp);
            //System.Web.Script.Serialization.JavaScriptSerializer objJS = new System.Web.Script.Serialization.JavaScriptSerializer();

            //string szHTTPString = HttpUtility.UrlEncode(objJS.Serialize(g_JSONResp));


            if (("" != merchantCallback.AuthCode))
            {
                merchantCallback.AuthCode = HttpUtility.UrlEncode(merchantCallback.AuthCode);
            }
            string parameter = "?"
                + "TxnType=" + TXN_TYPE + "&"
                + "Method=" + PYMT_METHOD + "&"
                + "MerchantID=" + merchantCallback.MerchantID + "&"
                + "MerchantPymtID=" + HttpUtility.UrlEncode(merchantCallback.MerchantPymtID) + "&"
                + "MerchantOrdID=" + HttpUtility.UrlEncode(merchantCallback.MerchantOrdID) + "&"
                + "MerchantTxnAmt=" + merchantCallback.MerchantTxnAmt + "&"
                + "MerchantCurrCode=" + HttpUtility.UrlEncode(merchantCallback.MerchantCurrCode) + "&"
                + "PTxnID=" + merchantCallback.PTxnID + "&"
                + "PTxnStatus=" + merchantCallback.PTxnStatus.ToString() + "&"
                + "PTxnMsg=" + System.Uri.EscapeDataString(merchantCallback.PTxnMsg) + "&"
                + "BankRefNo=" + merchantCallback.BankRefNo + "&"
                + "Sign=" + merchantCallback.Sign + "&"
                + "AuthCode=" + merchantCallback.AuthCode;

          
            return parameter;
        }

        private string LoadReceiptUI(string merchantReturnURL, string hTTPString, string sz_HTML, Host host, ResTxn resTxn, ReqTxn reqTxn, Merchant merchant)
        {
            string szTemplatePath = "";
            string szHTMLContent = "";
            string szTxnStatus = "";
            string[] szReceiptTemplate;
            string szPymtMethod = "";         // Added, 9 Dec 2013
            string szCCDetails = "";          // Added, 9 Dec 2013
            string szHideReceipt = "true";    // Added, 15 Dec 2013
            string szHideMerchantLogo = "";
            string szHideMerchantAddr = "";
         
            //TODO: put in web config
            szTemplatePath = ConfigurationManager.AppSettings["TemplateFilePath"];
            logMsg(CLoggerII.LOG_SEVERITY.INFORMATION,
                "[PayMasterSales Response][" + resTxn.GatewayTxnID + "][LoadReceiptUI][" + resTxn.MerchantID + resTxn.MerchantTxnID + " > [PurchasePayMasterService][LoadReceiptUI]Load Template File to [" + resTxn.MerchantReturnURL + "] " + szHTMLContent);

            if (szTemplatePath != "")
            {
                szReceiptTemplate = szTemplatePath.Split('.');
                szTemplatePath = szReceiptTemplate[0] + "_" + resTxn.LanguageCode + "." + szReceiptTemplate[1];
                szHTMLContent = GetTemplate(szTemplatePath, szHTMLContent, resTxn, reqTxn, merchant);

                if ((string.IsNullOrEmpty(szHTMLContent)))
                {
                    //if ((string.IsNullOrEmpty(GetTemplate(szReceiptTemplate[0] + "_" + Common.C_DEFAULT_LANGUAGE_CODE + "." + szReceiptTemplate[1], szHTMLContent, resTxn, reqTxn, merchant))))
                    //{
                        //iReturn = -1;
                        return szHTMLContent;
                    //}
                }
            }

            if (szHTMLContent != "")
            {
                if ((merchantReturnURL != ""))
                {
                    if ((int)Common.TXN_STATUS.TXN_SUCCESS == resTxn.TxnStatus)
                    {
                        szTxnStatus = "Successful"; // 'Modified 28 Jan 2014, Success to Successful
                        szHideReceipt = "false"; //'Added, 15 Dec 2013
                    }
                    else if ((int)Common.TXN_STATUS.TXN_AUTH_SUCCESS == resTxn.TxnStatus)
                    {
                        szTxnStatus = "Pre-Auth Successful";
                        szHideReceipt = "false";
                    }
                    else if ((int)Common.TXN_STATUS.TXN_FAILED == resTxn.TxnStatus ||
                        (int)Common.TXN_STATUS.INVALID_HOST_REPLY == resTxn.TxnStatus ||
                        (int)Common.TXN_STATUS.DECLINED_BY_VEENROLL == resTxn.TxnStatus) 
                    {
                        szTxnStatus = "Failed. Please Try Again.";   //'Modified 25 Jul 2014, added "Please Try Again."
                    }
                    else if ((int)Common.TXN_STATUS.DECLINED_BY_EXT_FDS == resTxn.TxnStatus)       //  ' 25 May 2017
                    {
                        szTxnStatus = "Declined";
                    }
                    else
                    {
                        szTxnStatus = "Pending";
                    }

                    if (("0" == merchant.ShowMerchantLogo))
                    {
                        szHideMerchantLogo = "hide";           // 'Bootstrap hide class
                    }

                    if (("0" == merchant.ShowMerchantAddr))
                    {
                        szHideMerchantAddr = "hide";
                    }

                    szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[MERCHANTADDR]", merchant.Addr1 + " " + merchant.Addr2 + " " + merchant.Addr3);            // Added, 14 Dec 2013
                    szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[MERCHANTCONTACTNO]", merchant.ContactNo);     // Added, 14 Dec 2013
                    szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[MERCHANTEMAILADDR]", merchant.EmailAddr);     // Added, 14 Dec 2013
                    szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[MERCHANTWEBSITEURL]", merchant.WebSiteURL);   // Added, 14 Dec 2013

                    // st_PayInfo.szHostDate ---> resTxn.DateCreated?
                    if (("" != resTxn.DateCreated & ("FPX" == Left(ISSUING_BANK.Trim(), 3).ToUpper() | ("FFFPX" == ISSUING_BANK.Trim().ToUpper()))))
                    {
                        DateTime dt;
                        dt = DateTime.ParseExact(resTxn.DateCreated, "yyyyMMddHHmmss", CultureInfo.CreateSpecificCulture("en-us"));
                        szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[TXNDATETIME]", dt.ToString("dd MMM yyyy hh:mm:ss tt", CultureInfo.CreateSpecificCulture("en-us")));  // Added 6 Nov 2017, cater FPX B2B Receipt Date Time
                    }
                    else
                    {
                        szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[TXNDATETIME]", System.DateTime.Now.ToString("dd MMM yyyy hh:mm:ss tt", CultureInfo.CreateSpecificCulture("en-us")));  // Added, 9 Dec 2013; Modified 17 Feb 2014, added CultureInfo or else some machine not able to display AM/PM
                    }
                    szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[PYMTMETHOD]", szPymtMethod);                                          // Added, 9 Dec 2013
                    szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[CCDETAILS]", szCCDetails);                                            // Added, 9 Dec 2013
                    szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[MERCHANTNAME]", merchant.MerchantName);
                    szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[ORDERDESC]", resTxn.OrderDesc);
                    szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[TXNAMOUNT]", resTxn.TxnAmount);
                    szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[CURRENCYCODE]", resTxn.CurrencyCode);
                    szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[MERCHANTTXNID]", resTxn.MerchantTxnID);
                    szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[ORDERNUMBER]", resTxn.OrderNumber);                 // Added, 14 Dec 2013
                    szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[GATEWAYTXNID]", resTxn.GatewayTxnID);
                    szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[BANKREFNO]", resTxn.BankRefNo);                     // Added, 1 Jun 2015
                    szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[ISSUINGBANK]", ISSUING_BANK);
                    szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[HOSTTXNID]", resTxn.BankRefNo);
                    szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[TXNSTATUS]", szTxnStatus);
                    szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[HOSTPARAMS]", hTTPString);
                    szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[HOSTURL]", resTxn.MerchantReturnURL);// to do: double check sz_URL); 
                    szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[REDIRECTMS]", ConfigurationManager.AppSettings["ReceiptRedirectMS"]);
                    szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[REDIRECTSEC]", Left(ConfigurationManager.AppSettings["ReceiptRedirectMS"], ConfigurationManager.AppSettings["ReceiptRedirectMS"].Length - 3));
                    szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[ISHIDERECEIPT]", szHideReceipt);                          // Added, 15 Dec 2013
                    szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[MERCHANTID]", resTxn.MerchantID);                     // Added, 24 Jun 2014, to show Merchant logo on receipt page
                    szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[HIDEMERCHANTLOGO]", szHideMerchantLogo);                  // 04 may 2017
                    szHTMLContent = szFormatPlaceHolder(szHTMLContent, "[HIDEMERCHANTADDR]", szHideMerchantAddr);                  // 04 may 2017

                }
                else
                {
                    return szHTMLContent;
                }

                sz_HTML = szHTMLContent;
            }
            else
            {
                return szHTMLContent;
            }
            return szHTMLContent;
        }

        public string szFormatPlaceHolder(string szOriText, string szTextToFind, string szTextToReplace)
        {
            if (szOriText.Contains(szTextToFind))
            {
                if ((szTextToReplace.Trim().Length > 0))
                {
                    szOriText = szOriText.Replace(szTextToFind, szTextToReplace);
                    return szOriText;
                }
                else
                {
                    szOriText = szOriText.Replace(szTextToFind, "");
                    return szOriText;
                }
            }
            return szOriText;
        }

        private string GetTemplate(string szTemplatePath, string szHTMLContent, ResTxn resTxn, ReqTxn reqTxn, Merchant merchant)
        {
            StreamReader objStreamReader = null;       // Modified Joyce 4 Apr 2019,  add "= Nothing" as requested by PCI
            string szTemplateFile = "";
            bool bReturn = true;

            try
            {
                szTemplateFile = szTemplatePath;

                // Load page
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(), resTxn.MerchantID + resTxn.MerchantTxnID + " > Load (" + szTemplateFile + ")");

                objStreamReader = System.IO.File.OpenText(szTemplateFile);
                szHTMLContent = objStreamReader.ReadToEnd();
                objStreamReader.Close();

                return szHTMLContent;
            }
            catch (Exception ex)
            {
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(), 
                 " > Invalid Template File Path Exception  (" + ex.ToString() + ")");

                return string.Empty;
            }
        }

        private string szGetReplyMerchantCallbackHTTPString(Host host, ResTxn resTxn, ReqTxn reqTxn)
        {
            string szHTTPString = string.Empty;

            //What is 1 and 4? Refer on Legacy Checkout
            if (("1" == host.HostReplyMethod | "4" == host.HostReplyMethod))
            {
                szHTTPString = "<INPUT type='hidden' name='TxnType' value='" + HttpUtility.UrlEncode(TXN_TYPE) + "'>" + System.Environment.NewLine;
                szHTTPString = szHTTPString + "<INPUT type='hidden' name='Method' value='" + PYMT_METHOD + "'>" + System.Environment.NewLine;     // Added, 16 Aug 2013; Modified 9 Dec 2013, add Method as variable instead of 'CC'
                szHTTPString = szHTTPString + "<INPUT type='hidden' name='MerchantID' value='" + resTxn.MerchantID + "'>" + System.Environment.NewLine;
                szHTTPString = szHTTPString + "<INPUT type='hidden' name='MerchantPymtID' value='" + resTxn.MerchantTxnID + "'>" + System.Environment.NewLine;
                szHTTPString = szHTTPString + "<INPUT type='hidden' name='MerchantOrdID' value='" + HttpUtility.UrlEncode(resTxn.OrderNumber) + "'>" + System.Environment.NewLine;    // Added, 16 Aug 2013

                // Modified 2 Sept 2014, use szTxnAmount and szCurrencyCode instead of st_PayInfo.szTxnAmount, for FX
                szHTTPString = szHTTPString + "<INPUT type='hidden' name='MerchantTxnAmt' value='" + resTxn.TxnAmount + "'>" + System.Environment.NewLine;
                szHTTPString = szHTTPString + "<INPUT type='hidden' name='MerchantCurrCode' value='" + resTxn.CurrencyCode + "'>" + System.Environment.NewLine;

                szHTTPString = szHTTPString + "<INPUT type='hidden' name='Sign' value='" + (resTxn.HashValue2) + "'>" + System.Environment.NewLine;                          // Added, 28 Apr 2016, response hash value 2 which includes OrderNumber and Param6 because some plugins like Magento update order based on these 2 fields
                szHTTPString = szHTTPString + "<INPUT type='hidden' name='PTxnID' value='" + resTxn.GatewayTxnID + "'>" + System.Environment.NewLine;

                //if (("CC" == reqTxn.))
                //{
                //szHTTPString = szHTTPString + "<INPUT type='hidden' name='AcqBank' value='" + Server.UrlEncode(Common.C_TXN_CC_ACQBANK) + "'>" + System.Environment.NewLine;    // Added, 16 Aug 2013
                //szHTTPString = szHTTPString + "<INPUT type='hidden' name='AcqBank' value='" + Server.UrlEncode(st_PayInfo.szIssuingBank) + "'>" + System.Environment.NewLine;    // Added, 16 Aug 2013
                //}
                szHTTPString = szHTTPString + "<INPUT type='hidden' name='PTxnStatus' value='" + resTxn.TxnStatus.ToString() + "'>" + System.Environment.NewLine;

                if (("" != resTxn.AuthCode))
                {
                    szHTTPString = szHTTPString + "<INPUT type='hidden' name='AuthCode' value='" + HttpUtility.UrlEncode(resTxn.AuthCode) + "'>" + System.Environment.NewLine;
                }

                szHTTPString = szHTTPString + "<INPUT type='hidden' name='BankRefNo' value='" + HttpUtility.UrlEncode(resTxn.BankRefNo) + "'>" + System.Environment.NewLine;    // Added, 10 Sept 2013

                //Added, 7 Oct 2015, returning card data, requested by MyDin
                //Added, 1 Aug 2014, One-Click Payment
                //Added 30 Nov 2015, Promotion
                //Added  6 Nov 2015, Param67
                //Added  24 May 2016, Installment

                szHTTPString = szHTTPString + "<INPUT type='hidden' name='PTxnMsg' value='" + resTxn.RespMesg + "'>" + System.Environment.NewLine;       // Modified 22 May 2017, removed EscapeDataString
                //sz_LogString = szHTTPString;
            }
            return szHTTPString;
            //if (("CC" == szPymtMethod.ToUpper() & 1 <= st_PayInfo.iAllowOCP & "1" == st_PayInfo.szCustOCP & 
            //    Common.TXN_STATUS.TXN_SUCCESS = st_PayInfo.iTxnStatus | 
            //    Common.TXN_STATUS.TXN_AUTH_SUCCESS == st_PayInfo.iTxnStatus;/* TODO ERROR: Skipped SkippedTokensTrivia *//* TODO ERROR: Skipped SkippedTokensTrivia */))
            //{

            //}

        }

        public MerchantCallback RetrieveMerchantParameters(Host host, ResTxn resTxn, ReqTxn reqTxn)
        {
            MerchantCallback merchantCallback = new MerchantCallback();
            merchantCallback.TxnType = TXN_TYPE;
            merchantCallback.Method = PYMT_METHOD;
            merchantCallback.MerchantID = resTxn.MerchantID;
            merchantCallback.MerchantPymtID = HttpUtility.UrlEncode(resTxn.MerchantTxnID);
            merchantCallback.MerchantOrdID = HttpUtility.UrlEncode(resTxn.OrderNumber);
            merchantCallback.MerchantTxnAmt = resTxn.TxnAmount;
            merchantCallback.MerchantCurrCode = resTxn.CurrencyCode;
            if (!String.IsNullOrEmpty(resTxn.AuthCode))
            { 
                merchantCallback.AuthCode = resTxn.AuthCode;
            }
            merchantCallback.Sign = resTxn.HashValue2;
            merchantCallback.PTxnID = resTxn.MerchantID + resTxn.MerchantTxnID;
            merchantCallback.PTxnStatus = resTxn.TxnStatus.ToString();
            merchantCallback.BankRefNo = HttpUtility.UrlEncode(resTxn.BankRefNo);
            merchantCallback.PTxnMsg = resTxn.RespMesg;

            return merchantCallback;
        }

        public Merchant GetMerchant(string merchantID)
        {
            Merchant merchant = PaymentProvider.GetMerchant(merchantID);
            return merchant;
        }

        private string generateSignature(string headerfield, CCMerchantCredential ccMerchantCredential)
        {
            headerfield = headerfield + ccMerchantCredential.HASHKEY;
            ASCIIEncoding encoding = new ASCIIEncoding();

            Byte[] textBytes = encoding.GetBytes(headerfield);
            Byte[] keyBytes = encoding.GetBytes(ccMerchantCredential.HASHKEY);
            Byte[] hashBytes;

            using (HMACSHA256 hash = new HMACSHA256(keyBytes))
                hashBytes = hash.ComputeHash(textBytes);

            return BitConverter.ToString(hashBytes).Replace("-", "");
        }

        public ReqTxn GetReqTxn(string gatewayTxnID)
        {
            ReqTxn reqtxn = PaymentProvider.GetReqTxn(gatewayTxnID);
            return reqtxn;
        }

        public ReqTxn GetReqTxnByMerchantTxnID(string merchantTxnID)
        {
            ReqTxn reqtxn = PaymentProvider.GetReqTxnByMerchantTxnId(merchantTxnID);
            return reqtxn;
        }

        public ResTxn GetResTxn(string gatewayTxnID)
        {
            ResTxn restxn = PaymentProvider.GetResTxn(gatewayTxnID);
            return restxn;
        }

        public Host GetHost(string host)
        {
            Host hostInfo = PaymentProvider.GetHost(host);
            return hostInfo;
        }

        public int InsertTxnResp(ReqTxn reqTxn, Query0790Resp query0790Resp, string txnType)
        {
            //reqTxn.CardPAN = query0790Resp.f004_PAN;
            //InboundTest(query0790Resp.f004_PAN);
            reqTxn.CardPAN = Aes256CbcEncrypter.Encrypt(query0790Resp.f004_PAN, Config.PayMaster_AES256Key);
            reqTxn.BankRefNo = query0790Resp.f257_PGRN;
            reqTxn.TxnMsg = query0790Resp.f259_TxnStatMsg;
            reqTxn.Authcode= query0790Resp.f011_AuthIDResp;

            int insertTxnResp = PaymentProvider.InsertTxnResp(reqTxn, txnType);
            return insertTxnResp;
        }

        public int InsertVGS(string token, string gatewayTransactionId)
        {
            int insertVGS = PaymentProvider.InsertVGSData(token, gatewayTransactionId);
            return insertVGS;
        }

        public int UpdatePayTxnRef(ReqTxn reqTxn, Query0790Resp query0790Resp, string txnType)
        {
            int updatePayTxnRef = PaymentProvider.UpdatePayTxnRef(reqTxn, txnType);
            return updatePayTxnRef;
        }

        public int InsertCallBackStatus(ReqTxn reqTxn, int callBackStatus)
        {
            int InsertCallBackStatus = PaymentProvider.InsertCallBackStatus(reqTxn, callBackStatus);
            return InsertCallBackStatus;
        }

        public string Left(string value, int maxLength)
        {
            if (string.IsNullOrEmpty(value)) return value;
            maxLength = Math.Abs(maxLength);

            return (value.Length <= maxLength
                   ? value
                   : value.Substring(0, maxLength)
                   );
        }

        private void logMsg(CLoggerII.LOG_SEVERITY LOG_SEVERITY, string msg)
        {
            objLoggerII.Log(LOG_SEVERITY, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(), msg);
        }

        //public VGSInbound InboundTest(string CardPAN)
        //{
        //    Logger logger = new Logger();
        //    CLoggerII objLoggerII = new CLoggerII();
        //    string msg = string.Empty;
        //    objLoggerII = logger.InitLogger();
        //    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
        //           "inbound enter");
        //    VGSInbound inBoundResponse = new VGSInbound();
            


        //    try
        //    {
        //        using (var httpClient = new HttpClient())
        //        {
        //            using (var request = new HttpRequestMessage(new HttpMethod("POST"), "https://tntcdm4iz0p.sandbox.verygoodproxy.com/post"))
        //            {
        //                request.Content = new StringContent("{\"card_number\": \"" + CardPAN + "\"}");
        //                request.Content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");
        //                var responseString = "";
        //                var response = httpClient.SendAsync(request);
        //                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
        //                           "checkpoit");
        //                responseString = response.Result.Content.ReadAsStringAsync().Result;
                      
        //                inBoundResponse = JsonConvert.DeserializeObject<VGSInbound>(responseString);
        //                return inBoundResponse;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
        //                  "error inbound: " + ex.Message);
        //    }
        //    return new VGSInbound();
        //}

     //   public Task<VGSOutbound> OutboundTest(string token)
     //   {
     //       string aliasData = "{\"card_number\":\"" + token + "\"}";
     //       //string aliasData = "{\"card_number\":\"tok_sandbox_tvmv2AcKNVQ9NtDFAChfyf\"}";
     //       Logger logger = new Logger();
     //       CLoggerII objLoggerII = new CLoggerII();
     //       string msg = string.Empty;
     //       objLoggerII = logger.InitLogger();

     //       NetworkCredential credentials = new NetworkCredential("US2ergWeupKZM7gj32snEVUM", "3962c9b1-7cbc-42e3-84ca-b56bbc7cb0ce");
     //       WebProxy proxy = new WebProxy("http://tntcdm4iz0p.sandbox.verygoodproxy.com:8080", false)
     //       {
     //           UseDefaultCredentials = false,
     //           Credentials = credentials,
     //       };

     //       var handler = new HttpClientHandler()
     //       {
     //           Proxy = proxy,
     //           PreAuthenticate = true,
     //           UseDefaultCredentials = false,
     //       };

     //       try
     //       {
     //           System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
     //           HttpClient httpClient = new HttpClient(handler);
     //           StringContent content = new StringContent(aliasData, Encoding.UTF8, "application/json");
     //           HttpResponseMessage response = httpClient.PostAsync("https://echo.apps.verygood.systems/post", content).Result;
     //           var responseBody = response.Content.ReadAsStringAsync();

     //           var outBoundResponse = JsonConvert.DeserializeObject<VGSOutbound>(responseBody.Result);

     //           objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
     //                  "outbound resp: " + outBoundResponse.json.cardNumber);
     //           return Task.Run(
     //                       () =>
     //                       {

     //                           return outBoundResponse;
     //                       }

     //                       );
     //       }
     //       catch (HttpRequestException e)
     //       {
     //           objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
     //"error outbound: " + e.InnerException.Message);
     //       }
     //       return Task.Run(
     //            () =>
     //            {
     //                return new VGSOutbound();
     //            }
     //            );

     //   }
    }
}