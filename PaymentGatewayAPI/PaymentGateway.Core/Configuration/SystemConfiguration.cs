﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewayAPI.Core.Configuration
{
    public class SystemConfiguration
    {
        //PAYMASTER
        public const string PayMaster_MIDKeyName = "Paydibs.PaymentGatewayAPI.PayMaster#MID";
        public const string PayMaster_TIDKeyName = "Paydibs.PaymentGatewayAPI.PayMaster#TID";
        public const string PayMaster_RURL_UPPPSKeyName = "Paydibs.PaymentGatewayAPI.PayMaster#RURL_UPPPS";
        public const string PayMaster_RURL_UPPPUKeyName = "Paydibs.PaymentGatewayAPI.PayMaster#RURL_UPPPU";
        public const string PayMaster_RURL_UPPPCKeyName = "Paydibs.PaymentGatewayAPI.PayMaster#RURL_UPPPC";
        public const string PayMaster_POSTURLKeyName = "Paydibs.PaymentGatewayAPI.PayMaster#POSTURL";
        public const string PayMaster_HASHKEYKeyName = "Paydibs.PaymentGatewayAPI.PayMaster#HASHKEY";
        public const string PayMaster_VERSIONKeyName = "Paydibs.PaymentGatewayAPI.PayMaster#VERSION";
        public const string PayMaster_ServIDKeyName = "Paydibs.PaymentGatewayAPI.PayMaster#ServID";
        public const string PayMaster_POSTURL0200KeyName = "Paydibs.PaymentGatewayAPI.PayMaster#POSTURL0200";
        public const string PayMaster_PayMaster_AES256KeyName = "Paydibs.PaymentGatewayAPI.PayMaster#AES256Key";

        //public string PayMaster_MID { get; set; }
        //public string PayMaster_TID { get; set; }
        public string PayMaster_RURL_UPPPS { get; set; }
        public string PayMaster_RURL_UPPPU { get; set; }
        public string PayMaster_RURL_UPPPC { get; set; }
        public string PayMaster_POSTURL { get; set; }
        public string PayMaster_POSTURL0200 { get; set; }
        //public string PayMaster_HASHKEY { get; set; }
        public string PayMaster_VERSION { get; set; }
        public string PayMaster_ServID { get; set; }
        public string PayMaster_AES256Key { get; set; }

        //TNG
        public const string TNG_clientIdKeyName = "Paydibs.PaymentGatewayAPI.TNG#clientId";
        public const string TNG_clientSecretKeyName = "Paydibs.PaymentGatewayAPI.TNG#clientSecret";
        public const string TNG_merchantIdKeyName = "Paydibs.PaymentGatewayAPI.TNG#merchantId";
        public const string TNG_productCodeKeyName = "Paydibs.PaymentGatewayAPI.TNG#productCode";
        public const string TNG_POSTURLKeyName = "Paydibs.PaymentGatewayAPI.TNG#POSTURL";
        public const string TNG_URLPAY_RETURNKeyName = "Paydibs.PaymentGatewayAPI.TNG#URLPAY_RETURN";
        public const string TNG_URLNOTIFICATIONKeyName = "Paydibs.PaymentGatewayAPI.TNG#URLNOTIFICATION";
        public const string TNG_privatekeyKeyName = "Paydibs.PaymentGatewayAPI.TNG#privatekey";
        public const string TNG_versionKeyName = "Paydibs.PaymentGatewayAPI.TNG#version";
        public const string TNG_timeoutPeriod = "Paydibs.PaymentGatewayAPI.TNG#timeoutPeriod";
        public const string TNG_QueryUrlKeyName = "Paydibs.PaymentGatewayAPI.TNG#QueryUrl";
        public const string TNG_RefundUrlKeyName = "Paydibs.PaymentGatewayAPI.TNG#RefundUrl";


        public string TNG_clientId { get; set; }
        public string TNG_clientSecret { get; set; }
        public string TNG_merchantId { get; set; }
        public string TNG_productCode { get; set; }
        public string TNG_POSTURL { get; set; }
        public string TNG_URLPAY_RETURN { get; set; }
        public string TNG_URLNOTIFICATION { get; set; }
        public string TNG_privatekey { get; set; }
        public string TNG_version { get; set; }
        public string TNG_timeout { get; set; }
        public string TNG_QueryUrl { get; set; }
        public string TNG_RefundUrl { get; set; }

    }
}
