﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewayAPI.Core.Models
{
    public class MerchantCallback
    {
        [DataMember(Name = "TxnType")]
        public string TxnType { get; set; }

        [DataMember(Name = "Method")]
        public string Method { get; set; }

        [DataMember(Name = "MerchantID")]
        public string MerchantID { get; set; }

        [DataMember(Name = "MerchantPymtID")]
        public string MerchantPymtID { get; set; }

        [DataMember(Name = "MerchantOrdID")]
        public string MerchantOrdID { get; set; }

        [DataMember(Name = "MerchantTxnAmt")]
        public string MerchantTxnAmt { get; set; }

        [DataMember(Name = "MerchantCurrCode")]
        public string MerchantCurrCode { get; set; }

        [DataMember(Name = "AuthCode")]
        public string AuthCode { get; set; }

        [DataMember(Name = "Sign")]
        public string Sign { get; set; }

        [DataMember(Name = "PTxnID")]
        public string PTxnID { get; set; }

        [DataMember(Name = "AcqBank")]
        public string AcqBank { get; set; }

        [DataMember(Name = "PTxnStatus")]
        public string PTxnStatus { get; set; }

        [DataMember(Name = "BankRefNo")]
        public string BankRefNo { get; set; }

        [DataMember(Name = "RespTime")]
        public string RespTime { get; set; }

        [DataMember(Name = "PTxnMsg")]
        public string PTxnMsg { get; set; }

        public string ToUrlEncoded()
        {
            var raw = from property in GetType().GetProperties(BindingFlags.Public | BindingFlags.GetProperty | BindingFlags.Instance)
                      let dataMember = (DataMemberAttribute)property.GetCustomAttributes(typeof(DataMemberAttribute), false).FirstOrDefault()
                      let name = dataMember == null || String.IsNullOrWhiteSpace(dataMember.Name) ? property.Name : dataMember.Name
                      let value = property.GetValue(this)
                      where !(!dataMember.EmitDefaultValue && (value == null || value.Equals(property.GetGetMethod().ReturnType.IsValueType ? Activator.CreateInstance(property.GetGetMethod().ReturnType) : null)))
                      select WebUtility.UrlEncode(name.ToUpperInvariant()) + "=" + WebUtility.UrlEncode(!property.GetType().IsValueType && value == null ? "null" : FormatOutput(name, value));

            string result;
            return result = String.Join("&", raw);
        }

        private string FormatOutput(string name, object o)
        {
            if (o.GetType().IsEnum)
            {
                return (FlagsAttribute)o.GetType().GetCustomAttributes(typeof(FlagsAttribute), false).FirstOrDefault() == null
                    ? ((int)o).ToString(CultureInfo.InvariantCulture)
                    : String.Format(CultureInfo.InvariantCulture, "{0}", o);
            }

            if (o.GetType() == typeof(DateTime))
            {
                return String.Format(CultureInfo.InvariantCulture, "{0:yyyy-MM-dd HH:mm:ss \\MYT}", o);
            }

            if (o.GetType() == typeof(String))
            {
                return String.IsNullOrWhiteSpace((string)o) ? "Not Provided" : String.Format(CultureInfo.InvariantCulture, "{0}", o);
            }

            if (o.GetType() == typeof(List<string>))
            {
                return "[" + String.Join(",", (List<string>)o) + "]";
            }

            return String.Format(CultureInfo.InvariantCulture, "{0}", o);
        }
    }
}
