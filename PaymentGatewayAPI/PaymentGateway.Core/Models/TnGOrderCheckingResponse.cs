﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewayAPI.Core.Models
{
    public class TnGOrderCheckingResponse
    {
        [JsonProperty("acquirementId")]
        public string acquirementId { get; set; }

        [JsonProperty("amountDetail")]
        public AmountDetail amountDetail { get; set; }

        [JsonProperty("resultStatus")]
        public string resultStatus { get; set; }

        [JsonProperty("resultCode")]
        public string resultCode { get; set; }

        [JsonProperty("createdTime")]
        public DateTime createdTime { get; set; }

        [JsonProperty("merchantTransId")]
        public string merchantTransId { get; set; }

    }

    
}
