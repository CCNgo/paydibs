﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PaymentGatewayAPI.Core.Models
{
    public class CCInfo
    {
        public string Param4 { get; set; } //Customer Name
        public string CardExp { get; set; }
        public string CVV2 { get; set; }
        public string CustOCP { get; set; }
        public string CustName { get; set; }
        public string CustEmail { get; set; }
        public string Param2 { get; set; } //Card Number
        public string MerchantTxnId { get; set; }
        public string BankIssuer { get; set; }

    }
}