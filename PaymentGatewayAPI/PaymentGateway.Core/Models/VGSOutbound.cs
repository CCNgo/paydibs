﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewayAPI.Core.Models
{
    public class VGSOutbound
    {
        [JsonProperty("orderAmount")]
        public object args { get; set; }

        [JsonProperty("data")]
        public object data { get; set; }

        [JsonProperty("files")]
        public object files { get; set; }

        [JsonProperty("form")]
        public object form { get; set; }

        [JsonProperty("headers")]
        public object headers { get; set; }

        [JsonProperty("json")]
        public VGSOutboundJson json { get; set; }

        [JsonProperty("origin")]
        public object origin { get; set; }

        [JsonProperty("url")]
        public object url { get; set; }

    }

    public class VGSOutboundJson
    {
        [JsonProperty("card_number")]
        public string cardNumber { get; set; }
    }
}
