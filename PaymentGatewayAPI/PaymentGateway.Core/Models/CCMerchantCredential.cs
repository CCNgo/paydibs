﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewayAPI.Core.Models
{
    public class CCMerchantCredential
    {
        public string HostID { get; set; }
        public string PaydibsMID { get; set; }
        public string MerchantID { get; set; }
        public string TerminalID { get; set; }
        public string HASHKEY { get; set; }
    }
}
