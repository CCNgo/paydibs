﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewayAPI.Core.Models
{
    public class TnGOrderRefundRequest
    {
        [JsonProperty("request")]
        public OrderRefundRequest TnGORRequest { get; set; }

        [JsonProperty("signature")]
        public string Signature { get; set; }
    }

    public class OrderRefundRequest
    {
        [JsonProperty("head")]
        public OrderRefundRequestHead Head { get; set; }

        [JsonProperty("body")]
        public OrderRefundRequestBody Body { get; set; }
    }

    public class OrderRefundRequestHead
    {
        [JsonProperty("version")]
        public string version { get; set; }

        [JsonProperty("function")]
        public string function { get; set; }

        [JsonProperty("clientId")]
        public string clientId { get; set; }

        [JsonProperty("clientSecret")]
        public string clientSecret { get; set; }

        [JsonProperty("reqTime")]
        public string reqTime { get; set; }

        [JsonProperty("reqMsgId")]
        public string reqMsgId { get; set; }
    }

    public class OrderRefundRequestBody
    {
        [JsonProperty("requestId")]
        public string requestId { get; set; }

        [JsonProperty("acquirementId")]
        public string acquirementId { get; set; }

        [JsonProperty("merchantId")]
        public string merchantId { get; set; }

        [JsonProperty("refundAmount")]
        public Money refundAmount { get; set; }

    }


    public enum ActorTypeEnum
    {
        USER = 1,
        MERCHANT = 2,
        MERCHANT_OPERATOR = 3,
        BACK_OFFICE = 4,
        SYSTEM = 5,
    }

    public enum RefundDestinationEnum   
    {
        TO_BALANCE = 1,
        TO_SOURCE = 2,
    }
}
