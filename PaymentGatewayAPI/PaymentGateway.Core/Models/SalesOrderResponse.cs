﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewayAPI.Core.Models
{
    public class SalesOrderResponse : BaseResponse<SalesOrderResponse>
    {
        public static SalesOrderResponse OK
        {
            get
            {
                return new SalesOrderResponse() { Result = new Result() { Code = "00", Message = "API Response OK" } };
            }
        }

        public static SalesOrderResponse InvalidAccount
        {
            get { return new SalesOrderResponse() { Result = new Result() { Code = "01", Message = "Invalid Account." } }; }
        }

        public static SalesOrderResponse InsufficientBalance
        {
            get
            {
                return new SalesOrderResponse() { Result = new Result() { Code = "02", Message = "Insufficient Balance" } };
            }
        }

        public static SalesOrderResponse InvalidProduct
        {
            get
            {
                return new SalesOrderResponse() { Result = new Result() { Code = "03", Message = "Invalid Product." } };
            }
        }

        public static SalesOrderResponse InsufficientProduct
        {
            get
            {
                return new SalesOrderResponse() { Result = new Result() { Code = "04", Message = "Insufficient Product." } };
            }
        }

        public static SalesOrderResponse ConnectionTimeout
        {
            get
            {
                return new SalesOrderResponse() { Result = new Result() { Code = "05", Message = "Connection timeout." } };
            }
        }

        public static SalesOrderResponse Pending
        {
            get
            {
                return new SalesOrderResponse() { Result = new Result() { Code = "06", Message = "Transaction In Progress..." } };
            }
        }

        public static SalesOrderResponse Failed
        {
            get
            {
                return new SalesOrderResponse() { Result = new Result() { Code = "07", Message = "Transaction Failed. Please Try Again." } };
            }
        }

        public string RedirectURL { get; set; }

        public string merchantTransId { get; set; }

        public decimal value { get; set; }

        public string acquirementStatus { get; set; }

        public string bankrefno { get; set; }

        //public IList<string> Orders { get; set; }

        //public string Response { get; set; }
    }
}
