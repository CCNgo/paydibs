﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewayAPI.Core.Models
{
    public class TnGOrderQueryResponse
    {
        [JsonProperty("response")]
        public OrderQueryResponse TnGOQResponse { get; set; }

        [JsonProperty("signature")]
        public string Signature { get; set; }
    }

    public class OrderQueryResponse
    {
        [JsonProperty("head")]
        public OrderQueryResponseHead Head { get; set; }

        [JsonProperty("body")]
        public OrderQueryResponseBody Body { get; set; }
    }

    public class OrderQueryResponseHead
    {
        [JsonProperty("version")]
        public string version { get; set; }

        [JsonProperty("function")]
        public string function { get; set; }

        [JsonProperty("clientId")]
        public string clientId { get; set; }

        [JsonProperty("respTime")]
        public string respTime { get; set; }

        [JsonProperty("reqMsgId")]
        public string reqMsgId { get; set; }

        [JsonProperty("reserve")]
        public string reserve { get; set; }
    }

    public class OrderQueryResponseBody
    {
        [JsonProperty("resultInfo")]
        internal ResultInfo resultInfo { get; set; }

        [JsonProperty("acquirementId")]
        public string acquirementId { get; set; }

        [JsonProperty("merchantTransId")]
        public string merchantTransId { get; set; }

        [JsonProperty("buyer")]
        public InputUserInfo buyer { get; set; }

        [JsonProperty("seller")]
        public InputUserInfo seller { get; set; }

        [JsonProperty("orderTitle")]
        public string orderTitle { get; set; }

        [JsonProperty("extendInfo")]
        public string extendInfo { get; set; }

        [JsonProperty("amountDetail")]
        public AmountDetail amountDetail { get; set; }

        [JsonProperty("timeDetail")]
        public TimeDetail timeDetail { get; set; }

        [JsonProperty("statusDetail")]
        public StatusDetail statusDetail { get; set; }

        [JsonProperty("goods")]
        public List<Goods> goods { get; set; }

        [JsonProperty("shippingInfo")]
        public List<ShippingInfo> shippingInfo { get; set; }

        [JsonProperty("orderMemo")]
        public string orderMemo { get; set; }

        [JsonProperty("paymentViews")]
        public List<PaymentView> paymentViews { get; set; }
    }

    public class InputUserInfo
    {
        [JsonProperty("userId")]
        public string userId { get; set; }

        [JsonProperty("externalUserId")]
        public string externalUserId { get; set; }

        [JsonProperty("externalUserType")]
        public string externalUserType { get; set; }

        [JsonProperty("nickname")]
        public string nickname { get; set; }

    }

    public class AmountDetail
    {
        [JsonProperty("orderAmount")]
        public Money orderAmount{ get; set; }

        [JsonProperty("payAmount")]
        public Money payAmount { get; set; }

        [JsonProperty("voidAmount")]
        public Money voidAmount { get; set; }

        [JsonProperty("confirmAmount")]
        public Money confirmAmount { get; set; }

        [JsonProperty("refundAmount")]
        public Money refundAmount { get; set; }

        [JsonProperty("chargebackAmount")]
        public Money chargebackAmount { get; set; }

        [JsonProperty("chargeAmount")]
        public Money chargeAmount { get; set; }
    }

    public class TimeDetail
    {
        [JsonProperty("createdTime")]
        public DateTime createdTime { get; set; }

        [JsonProperty("expiryTime")]
        public DateTime expiryTime { get; set; }

        [JsonProperty("paidTimes")]
        public List<DateTime> paidTimes { get; set; }

        [JsonProperty("confirmedTimes")]
        public List<DateTime> confirmedTimes { get; set; }
    }

    public class StatusDetail
    {
        [JsonProperty("acquirementStatus")]
        public StatusDetailEnum acquirementStatus { get; set; }

        [JsonProperty("frozen")]
        public bool frozen { get; set; }
    }

    public class Goods
    {
        [JsonProperty("merchantGoodsId")]
        public string merchantGoodsId { get; set; }

        [JsonProperty("description")]
        public string description { get; set; }

        [JsonProperty("category")]
        public string category { get; set; }

        [JsonProperty("price")]
        public Money price { get; set; }

        [JsonProperty("unit")]
        public string unit { get; set; }

        [JsonProperty("quantity")]
        public string quantity { get; set; }

        [JsonProperty("merchantShippingId")]
        public string merchantShippingId { get; set; }

        [JsonProperty("snapshotUrl")]
        public string snapshotUrl { get; set; }

        [JsonProperty("extendInfo")]
        public string extendInfo { get; set; }
    }

    public class ShippingInfo
    {
        [JsonProperty("merchantShippingId")]
        public string merchantShippingId { get; set; }
        [JsonProperty("trackingNo")]
        public string trackingNo { get; set; }
        [JsonProperty("carrier")]
        public string carrier { get; set; }
        [JsonProperty("chargeAmount")]
        public Money chargeAmount { get; set; }
        [JsonProperty("countryName")]
        public string countryName { get; set; }
        [JsonProperty("stateName")]
        public string stateName { get; set; }
        [JsonProperty("cityName")]
        public string cityName { get; set; }
        [JsonProperty("areaName")]
        public string areaName { get; set; }
        [JsonProperty("address1")]
        public string address1 { get; set; }
        [JsonProperty("address2")]
        public string address2 { get; set; }
        [JsonProperty("firstName")]
        public string firstName { get; set; }
        [JsonProperty("lastName")]
        public string lastName { get; set; }
        [JsonProperty("mobileNo")]
        public string mobileNo { get; set; }
        [JsonProperty("phoneNo")]
        public string phoneNo { get; set; }
        [JsonProperty("zipCode")]
        public string zipCode { get; set; }
        [JsonProperty("email")]
        public string email { get; set; }
        [JsonProperty("faxNo")]
        public string faxNo { get; set; }
    }

    public class PaymentView
    {
        [JsonProperty("cashierRequestId")]
        public string cashierRequestId { get; set; }
        [JsonProperty("paidTime")]
        public DateTime paidTime { get; set; }
        [JsonProperty("payOptionInfos")]
        public List<PayOptionInfo> payOptionInfos { get; set; }
        [JsonProperty("payRequestExtendInfo")]
        public string payRequestExtendInfo { get; set; }
        [JsonProperty("extendInfo")]
        public string extendInfo { get; set; }
    }

    public class PayOptionInfo
    {
        [JsonProperty("payMethod")]
        public PayMethodEnum payMethod { get; set; }
        [JsonProperty("payAmount")]
        public Money payAmount { get; set; }
        [JsonProperty("transAmount")]
        public Money transAmount { get; set; }
        [JsonProperty("chargeAmount")]
        public Money chargeAmount { get; set; }
        [JsonProperty("extendInfo")]
        public string extendInfo { get; set; }
        [JsonProperty("payOptionBillExtendInfo")]
        public string payOptionBillExtendInfo { get; set; }
    }

    public enum PayMethodEnum
    {
        BALANCE = 1,
        NET_BANKING = 2,
        CREDIT_CARD = 3,
        DEBIT_CARD = 4,
        VIRTUAL_ACCOUNT = 5,
        OTC = 6
    }
}
