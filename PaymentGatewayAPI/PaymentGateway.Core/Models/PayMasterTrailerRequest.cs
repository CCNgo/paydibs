﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewayAPI.Core.Models
{
    public class PayMasterTrailerRequest
    {
        public string t001_SHT { get; set; }
        public string t002_SHV { get; set; }
    }
}
