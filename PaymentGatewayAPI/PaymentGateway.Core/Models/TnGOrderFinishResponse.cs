﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewayAPI.Core.Models
{
    public class TnGOrderFinishFinalResponse
    {
        public HttpResponseMessage QueryResult { get; set; }

        public TnGOrderFinishResponse OFResponse { get; set; }
    }
    public class TnGOrderFinishResponse
    {
        [JsonProperty("request")]
        public OrderFinishResponse TnGOFResponse { get; set; }

        [JsonProperty("signature")]
        public string Signature { get; set; }
    }

    public class OrderFinishResponse
    {
        [JsonProperty("head")]
        public OrderFinishResponseHead Head { get; set; }

        [JsonProperty("body")]
        public OrderFinishResponseBody Body { get; set; }
    }

    public class OrderFinishResponseHead
    {
        [JsonProperty("version")]
        public string version { get; set; }

        [JsonProperty("function")]
        public string function { get; set; }

        [JsonProperty("clientId")]
        public string clientId { get; set; }

        [JsonProperty("respTime")]
        public string respTime { get; set; }

        [JsonProperty("reqMsgId")]
        public string reqMsgId { get; set; }
    }

    public class OrderFinishResponseBody
    {
        [JsonProperty("resultInfo")]
        internal ResultInfo resultInfo { get; set; }
    }  

    public class TNGPayReturn
    {
        [JsonProperty("clientId")]
        public string clientId { get; set; }
        [JsonProperty("merchantId")]
        public string merchantId { get; set; }
        [JsonProperty("merchantTransId")]
        public string merchantTransId { get; set; }
        [JsonProperty("version")]
        public string version { get; set; }
    }

}
