﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewayAPI.Core.Models
{
    public class ResTxn
    {
        //public string TxnType { get; set; }
        public string MerchantID { get; set; }
        public string MerchantTxnID { get; set; }
        public string MerchantName { get; set; }
        public string OrderNumber { get; set; }
        public string TxnAmount { get; set; }
        public string BaseTxnAmount { get; set; }
        public string CurrencyCode { get; set; }
        public string BaseCurrencyCode { get; set; }
        public string LanguageCode { get; set; }
        public string IssuingBank { get; set; }
        public string SessionID { get; set; }
        public string Param1 { get; set; }
        public string Param2 { get; set; }
        public string Param3 { get; set; }
        public string Param4 { get; set; }
        public string Param5 { get; set; }
        public string Param6 { get; set; }
        public string Param7 { get; set; }
        public string GatewayTxnID { get; set; }
        public string DateCreated { get; set; }
        public string OrderDesc { get; set; }
        public string TxnState { get; set; }
        public int TxnStatus { get; set; }
        public string MerchantTxnStatus { get; set; }
        public string Action { get; set; }
        public string Duration { get; set; }
        public string OSRet { get; set; }
        public string ErrSet { get; set; }
        public string ErrNum { get; set; }
        public string BankRespCode { get; set; }
        public string BankRefNo { get; set; }
        public string RespMesg { get; set; }
        public string HashMethod { get; set; }
        public string HashValue { get; set; }
        public string GatewayID { get; set; }
        public string CardPAN { get; set; }
        public string MachineID { get; set; }
        public string MerchantReturnURL { get; set; }
        public string MerchantSupportURL { get; set; }
        public string MerchantApprovalURL { get; set; }
        public string MerchantUnApprovalURL { get; set; }
        public string MerchantCallbackURL { get; set; }
        public string OSPymtCode { get; set; }
        public string HostID { get; set; }
        public string CustEmail { get; set; }
        public string CustName { get; set; }
        public string FXCurrencyCode { get; set; }
        public string FXTxnAmt { get; set; }
        public string TokenType { get; set; }
        public string RetCode { get; set; }
        public string HashValue2 { get; set; }
        public string AuthCode { get; set; }
    }
}
