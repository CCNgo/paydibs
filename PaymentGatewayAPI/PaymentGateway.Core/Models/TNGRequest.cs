﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewayAPI.Core.Models
{
    internal class TNGRequest
    {
        [JsonProperty("request")]
        public Request Request { get; set; }

        [JsonProperty("signature")]
        public string Signature { get; set; }
    }

    internal class Request
    {
        [JsonProperty("head")]
        public RequestHead Head { get; set; }

        [JsonProperty("body")]
        public RequestBody Body { get; set; }
    }

    internal class RequestHead
    {
        [JsonProperty("version")]
        public string version { get; set; }

        [JsonProperty("function")]
        public string function { get; set; }

        [JsonProperty("clientId")]
        public string clientId { get; set; }

        [JsonProperty("clientSecret")]
        public string clientSecret { get; set; }

        [JsonProperty("reqTime")]
        public string reqTime { get; set; }

        [JsonProperty("reqMsgId")]
        public string reqMsgId { get; set; }
    }

    internal class RequestBody
    {
        [JsonProperty("order")]
        public Order order { get; set; }

        [JsonProperty("merchantId")]
        public string merchantId { get; set; }

        [JsonProperty("productCode")]
        public string productCode { get; set; }

        [JsonProperty("signAgreementPay")]
        public Boolean signAgreementPay { get; set; }

        [JsonProperty("envInfo")]
        public EnvInfo envInfo { get; set; }

        [JsonProperty("notificationUrls")]
        public List<NotificationUrls> notificationUrls { get; set; }

        [JsonProperty("extendInfo")]
        public string extendInfo { get; set; }

        [JsonProperty("subMerchantId")]
        public string subMerchantId { get; set; }

        [JsonProperty("subMerchantName")]
        public string subMerchantName { get; set; }
    }

    internal class Order
    {
        [JsonProperty("orderTitle")]
        public string orderTitle { get; set; }

        [JsonProperty("orderAmount")]
        public OrderAmount orderAmount { get; set; }

        [JsonProperty("merchantTransId")]
        public string merchantTransId { get; set; }

        [JsonProperty("expiryTime")]
        public string expiryTime { get; set; }


    }

    internal class OrderAmount
    {
        [JsonProperty("currency")]
        public string currency { get; set; }

        [JsonProperty("value")]
        public string value { get; set; }
    }

    internal class EnvInfo
    {
        [JsonProperty("terminalType")]
        public string terminalType { get; set; }

        [JsonProperty("orderTerminalType")]
        public string orderTerminalType { get; set; }
    }

    internal class NotificationUrls
    {
        [JsonProperty("url")]
        public string url { get; set; }

        [JsonProperty("type")]
        public string type { get; set; }
    }
}
