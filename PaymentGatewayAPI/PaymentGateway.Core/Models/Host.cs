﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewayAPI.Core.Models
{
    public class Host
    {
        public string HostID { get; set; }
        public string PaymentTemplate { get; set; }
        public string SecondEntryTemplate { get; set; }
        public string MesgTemplate { get; set; }
        public string Require2ndEntry { get; set; }
        public string CfgFile { get; set; }
        public string NeedReplyAcknowledgement { get; set; }
        public string NeedRedirectOTP { get; set; }
        public string TxnStatusActionID { get; set; }
        public string HostReplyMethod { get; set; }
        public string OSPymtCode { get; set; }
        public string GatewayTxnIDFormat { get; set; }
        public string Timeout { get; set; }
        public string ChannelTimeout { get; set; }
        //public string OTCRevTimeout { get; set; }
        public string HashMethod { get; set; }
        public string ReturnURL { get; set; }
        public string ReturnIPAddresses { get; set; }
        public string QueryFlag { get; set; }
        public string IsActive { get; set; }
        public string AllowQuery { get; set; }
        public string AllowReversal { get; set; }
        public string Desc { get; set; }
        public string Protocol { get; set; }
        //public string FixedCurrency { get; set; }
        //public string LogoPath { get; set; }
        //public string MID { get; set; }
        public string SecretKey { get; set; }
        public string InitVector { get; set; }
        public string LogRes { get; set; }
        public string QueryURL { get; set; }
        //public string OTCGenMethod { get; set; }
    }
}
