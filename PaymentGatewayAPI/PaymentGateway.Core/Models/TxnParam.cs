﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewayAPI.Core.Models
{
    public class TxnParam
    {
        public string MerchantID { get; set; }
        public string MerchantTxnID { get; set; }
        public string ReqRes { get; set; }
    }
}
