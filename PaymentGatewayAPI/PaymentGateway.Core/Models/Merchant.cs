﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewayAPI.Core.Models
{
    public class Merchant
    {
        public string PaymentTemplate { get; set; }
        public string ErrorTemplate { get; set; }
        public string AllowReversal { get; set; }
        public string AllowFDS { get; set; }
        public string CollectShipAddr { get; set; }
        public string CollectBillAddr { get; set; }
        public string AllowMaxMind { get; set; }
        public string FraudByAmt { get; set; }
        public string SvcTypeID { get; set; }
        public string AllowPayment { get; set; }
        public string AllowQuery { get; set; }
        public string AllowOB { get; set; }
        public string AllowOTC { get; set; }
        public string AllowWallet { get; set; }
        public string AllowOCP { get; set; }
        public string AllowCallBack { get; set; }
        public string AllowFX { get; set; }
        public string RouteByParam1 { get; set; }
        public string PymtPageTimeout_S { get; set; }
        public string ThreeDAccept { get; set; }
        public string SelfMPI { get; set; }
        public string AutoReversal { get; set; }
        public string RespMethod { get; set; }
        public string HCProfileID { get; set; }
        public string PerTxnAmtLimit { get; set; }
        public string HitLimitAct { get; set; }
        public string City { get; set; }
        public string Country2 { get; set; }
        public string Addr1 { get; set; }
        public string Addr2 { get; set; }
        public string Addr3 { get; set; }
        public string PostCode { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string ContactNo { get; set; }
        public string EmailAddr { get; set; }
        public string NotifyEmailAddr { get; set; }
        public string WebSiteURL { get; set; }
        public string ValidDomain { get; set; }
        public string VISA { get; set; }
        public string MasterCard { get; set; }
        public string AMEX { get; set; }
        public string JCB { get; set; }
        public string Diners { get; set; }
        public string CUP { get; set; }
        public string MasterPass { get; set; }
        public string VisaCheckout { get; set; }
        public string SamsungPay { get; set; }
        public string NeedAddOSPymt { get; set; }
        public string TxnExpired_S { get; set; }
        public string MerchantName { get; set; }
        public string Receipt { get; set; }
        public string PymtNotificationEmail { get; set; }
        public string PymtNotificationSMS { get; set; }
        public string FXRate { get; set; }
        public string ShowMerchantAddr { get; set; }
        public string ShowMerchantLogo { get; set; }
        public string ExtraCSS { get; set; }
        public string OTCExpiryHour { get; set; }
        public string ReturnCardData { get; set; }
        public string PROMO { get; set; }
        public string AllowInstallment { get; set; }
        public string MCCCode { get; set; }
        public string AllowExtFDS { get; set; }
        public string FDSCustomerID { get; set; }
        public string FDSAuthCode { get; set; }
        public string Protocol { get; set; }
        public string AcqCountryCode { get; set; }
        public string CVVRequire { get; set; }
        public string MerchantPassword { get; set; }

    }
}
