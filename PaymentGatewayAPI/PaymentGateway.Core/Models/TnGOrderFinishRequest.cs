﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewayAPI.Core.Models
{
    public class TnGOrderFinishRequest
    {
        [JsonProperty("request")]
        public OrderFinishRequest TnGOFRequest { get; set; }

        [JsonProperty("signature")]
        public string Signature { get; set; }
    }

    public class OrderFinishRequest
    {
        [JsonProperty("head")]
        public OrderFinishRequestHead Head { get; set; }

        [JsonProperty("body")]
        public OrderFinishRequestBody Body { get; set; }
    }

    public class OrderFinishRequestHead
    {
        [JsonProperty("version")]
        public string version { get; set; }

        [JsonProperty("function")]
        public string function { get; set; }

        [JsonProperty("clientId")]
        public string clientId { get; set; }

        [JsonProperty("reqTime")]
        public string reqTime { get; set; }

        [JsonProperty("reqMsgId")]
        public string reqMsgId { get; set; }
    }

    public class OrderFinishRequestBody
    {
        [JsonProperty("acquirementId")]
        public string acquirementId { get; set; }

        [JsonProperty("merchantTransId")]
        public string merchantTransId { get; set; }

        [JsonProperty("finishedTime")]
        public DateTime finishedTime { get; set; }

        [JsonProperty("createdTime")]
        public DateTime createdTime { get; set; }

        [JsonProperty("merchantId")]
        public string merchantId { get; set; }

        [JsonProperty("orderAmount")]
        public Money orderAmount { get; set; }

        [JsonProperty("acquirementStatus")]
        public StatusDetailEnum acquirementStatus { get; set; }

        [JsonProperty("extendInfo")]
        public string extendInfo { get; set; }

      
    }

    public class Money
    {
        [JsonProperty("currency")]
        public string currency { get; set; }

        [JsonProperty("value")]
        public int value { get; set; }
    }
    public enum StatusDetailEnum
    {
        INIT = 1,
        SUCCESS = 2,
        CLOSED = 3,
        PAYING = 4,
        MERCHANT_ACCEPT = 5,
        CANCELLED = 6
    }

}
