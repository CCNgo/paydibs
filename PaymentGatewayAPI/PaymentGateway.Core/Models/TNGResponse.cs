﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewayAPI.Core.Models
{
    internal class TNGResponse
    {
        [JsonProperty("response")]
        public Response Response { get; set; }

        [JsonProperty("signature")]
        public string Signature { get; set; }
    }

    internal class Response
    {
        [JsonProperty("head")]
        public ResponseHead Head { get; set; }

        [JsonProperty("body")]
        public ResponseBody Body { get; set; }
    }

    internal class ResponseHead
    {
        [JsonProperty("version")]
        public string version { get; set; }

        [JsonProperty("function")]
        public string function { get; set; }

        [JsonProperty("clientId")]
        public string clientId { get; set; }

        [JsonProperty("clientSecret")]
        public string clientSecret { get; set; }

        [JsonProperty("reqTime")]
        public string reqTime { get; set; }

        [JsonProperty("reqMsgId")]
        public string reqMsgId { get; set; }
    }

    internal class ResponseBody
    {
        [JsonProperty("acquirementId")]
        public string acquirementId { get; set; }

        [JsonProperty("checkoutSign")]
        public string checkoutSign { get; set; }

        [JsonProperty("checkoutTimeStamp")]
        public string checkoutTimeStamp { get; set; }

        [JsonProperty("checkoutUrl")]
        public string checkoutUrl { get; set; }

        [JsonProperty("merchantTransId")]
        public string merchantTransId { get; set; }

        [JsonProperty("resultInfo")]
        public ResultInfo resultInfo { get; set; }
    }

    internal class ResultInfo
    {
        [JsonProperty("resultStatus")]
        public string resultStatus { get; set; }

        [JsonProperty("resultCodeId")]
        public string resultCodeId { get; set; }

        [JsonProperty("resultCode")]
        public string resultCode { get; set; }

        [JsonProperty("resultMsg")]
        public string resultMsg { get; set; }

    }
}
