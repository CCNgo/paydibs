﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewayAPI.Core.Models
{
    public class merchantinfo
    {
        public string id { get; set; }
        public string merchantName { get; set; }
        public string merchantTxnID { get; set; }
        public string merchantAddr { get; set; }
        public string merchantContactNo { get; set; }
        public string merchantWebsiteURL { get; set; }
        public string merchantEmailAddr { get; set; }
    }
}
