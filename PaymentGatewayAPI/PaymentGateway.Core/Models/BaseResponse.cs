﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewayAPI.Core.Models
{
    public class BaseResponse<T> where T : BaseResponse<T>
    {
        public Result Result { get; set; }

        public static T Success()
        {
            {
                var response = Activator.CreateInstance<T>();
                response.Result = new Result()
                {
                    Code = "00",
                    Message = "Success",
                };

                return response;
            }
        }

        //public static T SuccessWithUrl(string url)
        //{
        //    {
        //        var response = Activator.CreateInstance<T>();
        //        response.Result = new Result()
        //        {
        //            Code = "00",
        //            Message = "Success",
        //            Url = url
        //        };

        //        return response;
        //    }
        //}

        public static T SystemError
        {
            get
            {
                var response = Activator.CreateInstance<T>();
                response.Result = new Result()
                {
                    Code = "99",
                    Message = "System Error"
                };

                return response;
            }
        }
    }
}
