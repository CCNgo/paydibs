﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewayAPI.Core.Models
{
    public class NotifyStatus
    {
        public string CallBackStatus { get; set; }
        public string ShopperEmailNotifyStatus { get; set; }
        public string MerchantEmailNotifyStatus { get; set; }
    }
}
