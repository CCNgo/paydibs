﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewayAPI.Core.Models
{
    public class PayMasterHeaderResponse
    {
        public string h001_MTI { get; set; }
        public string h002_VNO { get; set; }
        public string h003_TDT { get; set; }
        public string h004_TTM { get; set; }
    }
}
