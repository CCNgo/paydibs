﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewayAPI.Core.Models
{
    public class TnGOrderQueryRequest
    {
        [JsonProperty("request")]
        public OrderQueryRequest TnGOQRequest { get; set; }

        [JsonProperty("signature")]
        public string Signature { get; set; }
    }

    public class OrderQueryRequest
    {
        [JsonProperty("head")]
        public OrderQueryRequestHead Head { get; set; }

        [JsonProperty("body")]
        public OrderQueryRequestBody Body { get; set; }
    }

    public class OrderQueryRequestHead
    {
        [JsonProperty("version")]
        public string version { get; set; }

        [JsonProperty("function")]
        public string function { get; set; }

        [JsonProperty("clientId")]
        public string clientId { get; set; }

        [JsonProperty("clientSecret")]
        public string clientSecret { get; set; }

        [JsonProperty("reqTime")]
        public string reqTime { get; set; }

        [JsonProperty("reqMsgId")]
        public string reqMsgId { get; set; }

        [JsonProperty("reserve")]
        public string reserve { get; set; }


    }

    public class OrderQueryRequestBody
    {
        [JsonProperty("merchantId")]
        public string merchantId { get; set; }

        [JsonProperty("acquirementId")]
        public string acquirementId { get; set; }

        [JsonProperty("merchantTransId")]
        public string merchantTransId { get; set; }

    }

}
