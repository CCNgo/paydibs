﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewayAPI.Core.Models
{
    public class SalesOrderRequest
    {
        public string mid { get; set; }
        public string gatewaytxnid { get; set; }
        public string txnamount { get; set; }
        public string orderdesc { get; set; }
        public string gatewayreturnurl { get; set; }
        public merchantinfo merchantinfo { get; set; }
        public string merchantId { get; set; }
        public string onlineRefNum { get; set; }
        public decimal amount { get; set; }
        public string orderTitle { get; set; }
        public string reqTime { get; set; }
        public string reqMsgId { get; set; }
        public string expiryTime { get; set; }
        public string version { get; set; }
        public string function { get; set; }
        public string clientId { get; set; }
        public string clientSecret { get; set; }
        public string productCode { get; set; }
        public string currency { get; set; }
        public string payreturnurl { get; set; }
        public string notificationurl { get; set; }
        public string ordernumber { get; set; }
        public string ipaddress { get; set; }
        public string custEmail { get; set; }
        public string custName { get; set; }
        public string custPhone { get; set; }
        public string gatewaycancelurl { get; set; }
        public string timeout { get; set; }
        //public string merchantinfoid { get; set; }
        //public string merchantinfoName { get; set; }

    }
}
