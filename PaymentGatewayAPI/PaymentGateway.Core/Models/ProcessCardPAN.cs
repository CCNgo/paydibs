﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewayAPI.Core.Models
{
    //local use only
    public class ProcessCardPAN
    {
        public string ID { get; set; }
        public int PKID { get; set; }
        public string DecryptedCardPAN { get; set; }
        public string EncryptedCardPAN { get; set; }
        public string ProcessAES256Key { get; set; }
    }
}
