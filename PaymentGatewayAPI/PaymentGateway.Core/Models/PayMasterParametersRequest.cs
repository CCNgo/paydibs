﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewayAPI.Core.Models
{
    public class PayMasterParametersRequest
    {
        public string f001_MID { get; set; }
        public string f003_ProcCode { get; set; }
        public string f004_PAN { get; set; }
        public string f005_ExpDate { get; set; }
        public string f006_TxnDtTm { get; set; }
        public string f007_TxnAmt { get; set; }
        public string f008_POSCond { get; set; }
        public string f010_CurrCode { get; set; }
        public string f011_AuthIDResp { get; set; }
        public string f012_CVV2 { get; set; }
        public string f014_3DXID { get; set; }
        public string f015_3DARC { get; set; }
        public string f016_3DCAVVLen { get; set; }
        public string f017_3DCAVV { get; set; }
        public string f019_ExpTxnAmt { get; set; }
        public string f022_ECI { get; set; }
        public string f023_RRN { get; set; }
        public string f247_OrgTxnAmt { get; set; }
        public string f248_OrgCurrCode { get; set; }
        public string f249_TxnCh { get; set; }
        public string f250_CCProcFeeAmt { get; set; }
        public string f251_DDProcFeeAmt { get; set; }
        public string f252_PromoCode { get; set; }
        public string f253_CntyCode { get; set; }
        public string f254_DDRespCode { get; set; }
        public string f255_IssCode { get; set; }
        public string f256_FICode { get; set; }
        public string f257_PGRN { get; set; }
        public string f260_ServID { get; set; }
        public string f261_HostID { get; set; }
        public string f262_SessID { get; set; }
        public string f263_MRN { get; set; }
        public string f264_Locale { get; set; }
        public string f265_RURL_CCPS { get; set; }
        public string f266_RURL_CCPU { get; set; }
        public string f267_RURL_CCPC { get; set; }
        public string f268_CHName { get; set; }
        public string f269_IssName { get; set; }
        public string f270_ORN { get; set; }
        public string f271_ODesc { get; set; }
        public string f272_RURL_CCR { get; set; }
        public string f278_EMailAddr { get; set; }
        public string f279_HP { get; set; }
        public string f280_RURL_UPPPS { get; set; }
        public string f281_RURL_UPPPU { get; set; }
        public string f282_RURL_UPPPC { get; set; }
        public string f285_IPAddr { get; set; }
        public string f287_ExpOrgTxnAmt { get; set; }
        public string f288_IssCntrCde { get; set; }
        public string f289_CustId { get; set; }
        public string f290_Signature { get; set; }
        public string f325_ECommMercInd { get; set; }
        public string f339_TokenFlg { get; set; }
        public string f340_MercPromoCde { get; set; }
        public string f341_MercPromoAmt { get; set; }
        public string f342_PromoAmtAcqCst { get; set; }
        public string f343_PromoAmtMercCst { get; set; }
        public string f344_MercCustId { get; set; }
        public string f347_TokenShrtNm { get; set; }
        public string f350_CrdTyp { get; set; }
        public string f353_IPPTenure { get; set; }
        public string f354_TID { get; set; }
        public string f352_AcqBank { get; set; }
        public string f362_PreAuthFlg { get; set; }
        public string f363_InvNum { get; set; }
        public string f364_Fee { get; set; }
    }
}
