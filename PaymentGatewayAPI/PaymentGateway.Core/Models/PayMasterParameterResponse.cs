﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewayAPI.Core.Models
{
    public class PayMasterParameterResponse
    {
        public string f001_MID { get; set; }
        public string f004_PAN { get; set; }
        public string f006_TxnDtTm { get; set; }
        public string f007_TxnAmt { get; set; }
        public string f009_RespCode { get; set; }
        public string f010_CurrCode { get; set; }

        public string f011_AuthIDResp { get; set; }
        public string f019_ExpTxnAmt { get; set; }
        public string f023_RRN { get; set; }
        public string f024_OrgRespCode { get; set; }

        public string f254_DDRespCode { get; set; }
        public string f256_FICode { get; set; }
        public string f257_PGRN { get; set; }
        public string f258_TxnStatDetCde { get; set; }
        public string f259_TxnStatMsg { get; set; }
        public string f260_ServID { get; set; }

        public string f261_HostID { get; set; }
        public string f262_SessID { get; set; }
        public string f263_MRN { get; set; }
        public string f270_ORN { get; set; }

        public string f277_DDRN { get; set; }

        public string f283_UPP_PM { get; set; }
        public string f286_OrgDDRespCode { get; set; }

        public string f350_CrdTyp { get; set; }
        public string f352_AcqBank { get; set; }
        public string f353_IPPTenure { get; set; }
    }
}
