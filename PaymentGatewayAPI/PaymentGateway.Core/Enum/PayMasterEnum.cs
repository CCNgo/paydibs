﻿namespace PaymentGatewayAPI.Core.Enum
{
    public static class h001_MTI
    {
        public static string UPPPaymentAuthorizationRequest = "0280";
        public static string UPPPaymentAuthorizationResponse = "0290";

        public static string UPPPaymentQueryRequestMessage = "0780";
        public static string UPPPaymentQueryResponseMessage = "0780";

        public static string CCPaymentReversalRequestMessage = "0400";
        public static string CCPaymentReversalResponseMessage = "0410";

        public static string PaymentSalesCompletionRequestMessage = "0220";
        public static string PaymentSalesCompletionResponseMessage = "0230";

        public static string CCPaymentAuthorizationRequestMessage = "0200";
        public static string CCPaymentAuthorizationResponseMessage = "0210";
    }

    public enum Field248OriginalCurrencyCode
    {
        MalaysiaRinggit = 458,
        SingaporeDollar = 702,
        USDollar = 840
    }

    public enum Field010CurrencyCode
    {
        MalaysiaRinggit = 458,
        SingaporeDollar = 702,
        USDollar = 840
    }

    public enum Field325_ECommMercInd
    {
        NonECommerceMerchant = 0,
        ECommerce3DSecure = 1,
        ECommerceNon3DSecureMOTO = 2
    }

    public static class t001_SHT
    {
        public static string MD5 = "MD5";
        public static string SH2 = "SH2";
    }

    public static class f249_TxnCh
    {
        public static string WEB = "WEB";
        public static string API = "API";
        public static string DIR = "DIR";
        public static string GDS = "GDS";
        public static string OTH = "OTH";
    }

    public static class f339_TokenFlg
    {
        public static string YES = "Y";
        public static string NO = "N";
    }

    public static class f362_PreAuthFlg
    {
        public static string YES = "Y";
        public static string NO = "N";
    }

    public static class f009_RespCode
    {
        public static string Approved = "00";
        public static string Pending = "22";
        public static string Error = "06";
        public static string DoNotHonour = "05";
        public static string TransactionTimeout = "68";
        public static string InvalidMerchantID = "03";
    }
}
