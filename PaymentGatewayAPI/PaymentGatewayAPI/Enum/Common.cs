﻿
namespace PaymentGatewayAPI.Enum
{
    public class Common
    {
        public const string C_TXN_SUCCESS_0 = "Payment Successful";
        public const string C_TXN_FAILED_1 = "Payment Failed";
        public const string C_TXN_PENDING_2 = "Payment Pending";                    // Added, 22 Sept 2013
        public const string C_TXN_REVERSED_9 = "Payment Reversed";                  // Added, 22 Sept 2013
        public const string C_TXN_EXPIRED_6 = "Payment Expired";                    // Added, 22 Sept 2013
        public const string C_COM_TIMEOUT_5 = "Timeout Out Failed";
        public const string C_FAILED_HOTCARD_9 = "Hot Card / Hot Name";
        public const string C_PENDING_RESP_30 = "Payment Pending For Response";
        public const string C_QUERY_HOST_33 = "Querying Host";
        public const string C_FAILED_TO_SEND_46 = "Payment Failed To Send"; // "Out of Per Txn Amt Limit" '
        public const string C_EXCEED_PER_TXN_AMT_LIMIT_50 = "Out of Per Txn Amt Limit"; // "Txn Amt Limit Out Of Range (Min MYR 1.00 and Max MYR 30000.00)" '   'Added, 16 Jan 2014. Modified 6 Feb 2018, changed from "Exceeded" to "Out of"
        public const string C_TXN_NOT_FOUND_21 = "Payment Not Exists";
        public const string C_DB_UNKNOWN_ERROR_901 = "Failed to Connect DB";
        public const string C_INVALID_HOST_902 = "Failed to Get Host Information";
        public const string C_FAILED_SET_TXN_STATUS_905 = "Failed to Set Transaction Status";
        public const string C_FAILED_STORE_TXN_906 = "Failed to Store Transaction";     // Failed bInsertNewTxn
        public const string C_FAILED_UPDATE_TXN_RESP_907 = "Failed to Update Transaction Response";
        public const string C_FAILED_FORM_REQ_914 = "Failed to Form Message";
        public const string C_FAILED_COM_INIT_917 = "Failed to Initiate Communications";
        public const string C_INVALID_MID_2901 = "Invalid Merchant ID";                  // Failed bGetMerchant in bInitPayment
        public const string C_COMMON_INVALID_FIELD_2902 = "Invalid Field";
        public const string C_COMMON_MISSING_ELEMENT_2903 = "Missing Required Field";   // Failed bPaymentMain
        public const string C_INVALID_INPUT_2906 = "Invalid Input";                     // Failed bCheckUniqueTxn, bInitPayment
        public const string C_INVALID_HOST_REPLY_2907 = "Invalid Host Reply";
        public const string C_INVALID_SIGNATURE_2909 = "Invalid Hash Value";
        public const string C_LATE_HOST_REPLY_2908 = "Late Host Reply";
        public const string C_COMMON_UNKNOWN_ERROR_2999 = "Internal Error";             // Failed to GenTxnID, bInsertTxnResp, bCheckUniqueTxn, bInsertNewTxn, bProcessSale, bInitPayment
        public const string C_DEFAULT_LANGUAGE_CODE = "EN";                                     // Added, 26 Jul 2013
        public const string C_3DESAPPKEYENCP = "25F4C4263E05868016F3CECEBBF027DC3286B002016EF737D8F5362773A2D742";   // Added, 11 Oct 2017, card data key encrypted by KEK retrieved from DB-p
        public const string C_3DESAPPKEYENCS = "67D7DC68CA3334ABC0FA785990457C696A65C2DCF92A01001974979C2DDE1D9A";  // "3537146182309002FEB7EBCE55ED3BBA71BED7CEB1D18CAA"  DB-stg
        public const string C_3DESAPPKEY2 = "4648245271298991EDA6FCDF66FE4CCB60ADC6BDA0C09DBB"; // To encrypt/decrypt non card data
        public const string C_3DESAPPVECTOR = "0000000000000000";                               // Added, 19 Nov 2013
        public const string C_TXN_DECLINED_BY_FDS_400 = "Declined by ExtFDS Rules";                   // Added 18 apr 2016  fraudwall
        public const string C_TXN_DECLINED_BY_FDS_299 = "Declined by ExtFDS Fraud Score";   // Added 18 apr 2016  fraudwall
        public const string C_TXN_CANCELLED_17 = "Payment Cancelled At Payment Page";
        public const string C_TXN_DECLINED_BY_FDS = "Declined by FDS Rules";                // Added by OM, 2 May 2019. Block Foreign Card.
        public const string C_TXN_CC_ACQBANK = "Visa/MasterCard";                           // Added by OM, 7 May 2019. CC's AcqBank return value

        public enum TXN_STATE
        {
            INITIAL = 1,             // Received request for a particular txn for the 1st time from AA Web or other merchants

            SENT_TO_HOST = 2,        // Can be redirect through client's browser to Host,e.g.CIMB(popup) OR sent through WebComm to Host  'PENDING_PROCESS = 2

            DECLINE_FDS = 3,         // Added, 12 Oct 2013, declined by FDS

            SUCCESS = 4,
            FAILED = 5,              // Failed spInsTxnResp, bCheckUniqueTxn, bInsertNewTxn

            ABORT = 6,               // Failed GenTxnID, GetData, bPaymentMain

            ROLLBACK_PENDING = 7,    // Reversal timeout/pending

            ROLLBACK_SUCCESS = 8,    // Reversal success

            COMMIT_PENDING = 9,
            COMMIT = 10,
            ROLLBACK_FAILED = 11,    // Reversal failed

            COMMIT_FAILED = 12,
            RECV_ADD_SEATS = 13,     // Received add seats reply from OpenSkies/NewSkies   - Added on 16 Aug 2009, to cater for add seats

            SENT_2ND_ENTRY = 15,     // Replied an entry page to client, e.g. UserID entry(BCA); Tokens info entry(Mandiri)

            RECV_2ND_ENTRY = 16,     // e.g. Received UserID (BCA);Tokens info(Mandiri)                  '    'PROCESSING = 3

            RECV_FROM_HOST = 17,     // Received reply from Host

            SENT_QUERY_HOST = 18,    // Sent query request to Host

            RECV_QUERY_HOST = 19,    // Received query reply from Host

            SENT_QUERY_OS = 20,      // Sent query request to OpenSkies/NewSkies

           RECV_QUERY_OS = 21,      // Received query reply from OpenSkies/NewSkies

           SENT_CONFIRM_OS = 22,    // Sent Add/Confirm Payment request to OpenSkies/NewSkies

            RECV_CONFIRM_OS = 23,    // Received Add/Confirm Payment reply from OpenSkies/NewSkies

            RECV_HOST_QUERY = 24,    // Received Host's Query request, e.g. BCA

            SENT_HOST_QUERY = 25,    // Sent Host's Query reply, e.g. BCA

            SENT_REPLY_CLIENT = 26,  // Sent reply to client/merchant

            SENT_VOID_HOST = 27,    // Sent Reversal request to Host     - Added on 10 Apr 2010

            RECV_VOID_HOST = 28,    // Received Reversal reply from Host - Added on 10 Apr 2010

            SENT_REFUND_HOST = 29,   // Sent Refund request to Host            - Added  8 July 2015

            RECV_REFUND_HOST = 30,   // Received Refund reply from Host        - Added  8 July 2015

            INSTL_NOT_ENTITLE = 31  // Installment Failed due to not able to get routing based on card BIN range. Added  25 May 2016
        }

        public enum TXN_STATUS
        {
            TXN_SUCCESS = 0,               // 1
            TXN_FAILED = 1,                   // 4

            TXN_PENDING = 2,                  // Processing

            TXN_PENDING_CONFIRM_BOOKING = 3,    // Host returned approved for the payment, pending OpenSkies AddPymt reply OR trying to send AddPymt request to OpenSkies, need extend booking

            TXN_TIMEOUT_HOST_RETURN = 4,         // Host no response for 1 hour 45 minutes, need extend booking, stop and generate exception report

            TXN_TIMEOUT_HOST_UNPROCESSED = 5,    // Host's reply status is "not processed" for 1 hour 45 minutes, no need extend booking, no need exception report

        TIME_OUT_FAILED = 6,          // Txn existed in Request table for more than 2 hours

            TXN_HOST_UNPROCESSED = 7,        // Host's reply status is "not processed"

            TXN_HOST_RETURN_TIMEOUT = 8,     // Host no response/timeout

            TXN_REVERSED = 9,           // Txn reversed successfully

            TXN_NOT_FOUND = -1,            // Added, 3 Sept 2013

            TXN_INT_ERR = -2,            // Added, 3 Sept 2013

            TXN_EXPIRED_IN_PYMTPAGE = -3,     // Added  1 Nov 2017. Expired in Payment Page

            TXN_AUTH_SUCCESS = 15,        // Added  14 April 2015. Auth&Capture. Txn status Auth Success returned to merchant in Query resp

            TXN_CAPTURE_SUCCESS = 16,       // Added  14 April 2015. Auth&Capture. Txn status Capture Success returned to merchant in Query resp

            TXN_REFUND_SUCCESS = 10,       // Added, 31 May 2015. Refund. Txn status Refund Success returned to merchant in Query resp

            TXN_CANCELLED = 17,
            PENDING_RESP = 30,
            PENDING_REVERSAL = 31,
            PROCESSING_RESP = 32,
            // QUERYING_HOST = 33                 'Commented  1 Dec 2015
            PENDING_REFUND = 33,               // Added  8 Jul 2015

            PENDING_CAPTURE = 34,           // Added  5 Aug 2016

            FDS_BlOCKCARD = 41,                // Added by OM, 2 May 2019. Block Foreign Card. Txn STatus follow Blacklist Card Range

            EXCEED_PER_TXN_AMT_LIMIT = 50,     // Added, 16 Jan 2014

            DECLINED_BY_EXT_FDS = 51,       // Added, 12 Jul 2016, FraudWall

            DECLINED_BY_PROMOTION = 52,          // Added 07 Oct 2016, Promotion

            DECLINED_BY_VEENROLL = 53,
            DECLINED_BY_PA = 54,
            E_FAILED_STORE_TXN = 906,  // Failed bInsertNewTxn

            E_INVALID_MERCHANT = 2901,
            INVALID_INPUT = 2906,  // Failed GetData, bInitPayment, bPaymentMain

            INVALID_HOST_REPLY = 2907,
            LATE_HOST_REPLY = 2908,     // Late reply from Host

            INVALID_HOST_IP = 2909,
            INVALID_HOST_REPLY_GATEWAYTXNID = 2910,
            INVALID_HOST = 2911,
            INTERNAL_ERROR = 2999    // Failed bInitPayment, bProcessSale
        }
    }
}