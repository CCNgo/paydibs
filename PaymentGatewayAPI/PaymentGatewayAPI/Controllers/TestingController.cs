﻿using PaymentGatewayAPI.Core.Utilities;
using System;
using System.Text;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Web.Http;
using PaymentGatewayAPI.Core.Models;
using PaymentGatewayAPI.Core.Services;

namespace PaymentGatewayAPI.Controllers
{
    [RoutePrefix("Testing")]
    public class TestingController : ApiController
    {
        [HttpGet]
        [Route("TestRSA2048")]
        public void testingRSA2048()
        {

            string merchantID = "217120000000222662788";
            string clientID = "2020072312473800008049";
            string clientSecret = "2020072312473800008049avqsR8";
            string productCode = "51051000101000100001";

            string reqTime = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz");
            string merchantTransId = "20201708_01";


            string input = "{\"head\":{\"version\":\"1.0\",\"function\":\"alipayplus.acquiring.order.create\",\"clientId\":\"" + clientID + "\",\"clientSecret\":\"" + clientSecret + "\",\"reqTime\":\"2019-10-01T11:22:00+08:00\",\"reqMsgId\":\"10207635486872290001\"},\"body\":{\"order\":{\"orderTitle\":\"Testing Order123\",\"orderAmount\":{\"currency\":\"MYR\",\"value\":\"1\"},\"merchantTransId\":\"" + merchantTransId + "\",\"expiryTime\":\"2020-10-09T13:38:00+00:00\"},\"merchantId\":\"" + merchantID + "\",\"productCode\":\"" + productCode + "\",\"signAgreementPay\":false,\"envInfo\":{\"terminalType\":\"SYSTEM\",\"orderTerminalType\":\"WEB\"},\"notificationUrls\":[{\"url\":\"http://www.google.com\",\"type\":\"PAY_RETURN\"},{\"type\":\"NOTIFICATION\",\"url\":\"https://devToolCenter.tngdigital.com.my/1.0/orderNotify/receive\"}],\"extendInfo\":\"{\\\"PARTNER_TRANSACTION_ID\\\":\\\"10207635486872290000\\\"}\",\"subMerchantId\":\"TestingMerchantId\",\"subMerchantName\":\"KEDAISERVIS\"}}";

            //string input = "{\"head\":{\"version\":\"1.0\",\"function\":\"alipayplus.acquiring.order.create\",\"clientId\":\"2019062017370700001324\",\"clientSecret\":\"20190620173707000013246pcQqj\",\"reqTime\":\"2019-10-01T11:22:00+08:00\",\"reqMsgId\":\"10207635486872290000\"},\"body\":{\"order\":{\"orderTitle\":\"Testing Order123\",\"orderAmount\":{\"currency\":\"MYR\",\"value\":\"1\"},\"merchantTransId\":\"10207635486872290000\",\"expiryTime\":\"2020-10-09T13:38:00+00:00\"},\"merchantId\":\"" + merchantTransId + "\",\"productCode\":\"51051000101000100001\",\"signAgreementPay\":false,\"envInfo\":{\"terminalType\":\"SYSTEM\",\"orderTerminalType\":\"WEB\"},\"notificationUrls\":[{\"url\":\"http://www.google.com\",\"type\":\"PAY_RETURN\"},{\"type\":\"NOTIFICATION\",\"url\":\"https://devToolCenter.tngdigital.com.my/1.0/orderNotify/receive\"}],\"extendInfo\":\"{\\\"PARTNER_TRANSACTION_ID\\\":\\\"10207635486872290000\\\"}\",\"subMerchantId\":\"TestingMerchantId\",\"subMerchantName\":\"KEDAISERVIS\"}}";

            //string input = "{ \"head\": { \"version\": \"1.0\", \"function\": \"alipayplus.acquiring.order.create\", \"clientId\": \"2020072312473800008049\", \"clientSecret\": \"2020072312473800008049avqsR8\", \"reqTime\": \"2020-08-05T12:08:56+05:30\", \"reqMsgId\": \"1234567asdfasdf1123fda\", \"reserve\": \"{ \\\"attr1\\\":\\\"val1\\\" }\" }, \"body\": { \"order\": { \"seller\": { \"userId\": \"20160322334445555\", \"nickname\": \"mike\" }, \"orderTitle\": \"Women Summer Dress New White Lace Sleeveless Cute Casual Summer Dresses Vestidos roupas femininas WQW1045\", \"orderAmount\": { \"currency\": \"MYR\", \"value\": \"239\" }, \"merchantTransId\": \"201505080003\", \"merchantTransType\": \"PLN_PRA_BAYAR_TOKEN\", \"orderMemo\": \"Memo\", \"createdTime\": \"2020-08-05T12:08:56+05:30\", \"expiryTime\": \"2020-08-05T12:36:56+05:30\", \"goods\": [{ \"merchantGoodsId\": \"24525635625623\", \"description\": \"Women Summer Dress New White Lace Sleeveless Cute Casual Summer Dresses Vestidos roupas femininas WQW1045\", \"price\": { \"currency\": \"MYR\", \"value\": \"239\" }} ]}, \"merchantId\": \"217120000000222662788\", \"mcc\": \"5732\", \"productCode\": \"51051000101000100001\", \"envInfo\": { \"terminalType\": \"SYSTEM\", \"orderTerminalType\": \"APP\" }, \"notificationUrls\": [{ \"url\": \"http://notify.com\", \"type\": \"PAY_RETURN\" } ] } }";

            string privateKey = "MIIEowIBAAKCAQEAoo0yuVnrVe70P/YCCqaQgksjgANO3rdDorgFpUz28IMpP7NBZovbz0KICgtvfYNCxzCVojybqwfg+n2edOKE0w4x8ppajqlmHtimyOfR4aH0VJqtcBghTAw/CnGoWoIV7EaskAyY6TjJWqv4+6InHV9G9O3ix1oznVq8xU+XEbkc+gqkbAoEoZ6WkrXyE7O+GKzncFeNnf4E4ovGiQBIb3uw2mt81mDEOfhhPSBt8+F9EvzmM5mPEZydLX+WLqtio1ZkrUwNm2VIL3VGnlxUl6wruw8NWpCbiYxU/JKwqaaykph41+r+QBRiSaGSyc2VxYwLQtfyQzlH89kNWHefNQIDAQABAoIBAF9a7y+WFRyjAXtn6Aixb0JPq7KTvjeeUDnrM7ylapLqjk8E68+pKbfqsn0Z+jwpvQm2cYDrhvIMvCiVelfc7cgVoq1LaOYZb4OW2jgu7YssA3WCC04fiaDf6jdJN1Zoy02ApAgYq7bcjhn3miabF9D+LFTQG+GQA4lCSkyBBQDqpD0B9Eak+qXY5djqxqDk1V17bIedUSAVZ2x/LluGJe91Dx1dZgPtI8LUne/Mg3TBZiSxqCiMl6yYLL+0X7xHzSVNREzKd++uXnU5FAadFYol51Oqv4OCVR/VvFhjKiMR0XiaghXHikOFa0135+4gCNfyX3MHUkYKdMjWYhfCL+ECgYEAzyp1FEgIM/JKfffS28yha6rdzetD+nYAfytrrIN+ebQ28rAlYEu2a9R0ALTq4v67tXNsXcQ2PmtCULLPnn7xI3uQGklxFdGFFTsXrBIp3dkosDGQ0duPmMH1j8M3XPXA4avnvYvDNGFkWGIv+bAwUAETWZTFdZfBX+cTwxbvP0kCgYEAyN53K4xBrarFLBwtlS83aP5k7/Yr70dnP5W4c2RfXWZ5f0hcosnxjPSFNE7LHoC+tTln13RYA3IaBLlhN4VGERGVoVkHkiBwYDX96FNBptU8wySH6cbeD5jKmFsWT56JUGwWZrb1nR2AuNOV62PcZGDd4twSuzo3W0lFoHXvpI0CgYEAuayB9sWApJ456aDeWaWT353q4g5EhQyR8WiHPr0z8/xzaxw4w4/YVdi6h4+fHgfVURG8RGYBf/w+GXnJ33JCydyLESRVuIpqztaBpvU4lN93OkOSWqSAlRbx8gGzcxvH3ftm3lmPgTjw6M5H1W/2PR5Zq2ZNe9GhvUSsT6QUlhECgYA2hp0WxyX0/Iw1YxjEjGKgL546vTpD48AVNJEzUaXJds24TW5ICkV1m9Kk1law2wXpr8GObNiPZuPFMIVBSv4wF/PkLoStl/ypQT7BWAhUP0IVPs9eGEujNDiaju9JgaGIoS6bh0K4+Y89nMHvedYTsW/J6y9yf2UTdJpf6mO46QKBgEql1uxOwUOKflny4gdFXy6+Nsh1W96XlsBH7mkuhCemS82rHw0/P40oLGhry5VzeO7z2np0U5j3Y7blThGBFbrwHi3lqNe/ynElJP6a5zkqqYd24gCfUQw8GC92R7P6CZdj9RN416Y+oSDZejEiPENEFQ2W/jplxp85D8a+rBHK";

            ASCIIEncoding ByteConverter = new ASCIIEncoding();
            byte[] inputBytes = ByteConverter.GetBytes(input);

            byte[] inputHash = new SHA256CryptoServiceProvider().ComputeHash(inputBytes);

            byte[] privateKeyBytes = Convert.FromBase64String(privateKey
                .Replace("-----BEGIN RSA PRIVATE KEY-----", string.Empty)
                .Replace("-----END RSA PRIVATE KEY-----", string.Empty)
                .Replace("\n", string.Empty));

            RSACryptoServiceProvider rsa = RSAUtils.DecodeRSAPrivateKey(privateKeyBytes);
            byte[] output = rsa.SignHash(inputHash, "SHA256");

            Console.WriteLine(Convert.ToBase64String(output));
        }

        static readonly byte[] SeqOid = { 0x30, 0x0D, 0x06, 0x09, 0x2A, 0x86, 0x48, 0x86, 0xF7, 0x0D, 0x01, 0x01, 0x01, 0x05, 0x00 };

        public static void ExtractPublicKeyParameters(byte[] publicKey, out byte[] modulus, out byte[] exponent)
        {
            modulus = new byte[0];
            exponent = new byte[0];

            byte[] seq = new byte[15];

            // ---------  Set up stream to read the asn.1 encoded SubjectPublicKeyInfo blob  ------
            MemoryStream mem = new MemoryStream(publicKey);
            BinaryReader binr = new BinaryReader(mem);    //wrap Memory Stream with BinaryReader for easy reading
            byte bt = 0;
            ushort twobytes = 0;

            try
            {

                twobytes = binr.ReadUInt16();
                if (twobytes == 0x8130) //data read as little endian order (actual data order for Sequence is 30 81)
                    binr.ReadByte();    //advance 1 byte
                else if (twobytes == 0x8230)
                    binr.ReadInt16();   //advance 2 bytes
                else
                    return;

                seq = binr.ReadBytes(15);       //read the Sequence OID
                if (!CompareBytearrays(seq, SeqOid))    //make sure Sequence for OID is correct
                    return;

                twobytes = binr.ReadUInt16();
                if (twobytes == 0x8103) //data read as little endian order (actual data order for Bit String is 03 81)
                    binr.ReadByte();    //advance 1 byte
                else if (twobytes == 0x8203)
                    binr.ReadInt16();   //advance 2 bytes
                else
                    return;

                bt = binr.ReadByte();
                if (bt != 0x00)     //expect null byte next
                    return;

                twobytes = binr.ReadUInt16();
                if (twobytes == 0x8130) //data read as little endian order (actual data order for Sequence is 30 81)
                    binr.ReadByte();    //advance 1 byte
                else if (twobytes == 0x8230)
                    binr.ReadInt16();   //advance 2 bytes
                else
                    return;

                twobytes = binr.ReadUInt16();
                byte lowbyte = 0x00;
                byte highbyte = 0x00;

                if (twobytes == 0x8102) //data read as little endian order (actual data order for Integer is 02 81)
                    lowbyte = binr.ReadByte();  // read next bytes which is bytes in modulus
                else if (twobytes == 0x8202)
                {
                    highbyte = binr.ReadByte(); //advance 2 bytes
                    lowbyte = binr.ReadByte();
                }
                else
                    return;
                byte[] modint = { lowbyte, highbyte, 0x00, 0x00 };   //reverse byte order since asn.1 key uses big endian order
                int modsize = BitConverter.ToInt32(modint, 0);

                int firstbyte = binr.PeekChar();
                if (firstbyte == 0x00)
                {   //if first byte (highest order) of modulus is zero, don't include it
                    binr.ReadByte();    //skip this null byte
                    modsize -= 1;   //reduce modulus buffer size by 1
                }

                modulus = binr.ReadBytes(modsize);   //read the modulus bytes

                if (binr.ReadByte() != 0x02)            //expect an Integer for the exponent data
                    return;
                int expbytes = (int)binr.ReadByte();        // should only need one byte for actual exponent data (for all useful values)
                exponent = binr.ReadBytes(expbytes);
            }

            finally
            {
                binr.Close();
            }
        }

        private static bool CompareBytearrays(byte[] a, byte[] b)
        {
            if (a.Length != b.Length)
                return false;
            int i = 0;
            foreach (byte c in a)
            {
                if (c != b[i])
                    return false;
                i++;
            }
            return true;
        }

        //[HttpGet]
        //[Route("TestAES256")]
        //public void testAES256()
        //{

        //    // The sample encryption key. Must be 32 characters.
        //    string Key = "8UHjPgXZzXCGkhxV2QCnooyJexUzvJrO";

        //    // The sample text to encrypt and decrypt.
        //    string Text = "Here is some text to encrypt!";

        //    // Encrypt and decrypt the sample text via the Aes256CbcEncrypter class.
        //    string Encrypted = Aes256CbcEncrypter.Encrypt(Text, Key);
        //    string Decrypted = Aes256CbcEncrypter.Decrypt(Encrypted, Key);

        //    // Show the encrypted and decrypted data and the key used.
        //    Console.WriteLine("Original: {0}", Text);
        //    Console.WriteLine("Key: {0}", Key);
        //    Console.WriteLine("Encrypted: {0}", Encrypted);
        //    Console.WriteLine("Decrypted: {0}", Decrypted);
        //}

        //private static string getString(byte[] b)
        //{
        //    return Encoding.UTF8.GetString(b);
        //}

        //public static byte[] Encrypt(byte[] data, byte[] key)
        //{
        //    using (AesCryptoServiceProvider csp = new AesCryptoServiceProvider())
        //    {
        //        csp.KeySize = 256;
        //        csp.BlockSize = 128;
        //        csp.Key = key;
        //        csp.Padding = PaddingMode.PKCS7;
        //        csp.Mode = CipherMode.ECB;
        //        ICryptoTransform encrypter = csp.CreateEncryptor();
        //        return encrypter.TransformFinalBlock(data, 0, data.Length);
        //    }
        //}

        //private static byte[] Decrypt(byte[] data, byte[] key)
        //{
        //    using (AesCryptoServiceProvider csp = new AesCryptoServiceProvider())
        //    {
        //        csp.KeySize = 256;
        //        csp.BlockSize = 128;
        //        csp.Key = key;
        //        csp.Padding = PaddingMode.PKCS7;
        //        csp.Mode = CipherMode.ECB;
        //        ICryptoTransform decrypter = csp.CreateDecryptor();
        //        return decrypter.TransformFinalBlock(data, 0, data.Length);
        //    }
        //}

        [HttpGet]
        [Route("ProcessTestAES256")]
        public void ProcessTestAES256()
        {
            // The sample encryption key. Must be 32 characters.
            string Key = "kdezwN7afZNC8EXHH8Q2pdu4RjuOuAXV"; //store in tblconfig use own encrypt aes256

            var purchasePayMasterService = new PurchasePayMasterService();
            List <ProcessCardPAN> ProcessCardPAN = purchasePayMasterService.GetCardPAN();

            foreach (var p in ProcessCardPAN)
            {
                string Encrypted = Aes256CbcEncrypter.Encrypt(p.DecryptedCardPAN, Key);
                string Decrypted = Aes256CbcEncrypter.Decrypt(Encrypted, Key);

                purchasePayMasterService.UpdateProcessCardPAN(Encrypted, Key, p.PKID);
            }

            //// Show the encrypted and decrypted data and the key used.
            //Console.WriteLine("Original: {0}", Text);
            //Console.WriteLine("Key: {0}", Key);
            //Console.WriteLine("Encrypted: {0}", Encrypted);
            //Console.WriteLine("Decrypted: {0}", Decrypted);
        }


        [HttpGet]
        [Route("TestAES256")]
        public void testAES256()
        {
            // The sample encryption key. Must be 32 characters.
            string Key = "kdezwN7afZNC8EXHH8Q2pdu4RjuOuAXV"; //store in tblconfig use own encrypt aes256

            // The sample text to encrypt and decrypt.
            string Text = "4966230077888877";

            // Encrypt and decrypt the sample text via the Aes256CbcEncrypter class.

            string Encrypted = Aes256CbcEncrypter.Encrypt(Text, Key);
            string Decrypted = Aes256CbcEncrypter.Decrypt(Encrypted, Key);

            // Show the encrypted and decrypted data and the key used.
            Console.WriteLine("Original: {0}", Text);
            Console.WriteLine("Key: {0}", Key);
            Console.WriteLine("Encrypted: {0}", Encrypted);
            Console.WriteLine("Decrypted: {0}", Decrypted);
        }

        private static string getString(byte[] b)
        {
            return Encoding.UTF8.GetString(b);
        }

        //public static byte[] Encrypt(byte[] data, byte[] key)
        //{
        //    using (AesCryptoServiceProvider csp = new AesCryptoServiceProvider())
        //    {
        //        csp.KeySize = 256;
        //        csp.BlockSize = 128;
        //        csp.Key = key;
        //        csp.Padding = PaddingMode.PKCS7;
        //        csp.Mode = CipherMode.ECB;
        //        ICryptoTransform encrypter = csp.CreateEncryptor();
        //        return encrypter.TransformFinalBlock(data, 0, data.Length);
        //    }
        //}

        //private static byte[] Decrypt(byte[] data, byte[] key)
        //{
        //    using (AesCryptoServiceProvider csp = new AesCryptoServiceProvider())
        //    {
        //        csp.KeySize = 256;
        //        csp.BlockSize = 128;
        //        csp.Key = key;
        //        csp.Padding = PaddingMode.PKCS7;
        //        csp.Mode = CipherMode.ECB;
        //        ICryptoTransform decrypter = csp.CreateDecryptor();
        //        return decrypter.TransformFinalBlock(data, 0, data.Length);
        //    }
        //}
    }
}
