﻿using LoggerII;
using PaymentGatewayAPI.Core;
using PaymentGatewayAPI.Core.Models;
using PaymentGatewayAPI.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PaymentGatewayAPI.Core.Enum;
using System.Threading;
using System.IO;
using System.Net.Http.Headers;
using PaymentGatewayAPI.Core.Utilities;
using System.Globalization;
using Newtonsoft.Json;
using System.Configuration;
using System.Web;

namespace PaymentGatewayAPI.Controllers
{
    [RoutePrefix("Order")]
    public class OrderController : ApiController
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        [HttpGet]
        [Route("Sales/PayMasterSales")]
        public HttpResponseMessage PayMasterSales([FromUri] SalesOrderRequest request)
        {
            try
            {
                objLoggerII = logger.InitLogger();
                logMsg(CLoggerII.LOG_SEVERITY.INFORMATION,
                    "[PayMasterSales Request][" + request.onlineRefNum + "][Request][" + Newtonsoft.Json.JsonConvert.SerializeObject(request) + "]");

                var purchasePayMasterService = new PurchasePayMasterService();

                ReqTxn reqTxn = new ReqTxn();
                reqTxn = purchasePayMasterService.GetReqTxn(request.onlineRefNum);
                request.merchantId = reqTxn.MerchantID;
                request.custEmail = reqTxn.CustEmail;
                request.custName = reqTxn.CustName;
                request.custPhone = reqTxn.CustPhone;

                var result = purchasePayMasterService.CommitPurchase(request);

                var response = Request.CreateResponse(HttpStatusCode.MovedPermanently);
                response.Headers.Location = new Uri(result.RedirectURL);

                return response;
            }
            catch (Exception ex)
            {
                if (request != null) //to avoid spam
                { 
                    logMsg(CLoggerII.LOG_SEVERITY.CRITICAL,
                    "[PayMasterSales][Exception][" + Newtonsoft.Json.JsonConvert.SerializeObject(request) + ex.ToString() + "]");
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Bad Request");
            }
        }

        [HttpGet]
        [Route("Sales/PayMasterResp")]
        public HttpResponseMessage PayMasterResp([FromUri] PayMasterResponse response)
        {
            string ISSUING_BANK = "GOBIZ";

            if (response.h001_MTI == h001_MTI.CCPaymentAuthorizationResponseMessage)
            {
                ISSUING_BANK = "GOBIZPCI";
            }

            const string TXN_TYPE = "PAY";
            
            objLoggerII = logger.InitLogger();
            logMsg(CLoggerII.LOG_SEVERITY.INFORMATION,
               "[Response PayMasterResp][" + ISSUING_BANK + "][" + response.f263_MRN + "][GetResponse][" + Newtonsoft.Json.JsonConvert.SerializeObject(response) + "]");

            var purchasePayMasterService = new PurchasePayMasterService();

            if (response != null)
            {
                Host host = purchasePayMasterService.GetHost(ISSUING_BANK);

                if (host == null)
                {
                    logMsg(CLoggerII.LOG_SEVERITY.INFORMATION,
                       "[Response PayMasterResp][" + ISSUING_BANK + "][" + response.f263_MRN + "][DB Host is Empty]" + Newtonsoft.Json.JsonConvert.SerializeObject(response));

                    return Request.CreateResponse(HttpStatusCode.BadRequest, response.f263_MRN + " Bad Request");
                }

                //Check Request table for stPayinfo's MerchantID, etc
                ReqTxn reqTxn = purchasePayMasterService.GetReqTxn(response.f263_MRN);
                logMsg(CLoggerII.LOG_SEVERITY.INFORMATION,
                    "[Response PayMasterResp][" + ISSUING_BANK + "][Check Response Transaction from Paydibs DB]" + Newtonsoft.Json.JsonConvert.SerializeObject(reqTxn) + "]");

                ResTxn resTxn = null;
                //Check reqTxn is null, if null then check resTxn
                if (reqTxn == null)
                {
                    if (response.f263_MRN != null)
                    {
                        //GetResTxn compare Gobiz CardPAN and VGS Outbound value
                        resTxn = purchasePayMasterService.GetResTxn(response.f263_MRN);
                        logMsg(CLoggerII.LOG_SEVERITY.INFORMATION,
                        "[Response PayMasterResp][" + ISSUING_BANK + "][" + response.f263_MRN + "][Get Response Transaction from DB][" + Newtonsoft.Json.JsonConvert.SerializeObject(resTxn) + "]");

                        resTxn.GatewayTxnID = response.f263_MRN;

                        string htmlContent = purchasePayMasterService.MerchantPayResp(host, resTxn, reqTxn);

                        var webResponse = new HttpResponseMessage();
                        webResponse.Content = new StringContent(htmlContent);
                        webResponse.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");

                        return webResponse;
                    }
                }
                else
                {
                    reqTxn.GatewayTxnID = response.f263_MRN;
                }

                Common.TXN_STATUS txnStatus;
                string transactionStatus = string.Empty;

                if (reqTxn != null)
                {
                    transactionStatus = reqTxn.TxnStatus;
                }
                else if (resTxn != null)
                {
                    transactionStatus = resTxn.TxnStatus.ToString();
                }
                System.Enum.TryParse(transactionStatus, out txnStatus);

                if (txnStatus != Common.TXN_STATUS.TXN_NOT_FOUND &
                    txnStatus != Common.TXN_STATUS.LATE_HOST_REPLY &
                    txnStatus != Common.TXN_STATUS.INVALID_HOST_IP &
                    txnStatus != Common.TXN_STATUS.INVALID_HOST_REPLY_GATEWAYTXNID &
                    txnStatus != Common.TXN_STATUS.INVALID_HOST_REPLY)
                {
                    logMsg(CLoggerII.LOG_SEVERITY.INFORMATION,
                     "[Response PayMasterResp][" + ISSUING_BANK + "][" + response.f263_MRN + "][Inserting TxnResp][MerchantReturnURL: " + reqTxn.MerchantReturnURL + "]" +
                     "[GatewayTxnID: " + reqTxn.MerchantTxnID + "] MerchantTxnStatus[" + reqTxn.RetCode.ToString() + "]" +
                     " TxnStatus[" + reqTxn.TxnStatus.ToString() + "] TxnState[" + reqTxn.TxnState.ToString() + "]" +
                     " RespMesg[" + reqTxn.RetDesc + "]." + "]");

                    reqTxn.TxnStatus = (Convert.ToInt32(Common.TXN_STATUS.TXN_PENDING)).ToString();
                    reqTxn.TxnState = (Convert.ToInt32(Common.TXN_STATE.COMMIT_PENDING)).ToString();
                    reqTxn.TxnMsg = Common.C_TXN_PENDING_2;

                    //Logic on Handling Resp Code from GoBiz
                    if (!string.IsNullOrEmpty(reqTxn.MerchantTxnID) & !string.IsNullOrEmpty(reqTxn.TxnAmount)
                        & !string.IsNullOrEmpty(reqTxn.TxnStatus) & !string.IsNullOrEmpty(reqTxn.RetCode) & !string.IsNullOrEmpty(reqTxn.OrderNumber))
                    {
                        Core.UPPPaymentQueryRequestMessage0780_TestURL.Query0790Resp query0790Resp = purchasePayMasterService.RequeryPurchase(response, reqTxn);

                        if (query0790Resp != null || !String.IsNullOrEmpty(query0790Resp.f009_RespCode))
                        {
                            if (query0790Resp.f009_RespCode == f009_RespCode.InvalidMerchantID) //RespCode=03 [Invalid Merchant ID]
                            {
                                Mail objMail = new Mail();
                                int iMailStatus = -1;
                                try
                                {
                                    iMailStatus = objMail.SendMail(ConfigurationManager.AppSettings["InternalEmail"], "Invalid Merchant ID: " + query0790Resp.f009_RespCode, "Paydibs Internal Email");
                                    logMsg(CLoggerII.LOG_SEVERITY.INFORMATION,
                                    "[Response PayMasterResp][" + ISSUING_BANK + "][" + response.f263_MRN + "][Invalid MerchantID Email] Send Invalid Merchant ID error for internal email notification");
                                }
                                catch (Exception ex)
                                {
                                    logMsg(CLoggerII.LOG_SEVERITY.CRITICAL,
                                    "[Response PayMasterResp][" + ISSUING_BANK + "][" + response.f263_MRN + "][Invalid Send Email] SendEmailNotification Exception: " + ex.ToString());
                                }
                            }

                            reqTxn.TxnStatus = (Convert.ToInt32(Common.TXN_STATUS.TXN_FAILED)).ToString();
                            reqTxn.TxnState = (Convert.ToInt32(Common.TXN_STATE.COMMIT_FAILED)).ToString();
                            reqTxn.TxnMsg = Common.C_TXN_FAILED_1;

                            logMsg(CLoggerII.LOG_SEVERITY.INFORMATION,
                             "[Response PayMasterResp][" + ISSUING_BANK + "][" + response.f263_MRN + "][Query status f009_RespCode status is][" + query0790Resp.f009_RespCode + "][" + reqTxn.MerchantTxnID +
                                "] TxnStatus[" + reqTxn.TxnStatus.ToString() +
                                "] TxnState[" + reqTxn.TxnState.ToString() + "] RespMesg[" + reqTxn.RetDesc + "]");

                        }
                        else
                        {
                            reqTxn.TxnStatus = (Convert.ToInt32(Common.TXN_STATUS.TXN_FAILED)).ToString();
                            reqTxn.TxnState = (Convert.ToInt32(Common.TXN_STATE.COMMIT_FAILED)).ToString();
                            reqTxn.TxnMsg = Common.C_TXN_FAILED_1;

                            if (query0790Resp == null)
                            {
                                logMsg(CLoggerII.LOG_SEVERITY.INFORMATION,
                            "[Response PayMasterResp][" + ISSUING_BANK + "][" + response.f263_MRN + "][Response query0790Resp is Empty][" + reqTxn.MerchantTxnID +
                               "] TxnStatus[" + reqTxn.TxnStatus.ToString() +
                               "] TxnState[" + reqTxn.TxnState.ToString() + "] RespMesg[" + reqTxn.RetDesc + "]");
                            }
                            else
                            { 
                                logMsg(CLoggerII.LOG_SEVERITY.INFORMATION,
                               "[Response PayMasterResp][" + ISSUING_BANK + "][" + response.f263_MRN + "][Query status f009_RespCode status is Empty][f009_RespCode: " + query0790Resp.f009_RespCode + "][" + reqTxn.MerchantTxnID +
                                  "] TxnStatus[" + reqTxn.TxnStatus.ToString() +
                                  "] TxnState[" + reqTxn.TxnState.ToString() + "] RespMesg[" + reqTxn.RetDesc + "]");
                            }
                        }

                        if (query0790Resp.f009_RespCode == f009_RespCode.Pending || query0790Resp.f009_RespCode == f009_RespCode.TransactionTimeout)
                        {
                            logMsg(CLoggerII.LOG_SEVERITY.INFORMATION,
                            "[Response PayMasterResp][" + ISSUING_BANK + "][" + response.f263_MRN + "][Query status f009_RespCode status is Transaction in pending stage(in progress)][f009_RespCode: " + query0790Resp.f009_RespCode + "][" + reqTxn.MerchantTxnID +
                            "] TxnStatus[" + reqTxn.TxnStatus.ToString() +
                            "] TxnState[" + reqTxn.TxnState.ToString() + "] RespMesg[" + reqTxn.RetDesc + "]");
                        }

                        if (query0790Resp.f009_RespCode == f009_RespCode.Approved)
                        {
                            reqTxn.TxnStatus = (Convert.ToInt32(Common.TXN_STATUS.TXN_SUCCESS)).ToString();
                            reqTxn.TxnState = (Convert.ToInt32(Common.TXN_STATE.COMMIT)).ToString();
                            reqTxn.TxnMsg = Common.C_TXN_SUCCESS_0;

                            logMsg(CLoggerII.LOG_SEVERITY.INFORMATION,
                            "[Response PayMasterResp][" + ISSUING_BANK + "][" + response.f263_MRN + "][query status f009_RespCode approved][f009_RespCode: " + query0790Resp.f009_RespCode + "][" + reqTxn.MerchantTxnID +
                            "] TxnStatus[" + reqTxn.TxnStatus.ToString() +
                            "] TxnState[" + reqTxn.TxnState.ToString() + "] RespMesg[" + reqTxn.RetDesc + "]");
                        }

                        //Create New Transaction if request and response data are correct
                        if (4 != purchasePayMasterService.InsertTxnResp(reqTxn, query0790Resp, TXN_TYPE))
                        {
                            logMsg(CLoggerII.LOG_SEVERITY.INFORMATION,
                            "[Response PayMasterResp][" + ISSUING_BANK + "][" + response.f263_MRN + "][InsertTxnResp][Error: InsertTxnResp Failed]");

                            reqTxn.TxnStatus = (Convert.ToInt32(Common.TXN_STATUS.TXN_FAILED)).ToString();
                            reqTxn.TxnState = (Convert.ToInt32(Common.TXN_STATE.COMMIT_FAILED)).ToString();
                            reqTxn.TxnMsg = Common.C_TXN_FAILED_1;

                            if (-1 != purchasePayMasterService.UpdatePayTxnRef(reqTxn, query0790Resp, TXN_TYPE))
                            {
                                logMsg(CLoggerII.LOG_SEVERITY.INFORMATION,
                                "[Response PayMasterResp][" + ISSUING_BANK + "][" + response.f263_MRN + "][UpdatePayTxnRef][f009_RespCode: Update Txn Failed");
                            }
                        }

                        resTxn = purchasePayMasterService.GetResTxn(response.f263_MRN);
                        if (resTxn != null)
                        {
                            Merchant merchant = purchasePayMasterService.GetMerchant(resTxn.MerchantID);
                            if (query0790Resp.f009_RespCode == f009_RespCode.Approved)
                            {
                                SendEmailNotification(reqTxn, resTxn, host, merchant, Common.EmailTemplate.EMAILTEMPLATESHOPPER);
                                SendEmailNotification(reqTxn, resTxn, host, merchant, Common.EmailTemplate.EMAILTEMPLATEMERCHANT);
                            }

                            //2.2.1 PAY Response example for MerchantRURL with Form Post format
                            string htmlContent = purchasePayMasterService.MerchantPayResp(host, resTxn, reqTxn);

                            //2.2.2 PAY Response example for MerchantCallbackURL with string
                            if (!String.IsNullOrEmpty(resTxn.MerchantCallbackURL))
                            {
                                logMsg(CLoggerII.LOG_SEVERITY.INFORMATION,
                               "[Response PayMasterResp][" + ISSUING_BANK + "][" + response.f263_MRN + "][Posting to Merchant CallbackURL][" + resTxn.MerchantCallbackURL + "]");

                                MerchantCallback merchantCallback = purchasePayMasterService.RetrieveMerchantParameters(host, resTxn, reqTxn);
                                string httpString = purchasePayMasterService.GetReplyMerchantCallbackHTTPString(merchantCallback);
                                int callBackStatus = 0;
                                for (int t = 0; t <= 4; t++) //Callback 5 times if failed get response from callback url
                                {
                                    string result = HttpHelper.HttpPostReadWithData(resTxn.MerchantCallbackURL, httpString, MIMEType.URL_ENCODED, "TLS12");
                 
                                    if (result != null)
                                    {
                                        callBackStatus = 1;
                                        
                                        t = 4;
                                        logMsg(CLoggerII.LOG_SEVERITY.INFORMATION,
                                        "[Response PayMasterResp][" + ISSUING_BANK + "][" + response.f263_MRN + "][Callback response]["
                                        + Newtonsoft.Json.JsonConvert.SerializeObject(merchantCallback) + "], Status [success and recv acknowledgement (" + result + ")" + "  ....)]" +
                                        "Protocol[" + merchant.Protocol + "]");
                                    }
                                    else
                                    {
                                        callBackStatus = 0;
                                        logMsg(CLoggerII.LOG_SEVERITY.INFORMATION,
                                        "[Response PayMasterResp][" + ISSUING_BANK + "][" + response.f263_MRN + "][Callback response]["
                                        + Newtonsoft.Json.JsonConvert.SerializeObject(merchantCallback) + "], Status [fail][Running: " + t.ToString() +
                                        "Protocol[" + merchant.Protocol + "]");
                                        Thread.Sleep(50);

                                    }

                                }
                                purchasePayMasterService.InsertCallBackStatus(reqTxn, callBackStatus);

                            }

                            var webResponse = new HttpResponseMessage();
                            webResponse.Content = new StringContent(htmlContent);
                            webResponse.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
                            return webResponse;
                        }
                    }
                    else
                    {
                        logMsg(CLoggerII.LOG_SEVERITY.INFORMATION,
                            "[Response PayMasterResp][" + ISSUING_BANK + "][" + response.f263_MRN + "][GatewayTxnID(" + reqTxn.MerchantTxnID + ") / TxnAmount(" + reqTxn.TxnAmount + ") / HostTxnStatus(" + reqTxn.TxnStatus
                            + ")/RespCode(" + reqTxn.RetDesc + ")/OrderNumber(" + reqTxn.OrderNumber + ") are empty from response. Bypass InsertTxnResp.");
                    }
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Bad Request");
            }
            return Request.CreateResponse(HttpStatusCode.OK, "Refer on Logging File");

        }

        [HttpGet]
        [Route("Sales/PayMasterReversal")]
        public HttpResponseMessage PayMasterReversal([FromUri] SalesOrderRequest request)
        {
            objLoggerII = logger.InitLogger();

            logMsg(CLoggerII.LOG_SEVERITY.INFORMATION,
                "[PayMasterReversal][" + request.onlineRefNum + "][Request][" + Newtonsoft.Json.JsonConvert.SerializeObject(request) + "]");

            var purchasePayMasterService = new PurchasePayMasterService();

            ResTxn resTxn = new ResTxn();
            resTxn = purchasePayMasterService.GetResTxn(request.onlineRefNum);
            request.txnamount = resTxn.TxnAmount;

            logMsg(CLoggerII.LOG_SEVERITY.INFORMATION,
              "[PayMasterReversal][" + request.onlineRefNum + "][resTxn][" + Newtonsoft.Json.JsonConvert.SerializeObject(resTxn) + "]");

            var result = purchasePayMasterService.ReversalPurchase(request);

            if (result.Result.Code == "00")
            {
                return Request.CreateResponse(HttpStatusCode.OK, "Refer on Logging File");
            }

            return Request.CreateResponse(HttpStatusCode.BadRequest, "Bad Request");
        }

        private int SendEmailNotification(ReqTxn reqTxn, ResTxn resTxn, Host host, Merchant merchant, Common.EmailTemplate emailTemplate)
        {
            int iMailStatus = -1;
            string szHTMLContent = "";
            string[] szReceiptTemplate = null;

            string szReceiptEmailBody = string.Empty;

            Mail objMail = new Mail();
            string szTemplatePath = objMail.GetTemplate(emailTemplate);
            szReceiptTemplate = szTemplatePath.Split('.');
            szTemplatePath = szReceiptTemplate[0] + "_" + Common.C_DEFAULT_LANGUAGE_CODE + "." + szReceiptTemplate[1];

            string htmlContent = string.Empty;

            if (szTemplatePath != string.Empty)
            {
                htmlContent = GetTemplate(szReceiptTemplate[0] + "_" + Common.C_DEFAULT_LANGUAGE_CODE + "." + szReceiptTemplate[1], szHTMLContent);
                if (string.IsNullOrEmpty(htmlContent))
                {
                    return iMailStatus;
                }
            }

            htmlContent = objMail.LoadHtmlContent(reqTxn, resTxn, host, merchant, htmlContent);

            try
            {
                string recipientEmail = string.Empty;
                if (emailTemplate == Common.EmailTemplate.EMAILTEMPLATESHOPPER)
                {
                    iMailStatus = objMail.SendMail(resTxn.CustEmail, htmlContent, "Payment Confirmation to [" + merchant.MerchantName + "]");
                }
                else if (emailTemplate == Common.EmailTemplate.EMAILTEMPLATEMERCHANT)
                {
                    iMailStatus = objMail.SendMail(merchant.NotifyEmailAddr, htmlContent, "Payment Confirmation to [" + resTxn.MerchantTxnID + "]");
                }

                return iMailStatus;
            }
            catch (Exception ex)
            {
                logMsg(CLoggerII.LOG_SEVERITY.INFORMATION,
                    "[SendEmailNotification Exception: " + ex.ToString() + "]");

                return iMailStatus;
            }
        }

        private string LoadCCTemplate(string szMerchantReturnURL, string szHTTPString, string sz_HTML, SalesOrderRequest request)
        {
            var purchasePayMasterService = new PurchasePayMasterService();
            
            string szTemplatePath = "";
            string szHTMLContent = "";

            szTemplatePath = ConfigurationManager.AppSettings["CCTemplate"];

            if (szTemplatePath != "")
            {
                szHTMLContent = GetTemplate(szTemplatePath, szHTMLContent);
                if (szHTMLContent != "")
                {
                    szHTMLContent = purchasePayMasterService.szFormatPlaceHolder(szHTMLContent, "[MERCHANTID]", request.merchantId);
                    szHTMLContent = purchasePayMasterService.szFormatPlaceHolder(szHTMLContent, "[MERCHANTNAME]", request.merchantinfo.merchantName);
                    szHTMLContent = purchasePayMasterService.szFormatPlaceHolder(szHTMLContent, "[MERCHANTADDR]", request.merchantinfo.merchantAddr);
                    szHTMLContent = purchasePayMasterService.szFormatPlaceHolder(szHTMLContent, "[MERCHANTCONTACTNO]", request.merchantinfo.merchantContactNo);
                    szHTMLContent = purchasePayMasterService.szFormatPlaceHolder(szHTMLContent, "[MERCHANTEMAILADDR]", request.merchantinfo.merchantEmailAddr);
                    szHTMLContent = purchasePayMasterService.szFormatPlaceHolder(szHTMLContent, "[MERCHANTWEBSITEURL]", request.merchantinfo.merchantWebsiteURL);

                    szHTMLContent = purchasePayMasterService.szFormatPlaceHolder(szHTMLContent, "[CUSTNAME]", request.custName);
                    szHTMLContent = purchasePayMasterService.szFormatPlaceHolder(szHTMLContent, "[CUSTCONTACTNO]", request.custPhone);
                    szHTMLContent = purchasePayMasterService.szFormatPlaceHolder(szHTMLContent, "[CUSTEMAIL]", request.custEmail);

                    szHTMLContent = purchasePayMasterService.szFormatPlaceHolder(szHTMLContent, "[MERCHANTTXNID]", request.merchantinfo.merchantTxnID);
                    szHTMLContent = purchasePayMasterService.szFormatPlaceHolder(szHTMLContent, "[ORDERNUMBER]", request.ordernumber);
                    szHTMLContent = purchasePayMasterService.szFormatPlaceHolder(szHTMLContent, "[ORDERDESC]", request.orderTitle);
                    szHTMLContent = purchasePayMasterService.szFormatPlaceHolder(szHTMLContent, "[CURRENCYCODE]", request.currency);
                    szHTMLContent = purchasePayMasterService.szFormatPlaceHolder(szHTMLContent, "[TXNAMOUNT]", request.txnamount);

                    szHTMLContent = purchasePayMasterService.szFormatPlaceHolder(szHTMLContent, "[PAYMENTURL]", "PayMasterSalesPCI");
                    szHTMLContent = purchasePayMasterService.szFormatPlaceHolder(szHTMLContent, "[HIDDENCARDNO]", string.Empty);
                    szHTMLContent = purchasePayMasterService.szFormatPlaceHolder(szHTMLContent, "[CANCELURL]", request.gatewaycancelurl);
                    szHTMLContent = purchasePayMasterService.szFormatPlaceHolder(szHTMLContent, "[TIMEOUT]", request.timeout);

                    sz_HTML = szHTMLContent;
                }
            }
            return sz_HTML;
        }

        //private int LoadReceiptUI(string szMerchantReturnURL, string szHTTPString, string sz_HTML, object st_PayInfo, object st_HostInfo)
        //{
        //    var purchasePayMasterService = new PurchasePayMasterService();

        //    int iReturn = 0;
        //    string szTemplatePath = "";
        //    string szHTMLContent = "";

        //    szTemplatePath = ConfigurationManager.AppSettings["RedirectTemplate"];


        //    if (szTemplatePath != "")
        //    {
        //        GetTemplate(szTemplatePath, szHTMLContent);
        //        if (szHTMLContent != "")
        //        {
        //            szHTMLContent = purchasePayMasterService.szFormatPlaceHolder(szHTMLContent, "[HOSTPARAMS]", szHTTPString);
        //            szHTMLContent = purchasePayMasterService.szFormatPlaceHolder(szHTMLContent, "[HOSTURL]", szMerchantReturnURL); //szMerchantReturnURL sz_URL

        //            sz_HTML = szHTMLContent;
        //        }
        //        else
        //        {
        //            iReturn = -1;
        //        }
        //    }
        //    else
        //    {
        //        iReturn = -1;
        //    }

        //    return iReturn;
        //}

        private string GetTemplate(string sz_FilePath, string szHTMLContent)
        {
            StreamReader objStreamReader = null;
            string szTemplateFile = "";
           
            try
            {
                szTemplateFile = sz_FilePath;
                objStreamReader = System.IO.File.OpenText(szTemplateFile);
                szHTMLContent = objStreamReader.ReadToEnd();
                objStreamReader.Close();
            }
            catch (Exception ex)
            {
                logMsg(CLoggerII.LOG_SEVERITY.INFORMATION,
                 "[Response GetTemplate() exception: " + ex.ToString() + "]");

                return szHTMLContent;
            }
            //finally
            //{
            //    if (objStreamReader != null)
            //        objStreamReader = null;
            //}
            return szHTMLContent;
        }


        [HttpGet]
        [Route("Sales/GetCustCCInfo")]
        public HttpResponseMessage GetCustCCInfo([FromUri] SalesOrderRequest request)
        {
            try
            {
                objLoggerII = logger.InitLogger();
                logMsg(CLoggerII.LOG_SEVERITY.INFORMATION,
                    "[PayMasterSales PCI GetCustCCInfo Requesting][" + request.onlineRefNum + "][Request][" + Newtonsoft.Json.JsonConvert.SerializeObject(request) + "]");

                var purchasePayMasterPCIService = new PurchasePayMasterPCIService();

                ReqTxn reqTxn = new ReqTxn();
                reqTxn = purchasePayMasterPCIService.GetReqTxn(request.onlineRefNum);
                Merchant merchant = purchasePayMasterPCIService.GetMerchant(reqTxn.MerchantID);

                request.merchantId = reqTxn.MerchantID;
                request.custEmail = reqTxn.CustEmail;
                request.custName = reqTxn.CustName;
                request.custPhone = reqTxn.CustPhone;
                request.currency = reqTxn.CurrencyCode;
                request.txnamount = reqTxn.TxnAmount;

                //reqTxn.Pagetimeout = request.pagetimeout;

                string gatewaycancelURL = string.Empty;

                gatewaycancelURL = gatewaycancelURL + "TxnType=CANCEL&";
                gatewaycancelURL = gatewaycancelURL + "MerchantID=" + request.merchantId + "&";
                gatewaycancelURL = gatewaycancelURL + "MerchantPymtID=" + HttpUtility.UrlEncode(reqTxn.MerchantTxnID) + "&";
                gatewaycancelURL = gatewaycancelURL + "MerchantOrdID=" + HttpUtility.UrlEncode(reqTxn.OrderNumber) + "&";
                gatewaycancelURL = gatewaycancelURL + "MerchantOrdDesc=" + reqTxn.OrderDesc + "&";
                gatewaycancelURL = gatewaycancelURL + "MerchantName=" + merchant.MerchantName + "&";
                gatewaycancelURL = gatewaycancelURL + "MerchantRURL=" + reqTxn.MerchantReturnURL + "&";
                gatewaycancelURL = gatewaycancelURL + "MerchantTxnAmt=" + request.txnamount + "&";
                gatewaycancelURL = gatewaycancelURL + "MerchantCurrCode=" + HttpUtility.UrlEncode(request.currency) + "&";
                gatewaycancelURL = gatewaycancelURL + "Param1=" + HttpUtility.UrlEncode(reqTxn.Param1) + "&";
                gatewaycancelURL = gatewaycancelURL + "Param2=" + HttpUtility.UrlEncode(reqTxn.Param2) + "&";
                gatewaycancelURL = gatewaycancelURL + "Param3=" + HttpUtility.UrlEncode(reqTxn.Param3) + "&";
                gatewaycancelURL = gatewaycancelURL + "Param4=" + HttpUtility.UrlEncode(reqTxn.Param4) + "&";
                gatewaycancelURL = gatewaycancelURL + "Param5=" + HttpUtility.UrlEncode(reqTxn.Param5) + "&";
                gatewaycancelURL = gatewaycancelURL + "CustName=" + HttpUtility.UrlEncode(reqTxn.CustName) + "&";
                gatewaycancelURL = gatewaycancelURL + "CustPhone=" + HttpUtility.UrlEncode(reqTxn.CustPhone) + "&";
                gatewaycancelURL = gatewaycancelURL + "CustEmail=" + HttpUtility.UrlEncode(reqTxn.CustEmail) + "&";
                gatewaycancelURL = gatewaycancelURL + "BillAddr=" + HttpUtility.UrlEncode(merchant.CollectBillAddr) + "&";
                gatewaycancelURL = gatewaycancelURL + "BillCountry=" + HttpUtility.UrlEncode(merchant.Country) + "&";
                //gatewaycancelURL = gatewaycancelURL + "ShipAddr=" + HttpUtility.UrlEncode(merchant.Addr1) + "&";
                //gatewaycancelURL = gatewaycancelURL + "ShipCity=" + Server.UrlEncode(merchant.City) + "&";
                //gatewaycancelURL = gatewaycancelURL + "ShipRegion=" + Server.UrlEncode(request.currency) + "&";
                //gatewaycancelURL = gatewaycancelURL + "ShipPostal=" + stPayInfo.szTxnID + "&";
                //gatewaycancelURL = gatewaycancelURL + "ShipCountry=" + stPayInfo.iMerchantTxnStatus.ToString() + "&";
                //gatewaycancelURL = gatewaycancelURL + "PageTimeout=" + stPayInfo.iMerchantTxnStatus.ToString() + "&";
                gatewaycancelURL = gatewaycancelURL + "LanguageCode=EN" + "&";
                gatewaycancelURL = gatewaycancelURL + "MerchantCallBackURL=" + HttpUtility.UrlEncode(reqTxn.MerchantCallbackURL) + "&";
                gatewaycancelURL = gatewaycancelURL + "MerchantSupportURL=" + HttpUtility.UrlEncode(reqTxn.MerchantSupportURL) + "&";

                Hash hash = new Hash();
                string sign = hash.GetSaleResHash2Cancel(reqTxn, merchant);

                gatewaycancelURL = gatewaycancelURL + "Sign=" + sign  + "&";
                gatewaycancelURL = gatewaycancelURL + "DefaultTxnType=PAY";

                request.gatewaycancelurl = ConfigurationManager.AppSettings["GWPaymentURL"] + "?" + gatewaycancelURL;

                merchantinfo merchantinfo = new merchantinfo()
                { 
                    merchantTxnID = reqTxn.MerchantTxnID,
                    merchantName = merchant.MerchantName,
                    merchantAddr = merchant.Addr1 + ",&nbsp;" + merchant.Addr2 + ",&nbsp;" + merchant.Addr3,
                    merchantWebsiteURL = merchant.WebSiteURL,
                    merchantEmailAddr = merchant.EmailAddr,
                    merchantContactNo = merchant.ContactNo
                };

                request.merchantinfo = merchantinfo;
                
                string szHTMLContent = LoadCCTemplate("", "", "", request);

                var webResponse = new HttpResponseMessage();
                webResponse.Content = new StringContent(szHTMLContent);
                webResponse.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
                return webResponse;
            }
            catch (Exception ex)
            {
                if (request != null) //to avoid spam
                {
                    logMsg(CLoggerII.LOG_SEVERITY.CRITICAL,
                    "[PayMasterSales][Exception][" + Newtonsoft.Json.JsonConvert.SerializeObject(request) + ex.ToString() + "]");
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Bad Request");
            }
        }


        [HttpPost]
        [Route("Sales/PayMasterSalesPCI")]
        public HttpResponseMessage PayMasterSalesPCI([FromUri] SalesOrderRequest request, CCInfo ccInfo)
        {
            try
            {
               objLoggerII = logger.InitLogger();
                logMsg(CLoggerII.LOG_SEVERITY.INFORMATION,
                    "[PayMasterSales PCI Request][" + ccInfo.MerchantTxnId + "][Request][" + Newtonsoft.Json.JsonConvert.SerializeObject(ccInfo) + "]");

                var purchasePayMasterPCIService = new PurchasePayMasterPCIService();

                ReqTxn reqTxn = new ReqTxn();
                reqTxn = purchasePayMasterPCIService.GetReqTxnByMerchantTxnID(ccInfo.MerchantTxnId);
                request = new SalesOrderRequest();
                request.merchantId = reqTxn.MerchantID;
                request.custEmail = reqTxn.CustEmail;
                request.custName = reqTxn.CustName;
                request.custPhone = reqTxn.CustPhone;
                request.onlineRefNum = reqTxn.GatewayTxnID;
                request.currency = reqTxn.CurrencyCode;
                request.txnamount = reqTxn.TxnAmount;
                request.amount = decimal.Parse(reqTxn.TxnAmount);
                request.orderTitle = reqTxn.OrderDesc;
                request.ordernumber = reqTxn.OrderNumber;
      

                //UI Paydibs Handling Redirect

                var result = purchasePayMasterPCIService.CommitPurchase(request, ccInfo);

                var response = Request.CreateResponse(HttpStatusCode.MovedPermanently);
                response.Headers.Location = new Uri(result.RedirectURL);

                return response;
            }
            catch (Exception ex)
            {
                if (request != null) //to avoid spam
                {
                    logMsg(CLoggerII.LOG_SEVERITY.CRITICAL,
                    "[PayMasterSales][Exception][" + Newtonsoft.Json.JsonConvert.SerializeObject(request) + ex.ToString() + "]");
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Bad Request");
            }
        }


        [HttpPost]
        [Route("Sales/TNGSales")]
        public HttpResponseMessage TNGSales(SalesOrderRequest request)
        {
            var purchaseTNGService = new PurchaseTNGService();
            var result = purchaseTNGService.CommitPurchase(request);
            Logger logger = new Logger();
            CLoggerII objLoggerII = new CLoggerII();
            objLoggerII = logger.InitLogger();
            
            if (result.RedirectURL == null)
            {
                var webResponse = new HttpResponseMessage();
                webResponse.Content = new StringContent(JsonConvert.SerializeObject(request));
                webResponse.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
              
                return webResponse;
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [HttpPost]
        [Route("Sales/orderFinish")]
        public HttpResponseMessage TnGOrderFinish([FromBody] TnGOrderFinishRequest request)
        {
            Logger logger = new Logger();
            CLoggerII objLoggerII = new CLoggerII();
            objLoggerII = logger.InitLogger();
            var purchaseTNGService = new PurchaseTNGService();
            var result = purchaseTNGService.CommitOrderFinish(request);

            //if (result.QueryResult.Content != null)
            //{
            //    return result.QueryResult;
            //}

            return Request.CreateResponse<TnGOrderFinishResponse>(HttpStatusCode.OK, result.OFResponse);
            //return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [HttpGet]
        [Route("Sales/orderFinishFinal")]
        public HttpResponseMessage TnGOrderFinishFinal([FromUri] TNGPayReturn PayReturn)
        {
            Logger logger = new Logger();
            CLoggerII objLoggerII = new CLoggerII();
            objLoggerII = logger.InitLogger();
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                   "TnGOrderFinishFinal input: " + PayReturn);
            var purchaseTNGService = new PurchaseTNGService();
            return purchaseTNGService.CommitOrderQuery(PayReturn);
            //return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [HttpGet]
        [Route("Sales/orderFinishFinalAuto")]
        public HttpResponseMessage TnGOrderFinishFinalAuto([FromUri] TNGPayReturn PayReturn)
        {
            Logger logger = new Logger();
            CLoggerII objLoggerII = new CLoggerII();
            objLoggerII = logger.InitLogger();
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                   "TnGOrderFinishFinal input: " + PayReturn);
            var purchaseTNGService = new PurchaseTNGService();
            return purchaseTNGService.CommitOrderQueryScheduler(PayReturn);
            //test
            //return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [HttpGet]
        [Route("Sales/orderChecking")]
        public HttpResponseMessage TnGOrderChecking([FromUri] TNGPayReturn PayReturn)
        {         
            var purchaseTNGService = new PurchaseTNGService();
            return purchaseTNGService.CommitOrderQueryInstant(PayReturn);
            //return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [HttpPost]
        [Route("Sales/orderRefund")]
        public HttpResponseMessage TnGOrderRefund([FromBody] TnGOrderRefundRequest RefundRequest)
        {
            Logger logger = new Logger();
            CLoggerII objLoggerII = new CLoggerII();
            objLoggerII = logger.InitLogger();

            var purchaseTNGService = new PurchaseTNGService();
            return purchaseTNGService.CommitOrderRefund(RefundRequest);
            //return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        private void logMsg(CLoggerII.LOG_SEVERITY LOG_SEVERITY, string msg)
        {
            objLoggerII.Log(LOG_SEVERITY, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(), msg);
        }

        //[HttpGet]
        //[Route("Sales/PayMasterSalesPCI")]
        //public HttpResponseMessage PayMasterSalesPCI([FromUri] SalesOrderRequest request)
        //{
            
        //}

    }

}

//Timeout
//int iStartTickCount;
//int iElapsedTime;
//int iDBTimeOut = 0;
//iElapsedTime = 0;
//iStartTickCount = System.Environment.TickCount;
//iDBTimeOut = Convert.ToInt32(10);

//string retCode = string.Empty;

//while ((iElapsedTime < iDBTimeOut * 1000))
//{
//    //get restxn
//    ResTxn resTxn = purchasePayMasterService.GetResTxn(response.f263_MRN); //PDBS0000000002020092300214"
//    if (resTxn !=null)
//    { 
//        retCode = resTxn.RetCode;
//        if ("-1" == retCode)
//        {
//            break;
//        }
//        else
//        {
//            Thread.Sleep(3000);
//        }

//        iElapsedTime = System.Environment.TickCount - iStartTickCount;
//    }
//}

//if (("-1" != retCode))
//{
//    bTimeOut = true;
//}

//private bool ResponseMain(ReqTxn reqTxn)
//{
//    var purchasePayMasterService = new PurchasePayMasterService();
//    purchasePayMasterService.GetMerchant(reqTxn.MerchantID);

//    return true;
//    //objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, 
//    //DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(), C_ISSUING_BANK + " Response: " + szGetAllHTTPVal());
//    //if ((Request.QueryString("order") == null & Request.QueryString("pid") == null))
//    //    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "> Error1_2 bResponseMain(): Empty Response from Host's reply.");

//    //return true;
//}


//public HttpResponseMessage Post(SalesOrderResponse result)
//{
//    var response = Request.CreateResponse(HttpStatusCode.MovedPermanently);
//    response.Headers.Location = new Uri(result.RedirectURL);

//    return response;
//}
//[HttpPost]
//[Route("Sales/PayMasterSales2")]
//public HttpResponseMessage PayMasterSales2(SalesOrderRequest request)
//{
//    var purchasePayMasterService = new PurchasePayMasterService();
//    var result = purchasePayMasterService.CommitPurchase(request);

//    return Request.CreateResponse(HttpStatusCode.OK, result);

//    //var response = Request.CreateResponse(HttpStatusCode.MovedPermanently);
//    //response.Headers.Location = new Uri(result.RedirectURL);

//    //return response;
//    //return Request.CreateResponse(HttpStatusCode.MovedPermanently, result);
//}


//[HttpGet]
//[Route("Sales/PayMasterSalesResponse")]
//public HttpResponseMessage PayMasterSales(CallbackResponse response)
//{

//}

//var result = purchasePayMasterService.CommitPurchase(request);

//return Request.CreateResponse(HttpStatusCode.OK, result);
//return Request.CreateResponse(HttpStatusCode.MovedPermanently, result);

