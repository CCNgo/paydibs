﻿<%@ Page Title="Revision History" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Revision.aspx.cs" Inherits="PaydibsAPIv2._2.Revision" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2><br />
    <p>This section details the changes that were made to this document.</p>
    <table class="table table-bordered">
        <thead style="background-color:gold; font-weight:bold">
            <tr>
                <td>#</td>
                <td>Version</td>
                <td>Date</td>
                <td>Description</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>1.</td>
                <td>1</td>
                <td>19 Apr 2018</td>
                <td>API Creation</td>
            </tr>
            <tr>
                <td>2.</td>
                <td>2</td>
                <td>1 Aug 2018</td>
                <td>Added Credit Card API</td>
            </tr>
            <tr>
                <td>3.</td>
                <td>2.1</td>
                <td>28 Aug 2018</td>
                <td>Added CC’s Test card</td>
            </tr>
            <tr>
                <td>4.</td>
                <td>2.2</td>
                <td>2 Nov 2018</td>
                <td>Added One Click Payment (OCP) Features Added URL</td>
            </tr>
            <tr>
                <td>5.</td>
                <td>2.3</td>
                <td>4 June 2020</td>
                <td>Added eWallet Option in Payment Method</td>
            </tr>
            <tr>
                <td>6.</td>
                <td>2.4</td>
                <td>10 June 2020</td>
                <td>Update Support Currency Code</td>
            </tr>
            <tr>
                <td>7.</td>
                <td>2.5</td>
                <td>23 Feb 2021</td>
                <td>Removing Features for CardHolder, CardNo, CardExp, CardCVV2 at PAY Request</td>
            </tr>
        </tbody>
    </table>
</asp:Content>
