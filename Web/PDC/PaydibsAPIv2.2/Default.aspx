﻿<%@ Page Title="Introduction" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="PaydibsAPIv2._2._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <h2><%: Title %></h2><br />
    <p>Paydibs provides a web interface that allows integration with Merchant System which would like to accept online payment by Online Banking or Credit Card.</p>
    <p>This Merchant Integration API provides merchants with the necessary technical information to integrate their applications (Merchant Systems) with Paydibs.</p>
    <p>This manual contains message format required between Paydibs and Merchant System for various payment transaction types. It is intended as a technical guide for merchant developers and system integrators who are responsible for designing or programming the respective online applications to integrate with Paydibs.</p>
    <br />
    <h4 class="sub-title">1.1 Pre-requisite</h4>
    <p>A test account is needed for merchant integration. Upon account generated, Merchant ID, Merchant Password and payment URL will be provided.</p>
    <br />
    <h4 class="sub-title">1.2 List of Abbreviation</h4>
    <div class="table-responsive">
        <table class="table table-bordered">
            <thead style="background-color:gold; font-weight:bold">
                <tr>
                    <td>No.</td>
                    <td>Abbreviation</td>
                    <td>Description</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1. </td>
                    <td>A</td>
                    <td>Alpha</td>
                </tr>
                <tr>
                    <td>2.</td>
                    <td>AN</td>
                    <td>Alphanumeric</td>
                </tr>
                    <tr>
                    <td>3.</td>
                    <td>ANS</td>
                    <td>Alphanumeric + Special Character</td>
                </tr>
                    <tr>
                    <td>4.</td>
                    <td>N</td>
                    <td>Numeric</td>
                </tr>
            </tbody>
        </table>
    </div>
    <br />
    <h4 class="sub-title">1.3 URL</h4>
    <p>Test URL for send in Pay request fields : https://dev.paydibs.com/PPGSG/PymtCheckout.aspx</p>
    <p style="margin-bottom:30px;">Production URL will be provided via email upon live payment account created.</p>

</asp:Content>
