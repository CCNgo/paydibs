﻿<%@ Page Title="API Reference" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PayResponse.aspx.cs" Inherits="PaydibsAPIv2.PayResponse" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <br />
    <h4 class="sub-title">2.2 PAY Response</h4>
    <p>Upon payment process completion, the following fields will be returned from Paydibs to Merchant’s MerchantRURL in order to complete an end-to-end payment process.</p>
    <div class="table-responsive">
        <table class="table table-bordered">
            <thead style="background-color:gold; font-weight:bold">
                <tr>
                    <td>No.</td>
                    <td>Field</td>
                    <td>Data Type</td>
                    <td>Max Length</td>
                    <td>Description</td>
                </tr>
            </thead>
            <tbody>
                <tr style="background-color:grey;color:white; text-align:center"><td colspan="5"><b>Mandatory Field</b></td></tr>
                <tr>
                    <td>1. </td>
                    <td>TxnType</td>
                    <td>A</td>
                    <td>7</td>
                    <td>As request received from merchant system</td>
                </tr>
                <tr>
                    <td>2.</td>
                    <td>Method</td>
                    <td>A</td>
                    <td>3</td>
                    <td>Payment Method<br /><b>OB</b> - Online Banking<br /><b>CC</b> - Credit Card<br /><b>WA</b> - eWallet</td>
                </tr>
                    <tr>
                    <td>3.</td>
                    <td>MerchantID</td>
                    <td>AN</td>
                    <td>3</td>
                    <td>As request received from merchant system</td>
                </tr>
                <tr>
                    <td>4.</td>
                    <td>MerchantPymtID</td>
                    <td>AN</td>
                    <td>20</td>
                    <td>As request received from merchant system</td>
                </tr>
                <tr>
                    <td>5.</td>
                    <td>MerchantOrdID</td>
                    <td>AN</td>
                    <td>20</td>
                    <td>Order’s descriptions</td>
                </tr>
                <tr>
                    <td>6.</td>
                    <td>MerchantTxnAmt</td>
                    <td>N</td>
                    <td>12(2)</td>
                    <td>As request received from merchant system</td>
                </tr>
                <tr>
                    <td>7.</td>
                    <td>MerchantCurrCode</td>
                    <td>A</td>
                    <td>3</td>
                    <td>As request received from merchant system</td>
                </tr>
                <tr>
                    <td>8.</td>
                    <td>PTxnID</td>
                    <td>AN</td>
                    <td>30</td>
                    <td>Unique payment transaction ID assigned by Paydibs for this transaction</td>
                </tr>
                <tr>
                    <td>9.</td>
                    <td>PTxnStatus</td>
                    <td>A</td>
                    <td>4</td>
                    <td>Indicate payment transaction status <br />Re: Section <a runat="server" href="~/Views/Appendices">3.2 Payment Status</a></td>
                </tr>
                <tr>
                    <td>10.</td>
                    <td>AcqBank</td>
                    <td>AN</td>
                    <td>30</td>
                    <td>
                        Bank/Acquirer name that processed the transaction
                    </td>
                </tr>
                <tr>
                    <td>11.</td>
                    <td>Sign</td>
                    <td>AN</td>
                    <td>128</td>
                    <td>
                        <p>Checksum calculated by Paydibs in <b>hexadecimal</b> string using <b>SHA512</b> hash algorithm</p>
                        <p>Re: Section <a runat="server" href="~/Views/Sign">2.7.2.1 PAY/QUERY/Void Response Sign</a></p>
                    </td>
                </tr>
                <tr style="background-color:grey;color:white; text-align:center"><td colspan="5"><b>Conditional Field</b></td></tr>
                <tr > 
                    <td>1.</td>
                    <td>TokenType</td>
                    <td>A</td>
                    <td>3</td>
                    <td>
                        As request received from merchant system for merchant which enabled One Click Payment (OCP) feature. 
                    </td>
                </tr>
                 <tr > 
                    <td>2.</td>
                    <td>Token</td>
                    <td>ANS</td>
                    <td>50</td>
                    <td>
                        Token value of One Click Payment (OCP) which merchant system is required to store for next payment request.
                    </td>
                </tr>
                <tr style="background-color:grey;color:white; text-align:center"><td colspan="5"><b>Optional Field</b></td></tr>
                <tr > 
                    <td>1.</td>
                    <td>PTxnMsg</td>
                    <td>AN</td>
                    <td>255</td>
                    <td>Payment transaction message that explains the PTxnStatus</td>
                </tr>
                 <tr > 
                    <td>2.</td>
                    <td>AuthCode</td>
                    <td>AN</td>
                    <td>12</td>
                    <td>Authorization Code returned by bank if any</td>
                </tr>
                <tr>
                    <td>3.</td>
                    <td>BankRefNo</td>
                    <td>AN</td>
                    <td>20</td>
                    <td>Bank transaction reference code returned by bank if any</td>
                </tr>
            </tbody>
        </table>
    </div>
    <br />
    <h4 class="sub-title">2.2.1 PAY Response example for MerchantRURL with Form Post format</h4>
    <pre>
        <code>
            &lt;INPUT type='hidden' name='TxnType' value='PAY'&gt;
            &lt;INPUT type='hidden' name='Method' value='OB'&gt;
            &lt;INPUT type='hidden' name='MerchantID' value='OM'&gt;
            &lt;INPUT type='hidden' name='MerchantPymtID' value='TestSG20180430009'&gt;
            &lt;INPUT type='hidden' name='MerchantOrdID' value='OMTest'&gt;
            &lt;INPUT type='hidden' name='MerchantTxnAmt' value='10.00'&gt;
            &lt;INPUT type='hidden' name='MerchantCurrCode' value='MYR'&gt;
            &lt;INPUT type='hidden' name='Sign' value='ab067822489ed11002f606bed57f8e604a7b2701071a8287969adb18e89c1292c69b8db0
                58914512090f12861a83ee268a04b3ed0d3203a812dade1e36f08b9e'&gt;
            &lt;INPUT type='hidden' name='PTxnID value='OM0000TestSG20180430009'&gt;
            &lt;INPUT type='hidden' name='AcqBank' value='FPXD'&gt;
            &lt;INPUT type='hidden' name='PTxnStatus' value='0'&gt;
            &lt;INPUT type='hidden' name='BankRefNo' value='1804301738070889'&gt;
            &lt;INPUT type='hidden' name='RespTime' value='2018-04-30 17:37:37'&gt;
            &lt;INPUT type='hidden' name='PTxnMsg' value='SBI BANK A|Nur@/() .-_,&'`~*;:Nik|Transaction Successful|00'&gt;
        </code>
   </pre>
    <br />
    <h4 class="sub-title">2.2.2 PAY Response example for MerchantCallbackURL with string</h4>
    <pre>
        <code>
            <p style="margin:0 20px;">{"TxnType":"PAY","Method":"OB","MerchantID":"ABC","MerchantPymtID":"TestSG20180430009","MerchantOrdID":"OMTest","MerchantTxnAmt":"10.00","MerchantCurrCode":"MYR","PTxnID":"ABC0000TestSG20180430009","PTxnStatus":"0","PTxnMsg":"SBI%20BANK%20A%7CNur%40%2F()%20.-_%2C%26\u0027%60~*%3B%3ANik%7CTransaction%20Successful%7C00","AcqBank":"FPXD","BankRefNo":"1804301738070889","Sign":"ab067822489ed11002f606bed57f8e604a7b2701071a8287969adb18e89c1292c69b8db058914512090f12861a83ee268a04b3ed0d3203a812dade1e36f08b9e"}</p>
        </code>
    </pre>
</asp:Content>
