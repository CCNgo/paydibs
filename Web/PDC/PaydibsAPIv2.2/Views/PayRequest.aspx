﻿

<%@ Page Title="API Reference" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PayRequest.aspx.cs" Inherits="PaydibsAPIv2.PayRequest" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <br />
    <h4 class="sub-title">2.1 PAY Request</h4>
    <p>The following fields are expected from Merchant to Paydibs in order to perform an online payment transaction:</p>
    <br />
    <p><i><b>Note:</b> Method can be HTTP GET or POST</i></p>
    <div class="table-responsive">
        <table class="table table-bordered">
            <thead style="background-color:gold; font-weight:bold">
                <tr>
                    <td>No.</td>
                    <td>Field</td>
                    <td>Data Type</td>
                    <td>Max Length</td>
                    <td>Description</td>
                </tr>
            </thead>
            <tbody>
                <tr style="background-color:grey;color:white; text-align:center"><td colspan="5"><b>Mandatory Field</b></td></tr>
                <tr>
                    <td>1. </td>
                    <td>TxnType</td>
                    <td>A</td>
                    <td>7</td>
                    <td><b>PAY</b> – Payment request type <br />Note: Uppercase is required</td>
                </tr>
                <tr>
                    <td>2.</td>
                    <td>MerchantID</td>
                    <td>AN</td>
                    <td>6</td>
                    <td>Merchant ID given by Paydibs.</td>
                </tr>
                    <tr>
                    <td>3.</td>
                    <td>MerchantPymtID</td>
                    <td>AN</td>
                    <td>20</td>
                    <td><b>Unique</b> transaction/payment ID assigned by merchant for this payment transaction. Duplicate ID is not allowed.</td>
                </tr>
                <tr>
                    <td>4.</td>
                    <td>MerchantOrdID</td>
                    <td>AN</td>
                    <td>20</td>
                    <td><p>Reference number / Invoice number for this order</p>
                        <p>MerchantOrdID can be same for different MerchantPymtID this indicates that multiple payment attempts are made for a particular order.</p>
                        <p>MerchantOrdID can be same as MerchantPymtID if MerchantOrdID is not applicable.</p>
                    </td>
                </tr>
                <tr>
                    <td>5.</td>
                    <td>MerchantOrdDesc</td>
                    <td>AN</td>
                    <td>100</td>
                    <td>Order’s descriptions <br /> Note: Not support Mandarin wording
                    </td>
                </tr>
                <tr>
                    <td>6.</td>
                    <td>MerchantTxnAmt</td>
                    <td>N</td>
                    <td>12(2)</td>
                    <td><p>Payment amount in 2 decimal places regardless whether the currency has decimal places or not.</p>
                        <p>Exclude “,” sign. e.g. 1000.00 <br />Invalid format: 1,000.00 or 1000</p>
                    </td>
                </tr>
                <tr>
                    <td>7.</td>
                    <td>MerchantCurrCode</td>
                    <td>A</td>
                    <td>3</td>
                    <td><p>Currency code with 3-letter of ISO4217 standard <br />Re: Section <a runat="server" href="~/Views/Appendices">3.4 Currency Code </a></p></td>
                </tr>
                <tr>
                    <td>8.</td>
                    <td>MerchantRURL</td>
                    <td>AN</td>
                    <td>255</td>
                    <td><p>Merchant system’s <b>browser redirect</b> URL which receives payment response from Paydibs when transaction iscompleted.</p>
                        <p>Note: Replace “&” with “;” if any. <br />e.g:<br /> https://merchantdomain/index.php?field1=value1&field2=value2
                            <br /><b>to</b><br />https://merchantdomain/index.php?field1=value1;field2=value2</p>
                    </td>
                </tr>
                <tr>
                    <td>9.</td>
                    <td>CustIP</td>
                    <td>AN</td>
                    <td>20</td>
                    <td><p>Customer’s IP address captured by merchant system</p></td>
                </tr>
                <tr>
                    <td>10.</td>
                    <td>CustName</td>
                    <td>AN</td>
                    <td>50</td>
                    <td>Customer Name</td>
                </tr>
                <tr>
                    <td>11.</td>
                    <td>CustEmail</td>
                    <td>AN</td>
                    <td>60</td>
                    <td>Customer’s Email Address</td>
                </tr>
                <tr>
                    <td>12.</td>
                    <td>CustPhone</td>
                    <td>AN</td>
                    <td>25</td>
                    <td>Customer’s Contact Number</td>
                </tr>
                <tr>
                    <td>13.</td>
                    <td>Sign</td>
                    <td>AN</td>
                    <td>128</td>
                    <td><p>Checksum calculated by Merchant System in hexadecimal string using SHA512 hash algorithm</p>
                        <p>Re: Section <a runat="server" href="~/Views/Sign">2.7.1.1 PAY Request Sign</a></p>
                    </td>
                </tr>
                <tr style="background-color:grey;color:white; text-align:center"><td colspan="5"><b>Conditional Field</b></td></tr>
                <tr > 
                    <td>1.</td>
                    <td>Method</td>
                    <td>A</td>
                    <td>3</td>
                    <td><p>Payment Method<br /><b>OB</b> - Online Banking
                        <br /><b>CC</b> - Credit Card
                        </p>
                    </td>
                </tr>
                 <%--<tr > 
                    <td>2.</td>
                    <td>CardHolder</td>
                    <td>AN</td>
                    <td>30</td>
                    <td>
                        <p>Card holder’s name </p>
                        <p>For merchant who had PCI compliance and collect Card details from merchant system.</p>
                        <p>Payment Method must be ‘CC’. Paydibs Payment Page will prompt if value not provided</p>
                    </td>
                </tr>
                 <tr > 
                    <td>3.</td>
                    <td>CardNo</td>
                    <td>N</td>
                    <td>19</td>
                    <td>
                        <p>Credit card number </p>
                        <p>For merchant who had PCI compliance and collect Card details from merchant system.</p>
                        <p>Payment Method must be ‘CC’. Paydibs Payment Page will prompt if value not provided.</p>
                    </td>
                </tr>
                 <tr > 
                    <td>4.</td>
                    <td>CardExp</td>
                    <td>N</td>
                    <td>16</td>
                    <td>
                        <p>Credit card expiry date</p>
                        <p>Date format : YYYYMM <br />Eg. 201807 (Year 2018, Month July)</p>
                        <p>For merchant who had PCI compliance and collect Card details from merchant system.</p>
                        <p>Payment Method must be ‘CC’. Paydibs Payment Page will prompt if value not provided.</p>
                    </td>
                </tr>
                 <tr > 
                    <td>5.</td>
                    <td>CardCVV2</td>
                    <td>N</td>
                    <td>4</td>
                    <td>
                        <p>Card verification value at the back of credit card.</p>
                        <p>MasterCard/Visa is 3 digits Amex is 4 digits</p>
                        <p>For merchant who had PCI compliance and collect Card details from merchant system.</p>
                        <p>Payment Method must be ‘CC’. Paydibs Payment Page will prompt if value not provided.</p>
                    </td>
                </tr>--%>
                 <tr > 
                    <td>2.</td>
                    <td>TokenType</td>
                    <td>A</td>
                    <td>3</td>
                    <td>
                        Token Type
                        <br />OCP – One Click Payment
                    </td>
                </tr>
                 <tr > 
                    <td>3.</td>
                    <td>Token</td>
                    <td>ANS</td>
                    <td>50</td>
                    <td>
                        <p>Token value is required if TokenType is specified.</p>
                        <p>TokenType=OCP
                            <br />This token value will be return to merchant in payment response.</p>
                    </td>
                </tr>
                <tr style="background-color:grey;color:white; text-align:center"><td colspan="5"><b>Optional Field</b></td></tr>
                <tr > 
                    <td>1.</td>
                    <td>MerchantName</td>
                    <td>AN</td>
                    <td>25</td>
                    <td>Merchant’s business name</td>
                </tr>
                 <tr > 
                    <td>2.</td>
                    <td>MerchantCallbackURL</td>
                    <td>AN</td>
                    <td>255</td>
                    <td>
                        <p>It is an alternative response return back to merchant’s website via Server-toserver/backend without involve browser.</p>
                        <p>It is <b>highly recommended </b>to have this MerchantCallbackURL to be include into payment request.</p>
                        <p>This is useful when MerchantRURL (browser redirect URL) not able to receive payment response due to buyer’s Internet connectivity problem or buyer closed browser.</p>
                        <p>An acknowledgment “OK” message need to be return to Paydibs. Maximum of 5 times will be retry when failed to receive acknowledgement from merchant website</p>
                        <p>
                            <b>Note:</b> Replace “&” with “;” if any.<br />
                            e.g. <br />
                            https://merchantdomain/index.php?field1=value1&field2=value2
                            <br /><b>to</b><br />
                            https://merchantdomain/index.php?field1=value1;field2=value2
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>3.</td>
                    <td>PageTimeout</td>
                    <td>N</td>
                    <td>4</td>
                    <td>
                        <p>Applicable for merchant system which would like to bring forward to Payment Gateway, the time remaining before product/order is released.</p>
                        <p>
                            For example, a ticket sales page shows time remaining countdown from 15 minutes till 5 minutes. Upon customer’s clicking “checkout / proceed / pay” button, merchant system can then pass the value of (5 minutes x 60 seconds=300) seconds in this field to Paydibs which will then continue the countdown from 5 minutes. Upon timeout, all entry fields and buttons on the payment page will be disabled
                        </p>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <h4 class="sub-title">2.1.1 PAY Request Form Post Example</h4>
    <pre>
        <code>
         &lt;form name="frmTestPay" method="post" action="<i>&lt;URL provided by Paydibs&gt;"</i>&gt;
             &lt;input type="hidden" name="TxnType" value="PAY"&gt;
             &lt;input type="hidden" name="MerchantID" value="<i>&lt;ID provided by Paydibs&gt;"</i>&gt;
             &lt;input type="hidden" name="MerchantPymtID" value="Test000123"&gt;
             &lt;input type="hidden" name="MerchantOrdID" value="Order 123"&gt;
             &lt;input type="hidden" name="MerchantOrdDesc" value="Payment for abc"&gt;
             &lt;input type="hidden" name="MerchantTxnAmt" value="10.99"&gt;
             &lt;input type="hidden" name="MerchantCurrCode" value="MYR"&gt;
             &lt;input type="hidden" name="MerchantRURL" value="https://MerchantTestEnv.com/response.aspx"&gt;
             &lt;input type="hidden" name="CustIP" value="123.123.123.10"&gt;
             &lt;input type="hidden" name="CustName" value="Test"&gt;
             &lt;input type="hidden" name="CustEmail" value="test@testmail.com"&gt;
             &lt;input type="hidden" name="CustPhone" value="60123334567"&gt;
             &lt;input type="hidden" name="Sign" value=" &lt;Sign generated with SHA512&gt;"&gt;
             &lt;input type="hidden" name="MerchantCallbackURL" value="http://MerchantTestEnv.com/callbackresponse.aspx"&gt;
             &lt;input type="submit" value="Pay Now" /&gt;
         &lt;/form&gt;
        </code>
   </pre>
   
</asp:Content>
