﻿<%@ Page Title="API Reference" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="VoidResponse.aspx.cs" Inherits="PaydibsAPIv2.VoidResponse" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2><br />
    <h4 class="sub-title">2.6 VOID Response</h4>
    <p>Upon void process completion, the following fields will be returned from Paydibs to Merchant’s MerchantRURL in order to complete an end-to-end void process.</p>
    <br />
    <div class="table-responsive">
        <table class="table table-bordered">
            <thead style="background-color:gold; font-weight:bold">
                <tr>
                    <td>No.</td>
                    <td>Field</td>
                    <td>Data Type</td>
                    <td>Max Length</td>
                    <td>Description</td>
                </tr>
            </thead>
            <tbody>
                <tr style="background-color:grey;color:white; text-align:center"><td colspan="5"><b>Mandatory Field</b></td></tr>
                <tr>
                    <td>1. </td>
                    <td>TxnType</td>
                    <td>A</td>
                    <td>7</td>
                    <td>As request received from merchant system</td>
                </tr>
                <tr>
                    <td>2.</td>
                    <td>Method</td>
                    <td>A</td>
                    <td>3</td>
                    <td>Payment Method <br /><b>CC</b> – Credit Card</td>
                </tr>
                    <tr>
                    <td>3.</td>
                    <td>MerchantID</td>
                    <td>AN</td>
                    <td>3</td>
                    <td>As request received from merchant system</td>
                </tr>
                <tr>
                    <td>4.</td>
                    <td>MerchantPymtID</td>
                    <td>AN</td>
                    <td>20</td>
                    <td>As request received from merchant system</td>

                </tr>
                <tr>
                    <td>5.</td>
                    <td>MerchantTxnAmt</td>
                    <td>N</td>
                    <td>12(2)</td>
                    <td>As request received from merchant system</td>
                </tr>
                <tr>
                    <td>6.</td>
                    <td>MerchantCurrCode</td>
                    <td>A</td>
                    <td>3</td>
                    <td>As request received from merchant system</td>
                </tr>
                <tr>
                    <td>7.</td>
                    <td>PTxnID</td>
                    <td>AN</td>
                    <td>30</td>
                    <td>Unique payment transaction ID assigned by Paydibs for this transaction</td>
                </tr>
                <tr>
                    <td>8.</td>
                    <td>PTxnStatus</td>
                    <td>N</td>
                    <td>4</td>
                    <td>Indicate payment transaction statusRe: Section 3.2</td>
                </tr>
                <tr>
                    <td>7.</td>
                    <td>PTxnMsg</td>
                    <td>AN</td>
                    <td>255</td>
                    <td>Void transaction message that explains the PTxnStatus</td>
                </tr>
                <tr > 
                    <td>8.</td>
                    <td>AcqBank</td>
                    <td>AN</td>
                    <td>30</td>
                    <td>Bank name that processed the transaction</td>
                </tr>
                <tr>
                    <td>9.</td>
                    <td>BankRefNo</td>
                    <td>AN</td>
                    <td>20</td>
                    <td>Bank transaction reference code returned by bank if any</td>
                </tr>
                <tr>
                    <td>10.</td>
                    <td>Sign</td>
                    <td>AN</td>
                    <td>128</td>
                    <td>
                        <p>Checksum calculated by Paydibs in <b>hexadecimal</b> string using <b>SHA512</b> hash algorithm</p>
                        <p>Re: Section <a runat="server" href="~/Views/Appendices">2.7.2.1 PAY/QUERY/Void Response Sign</a></p>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <br />
    <h4 class="sub-title">2.6.1 Sample of Void Response to Merchant</h4>
    <pre>
        <code>
            <p style="margin:0 20px;">{"TxnType":"VOID","Method":"CC","MerchantID":"MerchantA","MerchantPymtID":"SIT20180801013","MerchantTxnAmt":"1.00","MerchantCurrCode":"MYR","PTxnID":"SIT20180801013","PTxnStatus":"0","PTxnMsg":"Void%20Success","AcqBank":"HostSim","BankRefNo":"SIT20180801013","Sign":"28ac33d573fa75d0cc1315a3009e49d2b2a8d09d511afeb664a84794854a7fe7ad5e6fb51055cbe7b20dfbf8bc4858db71c458313bd787b2e196aa6a009c14c6"}</p>
        </code>
    </pre>
</asp:Content>
