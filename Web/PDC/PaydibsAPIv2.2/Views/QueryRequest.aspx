﻿<%@ Page Title="API Reference" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="QueryRequest.aspx.cs" Inherits="PaydibsAPIv2.QueryRequest" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2><br />
    <h4 class="sub-title">2.3 Query Request</h4>
    <p>The following fields are expected from Merchant to Paydibs in order to query payment status. Merchant will get query response on same session without MerchantRURL involved.</p>
    <br />
    <p><b><i>Note: </i></b>The protocol used to communicate with Paydibs for <b><i>test</i></b> environment is <b><i>not limited</i></b> but <b><i>production</i></b> environment only allows <b><i>TLS 1.2</i></b> protocol for PCI compliance and security concerns. For Status Inquiry integration, it is recommended to test out the TLS 1.2</i></b> connectivity with production environment before going LIVE.</p>
    <div class="table-responsive">
        <table class="table table-bordered">
            <thead style="background-color:gold; font-weight:bold">
                <tr>
                    <td>No.</td>
                    <td>Field</td>
                    <td>Data Type</td>
                    <td>Max Length</td>
                    <td>Description</td>
                </tr>
            </thead>
            <tbody>
                <tr style="background-color:grey;color:white; text-align:center"><td colspan="5"><b>Mandatory Field</b></td></tr>
                <tr>
                    <td>1. </td>
                    <td>TxnType</td>
                    <td>A</td>
                    <td>7</td>
                    <td><b>QUERY</b><br />Note: Uppercase is required</td>
                </tr>
                <tr>
                    <td>2.</td>
                    <td>Method</td>
                    <td>AN</td>
                    <td>3</td>
                    <td>
                        <p>Payment Method submitted in the original PAY Request being queried.</p>
                        <p>If Method did not include in PAY Request, leave the value empty.</p>
                    </td>
                </tr>
                <tr>
                    <td>3.</td>
                    <td>MerchantID</td>
                    <td>AN</td>
                    <td>6</td>
                    <td>Merchant ID given by Paydibs.</td>
                </tr>
                <tr>
                    <td>4.</td>
                    <td>MerchantPymtID</td>
                    <td>AN</td>
                    <td>20</td>
                    <td><b>Unique</b> transaction/payment ID assigned by merchant for the original PAY request being queried.</td>
                </tr>
                <tr>
                    <td>5.</td>
                    <td>MerchantTxnAmt</td>
                    <td>AN</td>
                    <td>20</td>
                    <td>Payment Amount submitted in PAY Request</td>
                </tr>
                <tr>
                    <td>6.</td>
                    <td>MerchantCurrCode</td>
                    <td>A</td>
                    <td>3</td>
                    <td>Payment Currency Code submitted in PAY Request</td>
                </tr>
                <tr>
                    <td>7.</td>
                    <td>Sign</td>
                    <td>AN</td>
                    <td>128</td>
                    <td><p>Checksum calculated by Merchant System in hexadecimal string using SHA512 hash algorithm</p>
                        <p>Re: Section <a runat="server" href="~/Views/Sign">2.7.1.2 QUERY/VOID Request Sign</a></p>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <br />
    <h4 class="sub-title">2.3.1 Query Request and Receive Query Response Sample Code</h4>
    <pre>
        <code>

            using System;
            using System.IO;
            using System.Net;
            using System.Text;
            namespace Examples.System.Net
            {
                public class WebRequestPostExample
                {
                    public static void Main ()
                    {
                         <i style="color:darkgrey">// Create a request using a URL that can receive a post.</i>
                         WebRequest request = WebRequest.Create ("&lt;URL provided by Paydibs&gt;");
                         <i style="color:darkgrey">// Set the Method property of the request to POST.</i>
                         request.Method = "POST";
                         <i style="color:darkgrey">// Create POST data and convert it to a byte array.</i>
                         String postData="TxnType=QUERY&Method=OB&MerchantID=MerchantA&MerchantPymtID=TEST000123&Amount=123.00&MerchantCurrCode=MYR
                                    &Sign=&lt;Sign generated using SHA512 algorithm&gt;";
                         byte[] byteArray = Encoding.UTF8.GetBytes (postData);
                         <i style="color:darkgrey">// Set the ContentType property of the WebRequest.</i>
                         request.ContentType = "application/x-www-form-urlencoded";
                         <i style="color:darkgrey">// Set the ContentLength property of the WebRequest.</i>
                         request.ContentLength = byteArray.Length;
                         <i style="color:darkgrey">// Get the request stream.</i>
                         Stream dataStream = request.GetRequestStream ();
                         <i style="color:darkgrey">// Write the data to the request stream.</i>
                         dataStream.Write (byteArray, 0, byteArray.Length);
                         <i style="color:darkgrey">// Close the Stream object.</i>
                         dataStream.Close ();
                         <i style="color:darkgrey">// Get the response.</i>
                         WebResponse response = request.GetResponse ();
                         <i style="color:darkgrey">// Display the status.</i>
                         Console.WriteLine (((HttpWebResponse)response).StatusDescription);
                         <i style="color:darkgrey">// Get the stream containing content returned by the server.</i>
                         dataStream = response.GetResponseStream ();
                         <i style="color:darkgrey">// Open the stream using a StreamReader for easy access.</i>
                         StreamReader reader = new StreamReader (dataStream);
                         <i style="color:darkgrey">// Read the content.</i>
                         string responseFromServer = reader.ReadToEnd ();
                         <i style="color:darkgrey">// Display the content.</i>
                         Console.WriteLine (responseFromServer);
                         <i style="color:darkgrey">//Parse query response from Paydibs. Response field refer to section 2.4</i>
                         <i style="color:darkgrey">// Clean up the streams.</i>
                         reader.Close ();
                         dataStream.Close ();
                         response.Close ();
                     }
                 }
            }
        </code>
   </pre>
   <p>Reference from <a href="http://msdn.microsoft.com/en-us/library/debx8sh9.aspx" target="_blank">http://msdn.microsoft.com/en-us/library/debx8sh9.aspx</a></p>
  
   
</asp:Content>
