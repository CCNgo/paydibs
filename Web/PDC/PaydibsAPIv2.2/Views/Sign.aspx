﻿<%@ Page Title="SIGN (Hashing)" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Sign.aspx.cs" Inherits="PaydibsAPIv2.Sign" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <br />
    <h3>2.7.1 Request Sign</h3>
    <h4 class="sub-title"> 2.7.1.1 PAY Request Sign</h4><br />
    <p> PAY request’s Sign source string should be formed as following fields: </p>
    <p>MerchantPassword + TxnType + MerchantID + MerchantPymtID + MerchantOrdID + MerchantRURL + MerchantTxnAmt + MerchantCurrCode + CustIP + PageTimeout + MerchantCallbackURL <!--+ CardNo--> + Token</p>
    <br />
    <b>NOTE:</b><br />
    <p>a) If PageTimeout / MerchantCallbackURL / <!--CardNo /--> Token are not provided in PAY request, then these fields in the above should be just left empty.</p>
    <p>b) Token value in Sign source string needs to be in clear format and not in encoded format.</p>
    <br />
    <p>Sign Example</p>
    1. MerchantPassword = abc123<br />
    2. TxnType = PAY<br />
    3. MerchantID = Paydibs<br />
    4. MerchantPymtID = INV20180420001<br />
    5. MerchantOrdID = Order_1<br />
    6. MerchantRURL = https://www/sample.com/response.php<br />
    7. MerchantTxnAmt = 10.99<br />
    8. MerchantCurrCode = MYR<br />
    9. CustIP = 123.10.100.123<br />
    10. PageTimeout = *not include in PAY request as this is optional field<br />
    11. MerchantCallbackURL = https://www/sample.com/callbackresponse.php<br />
    <br />
    <p><b>Source String</b></p>
    <p>abc123PAYPaydibsINV20180420001Order_1https://www/sample.com/response.php10.99MYR123.10.100.123https://www/sample.com/callbackresponse.php</p>
     <br />
    <p><b>Sign with SHA512</b></p>
    <p>5a5cce51af6d9e1949b086e7d5dab4901634a2eb0f35dee863c4606b79b5c2024699a2942ebb0a20f264b912b180d912da42afe71e0c011268a76d9ef493beb9</p>
    <br />
    <h4 class="sub-title"> 2.7.1.2 QUERY/VOID Request Sign</h4>
    <p>Query request’s Sign should be generated based on the following fields: </p>
    <p>MerchantPassword + MerchantID + MerchantPymtID + MerchantTxnAmt + MerchantCurrCode</p>
    <br />
    <p><b>Source String</b></p>
    <p>abc123PaydibsINV2018042000110.99MYR</p>
    <br />
    <p><b>Sign with SHA512</b></p>
    <p>3222edaa5e81fc4dbbd2f476e4b3b7b072574bde4635b079e2a68aeecb4d220fcba9286acc3f7aa2838e0f90f13f4a006a7c551684a07a9c414360ecf213b6d4</p>
    <br />
    <div style="margin:7em 0 0;">
        <h3> 2.7.2 Response Sign</h3>
        <h4 class="sub-title">2.7.2.1 PAY/QUERY/VOID Response Sign</h4><br />
        <p>Payment/Query response’s Sign should be generated based on the following fields:</p>
        <p>MerchantPassword + MerchantID + MerchantPymtID + PTxnID + MerchantOrdID + MerchantTxnAmt + MerchantCurrCode + PTxnStatus + AuthCode</p>
        <br />
        <p><b>Source String</b></p>
        <p>abc123PaydibsINV20180420001Paydibs000000101Order1234567810.99MYR0339900</p>
        <br />    
        <p><b>Sign with SHA512</b></p>
        <p>387e0d6b2651a21ce0fcb58c6b9c7d6451db4aab09632509f49b18cbf641d70fa141c7f86a9a3285e9f5d229002f01c9ca8ef92f590d51796ca840b80dc74735</p>
   </div>
</asp:Content>
