﻿<%@ Page Title=" VOID Request" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="VoidRequest.aspx.cs" Inherits="PaydibsAPIv2.VoidRequest" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2><br />
    <h4 class="sub-title">2.5 VOID Request</h4>
    <p>A <b>VOID</b> of a monetary transaction (e.g. ‘PAY’) is only allowed on the same calendar day as of the original request. It is advisable to void a transaction within <b>one hour</b> of its original pay time. Currently this is only applicable for credit card payment method only.</p>
    <p>It is recommended Merchant to send a Query request to Paydibs to get the actual payment status before sending Void request. Query response will help Merchant to know whether the payment status is successful in order to perform a Void request of the original payment.</p>
    <p>For a Void request, a valid merchant payment id from a previous request is required. The amount and currency code defined in the Void request has to match the amount and currency code given in the respective request that needs to be cancelled.</p>
    <p>The following fields are expected from Merchant to Paydibs in order to void a payment. Merchant will get void response on same session without MerchantRURL involved.</p>
    <p>Void function <b>will be only enabled upon request.</b></p>
    <br />
    <p><b>Note</b>: The protocol used to communicate with Paydibs for <b>test</b> environment is <b>not limited</b> but <b>production</b> environment only allows <b>TLS 1.2</b> protocol for PCI compliance and security concerns. For Status Inquiry integration, it is recommended to test out the TLS 1.2 connectivity with production environment before going LIVE.</p>
    <div class="table-responsive">
        <table class="table table-bordered">
            <thead style="background-color:gold; font-weight:bold">
                <tr>
                    <td>No.</td>
                    <td>Field</td>
                    <td>Data Type</td>
                    <td>Max Length</td>
                    <td>Description</td>
                </tr>
            </thead>
            <tbody>
                <tr style="background-color:grey;color:white; text-align:center"><td colspan="5"><b>Mandatory Field</b></td></tr>
                <tr>
                    <td>1. </td>
                    <td>TxnType</td>
                    <td>A</td>
                    <td>7</td>
                    <td><b>VOID</b> – Payment request type <br />Note: Uppercase is required</td>
                </tr>
                <tr>
                    <td>2.</td>
                    <td>Method</td>
                    <td>A</td>
                    <td>3</td>
                    <td>Payment Method submitted in the original PAY Request.</td>
                </tr>
                    <tr>
                    <td>3.</td>
                    <td>MerchantID</td>
                    <td>AN</td>
                    <td>3</td>
                    <td>Merchant ID given by Paydibs.</td>
                </tr>
                <tr>
                    <td>4.</td>
                    <td>MerchantPymtID</td>
                    <td>AN</td>
                    <td>20</td>
                    <td>
                        <p><b>Unique </b>transaction/payment ID assigned by merchant for the original PAY request.</p>
                    </td>
                </tr>
                <tr>
                    <td>5.</td>
                    <td>MerchantTxnAmt</td>
                    <td>N</td>
                    <td>12(2)</td>
                    <td>Payment Amount submitted in PAY Request</td>
                </tr>
                <tr>
                    <td>6.</td>
                    <td>MerchantCurrCode</td>
                    <td>A</td>
                    <td>3</td>
                    <td>Payment Currency Code submitted in PAY Request</td>
                </tr>
                <tr>
                    <td>7.</td>
                    <td>Sign</td>
                    <td>AN</td>
                    <td>128</td>
                    <td><p>Checksum calculated by Merchant System in hexadecimal string using SHA512 hash algorithm</p>
                        <p>Re: Section <a runat="server" href="~/Views/Sign">2.7.1.2 QUERY/VOID Request Sign</a></p>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <br />
    <h4 class="sub-title">2.5.1 Void Request and Void Response Sample Code</h4>
    <p>Refer section <a runat="server" href="~/Views/QueryRequest">2.3.1 Query Request and Receive Query Response Sample Code</a></p>
   
</asp:Content>
