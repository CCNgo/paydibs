﻿<%@ Page Title="APPENDICES" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Appendices.aspx.cs" Inherits="PaydibsAPIv2.Appendices" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <br />
    <h3>3.1 Transaction Type</h3>
    <div class="table-responsive">
        <table class="table table-bordered" style="width: 60%;">
            <thead style="background-color:gold; font-weight:bold">
                <tr>
                    <td style="width: 307px">TxnType</td>
                    <td>Description</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style="width: 307px">PAY</td>
                    <td>Payment</td>
                </tr>
                <tr>
                    <td style="width: 307px">QUERY</td>
                    <td>Status inquiry</td>
                </tr>
                <tr>
                    <td style="width: 307px">VOID</td>
                    <td>Reversal</td>
                </tr>
            </tbody>
        </table>
    </div>

    <div style="margin:5em 0 0;">
        <h3>3.2 Payment Status</h3>
        <div class="table-responsive">
        <table class="table table-bordered" style="width: 60%;">
            <thead style="background-color:gold; font-weight:bold">
                <tr>
                    <td style="width:30%">PTxnStatus</td>
                    <td>Description</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>0</td>
                    <td>Payment successful</td>
                </tr>
                <tr>
                    <td>1</td>
                    <td>Payment Failed</td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Payment pending</td>
                </tr>
            </tbody>
        </table>
    </div>
   </div>

    <div style="margin:5em 0 0;">
        <h3>3.3 Query Status</h3>
        <div class="table-responsive">
        <table class="table table-bordered" style="width: 60%;">
            <thead style="background-color:gold; font-weight:bold">
                <tr>
                    <td style="width:30%">PTxnStatus</td>
                    <td>Description</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>0</td>
                    <td>Payment successful (for TxnType=PAY)</td>
                </tr>
                <tr>
                    <td>1</td>
                    <td>Payment failed</td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Payment pending, retry Query</td>
                </tr>
                <tr>
                    <td>9</td>
                    <td>Payment voided</td>
                </tr>
                <tr>
                    <td>-1</td>
                    <td>Transaction not exists/not found</td>
                </tr>
                <tr>
                    <td>-2</td>
                    <td>Internal system error</td>
                </tr>
            </tbody>
        </table>
    </div>
   </div>

    <div style="margin:5em 0 0;">
        <h3>3.4 Currency Code</h3>
        <div class="table-responsive">
            <table class="table table-bordered" style="width: 60%;">
                <thead style="background-color:gold; font-weight:bold">
                    <tr>
                        <td style="width:30%">Currency Code</td>
                        <td>Currency</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>MYR</td>
                        <td>Malaysia Ringgit</td>
                    </tr>
                     <tr>
                        <td>SGD</td>
                        <td>Singapore Dollar</td>
                    </tr>
                     <tr>
                        <td>PHP</td>
                        <td>Philippine Peso</td>
                    </tr>
                </tbody>
            </table>
        </div>
   </div>

    <div style="margin:5em 0 0;">
        <h3>3.5 Language Code</h3>
        <div class="table-responsive">
            <table class="table table-bordered" style="width: 60%;">
                <thead style="background-color:gold; font-weight:bold">
                    <tr>
                        <td style="width:30%">Language Code</td>
                        <td>Language</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>EN</td>
                        <td>English (default language)</td>
                    </tr>
                </tbody>
            </table>
        </div>
   </div>

    <div style="margin:5em 0 0;">
        <h3>3.6 Country Code</h3>
        <div class="table-responsive">
            <table class="table table-bordered" style="width: 60%;">
                <thead style="background-color:gold; font-weight:bold">
                    <tr>
                        <td style="width:30%">Country Code</td>
                        <td>Country</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>MY</td>
                        <td>Malaysia</td>
                    </tr>
                </tbody>
            </table>
        </div>
   </div>

    <div style="margin:5em 0 0;">
        <h3>3.7 Credit Card Test</h3>
        <div class="table-responsive">
            <table class="table table-bordered" style="width: 60%;">
                <thead style="background-color:gold; font-weight:bold">
                    <tr>
                        <td style="width:30%">Test Card</td>
                        <td>Status</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>4111 1111 1111 1111<br />5500 0000 0000 0012</td>
                        <td>Success</td>
                    </tr>
                    <tr>
                        <td>4111 1111 1111 1194<br />5500 0000 0000 0004</td>
                        <td>Failed</td>
                    </tr>
                </tbody>
            </table>
        </div>
   </div>

    <p><i>Note: <br />
        - Expiry date for credit card need to be valid as of the current month and year.<br />
        - CVV 3 digits for Visa/MasterCard<br />
        - No OTP or any features available in testing environment<br />
       </i></p>
</asp:Content>
