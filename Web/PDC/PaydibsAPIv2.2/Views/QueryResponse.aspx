﻿<%@ Page Title="API Reference" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="QueryResponse.aspx.cs" Inherits="PaydibsAPIv2.QueryResponse" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2><br />
    <h4 class="sub-title">2.4 Query Response</h4>
    <p>Upon query process completion, Paydibs will return all fields same as PAY Response fields together with the following additional fields. Merchant will receive query response on the same session without involved MerchantRURL.</p>
    <br />
    <p>For Query transaction status, please refer to section <a runat="server" href="~/Views/Appendices">3.3 Query Status</a></p>
    <div class="table-responsive">
        <table class="table table-bordered">
            <thead style="background-color:gold; font-weight:bold">
                <tr>
                    <td>No.</td>
                    <td>Field</td>
                    <td>Data Type</td>
                    <td>Max Length</td>
                    <td>Description</td>
                </tr>
            </thead>
            <tbody>
                <tr style="background-color:grey;color:white; text-align:center"><td colspan="5"><b>Mandatory Field</b></td></tr>
                <tr>
                    <td>1. </td>
                    <td>PAYExists</td>
                    <td>N</td>
                    <td>1</td>
                    <td>
                        <p>An identifier to indicate whether the transaction being queried exists in Paydibs.</p>
                        <p><b>0</b> – <br />
                        Transaction being queried <b>exists</b> in Paydibs.</p>
                        <p><b>1</b> – <br />
                        Transaction being queried <b>does notexist</b> in Paydibs.</p>
                        <p><b>2</b> – <br />
                        There was some kind of <b>error</b> occurred during query processing. Merchant can retry sending query request</p>     
                    </td>
                </tr>
                
                <tr style="background-color:grey;color:white; text-align:center"><td colspan="5"><b>Optional Field</b></td></tr>
                <tr > 
                    <td>1.</td>
                    <td>QueryDesc</td>
                    <td>AN</td>
                    <td>255</td>
                    <td>Description of query result</td>
                </tr>
            </tbody>
        </table>
    </div>
    <br />
    <h4 class="sub-title">2.4.1 Sample of Query Response to Merchant</h4>
    <p>Sample 1:</p>
    <pre>
        <code>
            <p  style="margin:0 20px;">{"PAYExists":"0","QueryDesc":"Record exists","TxnType":"QUERY","Method":"OB","MerchantID":"MerchantA","MerchantPymtID":"TEST0000123","MerchantOrdID":"Test123","MerchantTxnAmt":"10.00","MerchantCurrCode":"MYR”, …..}</p>
        </code>
    </pre>
    <br />
    <p>Sample 2:</p>
    <pre>
        <code>
            <p  style="margin:0 20px;">{"PAYExists":"1","QueryDesc":"Transaction Not Exists","TxnType":"QUERY","Method":"","MerchantID":"MerchantA","MerchantPymtID":"TEST0000124","MerchantOrdID":"","MerchantTxnAmt":"10.00","MerchantCurrCode":"MYR", …..}</p>
        </code>
    </pre>
    <br />
    <p>Sample 3:</p>
    <pre>
        <code>
            <p  style="margin:0 20px;">{"PAYExists":"2","QueryDesc":"Required field(s) is missing.","TxnType":"QUERY","Method":"ANY","MerchantID":"","MerchantPymtID":"","MerchantOrdID":"","MerchantTxnAmt":"","MerchantCurrCode":"", …..}</p>
        </code>
    </pre>

</asp:Content>
