﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NavBar.ascx.cs" Inherits="PDCDocument.NavBar" %>
<!DOCTYPE html>



<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>PDP Document</title>

    <environment include="Development">
        <link rel="stylesheet" href="~/lib/bootstrap/dist/css/bootstrap.css" />
        <link rel="stylesheet" href="~/css/site.css" />
        <link rel="stylesheet" href="~/css/main.css" />
    </environment>
    <environment exclude="Development">
        <link rel="stylesheet" href="https://ajax.aspnetcdn.com/ajax/bootstrap/3.3.7/css/bootstrap.min.css"
              asp-fallback-href="~/lib/bootstrap/dist/css/bootstrap.min.css"
              asp-fallback-test-class="sr-only" asp-fallback-test-property="position" asp-fallback-test-value="absolute" />
        <link rel="stylesheet" href="~/css/site.min.css" asp-append-version="true" />
    </environment>
    <style>
        

    </style>
</head>
<body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a asp-area="" asp-controller="Home" asp-action="Index" class="navbar-brand">PDC Documentation</a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li><a asp-area="" asp-controller="Home" asp-action="Index">Home</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Transactions <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a asp-area="" asp-controller="Transactions" asp-action="Index">Transactions</a></li>
                            <li><a asp-area="" asp-controller="Transactions" asp-action="Reseller">Reseller</a></li>
                            <li><a asp-area="" asp-controller="Transactions" asp-action="TransactionsFeeReport">Transactions Fee Report</a></li>
                            <li><a asp-area="" asp-controller="Transactions" asp-action="ResellerMarginReport">Reseller Margin Report</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Merchant <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a asp-area="" asp-controller="Merchant" asp-action="ViewMerchant">View Merchant</a></li>
                            <li><a asp-area="" asp-controller="Merchant" asp-action="ViewChannel">View Channel</a></li>
                            <li><a asp-area="" asp-controller="Merchant" asp-action="ViewMerchantRate">View Merchant Rate</a></li>
                            <li><a asp-area="" asp-controller="Merchant" asp-action="ViewResellerRate">View Reseller Rate</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">User <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a asp-area="" asp-controller="User" asp-action="Index">User</a></li>
                            <li><a asp-area="" asp-controller="User" asp-action="ViewUser">View User</a></li>
                            <li><a asp-area="" asp-controller="User" asp-action="ViewUserGroup">View User Group</a></li>
                            <li><a asp-area="" asp-controller="User" asp-action="ChangePassword">Change Password</a></li>
                            <li><a asp-area="" asp-controller="User" asp-action="ForgotPassword">Forgot Password</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Merchant Services <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a asp-area="" asp-controller="Management" asp-action="EmailPayment">Email Payment (Single/Bulk)</a></li>
                            <li><a asp-area="" asp-controller="Management" asp-action="BuyBotton">Buy Now Button</a></li>
                        </ul>
                    </li>
                   
                </ul>

            </div>
        </div>
    </nav>
        <partial name="_CookieConsentPartial" />
        <div class="container body-content">
  
            <hr />
            <footer class="footer" style="text-align:center;color:#808080">
                <p>Paydibs Snd. Bhd &copy; 2019 - PDC Documentation</p>
            </footer>
        </div>

        <environment include="Development">
            <script src="~/lib/jquery/dist/jquery.js"></script>
            <script src="~/lib/bootstrap/dist/js/bootstrap.js"></script>
            <script src="~/js/site.js" asp-append-version="true"></script>
        </environment>
        <environment exclude="Development">
            <script src="https://ajax.aspnetcdn.com/ajax/jquery/jquery-3.3.1.min.js"
                    asp-fallback-src="~/lib/jquery/dist/jquery.min.js"
                    asp-fallback-test="window.jQuery"
                    crossorigin="anonymous"
                    integrity="sha384-tsQFqpEReu7ZLhBV2VZlAu7zcOV+rXbYlF2cqB8txI/8aZajjp4Bqd+V6D5IgvKT">
            </script>
            <script src="https://ajax.aspnetcdn.com/ajax/bootstrap/3.3.7/bootstrap.min.js"
                    asp-fallback-src="~/lib/bootstrap/dist/js/bootstrap.min.js"
                    asp-fallback-test="window.jQuery && window.jQuery.fn && window.jQuery.fn.modal"
                    crossorigin="anonymous"
                    integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa">
            </script>
            <script src="~/js/site.min.js" asp-append-version="true"></script>
        </environment>

</body>
</html>



