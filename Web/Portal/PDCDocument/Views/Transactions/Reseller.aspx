﻿<%@ Page Title="RESELLER" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" Inherits="PDCDocument._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <h4>Menu</h4>
    <p>Reseller Menu located on left side. Click on icon.</p><br />
    <p>Reseller Menu:</p>
    <ul>
        <li>Reseller Report</li>
        <li>Online Banking Report</li>
        <li>Credit Card Report</li>
    </ul>
    <img src="../../images/reseller/reseller_menu.png" class="img_info size_img300" /><br />

    <h4>Get Reseller Report</h4>
    <img src="../../images/reseller/reseller_report_get.png" class="img_info size_img800" /><br />
    <p>Filter by:</p>
    <ul>
        <li>Select Merchant Name</li>
        <li>Date Since/To: Date range to select for generate report</li>
        <li>
            <p>Select Status: </p>
            <ul style="list-style-type: square;display: inline-block;">
                <li>Success</li>
                <li>Failed</li>
                <li>Pending</li>
                <li>Other</li>
            </ul>
        </li>
    </ul>
    <p>Click Button 'Get Report'</p>
    <h4>Advanced Seacrh</h4>
    <p>Click button 'Advanced Search'</p>
    <img src="../../images/reseller/reseller_adv_search.png" class="img_info size_img800" /><br />
    <p>Select/ Insert info to Search then click button 'Ok'</p>
    <h4>Download Reseller Report</h4>
    <p>Example Report Excel</p>
    <img src="../../images/reseller/reseller_report_download.png" class="img_info size_img800" /><br />


    <h4>Get Report Online Banking</h4>
    <img src="../../images/reseller/reseller_online_banking.png" class="img_info size_img800" /><br />
    <h4>Download Report Online Banking</h4>
    <p>Example Report Excel</p>
    <img src="../../images/reseller/reseller_online_banking_download.png" class="img_info size_img800" /><br />

    <h4>Get Report Credit Card</h4>
    <img src="../../images/reseller/reseller_credit_card.png" class="img_info size_img800" /><br />
    <h4>Download Report Credit Card</h4>
    <p>Example Report Excel</p>
    <img src="../../images/reseller/reseller_credit_card_download.png" class="img_info size_img800" /><br />
</asp:Content>

