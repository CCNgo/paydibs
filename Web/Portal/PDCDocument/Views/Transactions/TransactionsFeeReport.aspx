﻿<%@ Page Title="TRANSACTIONS FEE REPORT" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" Inherits="PDCDocument._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <h4>Menu</h4>
    <p>Transactions Fee Report located on left side. Click on icon.</p><br />
    <img src="../../images/transactions/trans_fee_report_menu.png" class="img_info size_img300" /><br />
    <h4>Get Report</h4>
    <p>Filtered by:</p>
    <ul>
        <li>Merchant</li>
        <li>Date range From date and To date</li>
        <li>Currency</li>
    </ul>
    <img src="../../images/transactions/trans_fee_report.png" class="img_info size_img800" /><br />
    <h4>Download Report</h4>
    <p>Example Excel</p>
    <img src="../../images/transactions/trans_fee_report_download.png" class="img_info size_img800" /><br />

</asp:Content>

