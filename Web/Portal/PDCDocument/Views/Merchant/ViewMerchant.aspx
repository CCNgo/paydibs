﻿<%@ Page Title="VIEW MERCHANT" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" Inherits="PDCDocument._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <h4>Menu</h4>
    <p>View Menu located on left side. Click on icon. </p><br />
    <img src="../../images/merchant/view_merchant_menu.png" class="img_info size_img300" /><br />
    <h4>Details</h4>
    <p>Display all details of merchant. </p>
    <img src="../../images/merchant/view_merchant_profile.png" class="img_info size_img800" /><br />
    <img src="../../images/merchant/view_merchant_profile02.png" class="img_info size_img800" /><br />
</asp:Content>

