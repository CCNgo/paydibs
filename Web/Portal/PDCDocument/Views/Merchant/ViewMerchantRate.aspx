﻿<%@ Page Title="VIEW MERCHANT RATE" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" Inherits="PDCDocument._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <h4>Menu</h4>
    <p>View Merchant Rate located on left side. Click on icon. </p><br />
    <img src="../../images/merchant/merchant_rate_menu.png" class="img_info size_img300" /><br />
    <h4>Details</h4>
    <ul>
        <li>Payment Method : Type of payment method that Reseller can be used.</li>
        <li>Channel: It is the part of a carrier-multiplex terminal.</li>
        <li>Rate: Charges in respect of personal current accounts.</li>
        <li>Fixed Fee:  An amount that is charged or paid. </li>
        <li>Currency: Accepted currency </li>
    </ul>
    <img src="../../images/merchant/merchant_rate.png" class="img_info size_img800" /><br />

</asp:Content>

