﻿<%@ Page Title="VIEW CHANNEL" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" Inherits="PDCDocument._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <h4>Menu</h4>
    <p>View Channel located on left side. Click on icon. </p><br />
    <img src="../../images/merchant/view_channel_menu.png" class="img_info size_img300" /><br />
    <h4>Details</h4>
    <ul>
        <li>Type:</li>
        <li>Channel Desc: </li>
        <li>Currency Code: Accepted currency </li>
    </ul>
    <img src="../../images/merchant/view_channel.png" class="img_info size_img800" /><br />
</asp:Content>

