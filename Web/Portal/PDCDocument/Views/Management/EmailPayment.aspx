﻿<%@ Page Title="EMAIL PAYMENT" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"  Inherits="PDCDocument._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <h4>Menu</h4>
    <p>Email Payment (Single/Bulk) located on left side. Click on icon.</p><br />
    <img src="../../images/management/email_payment_menu.png" class="img_info size_img300" /><br />
    <h4>Manage Email</h4>
    <img src="../../images/management/email_payment.png" class="img_info size_img800" /><br />
    <h4>Email Preview</h4>
    <img src="../../images/management/email_payment_preview.png" class="img_info size_img800" /><br />
    <p>Click button 'Send' to send the email.</p>
    <h4>Recieve Email Content</h4>
    <img src="../../images/management/email_payment_send.png" class="img_info size_img800" /><br />
</asp:Content>
