﻿<%@ Page Title="PORTAL LOGIN" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" Inherits="PDCDocument._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <p>
        Merchant will receive a User ID and Password to login into Paydibs portal when account goes Live.
        User ID will be the main contact person’s <b><u>email</u></b> which provided from Merchant Application Form during submission.
        A temporary password will be given and user need to <u>change the password during first login</u>.
    </p>
    <img src="../../images/user/login.png" class="img_info size_img300" /><br />
    <ul style="list-style-type: square;display: inline-block;">
        <li>Enter User ID (Note: email) and Password</li>
        <li>Click on ‘Sign In’ button</li>
    </ul>
    <hr />
    <h3>PORTAL DASHBOARD</h3>
    <p>Portal dashboard after login (Check Reference For Details)</p>
    <img src="../../images/user/dashboard01.png" class=" size_img800" />
    <img src="../../images/user/dashboard02.png" class=" size_img800" />

    <h4>Reference</h4>
    <ol>
        <li>Logo Paydibs</li>
        <li>Click to expand menu for details</li>
        <li>Notification</li>
        <li>Account profile user</li>
        <li>Menu</li>
        <li>Date Since/To : Date range to select for generate report</li>
        <li>Total Payments Value: Total sales of the day</li>
        <li>Total Transaction: Total transaction of the day</li>
        <li>Total Revenue : Summary line chart of the total sales, transactions count and fees</li>
        <li>Time of the day : Summary line chart for sales of the day by hour</li>
        <li>Channel Usage : Channel usage of payment by percentage</li>
    </ol>

    <h4>Ref. No.4 : Accout Profile User</h4>
    User account profile located on dashboard top right. Click on icon.<br />
    <img src="../../images/user/user_profile.png" class="img_info size_img300" /><br />
    <p>Click Account and account user information will display.</p>
    <img src="../../images/user/account_info.png" class="img_info size_img500"/><br />
    <p>To Reset password, click link '<span style='color:blue'>Reset Password</span>'</p>
    <img src="../../images/user/account_info_reset_pass.png" class="img_info size_img500"/><br />
    <p>An email will send to User ID. Click on the link in email and it will redirect to ‘Change Password Page’. User get temporary password and have to change with user own password.</p>
    <p>Email Example</p>
    <img src="../../images/user/account_info_temp_pass.png" class="img_info size_img800"/><br />
</asp:Content>

