﻿<%@ Page Title="VIEW USER" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" Inherits="PDCDocument._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <h4>Menu</h4>
    <p>View User located on left side. Click on icon.</p><br />
    <img src="../../images/user/view_user_menu.png" class="img_info size_img300" /><br />
    <h4>List User</h4>
    <p>Filtered by:</p>
    <ul>
        <li>Merchant</li>
    </ul>
    <img src="../../images/user/user_view.png" class="img_info size_img800" /><br />
</asp:Content>

