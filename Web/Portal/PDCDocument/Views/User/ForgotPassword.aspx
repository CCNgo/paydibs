﻿<%@ Page Title="FORGOT PASSWORD" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" Inherits="PDCDocument._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
   <h2><%: Title %></h2>
    <img src="../../images/user/login.png" class="img_info size_img300" /><br />
    <p>Click link <span style='color:blue'>Forgot Password?</span></p>

    <img src="../../images/user/forgot_pass.png" class="img_info size_img300" /><br />
    <p>Insert UserID and click button Forgot Password</p>
    <img src="../../images/user/forgot_pass_suc.png" class="img_info size_img300" /><br />
    <p>Notification that your request 'Forgot Passsword' will be send to your Registered Email.</p>
    <p><u>Example Email</u></p>
    <img src="../../images/user/account_info_temp_pass.png" class="img_info size_img500" /><br />
    <p>Click link 'Step 2: ...' and insert your New Password and Confirm New Password</p>
    <img src="../../images/user/forgot_pass_new.png" class="img_info size_img300" /><br />
    <p>After insert New Password and Confirm New Password click button Update Password</p>
    <p>Now you can login with new password.</p>
</asp:Content>

