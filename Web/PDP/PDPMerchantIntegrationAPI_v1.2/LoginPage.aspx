﻿<%@ Page Title="Login/ Logout" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="LoginPage.aspx.cs" Inherits="PDPMerchantIntegrationAPI_v1._2.LoginPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
     <h2> </h2>
    <div class="row">
        <div class="col-md-8">       
            <% if (!this.Page.User.Identity.IsAuthenticated){ %>
                <section id="loginForm">
                    <div class="form-horizontal">
                        <h4>Enter password to log in.</h4>
                        <hr />
                        <asp:PlaceHolder runat="server" ID="ErrorMessage" Visible="false">
                            <p class="text-danger">
                                <asp:Literal runat="server" ID="FailureText" />
                            </p>
                        </asp:PlaceHolder>
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="Password" CssClass="col-md-2 control-label">Password</asp:Label>
                            <div class="col-md-10">
                                <asp:TextBox runat="server" ID="Password" TextMode="Password" CssClass="form-control" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="Password" CssClass="text-danger" ErrorMessage="The password field is required." />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-2 col-md-10">
                                <asp:Button runat="server" OnClick="LogIn" Text="Log in" CssClass="btn btn-primary" />
                            </div>
                        </div>
                    </div>
                </section>
            <% } else { %>
               <h4>Revision History</h4>
            <p>Please click here to <asp:LoginStatus ID="LoginStatus2" runat="server" /></p>
            <% } %>

        </div>
    </div>
</asp:Content>
