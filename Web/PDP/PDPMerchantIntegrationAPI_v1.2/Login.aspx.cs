﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;

namespace PDPMerchantIntegrationAPI_v1._2
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           
        }
        protected void LogIn(object sender, System.EventArgs e)
        {
            string postedValues = Password.Text;
            string szpw = System.Web.Configuration.WebConfigurationManager.AppSettings["LoginPw"];
            string pwd512 = GenerateSHA512String(postedValues);

            if(string.IsNullOrEmpty(postedValues))
            {
                ErrorMessage.Visible = true;
                FailureText.Text = "The password field is required.";
            }
            else if(szpw == pwd512)
            {
                FormsAuthentication.RedirectFromLoginPage(postedValues, true);
            }
            else
            {
                ErrorMessage.Visible = true;
                FailureText.Text = "Wrong Password!";
            }
        }
        protected static string GenerateSHA512String(string randomString)
        {
            var crypt = new System.Security.Cryptography.SHA512Managed();
            var hash = new System.Text.StringBuilder();
            byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(randomString));
            foreach (byte theByte in crypto)
            {
                hash.Append(theByte.ToString("X2"));
            }
            return hash.ToString();
        }
    }
}