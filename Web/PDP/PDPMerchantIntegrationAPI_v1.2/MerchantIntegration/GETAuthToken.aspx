﻿<%@ Page Title="2. GET AUTH TOKEN" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="GETAuthToken.aspx.cs" Inherits="PDPMerchantIntegrationAPI_v1._2.MerchantIntegration.GETAuthToken" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<h2><%: Title %></h2>
    <div style="margin:20px 10px;">
        <p>
            Description:<br />
            <span style="margin:0 20px 20px">• Request Auth-Token</span>
        </p>
        <p>
            Request URL:<br />
            <span style="margin:0 20px 20px">• http://xx.com/version1/token/api-token</span>
        </p>
        <p>
            Request Method:<br />
            <span style="margin:0 20px 20px">• POST</span>
        </p>
        <p>Request Parameter:</p>
        <pre>
            <code>
            header:
            {
                Content-Type: application/x-www-form-urlencoded
                Key: Tpa3b34Mulw98WW
            }

            body:
            {
                login_id: 99SM20018
                login_password: d0970714757783e6cf17b26fb8e2298f
                time: 1532052638
                sign: 778af1ceb548dada872af09ffeed3633
            }
            </code>
        </pre>
        <br />
        <p>
            Response Example:
        </p>
        <pre>
            <code>
            <b>Wrong password response:</b>
            {
                "code": "200",
                "msg": "success",
                "data": {
                    "status": "0",
                    "login_id": "99SM20018"
                }
            }

            <b>Success response:</b>
            {
                "code": "200",
                "msg": "success",
                "data": {
                    "status": "1",
                    "Auth_Token": "20426117-967A-44CF-9015-747CE8468EED"
                 }
            }
            </code>
        </pre>
        <br />
        <p>List of response status:</p>
        <p>"code" return:-<br />        <span style="margin:0 20px 20px">• 200 - Success</span><br /><br />        "status" in data block return:-<br />        <span style="margin:0 20px 20px">• 0 - Failed</span><br />        
        <span style="margin:0 20px 20px">• 1 - Success</span>    
        </p><br /> 
        <p><i>Note:</i></p>
        <ul>
            <li style="margin-bottom:10px">
                Login_password:MD5 value of the clear login password provided by Paydibs<br />
                Eg. MD5(112233) => d0970714757783e6cf17b26fb8e2298f
            </li>
            <li style="margin-bottom:10px">Time is Unix timestamp format</li>
            <li style="margin-bottom:10px">Sign refer to <a href="#" style="text-decoration:underline;font-weight:bold">1.3 Sign Regulation</a></li>
            <li style="margin-bottom:10px">Please refer to <a href="#" style="text-decoration:underline;font-weight:bold">1.4 Response Status</a> for more status description</li>
        </ul>
   </div>
</asp:Content>
