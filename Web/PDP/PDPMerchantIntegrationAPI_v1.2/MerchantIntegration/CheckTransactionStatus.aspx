﻿<%@ Page Title="8. CHECK TRANSACTION STATUS" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CheckTransactionStatus.aspx.cs" Inherits="PDPMerchantIntegrationAPI_v1._2.MerchantIntegration.CheckTransactionStatus" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <div style="margin:20px 10px;">
        <p>
            Description:<br />
            <span style="margin:0 20px 20px">• Check transaction status</span>
        </p>
        <p>
            Request URL:<br />
            <span style="margin:0 20px 20px">• http://xx.com//version1/payment/check-status</span>
        </p>
        <p>
            Request Method:<br />
            <span style="margin:0 20px 20px">• POST</span>
        </p>
        <p>Request Parameter:</p>
        <pre>
            <code>
            header:
            {
                Content-Type: application/x-www-form-urlencoded
                Key: Tpa3b34Mulw98WW
                Auth-Token: 20426117-967A-44CF-9015-747CE8468EED               
            }

            body:
            {
                time: 1532052638 //Unix timestamp
                sign: 2472bd90.… //Refer section 1.3 Sign Regulation
                integrator_id: 1
                trans_id: 20190115034950201002
            }
            </code>
        </pre>
        <br />
        <p>Response Example:</p>
        <pre>
            <code>
            {
                 "code": "200",
                 "msg": "success",
                 "data": {
                 "status": "202",
                 "msg": "Waiting Payment.",
                 "trans_id": "20190115034950201002",
                 "pay_status": "W"
                //P or W- In process/Waiting, R-Reversal, V-Void, D-Done, C-Cancel
            }
            </code>
        </pre>
        <br />
        <p>List of response status:</p>
        <p>"status" return:-<br />        <span style="margin:0 20px 20px">• 200 - Success</span><br />
        <span style="margin:0 20px 20px">• 201 - Cancel Payment</span><br />
        <span style="margin:0 20px 20px">• 202 - Waiting Payment</span><br />
        <span style="margin:0 20px 20px">• 203 - Reversal Payment / Void Payment</span><br />
        <span style="margin:0 20px 20px">• 204 - Payment Expired</span><br /><br />        "pay_status" in data block return:-<br />        <span style="margin:0 20px 20px">• P or W - In Process / Waiting</span><br />
        <span style="margin:0 20px 20px">• R - Reversal</span><br />
        <span style="margin:0 20px 20px">• V - Void</span><br />
        <span style="margin:0 20px 20px">• D - Done</span><br />
        <span style="margin:0 20px 20px">• C - Cancel</span>
        </p>
        <br />
        <p><i>Note:</i></p>
        Please refer to <a href="#" style="text-decoration:underline;font-weight:bold">1.4 Response Status</a> for more status description
    </div>
</asp:Content>