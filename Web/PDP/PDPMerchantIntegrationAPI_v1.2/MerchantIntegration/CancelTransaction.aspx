﻿<%@ Page Title="7. CANCEL TRANSACTION" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CancelTransaction.aspx.cs" Inherits="PDPMerchantIntegrationAPI_v1._2.MerchantIntegration.CancelTransaction" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <div style="margin:20px 10px;">
        <p>
            Description:<br />
               <span style="margin:0 20px 20px">• Cancel transaction</span>
        </p>
        <p>
            Request URL:<br />
                <span style="margin:0 20px 20px">• http://xx.com//version1/payment/cancel-trans</span>
        </p>
        <p>
            Request Method:<br />
                <span style="margin:0 20px 20px">• POST</span>
        </p>
       <p>Request Parameter: </p>
        <pre>
            <code>
            header:
            {
                Content-Type: application/x-www-form-urlencoded
                Key: Tpa3b34Mulw98WW
                Auth-Token: 20426117-967A-44CF-9015-747CE8468EED
            }

            body:
            {
                time: 1532052638 //Unix timestamp
                sign: 1956919a8…. //Refer section 1.3 Sign Regulation
                integrator_id: 1
                trans_id: 20190115034950201002
            }
            </code>
        </pre>
        <br />
        <p>Response Example: </p>
        <pre>
            <code>
            {
                "code": "200",
                "msg": "success",
                "data": {
                        'msg'=>'success'
                        'status'=>'1' //1=Success, 3=Not found, 4=Void not allow
                    }
             }
            </code>
        </pre>
        <br />
        <p>List of response status:</p>
        <p>"code" return:-<br />        <span style="margin:0 20px 20px">• 200 - Success</span><br /><br />        "status" in data block return:-<br />        <span style="margin:0 20px 20px">• 1 - Success</span><br />
        <span style="margin:0 20px 20px">• 3 - Not found / trans_id not found</span><br /> 
        <span style="margin:0 20px 20px">• 4 - Void not allow</span>
        <span style="margin:0 20px 20px">• 5 - Transaction not allow to cancel</span>
        </p>
        <br />
        <p><i>Note:</i></p>
        Please refer to <a href="#" style="text-decoration:underline;font-weight:bold">1.4 Response Status</a> for more status description
    </div>
</asp:Content>
