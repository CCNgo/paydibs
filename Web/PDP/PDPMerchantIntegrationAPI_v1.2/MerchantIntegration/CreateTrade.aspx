﻿<%@ Page Title="4. CREATE TRADE" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CreateTrade.aspx.cs" Inherits="PDPMerchantIntegrationAPI_v1._2.MerchantIntegration.CreateTrade" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <div style="margin:20px 10px;">
        <p>
            Description:<br />
            <span style="margin:0 20px 20px">• Create trade / payment</span>
        </p>
        <p>
            Request URL:<br />
            <span style="margin:0 20px 20px">• http://xx.com/version1/payment/create-trade</span>
        </p>
        <p>
            Request Method:<br />
            <span style="margin:0 20px 20px">• POST</span>
        </p>
        <p>Request Parameter:</p>
        <pre>
            <code>
                header:
                {
                    Content-Type: application/x-www-form-urlencoded
                    Key: Tpa3b34Mulw98WW
                    Auth-Token: 20426117-967A-44CF-9015-747CE8468EED
                }

                body:
                {
                    time: 1532052638 //Unix timestamp
                    sign: 67e4432b……. //Refer section 1.3 Sign Regulation
                    integrator_id: 1
                    amount: 2.00
                    trx_id: 967451
                    pay_type: MSU
                }
            </code>
        </pre>
        <br />
        <p>Response Example:</p>
        <pre>
            <code>
            {
                "code": "200",
                "msg": "success",
                "data": {
                    "trans_id": "20190115034950201002",
                    "pay_type": "MSU",
                    "amount": "2.00",
                    "integrator_id": "1"
                 }
            }
            </code>
        </pre>
         <br />
        <p>List of response status:</p>
        <p>"code" return:-<br />        <span style="margin:0 20px 20px">• 200 - Success</span><br />
        <span style="margin:0 20px 20px">• 204 - Failed</span><br />
        </p>
    </div>
    
</asp:Content>
