﻿<%@ Page Title="1. API REFERENCE" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Ah4PIReference.aspx.cs" Inherits="PDPMerchantIntegrationAPI_v1._2.MerchantIntegration.APIReference" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <div style="margin:20px 10px;">
        <h4><b>1.1 <span style="margin-left:5px">URL</span></b></h4>
        <div style="margin:2px 35px 35px ">
            Testing: https://merchanttest.paydibs.com/version1/<br />
            Live: https://mcashbizapi.paydibs.com/version1/<br />
            URL will be differed when there is new version release.<br />
        </div>

        <h4><b>1.2 <span style="margin-left:5px">HTTP Header details</span></b></h4>
        <div style="margin:2px 35px 35px">
        <ol style="margin-left:-25px;">
            <li>“Content-type”: “application/x-www-form-urlencoded; charset=utf-8”</li>
            <li>“Key”: “Key provided by Paydibs”</li>
            <li>“Auth-Token”: “Token returned from Get Auth Token Response”</li>
        </ol>
        <p> Note: Exclude “Auth-Token” in Get Auth Token Request</p>
        </div>
        <h4><b>1.3<span style="margin-left:5px"> Sign Regulation</span></b></h4>
        <div style="margin:2px 35px 35px">
            <p>
                Sign source string should be formed as follow.<br />
                Eg: B=h, A=z, C=w<br />
                MD5(zhw)<br />
            </p>
            <p>
                Fix parameters: sign, time<br />
                Hash algorithm： MD5<br />
            </p>
            <p>
                MD5 hashing:<br />
                For example, B=h, A=z, C=w, the sequence of parameters name should be<br />
                ascending A=z, B=h, C=w.<br />
            </p>
            <p>
                When fix parameter(s) exist like sign, time, the Sign will be as follow.<br />
                Sign = MD5(z + h + w + time + API_SIGN_KEY)<br />
            </p>
            <p>
                MD5 PHP sample code just for reference only:
            </p>
            <pre>
                <code>
                protected function getSign($compare_arr)
                {
                    $compare_key = array_keys($compare_arr);
                    sort($compare_key);
                    $compare_sign = $request_sign = $request_time = '';

                    foreach ( $compare_key as $api_key => $api_value ){
                        if ( $api_value == 'sign' ){
                            $request_sign = $compare_arr[$api_value] ;
                        }elseif ( $api_value == 'time' ){
                            $request_time = $compare_arr[$api_value] ;
                        }else{
                            $compare_sign .= $compare_arr[$api_value] ;
                        }
                    }

                    $compare_sign .= $request_time . API_SIGN_KEY ;
                    $sign = md5($compare_sign);
                    return $sign;
                }
            </code>
            </pre>   
        </div>
        <h4><b>1.4<span style="margin-left:5px">Message Status</span></b></h4>
        <div style="margin:2px 35px 35px">
            <pre>
                <code>
                {“code”:”200”, “msg”:”success”, “data”:{}}  
                </code>
            </pre>
            code = 200 - Success<br />
            code = 204 - Failed<br />
            <br />
            <pre>
                <code>
                {“code”:”400”, “msg”:”Incorrect parameter”}
                </code>
            </pre>
            code = 400 - Incorrect parameter<br />
            code = 401 - login failed<br />
            code = 402 - Request timeout<br />
            code = 403 - Incorrect header<br />
            code = 500 - Failed to connect<br />
        </div>
        <h4><b>1.5<span style="margin-left:5px">Order/Payment Status</span></b></h4>
        <div style="margin:2px 35px 35px">

            To capture the exact status of an order or payment, always refer to <b>&quot;code&quot;</b> and <b>&quot;data&quot;</b> block in response message
            <br />
            <pre>
                Example 1:
                <code>
                    "data": {
                    ...
                    "user_order_status": "S"
                }
                </code>
                Example 2:
                <code>
                    "data": {
                     ...
                     "pay_status": "W"
                    }
                </code>
            </pre>

        </div>
    </div>
</asp:Content>
