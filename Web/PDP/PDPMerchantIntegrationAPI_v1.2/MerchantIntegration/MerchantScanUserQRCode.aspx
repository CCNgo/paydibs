﻿<%@ Page Title="6. MERCHANT SCAN USER QRCODE" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MerchantScanUserQRCode.aspx.cs" Inherits="PDPMerchantIntegrationAPI_v1._2.MerchantIntegration.MerchantScanUserQRCode" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <div style="margin:20px 10px;">
        <p>
            Description:<br />
            <span style="margin:0 20px 20px">• Merchant scan user’s wallet QRCode</span>
        </p>
        <p>
            Request URL:<br />
            <span style="margin:0 20px 20px">• http://xx.com/version1/payment/payment</span>
        </p>
        <p>
            Request Method:<br />
            <span style="margin:0 20px 20px">• POST</span>
        </p>
        <p>Request Parameter:</p>
        <pre>
            <code>
            header:
            {
                Content-Type: application/x-www-form-urlencoded
                Key: Tpa3b34Mulw98WW
                Auth-Token: 20426117-967A-44CF-9015-747CE8468EED
            }

            body:
            {
                time: 1532052638 //Unix timestamp
                sign: 66fb95…… //Refer section 1.3 Sign Regulation
                integrator_id: 1
                user_qr_code: APP-USER-154140679702143518826-PAYDIBS //QRCode string
                amount: 10.00
                trans_id: 20190115034950201002
            }
            </code>
        </pre>
        <br />
        <p>Response Example:</p>
        <pre>
            <code>
            <b>Success response:</b>
            {
            "code": "200",
            "msg": "success",
            "data": {
                "message": "Success payment.",
                "status": "200",
                "unique_id": "0000026",
                "user_qr_code": " APP-USER-154140679702143518826-PAYDIBS",
                "trans_id": "20190115034950201002",
                "transaction_date": "2018-03-05 15:51:12.510",
                "user_order_id": "20180305023000000042",
                "user_order_mobile": "7245",
                "user_order_name": "",
                "user_order_status": "S"  // S-Success, P-Pending
                }
            }

            <b>Failed response:</b>
            {
                "code": "204",
                "msg": "fail",
            }
            </code>
        </pre>
        <br />
        <p>List of response status:</p>
        <p>"status" return:-<br />        <span style="margin:0 20px 20px">• 200 - Success</span><br />
        <span style="margin:0 20px 20px">• 203 - Failed by integrator</span><br />
        <span style="margin:0 20px 20px">• 204 - Invalid Token / Payment Failed / Connection Failed / Invalid trans_id</span><br /><br />        "user_order_status" in data block return:-<br />        <span style="margin:0 20px 20px">• S - Success</span><br />        
        <span style="margin:0 20px 20px">• P - Pending</span>    
        </p>
        <br />
        <p><i>Note:</i></p>
        Please refer to <a href="#" style="text-decoration:underline;font-weight:bold">1.4 Response Status</a> for more status description
    </div>
</asp:Content>
