﻿<%@ Page Title="5. QRCODE REQUEST" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="QRCodeRequest.aspx.cs" Inherits="PDPMerchantIntegrationAPI_v1._2.MerchantIntegration.QRCodeRequest" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<h2><%: Title %></h2>
    <div style="margin:20px 10px;">
        <p>
            Description:<br />
            <span style="margin:0 20px 20px">• QRCode request</span>
        </p>
        <p>
            Request URL:<br />
            <span style="margin:0 20px 20px">• http://xx.com//version1/payment/generate</span>
        </p>
        <p>
            Request Method:<br />
            <span style="margin:0 20px 20px">• POST</span>
        </p>
        <p>Request Parameter:</p>
        <pre>
            <code>
            header:
            {
                Content-Type: application/x-www-form-urlencoded
                Key: Tpa3b34Mulw98WW
                Auth-Token: 20426117-967A-44CF-9015-747CE8468EED
            }

            body:
            {
                time: 1532052638 //Unix timestamp
                sign: 67e4432…… //Refer section 1.3 Sign Regulation
                integrator_id: 1
                amount: 10.00
                trans_id: 20190115034950201002 //trans_id from create payment response
            }
            </code>
        </pre>
        <br />
        <p>Response Example:</p>
        <pre>
            <code>
            {
                "code": "200",
                "msg": "success",
                "data": {
                    "qrcode": "APP-USER-154140679702143518826-PAYDIBS", //QRCode string
                    "qrcode_format": "dynamic", //QRCode type – dynamic or static
                    "trans_id": "20190115064430322000",
                    "wait_time": "50"
                }
            }
            </code>
        </pre>
        <br />
        <p>List of response status:</p>
        <p>"code" return:-<br />        <span style="margin:0 20px 20px">• 200 - Success</span><br />
        <span style="margin:0 20px 20px">• 203 - Get QR Failed</span><br />
        <span style="margin:0 20px 20px">• 204 - QRCode not support / Invalid trans_id</span>
        </p>
    </div>
</asp:Content>
