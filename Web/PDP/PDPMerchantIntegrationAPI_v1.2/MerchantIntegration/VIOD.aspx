﻿<%@ Page Title="10. VOID" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="VIOD.aspx.cs" Inherits="PDPMerchantIntegrationAPI_v1._2.MerchantIntegration.VIOD" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <div style="margin:20px 10px;">
        <p>
            Description:<br />
            <span style="margin:0 20px 20px">• Void a success transaction</span>
        </p>
        <p>
            Request URL:<br />
            <span style="margin:0 20px 20px">• http://xx.com//version1/payment/process-void</span>
        </p>
        <p>
            Request Method:<br />
            <span style="margin:0 20px 20px">• POST</span>
        </p>
        <p>Request Parameter:</p>
        <pre>
            <code>
            header:
            {
                Content-Type: application/x-www-form-urlencoded
                Key: Tpa3b34Mulw98WW
                Auth-Token: 20426117-967A-44CF-9015-747CE8468EED
            }

            body:
            {
                time: 1532052638 //Unix timestamp
                sign: 41e4415…. //Refer section 1.3 Sign Regulation
                trans_id: 20190115034950201002
                void_pin: d0970714757783e6cf17b26fb8e2298f //MD5(login_password)
                integrator_id:1
            }
            </code>
        </pre>
        <br />
        <p>Response Example:</p>
        <pre>
            <code>
            {
            "code": "200",
            "msg": "success",
            "data": {
                    "status": "3",
                    //3=Transaction not found, 5=Timeout, 6=Wrong password, 7=Void failed, 8=Error, 1=Success
                    "transaction_id": "20190115034950201002"
                }
            }
            </code>
        </pre>
    </div>
</asp:Content>