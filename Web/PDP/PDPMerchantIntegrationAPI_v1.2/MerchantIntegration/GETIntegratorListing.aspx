﻿<%@ Page Title="3. GET INTEGRATOR LISTING" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="GETIntegratorListing.aspx.cs" Inherits="PDPMerchantIntegrationAPI_v1._2.MerchantIntegration.GETIntegratorListing" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <div style="margin:20px 10px;">
        <p>
             Description<br />
             <span style="margin:0 20px 20px">• Get integrator listing</span>
        </p>
        <p>
             Request URL:<br />
            <span style="margin:0 20px 20px">• http://xx.com/version1/payment/integrator-master</span>
        </p>
       <p>
            Request Method:<br />
            <span style="margin:0 20px 20px">• POST</span>
       </p>
       <p>Request Parameter: </p>
       <pre>
           <code>
            header:
            {
                Content-Type: application/x-www-form-urlencoded
                Key: Tpa3b34Mulw98WW
                Auth-Token: 20426117-967A-44CF-9015-747CE8468EED
            }

            body:
            {
                Time :1532052638 //Unix timestamp
                sign: 67e4432…….. //Refer section 1.3 Sign Regulation
                pay_type: MSU // MSU-Merchant Scan User OR USM-User Scan Merchant
            }
           </code>
       </pre>        <br />
        <p>Response Example:</p>
        <pre>
            <code>
            {
                "code": "200",
                "msg": "success",
                "data": [
                     {
                        "id": "1",
                        "integrator_name": "MCash",
                        "integrator_logo_link":
                        "https://mcashbizapi.paydibs.com/img/logo/mcash_paydibs.png",
                        "active": "A", //T-Test, A-Actual
                        "integrator_translog_link":"",
                     },
                     {
                        "id": "3",
                        "integrator_name": "BOOST",
                        "integrator_logo_link":
                        "https://mcashbizapi.paydibs.com/img/logo/boost_paydibs.png",
                        "active": "A",
                        "integrator_translog_link":"",
                     },
                     {
                        "id": "6",
                        "integrator_name": "TnG",
                        "integrator_logo_link":
                        "https://mcashbizapi.paydibs.com/img/logo/tng_paydibs.png",
                        "active": "A",
                        "integrator_translog_link":"",
                     }
                 ]
            }
            </code>
        </pre>
        <br />
        <p>List of response status:</p>
        <p>"code" return:-<br />        <span style="margin:0 20px 20px">• 200 - Success</span><br /></p>
    </div>
</asp:Content>
