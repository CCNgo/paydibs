﻿<%@ Page Title="Revision History" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="PDPMerchantIntegrationAPI_v1._2._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
     <h2><%: Title %>.</h2>
    <p>This section details the changes that were made to this document.</p>
               <table class="table table-bordered">
                   <thead style="background-color:gold; font-weight:bold">
                       <tr>
                           <td>#</td>
                           <td>Version</td>
                           <td>Date</td>
                           <td>Description</td>
                       </tr>
                   </thead>
                   <tbody>
                       <tr>
                           <td>1</td>
                           <td>1</td>
                           <td>5 Nov 2018</td>
                           <td>API Creation</td>
                       </tr>
                       <tr>
                           <td>2</td>
                           <td>1.1</td>
                           <td>11 Feb 2019</td>
                           <td>Updated to latest process flow</td>
                       </tr>
                       <tr>
                           <td>3</td>
                           <td>1.2</td>
                           <td>20 Mar 2019</td>
                           <td>Revised API</td>
                       </tr>
                       <tr>
                           <td>4</td>
                           <td>1.3</td>
                           <td>3 Oct 2019</td>
                           <td>Added list of response status</td>
                       </tr>
                    </tbody>
               </table>

</asp:Content>
