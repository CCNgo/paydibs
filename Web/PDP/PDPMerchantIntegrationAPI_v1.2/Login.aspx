﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="PDPMerchantIntegrationAPI_v1._2.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Merchant Integration V1.2</title>
    <link rel="stylesheet" href="Content/main.css" type="text/css" />
</head>
<body class="be-splash-screen">
    <form id="form1" runat="server">
        <div class="be-wrapper be-login">
            <div class="be-content">
                <div class="main-content container-fluid">
                    <div class="splash-container">
                        <div role="alert" class="alert alert-contrast alert-dismissible" id="dl_ErrorDialog">
                            <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
                            <div class="message" id="dl_message">
                            </div>
                        </div>
                        <div class="card card-border-color card-border-color-primary">
                            <div class="card-header"> 
                                <img src="paydibs_logo-07.png" alt="logo" width="250" />
                            </div>
                            <div> <span class="splash-description">Please enter password to log in.</span></div>
                            <div class="card-body">
                                <div class="form-group">
                                    <asp:TextBox runat="server" ID="Password" TextMode="Password" CssClass="form-control" />
                                    <asp:PlaceHolder runat="server" ID="ErrorMessage" Visible="false">
                                    <p class="text-danger">
                                        <asp:Literal runat="server" ID="FailureText" />
                                    </p>
                                </asp:PlaceHolder>
                                </div>
                                <div class="form-group login-submit">
                                    <asp:Button runat="server" OnClick="LogIn" Text="Log in" CssClass="btn btn-primary btn-xl" />
                                </div>
                                <div style="margin:5em 0 1em">
                                    <div style="text-align:center; font-size:12px"><i>Merchant Integration V1.2</i></div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>        </div>
    </form>
    <div style="margin-top:30px;">&nbsp;</div>
    <footer>
        <p style="line-height: 1.42857143;text-align:center;font-size: 12px;">Copyright &copy; <%: DateTime.Now.Year %> Paydibs Sdn. Bhd. All Rights Reserved.</p>
    </footer>
</body>
</html>
