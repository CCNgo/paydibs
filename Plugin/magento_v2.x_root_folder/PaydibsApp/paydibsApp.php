<?php
use Magento\Framework\FactoryInterface;
use Magento\Payment\Observer;
use Magento\Sales\Model\Order\Email\Sender\OrderCommentSender;
use Magento\Framework\Exception;
use Magento\Checkout\Model\Session;
use Magento\Sales\Model\Order;

class paydibsApp extends \Magento\Framework\App\Http implements \Magento\Framework\AppInterface {
    
	protected $helperData;
	protected $request;
	protected $urlType;
	protected $_debug;
	protected $_order;
	protected $_OrderCommentSender;
	protected $_invoiceService;
	protected $_transaction;
	protected $invoiceSender;
	
	protected function initApp(){
		// Setting area code to remove area code exceptions
		$this->_objectManager->get('Magento\Framework\App\State')->setAreaCode(\Magento\Framework\App\Area::AREA_GLOBAL);
		
		$this->helperData = $this->_objectManager->create('\Paydibs\PaymentMethod\Helper\Data');
		$this->_debug = $this->helperData->getGeneralConfig('debug');
		$this->request = $this->_objectManager->create('\Magento\Framework\App\Request\Http');
		$this->urlType = $this->request->getParam('urlType');
		$this->_OrderCommentSender = $this->_objectManager->create('Magento\Sales\Model\Order\Email\Sender\OrderCommentSender');
		
		$this->_invoiceService = $this->_objectManager->create('\Magento\Sales\Model\Service\InvoiceService');
		$this->_transaction = $this->_objectManager->create('\Magento\Framework\DB\Transaction');
		$this->invoiceSender = $this->_objectManager->create('\Magento\Sales\Model\Order\Email\Sender\InvoiceSender');
	}
	
	protected function add_log($message){
		if($this->_debug){
			$this->helperData->add_log("paydibsApp -> ".$this->urlType." -> ".$message);
		}
	}
	
	protected function createOrderInvoice(){
		if($this->_order->canInvoice() && !$this->_order->hasInvoices() ) {
			$invoice = $this->_invoiceService->prepareInvoice($this->_order);
			$invoice->register();
			$invoice->setState($invoice::STATE_PAID);
			$invoice->save();
			$transactionSave = $this->_transaction->addObject($invoice)->addObject($invoice->getOrder());
			$transactionSave->save();
			$this->invoiceSender->send($invoice);
			//send notification code
			$this->_order->addStatusHistoryComment(__('Notified customer about invoice #%1.', $invoice->getId()))->setIsCustomerNotified(true)->save();
		}
	}
	
	/*protected function orderStatusUpdate($status=NULL){
		//log debug
		$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/paydibs_debug_'.date('Ymd').'.log');
		$logger = new \Zend\Log\Logger();
		$logger->addWriter($writer);
		
		$logger->info(ucfirst($this->urlType).' Get Order: ', $this->request->getParams());
		
		// will update the status only if order status is 'pending_payment' and $status is not null
		if(!is_null($status) && "pending_payment" == $this->_order->getStatus()){
			$vars = $this->request->getParams();
			$this->_order->setStatus($vars['PTxnStatus'] = 0 ? 'processing' : 'pending_payment');
			$bSentEmail = true;
			$comment = "[Payment Processed by Paydibs][Order ID: ". $vars['MerchantOrdID'] ."][Amount:". $vars['MerchantCurrCode'] . $vars['MerchantTxnAmt']."]";
			
			if($vars['PTxnStatus']=='1'){
				if($vars['PTxnMsg']!= "Buyer cancelled"){
					$comment .= "[Transaction ID:" . $vars['PTxnID'] . "]" . " [Payment method: " . $vars['Method'] . "]";
				}
				if(!$this->helperData->getGeneralConfig('fail_payment_email')){
					$bSentEmail = false;
				}
			}else{
				$comment .= " [Transaction ID:" . $vars['PTxnID'] . "]" . " [Payment method: " . $vars['Method'] . "]";
			}
			
			$history = $this->_order->addStatusHistoryComment($comment, false);
			
			$logger->info('Return Comment: '. $comment);
			
			if($bSentEmail){
				$history->setIsCustomerNotified(true);
			}
			
			$this->_order->save();
			
			if($bSentEmail){
				$this->_OrderCommentSender->send($this->_order, true, $comment);
			}
			
			if($status==$this->helperData->getGeneralConfig('payment_success_status')){
				$this->createOrderInvoice();
			}
		}
		else{
			$logger->info('Return Comment: No Comment');
		}
	}*/
	
	protected function orderStatusUpdate($vars, $status){
		//log debug
		$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/paydibs_debug_'.date('Ymd').'.log');
		$logger = new \Zend\Log\Logger();
		$logger->addWriter($writer);
		
		$data = json_encode($vars);
		
		$logger->info(ucfirst($this->urlType).' Get Order: '. $data);
		
		foreach(json_decode($data) as $key=>$val){

			if(strpos($key,"{")!==false){
				$vars = $key;
				break;
			}
		}

		if (strstr($vars['MerchantTxnAmt'], '_')) {
			$MerchantTxnAmt = str_replace("_", ".", $vars['MerchantTxnAmt']);
		} else{
			$MerchantTxnAmt = $vars['MerchantTxnAmt'];
		}
		
		$orderId = $vars['MerchantOrdID'];
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$order = $objectManager->create('\Magento\Sales\Model\Order') ->load($orderId);
		
		$logger->info(ucfirst($this->urlType)." Current Status: ". $order->getStatus());
		
		$bSentEmail = true;
		
		//if( $order->getStatus() == 'pending_payment' &&  $vars['PTxnStatus'] == 0){
			
		$comment = "[".ucfirst($this->urlType)." Payment Processed by Paydibs][Order ID: " . $vars['MerchantOrdID'] . "] [Message: " . $vars['PTxnMsg'] . "] [Amount: ". $vars['MerchantCurrCode'] . $MerchantTxnAmt."] ";
		
		$method = (!empty($vars['Method'])) ? $vars['Method'] : "";
		
		if($vars['PTxnStatus']=='1'){
			if($vars['PTxnMsg']!= "Buyer cancelled"){
				$comment .= "[Transaction ID:" . $vars['PTxnID'] . "]" . " [Payment method: " . $method. "]";
			}
			if(!$this->helperData->getGeneralConfig('fail_payment_email')){
				$bSentEmail = false;
			}
		}else{
			$comment .= " [Transaction ID:" . $vars['PTxnID'] . "]" . " [Payment method: " . $method. "]";
		}
		
		if($vars['PTxnStatus'] == 0){
			$orderState = Order::STATE_PROCESSING;	
		}else{
			$orderState = Order::STATE_PENDING_PAYMENT;
		}
		
		
		
		$order->setState($orderState)->setStatus($orderState);
		$order->addStatusToHistory($order->getStatus(), $comment)->setIsCustomerNotified(true);
		
		$order->save();
		
		if($bSentEmail){
			$this->_OrderCommentSender->send($order, true, $comment);
		}
			
		if($status==$this->helperData->getGeneralConfig('payment_success_status')){
			if($order->canInvoice() && !$order->hasInvoices() ) {
				$invoice = $this->_invoiceService->prepareInvoice($order);
				$invoice->register();
				$invoice->setState($invoice::STATE_PAID);
				$invoice->save();
				$transactionSave = $this->_transaction->addObject($invoice)->addObject($invoice->getOrder());
				$transactionSave->save();
				$this->invoiceSender->send($invoice);
				//send notification code
				$order->addStatusHistoryComment(__('Notified customer about invoice #%1.', $invoice->getId()))->setIsCustomerNotified(true)->save();
				$comment .= '[ Notified customer about invoice #%1. : '. $invoice->getId() .' ]';
			}
		}	
		
		
		//}else{
		//	$comment = "Nothing to do!";
		//}

		$logger->info("Callback Comment: ". $comment);
		
	}
	
	// the callback and return are handeled here
	public function launch()
    {
		//log debug
		$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/paydibs_debug_'.date('Ymd').'.log');
		$logger = new \Zend\Log\Logger();
		$logger->addWriter($writer);
		
		$logger->info('Paydibs callback and return are handled here');
		
		try{
			
			$this->initApp();
		
			// get all request params
			$vars = $this->request->getParams();
			$this->add_log('vars: '.print_r($vars,1));
			
			if(strtolower($this->urlType)=='callback'){
				$data = json_encode($vars);	
				foreach(json_decode($data) as $key=>$val){
		
					if(strpos($key,"{")!==false){
						$vars = $key;
						break;
					}
				}
				
				//json_decode data
				$vars = json_decode($vars,true);
			}
			$logger->info(file_get_contents('php://input'));
			$logger->debug(strtolower($this->urlType) =='callback' ? 'Callback RURL:' : 'Return URL' ,$vars);
			
			if(isset($vars['MerchantOrdID'])){
				
				// instanciate order object
				$this->_order = $this->_objectManager->create('\Magento\Sales\Model\Order');
				// load order by ID
				if(is_numeric($vars['MerchantOrdID'])){
					// in case of logged in user
					$this->_order->loadByAttribute('quote_id',$vars['MerchantOrdID']);
				}
				else{
					// in case of guest checkout
					$checkoutSession = $this->_objectManager->create('\Magento\Checkout\Model\Session');
					$this->_order = $checkoutSession->getLastRealOrder();
				}
				
				if(strtolower($this->urlType)=='ordershipping'){
					$output = array(
										'BaseShipping'=>number_format($this->_order->getBaseShippingAmount(),2,'.',''),
										'DisplayShipping'=>number_format($this->_order->getShippingAmount(),2,'.',''),
										'BaseDiscount'=>number_format($this->_order->getBaseDiscountAmount(),2,'.',''),
										'DisplayDiscount'=>number_format($this->_order->getDiscountAmount(),2,'.',''),
										'BaseGrandTotal'=>number_format($this->_order->getBaseGrandTotal(),2,'.',''),
										'DisplayGrandTotal'=>number_format($this->_order->getGrandTotal(),2,'.','')
									);
					echo json_encode($output);
				}
				else{
					// Proceed only if TransactionType = PAY
					if($vars['TxnType']=="PAY"){
						$logger->info("Proceed only if TransactionType = PAY");
						
						$hash2 = $this->calculate_hash2($vars);
						
						if (strcasecmp($hash2,$vars['Sign'])!=0) {//Different hash between what we calculate and the hash sent by the payment platform so we do not do anything as we consider that the notification doesn't come from the payment platform.
							$logger->info("Hash2 error gateway (".$vars['Sign'].") - Calculated (".$hash2.")");
							$this->add_log("Hash2 error gateway (".$vars['Sign'].") - Calculated (".$hash2.")");
							$this->p_arr("Hash2 error gateway (".$vars['Sign'].") - Calculated (".$hash2.")");
						}
						else{
						
							if($vars['PTxnStatus']=='0') // Success
							{
								$this->orderStatusUpdate($vars, $this->helperData->getGeneralConfig('payment_success_status'));
								$this->add_log('Order Placed');
								if(strtolower($this->urlType)=='return'){
									//$this->orderStatusUpdate($this->helperData->getGeneralConfig('payment_success_status'));
									$logger->info("Order Placed [ Order ID: " . $vars['MerchantOrdID'] . " - MerchantPaymentID: " . $vars['MerchantPymtID'] . " - Status Message: " .$vars['PTxnMsg'] ."]\n\n");
									header("Location: ".$this->helperData->getBaseURL().'paydibsgw?MerchantOrdID='.$vars['MerchantOrdID'].'&gwresp=success');
									die();
								}//else{
									//$logger->info('Go to callbackOrderStatusUpdate');
									
								//}
							}
							elseif($vars['PTxnStatus']=='1') //Return code different from success so we set the "invalid" status to the order
							{
								if($vars['PTxnMsg']=='Buyer cancelled') //Buyer clicked cancel payment so don't treat it as failed transaction
								{
									$this->orderStatusUpdate($vars, $this->helperData->getGeneralConfig('payment_cancel_status'));
									$this->add_log('Order Cancelled');
									if(strtolower($this->urlType)=='return'){
										$logger->info("Order Cancelled [ Order ID: " . $vars['MerchantOrdID'] . " - MerchantPaymentID: " . $vars['MerchantPymtID'] . " - Status Message: " .$vars['PTxnMsg'] ."]\n\n");
										header("Location: ".$this->helperData->getBaseURL().'paydibsgw?MerchantOrdID='.$vars['MerchantOrdID'].'&gwresp=cancelled');
										die();
									}
								}
								else{
									$this->orderStatusUpdate($vars, $this->helperData->getGeneralConfig('payment_fail_status'));
									$this->add_log('Payment Failed');
									if(strtolower($this->urlType)=='return'){
										$logger->info("Payment Failed [ Order ID: " . $vars['MerchantOrdID'] . " - MerchantPaymentID: " . $vars['MerchantPymtID'] . " - Status Message: " .$vars['PTxnMsg'] ."]\n\n");
										header("Location: ".$this->helperData->getBaseURL().'paydibsgw?MerchantOrdID='.$vars['MerchantOrdID'].'&gwresp=failed');
										die();
									}
								}
								
							}
							else // Pending response
							{
								$this->orderStatusUpdate($vars, $this->helperData->getGeneralConfig('payment_pending_status'));
								$this->add_log('Payment Pending');
								if(strtolower($this->urlType)=='return'){
									$logger->info("Payment Pending [ Order ID: " . $vars['MerchantOrdID'] . " - MerchantPaymentID: " . $vars['MerchantPymtID'] . " - Status Message: " .$vars['PTxnMsg'] ."]\n\n");
									header("Location: ".$this->helperData->getBaseURL().'paydibsgw?MerchantOrdID='.$vars['MerchantOrdID'].'&gwresp=pending');
									die();
								}
							}
							
							if(strtolower($this->urlType)=='callback'){
								print "OK"; // acknowledgement sent to payment gateway
								$logger->info("Callback Order Placed [ Order ID: " . $vars['MerchantOrdID'] . " - MerchantPaymentID: " . $vars['MerchantPymtID'] . " - Status Message: " .$vars['PTxnMsg'] ."]\n\n");
								$this->add_log('acknowledgement sent to payment gateway');
							}
						}
					}
					else{
						$logger->info('Invalid TransactionType i.e. '.$vars['TxnType']);
						$this->add_log('Invalid TransactionType i.e. '.$vars['TxnType']);
					}
				}
				
			}
			else{
				$this->p_arr('"MerchantOrdID" is missing');
				$this->add_log('"MerchantOrdID" is missing');
			}
			
		}
		catch(\Exception $e){
			$this->add_log('Exception: '.print_r($e->getErrors(),1));
		}
        
        
		//the method must end with this line
        return $this->_response;
    }
	
	protected function calculate_hash2($vars){
		$clear_string = $this->helperData->getGeneralConfig('hashpass');
		
		$hashStrKeysOrder = array (
			'MerchantPassword',
			'MerchantID',
			'MerchantPymtID',
			'PTxnID',
			'MerchantOrdID',
			'MerchantTxnAmt',
			'MerchantCurrCode',
			'PTxnStatus',
			'AuthCode',
		);

		//Here we construct the hash string according to the payment gateway's requirements
		foreach ($hashStrKeysOrder as $key)
		{
			if(isset($vars[$key])){
				if (strstr($vars['MerchantTxnAmt'], '_')) {
					$vars['MerchantTxnAmt'] = str_replace("_", ".", $vars['MerchantTxnAmt']);
				} 
				$clear_string .= $vars[$key];
			}else{
				$clear_string .= '';
			}
		}
		//$logger->info($clear_string);
		
		$this->add_log("clear_string: $clear_string");
		$hash2 =  hash('SHA512', $clear_string);
		
		return $hash2;
	}
	
	public function p_arr($arr, $prefix=NULL){
		if(is_null($prefix)){
			echo "<pre>".print_r($arr,1)."</pre>";
		}
		else{
			echo "<pre>$prefix: ".print_r($arr,1)."</pre>";
		}
	}

    public function catchException(\Magento\Framework\App\Bootstrap $bootstrap, \Exception $exception)
    {
        return false;
    }

}
?>