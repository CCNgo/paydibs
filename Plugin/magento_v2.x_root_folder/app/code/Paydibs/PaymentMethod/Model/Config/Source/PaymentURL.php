<?php
/**
 * Copyright � 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Used in creating options for Payment URL config value selection
 *
 */
namespace Paydibs\PaymentMethod\Model\Config\Source;

class PaymentURL implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
     public function toOptionArray()
    {
        return [['value' => 'https://dev.paydibs.com/PPGSG/PymtCheckout.aspx', 'label' => __('Test payment URL')], ['value' => 'https://payment.paydibs.com/PPG/PymtCheckout.aspx', 'label' => __('Production payment URL')]];
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return ['https://dev.paydibs.com/PPGSG/PymtCheckout.aspx' => __('Test payment URL'), 'https://payment.paydibs.com/PPG/PymtCheckout.aspx' => __('Production payment URL')];
    }
}
