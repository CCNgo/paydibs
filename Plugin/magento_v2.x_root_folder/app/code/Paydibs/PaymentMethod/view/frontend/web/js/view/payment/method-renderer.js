define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'paydibspayment',
                component: 'Paydibs_PaymentMethod/js/view/payment/method-renderer/paydibspayment'
            }
        );
        return Component.extend({});
    }
);