<?php
namespace Paydibs\PaymentMethod\Block;

class Terminal extends \Magento\Framework\View\Element\Template
{
	protected $helperData;
	protected $request;
	protected $_store;
	protected $debug_html;
	protected $_logger;
	
	/**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;
	
	public function __construct(
								\Magento\Framework\View\Element\Template\Context $context,
								\Paydibs\PaymentMethod\Helper\Data $helperData,
								\Magento\Framework\App\Request\Http $request,
								\Magento\Store\Api\Data\StoreInterface $store,
								\Psr\Log\LoggerInterface $logger,
								\Magento\Checkout\Model\Session $checkoutSession
								)
	{
		$this->helperData = $helperData;
		$this->request = $request;
		$this->_store = $store;
		$this->debug_html = "";
		$this->_logger = $logger;
		$this->_checkoutSession = $checkoutSession;
		parent::__construct($context);
	}
	
	public function _prepareLayout()
	{
		//set page title
		$gwresp = $this->getParam('gwresp');
		if(!is_null($gwresp) && $gwresp!=""){
			if("success"==$gwresp){
				$this->pageConfig->getTitle()->set(__("Payment Successful!"));
			}
			elseif("failed"==$gwresp){
				$this->pageConfig->getTitle()->set(__("Payment Failed!"));
			}
			elseif("pending"==$gwresp){
				$this->pageConfig->getTitle()->set(__("Payment Pending!"));
			}
			elseif("canceled"==$gwresp){
				$this->pageConfig->getTitle()->set(__("Payment Canceled!"));
			}
		}
		else{
			$this->pageConfig->getTitle()->set(__('Redirecting to Paydibs Checkout'));
		}
		
		return parent::_prepareLayout();
	} 
	
	/**
     * Get frontend checkout session object
     *
     * @return \Magento\Checkout\Model\Session
     */
    protected function _getCheckoutSession(){
		//log debug
		$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/'.date('Ymd').'.log');
		$logger = new \Zend\Log\Logger();
		$logger->addWriter($writer);
		
		$logger->info('_getCheckoutSession: ');
		
        return $this->_checkoutSession;
    }
	
	public function getPaydibsConfig($config){
		return $this->helperData->getGeneralConfig($config);
	}
	
	public function getPost($param, $default=NULL){
		return $this->request->getPost($param, $default);
	}
	
	public function getParam($param, $default=NULL){
		return $this->request->getParam($param, $default);
	}
	
	public function getParams($params, $default=NULL){ 
		return $this->request->getParams($params, $default);
	}
	
	public function paydibs_acceptable_locale(){
		list($first,$second) =  explode('_',$this->helperData->getCurrentLocale());
		return $first;
	}
	
	protected function calculateHashValue(&$pgw_params){
		$clearString	=	$this->getPaydibsConfig('hashpass');
		$hashStrKeysOrder = array (
			//'MerchantPassword',
			'TxnType',
			'MerchantID',
			'MerchantPymtID',
			'MerchantOrdID',
			'MerchantRURL',
			'MerchantTxnAmt',
			'MerchantCurrCode',
			'CustIP',
			'PageTimeout',
			'MerchantCallBackURL',
		);
		
		foreach($hashStrKeysOrder as $ind){
			$clearString	.=	$pgw_params[$ind];
		}
		
		//print_r($clearString);die();
		$pgw_params["Sign"]	=	hash('SHA512', $clearString);
	}
	
	public function content_controller(){
		
		$gwresp = $this->getParam('gwresp');
		$order_id = $this->getParam('MerchantOrdID');
		$content = "";
		if(!is_null($gwresp) && $gwresp!=""){
			if("success"==$gwresp){
				$content .= "<p>".__("Thanks for purchasing with us.")."</p>";
			}
			elseif("failed"==$gwresp){
				$content .= "<p>".__("Something went wrong. Your order is under review with us.")."</p>";
			}
			elseif("pending"==$gwresp){
				$content .= "<p>".__("Your order status is pending with us.")."</p>";
			}
			elseif("canceled"==$gwresp){
				$content .= "<p>".__("The order was canceled by you.")."</p>";
			}
			$content .= "	<center>
								<a class='Paydibs_btn' href='".$this->helperData->getBaseURL()."sales/order/view/order_id/$order_id'>View Order</a>
							</center>";
		}
		else{
			$content .= $this->PaydibsPaymentForm();
		}
		
		return $content;
	}
	
	public function PaydibsPaymentForm(){
		//log debug
		$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/paydibs_debug_'.date('Ymd').'.log');
		$logger = new \Zend\Log\Logger();
		$logger->addWriter($writer);
		
		$CurrencyCode = '';
		$amount = 0;
		$shipping = 0;
		$discount = 0;

		$logger->info("Page: Terminal.php");
		$logger->info(file_get_contents('php://input'));
		
		if($this->getPaydibsConfig('currency_type')=="Base"){
			$CurrencyCode = $this->getPost('BaseCurrencyCode');
			$amount = number_format($this->getPost('BaseAmount'), 2, '.','');
			$shipping = $this->getPost('BaseShipping');
			$discount =  $this->getPost('BaseDiscount');
			$logger->info("Base Shipping");
		}
		elseif($this->getPaydibsConfig('currency_type')=="Display"){
			$CurrencyCode = $this->getPost('DisplayCurrencyCode');
			$amount = number_format($this->getPost('DisplayAmount'), 2, '.','');
			$shipping = $this->getPost('DisplayShipping');
			$discount =  $this->getPost('DisplayDiscount');
			$logger->info("Display Shipping");
		}
		

		$orderId = $this->_checkoutSession->getLastOrderId();
		
		$pgw_params	=	array(
							"TxnType"			  => "PAY",
							"MerchantID"		  => $this->getPaydibsConfig('mid'),
							"MerchantPymtID"	  => date('ymdHis') .rand(0, 9999), 
							'MerchantOrdID'       => $orderId,//$this->getPost('MerchantOrdID'),
							'MerchantOrdDesc'     => $this->getPost('PaymentDesc'),

							'MerchantTxnAmt'      => number_format(($amount+$shipping+$discount), 2, '.',''),
							'MerchantCurrCode'    => $CurrencyCode,

							//Customer Info
							'CustIP'              => $this->getPost('CustIP'), //$_SERVER['REMOTE_ADDR'],
							'CustName'            => $this->getPost('CustName'),
							'CustPhone'           => $this->getPost('CustPhone'),
							'CustEmail'           => $this->getPost('CustEmail'),
							"PageTimeout"		  => $this->getPaydibsConfig('page_timeout'),

							//"LanguageCode"	=> $this->paydibs_acceptable_locale(),
							"MerchantRURL"			=>	$this->helperData->getBaseURL()."PaydibsApp/?urlType=return",
							"MerchantCallBackURL"	=>	$this->helperData->getBaseURL()."PaydibsApp/?urlType=callback"
						);
				
		$this->calculateHashValue($pgw_params);
		
		$logger->info("SEND INFO:", $pgw_params);
		
		$this->add_log($pgw_params);
		$output = 	"	<p>
							You will be redirected automatically to Paydibs Checkout shortly...
						</p>
						<p>
							Or you can manually click the 'Pay Now' button
						</p>
						<form id='frm_Paydibs_payment' method='post' action='".$this->getPaydibsConfig('payment_url')."'>";
							foreach($pgw_params as $ind=>$val){
								$output .= 	"	<input type='hidden' name='".$ind."' id='".$ind."' value='".$val."'/>";
							}
		$output .= 	"	<center><input class='Paydibs_btn' type='submit' value='Pay Now'/></center>
						<div class='loader'>Loading...</div>";
		$output .= 	"</form>";
		
		return $output;
	}
	
	protected function add_log($message){
		if($this->getPaydibsConfig('debug')){
			$this->helperData->add_log(print_r($message,1));
		}
	}

}
?>