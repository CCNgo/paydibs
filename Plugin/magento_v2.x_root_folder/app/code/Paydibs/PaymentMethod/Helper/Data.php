<?php

namespace Paydibs\PaymentMethod\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{
    protected $storeManager;
    protected $objectManager;
	protected $_logger;

    const XML_PATH_PAYDIBS = 'payment/paydibspayment/';



    public function __construct(Context $context,
        ObjectManagerInterface $objectManager,
        StoreManagerInterface $storeManager
    ) {
        $this->objectManager = $objectManager;
        $this->storeManager  = $storeManager;
		$this->_logger = $context->getLogger();
        parent::__construct($context);
    }
	
	public function add_log($message){
		$this->_logger->addDebug("Paydibs_Logs: ".$message);
	}

    public function getConfigValue($field, $storeId = null)
    {
       return $this->scopeConfig->getValue(
            $field, ScopeInterface::SCOPE_STORE, $storeId
        );
    }
	
	public function getBaseURL(){
		return $this->storeManager->getStore()->getBaseUrl();
	}

    public function getGeneralConfig($code, $storeId = null)
    {
        return $this->getConfigValue(self::XML_PATH_PAYDIBS . $code, $storeId);
    }
	
	public function getCurrentLocale(){
		$resolver = $this->objectManager->get('Magento\Framework\Locale\Resolver');
		return $resolver->getLocale();
	}

	

}