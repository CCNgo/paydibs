﻿Imports System
Imports System.IO
Imports System.Xml
Imports System.Web
Imports System.Net
Imports System.Data
Imports System.Security.Cryptography

Partial Class Simulator
    Inherits System.Web.UI.Page

    'Private txtSvcID As TextBox
    'Protected txtMerchantName As TextBox
    'Protected txtTxnID As TextBox
    'Protected txtOrderNum As TextBox
    'Protected txtOrderDesc As TextBox
    'Protected txtTxnAmt As TextBox
    'Protected txtCurrCode As TextBox
    'Protected txtIssuingBank As TextBox
    'Protected txtLangCode As TextBox
    'Protected txtMReturnURL As TextBox
    'Protected txtMSupportURL As TextBox
    'Protected txtParam1 As TextBox
    'Protected txtParam2 As TextBox
    'Protected txtParam3 As TextBox
    'Protected txtParam4 As TextBox
    'Protected txtParam5 As TextBox
    'Protected txtParam6 As TextBox
    'Protected txtParam7 As TextBox
    'Protected txtParam8 As TextBox
    'Protected txtParam9 As TextBox
    'Protected txtParam10 As TextBox
    'Protected txtParam11 As TextBox
    'Protected txtParam12 As TextBox
    'Protected txtParam13 As TextBox
    'Protected txtParam14 As TextBox
    'Protected txtParam15 As TextBox
    'Protected txtCVV2 As TextBox
    'Protected txtBillAddr As TextBox
    'Protected txtBillCity As TextBox
    'Protected txtBillRegion As TextBox
    'Protected txtBillPostal As TextBox
    'Protected txtBillCountry As TextBox
    'Protected txtShipAddr As TextBox
    'Protected txtShipCity As TextBox
    'Protected txtShipRegion As TextBox
    'Protected txtShipPostal As TextBox
    'Protected txtShipCountry As TextBox
    'Protected txtGatewayID As TextBox
    'Protected txtMerchantPwd As TextBox
    'Protected txtPaymentMethod As TextBox
    'Protected txtPaymentURL As TextBox
    'Protected txtPageTimeout As TextBox

    Public szServiceID As String
    Public szMerchantName As String
    Public szMerchantTxnID As String
    Public szOrderNumber As String
    Public szOrderDesc As String
    Public szTxnAmount As String
    Public szCurrencyCode As String
    Public szIssuingBank As String
    Public szLanguageCode As String
    Public szMerchantReturnURL As String
    Public szMerchantSupportURL As String
    Public szParam1 As String
    Public szParam2 As String
    Public szParam3 As String
    Public szParam4 As String
    Public szParam5 As String
    Public szParam6 As String
    Public szParam7 As String
    Public szParam8 As String
    Public szParam9 As String
    Public szParam10 As String
    Public szParam11 As String
    Public szParam12 As String
    Public szParam13 As String
    Public szParam14 As String
    Public szParam15 As String
    Public szGatewayID As String
    Public szCVV2 As String
    Public szBillAddr As String
    Public szBillCity As String
    Public szBillRegion As String
    Public szBillPostal As String
    Public szBillCountry As String
    Public szShipAddr As String
    Public szShipCity As String
    Public szShipRegion As String
    Public szShipPostal As String
    Public szShipCountry As String
    Public szMerchantPassword As String
    Public szPaymentMethod As String
    Public szPaymentURL As String
    Public szPageTimeout As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack <> True Then

            Dim xmlReader As XmlTextReader = New XmlTextReader(MapPath(ConfigurationManager.AppSettings("ConfigPath")))
            Dim xmlDoc As New XmlDocument()

            Try
                xmlDoc.Load(xmlReader)
                xmlReader.Close()
            Catch
                Return
            End Try

            Try
                Dim xmlNodes As XmlNode = xmlDoc.SelectSingleNode("DDGW")

                For Each node As XmlNode In xmlNodes
                    Select Case node.Name
                        Case "ServiceID"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szServiceID = node.ChildNodes(0).Value
                                Else
                                    szServiceID = ""
                                End If
                                Exit Select
                            End If
                        Case "MerchantName"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szMerchantName = node.ChildNodes(0).Value
                                Else
                                    szMerchantName = ""
                                End If
                                Exit Select
                            End If
                        Case "MerchantTxnID"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szMerchantTxnID = node.ChildNodes(0).Value
                                Else
                                    szMerchantTxnID = ""
                                End If
                                Exit Select
                            End If
                        Case "OrderNumber"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szOrderNumber = node.ChildNodes(0).Value
                                Else
                                    szOrderNumber = ""
                                End If
                                Exit Select
                            End If
                        Case "OrderDesc"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szOrderDesc = node.ChildNodes(0).Value
                                Else
                                    szOrderDesc = ""
                                End If
                                Exit Select
                            End If
                        Case "TxnAmount"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szTxnAmount = node.ChildNodes(0).Value
                                Else
                                    szTxnAmount = ""
                                End If
                                Exit Select
                            End If
                        Case "CurrencyCode"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szCurrencyCode = node.ChildNodes(0).Value
                                Else
                                    szCurrencyCode = ""
                                End If
                                Exit Select
                            End If
                        Case "IssuingBank"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szIssuingBank = node.ChildNodes(0).Value
                                Else
                                    szIssuingBank = ""
                                End If
                                Exit Select
                            End If
                        Case "LanguageCode"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szLanguageCode = node.ChildNodes(0).Value
                                Else
                                    szLanguageCode = ""
                                End If
                                Exit Select
                            End If
                        Case "MerchantReturnURL"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szMerchantReturnURL = node.ChildNodes(0).Value
                                Else
                                    szMerchantReturnURL = ""
                                End If
                                Exit Select
                            End If
                        Case "MerchantSupportURL"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szMerchantSupportURL = node.ChildNodes(0).Value
                                Else
                                    szMerchantSupportURL = ""
                                End If
                                Exit Select
                            End If
                        Case "Param1"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szParam1 = node.ChildNodes(0).Value
                                Else
                                    szParam1 = ""
                                End If
                                Exit Select
                            End If
                        Case "Param2"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szParam2 = node.ChildNodes(0).Value
                                Else
                                    szParam2 = ""
                                End If
                                Exit Select
                            End If
                        Case "Param3"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szParam3 = node.ChildNodes(0).Value
                                Else
                                    szParam3 = ""
                                End If
                                Exit Select
                            End If
                        Case "Param4"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szParam4 = node.ChildNodes(0).Value
                                Else
                                    szParam4 = ""
                                End If
                                Exit Select
                            End If
                        Case "Param5"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szParam5 = node.ChildNodes(0).Value
                                Else
                                    szParam5 = ""
                                End If
                                Exit Select
                            End If
                        Case "Param5"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szParam5 = node.ChildNodes(0).Value
                                Else
                                    szParam5 = ""
                                End If
                                Exit Select
                            End If
                        Case "Param6"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szParam6 = node.ChildNodes(0).Value
                                Else
                                    szParam6 = ""
                                End If
                                Exit Select
                            End If
                        Case "Param7"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szParam7 = node.ChildNodes(0).Value
                                Else
                                    szParam7 = ""
                                End If
                                Exit Select
                            End If
                        Case "Param8"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szParam8 = node.ChildNodes(0).Value
                                Else
                                    szParam8 = ""
                                End If
                                Exit Select
                            End If
                        Case "Param9"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szParam9 = node.ChildNodes(0).Value
                                Else
                                    szParam9 = ""
                                End If
                                Exit Select
                            End If
                        Case "Param10"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szParam10 = node.ChildNodes(0).Value
                                Else
                                    szParam10 = ""
                                End If
                                Exit Select
                            End If
                        Case "Param11"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szParam11 = node.ChildNodes(0).Value
                                Else
                                    szParam11 = ""
                                End If
                                Exit Select
                            End If
                        Case "Param12"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szParam12 = node.ChildNodes(0).Value
                                Else
                                    szParam12 = ""
                                End If
                                Exit Select
                            End If
                        Case "Param13"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szParam13 = node.ChildNodes(0).Value
                                Else
                                    szParam13 = ""
                                End If
                                Exit Select
                            End If
                        Case "Param14"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szParam14 = node.ChildNodes(0).Value
                                Else
                                    szParam14 = ""
                                End If
                                Exit Select
                            End If
                        Case "Param15"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szParam15 = node.ChildNodes(0).Value
                                Else
                                    szParam15 = ""
                                End If
                                Exit Select
                            End If
                        Case "CVV2"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szCVV2 = node.ChildNodes(0).Value
                                Else
                                    szCVV2 = ""
                                End If
                                Exit Select
                            End If
                        Case "BillAddr"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szBillAddr = node.ChildNodes(0).Value
                                Else
                                    szBillAddr = ""
                                End If
                                Exit Select
                            End If
                        Case "BillCity"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szBillCity = node.ChildNodes(0).Value
                                Else
                                    szBillCity = ""
                                End If
                                Exit Select
                            End If
                        Case "BillRegion"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szBillRegion = node.ChildNodes(0).Value
                                Else
                                    szBillRegion = ""
                                End If
                                Exit Select
                            End If
                        Case "BillPostal"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szBillPostal = node.ChildNodes(0).Value
                                Else
                                    szBillPostal = ""
                                End If
                                Exit Select
                            End If
                        Case "BillCountry"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szBillCountry = node.ChildNodes(0).Value
                                Else
                                    szBillCountry = ""
                                End If
                                Exit Select
                            End If
                        Case "ShipAddr"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szShipAddr = node.ChildNodes(0).Value
                                Else
                                    szShipAddr = ""
                                End If
                                Exit Select
                            End If
                        Case "ShipCity"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szShipCity = node.ChildNodes(0).Value
                                Else
                                    szShipCity = ""
                                End If
                                Exit Select
                            End If
                        Case "ShipRegion"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szShipRegion = node.ChildNodes(0).Value
                                Else
                                    szShipRegion = ""
                                End If
                                Exit Select
                            End If
                        Case "ShipPostal"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szShipPostal = node.ChildNodes(0).Value
                                Else
                                    szShipPostal = ""
                                End If
                                Exit Select
                            End If
                        Case "ShipCountry"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szShipCountry = node.ChildNodes(0).Value
                                Else
                                    szShipCountry = ""
                                End If
                                Exit Select
                            End If
                        Case "GatewayID"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szGatewayID = node.ChildNodes(0).Value
                                Else
                                    szGatewayID = ""
                                End If
                                Exit Select
                            End If
                        Case "MerchantPassword"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szMerchantPassword = node.ChildNodes(0).Value
                                Else
                                    szMerchantPassword = ""
                                End If
                                Exit Select
                            End If
                        Case "PaymentMethod"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szPaymentMethod = node.ChildNodes(0).Value
                                Else
                                    szPaymentMethod = ""
                                End If
                                Exit Select
                            End If
                        Case "PaymentURL"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szPaymentURL = node.ChildNodes(0).Value
                                Else
                                    szPaymentURL = ""
                                End If
                                Exit Select
                            End If
                        Case "PageTimeout"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szPageTimeout = node.ChildNodes(0).Value
                                Else
                                    szPageTimeout = ""
                                End If
                                Exit Select
                            End If
                    End Select
                Next

                txtSvcID.Text = szServiceID
                txtMerchantName.Text = szMerchantName
                txtTxnID.Text = szMerchantTxnID
                txtOrderNum.Text = szOrderNumber
                txtOrderDesc.Text = szOrderDesc
                txtTxnAmt.Text = szTxnAmount
                txtCurrCode.Text = szCurrencyCode
                txtIssuingBank.Text = szIssuingBank
                txtLangCode.Text = szLanguageCode
                txtMReturnURL.Text = szMerchantReturnURL
                txtMSupportURL.Text = szMerchantSupportURL
                txtParam1.Text = szParam1
                txtParam2.Text = szParam2
                txtParam3.Text = szParam3
                txtParam4.Text = szParam4
                txtParam5.Text = szParam5
                txtParam6.Text = szParam6
                txtParam7.Text = szParam7
                txtParam8.Text = szParam8
                txtParam9.Text = szParam9
                txtParam10.Text = szParam10
                txtParam11.Text = szParam11
                txtParam12.Text = szParam12
                txtParam13.Text = szParam13
                txtParam14.Text = szParam14
                txtParam15.Text = szParam15
                txtCVV2.Text = szCVV2
                txtBillAddr.Text = szBillAddr
                txtBillCity.Text = szBillCity
                txtBillRegion.Text = szBillRegion
                txtBillPostal.Text = szBillPostal
                txtBillCountry.Text = szBillCountry
                txtShipAddr.Text = szShipAddr
                txtShipCity.Text = szShipCity
                txtShipRegion.Text = szShipRegion
                txtShipPostal.Text = szShipPostal
                txtShipCountry.Text = szShipCountry
                txtGatewayID.Text = szGatewayID
                txtMerchantPwd.Text = szMerchantPassword
                txtPaymentMethod.Text = szPaymentMethod
                txtPaymentURL.Text = szPaymentURL
                txtPageTimeout.Text = szPageTimeout

            Catch ex As Exception
                Response.Write("<script>alert('Page Load Error! " + ex.ToString() + "');</script>")
                Return
            End Try
        End If
    End Sub

    Protected Sub btnSale_Click(ByVal sender As Object, ByVal e As System.EventArgs) ' Handles btnSale.Click
        Try
            Dim szHashKey As String = ""
            Dim szHashValue As String = ""
            Dim szPostMsg As String = ""

            If txtPaymentMethod.Text = "CC" Then

                szHashKey = txtMerchantPwd.Text + txtSvcID.Text + txtTxnID.Text + txtMReturnURL.Text + _
                        txtTxnAmt.Text + txtCurrCode.Text

                'szHashValue = szComputeSHA1Hash(szHashKey)
                szHashValue = szComputeSHA256Hash(szHashKey)

                szPostMsg = "TransactionType=SALE&ServiceID=" + txtSvcID.Text + "&PaymentID=" + txtTxnID.Text + _
                "&OrderNumber=" + txtOrderNum.Text + "&PaymentDesc=" + txtOrderDesc.Text + "&MerchantName=" + _
                txtMerchantName.Text + "&MerchantReturnURL=" + txtMReturnURL.Text + "&Amount=" + txtTxnAmt.Text + _
                "&CurrencyCode=" + txtCurrCode.Text + "&HashValue=" + szHashValue + "&Param1=" + txtParam1.Text + _
                "&Param2=" + txtParam2.Text + "&Param3=" + txtParam3.Text + "&Param4=" + txtParam4.Text + _
                "&Param5=" + txtParam5.Text + "&CardCVV2=" + txtCVV2.Text + "&IssuingBank=" + txtIssuingBank.Text

                If txtParam6.Text <> "" Then
                    szPostMsg = szPostMsg + "&Param6=" + txtParam6.Text
                End If
                If txtParam7.Text <> "" Then
                    szPostMsg = szPostMsg + "&Param7=" + txtParam7.Text
                End If
                If txtParam8.Text <> "" Then
                    szPostMsg = szPostMsg + "&Param8=" + txtParam8.Text
                End If
                If txtParam9.Text <> "" Then
                    szPostMsg = szPostMsg + "&Param9=" + txtParam9.Text
                End If
                If txtParam10.Text <> "" Then
                    szPostMsg = szPostMsg + "&Param10=" + txtParam10.Text
                End If
                If txtParam11.Text <> "" Then
                    szPostMsg = szPostMsg + "&Param11=" + txtParam11.Text
                End If
                If txtParam12.Text <> "" Then
                    szPostMsg = szPostMsg + "&Param12=" + txtParam12.Text
                End If
                If txtParam13.Text <> "" Then
                    szPostMsg = szPostMsg + "&Param13=" + txtParam13.Text
                End If
                If txtParam14.Text <> "" Then
                    szPostMsg = szPostMsg + "&Param14=" + txtParam14.Text
                End If
                If txtParam15.Text <> "" Then
                    szPostMsg = szPostMsg + "&Param15=" + txtParam15.Text
                End If
                If txtBillAddr.Text <> "" Then
                    szPostMsg = szPostMsg + "&BillAddr=" + txtBillAddr.Text
                End If
                If txtBillCity.Text <> "" Then
                    szPostMsg = szPostMsg + "&BillCity=" + txtBillCity.Text
                End If
                If txtBillRegion.Text <> "" Then
                    szPostMsg = szPostMsg + "&BillRegion=" + txtBillRegion.Text
                End If
                If txtBillPostal.Text <> "" Then
                    szPostMsg = szPostMsg + "&BillPostal=" + txtBillPostal.Text
                End If
                If txtBillCountry.Text <> "" Then
                    szPostMsg = szPostMsg + "&BillCountry=" + txtBillCountry.Text
                End If
                If txtShipAddr.Text <> "" Then
                    szPostMsg = szPostMsg + "&ShipAddr=" + txtShipAddr.Text
                End If
                If txtShipCity.Text <> "" Then
                    szPostMsg = szPostMsg + "&ShipCity=" + txtShipCity.Text
                End If
                If txtShipRegion.Text <> "" Then
                    szPostMsg = szPostMsg + "&ShipRegion=" + txtShipRegion.Text
                End If
                If txtShipPostal.Text <> "" Then
                    szPostMsg = szPostMsg + "&ShipPostal=" + txtShipPostal.Text
                End If
                If txtShipCountry.Text <> "" Then
                    szPostMsg = szPostMsg + "&ShipCountry=" + txtShipCountry.Text
                End If
                If txtPageTimeout.Text <> "" Then
                    szPostMsg = szPostMsg + "&PageTimeout=" + txtPageTimeout.Text
                End If

                szPostMsg = szPostMsg + "&PymtMethod=" + txtPaymentMethod.Text

            ElseIf txtPaymentMethod.Text = "DD" Then
                'DDB Message
                'szPostMsg = "TxnType=SALE&ServiceID=" + TextBox1.Text + "&MerchantName=" + _
                'TextBox2.Text + "&MerchantTxnID=" + TextBox3.Text + "&OrderNumber=" + TextBox4.Text + _
                '"&OrderDesc=" + TextBox5.Text + "&TxnAmount=" + TextBox6.Text + "&CurrencyCode=" + _
                'TextBox7.Text + "&IssuingBank=" + TextBox8.Text + "&LanguageCode=" + TextBox9.Text + _
                '"&MerchantReturnURL=" + TextBox10.Text + "&MerchantSupportURL=" + TextBox11.Text + _
                '"&HashMethod=SHA1" + "&Param1=" + TextBox12.Text + "&Param2=" + TextBox13.Text + _
                '"&Param3=" + TextBox14.Text + "&Param4=" + TextBox15.Text + "&Param5=" + _
                'TextBox16.Text + "&GatewayID=" + TextBox17.Text + "&HashValue=" + szHashValue
            ElseIf txtPaymentMethod.Text = "WA" Then
                ''WA message
            End If

            Dim xmlReader As XmlTextReader = New XmlTextReader(MapPath(ConfigurationManager.AppSettings("ConfigPath")))
            Dim xmlDoc As New XmlDocument()

            Try
                xmlDoc.Load(xmlReader)
                xmlReader.Close()
            Catch
                Response.Write("<script>alert('XML Read Error in before update new Txn into XML!');</script>")
                Return
            End Try

            Dim xmlNodes As XmlNode = xmlDoc.SelectSingleNode("IPG")

            For Each node As XmlNode In xmlNodes
                Select Case node.Name
                    Case "ServiceID"
                        If True Then
                            If txtSvcID.Text <> "" Then
                                node.ChildNodes(0).Value = txtSvcID.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "MerchantName"
                        If True Then
                            If txtMerchantName.Text <> "" Then
                                node.ChildNodes(0).Value = txtMerchantName.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "MerchantTxnID"
                        If True Then
                            If txtTxnID.Text <> "" Then
                                node.ChildNodes(0).Value = txtTxnID.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "OrderNumber"
                        If True Then
                            If txtOrderNum.Text <> "" Then
                                node.ChildNodes(0).Value = txtOrderNum.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "OrderDesc"
                        If True Then
                            If txtOrderDesc.Text <> "" Then
                                node.ChildNodes(0).Value = txtOrderDesc.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "TxnAmount"
                        If True Then
                            If txtTxnAmt.Text <> "" Then
                                node.ChildNodes(0).Value = txtTxnAmt.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "CurrencyCode"
                        If True Then
                            If txtCurrCode.Text <> "" Then
                                node.ChildNodes(0).Value = txtCurrCode.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "IssuingBank"
                        If True Then
                            If txtIssuingBank.Text <> "" Then
                                node.ChildNodes(0).Value = txtIssuingBank.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "LanguageCode"
                        If True Then
                            If txtLangCode.Text <> "" Then
                                node.ChildNodes(0).Value = txtLangCode.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "MerchantReturnURL"
                        If True Then
                            If txtMReturnURL.Text <> "" Then
                                node.ChildNodes(0).Value = txtMReturnURL.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "MerchantSupportURL"
                        If True Then
                            If txtMSupportURL.Text <> "" Then
                                node.ChildNodes(0).Value = txtMSupportURL.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param1"
                        If True Then
                            If txtParam1.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam1.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param2"
                        If True Then
                            If txtParam2.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam2.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param3"
                        If True Then
                            If txtParam3.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam3.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param4"
                        If True Then
                            If txtParam4.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam4.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param5"
                        If True Then
                            If txtParam5.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam5.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param6"
                        If True Then
                            If txtParam6.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam6.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param7"
                        If True Then
                            If txtParam7.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam7.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param8"
                        If True Then
                            If txtParam8.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam8.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param9"
                        If True Then
                            If txtParam9.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam9.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param10"
                        If True Then
                            If txtParam10.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam10.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param11"
                        If True Then
                            If txtParam11.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam11.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param12"
                        If True Then
                            If txtParam12.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam12.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param13"
                        If True Then
                            If txtParam13.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam13.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param14"
                        If True Then
                            If txtParam14.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam14.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param15"
                        If True Then
                            If txtParam15.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam15.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "CVV2"
                        If True Then
                            If txtCVV2.Text <> "" Then
                                node.ChildNodes(0).Value = txtCVV2.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "BillAddr"
                        If True Then
                            If txtBillAddr.Text <> "" Then
                                node.ChildNodes(0).Value = txtBillAddr.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "BillCity"
                        If True Then
                            If txtBillCity.Text <> "" Then
                                node.ChildNodes(0).Value = txtBillCity.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "BillRegion"
                        If True Then
                            If txtBillRegion.Text <> "" Then
                                node.ChildNodes(0).Value = txtBillRegion.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "BillPostal"
                        If True Then
                            If txtBillPostal.Text <> "" Then
                                node.ChildNodes(0).Value = txtBillPostal.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "BillCountry"
                        If True Then
                            If txtBillCountry.Text <> "" Then
                                node.ChildNodes(0).Value = txtBillCountry.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "ShipAddr"
                        If True Then
                            If txtShipAddr.Text <> "" Then
                                node.ChildNodes(0).Value = txtShipAddr.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "ShipCity"
                        If True Then
                            If txtShipCity.Text <> "" Then
                                node.ChildNodes(0).Value = txtShipCity.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "ShipRegion"
                        If True Then
                            If txtShipRegion.Text <> "" Then
                                node.ChildNodes(0).Value = txtShipRegion.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "ShipPostal"
                        If True Then
                            If txtShipPostal.Text <> "" Then
                                node.ChildNodes(0).Value = txtShipPostal.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "ShipCountry"
                        If True Then
                            If txtShipCountry.Text <> "" Then
                                node.ChildNodes(0).Value = txtShipCountry.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "GatewayID"
                        If True Then
                            If txtGatewayID.Text <> "" Then
                                node.ChildNodes(0).Value = txtGatewayID.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "MerchantPassword"
                        If True Then
                            If txtMerchantPwd.Text <> "" Then
                                node.ChildNodes(0).Value = txtMerchantPwd.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "PaymentMethod"
                        If True Then
                            If txtPaymentMethod.Text <> "" Then
                                node.ChildNodes(0).Value = txtPaymentMethod.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "PaymentURL"
                        If True Then
                            If txtPaymentURL.Text <> "" Then
                                node.ChildNodes(0).Value = txtPaymentURL.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "PageTimeout"
                        If True Then
                            If txtPageTimeout.Text <> "" Then
                                node.ChildNodes(0).Value = txtPageTimeout.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                End Select
            Next

            xmlDoc.Save(MapPath(ConfigurationManager.AppSettings("ConfigPath")))

            Response.Redirect(txtPaymentURL.Text + szPostMsg)

        Catch ex As Exception
            Response.Write("<script>alert('Error! Possible XML permission not set!');</script>")
            Return
        End Try
    End Sub

    Protected Sub btnQuery_Click(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles Button2.Click
        '    Dim szHashKey As String
        '    Dim szQueryString As String
        '    Dim szHashValue As String

        '    szHashKey = TextBox18.Text + TextBox1.Text + TextBox3.Text

        '    szHashValue = szComputeSHA1Hash(szHashKey)

        '    szQueryString = "TransactionType=QUERY&ServiceID=" + TextBox1.Text + "&PaymentID=" + _
        '        TextBox3.Text + "&HashMethod=SHA1&HashValue=" + szHashValue

        '    Response.Redirect(TextBox19.Text + szQueryString)

    End Sub

    Protected Sub btnRSale_Click(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles Button3.Click
        '    Dim szHashKey As String
        '    Dim szQueryString As String
        '    Dim szHashValue As String

        '    szHashKey = TextBox18.Text + TextBox1.Text + TextBox3.Text + TextBox6.Text + TextBox7.Text

        '    szHashValue = szComputeSHA1Hash(szHashKey)

        '    szQueryString = "TransactionType=RSale&ServiceID=" + TextBox1.Text + "&PaymentID=" + _
        '        TextBox3.Text + "&TxnAmount=" + TextBox6.Text + "&CurrencyCode=" + _
        '        TextBox7.Text + "&HashMethod=SHA1&HashValue=" + szHashValue

        '    Response.Redirect(TextBox19.Text + szQueryString)

    End Sub

    Public Function szComputeSHA1Hash(ByVal sz_Key As String) As String
        Dim szHashValue As String = ""
        Dim objSHA1 As SHA1CryptoServiceProvider = Nothing
        Dim btHashbuffer() As Byte

        Try
            objSHA1 = New SHA1CryptoServiceProvider
            objSHA1.ComputeHash(System.Text.Encoding.UTF8.GetBytes(sz_Key.ToCharArray))
            btHashbuffer = objSHA1.Hash
            szHashValue = System.Convert.ToBase64String(btHashbuffer)

        Catch ex As Exception
            Throw ex
        Finally
            If Not objSHA1 Is Nothing Then objSHA1 = Nothing
        End Try

        Return szHashValue
    End Function

    Public Function szWriteHex(ByVal bt_Array() As Byte) As String
        Dim szHex As String = ""
        Dim iCounter As Integer

        For iCounter = 0 To (bt_Array.Length() - 1)
            szHex += bt_Array(iCounter).ToString("X2")
        Next

        Return szHex
    End Function

    Public Function szComputeSHA256Hash(ByVal sz_Key As String) As String
        Dim szHashValue As String = ""
        Dim objSHA256 As SHA256 = Nothing
        Dim btHashbuffer() As Byte

        Try
            objSHA256 = New SHA256Managed
            objSHA256.ComputeHash(System.Text.Encoding.ASCII.GetBytes(sz_Key))
            btHashbuffer = objSHA256.Hash
            szHashValue = szWriteHex(btHashbuffer).ToLower()

        Catch ex As Exception
            Throw ex
        Finally
            If Not objSHA256 Is Nothing Then objSHA256 = Nothing
        End Try

        Return szHashValue
    End Function

    Protected Sub cbShipAddr_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles cbShipAddr.CheckedChanged
        If cbShipAddr.Checked = True Then
            pnShip.Visible = True
        Else
            pnShip.Visible = False
        End If
    End Sub

    Protected Sub cbParam_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles cbParam.CheckedChanged
        If cbParam.Checked = True Then
            pnParam.Visible = True
        Else
            pnParam.Visible = False
        End If
    End Sub

End Class
