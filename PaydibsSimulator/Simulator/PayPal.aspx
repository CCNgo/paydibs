﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PayPal.aspx.vb" Inherits="Simulator.PayPal" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
</head>
<body>
    <form id="form1" runat="server">
        <div id="paypal-button"></div>
        <br /><br />
        <asp:Button ID="Button1" runat="server" Text="Click" />
        <br /><br />
        <div id="test-button"></div>

    </form>

    <script src="https://www.paypalobjects.com/api/checkout.js"></script>
    
    <script>
        paypal.Button.render({

            env: 'sandbox', // sandbox | production

            // Specify the style of the button
            style: {
                label: 'paypal',
                size: 'medium',    // small | medium | large | responsive
                shape: 'pill',     // pill | rect
                color: 'silver',     // gold | blue | silver | black
                tagline: false
            },

            // PayPal Client IDs - replace with your own
            // Create a PayPal app: https://developer.paypal.com/developer/applications/create
            client: {
                sandbox: 'AdWcZN7GOKboramp5cd_UM3LAHvEUNJH1KjuwHgVbIc2oFDAGEgCLJuwCA2XLTADtzQDDN48owmF6mgw'
            },

            // Show the buyer a 'Pay Now' button in the checkout flow
            //commit: true,

            // Set up the payment:
            // 1. Add a payment callback
            payment: function(data, actions) {
                // 2. Make a request to your server
                return actions.request.post('/Pymt2GW.aspx/CreatePayment')
                  .then(function(res) {
                      // 3. Return res.id from the response
                      return res.id;
                  });
            },
            // Execute the payment:
            // 1. Add an onAuthorize callback
            onAuthorize: function (data, actions) {
                // 2. Make a request to your server
                return actions.request.post('/my-api/execute-payment/', {
                    paymentID: data.paymentID,
                    payerID: data.payerID
                })
                  .then(function (res) {
                      // 3. Show the buyer a confirmation message.
                  });
            }
            // payment() is called when the button is clicked
            //payment: function (data, actions) {
            //     //Make a call to the REST api to create the payment
            //    return actions.payment.create({
            //        payment: {
            //            transactions: [
            //                {
            //                    amount: { total: '0.01', currency: 'MYR' },
            //                    soft_descriptor: 'MTxnID12345',
            //                    invoice_number: 'GUniqueID12345',
            //                    description: 'The payment transaction description.'
            //                }
            //            ]
            //        }
            //    });
            //},

            //// onAuthorize() is called when the buyer approves the payment
            //onAuthorize: function (data, actions) {

            //    // Make a call to the REST api to execute the payment
            //    return actions.payment.execute().then(function () {
            //        window.alert('Payment Complete!');
            //    });
            //}

        }, '#paypal-button');
    </script>
    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#<%=Button1.ClientID %>').click(function () {
                $.ajax({
                        type: "POST",
                        url: "http://localhost:57029/Pymt2GW.aspx/CreatePayment",
                        data: "{}",
                        contentType: "application/text; charset=utf-8",
                        dataType: "text",
                        async: true,
                        cache: false,
                        success: function (msg) {
                            $('#test-button').text(msg.d); 
                        }
                    })
                return false;
            });
          });
    </script>

</body>
</html>
