﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Pymt2GW.aspx.vb" Inherits="Simulator.Pymt2GW" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
		<title>Paydibs Simulator</title>
		<style type="text/css">
            .style1 { width: 533px; }
            .style2 { width: 180px; }
            .style3 { width: 255px; }
            .style10
            {
                width: 551px;
            }
        </style>
	</head>
	<body bgcolor="#ffffff">
		<form id="form1" runat="server">
			<div align="center">
				<div>
					<b style="COLOR: #f55a00; FONT-SIZE: 30px">Paydibs Checkout Simulator</b>
				</div>
				<div align="center">
					<br />
					<asp:Button ID="btnPay" runat="server" Text="Pay" Width="150px" BackColor="#FACC30" />
					<asp:Button ID="btnQuery" runat="server" Text="Query" Width="150px" 
                        TabIndex="1" BackColor="#FACC30"/>
					<asp:Button ID="btnVoid" runat="server" Text="Void" Width="150px" 
                        TabIndex="2" BackColor="#FACC30"/>
				    <asp:Button ID="btnAuth" runat="server" Text="Auth" Width="150px" 
                        TabIndex="3" BackColor="#FACC30"/>
                    <asp:Button ID="btnCapture" runat="server" Text="Capture" Width="150px" 
                        TabIndex="4" BackColor="#FACC30"/>
				    <asp:Button ID="btnRefund" runat="server" Text="Refund" Width="150px" 
                        TabIndex="5" BackColor="#FACC30"/>
                    <asp:Button ID="btnSOP" runat="server" Text="SOP" Width="150px" 
                        TabIndex="6" BackColor="#FACC30"/>
				</div>
				<br />
				<table class="style1" align="center" border="1">
					<tr>
						<td class="style2">
							<asp:Label ID="Label1" runat="server" Text="MerchantID"></asp:Label>
						</td>
						<td class="style3">
						    <asp:TextBox ID="txtMerchantID" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label ID="Label2" runat="server" Text="MerchantName"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtMerchantName" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label ID="Label3" runat="server" Text="MerchantPymtID"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtMerchantPymtID" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label ID="Label4" runat="server" Text="MerchantOrdID"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtMerchantOrdID" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label ID="Label5" runat="server" Text="MerchantOrdDesc"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtMerchantOrdDesc" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label ID="Label6" runat="server" Text="MerchantTxnAmt"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtMerchantTxnAmt" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label ID="Label7" runat="server" Text="MerchantCurrCode"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtMerchantCurrCode" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label ID="Label8" runat="server" Text="AcqBank"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtAcqBank" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label ID="Label9" runat="server" Text="LanguageCode"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtLangCode" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label ID="Label10" runat="server" Text="MerchantRURL"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtMReturnURL" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label ID="Label55" runat="server" Text="MerchantApprovalURL"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtMApprovalURL" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label ID="Label56" runat="server" Text="MerchantUnApprovalURL"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtMUnApprovalURL" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label ID="Label58" runat="server" Text="MerchantCancelURL"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtMCancelURL" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label ID="Label57" runat="server" Text="MerchantCallBackURL"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtMCallBackURL" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label ID="Label11" runat="server" Text="MerchantSupportURL"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtMSupportURL" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label ID="Label12" runat="server" Text="Param1"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtParam1" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label ID="Label13" runat="server" Text="Param2 / CardPAN"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtParam2" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label ID="Label14" runat="server" Text="Param3 / ExpDate"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtParam3" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label ID="Label15" runat="server" Text="Param4 / CardHolder"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtParam4" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label ID="Label16" runat="server" Text="Param5"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtParam5" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
				</table>
				
				<table class="style1" align="center" border="1">
				    <tr>
					    <td class="style10">
					        <asp:CheckBox OnCheckedChanged="cbParam_CheckedChanged" ID="cbParam" runat="server" Text="More Params" AutoPostBack="true" />
					    </td>
					</tr>
				</table>
					
				<asp:Panel id="pnParam" runat="server" Visible="False" >
					<table class="style1" align="center" border="1">
					    <tr>
						    <td class="style2">
							    <asp:Label ID="Label20" runat="server" Text="Param6"></asp:Label>
						    </td>
						    <td class="style3">
							    <asp:TextBox ID="txtParam6" runat="server" Width="400px"></asp:TextBox>
						    </td>
					    </tr>
					    <tr>
						    <td class="style2">
							    <asp:Label ID="Label21" runat="server" Text="Param7"></asp:Label>
						    </td>
						    <td class="style3">
							    <asp:TextBox ID="txtParam7" runat="server" Width="400px"></asp:TextBox>
						    </td>
					    </tr>
					    <tr>
						    <td class="style2">
							    <asp:Label ID="Label22" runat="server" Text="Param8"></asp:Label>
						    </td>
						    <td class="style3">
							    <asp:TextBox ID="txtParam8" runat="server" Width="400px"></asp:TextBox>
						    </td>
					    </tr>
					    <tr>
						    <td class="style2">
							    <asp:Label ID="Label23" runat="server" Text="Param9"></asp:Label>
						    </td>
						    <td class="style3">
							    <asp:TextBox ID="txtParam9" runat="server" Width="400px"></asp:TextBox>
						    </td>
					    </tr>
					    <tr>
						    <td class="style2">
							    <asp:Label ID="Label24" runat="server" Text="Param10"></asp:Label>
						    </td>
						    <td class="style3">
							    <asp:TextBox ID="txtParam10" runat="server" Width="400px"></asp:TextBox>
						    </td>
					    </tr>
					    <tr>
						    <td class="style2">
							    <asp:Label ID="Label25" runat="server" Text="Param11"></asp:Label>
						    </td>
						    <td class="style3">
							    <asp:TextBox ID="txtParam11" runat="server" Width="400px"></asp:TextBox>
						    </td>
					    </tr>
					    <tr>
						    <td class="style2">
							    <asp:Label ID="Label26" runat="server" Text="Param12"></asp:Label>
						    </td>
						    <td class="style3">
							    <asp:TextBox ID="txtParam12" runat="server" Width="400px"></asp:TextBox>
						    </td>
					    </tr>
					    <tr>
						    <td class="style2">
							    <asp:Label ID="Label27" runat="server" Text="Param13"></asp:Label>
						    </td>
						    <td class="style3">
							    <asp:TextBox ID="txtParam13" runat="server" Width="400px"></asp:TextBox>
						    </td>
					    </tr>
					    <tr>
						    <td class="style2">
							    <asp:Label ID="Label28" runat="server" Text="Param14"></asp:Label>
						    </td>
						    <td class="style3">
							    <asp:TextBox ID="txtParam14" runat="server" Width="400px"></asp:TextBox>
						    </td>
					    </tr>
					    <tr>
						    <td class="style2">
							    <asp:Label ID="Label29" runat="server" Text="Param15"></asp:Label>
						    </td>
						    <td class="style3">
							    <asp:TextBox ID="txtParam15" runat="server" Width="400px"></asp:TextBox>
						    </td>
					    </tr>
					</table>
				</asp:Panel>
				
				<table class="style1" align="center" border="1">
				    <tr>
						<td class="style2">
							<asp:Label ID="Label46" runat="server" Text="CustName"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtCustName" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label ID="Label47" runat="server" Text="CustPhone"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtCustPhone" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label ID="Label48" runat="server" Text="CustEmail"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtCustEmail" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
				    <tr>
						<td class="style2">
							<asp:Label ID="Label44" runat="server" Text="CardNo"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtCardNo" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label ID="Label45" runat="server" Text="CardExp"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtCardExp" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label ID="Label30" runat="server" Text="CVV2"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtCVV2" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label ID="Label53" runat="server" Text="TokenType"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtTokenType" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label ID="Label54" runat="server" Text="Token"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtToken" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label ID="Label49" runat="server" Text="3DFlag"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txt3DFlag" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label ID="Label50" runat="server" Text="ECI"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtECI" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label ID="Label51" runat="server" Text="CAVV"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtCAVV" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label ID="Label52" runat="server" Text="XID"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtXID" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label ID="Label43" runat="server" Text="CustIP"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtCustIP" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label ID="Label31" runat="server" Text="BillAddr"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtBillAddr" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label ID="Label32" runat="server" Text="BillCity"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtBillCity" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label ID="Label33" runat="server" Text="BillRegion"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtBillRegion" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label ID="Label34" runat="server" Text="BillPostal"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtBillPostal" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label ID="Label35" runat="server" Text="BillCountry"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtBillCountry" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label ID="Label67" runat="server" Text="SettleTAID"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtSettleTAID" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label ID="Label68" runat="server" Text="SettleAmount"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtSettleAmount" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label ID="Label69" runat="server" Text="SettleTxnCount"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtSettleTxnCount" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label ID="Label70" runat="server" Text="PromoCode"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtPromoCode" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
				</table>
				
				<table class="style1" align="center" border="1">
				    <tr>
					    <td class="style2">
					        <asp:CheckBox OnCheckedChanged="cbShipAddr_CheckedChanged" ID="cbShipAddr" runat="server" Text="Shipping Address" AutoPostBack="true" />
					    </td>
					</tr>
				</table>
								
				<asp:Panel ID="pnShip" runat="server"  Visible="False">
				    <table class="style1" align="center" border="1">
					    <tr>
						    <td class="style2">
							    <asp:Label ID="Label37" runat="server" Text="ShipAddr"></asp:Label>
						    </td>
						    <td class="style3">
							    <asp:TextBox ID="txtShipAddr" runat="server" Width="400px"></asp:TextBox>
						    </td>
					    </tr>
					    <tr>
						    <td class="style2">
							    <asp:Label ID="Label38" runat="server" Text="ShipCity"></asp:Label>
						    </td>
						    <td class="style3">
							    <asp:TextBox ID="txtShipCity" runat="server" Width="400px"></asp:TextBox>
						    </td>
					    </tr>
					    <tr>
						    <td class="style2">
							    <asp:Label ID="Label39" runat="server" Text="ShipRegion"></asp:Label>
						    </td>
						    <td class="style3">
							    <asp:TextBox ID="txtShipRegion" runat="server" Width="400px"></asp:TextBox>
						    </td>
					    </tr>
					    <tr>
						    <td class="style2">
							    <asp:Label ID="Label40" runat="server" Text="ShipPostal"></asp:Label>
						    </td>
						    <td class="style3">
							    <asp:TextBox ID="txtShipPostal" runat="server" Width="400px"></asp:TextBox>
						    </td>
					    </tr>
					    <tr>
						    <td class="style2">
							    <asp:Label ID="Label41" runat="server" Text="ShipCountry"></asp:Label>
						    </td>
						    <td class="style3">
							    <asp:TextBox ID="txtShipCountry" runat="server" Width="400px"></asp:TextBox>
						    </td>
					    </tr>
					</table>
				</asp:Panel>
					
				<table class="style1" align="center" border="1">
				    <tr>
						<td class="style2">
							<asp:Label ID="Label59" runat="server" Text="TermMth (EPP)"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtTermMth" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label ID="Label71" runat="server" Text="InstallmentList (HIID)"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtInstallmentList" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label ID="Label17" runat="server" Text="GatewayID"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtGatewayID" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label id="Label18" runat="server">MerchantPassword</asp:Label></td>
						<td class="style3">
							<asp:TextBox ID="txtMerchantPwd" runat="server" Width="400px"></asp:TextBox></td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label id="Label36" runat="server">PaymentMethod</asp:Label></td>
						<td class="style3">
							<asp:TextBox ID="txtPaymentMethod" runat="server" Width="400px"></asp:TextBox></td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label id="Label19" runat="server">PaymentURL</asp:Label></td>
						<td class="style3">
							<asp:TextBox ID="txtPaymentURL" runat="server" Width="400px"></asp:TextBox></td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label ID="Label42" runat="server" Text="PageTimeout (S)"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtPageTimeout" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label ID="Label65" runat="server" Text="ReqToken"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtReqToken" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label ID="Label60" runat="server" Text="ReqVerifier"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtReqVerifier" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label ID="Label66" runat="server" Text="PairingToken"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtPairingToken" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label ID="Label61" runat="server" Text="PairingVerifier"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtPairingVerifier" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label ID="Label62" runat="server" Text="CheckoutResourceURL"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtCheckoutResourceURL" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label ID="Label63" runat="server" Text="CardId"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtCardId" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style2">
							<asp:Label ID="Label64" runat="server" Text="PreCheckoutId"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtPreCheckoutId" runat="server" Width="400px"></asp:TextBox>
						</td>
					</tr>
				</table>
			</div>
		</form>
	</body>
</html>
