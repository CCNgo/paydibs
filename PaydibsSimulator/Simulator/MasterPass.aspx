﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="MasterPass.aspx.vb" Inherits="Simulator.MasterPass" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>IPG MasterPass Simulator</title>
</head>
<body>
    	
   <form id="form1" runat="server">
    <div>
	    <script type="text/javascript" src="https://sandbox.static.masterpass.com/dyn/js/switch/integration/MasterPass.client.js"></script>
	    <div id='MPCheckoutBtn' onClick='MasterPassCheckout();' style="cursor:pointer;">
	    <img src="https://www.mastercard.com/mc_us/wallet/img/mcpp_wllt_btn_chk_147x034px.png" alt="Buy with MasterPass">
	    </div>
	    <script>function MasterPassCheckout(){MasterPass.client.checkout({"requestPairing":"true","requestToken":"f2426181f6daccc79f6648234189e71693125581","pairingRequestToken":"a2b22ad455c083b24528e39bb07d15597a34a0a6","callbackUrl":"https://test2pay.ghl.com/IPGSimulatorOoiMei/RespFrmGW.aspx","failureCallback":"https://test2pay.ghl.com/IPGSimulatorOoiMei/RespFrmGW.aspx","cancelCallback": "https://test2pay.ghl.com/IPGSimulatorOoiMei/RespFrmGW.aspx","merchantCheckoutId":"a32d8440202b408dbdcc3ca8763d4625","requestedDataTypes":["ADDRESS","PROFILE","CARD"],"requestExpressCheckout":"true","allowedCardTypes":["master","visa"],"suppressShippingAddressEnable":"true","version":"v6"});}</script>
		<a href="javascript:openWindow('https://www.mastercard.com/mc_us/wallet/learnmore/en/MY/',600,340,'LearnMore');" style="font-size:12px;">Learn More</a>
    </div>
    </form>
    
    <script src="jquery/jquery.min.js"></script>
	<script src="jquery/jquery-migrate.min.js"></script>
	<script type="text/javascript">
	function openWindow(szURL, winWidth, winHeight, szWinName) {
		    window.open(szURL, szWinName, config = 'height=' + winHeight + ', width=' + winWidth + ', toolbar=no, menubar=no, scrollbars=yes, resizable=no,location=no, directories=no, status=no');
		}
	</script>
    
</body>
</html>
