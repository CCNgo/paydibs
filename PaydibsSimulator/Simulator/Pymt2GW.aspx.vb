﻿Imports System
Imports System.IO
Imports System.Xml
Imports System.Web
Imports System.Net
Imports System.Data
Imports System.Security.Cryptography

Partial Public Class Pymt2GW
    Inherits System.Web.UI.Page

    Public szMerchantID As String
    Public szMerchantName As String
    Public szMerchantTxnID As String
    Public szMerchantOrdID As String
    Public szMerchantOrdDesc As String
    Public szMerchantTxnAmt As String
    Public szMerchantCurrCode As String
    Public szAcqBank As String
    Public szLanguageCode As String
    Public szMerchantReturnURL As String
    Public szMerchantApprovalURL As String
    Public szMerchantUnApprovalURL As String
    Public szMerchantCancelURL As String
    Public szMerchantCallBackURL As String
    Public szMerchantSupportURL As String
    Public szParam1 As String
    Public szParam2 As String
    Public szParam3 As String
    Public szParam4 As String
    Public szParam5 As String
    Public szParam6 As String
    Public szParam7 As String
    Public szParam8 As String
    Public szParam9 As String
    Public szParam10 As String
    Public szParam11 As String
    Public szParam12 As String
    Public szParam13 As String
    Public szParam14 As String
    Public szParam15 As String
    Public szGatewayID As String
    Public szCustName As String
    Public szCustPhone As String
    Public szCustEmail As String
    Public szCardNo As String
    Public szCardExp As String
    Public szCVV2 As String
    Public szTokenType As String
    Public szToken As String
    Public sz3DFlag As String
    Public szECI As String
    Public szCAVV As String
    Public szXID As String
    Public szCustIP As String
    Public szBillAddr As String
    Public szBillCity As String
    Public szBillRegion As String
    Public szBillPostal As String
    Public szBillCountry As String
    Public szShipAddr As String
    Public szShipCity As String
    Public szShipRegion As String
    Public szShipPostal As String
    Public szShipCountry As String
    Public szMerchantPassword As String
    Public szPaymentMethod As String
    Public szPaymentURL As String
    Public szPageTimeout As String
    Public szTermMth As String
    Public szInstallmentList As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack <> True Then

            Dim xmlReader As XmlTextReader = New XmlTextReader(MapPath(ConfigurationManager.AppSettings("ConfigPath")))
            Dim xmlDoc As New XmlDocument()

            Try
                xmlDoc.Load(xmlReader)
                xmlReader.Close()
            Catch
                Return
            End Try

            Try
                Dim xmlNodes As XmlNode = xmlDoc.SelectSingleNode("Checkout")

                For Each node As XmlNode In xmlNodes
                    Select Case node.Name
                        Case "MerchantID"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szMerchantID = node.ChildNodes(0).Value
                                Else
                                    szMerchantID = ""
                                End If
                                Exit Select
                            End If
                        Case "MerchantName"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szMerchantName = node.ChildNodes(0).Value
                                Else
                                    szMerchantName = ""
                                End If
                                Exit Select
                            End If
                        Case "MerchantPymtID"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szMerchantTxnID = node.ChildNodes(0).Value
                                Else
                                    szMerchantTxnID = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 12)
                                End If
                                Exit Select
                            End If
                        Case "MerchantOrdID"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szMerchantOrdID = node.ChildNodes(0).Value
                                Else
                                    szMerchantOrdID = ""
                                End If
                                Exit Select
                            End If
                        Case "MerchantOrdDesc"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szMerchantOrdDesc = node.ChildNodes(0).Value
                                Else
                                    szMerchantOrdDesc = ""
                                End If
                                Exit Select
                            End If
                        Case "MerchantTxnAmt"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szMerchantTxnAmt = node.ChildNodes(0).Value
                                Else
                                    szMerchantTxnAmt = ""
                                End If
                                Exit Select
                            End If
                        Case "MerchantCurrCode"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szMerchantCurrCode = node.ChildNodes(0).Value
                                Else
                                    szMerchantCurrCode = ""
                                End If
                                Exit Select
                            End If
                        Case "AcqBank"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szAcqBank = node.ChildNodes(0).Value
                                Else
                                    szAcqBank = ""
                                End If
                                Exit Select
                            End If
                        Case "LanguageCode"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szLanguageCode = node.ChildNodes(0).Value
                                Else
                                    szLanguageCode = ""
                                End If
                                Exit Select
                            End If
                        Case "MerchantRURL"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szMerchantReturnURL = node.ChildNodes(0).Value
                                Else
                                    szMerchantReturnURL = ""
                                End If
                                Exit Select
                            End If
                        Case "MerchantApprovalURL"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szMerchantApprovalURL = node.ChildNodes(0).Value
                                Else
                                    szMerchantApprovalURL = ""
                                End If
                                Exit Select
                            End If
                        Case "MerchantUnApprovalURL"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szMerchantUnApprovalURL = node.ChildNodes(0).Value
                                Else
                                    szMerchantUnApprovalURL = ""
                                End If
                                Exit Select
                            End If
                        Case "MerchantCancelURL"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szMerchantCancelURL = node.ChildNodes(0).Value
                                Else
                                    szMerchantCancelURL = ""
                                End If
                                Exit Select
                            End If
                        Case "MerchantCallBackURL"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szMerchantCallBackURL = node.ChildNodes(0).Value
                                Else
                                    szMerchantCallBackURL = ""
                                End If
                                Exit Select
                            End If
                        Case "MerchantSupportURL"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szMerchantSupportURL = node.ChildNodes(0).Value
                                Else
                                    szMerchantSupportURL = ""
                                End If
                                Exit Select
                            End If
                        Case "Param1"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szParam1 = node.ChildNodes(0).Value
                                Else
                                    szParam1 = ""
                                End If
                                Exit Select
                            End If
                        Case "Param2"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szParam2 = node.ChildNodes(0).Value
                                Else
                                    szParam2 = ""
                                End If
                                Exit Select
                            End If
                        Case "Param3"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szParam3 = node.ChildNodes(0).Value
                                Else
                                    szParam3 = ""
                                End If
                                Exit Select
                            End If
                        Case "Param4"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szParam4 = node.ChildNodes(0).Value
                                Else
                                    szParam4 = ""
                                End If
                                Exit Select
                            End If
                        Case "Param5"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szParam5 = node.ChildNodes(0).Value
                                Else
                                    szParam5 = ""
                                End If
                                Exit Select
                            End If
                        Case "Param5"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szParam5 = node.ChildNodes(0).Value
                                Else
                                    szParam5 = ""
                                End If
                                Exit Select
                            End If
                        Case "Param6"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szParam6 = node.ChildNodes(0).Value
                                Else
                                    szParam6 = ""
                                End If
                                Exit Select
                            End If
                        Case "Param7"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szParam7 = node.ChildNodes(0).Value
                                Else
                                    szParam7 = ""
                                End If
                                Exit Select
                            End If
                        Case "Param8"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szParam8 = node.ChildNodes(0).Value
                                Else
                                    szParam8 = ""
                                End If
                                Exit Select
                            End If
                        Case "Param9"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szParam9 = node.ChildNodes(0).Value
                                Else
                                    szParam9 = ""
                                End If
                                Exit Select
                            End If
                        Case "Param10"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szParam10 = node.ChildNodes(0).Value
                                Else
                                    szParam10 = ""
                                End If
                                Exit Select
                            End If
                        Case "Param11"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szParam11 = node.ChildNodes(0).Value
                                Else
                                    szParam11 = ""
                                End If
                                Exit Select
                            End If
                        Case "Param12"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szParam12 = node.ChildNodes(0).Value
                                Else
                                    szParam12 = ""
                                End If
                                Exit Select
                            End If
                        Case "Param13"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szParam13 = node.ChildNodes(0).Value
                                Else
                                    szParam13 = ""
                                End If
                                Exit Select
                            End If
                        Case "Param14"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szParam14 = node.ChildNodes(0).Value
                                Else
                                    szParam14 = ""
                                End If
                                Exit Select
                            End If
                        Case "Param15"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szParam15 = node.ChildNodes(0).Value
                                Else
                                    szParam15 = ""
                                End If
                                Exit Select
                            End If
                        Case "CustName"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szCustName = node.ChildNodes(0).Value
                                Else
                                    szCustName = ""
                                End If
                                Exit Select
                            End If
                        Case "CustPhone"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szCustPhone = node.ChildNodes(0).Value
                                Else
                                    szCustPhone = ""
                                End If
                                Exit Select
                            End If
                        Case "CustEmail"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szCustEmail = node.ChildNodes(0).Value
                                Else
                                    szCustEmail = ""
                                End If
                                Exit Select
                            End If
                        Case "CardNo"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szCardNo = node.ChildNodes(0).Value
                                Else
                                    szCardNo = ""
                                End If
                                Exit Select
                            End If
                        Case "CardExp"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szCardExp = node.ChildNodes(0).Value
                                Else
                                    szCardExp = ""
                                End If
                                Exit Select
                            End If
                        Case "CVV2"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szCVV2 = node.ChildNodes(0).Value
                                Else
                                    szCVV2 = ""
                                End If
                                Exit Select
                            End If
                        Case "TokenType"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szTokenType = node.ChildNodes(0).Value
                                Else
                                    szTokenType = ""
                                End If
                                Exit Select
                            End If
                        Case "Token"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szToken = node.ChildNodes(0).Value
                                Else
                                    szToken = ""
                                End If
                                Exit Select
                            End If
                        Case "ThreeDFlag"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    sz3DFlag = node.ChildNodes(0).Value
                                Else
                                    sz3DFlag = ""
                                End If
                                Exit Select
                            End If
                        Case "ECI"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szECI = node.ChildNodes(0).Value
                                Else
                                    szECI = ""
                                End If
                                Exit Select
                            End If
                        Case "CAVV"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szCAVV = node.ChildNodes(0).Value
                                Else
                                    szCAVV = ""
                                End If
                                Exit Select
                            End If
                        Case "XID"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szXID = node.ChildNodes(0).Value
                                Else
                                    szXID = ""
                                End If
                                Exit Select
                            End If
                        Case "CustIP"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szCustIP = node.ChildNodes(0).Value
                                Else
                                    szCustIP = ""
                                End If
                                Exit Select
                            End If
                        Case "BillAddr"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szBillAddr = node.ChildNodes(0).Value
                                Else
                                    szBillAddr = ""
                                End If
                                Exit Select
                            End If
                        Case "BillCity"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szBillCity = node.ChildNodes(0).Value
                                Else
                                    szBillCity = ""
                                End If
                                Exit Select
                            End If
                        Case "BillRegion"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szBillRegion = node.ChildNodes(0).Value
                                Else
                                    szBillRegion = ""
                                End If
                                Exit Select
                            End If
                        Case "BillPostal"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szBillPostal = node.ChildNodes(0).Value
                                Else
                                    szBillPostal = ""
                                End If
                                Exit Select
                            End If
                        Case "BillCountry"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szBillCountry = node.ChildNodes(0).Value
                                Else
                                    szBillCountry = ""
                                End If
                                Exit Select
                            End If
                        Case "ShipAddr"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szShipAddr = node.ChildNodes(0).Value
                                Else
                                    szShipAddr = ""
                                End If
                                Exit Select
                            End If
                        Case "ShipCity"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szShipCity = node.ChildNodes(0).Value
                                Else
                                    szShipCity = ""
                                End If
                                Exit Select
                            End If
                        Case "ShipRegion"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szShipRegion = node.ChildNodes(0).Value
                                Else
                                    szShipRegion = ""
                                End If
                                Exit Select
                            End If
                        Case "ShipPostal"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szShipPostal = node.ChildNodes(0).Value
                                Else
                                    szShipPostal = ""
                                End If
                                Exit Select
                            End If
                        Case "ShipCountry"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szShipCountry = node.ChildNodes(0).Value
                                Else
                                    szShipCountry = ""
                                End If
                                Exit Select
                            End If
                        Case "EPPMonth"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szTermMth = node.ChildNodes(0).Value
                                Else
                                    szTermMth = ""
                                End If
                                Exit Select
                            End If
                        Case "GatewayID"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szGatewayID = node.ChildNodes(0).Value
                                Else
                                    szGatewayID = ""
                                End If
                                Exit Select
                            End If
                        Case "MerchantPassword"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szMerchantPassword = node.ChildNodes(0).Value
                                Else
                                    szMerchantPassword = ""
                                End If
                                Exit Select
                            End If
                        Case "PaymentMethod"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szPaymentMethod = node.ChildNodes(0).Value
                                Else
                                    szPaymentMethod = ""
                                End If
                                Exit Select
                            End If
                        Case "PaymentURL"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szPaymentURL = node.ChildNodes(0).Value
                                Else
                                    szPaymentURL = ""
                                End If
                                Exit Select
                            End If
                        Case "PageTimeout"
                            If True Then
                                If node.ChildNodes(0).Value <> "-" Then
                                    szPageTimeout = node.ChildNodes(0).Value
                                Else
                                    szPageTimeout = ""
                                End If
                                Exit Select
                            End If
                    End Select
                Next

                txtMerchantID.Text = szMerchantID
                txtMerchantName.Text = szMerchantName
                txtMerchantPymtID.Text = szMerchantTxnID
                txtMerchantOrdID.Text = szMerchantOrdID
                txtMerchantOrdDesc.Text = szMerchantOrdDesc
                txtMerchantTxnAmt.Text = szMerchantTxnAmt
                txtMerchantCurrCode.Text = szMerchantCurrCode
                txtAcqBank.Text = szAcqBank
                txtLangCode.Text = szLanguageCode
                txtMReturnURL.Text = szMerchantReturnURL
                txtMApprovalURL.Text = szMerchantApprovalURL
                txtMUnApprovalURL.Text = szMerchantUnApprovalURL
                txtMCancelURL.Text = szMerchantCancelURL
                txtMCallBackURL.Text = szMerchantCallBackURL
                txtMSupportURL.Text = szMerchantSupportURL
                txtParam1.Text = szParam1
                txtParam2.Text = szParam2
                txtParam3.Text = szParam3
                txtParam4.Text = szParam4
                txtParam5.Text = szParam5
                txtParam6.Text = szParam6
                txtParam7.Text = szParam7
                txtParam8.Text = szParam8
                txtParam9.Text = szParam9
                txtParam10.Text = szParam10
                txtParam11.Text = szParam11
                txtParam12.Text = szParam12
                txtParam13.Text = szParam13
                txtParam14.Text = szParam14
                txtParam15.Text = szParam15
                txtCustName.Text = szCustName
                txtCustPhone.Text = szCustPhone
                txtCustEmail.Text = szCustEmail
                txtCardNo.Text = szCardNo
                txtCardExp.Text = szCardExp
                txtCVV2.Text = szCVV2
                txtTokenType.Text = szTokenType
                txtToken.Text = szToken
                txt3DFlag.Text = sz3DFlag
                txtECI.Text = szECI
                txtCAVV.Text = szCAVV
                txtXID.Text = szXID
                txtCustIP.Text = szCustIP
                txtBillAddr.Text = szBillAddr
                txtBillCity.Text = szBillCity
                txtBillRegion.Text = szBillRegion
                txtBillPostal.Text = szBillPostal
                txtBillCountry.Text = szBillCountry
                txtShipAddr.Text = szShipAddr
                txtShipCity.Text = szShipCity
                txtShipRegion.Text = szShipRegion
                txtShipPostal.Text = szShipPostal
                txtShipCountry.Text = szShipCountry
                txtTermMth.Text = szTermMth
                txtInstallmentList.Text = szInstallmentList
                txtGatewayID.Text = szGatewayID
                txtMerchantPwd.Text = szMerchantPassword
                txtPaymentMethod.Text = szPaymentMethod
                txtPaymentURL.Text = szPaymentURL
                txtPageTimeout.Text = szPageTimeout

            Catch ex As Exception
                Response.Write("<script>alert('Page Load Error! " + ex.ToString() + "');</script>")
                Return
            End Try
        End If
    End Sub

    Protected Sub btnPay_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPay.Click
        Dim szHashKey As String = ""
        Dim szHashValue As String = ""
        Dim szPostMsg As String = ""

        Try
            'If ("OB" = txtPaymentMethod.Text) Then
            '    szPostMsg = "TxnType=PAY&MerchantID=" + txtMerchantID.Text + "&MerchantName=" + txtMerchantName.Text + _
            '    "&MerchantTxnID=" + txtMerchantPymtID.Text + "&MerchantOrdID=" + txtMerchantOrdID.Text + "&MerchantOrdDesc=" + txtMerchantOrdDesc.Text + _
            '    "&MerchantTxnAmt=" + txtMerchantTxnAmt.Text + "&MerchantCurrCode=" + txtMerchantCurrCode.Text + "&BaseMerchantTxnAmt=" + "&BaseMerchantCurrCode=" + _
            '    "&MerchantRURL=" + txtMReturnURL.Text + "&MerchantSupportURL=" + txtMSupportURL.Text + "&HashMethod=SHA1" + _
            '    "&Param1=" + txtParam1.Text + "&Param2=" + txtParam2.Text + "&Param3=" + txtParam3.Text + "&Param4=" + txtParam4.Text + _
            '    "&Param5=" + txtParam5.Text
            'Else
            szPostMsg = "TxnType=PAY&MerchantID=" + txtMerchantID.Text + "&MerchantPymtID=" + Server.UrlEncode(txtMerchantPymtID.Text) +
            "&MerchantOrdID=" + txtMerchantOrdID.Text + "&MerchantOrdDesc=" + txtMerchantOrdDesc.Text + "&MerchantName=" +
            txtMerchantName.Text + "&MerchantRURL=" + txtMReturnURL.Text + "&MerchantTxnAmt=" + txtMerchantTxnAmt.Text +
            "&MerchantCurrCode=" + txtMerchantCurrCode.Text + "&Param1=" + txtParam1.Text + "&Param2=" + txtParam2.Text +
            "&Param3=" + txtParam3.Text + "&Param4=" + txtParam4.Text + "&Param5=" + txtParam5.Text
            'End If

            If txtParam6.Text <> "" Then
                szPostMsg = szPostMsg + "&Param6=" + txtParam6.Text
            End If
            If txtParam7.Text <> "" Then
                szPostMsg = szPostMsg + "&Param7=" + txtParam7.Text
            End If
            If txtParam8.Text <> "" Then
                szPostMsg = szPostMsg + "&Param8=" + txtParam8.Text
            End If
            If txtParam9.Text <> "" Then
                szPostMsg = szPostMsg + "&Param9=" + txtParam9.Text
            End If
            If txtParam10.Text <> "" Then
                szPostMsg = szPostMsg + "&Param10=" + txtParam10.Text
            End If
            If txtParam11.Text <> "" Then
                szPostMsg = szPostMsg + "&Param11=" + txtParam11.Text
            End If
            If txtParam12.Text <> "" Then
                szPostMsg = szPostMsg + "&Param12=" + txtParam12.Text
            End If
            If txtParam13.Text <> "" Then
                szPostMsg = szPostMsg + "&Param13=" + txtParam13.Text
            End If
            If txtParam14.Text <> "" Then
                szPostMsg = szPostMsg + "&Param14=" + txtParam14.Text
            End If
            If txtParam15.Text <> "" Then
                szPostMsg = szPostMsg + "&Param15=" + txtParam15.Text
            End If
            If txtCustName.Text <> "" Then
                szPostMsg = szPostMsg + "&CustName=" + txtCustName.Text
            End If
            If txtCustPhone.Text <> "" Then
                szPostMsg = szPostMsg + "&CustPhone=" + txtCustPhone.Text
            End If
            If txtCustEmail.Text <> "" Then
                szPostMsg = szPostMsg + "&CustEmail=" + txtCustEmail.Text
            End If
            If txtCardNo.Text <> "" Then
                szPostMsg = szPostMsg + "&CardNo=" + txtCardNo.Text
            End If
            If txtCardExp.Text <> "" Then
                szPostMsg = szPostMsg + "&CardExp=" + txtCardExp.Text
            End If
            If txtCVV2.Text <> "" Then
                szPostMsg = szPostMsg + "&CardCVV2=" + txtCVV2.Text
            End If
            If txtTokenType.Text <> "" Then
                szPostMsg = szPostMsg + "&TokenType=" + txtTokenType.Text
            End If
            If txtToken.Text <> "" Then
                szPostMsg = szPostMsg + "&Token=" + Server.UrlEncode(txtToken.Text)
            End If
            If txt3DFlag.Text <> "" Then
                szPostMsg = szPostMsg + "&3DFlag=" + txt3DFlag.Text
            End If
            If txtECI.Text <> "" Then
                szPostMsg = szPostMsg + "&ECI=" + txtECI.Text
            End If
            If txtCAVV.Text <> "" Then
                szPostMsg = szPostMsg + "&CAVV=" + txtCAVV.Text
            End If
            If txtXID.Text <> "" Then
                szPostMsg = szPostMsg + "&3DXID=" + txtXID.Text
            End If
            If txtCustIP.Text <> "" Then
                szPostMsg = szPostMsg + "&CustIP=" + txtCustIP.Text
            End If
            If txtBillAddr.Text <> "" Then
                szPostMsg = szPostMsg + "&BillAddr=" + txtBillAddr.Text
            End If
            If txtBillCity.Text <> "" Then
                szPostMsg = szPostMsg + "&BillCity=" + txtBillCity.Text
            End If
            If txtBillRegion.Text <> "" Then
                szPostMsg = szPostMsg + "&BillRegion=" + txtBillRegion.Text
            End If
            If txtBillPostal.Text <> "" Then
                szPostMsg = szPostMsg + "&BillPostal=" + txtBillPostal.Text
            End If
            If txtBillCountry.Text <> "" Then
                szPostMsg = szPostMsg + "&BillCountry=" + txtBillCountry.Text
            End If
            If txtShipAddr.Text <> "" Then
                szPostMsg = szPostMsg + "&ShipAddr=" + txtShipAddr.Text
            End If
            If txtShipCity.Text <> "" Then
                szPostMsg = szPostMsg + "&ShipCity=" + txtShipCity.Text
            End If
            If txtShipRegion.Text <> "" Then
                szPostMsg = szPostMsg + "&ShipRegion=" + txtShipRegion.Text
            End If
            If txtShipPostal.Text <> "" Then
                szPostMsg = szPostMsg + "&ShipPostal=" + txtShipPostal.Text
            End If
            If txtShipCountry.Text <> "" Then
                szPostMsg = szPostMsg + "&ShipCountry=" + txtShipCountry.Text
            End If
            If txtPageTimeout.Text <> "" Then
                szPostMsg = szPostMsg + "&PageTimeout=" + txtPageTimeout.Text
            End If
            If txtAcqBank.Text <> "" Then
                szPostMsg = szPostMsg + "&AcqBank=" + txtAcqBank.Text
            End If
            If txtLangCode.Text <> "" Then
                szPostMsg = szPostMsg + "&LanguageCode=" + txtLangCode.Text
            End If
            If txtMApprovalURL.Text <> "" Then
                szPostMsg = szPostMsg + "&MerchantApprovalURL=" + txtMApprovalURL.Text
            End If
            If txtMUnApprovalURL.Text <> "" Then
                szPostMsg = szPostMsg + "&MerchantUnApprovalURL=" + txtMUnApprovalURL.Text
            End If
            If txtMCancelURL.Text <> "" Then
                szPostMsg = szPostMsg + "&MerchantCancelURL=" + txtMCancelURL.Text
            End If
            If txtMCallBackURL.Text <> "" Then
                szPostMsg = szPostMsg + "&MerchantCallBackURL=" + txtMCallBackURL.Text
            End If
            If txtMSupportURL.Text <> "" Then
                szPostMsg = szPostMsg + "&MerchantSupportURL=" + txtMSupportURL.Text
            End If
            If txtTermMth.Text <> "" Then
                szPostMsg = szPostMsg + "&EPPMonth=" + txtTermMth.Text
            End If
            If txtInstallmentList.Text <> "" Then
                szPostMsg = szPostMsg + "&InstallmentList=" + txtInstallmentList.Text
            End If
            If txtPaymentMethod.Text <> "" Then
                szPostMsg = szPostMsg + "&Method=" + txtPaymentMethod.Text
            End If


            'If ("OB" = txtPaymentMethod.Text) Then
            '    szHashKey = txtMerchantPwd.Text + txtMerchantID.Text + txtMerchantPymtID.Text + txtMReturnURL.Text + _
            '    txtMerchantTxnAmt.Text + txtMerchantCurrCode.Text

            '    szHashValue = szComputeSHA1Hash(szHashKey)
            If ("OB" = txtPaymentMethod.Text.ToUpper() Or "WA" = txtPaymentMethod.Text.ToUpper()) Then
                szHashKey = txtMerchantPwd.Text + "PAY" + txtMerchantID.Text + txtMerchantPymtID.Text + txtMerchantOrdID.Text + txtMReturnURL.Text +
                        txtMApprovalURL.Text + txtMUnApprovalURL.Text + txtMCancelURL.Text + txtMerchantTxnAmt.Text + txtMerchantCurrCode.Text +
                        txtCustIP.Text + txtPageTimeout.Text + txtMCallBackURL.Text

                'Log("D:\IIS\PGSimulator\log.txt", szHashKey)
            Else ''If txtPaymentMethod.Text = "CC" Then
                szHashKey = txtMerchantPwd.Text + "PAY" + txtMerchantID.Text + txtMerchantPymtID.Text + txtMerchantOrdID.Text + txtMReturnURL.Text +
                    txtMApprovalURL.Text + txtMUnApprovalURL.Text + txtMCancelURL.Text + txtMerchantTxnAmt.Text + txtMerchantCurrCode.Text +
                    txtCustIP.Text + txtPageTimeout.Text + txtMCallBackURL.Text + txtCardNo.Text + txtToken.Text
                'Log("E:\Logs\PGSGOM\log.txt", szHashKey)
            End If
            'Log("D:\IIS\PGSimulator\log.txt", szHashKey)
            szHashValue = szComputeSHA512Hash(szHashKey) 'szHashValue = szComputeSHA256Hash(szHashKey)

            '' ElseIf txtPaymentMethod.Text = "OB" Then
            'DDB Message
            'szPostMsg = "TxnType=PAY&MerchantID=" + TextBox1.Text + "&MerchantName=" + _
            'TextBox2.Text + "&MerchantTxnID=" + TextBox3.Text + "&MerchantOrdID=" + TextBox4.Text + _
            '"&MerchantOrdDesc=" + TextBox5.Text + "&MerchantTxnAmt=" + TextBox6.Text + "&MerchantCurrCode=" + _
            'TextBox7.Text + "&AcqBank=" + TextBox8.Text + "&LanguageCode=" + TextBox9.Text + _
            '"&MerchantRURL=" + TextBox10.Text + "&MerchantSupportURL=" + TextBox11.Text + _
            '"&HashMethod=SHA1" + "&Param1=" + TextBox12.Text + "&Param2=" + TextBox13.Text + _
            '"&Param3=" + TextBox14.Text + "&Param4=" + TextBox15.Text + "&Param5=" + _
            'TextBox16.Text + "&GatewayID=" + TextBox17.Text + "&Sign=" + szHashValue

            ''ElseIf txtPaymentMethod.Text = "WA" Then
            ''WA message
            ''End If

            szPostMsg = szPostMsg + "&Sign=" + szHashValue
            'MasterPass field 11 Jan 2017
            If txtReqToken.Text <> "" Then
                szPostMsg = szPostMsg + "&ReqToken=" + txtReqToken.Text
            End If
            If txtReqVerifier.Text <> "" Then
                szPostMsg = szPostMsg + "&ReqVerifier=" + txtReqVerifier.Text
            End If
            If txtPairingToken.Text <> "" Then
                szPostMsg = szPostMsg + "&PairingToken=" + txtPairingToken.Text
            End If
            If txtPairingVerifier.Text <> "" Then
                szPostMsg = szPostMsg + "&PairingVerifier=" + txtPairingVerifier.Text
            End If
            If txtCheckoutResourceURL.Text <> "" Then
                szPostMsg = szPostMsg + "&CheckoutResourceURL=" + txtCheckoutResourceURL.Text
            End If
            If txtCardId.Text <> "" Then
                szPostMsg = szPostMsg + "&CardId=" + txtCardId.Text
            End If
            If txtPreCheckoutId.Text <> "" Then
                szPostMsg = szPostMsg + "&PreCheckoutId=" + txtPreCheckoutId.Text
            End If
            If txtSettleTAID.Text <> "" Then
                szPostMsg = szPostMsg + "&SettleTAID=" + txtSettleTAID.Text
            End If
            If txtSettleAmount.Text <> "" Then
                szPostMsg = szPostMsg + "&SettleAmount=" + txtSettleAmount.Text
            End If
            If txtSettleTxnCount.Text <> "" Then
                szPostMsg = szPostMsg + "&SettleTxnCount=" + txtSettleTxnCount.Text
            End If
            If txtPromoCode.Text <> "" Then
                szPostMsg = szPostMsg + "&PromoCode=" + txtPromoCode.Text
            End If

            Dim xmlReader As XmlTextReader = New XmlTextReader(MapPath(ConfigurationManager.AppSettings("ConfigPath")))
            Dim xmlDoc As New XmlDocument()

            Try
                xmlDoc.Load(xmlReader)
                xmlReader.Close()
            Catch
                Response.Write("<script>alert('XML Read Error in before update new Txn into XML!');</script>")
                Return
            End Try

            Dim xmlNodes As XmlNode = xmlDoc.SelectSingleNode("Checkout")

            For Each node As XmlNode In xmlNodes
                Select Case node.Name
                    Case "MerchantID"
                        If True Then
                            If txtMerchantID.Text <> "" Then
                                node.ChildNodes(0).Value = txtMerchantID.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "MerchantName"
                        If True Then
                            If txtMerchantName.Text <> "" Then
                                node.ChildNodes(0).Value = txtMerchantName.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "MerchantPymtID"
                        If True Then
                            'node.ChildNodes(0).Value = "-"
                            If txtMerchantPymtID.Text <> "" Then
                                node.ChildNodes(0).Value = txtMerchantPymtID.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "MerchantOrdID"
                        If True Then
                            If txtMerchantOrdID.Text <> "" Then
                                node.ChildNodes(0).Value = txtMerchantOrdID.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "MerchantOrdDesc"
                        If True Then
                            If txtMerchantOrdDesc.Text <> "" Then
                                node.ChildNodes(0).Value = txtMerchantOrdDesc.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "MerchantTxnAmt"
                        If True Then
                            If txtMerchantTxnAmt.Text <> "" Then
                                node.ChildNodes(0).Value = txtMerchantTxnAmt.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "MerchantCurrCode"
                        If True Then
                            If txtMerchantCurrCode.Text <> "" Then
                                node.ChildNodes(0).Value = txtMerchantCurrCode.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "AcqBank"
                        If True Then
                            If txtAcqBank.Text <> "" Then
                                node.ChildNodes(0).Value = txtAcqBank.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "LanguageCode"
                        If True Then
                            If txtLangCode.Text <> "" Then
                                node.ChildNodes(0).Value = txtLangCode.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "MerchantRURL"
                        If True Then
                            If txtMReturnURL.Text <> "" Then
                                node.ChildNodes(0).Value = txtMReturnURL.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "MerchantApprovalURL"
                        If True Then
                            If txtMApprovalURL.Text <> "" Then
                                node.ChildNodes(0).Value = txtMApprovalURL.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "MerchantUnApprovalURL"
                        If True Then
                            If txtMUnApprovalURL.Text <> "" Then
                                node.ChildNodes(0).Value = txtMUnApprovalURL.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "MerchantCancelURL"
                        If True Then
                            If txtMCancelURL.Text <> "" Then
                                node.ChildNodes(0).Value = txtMCancelURL.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "MerchantCallBackURL"
                        If True Then
                            If txtMCallBackURL.Text <> "" Then
                                node.ChildNodes(0).Value = txtMCallBackURL.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "MerchantSupportURL"
                        If True Then
                            If txtMSupportURL.Text <> "" Then
                                node.ChildNodes(0).Value = txtMSupportURL.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param1"
                        If True Then
                            If txtParam1.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam1.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param2"
                        If True Then
                            If txtParam2.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam2.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param3"
                        If True Then
                            If txtParam3.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam3.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param4"
                        If True Then
                            If txtParam4.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam4.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param5"
                        If True Then
                            If txtParam5.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam5.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param6"
                        If True Then
                            If txtParam6.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam6.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param7"
                        If True Then
                            If txtParam7.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam7.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param8"
                        If True Then
                            If txtParam8.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam8.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param9"
                        If True Then
                            If txtParam9.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam9.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param10"
                        If True Then
                            If txtParam10.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam10.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param11"
                        If True Then
                            If txtParam11.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam11.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param12"
                        If True Then
                            If txtParam12.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam12.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param13"
                        If True Then
                            If txtParam13.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam13.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param14"
                        If True Then
                            If txtParam14.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam14.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param15"
                        If True Then
                            If txtParam15.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam15.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "CustName"
                        If True Then
                            If txtCustName.Text <> "" Then
                                node.ChildNodes(0).Value = txtCustName.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "CustPhone"
                        If True Then
                            If txtCustPhone.Text <> "" Then
                                node.ChildNodes(0).Value = txtCustPhone.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "CustEmail"
                        If True Then
                            If txtCustEmail.Text <> "" Then
                                node.ChildNodes(0).Value = txtCustEmail.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "CardNo"
                        If True Then
                            If txtCardNo.Text <> "" Then
                                node.ChildNodes(0).Value = txtCardNo.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "CardExp"
                        If True Then
                            If txtCardExp.Text <> "" Then
                                node.ChildNodes(0).Value = txtCardExp.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "CVV2"
                        If True Then
                            If txtCVV2.Text <> "" Then
                                node.ChildNodes(0).Value = txtCVV2.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "TokenType"
                        If True Then
                            If txtTokenType.Text <> "" Then
                                node.ChildNodes(0).Value = txtTokenType.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Token"
                        If True Then
                            If txtToken.Text <> "" Then
                                node.ChildNodes(0).Value = txtToken.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "ThreeDFlag"
                        If True Then
                            If txt3DFlag.Text <> "" Then
                                node.ChildNodes(0).Value = txt3DFlag.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "ECI"
                        If True Then
                            If txtECI.Text <> "" Then
                                node.ChildNodes(0).Value = txtECI.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "CAVV"
                        If True Then
                            If txtCAVV.Text <> "" Then
                                node.ChildNodes(0).Value = txtCAVV.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "XID"
                        If True Then
                            If txtXID.Text <> "" Then
                                node.ChildNodes(0).Value = txtXID.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "CustIP"
                        If True Then
                            If txtCustIP.Text <> "" Then
                                node.ChildNodes(0).Value = txtCustIP.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "BillAddr"
                        If True Then
                            If txtBillAddr.Text <> "" Then
                                node.ChildNodes(0).Value = txtBillAddr.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "BillCity"
                        If True Then
                            If txtBillCity.Text <> "" Then
                                node.ChildNodes(0).Value = txtBillCity.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "BillRegion"
                        If True Then
                            If txtBillRegion.Text <> "" Then
                                node.ChildNodes(0).Value = txtBillRegion.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "BillPostal"
                        If True Then
                            If txtBillPostal.Text <> "" Then
                                node.ChildNodes(0).Value = txtBillPostal.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "BillCountry"
                        If True Then
                            If txtBillCountry.Text <> "" Then
                                node.ChildNodes(0).Value = txtBillCountry.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "ShipAddr"
                        If True Then
                            If txtShipAddr.Text <> "" Then
                                node.ChildNodes(0).Value = txtShipAddr.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "ShipCity"
                        If True Then
                            If txtShipCity.Text <> "" Then
                                node.ChildNodes(0).Value = txtShipCity.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "ShipRegion"
                        If True Then
                            If txtShipRegion.Text <> "" Then
                                node.ChildNodes(0).Value = txtShipRegion.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "ShipPostal"
                        If True Then
                            If txtShipPostal.Text <> "" Then
                                node.ChildNodes(0).Value = txtShipPostal.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "ShipCountry"
                        If True Then
                            If txtShipCountry.Text <> "" Then
                                node.ChildNodes(0).Value = txtShipCountry.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "EPPMonth"
                        If True Then
                            If txtTermMth.Text <> "" Then
                                node.ChildNodes(0).Value = txtTermMth.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "InstallmentList"
                        If True Then
                            If txtInstallmentList.Text <> "" Then
                                node.ChildNodes(0).Value = txtInstallmentList.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "GatewayID"
                        If True Then
                            If txtGatewayID.Text <> "" Then
                                node.ChildNodes(0).Value = txtGatewayID.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "MerchantPassword"
                        If True Then
                            If txtMerchantPwd.Text <> "" Then
                                node.ChildNodes(0).Value = txtMerchantPwd.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "PaymentMethod"
                        If True Then
                            If txtPaymentMethod.Text <> "" Then
                                node.ChildNodes(0).Value = txtPaymentMethod.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "PaymentURL"
                        If True Then
                            If txtPaymentURL.Text <> "" Then
                                node.ChildNodes(0).Value = txtPaymentURL.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "PageTimeout"
                        If True Then
                            If txtPageTimeout.Text <> "" Then
                                node.ChildNodes(0).Value = txtPageTimeout.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                End Select
            Next

            xmlDoc.Save(MapPath(ConfigurationManager.AppSettings("ConfigPath")))

            Response.Redirect(txtPaymentURL.Text + szPostMsg)

        Catch ex As Exception
            Response.Write("<script>alert('Error! Possible XML permission not set!');</script>")
            Return
        End Try
    End Sub

    Public Sub btnQuery_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnQuery.Click

        Dim szHashKey As String = ""
        Dim szQueryString As String = ""
        Dim szHashValue As String = ""

        'If ("OB" = txtPaymentMethod.Text) Then
        '    szHashKey = txtMerchantPwd.Text + txtMerchantID.Text + txtMerchantPymtID.Text
        '    szHashValue = szComputeSHA1Hash(szHashKey)

        '    szQueryString = "TxnType=QUERY&Method=" + txtPaymentMethod.Text + "&MerchantID=" + txtMerchantID.Text + _
        '                    "&MerchantTxnID=" + txtMerchantPymtID.Text + "&MerchantTxnAmt=" + txtMerchantTxnAmt.Text + "&MerchantCurrCode=" + txtMerchantCurrCode.Text + _
        '                    "&HashMethod=SHA1" + "&Sign=" + szHashValue
        'Else 'cc
        szHashKey = txtMerchantPwd.Text + txtMerchantID.Text + txtMerchantPymtID.Text + txtMerchantTxnAmt.Text + txtMerchantCurrCode.Text
        szHashValue = szComputeSHA512Hash(szHashKey) 'szHashValue = szComputeSHA256Hash(szHashKey)

        szQueryString = "TxnType=QUERY&Method=" + txtPaymentMethod.Text + "&MerchantID=" + txtMerchantID.Text +
                        "&MerchantPymtID=" + txtMerchantPymtID.Text + "&MerchantTxnAmt=" + txtMerchantTxnAmt.Text + "&MerchantCurrCode=" + txtMerchantCurrCode.Text +
                        "&Sign=" + szHashValue
        'End If
        'szMerchantID + szMerchantTxnID + szMerchantTxnAmt + szMerchantCurrCode

        ''ElseIf txtPaymentMethod.Text = "OB" Then
        'DDB Message
        ''ElseIf txtPaymentMethod.Text = "WA" Then
        ''WA message
        ''End If

        Response.Redirect(txtPaymentURL.Text + szQueryString)

    End Sub

    Public Sub btnVoid_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnVoid.Click
        Dim szHashKey As String = ""
        Dim szQueryString As String = ""
        Dim szHashValue As String = ""

        ''If txtPaymentMethod.Text = "CC" Then
        szHashKey = txtMerchantPwd.Text + txtMerchantID.Text + txtMerchantPymtID.Text + txtMerchantTxnAmt.Text + txtMerchantCurrCode.Text
        szHashValue = szComputeSHA512Hash(szHashKey) 'szHashValue = szComputeSHA256Hash(szHashKey)

        szQueryString = "TxnType=VOID&Method=" + txtPaymentMethod.Text + "&MerchantID=" + txtMerchantID.Text +
                        "&MerchantPymtID=" + txtMerchantPymtID.Text + "&MerchantTxnAmt=" + txtMerchantTxnAmt.Text + "&MerchantCurrCode=" + txtMerchantCurrCode.Text +
                        "&Sign=" + szHashValue

        Response.Redirect(txtPaymentURL.Text + szQueryString)
    End Sub

    Public Sub btnAuth_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAuth.Click
        Dim szHashKey As String = ""
        Dim szHashValue As String = ""
        Dim szPostMsg As String = ""

        Try

            szPostMsg = "TxnType=AUTH&MerchantID=" + txtMerchantID.Text + "&MerchantPymtID=" + Server.UrlEncode(txtMerchantPymtID.Text) +
            "&MerchantOrdID=" + txtMerchantOrdID.Text + "&MerchantOrdDesc=" + txtMerchantOrdDesc.Text + "&MerchantName=" +
            txtMerchantName.Text + "&MerchantRURL=" + txtMReturnURL.Text + "&MerchantTxnAmt=" + txtMerchantTxnAmt.Text +
            "&MerchantCurrCode=" + txtMerchantCurrCode.Text + "&Param1=" + txtParam1.Text + "&Param2=" + txtParam2.Text +
            "&Param3=" + txtParam3.Text + "&Param4=" + txtParam4.Text + "&Param5=" + txtParam5.Text
            'End If
            'Log("E:\Logs\PGSGOM\log.txt", szPostMsg)
            If txtParam6.Text <> "" Then
                szPostMsg = szPostMsg + "&Param6=" + txtParam6.Text
            End If
            If txtParam7.Text <> "" Then
                szPostMsg = szPostMsg + "&Param7=" + txtParam7.Text
            End If
            If txtParam8.Text <> "" Then
                szPostMsg = szPostMsg + "&Param8=" + txtParam8.Text
            End If
            If txtParam9.Text <> "" Then
                szPostMsg = szPostMsg + "&Param9=" + txtParam9.Text
            End If
            If txtParam10.Text <> "" Then
                szPostMsg = szPostMsg + "&Param10=" + txtParam10.Text
            End If
            If txtParam11.Text <> "" Then
                szPostMsg = szPostMsg + "&Param11=" + txtParam11.Text
            End If
            If txtParam12.Text <> "" Then
                szPostMsg = szPostMsg + "&Param12=" + txtParam12.Text
            End If
            If txtParam13.Text <> "" Then
                szPostMsg = szPostMsg + "&Param13=" + txtParam13.Text
            End If
            If txtParam14.Text <> "" Then
                szPostMsg = szPostMsg + "&Param14=" + txtParam14.Text
            End If
            If txtParam15.Text <> "" Then
                szPostMsg = szPostMsg + "&Param15=" + txtParam15.Text
            End If
            If txtCustName.Text <> "" Then
                szPostMsg = szPostMsg + "&CustName=" + txtCustName.Text
            End If
            If txtCustPhone.Text <> "" Then
                szPostMsg = szPostMsg + "&CustPhone=" + txtCustPhone.Text
            End If
            If txtCustEmail.Text <> "" Then
                szPostMsg = szPostMsg + "&CustEmail=" + txtCustEmail.Text
            End If
            If txtCardNo.Text <> "" Then
                szPostMsg = szPostMsg + "&CardNo=" + txtCardNo.Text
            End If
            If txtCardExp.Text <> "" Then
                szPostMsg = szPostMsg + "&CardExp=" + txtCardExp.Text
            End If
            If txtCVV2.Text <> "" Then
                szPostMsg = szPostMsg + "&CardCVV2=" + txtCVV2.Text
            End If
            If txtTokenType.Text <> "" Then
                szPostMsg = szPostMsg + "&TokenType=" + txtTokenType.Text
            End If
            If txtToken.Text <> "" Then
                szPostMsg = szPostMsg + "&Token=" + txtToken.Text
            End If
            If txt3DFlag.Text <> "" Then
                szPostMsg = szPostMsg + "&3DFlag=" + txt3DFlag.Text
            End If
            If txtECI.Text <> "" Then
                szPostMsg = szPostMsg + "&ECI=" + txtECI.Text
            End If
            If txtCAVV.Text <> "" Then
                szPostMsg = szPostMsg + "&CAVV=" + txtCAVV.Text
            End If
            If txtXID.Text <> "" Then
                szPostMsg = szPostMsg + "&3DXID=" + txtXID.Text
            End If
            If txtCustIP.Text <> "" Then
                szPostMsg = szPostMsg + "&CustIP=" + txtCustIP.Text
            End If
            If txtBillAddr.Text <> "" Then
                szPostMsg = szPostMsg + "&BillAddr=" + txtBillAddr.Text
            End If
            If txtBillCity.Text <> "" Then
                szPostMsg = szPostMsg + "&BillCity=" + txtBillCity.Text
            End If
            If txtBillRegion.Text <> "" Then
                szPostMsg = szPostMsg + "&BillRegion=" + txtBillRegion.Text
            End If
            If txtBillPostal.Text <> "" Then
                szPostMsg = szPostMsg + "&BillPostal=" + txtBillPostal.Text
            End If
            If txtBillCountry.Text <> "" Then
                szPostMsg = szPostMsg + "&BillCountry=" + txtBillCountry.Text
            End If
            If txtShipAddr.Text <> "" Then
                szPostMsg = szPostMsg + "&ShipAddr=" + txtShipAddr.Text
            End If
            If txtShipCity.Text <> "" Then
                szPostMsg = szPostMsg + "&ShipCity=" + txtShipCity.Text
            End If
            If txtShipRegion.Text <> "" Then
                szPostMsg = szPostMsg + "&ShipRegion=" + txtShipRegion.Text
            End If
            If txtShipPostal.Text <> "" Then
                szPostMsg = szPostMsg + "&ShipPostal=" + txtShipPostal.Text
            End If
            If txtShipCountry.Text <> "" Then
                szPostMsg = szPostMsg + "&ShipCountry=" + txtShipCountry.Text
            End If
            If txtPageTimeout.Text <> "" Then
                szPostMsg = szPostMsg + "&PageTimeout=" + txtPageTimeout.Text
            End If
            If txtAcqBank.Text <> "" Then
                szPostMsg = szPostMsg + "&AcqBank=" + txtAcqBank.Text
            End If
            If txtLangCode.Text <> "" Then
                szPostMsg = szPostMsg + "&LanguageCode=" + txtLangCode.Text
            End If
            If txtMApprovalURL.Text <> "" Then
                szPostMsg = szPostMsg + "&MerchantApprovalURL=" + txtMApprovalURL.Text
            End If
            If txtMUnApprovalURL.Text <> "" Then
                szPostMsg = szPostMsg + "&MerchantUnApprovalURL=" + txtMUnApprovalURL.Text
            End If
            If txtMCallBackURL.Text <> "" Then
                szPostMsg = szPostMsg + "&MerchantCallBackURL=" + txtMCallBackURL.Text
            End If
            If txtMSupportURL.Text <> "" Then
                szPostMsg = szPostMsg + "&MerchantSupportURL=" + txtMSupportURL.Text
            End If

            szPostMsg = szPostMsg + "&Method=" + txtPaymentMethod.Text

            'If ("OB" = txtPaymentMethod.Text) Then
            '    szHashKey = txtMerchantPwd.Text + txtMerchantID.Text + txtMerchantPymtID.Text + txtMReturnURL.Text + _
            '    txtMerchantTxnAmt.Text + txtMerchantCurrCode.Text

            '    szHashValue = szComputeSHA1Hash(szHashKey)
            If ("OB" = txtPaymentMethod.Text.ToUpper() Or "WA" = txtPaymentMethod.Text.ToUpper()) Then
                szHashKey = txtMerchantPwd.Text + txtMerchantID.Text + txtMerchantPymtID.Text + txtMReturnURL.Text +
                        txtMApprovalURL.Text + txtMUnApprovalURL.Text + txtMCallBackURL.Text +
                        txtMerchantTxnAmt.Text + txtMerchantCurrCode.Text + txtCustIP.Text + txtPageTimeout.Text + ""
                'Log("D:\IIS\PGSimulator\log.txt", szHashKey)
            Else ''If txtPaymentMethod.Text = "CC" Then
                szHashKey = txtMerchantPwd.Text + txtMerchantID.Text + txtMerchantPymtID.Text + txtMReturnURL.Text +
                        txtMApprovalURL.Text + txtMUnApprovalURL.Text + txtMCallBackURL.Text +
                        txtMerchantTxnAmt.Text + txtMerchantCurrCode.Text + txtCustIP.Text + txtPageTimeout.Text + txtCardNo.Text +
                        txtToken.Text
                'Log("E:\Logs\PGSGOM\log.txt", szHashKey)
            End If
            'Log("D:\IIS\PGSimulator\log.txt", szHashKey)
            szHashValue = szComputeSHA512Hash(szHashKey) 'szHashValue = szComputeSHA256Hash(szHashKey)

            szPostMsg = szPostMsg + "&Sign=" + szHashValue

            Dim xmlReader As XmlTextReader = New XmlTextReader(MapPath(ConfigurationManager.AppSettings("ConfigPath")))
            Dim xmlDoc As New XmlDocument()
            'Log("E:\Logs\PGSGOM\log.txt", szPostMsg)
            Try
                xmlDoc.Load(xmlReader)
                xmlReader.Close()
            Catch
                Response.Write("<script>alert('XML Read Error in before update new Txn into XML!');</script>")
                Return
            End Try

            Dim xmlNodes As XmlNode = xmlDoc.SelectSingleNode("Checkout")

            For Each node As XmlNode In xmlNodes
                Select Case node.Name
                    Case "MerchantID"
                        If True Then
                            If txtMerchantID.Text <> "" Then
                                node.ChildNodes(0).Value = txtMerchantID.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "MerchantName"
                        If True Then
                            If txtMerchantName.Text <> "" Then
                                node.ChildNodes(0).Value = txtMerchantName.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "MerchantPymtID"
                        If True Then
                            If txtMerchantPymtID.Text <> "" Then
                                node.ChildNodes(0).Value = txtMerchantPymtID.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "MerchantOrdID"
                        If True Then
                            If txtMerchantOrdID.Text <> "" Then
                                node.ChildNodes(0).Value = txtMerchantOrdID.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "MerchantOrdDesc"
                        If True Then
                            If txtMerchantOrdDesc.Text <> "" Then
                                node.ChildNodes(0).Value = txtMerchantOrdDesc.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "MerchantTxnAmt"
                        If True Then
                            If txtMerchantTxnAmt.Text <> "" Then
                                node.ChildNodes(0).Value = txtMerchantTxnAmt.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "MerchantCurrCode"
                        If True Then
                            If txtMerchantCurrCode.Text <> "" Then
                                node.ChildNodes(0).Value = txtMerchantCurrCode.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "AcqBank"
                        If True Then
                            If txtAcqBank.Text <> "" Then
                                node.ChildNodes(0).Value = txtAcqBank.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "LanguageCode"
                        If True Then
                            If txtLangCode.Text <> "" Then
                                node.ChildNodes(0).Value = txtLangCode.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "MerchantRURL"
                        If True Then
                            If txtMReturnURL.Text <> "" Then
                                node.ChildNodes(0).Value = txtMReturnURL.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "MerchantApprovalURL"
                        If True Then
                            If txtMApprovalURL.Text <> "" Then
                                node.ChildNodes(0).Value = txtMApprovalURL.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "MerchantUnApprovalURL"
                        If True Then
                            If txtMUnApprovalURL.Text <> "" Then
                                node.ChildNodes(0).Value = txtMUnApprovalURL.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "MerchantCallBackURL"
                        If True Then
                            If txtMCallBackURL.Text <> "" Then
                                node.ChildNodes(0).Value = txtMCallBackURL.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "MerchantSupportURL"
                        If True Then
                            If txtMSupportURL.Text <> "" Then
                                node.ChildNodes(0).Value = txtMSupportURL.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param1"
                        If True Then
                            If txtParam1.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam1.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param2"
                        If True Then
                            If txtParam2.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam2.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param3"
                        If True Then
                            If txtParam3.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam3.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param4"
                        If True Then
                            If txtParam4.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam4.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param5"
                        If True Then
                            If txtParam5.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam5.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param6"
                        If True Then
                            If txtParam6.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam6.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param7"
                        If True Then
                            If txtParam7.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam7.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param8"
                        If True Then
                            If txtParam8.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam8.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param9"
                        If True Then
                            If txtParam9.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam9.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param10"
                        If True Then
                            If txtParam10.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam10.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param11"
                        If True Then
                            If txtParam11.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam11.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param12"
                        If True Then
                            If txtParam12.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam12.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param13"
                        If True Then
                            If txtParam13.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam13.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param14"
                        If True Then
                            If txtParam14.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam14.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param15"
                        If True Then
                            If txtParam15.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam15.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "CustName"
                        If True Then
                            If txtCustName.Text <> "" Then
                                node.ChildNodes(0).Value = txtCustName.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "CustPhone"
                        If True Then
                            If txtCustPhone.Text <> "" Then
                                node.ChildNodes(0).Value = txtCustPhone.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "CustEmail"
                        If True Then
                            If txtCustEmail.Text <> "" Then
                                node.ChildNodes(0).Value = txtCustEmail.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "CardNo"
                        If True Then
                            If txtCardNo.Text <> "" Then
                                node.ChildNodes(0).Value = txtCardNo.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "CardExp"
                        If True Then
                            If txtCardExp.Text <> "" Then
                                node.ChildNodes(0).Value = txtCardExp.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "CVV2"
                        If True Then
                            If txtCVV2.Text <> "" Then
                                node.ChildNodes(0).Value = txtCVV2.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "TokenType"
                        If True Then
                            If txtTokenType.Text <> "" Then
                                node.ChildNodes(0).Value = txtTokenType.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Token"
                        If True Then
                            If txtToken.Text <> "" Then
                                node.ChildNodes(0).Value = txtToken.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "ThreeDFlag"
                        If True Then
                            If txt3DFlag.Text <> "" Then
                                node.ChildNodes(0).Value = txt3DFlag.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "ECI"
                        If True Then
                            If txtECI.Text <> "" Then
                                node.ChildNodes(0).Value = txtECI.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "CAVV"
                        If True Then
                            If txtCAVV.Text <> "" Then
                                node.ChildNodes(0).Value = txtCAVV.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "XID"
                        If True Then
                            If txtXID.Text <> "" Then
                                node.ChildNodes(0).Value = txtXID.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "CustIP"
                        If True Then
                            If txtCustIP.Text <> "" Then
                                node.ChildNodes(0).Value = txtCustIP.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "BillAddr"
                        If True Then
                            If txtBillAddr.Text <> "" Then
                                node.ChildNodes(0).Value = txtBillAddr.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "BillCity"
                        If True Then
                            If txtBillCity.Text <> "" Then
                                node.ChildNodes(0).Value = txtBillCity.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "BillRegion"
                        If True Then
                            If txtBillRegion.Text <> "" Then
                                node.ChildNodes(0).Value = txtBillRegion.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "BillPostal"
                        If True Then
                            If txtBillPostal.Text <> "" Then
                                node.ChildNodes(0).Value = txtBillPostal.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "BillCountry"
                        If True Then
                            If txtBillCountry.Text <> "" Then
                                node.ChildNodes(0).Value = txtBillCountry.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "ShipAddr"
                        If True Then
                            If txtShipAddr.Text <> "" Then
                                node.ChildNodes(0).Value = txtShipAddr.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "ShipCity"
                        If True Then
                            If txtShipCity.Text <> "" Then
                                node.ChildNodes(0).Value = txtShipCity.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "ShipRegion"
                        If True Then
                            If txtShipRegion.Text <> "" Then
                                node.ChildNodes(0).Value = txtShipRegion.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "ShipPostal"
                        If True Then
                            If txtShipPostal.Text <> "" Then
                                node.ChildNodes(0).Value = txtShipPostal.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "ShipCountry"
                        If True Then
                            If txtShipCountry.Text <> "" Then
                                node.ChildNodes(0).Value = txtShipCountry.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "GatewayID"
                        If True Then
                            If txtGatewayID.Text <> "" Then
                                node.ChildNodes(0).Value = txtGatewayID.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "MerchantPassword"
                        If True Then
                            If txtMerchantPwd.Text <> "" Then
                                node.ChildNodes(0).Value = txtMerchantPwd.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "PaymentMethod"
                        If True Then
                            If txtPaymentMethod.Text <> "" Then
                                node.ChildNodes(0).Value = txtPaymentMethod.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "PaymentURL"
                        If True Then
                            If txtPaymentURL.Text <> "" Then
                                node.ChildNodes(0).Value = txtPaymentURL.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "PageTimeout"
                        If True Then
                            If txtPageTimeout.Text <> "" Then
                                node.ChildNodes(0).Value = txtPageTimeout.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                End Select
            Next
            'Log("E:\Logs\IPGSGOM\log.txt", szPostMsg)
            xmlDoc.Save(MapPath(ConfigurationManager.AppSettings("ConfigPath")))

            Response.Redirect(txtPaymentURL.Text + szPostMsg)

        Catch ex As Exception
            Response.Write("<script>alert('Error! Possible XML permission not set!');</script>")
            Return
        End Try
    End Sub

    Public Sub btnCapture_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCapture.Click
        Dim szHashKey As String = ""
        Dim szQueryString As String = ""
        Dim szHashValue As String = ""

        szHashKey = txtMerchantPwd.Text + txtMerchantID.Text + txtMerchantPymtID.Text + txtMerchantTxnAmt.Text + txtMerchantCurrCode.Text
        szHashValue = szComputeSHA512Hash(szHashKey) 'szHashValue = szComputeSHA256Hash(szHashKey)

        szQueryString = "TxnType=CAPTURE&Method=" + txtPaymentMethod.Text + "&MerchantID=" + txtMerchantID.Text +
                        "&MerchantPymtID=" + txtMerchantPymtID.Text + "&MerchantTxnAmt=" + txtMerchantTxnAmt.Text + "&MerchantCurrCode=" + txtMerchantCurrCode.Text +
                        "&Sign=" + szHashValue

        Response.Redirect(txtPaymentURL.Text + szQueryString)
    End Sub

    Public Sub btnRefund_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnRefund.Click
        Dim szHashKey As String = ""
        Dim szQueryString As String = ""
        Dim szHashValue As String = ""

        szHashKey = txtMerchantPwd.Text + txtMerchantID.Text + txtMerchantPymtID.Text + txtMerchantTxnAmt.Text + txtMerchantCurrCode.Text
        szHashValue = szComputeSHA512Hash(szHashKey) 'szHashValue = szComputeSHA256Hash(szHashKey)

        szQueryString = "TxnType=REFUND&Method=" + txtPaymentMethod.Text + "&MerchantID=" + txtMerchantID.Text +
                        "&MerchantPymtID=" + txtMerchantPymtID.Text + "&MerchantTxnAmt=" + txtMerchantTxnAmt.Text + "&MerchantCurrCode=" + txtMerchantCurrCode.Text +
                        "&Sign=" + szHashValue

        Response.Redirect(txtPaymentURL.Text + szQueryString)
    End Sub

    Public Function szComputeSHA1Hash(ByVal sz_Key As String) As String
        Dim szHashValue As String = ""
        Dim objSHA1 As SHA1CryptoServiceProvider = Nothing
        Dim btHashbuffer() As Byte

        Try
            objSHA1 = New SHA1CryptoServiceProvider
            objSHA1.ComputeHash(System.Text.Encoding.UTF8.GetBytes(sz_Key.ToCharArray))
            btHashbuffer = objSHA1.Hash
            szHashValue = System.Convert.ToBase64String(btHashbuffer)

        Catch ex As Exception
            Throw ex
        Finally
            If Not objSHA1 Is Nothing Then objSHA1 = Nothing
        End Try

        Return szHashValue
    End Function

    Public Function szWriteHex(ByVal bt_Array() As Byte) As String
        Dim szHex As String = ""
        Dim iCounter As Integer

        For iCounter = 0 To (bt_Array.Length() - 1)
            szHex += bt_Array(iCounter).ToString("X2")
        Next

        Return szHex
    End Function

    Public Function szComputeSHA256Hash(ByVal sz_Key As String) As String
        Dim szHashValue As String = ""
        Dim objSHA256 As SHA256 = Nothing
        Dim btHashbuffer() As Byte

        Try
            objSHA256 = New SHA256Managed
            objSHA256.ComputeHash(System.Text.Encoding.ASCII.GetBytes(sz_Key))
            btHashbuffer = objSHA256.Hash
            szHashValue = szWriteHex(btHashbuffer).ToLower()

        Catch ex As Exception
            Throw ex
        Finally
            If Not objSHA256 Is Nothing Then objSHA256 = Nothing
        End Try

        Return szHashValue
    End Function

    Public Function szComputeSHA512Hash(ByVal sz_Key As String) As String
        Dim szHashValue As String = ""
        Dim objSHA512 As SHA512 = Nothing
        Dim btHashbuffer() As Byte = Nothing

        Try
            objSHA512 = New SHA512Managed
            objSHA512.ComputeHash(System.Text.Encoding.ASCII.GetBytes(sz_Key))
            btHashbuffer = objSHA512.Hash
            szHashValue = szWriteHex(btHashbuffer).ToLower()

        Catch ex As Exception
            Throw ex
        Finally
            If Not objSHA512 Is Nothing Then objSHA512 = Nothing
        End Try

        Return szHashValue
    End Function

    Protected Sub cbShipAddr_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles cbShipAddr.CheckedChanged
        If cbShipAddr.Checked = True Then
            pnShip.Visible = True
        Else
            pnShip.Visible = False
        End If
    End Sub

    Protected Sub cbParam_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles cbParam.CheckedChanged
        If cbParam.Checked = True Then
            pnParam.Visible = True
        Else
            pnParam.Visible = False
        End If
    End Sub

    Private Sub Log(ByVal sz_LogFile As String, ByVal sz_Text As String)

        Dim objStreamWriter As StreamWriter
        Dim szLogFile(2) As String
        Dim szLogPath As String = ""

        If InStr(sz_LogFile, ".") > 0 Then
            szLogFile = Split(sz_LogFile, ".")
            szLogPath = szLogFile(0) + "_" + DateTime.Now.ToString("yyyyMMdd") + "." + szLogFile(1)
        Else
            szLogPath = sz_LogFile + "_" + DateTime.Now.ToString("yyyyMMdd")
        End If

        objStreamWriter = New StreamWriter(szLogPath, True)
        objStreamWriter.WriteLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + " >>> " + sz_Text)
        objStreamWriter.Close()

    End Sub

    Protected Sub btnSOP_Click(sender As Object, e As EventArgs) Handles btnSOP.Click
        Dim szHashKey As String = ""
        Dim szHashValue As String = ""
        Dim szPostMsg As String = ""

        Try
            'If ("OB" = txtPaymentMethod.Text) Then
            '    szPostMsg = "TxnType=PAY&MerchantID=" + txtMerchantID.Text + "&MerchantName=" + txtMerchantName.Text + _
            '    "&MerchantTxnID=" + txtMerchantPymtID.Text + "&MerchantOrdID=" + txtMerchantOrdID.Text + "&MerchantOrdDesc=" + txtMerchantOrdDesc.Text + _
            '    "&MerchantTxnAmt=" + txtMerchantTxnAmt.Text + "&MerchantCurrCode=" + txtMerchantCurrCode.Text + "&BaseMerchantTxnAmt=" + "&BaseMerchantCurrCode=" + _
            '    "&MerchantRURL=" + txtMReturnURL.Text + "&MerchantSupportURL=" + txtMSupportURL.Text + "&HashMethod=SHA1" + _
            '    "&Param1=" + txtParam1.Text + "&Param2=" + txtParam2.Text + "&Param3=" + txtParam3.Text + "&Param4=" + txtParam4.Text + _
            '    "&Param5=" + txtParam5.Text
            'Else
            szPostMsg = "TxnType=SOPTR&MerchantID=" + txtMerchantID.Text + "&MerchantPymtID=" + Server.UrlEncode(txtMerchantPymtID.Text) +
            "&MerchantOrdID=" + txtMerchantOrdID.Text + "&MerchantOrdDesc=" + txtMerchantOrdDesc.Text + "&MerchantName=" +
            txtMerchantName.Text + "&MerchantRURL=" + txtMReturnURL.Text + "&MerchantTxnAmt=" + txtMerchantTxnAmt.Text +
            "&MerchantCurrCode=" + txtMerchantCurrCode.Text + "&Param1=" + txtParam1.Text + "&Param2=" + txtParam2.Text +
            "&Param3=" + txtParam3.Text + "&Param4=" + txtParam4.Text + "&Param5=" + txtParam5.Text
            'End If

            If txtParam6.Text <> "" Then
                szPostMsg = szPostMsg + "&Param6=" + txtParam6.Text
            End If
            If txtParam7.Text <> "" Then
                szPostMsg = szPostMsg + "&Param7=" + txtParam7.Text
            End If
            If txtParam8.Text <> "" Then
                szPostMsg = szPostMsg + "&Param8=" + txtParam8.Text
            End If
            If txtParam9.Text <> "" Then
                szPostMsg = szPostMsg + "&Param9=" + txtParam9.Text
            End If
            If txtParam10.Text <> "" Then
                szPostMsg = szPostMsg + "&Param10=" + txtParam10.Text
            End If
            If txtParam11.Text <> "" Then
                szPostMsg = szPostMsg + "&Param11=" + txtParam11.Text
            End If
            If txtParam12.Text <> "" Then
                szPostMsg = szPostMsg + "&Param12=" + txtParam12.Text
            End If
            If txtParam13.Text <> "" Then
                szPostMsg = szPostMsg + "&Param13=" + txtParam13.Text
            End If
            If txtParam14.Text <> "" Then
                szPostMsg = szPostMsg + "&Param14=" + txtParam14.Text
            End If
            If txtParam15.Text <> "" Then
                szPostMsg = szPostMsg + "&Param15=" + txtParam15.Text
            End If
            If txtCustName.Text <> "" Then
                szPostMsg = szPostMsg + "&CustName=" + txtCustName.Text
            End If
            If txtCustPhone.Text <> "" Then
                szPostMsg = szPostMsg + "&CustPhone=" + txtCustPhone.Text
            End If
            If txtCustEmail.Text <> "" Then
                szPostMsg = szPostMsg + "&CustEmail=" + txtCustEmail.Text
            End If
            If txtCardNo.Text <> "" Then
                szPostMsg = szPostMsg + "&CardNo=" + txtCardNo.Text
            End If
            If txtCardExp.Text <> "" Then
                szPostMsg = szPostMsg + "&CardExp=" + txtCardExp.Text
            End If
            If txtCVV2.Text <> "" Then
                szPostMsg = szPostMsg + "&CardCVV2=" + txtCVV2.Text
            End If
            If txtTokenType.Text <> "" Then
                szPostMsg = szPostMsg + "&TokenType=" + txtTokenType.Text
            End If
            If txtToken.Text <> "" Then
                szPostMsg = szPostMsg + "&Token=" + Server.UrlEncode(txtToken.Text)
            End If
            If txt3DFlag.Text <> "" Then
                szPostMsg = szPostMsg + "&3DFlag=" + txt3DFlag.Text
            End If
            If txtECI.Text <> "" Then
                szPostMsg = szPostMsg + "&ECI=" + txtECI.Text
            End If
            If txtCAVV.Text <> "" Then
                szPostMsg = szPostMsg + "&CAVV=" + txtCAVV.Text
            End If
            If txtXID.Text <> "" Then
                szPostMsg = szPostMsg + "&3DXID=" + txtXID.Text
            End If
            If txtCustIP.Text <> "" Then
                szPostMsg = szPostMsg + "&CustIP=" + txtCustIP.Text
            End If
            If txtBillAddr.Text <> "" Then
                szPostMsg = szPostMsg + "&BillAddr=" + txtBillAddr.Text
            End If
            If txtBillCity.Text <> "" Then
                szPostMsg = szPostMsg + "&BillCity=" + txtBillCity.Text
            End If
            If txtBillRegion.Text <> "" Then
                szPostMsg = szPostMsg + "&BillRegion=" + txtBillRegion.Text
            End If
            If txtBillPostal.Text <> "" Then
                szPostMsg = szPostMsg + "&BillPostal=" + txtBillPostal.Text
            End If
            If txtBillCountry.Text <> "" Then
                szPostMsg = szPostMsg + "&BillCountry=" + txtBillCountry.Text
            End If
            If txtShipAddr.Text <> "" Then
                szPostMsg = szPostMsg + "&ShipAddr=" + txtShipAddr.Text
            End If
            If txtShipCity.Text <> "" Then
                szPostMsg = szPostMsg + "&ShipCity=" + txtShipCity.Text
            End If
            If txtShipRegion.Text <> "" Then
                szPostMsg = szPostMsg + "&ShipRegion=" + txtShipRegion.Text
            End If
            If txtShipPostal.Text <> "" Then
                szPostMsg = szPostMsg + "&ShipPostal=" + txtShipPostal.Text
            End If
            If txtShipCountry.Text <> "" Then
                szPostMsg = szPostMsg + "&ShipCountry=" + txtShipCountry.Text
            End If
            If txtPageTimeout.Text <> "" Then
                szPostMsg = szPostMsg + "&PageTimeout=" + txtPageTimeout.Text
            End If
            If txtAcqBank.Text <> "" Then
                szPostMsg = szPostMsg + "&AcqBank=" + txtAcqBank.Text
            End If
            If txtLangCode.Text <> "" Then
                szPostMsg = szPostMsg + "&LanguageCode=" + txtLangCode.Text
            End If
            If txtMApprovalURL.Text <> "" Then
                szPostMsg = szPostMsg + "&MerchantApprovalURL=" + txtMApprovalURL.Text
            End If
            If txtMUnApprovalURL.Text <> "" Then
                szPostMsg = szPostMsg + "&MerchantUnApprovalURL=" + txtMUnApprovalURL.Text
            End If
            If txtMCancelURL.Text <> "" Then
                szPostMsg = szPostMsg + "&MerchantCancelURL=" + txtMCancelURL.Text
            End If
            If txtMCallBackURL.Text <> "" Then
                szPostMsg = szPostMsg + "&MerchantCallBackURL=" + txtMCallBackURL.Text
            End If
            If txtMSupportURL.Text <> "" Then
                szPostMsg = szPostMsg + "&MerchantSupportURL=" + txtMSupportURL.Text
            End If
            If txtTermMth.Text <> "" Then
                szPostMsg = szPostMsg + "&EPPMonth=" + txtTermMth.Text
            End If
            If txtInstallmentList.Text <> "" Then
                szPostMsg = szPostMsg + "&InstallmentList=" + txtInstallmentList.Text
            End If
            If txtPaymentMethod.Text <> "" Then
                szPostMsg = szPostMsg + "&Method=" + txtPaymentMethod.Text
            End If


            'If ("OB" = txtPaymentMethod.Text) Then
            '    szHashKey = txtMerchantPwd.Text + txtMerchantID.Text + txtMerchantPymtID.Text + txtMReturnURL.Text + _
            '    txtMerchantTxnAmt.Text + txtMerchantCurrCode.Text

            '    szHashValue = szComputeSHA1Hash(szHashKey)
            If ("OB" = txtPaymentMethod.Text.ToUpper() Or "WA" = txtPaymentMethod.Text.ToUpper()) Then
                szHashKey = txtMerchantPwd.Text + "SOPTR" + txtMerchantID.Text + txtMerchantPymtID.Text + txtMerchantOrdID.Text + txtMReturnURL.Text +
                        txtMApprovalURL.Text + txtMUnApprovalURL.Text + txtMCancelURL.Text + txtMerchantTxnAmt.Text + txtMerchantCurrCode.Text +
                        txtCustIP.Text + txtPageTimeout.Text + txtMCallBackURL.Text

                'Log("D:\IIS\PGSimulator\log.txt", szHashKey)
            Else ''If txtPaymentMethod.Text = "CC" Then
                szHashKey = txtMerchantPwd.Text + "SOPTR" + txtMerchantID.Text + txtMerchantPymtID.Text + txtMerchantOrdID.Text + txtMReturnURL.Text +
                    txtMApprovalURL.Text + txtMUnApprovalURL.Text + txtMCancelURL.Text + txtMerchantTxnAmt.Text + txtMerchantCurrCode.Text +
                    txtCustIP.Text + txtPageTimeout.Text + txtMCallBackURL.Text + txtCardNo.Text + txtToken.Text
                'Log("E:\Logs\PGSGOM\log.txt", szHashKey)
            End If
            'Log("D:\IIS\PGSimulator\log.txt", szHashKey)
            szHashValue = szComputeSHA512Hash(szHashKey) 'szHashValue = szComputeSHA256Hash(szHashKey)

            '' ElseIf txtPaymentMethod.Text = "OB" Then
            'DDB Message
            'szPostMsg = "TxnType=PAY&MerchantID=" + TextBox1.Text + "&MerchantName=" + _
            'TextBox2.Text + "&MerchantTxnID=" + TextBox3.Text + "&MerchantOrdID=" + TextBox4.Text + _
            '"&MerchantOrdDesc=" + TextBox5.Text + "&MerchantTxnAmt=" + TextBox6.Text + "&MerchantCurrCode=" + _
            'TextBox7.Text + "&AcqBank=" + TextBox8.Text + "&LanguageCode=" + TextBox9.Text + _
            '"&MerchantRURL=" + TextBox10.Text + "&MerchantSupportURL=" + TextBox11.Text + _
            '"&HashMethod=SHA1" + "&Param1=" + TextBox12.Text + "&Param2=" + TextBox13.Text + _
            '"&Param3=" + TextBox14.Text + "&Param4=" + TextBox15.Text + "&Param5=" + _
            'TextBox16.Text + "&GatewayID=" + TextBox17.Text + "&Sign=" + szHashValue

            ''ElseIf txtPaymentMethod.Text = "WA" Then
            ''WA message
            ''End If

            szPostMsg = szPostMsg + "&Sign=" + szHashValue
            'MasterPass field 11 Jan 2017
            If txtReqToken.Text <> "" Then
                szPostMsg = szPostMsg + "&ReqToken=" + txtReqToken.Text
            End If
            If txtReqVerifier.Text <> "" Then
                szPostMsg = szPostMsg + "&ReqVerifier=" + txtReqVerifier.Text
            End If
            If txtPairingToken.Text <> "" Then
                szPostMsg = szPostMsg + "&PairingToken=" + txtPairingToken.Text
            End If
            If txtPairingVerifier.Text <> "" Then
                szPostMsg = szPostMsg + "&PairingVerifier=" + txtPairingVerifier.Text
            End If
            If txtCheckoutResourceURL.Text <> "" Then
                szPostMsg = szPostMsg + "&CheckoutResourceURL=" + txtCheckoutResourceURL.Text
            End If
            If txtCardId.Text <> "" Then
                szPostMsg = szPostMsg + "&CardId=" + txtCardId.Text
            End If
            If txtPreCheckoutId.Text <> "" Then
                szPostMsg = szPostMsg + "&PreCheckoutId=" + txtPreCheckoutId.Text
            End If
            If txtSettleTAID.Text <> "" Then
                szPostMsg = szPostMsg + "&SettleTAID=" + txtSettleTAID.Text
            End If
            If txtSettleAmount.Text <> "" Then
                szPostMsg = szPostMsg + "&SettleAmount=" + txtSettleAmount.Text
            End If
            If txtSettleTxnCount.Text <> "" Then
                szPostMsg = szPostMsg + "&SettleTxnCount=" + txtSettleTxnCount.Text
            End If
            If txtPromoCode.Text <> "" Then
                szPostMsg = szPostMsg + "&PromoCode=" + txtPromoCode.Text
            End If

            Dim xmlReader As XmlTextReader = New XmlTextReader(MapPath(ConfigurationManager.AppSettings("ConfigPath")))
            Dim xmlDoc As New XmlDocument()

            Try
                xmlDoc.Load(xmlReader)
                xmlReader.Close()
            Catch
                Response.Write("<script>alert('XML Read Error in before update new Txn into XML!');</script>")
                Return
            End Try

            Dim xmlNodes As XmlNode = xmlDoc.SelectSingleNode("Checkout")

            For Each node As XmlNode In xmlNodes
                Select Case node.Name
                    Case "MerchantID"
                        If True Then
                            If txtMerchantID.Text <> "" Then
                                node.ChildNodes(0).Value = txtMerchantID.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "MerchantName"
                        If True Then
                            If txtMerchantName.Text <> "" Then
                                node.ChildNodes(0).Value = txtMerchantName.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "MerchantPymtID"
                        If True Then
                            'node.ChildNodes(0).Value = "-"
                            If txtMerchantPymtID.Text <> "" Then
                                node.ChildNodes(0).Value = txtMerchantPymtID.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "MerchantOrdID"
                        If True Then
                            If txtMerchantOrdID.Text <> "" Then
                                node.ChildNodes(0).Value = txtMerchantOrdID.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "MerchantOrdDesc"
                        If True Then
                            If txtMerchantOrdDesc.Text <> "" Then
                                node.ChildNodes(0).Value = txtMerchantOrdDesc.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "MerchantTxnAmt"
                        If True Then
                            If txtMerchantTxnAmt.Text <> "" Then
                                node.ChildNodes(0).Value = txtMerchantTxnAmt.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "MerchantCurrCode"
                        If True Then
                            If txtMerchantCurrCode.Text <> "" Then
                                node.ChildNodes(0).Value = txtMerchantCurrCode.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "AcqBank"
                        If True Then
                            If txtAcqBank.Text <> "" Then
                                node.ChildNodes(0).Value = txtAcqBank.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "LanguageCode"
                        If True Then
                            If txtLangCode.Text <> "" Then
                                node.ChildNodes(0).Value = txtLangCode.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "MerchantRURL"
                        If True Then
                            If txtMReturnURL.Text <> "" Then
                                node.ChildNodes(0).Value = txtMReturnURL.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "MerchantApprovalURL"
                        If True Then
                            If txtMApprovalURL.Text <> "" Then
                                node.ChildNodes(0).Value = txtMApprovalURL.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "MerchantUnApprovalURL"
                        If True Then
                            If txtMUnApprovalURL.Text <> "" Then
                                node.ChildNodes(0).Value = txtMUnApprovalURL.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "MerchantCancelURL"
                        If True Then
                            If txtMCancelURL.Text <> "" Then
                                node.ChildNodes(0).Value = txtMCancelURL.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "MerchantCallBackURL"
                        If True Then
                            If txtMCallBackURL.Text <> "" Then
                                node.ChildNodes(0).Value = txtMCallBackURL.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "MerchantSupportURL"
                        If True Then
                            If txtMSupportURL.Text <> "" Then
                                node.ChildNodes(0).Value = txtMSupportURL.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param1"
                        If True Then
                            If txtParam1.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam1.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param2"
                        If True Then
                            If txtParam2.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam2.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param3"
                        If True Then
                            If txtParam3.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam3.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param4"
                        If True Then
                            If txtParam4.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam4.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param5"
                        If True Then
                            If txtParam5.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam5.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param6"
                        If True Then
                            If txtParam6.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam6.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param7"
                        If True Then
                            If txtParam7.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam7.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param8"
                        If True Then
                            If txtParam8.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam8.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param9"
                        If True Then
                            If txtParam9.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam9.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param10"
                        If True Then
                            If txtParam10.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam10.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param11"
                        If True Then
                            If txtParam11.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam11.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param12"
                        If True Then
                            If txtParam12.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam12.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param13"
                        If True Then
                            If txtParam13.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam13.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param14"
                        If True Then
                            If txtParam14.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam14.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Param15"
                        If True Then
                            If txtParam15.Text <> "" Then
                                node.ChildNodes(0).Value = txtParam15.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "CustName"
                        If True Then
                            If txtCustName.Text <> "" Then
                                node.ChildNodes(0).Value = txtCustName.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "CustPhone"
                        If True Then
                            If txtCustPhone.Text <> "" Then
                                node.ChildNodes(0).Value = txtCustPhone.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "CustEmail"
                        If True Then
                            If txtCustEmail.Text <> "" Then
                                node.ChildNodes(0).Value = txtCustEmail.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "CardNo"
                        If True Then
                            If txtCardNo.Text <> "" Then
                                node.ChildNodes(0).Value = txtCardNo.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "CardExp"
                        If True Then
                            If txtCardExp.Text <> "" Then
                                node.ChildNodes(0).Value = txtCardExp.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "CVV2"
                        If True Then
                            If txtCVV2.Text <> "" Then
                                node.ChildNodes(0).Value = txtCVV2.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "TokenType"
                        If True Then
                            If txtTokenType.Text <> "" Then
                                node.ChildNodes(0).Value = txtTokenType.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "Token"
                        If True Then
                            If txtToken.Text <> "" Then
                                node.ChildNodes(0).Value = txtToken.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "ThreeDFlag"
                        If True Then
                            If txt3DFlag.Text <> "" Then
                                node.ChildNodes(0).Value = txt3DFlag.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "ECI"
                        If True Then
                            If txtECI.Text <> "" Then
                                node.ChildNodes(0).Value = txtECI.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "CAVV"
                        If True Then
                            If txtCAVV.Text <> "" Then
                                node.ChildNodes(0).Value = txtCAVV.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "XID"
                        If True Then
                            If txtXID.Text <> "" Then
                                node.ChildNodes(0).Value = txtXID.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "CustIP"
                        If True Then
                            If txtCustIP.Text <> "" Then
                                node.ChildNodes(0).Value = txtCustIP.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "BillAddr"
                        If True Then
                            If txtBillAddr.Text <> "" Then
                                node.ChildNodes(0).Value = txtBillAddr.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "BillCity"
                        If True Then
                            If txtBillCity.Text <> "" Then
                                node.ChildNodes(0).Value = txtBillCity.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "BillRegion"
                        If True Then
                            If txtBillRegion.Text <> "" Then
                                node.ChildNodes(0).Value = txtBillRegion.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "BillPostal"
                        If True Then
                            If txtBillPostal.Text <> "" Then
                                node.ChildNodes(0).Value = txtBillPostal.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "BillCountry"
                        If True Then
                            If txtBillCountry.Text <> "" Then
                                node.ChildNodes(0).Value = txtBillCountry.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "ShipAddr"
                        If True Then
                            If txtShipAddr.Text <> "" Then
                                node.ChildNodes(0).Value = txtShipAddr.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "ShipCity"
                        If True Then
                            If txtShipCity.Text <> "" Then
                                node.ChildNodes(0).Value = txtShipCity.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "ShipRegion"
                        If True Then
                            If txtShipRegion.Text <> "" Then
                                node.ChildNodes(0).Value = txtShipRegion.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "ShipPostal"
                        If True Then
                            If txtShipPostal.Text <> "" Then
                                node.ChildNodes(0).Value = txtShipPostal.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "ShipCountry"
                        If True Then
                            If txtShipCountry.Text <> "" Then
                                node.ChildNodes(0).Value = txtShipCountry.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "EPPMonth"
                        If True Then
                            If txtTermMth.Text <> "" Then
                                node.ChildNodes(0).Value = txtTermMth.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "InstallmentList"
                        If True Then
                            If txtInstallmentList.Text <> "" Then
                                node.ChildNodes(0).Value = txtInstallmentList.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "GatewayID"
                        If True Then
                            If txtGatewayID.Text <> "" Then
                                node.ChildNodes(0).Value = txtGatewayID.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "MerchantPassword"
                        If True Then
                            If txtMerchantPwd.Text <> "" Then
                                node.ChildNodes(0).Value = txtMerchantPwd.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "PaymentMethod"
                        If True Then
                            If txtPaymentMethod.Text <> "" Then
                                node.ChildNodes(0).Value = txtPaymentMethod.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "PaymentURL"
                        If True Then
                            If txtPaymentURL.Text <> "" Then
                                node.ChildNodes(0).Value = txtPaymentURL.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                    Case "PageTimeout"
                        If True Then
                            If txtPageTimeout.Text <> "" Then
                                node.ChildNodes(0).Value = txtPageTimeout.Text
                            Else
                                node.ChildNodes(0).Value = "-"
                            End If
                            Exit Select
                        End If
                End Select
            Next

            xmlDoc.Save(MapPath(ConfigurationManager.AppSettings("ConfigPath")))

            Response.Redirect(txtPaymentURL.Text + szPostMsg)

        Catch ex As Exception
            Response.Write("<script>alert('Error! Possible XML permission not set!');</script>")
            Return
        End Try
    End Sub
    <System.Web.Services.WebMethod()> _
    Public Shared Function CreatePayment() As String
        Return "id: Message from server."
    End Function
    'Async Function bHTTP() As Threading.Tasks.Task(Of Boolean)
    '    Dim bRet As Boolean = False

    '    Dim values = New Dictionary(Of String, String) From {{"TxnType", "PAY"}, {"MerchantID", "OM"}}

    '    Dim content = New Http.FormUrlEncodedContent(values)

    '    Dim response = Await HTTPclient.PostAsync(txtPaymentURL.Text, content)
    '    'Dim GetResp = Await HTTPclient.GetStringAsync(txtPaymentURL.Text)

    '    Dim responseString = Await response.Content.ReadAsStringAsync()


    '    Return bRet
    'End Function


End Class