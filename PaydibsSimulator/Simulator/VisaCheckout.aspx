﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="VisaCheckout.aspx.vb" Inherits="Simulator.VisaCheckout" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>IPG VisaCheckout Simulator</title>
</head>
<body>
<div class="v-checkout-wrapper">
    <img alt="Visa Checkout" class="v-button" role="button" style="cursor:pointer;" src="https://sandbox.secure.checkout.visa.com/wallet-services-web/xo/button.png" height="41"/>
	<script type="text/javascript" src="https://sandbox-assets.secure.checkout.visa.com/checkout-widget/resources/js/integration/v1/sdk.js"></script>
	<br/>
	<a class="v-learn v-learn-default" aria-label="Learn more about Visa Checkout" href="#" data-locale="en_US">Tell Me More</a> 
</div>
</body>
</html>
