<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Simulator.aspx.vb" Inherits="IPGSimulator.Simulator" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html>
	<head id="Head1" runat="server">
		<title>Internet Payment Gateway transactions Simulator</title>
		<style type="text/css">
        .style1 { WIDTH: 450px }
            .style3
            {
                width: 255px;
            }
            .style4
            {
                width: 412px;
            }
            .style5
            {
                width: 252px;
            }
            </style>
	</head>
	<body bgcolor="#ffff99">
		<form id="form1" runat="server">
			<div align="center">
				<div>
					<b style="COLOR: #6666ff; FONT-SIZE: 30px">Internet Payment Gateway transactions Simulator</b>
				</div>
				<div align="center">
					<br />
					<asp:Button ID="btnSale" runat="server" Text="Send Sale" Width="100px" />
					<asp:Button ID="btnQuery" runat="server" Text="Send Query" Width="100px" />
					<asp:Button ID="btnRSale" runat="server" Text="Send RSale" Width="100px" />
				</div>
				<br />
				<table class="style1" align="center" border="1">
					<tr>
						<td class="style4">
							<asp:Label ID="Label1" runat="server" Text="ServiceID"></asp:Label>
						</td>
						<td class="style3">
						    <asp:TextBox ID="txtSvcID" runat="server" Width="250px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style4">
							<asp:Label ID="Label2" runat="server" Text="MerchantName"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtMerchantName" runat="server" Width="250px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style4">
							<asp:Label ID="Label3" runat="server" Text="MerchantTxnID"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtTxnID" runat="server" Width="250px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style4">
							<asp:Label ID="Label4" runat="server" Text="OrderNumber"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtOrderNum" runat="server" Width="250px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style4">
							<asp:Label ID="Label5" runat="server" Text="OrderDesc"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtOrderDesc" runat="server" Width="250px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style4">
							<asp:Label ID="Label6" runat="server" Text="TxnAmount"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtTxnAmt" runat="server" Width="250px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style4">
							<asp:Label ID="Label7" runat="server" Text="CurrencyCode"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtCurrCode" runat="server" Width="250px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style4">
							<asp:Label ID="Label8" runat="server" Text="IssuingBank"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtIssuingBank" runat="server" Width="250px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style4">
							<asp:Label ID="Label9" runat="server" Text="LanguageCode"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtLangCode" runat="server" Width="250px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style4">
							<asp:Label ID="Label10" runat="server" Text="MerchantReturnURL"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtMReturnURL" runat="server" Width="250px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style4">
							<asp:Label ID="Label11" runat="server" Text="MerchantSupportURL"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtMSupportURL" runat="server" Width="250px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style4">
							<asp:Label ID="Label12" runat="server" Text="Param1"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtParam1" runat="server" Width="250px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style4">
							<asp:Label ID="Label13" runat="server" Text="Param2 / CardNo"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtParam2" runat="server" Width="250px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style4">
							<asp:Label ID="Label14" runat="server" Text="Param3 / ExpDate"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtParam3" runat="server" Width="250px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style4">
							<asp:Label ID="Label15" runat="server" Text="Param4 / CardHolder"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtParam4" runat="server" Width="250px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style4">
							<asp:Label ID="Label16" runat="server" Text="Param5"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtParam5" runat="server" Width="250px"></asp:TextBox>
						</td>
					</tr>
				</table>
				
				<table class="style1" align="center" border="1">
				    <tr>
					    <td class="style4">
					        <asp:CheckBox OnCheckedChanged="cbParam_CheckedChanged" ID="cbParam" runat="server" Text="More Params" AutoPostBack="true" />
					    </td>
					</tr>
				</table>
					
				<asp:Panel id="pnParam" runat="server" Visible="False" >
					<table class="style1" align="center" border="1">
					    <tr>
						    <td class="style4">
							    <asp:Label ID="Label20" runat="server" Text="Param6"></asp:Label>
						    </td>
						    <td class="style3">
							    <asp:TextBox ID="txtParam6" runat="server" Width="250px"></asp:TextBox>
						    </td>
					    </tr>
					    <tr>
						    <td class="style4">
							    <asp:Label ID="Label21" runat="server" Text="Param7"></asp:Label>
						    </td>
						    <td class="style3">
							    <asp:TextBox ID="txtParam7" runat="server" Width="250px"></asp:TextBox>
						    </td>
					    </tr>
					    <tr>
						    <td class="style4">
							    <asp:Label ID="Label22" runat="server" Text="Param8"></asp:Label>
						    </td>
						    <td class="style3">
							    <asp:TextBox ID="txtParam8" runat="server" Width="250px"></asp:TextBox>
						    </td>
					    </tr>
					    <tr>
						    <td class="style4">
							    <asp:Label ID="Label23" runat="server" Text="Param9"></asp:Label>
						    </td>
						    <td class="style3">
							    <asp:TextBox ID="txtParam9" runat="server" Width="250px"></asp:TextBox>
						    </td>
					    </tr>
					    <tr>
						    <td class="style4">
							    <asp:Label ID="Label24" runat="server" Text="Param10"></asp:Label>
						    </td>
						    <td class="style3">
							    <asp:TextBox ID="txtParam10" runat="server" Width="250px"></asp:TextBox>
						    </td>
					    </tr>
					    <tr>
						    <td class="style4">
							    <asp:Label ID="Label25" runat="server" Text="Param11"></asp:Label>
						    </td>
						    <td class="style3">
							    <asp:TextBox ID="txtParam11" runat="server" Width="250px"></asp:TextBox>
						    </td>
					    </tr>
					    <tr>
						    <td class="style4">
							    <asp:Label ID="Label26" runat="server" Text="Param12"></asp:Label>
						    </td>
						    <td class="style3">
							    <asp:TextBox ID="txtParam12" runat="server" Width="250px"></asp:TextBox>
						    </td>
					    </tr>
					    <tr>
						    <td class="style4">
							    <asp:Label ID="Label27" runat="server" Text="Param13"></asp:Label>
						    </td>
						    <td class="style3">
							    <asp:TextBox ID="txtParam13" runat="server" Width="250px"></asp:TextBox>
						    </td>
					    </tr>
					    <tr>
						    <td class="style4">
							    <asp:Label ID="Label28" runat="server" Text="Param14"></asp:Label>
						    </td>
						    <td class="style3">
							    <asp:TextBox ID="txtParam14" runat="server" Width="250px"></asp:TextBox>
						    </td>
					    </tr>
					    <tr>
						    <td class="style4">
							    <asp:Label ID="Label29" runat="server" Text="Param15"></asp:Label>
						    </td>
						    <td class="style3">
							    <asp:TextBox ID="txtParam15" runat="server" Width="250px"></asp:TextBox>
						    </td>
					    </tr>
					</table>
				</asp:Panel>
				
				<table class="style1" align="center" border="1">
					<tr>
						<td class="style4">
							<asp:Label ID="Label30" runat="server" Text="CVV2"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtCVV2" runat="server" Width="250px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style4">
							<asp:Label ID="Label31" runat="server" Text="BillAddr"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtBillAddr" runat="server" Width="250px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style4">
							<asp:Label ID="Label32" runat="server" Text="BillCity"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtBillCity" runat="server" Width="250px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style4">
							<asp:Label ID="Label33" runat="server" Text="BillRegion"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtBillRegion" runat="server" Width="250px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style4">
							<asp:Label ID="Label34" runat="server" Text="BillPostal"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtBillPostal" runat="server" Width="250px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style4">
							<asp:Label ID="Label35" runat="server" Text="BillCountry"></asp:Label>
						</td>
						<td class="style3">
							<asp:TextBox ID="txtBillCountry" runat="server" Width="250px"></asp:TextBox>
						</td>
					</tr>
				</table>
				
				<table class="style1" align="center" border="1">
				    <tr>
					    <td class="style4">
					        <asp:CheckBox OnCheckedChanged="cbShipAddr_CheckedChanged" ID="cbShipAddr" runat="server" Text="Shipping Address" AutoPostBack="true" />
					    </td>
					</tr>
				</table>
								
				<asp:Panel ID="pnShip" runat="server"  Visible="False">
				    <table class="style1" align="center" border="1">
					    <tr>
						    <td class="style4">
							    <asp:Label ID="Label37" runat="server" Text="ShipAddr"></asp:Label>
						    </td>
						    <td class="style3">
							    <asp:TextBox ID="txtShipAddr" runat="server" Width="250px"></asp:TextBox>
						    </td>
					    </tr>
					    <tr>
						    <td class="style4">
							    <asp:Label ID="Label38" runat="server" Text="ShipCity"></asp:Label>
						    </td>
						    <td class="style3">
							    <asp:TextBox ID="txtShipCity" runat="server" Width="250px"></asp:TextBox>
						    </td>
					    </tr>
					    <tr>
						    <td class="style4">
							    <asp:Label ID="Label39" runat="server" Text="ShipRegion"></asp:Label>
						    </td>
						    <td class="style3">
							    <asp:TextBox ID="txtShipRegion" runat="server" Width="250px"></asp:TextBox>
						    </td>
					    </tr>
					    <tr>
						    <td class="style4">
							    <asp:Label ID="Label40" runat="server" Text="ShipPostal"></asp:Label>
						    </td>
						    <td class="style3">
							    <asp:TextBox ID="txtShipPostal" runat="server" Width="250px"></asp:TextBox>
						    </td>
					    </tr>
					    <tr>
						    <td class="style4">
							    <asp:Label ID="Label41" runat="server" Text="ShipCountry"></asp:Label>
						    </td>
						    <td class="style3">
							    <asp:TextBox ID="txtShipCountry" runat="server" Width="250px"></asp:TextBox>
						    </td>
					    </tr>
					</table>
				</asp:Panel>
					
				<table class="style1" align="center" border="1">
					<tr>
						<td class="style4">
							<asp:Label ID="Label17" runat="server" Text="GatewayID"></asp:Label>
						</td>
						<td class="style5">
							<asp:TextBox ID="txtGatewayID" runat="server" Width="250px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="style4">
							<asp:Label style="Z-INDEX: 0" id="Label18" runat="server">MerchantPassword</asp:Label></td>
						<td class="style5">
							<asp:TextBox ID="txtMerchantPwd" runat="server" style="Z-INDEX: 0" 
                                Width="250px"></asp:TextBox></td>
					</tr>
					<tr>
						<td class="style4">
							<asp:Label style="Z-INDEX: 0" id="Label36" runat="server">PaymentMethod</asp:Label></td>
						<td class="style5">
							<asp:TextBox ID="txtPaymentMethod" runat="server" style="Z-INDEX: 0" Width="250px"></asp:TextBox></td>
					</tr>
					<tr>
						<td class="style4">
							<asp:Label style="Z-INDEX: 0" id="Label19" runat="server">PaymentURL</asp:Label></td>
						<td class="style5">
							<asp:TextBox ID="txtPaymentURL" runat="server" style="Z-INDEX: 0" Width="250px"></asp:TextBox></td>
					</tr>
					<tr>
						<td class="style4">
							<asp:Label ID="Label42" runat="server" Text="PageTimeout (S)"></asp:Label>
						</td>
						<td class="style5">
							<asp:TextBox ID="txtPageTimeout" runat="server" Width="250px"></asp:TextBox>
						</td>
					</tr>
				</table>
			</div>
		</form>
	</body>
</html>
