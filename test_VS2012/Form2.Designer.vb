﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form2
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnDecrypt = New System.Windows.Forms.Button()
        Me.btnEncrypt = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txt_Output = New System.Windows.Forms.TextBox()
        Me.txt_Input = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txt_Amount_In = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txt_Amount_Out = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btnTransform = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnDecrypt
        '
        Me.btnDecrypt.Location = New System.Drawing.Point(173, 111)
        Me.btnDecrypt.Name = "btnDecrypt"
        Me.btnDecrypt.Size = New System.Drawing.Size(120, 34)
        Me.btnDecrypt.TabIndex = 14
        Me.btnDecrypt.Text = "Dencrypt"
        '
        'btnEncrypt
        '
        Me.btnEncrypt.Location = New System.Drawing.Point(20, 111)
        Me.btnEncrypt.Name = "btnEncrypt"
        Me.btnEncrypt.Size = New System.Drawing.Size(120, 34)
        Me.btnEncrypt.TabIndex = 13
        Me.btnEncrypt.Text = "Encrypt"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(36, 75)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(58, 20)
        Me.Label2.TabIndex = 12
        Me.Label2.Text = "Output"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(36, 40)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 20)
        Me.Label1.TabIndex = 11
        Me.Label1.Text = "Input"
        '
        'txt_Output
        '
        Me.txt_Output.Location = New System.Drawing.Point(148, 75)
        Me.txt_Output.Name = "txt_Output"
        Me.txt_Output.Size = New System.Drawing.Size(812, 26)
        Me.txt_Output.TabIndex = 10
        '
        'txt_Input
        '
        Me.txt_Input.Location = New System.Drawing.Point(148, 34)
        Me.txt_Input.Name = "txt_Input"
        Me.txt_Input.Size = New System.Drawing.Size(812, 26)
        Me.txt_Input.TabIndex = 9
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(36, 167)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(46, 20)
        Me.Label3.TabIndex = 16
        Me.Label3.Text = "Input"
        '
        'txt_Amount_In
        '
        Me.txt_Amount_In.Location = New System.Drawing.Point(148, 161)
        Me.txt_Amount_In.Name = "txt_Amount_In"
        Me.txt_Amount_In.Size = New System.Drawing.Size(812, 26)
        Me.txt_Amount_In.TabIndex = 15
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(36, 203)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(58, 20)
        Me.Label4.TabIndex = 18
        Me.Label4.Text = "Output"
        '
        'txt_Amount_Out
        '
        Me.txt_Amount_Out.Location = New System.Drawing.Point(148, 203)
        Me.txt_Amount_Out.Name = "txt_Amount_Out"
        Me.txt_Amount_Out.Size = New System.Drawing.Size(812, 26)
        Me.txt_Amount_Out.TabIndex = 17
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(173, 247)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(120, 34)
        Me.Button1.TabIndex = 20
        Me.Button1.Text = "Dencrypt"
        '
        'btnTransform
        '
        Me.btnTransform.Location = New System.Drawing.Point(20, 247)
        Me.btnTransform.Name = "btnTransform"
        Me.btnTransform.Size = New System.Drawing.Size(120, 34)
        Me.btnTransform.TabIndex = 19
        Me.btnTransform.Text = "Transform"
        '
        'Form2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1037, 311)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.btnTransform)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txt_Amount_Out)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txt_Amount_In)
        Me.Controls.Add(Me.btnDecrypt)
        Me.Controls.Add(Me.btnEncrypt)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txt_Output)
        Me.Controls.Add(Me.txt_Input)
        Me.Name = "Form2"
        Me.Text = "Form2"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnDecrypt As Button
    Friend WithEvents btnEncrypt As Button
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents txt_Output As TextBox
    Friend WithEvents txt_Input As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents txt_Amount_In As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents txt_Amount_Out As TextBox
    Friend WithEvents Button1 As Button
    Friend WithEvents btnTransform As Button
End Class
