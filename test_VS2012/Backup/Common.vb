Imports System.Text.RegularExpressions

'Module Common
Public Class Common

    Public Const C_TXN_SUCCESS_0 = "Transaction Successful"
    Public Const C_TXN_FAILED_1 = "Transaction Failed"
    Public Const C_COM_TIMEOUT_5 = "Timeout Out Failed"
    Public Const C_FAILED_HOTCARD_9 = "Hot Card"
    Public Const C_PENDING_RESP_30 = "Transaction Pending For Response"
    Public Const C_QUERY_HOST_33 = "Querying Host"
    Public Const C_FAILED_TO_SEND_46 = "Transaction Failed To Send"
    Public Const C_TXN_NOT_FOUND_21 = "Transaction Not Found"
    Public Const C_DB_UNKNOWN_ERROR_901 = "Failed to Connect DB"
    Public Const C_INVALID_HOST_902 = "Failed to Get Host Information"
    Public Const C_FAILED_SET_TXN_STATUS_905 = "Failed to Set Transaction Status"
    Public Const C_FAILED_STORE_TXN_906 = "Failed to Store Transaction" ' Failed bInsertNewTxn
    Public Const C_FAILED_UPDATE_TXN_RESP_907 = "Failed to Update Transaction Response"
    Public Const C_FAILED_FORM_REQ_914 = "Failed to Form Message"
    Public Const C_FAILED_COM_INIT_917 = "Failed to Initiate Communications"
    Public Const C_INVALID_MID_2901 = "Invalid Service ID"                  ' Failed bGetMerchant in bInitPayment
    Public Const C_COMMON_INVALID_FIELD_2902 = "Invalid Field"
    Public Const C_COMMON_MISSING_ELEMENT_2903 = "Missing Required Field"   ' Failed bPaymentMain
    Public Const C_INVALID_INPUT_2906 = "Invalid Input"         ' Failed bCheckUniqueTxn, bInitPayment
    Public Const C_INVALID_HOST_REPLY_2907 = "Invalid Host Reply"
    Public Const C_INVALID_SIGNATURE_2909 = "Invalid Hash Value"
    Public Const C_LATE_HOST_REPLY_2908 = "Late Host Reply"
    Public Const C_COMMON_UNKNOWN_ERROR_2999 = "Internal Error" 'Failed to GenTxnID, bInsertTxnResp, bCheckUniqueTxn, bInsertNewTxn, bProcessSale, bInitPayment

    Public Enum TXN_STATUS
        TXN_SUCCESS = 0                     '1
        TXN_FAILED = 1                      '4
        TXN_PENDING = 2                     'Processing
        TXN_PENDING_CONFIRM_BOOKING = 3     'Host returned approved for the payment, pending OpenSkies AddPymt reply OR trying to send AddPymt request to OpenSkies, need extend booking
        TXN_TIMEOUT_HOST_RETURN = 4         'Host no response for 1 hour 45 minutes, need extend booking, stop and generate exception report
        TXN_TIMEOUT_HOST_UNPROCESSED = 5    'Host's reply status is "not processed" for 1 hour 45 minutes, no need extend booking, no need exception report
        TIME_OUT_FAILED = 6                 'Txn existed in Request table for more than 2 hours
        TXN_HOST_UNPROCESSED = 7            'Host's reply status is "not processed"
        TXN_HOST_RETURN_TIMEOUT = 8         'Host no response/timeout
        TXN_REVERSED = 9

        PENDING_RESP = 30
        PENDING_REVERSAL = 31
        PROCESSING_RESP = 32
        QUERYING_HOST = 33
        E_FAILED_STORE_TXN = 906    ' Failed bInsertNewTxn
        E_INVALID_MERCHANT = 2901
        INVALID_INPUT = 2906        ' Failed GetData, bInitPayment, bPaymentMain
        INVALID_HOST_REPLY = 2907
        LATE_HOST_REPLY = 2908      ' Late reply from Host
        INVALID_HOST_IP = 2909
        INVALID_HOST_REPLY_GATEWAYTXNID = 2910
        INTERNAL_ERROR = 2999       ' Failed bInitPayment, bProcessSale
    End Enum

    Public Enum TXN_STATE
        INITIAL = 1             'Received request for a particular txn for the 1st time from AA Web or other merchants

        SENT_TO_HOST = 2        'Can be redirect through client's browser to Host,e.g.CIMB(popup) OR sent through WebComm to Host  'PENDING_PROCESS = 2
        SENT_2ND_ENTRY = 15     'Replied an entry page to client, e.g. UserID entry(BCA); Tokens info entry(Mandiri)
        RECV_2ND_ENTRY = 16     'e.g. Received UserID (BCA);Tokens info(Mandiri)                  '    'PROCESSING = 3
        RECV_FROM_HOST = 17     'Received reply from Host
        SENT_QUERY_HOST = 18    'Sent query request to Host
        RECV_QUERY_HOST = 19    'Received query reply from Host
        SENT_QUERY_OS = 20      'Sent query request to OpenSkies/NewSkies
        RECV_QUERY_OS = 21      'Received query reply from OpenSkies/NewSkies
        SENT_CONFIRM_OS = 22    'Sent Add/Confirm Payment request to OpenSkies/NewSkies
        RECV_CONFIRM_OS = 23    'Received Add/Confirm Payment reply from OpenSkies/NewSkies
        RECV_HOST_QUERY = 24    'Received Host's Query request, e.g. BCA
        SENT_HOST_QUERY = 25    'Sent Host's Query reply, e.g. BCA
        SENT_REPLY_CLIENT = 26  'Sent reply to client/merchant

        SUCCESS = 4
        FAILED = 5              ' Failed spInsTxnResp, bCheckUniqueTxn, bInsertNewTxn
        ABORT = 6               ' Failed GenTxnID, GetData, bPaymentMain
        ROLLBACK_PENDING = 7
        ROLLBACK_SUCCESS = 8
        COMMIT_PENDING = 9
        COMMIT = 10
        ROLLBACK_FAILED = 11
        COMMIT_FAILED = 12
    End Enum

    Public Enum TXN_Type
        SALE = 3
        REVERSAL = 13
    End Enum

    Public Enum ERROR_CODE
        E_SUCCESS = 0 '

        E_TXN_SUCCESSFUL = 1 'From NA--
        E_TXN_INITIATED = 2 'From NA--
        E_TXN_AUTHORIZED = 3 'From NA--
        E_TXN_FAILED = 4 'From NA--
        E_COM_TIMEOUT = 5 'From NA--
        E_TXN_REVERSED = 7 'From NA--
        E_TXN_VOIDED = 8 'From NA
        E_TXN_HOTCARD = 9

        E_TXN_PENDING_SALE_RESP = 30 'From NA
        E_TXN_PENDING_REVERSAL_RESP = 31 'From NA
        E_TXN_PENDING_VOID_RESP = 32 'From NA
        E_TXN_PENDING_AUTH_RESP = 33 'From NA

        E_INVALID_PAN = 101 'From NA
        E_INVALID_PAN_LENGTH = 102 'From NA
        E_INVALID_PAN_UNKNOWN = 103 'From NA
        E_INVALID_TRACK2_FORMAT = 104 'From NA

        E_DB_UNKNOWN_ERROR = 901 ' From NA--
        E_INVALID_HOST = 902 ' From NA--
        E_FAILED_GET_REV_RESP = 903 'From NA--
        E_FAILED_REVERSAL = 904 ' From NA--
        E_FAILED_SET_TXN_STATUS = 905 'From NA--
        E_FAILED_STORE_TXN = 906            ' Failed bInsertNewTxn
        E_FAILED_UPDATE_TXN_RESP = 907 ' From NA--
        E_FAILED_FORM_REQ = 914 ' From NA--
        E_FAILED_COM_INIT = 917 ' From NA--

        E_COMMON_INVALID_FIELD = 2902 '
        E_COMMON_MISSING_ELEMENT = 2903

        E_INVALID_INPUT = 2906              ' Failed bCheckUniqueTxn
        E_INVALID_HOST_REPLY = 2907
        E_INVALID_MID = 2901 '
        E_INVALID_SIGNATURE = 2909 ''

        E_COMMON_UNKNOWN_ERROR = 2999       ' Failed GenTxnID, bInsertTxnResp, bCheckUniqueTxn, bInsertNewTxn


        'E_PAN_AUTH_NOT_AVAILABLE = 16
        'E_PAN_NOT_PARTICIPATING = 17

        'E_DB_INVALID_FIELD = 101
        'E_DB_INVALID_TYPE = 102
        'E_DB_MISSING_FIELD = 106
        'E_DB_FAIL_INSERT = 111
        'E_DB_FAIL_UPDATE = 112
        'E_DB_FAIL_DELETE = 113
        'E_DB_FAIL_QUERY = 114

        'E_CERT_INVALID_FILE = 141
        'E_CERT_FILE_EXISTED = 142
        'E_CERT_SIGNATURE_FAILED = 143
        'E_CERT_CHAIN_INCOMPLETE = 144
        E_CERT_ERROR = 51 '

        E_COM_BAD_REQUEST = 41 '
        E_COM_UNAUTHORIZED_URL = 42
        E_COM_FORBIDDEN_URL = 43
        E_COM_URL_UNREACHABLE = 44 '
        E_COM_FILE_ERROR = 45

        'E_MERCHANT_DOES_NOT_EXIST = 241
        'E_MERCHANT_ALREADY_EXIST = 242
        'E_MERCHANT_INACTIVE = 243
        'E_MERCHANT_PWD_MISMATCH = 244
        'E_MERCHANT_PWD_FORMAT = 245
        'E_MERCHANT_ACQ_INVALID = 246
        'E_MERCHANT_INVALID_CURRENCY = 247

        E_CRYPTO_INIT_KEY = 51
        E_CRYPTO_FAIL = 52 ''
        E_CRYPTO_UNKNOWN_ERROR = 53

        'E_CRANGE_NOT_IN_RANGE = 331
        'E_CRANGE_INVALID_BIN = 332

        E_TXN_FAIL_GEN_ID = 61
        E_TXN_INVALID_STATE = 62 '
        E_TXN_NOT_FOUND = 63 '
        E_TXN_INVALID_AMOUNT = 64 '

        E_CAPTURE_FAILED = 500

    End Enum

    '************************************
    'Transaction Member variables
    '************************************
    Public Structure STPayInfo

        'Request fields
        Public szTxnType As String
        Public szServiceID As String
        Public szMerchantName As String
        Public szGatewayID As String
        Public szMerchantTxnID As String
        Public szOrderNumber As String
        Public szOrderDesc As String
        Public szTxnAmount As String
        Public szCurrencyCode As String
        Public szIssuingBank As String
        Public szLanguageCode As String
        Public szCardPAN As String
        Public szSessionID As String
        Public szMerchantReturnURL As String
        Public szMerchantSupportURL As String
        Public szParam1 As String
        Public szParam2 As String
        Public szParam3 As String
        Public szParam4 As String
        Public szParam5 As String
        Public szHashMethod As String
        Public szHashValue As String
        Public szHashKey As String

        Public szMaskedCardPAN As String
        Public szTxnID As String
        Public szMachineID As String
        Public szRecv2ndEntry As String     'e.g. BCA (UserID entry), Mandiri (Tokens info entry)

        Public iPayAmount As Double
        Public iTxnType As Integer
        Public szPostMethod As String
        Public szTxnDateTime As String      'Date Created

        'Merchant 
        Public szMerchantPassword As String
        Public szPaymentTemplate As String
        Public szErrorTemplate As String
        Public szRunningNo As String
        Public iAllowReversal As Integer
        Public iNeedAddOSPymt As Integer

        'Res to client
        Public iTxnStatus As Integer
        Public iTxnState As Integer
        Public iMerchantTxnStatus As Integer
        Public iQueryStatus As Integer
        Public iDuration As Integer
        'Res from Host
        Public szHostTxnID As String
        Public szRespCode As String
        Public szAuthCode As String
        Public szBankRespMsg As String
        Public szHostDate As String
        Public szHostTime As String
        Public szHostMID As String
        Public szHostTID As String
        Public szHostTxnStatus As String
        Public szHostTxnAmount As String
        Public szHostOrderNumber As String
        Public szHostGatewayTxnID As String
        Public szHostCurrencyCode As String

        'Res from Host - Action
        Public iAction As Integer
        Public bVerified As Boolean
        'Res from OpenSkies
        Public iOSRet As Integer
        Public iErrSet As Integer
        Public iErrNum As Integer
        'Http connection details
        Public iHttpTimeoutSeconds As Integer

        'Error
        Public szTxnMsg As String   ' RespMesg
        Public szQueryMsg As String
        Public szErrDesc As String
        Public szDate As String
        Public szTime As String

    End Structure

    '************************************
    'Transaction Member variables
    '************************************
    Public Structure STHost
        Public szTID As String
        Public szMID As String
        Public szServerCertName As String
        Public szHostTemplate As String
        Public szPaymentTemplate As String
        Public szSecondEntryTemplate As String
        Public szURL As String
        Public szQueryURL As String
        Public szReversalURL As String
        Public szAcknowledgementURL As String
        Public szReturnURL As String
        Public szReturnIPAddresses As String
        Public szDBConnStr As String
        Public szOSPymtCode As String
        Public szSendKey As String      'Secret key to form hash value for request to Host
        Public szReturnKey As String    'Secret key to form hash value for response from Host
        Public szHashMethod As String
        Public szHashValue As String
        Public szSecretKey As String
        Public szInitVector As String
        Public sz3DESAppKey As String
        Public sz3DESAppVector As String
        Public szReserved As String     'Reserved field to store Host reply, especially for the whole encrypted reply

        Public iChannelID As Integer
        Public iHostID As Integer
        Public iPortNumber As Integer
        Public iQueryPortNumber As Integer
        Public iReversalPortNumber As Integer
        Public iAcknowledgementPortNumber As Integer
        Public iTimeOut As Long
        Public iRequire2ndEntry As Integer
        Public iNeedReplyAcknowledgement As Integer
        Public iTxnStatusActionID As Integer
        Public iHostReplyMethod As Integer
        Public iGatewayTxnIDFormat As Integer
        Public iQueryFlag As Integer    ' Query flag to determine whether to query the host
        Public iIsActive As Integer     ' Flag to indicate whether Host is enabled or disabled
    End Structure
    'End Module

    Public Sub InitHostInfo(ByRef st_HostInfo As STHost)
        st_HostInfo.szTID = ""
        st_HostInfo.szMID = ""
        st_HostInfo.szServerCertName = ""
        st_HostInfo.szHostTemplate = ""
        st_HostInfo.szPaymentTemplate = ""
        st_HostInfo.szSecondEntryTemplate = ""
        st_HostInfo.szURL = ""
        st_HostInfo.szQueryURL = ""
        st_HostInfo.szReversalURL = ""
        st_HostInfo.szAcknowledgementURL = ""
        st_HostInfo.szReturnURL = ""
        st_HostInfo.szReturnIPAddresses = ""
        st_HostInfo.szDBConnStr = ""
        st_HostInfo.szOSPymtCode = ""
        st_HostInfo.szSendKey = ""
        st_HostInfo.szReturnKey = ""
        st_HostInfo.szHashMethod = ""
        st_HostInfo.szHashValue = ""
        st_HostInfo.szSecretKey = ""
        st_HostInfo.szInitVector = ""
        st_HostInfo.sz3DESAppKey = "3537146182309002FEB7EBCE55ED3BBA71BED7CEB1D18CAA" '"3537146182309002yeKterceSSED3ppAtibeDtceriDLHGAA"
        st_HostInfo.sz3DESAppVector = "35371461"
        st_HostInfo.szReserved = ""

        st_HostInfo.iChannelID = 0
        st_HostInfo.iHostID = 0
        st_HostInfo.iPortNumber = 0
        st_HostInfo.iQueryPortNumber = 0
        st_HostInfo.iReversalPortNumber = 0
        st_HostInfo.iAcknowledgementPortNumber = 0
        st_HostInfo.iTimeOut = 900  'seconds
        st_HostInfo.iRequire2ndEntry = 0
        st_HostInfo.iNeedReplyAcknowledgement = 0
        st_HostInfo.iTxnStatusActionID = 0
        st_HostInfo.iHostReplyMethod = 0
        st_HostInfo.iGatewayTxnIDFormat = 0
        st_HostInfo.iQueryFlag = 1
        st_HostInfo.iIsActive = 1
    End Sub

    Public Sub InitPayInfo(ByRef st_PayInfo As STPayInfo)

        st_PayInfo.szTxnType = ""
        st_PayInfo.szServiceID = ""
        st_PayInfo.szMerchantName = ""
        st_PayInfo.szGatewayID = ""
        st_PayInfo.szMerchantTxnID = ""
        st_PayInfo.szOrderNumber = ""
        st_PayInfo.szOrderDesc = ""
        st_PayInfo.szTxnAmount = ""
        st_PayInfo.szCurrencyCode = ""
        st_PayInfo.szIssuingBank = ""
        st_PayInfo.szLanguageCode = ""
        st_PayInfo.szCardPAN = ""
        st_PayInfo.szSessionID = ""
        st_PayInfo.szMerchantReturnURL = ""
        st_PayInfo.szMerchantSupportURL = ""
        st_PayInfo.szParam1 = ""
        st_PayInfo.szParam2 = ""
        st_PayInfo.szParam3 = ""
        st_PayInfo.szParam4 = ""
        st_PayInfo.szParam5 = ""
        st_PayInfo.szHashMethod = ""
        st_PayInfo.szHashValue = ""
        st_PayInfo.szHashKey = ""
        st_PayInfo.szPostMethod = ""
        st_PayInfo.szTxnDateTime = ""

        st_PayInfo.szMaskedCardPAN = ""
        st_PayInfo.szTxnID = ""
        st_PayInfo.szMachineID = ""
        st_PayInfo.szRecv2ndEntry = ""

        st_PayInfo.iTxnStatus = -1
        st_PayInfo.iTxnState = -1
        st_PayInfo.iMerchantTxnStatus = -1

        st_PayInfo.szHostTxnID = ""
        st_PayInfo.szRespCode = ""
        st_PayInfo.szAuthCode = ""
        st_PayInfo.szBankRespMsg = ""
        st_PayInfo.szHostDate = ""
        st_PayInfo.szHostTime = ""
        st_PayInfo.szHostMID = ""
        st_PayInfo.szHostTID = ""
        st_PayInfo.szHostTxnStatus = ""
        st_PayInfo.szHostTxnAmount = ""
        st_PayInfo.szHostOrderNumber = ""
        st_PayInfo.szHostGatewayTxnID = ""
        st_PayInfo.szHostCurrencyCode = ""

        st_PayInfo.iAction = 0
        st_PayInfo.bVerified = True    ' Added on 22 Apr 2009 for verification of host's replied fields

        st_PayInfo.iOSRet = 99
        st_PayInfo.iErrSet = -1
        st_PayInfo.iErrNum = -1
        st_PayInfo.iHttpTimeoutSeconds = 0

        st_PayInfo.iQueryStatus = 2    ' Query error
        st_PayInfo.iDuration = -1
        st_PayInfo.szTxnMsg = ""
        st_PayInfo.szQueryMsg = ""
        st_PayInfo.szErrDesc = ""

        st_PayInfo.szMerchantPassword = ""
        st_PayInfo.szPaymentTemplate = ""
        st_PayInfo.szErrorTemplate = ""
        st_PayInfo.szRunningNo = ""
        st_PayInfo.iAllowReversal = 0
        st_PayInfo.iNeedAddOSPymt = 0

        st_PayInfo.szDate = ""
        st_PayInfo.szTime = ""
    End Sub

    Public Function szFormatPlaceHolder(ByRef szOriText As String, ByVal szTextToFind As String, ByVal szTextToReplace As String)

        If InStr(szOriText, szTextToFind) Then
            If (szTextToReplace.Trim.Length > 0) Then
                szOriText = szOriText.Replace(szTextToFind, szTextToReplace)
            Else
                szOriText = szOriText.Replace(szTextToFind, "")
            End If
        End If

    End Function
End Class

