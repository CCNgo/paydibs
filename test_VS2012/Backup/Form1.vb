Imports System
Imports System.Reflection   'Assembly
Imports System.Text 'UTF8Encoding

'Imports Common
'Imports WebCommII

Imports System.IO
Imports System.Data
Imports System.Security.Cryptography
Imports System.Threading
Imports System.Text.RegularExpressions
Imports System.Web
Imports System.Xml
Imports Microsoft.VisualBasic.Strings
Imports JavaCrypto
Imports Crypto
Imports System.Net
Imports System.Net.Security

Public Class Form1
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Button1 As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.Button3 = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(0, 0)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "Button1"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(0, 49)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = "Button2"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(0, 78)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(75, 23)
        Me.Button3.TabIndex = 2
        Me.Button3.Text = "Button3"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(292, 273)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'LoadLibraryTest()
        'LoadConfig()
        HashTest()
        'EncodeTest()
        'SOAPClient()
        'StringTest()
        'CryptoTest()
        'bParseQueryString("B6961CF4A861679BF38E8F3D78A5A30233D1E1F6E9E2FD591B1729ED0E99FF815C1E98C3206D697E24FA1AED9407C1B5C270DEFC2F378824B056F1D65B81D872257565BE4EC79023B84A09E1EA5F5D305BFF358A36630EA1C8881175E728C52EF12CC5ADB418E30408E4338B420052BE029AFA5731DF7968EBDF714556C86141")
        'ASCIITest()
        'PBEWithMD5AndDES()
        'DateTimestamp()
        'IntegerTest()\
        ' HttpPostWithRecv()
        'HttpPostWithRecv1()
        'RemoveSpecChar()
        'ReadXML()

    End Sub

    Public Function bParseQueryString(ByVal sz_QueryString As String) As Boolean
        Dim objCommon As Common
        Dim stPayInfo As Common.STPayInfo
        Dim stHostInfo As Common.STHost

        Dim bReturn As Boolean = False

        Dim iLoop As Integer = 0
        Dim iCounter As Integer = 0
        Dim iIndex As Integer = 0
        Dim iField As Integer = 0
        Dim szConfigFile As String = ""
        Dim objStreamReader As StreamReader
        Dim szLine As String = ""
        Dim szReqOrRes As String = ""
        Dim szConfig() As String
        Dim szFieldSet
        Dim szFieldSets() As String
        Dim szField() As String
        Dim szFieldName() As String
        Dim szFieldValue() As String
        Dim szValue As String = ""
        Dim szHashValue As String = ""
        Dim aszValue() As String
        Dim szFieldEx As String = ""
        Dim szFormat As String = ""
        Dim szHashMethod As String = ""
        Dim szDelimiter
        Dim bFound As Boolean = False

        Try
            'Parse Host reply based on the respective Host configuration file
            szReqOrRes = "RES"
            szConfigFile = "D:\Projects\Clients\AirAsia\DirectDebit\Templates\pbb.txt"
            objStreamReader = System.IO.File.OpenText(szConfigFile)

            Do
                szLine = objStreamReader.ReadLine()
                szConfig = Split(szLine, "|")
                If ((szConfig(0).Trim().ToUpper() = "QUERY") And (szConfig(1).Trim().ToUpper() = szReqOrRes.ToUpper())) Then
                    bFound = True
                    Exit Do
                End If
            Loop Until (szLine Is Nothing)

            objStreamReader.Close()

            If (False = bFound) Then
                GoTo CleanUp
            End If

            'Gets all response field names and field values into array
            Select Case szConfig(2).Trim().ToUpper()
                Case "LF"
                    szDelimiter = vbLf
                Case "CR"
                    szDelimiter = vbCr
                Case "CRLF"
                    szDelimiter = vbCrLf
                Case Else
                    szDelimiter = szConfig(2).Trim()
            End Select

            If ("" = szDelimiter) Then
                GoTo CleanUp
            End If

            If (szConfig(2).Trim().ToUpper() <> "WS" And Mid(szConfig(2).Trim().ToUpper(), 1, 3) <> "GET" And szConfig(2).Trim().ToUpper() <> "POST") Then
                ' Splits as case NOT sensitive, e.g. <BR> is same as <br>
                szFieldSets = Split(sz_QueryString, szDelimiter, -1, CompareMethod.Text)

                iField = 0
                For Each szFieldSet In szFieldSets
                    szField = Split(szFieldSet, "=")
                    If (1 = szField.GetUpperBound(0)) Then
                        iField = iField + 1
                    End If
                Next

                ReDim szFieldName(iField - 1)
                ReDim szFieldValue(iField - 1)

                iField = 0
                For Each szFieldSet In szFieldSets
                    szField = Split(szFieldSet, "=")

                    If (1 = szField.GetUpperBound(0)) Then
                        szFieldName(iField) = szField(0)
                        szFieldValue(iField) = szField(1)
                        iField = iField + 1
                    End If
                Next
            End If

            'Parse text line of host reply message configuration and populate payment information
            'Txn Type|Request/Response?|Post Method|Host Field Name|Gateway Field Variable to be filled with Host's value|....
            'CIMB:QUERY|RES|GET|billAccountNo|[OrderNumber]|billReferenceNo|[GatewayTxnID]|amount|[TxnAmount]|paymentRefNo|[BankRefNo]|status|[TxnStatus]
            For iLoop = 3 To szConfig.GetUpperBound(0)
                If (0 <> (iLoop Mod 2)) Then
                    'Verify Host Field Name
                    If (InStr(szConfig(iLoop), "[") > 0) Or (InStr(szConfig(iLoop), "]") > 0) Then
                        GoTo CleanUp
                    End If

                    If ("[=" = Mid(szConfig(iLoop).Trim(), 1, 2)) Then
                        szValue = Mid(szConfig(iLoop).Trim(), 3, szConfig(iLoop).Trim().Length - 3)
                    ElseIf ("0" = szConfig(iLoop).Trim()) Then
                        'Retrieve the whole reply string
                        szValue = sz_QueryString
                    Else
                        bFound = False

                        If ("WS" = szConfig(2).Trim().ToUpper()) Then

                            If (szValue <> "") Then
                                bFound = True
                            End If
                        Else
                            For iField = 0 To szFieldName.GetUpperBound(0)
                                If (szConfig(iLoop).Trim().ToLower() = szFieldName(iField).ToLower()) Then
                                    szValue = szFieldValue(iField)
                                    bFound = True
                                    Exit For
                                End If
                            Next
                        End If

                        If (False = bFound) Then
                            GoTo CleanUp
                        End If
                    End If
                Else
                    'Verify Gateway Field Value tag
                    If (InStr(szConfig(iLoop), "[") <= 0) Or (InStr(szConfig(iLoop), "]") <= 0) Then
                        GoTo CleanUp
                    End If

                    szFormat = ""
                    szFieldEx = szConfig(iLoop).Trim().ToLower()
                    iIndex = InStr(szFieldEx, "{")    'InStr starts with 1
                    If (iIndex > 0) Then
                        'Example: Hash|[HashValue]{#[HashMethod]![ReturnKey]![-]!RespCode![-]!InvID![-]!InvRef![-]!InvCy![-]!InvTotal![-]!InvTest![-]!MerchRef#}
                        szFormat = szFieldEx.Substring(iIndex).Replace("}", "")
                        szFieldEx = szFieldEx.Substring(0, iIndex - 1)
                    End If

                    Select Case szFieldEx
                        Case "[ordernumber]"
                            stPayInfo.szHostOrderNumber = szValue
                        Case "[gatewaytxnid]"
                            stPayInfo.szHostGatewayTxnID = szValue
                        Case "[txnamount]"      'Amount in 2 decimals
                            stPayInfo.szHostTxnAmount = szValue
                        Case "[txnamount_(2)]"  'Amount in cents whereby the last two digits are decimal points
                            stPayInfo.szHostTxnAmount = szValue.Replace(".", "")
                        Case "[bankrefno]"
                            stPayInfo.szHostTxnID = szValue
                        Case "[param1]"
                            stPayInfo.szParam1 = szValue
                        Case "[param2]"
                            stPayInfo.szParam2 = szValue
                        Case "[param3]"
                            stPayInfo.szParam3 = szValue
                        Case "[param4]"
                            stPayInfo.szParam4 = szValue
                        Case "[param5]"
                            stPayInfo.szParam5 = szValue
                        Case "[currencycode]"   'ASCII currency code, e.g. MYR, IDR..
                            stPayInfo.szHostCurrencyCode = szValue
                        Case "[mid]"
                            stPayInfo.szHostMID = szValue
                        Case "[tid]"
                            stPayInfo.szHostTID = szValue
                        Case "[txnstatus]"
                            stPayInfo.szHostTxnStatus = szValue
                        Case "[respcode]"
                            stPayInfo.szRespCode = szValue
                        Case "[authcode]"
                            stPayInfo.szAuthCode = szValue
                        Case "[respmesg]"
                            stPayInfo.szBankRespMsg = szValue
                        Case "[date]"
                            stPayInfo.szHostDate = szValue
                        Case "[time]"
                            stPayInfo.szHostTime = szValue
                        Case "[cardpan]"
                            stPayInfo.szCardPAN = szValue

                            If (stPayInfo.szCardPAN.Length > 20) Then
                                stPayInfo.szCardPAN = stPayInfo.szCardPAN.Substring(0, 20)
                            End If

                            If stPayInfo.szCardPAN.Length > 10 Then
                                stPayInfo.szMaskedCardPAN = stPayInfo.szCardPAN.Substring(0, 6) + "".PadRight(stPayInfo.szCardPAN.Length - 6 - 4, "X") + stPayInfo.szCardPAN.Substring(stPayInfo.szCardPAN.Length - 4, 4)
                            Else
                                stPayInfo.szMaskedCardPAN = "".PadRight(stPayInfo.szCardPAN.Length, "X")
                            End If
                        Case "[sessionid]"
                            stPayInfo.szSessionID = szValue
                        Case "[hashvalue]"
                            stHostInfo.szHashValue = szValue
                        Case "[reserved]"
                            stHostInfo.szReserved = szValue
                        Case Else
                            'Unsupported value tag
                            stPayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                            stPayInfo.szErrDesc = " > Unsupported gateway field value tag(" + szConfig(iLoop) + ") in Config Template for " + stPayInfo.szIssuingBank

                            GoTo CleanUp
                    End Select

                    'Verification of field value based on format specified
                    'Example: Hashing    : #[HashMethod]![ReturnKey]![-]!RespCode![-]!InvID![-]!InvRef![-]!InvCy![-]!InvTotal![-]!InvTest![-]!MerchRef#
                    'Example: Fixed Value: [=5]
                    'Example: Encryption : *[3DES]!&!BillRef1![GatewayTxnID]!TxDate![Date]!TxTime![Time]!Currency![CurrencyCode]!Amount![TxnAmount]!ReturnCode![RespCode]!ReturnMessage![RespMesg]!Md5![HashValue]*
                    If (szFormat <> "") Then
                        'Encryption - The whole reply message is encrypted
                        If ("*" = Mid(szFormat, 1, 1)) Then
                            szValue = szFormat.Replace("*", "")
                            'szValue: e.g. [3DES]!&!BillRef1![GatewayTxnID]!TxDate![Date]!TxTime![Time]!Currency![CurrencyCode]!Amount![TxnAmount]!ReturnCode![RespCode]!ReturnMessage![RespMesg]!Md5![HashValue]
                            aszValue = Split(szValue, "!")

                            If ("[3des]" = aszValue(0).Trim().ToLower()) Then
                                Dim szKey As String = stHostInfo.szSecretKey
                                Dim szIV As String = stHostInfo.szInitVector
                                Dim szFldSet
                                Dim szFldSets() As String
                                Dim szFldEx() As String

                                'Decrypts the whole encrypted reply message stored in Reserved field
                                szValue = szDecrypt3DES(stHostInfo.szReserved, "AAAAAAAAAAAAAAAA1111111111111111AAAAAAAAAAAAAAAA", "0000000000000000")

                                'szValue: e.g. BillRef1=20050718-021&BillRef2=Richard&TxDate=18072005&TxTime=180203&ExDate=18072005&ExTime=166607&Currency=458&Amount=1000.99&ConfNum=669987&ReturnCode=000&ReturnMessage=Approved.&Active=1&Md5=cb2fe847bfb14fb64cd79f40a058347b
                                'szValue: e.g. BillRef1=330700000000000073&TxDate=30042009&TxTime=180815&Currency=458&Amount=190.99&ReturnCode=666&ReturnMessage=NOT FOUND

                                szFldSets = Split(szValue, aszValue(1).Trim(), -1, CompareMethod.Text)

                                'Gets number of field sets (e.g. 2 sets ==> Field1=Value1&Field2=Value2)
                                iField = 0
                                For Each szFldSet In szFldSets
                                    szFldEx = Split(szFldSet, "=")
                                    If (1 = szFldEx.GetUpperBound(0)) Then
                                        iField = iField + 1
                                    End If
                                Next

                                ReDim szFieldName(iField - 1)
                                ReDim szFieldValue(iField - 1)

                                'Populates Field Names and Values array
                                iField = 0
                                For Each szFldSet In szFldSets
                                    szFldEx = Split(szFldSet, "=")

                                    If (1 = szFldEx.GetUpperBound(0)) Then
                                        szFieldName(iField) = szFldEx(0)
                                        szFieldValue(iField) = szFldEx(1)
                                        iField = iField + 1
                                    End If
                                Next

                                'Matches each Field Name from message template against Field Names array to get its respective Field Value.
                                'Then, assigns the respective Field Value to the respective Gateway Varialbes as defined in the message template.
                                'e.g. BillRef1![GatewayTxnID]!TxDate![Date]!TxTime![Time]!Currency![CurrencyCode]!Amount![TxnAmount]!ConfNum![BankRefNo]!ReturnCode![RespCode]!ReturnMessage![RespMesg]!Md5![HashValue]{#[HashMethod]:BillRef1:[]:Amount:[]:Currency:[]:ConfNum#}*
                                bFound = False
                                For iCounter = 2 To aszValue.GetUpperBound(0)
                                    If (0 = (iCounter Mod 2)) Then
                                        For iField = 0 To szFieldName.GetUpperBound(0)
                                            If (aszValue(iCounter).Trim().ToLower() = szFieldName(iField).ToLower()) Then
                                                szValue = szFieldValue(iField)
                                                bFound = True
                                                Exit For
                                            End If
                                        Next
                                    Else
                                        szFormat = ""
                                        szFieldEx = aszValue(iCounter).Trim().ToLower()
                                        iIndex = InStr(szFieldEx, "{")    'InStr starts with 1
                                        If (iIndex > 0) Then
                                            'Example1: Md5|[HashValue]{#[HashMethod]:BillRef1:[]:Amount:[]:Currency:[]:ConfNum#}
                                            'Example2: Hash|[HashValue]{#[HashMethod]![ReturnKey]![-]!RespCode![-]!InvID![-]!InvRef![-]!InvCy![-]!InvTotal![-]!InvTest![-]!MerchRef#}
                                            szFormat = szFieldEx.Substring(iIndex).Replace("}", "")
                                            szFieldEx = szFieldEx.Substring(0, iIndex - 1)
                                        End If

                                        Select Case szFieldEx
                                            Case "[ordernumber]"
                                                stPayInfo.szOrderNumber = szValue
                                            Case "[gatewaytxnid]"
                                                stPayInfo.szTxnID = szValue
                                            Case "[txnamount]"      'Amount in 2 decimals
                                                stPayInfo.szTxnAmount = szValue
                                            Case "[txnamount_2]"    'Amount in cents whereby the last two digits are decimal points
                                                stPayInfo.szTxnAmount = Mid(szValue, 1, szValue.Length - 2) + "." '+ Right(szValue, 2)
                                            Case "[bankrefno]"
                                                stPayInfo.szHostTxnID = szValue
                                            Case "[param1]"
                                                stPayInfo.szParam1 = szValue
                                            Case "[param2]"
                                                stPayInfo.szParam2 = szValue
                                            Case "[param3]"
                                                stPayInfo.szParam3 = szValue
                                            Case "[param4]"
                                                stPayInfo.szParam4 = szValue
                                            Case "[param5]"
                                                stPayInfo.szParam5 = szValue
                                            Case "[currencycode]"   'ASCII currency code, e.g. MYR, IDR
                                                stPayInfo.szCurrencyCode = szValue
                                            Case "[mid]"
                                                stPayInfo.szHostMID = szValue
                                            Case "[tid]"
                                                stPayInfo.szHostTID = szValue
                                            Case "[txnstatus]"
                                                stPayInfo.szHostTxnStatus = szValue
                                            Case "[respcode]"
                                                stPayInfo.szRespCode = szValue
                                            Case "[authcode]"
                                                stPayInfo.szAuthCode = szValue
                                            Case "[respmesg]"
                                                stPayInfo.szBankRespMsg = szValue
                                            Case "[date]"
                                                stPayInfo.szHostDate = szValue
                                            Case "[time]"
                                                stPayInfo.szHostTime = szValue
                                            Case "[cardpan]"
                                                stPayInfo.szCardPAN = szValue

                                                If (stPayInfo.szCardPAN.Length > 20) Then
                                                    stPayInfo.szCardPAN = stPayInfo.szCardPAN.Substring(0, 20)
                                                End If

                                                If stPayInfo.szCardPAN.Length > 10 Then
                                                    stPayInfo.szMaskedCardPAN = stPayInfo.szCardPAN.Substring(0, 6) + "".PadRight(stPayInfo.szCardPAN.Length - 6 - 4, "X") + stPayInfo.szCardPAN.Substring(stPayInfo.szCardPAN.Length - 4, 4)
                                                Else
                                                    stPayInfo.szMaskedCardPAN = "".PadRight(stPayInfo.szCardPAN.Length, "X")
                                                End If
                                            Case "[sessionid]"
                                                stPayInfo.szSessionID = szValue
                                            Case "[hashvalue]"
                                                stHostInfo.szHashValue = szValue
                                            Case "[reserved]"
                                                stHostInfo.szReserved = szValue
                                            Case Else
                                                'Unsupported value tag
                                                stPayInfo.szTxnMsg = Common.C_COMMON_UNKNOWN_ERROR_2999
                                                stPayInfo.szErrDesc = "> Unsupported gateway field value tag(" + szConfig(iLoop) + ") in Config Template for " + stPayInfo.szIssuingBank

                                                GoTo CleanUp
                                        End Select

                                        'Sets szValue to empty so that the next Gateway variable will take empty szValue
                                        'if the Field Name defined in message template does not match any Field Name of
                                        'reply message or else the next Gateway variable will take the last szValue which
                                        'is not empty and belongs to the previous Gateway variable.
                                        szValue = ""
                                    End If
                                Next
                            End If  'If ("[3des]" = aszValue(0).Trim().ToLower()) Then
                        End If  'If ("*" = Left(szFormat, 1)) Then

                        'Cater for hash value verification included in the encrypted reply above.
                        'Verification of field value based on format specified
                        'Example: Hashing    : #[HashMethod]![ReturnKey]![-]!RespCode![-]!InvID![-]!InvRef![-]!InvCy![-]!InvTotal![-]!InvTest![-]!MerchRef#
                        'Example: Fixed Value: [=5]
                        If (szFormat <> "") Then
                            If ("#" = Mid(szFormat, 1, 1)) Then
                                szValue = szFormat.Replace("#", "")
                                aszValue = Split(szValue, "!")

                                szValue = ""
                                szHashValue = ""
                                szHashMethod = ""
                                ' Hash Method
                                If ("[hashmethod]" = aszValue(0).Trim().ToLower()) Then
                                    szHashMethod = stHostInfo.szHashMethod
                                Else
                                    'szHashMethod = Server.UrlDecode(HttpContext.Current.Request(aszValue(0).Trim()))
                                    bFound = False
                                    For iField = 0 To szFieldName.GetUpperBound(0)
                                        If (aszValue(0).Trim().ToLower() = szFieldName(iField).ToLower()) Then
                                            szHashMethod = szFieldValue(iField)
                                            bFound = True
                                            Exit For
                                        End If
                                    Next
                                    If (bFound <> True) Then
                                        szHashMethod = ""
                                    End If
                                End If

                                For iCounter = 1 To aszValue.GetUpperBound(0)
                                    If (0 <> (iCounter Mod 2)) Then ' Field
                                        Select Case aszValue(iCounter).Trim().ToLower()
                                            Case "[returnkey]"
                                                'stHostInfo.szReturnKey = objHash.szDecrypt3DES(stHostInfo.szReturnKey, stHostInfo.sz3DESAppKey, stHostInfo.sz3DESAppVector)
                                                szValue = stHostInfo.szReturnKey
                                            Case Else
                                                'szValue = Server.UrlDecode(HttpContext.Current.Request(aszValue(iCounter).Trim()))
                                                bFound = False
                                                For iField = 0 To szFieldName.GetUpperBound(0)
                                                    If (aszValue(iCounter).Trim().ToLower() = szFieldName(iField).ToLower()) Then
                                                        szValue = szFieldValue(iField)
                                                        bFound = True
                                                        Exit For
                                                    End If
                                                Next
                                                If (bFound <> True) Then
                                                    szValue = ""
                                                End If
                                        End Select

                                        szHashValue = szHashValue + szValue
                                    Else
                                        ' Delimiter
                                        szHashValue = szHashValue + aszValue(iCounter).Trim().Replace("[", "").Replace("]", "")
                                    End If
                                Next

                                'szHashValue = objHash.szGetHash(szHashValue, szHashMethod)

                                'Verify response Hash Value with Hash Value calculated above by Gateway
                                If (szHashValue <> stHostInfo.szHashValue) Then
                                    GoTo CleanUp
                                End If

                            ElseIf ("[=" = Mid(szFormat, 1, 2)) Then
                                If (szValue <> szFormat.Replace("[=", "").Replace("]", "")) Then
                                    GoTo CleanUp
                                End If
                            Else
                                GoTo CleanUp
                            End If  'If ("#" = Left(szFormat, 1)) Then
                        End If  'If (szFormat <> "") Then
                    End If  'If (szFormat <> "") Then
                End If
            Next iLoop

            bReturn = True

        Catch ex As Exception
            stPayInfo.szErrDesc = "Fail to parse response data. Exceptions: " + ex.Message()
            stPayInfo.szTxnMsg = "System Error: Fail to parse response data."

            bReturn = False
        End Try

CleanUp:
        If Not objStreamReader Is Nothing Then objStreamReader = Nothing

        Return bReturn
    End Function

    Public Sub CryptoTest()
        Dim szString As String = ""

        Dim szTotalHash As String = "7090982.12345678901234567890"
        Dim iInt As Integer

        'm7pE6EbL = A826A1CD78C3EC3D2797918E57ADCB42 - JPJ in server 82 old password
        'O2e5hTxP = 7F7E80C63D1C1DAC667E638C5B0D5ADE - new password

        'Direct Debit Gateway 3DES Key     

        szString = szEncrypt3DES("3106GHL", "3537146182309002FEB7EBCE55ED3BBA71BED7CEB1D18CAA", "0000000000000000")
        szString = szDecrypt3DES("28BCB387FEBA55A6E0DA70BC2FBE0226D81798C0D002F419C45957852103261D7E431A6188B3A9A821225F0823082226AF85CEC5D7EF95DF", "3537146182309002FEB7EBCE55ED3BBA71BED7CEB1D18CAA", "0000000000000000")
        '"9C7F89D61E4C9A98CA427FBCA5C7DA"
        '"DA09F688D896EBBB"  0123456789ABCDEF0123456789ABCDEF
        '8AA8B47711D1945BD4B014751537A5C11D7D9F76ED86150CB5B26A1DDE7
        'szString = szEncrypt3DESNew("923315FF5235C0A9335291CE04C61DCE", "4B6C696B506179546F41697241736961", "0000000000000000")

        'szString = szEncrypt3DES("vA4aramewrufE29neqUtuWEKuwuth8Ma", "3537146182309002FEB7EBCE55ED3BBA71BED7CEB1D18CAA", "0000000000000000")
        'szString = szDecrypt3DEStoHex("E22D7227CD475C2F525F180C2A235621D7F733639387D", "3537146182309002FEB7EBCE55ED3BBA71BED7CEB1D18CAA", "0000000000000000")

        'szString = szEncrypt3DES("3537146182309002FEB7EBCE55ED3BBA71BED7CEB1D18CAA", "3537146182309002FEB7EBCE55ED3BBA71BED7CEB1D18CAA", "35371461")
        'szString = szEncrypt3DES("F7468B69D12BB6CE76D6206419A6AC28", "12345678901234561234567890123456", "0000000000000000")
        'szString = szDecrypt3DES("40C5435F26BD9798C1E0DE3018388F123479FBD7BA516A9426CE9D3B895", "AAAAAAAAAAAAAAAA9999999999999999AAAAAAAAAAAAAAAA", "iviviviv")

        '"F4A249415525623BE0248C11B9365AC2A067A79D66434F54DEE7F2D730D6B13326BC95694163423D76ED572BAF88B57529F89874EABFC255CA820A148DD2DAC9A4AE1E4AD25C6423F8A93DA1F294EE34DFE9A5CED1562352DD97C11F2CD6C658E8A892C166D3F3D46A3A8315C4436D350597672454888BED47E29192E837D67F5A513EC97B73E1CAC42256A17F373317A3C6488753AA51B328D1C5E213E8F54E364C1AC31B58D66ECFA5B66FB3677424C4D89B7C02731F3C3FD213A960F02FAFCBDE6783C4B875DB163E7FA70718441EB4B13BD2BD67A5534881F"
        '0F         05                                                                                                        00               
        'szString = szDecrypt3DES(szString, "AAAAAAAAAAAAAAAA9999999999999999AAAAAAAAAAAAAAAA", "iviviviv")
        'Doc:"0F4A2494155205623BE0248C11B9365AC2A067A79D66434F54DEE7F2D730D6B13326BC95694163423D76ED572BAF88B57529F89874EABFC255CA8200A148DD2DAC9A4AE10E4AD25C6423F8A93DA1F294EE34DFE9A5CED1562352DD97C101F2CD6C658E8A892C166D3F3D46A3A803150C4436D350597672454888BED47E290192E837D67F5A513EC97B73E1CAC42256A17F373317A3C6488753AA51B328D1C5E213E8F54E364C1AC301B58D66ECFA5B66FB3677424C4D89B7C027031F3C3FD213A960F02FAFCBDE6783C4B875DB1603E7FA70718441EB4B13BD2BD67A5534881F"
        'szString = szDecrypt3DES("1F9853E656BEA2A8047EF5464D05A717", "3537146182309002FEB7EBCE55ED3BBA71BED7CEB1D18CAA", "0000000000000000")
        'BillRef1=20050718933&BillRef2=Marry&TxDate=18092005&TxTime=200903&ExDate=31072005&ExTime=196607&Currency=458&Amount=1963&ConfNum=669000&ReturnCode=000&ReturnMessage=Approved.&Active=1&Md5=482e25e4e22995f7843d94cfc167d35f
        'Doc:BillRef1=20050718933&BillRef2=Marry&TxDate=18092005&TxTime=200903&ExDate=31072005&ExTime=196607&Currency=458&Amount=1963&ConfNum=669000&ReturnCode=000&ReturnMessage=Approved.&Active=1&Md5=482e25e4e22995f7843d94cfc167d35f

        szString = szEncryptAES("INT_MIB_PROD", "3537146182309002FEB7EBCE55ED3BBA71BED7CEB1D18CAA")
        szString = szDecryptAES("BwW/uq88cXj9XDl1OkWGgA==", "3537146182309002FEB7EBCE55ED3BBA71BED7CEB1D18CAA")
        'szString = szDecryptAES("RLIZQ3t9aI0=huqutNhAPUfHm9MaNlNsChroqam+O0qMrgszHc5Zq+y2zBdPNNICZzOSVw/N20dHKb2Q/BbhWCwHWjzjft1tO01gCyfWE9LLzI+bRCHnzIAItyO+q8gDlMNIMk/U7lUtE9JL4kF7D4Rdj8M3p1qsWw==", "0000020423117test3117")

        Dim szDelimiter = "GET$"
        Dim szFieldSets() As String
        Dim szField() As String
        Dim szFieldSet
        Dim szFieldName() As String
        Dim szFieldValue() As String
        Dim szFldSet
        Dim szFldSets() As String
        Dim szFldEx() As String
        Dim iField As Integer
        szFieldSets = Split("B6961CF4A861679BF38E8F3D78A5A30233D1E1F6E9E2FD591B1729ED0E99FF815C1E98C3206D697E24FA1AED9407C1B5C270DEFC2F378824B056F1D65B81D872257565BE4EC79023B84A09E1EA5F5D305BFF358A36630EA1C8881175E728C52EF12CC5ADB418E30408E4338B420052BE029AFA5731DF7968EBDF714556C86141", szDelimiter, -1, CompareMethod.Text)
        iField = 0
        For Each szFieldSet In szFieldSets
            szField = Split(szFieldSet, "=")
            If (1 = szField.GetUpperBound(0)) Then
                iField = iField + 1
            End If
        Next

        ReDim szFieldName(iField - 1)
        ReDim szFieldValue(iField - 1)

        iField = 0
        For Each szFieldSet In szFieldSets
            szField = Split(szFieldSet, "=")

            If (1 = szField.GetUpperBound(0)) Then
                szFieldName(iField) = szField(0)
                szFieldValue(iField) = szField(1)
                iField = iField + 1
            End If
        Next

        szString = szDecrypt3DES("B6961CF4A861679BF38E8F3D78A5A30233D1E1F6E9E2FD591B1729ED0E99FF815C1E98C3206D697E24FA1AED9407C1B5C270DEFC2F378824B056F1D65B81D872257565BE4EC79023B84A09E1EA5F5D305BFF358A36630EA1C8881175E728C52EF12CC5ADB418E30408E4338B420052BE029AFA5731DF7968EBDF714556C86141", "AAAAAAAAAAAAAAAA1111111111111111AAAAAAAAAAAAAAAA", "0000000000000000")

        szFldSets = Split(szString, "&", -1, CompareMethod.Text)

        iField = 0
        For Each szFldSet In szFldSets
            szFldEx = Split(szFldSet, "=")
            If (1 = szFldEx.GetUpperBound(0)) Then
                iField = iField + 1
            End If
        Next
    End Sub

    Public Function szDecrypt3DEStoHex(ByVal sz_CipherText As String, ByVal sz_Key As String, ByVal sz_InitVector As String) As String
        Dim obj3DES As New TripleDESCryptoServiceProvider
        Dim objUTF8 As New UTF8Encoding
        Dim btInput() As Byte
        Dim btOutput() As Byte

        Try
            szDecrypt3DEStoHex = ""

            obj3DES.Padding = PaddingMode.PKCS7 ' Default value
            obj3DES.Mode = CipherMode.CBC       ' Default value
            obj3DES.Key = btHexToByte(sz_Key)

            If (False = bValidHex(sz_InitVector)) Then
                sz_InitVector = szStringToHex(sz_InitVector)
            End If
            obj3DES.IV = btHexToByte(sz_InitVector)

            'Added on 4 May 2009, to remove LF or CRLF
            'or else will encounter exception "Index was outside the bounds of the array" in btHextoByte
            'e.g. PBB Query reply contains CRLF or LF, causing its length become not genap but ganjil
            sz_CipherText = Replace(sz_CipherText, vbCrLf, "")
            sz_CipherText = Replace(sz_CipherText, vbCr, "")
            sz_CipherText = Replace(sz_CipherText, vbLf, "")

            If (False = bValidHex(sz_CipherText)) Then
                sz_CipherText = szStringToHex(sz_CipherText)
            End If
            btInput = btHexToByte(sz_CipherText)

            btOutput = btTransform(btInput, obj3DES.CreateDecryptor())

            'Comment on 25 May 2009, convert bytes to hex if encrypted value's clear texts are hexadecimal value
            szDecrypt3DEStoHex = szWriteHex(btOutput)

        Catch ex As Exception
            Throw ex
        Finally
            If Not obj3DES Is Nothing Then obj3DES = Nothing
            If Not objUTF8 Is Nothing Then objUTF8 = Nothing
        End Try
    End Function

    Public Function szDecrypt3DES(ByVal sz_CipherText As String, ByVal sz_Key As String, ByVal sz_InitVector As String) As String
        Dim obj3DES As New TripleDESCryptoServiceProvider
        Dim objUTF8 As New UTF8Encoding
        Dim btInput() As Byte
        Dim btOutput() As Byte

        Try
            szDecrypt3DES = ""

            obj3DES.Padding = PaddingMode.PKCS7 ' Default value
            obj3DES.Mode = CipherMode.CBC       ' Default value
            obj3DES.Key = btHexToByte(sz_Key)

            If (False = bValidHex(sz_InitVector)) Then
                sz_InitVector = szStringToHex(sz_InitVector)
            End If
            obj3DES.IV = btHexToByte(sz_InitVector)

            'Added on 4 May 2009, to remove LF or CRLF
            'or else will encounter exception "Index was outside the bounds of the array" in btHextoByte
            'e.g. PBB Query reply contains CRLF or LF, causing its length become not genap but ganjil
            sz_CipherText = Replace(sz_CipherText, vbCrLf, "")
            sz_CipherText = Replace(sz_CipherText, vbCr, "")
            sz_CipherText = Replace(sz_CipherText, vbLf, "")

            If (False = bValidHex(sz_CipherText)) Then
                sz_CipherText = szStringToHex(sz_CipherText)
            End If
            btInput = btHexToByte(sz_CipherText)

            btOutput = btTransform(btInput, obj3DES.CreateDecryptor())

            szDecrypt3DES = objUTF8.GetString(btOutput)

        Catch ex As Exception
            Throw ex
        Finally
            If Not obj3DES Is Nothing Then obj3DES = Nothing
            If Not objUTF8 Is Nothing Then objUTF8 = Nothing
        End Try
    End Function

    Public Function szEncrypt3DES(ByVal sz_ClearText As String, ByVal sz_Key As String, ByVal sz_InitVector As String) As String
        Dim obj3DES As New TripleDESCryptoServiceProvider
        Dim btInput() As Byte
        Dim btOutput() As Byte

        Try
            szEncrypt3DES = ""

            obj3DES.Padding = PaddingMode.PKCS7 ' Default value
            obj3DES.Mode = CipherMode.CBC       ' Default value
            obj3DES.Key = btHexToByte(sz_Key)

            If (False = bValidHex(sz_InitVector)) Then
                sz_InitVector = szStringToHex(sz_InitVector)
            End If
            obj3DES.IV = btHexToByte(sz_InitVector)

            'Added bValidHex by Jeff, 29 Mar 2009
            If (False = bValidHex(sz_ClearText)) Then
                sz_ClearText = szStringToHex(sz_ClearText)
            End If
            btInput = btHexToByte(sz_ClearText)

            btOutput = btTransform(btInput, obj3DES.CreateEncryptor())

        Catch ex As Exception
            Throw ex
        Finally
            If Not obj3DES Is Nothing Then obj3DES = Nothing
        End Try

        Return szWriteHex(btOutput)
    End Function

    Public Function szEncrypt3DESNew(ByVal sz_ClearText As String, ByVal sz_Key As String, ByVal sz_InitVector As String) As String
        Dim obj3DES As New TripleDESCryptoServiceProvider
        Dim btInput() As Byte
        Dim btOutput() As Byte

        Try
            szEncrypt3DESNew = ""

            obj3DES.Padding = PaddingMode.None ' Default value - Different from Encrypt3DES
            obj3DES.Mode = CipherMode.ECB     ' Default value - Different from Encrypt3DES
            obj3DES.Key = btHexToByte(sz_Key)

            If (False = bValidHex(sz_InitVector)) Then
                sz_InitVector = szStringToHex(sz_InitVector)
            End If
            obj3DES.IV = btHexToByte(sz_InitVector)

            'Added bValidHex by Jeff, 29 Mar 2009
            If (False = bValidHex(sz_ClearText)) Then
                sz_ClearText = szStringToHex(sz_ClearText)
            End If
            btInput = btHexToByte(sz_ClearText)

            btOutput = btTransform(btInput, obj3DES.CreateEncryptor())

        Catch ex As Exception
            Throw ex
        Finally
            If Not obj3DES Is Nothing Then obj3DES = Nothing
        End Try

        Return szWriteHex(btOutput)
    End Function

    Public Function szEncryptDES(ByVal sz_ClearText As String, ByVal sz_Key As String, ByVal sz_InitVector As String) As String
        Dim objDES As New DESCryptoServiceProvider
        Dim btInput() As Byte
        Dim btOutput() As Byte

        Try
            szEncryptDES = ""

            objDES.Padding = PaddingMode.PKCS7 ' Default value
            objDES.Mode = CipherMode.CBC       ' Default value
            objDES.Key = btHexToByte(sz_Key)

            If (False = bValidHex(sz_InitVector)) Then
                sz_InitVector = szStringToHex(sz_InitVector)
            End If
            objDES.IV = btHexToByte(sz_InitVector)

            'Added bValidHex by Jeff, 29 Mar 2009
            If (False = bValidHex(sz_ClearText)) Then
                sz_ClearText = szStringToHex(sz_ClearText)
            End If
            btInput = btHexToByte(sz_ClearText)

            btOutput = btTransform(btInput, objDES.CreateEncryptor())

        Catch ex As Exception
            Throw ex
        Finally
            If Not objDES Is Nothing Then objDES = Nothing
        End Try

        Return szWriteHex(btOutput)
    End Function

    Public Function szEncryptAES(ByVal sz_ClearText As String, ByVal sz_Key As String) As String
        Dim szRet As String = ""
        Dim objRijndael As New Crypto.Rijndael(sz_Key)

        Try
            szRet = objRijndael.EncryptStringAES(sz_ClearText)
        Catch ex As Exception
            Throw ex
        Finally
            objRijndael = Nothing
        End Try

        Return szRet
    End Function

    Public Function szDecryptAES(ByVal sz_ClearText As String, ByVal sz_Key As String) As String
        Dim szRet As String = ""
        Dim objRijndael As New Crypto.Rijndael(sz_Key)

        Try
            szRet = objRijndael.DecryptStringAES(sz_ClearText)
        Catch ex As Exception
            Throw ex
        Finally
            objRijndael = Nothing
        End Try

        Return szRet
    End Function

    Public Function szStringToHex(ByVal sz_Text As String) As String
        Dim szHex As String = ""

        For i As Integer = 0 To sz_Text.Length - 1
            szHex &= Asc(sz_Text.Substring(i, 1)).ToString("x").ToUpper
        Next

        Return szHex
    End Function

    Public Function bValidHex(ByVal sz_Txt As String) As Boolean
        Dim szch As String
        Dim i As Integer

        sz_Txt = sz_Txt.ToUpper()
        For i = 1 To Len(sz_Txt)

            szch = Mid(sz_Txt, i, 1)
            ' See if the next character is a non-digit.
            If (False = IsNumeric(szch)) Then
                If szch >= "G" Then
                    ' This is not a letter.
                    Return False
                End If
            End If
        Next i

        Return True
    End Function

    Public Function btHexToByte(ByVal c_Hex As Char()) As Byte()
        Dim szHex As String = ""

        Dim btOutput(c_Hex.Length() / 2 - 1) As Byte 'Byte() = New Byte(c_Hex.Length / 2 - 1) {}
        Dim iOffset As Integer = 0
        Dim iCounter As Integer = 0

        iCounter = 0
        While iCounter < c_Hex.Length()
            szHex = Convert.ToString(c_Hex(iCounter)) + Convert.ToString(c_Hex(iCounter + 1))
            btOutput(iOffset) = Byte.Parse(szHex, System.Globalization.NumberStyles.HexNumber)
            iOffset += 1
            iCounter = iCounter + 2
        End While

        Return btOutput
    End Function

    Public Function btTransform(ByVal bt_Input() As Byte, ByVal I_CryptoTransform As ICryptoTransform) As Byte()
        ' Create the necessary streams
        Dim objMemStream As MemoryStream = New MemoryStream
        Dim objCryptStream As CryptoStream = New CryptoStream(objMemStream, I_CryptoTransform, CryptoStreamMode.Write)
        Dim btResult() As Byte

        Try
            ' Transform the bytes as requested
            objCryptStream.Write(bt_Input, 0, bt_Input.Length())
            objCryptStream.FlushFinalBlock()

            ' Read the memory stream and convert it back into byte array
            objMemStream.Position = 0

            ReDim btResult(CType(objMemStream.Length() - 1, System.Int32))
            objMemStream.Read(btResult, 0, CType(btResult.Length(), System.Int32))

        Catch ex As Exception
            Throw ex
        Finally
            ' close and release the streams
            objMemStream.Close()
            objCryptStream.Close()

            If Not objMemStream Is Nothing Then objMemStream = Nothing
            If Not objCryptStream Is Nothing Then objCryptStream = Nothing
        End Try

        ' hand back the result buffer
        Return btResult
    End Function

    Public Sub StringTest()
        Dim szString As String = ""
        Dim iInt As Integer = 0
        Dim aszString() As String
        Dim iHostID(3) As Integer
        Dim szValue As String
        Dim aszValue() As String
        Dim iCounter As Integer
        Dim szFieldSet
        Dim szField() As String

        Dim szSeats() As String
        Dim szSeatDetail() As String
        Dim szSeat
        Dim szXML As String
        Dim iLoop As Integer
        Dim iRet As Integer = -1

        Dim szFilterString As String
        Dim szTag2Search As String
        Dim iStart As Integer
        Dim iEnd As Integer
        Dim iNumOfSeats As Integer
        Dim iReservedSeats As Integer
        Dim iStartIndex As Integer
        Dim iMesgSet As Integer
        Dim iMesgNum As Integer
        Dim szSOAPResp As String
        Dim dAmt As Double

        ''true false test
        Dim btest1 As Boolean = False 'check express
        Dim btest2 As Boolean = False 'check express with checkout
        ''express with checkout
        'btest1 = True
        'btest2 = True
        ''express no checkout
        'btest1 = True
        'btest2 = False
        ''standard 
        btest1 = False
        btest2 = False

        If (True = btest1) Then
            MsgBox("True1")
        End If

        If (False = btest1 Or (True = btest1 And True = btest2)) Then
            MsgBox("True2")
        End If
        ''

        'szString = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
        'szString = szString.Replace("-", "").Replace(":", "").Replace(" ", "")
        'szString = "msgfromfpx=ABCDEF"
        Dim test As String
        szString = "SALE|REQ|SC|""requestToken"": ""[param1]"",""callbackUrl"": ""[GatewayReturnURL]"",""failureCallback"": ""[GatewayReturnURL]"",""cancelCallback"":""[GatewayReturnURL]"",""merchantCheckoutId"": ""[PayeeCode]"",""allowedCardTypes"":""master,visa"",'suppressShippingAddressEnable':'true',""version"":""v6""|[param1]|[GatewayReturnURL]|[PayeeCode]"
        aszString = Split(szString, "|")
        test = aszString(3).Trim()
        For iLoop = 4 To aszString.GetUpperBound(0)
            Select Case aszString(iLoop).Trim().ToLower()
                Case "[param1]"
                    test = InStr(test, "[param1]")
                Case "[gatewayreturnurl]"
                    test = InStr(test, "[gatewayreturnurl]")
                Case "[payeecode]"
            End Select
        Next
        '"requestToken": "[param1]","callbackUrl": "[GatewayReturnURL]","failureCallback": "[GatewayReturnURL]","cancelCallback":"[GatewayReturnURL]","merchantCheckoutId": "#PayeeCode","allowedCardTypes":"master,visa",'suppressShippingAddressEnable':'true',"version":"v6"
        'If InStr(szOriText, szTextToFind) Then
        '    If (szTextToReplace.Trim.Length > 0) Then
        '        szOriText = szOriText.Replace(szTextToFind, szTextToReplace)
        '    Else
        '        szOriText = szOriText.Replace(szTextToFind, "")
        '    End If
        'End If

        'szFormatStringRestructure - start
        Dim szOutput As String = ""
        Dim szSubStr As String = ""
        Dim szMaskStr As String = ""
        Dim szInsert As String = ""
        Dim aszIndicator() As String
        Dim i As Integer
        Dim j As Integer
        Dim asz_Src() As String
        Dim sz_Value As String

        ''PAD test
        'Dim sz_Src As String = "24"
        'Dim sz_PadSide As String = "L"
        'Dim i_Length As Integer = 2
        'Dim sz_PadChar As String = "0"

        'If ("R" = sz_PadSide.ToUpper) Then
        '    sz_Src = sz_Src.PadRight(i_Length, sz_PadChar)
        'ElseIf ("L" = sz_PadSide.ToUpper) Then
        '    sz_Src = sz_Src.PadLeft(i_Length, sz_PadChar)
        'End If
        ''PAD test

        Dim word1 As String = "ViSuAL bASiC"
        Dim word2 As String = "ViSuALbASiC" '"Visual Basic"

        If String.Compare(word1, word2) = 0 Then 'False to check case
            MsgBox("Strings Match")
        Else
            MsgBox("Strings Do not Match")
        End If

        szString = "IPGOMSIT161021028MYR_OM&MPID=ieieoe"
        szOutput = szString.Substring(0, szString.IndexOf("&MPID"))
        szOutput = szString.Substring(szString.Length - 6, 3)
        szOutput = Microsoft.VisualBasic.Strings.Right(szString, 3)
        'someString.Substring(someString.IndexOf("@"), 10)

        'asz_Src = Split("SR@L:2@[param3]", "@")
        asz_Src = Split("MASK@L:2@[cvv2]", "@")
        sz_Value = "789"

        'Mask
        For i = 0 To asz_Src.GetUpperBound(0) - 1
            aszIndicator = Split(asz_Src(i), ":")
            Select Case aszIndicator(0).ToUpper()
                Case "R"
                    szSubStr = Microsoft.VisualBasic.Strings.Left(sz_Value, sz_Value.Length() - aszIndicator(1))
                    szMaskStr = szSubStr.PadRight(sz_Value.Length(), "x")
                Case "L"
                    szSubStr = Microsoft.VisualBasic.Strings.Right(sz_Value, sz_Value.Length() - aszIndicator(1))
                    szMaskStr = szSubStr.PadLeft(sz_Value.Length(), "x")
                Case "M"
                    szSubStr = sz_Value.Remove(aszIndicator(1), aszIndicator(2))
                    For j = 0 To aszIndicator(2) - 1
                        szInsert = szInsert + "x"
                    Next
                    szMaskStr = szSubStr.Insert(aszIndicator(1), szInsert)
            End Select

            szOutput = szMaskStr
        Next

        ''Reverse Mask
        asz_Src = Split("M:2:2@[cvv2]", "@") '("L:1@[cvv2]", "@")
        sz_Value = "1234"

        For i = 0 To asz_Src.GetUpperBound(0) - 1
            aszIndicator = Split(asz_Src(i), ":")
            Select Case aszIndicator(0).ToUpper()
                Case "R"
                    szSubStr = Microsoft.VisualBasic.Strings.Right(sz_Value, sz_Value.Length() - (sz_Value.Length() - aszIndicator(1)))
                    szMaskStr = szSubStr.PadLeft(sz_Value.Length(), "x")
                Case "L"
                    szSubStr = Microsoft.VisualBasic.Strings.Left(sz_Value, sz_Value.Length() - (sz_Value.Length() - aszIndicator(1)))
                    szMaskStr = szSubStr.PadRight(sz_Value.Length(), "x")
                Case "M"
                    szSubStr = sz_Value.Substring(aszIndicator(1), aszIndicator(2))
                    szMaskStr = szSubStr.PadLeft(Convert.ToInt16(aszIndicator(1)) + Convert.ToInt16(aszIndicator(2)), "x").PadRight(sz_Value.Length, "x")
            End Select

            szOutput = szMaskStr
        Next

        MsgBox(szOutput)

        For i = 0 To asz_Src.GetUpperBound(0) - 1
            aszIndicator = Split(asz_Src(i), ":")
            Select Case aszIndicator(0).ToUpper()
                Case "R"
                    szSubStr = Microsoft.VisualBasic.Strings.Right(sz_Value, aszIndicator(1))
                Case "L"
                    szSubStr = Microsoft.VisualBasic.Strings.Left(sz_Value, aszIndicator(1))
                Case "M"
                    szSubStr = sz_Value.Substring(aszIndicator(1), aszIndicator(2))
                Case "F"            'Added by OoiMei, 30 June 2014, GlobalPay Cybersource
                    szSubStr = aszIndicator(1)
            End Select
            szOutput += szSubStr
        Next
        MsgBox(szOutput)
        'szFormatStringRestructure - end

        iInt = InStr("Templates\popup\popupredirect.htm", "redirect")

        szString = "371891410719507"
        szString = szString.Substring(12, 3)
        MsgBox(szString)

        szSeats = Split(szString, "msgfromfpx=")
        MsgBox(szSeats(0))
        MsgBox(szSeats(1))

        szFilterString = "SIGNDATA_QUERY^808080102999343^808080102999343^20091102^2009110200000005^0001"
        szSeats = Split(szFilterString, "^")
        iLoop = szSeats.GetUpperBound(0)


        'szTemp = Mid(szTemp, 1, 1) '+ CInt(Mid(szTemp, 2, 1)) + CInt(Mid(szTemp, 3, 1))
        'acServiceID = Left(stPayInfo.szServiceID, 1).ToCharArray()
        'Converts "A" to "65" then minus 32 because decimal 0 to 32 is not printable. "z" is 122, 122-32=90 (2 digits)
        'stPayInfo.szTxnID = (Decimal.op_Implicit(acServiceID(0)) - 32).ToString()

        szFilterString = "hello<bank_status>hello"
        iStart = szFilterString.IndexOfAny("Bank_status")
        szFilterString = "#=="
        If ("=" = Mid(szFilterString, 2, 1)) Then
            iStart = szFilterString.IndexOf("=", 0) 'IndexOf is zero-based
            iEnd = szFilterString.IndexOf("=", 2)
            szFilterString = szFilterString.Substring(iStart + 1, iEnd - iStart - 1)
        End If

        szTag2Search = System.DateTime.Now.ToString("fff")
        szTag2Search = CInt(Mid(szTag2Search, 1, 1)) + CInt(Mid(szTag2Search, 2, 1)) + CInt(Mid(szTag2Search, 3, 1))

        dAmt = 1234567890.79
        szTag2Search = "1234567890"
        szFilterString = String.Format("{0:N2}", CDbl(szTag2Search))
        szTag2Search = Convert.ToDateTime(System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")).ToString("ddMMyyyyhhmmss")
        szSeats = Split("D7!OOL!KUL!2008-11-28T21:40:00.000Z!2702!1!YYUWLA01!YYUWLA!07A|D7!KUL!OOL!2008-12-04T08:55:00.000Z!2703!1!YYUWLA01!YYUWLA!07A|", "|")

        For Each szSeat In szSeats

            szSeatDetail = Split(szSeat, "!")

            'Split by "!" for the 3rd element which is empty will have GetUpperBound result as 0
            If (szSeatDetail.GetUpperBound(0) > 0) Then
                'Increases seat by one
                iNumOfSeats = iNumOfSeats + 1
            End If
        Next

        szSOAPResp = "Server: Netscape-Enterprise/6.0Date: Mon, 17 Aug 2009 09:20:24 GMTContent-length: 761Content-type: text/xml; charset=utf-8Set-cookie: JSESSIONID=viammad003-10fc4%253A4a8920d5%253A494387f7dd448a;path=/;expires=Mon, 17-Aug-2009 09:22:22 GMTConnection: Close()<?xml version='1.0' encoding='UTF-8'?><SOAP-ENV:Envelope xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema""><SOAP-ENV:Header><TransactionId>0787555000399913</TransactionId></SOAP-ENV:Header><SOAP-ENV:Body><ns1:reserveSeatsResponse xmlns:ns1=""urn:os:reserveSeats"" SOAP-ENV:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/""><return xsi:type=""ns1:ReserveSeatsOut""><ResultCodes xmlns:ns2=""http://schemas.xmlsoap.org/soap/encoding/"" xsi:type=""ns2:Array"" ns2:arrayType=""xsd:int[2]""><item xsi:type=""xsd:int"">0</item><item xsi:type=""xsd:int"">0</item><item xsi:type=""xsd:int"">2</item><item xsi:type=""xsd:int"">3</item></ResultCodes></return></ns1:reserveSeatsResponse></SOAP-ENV:Body></SOAP-ENV:Envelope>"
        szSOAPResp = "Server: Netscape-Enterprise/6.0Date: Mon, 17 Aug 2009 09:20:24 GMTContent-length: 761Content-type: text/xml; charset=utf-8Set-cookie: JSESSIONID=viammad003-10fc4%253A4a8920d5%253A494387f7dd448a;path=/;expires=Mon, 17-Aug-2009 09:22:22 GMTConnection: Close()<?xml version='1.0' encoding='UTF-8'?><SOAP-ENV:Envelope xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema""><SOAP-ENV:Header><TransactionId>0787555000399913</TransactionId></SOAP-ENV:Header><SOAP-ENV:Body><ns1:reserveSeatsResponse xmlns:ns1=""urn:os:reserveSeats"" SOAP-ENV:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/""><return xsi:type=""ns1:ReserveSeatsOut""><ResultCodes xmlns:ns2=""http://schemas.xmlsoap.org/soap/encoding/"" xsi:type=""ns2:Array"" ns2:arrayType=""xsd:int[2]""><item xsi:type=""xsd:int"">0</item></ResultCodes></return></ns1:reserveSeatsResponse></SOAP-ENV:Body></SOAP-ENV:Envelope>"
        szSOAPResp = "Server: Netscape-Enterprise/6.0Date: Mon, 17 Aug 2009 09:20:24 GMTContent-length: 761Content-type: text/xml; charset=utf-8Set-cookie: JSESSIONID=viammad003-10fc4%253A4a8920d5%253A494387f7dd448a;path=/;expires=Mon, 17-Aug-2009 09:22:22 GMTConnection: Close()<?xml version='1.0' encoding='UTF-8'?><SOAP-ENV:Envelope xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema""><SOAP-ENV:Header><TransactionId>0787555000399913</TransactionId></SOAP-ENV:Header><SOAP-ENV:Body><ns1:reserveSeatsResponse xmlns:ns1=""urn:os:reserveSeats"" SOAP-ENV:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/""><return xsi:type=""ns1:ReserveSeatsOut""><item xsi:type=""ns1:Status""><StatusCode xsi:type=""xsd:string"">ER</StatusCode><MessageNumber xsi:type=""xsd:int"">5</MessageNumber><MessageSet xsi:type=""xsd:int"">12</MessageSet><MessageText xsi:type=""xsd:string"">Could not find PNR record</MessageText></item></Exceptions></return></ns1:reserveSeatsResponse></SOAP-ENV:Body></SOAP-ENV:Envelope>"

        If ("" = szSOAPResp) Then
            iRet = -1   'Failed - Generic Web Service system error - Invalid params, Exception, HTTP error, HTTP timeout
        Else
            'Server: Netscape-Enterprise/6.0
            'Date: Mon, 17 Aug 2009 09:20:24 GMT
            'Content-length: 761
            'Content-type: text/xml; charset=utf-8
            'Set-cookie: JSESSIONID=viammad003-10fc4%253A4a8920d5%253A494387f7dd448a;path=/;expires=Mon, 17-Aug-2009 09:22:22 GMT
            'Connection:             close()

            '<?xml version='1.0' encoding='UTF-8'?>
            '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
            '<SOAP-ENV:Header>
            '<TransactionId>0787555000399913</TransactionId>
            '</SOAP-ENV:Header>
            '<SOAP-ENV:Body>
            '<ns1:reserveSeatsResponse xmlns:ns1="urn:os:reserveSeats" SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
            '<return xsi:type="ns1:ReserveSeatsOut">
            '<ResultCodes xmlns:ns2="http://schemas.xmlsoap.org/soap/encoding/" xsi:type="ns2:Array" ns2:arrayType="xsd:int[2]">
            '<item xsi:type="xsd:int">0</item> =====> Indicates reservation status for the 1st seat
            '<item xsi:type="xsd:int">0</item> =====> Indicates reservation status for the 2nd seat
            '</ResultCodes>
            '</return>
            '</ns1:reserveSeatsResponse>
            '</SOAP-ENV:Body>
            '</SOAP-ENV:Envelope>

            'Removes the header and only takes response data started from <?xml version='1.0' encoding='UTF-8'?>
            iRet = szSOAPResp.IndexOf("<")
            szSOAPResp = szSOAPResp.Substring(iRet)

            iReservedSeats = 0
            iStartIndex = 0
            For iLoop = 1 To iNumOfSeats
                'Looks for ReserveSeats Items from ResultCodes <item xsi:type="xsd:int">0</item>
                'NOTE: szTag2Search cannot include "<" or partial tag, MUST include the WHOLE tag, or else will not get accurate value
                szTag2Search = "item xsi:type=""xsd:int"""
                iRet = szSOAPResp.IndexOf(szTag2Search, iStartIndex) 'zero based - n-th occurence of szTag2Search

                'No Item(s) found from ResultCodes AT ALL or
                'Not enough Item(s) found from ResultCodes, e.g. 8 seats but only 7 Items available
                If (iRet < 0) Then
                    'Probably it's OpenSkies error, e.g. <MessageNumber xsi:type="xsd:int">5</MessageNumber><MessageSet xsi:type="xsd:int">12</MessageSet>
                    szTag2Search = "MessageNumber xsi:type=""xsd:int"""
                    iRet = szSOAPResp.IndexOf(szTag2Search)
                    If (iRet >= 0) Then
                        szFilterString = szSOAPResp.Substring(iRet + szTag2Search.Length) 'from end of szTag2Search up to end of szSOAPMesg
                        iStart = szFilterString.IndexOf(">") + 1            'Start offset of the searched tag's value
                        iEnd = szFilterString.IndexOf("<") - 1              'End offset of the searched tag's value
                        szValue = szFilterString.Substring(iStart, iEnd)

                        iMesgNum = Convert.ToInt32(szValue)
                    End If

                    szTag2Search = "MessageSet xsi:type=""xsd:int"""
                    iRet = szSOAPResp.IndexOf(szTag2Search)
                    If (iRet >= 0) Then
                        szFilterString = szSOAPResp.Substring(iRet + szTag2Search.Length) 'from end of szTag2Search up to end of szSOAPMesg
                        iStart = szFilterString.IndexOf(">") + 1            'Start offset of the searched tag's value
                        iEnd = szFilterString.IndexOf("<") - 1              'End offset of the searched tag's value
                        szValue = szFilterString.Substring(iStart, iEnd)

                        iMesgSet = Convert.ToInt32(szValue)
                    End If

                    If (9102 = iMesgNum And 9100 = iMesgSet) Then
                        iRet = 5    'Failed - Communication error with OpenSkies Reservation System
                    Else
                        iRet = 2    'Failed - OS failure with MesgNum and MesgSet OR invalid OS reply with insufficient Items
                    End If

                    Exit For
                Else
                    'Found Item(s) from ResultCodes
                    szFilterString = szSOAPResp.Substring(iRet + szTag2Search.Length) 'from end of szTag2Search up to end of szSOAPMesg
                    iStart = szFilterString.IndexOf(">") + 1            'Start offset of the searched tag's value
                    iEnd = szFilterString.IndexOf("<") - 1              'End offset of the searched tag's value
                    szValue = szFilterString.Substring(iStart, iEnd)

                    If (szValue <> "0") Then
                        iRet = 2    'Failed - At least one seat not added successfully
                        Exit For
                    Else
                        'Sets the next start index of searching Item tag for Seat Reservation Status
                        iStartIndex = iRet + szTag2Search.Length
                        iReservedSeats = iReservedSeats + 1
                    End If
                End If
            Next

            'All seats reserved successfully
            If (iNumOfSeats = iReservedSeats) Then
                iRet = 0
            End If
        End If

        '<MessageNumber xsi:type="xsd:int">5</MessageNumber>
        '<MessageSet xsi:type="xsd:int">12</MessageSet>
        szXML = "<MessageNumber xsi:type=""xsd:int"">5</MessageNumber><MessageSet xsi:type=""xsd:int"">12</MessageSet>"
        szTag2Search = "MessageNumber xsi:type=""xsd:int"""
        iRet = szXML.IndexOf(szTag2Search)
        If (iRet >= 0) Then
            szFilterString = szXML.Substring(iRet + szTag2Search.Length) 'from end of szTag2Search up to end of szSOAPMesg
            iStart = szFilterString.IndexOf(">") + 1            'Start offset of the searched tag's value
            iEnd = szFilterString.IndexOf("<") - 1              'End offset of the searched tag's value
            szValue = szFilterString.Substring(iStart, iEnd)

            iStart = Convert.ToInt32(szValue)
        End If

        szTag2Search = "MessageSet xsi:type=""xsd:int"""
        iRet = szXML.IndexOf(szTag2Search)
        If (iRet >= 0) Then
            szFilterString = szXML.Substring(iRet + szTag2Search.Length) 'from end of szTag2Search up to end of szSOAPMesg
            iStart = szFilterString.IndexOf(">") + 1            'Start offset of the searched tag's value
            iEnd = szFilterString.IndexOf("<") - 1              'End offset of the searched tag's value
            szValue = szFilterString.Substring(iStart, iEnd)

            iEnd = Convert.ToInt32(szValue)
        End If

        szXML = "Server: Netscape-Enterprise/6.0Date: Mon, 17 Aug 2009 09:20:24 GMTContent-length: 761Content-type: text/xml; charset=utf-8Set-cookie: JSESSIONID=viammad003-10fc4%253A4a8920d5%253A494387f7dd448a;path=/;expires=Mon, 17-Aug-2009 09:22:22 GMTConnection: Close()<?xml version='1.0' encoding='UTF-8'?><SOAP-ENV:Envelope xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema""><SOAP-ENV:Header><TransactionId>0787555000399913</TransactionId></SOAP-ENV:Header><SOAP-ENV:Body><ns1:reserveSeatsResponse xmlns:ns1=""urn:os:reserveSeats"" SOAP-ENV:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/""><return xsi:type=""ns1:ReserveSeatsOut""><ResultCodes xmlns:ns2=""http://schemas.xmlsoap.org/soap/encoding/"" xsi:type=""ns2:Array"" ns2:arrayType=""xsd:int[2]""><item xsi:type=""xsd:int"">0</item><item xsi:type=""xsd:int"">1</item><item xsi:type=""xsd:int"">2</item><item xsi:type=""xsd:int"">3</item></ResultCodes></return></ns1:reserveSeatsResponse></SOAP-ENV:Body></SOAP-ENV:Envelope>"
        iRet = szXML.IndexOf("<") 'zero based - first occurence of sz_Tag2Search
        szXML = szXML.Substring(iRet)

        szTag2Search = "item xsi:type=""xsd:int"""

        iRet = szXML.IndexOf(szTag2Search, 0) 'zero based - first occurence of szTag2Search
        If (iRet < 0) Then    'Not found
            iRet = -1
            'Return
        Else

        End If

        szFilterString = szXML.Substring(iRet + szTag2Search.Length) 'from end of szTag2Search up to end of szSOAPMesg

        iStart = szFilterString.IndexOf(">") + 1            'Start offset of the searched tag's value
        iEnd = szFilterString.IndexOf("<") - 1              'End offset of the searched tag's value
        szValue = szFilterString.Substring(iStart, iEnd)

        '2nd occurence
        iRet = szXML.IndexOf(szTag2Search, iRet + szTag2Search.Length) 'zero based - first occurence of szTag2Search
        If (iRet < 0) Then    'Not found
            iRet = -1
            'Return
        Else

        End If

        szFilterString = szXML.Substring(iRet + szTag2Search.Length) 'from end of szTag2Search up to end of szSOAPMesg

        iStart = szFilterString.IndexOf(">") + 1            'Start offset of the searched tag's value
        iEnd = szFilterString.IndexOf("<") - 1              'End offset of the searched tag's value
        szValue = szFilterString.Substring(iStart, iEnd)

        '3rd occurence
        iRet = szXML.IndexOf(szTag2Search, iRet + szTag2Search.Length) 'zero based - first occurence of szTag2Search
        If (iRet < 0) Then    'Not found
            iRet = -1
            'Return
        Else

        End If

        szFilterString = szXML.Substring(iRet + szTag2Search.Length) 'from end of szTag2Search up to end of szSOAPMesg

        iStart = szFilterString.IndexOf(">") + 1            'Start offset of the searched tag's value
        iEnd = szFilterString.IndexOf("<") - 1              'End offset of the searched tag's value
        szValue = szFilterString.Substring(iStart, iEnd)

        szString = "D7!OOL!KUL!2008-11-28T21:40:00.000Z!2702!1!YYUWLA01!YYUWLA!07A|D7!KUL!OOL!2008-12-04T08:55:00.000Z!2703!1!YYUWLA01!YYUWLA!07A|"
        szSeats = Split(szString, "|")

        '  0      1     2
        'D7!OOL|D7!KUL|    ==> GetUpperBound(0) result will be 0,1,2 ==> 2 ==> 3 array elements
        If (szSeats.GetUpperBound(0) > 0) Then
            szXML = szXML + "<?xml version='1.0' encoding='UTF-8' ?>" + vbCrLf
            szXML = szXML + "<SOAP-ENV:Envelope xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance""" + vbCrLf
            szXML = szXML + "xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">" + vbCrLf
            szXML = szXML + "<SOAP-ENV:Header>" + vbCrLf
            szXML = szXML + "<ClientId>airasia-pnrsearch</ClientId>" + vbCrLf
            szXML = szXML + "<CarrierAccount>AK</CarrierAccount>" + vbCrLf
            szXML = szXML + "<AgentID></AgentID>" + vbCrLf
            szXML = szXML + "<APISourceType></APISourceType>" + vbCrLf
            szXML = szXML + "</SOAP-ENV:Header>" + vbCrLf
            szXML = szXML + "<SOAP-ENV:Body>" + vbCrLf
            szXML = szXML + "<ns1:reserveSeats SOAP-ENV:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/"" xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/""" + vbCrLf
            szXML = szXML + "xmlns:ns1=""urn:os:reserveSeats"">" + vbCrLf
            szXML = szXML + "<input xsi:type=""ns1:ReserveSeatsIn"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">" + vbCrLf
            szXML = szXML + "<ReserveSeatRequests xmlns:ns2=""http://xml.apache.org/xml-soap"" xsi:type=""ns2:Vector"">" + vbCrLf

            For Each szSeat In szSeats
                szSeatDetail = Split(szSeat, "!")

                'Split by "!" for the 3rd element which is empty will have GetUpperBound result as 0
                If (szSeatDetail.GetUpperBound(0) > 0) Then
                    For iLoop = 0 To szSeatDetail.GetUpperBound(0)
                        Select Case iLoop
                            Case 0
                                szXML = szXML + "<item xsi:type=""ns1:ReserveSeatRequest"">" + vbCrLf
                                szXML = szXML + "<FlightKey xsi:type=""ns1:FlightKey"">" + vbCrLf
                                szXML = szXML + "<AirlineCode xsi:type=""xsd:string"">" + szSeatDetail(iLoop) + "</AirlineCode>" + vbCrLf
                            Case 1
                                szXML = szXML + "<ArrivalAirportCode xsi:type=""xsd:string"">" + szSeatDetail(iLoop) + "</ArrivalAirportCode>" + vbCrLf
                            Case 2
                                szXML = szXML + "<DptrAirportCode xsi:type=""xsd:string"">" + szSeatDetail(iLoop) + "</DptrAirportCode>" + vbCrLf
                            Case 3
                                szXML = szXML + "<DptrDateTime xsi:type=""xsd:dateTime"">" + szSeatDetail(iLoop) + "</DptrDateTime>" + vbCrLf
                            Case 4
                                szXML = szXML + "<FltNum xsi:type=""xsd:string"">" + szSeatDetail(iLoop) + "</FltNum>" + vbCrLf
                                szXML = szXML + "</FlightKey>" + vbCrLf
                            Case 5
                                szXML = szXML + "<HeldCabin xsi:type=""xsd:string"">" + szSeatDetail(iLoop) + "</HeldCabin>" + vbCrLf
                            Case 6
                                szXML = szXML + "<PaxKey xsi:type=""xsd:string"">" + szSeatDetail(iLoop) + "</PaxKey>" + vbCrLf
                            Case 7
                                szXML = szXML + "<RecordLocator xsi:type=""xsd:string"">" + szSeatDetail(iLoop) + "</RecordLocator>" + vbCrLf
                            Case 8
                                szXML = szXML + "<RequestedSeat xsi:type=""xsd:string"">" + szSeatDetail(iLoop) + "</RequestedSeat>" + vbCrLf
                                szXML = szXML + "</item>" + vbCrLf
                        End Select
                    Next
                End If
            Next

            szXML = szXML + "</ReserveSeatRequests>" + vbCrLf
            szXML = szXML + "</input>" + vbCrLf
            szXML = szXML + "</ns1:reserveSeats>" + vbCrLf
            szXML = szXML + "</SOAP-ENV:Body>" + vbCrLf
            szXML = szXML + "</SOAP-ENV:Envelope>" + vbCrLf
        End If

        iInt = iHostID.GetUpperBound(0)

        aszString = Split(szString, "?")
        MsgBox(aszString.GetUpperBound(0))
        For Each szValue In aszString
            MsgBox("Inside loop")
        Next
        MsgBox("Outside loop")
        For iInt = 0 To aszString.GetUpperBound(0)
            MsgBox(aszString(iInt))
            szField = Split(aszString(iInt), "!")
            MsgBox(szField.GetUpperBound(0))
        Next
        MsgBox(iInt)
        szString = "[]"
        iInt = szString.IndexOf(">")
        iInt = InStr(szString, "A")

        iInt = InStr(szString, "[")
        szString = szString.Substring(iInt, szString.Length - 2)
        szString = szString.Trim()

        'szString = "profile_id=19022007101611&terminal=61099807&enctext=OTQzNTlCRjcyRDRBMjI5MEY3ODJFNzQwNDVBMTE0MzgzNEVCQzc5QzJDQjczOEU4RjUwMDFGNjdFRENFNDdGRjZERkZCMjc1OTlEQUYzQ0UxRDM1MjU1OTg4MzI3OUNENjY2RjQ3RkVBRjBERjcyOTA0QkJFRUE0QkU2MzAxNTgyMEE3NzhDMUUyRTgzNTIyOTU3Q0E4OTJGMzNGNDc5OUI4QTg3NTg2RTg4MzdFMjNERjU3QUQ1MDg4MzNEOTlBQjg4NjgyQzk2QjkwQUU4MkU4MUE2NzcyREIwREFDRkY4OTVFNzdENUI3RUNEMDM5RUY0NkY0MUVDOTNFMjc2NitDMzRBMjVFNDc4NDBDNkFENzE1MDA5MTlBMDYyNUY3RjJEOEM2OEE3RDBBREMyN0Y3MUMxOUJBRUEwNDY3N0E4Nzk4RjM2ODg0NTI4NTM3MjM0MkU1OEY4QjdBNUMyQkQ0QzIzNjk3MDFEMkVBQTAyNTg5OEEyM0UyMTcyNzkzRDlENDRGODdEOUY1ODAwNjQyOTFGQjhGRUZGRDNFQUFBOERCQzYwODM2NUU4QTA1RjFDQjgzQjYxMjVBQjU2NzM0NEEyOTc5RURBRTAwRDRENEM1QkM1MEVBMzU3RDJCNTlFRUFDQjBEMUFCNThEMDFBRjcyNUZERjkzODkzRDkzMDJERUYyN0E5MDQ5MUE3QTNCMjNEQjlCMDlCMjE0RUMxQURFOTJGNzEzQ0NGQTc5OEFEMjVFMTg1RUQ4OEQ0Mw=="
        szString = "profile_id=19022007101611&terminal=61099807&enctext=OTQzNTlCRjcyRDRBMjI5MEY3ODJFNzQwNDVBMTE0MzgzNEVCQzc5QzJDQjczOEU4RjUwMDFGNjdFRENFNDdGRjZERkZCMjc1OTlEQUYzQ0UxRDM1MjU1OTg4MzI3OUNENjY2RjQ3RkVBRjBERjcyOTA0QkJFRUE0QkU2MzAxNTgyMEE3NzhDMUUyRTgzNTIyOTU3Q0E4OTJGMzNGNDc5OUI4QTg3NTg2RTg4MzdFMjNERjU3QUQ1MDg4MzNEOTlBQjg4NjgyQzk2QjkwQUU4MkU4MUE2NzcyREIwREFDRkY4OTVFNzdENUI3RUNEMDM5RUY0NkY0MUVDOTNFMjc2NitDMzRBMjVFNDc4NDBDNkFENzE1MDA5MTlBMDYyNUY3RjJEOEM2OEE3RDBBREMyN0Y3MUMxOUJBRUEwNDY3N0E4Nzk4RjM2ODg0NTI4NTM3MjM0MkU1OEY4QjdBNUMyQkQ0QzIzNjk3MDFEMkVBQTAyNTg5OEEyM0UyMTcyNzkzRDlENDRGODdEOUY1ODAwNjQyOTFGQjhGRUZGRDNFQUFBOERCQzYwODM2NUU4QTA1RjFDQjgzQjYxMjVBQjU2NzM0NEEyOTc5RURBRTAwRDRENEM1QkM1MEVBMzU3RDJCNTlFRUFDQjBEMUFCNThEMDFBRjcyNUZERjkzODkzRDkzMDJERUYyN0E5MDQ5MUE3QTNCMjNEQjlCMDlCMjE0RUMxQURFOTJGNzEzQ0NGQTc5OEFEMjVFMTg1RUQ4OEQ0Mw=="
        aszString = Split(szString, "&", -1, CompareMethod.Text)
        For Each szFieldSet In aszString
            'Modified on 5 Aug 2009, to cater for the presence of rightmost "==" or "=", normally for encrypted or hash value
            If ("==" = Microsoft.VisualBasic.Strings.Right(szFieldSet, 2)) Then
                szFieldSet = Microsoft.VisualBasic.Strings.Left(szFieldSet, Len(szFieldSet) - 2) + "@@"
            ElseIf ("=" = Microsoft.VisualBasic.Strings.Right(szFieldSet, 1)) Then
                szFieldSet = Microsoft.VisualBasic.Strings.Left(szFieldSet, Len(szFieldSet) - 1) + "@"
            End If
            MsgBox(szFieldSet)

            szField = Split(szFieldSet, "=")

            If ("@@" = Microsoft.VisualBasic.Strings.Right(szField(1), 2)) Then
                szField(1) = Microsoft.VisualBasic.Strings.Left(szField(1), Len(szField(1)) - 2) + "=="
            ElseIf ("@" = Microsoft.VisualBasic.Strings.Right(szField(1), 1)) Then
                szField(1) = Microsoft.VisualBasic.Strings.Left(szField(1), Len(szField(1)) - 1) + "="
            End If
            MsgBox(szField(1))
        Next
    End Sub

    Public Function szGetTagValue(ByVal sz_SOAPMesg As String, ByVal sz_Tag2Search As String) As String
        Dim iStart As Integer = 0
        Dim iEnd As Integer = 0
        Dim szFilterString As String = ""
        Dim szResult As String = ""

        Try
            iStart = sz_SOAPMesg.IndexOf(sz_Tag2Search) 'zero based - first occurence of sz_Tag2Search
            If (iStart < 0) Then    'Not found
                szResult = ""
                Return szResult
            End If

            szFilterString = sz_SOAPMesg.Substring(iStart + sz_Tag2Search.Length) 'from end of sz_Tag2Search up to end of sz_SOAPMesg

            iStart = szFilterString.IndexOf(">") + 1            'Start offset of the searched tag's value
            iEnd = szFilterString.IndexOf("<") - 1              'End offset of the searched tag's value
            szResult = szFilterString.Substring(iStart, iEnd)   'Searched tag's value

        Catch ex As Exception

        End Try

        Return szResult
    End Function

    'Default log function
    Private Sub Log(ByVal sz_Text As String)
        Dim objStreamWriter As StreamWriter

        objStreamWriter = New StreamWriter("D:\Test_" + DateTime.Now.ToString("yyyy-MM-dd") + ".log", True)
        objStreamWriter.WriteLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + " >>> " + sz_Text)
        objStreamWriter.Close()
    End Sub

    '<Cofigurations>
    '   <AppSettings>
    '       <Log>[App]</Log>
    '       <LogLevel>0</LogLevel>
    '       <Interval>60000</Interval> <!-- Default= 900000 -->
    '       <EmailTemplate>[App]EmailTemplate.xml</EmailTemplate>
    '       <LogOption>1</LogOption> <!-- 0=Log to Registry; 1=Log to TextFile-->
    '       <IntervalLogPath>[App]Temp\\</IntervalLogPath>
    '   </AppSettings>
    '</Configurations>
    Public szConfigPath As String = "D:\Projects\Clients\AirAsia\PaymentGateway\NetMerchant3D\test\Settings.xml"
    Public szLogPath As String = ""
    Public szMachineID As String = ""
    Public szDBString As String = ""
    Public iLogLevel As Integer = -1

    Private Sub SOAPClient()
        Dim o As New SOAPClient.CSOAPClient
        Dim iRet As Integer
        Dim iMesgSet As Integer
        Dim iMesgNum As Integer
        'iRet = o.ConfirmBooking("D:\\Projects\\Clients\\AirAsia\\DirectDebit\\UASOAPClient\\Release\\UASOAPClient.dll", "OrderN", "GatewayTxnID", "MerchantTxnID", "HostTxnID", "123.45", "CurrCode", "OSPymtCode", "CIMB", iMesgSet, iMesgNum)
        iRet = o.ConfirmBooking("D:\\Projects\\Clients\\AirAsia\\DirectDebit\\bin\\UASOAPClient.dll", "T1TREL", "A07T1TREL08112815494", "T1TREL08112815494", "60000872", "160.00", "MYR", "D1", "CIMB", iMesgSet, iMesgNum)
        MsgBox(iRet.ToString())
        MsgBox(iMesgSet.ToString())
        MsgBox(iMesgNum.ToString())
    End Sub

    Private Sub HashTest()
        'TxnType=SALE&ServiceID=A07&MerchantName=AirAsia&MerchantTxnID=QZVLLI08111712172&OrderNumber=QZVLLI&OrderDesc=Booking No: QZVLLI, Sector: KUL-BKI, First Flight Date: 17 Jan 2009&TxnAmount=78.50&CurrencyCode=MYR&IssuingBank=CIMB&MerchantReturnURL=https://www.airasia.com/trylights/cgi-bin/skylights.cgi?page=DIRECTDEBIT;event=response&HashMethod=SHA1&HashValue=Rez0iALXrTBkf38i4XjQ73bllQw
        'TxnType=SALE&ServiceID=A07&MerchantName=AirAsia&MerchantTxnID=QZVLLI08111712004&OrderNumber=QZVLLI&OrderDesc=Booking No: QZVLLI, Sector: KUL-BKI, First Flight Date: 17 Jan 2009&TxnAmount=78.50&CurrencyCode=MYR&IssuingBank=CIMB&MerchantReturnURL=https://www.airasia.com/trylights/cgi-bin/skylights?page=DEBITRETURN;event=response&HashMethod=SHA1&HashValue=fm92hN2ZnJE23PypV/dtEONEUqE

        Dim szKey As String
        Dim szHashValue As String
        Dim objMD5 As MD5CryptoServiceProvider
        Dim objSHA1 As SHA1CryptoServiceProvider
        Dim objSHA256 As SHA256
        Dim btHashbuffer() As Byte
        Dim btHashHexbuffer() As Byte
        Dim szArray() As String
        Dim szField() As String

        'szKey = "bf28w3bg" + "A07PZ3JEN10011100003" + "A07" + "PZ3JEN10011100003" + "3" + "0.01" + "USD" + "" + "" + "2010011106369111"
        'szKey = "nv15p1qk" + "JPJ" + "TESTER11122900002"


        'Host(294eccaf9d834177451dc06257645220b1cdabf3) Gateway(KU7Mr52DQXdFHcBiV2RSILHNq/M=) Hash Method(SHA1)
        'Merchant(Password(SharedSecret) + MerchantID + MerchantTxnID + TxnStatus + TxnAmount + CurrencyCode + BankRefNo)
        'HashValue=da4b9237bacccdf19c0760cab7aec4a8359010b0
        '          1817c9b78ecc4a21015d972a8a1c5a3a7d317aaf
        'szKey = "a07p@ssword0000022222222262222222612021500000000000011https://ddgwsg.airasia.com/directdebit1/response_big.aspx1.00458"
        ' A07PZ3JEN11032810006A01A07PZ3JEN1103281000602.00THB297466
        'szKey = "directdebit2011"
        'szKey = "hi97f8mjA0800000000100007648A08100007648057.00MYR"
        'szKey = "nv15p1qkJPJTESTER12071700002" 'JPJ-Query hash
        szKey = "t1m2p380000101OM0000IPGSITOM160907005300"
        szHashValue = szComputeSHA1Hash(szKey)
        szKey = "affintest2014107GHL000IPGSIT20140324006IPGSIT20140324006https://test2pay.ghl.com/ipgsgom/response_abb.aspx1.00"
        szHashValue = szComputeSHA1BCHash(szKey, "L")

        'DxCM0B68Y+Df56s0+N/eDQ==
        objMD5 = New MD5CryptoServiceProvider
        objSHA1 = New SHA1CryptoServiceProvider
        objSHA256 = New SHA256Managed
        '0123456789ABCDEF0123456789ABCDEFRef1=2011030800000046&&Ref2=&&Amount=0123456789ABCDEF0123456789ABCDEF
        'szKey = "A0762C33876A2A234C88585853104356AF38C872100payVNDenA07PZ3JEN11071410021AIRASIAPZ3JEN11071410021https://ddgwsg.airasia.com/directdebit1/response_smartlink.aspx172.30.0.2071" '2ybQaM8hI9BCNBoAzm+C7A==

        'szKey = "1896A39062A052D5BC64F561746FC44D60C8349D100VC17054005550000000001paysslenTEST028600027000TIS000GHLSIT13103100004GHLSIT13103100004https://202.129.171.230/ipg/response_hsbc.aspx1"
        szKey = "ghlepayments:3986025:cashUtest"
        objMD5.ComputeHash(System.Text.Encoding.UTF8.GetBytes(szKey.ToCharArray))
        btHashbuffer = objMD5.Hash

        'btHashbuffer = objMD5.Hash
        szHashValue = szBytesToHex(btHashbuffer)  ''ComputeMD5HexHash
        'szHashValue = System.Convert.ToBase64String(btHashbuffer) 'z33yILyFu9sxKPI+zm0eUw==
        'MerID=010000099&AcqID=456376&OrderID=MBOMBOSIT17071300003&ResponseCode=1&ReasonCode=1&ReasonCodeDesc=Transaction is approved.&ReferenceNo=000021313341&PaddedCardNo=XXXXXXXXXXXX8195&AuthCode=584653&PurchaseAmt=000000000100&Signature=NigxHWWd6WkcoaxWGQdyxaIxLEA=&SignatureMethod=SHA1
        'szKey = "O2e5hTxP027017000118450618JPJDEVOT201305270001000000000100458"
        szKey = "t1m2p380000101OM0000IPGSITOM160908013300100"
        'GRWvNN5ulf+PEyDD7QoBUPxW01E=
        objSHA1.ComputeHash(System.Text.Encoding.UTF8.GetBytes(szKey.ToCharArray))
        btHashbuffer = objSHA1.Hash
        szHashValue = szBytesToHex(btHashbuffer)  'SHA1Hex    a1884747fd028b54ba719fc553c6d3aa404555c7

        szHashValue = System.Convert.ToBase64String(btHashbuffer) '"t0xhkv4bQdQBh6Ir7ZUtNAGLvo0=" "nNQW6ixGkHfVr96f38eR82i3J+w="

        szKey = "tgv12345TGVIPGOMsit170427019https://test2pay.ghl.com/IPGSimulatorOoiMei/RespFrmGW.aspxhttps://test2pay.ghl.com/IPGSimulatorOoiMei/RespFrmGW.aspx?test=testhttps://test2pay.ghl.com/IPGSimulatorOoiMei/RespFrmGW.aspx?test=test22https://test2pay.ghl.com/IPGSimulatorOoiMei/RespFrmGW.aspx?test1=123;test2=abc10.00MYR202.129.171.230180TNbZDSHcczAikhH1ebyihwhL+R4B1ykvLoX9N1jlFTc="
        objSHA256.ComputeHash(System.Text.Encoding.ASCII.GetBytes(szKey))
        btHashbuffer = objSHA256.Hash
        szHashValue = szWriteHex(btHashbuffer).ToLower()

        '43942EB4F324933824C38A0E7C7883038090AAE6
        szKey = "air2009$705A07FQYBCT09120217095https://ddgw.airasia.com/directdebit/response_bankrakyat.aspx179.00MYR"
        szHashValue = szComputeSHA160Hash(szKey)

        szKey = "airasia705705A07XN1GEM09120218263https://ddgwsg.airasia.com/directdebit/response_bankrakyat.aspx79.50MYR"
        szHashValue = szComputeSHA160Hash(szKey)

        'szKey = "_input_charset=utf-8&out_trade_no=A07PZ3JEN09120200002&partner=2088201457774322&return_url=https://ddgwsg.airasia.com/directdebit/response_alipay_int.aspx&service=create_forex_trade&subject=A07PZ3JEN09120200002&total_fee=0.01cu7wdztui0lm7bb72qcln3955smg8e3r"
        '"AF38C872queryDRAIRASIAA07PZ3JEN110721100941"
        'szKey = "A0762C33876A2A234C88585853104356AF38C872100payVNDenA07PZ3JEN11071410021AIRASIAPZ3JEN11071410021https://ddgwsg.airasia.com/directdebit1/response_smartlink.aspx172.30.0.2071"
        szKey = "D43B8BC88DC56307747C152FDE1B8F540505nsaop2IL44EZu0oY7HQ L t4OtQ=YYZUnsupportedUnsupportedUnsupported51100020160127UnsupportedVCpayenEPS00000PAYPRODTest6010028600046298InsufficientFundsPAY_PROD_Test60106027133646251100001638505YAAABA4MYZAAAAAAyMBhkAAAAAAA=3DS1"
        'szHashValue = szComputeMD5Hash(szKey, "L") '"H/EIDKwfA5O6eb4JO+6Ibg==" "h/eidkwfa5o6eb4jo+6ibg=="
        ' btHashHexbuffer = System.Text.Encoding.UTF8.GetBytes(szHashValue)
        szHashValue = szComputeMD5HexHash(szKey)
        btHashHexbuffer = System.Text.Encoding.UTF8.GetBytes(szHashValue.ToLower())
        szHashValue = Convert.ToBase64String(btHashHexbuffer)

        szKey = "M_AAFTTS_11906|A07I178DE10030411135|5.00|https://ddgwsg.airasia.com/directdebit/response_ccavenue.aspx|gmhrqmpi6mhxg8wco1"
        szKey = "M_AAFTTS_11906|A07I178DE10030411135|5.00|Y|gmhrqmpi6mhxg8wco1"
        szHashValue = szComputeAdler32Hash(szKey)


        'HMACSHA256
        'Dim szsecret As String = "BB48A64077A1CBF08FF0D91C5A9FE42B" '
        Dim szsecret As String = "B26F31E5FB5A724F8290B24448C645C8"
        Dim convertedHash As Byte() = New Byte(szsecret.Length / 2 - 1) {}
        For i As Integer = 0 To szsecret.Length / 2 - 1
            convertedHash(i) = CByte(Int32.Parse(szsecret.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber))
        Next
        'szKey = "user_SessionId=567890&vpc_AccessCode=75A6GH9&vpc_Amount=1000&vpc_CardExp=1305&vpc_CardNum=345678901234564&vpc_Command=pay&vpc_MerchTxnRef=txn1&vpc_Merchant=TNSITESTMERCHANT&vpc_Version=1"
        'szKey = "vpc_AccessCode=4EB84417&vpc_AddendumData=OoiMei Test 2&vpc_Amount=1100&vpc_Card=Mastercard&vpc_CardExp=2012&vpc_CardNum=5204740009900014&vpc_Command=pay&vpc_Gateway=ssl&vpc_Locale=en&vpc_MerchTxnRef=OM2000IPGOMSIT161028003&vpc_Merchant=TEST028600046000&vpc_OrderInfo=TEST028600046000&vpc_ReturnURL=https://test2pay.ghl.com/ipgsgom/response_gpay2.aspx&vpc_Version=1"
        szKey = "vpc_3DSECI=05&vpc_3DSXID= l LukS9HsmeuzOPDCo4mwq Yu4=&vpc_3DSenrolled=Y&vpc_3DSstatus=Y&vpc_AVSRequestCode=Z&vpc_AVSResultCode=Unsupported&vpc_AcqAVSRespCode=Unsupported&vpc_AcqCSCRespCode=Unsupported&vpc_AcqResponseCode=00&vpc_Amount=2000&vpc_AuthorizeId=005939&vpc_BatchNo=20161104&vpc_CSCResultCode=Unsupported&vpc_Card=VC&vpc_Command=pay&vpc_Locale=en&vpc_MerchTxnRef=EPS0000000EPAYWEB122805&vpc_Merchant=028600046298&vpc_Message=Approved&vpc_OrderInfo=EPAY_WEB_122805&vpc_ReceiptNo=630913352770&vpc_RiskOverallResult=ACC&vpc_TransactionNo=21121&vpc_TxnResponseCode=0&vpc_VerSecurityLevel=05&vpc_VerStatus=Y&vpc_VerToken=AAABBiIyWAAAAAAJkjJYAAAAAAA=&vpc_VerType=3DS&vpc_Version=1"
        ' Build string from collection in preperation to be hashed
        'Dim sb As New StringBuilder()
        'Dim list As Generic.SortedList(Of String, String)
        'For Each kvp As Generic.KeyValuePair(Of String, String) In list
        '    If Not String.IsNullOrEmpty(kvp.Value) Then
        '        sb.Append(kvp.Key + "=" + kvp.Value + "&")
        '    End If
        'Next
        'sb.Remove(sb.Length - 1, 1)

        ' Create secureHash on string
        Dim hexHash As String = ""
        Using hasher As New HMACSHA256(convertedHash)
            Dim utf8bytes As Byte() = Encoding.UTF8.GetBytes(szKey) 'sb.tostring()
            Dim iso8859bytes As Byte() = Encoding.Convert(Encoding.UTF8, Encoding.GetEncoding("iso-8859-1"), utf8bytes)
            Dim hashValue As Byte() = hasher.ComputeHash(iso8859bytes)
            hexHash = szWriteHex(hashValue)
            'For Each b As Byte In hashValue
            '    hexHash += b.ToString("X2")
            'Next
        End Using
        szHashValue = hexHash

        'G->ACD10485922B4CF66F6E5D57243FFCDED726F2C01AC7AD2F51B24EAC50A69D2A
        'H->39905922C7CD6E975AB185832824AD79EDE121C5FA222EE581EC891E84A86007
        '"39905922C7CD6E975AB185832824AD79EDE121C5FA222EE581EC891E84A8607"



    End Sub

    Public Function szComputeAdler32Hash(ByVal sz_Key As String) As String
        Dim lAdler As Long = 0

        Try
            lAdler = 1
            lAdler = lAdler32(lAdler, sz_Key)
        Catch ex As Exception
            Throw ex
        End Try

        Return CStr(lAdler)
    End Function

    Public Function lAdler32(ByVal i_Adler As Integer, ByVal sz_Buf As String) As Long
        Dim iBASE As Integer = 0
        Dim iLoop As Integer = 0
        Dim iS1 As Integer = 0
        Dim iS2 As Integer = 0

        Try
            iBASE = 65521
            iS1 = lAndop(i_Adler, 65535)
            iS2 = lAndop(lCDec(szRightShift(szCBin(i_Adler), 16)), 65535)

            For iLoop = 1 To Len(sz_Buf)
                iS1 = (iS1 + Asc(Mid(sz_Buf, iLoop, 1))) Mod iBASE
                iS2 = (iS2 + iS1) Mod iBASE
            Next
        Catch ex As Exception
            Throw ex
        End Try

        Return lCDec(szLeftShift(szCBin(iS2), 16)) + iS1
    End Function

    Public Function lAndop(ByVal l_Op1 As Long, ByVal l_Op2 As Long) As Long
        Dim szOp3 As String = ""
        Dim szOp4 As String = ""
        Dim szOp As String = ""
        Dim iLoop As Integer = 0

        Try
            szOp3 = szCBin(l_Op1)
            szOp4 = szCBin(l_Op2)

            For iLoop = 1 To 32
                szOp = szOp & "" & (CInt(Mid(szOp3, iLoop, 1)) And CInt(Mid(szOp4, iLoop, 1)))
            Next
        Catch ex As Exception
            Throw ex
        End Try

        Return lCDec(szOp)
    End Function

    'Function used for bitwise right shifting
    Public Function szRightShift(ByVal sz_Str As String, ByVal i_Num As Integer) As String
        Dim iLoop As Integer = 0

        Try
            For iLoop = 1 To i_Num
                sz_Str = "0" & sz_Str
                sz_Str = Microsoft.VisualBasic.Left(sz_Str, Len(sz_Str) - 1)
            Next
        Catch ex As Exception
            Throw ex
        End Try

        Return sz_Str
    End Function

    'Function used for bitwise left shifting
    Public Function szLeftShift(ByVal sz_Str As String, ByVal i_Num As Integer) As String
        Dim iLoop As Integer = 0

        Try
            For iLoop = 1 To 32 - Len(sz_Str)
                sz_Str = "0" & sz_Str
            Next

            For iLoop = 1 To i_Num
                sz_Str = sz_Str & "0"
                sz_Str = Microsoft.VisualBasic.Right(sz_Str, Len(sz_Str) - 1)
            Next
        Catch ex As Exception
            Throw ex
        End Try

        Return sz_Str
    End Function

    'Function used for decimal to binary conversion
    Public Function szCBin(ByVal l_Num As Long) As String
        Dim szBin As String = ""
        Dim iLoop As Integer = 0

        Try
            Do
                szBin = CStr((l_Num Mod 2)) & szBin
                l_Num = Fix(l_Num / 2)
            Loop While Not ((l_Num = 0))

            For iLoop = 1 To 32 - Len(szBin)
                szBin = "0" & szBin
            Next
        Catch ex As Exception
            Throw ex
        End Try

        Return szBin
    End Function

    'Function used for  binary to decimal conversion
    Public Function lCDec(ByVal sz_Num As String) As Long
        Dim iLoop As Integer = 0
        Dim lDec As Long = 0

        Try
            For iLoop = 1 To Len(sz_Num)
                lDec = lDec + CInt(Mid(sz_Num, iLoop, 1)) * lPower(Len(sz_Num) - iLoop)
            Next
        Catch ex As Exception
            Throw ex
        End Try

        Return lDec
    End Function

    'Function used for calculate value of 2^n
    Public Function lPower(ByVal l_Num As Long) As Long
        Dim lResult As Long = 1
        Dim iLoop As Integer = 0

        Try
            For iLoop = 1 To l_Num
                lResult = lResult * 2
            Next
        Catch ex As Exception
            Throw ex
        End Try

        Return lResult
    End Function

    Public Function szComputeSHA160Hash(ByVal sz_Key As String) As String
        Dim szHashValue As String = ""
        Dim objSHA160 As New SHA1CryptoServiceProvider
        Dim btHashbuffer() As Byte = System.Text.Encoding.ASCII.GetBytes(sz_Key)

        Try
            btHashbuffer = objSHA160.ComputeHash(btHashbuffer)
            szHashValue = szWriteHex(btHashbuffer)
        Catch ex As Exception
            Throw ex
        Finally
            If Not objSHA160 Is Nothing Then objSHA160 = Nothing
        End Try

        Return szHashValue
    End Function

    Private Sub EncodeTest()
        Dim szData As String
        Dim szOutput As String

        'szData = "22;5;A07TS56LP09111914581;2285714.29;VND;19112009145837"
        'szData = "Ref1=2013011000000016&&Ref2=&&Amount=" '"UmVmMT0yMDExMDMwMjAwMDAwMDMwJiZSZWYyPSYmQW1vdW50PTIuMDA="
        'szData = "0nhMg0exS+w=q098oBHrDNraLFLr9xUlFBhBn5hbfkNh915rjuBeopUC7CabjSPerFvVBfVVCr/BHpyvXdgeCFKv5DgLUcD7vTzV0xeVtbkS(+H1bG0D8SC2Pa4dRLEiNCjD79E6xnP4Do3bbEW0Uqz8="
        szData = "TgEVXcWi/T/6GlKyWx57EeZ9wAs="
        szOutput = szEncode(szData)
        szOutput = szDecode(szData)
    End Sub

    Public Function szComputeMD5HexHash(ByVal sz_Key As String) As String
        Dim szHashValue As String
        Dim objMD5 As MD5CryptoServiceProvider
        Dim btHashbuffer() As Byte

        Try
            objMD5 = New MD5CryptoServiceProvider
            objMD5.ComputeHash(System.Text.Encoding.UTF8.GetBytes(sz_Key.ToCharArray))
            btHashbuffer = objMD5.Hash

            szHashValue = szBytesToHex(btHashbuffer)

        Catch ex As Exception
            Throw ex
        Finally
            If Not objMD5 Is Nothing Then objMD5 = Nothing
        End Try

        Return szHashValue
    End Function

    Public Function szComputeMD5Hash(ByVal sz_Key As String, ByVal sz_Case As String) As String
        Dim szHashValue As String = ""
        Dim objMD5 As MD5CryptoServiceProvider
        Dim btHashbuffer() As Byte

        Try
            objMD5 = New MD5CryptoServiceProvider
            objMD5.ComputeHash(System.Text.Encoding.UTF8.GetBytes(sz_Key.ToCharArray))
            btHashbuffer = objMD5.Hash
            szHashValue = System.Convert.ToBase64String(btHashbuffer)
            'Added on 15 Oct 2009
            If ("L" = sz_Case.ToUpper()) Then
                szHashValue = szHashValue.ToLower()
            ElseIf ("U" = sz_Case.ToUpper()) Then
                szHashValue = szHashValue.ToUpper()
            End If

        Catch ex As Exception
            Throw ex
        Finally
            If Not objMD5 Is Nothing Then objMD5 = Nothing
        End Try

        Return szHashValue
    End Function

    Public Function szBytesToHex(ByVal bt_bytes As Byte()) As String
        Dim sbHexStr As New StringBuilder(bt_bytes.Length)  ' System.Text
        Dim szHex As String = ""
        Dim iCounter As Integer = 0

        For iCounter = 0 To bt_bytes.Length - 1
            sbHexStr.Append(bt_bytes(iCounter).ToString("X2"))
        Next

        szHex = sbHexStr.ToString()

        If Not sbHexStr Is Nothing Then
            sbHexStr = Nothing
        End If

        Return szHex
    End Function

    Public Function szWriteHex(ByVal bt_Array() As Byte) As String
        Dim szHex As String = ""
        Dim iCounter As Integer

        For iCounter = 0 To (bt_Array.Length() - 1)
            szHex += bt_Array(iCounter).ToString("X") '("X2")
        Next

        Return szHex
    End Function

    Function szDecode(ByVal sz_Data As String) As String
        Dim bt() As Byte
        Dim szOutput As String

        bt = System.Convert.FromBase64String(sz_Data)
        szOutput = System.Text.Encoding.ASCII.GetString(bt)

        Return szOutput
    End Function

    Function szEncode(ByVal sz_Data As String) As String
        Dim bt() As Byte
        Dim szOutput As String

        bt = System.Text.Encoding.ASCII.GetBytes(sz_Data.ToCharArray)
        szOutput = System.Convert.ToBase64String(bt)

        Return szOutput
    End Function

    Public Function szComputeSHA1Hash(ByVal sz_Key As String) As String
        Dim szHashValue As String = ""
        Dim objSHA1 As SHA1CryptoServiceProvider
        Dim btHashbuffer() As Byte

        Try
            'UTF-8 (8-bit UCS/Unicode Transformation Format) is a variable-length character encoding for Unicode.
            'It is able to represent any character in the Unicode standard, yet the initial encoding of byte codes and
            'character assignments for UTF-8 is backwards compatible with ASCII.
            'For these reasons, it is steadily becoming the preferred encoding for e-mail, web pages, and other places
            'where characters are stored or streamed.
            'String.ToCharArray - Copies the characters in this instance to a Unicode character array.
            'System.Text.Encoding.UTF8 - This class encodes Unicode characters using UCS Transformation Format, 8-bit form (UTF-8).
            '.GetBytes - This method encodes characters into an array of bytes.
            'System.Convert.ToBase64String - Converts the value of an array of 8-bit unsigned integers to its equivalent
            'String representation consisting of base 64 digits.

            objSHA1 = New SHA1CryptoServiceProvider
            objSHA1.ComputeHash(System.Text.Encoding.UTF8.GetBytes(sz_Key.ToCharArray))
            btHashbuffer = objSHA1.Hash
            szHashValue = System.Convert.ToBase64String(btHashbuffer)

        Catch ex As Exception
            'oLogger.Log(Logger.CLogger.LOG_SEVERITY.WARNING, __FILE__, __METHOD__, __LINE__, "Err: " + ex.ToString)
            Throw ex
        Finally
            If Not objSHA1 Is Nothing Then objSHA1 = Nothing
        End Try

        Return szHashValue
    End Function

    Public Function szComputeSHA1BCHash(ByVal sz_Key As String, ByVal sz_Case As String) As String
        Dim szHashValue As String = ""
        Dim objSHA1 As SHA1CryptoServiceProvider
        Dim btHashbuffer() As Byte

        Try
            objSHA1 = New SHA1CryptoServiceProvider
            objSHA1.ComputeHash(System.Text.Encoding.UTF8.GetBytes(sz_Key.ToCharArray))
            btHashbuffer = objSHA1.Hash
            szHashValue = BitConverter.ToString(btHashbuffer)
            szHashValue = szHashValue.Replace("-", "")
            If ("L" = sz_Case.ToUpper()) Then
                szHashValue = szHashValue.ToLower()
            End If

        Catch ex As Exception
            Throw ex
        Finally
            If Not objSHA1 Is Nothing Then objSHA1 = Nothing
        End Try

        Return szHashValue
    End Function

    Public Function szComputeSHA256Hash(ByVal sz_Key As String) As String
        Dim szHashValue As String = ""
        Dim objSHA256 As SHA256
        Dim btHashbuffer() As Byte

        Try
            objSHA256 = New SHA256Managed
            objSHA256.ComputeHash(System.Text.Encoding.ASCII.GetBytes(sz_Key))
            btHashbuffer = objSHA256.Hash
            szHashValue = szWriteHex(btHashbuffer).ToLower()

        Catch ex As Exception
            Throw ex
        Finally
            If Not objSHA256 Is Nothing Then objSHA256 = Nothing
        End Try

        Return szHashValue
    End Function

    Private Sub LoadConfig()
        Dim iCount As Integer = 0
        Dim iCountRead As Integer = 0
        Dim objXMLReader As XmlTextReader
        Dim szTemp As String = ""

        Try
            Log("Load XML From : " + szConfigPath)
            objXMLReader = New XmlTextReader(szConfigPath)

            While (Not objXMLReader.EOF)
                objXMLReader.Read()
                szTemp = objXMLReader.Name

                If (objXMLReader.NodeType <> XmlNodeType.EndElement) Then
                    Select Case szTemp.ToLower()
                        Case "logpath"
                            szLogPath = objXMLReader.ReadInnerXml()
                        Case "loglevel"
                            iLogLevel = Convert.ToInt32(objXMLReader.ReadInnerXml())
                        Case "machineid"
                            szMachineID = objXMLReader.ReadInnerXml()
                        Case "dbstring"
                            szDBString = objXMLReader.ReadInnerXml()
                    End Select
                End If
            End While

        Catch ex As Exception
            Log("Error reading Config File!")
            Log("Err [LoadConfig] : " + ex.Message)
        End Try

        objXMLReader.Close()
        objXMLReader = Nothing
    End Sub
    Private Function LoadLibraryTest() As Boolean

        'Use the file name to load the assembly into the current application domain.
        'Dim a As [Assembly]
        'Dim myType As Type()
        'Dim mymethod As MethodInfo()
        'a = [Assembly].LoadFrom("E:\DATA\D\Programming\Projects\AirAsia\NetMerchant3D\NetMerchant3D\bin\WebCommII.dll")
        'Get the type to use.
        'myType = a.GetType("adname")
        'Get the method to call.
        'mymethod = a.GetTypes()(0).GetMethod("GetContext")
        'Create an instance
        'obj = Activator.CreateInstance(myType)
        'Execute the adnamemethod method.
        'mymethod.Invoke(obj, null)

        'Dim SampleAssembly As [Assembly]
        ''' You must supply a valid fully qualified assembly name here.            
        ''SampleAssembly = [Assembly].Load("Assembly text name, Version, Culture, PublicKeyToken")
        'SampleAssembly = [Assembly].LoadFrom("E:\DATA\D\Programming\Projects\AirAsia\NetMerchant3D\NetMerchant3D\bin\WebCommII.dll")
        'Dim Types As Type() = SampleAssembly.GetTypes()
        'Dim oType As Type
        '' Display all the types contained in the specified assembly.
        'For Each oType In Types
        '    MsgBox(oType.Name.ToString())
        'Next oType

        MsgBox([Assembly].GetExecutingAssembly().Location) '.Replace([Assembly].GetExecutingAssembly().GetModules()(0).FullyQualifiedName, "")
        MsgBox([Assembly].GetExecutingAssembly().GetModules()(0).Name)
        MsgBox([Assembly].GetExecutingAssembly().Location.Replace([Assembly].GetExecutingAssembly().GetModules()(0).Name, "Settings.xml"))

        Dim szString As String = "SALE|REQ|GET|payeeId|[=AIRASIA]|billAccountNo|[OrderNumber]|billReferenceNo|[GatewayTxnID]|billReferenceNo2|[]|billReferenceNo3|[]|billReferenceNo4|[]|amount|[TxnAmount]|payeeResponseURL|[GatewayReturnURL]"
        Dim szSplit() As String
        szSplit = Split(szString, "|")
        MsgBox(szSplit.GetLowerBound(0).ToString())
        MsgBox(szSplit.GetUpperBound(0).ToString())

        Dim acServiceID() As Char
        Dim szServiceID As String
        Dim szTxnID As String
        Dim szRunningNo As String
        Dim iNumOfZeroes As Integer

        szString = "CIMB:SALE|RES|GET|billAccountNo|[OrderNumber]"
        Dim szArray() As String
        Dim iPosition As Integer
        Dim szURL As String = "http://url?page=1"

        iPosition = InStr(szURL, "?")
        MsgBox(iPosition)

        szArray = Split(szString, "|")

        MsgBox(szArray.GetLowerBound(0))
        MsgBox(szArray.GetUpperBound(0))

        szArray(0) = "string"
        szArray(1) = "string"
        szArray(2) = "string"
        szArray(3) = "string"
        szArray(4) = "string"
        szArray(5) = "string"   'exception out of bound

        szTxnID = ""
        szRunningNo = "1"
        szServiceID = "A"
        acServiceID = szServiceID.ToCharArray()
        'Converts "A" to "65" then minus 32 because decimal 0 to 32 is not printable. "z" is 122, 122-32=90 (2 digits)
        szTxnID = szTxnID + (Decimal.op_Implicit(acServiceID(0)) - 32).ToString()
        'Append the remainder two digits of Service ID, e.g. "07" from "A07", resulting in "3307" (4 digits)
        szTxnID = szTxnID + "07"
        'Append running number based on Service ID retrieved from database, then pad with leading zeroes.
        iNumOfZeroes = 20 - 4 - 1
        szTxnID = szTxnID + "".PadRight(iNumOfZeroes, "0") + szRunningNo
        MsgBox(szTxnID.Length)

        Dim iSeed As Integer = 0
        Dim szTest As String = ""
        Dim iTest As Integer = 0
        Dim ac() As Char
        iSeed = Convert.ToInt32(DateTime.Now.ToString("mmssfff")) Mod (1000000)
        iTest = Convert.ToInt32(Rnd(iSeed))
        szTest = String.Format("{0:000000}", Convert.ToString(Convert.ToInt32(Rnd(iSeed) * 1000000)))
        szTest = "012"
        ac = szTest.ToCharArray()
        szTest = Decimal.op_Implicit(ac(0)).ToString + Decimal.op_Implicit(ac(1)).ToString + Decimal.op_Implicit(ac(2)).ToString

        MsgBox(Decimal.op_Implicit(ac(0)))
        MsgBox(Decimal.op_Implicit(ac(1)))
        MsgBox(Decimal.op_Implicit(ac(2)))

        Dim SampleAssembly As [Assembly]
        Dim Method As MethodInfo
        Dim parameters As ParameterInfo()
        Dim obj, objtest
        Dim szContextVal As String

        Dim i As Integer

        'Tested OK
        'SampleAssembly = [Assembly].LoadFrom("D:\Projects\Clients\AirAsia\PaymentGateway\NetMerchant3D\WebCommII\bin\WebCommII.dll")

        'Tested - NEGATIVE for UASOAPClient.dll written in C++, why? because UASOAPClient.dll written in C++, considered unmanaged code for .NET
        'SampleAssembly = [Assembly].LoadFrom("D:\Projects\Philippines\CTI\SOAPClient_Tester\UASOAPClient.dll")
        'SampleAssembly = [Assembly].LoadFrom("D:\Projects\Clients\AirAsia\DirectDebit\SOAPClient\release\SOAPClient.dll")

        '        Dim same = SampleAssembly.GetTypes("WebCommII.SecureCom")

        'Tested OK for both FullName and ToString
        'szContextVal = String.Format("{0}", SampleAssembly.GetTypes()(0).FullName) 'ToString())    '0-SecureCom; 1-SecurityPolicy; 2-RespReader

        'Tested OK
        'szContextVal = String.Format("{0}", SampleAssembly.GetType("WebCommII.SecureCom"))

        'Tested OK
        'Searches for the specified method whose parameters match the specified argument types and modifiers, using the specified binding constraints and
        'the specified calling convention
        'ByVal sz_ContextName As String, ByVal sz_ContextVal As Long
        ' Obtain a reference to a method known to exist in assembly.
        'Method = SampleAssembly.GetType("WebCommII.SecureCom").GetMethod("SetContext", BindingFlags.Public Or BindingFlags.Instance, Nothing, CallingConventions.Any, New Type() {GetType(String), GetType(Long)}, Nothing)
        'szContextVal = String.Format("{0}", Method)

        ' Call a method with parameters.
        'Method.Invoke(...) or


        '>>Dim args() As Object = {"ConnTimeout", 18}
        '>>Dim result As Object
        'Dim instance As [Object] = SampleAssembly.GetType("WebCommII.SecureCom").InvokeMember(Nothing, BindingFlags.DeclaredOnly Or BindingFlags.Public Or BindingFlags.NonPublic Or BindingFlags.Instance Or BindingFlags.CreateInstance, Nothing, Nothing, Nothing)

        'result = SampleAssembly.GetType("WebCommII.SecureCom").InvokeMember("SetContext", BindingFlags.InvokeMethod, Nothing, instance, args)
        '>>szContextVal = String.Format("SetContext {0} to {1}", args(0), args(1), result)

        '        obj = Activator.CreateInstance(SampleAssembly.GetTypes()(0))
        '>>obj = Activator.CreateInstance(SampleAssembly.GetType("WebCommII.SecureCom"))
        '>>Dim aa As WebCommII.SecureCom
        '>>aa = CType(obj, WebCommII.SecureCom)
        '>>aa.SetContext("StreamURL", szContextVal)

        '>>Method.Invoke(obj, New [Object]() {"StreamRes", "hahaValue"}) ' ByVal sz_ContextName As String, ByRef sz_ContextVal As String

        '>>Method = SampleAssembly.GetTypes()(0).GetMethod("GetContext")
        '>>Method.Invoke(obj, New [Object]() {"StreamRes", szContextVal})
        '>>MsgBox("ContextVal: " & szContextVal)

        ' Obtain a reference to the parameters collection of the MethodInfo instance.
        '>>Dim Params As ParameterInfo() = Method.GetParameters()
        ' Display information about method parameters.
        'Param = sParam1
        'Type = System.String
        'Position = 0
        '   Optional=False
        '>>Dim Param As ParameterInfo
        '>>For Each Param In Params
        '>>MsgBox(("Param=" + Param.Name.ToString()))
        '>>MsgBox(("  Type=" + Param.ParameterType.ToString()))
        '>>MsgBox(("  Position=" + Param.Position.ToString()))
        '>>MsgBox(("  Optional=" + Param.IsOptional.ToString()))
        '>>Next Param

        'Public Class MyClass1
        '    Public myFieldB As Short
        '    Public myFieldA As Integer

        '    Public Overloads Sub MyMethod(ByVal i As Long, ByVal k As Char)
        '        Console.WriteLine(ControlChars.NewLine & "This is MyMethod(long i, char k).")
        '    End Sub 'MyMethod

        '    Public Overloads Sub MyMethod(ByVal i As Long, ByVal j As Long)
        '        Console.WriteLine(ControlChars.NewLine & "This is MyMethod(long i, long j).")
        '    End Sub 'MyMethod
        'End Class 'MyClass1


        'Public Class Binder_Example
        '    Public Shared Sub Main()
        '        ' Get the type of MyClass1.
        '        Dim myType As Type = GetType(MyClass1)
        '        ' Get the instance of MyClass1.
        '        Dim myInstance As New MyClass1()
        '        Console.WriteLine(ControlChars.Cr & "Displaying the results of using the MyBinder binder.")
        '        Console.WriteLine()
        '        ' Get the method information for MyMethod.
        '        Dim myMethod As MethodInfo = myType.GetMethod("MyMethod", BindingFlags.Public Or BindingFlags.Instance, New MyBinder(), New Type() {GetType(Short), GetType(Short)}, Nothing)
        '        Console.WriteLine(MyMethod)
        '        ' Invoke MyMethod.
        '        myMethod.Invoke(myInstance, BindingFlags.InvokeMethod, New MyBinder(), New [Object]() {CInt(32), CInt(32)}, CultureInfo.CurrentCulture)
        '    End Sub 'Main
        'End Class 'Binder_Example

        Return True
    End Function
    Public Sub ASCIITest()
        Dim iValue As Integer
        Dim szTestStr As String
        Dim szTestMID As String
        Dim szResult As String

        szTestMID = "000000020423117"
        szTestStr = "GHLIPGSIT20140410012"
        'szTestMID = "111112222233333"
        'szTestStr = "111222"

        For Each c As Char In szTestStr
            iValue += Asc(c)
        Next
        'iValue = 1000
        If (1000 <= iValue) Then
            iValue = iValue Mod 1000
        End If

        szResult = Convert.ToString(Convert.ToInt64(szTestMID) + iValue)

    End Sub

    Public Sub PBEWithMD5AndDES()
        Dim szEncryptResult As String
        Dim szDecryptResult As String
        Dim aPwd As Char()
        Dim szReqData As String = ""
        Dim szResData As String = ""
        Dim szEncrpytData As String = ""
        Dim szTxnVer As String = "1.1"
        Dim szTxnTyp As String = "SALS"
        Dim szTxnID As String = "GHLIPGSIT20131227003"
        Dim szPAN As String = "4444555566667777"
        Dim szExpired As String = "1217"
        Dim szMID As String = "20423338"
        Dim szAmt As String = "100"
        Dim szTxnDesc As String = "Testing from 81 stg apps server"
        Dim szTxnDate As String = "10122013 11:27:29"
        Dim szCVV2 As String = "115"
        Dim szRef As String = "test3117"

        Dim szTxnStatus As String = "201"
        Dim szTxnStatusDesc As String = "Error during 3D authentication : 1388125078824"
        Dim szAuthCode As String = ""
        Dim szRRN As String = ""

        'aPwd = "20423117test3117".ToCharArray()

        'szReqData = szTxnVer + vbLf + szTxnTyp + vbLf + szTxnID + vbLf + szPAN + vbLf + szExpired + vbLf + szMID + vbLf + _
        'szAmt + vbLf + szTxnDesc + vbLf + szTxnDate + vbLf + szCVV2 + vbLf + "" + vbLf + "" + vbLf + "" + vbLf + _
        '"" + vbLf + "" + vbLf + szRef + vbLf + "" + vbLf + "" + vbLf

        'szEncryptResult = PBE.EncryptPBEMD5DES(aPwd, szReqData, "PBEWithMD5AndDES")

        aPwd = "20423117test3117".ToCharArray()

        szResData = szTxnVer + vbLf + szTxnTyp + vbLf + szTxnID + vbLf + szPAN + vbLf + szAmt + vbLf + _
        szTxnStatus + vbLf + szTxnStatusDesc + vbLf + szAuthCode + vbLf + szRRN + vbLf + _
        "" + vbLf + "" + vbLf + "" + vbLf + "" + vbLf + "" + vbLf

        szEncryptResult = PBE.EncryptPBEMD5DES(aPwd, szResData, "PBEWithMD5AndDES")

        'Log(szEncryptResult.Replace(vbCrLf, "").Replace(vbLf, ""))
        szEncrpytData = "iLLgdqP4Yys=Fxw9uvb+1+fvAo9f1cCcXXJoOgm+HSaVNYcI2OC5gY286z5kj2SaK5Wp9yYtUlTM4Hf9HDU/zNCQ1ZGyhfD/1MAruLJY9WFbiekOGM8cYjm7HZW6dxqE43wuqr49sFeFdT+otS2LnbwGykju+Newpw=="
        szDecryptResult = PBE.DecryptPBEMD5DES(aPwd, szEncrpytData, "PBEWithMD5AndDES")

    End Sub

    Public Sub DateTimestamp()
        Dim szTimeStamp As String
        Dim szCurrDateTime As String
        Dim tsOffSetDateTime As TimeSpan
        Dim tzLocalTimeZone As TimeZone = TimeZone.CurrentTimeZone
        Dim osDate As DateTimeOffset

        osDate = New DateTimeOffset("2013-10-16 15:49:26")
        szCurrDateTime = osDate.ToString("yyyy-MM-dd'T'HH:mm:ss.fffzzz") '+ "-" + osDate.Offset.
        'szCurrDateTime = DateTime.UtcNow.ToString()
        'szTimeStamp = DateDiff("s", "01/01/1970 00:00:00", Now()).ToString()
        '2016-05-27T12:38:40.479+05:30
        '2017-04-10T11:27:02.940
        tsOffSetDateTime = tzLocalTimeZone.GetUtcOffset(DateTime.Now())
        szTimeStamp = DateTime.Now().ToString("yyyy-MM-dd'T'HH:mm:ss.fff") + tsOffSetDateTime.TotalHours.ToString()

    End Sub
    Public Sub IntegerTest()
        Dim iValue As Integer
        Dim szString As String = ""
        Dim aValue() As Char

        iValue = 919
        szString = iValue.ToString

        aValue = szString.ToCharArray()

    End Sub

    Public Class MyClass1
        Public myFieldB As Short
        Public myFieldA As Integer

        Public Overloads Sub MyMethod(ByVal i As Long, ByVal k As Char)
            Console.WriteLine(ControlChars.NewLine & "This is MyMethod(long i, char k).")
        End Sub 'MyMethod

        Public Overloads Sub MyMethod(ByVal i As Long, ByVal j As Long)
            Console.WriteLine(ControlChars.NewLine & "This is MyMethod(long i, long j).")
        End Sub 'MyMethod
    End Class 'MyClass1

    Private Function Test() As Boolean
        Dim iTest As Integer
        Dim iZero As Integer

        iZero = 1

        Try
            iTest = 1 / iZero
            MsgBox("try")
            Return True
        Catch ex As Exception
            MsgBox("catch")
        Finally
            MsgBox("final")
        End Try

        MsgBox("return")
        Return False
    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szComputeFPXCheckSum
    ' Function Type:	String. Hashing value.
    ' Parameter:		sz_HashStr and sz_CertKey. String values to be sign with key.
    ' Description:		Checksum for Direct FPX
    ' History:			5 May 2015, added by OoiMei. FPXD
    '********************************************************************************************
    Public Function szComputeFPXCheckSum(ByVal sz_HashStr As String, ByVal sz_CertKey As String) As String
        Dim rsaCsp As RSACryptoServiceProvider = Nothing
        Dim dataBytes As Byte() = Nothing
        Dim signatureBytes As Byte() = Nothing

        Try
            rsaCsp = LoadCertificateFile(sz_CertKey)
            dataBytes = Encoding.Default.GetBytes(sz_HashStr)
            signatureBytes = rsaCsp.SignData(dataBytes, "SHA1")

            Return BitConverter.ToString(signatureBytes).Replace("-", Nothing)
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	LoadCertificateFile
    ' Function Type:	RSACryptoServiceProvider
    ' Parameter:		sz_CertKey. Certificate key path
    ' Description:		Checksum for Direct FPX 
    ' History:			5 May 2015, added by OoiMei. FPXD
    '********************************************************************************************
    Private Shared Function LoadCertificateFile(ByVal sz_CertKey As String) As RSACryptoServiceProvider
        Dim rsaCsp As RSACryptoServiceProvider = Nothing
        Dim btData As Byte() = Nothing
        Dim btRes As Byte() = Nothing

        Using fs As FileStream = File.OpenRead(sz_CertKey)
            btData = New Byte(fs.Length - 1) {}
            fs.Read(btData, 0, btData.Length)

            If btData(0) <> &H30 Then
                btRes = btGetPem("RSA PRIVATE KEY", btData)
            End If

            Try
                rsaCsp = DecodeRSAPrivateKey(btRes)
                Return rsaCsp
            Catch ex As Exception
                Throw ex
            End Try

            Return Nothing
        End Using

    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	btGetPem
    ' Function Type:	Bytes.
    ' Parameter:		sz_Type and sz_Data. 
    ' Description:		Checksum for Direct FPX 
    ' History:			5 May 2015, added by OoiMei. FPXD
    '********************************************************************************************
    Private Shared Function btGetPem(ByVal sz_Type As String, ByVal sz_Data As Byte()) As Byte()
        Dim szPEM As String = ""
        Dim szHeader As String = ""
        Dim szFooter As String = ""
        Dim iStart As Integer = 0
        Dim iEnd As Integer = 0
        Dim szBase64 As String = ""

        szPEM = Encoding.UTF8.GetString(sz_Data)
        szHeader = String.Format("-----BEGIN {0}-----\n", sz_Type)
        szFooter = String.Format("-----END {0}-----", sz_Type)
        iStart = szPEM.IndexOf(szHeader) + szHeader.Length
        iEnd = szPEM.IndexOf(szFooter, iStart)
        szBase64 = szPEM.Substring(iStart, (iEnd - iStart))

        Return Convert.FromBase64String(szBase64)

    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	DecodeRSAPrivateKey
    ' Function Type:	RSACryptoServiceProvider
    ' Parameter:		bt_PrivKey
    ' Description:		Checksum for Direct FPX 
    ' History:			5 May 2015, added by OoiMei. FPXD
    '********************************************************************************************
    Public Shared Function DecodeRSAPrivateKey(ByVal bt_PrivKey As Byte()) As RSACryptoServiceProvider
        Dim MODULUS As Byte() = Nothing
        Dim E As Byte() = Nothing
        Dim D As Byte() = Nothing
        Dim P As Byte() = Nothing
        Dim Q As Byte() = Nothing
        Dim DP As Byte() = Nothing
        Dim DQ As Byte() = Nothing
        Dim IQ As Byte() = Nothing

        ' --------- Set up stream to decode the asn.1 encoded RSA private key ------
        Dim mem As New MemoryStream(bt_PrivKey)
        Dim binr As New BinaryReader(mem)
        'wrap Memory Stream with BinaryReader for easy reading
        Dim bt As Byte = 0
        Dim twobytes As UShort = 0
        Dim elems As Integer = 0

        Try
            twobytes = binr.ReadUInt16()
            If twobytes = &H8130 Then
                'data read as little endian order (actual data order for Sequence is 30 81)
                binr.ReadByte()
                'advance 1 byte
            ElseIf twobytes = &H8230 Then
                binr.ReadInt16()
            Else
                'advance 2 bytes
                Return Nothing
            End If

            twobytes = binr.ReadUInt16()
            If twobytes <> &H102 Then
                'version number
                Return Nothing
            End If
            bt = binr.ReadByte()
            If bt <> &H0 Then
                Return Nothing
            End If


            '------ all private key components are Integer sequences ----
            elems = iGetIntegerSize(binr)
            MODULUS = binr.ReadBytes(elems)

            elems = iGetIntegerSize(binr)
            E = binr.ReadBytes(elems)

            elems = iGetIntegerSize(binr)
            D = binr.ReadBytes(elems)

            elems = iGetIntegerSize(binr)
            P = binr.ReadBytes(elems)

            elems = iGetIntegerSize(binr)
            Q = binr.ReadBytes(elems)

            elems = iGetIntegerSize(binr)
            DP = binr.ReadBytes(elems)

            elems = iGetIntegerSize(binr)
            DQ = binr.ReadBytes(elems)

            elems = iGetIntegerSize(binr)
            IQ = binr.ReadBytes(elems)

            'Console.WriteLine("showing components ..")
            'If verbose Then
            '    showBytes(vbLf & "Modulus", MODULUS)
            '    showBytes(vbLf & "Exponent", E)
            '    showBytes(vbLf & "D", D)
            '    showBytes(vbLf & "P", P)
            '    showBytes(vbLf & "Q", Q)
            '    showBytes(vbLf & "DP", DP)
            '    showBytes(vbLf & "DQ", DQ)
            '    showBytes(vbLf & "IQ", IQ)
            'End If

            ' ------- create RSACryptoServiceProvider instance and initialize with public key -----
            Dim CspParameters As New CspParameters()
            CspParameters.Flags = CspProviderFlags.UseMachineKeyStore

            Dim RSA As New RSACryptoServiceProvider(1024, CspParameters)
            Dim RSAparams As New RSAParameters()

            RSAparams.Modulus = MODULUS
            RSAparams.Exponent = E
            RSAparams.D = D
            RSAparams.P = P
            RSAparams.Q = Q
            RSAparams.DP = DP
            RSAparams.DQ = DQ
            RSAparams.InverseQ = IQ
            RSA.ImportParameters(RSAparams)

            Return RSA

        Catch ex As Exception
            Throw ex
            Return Nothing
        Finally
            binr.Close()
        End Try

    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	iGetIntegerSize
    ' Function Type:	Integer
    ' Parameter:		br_binr
    ' Description:		Checksum for Direct FPX 
    ' History:			5 May 2015, added by OoiMei. FPXD
    '********************************************************************************************
    Private Shared Function iGetIntegerSize(ByVal br_binr As BinaryReader) As Integer
        Dim bt As Byte = 0
        Dim lowbyte As Byte = &H0
        Dim highbyte As Byte = &H0
        Dim count As Integer = 0

        bt = br_binr.ReadByte()
        If bt <> &H2 Then
            'expect integer
            Return 0
        End If
        bt = br_binr.ReadByte()

        If bt = &H81 Then
            count = br_binr.ReadByte()
            ' data size in next byte
        ElseIf bt = &H82 Then
            highbyte = br_binr.ReadByte()
            ' data size in next 2 bytes
            lowbyte = br_binr.ReadByte()
            Dim modint As Byte() = {lowbyte, highbyte, &H0, &H0}
            count = BitConverter.ToInt32(modint, 0)
        Else
            ' we already have the data size
            count = bt
        End If

        While br_binr.ReadByte() = &H0
            'remove high order zeros in data
            count -= 1
        End While

        br_binr.BaseStream.Seek(-1, SeekOrigin.Current)
        'last ReadByte wasn't a removed zero, so back up a byte
        Return count

    End Function

    '********************************************************************************************
    ' Class Name:		Hash
    ' Function Name:	szVerifyFPXCheckSum
    ' Function Type:	String. Hashing value.
    ' Parameter:		sz_CheckSum and sz_CertKey. String values to be verify with key.
    ' Description:		Verify checksum from Direct FPX
    ' History:			8 May 2015, added by OoiMei. FPXD
    '********************************************************************************************
    Public Function szVerifyFPXCheckSum(ByVal sz_RespMsg As String, ByVal sz_CheckSum As String, ByVal sz_CertKey As String) As String
        Dim szCert As String = ""
        Dim szRetMsg As String = ""
        Dim btData As Byte() = Nothing
        Dim btSignData As Byte() = Nothing
        Dim rsaEncryptor As RSACryptoServiceProvider = Nothing

        Try
            szCert = sz_CertKey

            Dim x509_2 As New X509Certificates.X509Certificate2(szCert)

            btData = Encoding.Default.GetBytes(sz_RespMsg)
            btSignData = btHexToByte(sz_CheckSum)
            rsaEncryptor = DirectCast(x509_2.PublicKey.Key, RSACryptoServiceProvider)

            If rsaEncryptor.VerifyData(btData, "SHA1", btSignData) Then
                szRetMsg = "FPX Checksum verified successfully."
            Else
                szRetMsg = "FPX CheckSum verified failed."
            End If

        Catch ex As Exception
            'szRetMsg = "ERROR :" + e.Message
            Throw ex
        End Try

        Return szRetMsg

    End Function

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim szCheckSum As String = ""

        szCheckSum = "||||||||01|AR|CETAK PERAKUAN PENDAFTARAN KEN|01|EX00002554|JPJUATOT201703020001|SE00003008|UATOT201703020001|20170302112416|20.00|MYR|5.0"
        szComputeFPXCheckSum(szCheckSum, "D:\Projects\Malaysia\test\EX00003948.key")
        MsgBox("no error")
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim szRespMsg As String = ""
        Dim szCheckSum As String = ""
        Dim szResult As String = ""

        'szRespMsg = "TESTBANK A|TEST0001|||Meps User|00|9999999999|00|54742|1505131547320212|20150513154732||01|AC|EX00003948|OM0000IPGSITOM150513005|SE00004415|IPGSITOM150513005|20150513154730|1.00|MYR"
        'szRespMsg = "MAYBANK2U|MB2U0227|||MUHAMMAD HAIDIR BIN AZMAN|00|9999999999|00|3998863408|1702071828060313|20170207182748||01|AC|EX00006061|TGS000000TRNX0000034828|SE00025635|TGS000000TRNX0000034828|20170207182747|29.00|MYR"
        'szCheckSum = "71E3203327A3E0FAD7EE4A710F95DAB8EBEAF7C27960A5BAEC35787AF25B4FB673494762B5E4895CBDA8F07D1E48E1677524930BA3352A187AE0EBD05A1FE5F5CAD278B8A7E6A19B13B163082AB39E49804A1D474CCE628E3252C25152112A1A1273DA48CA68122885645AF651A813E617BD046BB695E0519B4D150F5048A03E95EB5C3438825B0E2393BBF16E0B6DB91768485A3DB0C1C2C56F1D68C2CB072D74A02AFDA650C50B3E85C93CB57873C28CD54634873FDBE0628BBD2C25ACCA134722A5EDEFF0A77BC5C9AB435D2A5C87D97AB2973FB03DEC96DD77482F2C3CB69B6BB08A3715AAF394678FAB2FEA189716374137841EDCBE7BCAEE46238686E8"
        szRespMsg = "MAYBANK2U|MB2U0227|||NORLELA BINTI KAMARUDIN@|00|9999999999|00|4001092886|1702081141370354|20170208114120||01|AC|EX00006061|SPT00000011532170200429|SE00025635|SPT00000011532170200429|20170208114119|20.00|MYR"
        szCheckSum = "76722231172E7868F9720123598CEE1B502F9B22A154F0EBF2133E63DB9DFAF755D0B35F62F01DC2BC8CA6A053D8BBF2F89C78E4654A9F3006ED47A25ACADE1BBF4C5EC283C2898EFEAA5B87FD79E5852B9C6975B310D9A8A961B00C0885ADEF4C701729CB16C2F7C6F66E023E6992BE1380AC4F9205C984045D555ED65C2D2552DEDEC964B4452CCB5C7939000795BBEF8179B4D13287817F9B2677EEA6B342315C6A453200583AD6DDFA4CCB570FAE8802D9A3AD4582AF369022840240A14780CFC36F6FA4DB5F496B57F188C75B28AF72A2280451DB93A0CA44CB4DE4C7FCCEABD394D96235D0A25FDE21F42E83115EF4ABD6ADADA66751ECC461D65BBC5E"
        'szRespMsg = "MBB0227%7EA%2CMBB0228%7EA%2CABMB0212%7EA%2CBIMB0340%7EA%2CBMMB0341%7EA%2CABB0233%7EA%2COCBC0229%7EA%2CHLB0224%7EA%2CBCBB0235%7EA%2CPBB0233%7EA%2CBKRM0602%7EA%2CMB2U0227%7EA%2CKFH0346%7EA%2CRHB0218%7EA%2CSCB0216%7EA%2CTEST0023%7EA%2CTEST0004%7EA%2CTEST0022%7EA%2CTEST0003%7EA%2CTEST0021%7EA%2CTEST0002%7EA%2CTEST0001%7EA%2CUOB0226%7EA%2CBSN0601%7EA%2CAMBB0209%7EA|01|BC|EX00003948"
        'szCheckSum = "2F143A7E880969B829867ADB5CC0DB3D5AB9F320F1EC91F078C489F86B1ED63E351288EEE7478AD93033531D9C27BFFD4C605D0DC76049C9439C82566D4C591BD7E75B4ED4DE24350CCC3FE8A1927956C476FBFBD5BF00B33ECFBF14C65D6B77BFAE3D0E68DE67519A458CB30FAB5CD976813F25518806D2E39AD5B1513720877E79F4966C0466D6B07136A946FD6234295029165A6103D28A0902C4B21E8E10F5C9FA44A0F32CB79DC3B1285D99C02CF132B13788578525B086BBDF87C164B089AAE320D4077F28C820C272A9C5F322A7E8AB5344EB1817352B422C1050C824146EC1B5F38D3F4C3D619EB1B3CE982EC65B5521749ED228B4E943E5E0FF778F"
        'D:\Projects\Doc\DirectDebit\FPX\eGHL_FPX_cert\13Dec2016\FPX CERTIFICATE RENEWAL
        szResult = szVerifyFPXCheckSum(szRespMsg, szCheckSum, "D:\Projects\Doc\DirectDebit\FPX\eGHL_FPX_cert\13Dec2016\FPX CERTIFICATE RENEWAL\fpxprod.cer") '"D:\Projects\Malaysia\test\bin\FPXCERT_MERC_UAT_2015.cer")
        MsgBox(szResult)

    End Sub

    Public Sub HttpPostWithRecv()
        Dim iRet As Integer = -1
        Dim objHttpWebRequest As HttpWebRequest
        Dim objHTTPWebResponse As HttpWebResponse
        Dim objStreamWriter As StreamWriter
        Dim objStreamReader As StreamReader
        Dim sbInStream As System.Text.StringBuilder
        Dim iStartTickCount As Integer
        Dim iTimeout As Integer
        Dim iResCode As Integer

        'Dim sz_HTTPString As String = "MERCHANT_ACC_NO=20108510171575300170&MERCHANT_TRANID=K5Z00020170222171055217&AMOUNT=10250.00&TRANSACTION_TYPE=1&TXN_SIGNATURE=c8dd6cbde98a9303d319311b341ee41123e8d85e521706a9752e8a038b2e18204276225445f165c824034bd3e0d12a893082619ed9e79ef912298589ed57db90&RESPONSE_TYPE=XML"
        'Dim sz_URL As String = "https://cards.maybank.com/BPG/admin/payment/PaymentInterface.jsp" 'https://www.mepsfpx.com.my/FPXMain/sellerNVPTxnStatus.jsp
        'Dim sz_HTTPString As String = "fpx_msgType=AE&fpx_msgToken=01&fpx_sellerExId=EX00006061&fpx_sellerExOrderNo=DIS00000000000240063863&fpx_sellerTxnTime=20170207131317&fpx_sellerOrderNo=DIS00000000000240063863&fpx_sellerId=SE00025635&fpx_sellerBankCode=01&fpx_txnCurrency=MYR&fpx_txnAmount=150.10&fpx_checkSum=1867031C53FF3F152CDDD58919D2C353A604C181BDE30A6859CE1756B5132AE8DED2D646BF3BB91A2136E0F39A9B0A456D7F85FFC9445FA3078BA5229A51F4FA991E13CB493B3890DD18A3503AFA9AB53B5C1FF0AE1758981F9E5BCF2F151705236448ED9326E75A9670056650E2DBAF98EB303DFD442C3142A8A8AFA20BD755879E0CBEBFC2E926CA88D22A1E1AEE15AA2F1C00296D9545C836F812D0F4DAB99446D0B09F73E46FB8B0240EFD847C11626B3C3958035AC05C74BC22FA1222026EA333A4BFAEBCE8193DE9192695BF12ED8DF398D97ACC1EB487FE8A27748ED3EBC6F3614A646E603C58C7D170313A0BE6D43E454A5C10B78FF4F839E5B6B739&fpx_productDesc=240063863&fpx_version=5.0"
        'Dim sz_URL As String = "https://www.mepsfpx.com.my/FPXMain/sellerNVPTxnStatus.jsp" 'https://www.mepsfpx.com.my/FPXMain/sellerNVPTxnStatus.jsp
        Dim sz_HTTPString As String = "fpx_msgType=BE&fpx_msgToken=01&fpx_sellerExId=EX00003948&fpx_version=5.0&fpx_checkSum=27FD01617A4FF2F36EA3635EA9769921FA1FFCE987A576F086D39A3CA6E73CBFF318C2207EB17D445A37A1079A87C2F8480ACED293A0674F0E83A473918F61EB572DFD6177CF5F731FD8068A63EA90357874E67446F5BD35CA3C35AB8F657D27588B94D9C4E4180EDB876E22F50242DB45EF2BD80AB5CCF5158EA911487086D8497982654B860D8FF67CF1B4A8360E27CBB565D79155527C3075058D4978CF5DA324C99575402AC98DB9E61212168D8C751E1D23CC4F4C0156CB4583EE91CBEC5D9E7291685E2CE84FF31427BD2082CADED340B17042DF011650A7B0A7886F8341E2C78F15EB1FAD52F4B3A7689D133A55778D521B9CB253137153F96CEA69EE"
        Dim sz_URL As String = "https://uat.mepsfpx.com.my/FPXMain/RetrieveBankList"
        'Dim sz_HTTPString As String = "merID=3301229212&invoiceNo=POP00000000001800085&amount=000000003100"
        'Dim sz_URL As String = "https://ecom.pbebank.com/PGW/Pay/Check" '"https://uat.mepsfpx.com.my/FPXMain/RetrieveBankList" '"https://ecom.pbebank.com/PGW/Pay/Detail" ' "https://uattds2.pbebank.com/PGW/Pay/Check" 
        'Dim sz_HTTPString As String = "TransactionType=QUERY&PymtMethod=CC&ServiceID=A07&PaymentID= ABCDEFGH130820142128&Amount=228.00&CurrencyCode=MYR&HashValue=hash value generated"
        'Dim sz_URL As String = "https://test2pay.ghl.com/IPGSG/Payment.aspx"
        'Dim sz_HttpString As String = "<INPUT type='hidden' name='fpx_msgType' value='AR'><INPUT type='hidden' name='fpx_msgToken' value='01'><INPUT type='hidden' name='fpx_sellerExId' value='EX00003948'><INPUT type='hidden' name='fpx_sellerExOrderNo' value='ghl000IPGOMSIT160808005'><INPUT type='hidden' name='fpx_sellerTxnTime' value='20160808112943'><INPUT type='hidden' name='fpx_sellerOrderNo' value='ghl000IPGOMSIT160808005'><INPUT type='hidden' name='fpx_sellerId' value='SE00004415'><INPUT type='hidden' name='fpx_sellerBankCode' value='01'><INPUT type='hidden' name='fpx_txnCurrency' value='MYR'><INPUT type='hidden' name='fpx_txnAmount' value='10.00'><INPUT type='hidden' name='fpx_checkSum' value='AE830A244897317F2D5D86C2608E4A570E52E32E79EA1240F9E6A4BD6593AE8403A79C84CB07452EFE414929C7719632B3C3B691D35B9950695FD633B65C273FEBFB97070478CB9C98FB1C4B7133D053B0E1079A085806D33835797D9E932F0793A87B32EB1F66B9326F3366FC266A02F9DF229F94DB8843DF17092AEF1198F1576B62834A2648EA1C06DFFFC96F9606A2EEBFE91CA8409BB6214471FCDE2F9890F2370706C6786491C28853E5064787659845F3F3B07C88EEA53FCC66E0D769277BA6BD33E265343D158D24B4CE0A2BE50CD3B4C3B6154B3C3C46C038FBF287F59A0F1FFC909E5C04669402657CD2F60F8BF484A5043C3097D93481687BCC09'><INPUT type='hidden' name='fpx_productDesc' value='IPGOMSIT160808005'><INPUT type='hidden' name='fpx_version' value='5.0'>"
        'Dim sz_URL As String = "https://uat.mepsfpx.com.my/FPXMain/sellerNVPReceiver.jsp"
        Dim i_Timeout As Integer = 3000
        Dim sz_SecurityProtocol As String = "TLS"
        Dim sz_HTTPResponse As String = ""
        'tran_type=I&site_name=https://securepay.e-ghl.com/IPG/Payment.aspx&term_id=KT21071&term_seq=000800&tran_amt=152.84&ref1=A0120160203014000800&servProviderId=21071 to https://www.ktbnetbank.com/Page2PagePayment/NewIBEcom.Login
        Try
            '======================================== Sets HttpWebRequest properties ====================================
            sbInStream = New System.Text.StringBuilder
            sbInStream.Append(sz_HTTPString)
            iTimeout = i_Timeout * 1000 'converts input timeout param from second to milliseconds

            objHttpWebRequest = WebRequest.Create(sz_URL)                       'Posting URL
            objHttpWebRequest.ContentType = "application/x-www-form-urlencoded" 'default value
            objHttpWebRequest.Method = "POST"                                   'default value
            objHttpWebRequest.ContentLength = sbInStream.Length ' Must be set b4 retrieving stream for sending data
            objHttpWebRequest.KeepAlive = False
            objHttpWebRequest.Timeout = iTimeout                                '(in milliseconds)

            'Ignore invalid server certificates or else may encounter WebException with e.Status = WebExceptionStatus.TrustFailure
            'When the CertificatePolicy property is set to an ICertificatePolicy interface instance, the ServicePointManager
            'uses the certificate policy defined in that instance instead of the default certificate policy.
            'The default certificate policy allows valid certificates, as well as valid certificates that have expired.
            ServicePointManager.CertificatePolicy = New SecPolicy

            'Added by Jeff, 19 Jun 2015
            If (sz_SecurityProtocol.Trim() <> "") Then
                Select Case sz_SecurityProtocol.Trim().ToUpper()
                    Case "SSL3"
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3
                        'Case "TLS12"
                        '    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
                        'Case "TLS11"
                        '    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11
                    Case "TLS"
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                    Case Else
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3
                End Select

                ' allows for validation of SSL conversations
                ServicePointManager.ServerCertificateValidationCallback = Nothing 'Function() True   'C#: delegate { return true }
            End If

            iStartTickCount = System.Environment.TickCount()

            '=========================================== Post request ==================================================
            '- StreamWriter constructor creates a StreamWriter with UTF-8 encoding whose GetPreamble method returns an
            'empty byte array. The BaseStream property is initialized using the stream parameter (i.e. objHttpWebRequest.GetRequestStream)
            '- GetRequestStream method returns a stream to use to send data for the HttpWebRequest. Once the Stream
            'instance has been returned, you can send data with the HttpWebRequest by using the Stream.Write method.
            'NOTE:    Must set the value of the ContentLength property before retrieving the stream.
            'CAUTION: Must call the Stream.Close method to close the stream and release the connection for reuse.
            '         Failure to close the stream will cause application to run out of connections.

            objStreamWriter = New StreamWriter(objHttpWebRequest.GetRequestStream())
            objStreamWriter.Write(sbInStream.ToString)  ' Sends data
            objStreamWriter.Close() ' Closes the stream and release the connection for reuse to avoid running out of connections

            '============================================= Get response =================================================
            '- GetResponse method returns a WebResponse instance containing the response from the Internet resource.
            'The actual instance returned is an instance of HttpWebResponse, and can be typecast to that class to access
            'HTTP-specific properties. When POST method is used, must get the request stream, write the data to be posted,
            'and close the stream. This method blocks waiting for content to post; If there is no time-out set and
            'content is not provided, application will block indefinitely.

            objHTTPWebResponse = objHttpWebRequest.GetResponse()

            If (objHttpWebRequest.HaveResponse And objHTTPWebResponse.StatusCode = HttpStatusCode.OK) Then

                objStreamReader = New StreamReader(objHTTPWebResponse.GetResponseStream)

                '================ Implements Timeout for getting HTTP reply from host <START> ====================
                Dim objRespReader As RespReader
                Dim oThreadStart As ThreadStart
                Dim oThread As Thread
                Dim iElapsedTime As Integer

                objRespReader = New RespReader
                'objRespReader.SetLogger(objLoggerII)
                objRespReader.SetReader(objStreamReader)

                oThreadStart = New ThreadStart(AddressOf objRespReader.WaitForResp)
                oThread = New Thread(oThreadStart)
                oThread.Start()

                'iStartTickCount = System.Environment.TickCount()
                iElapsedTime = System.Environment.TickCount() - iStartTickCount

                While ((iElapsedTime < iTimeout) And Not objRespReader.IsCompleted())
                    System.Threading.Thread.Sleep(50)
                    iElapsedTime = System.Environment.TickCount() - iStartTickCount
                End While
                'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp Elapsed Time(ms): " & iElapsedTime.ToString())

                If (iElapsedTime >= iTimeout) Then
                    ' Timeout
                    iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                    'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp Timeout, iRet(" + iRet.ToString() + ")")
                Else
                    'Completed AND No Timeout. Could be got reply or exception
                    If (True = objRespReader.IsSucceeded()) Then
                        'Received reply within timeout period
                        sz_HTTPResponse = objRespReader.GetRespStream()

                        If ("" = sz_HTTPResponse) Then  'Added by OoiMei 4 jun 2014. Firefly FF
                            iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                            'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp Response received with empty string. Set to Timeout.")
                        End If
                    Else
                        'Exception within timeout period
                        iRet = Common.ERROR_CODE.E_COM_TIMEOUT
                        'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "WaitForResp exception within timeout period, iRet(" + iRet.ToString() + ")")
                    End If
                End If

                oThread.Abort()
                oThreadStart = Nothing
                oThread = Nothing
                'objRespReader.Dispose()
                objRespReader = Nothing

                'Releases the resources of the response stream
                'NOTE: Failure to close the stream will cause application to run out of connections.
                objStreamReader.Close()
                If Not objStreamReader Is Nothing Then objStreamReader = Nothing 'Added on 1 Mar 2009
                '================== Implements Timeout for getting HTTP reply from host <END> =====================
            Else
                'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "ERROR: Response was not received.")
            End If

            'Releases the resources of the response
            objHTTPWebResponse.Close()
            Dim s As String = sz_HTTPResponse.Trim()
            MsgBox(sz_HTTPResponse.Trim())

            'If (-1 = iRet) Then 'Modified by OoiMei, 4 Jun 2014, added checking of (-1=iRet), Firefly FF
            '    iRet = 0
            'End If

        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            'Commented by Jeff, 15 Aug 2013
            'objLoggerII.Log(LoggerII.CLoggerII.LOG_SEVERITY.INFORMATION, LoggerII.DebugInfo.__FILE__(), LoggerII.DebugInfo.__METHOD__(), LoggerII.DebugInfo.__LINE__(), "iRet: " + CStr(iRet))
        End Try

    End Sub

    Public Sub HttpPostWithRecv1()
        Dim request As WebRequest = WebRequest.Create("https://test2pay.ghl.com/IPGSG/Payment.aspx")
        ' Set the Method property of the request to POST. 
        request.Method = "POST"
        ' Create POST data and convert it to a byte array.
        Dim postData As String = "TransactionType=QUERY&PymtMethod=CC&ServiceID=A07&PaymentID= ABCDEFGH130820142128&Amount=228.00&CurrencyCode=MYR&HashValue=hash value generated"
        Dim byteArray As Byte() = Encoding.UTF8.GetBytes(postData)
        ' Set the ContentType property of the WebRequest. 
        request.ContentType = "application/x-www-form-urlencoded"
        ' Set the ContentLength property of the WebRequest. 
        request.ContentLength = byteArray.Length ' Get the request stream. 

        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
        'ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 (VS2008 not support)

        Dim dataStream As Stream = request.GetRequestStream() ' Write the data to the request stream. 
        dataStream.Write(byteArray, 0, byteArray.Length) ' Close the Stream object. 
        dataStream.Close() ' Get the response. 
        Dim response As WebResponse = request.GetResponse() ' Display the status. 
        Console.WriteLine(CType(response, HttpWebResponse).StatusDescription) ' Get the stream containing content returned by the server. 
        dataStream = response.GetResponseStream() ' Open the stream using a StreamReader for easy access. 
        Dim reader As New StreamReader(dataStream) ' Read the content. 
        Dim responseFromServer As String = reader.ReadToEnd() ' Display the content. 
        Console.WriteLine(responseFromServer) ' +++++ Parse Query Response Here +++++++++++++++++++++++++++++++++++++ 
        ' +++++ Sample of Query Response Is Available On The Next Section +++++ 
        ' Clean up the streams. 
        reader.Close()
        dataStream.Close()
        response.Close()

    End Sub

    Public Sub RemoveSpecChar()
        'Dim illegalChars As Char() = "!@#$%^&*(){}[]""_+<>?/:".ToCharArray()
        'Dim str As String = "wc#abc: 123"
        'Dim sb As New System.Text.StringBuilder

        'For Each ch As Char In str
        '    If Array.IndexOf(illegalChars, ch) = -1 Then
        '        sb.Append(ch)
        '    End If
        'Next
        'MessageBox.Show(sb.ToString().Trim()) 'remove space
        ''MessageBox.Show(sb.ToString().Trim().Replace(" ", "")) 'remove space

        'TPA,MBB,MB2U
        Dim szBankList As String = "MBB0227~A,MBB0228~A,ABMB0212~A,BIMB0340~A,BMMB0341~A,ABB0233~A,OCBC0229~A,HLB0224~A,BCBB0235~A,PBB0233~A,BKRM0602~A,MB2U0227~A,KFH0346~A,RHB0218~A,SCB0216~A,TEST0023~A,TEST0004~A,TEST0022~A,TEST0003~A,TEST0021~A,TEST0002~A,TEST0001~A,UOB0226~A,BSN0601~A,AMBB0209~A"
        Dim m_szExclude As String = "TPA,MBB,MB2U"
        Dim aszBankListSets() As String = Nothing
        Dim aszExcludeSets() As String = Nothing
        Dim j, i As Integer
        Dim sztest As String = ""
        aszBankListSets = Split(szBankList, ",")
        aszExcludeSets = Split(m_szExclude, ",")
        Dim pattern As String = "\b(TPA|MBB0227|MB2U|TEST)"

        'sztest = Regex.Replace(szBankList, pattern, String.Empty)
        For Each bank As String In aszBankListSets
            If (Not Regex.Match(bank, pattern, RegexOptions.IgnoreCase).Success) Then

                'End If
                'If ("" <> (Regex.Replace(bank, pattern, String.Empty))) Then
                sztest = sztest + bank
            End If
        Next

        'For j = 0 To aszBankListSets.GetUpperBound(0)
        '    For i = 0 To aszExcludeSets.GetUpperBound(0) 'aszBankListSets(j), aszExcludeSets(i).Length()), aszExcludeSets(i))
        '        If (False = String.Equals(aszBankListSets(j).Substring(0, aszExcludeSets(i).Length()), aszExcludeSets(i))) Then
        '            sztest = sztest + aszBankListSets(j)
        '        End If
        '    Next
        'Next
        MessageBox.Show(sztest)

    End Sub

    Public Sub ReadXML()
        Dim sz_Tag2Search As String = ""
        Dim szresult As String = ""
        'mbb
        'Dim xmlString As String = "<?xml version=""1.0"" ?><TRANSACTION_RESPONSE><TRANSACTION><TRANSACTION_ID>184711</TRANSACTION_ID>" & _
        '            "<MERCHANT_ACC_NO>02700770157675001239</MERCHANT_ACC_NO><TXN_STATUS>V</TXN_STATUS>" & _
        '            "<TXN_SIGNATURE>038fd349a2070f78ab6c73878d551b600903225d5fefbbab9984bcda5b069c98299f2327d2cb41084dd0bb587e1c828275df33201b3e9b5efaadc482b471663f</TXN_SIGNATURE>" & _
        '            "<TXN_SIGNATURE2>09D0B9ADF80D56137D9F30F827645A0B718D20845499F7E3F38674F66E2D13A6B55EE917BD3D2AABE46BCEEC2FD882C1C7C3E8679AC3EE4ABF0EBA9A84E1EA37</TXN_SIGNATURE2>" & _
        '            "<TRAN_DATE>11-04-2016 17:51:50</TRAN_DATE><MERCHANT_TRANID>MMH000IPGSITOM160411010</MERCHANT_TRANID>" & _
        '            "<RESPONSE_CODE>0</RESPONSE_CODE><RESPONSE_DESC>APPROVED OR COMPLETED</RESPONSE_DESC><AUTH_ID>657300</AUTH_ID>" & _
        '            "<AUTH_DATE>11-04-2016 17:51:50</AUTH_DATE><CAPTURE_DATE>11-04-2016 17:51:50</CAPTURE_DATE>" & _
        '            "<SALES_DATE></SALES_DATE><VOID_REV_DATE></VOID_REV_DATE><REFUND_DATE>11-04-2016 17:54:06</REFUND_DATE>" & _
        '            "<REFUND_AMOUNT>3.00</REFUND_AMOUNT></TRANSACTION></TRANSACTION_RESPONSE>"
        'poli
        'Dim xmlString As String = "<?xml version=""1.0"" encoding=""utf-8""?><GetTransactionResponse xmlns=""http://schemas.datacontract.org/2004/07/Centricom.POLi.Services.MerchantAPI.Contracts"" xmlns:i=""http://www.w3.org/2001/XMLSchema-instance""><Errors xmlns:a=""http://schemas.datacontract.org/2004/07/Centricom.POLi.Services.MerchantAPI.DCO"" /><TransactionStatusCode>Completed</TransactionStatusCode><Transaction xmlns:a=""http://schemas.datacontract.org/2004/07/Centricom.POLi.Services.MerchantAPI.DCO""><a:AmountPaid>3.46</a:AmountPaid><a:BankReceipt>C4226974126</a:BankReceipt><a:BankReceiptDateTime>02/03/16</a:BankReceiptDateTime><a:CountryCode>AU</a:CountryCode><a:CountryName>Australia</a:CountryName><a:CurrencyCode>AUD</a:CurrencyCode><a:CurrencyName>Australian Dollar</a:CurrencyName><a:EndDateTime>2016-03-02T01:13:58.34</a:EndDateTime><a:ErrorCode i:nil=""true"" /><a:ErrorMessage i:nil=""true"" /><a:EstablishedDateTime>2016-03-02T01:12:09.823</a:EstablishedDateTime><a:FinancialInstitutionCode>NAB</a:FinancialInstitutionCode><a:FinancialInstitutionCountryCode>AU</a:FinancialInstitutionCountryCode><a:FinancialInstitutionName>NAB</a:FinancialInstitutionName><a:MerchantAcctName>POLi Holdings</a:MerchantAcctName><a:MerchantAcctNumber>835451256</a:MerchantAcctNumber><a:MerchantAcctSortCode>013440</a:MerchantAcctSortCode><a:MerchantAcctSuffix /><a:MerchantDefinedData>Phone</a:MerchantDefinedData><a:MerchantEstablishedDateTime>2016-03-01T22:11:57</a:MerchantEstablishedDateTime><a:MerchantReference>GHL000RAJSIT20160301004</a:MerchantReference><a:PaymentAmount>3.46</a:PaymentAmount><a:StartDateTime>2016-03-02T01:12:09.823</a:StartDateTime><a:TransactionID>00efa3a3-eb13-49e6-a50c-91af7634b9b8</a:TransactionID><a:TransactionRefNo>996124327777</a:TransactionRefNo></Transaction></GetTransactionResponse>"
        'alb
        Dim xmlString As String = "<?xml version=""1.0"" encoding=""UTF-8""?><PxInquiryRes>" & _
 "<PX_VERSION>1.1</PX_VERSION>" & _
 "<PX_TRANSACTION_TYPE>null</PX_TRANSACTION_TYPE>" & _
 "<PX_PURCHASE_ID>null</PX_PURCHASE_ID>" & _
 "<PX_PAN>null</PX_PAN>" & _
 "<PX_PURCHASE_AMOUNT>null</PX_PURCHASE_AMOUNT>" & _
 "<PX_TRANSACTION_DATE></PX_TRANSACTION_DATE>" & _
 "<PX_HOST_DATE></PX_HOST_DATE>" & _
 "<PX_ERROR_CODE>301</PX_ERROR_CODE>" & _
 "<PX_ERROR_DESCRIPTION>Invalid Transaction ID</PX_ERROR_DESCRIPTION>" & _
 "<PX_APPROVAL_CODE>null</PX_APPROVAL_CODE>" & _
 "<PX_RRN>null</PX_RRN>" & _
 "<PX_3D_FLAG>null</PX_3D_FLAG>" & _
 "<merchantData>" & _
 "	<PX_CUSTOM_FIELD1>null</PX_CUSTOM_FIELD1>" & _
 "	<PX_CUSTOM_FIELD2>null</PX_CUSTOM_FIELD2>" & _
 "	<PX_CUSTOM_FIELD3>null</PX_CUSTOM_FIELD3>" & _
 "	<PX_CUSTOM_FIELD4>null</PX_CUSTOM_FIELD4>" & _
 "	<PX_CUSTOM_FIELD5>null</PX_CUSTOM_FIELD5>" & _
 "</merchantData>" & _
"</PxInquiryRes>"

        sz_Tag2Search = "PX_error_DESCRIPTION" '"TransactionStatusCode" '"a:MerchantReference" '"Txn_Signature2" '"REFUND_AMOUNT" '"RESPONSE_code" '"Txn_Status" '"TXN_STATUS" '"txn_status"
        ' Create an XmlReader
        Using reader As XmlReader = XmlReader.Create(New StringReader(xmlString))
            While reader.Read()
                If sz_Tag2Search.ToLower() = reader.Name.ToLower() Then
                    szresult = reader.ReadElementContentAsString()
                End If
            End While
        End Using

        MessageBox.Show(szresult)
    End Sub
End Class
