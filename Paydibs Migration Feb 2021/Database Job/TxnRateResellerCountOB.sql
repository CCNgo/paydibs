USE [msdb]
GO

/****** Object:  Job [TxnRateResellerCountOB]    Script Date: 2/10/2021 4:36:36 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 2/10/2021 4:36:36 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'TxnRateResellerCountOB', 
		@enabled=1, 
		@notify_level_eventlog=2, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'paydibsm', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [TxnRateResellerCountOB]    Script Date: 2/10/2021 4:36:36 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'TxnRateResellerCountOB', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'DECLARE @pkid AS INT
DECLARE @cnt AS INT, @idx AS INT = 1
DECLARE @ResPKID AS INT, @DCreated AS DATETIME, @TType AS VARCHAR(7), @MID AS VARCHAR(30), @MTxnID AS VARCHAR(30), @OrdNum AS VARCHAR(20),
		@GTxnID AS VARCHAR(30), @BRefNum AS VARCHAR(30), @CCode AS CHAR(3), @HID AS INT, @Bank AS VARCHAR(20), @TAmt AS DECIMAL(18,2),
		@OriTAmt AS DECIMAL(18,2), @FXTAmt AS DECIMAL(18,2), @OriCCode AS CHAR(3), @FXCCode AS CHAR(3),
		@TStatus AS INT, @MTStatus AS INT, @TDay AS INT, @DCompleted AS DATETIME
DECLARE @FrAmt AS DECIMAL(18,2), @ToAmt AS DECIMAL(18,2), @MR AS DECIMAL(18,4), @MFF AS DECIMAL(18,4)
DECLARE @GTax AS DECIMAL(18,4), @GTaxAmt AS DECIMAL(18,4), @MRAmt AS DECIMAL(18,4), @MRMinusResAmt AS DECIMAL(18,4), @MFFAmt AS DECIMAL(18,4), @NetFee AS DECIMAL(18,4)/*@Round AS INT*/


IF 0=(SELECT COUNT(*) FROM TB_TxnRateReseller)
	SET @pkid = 0
ELSE
	SELECT TOP 1 @DCompleted=DateCompleted FROM TB_TxnRateReseller ORDER BY PKID DESC 	

SELECT IDENTITY(INT,1,1) AS ID, R.PKID, R.DateCreated, R.TxnType, R.MerchantID, R.MerchantTxnID, R.OrderNumber, R.GatewayTxnID, R.BankRefNo, R.HostID,
		R.IssuingBank, R.CurrencyCode, R.TxnAmt, FX.FXCurrencyCode, FX.FXTxnAmt, R.TxnStatus, R.MerchantTxnStatus, R.TxnDay, R.DateCompleted
INTO #tmptbl_Res
FROM TB_PayRes R WITH (NOLOCK) 
LEFT JOIN TB_PayFX FX ON FX.GatewayTxnID = R.GatewayTxnID
WHERE (R.PKID > @pkid OR R.DateCompleted > @DCompleted)

SELECT @cnt = COUNT(*) FROM #tmptbl_Res

WHILE @idx <= @cnt
BEGIN  
	
    SELECT @ResPKID=PKID, @DCreated=DateCreated, @TType=TxnType, @MID=MerchantID, @MTxnID=MerchantTxnID, @OrdNum=OrderNumber,
		@GTxnID=GatewayTxnID, @BRefNum=BankRefNo, @OriCCode=CurrencyCode, @HID=HostID, @Bank=IssuingBank, @OriTAmt=TxnAmt,
		@TStatus=TxnStatus, @MTStatus=MerchantTxnStatus, @TDay=TxnDay, @DCompleted=DateCompleted, @FXCCode=FXCurrencyCode, @FXTAmt=FXTxnAmt
	FROM #tmptbl_Res WHERE ID=@idx
	
	/* Check Reseller MerchantGrpID*/
	DECLARE @MerchantGrpID	VARCHAR(100)
	DECLARE @Reseller AS INT = 0

	SELECT @MerchantGrpID=MerchantGroup FROM PG..Merchant 
	WHERE MerchantID != MerchantGroup AND MerchantID = @MID 
	
	IF @@ROWCOUNT > 0 
		BEGIN
			IF NOT EXISTS ( SELECT 1 FROM TB_TxnRateReseller WHERE ResID = @ResPKID )
				BEGIN
					/*add field MerchantGrpID*/
					INSERT INTO TB_TxnRateReseller
					(		
						ResID, DateCreated, TxnType, MerchantID, MerchantTxnID, OrderNumber, GatewayTxnID, BankRefNo, CurrencyCode, HostID, IssuingBank,
						TxnAmt, TxnStatus, MerchantTxnStatus, TxnDay, DateCompleted, MerchantGrpID, FXTxnAmt, FXCurrencyCode
					)
					VALUES(@ResPKID, @DCreated, @TType, @MID, REPLACE(@MTxnID,'' '',''''), @OrdNum, @GTxnID, REPLACE(@BRefNum,'' '',''''), @OriCCode, @HID, REPLACE(@Bank,'' '',''''), 
						@OriTAmt, @TStatus, @MTStatus, @TDay, @DCompleted, @MerchantGrpID, @FXTAmt, @FXCCode)
				END

				SELECT TOP 1 @GTax=Rate FROM PG..GovtTax ORDER BY StartDate DESC

				IF @FXCCode IS NULL OR @FXCCode = ''''
					BEGIN
						SET @CCode = @OriCCode
						SET @TAmt = @OriTAmt
					END
				ELSE
					BEGIN
						SET @CCode = @FXCCode
						SET @TAmt = @FXTAmt
					END
				/* Reseller Calculation = PG Database */
				DECLARE @FrAmtRes AS DECIMAL(18,2), @ToAmtRes AS DECIMAL(18,2), @MRRes AS DECIMAL(18,4), @MFFRes AS DECIMAL(18,4)
				DECLARE @DataResseller AS INT, @DataMerchant AS INT

				/* 1. Get Reseller Rate */
				SELECT @FrAmtRes=FromAmt, @ToAmtRes=ToAmt, @MRRes=Rate, @MFFRes=FixedFee 
				FROM (SELECT FromAmt, ToAmt, MerchantID, PymtMethod, HostID, Rate, FixedFee, Currency, StartDate,
				RANK() OVER (PARTITION BY MerchantID, HostID, PymtMethod, Currency ORDER BY StartDate DESC) AS Rank
				FROM PG..MerchantResellerRate MResRate
				WHERE CONVERT(VARCHAR(8), StartDate, 112) <= @TDay 
					AND MerchantID=@MerchantGrpID 
					AND HostID=@HID 
					AND Currency=@CCode 
					AND (PymtMethod=''OB'' OR PymtMethod=''WA'')) M
				WHERE Rank=1

				IF 0 = @@ROWCOUNT
				-- DEL START KENT LOONG 20190313 [REMOVE]
				--IF 1 = @@ROWCOUNT
					-- SET @DataResseller = 1
				-- ELSE
					-- SET @DataResseller = 0
				-- DEL E N D KENT LOONG 20190313 [REMOVE]
				-- ADD START KENT LOONG 20190313 [ADD RESELLER DEF RATE]
				-- ADD BY KENT LOONG 20190408 [ADD RESELLER CHECK]
					SELECT @Reseller=Reseller 
					FROM pg..CL_Plugin
					WHERE MerchantID=@MerchantGrpID 
					
					IF 1 = @Reseller
						SELECT @FrAmtRes=FromAmt, @ToAmtRes=ToAmt, @MRRes=Rate, @MFFRes=FixedFee 
						FROM (SELECT FromAmt, ToAmt, MerchantID, PymtMethod, HostID, Rate, FixedFee, Currency, StartDate,
						RANK() OVER (PARTITION BY MerchantID, HostID, PymtMethod, Currency ORDER BY StartDate DESC) AS Rank
						FROM PG..MerchantResellerRate MResRate
						WHERE CONVERT(VARCHAR(8), StartDate, 112) <= @TDay 
							AND MerchantID=''DEF'' 
							AND HostID=@HID 
							AND Currency=@CCode 
							AND (PymtMethod=''OB'' OR PymtMethod=''WA'')) M
						WHERE Rank=1
					ELSE
						BEGIN
							SET @FrAmtRes = 0
							SET @ToAmtRes = 0
							SET @MRRes = 0
							SET @MFFRes = 0
						END
				-- ADD E N D KENT LOONG 20190313 [ADD RESELLER DEF RATE]

				/* Get Merchant Rate */
				SELECT @FrAmt=FromAmt, @ToAmt=ToAmt, @MR=Rate, @MFF=FixedFee 
				FROM (SELECT FromAmt, ToAmt, MerchantID, PymtMethod, HostID, Rate, FixedFee, Currency, StartDate,
				RANK() OVER (PARTITION BY MerchantID, HostID, PymtMethod, Currency ORDER BY StartDate DESC) AS Rank
				FROM PG..MerchantRate MR
				WHERE CONVERT(VARCHAR(8), StartDate, 112) <= @TDay 
					AND MerchantID=@MID 
					AND HostID=@HID 
					AND Currency=@CCode 
					AND (PymtMethod=''OB'' OR PymtMethod=''WA'')) M
				WHERE Rank=1

				IF 0 = @@ROWCOUNT
				-- DEL START KENT LOONG 20190313 [DELETE]
				--IF 1 = @@ROWCOUNT
					-- SET @DataMerchant = 1
				-- ELSE
					--SET @DataMerchant = 0
				-- DEL E N D KENT LOONG 20190313 [DELETE]
				-- ADD START KENT LOONG 20190313 [ADD MERCHANT DEF RATE]
					SELECT @FrAmt=FromAmt, @ToAmt=ToAmt, @MR=Rate, @MFF=FixedFee 
					FROM (SELECT FromAmt, ToAmt, MerchantID, PymtMethod, HostID, Rate, FixedFee, Currency, StartDate,
					RANK() OVER (PARTITION BY MerchantID, HostID, PymtMethod, Currency ORDER BY StartDate DESC) AS Rank
					FROM PG..MerchantRate MR
					WHERE CONVERT(VARCHAR(8), StartDate, 112) <= @TDay 
						AND MerchantID=''DEF'' 
						AND HostID=@HID 
						AND Currency=@CCode 
						AND (PymtMethod=''OB'' OR PymtMethod=''WA'')) M
					WHERE Rank=1
				-- ADD E N D KENT LOONG 20190313 [ADD MERCHANT DEF RATE]
				-- DEL START KENT LOONG 20190313 [DELETE]
				-- IF (@DataResseller = 1 AND @DataMerchant = 1)
				-- DEL START KENT LOONG 20190313 [DELETE]
				BEGIN
					/* sub-merchant - Reseller * Amount */
					SET @MRMinusResAmt = @MR - @MRRes
					SET @MRAmt = (@MR - @MRRes) * @TAmt 
					SET @MFFAmt= @MFF - @MFFRes
				END
				-- DEL START KEN LOONG 20190313 [DELETE]
				-- ELSE --if one of the reseller/merchant data no rate added
				-- ADD START KENT LOONG 20190301 [Add in get default Rate if one of the reseller/merchant data no rate added]
				-- SELECT @FrAmt=FromAmt, @ToAmt=ToAmt, @MR=Rate, @MFF=FixedFee 
				-- FROM (SELECT FromAmt, ToAmt, MerchantID, PymtMethod, HostID, Rate, FixedFee, Currency, StartDate,
				-- RANK() OVER (PARTITION BY MerchantID, HostID, PymtMethod, Currency ORDER BY StartDate DESC) AS Rank
				-- FROM PG..MerchantRate MR 
				-- WHERE CONVERT(VARCHAR(8), StartDate, 112) <= @TDay AND MerchantID=''DEF'' AND HostID=@HID AND Currency=@CCode AND (PymtMethod=''OB'' OR PymtMethod=''WA'')) M
				-- WHERE Rank=1
				
				-- BEGIN
					-- SET @MRAmt = @TAmt * @MR --@TAmt * @MR
					-- SET @MFFAmt= @MFF --@MFF
					-- SET @MRMinusResAmt =  @MR --@MR
				-- END
				-- ADD E N D KENT LOONG 20190301 [Add in get default Rate if one of the reseller/merchant data no rate added]
				-- DEL E N D KEN LOONG 20190313 [DELETE]
				/* Whichever higher */
				IF @MRAmt > @MFFAmt
				BEGIN
					SET @GTaxAmt = @MRAmt * @GTax
					SET @NetFee = @MRAmt
				END
				ELSE
				BEGIN
					SET @GTaxAmt = @MFFAmt * @GTax
					SET @NetFee = @MFFAmt
				END
				

				UPDATE TB_TxnRateReseller
				SET MerchantRate= ISNULL(@MRMinusResAmt,0), 
					MerchantFixedFee = ISNULL(@MFFAmt,0),
					MerchantRateAmt = ISNULL(@MRAmt,0),
					MerchantFFeeAmt = ISNULL(@MFFAmt,0),
					MerchantGovtTax = ISNULL(@GTax,0),
					MerchantGovtTaxAmt = ISNULL(@GTaxAmt,0), 
					NetFee = ISNULL(@NetFee,0)
				WHERE ResID = @ResPKID
	
				SET @MRMinusResAmt=0
				SET @MFF=0
				SET @MRAmt=0
				SET @MFFAmt=0
				SET @GTax=0
				SET @GTaxAmt=0
				SET @NetFee=0

		END /*END @@ROWCOUNT*/
	
	SET @idx = @idx + 1;

END  

DROP TABLE #tmptbl_Res
', 
		@database_name=N'OB', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'InsTxnResellerRateOB', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=4, 
		@freq_subday_interval=1, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20190111, 
		@active_end_date=99991231, 
		@active_start_time=0, 
		@active_end_time=235959, 
		@schedule_uid=N'3554aa92-afa2-4997-9d26-242fb1054f1c'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO


