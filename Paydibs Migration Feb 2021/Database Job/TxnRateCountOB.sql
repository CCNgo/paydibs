USE [msdb]
GO

/****** Object:  Job [TxnRateCountOB]    Script Date: 2/10/2021 4:36:02 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 2/10/2021 4:36:02 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'TxnRateCountOB', 
		@enabled=1, 
		@notify_level_eventlog=2, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'paydibsm', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [InsTxnRate]    Script Date: 2/10/2021 4:36:02 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'InsTxnRate', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=3, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'
DECLARE @pkid AS INT  --PKID from TB_TxnRate
DECLARE @cnt AS INT, @idx AS INT = 1
DECLARE @ResPKID AS INT, @DCreated AS DATETIME, @TType AS VARCHAR(7), @MID AS VARCHAR(30), @MTxnID AS VARCHAR(30), @OrdNum AS VARCHAR(20),
		@GTxnID AS VARCHAR(30), @BRefNum AS VARCHAR(30), @CCode AS CHAR(3), @HID AS INT, @Bank AS VARCHAR(20), @TAmt AS DECIMAL(18,2),
		@OriTAmt AS DECIMAL(18,2), @FXTAmt AS DECIMAL(18,2), @OriCCode AS CHAR(3), @FXCCode AS CHAR(3), @TStatus AS INT, @MTStatus AS INT, 
		@TDay AS INT, @DCompleted AS DATETIME
DECLARE @FrAmt AS DECIMAL(18,2), @ToAmt AS DECIMAL(18,2), @MR AS DECIMAL(18,4), @MFF AS DECIMAL(18,4)
DECLARE @GTax AS DECIMAL(18,4), @GTaxAmt AS DECIMAL(18,4), @MRAmt AS DECIMAL(18,4), @MFFAmt AS DECIMAL(18,4), @NetFee AS DECIMAL(18,4)/*@Round AS INT*/


IF 0=(SELECT COUNT(*) FROM TB_TxnRate)
	SET @pkid = 0
ELSE
	SELECT TOP 1 @DCompleted=DateCompleted FROM TB_TxnRate ORDER BY PKID DESC --SELECT TOP 1 @pkid=ResID FROM TB_TxnRate ORDER BY PKID DESC

SELECT IDENTITY(INT,1,1) AS ID, R.PKID, R.DateCreated, R.TxnType, R.MerchantID, R.MerchantTxnID, R.OrderNumber, R.GatewayTxnID, R.BankRefNo, R.HostID, 
	R.IssuingBank, R.CurrencyCode, R.TxnAmt, FX.FXCurrencyCode, FX.FXTxnAmt, R.TxnStatus, R.MerchantTxnStatus, R.TxnDay, R.DateCompleted
INTO #tmptbl
FROM TB_PayRes R WITH (NOLOCK)
LEFT JOIN TB_PayFX FX ON FX.GatewayTxnID = R.GatewayTxnID
WHERE R.DateCompleted > @DCompleted-- R.PKID > @pkid

SELECT @cnt = COUNT(*) FROM #tmptbl

WHILE @idx <= @cnt
BEGIN

	SELECT @ResPKID=PKID, @DCreated=DateCreated, @TType=TxnType, @MID=MerchantID, @MTxnID=MerchantTxnID, @OrdNum=OrderNumber,
		@GTxnID=GatewayTxnID, @BRefNum=BankRefNo, @OriCCode=CurrencyCode, @HID=HostID, @Bank=IssuingBank, @OriTAmt=TxnAmt,
		@TStatus=TxnStatus, @MTStatus=MerchantTxnStatus, @TDay=TxnDay, @DCompleted=DateCompleted, @FXCCode=FXCurrencyCode, @FXTAmt=FXTxnAmt
	FROM #tmptbl WHERE ID=@idx
	
	IF NOT EXISTS ( SELECT 1 FROM TB_TxnRate WHERE ResID = @ResPKID )
	BEGIN
		INSERT INTO TB_TxnRate
		(		
			ResID, DateCreated, TxnType, MerchantID, MerchantTxnID, OrderNumber, GatewayTxnID, BankRefNo, CurrencyCode, HostID, IssuingBank,
			TxnAmt, TxnStatus, MerchantTxnStatus, TxnDay, DateCompleted, FXTxnAmt, FXCurrencyCode
		)
		VALUES(@ResPKID, @DCreated, @TType, REPLACE(@MID,'' '',''''), REPLACE(@MTxnID,'' '',''''), @OrdNum, @GTxnID, REPLACE(@BRefNum,'' '',''''), @OriCCode, @HID, REPLACE(@Bank,'' '',''''), 
			@OriTAmt, @TStatus, @MTStatus, @TDay, @DCompleted, @FXTAmt, @FXCCode)
	END

	SELECT TOP 1 @GTax=Rate FROM PG..GovtTax ORDER BY StartDate DESC
	/*SELECT @Round=Rounding FROM PG..MerchantRoundUp WHERE MerchantID=@MID*/
	--Check condition here
	--Check FXCCode or FXTAmt
	--IF NOT NULL CCode = FXCCode TAmt = FXTAmt
	IF @FXCCode IS NULL OR @FXCCode = ''''
		BEGIN
			SET @CCode = @OriCCode
			SET @TAmt = @OriTAmt
		END
	ELSE
		BEGIN
			SET @CCode = @FXCCode
			SET @TAmt = @FXTAmt
		END
	SELECT @FrAmt=FromAmt, @ToAmt=ToAmt, @MR=Rate, @MFF=FixedFee 
	FROM (SELECT FromAmt, ToAmt, MerchantID, PymtMethod, HostID, Rate, FixedFee, Currency, StartDate,
	RANK() OVER (PARTITION BY MerchantID, HostID, PymtMethod, Currency ORDER BY StartDate DESC) AS Rank
	FROM PG..MerchantRate MR 
	WHERE CONVERT(VARCHAR(8), StartDate, 112) <= @TDay AND MerchantID=@MID AND HostID=@HID AND Currency=@CCode AND (PymtMethod=''OB'' OR PymtMethod=''WA'')) M
	WHERE Rank=1

	IF 1 = @@ROWCOUNT
	BEGIN
		SET @MRAmt = @TAmt * @MR
		SET @MFFAmt= @MFF
	END
	ELSE
	BEGIN
		SELECT @MR=Rate, @MFF=FixedFee
		FROM (SELECT * FROM 
			(SELECT PKID, FromAmt, ToAmt, MerchantID, PymtMethod, HostID, Rate, FixedFee, Currency, StartDate,
			RANK() OVER (PARTITION BY MerchantID, HostID, PymtMethod, Currency ORDER BY StartDate DESC) AS Rank
			FROM PG..MerchantRate MR 
			WHERE CONVERT(VARCHAR(8), StartDate, 112) <= @TDay AND MerchantID=@MID AND HostID=@HID AND Currency=@CCode AND (PymtMethod=''OB'' OR PymtMethod=''WA'')) M
			WHERE Rank=1) T
		WHERE @TAmt BETWEEN FromAmt AND ToAmt

		IF 1 = @@ROWCOUNT
		BEGIN
			SET @MRAmt = @TAmt * @MR
			SET @MFFAmt= @MFF
		END
		ELSE
		SELECT @FrAmt=FromAmt, @ToAmt=ToAmt, @MR=Rate, @MFF=FixedFee 
		FROM (SELECT FromAmt, ToAmt, MerchantID, PymtMethod, HostID, Rate, FixedFee, Currency, StartDate,
		RANK() OVER (PARTITION BY MerchantID, HostID, PymtMethod, Currency ORDER BY StartDate DESC) AS Rank
		FROM PG..MerchantRate MR 
		WHERE CONVERT(VARCHAR(8), StartDate, 112) <= @TDay AND MerchantID=''DEF'' AND HostID=@HID AND Currency=@CCode AND (PymtMethod=''OB'' OR PymtMethod=''WA'')) M
		WHERE Rank=1
		
		SET @MRAmt = @TAmt * @MR
		SET @MFFAmt= @MFF
		
	END
	--Whichever higher
	IF @MRAmt > @MFFAmt
	BEGIN
		SET @GTaxAmt = @MRAmt * @GTax
		SET @NetFee = @MRAmt
	END
	ELSE
	BEGIN
		SET @GTaxAmt = @MFFAmt * @GTax
		SET @NetFee = @MFFAmt
	END

	UPDATE TB_TxnRate
	SET MerchantRate= ISNULL(@MR,0), 
		MerchantFixedFee = ISNULL(@MFF,0),
		MerchantRateAmt = ISNULL(@MRAmt,0),
		MerchantFFeeAmt = ISNULL(@MFFAmt,0),
		MerchantGovtTax = ISNULL(@GTax,0),
		MerchantGovtTaxAmt = ISNULL(@GTaxAmt,0), 
		/*MerchantVATRate, MerchantVATFixedFee, MerchantVATTtlAmt,*/
		NetFee = ISNULL(@NetFee,0)
	WHERE ResID = @ResPKID

	/*INSERT INTO TB_TxnRate
	(		
		ResID, DateCreated, TxnType, MerchantID, MerchantTxnID, OrderNumber, GatewayTxnID, BankRefNo, CurrencyCode, HostID, IssuingBank,
		TxnAmt, TxnStatus, MerchantTxnStatus, MerchantRate, MerchantFixedFee, MerchantRateAmt, MerchantFFeeAmt, MerchantGovtTax,
		MerchantGovtTaxAmt, MerchantVATRate, MerchantVATFixedFee, MerchantVATTtlAmt, NetFee, TxnDay
	)
	VALUES(@ResPKID, @DCreated, @TType, @MID, @MTxnID, @OrdNum,	@GTxnID, @BRefNum, @CCode, @HID, @Bank, 
			@TAmt, @TStatus, @MTStatus, @MR, @MFF, @MRAmt, @MFFAmt, @GTax, @GTaxAmt, 0.00, 0.00, 0.00, @NetFee, @TDay)*/
	
	SET @MR=0
	SET @MFF=0
	SET @MRAmt=0
	SET @MFFAmt=0
	SET @GTax=0
	SET @GTaxAmt=0
	SET @NetFee=0

	SET @idx = @idx + 1
END

DROP TABLE #tmptbl', 
		@database_name=N'OB', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'InsTxnRate', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=4, 
		@freq_subday_interval=1, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20180515, 
		@active_end_date=99991231, 
		@active_start_time=0, 
		@active_end_time=235959, 
		@schedule_uid=N'd6145462-0c9f-4387-8cd9-af7a0c46e1fa'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO


