using System;
using gateway_csharp_sample_code.Controllers;
using gateway_csharp_sample_code.Gateway;
using gateway_csharp_sample_code.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using Xunit;

namespace gateway_csharp_sample_code.xunittest
{

    /// <summary>
    /// Bundle package test
    /// </summary>
    public class IdUtilsTest
    {
        public IdUtilsTest()
        {
        }

        /// <summary>
        /// Tests the IdUtil.
        /// </summary>
        [Fact]
        public void TestIdUtilsSampleId(){
            String firstId = IdUtils.generateSampleId();
            String secondId = IdUtils.generateSampleId();

            Assert.NotNull(firstId);
            Assert.NotNull(secondId);
            Assert.True(firstId != secondId);
        }

    }
}
