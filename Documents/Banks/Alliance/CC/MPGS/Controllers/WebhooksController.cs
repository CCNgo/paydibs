using System;
using System.IO;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using gateway_csharp_sample_code.Models;
using Microsoft.Extensions.Options;
using gateway_csharp_sample_code.Gateway;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace gateway_csharp_sample_code.Controllers
{
    public class WebhooksController : Controller
    {
        private GatewayApiConfig GatewayApiConfig { get; }
        private readonly ILogger Logger;

        public WebhooksController(IOptions<GatewayApiConfig> gatewayApiConfig, ILogger<WebhooksController> logger)
        {
            GatewayApiConfig = gatewayApiConfig.Value;
            Logger = logger;
        }

        [HttpGet("webhooks")]
        public IActionResult ShowWebhooks()
        {
            Logger.LogInformation("Webhooks controller ShowWebhooks action");

            return View("Webhooks");
        }

        [HttpGet("list-webhook-notifications")]
        public JsonResult ListWebhooks()
        {
            Logger.LogInformation("Webhooks controller ListWebhooks action");

            IList<WebhookNotificationModel> notifications = new List<WebhookNotificationModel>();

            string[] files = Directory.GetFiles(GatewayApiConfig.WEBHOOKS_NOTIFICATION_FOLDER);
            if (files != null && files.Length > 0)
            {
                foreach (string fileName in files)
                {
                    Logger.LogInformation($"Reading webhook notification file {fileName}");

                    string json = System.IO.File.ReadAllText(fileName);

                    Logger.LogInformation($"File ReadAllText = {json}");

                    WebhookNotificationModel notification = JsonConvert.DeserializeObject<WebhookNotificationModel>(json);

                    Logger.LogInformation($"Webhook notification created from file, WebhookNotificationModel {notification}");

                    notifications.Add(notification);
                }
            }
            else
            {
                Logger.LogInformation("No webhook notifications files found!");
            }

            return Json(notifications);
        }

        [HttpPost("process-webhook")]
        public IActionResult ProcessWebhook([FromBody] WebhookNotificationModel notification, [FromHeader(Name = "X-Notification-Secret")] string notificationSecret)
        {
            Logger.LogInformation("Webhooks controller ProcessWebhook action");

            Debug.WriteLine($"-------------GatewayApiConfig.WebhooksNotificationSecret {GatewayApiConfig.WebhooksNotificationSecret} --- {notificationSecret}");

            if (GatewayApiConfig.WebhooksNotificationSecret == null || notificationSecret == null || notificationSecret != GatewayApiConfig.WebhooksNotificationSecret)
            {
                Logger.LogError("Webhooks notification secret doesn't match, so not processing the incoming request!");
                return Ok();
            }
            Logger.LogInformation("Webhooks notification secret matches");

            string json = JsonConvert.SerializeObject(notification);
            Logger.LogInformation($"Webhooks notification model {json}");

            // epoch
            notification.Timestamp = Convert.ToInt64((DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds);

            System.IO.File.WriteAllText($@"{GatewayApiConfig.WEBHOOKS_NOTIFICATION_FOLDER}/WebHookNotifications_{notification.Timestamp}.json", json);

            return Ok();
        }
    }
}
