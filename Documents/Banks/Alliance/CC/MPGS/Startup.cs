using System;
using System.IO;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using gateway_csharp_sample_code.Gateway;
using System.Runtime.InteropServices;

namespace gateway_csharp_sample_code
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        private readonly ILogger Logger;
     
        public Startup(IConfiguration configuration, ILogger<Startup> logger)
        {
            Configuration = configuration;
            Logger = logger;

            // Runtime Info:
            Logger.LogInformation($@"
                FrameworkDescription:   {RuntimeInformation.FrameworkDescription}
                OSDescription:          {RuntimeInformation.OSDescription}
                OSArchitecture:         {RuntimeInformation.OSArchitecture}
                ProcessArchitecture:    {RuntimeInformation.ProcessArchitecture}
            ");
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<GatewayApiConfig>(Configuration.GetSection("GatewayApiConfig"));
            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromMinutes(30);
            });

            services.AddMvc()
                    .AddSessionStateTempDataProvider();

            initWebhooksNotificationsFolder();

            services.AddSingleton<GatewayApiClient, GatewayApiClient>();
            services.AddSingleton<NVPApiClient, NVPApiClient>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Payment/Error");
            }
            
            app.UseStaticFiles();
            app.UseSession();
            app.UseMvc();
        }

        private void initWebhooksNotificationsFolder()
        {
            try
            {
                if (Directory.Exists(GatewayApiConfig.WEBHOOKS_NOTIFICATION_FOLDER))
                {
                    Directory.Delete(GatewayApiConfig.WEBHOOKS_NOTIFICATION_FOLDER, true);
                }

                Directory.CreateDirectory(GatewayApiConfig.WEBHOOKS_NOTIFICATION_FOLDER);
            }
            catch (IOException e)
            {
                Logger.LogError(e, $"Error deleting Webhook notification folder: {GatewayApiConfig.WEBHOOKS_NOTIFICATION_FOLDER}");
            }
        }
    }
}
