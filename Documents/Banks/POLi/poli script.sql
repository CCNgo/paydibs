-- Channel --

INSERT [dbo].[Channel] ([IPAddress], [PortNumber], [QueryIPAddress], [QueryPortNumber], [CancelIPAddress], [CancelPortNumber], [RevIPAddress], [RevPortNumber], [AckIPAddress], [AckPortNumber], [ReturnIPAddresses], [ReturnURL], [ReturnURL2], [TimeOut], [DBConnStr], [ServerCertName], [CfgFile], [Protocol], [Desc], [RecStatus]) VALUES 
(N'https://poliapi.uat1.paywithpoli.com/api/v2/Transaction/Initiate', 443, N'https://poliapi.uat1.paywithpoli.com/api/v2/Transaction/GetTransaction', 443, N'', 0, N'', 0, N'https://poliapi.uat1.paywithpoli.com/api/v2/Transaction/GetTransaction', 0, N'', N'https://payment.paydibs.com/PPGtest/resppoli.aspx', N'https://payment.paydibs.com/PPGtest/resppoli_s2s.aspx', 300, N'', N'', N'templates\POLi2.txt', N'TLS12', N'POLi', 1)


-- Host --

INSERT [dbo].[Host] ([HostName], [HostCode], [SvcTypeID], [PymtMethod], [SignBy], [PaymentTemplate], [SecondEntryTemplate], [MesgTemplate], [NeedReplyAcknowledgement], [NeedRedirectOTP], [Require2ndEntry], [AllowQuery], [AllowReversal], [MaxRefund], [ChannelID], [Timeout], [OTCRevTimeout], [OTCGenMethod], [TxnStatusActionID], [HostReplyMethod], [OSPymtCode], [GatewayTxnIDFormat], [MID], [MerchantPassword], [ReturnKey], [HashMethod], [SecretKey], [InitVector], [LogoPath], [RepStartTime], [QueryFlag], [SettlePeriod], [SettleTime], [HostMDR], [LogRes], [DateActivated], [DateDeactivated], [RecStatus], [Desc], [FixedCurrency], [IsExponent], [FloorAmt], [CeilingAmt], [HostDesc]) VALUES 
(N'POLi', NULL, 2, N'OB', 0, N'RTTemplates\redirect_post.html', N'', N'', 0, 1, 0, 1, 0, -1, 8, 300, 0, 0, 8, 1, N'', 1, NULL, N'', N'', N'', N'', N'', N'POLI.png', N'00:00', 1, 1, NULL, CAST(0.00 AS Decimal(18, 2)), 1, CAST(N'2019-02-27T10:52:14.683' AS DateTime), CAST(N'2099-12-31T00:00:00.000' AS DateTime), 1, N'POLi', NULL, 0, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'POLi')


--- TxnStatusAction ---
Set Action ID
-------------------------
INSERT [dbo].[TxnStatusAction] ([ActionID], [TxnType], [TxnField], [TxnStatus], [Action], [Desc], [Priority], [RecStatus]) VALUES (8, N'PAY3;QUERY', NULL, N'Initiated', 3, N'Pending', 1, 1)
INSERT [dbo].[TxnStatusAction] ([ActionID], [TxnType], [TxnField], [TxnStatus], [Action], [Desc], [Priority], [RecStatus]) VALUES (8, N'PAY3;QUERY', NULL, N'FinancialInstitutionSelected', 3, N'Pending', 1, 1)
INSERT [dbo].[TxnStatusAction] ([ActionID], [TxnType], [TxnField], [TxnStatus], [Action], [Desc], [Priority], [RecStatus]) VALUES (8, N'PAY3;QUERY', NULL, N'EULAAccepted', 1, N'Success', 1, 1)
INSERT [dbo].[TxnStatusAction] ([ActionID], [TxnType], [TxnField], [TxnStatus], [Action], [Desc], [Priority], [RecStatus]) VALUES (8, N'PAY3;QUERY', NULL, N'InProcess', 3, N'Pending', 1, 1)
INSERT [dbo].[TxnStatusAction] ([ActionID], [TxnType], [TxnField], [TxnStatus], [Action], [Desc], [Priority], [RecStatus]) VALUES (8, N'PAY3;QUERY', NULL, N'Unknown', 3, N'Pending', 1, 1)
INSERT [dbo].[TxnStatusAction] ([ActionID], [TxnType], [TxnField], [TxnStatus], [Action], [Desc], [Priority], [RecStatus]) VALUES (8, N'PAY3;QUERY', NULL, N'Completed', 1, N'Success', 1, 1)
INSERT [dbo].[TxnStatusAction] ([ActionID], [TxnType], [TxnField], [TxnStatus], [Action], [Desc], [Priority], [RecStatus]) VALUES (8, N'PAY3;QUERY', NULL, N'!Completed', 2, N'Failed', 1, 1)
INSERT [dbo].[TxnStatusAction] ([ActionID], [TxnType], [TxnField], [TxnStatus], [Action], [Desc], [Priority], [RecStatus]) VALUES (8, N'PAY3:QUERY', NULL, N'ReceiptUnverified', 3, N'Pending', 1, 1)



---- Terminals_PreSetCredential ----

INSERT [dbo].[Terminals_PreSetCredential] ([HostID], [PaymentMethod], [Bank], [FieldName], [DBFieldName], [Encrypt], [DefaultValue], [Hex], [Currency]) VALUES (10, N'OB', N'POLi', N'Merchant ID', N'SendKeyPath', N'0', N'T6100205', 0, N'AUD')
INSERT [dbo].[Terminals_PreSetCredential] ([HostID], [PaymentMethod], [Bank], [FieldName], [DBFieldName], [Encrypt], [DefaultValue], [Hex], [Currency]) VALUES (10, N'OB', N'POLi', N'Password', N'ReturnKeyPath', N'0', N'mM9uYnFk!0xqaHpfX', 0, N'AUD')


