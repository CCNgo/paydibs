USE [OB]
GO
--Channel
SET IDENTITY_INSERT [dbo].[Channel] ON 
INSERT [dbo].[Channel] ([ChannelID], [IPAddress], [PortNumber], [QueryIPAddress], [QueryPortNumber], 
[CancelIPAddress], [CancelPortNumber], [RevIPAddress], [RevPortNumber], [AckIPAddress], [AckPortNumber], 
[ReturnIPAddresses], [ReturnURL], [ReturnURL2], [TimeOut], [DBConnStr], [ServerCertName], [CfgFile], 
[Protocol], [Desc], [RecStatus]) 
VALUES (9, N'https://gw.dragonpay.ph/Pay.aspx', 443, N'https://gw.dragonpay.ph/DragonPayWebService/MerchantService.asmx', 443, 
N'', 443, N'', 0, N'', 0, N'', N'', N'', 900, N'', N'', N'template\DragonPay.txt', N'TLS', N'DragonPay', 1)
SET IDENTITY_INSERT [dbo].[Channel] OFF


--TxnStatusAction
SET IDENTITY_INSERT [dbo].[TxnStatusAction] ON 
INSERT [dbo].[TxnStatusAction] ([PKID], [ActionID], [TxnType], [TxnField], [TxnStatus], [Action], [Desc], [Priority], [RecStatus]) VALUES (36, 9, N'PAY;QUERY', NULL, N'S', 1, N'Success', 1, 1)
INSERT [dbo].[TxnStatusAction] ([PKID], [ActionID], [TxnType], [TxnField], [TxnStatus], [Action], [Desc], [Priority], [RecStatus]) VALUES (37, 9, N'PAY;QUERY', NULL, N'P', 3, N'Pending', 1, 1)
INSERT [dbo].[TxnStatusAction] ([PKID], [ActionID], [TxnType], [TxnField], [TxnStatus], [Action], [Desc], [Priority], [RecStatus]) VALUES (38, 9, N'PAY;QUERY', NULL, N'!S', 2, N'Failed', 1, 1)
SET IDENTITY_INSERT [dbo].[TxnStatusAction] OFF


--Host
SET IDENTITY_INSERT [dbo].[Host] ON 
INSERT [dbo].[Host] ([HostID], [HostName], [HostCode], [SvcTypeID], [PymtMethod], 
[SignBy], [PaymentTemplate], [SecondEntryTemplate], [MesgTemplate], [NeedReplyAcknowledgement], 
[NeedRedirectOTP], [Require2ndEntry], [AllowQuery], [AllowReversal], [MaxRefund], [ChannelID], 
[Timeout], [OTCRevTimeout], [OTCGenMethod], [TxnStatusActionID], [HostReplyMethod], [OSPymtCode], 
[GatewayTxnIDFormat], [MID], [MerchantPassword], [ReturnKey], [HashMethod], [SecretKey], [InitVector], 
[LogoPath], [RepStartTime], [QueryFlag], [SettlePeriod], [SettleTime], [HostMDR], [LogRes], [DateActivated], 
[DateDeactivated], [RecStatus], [Desc], [FixedCurrency], [IsExponent], [FloorAmt], [CeilingAmt], [HostDesc]) 
VALUES (11, N'DragonPay', NULL, 2, N'OB', 0, N'RTTemplates\redirect_post.html', N'', N'', 0, 1, 0, 1, 0, -1, 9, 900, 0, 0, 9, 0, N'', 0, NULL, N'', N'', N'', N'', N'', N'DragonPay.png', N'00:00', 1, 1, NULL, CAST(0.00 AS Decimal(18, 2)), 1, CAST(N'2019-03-06T16:44:00.000' AS DateTime), CAST(N'2019-03-06T16:44:00.000' AS DateTime), 0, N'DragonPay', NULL, 0, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'DragonPay')
SET IDENTITY_INSERT [dbo].[Host] OFF


USE [PG]
GO
--Terminals_PreSetCredential
SET IDENTITY_INSERT [dbo].[Terminals_PreSetCredential] ON 
INSERT [dbo].[Terminals_PreSetCredential] ([ID], [HostID], [PaymentMethod], [Bank], [FieldName], [DBFieldName], [Encrypt], [DefaultValue], 
[Hex]) VALUES (40, 11, N'OB', N'DragonPay', N'MerchantID', N'MID', N'0', N'PAYDIBS2', 0)
INSERT [dbo].[Terminals_PreSetCredential] ([ID], [HostID], [PaymentMethod], [Bank], [FieldName], [DBFieldName], [Encrypt], [DefaultValue], 
[Hex]) VALUES (41, 11, N'OB', N'DragonPay', N'Secret Key (REQ)', N'Password', N'0', N'6A90E23BDCAE9DDF001F8570F5516FC6', 0)
INSERT [dbo].[Terminals_PreSetCredential] ([ID], [HostID], [PaymentMethod], [Bank], [FieldName], [DBFieldName], [Encrypt], [DefaultValue], 
[Hex]) VALUES (42, 11, N'OB', N'DragonPay', N'Secret Key (RES)', N'ReturnKey', N'0', N'6A90E23BDCAE9DDF001F8570F5516FC6', 0)
SET IDENTITY_INSERT [dbo].[Terminals_PreSetCredential] OFF