<? 

session_start(); 
	
$rand = rand(0,9999);
date_default_timezone_set("Asia/Kuala_Lumpur");
$today = date("Ymd");
$threeid = $today.$rand;	
	
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>

    <link rel="stylesheet" type="text/css" href="assets/paymentstyle.css" />

    <head>
        <title>API Example Code</title>
        <meta http-equiv="Content-Type" content="text/html, charset=iso-8859-1">
    </head>

    <body>

       
        <h3>Simple Pay Operation - 3DS</h3>
        

        <form action="process.php" method="post">

        <table width="60%" align="center" cellpadding="5" border="0">

           

             <tr class="shade">
                 <td align="right" width="50%"><strong>version </strong></td>
                 <td width="50%"><input type="text" readonly="readonly" name="version" value="42" size="8" maxlength="80" /></td>
             </tr>
			 
             <tr class="shade">
                 <td align="right" width="50%"><strong>Merchant ID </strong></td>
                 <td width="50%"><input type="text"  name="apiMerchant" value="TEST001918500471" size="30" maxlength="80"/></td>
             </tr>
			 
             <tr class="shade">
                 <td align="right" width="50%"><strong>API Username </strong></td>
                 <td width="50%"><input type="text"  name="apiUsername" value="merchant.TEST001918500471" size="30" maxlength="80"/></td>
             </tr>
			 
             <tr class="shade">
                 <td align="right" width="50%"><strong>API Password </strong></td>
                 <td width="50%"><input type="text"  name="apiPassword" value="d253e202f17dc6b0212f5a9fb88cd95e" size="30" maxlength="80"/></td>
             </tr>

             <tr><td colspan="2"></td></tr>

             <tr class="title">
                 <td colspan="2" height="25"><P><strong>&nbsp;Transaction Fields</strong></P></td>
             </tr>

             <tr>
                 <td align="right" width="50%"><strong>method </strong></td>
                 <td width="50%"><input type="text" readonly="readonly" name="method" value="PUT" size="20" maxlength="80"/></td>
             </tr>

             <tr class="shade">
                 <td align="right" width="50%"><strong>apiOperation </strong></td>
                 <td width="50%"><input type="text" readonly="readonly" name="apiOperation" value="CHECK_3DS_ENROLLMENT" size="30" maxlength="80"/></td>
             </tr>
			 
            <tr>
                <td align="right"><strong>3DSecureId </strong></td>
                <td><input type="text" name="3DSecureId" value="<?=$threeid?>" size="40" maxlength="80"/></td>
            </tr>

            <tr>
                <td align="right"><strong>3DSecure.authenticationRedirect.responseUrl </strong></td>
                <td><input type="text" name="3DSecure[authenticationRedirect][responseUrl]" value="http://localhost/MPGS/Pay_3DS_PRD/authenticate.php" size="40" maxlength="80"/></td>
            </tr>
            
            <tr class="shade">
                 <td align="right"><strong>order.amount </strong></td>
                 <td><input type="text" name="order[amount]" value="1" size="8" maxlength="13"/></td>
             </tr>

             <tr>
                 <td align="right"><strong>order.currency </strong></td>
                 <td><input type="text" name="order[currency]" value="MYR" size="8" maxlength="3"/></td>
             </tr>

             <tr class="shade">
                 <td align="right"><strong>sourceOfFunds.provided.card.number </strong></td>
                 <td><input type="text" name="sourceOfFunds[provided][card][number]" value="5123450000000008" size="19" maxlength="80"/></td>
             </tr>

             <tr>
                 <td align="right"><strong>sourceOfFunds.provided.card.expiry.month </strong></td>
                 <td><input type="text" name="sourceOfFunds[provided][card][expiry][month]" value="05" size="1" maxlength="2"/></td>
             </tr>

             <tr class="shade">
                 <td align="right"><strong>sourceOfFunds.provided.card.expiry.year </strong></td>
                 <td><input type="text" name="sourceOfFunds[provided][card][expiry][year]" value="21" size="1" maxlength="2"/></td>
             </tr>
	 		 <!--
             <tr>
                 <td align="right"><strong>sourceOfFunds.provided.card.securityCode </strong></td>
                 <td><input type="text" name="sourceOfFunds[provided][card][securityCode]" value="100" size="8" maxlength="4"/></td>
             </tr>
			 -->
             

             <tr>
                 <td colspan="2"><center><input type="submit" name="submit" value="Check 3DS Enrollment"/></center></td>
             </tr>

             <tr><td colspan="2"></td></tr>

            
        </table>


        </form>
        <br/><br/>

    </body>
</html>
