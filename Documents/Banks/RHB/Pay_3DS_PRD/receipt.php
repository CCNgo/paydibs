<?php

session_start();

$rawdata = file_get_contents('php://input');
$data = array();
parse_str($rawdata, $data);
$_POST = array_merge($data, $_POST);


$_SESSION['post-data'] = $_POST;




$errorMessage = "";
$errorCode = "";
$gatewayCode = "";
$result = "";
$xid = "";
$summaryStatus = "";
$gatewayCode = "";

$tmpArray = array();

// [Snippet] howToDecodeResponse - start
// $response is defined in process.php as the server response
$responseArray = json_decode($response, TRUE);
  
//echo $response;
  
// [Snippet] howToDecodeResponse - end

// either a HTML error was received
// or response is a curl error
if ($responseArray == NULL) {
  print("JSON decode failed. Please review server response (enable debug in config.php).");
  //die();
}



$xid = $responseArray['3DSecure']['xid'];
$summaryStatus = $responseArray['3DSecure']['summaryStatus'];
$gatewayCodeRes = $responseArray['response']['3DSecure']['gatewayCode'];
$htmlBodyContent = $responseArray['3DSecure']['authenticationRedirect']['simple']['htmlBodyContent'];
$authenticationToken = $responseArray['3DSecure']['authenticationToken'];
$acsEci = $responseArray['3DSecure']['acsEci'];
$cardNo = $_SESSION['post-data']["sourceOfFunds"]["provided"]["card"]["number"];
$expMonth = $_SESSION['post-data']["sourceOfFunds"]["provided"]["card"]["expiry"]["month"];
$expYear = $_SESSION['post-data']["sourceOfFunds"]["provided"]["card"]["expiry"]["year"];
//$securityCode = $_POST["sourceOfFunds"]["provided"]["card"]["securityCode"];
$orderAmount = $_SESSION['post-data']["order"]["amount"];
$orderCurrency = $_SESSION['post-data']["order"]["currency"];
$merchantId = $configArray["merchantId"];
$password = $configArray["password"];
$apiUsername = $configArray["apiUsername"];


//session_start();

	
// [Snippet] howToParseResponse - start
if (array_key_exists("result", $responseArray))
  $result = $responseArray["result"];
// [Snippet] howToParseResponse - end

// Form error string if error is triggered
if ($result == "FAIL") {
  if (array_key_exists("reason", $responseArray)) {
    $tmpArray = $responseArray["reason"];

    if (array_key_exists("explanation", $tmpArray)) {
      $errorMessage = rawurldecode($tmpArray["explanation"]);
    }
    else if (array_key_exists("supportCode", $tmpArray)) {
      $errorMessage = rawurldecode($tmpArray["supportCode"]);
    }
    else {
      $errorMessage = "Reason unspecified.";
    }

    if (array_key_exists("code", $tmpArray)) {
      $errorCode = "Error (" . $tmpArray["code"] . ")";
    }
    else {
      $errorCode = "Error (UNSPECIFIED)";
    }
  }
}

else {
  if (array_key_exists("gatewayCode", $responseArray)) {
    $tmpArray = $responseArray["gatewayCode"];
    if (array_key_exists("gatewayCode", $tmpArray))
      $gatewayCode = rawurldecode($tmpArray["gatewayCode"]);
    else
      $gatewayCode = "Response not received.";
  }
}

?>
<!-- 	The following is a simple HTML page to display the response to the transaction.
      This should never be used in your integration -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <link rel="stylesheet" type="text/css" href="assets/paymentstyle.css" />
    <head>
      <title>API Example Code</title>
      <meta http-equiv="Content-Type" content="text/html, charset=iso-8859-1">
    </head>
    <body>
    <br/>
    <center><h1>PHP Example - REST (JSON)</h1></center>
    <center><h3>Receipt Page</h3></center><br/><br/>

  <table width="60%" align="center" cellpadding="5" border="0">

  <?php
    // echo HTML displaying Error headers if error is found
    if ($errorCode != "" || $errorMessage != "") {
  ?>
      <tr class="title">
             <td colspan="2" height="25"><P><strong>&nbsp;Error Response</strong></P></td>
         </tr>
         <tr>
             <td align="right" width="50%"><strong><i><?=$errorCode?>: </i></strong></td>
             <td width="50%"><?=$errorMessage?></td>
         </tr>
  <?php
    }

    else {
  ?>
      <tr class="title">
             <td colspan="2" height="25"><P><strong>&nbsp;<?=$gatewayCode?></strong></P></td>
         </tr>
         <tr>
             <td align="right" width="50%"><strong><i>xid: </i></strong></td>
             <td width="50%"><?=$xid ?></td>
         </tr>
         <tr>
             <td align="right" width="50%"><strong><i>summaryStatus: </i></strong></td>
             <td width="50%"><?=$summaryStatus?></td>	
         </tr>
         <tr>
             <td align="right" width="50%"><strong><i>gatewayCode: </i></strong></td>
             <td width="50%"><?=$gatewayCodeRes?></td>
         </tr>
		 
  <?php
    // echo HTML displaying Error headers if error is found
    if ($summaryStatus == "CARD_ENROLLED") {
  ?>
  
  <tr>
  <td align="right" width="50%"><strong><i>htmlBodyContent: </i></strong></td>
	<td><?=$htmlBodyContent?></td>
  </tr>
  
  <?php
	}else if ($summaryStatus == "CARD_NOT_ENROLLED"){
		
     }else if ($summaryStatus == "AUTHENTICATION_SUCCESSFUL"){
		
		?>
		
	<form action="process2.php" method="post">

        <table width="60%" align="center" cellpadding="5" border="0">

             <tr class="shade">
                 <td align="right" width="50%"><strong>version </strong></td>
                 <td width="50%"><input type="text" readonly="readonly" name="version" value="42" size="8" maxlength="80" /></td>
             </tr>
			 
             <tr class="shade">
                 <td align="right" width="50%"><strong>Merchant ID </strong></td>
                 <td width="50%"><input type="text" readonly="readonly" name="apiMerchant" value="<?=$merchantId?>" size="30" maxlength="80"/></td>
             </tr>
			 
             <tr class="shade">
                 <td align="right" width="50%"><strong>API Username </strong></td>
                 <td width="50%"><input type="text" readonly="readonly" name="apiUsername" value="<?=$apiUsername?>" size="30" maxlength="80"/></td>
             </tr>
			 
             <tr class="shade">
                 <td align="right" width="50%"><strong>API Password </strong></td>
                 <td width="50%"><input type="text" readonly="readonly" name="apiPassword" value="<?=$password?>" size="30" maxlength="80"/></td>
             </tr>

             <tr>
                 <td align="right" width="50%"><strong>order.id</strong></td>
                 <td><input type="text" name="orderId" value="" size="20" maxlength="60"/></td>
             </tr>

             <tr class="shade">
                  <td align="right" width="50%"><strong>transaction.id</strong></td>
                <td><input type="text" name="transactionId" value="" size="20" maxlength="60"/></td>
             </tr>

             <tr><td colspan="2"></td></tr>

             <tr class="title">
                 <td colspan="2" height="25"><P><strong>&nbsp;Transaction Fields</strong></P></td>
             </tr>

             <tr>
                 <td align="right" width="50%"><strong>method </strong></td>
                 <td width="50%"><input type="text" readonly="readonly" name="method" value="PUT" size="20" maxlength="80"/> ** See Note 1 Below</td>
             </tr>

             <tr class="shade">
                 <td align="right" width="50%"><strong>apiOperation </strong></td>
                 <td width="50%"><input type="text" readonly="readonly" name="apiOperation" value="PAY" size="20" maxlength="80"/></td>
             </tr>

            <tr>
                <td align="right"><strong>sourceOfFunds.type </strong></td>
                <td><input type="text" name="sourceOfFunds[type]" value="CARD" size="19" maxlength="80"/></td>
            </tr>
			
            <tr>
                <td align="right"><strong>3DSecure.acsEci</strong></td>
                <td><input type="text" name="3DSecure[acsEci]" readonly="readonly" value="<?=$acsEci?>" size="40" maxlength="80"/></td>
            </tr>
			
            <tr>
                <td align="right"><strong>3DSecure.authenticationStatus</strong></td>
                <td><input type="text" name="3DSecure[authenticationStatus]" readonly="readonly" value="<?=$summaryStatus?>" size="40" maxlength="80"/></td>
            </tr>
			
            <tr>
                <td align="right"><strong>3DSecure.enrollmentStatus</strong></td>
                <td><input type="text" name="3DSecure[enrollmentStatus]" readonly="readonly" value="ENROLLED" size="40" maxlength="80"/></td>
            </tr>
			
            <tr>
                <td align="right"><strong>3DSecure.authenticationToken</strong></td>
                <td><input type="text" name="3DSecure[authenticationToken]" readonly="readonly" value="<?=$authenticationToken?>" size="40" maxlength="80"/></td>
            </tr>
		
             <tr class="shade">
                 <td align="right"><strong>sourceOfFunds.provided.card.number </strong></td>
                 <td><input type="text" name="sourceOfFunds[provided][card][number]" value="<?=$cardNo?>" size="19" maxlength="80"/></td>
             </tr>

             <tr>
                 <td align="right"><strong>sourceOfFunds.provided.card.expiry.month </strong></td>
                 <td><input type="text" name="sourceOfFunds[provided][card][expiry][month]" value="<?=$expMonth?>" size="1" maxlength="2"/></td>
             </tr>

             <tr class="shade">
                 <td align="right"><strong>sourceOfFunds.provided.card.expiry.year </strong></td>
                 <td><input type="text" name="sourceOfFunds[provided][card][expiry][year]" value="<?=$expYear?>" size="1" maxlength="2"/></td>
             </tr>

<!--             <tr>
                 <td align="right"><strong>sourceOfFunds.provided.card.securityCode </strong></td>
                 <td><input type="text" name="sourceOfFunds[provided][card][securityCode]" value="<?=$securityCode?>" size="8" maxlength="4"/></td>
             </tr>
-->
             <tr class="shade">
                 <td align="right"><strong>order.amount </strong></td>
                 <td><input type="text" name="order[amount]" value="<?=$orderAmount?>" size="8" maxlength="13"/></td>
             </tr>

             <tr>
                 <td align="right"><strong>order.currency </strong></td>
                 <td><input type="text" name="order[currency]" value="<?=$orderCurrency?>" size="8" maxlength="3"/></td>
             </tr>

             <tr>
                 <td colspan="2"><center><input type="submit" name="submit" value="Process Payment"/></center></td>
             </tr>

             <tr><td colspan="2"></td></tr>

             <tr>
                 <td colspan="2" height="25"><P class="desc"><strong>Note 1:</strong> This field is used by this example to set the HTTP Method for sending the transaction. In your integration, you should determine the HTTP Method in your code (process.php based on this example) and never display it to the card holder or pass it as a hidden field.</P></td>
             </tr>

        </table>


        </form>
		
		
		<?php
		
     }
	}
  ?>
      <!-- Response Fields -->

         <tr class="title">
             <td colspan="2" height="20"><P><strong>&nbsp;JSON Response</strong></P></td>
         </tr>

  </table>

  <table width="50%" align="center" cellpadding="5" border="0">
    <tr>
      <td><p>The display of the below response is intended to be for this example only. In your integration, you should parse this 		response to extract and use the response fields required.</p>
      </td>
    </tr>
     <tr>
       <td align="center" width="100%">
          <textarea rows="40" cols="118" name="outContent" id="outContent"><?=$response?></textarea>
      </td>
    </tr>

    <!-- The below Java Script & HTML formats the JSON output result to make it clean
         and readable. You should not use these scripts to format or expose the
         JSON response in your integration, but rather store/use any of the
         specific fields required for your integration -->
    <tr>
      <td align="center" width="100%">
        <p>Note: The above response has been formatted to make it easier to read. The reformatting also changes amounts to be strictly defined JSON numbers. This means 0's are removed from after the decimal place i.e. 1.00 is displayed as 1 and 1.10 is displayed as 1.1. <a href="javascript:displayRawJSON()">Click here to display the unformatted JSON Response</a></p>
      </td>
    </tr>
    <script type="text/javascript" src="assets/json2.js"></script>
    <script type="text/javascript" src="assets/jsonformatter.js"></script>
    <script type="text/javascript" src="assets/jquery-1.3.2.js"></script>

    <script>
      var orginalJSON = $("#outContent").val();
      function FormatTextarea() {
        var sJSON = $("#outContent").val();
        var oJSON = JSON.parse(sJSON);
        sJSON = FormatJSON(oJSON);
        $("#outContent").val(sJSON);
      }
      FormatTextarea();

      function displayRawJSON() {
        $("#outContent").val(orginalJSON);
      }
    </script>

   </table>

  <br/><br/>
   </body>
</html>