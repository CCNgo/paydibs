<?php
session_start();
include "api_lib.php";
include "configuration.php";
include "connection.php";

$ord = 'ORD';
$rand = $ord.rand(0,999999);

$merchantId = $configArray["merchantId"];
$password = $configArray["password"];
$apiUsername = $configArray["apiUsername"];
$cardNo = $_SESSION['post-data']["sourceOfFunds"]["provided"]["card"]["number"];
$expMonth = $_SESSION['post-data']["sourceOfFunds"]["provided"]["card"]["expiry"]["month"];
$expYear = $_SESSION['post-data']["sourceOfFunds"]["provided"]["card"]["expiry"]["year"];
$orderAmount = $_SESSION['post-data']["order"]["amount"];
$orderCurrency = $_SESSION['post-data']["order"]["currency"];
$secureId = $_SESSION['post-data']['3DSecureId'];
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>

    <link rel="stylesheet" type="text/css" href="assets/paymentstyle.css" />

    <head>
        <title>API Example Code</title>
        <meta http-equiv="Content-Type" content="text/html, charset=iso-8859-1">
    </head>

    <body>

<form action="process2.php" method="post">

        <table width="60%" align="center" cellpadding="5" border="0">

             <tr class="shade">
                 <td align="right" width="50%"><strong>version </strong></td>
                 <td width="50%"><input type="text" name="version" value="42" size="8" maxlength="80" /></td>
             </tr>
			 
             <tr class="shade">
                 <td align="right" width="50%"><strong>Merchant ID </strong></td>
                 <td width="50%"><input type="text"  name="apiMerchant" value="<?=$merchantId?>" size="30" maxlength="80"/></td>
             </tr>
			 
             <tr class="shade">
                 <td align="right" width="50%"><strong>API Username </strong></td>
                 <td width="50%"><input type="text" name="apiUsername" value="<?=$apiUsername?>" size="30" maxlength="80"/></td>
             </tr>
			 
             <tr class="shade">
                 <td align="right" width="50%"><strong>API Password </strong></td>
                 <td width="50%"><input type="text" name="apiPassword" value="<?=$password?>" size="30" maxlength="80"/></td>
             </tr>

             <tr>
                 <td align="right" width="50%"><strong>order.id</strong></td>
                 <td><input type="text" name="orderId" value="<?=$rand?>" size="20" maxlength="60"/></td>
             </tr>
             <tr>
                 <td align="right" width="50%"><strong>order.id</strong></td>
                 <td><input type="text" name="order[reference]" value="<?=$rand?>" size="20" maxlength="60"/></td>
             </tr>

             <tr class="shade">
                  <td align="right" width="50%"><strong>transaction.id</strong></td>
                <td><input type="text" name="transactionId" value="<?=$rand?>" size="20" maxlength="60"/></td>
             </tr>
             <tr class="shade">
                  <td align="right" width="50%"><strong>transaction.id</strong></td>
                <td><input type="text" name="transaction[reference]" value="<?=$rand?>" size="20" maxlength="60"/></td>
             </tr>

             <tr><td colspan="2"></td></tr>

             <tr class="title">
                 <td colspan="2" height="25"><P><strong>&nbsp;Transaction Fields</strong></P></td>
             </tr>

             <tr>
                 <td align="right" width="50%"><strong>method </strong></td>
                 <td width="50%"><input type="text"  name="method" value="PUT" size="20" maxlength="80"/></td>
             </tr>

             <tr class="shade">
                 <td align="right" width="50%"><strong>apiOperation </strong></td>
                 <td width="50%"><input type="text"  name="apiOperation" value="PAY" size="20" maxlength="80"/></td>
             </tr>

            <tr>
                <td align="right"><strong>sourceOfFunds.type </strong></td>
                <td><input type="text" name="sourceOfFunds[type]" value="CARD" size="19" maxlength="80"/></td>
            </tr>			
            	
             <tr class="shade">
                 <td align="right"><strong>sourceOfFunds.provided.card.number </strong></td>
                 <td><input type="text" name="sourceOfFunds[provided][card][number]" value="<?=$cardNo?>" size="19" maxlength="80"/></td>
             </tr>

             <tr>
                 <td align="right"><strong>sourceOfFunds.provided.card.expiry.month </strong></td>
                 <td><input type="text" name="sourceOfFunds[provided][card][expiry][month]" value="<?=$expMonth?>" size="1" maxlength="2"/></td>
             </tr>

             <tr class="shade">
                 <td align="right"><strong>sourceOfFunds.provided.card.expiry.year </strong></td>
                 <td><input type="text" name="sourceOfFunds[provided][card][expiry][year]" value="<?=$expYear?>" size="1" maxlength="2"/></td>
             </tr>

          <tr>
                 <td align="right"><strong>sourceOfFunds.provided.card.securityCode </strong></td>
                 <td><input type="text" name="sourceOfFunds[provided][card][securityCode]?>" value="100" size="8" maxlength="4"/></td>
             </tr>

             <tr class="shade">
                 <td align="right"><strong>order.amount </strong></td>
                 <td><input type="text" name="order[amount]" value="<?=$orderAmount?>" size="8" maxlength="13"/></td>
             </tr>

             <tr>
                 <td align="right"><strong>order.currency </strong></td>
                 <td><input type="text" name="order[currency]" value="<?=$orderCurrency?>" size="8" maxlength="3"/></td>
             </tr>
			<tr>
                <td align="right"><strong>3DSecureId </strong></td>
                <td><input type="text" name="3DSecureId" value="<?=$secureId?>" size="40" maxlength="80"/></td>
            </tr>

             <tr>
                 <td colspan="2"><center><input type="submit" name="submit" value="Process Payment"/></center></td>
             </tr>

             <tr><td colspan="2"></td></tr>

            

        </table>


        </form>

    </body>
</html>



