<?php
//session_start();
//echo $response;

$acsEci = "";
$summaryStatus = "";
$SecureId = "";
$merchant  = "";
$xid = "";
$gatewayCode = "";
$htmlBodyContent = "";

$responseArray = json_decode($response, TRUE);
//echo $responseArray['3DSecure'];
$acsEci = $responseArray['3DSecure']['acsEci'];
$summaryStatus = $responseArray['3DSecure']['summaryStatus'];
$SecureId = $_SESSION['post-data']['3DSecureId'];
$merchant = $responseArray['merchant'];
$xid = $responseArray['3DSecure']['xid'];
$gatewayCode = $responseArray['response']['3DSecure']['gatewayCode'];
$htmlBodyContent = $responseArray['3DSecure']['authenticationRedirect']['simple']['htmlBodyContent'];


?>
<!-- 	The following is a simple HTML page to display the response to the transaction.
      This should never be used in your integration -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <link rel="stylesheet" type="text/css" href="http://localhost/MPGS/Pay_3DS_PRD/assets/paymentstyle.css" />
    <head>
      <title>API Example Code</title>
      <meta http-equiv="Content-Type" content="text/html, charset=iso-8859-1">
    </head>
    <body>
    <br/>
   
    <center><h3>Process ACS Result Page</h3></center><br/><br/>

  <table width="60%" align="center" cellpadding="5" border="0">
		
	<form action="http://localhost/MPGS/Pay_3DS_PRD/payment.php" method="post">

        <table width="60%" align="center" cellpadding="5" border="0">

             <tr class="shade">
                 <td align="right" width="50%"><strong>3DSecure.acsEci </strong></td>
                 <td width="50%"><input type="text" readonly="readonly" name="3DSecure[acsEci]" value="<?=$acsEci?>" size="30" maxlength="80" /></td>
             </tr>
			 
             <tr class="shade">
                 <td align="right" width="50%"><strong>3DSecure.summaryStatus </strong></td>
                 <td width="50%"><input type="text" name="3DSecure[summaryStatus]" value="<?=$summaryStatus?>" size="30" maxlength="80"/></td>
             </tr>
			 
             <tr class="shade">
                 <td align="right" width="50%"><strong>3DSecure.xid </strong></td>
                 <td width="50%"><input type="text" name="3DSecure[xid]" value="<?=$xid?>" size="30" maxlength="80"/></td>
             </tr>
			 
             <tr class="shade">
                 <td align="right" width="50%"><strong>3DSecureId</strong></td>
                 <td width="50%"><input type="text" name="3DSecureId" value="<?=$SecureId?>" size="30" maxlength="80"/></td>
             </tr>
			 
             <tr class="shade">
                 <td align="right" width="50%"><strong>merchant </strong></td>
                 <td width="50%"><input type="text" name="merchant" value="<?=$merchant?>" size="30" maxlength="80"/></td>
             </tr>
			 
             <tr class="shade">
                 <td align="right" width="50%"><strong>response.3DSecure.gatewayCode </strong></td>
                 <td width="50%"><input type="text" name="response[3DSecure][gatewayCode]" value="<?=$gatewayCode?>" size="30" maxlength="80"/></td>
             </tr>

    <tr class="shade">
      <td  colspan="2"><center><p>3DSecure.authenticationRedirect.simple.htmlBodyContent</p></center>
      </td>
    </tr>
     <tr class="shade">
       <td  colspan="2"><center>
          <textarea rows="40" cols="118" name="3DSecure[authenticationRedirect][simple][htmlBodyContent]" id="3DSecure[authenticationRedirect][simple][htmlBodyContent]"><?=$htmlBodyContent?></textarea></center>
      </td>
    </tr>
	

             <tr>
                 <td colspan="2"><center><input type="submit" name="submit" value="Perform Payment"/></center></td>
             </tr>

        </table>


        </form>
	
    <script type="text/javascript" src="http://localhost/MPGS/Pay_3DS_PRD/assets/json2.js"></script>
    <script type="text/javascript" src="http://localhost/MPGS/Pay_3DS_PRD/assets/jsonformatter.js"></script>
    <script type="text/javascript" src="http://localhost/MPGS/Pay_3DS_PRD/assets/jquery-1.3.2.js"></script>

    <script>
      var orginalJSON = $("#outContent").val();
      function FormatTextarea() {
        var sJSON = $("#outContent").val();
        var oJSON = JSON.parse(sJSON);
        sJSON = FormatJSON(oJSON);
        $("#outContent").val(sJSON);
      }
      FormatTextarea();

      function displayRawJSON() {
        $("#outContent").val(orginalJSON);
      }
    </script>

  </table>


  <br/><br/>
   </body>
</html>