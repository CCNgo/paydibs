<?php
session_start();
$errorMessage = "";
$errorCode = "";
$gatewayCode = "";
$result = "";
$xid = "";
$summaryStatus = "";
$gatewayCode = "";

$tmpArray = array();


$merchantId = $_SESSION['post-data']['apiMerchant'];
$password = $_SESSION['post-data']['apiPassword'];
$apiUsername = $_SESSION['post-data']['apiUsername'];
$pares = $_POST["PaRes"];
$secureId = $_SESSION['post-data']['3DSecureId'];
	

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <link rel="stylesheet" type="text/css" href="assets/paymentstyle.css" />
    <head>
      <title>API Example Code</title>
      <meta http-equiv="Content-Type" content="text/html, charset=iso-8859-1">
    </head>
    <body>
    <br/>
   
    <center><h3>Authenticate Page</h3></center><br/><br/>

  <table width="60%" align="center" cellpadding="5" border="0">
		
	<form action="processPares.php" method="post">

        <table width="60%" align="center" cellpadding="5" border="0">

             <tr class="shade">
                 <td align="right" width="50%"><strong>apiOperation </strong></td>
                 <td width="50%"><input type="text" readonly="readonly" name="apiOperation" value="PROCESS_ACS_RESULT" size="30" maxlength="80" /></td>
             </tr>
			 
             <tr class="shade">
                 <td align="right" width="50%"><strong>3DSecureId </strong></td>
                 <td width="50%"><input type="text" name="3DSecureId" value="<?=$secureId?>" size="30" maxlength="80"/></td>
             </tr>
			 
             <tr class="shade">
                 <td align="right" width="50%"><strong>Merchant ID </strong></td>
                 <td width="50%"><input type="text" name="apiMerchant" value="<?=$merchantId?>" size="30" maxlength="80"/></td>
             </tr>
			 
             <tr class="shade">
                 <td align="right" width="50%"><strong>API Username </strong></td>
                 <td width="50%"><input type="text" name="apiUsername" value="<?=$apiUsername?>" size="30" maxlength="80"/></td>
             </tr>
			 
             <tr class="shade">
                 <td align="right" width="50%"><strong>API Password </strong></td>
                 <td width="50%"><input type="text" name="apiPassword" value="<?=$password?>" size="30" maxlength="80"/></td>
             </tr>
			

    <tr class="shade">
      <td  colspan="2"><center><p>The display of the below response is intended to be for this example only. In your integration, you should parse this 		response to extract and use the response fields required.</p></center>
      </td>
    </tr>
     <tr class="shade">
       <td  colspan="2"><center>
          <textarea rows="40" cols="118" name="3DSecure[paRes]" id="3DSecure[paRes]"><?=$pares?></textarea></center>
      </td>
    </tr>
	

             <tr>
                 <td colspan="2"><center><input type="submit" name="submit" value="Process ACS"/></center></td>
             </tr>

        </table>


        </form>
		
    
         

  </table>


  <br/><br/>
   </body>
</html>