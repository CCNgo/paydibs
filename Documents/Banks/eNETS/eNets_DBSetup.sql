USE [OB]
GO
--Channel
INSERT [dbo].[Channel] ([IPAddress], [PortNumber], [QueryIPAddress], [QueryPortNumber], 
[CancelIPAddress], [CancelPortNumber], [RevIPAddress], [RevPortNumber], [AckIPAddress], [AckPortNumber], 
[ReturnIPAddresses], [ReturnURL], [ReturnURL2], [TimeOut], [DBConnStr], [ServerCertName], [CfgFile], 
[Protocol], [Desc], [RecStatus]) 
VALUES (N'https://www2.enets.sg/GW2/TxnReqListenerToHost', 443, N'https://api.nets.com.sg/GW2/TxnQuery', 443, 
N'', 443, N'', 0, N'', 0, 
N'', N'https://payment.paydibs.com/PPG/respenets.aspx', N'https://payment.paydibs.com/PPG/respenets_s2s.aspx', 900, N'', N'', N'templates\eNets.txt', 
N'TLS12', N'eNETS', 1)


--TxnStatusAction
INSERT [dbo].[TxnStatusAction] ([TxnType], [TxnField], [TxnStatus], [Action], [Desc], [Priority], [RecStatus]) VALUES (N'PAY;QUERY', NULL, N'00000', 1, N'Success', 1, 1)
INSERT [dbo].[TxnStatusAction] ([TxnType], [TxnField], [TxnStatus], [Action], [Desc], [Priority], [RecStatus]) VALUES (N'PAY;QUERY', NULL, N'!00000', 2, N'Failed', 1, 1)


--Host
INSERT INTO [dbo].[Host] ([HostName], [HostCode], [SvcTypeID], [PymtMethod], [SignBy], [PaymentTemplate], [SecondEntryTemplate], [MesgTemplate], 
[NeedReplyAcknowledgement], [NeedRedirectOTP], [Require2ndEntry], [AllowQuery], [AllowReversal], [MaxRefund], [ChannelID], [Timeout], 
[OTCRevTimeout], [OTCGenMethod], [TxnStatusActionID], [HostReplyMethod], [OSPymtCode], [GatewayTxnIDFormat], [MID], [MerchantPassword], 
[ReturnKey], [HashMethod], [SecretKey], [InitVector], [LogoPath], [RepStartTime], [QueryFlag], [SettlePeriod], [SettleTime], [HostMDR], 
[LogRes], [DateActivated], [DateDeactivated], [RecStatus], [Desc], [FixedCurrency], [IsExponent], [FloorAmt], [CeilingAmt], [HostDesc])
VALUES ('ENETS', NULL, '2', 'OB', '1', 'RTTemplates\redirect_post.html', '', '', 
'0', '0', '0', '1', '0', '-1', '??', '500', 
'0', '0', '??', '1', '', '7', NULL, '', 
'', 'SHA2B64', '', '', 'enets.png', '00:00', '1', '1', NULL, '0.00', 
'1', '2019-02-13 11:11:23.143', '2099-12-31 00:00:00.000', '1', 'eNETS', NULL, '0', '0.00', '0.00', 'eNETS')

USE [PG]
GO
--Terminals_PreSetCredential
INSERT [dbo].[Terminals_PreSetCredential] ([ID], [HostID], [PaymentMethod], [Bank], [FieldName], [DBFieldName], [Encrypt], [DefaultValue], 
[Hex]) VALUES (N'OB', N'eNets', N'MID', N'MID', N'0', N'??', 0)
INSERT [dbo].[Terminals_PreSetCredential] ([ID], [HostID], [PaymentMethod], [Bank], [FieldName], [DBFieldName], [Encrypt], [DefaultValue], 
[Hex]) VALUES (N'OB', N'eNets', N'SendKey', N'Password', N'1', N'??', 0)
INSERT [dbo].[Terminals_PreSetCredential] ([ID], [HostID], [PaymentMethod], [Bank], [FieldName], [DBFieldName], [Encrypt], [DefaultValue], 
[Hex]) VALUES (N'OB', N'eNets', N'ReturnKey', N'ReturnKey', N'1', N'??', 0)
INSERT [dbo].[Terminals_PreSetCredential] ([ID], [HostID], [PaymentMethod], [Bank], [FieldName], [DBFieldName], [Encrypt], [DefaultValue], 
[Hex]) VALUES (N'OB', N'eNets', N'API Key', N'AcquirerID', N'0', N'??', 0)