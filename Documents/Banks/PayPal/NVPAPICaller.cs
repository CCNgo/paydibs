﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Collections.Specialized;
using System.Net;
using System.IO;
using System.Text;
using Kenji.Apps;
using PayPal;
using MfaceV2.WEB.DAL;

    public class NVPAPICaller
    {
        DAL dal = new DAL();

        private string pendpointurl = "https://api-3t.paypal.com/nvp";
        private const string CVV2 = "CVV2";

        private string returnURL = "http://" + HttpContext.Current.Request.Url.Authority + "/paypales/Success.aspx";
        private string cancelURL = "http://" + HttpContext.Current.Request.Url.Authority + "/paypales/cancel.aspx";
        //private string IPNURL    = "http://localhost:11911/paypales/PayPalIPN.ashx";

        private const string SIGNATURE = "SIGNATURE";
        private const string PWD       = "PWD";
        private const string ACCT      = "ACCT";


    #region Important Setting

    //private const bool bSandbox = true;
    //public string APIUsername   = "paypal_api1.gameview.asia";
    //private string APIPassword  = "9D6LS65UMW6UA5XJ";
    //private string APISignature = "Ah61m4wh3lfwj1Af47QHPZcdvCgaA3Od1bzsrV8AJbjaugdqNLXsd0Nb";
    //private string merchantID   = "H7CM9ZA9VQQDA";
    //private string clientId     = "ASJE0BA_WCmdYRx0kKvxjxZvg45uhul5Hkv5fmioZWy520mBDPBAPfILPxpb";
    //private string secret       = "EGJBahB7n4RIbG-V17PK2S1xTkEOQutXN3UK5Zk7w3t3Lxbjbv9bH5QlclLH";

    //private const bool bSandbox = false;
    //public string APIUsername   = "paypal_api1.gameview.asia";
    //private string APIPassword  = "YNVXBLWU6ELLUSUW";
    //private string APISignature = "A3QiMF.8yuTOffK5Q.juzhHRYM7BASSEVQIT0EFNd.WIj-.mHn5m1PIK";
    //private string merchantID   = "QP44V34NSN4KA";
    //private string clientId     = "AZHbdRCubnbcTljjw_7xYzlLqeatOE0qQw4HsrmxM9UF1bfXN2JfWvU8xdJO";
    //private string secret       = "ELdTjBCG8vsOBRbxykZ4wQibfqkQkXC5mcfBCHHJOQm2wD4rJrKtcpNaJUBR";

    private bool bSandbox = false;
    public string APIUsername = "";
    private string APIPassword = "";
    private string APISignature = "";
    private string merchantID = "";
    private string clientId = "";
    private string secret = "";

    #endregion

    public NVPAPICaller(bool PSandBox = false)
    {
        //returnURL = dal.tblGlobalVariable_SelectDetail("paypalReturnURL").Value;
        //cancelURL = dal.tblGlobalVariable_SelectDetail("paypalCancelURL").Value;
        //IPNURL = dal.tblGlobalVariable_SelectDetail("paypalIPNURL").Value;

        if (!PSandBox)
        {
            bSandbox = false;
            APIUsername = "paypal_api1.gameview.asia";
            APIPassword = "YNVXBLWU6ELLUSUW";
            APISignature = "A3QiMF.8yuTOffK5Q.juzhHRYM7BASSEVQIT0EFNd.WIj-.mHn5m1PIK";
            merchantID = "QP44V34NSN4KA";
            clientId = "AZHbdRCubnbcTljjw_7xYzlLqeatOE0qQw4HsrmxM9UF1bfXN2JfWvU8xdJO";
            secret = "ELdTjBCG8vsOBRbxykZ4wQibfqkQkXC5mcfBCHHJOQm2wD4rJrKtcpNaJUBR";
        }
        else
        {
            bSandbox = true;
            APIUsername = "paypal_api1.gameview.asia";
            APIPassword = "9D6LS65UMW6UA5XJ";
            APISignature = "Ah61m4wh3lfwj1Af47QHPZcdvCgaA3Od1bzsrV8AJbjaugdqNLXsd0Nb";
            merchantID = "H7CM9ZA9VQQDA";
            clientId = "ASJE0BA_WCmdYRx0kKvxjxZvg45uhul5Hkv5fmioZWy520mBDPBAPfILPxpb";
            secret = "EGJBahB7n4RIbG-V17PK2S1xTkEOQutXN3UK5Zk7w3t3Lxbjbv9bH5QlclLH";
        }
    }

    private string Subject = "";
        private string BNCode = "PP-ECWizard";

        //HttpWebRequest Timeout specified in milliseconds 
        private const int Timeout = 10000;
        private static readonly string[] SECURED_NVPS = new string[] { ACCT, CVV2, SIGNATURE, PWD };

        PostData pd = new PostData();

        /// <summary>
        /// Sets the API Credentials
        /// </summary>
        /// <param name="Userid"></param>
        /// <param name="Pwd"></param>
        /// <param name="Signature"></param>
        /// <returns></returns>
        public void SetCredentials(string Userid, string Pwd, string Signature)
        {
            APIUsername = Userid;
            APIPassword = Pwd;
            APISignature = Signature;
        }

        public bool ExpressCheckout(int PLogIDN, string name, string description, string price, string quantity, string currency, string orderID, ref string token, ref string retMsg)
        {

            string host = "www.paypal.com";
            if (bSandbox)
            {
                pendpointurl = "https://api-3t.sandbox.paypal.com/nvp";
                host = "www.sandbox.paypal.com";
            }

            NVPCodec encoder                            = new NVPCodec();
            encoder["METHOD"]                           = "SetExpressCheckout";
            encoder["RETURNURL"]                        = returnURL + "?orderid=" + orderID;
            encoder["CANCELURL"]                        = returnURL + "?orderid=" + orderID;
            encoder["NOSHIPPING"]                       = "1";
            encoder["ALLOWNOTE"]                        = "1";
            encoder["PAYMENTREQUEST_0_INVNUM"]          = orderID;
            encoder["PAYMENTREQUEST_0_CUSTOM"]          = orderID;
            encoder["L_BILLINGTYPE0"]                   = "MerchantInitiatedBillingSingleAgreement";
            encoder["L_BILLINGAGREEMENTDESCRIPTION0"]   = "billing agreement description kk";

            double dblQuantity = Convert.ToDouble(quantity);
            double dblPrice    = Convert.ToDouble(price);
            double totalPrice  = dblQuantity * dblPrice;

            encoder["L_PAYMENTREQUEST_0_NAME0"] = name;
            encoder["L_PAYMENTREQUEST_0_DESC0"] = description;
            encoder["L_PAYMENTREQUEST_0_AMT0"]  = price;
            encoder["L_PAYMENTREQUEST_0_QTY0"]  = quantity;

            encoder["PAYMENTREQUEST_0_AMT"]           = totalPrice.ToString();
            encoder["PAYMENTREQUEST_0_ITEMAMT"]       = totalPrice.ToString();
            encoder["PAYMENTREQUEST_0_PAYMENTACTION"] = "SALE";
            encoder["PAYMENTREQUEST_0_CURRENCYCODE"]  = currency;
            //encoder["PAYMENTREQUEST_0_NOTIFYURL"]     = IPNURL;

            string pStrrequestforNvp = encoder.Encode();
            string pStresponsenvp = HttpCall(pStrrequestforNvp);

            NVPCodec decoder = new NVPCodec();
            decoder.Decode(pStresponsenvp);

            string strAck = decoder["ACK"].ToLower();
            if (strAck != null && (strAck == "success" || strAck == "successwithwarning"))
            {
                token = decoder["TOKEN"];

                string ECURL = "https://" + host + "/cgi-bin/webscr?cmd=_express-checkout" + "&token=" + token + "&useraction=COMMIT";
                SetTransactionContext(token, PLogIDN);
                retMsg = ECURL;
                return true;
            }
            else
            {
                retMsg = "ErrorCode=" + decoder["L_ERRORCODE0"] + "&" +
                    "Desc=" + decoder["L_SHORTMESSAGE0"] + "&" +
                    "Desc2=" + decoder["L_LONGMESSAGE0"];

                return false;
            }
        }

        /// <summary>
        /// GetShippingDetails: The method that calls SetExpressCheckout API, invoked from the 
        /// Billing Page EC placement
        /// </summary>
        /// <param name="token"></param>
        /// <param ref name="retMsg"></param>
        /// <returns></returns>
        public bool GetDetails(string token, ref NVPCodec decoder, ref string retMsg)
        {

            if (bSandbox)
            {
                pendpointurl = "https://api-3t.sandbox.paypal.com/nvp";
            }

            NVPCodec encoder = new NVPCodec();
            encoder["METHOD"] = "GetExpressCheckoutDetails";
            encoder["TOKEN"] = token;

            string pStrrequestforNvp = encoder.Encode();
            string pStresponsenvp = HttpCall(pStrrequestforNvp);

            decoder = new NVPCodec();
            decoder.Decode(pStresponsenvp);

            string strAck = decoder["ACK"].ToLower();
            if (strAck != null && (strAck == "success" || strAck == "successwithwarning"))
            {
                return true;
            }
            else
            {
                retMsg = "ErrorCode=" + decoder["L_ERRORCODE0"] + "&" +
                    "Desc=" + decoder["L_SHORTMESSAGE0"] + "&" +
                    "Desc2=" + decoder["L_LONGMESSAGE0"];

                return false;
            }
        }

        /// <summary>
        /// ConfirmPayment: The method that calls SetExpressCheckout API, invoked from the 
        /// Billing Page EC placement
        /// </summary>
        /// <param name="token"></param>
        /// <param ref name="retMsg"></param>
        /// <returns></returns>
        public bool ConfirmPayment(string finalPaymentAmount, string token, string PayerId, string currency, ref NVPCodec decoder, ref string retMsg)
        {
            if (bSandbox)
            {
                pendpointurl = "https://api-3t.sandbox.paypal.com/nvp";
            }

            NVPCodec encoder = new NVPCodec();
            encoder["METHOD"] = "DoExpressCheckoutPayment";
            encoder["TOKEN"] = token;
            encoder["PAYMENTACTION"] = "Sale";
            encoder["PAYERID"] = PayerId;
            encoder["AMT"] = finalPaymentAmount;
            encoder["CURRENCYCODE"] = currency;

            string pStrrequestforNvp = encoder.Encode();
            string pStresponsenvp = HttpCall(pStrrequestforNvp);

            decoder = new NVPCodec();
            decoder.Decode(pStresponsenvp);

            string strAck = decoder["ACK"].ToLower();
            if (strAck != null && (strAck == "success" || strAck == "successwithwarning"))
            {
                return true;
            }
            else
            {
                retMsg = "ErrorCode=" + decoder["L_ERRORCODE0"] + "&" +
                    "Desc=" + decoder["L_SHORTMESSAGE0"] + "&" +
                    "Desc2=" + decoder["L_LONGMESSAGE0"];

                return false;
            }
        }

        /// <summary>
        /// HttpCall: The main method that is used for all API calls
        /// </summary>
        /// <param name="NvpRequest"></param>
        /// <returns></returns>
        public string HttpCall(string NvpRequest) //CallNvpServer
        {
            string url = pendpointurl;

            //To Add the credentials from the profile
            string strPost = NvpRequest + "&" + buildCredentialsNVPString();
            strPost = strPost + "&BUTTONSOURCE=" + HttpUtility.UrlEncode(BNCode);

            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(url);
            objRequest.Timeout = Timeout;
            objRequest.Method = "POST";
            objRequest.ContentLength = strPost.Length;
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
            try
            {
                using (StreamWriter myWriter = new StreamWriter(objRequest.GetRequestStream()))
                {
                    myWriter.Write(strPost);
                }
            }
            catch (Exception e)
            {
              //do some error handling
            }

            //Retrieve the Response returned from the NVP API call to PayPal
            HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
            string result;
            using (StreamReader sr = new StreamReader(objResponse.GetResponseStream()))
            {
                result = sr.ReadToEnd();
            }

            return result;
        }

        /// <summary>
        /// Credentials added to the NVP string
        /// </summary>
        /// <param name="profile"></param>
        /// <returns></returns>
        private string buildCredentialsNVPString()
        {
            NVPCodec codec = new NVPCodec();

            if (!IsEmpty(APIUsername))
                codec["USER"] = APIUsername;

            if (!IsEmpty(APIPassword))
                codec["PWD"] = APIPassword;

            if (!IsEmpty(APISignature))
                codec["SIGNATURE"] = APISignature;

            if (!IsEmpty(Subject))
                codec["SUBJECT"] = Subject;

            codec["VERSION"] = "84.0";

            return codec.Encode();
        }

        /// <summary>
        /// Returns if a string is empty or null
        /// </summary>
        /// <param name="s">the string</param>
        /// <returns>true if the string is not null and is not empty or just whitespace</returns>
        public static bool IsEmpty(string s)
        {
            return s == null || s.Trim() == string.Empty;
        }

        public bool IPN_Verify(string PRequestForm, out string PIPNStatus)
        {
            PIPNStatus = "";
            string strFormValues = PRequestForm;
            string strNewValue;
            string strResponse;

            string host = "www.paypal.com";
            if (bSandbox)
            {
                pendpointurl = "https://api-3t.sandbox.paypal.com/nvp";
                host = "www.sandbox.paypal.com";
            }

            string serverURL   = "https://" + host + "/cgi-bin/webscr";
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(serverURL);
            req.Method         = "POST";
            req.ContentType    = "application/x-www-form-urlencoded";
            req.UserAgent      = "my host";
            strNewValue        = strFormValues + "&cmd=_notify-validate";
            req.ContentLength  = strNewValue.Length;
            StreamWriter stOut = new StreamWriter(req.GetRequestStream(), System.Text.Encoding.ASCII);
            stOut.Write(strNewValue);
            stOut.Close();
            StreamReader stIn = new StreamReader(req.GetResponse().GetResponseStream());
            strResponse       = stIn.ReadToEnd(); stIn.Close();
            PIPNStatus        = strResponse;
            if (strResponse   == "VERIFIED") return true;
            else return false;
        }
        private Dictionary<string, string> GetConfig()
        {
            Dictionary<string, string> configMap = new Dictionary<string, string>();

            // Endpoints are varied depending on whether sandbox OR live is chosen for mode

            string mode = "";

            if (!bSandbox)
            {
                mode = "live";
            }
            else
            {
                mode = "sandbox";
            }

            configMap.Add("mode", mode);

            //configMap.Add("mode", "sandbox");

            // These values are defaulted in SDK. If you want to override default values, uncomment it and add your value
            // configMap.Add("connectionTimeout", "360000");
            // configMap.Add("requestRetries", "1");
            return configMap;
        }
        private string GetAccessToken()
        {
            // ###AccessToken
            // Retrieve the access token from
            // OAuthTokenCredential by passing in
            // ClientID and ClientSecret
            // It is not mandatory to generate Access Token on a per call basis.
            // Typically the access token can be generated once and
            // reused within the expiry window                
            string accessToken = new OAuthTokenCredential(clientId, secret, GetConfig()).GetAccessToken();
            return accessToken;
        }
        public bool SetTransactionContext(string PToken, int PTranxLogIDN)
        {
            tblMember member           = null;
            tblMobile_PayPalLog paypal = dal.tblMobile_PayPalLog_SelectDetail(PTranxLogIDN);
            if (paypal != null) member = dal.tblMember_SelectDetail_IDN(paypal.MemberIDN);

            string host = "https://api.paypal.com/v1/risk/transaction-contexts/#payerID/#trackingID";
            if (bSandbox)
            {
                host = "https://api.sandbox.paypal.com/v1/risk/transaction-contexts/#payerID/#trackingID";
            }

            string Email     = "sender@mail.com";
            string Phone     = "402-935-2050";
            string FirstName = "First Name";
            string LastName  = "Last Name";

            if (string.IsNullOrEmpty(member.Email)) { Email = paypal.payer_email; }
            else Email                                      = member.Email;
            if (!string.IsNullOrEmpty(member.ContactNumber)) Phone = member.ContactNumber;

            DataTable DTTranx                             = dal.tblMobile_PayPalLog_SelectSuccessTransaction(member.IDN);
            string LastGoodTranxDate                      = DateTime.Now.ToString("O");
            string TranxTotalCount                        = "0";
            if (DTTranx.Rows.Count > 0) {
                LastGoodTranxDate = DTTranx.Rows[0]["payment_date"].ToString();
                TranxTotalCount   = DTTranx.Rows.Count.ToString();
                FirstName         = DTTranx.Rows[0]["first_name"].ToString();
                LastName          = DTTranx.Rows[0]["last_name"].ToString();
                Email             = DTTranx.Rows[0]["payer_email"].ToString();
            }

            if (string.IsNullOrEmpty(member.Email)) { Email = member.Email; }
            if (!string.IsNullOrEmpty(member.ContactNumber)) Phone = member.ContactNumber;

            DateTime StartDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 00, 00, 00).AddMonths(-3);
            DateTime EndDate   = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);

            string Last3MonthTranxCount = "0";
            DataTable DTTranxLast3Month = dal.tblMobile_PayPalLog_SelectSuccessTransaction(member.IDN, StartDate, EndDate);
            Last3MonthTranxCount        = DTTranxLast3Month.Rows.Count.ToString();

            string chargeBackThreeMonth = "0";
            string chargeBackTotalCount = "0";
            string totalIPCount         = "0";
            if (dal.tblMobile_PayPalLog_SelectIPTotalCount(member.IDN) > 0)
            {
                totalIPCount = dal.tblMobile_PayPalLog_SelectIPTotalCount(member.IDN).ToString();
            }
            string IP                   = paypal.IPAddress.Replace("::1","127.0.0.1");

            string LastDeviceID = "deviceId";

            //DataTable DTDeviceSeen = dal.tblLog_MobileGameLoginLog_SelectTotalDeviceUsed(member.LoginID);
            //if (DTDeviceSeen.Rows.Count > 0)
            //{
            //    LastDeviceID = DTDeviceSeen.Rows[0]["IMEI"].ToString();
            //}
            //string TotalDeviceSeen = DTDeviceSeen.Rows.Count.ToString();
            string TotalDeviceSeen   = "0";

            DataTable DT = dal.tblLog_TopupLog_SelectAll(member.IDN);

            var AA = "{ " +
             //" \"sender_account\": \"" + paypal.invoice + "\"," +
            " \"sender_account\": { " +
            " \"partner_account\": { " +
            " \"email\": \""+Email+"\", " +
            " \"phone\": \"" + Phone + "\", " +
            " \"first_name\": \"" + FirstName + "\", " +
            " \"last_name\": \""+LastName+"\", " +
            //" \"mailing_address\": { " +
            //" \"line1\": \"2211 North 1st Street\", " +
            //" \"line2\": \"Address Line2\", " +
            //" \"city\": \"San Jose\", " +
            //" \"state\": \"CA\", " +
            //" \"postal_code\": \"95131\", " +
            //" \"country_code\": \"US\" " +
            //" }, " +
            " \"create_date\": \"" + member.CreateDate.ToString("O") + "\", " +
            " \"last_good_transaction_date\": \"2012-12-09T19:14:55.277-08:00\", " +
            " \"transaction_count_total\": \""+TranxTotalCount+"\", " +
            " \"chargeback_count_total\": \""+chargeBackThreeMonth+"\", " +
            " \"transaction_count_three_months\": \"" + Last3MonthTranxCount + "\", " +
            " \"chargeback_count_three_months\": \"" + chargeBackTotalCount + "\" " +
            "       } " +
            " }, " +
            " \"device\": { " +
            " \"id\": \"" + LastDeviceID + "\", " +
            " \"seen_count\": \"" + TotalDeviceSeen + "\" " +
            " }, " +
            " \"ip_address_info\": { " +
            " \"ip_address\": \"" + IP + "\", " +
            " \"seen_count\": \"" + totalIPCount + "\" " +
            " }, " +
            " \"additional_data\": [ " +
            " { " +
            " \"key\": \"WasBlackListed\", " +
            " \"value\": \"no\" " +
            " } " +
            " ] " +
            " }";


            //$sender_account = new Account(); $sender_account->setPartnerAccount($partner_account); 
            string authToken = GetAccessToken();
            dal.tblMobile_payPaylLog_UpdateAuthToken(paypal.invoice, authToken);

            ASCIIEncoding encoding = new ASCIIEncoding();
            string stringData      = AA;
            byte[] data            = encoding.GetBytes(stringData);

            string serverURL      = host.Replace("#trackingID", PToken).Replace("#payerID", merchantID);

            dal.tblMobile_payPaylLog_UpdateStcReq(paypal.invoice, serverURL + " ; " + stringData);

            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(serverURL);
            req.Method            = "PUT";
            req.ContentType       = "application/json";
            req.ContentLength     = data.Length;
            req.Headers.Add("Authorization", authToken);

            Stream newStream = req.GetRequestStream();
            newStream.Write(data, 0, data.Length);
            newStream.Close();
            
            HttpWebResponse response = (HttpWebResponse)req.GetResponse();
            string returnString = response.StatusCode.ToString();

            dal.tblMobile_payPaylLog_UpdateAuthStcRes(paypal.invoice, returnString);

            return false;
        }
    }

    public sealed class NVPCodec : NameValueCollection
    {
        private const string AMPERSAND = "&";
        private const string EQUALS = "=";
        private static readonly char[] AMPERSAND_CHAR_ARRAY = AMPERSAND.ToCharArray();
        private static readonly char[] EQUALS_CHAR_ARRAY = EQUALS.ToCharArray();

        /// <summary>
        /// Returns the built NVP string of all name/value pairs in the Hashtable
        /// </summary>
        /// <returns></returns>
        public string Encode()
        {
            StringBuilder sb = new StringBuilder();
            bool firstPair = true;
            foreach (string kv in AllKeys)
            {
                string name = HttpUtility.UrlEncode(kv);
                string value = HttpUtility.UrlEncode(this[kv]);

                if (!firstPair)
                {
                    sb.Append(AMPERSAND);
                }
                sb.Append(name).Append(EQUALS).Append(value);
                firstPair = false;
            }
            return sb.ToString();
        }

        /// <summary>
        /// Decoding the string
        /// </summary>
        /// <param name="nvpstring"></param>
        public void Decode(string nvpstring)
        {
            Clear();
            foreach (string nvp in nvpstring.Split(AMPERSAND_CHAR_ARRAY))
            {
                string[] tokens = nvp.Split(EQUALS_CHAR_ARRAY);
                if (tokens.Length >= 2)
                {
                    string name = HttpUtility.UrlDecode(tokens[0]);
                    string value = HttpUtility.UrlDecode(tokens[1]);
                    Add(name, value);
                }
            }
        }


        #region Array methods
        public void Add(string name, string value, int index)
        {
            this.Add(GetArrayName(index, name), value);
        }

        public void Remove(string arrayName, int index)
        {
            this.Remove(GetArrayName(index, arrayName));
        }

        /// <summary>
        /// 
        /// </summary>
        public string this[string name, int index]
        {
            get
            {
                return this[GetArrayName(index, name)];
            }
            set
            {
                this[GetArrayName(index, name)] = value;
            }
        }

        private static string GetArrayName(int index, string name)
        {
            if (index < 0)
            {
                throw new ArgumentOutOfRangeException("index", "index can not be negative : " + index);
            }
            return name + index;
        }
        #endregion
    }


    public class sender_account
    {
        public partner_account partner_account { get; set; }

    }

    public class partner_account
    {
        public string email { get; set; }
        public string phone { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public mailing_address mailing_address { get; set; }
        public DateTime create_date { get; set; }
        public DateTime last_good_transaction_date { get; set; }
        public DateTime transaction_count_total { get; set; }
        public DateTime chargeback_count_total { get; set; }
        public DateTime transaction_count_three_months { get; set; }
        public DateTime chargeback_count_three_months { get; set; }
    }
    public class mailing_address {
        public string line1 { get; set; }
        public string line2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string postal_code { get; set; }
        public string country_code { get; set; }
    }

    public class device {
        public string id { get; set; }
        public string seen_count { get; set; }
    }

    public class ip_address_info {
        public string ip_address { get; set; }
        public string seen_count { get; set; }
    }

    public class additional_data {
        public string key { get; set; }
        public string value { get; set; }
    }

    


//$partner_account = new PartnerAccount();
//$partner_account->setEmail("sender@mail.com");
//$partner_account->setPhone("402-935-2050");
//$partner_account->setFirstName("Michael");
//$partner_account->setLastName("Woods");
//$partner_account->setCreateDate("2012-12-09T19:14:55.277-08:00");
//$partner_account->setLastGoodTransactionDate("2012-12-09T19:14:55.277-08:00");
//$partner_account->setChargebackCountThreeMonths("1");
//$partner_account->setChargebackCountTotal("1");
//$partner_account->setTransactionCountThreeMonths("1");
//$partner_account->setTransactionCountTotal("3");

//$mailing_address = new Address();
//$mailing_address->setLine1("2211 North 1st Street");
//$mailing_address->setLine2("Address Line2");
//$mailing_address->setCity("San Jose");
//$mailing_address->setState("CA");
//$mailing_address->setPostalCode("95131");
//$mailing_address->setCountryCode("US");
//$partner_account->setMailingAddress($mailing_address);

//$sender_account = new Account();
//$sender_account->setPartnerAccount($partner_account);

//$device = new Device();
//$device->setId("device_id");
//$device->setSeenCount("3");

//$ip_address_info = new IpAddressInfo();
//$ip_address_info->setIpAddress("127.0.0.1");
//$ip_address_info->setSeenCount("2");

//$additional_data = new KeyValuePair();
//$additional_data->setKey("WasBlackListed");
//$additional_data->setValue("no");

//$context->setSenderAccount($sender_account);
//$context->setDevice($device);
//$context->setIpAddressInfo($ip_address_info);
//$context->setAdditionalData(array($additional_data));
