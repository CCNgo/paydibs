CREATE PROCEDURE [OB].[dbo].[spGetAllPendingTransactionsGatewayTxnID]
(
 @IssuingBank AS VARCHAR(30),
	@DurationMinute AS INT = 1

)
 AS

SELECT GatewayTxnID FROM [OB].[dbo].[TB_PayReq] WITH (NOLOCK) WHERE IssuingBank = @IssuingBank AND DateModified BETWEEN GETDATE() and DATEADD(MINUTE,-@DurationMinute,GETDATE())


