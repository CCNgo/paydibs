USE [PGAdmin]
GO

/****** Object:  StoredProcedure [dbo].[spRptGetAllMissingTransactionInTB_TxnRateForOB]    Script Date: 29/1/2021 3:38:56 PM ******/
DROP PROCEDURE [dbo].[spRptGetAllMissingTransactionInTB_TxnRateForOB]
GO

/****** Object:  StoredProcedure [dbo].[spRptGetAllMissingTransactionInTB_TxnRateForOB]    Script Date: 29/1/2021 3:38:56 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spRptGetAllMissingTransactionInTB_TxnRateForOB] 
	@PSince AS DateTime,
	@PTo AS DateTime
AS
BEGIN
	SELECT R.PKID, R.DateCreated, R.TxnType, R.MerchantID, R.MerchantTxnID, R.OrderNumber, R.GatewayTxnID, R.BankRefNo, R.HostID, 
	R.IssuingBank, R.CurrencyCode, R.TxnAmt, FX.FXCurrencyCode, FX.FXTxnAmt, R.TxnStatus, R.MerchantTxnStatus, R.TxnDay, R.DateCompleted
	FROM OB.dbo.TB_PayRes R WITH (NOLOCK)
	LEFT JOIN OB.dbo.TB_PayFX FX WITH (NOLOCK) ON FX.GatewayTxnID = R.GatewayTxnID

	WHERE R.DateCompleted between @PSince and @PTo-- R.PKID > @pkid 
	and NOT EXISTS ( SELECT 1 FROM OB.dbo.TB_TxnRate WITH (NOLOCK) WHERE ResID = R.PKID )
END
GO


