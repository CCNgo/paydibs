USE [PGAdmin]
GO

/****** Object:  StoredProcedure [dbo].[spRepTBLateRes_By_BankRespCode]    Script Date: 29/1/2021 7:43:14 PM ******/
DROP PROCEDURE [dbo].[spRepTBLateRes_By_BankRespCode]
GO

/****** Object:  StoredProcedure [dbo].[spRepTBLateRes_By_BankRespCode]    Script Date: 29/1/2021 7:43:14 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO










CREATE PROCEDURE [dbo].[spRepTBLateRes_By_BankRespCode] 
	@PGatewayTxnID AS VARCHAR(30),
	@PMerchantID AS VARCHAR(30),
	@PSince AS VARCHAR(30),
	@PTo AS VARCHAR(30),
	@PFPXbankRespCode AS VARCHAR(2),
	@PDragonPayRespCode AS VARCHAR(1),
	@PCurrentStatus AS VARCHAR(1)
	
AS
BEGIN
	DECLARE @sz_Select AS NVARCHAR(MAX)
	
	SET @sz_Select = 'SELECT A.GatewayTxnID, A.MerchantID, A.BankRespCode, A.BankRefNo, A.LastUpdated, C.RespMesg, '
	SET @sz_Select = @sz_Select + '(SELECT B.TxnDescription FROM OB..CL_TxnStatus B WHERE C.TxnStatus = B.TxnStatus) AS CurrentStatus '
	SET @sz_Select = @sz_Select + 'FROM OB..TB_LateRes(NOLOCK) A '
	SET @sz_Select = @sz_Select + 'LEFT JOIN OB..TB_PayRes C on A.GatewayTxnID = C.GatewayTxnID '

	
	SET @sz_Select = @sz_Select + 'WHERE A.LastUpdated BETWEEN ''' + @PSince + ''' AND ''' + @PTo + ''' '
	
	IF (@PMerchantID <> '')
		SET @sz_Select = @sz_Select + 'AND A.MerchantID = ''' +@PMerchantID+ ''' '

	IF (@PGatewayTxnID <> '')
		SET @sz_Select = @sz_Select + 'AND A.GatewayTxnID = ''' + @PGatewayTxnID + ''' '

	SET @sz_Select = @sz_Select + 'AND (A.BankRespCode  = ''' + @PFPXbankRespCode + ''' '

	SET @sz_Select = @sz_Select + 'OR A.BankRespCode  = ''' + @PDragonPayRespCode + ''' ) '

	--Not Success
	SET @sz_Select = @sz_Select + 'AND C.TxnStatus  <> ''' + @PCurrentStatus + ''' '

	SET @sz_Select = @sz_Select + 'ORDER BY LastUpdated DESC'

	EXEC (@sz_Select);
	print (@sz_Select);
END
GO


