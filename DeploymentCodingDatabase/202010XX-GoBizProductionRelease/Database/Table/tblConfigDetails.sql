USE [PGAdmin]
GO
/****** Object:  Table [dbo].[tblConfigDetails]    Script Date: 10/02/2012 10:57:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [tblConfigDetails](
	[fk_ID] [uniqueidentifier] NULL,
	[KeyName] [varchar](50) NULL,
	[KeyValue] [varchar](50) NULL,
	[Status] [smallint] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDateTime] [datetime] NULL,
	[UpdateBy] [varchar](50) NULL,
	[UpdatedDateTime] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
