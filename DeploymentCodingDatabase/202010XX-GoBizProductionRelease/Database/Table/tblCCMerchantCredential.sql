USE [PGAdmin]
GO
/****** Object:  Table [dbo].[tblCCMerchantCredential]  ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [tblCCMerchantCredential](
	[HostID] [int] NOT NULL,
	[PaydibsMID] [varchar](30) NOT NULL,
	[MerchantID] [varchar](15) NOT NULL,
	[TerminalID] [varchar](8) NOT NULL,
	[HASHKEY] [varchar](64) NOT NULL,
	[ECI][char](1) NOT NULL,
	[Status] [int] Not NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTime] [datetime] NOT NULL,
	[UpdateBy] [varchar](50) NULL,
	[UpdatedDateTime] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO

IF NOT EXISTS (SELECT Top 1 1 FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('tblCCMerchantCredential') 
AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS 
WHERE [name] = 'ECI' AND [object_id] = OBJECT_ID('tblCCMerchantCredential')))
BEGIN
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 - Non E - Commerce Merchant, 1 - E - Commerce(3D Secure), 2 - E - Commerce(Non 3D Secure / MOTO)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tblCCMerchantCredential', @level2type=N'COLUMN',@level2name=N'ECI'
END
GO

IF NOT EXISTS (SELECT Top 1 1 FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('tblCCMerchantCredential') 
AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS 
WHERE [name] = 'Status' AND [object_id] = OBJECT_ID('tblCCMerchantCredential')))
BEGIN
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 - Inactive, 1 - Active' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tblCCMerchantCredential', @level2type=N'COLUMN',@level2name=N'Status'
END
GO