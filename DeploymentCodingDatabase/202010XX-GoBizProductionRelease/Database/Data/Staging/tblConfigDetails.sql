USE [PGAdmin]

DECLARE @FKID uniqueidentifier 
SELECT @FKID = ID FROM [dbo].[tblConfig] WITH (NOLOCK)  WHERE Name = 'Paydibs.PaymentGatewayAPI'

IF NOT Exists (select fk_ID FROM [dbo].[tblConfigDetails] WITH (NOLOCK) WHERE fk_ID = @FKID AND KeyName = 'Paydibs.PaymentGatewayAPI.PayMaster#MID')
BEGIN 
	INSERT INTO [dbo].[tblConfigDetails] 
	(fk_ID,KeyName,KeyValue,Status,CreatedBy,CreatedDateTime) 
	VALUES (@FKID,'Paydibs.PaymentGatewayAPI.PayMaster#MID', '000000000010521', 1, 'xevos', GETDATE())
END

IF NOT Exists (select fk_ID FROM [dbo].[tblConfigDetails] WITH (NOLOCK) WHERE fk_ID = @FKID AND KeyName = 'Paydibs.PaymentGatewayAPI.PayMaster#TID')
BEGIN 
	INSERT INTO [dbo].[tblConfigDetails] 
	(fk_ID,KeyName,KeyValue,Status,CreatedBy,CreatedDateTime) 
	VALUES (@FKID,'Paydibs.PaymentGatewayAPI.PayMaster#TID', '10003647', 1, 'xevos', GETDATE())
END

IF NOT Exists (select fk_ID FROM [dbo].[tblConfigDetails] WITH (NOLOCK) WHERE fk_ID = @FKID AND KeyName = 'Paydibs.PaymentGatewayAPI.PayMaster#RURL_UPPPS')
BEGIN 
	INSERT INTO [dbo].[tblConfigDetails] 
	(fk_ID,KeyName,KeyValue,Status,CreatedBy,CreatedDateTime) 
	VALUES (@FKID,'Paydibs.PaymentGatewayAPI.PayMaster#RURL_UPPPS', 'http://localhost:56019/order/sales/paymastersales?status=success', 1, 'xevos', GETDATE())
END

IF NOT Exists (select fk_ID FROM [dbo].[tblConfigDetails] WITH (NOLOCK) WHERE fk_ID = @FKID AND KeyName = 'Paydibs.PaymentGatewayAPI.PayMaster#RURL_UPPPU')
BEGIN 
	INSERT INTO [dbo].[tblConfigDetails] 
	(fk_ID,KeyName,KeyValue,Status,CreatedBy,CreatedDateTime) 
	VALUES (@FKID,'Paydibs.PaymentGatewayAPI.PayMaster#RURL_UPPPU', 'http://localhost:56019/order/sales/paymastersales?status=declined', 1, 'xevos', GETDATE())
END

IF NOT Exists (select fk_ID FROM [dbo].[tblConfigDetails] WITH (NOLOCK) WHERE fk_ID = @FKID AND KeyName = 'Paydibs.PaymentGatewayAPI.PayMaster#RURL_UPPPC')
BEGIN 
	INSERT INTO [dbo].[tblConfigDetails] 
	(fk_ID,KeyName,KeyValue,Status,CreatedBy,CreatedDateTime) 
	VALUES (@FKID,'Paydibs.PaymentGatewayAPI.PayMaster#RURL_UPPPC', 'http://localhost:56019/order/sales/paymastersales?status=cancel', 1, 'xevos', GETDATE())
END

IF NOT Exists (select fk_ID FROM [dbo].[tblConfigDetails] WITH (NOLOCK) WHERE fk_ID = @FKID AND KeyName = 'Paydibs.PaymentGatewayAPI.PayMaster#POSTURL')
BEGIN 
	INSERT INTO [dbo].[tblConfigDetails] 
	(fk_ID,KeyName,KeyValue,Status,CreatedBy,CreatedDateTime) 
	VALUES (@FKID,'Paydibs.PaymentGatewayAPI.PayMaster#POSTURL', 'https://dev.finexusgroup.com:4445/upp/faces/upp/payment.xhtml?', 1, 'xevos', GETDATE())
END

IF NOT Exists (select fk_ID FROM [dbo].[tblConfigDetails] WITH (NOLOCK) WHERE fk_ID = @FKID AND KeyName = 'Paydibs.PaymentGatewayAPI.PayMaster#HASHKEY')
BEGIN 
	INSERT INTO [dbo].[tblConfigDetails] 
	(fk_ID,KeyName,KeyValue,Status,CreatedBy,CreatedDateTime) 
	VALUES (@FKID,'Paydibs.PaymentGatewayAPI.PayMaster#HASHKEY', 'B96856FA91749A259F71340E3C6BC3478524320F218587D22071A35DD4E7B4FF', 1, 'xevos', GETDATE())
END

IF NOT Exists (select fk_ID FROM [dbo].[tblConfigDetails] WITH (NOLOCK) WHERE fk_ID = @FKID AND KeyName = 'Paydibs.PaymentGatewayAPI.PayMaster#VERSION')
BEGIN 
	INSERT INTO [dbo].[tblConfigDetails] 
	(fk_ID,KeyName,KeyValue,Status,CreatedBy,CreatedDateTime) 
	VALUES (@FKID,'Paydibs.PaymentGatewayAPI.PayMaster#VERSION', '05', 1, 'xevos', GETDATE())
END

IF NOT Exists (select fk_ID FROM [dbo].[tblConfigDetails] WITH (NOLOCK) WHERE fk_ID = @FKID AND KeyName = 'Paydibs.PaymentGatewayAPI.PayMaster#ServID')
BEGIN 
	INSERT INTO [dbo].[tblConfigDetails] 
	(fk_ID,KeyName,KeyValue,Status,CreatedBy,CreatedDateTime) 
	VALUES (@FKID,'Paydibs.PaymentGatewayAPI.PayMaster#ServID', 'UM', 1, 'xevos', GETDATE())
END