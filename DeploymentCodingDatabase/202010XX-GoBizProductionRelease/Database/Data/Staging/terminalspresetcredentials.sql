/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP (1000) [ID]
      ,[HostID]
      ,[PaymentMethod]
      ,[Bank]
      ,[FieldName]
      ,[DBFieldName]
      ,[Encrypt]
      ,[DefaultValue]
      ,[Hex]
      ,[Currency]
  FROM [PG].[dbo].[Terminals_PreSetCredential]

  ID	HostID	PaymentMethod	Bank	FieldName	DBFieldName	Encrypt	DefaultValue	Hex	Currency
62	4	CC	Alliance Bank	Merchant ID	MID	0	TEST001918500471	0	MYR
5 CC GoBiz  Merchant ID MID 0 000000000010521 0 MYR

Insert [PG].[dbo].[Terminals_PreSetCredential]
(HostID,	PaymentMethod,	Bank,	FieldName,	DBFieldName,	Encrypt,	DefaultValue,	Hex,	Currency)
values 
(5, 'CC', 'GoBiz', 'Merchant ID', 'MID', 0, '000000000010521', 0, 'MYR')
