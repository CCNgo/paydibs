USE [PGAdmin]
GO


IF NOT EXISTS(SELECT ID FROM [dbo].[tblConfig] WITH (NOLOCK) WHERE [Name] = 'Paydibs.PaymentGatewayAPI')
BEGIN
	INSERT INTO [dbo].[tblConfig]
			   ([ID]
			   ,[Name]
			   ,[Description]
			   ,[Status]
			   ,[CreatedBy]
			   ,[CreatedDateTime]
			   ,[UpdateBy]
			   ,[UpdatedDateTime])
		 VALUES
			   (NEWID()
			   ,'Paydibs.PaymentGatewayAPI'
			   ,'Paydibs PaymentGatewayAPI SETTINGS'
			   ,1
			   ,'Xevos'
			   ,GETDATE()
			   ,NULL
			   ,NULL)
END

GO