USE [PGAdmin]
GO


IF NOT EXISTS(SELECT [PaydibsMID] FROM [dbo].[tblCCMerchantCredential] WITH (NOLOCK) WHERE [PaydibsMID] = 'PDBS')

BEGIN
	INSERT INTO [dbo].[tblCCMerchantCredential]
			   ([HostID]
			  ,[PaydibsMID]
			  ,[MerchantID]
			  ,[TerminalID]
			  ,[HASHKEY]
			  ,[ECI]
			  ,[Status]
			  ,[CreatedBy]
			  ,[CreatedDateTime]
			  ,[UpdateBy]
			  ,[UpdatedDateTime])
		 VALUES
			   (5
			   ,'PDBS'
			   ,'000000000010521'
			   ,'10003647'
			   ,'B96856FA91749A259F71340E3C6BC3478524320F218587D22071A35DD4E7B4FF'
			   , '1'
			   , 1
			   ,'Xevos'
			   ,GETDATE()
			   ,NULL
			   ,NULL)
END

GO