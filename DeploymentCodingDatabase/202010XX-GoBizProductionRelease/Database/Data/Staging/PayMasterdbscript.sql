USE [PG]
GO
--Channel
--SET IDENTITY_INSERT [dbo].[Channel] ON 
--INSERT [dbo].[Channel] ([ChannelID], [IPAddress], [PortNumber], [QueryIPAddress], [QueryPortNumber], 
--[CancelIPAddress], [CancelPortNumber], [RevIPAddress], [RevPortNumber], [AckIPAddress], [AckPortNumber], 
--[ReturnIPAddresses], [ReturnURL], [ReturnURL2], [TimeOut], [DBConnStr], [ServerCertName], [CfgFile], 
--[Protocol], [Desc], [RecStatus]) 
--VALUES (5, N'http://localhost:56019/order/sales/paymastersales', 0, N'http://localhost:56019/order/query/paymastersales', 0, 
--N'', 0, N'', 0, N'', 0, N'', N'', N'', 900, N'', N'', N'templates\GOBIZ.txt', N'TLS12', N'GOBIZ', 1)
--SET IDENTITY_INSERT [dbo].[Channel] OFF

--Staging Path
SET IDENTITY_INSERT [dbo].[Channel] ON 
INSERT [dbo].[Channel] ([ChannelID], [IPAddress], [PortNumber], [QueryIPAddress], [QueryPortNumber], 
[CancelIPAddress], [CancelPortNumber], [RevIPAddress], [RevPortNumber], [AckIPAddress], [AckPortNumber], 
[ReturnIPAddresses], [ReturnURL], [ReturnURL2], [TimeOut], [DBConnStr], [ServerCertName], [CfgFile], 
[Protocol], [Desc], [RecStatus]) 
VALUES (5, N'https://devapi.paydibs.com/order/sales/paymastersales', 0, N'https://devapi.paydibs.com/order/query/paymastersales', 0, 
N'', 0, N'', 0, N'', 0, N'', N'', N'', 900, N'', N'', N'templates\GOBIZ.txt', N'TLS12', N'GOBIZ', 1)
SET IDENTITY_INSERT [dbo].[Channel] OFF

----TxnStatusAction
--SET IDENTITY_INSERT [dbo].[TxnStatusAction] ON 
--INSERT [dbo].[TxnStatusAction] ([PKID], [ActionID], [TxnType], [TxnField], [TxnStatus], [Action], [Desc], [Priority], [RecStatus]) VALUES (36, 9, N'PAY;QUERY', NULL, N'S', 1, N'Success', 1, 1)
--INSERT [dbo].[TxnStatusAction] ([PKID], [ActionID], [TxnType], [TxnField], [TxnStatus], [Action], [Desc], [Priority], [RecStatus]) VALUES (37, 9, N'PAY;QUERY', NULL, N'P', 3, N'Pending', 1, 1)
--INSERT [dbo].[TxnStatusAction] ([PKID], [ActionID], [TxnType], [TxnField], [TxnStatus], [Action], [Desc], [Priority], [RecStatus]) VALUES (38, 9, N'PAY;QUERY', NULL, N'!S', 2, N'Failed', 1, 1)
--SET IDENTITY_INSERT [dbo].[TxnStatusAction] OFF


--Host
SET IDENTITY_INSERT [dbo].[Host] ON 
INSERT [dbo].[Host] (
[HostID], 
[HostName], 
[HostCode], 
[SvcTypeID], 
[SignBy], 
[PaymentTemplate], 
[SecondEntryTemplate], 
[MesgTemplate], 
[NeedReplyAcknowledgement], 
[NeedRedirectOTP], 
[Require2ndEntry], 
[AllowQuery], 
[AllowReversal], 
[MaxRefund], 
[ChannelID], 
[Timeout], 
[TxnStatusActionID], 
[HostReplyMethod], 
[OSPymtCode], 
[GatewayTxnIDFormat], 
[MerchantPassword], 
[ReturnKey], 
[HashMethod], 
[SecretKey], 
[InitVector], 
[LogoPath], 
[QueryFlag], 
[SettlePeriod], 
[SettleTime], 
[HostMDR], 
[LogRes],
[DateActivated], 
[DateDeactivated], 
[RecStatus], 
[Desc], 
[IsExponent], 
[HostDesc]) 
VALUES 
(5, 
N'GOBIZ',
NULL,
2,
0,
N'RTTemplates\redirect_post.html',
N'',
N'',
0,
0,
0,
1,
1,
-1,
5,
1500,
13,
1,
N'',
1,
N'',
N'',
N'GOBIZ',
N'',
N'',
N'gobiz.png',
1,
1,
NULL,
CAST(0.00 AS Decimal(18, 2)),
1,
CAST(N'2020-07-23T11:44:00.000' AS DateTime), 
CAST(N'2099-12-31T00:00:00.000' AS DateTime), 
1, 
N'Credit Card', 
0, 
N'GOBIZ')
SET IDENTITY_INSERT [dbo].[Host] OFF


--USE [PG]
--GO
----Terminals_PreSetCredential
--SET IDENTITY_INSERT [dbo].[Terminals_PreSetCredential] ON 
--INSERT [dbo].[Terminals_PreSetCredential] ([ID], [HostID], [PaymentMethod], [Bank], [FieldName], [DBFieldName], [Encrypt], [DefaultValue], 
--[Hex],[Currency]) VALUES (64, 15, N'WA', N'TNG', N'Client Id', N'MID', N'0', N'2014000014442', 0, 'MYR')
--INSERT [dbo].[Terminals_PreSetCredential] ([ID], [HostID], [PaymentMethod], [Bank], [FieldName], [DBFieldName], [Encrypt], [DefaultValue], 
--[Hex],[Currency]) VALUES (65, 15, N'WA', N'TNG', N'Client Secret', N'Password', N'0', N'67asdf', 0, 'MYR')
--SET IDENTITY_INSERT [dbo].[Terminals_PreSetCredential] OFF


--insert  into [OB].[dbo].[HostCountry]
--	([CountryID]
--      ,[HostID]
--      ,[CurrencyCode]
--      ,[RecStatus]
--	) values
--	(
--	135,
--	15,
--	'MYR',
--	1
--	)