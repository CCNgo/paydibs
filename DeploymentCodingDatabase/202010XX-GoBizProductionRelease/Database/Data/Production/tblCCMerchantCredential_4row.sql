USE [PGAdmin]
GO


IF NOT EXISTS(SELECT [PaydibsMID] FROM [dbo].[tblCCMerchantCredential] WITH (NOLOCK) WHERE [PaydibsMID] = 'PDBS')

BEGIN
	INSERT INTO [dbo].[tblCCMerchantCredential]
			   ([HostID]
			  ,[PaydibsMID]
			  ,[MerchantID]
			  ,[TerminalID]
			  ,[HASHKEY]
			  ,[ECI]
			  ,[Status]
			  ,[CreatedBy]
			  ,[CreatedDateTime]
			  ,[UpdateBy]
			  ,[UpdatedDateTime])
		 VALUES
			   (5
			   ,'PDBS'
			   ,'000000000010474' --Production 3ds PAYDIBS SDN BHD
			   ,'10003918'
			   ,'AEFEBBE5622B8C9B1C957E6B38C7F8E50FDF5E6FFC647BCD8391D006922E5076'
			   , '1'
			   , 1
			   ,'Xevos'
			   ,GETDATE()
			   ,NULL
			   ,NULL)

		INSERT INTO [dbo].[tblCCMerchantCredential]
			   ([HostID]
			  ,[PaydibsMID]
			  ,[MerchantID]
			  ,[TerminalID]
			  ,[HASHKEY]
			  ,[ECI]
			  ,[Status]
			  ,[CreatedBy]
			  ,[CreatedDateTime]
			  ,[UpdateBy]
			  ,[UpdatedDateTime])
		 VALUES
			   (5
			   ,'PDBS'
			   ,'000000000010481' --Production Non-3ds PAYDIBS SDN BHD
			   ,'10003921'
			   ,'5EB0A43342EFF813AB4EABEA7E161E2B7B483B9AB6110488B4253CAE537AED7C'
			   , '0'
			   , 0
			   ,'Xevos'
			   ,GETDATE()
			   ,NULL
			   ,NULL)
END


IF NOT EXISTS(SELECT [PaydibsMID] FROM [dbo].[tblCCMerchantCredential] WITH (NOLOCK) WHERE [PaydibsMID] = 'MNTMRS')

BEGIN
	INSERT INTO [dbo].[tblCCMerchantCredential]
			   ([HostID]
			  ,[PaydibsMID]
			  ,[MerchantID]
			  ,[TerminalID]
			  ,[HASHKEY]
			  ,[ECI]
			  ,[Status]
			  ,[CreatedBy]
			  ,[CreatedDateTime]
			  ,[UpdateBy]
			  ,[UpdatedDateTime])
		 VALUES
			   (5
			   ,'MNTMRS'
			   ,'000000000010512' --Production 3ds MRUNCIT COMMERCE SDN BHD
			   ,'10003927'
			   ,'01F2346152B9DDCF1F4EE1A88AE72987A7CB1E8724F547727D5B5913305393C1'
			   , '1'
			   , 0
			   ,'Xevos'
			   ,GETDATE()
			   ,NULL
			   ,NULL)
END

IF NOT EXISTS(SELECT [PaydibsMID] FROM [dbo].[tblCCMerchantCredential] WITH (NOLOCK) WHERE [PaydibsMID] = 'HOCOGS')

BEGIN
	INSERT INTO [dbo].[tblCCMerchantCredential]
			   ([HostID]
			  ,[PaydibsMID]
			  ,[MerchantID]
			  ,[TerminalID]
			  ,[HASHKEY]
			  ,[ECI]
			  ,[Status]
			  ,[CreatedBy]
			  ,[CreatedDateTime]
			  ,[UpdateBy]
			  ,[UpdatedDateTime])
		 VALUES
			   (5
			   ,'HOCOGS'
			   ,'000000000010494' --Production 3ds HOHO TECHNOLOGIES SDN BHD
			   ,'10003923'
			   ,'1C784B2A792FE7239B7588B6BB41E5F743B10ABF1E215841DA7E8A67FE678124'
			   , '1'
			   , 0
			   ,'Xevos'
			   ,GETDATE()
			   ,NULL
			   ,NULL)
END
GO