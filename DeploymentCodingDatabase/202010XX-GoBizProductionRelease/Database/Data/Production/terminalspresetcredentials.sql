/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP (1000) [ID]
      ,[HostID]
      ,[PaymentMethod]
      ,[Bank]
      ,[FieldName]
      ,[DBFieldName]
      ,[Encrypt]
      ,[DefaultValue]
      ,[Hex]
      ,[Currency]
  FROM [PG].[dbo].[Terminals_PreSetCredential]

Insert [PG].[dbo].[Terminals_PreSetCredential]
(HostID,	PaymentMethod,	Bank,	FieldName,	DBFieldName,	Encrypt,	DefaultValue,	Hex,	Currency)
values 
(5, 'CC', 'GoBiz', 'Merchant ID', 'MID', 0, '000000000010424', 0, 'MYR')
