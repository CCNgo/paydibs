USE [PGAdmin]
GO
/****** Object:  StoredProcedure [dbo].[GettblCCMerchantCredential_By_PaydibsMID]    Script Date: 18/10/2020 11:01:12 ******/
DROP PROCEDURE [GettblCCMerchantCredential_By_PaydibsMID]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author : Xevos
-- Create Date : 18/10/2020
-- Description : Get CC Merchant Credential By Paydibs MID
-- Use By : Scheduler
-- =============================================

CREATE PROCEDURE [GettblCCMerchantCredential_By_PaydibsMID]
@PaydibsMID [varchar](30)
AS
BEGIN
	SET NOCOUNT ON 	
	
	SELECT [HostID]
      ,[PaydibsMID]
      ,[MerchantID]
      ,[TerminalID]
      ,[HASHKEY]
      ,[CreatedBy]
      ,[CreatedDateTime]
      ,[UpdateBy]
      ,[UpdatedDateTime]
  FROM [PGAdmin].[dbo].[tblCCMerchantCredential]
    WHERE [PaydibsMID] = @PaydibsMID and [ECI] = 1 and [Status] = 1 
END
GO
