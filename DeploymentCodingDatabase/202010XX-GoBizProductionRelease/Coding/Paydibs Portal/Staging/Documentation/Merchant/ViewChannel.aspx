﻿<%@ page title="MERCHANT" language="C#" masterpagefile="~/MasterPage/DocMasterPage.master" autoeventwireup="true" inherits="Documentation_Merchant_ViewChannel, App_Web_opb0hncf" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <h4>View Channel</h4>
    <p>To view merchant engaged channel(s).</p><br />
    <img src="../../assets/img/doc_Img/merchant/view_channel_menu.png" class="img_info size_img300" /><br />
    <h4>Details</h4>
    <ul>
        <li>Type: Type of payment method</li>
        <li>Channel Desc: Description of the channel</li>
        <li>Currency Code: Accepted currency by channel</li>
    </ul>
    <img src="../../assets/img/doc_Img/merchant/view_channel.png" class="img_info size_img800" /><br />
    <div id="next_previous" style="margin-top:50px">
        <a runat="server" href="/Documentation/Merchant/ViewMerchant.aspx" class="previous">&laquo; Previous :  View Merchant</a>
        <a runat="server" href="/Documentation/Merchant/ViewMerchantRate.aspx" class="next">Next: View Merchant Rate  &raquo;</a>
    </div>
</asp:Content>

