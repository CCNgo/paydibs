﻿<%@ page title="USER" language="C#" masterpagefile="~/MasterPage/DocMasterPage.master" autoeventwireup="true" inherits="Documentation_User_ViewUserGroup, App_Web_ccouw5zn" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <h4>View User Group</h4>
    <p>To view user group</p><br />
    <img src="../../assets/img/doc_Img/user/view_user_group_menu.png" class="img_info size_img300" /><br />
    <h4>List User Group</h4>
    <p>Click on 'Search' button, a list of user group will be display</p>
    <img src="../../assets/img/doc_Img/user/view_user_group.png" class="img_info size_img800" /><br />
    <div id="next_previous" style="margin-top:50px">
        <a runat="server" href="/Documentation/User/ViewUser.aspx" class="previous">&laquo; Previous :  View User</a>
        <a runat="server" href="/Documentation/User/ChangePassword.aspx" class="next">Next: Change Password  &raquo;</a>
    </div>
</asp:Content>

