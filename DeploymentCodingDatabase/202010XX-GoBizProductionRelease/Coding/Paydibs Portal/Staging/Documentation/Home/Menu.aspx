﻿<%@ page title="MENU" language="C#" masterpagefile="~/MasterPage/DocMasterPage.master" autoeventwireup="true" inherits="Documentation_Home_Menu, App_Web_53ghmbg1" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <p>
        A list of image menu option on left side of dashboard. <br />
        Click <img src="../../assets/img/doc_Img/home/menu_icon.png" class="" />to expand details menu with name or shrink menu. <br /><br />
        Default menu:  
    </p>
    <img src="../../assets/img/doc_Img/home/menu_close.png" class="img_info " /><br /><br />
     <p>Expanded menu:</p>
    <img src="../../assets/img/doc_Img/home/menu_open.png" class="img_info" /><br />
    <div id="next_previous" style="margin-top:30px">
        <a runat="server" href="/Documentation/Home/Index.aspx" class="previous">&laquo; Previous :  Dashboard</a>
        <a runat="server" href="/Documentation/Home/UserAccount.aspx" class="next">Next: User Account &raquo;</a>
    </div>
</asp:Content>

