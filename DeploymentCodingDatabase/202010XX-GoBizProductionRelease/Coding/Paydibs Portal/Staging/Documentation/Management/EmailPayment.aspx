﻿<%@ page title="MERCHANT SERVICES" language="C#" masterpagefile="~/MasterPage/DocMasterPage.master" autoeventwireup="true" inherits="Documentation_Management_EmailPayment, App_Web_ng5oxxnf" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <h4>Email Payment (Single/Bulk)</h4>
    <p>To send out payment request by email to a single or group of customers.</p><br />
    <img src="../../assets/img/doc_Img/management/email_payment_menu.png" class="img_info size_img300" /><br />
    <h4>Generate email payment</h4>
    <p>Enter customer and order details.</p>
    <ul>
        <li>Customer Name: Customer name</li>
        <li>Customer Contact Number: Customer contact number</li>
        <li>Customer Email Address: Customer(s) email address.</li>
        <li>Order Description: Order description</li>
        <li>Order Number: Order number / invoice number / reference number</li>
        <li>Currency: Payment currency</li>
        <li>Amount: Payment amount</li>
    </ul>
    <p>Click on 'Generate Email Template' button, to preview your email payment or click on 'Reset' button to start a new entry</p>
    <img src="../../assets/img/doc_Img/management/email_payment.png" class="img_info size_img800" /><br />
    <h4>Email Payment Preview</h4>
    <p>Check your email payment details before send to customer. Click on 'Send' button to send out the email.</p>
    <img src="../../assets/img/doc_Img/management/email_payment_preview.png" class="img_info size_img800" /><br />
    <p>Advise your customer to use the link in email to proceed for payment.</p>
    <div id="next_previous" style="margin-top:50px">
        <a runat="server" href="/Documentation/User/ChangePassword.aspx" class="previous">&laquo; Previous :  Change Password</a>
        <a runat="server" href="/Documentation/Management/BuyButton.aspx" class="next">Next: Buy Button &raquo;</a>
    </div>
</asp:Content>
