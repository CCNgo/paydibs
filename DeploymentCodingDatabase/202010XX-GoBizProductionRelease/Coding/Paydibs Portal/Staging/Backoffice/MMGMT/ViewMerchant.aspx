﻿<%@ page language="C#" autoeventwireup="true" inherits="ViewMerchant, App_Web_ufpja5qk" %>

<!DOCTYPE html>
<%@ Register TagPrefix="uc" TagName="Left_Side_Bar" Src="~/UserControl/LeftSideBar.ascx" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Paydibs-Checkout View Merchant Profile</title>
    <link rel="stylesheet" type="text/css" href="../../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/material-design-icons/css/material-design-iconic-font.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/select2/css/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/bootstrap-slider/css/bootstrap-slider.min.css" />
    <link rel="stylesheet" href="../../assets/css/app.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <%--ADD START DANIEL 20190110 [Add Hidden1 and Hidden2]--%>
        <asp:HiddenField ID="HiddenField2" runat="server" />
        <asp:HiddenField ID="HiddenField1" runat="server" />
        <%--ADD START DANIEL 20190110 [Add Hidden1 and Hidden2]--%>
        <div class="be-wrapper be-collapsible-sidebar be-collapsible-sidebar-collapsed">
            <uc:Left_Side_Bar ID="Left_Side_Bar" runat="server" />
            <div class="be-content">
                <div class="main-content container-fluid">
                    <%--ADD START DANIEL 20190110 [Display message type]--%>
                    <div role="alert" class="alert alert-contrast alert-dismissible" id="dl_ErrorDialog">
                        <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
                        <div class="message" id="dl_message">
                        </div>
                    </div>
                    <%--ADD E N D DANIEL 20190110 [Display message type]--%>
                    <div class="row">
                        <div class="col-md-12 ">
                            <div class="card card-table card-border-color card-border-color-primary">
                                <div class="row table-filters-container">
                                    <div class="col-12 col-lg-12 col-xl-6">
                                        <div class="row">
                                            <div class="col-12 col-lg-8 table-filters pb-0 pb-xl-4">
                                                <span class="table-filter-title">Merchant</span>
                                                <div class="filter-container">
                                                    <label class="control-label">Select Merchant:</label>
                                                    <asp:DropDownList ID="DD_merchant" runat="server" CssClass="select2"></asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-12 col-lg-2 table-filters pb-0 pb-xl-4">
                                                <span class="table-filter-title"></span>
                                                <div class="filter-container" style="margin-top: 45px">
                                                    <asp:Button ID="btn_GetInfo" runat="server" Text="Get Info" CssClass="btn btn-primary btn-xl" OnClick="btn_GetInfo_Click" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card card-border-color card-border-color-primary">
                                <div class="card-header card-header-divider font-weight-bold">
                                    Merchant Profile
                                </div>

                                <div class="card-body">
                                    <div class="filter-container">
                                        <div class="row">
                                            <div class="col-6">
                                                <h4>General Information</h4>
                                                <div class="row">
                                                    <div class="col-4">
                                                        <label class="control-label font-weight-bold">Merchant ID</label>
                                                    </div>
                                                    <div class="col-4">
                                                        <asp:Label ID="LB_MerchantID" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-4">
                                                        <label id="LB_Pwd" class="control-label font-weight-bold" runat="server" visible="false">Merchant Password</label>
                                                    </div>
                                                    <div class="col-4">
                                                        <asp:Label ID="LB_MerchantPwd" runat="server" Text="[--]" CssClass="control-label" Visible="false"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-4">
                                                        <label class="control-label font-weight-bold">Date Activated</label>
                                                    </div>
                                                    <div class="col-4">
                                                        <asp:Label ID="LB_DateActivated" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-4">
                                                        <label class="control-label font-weight-bold">Company Registered Name</label>
                                                    </div>
                                                    <div class="col-4">
                                                        <asp:Label ID="LB_CompanyName" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-4">
                                                        <label class="control-label font-weight-bold">Company Registered No.</label>
                                                    </div>
                                                    <div class="col-4">
                                                        <asp:Label ID="LB_RegisteredNo" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                                    </div>
                                                </div>
                                                <%--<div class="row">
                                                        <div class="col-4">
                                                            <label class="control-label font-weight-bold">GST No.</label>
                                                        </div>
                                                        <div class="col-4">
                                                            <asp:Label ID="LB_GSTNo" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                                        </div>
                                                    </div>--%>
                                                <div class="row">
                                                    <div class="col-4">
                                                        <label class="control-label font-weight-bold">Business Address</label>
                                                    </div>
                                                    <div class="col-4">
                                                        <asp:Label ID="LB_Address1" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-4">
                                                    </div>
                                                    <div class="col-4">
                                                        <asp:Label ID="LB_Address2" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-4">
                                                        <label class="control-label font-weight-bold">Office Contact No.</label>
                                                    </div>
                                                    <div class="col-4">
                                                        <asp:Label ID="LB_ContactNo" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                                    </div>
                                                </div>
                                                <%--<div class="row">
                                                        <div class="col-4">
                                                            <label class="control-label font-weight-bold">Type of Business</label>
                                                        </div>
                                                        <div class="col-4">
                                                            <asp:Label ID="LB_BusinessType" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                                        </div>
                                                    </div>--%>

                                                <h4>MCC Code and Logo</h4>
                                                <div class="row">
                                                    <div class="col-4">
                                                        <label class="control-label font-weight-bold">MCC Code</label>
                                                    </div>
                                                    <div class="col-4">
                                                        <asp:Label ID="LB_MCCCode" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-4">
                                                        <label class="control-label font-weight-bold">Company Logo</label>
                                                    </div>
                                                    <div class="col-4">
                                                        <img id="img_MerchantLogo" src="~/assets/img/120x270.png" style="width: 80%; max-width: 150px" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <h4>Authorized Contact Person 1</h4>
                                                <div class="row">
                                                    <div class="col-4">
                                                        <label class="control-label font-weight-bold">Name</label>
                                                    </div>
                                                    <div class="col-4">
                                                        <asp:Label ID="LB_ACP1_Name" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-4">
                                                        <label class="control-label font-weight-bold">Contact No.</label>
                                                    </div>
                                                    <div class="col-4">
                                                        <asp:Label ID="LB_ACP1_Contact" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-4">
                                                        <label class="control-label font-weight-bold">Email Address</label>
                                                    </div>
                                                    <div class="col-4">
                                                        <asp:Label ID="LB_ACP1_Email" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                                    </div>
                                                </div>

                                                <h4>Authorized Contact Person 2</h4>
                                                <div class="row">
                                                    <div class="col-4">
                                                        <label class="control-label font-weight-bold">Name</label>
                                                    </div>
                                                    <div class="col-4">
                                                        <asp:Label ID="LB_ACP2_Name" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-4">
                                                        <label class="control-label font-weight-bold">Contact No.</label>
                                                    </div>
                                                    <div class="col-4">
                                                        <asp:Label ID="LB_ACP2_Contact" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-4">
                                                        <label class="control-label font-weight-bold">Email Address</label>
                                                    </div>
                                                    <div class="col-4">
                                                        <asp:Label ID="LB_ACP2_Email" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                                    </div>
                                                </div>

                                                <h4>Billing Contact Person</h4>
                                                <div class="row">
                                                    <div class="col-4">
                                                        <label class="control-label font-weight-bold">Name</label>
                                                    </div>
                                                    <div class="col-4">
                                                        <asp:Label ID="LB_Bill_Name" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-4">
                                                        <label class="control-label font-weight-bold">Contact No.</label>
                                                    </div>
                                                    <div class="col-4">
                                                        <asp:Label ID="LB_Bill_Contact" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-4">
                                                        <label class="control-label font-weight-bold">Email Address</label>
                                                    </div>
                                                    <div class="col-4">
                                                        <asp:Label ID="LB_Bill_Email" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <%--<div class="col-lg-6">
                            <div class="card card-border-color card-border-color-primary">
                                <div class="card-header card-header-divider font-weight-bold">
                                    Company Ownership Profile
                                </div>
                                <div class="card-body">
                                    <div class="filter-container">
                                        <h4>Director / Proprietor</h4>
                                        <div class="row">
                                            <div class="col-5">
                                                <label class="control-label font-weight-bold">Director Name</label>
                                            </div>
                                            <div class="col-5">
                                                <asp:Label ID="LB_i_director_name" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-5">
                                                <label class="control-label font-weight-bold">Identity Card / Passport No.</label>
                                            </div>
                                            <div class="col-5">
                                                <asp:Label ID="LB_i_director_id" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-5">
                                                <label class="control-label font-weight-bold">Director Name</label>
                                            </div>
                                            <div class="col-5">
                                                <asp:Label ID="LB_ii_director_name" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-5">
                                                <label class="control-label font-weight-bold">Identity Card / Passport No.</label>
                                            </div>
                                            <div class="col-5">
                                                <asp:Label ID="LB_ii_director_id" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-5">
                                                <label class="control-label font-weight-bold">Director Name</label>
                                            </div>
                                            <div class="col-5">
                                                <asp:Label ID="LB_iii_director_name" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-5">
                                                <label class="control-label font-weight-bold">Identity Card / Passport No.</label>
                                            </div>
                                            <div class="col-5">
                                                <asp:Label ID="LB_iii_director_id" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-5">
                                                <label class="control-label font-weight-bold">Director Name</label>
                                            </div>
                                            <div class="col-5">
                                                <asp:Label ID="LB_iv_director_name" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-5">
                                                <label class="control-label font-weight-bold">Identity Card / Passport No.</label>
                                            </div>
                                            <div class="col-5">
                                                <asp:Label ID="LB_iv_director_id" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-5">
                                                <label class="control-label font-weight-bold">Director Name</label>
                                            </div>
                                            <div class="col-5">
                                                <asp:Label ID="LB_v_director_name" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-5">
                                                <label class="control-label font-weight-bold">Identity Card / Passport No.</label>
                                            </div>
                                            <div class="col-5">
                                                <asp:Label ID="LB_v_director_id" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                            </div>
                                        </div>

                                        <h4>ShareHolders</h4>
                                        <div class="row">
                                            <div class="col-5">
                                                <label class="control-label font-weight-bold">Shareholder Name</label>
                                            </div>
                                            <div class="col-5">
                                                <asp:Label ID="LB_i_shareholder_name" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-5">
                                                <label class="control-label font-weight-bold">Identity Card / Passport No.</label>
                                            </div>
                                            <div class="col-5">
                                                <asp:Label ID="LB_i_shareholder_id" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-5">
                                                <label class="control-label font-weight-bold">Shareholder Name</label>
                                            </div>
                                            <div class="col-5">
                                                <asp:Label ID="LB_ii_shareholder_name" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-5">
                                                <label class="control-label font-weight-bold">Identity Card / Passport No.</label>
                                            </div>
                                            <div class="col-5">
                                                <asp:Label ID="LB_ii_shareholder_id" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-5">
                                                <label class="control-label font-weight-bold">Shareholder Name</label>
                                            </div>
                                            <div class="col-5">
                                                <asp:Label ID="LB_iii_shareholder_name" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-5">
                                                <label class="control-label font-weight-bold">Identity Card / Passport No.</label>
                                            </div>
                                            <div class="col-5">
                                                <asp:Label ID="LB_iii_shareholder_id" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-5">
                                                <label class="control-label font-weight-bold">Shareholder Name</label>
                                            </div>
                                            <div class="col-5">
                                                <asp:Label ID="LB_iv_shareholder_name" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-5">
                                                <label class="control-label font-weight-bold">Identity Card / Passport No.</label>
                                            </div>
                                            <div class="col-5">
                                                <asp:Label ID="LB_iv_shareholder_id" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-5">
                                                <label class="control-label font-weight-bold">Shareholder Name</label>
                                            </div>
                                            <div class="col-5">
                                                <asp:Label ID="LB_v_shareholder_name" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-5">
                                                <label class="control-label font-weight-bold">Identity Card / Passport No.</label>
                                            </div>
                                            <div class="col-5">
                                                <asp:Label ID="LB_v_shareholder_id" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>--%>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="card card-border-color card-border-color-primary">
                                <div class="card-header card-header-divider font-weight-bold">
                                    Business Profile
                                </div>
                                <div class="card-body">
                                    <div class="filter-container">
                                        <div class="row">
                                            <div class="col-5">
                                                <label class="control-label font-weight-bold">Doing Business As (DBA) Name / Trading Name</label>
                                            </div>
                                            <div class="col-5">
                                                <asp:Label ID="LB_TradingName" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-5">
                                                <label class="control-label font-weight-bold">Online Store URL</label>
                                            </div>
                                            <div class="col-5">
                                                <asp:Label ID="LB_WebsiteURL" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-5">
                                                <label class="control-label font-weight-bold">Type of Products / Services sold</label>
                                            </div>
                                            <div class="col-5">
                                                <asp:Label ID="LB_ProductType" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                            </div>
                                        </div>
                                        <%--<div class="row">
                                            <div class="col-5">
                                                <label class="control-label font-weight-bold">Target Market(s)</label>
                                            </div>
                                            <div class="col-5">
                                                <asp:Label ID="LB_TargetMarket" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                            </div>
                                        </div>--%>
                                        <div class="row">
                                            <div class="col-5">
                                                <label class="control-label font-weight-bold">Product Delivered Period</label>
                                            </div>
                                            <div class="col-5">
                                                <asp:Label ID="LB_DeliveryPeriod" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-5">
                                                <label class="control-label font-weight-bold">Shopping Cart/E-Commerce System/POS Used</label>
                                            </div>
                                            <div class="col-5">
                                                <asp:Label ID="LB_Plugin" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="card card-border-color card-border-color-primary">
                                <div class="card-header card-header-divider font-weight-bold">
                                    Merchant Account Information
                                </div>
                                <div class="card-body">
                                    <div class="filter-container">
                                        <h4>Payment Notification</h4>
                                        <div class="row">
                                            <div class="col-5">
                                                <label class="control-label font-weight-bold">Receive Notification</label>
                                            </div>
                                            <div class="col-5">
                                                <asp:Label ID="LB_NotifyEmailFlag" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-5">
                                                <label class="control-label font-weight-bold">Email Address</label>
                                            </div>
                                            <div class="col-5">
                                                <asp:Label ID="LB_NotifyEmail" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                            </div>
                                        </div>

                                        <h4>Customer Service</h4>
                                        <div class="row">
                                            <div class="col-5">
                                                <label class="control-label font-weight-bold">Contact No.</label>
                                            </div>
                                            <div class="col-5">
                                                <asp:Label ID="LB_CS_Contact" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-5">
                                                <label class="control-label font-weight-bold">Email Address</label>
                                            </div>
                                            <div class="col-5">
                                                <asp:Label ID="LB_CS_Email" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="card card-border-color card-border-color-primary">
                                <div class="card-header card-header-divider font-weight-bold">
                                    Bank Account Information
                                </div>
                                <div class="card-body">
                                    <div class="filter-container">
                                        <div class="row">
                                            <div class="col-5">
                                                <label class="control-label font-weight-bold">Bank Name</label>
                                            </div>
                                            <div class="col-5">
                                                <asp:Label ID="LB_BankName" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-5">
                                                <label class="control-label font-weight-bold">Account Number</label>
                                            </div>
                                            <div class="col-5">
                                                <asp:Label ID="LB_AccountNo" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-5">
                                                <label class="control-label font-weight-bold">Account Name</label>
                                            </div>
                                            <div class="col-5">
                                                <asp:Label ID="LB_AccountName" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-5">
                                                <label class="control-label font-weight-bold">Swift Code</label>
                                            </div>
                                            <div class="col-5">
                                                <asp:Label ID="LB_SwiftCode" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-5">
                                                <label class="control-label font-weight-bold">Bank Address</label>
                                            </div>
                                            <div class="col-5">
                                                <asp:Label ID="LB_BankAddress" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-5">
                                                <label class="control-label font-weight-bold">Bank Country</label>
                                            </div>
                                            <div class="col-5">
                                                <asp:Label ID="LB_BankCountry" runat="server" Text="[--]" CssClass="control-label"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-5">
                                                <label id="LB_TrxLimit" class="control-label font-weight-bold" runat="server" visible="false">Per Transaction Limit</label>
                                            </div>
                                            <div class="col-5">
                                                <asp:Label ID="LB_Limit" runat="server" Text="[--]" CssClass="control-label" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center" id="footer">
                Copyright © 2019 Paydibs Sdn. Bhd. All Rights Reserved.
            </div>
        </div>
    </form>
    <script src="../../assets/lib/customize/General.js"></script>
    <script src="../../assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/bootstrap/dist/js/bootstrap.bundle.min.js" type="text/javascript"></script>
    <script src="../../assets/js/app.js" type="text/javascript"></script>
    <script src="../../assets/lib/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/select2/js/select2.full.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/bootstrap-slider/bootstrap-slider.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="../../assets/js/app-table-filters.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net/js/jquery.dataTables.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-bs4/js/dataTables.bootstrap4.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/dataTables.buttons.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/buttons.html5.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/buttons.flash.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/buttons.print.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/buttons.colVis.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js" type="text/javascript"></script>
    <script src="../../assets/js/app-tables-datatables.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            //initialize the javascript
            App.init();
            App.tableFilters();
            App.dataTables();
            //prettyPrint();
        });

        //ADD START DANIEL 20190110 [Javascript to display error message]
        $(function () {
            $('#dl_ErrorDialog').hide();
            var msg = $('#<%= HiddenField1.ClientID %>').val();
            var classType = $('#<%= HiddenField2.ClientID %>').val();
            var type = "alert-danger";
            if (msg != "") {
                if (classType != "") type = classType;
                $('#dl_ErrorDialog').addClass(type);
                document.getElementById('dl_message').innerText = msg;
                $('#dl_ErrorDialog').show();
            }
        });
        //ADD E N D DANIEL 20190110 [Javascript to display error message]
    </script>
</body>
</html>
