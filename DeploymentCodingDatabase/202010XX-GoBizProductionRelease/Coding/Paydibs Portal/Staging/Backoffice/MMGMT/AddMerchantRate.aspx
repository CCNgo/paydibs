﻿<%@ page language="C#" autoeventwireup="true" inherits="Backoffice_MMGMT_AddMerchantRate, App_Web_ufpja5qk" %>

<!DOCTYPE html>
<%@ Register TagPrefix="uc" TagName="Left_Side_Bar" Src="~/UserControl/LeftSideBar.ascx" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Paydibs-Checkout Add Merchant Rate</title>
    <link rel="stylesheet" type="text/css" href="../../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/material-design-icons/css/material-design-iconic-font.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/select2/css/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/bootstrap-slider/css/bootstrap-slider.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/datatables/datatables.net-bs4/css/dataTables.bootstrap4.css" />
    <link rel="stylesheet" href="../../assets/css/app.css" type="text/css" />

    <style>
        input[type="date"]::-webkit-inner-spin-button {
            display: none;
            -webkit-appearance: none;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:HiddenField ID="HiddenField2" runat="server" />
        <asp:HiddenField ID="HiddenField1" runat="server" />
        <div class="be-wrapper be-collapsible-sidebar be-collapsible-sidebar-collapsed">
            <uc:Left_Side_Bar ID="Left_Side_Bar" runat="server" />
            <div class="be-content">
                <div class="main-content container-fluid">
                    <div role="alert" class="alert alert-contrast alert-dismissible" id="dl_ErrorDialog">
                        <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
                        <div class="message" id="dl_message">
                        </div>
                    </div>
                    <div class="row" id="merchant_DD_list">
                        <div class="col-md-12 ">
                            <div class="card card-table card-border-color card-border-color-primary">
                                <div class="row table-filters-container">
                                    <div class="col-12 col-lg-12 col-xl-6">
                                        <div class="row">
                                            <div class="col-12 col-lg-8 table-filters pb-0 pb-xl-4">
                                                <span class="table-filter-title">Merchant</span>
                                                <div class="filter-container" id="normalMerchant">
                                                    <label class="control-label">Select Merchant:</label>
                                                    <asp:DropDownList ID="DD_merchant" runat="server" CssClass="select2" onchange="getChannel()"></asp:DropDownList>
                                                </div>
                                                    <asp:HiddenField ID="tabID" runat="server" />
                                            </div>
                                            <div class="col-12 col-lg-2 table-filters pb-0 pb-xl-4">
                                                <span class="table-filter-title"></span>
                                                <div class="filter-container" style="margin-top: 45px">
                                                    <%--<asp:Button  runat="server" Text="Get Info" style="display:none" CssClass="btn btn-primary btn-xl" OnClick="getChannel()"/>--%>
                                                    <%--<asp:Button ID="btn_GetInfo" runat="server" Text="Get Info" style="display:none" CssClass="btn btn-primary btn-xl" OnClick="btn_GetInfo_Click" />--%>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row"> 
                        <div class="col-md-12">
                            <div class="card card-border-color card-border-color-primary">
                                <nav>
	                              <div class="nav nav-tabs" id="nav-tab" role="tablist">
		                            <a class="nav-item nav-link active" id="nav-addRate-tab" data-toggle="tab" href="#nav-addRate" role="tab" aria-controls="nav-addRate" aria-selected="true">Add Merchant Rate</a>
		                            <a class="nav-item nav-link" id="nav-addFutureRate-tab" data-toggle="tab" href="#nav-addFutureRate" role="tab" aria-controls="nav-addFutureRate" aria-selected="false">New Effective Rate</a>
		                            <%--<a class="nav-item nav-link" id="nav-addPromotion-tab" data-toggle="tab" href="#nav-addPromotion" role="tab" aria-controls="nav-addPromotion" aria-selected="false">Add Promotion Rate</a>--%>
	                              </div>
	                            </nav>
                                <div class="tab-content" id="nav-tabContent">
                                    <div class="tab-pane fade show active" id="nav-addRate" role="tabpanel" aria-labelledby="nav-addRate-tab">
                                        <div id="merchantChannel"></div>
                                        <div class="card-body text-right">
                                            <asp:TextBox ID="TextBox1" style="display:none" runat="server" CssClass="form-control"></asp:TextBox>
                                            <asp:Button ID="Button" runat="server" Text="Add" CssClass="btn btn-primary btn-xl" OnClientClick="dataValidation()" OnClick="btn_Add_New_Rate" />
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="nav-addFutureRate" role="tabpanel" aria-labelledby="nav-addFutureRate-tab">
                                        <div id="merchantFutureRate"></div>
                                        <div class="card-body text-right">
                                            <asp:TextBox ID="TextBox2" style="display:none" runat="server" CssClass="form-control"></asp:TextBox>
                                            <%--<asp:Button ID="Button1" runat="server" Text="Add" CssClass="btn btn-primary btn-xl" OnClientClick="dataValidation()" OnClick="btn_AddRate_Click" />--%>
                                            <asp:Button ID="Button1" runat="server" Text="Add" CssClass="btn btn-primary btn-xl" OnClientClick="if (!futureRateInputValidation()) { return false;};" OnClick="btn_Add_Future_Rate"/>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="nav-addPromotion" role="tabpanel" aria-labelledby="nav-addPromotion-tab">
                                        <div class="row">
			                                <div class="col-md-12">
				                                <div class="card card-border-color card-border-color-primary">
					                                <div class="card-header card-header-divider font-weight-bold">
						                                Add Merchant Promotion Rate
					                                </div>
					                                <div class="card-body">
						                                <div class="filter-container">
                                                            <div class="row">
                                                                <div class="col-4">
									                                <label class="control-label font-weight-bold">Merchant</label>
								                                </div>
                                                                <div class="col-6">
                                                                    <div class="filter-container">
                                                                        <asp:DropDownList ID="DropDownList1" runat="server" CssClass="select2" onchange="getChannel()"></asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                                <div class="col-2">
                                                                    <asp:Button ID="btn_GetInfo" runat="server" Text="Get Info" CssClass="btn btn-primary btn-xl" OnClick="btn_GetInfo_Click" />
                                                                </div>
                                                            </div>
							                                <div class="row">
								                                <div class="col-4">
									                                <label class="control-label font-weight-bold">Type</label>
								                                </div>
								                                <div class="col-6">
									                                <asp:DropDownList ID="DD_PymtMethod" runat="server" CssClass="select2" OnSelectedIndexChanged="OnChanged_DD_PymtMethod" AutoPostBack="true" Enabled="false"></asp:DropDownList>
								                                </div>
							                                </div>
							                                <div class="row">
								                                <div class="col-4">
									                                <label class="control-label font-weight-bold">Channel</label>
								                                </div>
								                                <div class="col-6">
									                                <asp:DropDownList ID="DD_Channel" runat="server" CssClass="select2" OnSelectedIndexChanged="OnChanged_DD_Channel" AutoPostBack="true" Enabled="false"></asp:DropDownList>
								                                </div>
							                                </div>
                                                            <div class="row">
								                                <div class="col-4">
                                                                    <label class="control-label font-weight-bold">Start Date</label>
								                                </div>
                                                                <div class="col-6">
                                                                    <asp:TextBox ID="startDate" runat="server" data-date-format="yyyy-mm-dd" data-min-view="2" type="date" CssClass="form-control date"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="row">
								                                <div class="col-4">
                                                                    <label class="control-label font-weight-bold">End Date</label>
								                                </div>
                                                                <div class="col-6">
                                                                    <asp:TextBox ID="endDate" runat="server" data-date-format="yyyy-mm-dd" data-min-view="2" type="date" CssClass="form-control date"></asp:TextBox>
                                                                </div>
                                                            </div>
							                                <div class="row">
								                                <div class="col-4">
									                                <label class="control-label font-weight-bold">Rate (%)</label>
								                                </div>
								                                <div class="col-6">
									                                <asp:TextBox ID="txt_Rate" placeholder="(Mandatory) Rate (%)" runat="server" CssClass="form-control" Onkeypress="return validateFloatKeyPress(this,event)"></asp:TextBox>
								                                </div>
							                                </div>
							                                <div class="row">
								                                <div class="col-4">
									                                <label class="control-label font-weight-bold">Fixed Rate</label>
								                                </div>
								                                <div class="col-6">
									                                <asp:TextBox ID="txt_FixedRate" placeholder="(Mandatory) Fixed Rate" runat="server" CssClass="form-control" Onkeypress="return validateFloatKeyPress(this,event)"></asp:TextBox>
								                                </div>
							                                </div>
							                                <div class="row">
								                                <div class="col-4">
									                                <label class="control-label font-weight-bold">Currency</label>
								                                </div>
								                                <div class="col-6">
									                                <asp:DropDownList ID="DD_Currency" runat="server" CssClass="select2"></asp:DropDownList>
								                                </div>
								                                <div class="col-2">
									                                <asp:Button ID="btn_AddRate" runat="server" Text="Add" CssClass="btn btn-primary btn-xl" OnClick="btn_AddRate_Click" />
								                                </div>
							                                </div>
						                                </div>
					                                </div>
				                                </div>
			                                </div>
		                                </div>
                                    </div>
                                </div>
                                
                                <%-- DEL START KENT LOONG [REMOVE OLD ADD RATE] --%>
                                <%--<div style="display:none" class="card-body">
                                    <div class="filter-container">
                                        <div class="row">
                                            <div class="col-4">
                                                <label class="control-label font-weight-bold">Type</label>
                                            </div>
                                            <div class="col-6">--%>
                                                <%--CHG START DANIEL 20190116 [Add OnSelectedIndexChanged and AutoPostBack]--%>
                                                <%--<asp:DropDownList ID="DD_PymtMethod" runat="server" CssClass="select2"></asp:DropDownList>--%>
                                                <%--<asp:DropDownList ID="DD_PymtMethod" runat="server" CssClass="select2" OnSelectedIndexChanged="OnChanged_DD_PymtMethod" AutoPostBack="true" Enabled="false"></asp:DropDownList>--%>
                                                <%--CHG E N D DANIEL 20190116 [Add OnSelectedIndexChanged and AutoPostBack]--%>
                                            <%--</div>--%>
                                            <%--DEL START DANIEL 20190116 [Remove Get button]--%>
                                            <%--  <div class="col-2">
                                                <asp:Button ID="btn_GetChannel" runat="server" Text="Get" CssClass="btn btn-primary btn-xl" OnClick="btn_GetChannel_Click" />
                                            </div>--%>
                                            <%--DEL E N D DANIEL 20190116 [Remove Get button]--%>
                                        <%--</div>
                                        <div class="row">
                                            <div class="col-4">
                                                <label class="control-label font-weight-bold">Channel</label>
                                            </div>
                                            <div class="col-6">--%>

                                                <%--CHG START DANIEL 20190116 [Add OnSelectedIndexChanged and AutoPostBack]--%>
                                                <%--<asp:DropDownList ID="DD_Channel" runat="server" CssClass="select2"></asp:DropDownList>--%>
                                                <%--<asp:DropDownList ID="DD_Channel" runat="server" CssClass="select2" OnSelectedIndexChanged="OnChanged_DD_Channel" AutoPostBack="true" Enabled="false"></asp:DropDownList>--%>
                                                <%--CHG E N D DANIEL 20190116 [Add OnSelectedIndexChanged and AutoPostBack]--%>
                                            <%--</div>
                                        </div>
                                        <div class="row">--%>
                                            <%--CHG START DANIEL 20181205 [Add [%] to prompt user to input in percentage]--%>
                                            <%--   <div class="col-4">
                                                <label class="control-label font-weight-bold">Rate</label>
                                            </div>
                                            <div class="col-6">
                                                <asp:TextBox ID="txt_Rate" placeholder="(Mandatory) Rate" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>--%>


                                            <%--<div class="col-4">
                                                <label class="control-label font-weight-bold">Rate (%)</label>
                                            </div>
                                            <div class="col-6">--%>
                                                <%--CHG START DANIEL 20190109 [Add onkeypress to isNumberKey function]--%>
                                                <%--<asp:TextBox ID="txt_Rate" placeholder="(Mandatory) Rate (%)" runat="server" CssClass="form-control"></asp:TextBox>--%>

                                                <%--CHG START DANIEL 20190204 [Restrict to 2 decimal places and integer only]--%>
                                                <%--<asp:TextBox ID="txt_Rate" placeholder="(Mandatory) Rate (%)" runat="server" CssClass="form-control" Onkeypress="return isNumberKey(event)"></asp:TextBox>--%>
                                                <%--<asp:TextBox ID="txt_Rate" placeholder="(Mandatory) Rate (%)" runat="server" CssClass="form-control" Onkeypress="return validateFloatKeyPress(this,event)"></asp:TextBox>--%>
                                                <%--CHG E N D DANIEL 20190204 [Restrict to 2 decimal places and integer only]--%>


                                                <%--CHG E N D DANIEL 20190109 [Add onkeypress to isNumberKey function]--%>
                                            <%--</div>
                                        </div>--%>
                                        <%--CHG E N D DANIEL 20181205 [Add [%] to prompt user to input in percentage]--%>

                                        <%--<div class="row">
                                            <div class="col-4">
                                                <label class="control-label font-weight-bold">Fixed Rate</label>
                                            </div>
                                            <div class="col-6">--%>
                                                <%--CHG START DANIEL 20190109 [Add onkeypress to isNumberKey function]--%>
                                                <%--<asp:TextBox ID="txt_FixedRate" placeholder="(Mandatory) Fixed Rate" runat="server" CssClass="form-control"></asp:TextBox>--%>

                                                <%--CHG START DANIEL 20190204 [Restrict to 2 decimal places and integer only]--%>
                                                <%--<asp:TextBox ID="txt_FixedRate" placeholder="(Mandatory) Fixed Rate" runat="server" CssClass="form-control" Onkeypress="return isNumberKey(event)"></asp:TextBox>--%>
                                                <%--<asp:TextBox ID="txt_FixedRate" placeholder="(Mandatory) Fixed Rate" runat="server" CssClass="form-control" Onkeypress="return validateFloatKeyPress(this,event)"></asp:TextBox>--%>
                                                <%--CHG E N D DANIEL 20190204 [Restrict to 2 decimal places and integer only]--%>
                                                <%--CHG E N D DANIEL 20190109 [Add onkeypress to isNumberKey function]--%>
                                            <%--</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-4">
                                                <label class="control-label font-weight-bold">Currency</label>
                                            </div>
                                            <div class="col-6">
                                                <asp:DropDownList ID="DD_Currency" runat="server" CssClass="select2"></asp:DropDownList>
                                            </div>
                                            <div class="col-2">
                                                <asp:Button ID="btn_AddRate" runat="server" Text="Add" CssClass="btn btn-primary btn-xl" OnClick="btn_AddRate_Click" />
                                            </div>
                                        </div>
                                    </div>
                                </div>--%>
                                <%-- DEL E N D KENT LOONG 20190218 [REMOVE OLD ADD RATE] --%>
                            </div>
                        </div>
                    </div>
                    <%-- DEL START KENT LOONG 20190218 [REMOVE MERCHANT RATE] --%>
                    <%--<div class="row">
                        <div class=" col-md-12">
                            <div class="card card-border-color card-border-color-primary">
                                <div class="card-header card-header-divider font-weight-bold">
                                    Merchant Rate
                                </div>
                                <div class="card-body">
                                    <div>
                                        <asp:Repeater ID="RP_Rate" runat="server">
                                            <HeaderTemplate>
                                                <table id="table1" class="table table-striped table-hover table-fw-widget">
                                                    <tr class="odd gradeX">
                                                        <th>Payment Method</th>
                                                        <th>Channel</th>--%>
                                                        <%--CHG START DANIEL 20181211 [Change Rate to Rate (%)]--%>
                                                        <%--<th>Rate</th>
                                                        <th>Rate (%)</th>--%>
                                                        <%--CHG E N D DANIEL 20181211 [Change Rate to Rate (%)]--%>
                                                        <%--<th>Fixed Fee</th>
                                                        <th>Currency</th>
                                                    </tr>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr class="odd gradeX">
                                                    <td><%#  Eval("PymtMethod") %></td>
                                                    <td><%#  Eval("HostName") %></td>
                                                    <td><%#  Eval("Rate") %></td>
                                                    <td><%#  Eval("FixedFee") %></td>
                                                    <td><%#  Eval("Currency") %></td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>--%>
                    <%-- DEL E N D KENT LOONG 20190218 [REMOVE MERCHANT RATE] --%>
                </div>
            </div>
            <div class="text-center" id="footer">
                Copyright © 2019 Paydibs Sdn. Bhd. All Rights Reserved.
            </div>
        </div>
    </form>
    <script src="../../assets/lib/customize/General.js"></script>
    <script src="../../assets/lib/customize/DisplayMerchantRateMain.js"></script>
    <script src="../../assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/bootstrap/dist/js/bootstrap.bundle.min.js" type="text/javascript"></script>
    <script src="../../assets/js/app.js" type="text/javascript"></script>
    <script src="../../assets/lib/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/select2/js/select2.full.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/bootstrap-slider/bootstrap-slider.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="../../assets/js/app-table-filters.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net/js/jquery.dataTables.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-bs4/js/dataTables.bootstrap4.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/dataTables.buttons.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/buttons.html5.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/buttons.flash.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/buttons.print.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/buttons.colVis.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js" type="text/javascript"></script>
    <script src="../../assets/js/app-tables-datatables.js" type="text/javascript"></script>
    <script type="text/javascript">
        
        $(document).ready(function () {
            App.init();
            App.tableFilters();
            App.dataTables();
        });

        $(function () {
            $('#dl_ErrorDialog').hide();
            var msg = $('#<%= HiddenField1.ClientID %>').val();
            var classType = $('#<%= HiddenField2.ClientID %>').val();
            var type = "alert-danger";
            if (msg != "") {
                if (classType != "") type = classType;
                $('#dl_ErrorDialog').addClass(type);
                document.getElementById('dl_message').innerText = msg;
                $('#dl_ErrorDialog').show();
            }
        });

        //ADD START DANIEL 20190109 [Validate integer and decimals]
        /*DEL START DANIEL 20190204 [Remove unnecessary code]
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
        DEL START DANIEL 20190204 [Remove unnecessary code]*/
        //ADD E N D DANIEL 20190109 [Validate integer and decimals]

        //DEL START KENT LOONG 20190218 [REMOVE validateFloatKeyPress]
        //ADD START DANIEL 20190204 [Restrict to 2 decimal places and integer only]
        //function validateFloatKeyPress(el, evt) {

        //    var charCode = (evt.which) ? evt.which : event.keyCode;
        //    var number = el.value.split('.');
        //    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
        //        return false;
        //    }

        //    if (number.length > 1 && charCode == 46) {
        //        return false;
        //    }

        //    var caratPos = getSelectionStart(el);
        //    var dotPos = el.value.indexOf(".");
        //    if (caratPos > dotPos && dotPos > -1 && (number[1].length > 1)) {
        //        return false;
        //    }
        //    return true;
        //}

        //function getSelectionStart(o) {
        //    if (o.createTextRange) {
        //        var r = document.selection.createRange().duplicate()
        //        r.moveEnd('character', o.value.length)
        //        if (r.text == '') return o.value.length
        //        return o.value.lastIndexOf(r.text)
        //    } else return o.selectionStart
        //}
        //ADD E N D DANIEL 20190204 [Restrict to 2 decimal places and integer only]
        //DEL E N D KENT LOONG 20190218 [REMOVE validateFloatKeyPress]
        //ADD START KENT LOONG 20190216 [Add function for get channel data, append channel data and generate json data]
        

      //LOCAL
      /*function callApi(MID) {
            $.ajax({
                type: 'GET', 
                url: 'http://localhost:51360/GetMerchantChannel.aspx?Merchant='+MID, 
                data: {},
                success: function (data) { 
                    data = (data.replace(/<(.|\n)*?>/g, '')).trim();
                    var merchantChannel = JSON.parse(data);
                    console.log(merchantChannel);
                    AppendToAddRate(merchantChannel);
                    AppendToFutureRate(merchantChannel);
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) { 
                    alert("Status: " + textStatus); alert("Error: " + errorThrown); 
                } 
            });
        }*/

        //STAGING
        function callApi(MID) {
              var url = window.location.hostname;
              url = "https://"+ url+'/GetMerchantChannel.aspx?Merchant='+MID;
              $.ajax({
                  type: 'GET',
                  url: url,
                  data: {},
                  success: function (data) {
                      data = (data.replace(/<(.|\n)*?>/g, '')).trim();
                      var merchantChannel = JSON.parse(data);
                      console.log(merchantChannel);
                      AppendToAddRate(merchantChannel);
                      AppendToFutureRate(merchantChannel);
                  },
                  error: function(XMLHttpRequest, textStatus, errorThrown) {
                      alert("Status: " + textStatus); alert("Error: " + errorThrown);
                  }
              });
          }

        $("#btn_GetInfo").click(function () {
            $("#tabID").val("#nav-addPromotion-tab");
        });

        $("#nav-addPromotion-tab").click(function () {
            $("#merchant_DD_list").css("display", "none");
        });

        $("#nav-addRate-tab").click(function () {
            $("#merchant_DD_list").css("display", "block");
        });

        $("#nav-addFutureRate-tab").click(function () {
            $("#merchant_DD_list").css("display", "block");
        });

        $(document).ready(function () {
            var urlParams = new URLSearchParams(window.location.search);
            var tab = document.getElementById("tabID").value;
            
            if (urlParams.get('Merchant') > 0) {
                callApi(urlParams.get('Merchant'));
            }
            else {
                callApi($("#DD_merchant").val());
            }

            $("#TextBox1").val("");
            $("#TextBox2").val("");

            $("#startDate").attr({
                "min": todaydate
            });
            $("#endDate").attr({
                "min": todaydate
            });

            if (tab.length>0) {
                $(tab).tab( 'show' );
            }
        });
        //ADD E N D KENT LOONG 20190216 [Add function for get channel data, append channel data and generate json data]
        
    </script>

</body>
</html>
