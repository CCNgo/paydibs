﻿<%@ page language="C#" autoeventwireup="true" inherits="Backoffice_CMS_EditUserPermission, App_Web_axpnkjn1" %>

<!DOCTYPE html>
<%@ Register TagPrefix="uc" TagName="Left_Side_Bar" Src="~/UserControl/LeftSideBar.ascx" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Paydibs-Checkout Edit User Permission</title>
    <link rel="stylesheet" type="text/css" href="../../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/material-design-icons/css/material-design-iconic-font.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/select2/css/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/bootstrap-slider/css/bootstrap-slider.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/datatables/datatables.net-bs4/css/dataTables.bootstrap4.css" />
    <link rel="stylesheet" href="../../assets/css/app.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:HiddenField ID="HiddenField2" runat="server" />
        <asp:HiddenField ID="HiddenField1" runat="server" />
        <div class="be-wrapper be-collapsible-sidebar be-collapsible-sidebar-collapsed">
            <uc:Left_Side_Bar ID="Left_Side_Bar" runat="server" />
            <div class="be-content">
                <div class="main-content container-fluid">
                    <div role="alert" class="alert alert-contrast alert-dismissible" id="dl_ErrorDialog">
                        <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
                        <div class="message" id="dl_message">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 ">
                            <div class="card card-table card-border-color card-border-color-primary">
                                <div class="card-header">
                                    <span class="font-weight-bold">Edit User Permission</span>
                                </div>
                                <div class="row table-filters-container">
                                    <div class="col-12 col-lg-12 col-xl-6">
                                        <div class="row">
                                            <div class="col-12 col-lg-8 table-filters pb-0 pb-xl-4">
                                                <span class="table-filter-title">Merchant</span>
                                                <div class="filter-container">
                                                    <label class="control-label">Select Merchant:</label>
                                                    <asp:DropDownList ID="DD_merchant" runat="server" CssClass="select2"></asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-12 col-lg-2 table-filters pb-0 pb-xl-4">
                                                <span class="table-filter-title"></span>
                                                <div class="filter-container" style="margin-top: 45px">
                                                    <asp:Button ID="btn_Search" runat="server" Text="Search" CssClass="btn btn-primary btn-xl" OnClick="btn_Search_Click" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row table-filters-container">
                                    <div class="col-12 col-lg-12">
                                        <div class="row">
                                            <div class="col-12 col-lg-2 table-filters pb-0 pb-xl-4">
                                                <span class="table-filter-title">Edit User</span>
                                                <div class="filter-container">
                                                    <label class="control-label">User Group:</label>

                                                    <%--CHG START DANIEL 20190110 [Change OnSelectedIndexChange to OnSelected_UserGroup]--%>
                                                    <%--<asp:DropDownList ID="DD_UserGroup" runat="server" CssClass="select2" Enabled="false" OnSelectedIndexChanged="OnSelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>--%>
                                                    <asp:DropDownList ID="DD_UserGroup" runat="server" CssClass="select2" Enabled="false" OnSelectedIndexChanged="OnSelected_UserGroup" AutoPostBack="true"></asp:DropDownList>
                                                    <%--CHG E N D DANIEL 20190110 [Change OnSelectedIndexChange to OnSelected_UserGroup]--%>
                                                </div>

                                            </div>
                                            <div class="col-12 col-lg-3 table-filters pb-0 pb-xl-4">
                                                <span class="table-filter-title"></span>
                                                <div class="filter-container" style="margin-top: 19px">
                                                    <label class="control-label">User ID:</label>
                                                    <%--CHG START DANIEL 20190110 [Change OnSelectedIndexChange to OnSelected_UserID]--%>
                                                    <%--<asp:DropDownList ID="DD_UserID" runat="server" CssClass="select2" Enabled="false" OnSelectedIndexChanged="OnSelectedIndexChanged_2" AutoPostBack="true"></asp:DropDownList>--%>
                                                    <asp:DropDownList ID="DD_UserID" runat="server" CssClass="select2" Enabled="false" OnSelectedIndexChanged="OnSelected_UserID" AutoPostBack="true"></asp:DropDownList>
                                                    <%--CHG E N D DANIEL 20190110 [Change OnSelectedIndexChange to OnSelected_UserID]--%>
                                                </div>
                                            </div>
                                            <div class="col-12 col-lg-3 table-filters pb-0 pb-xl-4">
                                                <span class="table-filter-title"></span>
                                                <div class="filter-container" style="margin-top: 19px">
                                                    <label class="control-label">Operation Group:</label>
                                                    <%--CHG START DANIEL 20190110 [Change OnSelectedIndexChange to OnSelected_OpsGroup]--%>
                                                    <%--<asp:DropDownList ID="DD_OpsGroup" runat="server" CssClass="select2" Enabled="false" OnSelectedIndexChanged="OnSelectedIndexChanged_3" AutoPostBack="true"></asp:DropDownList>--%>
                                                    <asp:DropDownList ID="DD_OpsGroup" runat="server" CssClass="select2" Enabled="false" OnSelectedIndexChanged="OnSelected_OpsGroup" AutoPostBack="true"></asp:DropDownList>
                                                    <%--CHG E N D DANIEL 20190110 [Change OnSelectedIndexChange to OnSelected_OpsGroup]--%>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive noSwipe">
                                        <table id="table1" class="table table-striped table-hover table-fw-widget">
                                            <thead>
                                                <tr>
                                                    <th>Operation Name</th>
                                                    <th>Permission</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="Repeater2" runat="server">
                                                    <ItemTemplate>
                                                        <tr class="odd gradeX">
                                                            <td>
                                                                <asp:Label ID="lbl_opsName" runat="server" Text='<%#  Eval("OpsName") %>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:CheckBox ID="cb_permission" runat="server" Checked='<%# Convert.ToInt32(Eval("IsUserAllowed"))== 1 ? true: false%>' />
                                                            </td>
                                                            <td style="display: none">
                                                                <asp:Label ID="lbl_bitIdx" runat="server" Text='<%# Eval("BitIdx")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="filter-container" style="margin-left: 85%">
                                <asp:Button ID="btn_update" Text="Update" CssClass="btn btn-primary btn-xl" runat="server" OnClick="btn_update_Click" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center" id="footer">
                Copyright © 2019 Paydibs Sdn. Bhd. All Rights Reserved.
            </div>
        </div>
    </form>
    <script src="../../assets/lib/customize/General.js"></script>
    <script src="../../assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/bootstrap/dist/js/bootstrap.bundle.min.js" type="text/javascript"></script>
    <script src="../../assets/js/app.js" type="text/javascript"></script>
    <script src="../../assets/lib/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/select2/js/select2.full.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/bootstrap-slider/bootstrap-slider.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="../../assets/js/app-table-filters.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net/js/jquery.dataTables.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-bs4/js/dataTables.bootstrap4.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/dataTables.buttons.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/buttons.html5.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/buttons.flash.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/buttons.print.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/buttons.colVis.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js" type="text/javascript"></script>
    <script src="../../assets/js/app-tables-datatables.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            //initialize the javascript
            App.init();
            App.tableFilters();
            App.dataTables();
        });

        $(function () {
            $('#dl_ErrorDialog').hide();
            var msg = $('#<%= HiddenField1.ClientID %>').val();
            var classType = $('#<%= HiddenField2.ClientID %>').val();
            var type = "alert-danger";
            if (msg != "") {
                if (classType != "") type = classType;
                $('#dl_ErrorDialog').addClass(type);
                document.getElementById('dl_message').innerText = msg;
                $('#dl_ErrorDialog').show();
            }
        });
        
    </script>
</body>
</html>
