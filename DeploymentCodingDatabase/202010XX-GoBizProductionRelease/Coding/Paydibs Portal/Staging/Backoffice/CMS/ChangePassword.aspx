﻿<%@ page language="C#" autoeventwireup="true" inherits="ChangePassword, App_Web_axpnkjn1" %>

<!DOCTYPE html>
<%@ Register TagPrefix="uc" TagName="Left_Side_Bar" Src="~/UserControl/LeftSideBar.ascx" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <link rel="shortcut icon" href="assets/img/logo-fav.png" />
    <title>Paydibs-Checkout Change Password</title>
    <link rel="stylesheet" type="text/css" href="../../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/material-design-icons/css/material-design-iconic-font.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/select2/css/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/bootstrap-slider/css/bootstrap-slider.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/datatables/datatables.net-bs4/css/dataTables.bootstrap4.css" />
    <link rel="stylesheet" href="../../assets/css/app.css" type="text/css" />
</head>
<body>
    <form id="Form1" runat="server">
        <asp:HiddenField ID="HiddenField2" runat="server" />
        <asp:HiddenField ID="HiddenField1" runat="server" />

        <div class="be-wrapper be-login">
            <div class="be-wrapper be-collapsible-sidebar be-collapsible-sidebar-collapsed">
                <uc:Left_Side_Bar ID="Left_Side_Bar" runat="server" />
                <div class="be-content">
                    <div class="main-content container-fluid">
                        <div class="splash-container">
                            <div role="alert" class="alert alert-contrast alert-dismissible" id="dl_ErrorDialog">
                                <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
                                <div class="message" id="dl_message">
                                </div>
                            </div>
                            <div class="card card-border-color card-border-color-primary">
                                <div class="card-header">
                                    <img src="../../assets/img/logo-xx.png" alt="logo" width="102" height="" class="logo-img" /><span class="splash-description">Please change your password.</span>
                                </div>
                                <div class="card-body">
                                    <div class="form-group">
                                        <asp:TextBox ID="txt_currentPassword" runat="server" placeholder="(Mandatory) Current Password" CssClass="form-control" TextMode="Password" required=""></asp:TextBox>
                                    </div>
                                    <div class="form-group">
                                        <asp:TextBox ID="txt_password" runat="server" placeholder="(Mandatory) New Password" CssClass="form-control" TextMode="Password" required=""></asp:TextBox>
                                    </div>
                                    <div class="form-group">
                                        <asp:TextBox ID="txt_confirmPassword" runat="server" placeholder="(Mandatory) Confirm New Password" CssClass="form-control" TextMode="Password" required="" data-parsley-equalto="#txt_password"></asp:TextBox>
                                    </div>
                                    <div class="form-group login-submit">
                                        <asp:Button ID="btn_Change" runat="server" Text="Change Password" CssClass="btn btn-primary btn-xl" OnClick="btn_Change_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center" id="footer">
                Copyright © 2018 Paydibs Sdn. Bhd. All Rights Reserved.
            </div>
        </div>
        <script src="../../assets/lib/customize/General.js"></script>
        <script src="../../assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
        <script src="../../assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
        <script src="../../assets/lib/bootstrap/dist/js/bootstrap.bundle.min.js" type="text/javascript"></script>
        <script src="../../assets/js/app.js" type="text/javascript"></script>
        <script src="../../assets/lib/parsley/parsley.min.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                App.init();
                $('form').parsley();
            });

            $(function () {
                $('#dl_ErrorDialog').hide();
                var msg = $('#<%= HiddenField1.ClientID %>').val();
                var classType = $('#<%= HiddenField2.ClientID %>').val();
                if (msg != "") {
                    if (classType == "") classType = "alert-danger";
                    else classType = classType;
                    $('#dl_ErrorDialog').addClass(classType);
                    document.getElementById('dl_message').innerText = msg;
                    $('#dl_ErrorDialog').show();
                }
            });
        </script>
    </form>
</body>
</html>
