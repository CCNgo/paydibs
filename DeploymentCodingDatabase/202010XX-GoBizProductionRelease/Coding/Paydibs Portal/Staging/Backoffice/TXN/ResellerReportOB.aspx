﻿<%@ page language="C#" autoeventwireup="true" inherits="Backoffice_TXN_ResellerReportOB, App_Web_ormeocvd" %>

<!DOCTYPE html>
<%@ Register TagPrefix="uc" TagName="Left_Side_Bar" Src="~/UserControl/LeftSideBar.ascx" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Paydibs-Checkout Transaction Reseller Online Banking</title>
    <link rel="stylesheet" type="text/css" href="../../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/material-design-icons/css/material-design-iconic-font.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/select2/css/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/bootstrap-slider/css/bootstrap-slider.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/datatables/datatables.net-bs4/css/dataTables.bootstrap4.css" />
    <link rel="stylesheet" href="../../assets/css/app.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <%--ADD START DANIEL 20190104 [Add HiddenFields]--%>
        <asp:HiddenField ID="HiddenField2" runat="server" />
        <asp:HiddenField ID="HiddenField1" runat="server" />
        <%--ADD START DANIEL 20190104 [Add HiddenFields]--%>
        <div class="be-wrapper be-collapsible-sidebar be-collapsible-sidebar-collapsed">
            <uc:Left_Side_Bar ID="Left_Side_Bar" runat="server" />
            <div class="be-content">
                <div class="main-content container-fluid">
                    <%--ADD START DANIEL 20190104 [Display error messages]--%>
                    <div role="alert" class="alert alert-contrast alert-dismissible" id="dl_ErrorDialog">
                        <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
                        <div class="message" id="dl_message">
                        </div>
                    </div>
                    <%--ADD E N D DANIEL 20190104 [Display error messages]--%>
                    <div class="row">
                        <div class="col-md-12 ">
                            <div class="card card-table card-border-color card-border-color-primary">
                                <div class="card-header">
                                    <%-- CHG START DANIEL 20181204 [Change title to Reseller Online Banking Report] --%>
                                    <%--<span class="font-weight-bold"> Reseller Transactions Online Banking</span>--%>
                                    <span class="font-weight-bold">Reseller Online Banking Report</span>
                                    <%-- CHG E N D DANIEL 20181204 [Change title to Reseller Online Banking Report] --%>
                                </div>
                                <div class="row table-filters-container">
                                    <div class="col-12 col-lg-12 col-xl-12">
                                        <div class="row">
                                            <div class="col-12 col-lg-3 table-filters pb-0 pb-xl-0">
                                                <span class="table-filter-title">Merchant</span>
                                                <div class="filter-container">
                                                    <asp:DropDownList ID="DD_merchant" runat="server" CssClass="select2"></asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-12 col-lg-3 table-filters pb-0 pb-xl-0">
                                                <span class="table-filter-title">Date</span>
                                                <div class="filter-container">
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <asp:TextBox ID="TB_Since" runat="server" data-date-format="yyyy-mm-dd" data-min-view="2" CssClass="form-control date datetimepicker"></asp:TextBox>
                                                        </div>
                                                        <div class="col-6">
                                                            <asp:TextBox ID="TB_To" runat="server" data-date-format="yyyy-mm-dd" data-min-view="2" CssClass="form-control date datetimepicker"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12 col-lg-6 table-filters pb-xl-0">
                                                <span class="table-filter-title">Status</span>
                                                <div class="filter-container">
                                                    <div class="row">
                                                        <div class="col-3">
                                                            <label class="custom-control custom-checkbox">
                                                                <input type="checkbox" checked="" class="custom-control-input" id="CB_success" runat="server" /><span class="custom-control-label">Success</span>
                                                            </label>
                                                        </div>
                                                        <div class="col-3">
                                                            <label class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="CB_failed" runat="server" /><span class="custom-control-label">Failed</span>
                                                            </label>
                                                        </div>
                                                        <div class="col-3">
                                                            <label class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="CB_pending" runat="server" /><span class="custom-control-label">Pending</span>
                                                            </label>
                                                        </div>
                                                         <div class="col-3">
                                                            <label class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="CB_refunded" runat="server" /><span class="custom-control-label">Refunded</span>
                                                            </label>
                                                        </div>
                                                        <div class="col-3">
                                                            <label class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="CB_other" runat="server" /><span class="custom-control-label">Other</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12 col-lg-12 col-xl-12">
                                        <div class="row">
                                            <div class="col-12 col-lg-12 table-filters pb-0 pb-xl-2">
                                                <%--<span class="table-filter-title"></span>--%>
                                                <div class="row">
                                                    <div class="col-2">
                                                        <button id="btnShowPopup" data-toggle="modal" data-target="#form-bp1" type="button" class="btn btn-primary btn-lg">Advanced Search</button>
                                                    </div>
                                                    <div class="col-5">
                                                    </div>
                                                    <div class="col-2">
                                                        <asp:Button ID="btn_GetReport" runat="server" Text="Get Report" CssClass="btn btn-primary btn-xl" OnClick="btn_GetReport_Click" />
                                                    </div>
                                                    <div class="col-3">
                                                        <asp:Button ID="btn_Download" runat="server" Text="Download Report" CssClass="btn btn-primary btn-xl" OnClick="btn_Download_Click" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!--Form Modals-->
                                        <div id="form-bp1" tabindex="-1" role="dialog" class="modal fade colored-header colored-header-primary">
                                            <div class="modal-dialog full-width">
                                                <div class="modal-content">
                                                    <div class="modal-header modal-header-colored">
                                                        <h4 class="modal-title">Advanced Search</h4>
                                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><span class="mdi mdi-close"></span></button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row no-margin-y">
                                                            <div class="form-group col-sm-2">
                                                                <label class="col-form-label">Payment ID</label>
                                                            </div>
                                                            <div class="form-group col-sm-3">
                                                                <asp:TextBox ID="TB_PaymentID" runat="server" CssClass="form-control" placeholder="Payment ID"></asp:TextBox>
                                                            </div>
                                                            <div class="form-group col-sm-1">
                                                            </div>
                                                            <div class="form-group col-sm-2">
                                                                <label class="col-form-label">Gateway Transaction ID</label>
                                                            </div>
                                                            <div class="form-group col-sm-3">
                                                                <asp:TextBox ID="TB_GatewayTxnID" runat="server" CssClass="form-control" placeholder="Gateway Transaction ID"></asp:TextBox>
                                                            </div>
                                                            <div class="form-group col-sm-1">
                                                            </div>
                                                        </div>
                                                        <div class="row no-margin-y">
                                                            <div class="form-group col-sm-2">
                                                                <label class="col-form-label">Customer Email</label>
                                                            </div>
                                                            <div class="form-group col-sm-3">
                                                                <asp:TextBox ID="TB_CustEmail" runat="server" CssClass="form-control" placeholder="Customer Email"></asp:TextBox>
                                                            </div>
                                                            <div class="form-group col-sm-1">
                                                            </div>
                                                            <div class="form-group col-sm-2">
                                                                <label class="col-form-label">Order Number</label>
                                                            </div>
                                                            <div class="form-group col-sm-3">
                                                                <asp:TextBox ID="TB_OrderId" runat="server" CssClass="form-control" placeholder="Order Number"></asp:TextBox>
                                                            </div>
                                                            <div class="form-group col-sm-1">
                                                            </div>
                                                        </div>
                                                        <div class="row no-margin-y">
                                                            <div class="form-group col-sm-2">
                                                                <label class="col-form-label">Currency</label>
                                                            </div>
                                                            <div class="form-group col-sm-3">
                                                                <asp:DropDownList ID="DD_Currency" runat="server" CssClass="select2"></asp:DropDownList>
                                                            </div>
                                                            <div class="form-group col-sm-1">
                                                            </div>
                                                            <%--ADD START DANIEL 20181129 [Add Channel in the form]--%>
                                                            <div class="form-group col-sm-2">
                                                                <label class="col-form-label">Channel</label>
                                                            </div>
                                                            <div class="form-group col-sm-3">
                                                                <asp:TextBox ID="TB_Channel" runat="server" CssClass="form-control" placeholder="Channel"></asp:TextBox>
                                                            </div>
                                                            <div class="form-group col-sm-1">
                                                            </div>
                                                            <%--ADD E N D DANIEL 20181129 [Add Channel in the form]--%>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <%-- CHG START SUKI 20181204 [OK button onclick event same as Get Report button] --%>
                                                        <%--<asp:Button ID="btn_OK" runat="server" Text="OK" CssClass="btn btn-primary btn-xl" OnClick="btn_OK_Click" />--%>
                                                        <asp:Button ID="btn_OK" runat="server" Text="OK" CssClass="btn btn-primary btn-xl" OnClick="btn_GetReport_Click" />
                                                        <%-- CHG E N D SUKI 20181204 [OK button onclick event same as Get Report button] --%>
                                                        <asp:Button ID="btn_Cancel" runat="server" Text="Cancel" CssClass="btn btn-primary btn-xl" OnClick="btn_Cancel_Click" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Form Modals-->
                                    </div>
                                </div>
                                <!-- Content Goes Here
================================================== -->
                                <div class="card-body">
                                    <div class="table-responsive noSwipe">
                                        <%--CHG START DANIEL 20190211 [Change table id to table4]--%>
                                        <%--<table id="table1" class="table table-striped table-hover table-fw-widget">--%>
                                        <table id="table4" class="table table-striped table-hover table-fw-widget">
                                            <%--CHG E N D DANIEL 20190211 [Change table id to table4]--%>
                                            <thead>
                                                <tr id="TB_column" runat="server">
                                                    <th>Transaction Date</th>
                                                    <th>Merchant</th>
                                                    <th>Order Number</th>
                                                    <th>Payment ID</th>
                                                    <th>Gateway Transaction ID</th>
                                                    <th>Bank Ref. No.</th>
                                                    <th>Currency Code</th>
                                                    <th>Transaction Amount</th>
                                                    <th>Status</th>
                                                    <th>Channel</th>
                                                    <%--ADD START DANIEL 20190102 [Add header for Trading Name]--%>
                                                    <th>Trading Name</th>
                                                    <%--ADD E N D DANIEL 20190102 [Add header for Trading Name]--%>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="Repeater2" runat="server">
                                                    <ItemTemplate>
                                                        <tr class="odd gradeX">
                                                            <td><%# Eval("DateCreated","{0:dd/MM/yyyy HH:mm:ss}") %></td>
                                                            <td><%# Eval("MerchantID") %></td>
                                                            <td><%# Eval("OrderNumber") %></td>
                                                            <td><%# Eval("PaymentID") %></td>
                                                            <td><%# Eval("GatewayTxnID") %></td>
                                                            <td><%# Eval("BankRefNo") %></td>
                                                            <td><%# Eval("CurrencyCode") %></td>
                                                            <td><%# Convert.ToDecimal(Eval("TxnAmt")).ToString("N") %></td>
                                                            <td><%# Eval("Status") %></td>
                                                            <td><%# Eval("Channel") %></td>
                                                            <%--ADD START DANIEL 20190102 [Add field column for TradingName]--%>
                                                            <td><%# Eval("TradingName") %></td>
                                                            <%--ADD E N D DANIEL 20190102 [Add field column for TradingName]--%>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center" id="footer">
                Copyright © 2019 Paydibs Sdn. Bhd. All Rights Reserved.
            </div>
        </div>
    </form>
    <script src="../../assets/lib/customize/General.js"></script>
    <script src="../../assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/bootstrap/dist/js/bootstrap.bundle.min.js" type="text/javascript"></script>
    <script src="../../assets/js/app.js" type="text/javascript"></script>
    <script src="../../assets/lib/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/select2/js/select2.full.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/bootstrap-slider/bootstrap-slider.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="../../assets/js/app-table-filters.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net/js/jquery.dataTables.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-bs4/js/dataTables.bootstrap4.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/dataTables.buttons.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/buttons.html5.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/buttons.flash.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/buttons.print.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/buttons.colVis.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js" type="text/javascript"></script>
    <script src="../../assets/js/app-tables-datatables.js" type="text/javascript"></script>
    <%--CHG START DANIEL 20190104 [Add javascript to display messages on page]--%>
    <%--    <script type="text/javascript">
        $(document).ready(function () {
            //initialize the javascript
            App.init();
            App.tableFilters();
            App.dataTables();
        });
    </script>--%>

    <script type="text/javascript">
        $(document).ready(function () {
            //initialize the javascript
            App.init();
            App.tableFilters();
            App.dataTables();
        });

        $(function () {
            $('#dl_ErrorDialog').hide();
            var msg = $('#<%= HiddenField1.ClientID %>').val();
            var classType = $('#<%= HiddenField2.ClientID %>').val();
            var type = "alert-danger";
            if (msg != "") {
                if (classType != "") type = classType;
                $('#dl_ErrorDialog').addClass(type);
                document.getElementById('dl_message').innerText = msg;
                $('#dl_ErrorDialog').show();
            }
        });
    </script>
    <%--CHG E N D DANIEL 20190104 [Add javascript to display messages on page]--%>
</body>
