﻿<%@ page language="C#" autoeventwireup="true" inherits="Backoffice_TXN_ResellerMarginReport, App_Web_ormeocvd" %>

<!DOCTYPE html>
<%@ Register TagPrefix="uc" TagName="Left_Side_Bar" Src="~/UserControl/LeftSideBar.ascx" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Paydibs-Checkout Reseller Margin Report</title>
    <link rel="stylesheet" type="text/css" href="../../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/material-design-icons/css/material-design-iconic-font.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/select2/css/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/bootstrap-slider/css/bootstrap-slider.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/datatables/datatables.net-bs4/css/dataTables.bootstrap4.css" />
    <link rel="stylesheet" href="../../assets/css/app.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:HiddenField ID="HiddenField2" runat="server" />
        <asp:HiddenField ID="HiddenField1" runat="server" />
        <div class="be-wrapper be-collapsible-sidebar be-collapsible-sidebar-collapsed">
            <uc:Left_Side_Bar ID="Left_Side_Bar" runat="server" />
            <div class="be-content">
                <div class="main-content container-fluid">
                    <div role="alert" class="alert alert-contrast alert-dismissible" id="dl_ErrorDialog">
                        <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
                        <div class="message" id="dl_message">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 ">
                            <div class="card card-table card-border-color card-border-color-primary">
                                <div class="card-header">
                                    <span class="font-weight-bold">Reseller Margin Report</span>
                                </div>
                                <div class="row table-filters-container">
                                    <div class="col-12 col-lg-12 col-xl-12">
                                        <div class="row">
                                            <div class="col-12 col-lg-3 table-filters pb-0 pb-xl-0">
                                                <span class="table-filter-title">Reseller</span>
                                                <div class="filter-container">
                                                    <label class="control-label">Select a Reseller:</label>
                                                    <asp:DropDownList ID="DD_merchant" runat="server" CssClass="select2"></asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-12 col-lg-3 table-filters pb-0 pb-xl-0">
                                                <span class="table-filter-title">Date</span>
                                                <div class="filter-container">
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <label class="control-label">Since:</label>
                                                            <asp:TextBox ID="TB_Since" runat="server" data-date-format="yyyy-mm-dd" data-min-view="2" CssClass="form-control date datetimepicker"></asp:TextBox>
                                                        </div>
                                                        <div class="col-6">
                                                            <label class="control-label">To:</label>
                                                            <asp:TextBox ID="TB_To" runat="server" data-date-format="yyyy-mm-dd" data-min-view="2" CssClass="form-control date datetimepicker"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <%--ADD START DANIEL 20190313 [Add dropdownlist for Currency]--%>
                                            <div class="col-7 col-lg-2 table-filters pb-0 pb-xl-0">
                                                <span class="table-filter-title">Currency</span>
                                                <div class="filter-container">
                                                    <label class="control-label">Select Currency:</label>
                                                    <asp:DropDownList ID="DD_Currency" runat="server" CssClass="select2"></asp:DropDownList>
                                                </div>
                                            </div>
                                            <%--ADD E N D DANIEL 20190313 [Add dropdownlist for Currency]--%>


                                            <div class="col-12 col-lg-3 table-filters pb-0 pb-xl-0">
                                                <span class="table-filter-title"></span>
                                                <div class="filter-container" style="margin-top: 45px">
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <asp:Button ID="btn_GetReport" runat="server" Text="Get Report" CssClass="btn btn-primary btn-xl" OnClick="btn_GetReport_Click" />
                                                        </div>
                                                        <div class="col-6">
                                                            <asp:Button ID="btn_Download" runat="server" Text="Download Report" CssClass="btn btn-primary btn-xl" OnClick="btn_Download_Click" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive noSwipe">
                                        <%--CHG START DANIEL 20190211 [Change table id to table4]--%>
                                        <%--<table id="table1" class="table table-striped table-hover table-fw-widget">--%>
                                        <table id="table4" class="table table-striped table-hover table-fw-widget">
                                            <%--CHG E N D DANIEL 20190211 [Change table id to table4]--%>
                                            <thead>
                                                <tr id="TB_column" runat="server">
                                                    <th>Transaction Date</th>
                                                    <th>Merchant</th>
                                                    <th>Payment ID</th>
                                                    <th>Gateway Transaction ID</th>
                                                    <th>Bank Ref. No.</th>
                                                    <th>Channel</th>
                                                    <th>Currency Code</th>
                                                    <th>Transaction Amount</th>
                                                    <th>Transacted Currency Code</th>
                                                    <th>Transacted Amount</th>
                                                    <th>Margin Rate</th>
                                                    <th>Fixed Fee</th>
                                                    <th>Rate Amount</th>
                                                    <th>Fixed Fee Amount</th>
                                                    <th>Txn Fee</th>
                                                    <%--<th>Total</th>  Commented By Daniel 11 Apr 2019--%> 
                                                    <th>Trading Name</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="Repeater2" runat="server">
                                                    <ItemTemplate>
                                                        <tr class="odd gradeX">
                                                            <td><%# Eval("DateCreated","{0:dd/MM/yyyy HH:mm:ss}") %></td>
                                                            <td><%# Eval("MerchantID") %></td>

                                                            <%--CHG START DANIEL 20190313 [Change MerchantTxnID  to PaymentID]--%>
                                                            <%--<td><%# Eval("MerchantTxnID") %></td>--%>
                                                            <td><%# Eval("PaymentID") %></td>
                                                            <%--CHG E N D DANIEL 20190313 [Change MerchantTxnID  to PaymentID]--%>
                                                            <td><%# Eval("GatewayTxnID") %></td>
                                                            <td><%# Eval("BankRefNo") %></td>
                                                            <td><%# Eval("HostDesc") %></td>
                                                            <td><%# Eval("CurrencyCode") %></td>
                                                            <td><%# Convert.ToDecimal(Eval("TxnAmt")).ToString("N") %></td>
                                                            <td><%# Eval("TransactedCurrencyCode") %></td>
                                                            <td><%# Convert.ToDecimal(Eval("TransactedAmt")).ToString("N") %></td>
                                                            <td><%# Convert.ToDecimal(Eval("MarginRate")) %></td>
                                                            <td><%# Convert.ToDecimal(Eval("MerchantFixedFee")).ToString("N") %></td>
                                                            <td><%# Convert.ToDecimal(Eval("MerchantRateAmt")).ToString("N") %></td>
                                                            <td><%# Convert.ToDecimal(Eval("MerchantFFeeAmt")).ToString("N") %></td>
                                                            <td><%# Convert.ToDecimal(Eval("NetFee")).ToString("N") %></td>
                                                         <%--   <td><%# Convert.ToDecimal(Eval("Total")).ToString("N") %></td>  Commented By Daniel on 11 Apr 2019--%>
                                                            <td><%# Eval("TradingName") %></td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center" id="footer">
                Copyright © 2019 Paydibs Sdn. Bhd. All Rights Reserved.
            </div>
        </div>
    </form>
    <script src="../../assets/lib/customize/General.js"></script>
    <script src="../../assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/bootstrap/dist/js/bootstrap.bundle.min.js" type="text/javascript"></script>
    <script src="../../assets/js/app.js" type="text/javascript"></script>
    <script src="../../assets/lib/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/select2/js/select2.full.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/bootstrap-slider/bootstrap-slider.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="../../assets/js/app-table-filters.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net/js/jquery.dataTables.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-bs4/js/dataTables.bootstrap4.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/dataTables.buttons.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/buttons.html5.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/buttons.flash.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/buttons.print.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/buttons.colVis.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js" type="text/javascript"></script>
    <script src="../../assets/js/app-tables-datatables.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            //initialize the javascript
            App.init();
            App.tableFilters();
            App.dataTables();
        });

        $(function () {
            $('#dl_ErrorDialog').hide();
            var msg = $('#<%= HiddenField1.ClientID %>').val();
            var classType = $('#<%= HiddenField2.ClientID %>').val();
            var type = "alert-danger";
            if (msg != "") {
                if (classType != "") type = classType;
                $('#dl_ErrorDialog').addClass(type);
                document.getElementById('dl_message').innerText = msg;
                $('#dl_ErrorDialog').show();
            }
        });
    </script>
</body>
</html>
