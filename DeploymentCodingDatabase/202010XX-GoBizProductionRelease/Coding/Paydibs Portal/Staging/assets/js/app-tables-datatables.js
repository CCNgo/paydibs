var App = (function () {
    'use strict';

    App.dataTables = function () {

        //We use this to apply style to certain elements
        $.extend(true, $.fn.dataTable.defaults, {
            dom:
                "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6'f>>" +
                "<'row be-datatable-body'<'col-sm-12'tr>>" +
                "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>"
        });

        //ADD START DANIEL 20190118 [Extend oSort to create custom sorting function]
        $.extend(jQuery.fn.dataTableExt.oSort, {
            "datetime-my-pre": function (a) {
                var x;
                if ($.trim(a) != '') {
                    var frDatea = $.trim(a).split(' ');
                    var frTimea = frDatea[1].split(':');
                    var frDatea2 = frDatea[0].split('/');
                    var year = frDatea2[2] * 60 * 24 * 366;
                    var month = frDatea2[1] * 60 * 24 * 31;;
                    var day = frDatea2[0] * 60 * 24;
                    var hour = frTimea[0];
                    var minute = frTimea[1];
                    var second = frTimea[2];
                    var ampm = frDatea[2];

                    if (day < 10) {
                        day = '0' + day;
                    }

                    if (ampm == 'PM' && hour < 12) {
                        hour = parseInt(hour, 10) + 12;
                    }

                    if (hour < 10) {
                        hour = '0' + hour;
                    }
                    var hour1 = hour * 60;
                    x = (year + month + day + hour1 + minute + second) * 1;
                } else {
                    x = 10000000000000;
                }

                return x;
            },

            "datetime-my-asc": function (a, b) {
                return ((a < b) ? -1 : ((a > b) ? 1 : 0));
            },

            "datetime-my-desc": function (a, b) {
                return ((a < b) ? 1 : ((a > b) ? -1 : 0));
            }
        });
        //ADD E N D DANIEL 20190118 [Extend oSort to create custom sorting function]

        $("#table1").dataTable();

        //Remove search & paging dropdown
        $("#table2").dataTable({
            pageLength: 6,
            dom: "<'row be-datatable-body'<'col-sm-12'tr>>" +
                "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>"
        });

        //Enable toolbar button functions
        $("#table3").dataTable({
            buttons: [
                'copy', 'excel', 'pdf', 'print'
            ],
            "lengthMenu": [[6, 10, 25, 50, -1], [6, 10, 25, 50, "All"]],
            dom: "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6 text-right'B>>" +
                "<'row be-datatable-body'<'col-sm-12'tr>>" +
                "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>"
        });

        //ADD START DANIEL 20190204 [Add in attributes for ID: table4. Applies at first index header of the table]
        $("#table4").dataTable({
            aoColumnDefs: [{ "sType": "datetime-my", "aTargets": [0] }],
            aaSorting: [[0, "desc"]],
            "bStateSave": true
        });
        //ADD E N D DANIEL 20190204 [Add in attributes for ID: table1. Applies at first index header of the table]
    };

    return App;
})(App || {});
