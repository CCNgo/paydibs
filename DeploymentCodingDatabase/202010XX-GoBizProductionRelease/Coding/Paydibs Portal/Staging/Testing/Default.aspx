﻿<%@ page language="C#" autoeventwireup="true" inherits="Testing_Default, App_Web_jveefbay" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>


    <form name="frmTestPay" method="post" action="<%= apiURL %>">
        <input type="hidden" name="TxnType" value="<%= TxnType %>">
        <input type="hidden" name="MerchantID" value="<%= MerchantID %>">
        <input type="hidden" name="MerchantPymtID" value="<%= MerchantPymtID %>">
        <input type="hidden" name="MerchantOrdID" value="<%= MerchantOrdID %>">
        <input type="hidden" name="MerchantOrdDesc" value="<%= MerchantOrdDesc %>">
        <input type="hidden" name="MerchantTxnAmt" value="<%= MerchantTxnAmt %>">
        <input type="hidden" name="MerchantCurrCode" value="<%= MerchantCurrCode %>">
        <input type="hidden" name="MerchantRURL" value="<%= MerchantRURL %>">
        <input type="hidden" name="CustIP" value="<%= CustIP %>">
        <input type="hidden" name="CustName" value="<%= CustName %>">
        <input type="hidden" name="CustEmail" value="<%= CustEmail %>">
        <input type="hidden" name="CustPhone" value="<%= CustPhone %>">
        <input type="hidden" name="Sign" value="<%= Sign %>">
        <input type="hidden" name="MerchantCallBackURL" value="<%= MerchantCallBackURL %>">
        <input type="submit" value="Pay Now" />
    </form>


    <form id="form1" runat="server">
        <asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click" />
        <asp:Button ID="Button2" runat="server" Text="TEST QUERY" OnClick="Button2_Click" />
    </form>
</body>
</html>
