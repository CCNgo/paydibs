﻿<%@ page language="C#" autoeventwireup="true" inherits="Backoffice_MMGMT_AddChannel, App_Web_ufpja5qk" validaterequest="false" %>

<!DOCTYPE html>
<%@ Register TagPrefix="uc" TagName="Left_Side_Bar" Src="~/UserControl/LeftSideBar.ascx" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Paydibs-Checkout Add Channel</title>
    <link rel="stylesheet" type="text/css" href="../../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/material-design-icons/css/material-design-iconic-font.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/select2/css/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/bootstrap-slider/css/bootstrap-slider.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/datatables/datatables.net-bs4/css/dataTables.bootstrap4.css" />
    <link rel="stylesheet" href="../../assets/css/app.css" type="text/css" />

    <style>
        a:hover {
            color: #e00c0c;
        }

        a {
            color: #980505;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:HiddenField ID="HiddenField2" runat="server" />
        <asp:HiddenField ID="HiddenField1" runat="server" />
        <div class="be-wrapper be-collapsible-sidebar be-collapsible-sidebar-collapsed">
            <uc:Left_Side_Bar ID="Left_Side_Bar" runat="server" />
            <div class="be-content">
                <div class="main-content container-fluid">
                    <div role="alert" class="alert alert-contrast alert-dismissible" id="dl_ErrorDialog">
                        <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
                        <div class="message" id="dl_message">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 ">
                            <div class="card card-table card-border-color card-border-color-primary">
                                <div class="row table-filters-container">
                                    <div class="col-12 col-lg-12 col-xl-6">
                                        <div class="row">
                                            <div class="col-12 col-lg-8 table-filters pb-0 pb-xl-4">
                                                <span class="table-filter-title">Merchant</span>
                                                <div class="filter-container">
                                                    <label class="control-label">Select Merchant:</label>
                                                    <asp:DropDownList ID="DD_merchant" runat="server" CssClass="select2"></asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-12 col-lg-2 table-filters pb-0 pb-xl-4">
                                                <span class="table-filter-title"></span>
                                                <div class="filter-container" style="margin-top: 45px">
                                                    <asp:Button ID="btn_GetInfo" runat="server" Text="Get Info" CssClass="btn btn-primary btn-xl" OnClick="btn_GetInfo_Click" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card card-border-color card-border-color-primary">
                                <div class="card-body">
                                    <div class="filter-container">
                                        <div class="form-group row">
                                            <label class="col-12 col-sm-3 col-form-label text-left">Type</label>
                                            <div class="col-12 col-sm-8 col-lg-4">
                                                <%--CHG START DANIEL 20190122 [Add OnSelectedIndexChanged and AutoPostBack]--%>
                                                <%--<asp:DropDownList ID="DD_PymtMethod" runat="server" CssClass="select2"></asp:DropDownList>--%>
                                                <asp:DropDownList ID="DD_PymtMethod" runat="server" CssClass="select2" OnSelectedIndexChanged="OnChanged_DD_PymtMethod" AutoPostBack="true" Enabled="false"></asp:DropDownList>
                                                <%--CHG E N D DANIEL 20190122 [Add OnSelectedIndexChanged and AutoPostBack]--%>
                                            </div>
                                            <%--DEL START DANIEL 20190122 [Remove GetChannel button]--%>
                                            <%-- <div class="col-12 col-sm-8 col-lg-2">
                                                <asp:Button ID="btn_GetChannel" runat="server" Text="Get Channel" CssClass="btn btn-primary btn-xl" OnClick="btn_GetChannel_Click" />
                                            </div>--%>
                                            <%--DEL E N D DANIEL 20190122 [Remove GetChannel button]--%>
                                        </div>

                                        <%--Added By Daniel 22 APR 2019--%>
                                          <div class="form-group row">
                                            <label class="col-12 col-sm-3 col-form-label text-left">Currency</label>
                                            <div class="col-12 col-sm-8 col-lg-4">
                                                <asp:DropDownList ID="DD_CurrencyCode" runat="server" CssClass="select2" OnSelectedIndexChanged="OnChanged_DD_Currency" AutoPostBack="true" Enabled="false"></asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-12 col-sm-3 col-form-label text-left">Channel</label>
                                            <div class="col-12 col-sm-8 col-lg-4">
                                                <%--CHG START DANILE 20190122 [Add OnSelectedIndexChanged and AutoPostBack]--%>
                                                <%--<asp:DropDownList ID="DD_Channel" runat="server" CssClass="select2"></asp:DropDownList>--%>
                                                <asp:DropDownList ID="DD_Channel" runat="server" CssClass="select2" OnSelectedIndexChanged="OnChanged_DD_Channel" AutoPostBack="true" Enabled="false"></asp:DropDownList>
                                                <%--CHG E N D DANILE 20190122 [Add OnSelectedIndexChanged and AutoPostBack]--%>
                                            </div>
                                            <%--DEL START DANIEL 20190122 [Remove GetCredential button]--%>
                                            <%--   <div class="col-12 col-sm-8 col-lg-2">
                                                <asp:Button ID="btn_GetCredential" runat="server" Text="Get Credential" CssClass="btn btn-primary btn-xl" OnClick="btn_GetCredential_Click" />
                                            </div>--%>
                                            <%--DEL E N D DANIEL 20190122 [Remove GetCredential button]--%>
                                        </div>


                                        <asp:Panel ID="Panel_Credential" runat="server" Visible="false">
                                            <div class="form-group row">
                                                <asp:Label ID="LB_Credential1" class="col-12 col-sm-3 col-form-label text-left" runat="server" Text="Credential 1" Visible="false"></asp:Label>
                                                <div class="col-12 col-sm-8 col-lg-4">
                                                    <asp:TextBox ID="txt_Credential1" placeholder="(Mandatory) Credential 1" runat="server" CssClass="form-control" Visible="false"></asp:TextBox>
                                                    <asp:HiddenField ID="hid_Encrypt1" runat="server" />
                                                    <asp:HiddenField ID="hid_Hex1" runat="server" />
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <asp:Label ID="LB_Credential2" class="col-12 col-sm-3 col-form-label text-left" runat="server" Text="Credential 2" Visible="false"></asp:Label>
                                                <div class="col-12 col-sm-8 col-lg-4">
                                                    <asp:TextBox ID="txt_Credential2" placeholder="(Mandatory) Credential 2" runat="server" CssClass="form-control" Visible="false"></asp:TextBox>
                                                    <asp:HiddenField ID="hid_Encrypt2" runat="server" />
                                                    <asp:HiddenField ID="hid_Hex2" runat="server" />
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <asp:Label ID="LB_Credential3" class="col-12 col-sm-3 col-form-label text-left" runat="server" Text="Credential 3" Visible="false"></asp:Label>
                                                <div class="col-12 col-sm-8 col-lg-4">
                                                    <asp:TextBox ID="txt_Credential3" placeholder="(Mandatory) Credential 3" runat="server" CssClass="form-control" Visible="false"></asp:TextBox>
                                                    <asp:HiddenField ID="hid_Encrypt3" runat="server" />
                                                    <asp:HiddenField ID="hid_Hex3" runat="server" />
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <asp:Label ID="LB_Credential4" class="col-12 col-sm-3 col-form-label text-left" runat="server" Text="Credential 4" Visible="false"></asp:Label>
                                                <div class="col-12 col-sm-8 col-lg-4">
                                                    <asp:TextBox ID="txt_Credential4" placeholder="(Mandatory) Credential 4" runat="server" CssClass="form-control" Visible="false"></asp:TextBox>
                                                    <asp:HiddenField ID="hid_Encrypt4" runat="server" />
                                                    <asp:HiddenField ID="hid_Hex4" runat="server" />
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <asp:Label ID="LB_Credential5" class="col-12 col-sm-3 col-form-label text-left" runat="server" Text="Credential 5" Visible="false"></asp:Label>
                                                <div class="col-12 col-sm-8 col-lg-4">
                                                    <asp:TextBox ID="txt_Credential5" placeholder="(Mandatory) Credential 5" runat="server" CssClass="form-control" Visible="false"></asp:TextBox>
                                                    <asp:HiddenField ID="hid_Encrypt5" runat="server" />
                                                    <asp:HiddenField ID="hid_Hex5" runat="server" />
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <asp:Label ID="LB_Credential6" class="col-12 col-sm-3 col-form-label text-left" runat="server" Text="Credential 6" Visible="false"></asp:Label>
                                                <div class="col-12 col-sm-8 col-lg-4">
                                                    <asp:TextBox ID="txt_Credential6" placeholder="(Mandatory) Credential 6" runat="server" CssClass="form-control" Visible="false"></asp:TextBox>
                                                    <asp:HiddenField ID="hid_Encrypt6" runat="server" />
                                                    <asp:HiddenField ID="hid_Hex6" runat="server" />
                                                </div>
                                            </div>
                                        </asp:Panel>
                                        <asp:Panel ID="Panel_OB" runat="server" Visible="false">
                                         <%--   <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-left">Currency</label>
                                                <div class="col-12 col-sm-8 col-lg-4">
                                                    <asp:DropDownList ID="DD_CurrencyOB" runat="server" CssClass="select2"></asp:DropDownList>
                                                </div>
                                            </div>--%>
                                        </asp:Panel>
                                        <asp:Panel ID="Panel_CC" runat="server" Visible="false">
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-left">Card Type</label>
                                                <div class="col-12 col-sm-8 col-lg-8">
                                                    <div class="row">
                                                        <div class="col-2">
                                                            <label class="custom-control custom-checkbox">
                                                                <input type="checkbox" name="checkbox-stacked" id="cb_Visa" runat="server" class="custom-control-input" /><span class="custom-control-label">VISA</span>
                                                            </label>
                                                        </div>
                                                        <div class="col-2">
                                                            <label class="custom-control custom-checkbox">
                                                                <input type="checkbox" name="checkbox-stacked" id="cb_Master" runat="server" class="custom-control-input" /><span class="custom-control-label">MasterCard</span>
                                                            </label>
                                                        </div>
                                                        <div class="col-2">
                                                            <label class="custom-control custom-checkbox">
                                                                <input type="checkbox" name="checkbox-stacked" id="cb_Amex" runat="server" class="custom-control-input" /><span class="custom-control-label">AMEX</span>
                                                            </label>
                                                        </div>

                                                        <%--DEL START DANIEL 20190122 [Hide unsupported card type profile]--%>
                                                        <%--  <div class="col-2">
                                                            <label class="custom-control custom-checkbox">
                                                                <input type="checkbox" name="checkbox-stacked" id="cb_Diners" runat="server" class="custom-control-input" visible="false" /><span class="custom-control-label">Diners</span>
                                                            </label>
                                                        </div>
                                                        <div class="col-2">
                                                            <label class="custom-control custom-checkbox">
                                                                <input type="checkbox" name="checkbox-stacked" id="cb_JCB" runat="server" class="custom-control-input" visible="false" /><span class="custom-control-label">JCB</span>
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-2">
                                                            <label class="custom-control custom-checkbox">
                                                                <input type="checkbox" name="checkbox-stacked" id="cb_CUP" runat="server" class="custom-control-input" visible="false" /><span class="custom-control-label">CUP</span>
                                                            </label>
                                                        </div>
                                                        <div class="col-2">
                                                            <label class="custom-control custom-checkbox">
                                                                <input type="checkbox" name="checkbox-stacked" id="cb_MasterPass" runat="server" class="custom-control-input" visible="false" /><span class="custom-control-label">MasterPass</span>
                                                            </label>
                                                        </div>
                                                        <div class="col-2">
                                                            <label class="custom-control custom-checkbox">
                                                                <input type="checkbox" name="checkbox-stacked" id="cb_VisaCheckout" runat="server" class="custom-control-input" visible="false" /><span class="custom-control-label">VisaCheckout</span>
                                                            </label>
                                                        </div>
                                                        <div class="col-2">
                                                            <label class="custom-control custom-checkbox">
                                                                <input type="checkbox" name="checkbox-stacked" id="cb_SamsungPay" runat="server" class="custom-control-input" visible="false" /><span class="custom-control-label">SamsungPay</span>
                                                            </label>
                                                        </div>--%>
                                                        <%--DEL E N D DANIEL 20190122 [Hide unsupported card type profile]--%>
                                                    </div>
                                                </div>
                                            </div>
                                       <%--     <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-left">Currency</label>
                                                <div class="col-12 col-sm-8 col-lg-4">
                                                    <asp:DropDownList ID="DD_Currency" runat="server" CssClass="select2"></asp:DropDownList>
                                                </div>
                                            </div>--%>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-left">Card Group</label>
                                                <div class="col-12 col-sm-8 col-lg-8">
                                                    <asp:ListBox ID="LB_CardGroup" runat="server" SelectionMode="Multiple" CssClass="select2"></asp:ListBox>
                                                </div>
                                            </div>

                                        </asp:Panel>

                                        <asp:Panel ID="Panel_Add" runat="server" Visible="false">
                                            <div class="form-group row">
                                                <div class="col-12 col-lg-12 col-lg-10">
                                                    <div class="row">
                                                        <div class="col-10">
                                                        </div>
                                                        <div class="col-2">
                                                            <asp:Button ID="btn_AddHost" runat="server" Text="Add Channel" CssClass="btn btn-primary btn-xl" OnClick="btn_AddHost_Click" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card card-border-color card-border-color-primary">
                                <div class="card-header card-header-divider font-weight-bold">
                                    Channel
                                </div>
                                <div class="card-body">
                                    <div>
                                        <asp:Repeater ID="RP_Channel" runat="server">
                                            <HeaderTemplate>
                                                <table id="table1" class="table table-striped table-hover table-fw-widget">
                                                    <tr class="odd gradeX">
                                                        <th>Type</th>
                                                        <th>Channel Desc</th>
                                                        <%--ADD START DANIEL 20190123 [Add CurrencyCode]--%>
                                                        <th>Currency Code</th>
                                                        <%--ADD E N D DANIEL 20190123 [Add CurrencyCode]--%>
                                                    </tr>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr class="odd gradeX">
                                                    <td><%# Eval("Type") %></td>
                                                    <td><%# Eval("HostDesc") %></td>
                                                    <%--ADD START DANIEL 20190123 [Add CurrencyCode]--%>
                                                    <td><%# Eval("CurrencyCode") %></td>
                                                    <%--ADD E N D DANIEL 20190123 [Add CurrencyCode]--%>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center" id="footer">
                Copyright © 2019 Paydibs Sdn. Bhd. All Rights Reserved.
            </div>
        </div>
    </form>
    <script src="../../assets/lib/customize/General.js"></script>
    <script src="../../assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/bootstrap/dist/js/bootstrap.bundle.min.js" type="text/javascript"></script>
    <script src="../../assets/js/app.js" type="text/javascript"></script>
    <script src="../../assets/lib/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/select2/js/select2.full.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/bootstrap-slider/bootstrap-slider.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="../../assets/js/app-table-filters.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net/js/jquery.dataTables.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-bs4/js/dataTables.bootstrap4.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/dataTables.buttons.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/buttons.html5.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/buttons.flash.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/buttons.print.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/buttons.colVis.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js" type="text/javascript"></script>
    <script src="../../assets/js/app-tables-datatables.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            App.init();
            App.tableFilters();
            App.dataTables();
        });

        $(function () {
            var count = document.getElementById("Panel_Credential").childElementCount;
            for (i = 1; i <= count; i++) {
                if (!document.getElementById("txt_Credential" + i)) {
                    document.getElementById("hid_Encrypt" + i).parentElement.parentElement.style.display = "none";
                }
             }
        });
        
        $(function () {
            $('#dl_ErrorDialog').hide();
            var msg = $('#<%= HiddenField1.ClientID %>').val();
            var classType = $('#<%= HiddenField2.ClientID %>').val();
            var type = "alert-danger";
            if (msg != "") {
                if (classType != "") type = classType;
                $('#dl_ErrorDialog').addClass(type);
                $("#dl_message").append(msg);
                $('#dl_ErrorDialog').show();
            }
        });

        //Added by Daniel on 9 Apr 2019. Set Visa and Master checked on default
        $("#cb_Visa").prop( "checked", true );
        $("#cb_Master").prop( "checked", true );
    </script>
</body>
</html>
