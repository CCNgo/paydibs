﻿<%@ page language="C#" autoeventwireup="true" inherits="Backoffice_MMGMT_ViewMerchantRate, App_Web_ufpja5qk" %>

<!DOCTYPE html>
<%@ Register TagPrefix="uc" TagName="Left_Side_Bar" Src="~/UserControl/LeftSideBar.ascx" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Paydibs-Checkout View Merchant Rate</title>
    <link rel="stylesheet" type="text/css" href="../../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/material-design-icons/css/material-design-iconic-font.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/select2/css/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/bootstrap-slider/css/bootstrap-slider.min.css" />
    <link rel="stylesheet" href="../../assets/css/app.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <%--ADD START DANIEL 20190110 [Add Hidden1 and Hidden2]--%>
        <asp:HiddenField ID="HiddenField2" runat="server" />
        <asp:HiddenField ID="HiddenField1" runat="server" />
        <%--ADD START DANIEL 20190110 [Add Hidden1 and Hidden2]--%>
        <div class="be-wrapper be-collapsible-sidebar be-collapsible-sidebar-collapsed">
            <uc:Left_Side_Bar ID="Left_Side_Bar" runat="server" />
            <div class="be-content">
                <div class="main-content container-fluid">
                    <%--ADD START DANIEL 20190110 [Display message type]--%>
                    <div role="alert" class="alert alert-contrast alert-dismissible" id="dl_ErrorDialog">
                        <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
                        <div class="message" id="dl_message">
                        </div>
                    </div>
                    <%--ADD E N D DANIEL 20190110 [Display message type]--%>
                    <div class="row">
                        <div class="col-md-12 ">
                            <div class="card card-table card-border-color card-border-color-primary">
                                <div class="row table-filters-container">
                                    <div class="col-12 col-lg-12 col-xl-6">
                                        <div class="row">
                                            <div class="col-12 col-lg-8 table-filters pb-0 pb-xl-4">
                                                <span class="table-filter-title">Merchant</span>
                                                <div class="filter-container">
                                                    <label class="control-label">Select Merchant:</label>
                                                    <asp:DropDownList ID="DD_merchant" runat="server" CssClass="select2"></asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-12 col-lg-2 table-filters pb-0 pb-xl-4">
                                                <span class="table-filter-title"></span>
                                                <div class="filter-container" style="margin-top: 45px">
                                                    <asp:Button ID="btn_GetInfo" runat="server" Text="Get Info" CssClass="btn btn-primary btn-xl" OnClick="btn_GetInfo_Click" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card card-border-color card-border-color-primary">
                                <nav>
	                              <div class="nav nav-tabs" id="nav-tab" role="tablist">
		                            <a class="nav-item nav-link active" id="nav-viewRate-tab" data-toggle="tab" href="#nav-viewRate" role="tab" aria-controls="nav-viewRate" aria-selected="true">View Merchant Rate</a>
		                            <a class="nav-item nav-link" id="navviewFutureRatetab" data-toggle="tab" href="#nav-viewFutureRate" role="tab" aria-controls="nav-viewFutureRate" aria-selected="false" runat="server">View New Effective Rate</a>
		                            <%--<a class="nav-item nav-link" id="nav-addPromotion-tab" data-toggle="tab" href="#nav-addPromotion" role="tab" aria-controls="nav-addPromotion" aria-selected="false">Add Promotion Rate</a>--%>
	                              </div>
	                            </nav>
                                <div class="tab-content" id="nav-tabContent">
                                    <div class="tab-pane fade show active" id="nav-viewRate" role="tabpanel" aria-labelledby="nav-viewRate-tab">
                                        <div class="card-header card-header-divider font-weight-bold">
                                            Merchant Rate
                                        </div>
                                        <div class="card-body">
                                            <div>
                                                <asp:Repeater ID="RP_Rate" runat="server">
                                                    <HeaderTemplate>
                                                        <table id="table1" class="table table-striped table-hover table-fw-widget">
                                                            <tr class="odd gradeX">
                                                                <th>Payment Method</th>
                                                                <th>Channel</th>
                                                                <%--CHG START DANIEL 20181211 [Add percentage symbol to the Rate]--%>
                                                                <%--    <th>Rate</th>--%>
                                                                <th>Rate (%)</th>
                                                                <%--CHG E N D DANIEL 20181211 [Add percentage symbol to the Rate]--%>
                                                                <th>Fixed Fee</th>
                                                                <th>Currency</th>
                                                            </tr>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr class="odd gradeX">
                                                            <td><%#  Eval("PymtMethod") %></td>
                                                            <td><%#  Eval("HostName") %></td>
                                                            <td><%#  Eval("Rate") %></td>
                                                            <td><%#  Eval("FixedFee") %></td>
                                                            <td><%#  Eval("Currency") %></td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </table>
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="nav-viewFutureRate" role="tabpanel" aria-labelledby="nav-viewFutureRate-tab">
                                        <div class="card-header card-header-divider font-weight-bold">
                                            New Effective Rate
                                        </div>
                                        <div class="card-body">
                                            <div>
                                                <asp:Repeater ID="Repeater1" runat="server">
                                                    <HeaderTemplate>
                                                        <table id="table1" class="table table-striped table-hover table-fw-widget">
                                                            <tr class="odd gradeX">
                                                                <th>Payment Method</th>
                                                                <th>Channel</th>
                                                                <th>Rate (%)</th>
                                                                <th>Fixed Fee</th>
                                                                <th>Currency</th>
                                                                <th>Effective Date</th>
                                                                <th></th>
                                                            </tr>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr class="odd gradeX">
                                                            <td><%#  Eval("PymtMethod") %></td>
                                                            <td><%#  Eval("HostName") %></td>
                                                            <td><%#  Eval("Rate") %></td>
                                                            <td><%#  Eval("FixedFee") %></td>
                                                            <td><%#  Eval("Currency") %></td>
                                                            <td><%#  Eval("StartDate","{0:dd/MM/yyyy}") %></td>
                                                            <td><a href="<%# Eval("redirectURL") %>">Edit</a></td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </table>
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center" id="footer">
                Copyright © 2019 Paydibs Sdn. Bhd. All Rights Reserved.
            </div>
        </div>
    </form>
    <script src="../../assets/lib/customize/General.js"></script>
    <script src="../../assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/bootstrap/dist/js/bootstrap.bundle.min.js" type="text/javascript"></script>
    <script src="../../assets/js/app.js" type="text/javascript"></script>
    <script src="../../assets/lib/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/select2/js/select2.full.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/bootstrap-slider/bootstrap-slider.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="../../assets/js/app-table-filters.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net/js/jquery.dataTables.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-bs4/js/dataTables.bootstrap4.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/dataTables.buttons.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/buttons.html5.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/buttons.flash.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/buttons.print.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/buttons.colVis.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js" type="text/javascript"></script>
    <script src="../../assets/js/app-tables-datatables.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            App.init();
            App.tableFilters();
            App.dataTables();
        });

  //ADD START DANIEL 20190110 [Javascript to display error message]
        $(function () {
            $('#dl_ErrorDialog').hide();
            var msg = $('#<%= HiddenField1.ClientID %>').val();
            var classType = $('#<%= HiddenField2.ClientID %>').val();
            var type = "alert-danger";
            if (msg != "") {
                if (classType != "") type = classType;
                $('#dl_ErrorDialog').addClass(type);
                document.getElementById('dl_message').innerText = msg;
                $('#dl_ErrorDialog').show();
            }
        });
        //ADD E N D DANIEL 20190110 [Javascript to display error message]
    </script>
</body>
</html>
