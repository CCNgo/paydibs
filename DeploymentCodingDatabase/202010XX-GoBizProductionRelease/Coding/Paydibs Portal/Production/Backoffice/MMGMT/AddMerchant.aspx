﻿<%@ page language="C#" autoeventwireup="true" inherits="AddMerchant, App_Web_ufpja5qk" %>

<!DOCTYPE html>
<%@ Register TagPrefix="uc" TagName="Left_Side_Bar" Src="~/UserControl/LeftSideBar.ascx" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <link rel="shortcut icon" href="../../assets/img/logo-fav.png" />
    <title>Paydibs-Checkout New Merchant</title>
    <link rel="stylesheet" type="text/css" href="../../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/bootstrap-slider/css/bootstrap-slider.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/material-design-icons/css/material-design-iconic-font.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/select2/css/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/bootstrap-slider/css/bootstrap-slider.min.css" />
    <link rel="stylesheet" href="../../assets/css/app.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server" method="post" enctype="multipart/form-data">
        <asp:HiddenField ID="HiddenField2" runat="server" />
        <asp:HiddenField ID="HiddenField1" runat="server" />
        <div class="be-wrapper be-collapsible-sidebar be-collapsible-sidebar-collapsed">
            <uc:Left_Side_Bar ID="Left_Side_Bar" runat="server" />
            <div class="be-content">
                <div class="main-content container-fluid">
                    <div role="alert" class="alert alert-contrast alert-dismissible" id="dl_ErrorDialog">
                        <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
                        <div class="message" id="dl_message">
                        </div>
                    </div>
                    <div class="row wizard-row">
                        <div class="col-md-12 fuelux">
                            <div class="block-wizard card-border-color card-border-color-primary">
                                <div id="wizard1" class="wizard wizard-ux">
                                    <div class="steps-container">
                                        <ul class="steps">
                                            <li data-step="1" class="active">Section A - Company / Merchant Profile<span class="chevron"></span></li>
                                            <%--<li data-step="2">Section B - Company Ownership Profile<span class="chevron"></span></li>--%>
                                            <li data-step="3">Section B - Business Profile<span class="chevron"></span></li>
                                            <li data-step="4">Section C - Merchant Account Setup Information<span class="chevron"></span></li>
                                            <li data-step="5">Section D - Bank Account Information<span class="chevron"></span></li>
                                            <li data-step="6">Section E - Office Use<span class="chevron"></span></li>
                                        </ul>
                                    </div>
                                    <div class="actions">
                                        <button type="button" class="btn btn-xs btn-prev btn-secondary"><i class="icon mdi mdi-chevron-left"></i>Prev</button>
                                        <button type="button" data-last="Finish" class="btn btn-xs btn-next btn-secondary">Next<i class="icon mdi mdi-chevron-right"></i></button>
                                    </div>
                                    <div class="step-content">
                                        <div data-step="1" class="step-pane active">
                                            <div class="form-group row">
                                                <div class="col-sm-7">
                                                    <h3 class="wizard-title">Company Info</h3>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">Company Registered Name</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                    <asp:TextBox ID="txt_BusinessRegName" placeholder="(Mandatory) Company Registered Name" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">Company Registration No.</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                    <asp:TextBox ID="txt_BusinessRegNo" placeholder="(Mandatory) Company Registration No." runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                                </div>
                                            </div>
                                            <%--<div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">GST No. (if any)</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                    <asp:TextBox ID="txt_GSTNo" placeholder="GST No." runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                                </div>
                                            </div>--%>
                                            <%-- <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">Type Of Business</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                    <label class="custom-control custom-radio">
                                                        <input type="radio" name="rd_TypeOfBusiness" id="rd_TypeOfBusiness_SP" runat="server" class="custom-control-input" /><span class="custom-control-label">Sole Proprietorship</span>
                                                    </label>
                                                    <label class="custom-control custom-radio">
                                                        <input type="radio" name="rd_TypeOfBusiness" id="rd_TypeOfBusiness_CO" runat="server" class="custom-control-input" /><span class="custom-control-label">Corporation (Sdn Bhd/ Bhd)</span>
                                                    </label>
                                                    <label class="custom-control custom-radio">
                                                        <input type="radio" name="rd_TypeOfBusiness" id="rd_TypeOfBusiness_PS" runat="server" class="custom-control-input" /><span class="custom-control-label">Partnership</span>
                                                    </label>
                                                    <label class="custom-control custom-radio">
                                                        <input type="radio" name="rd_TypeOfBusiness" id="rd_TypeOfBusiness_NPO" runat="server" class="custom-control-input" /><span class="custom-control-label">Non-Profit Organization</span>
                                                    </label>
                                                    <label class="custom-control custom-radio">
                                                        <input type="radio" name="rd_TypeOfBusiness" id="rd_TypeOfBusiness_Other" runat="server" class="custom-control-input" /><span class="custom-control-label">Other</span>
                                                    </label>
                                                </div>
                                            </div>--%>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">Business Address Line 1</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                    <asp:TextBox ID="txt_BusinessAdd1" placeholder="(Mandatory) Business Address Line 1" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">Business Address Line 2</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                    <asp:TextBox ID="txt_BusinessAdd2" placeholder="Business Address Line 2" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">Business Address Line 3</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                    <asp:TextBox ID="txt_BusinessAdd3" placeholder="Business Address Line 3" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">Business City</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                    <asp:TextBox ID="txt_BusinessCity" placeholder="Business City" runat="server" CssClass="form-control" MaxLength="30"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">Business State</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                    <asp:DropDownList ID="DD_BusinesssState" runat="server" CssClass="select2"></asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">Business Zipcode</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                    <asp:TextBox ID="txt_BusinessZipcode" placeholder="Business Zipcode" runat="server" CssClass="form-control" MaxLength="10"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">Business Country</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                    <asp:DropDownList ID="DD_Country" runat="server" CssClass="select2">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">Office Number</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                    <asp:TextBox ID="txt_OfficeNumber" placeholder="(Mandatory) Office Number" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
                                                </div>
                                            </div>


                                            <div class="form-group row">
                                                <div class="col-sm-7">
                                                    <h3 class="wizard-title">Authorized Contact Person 1</h3>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">Full Name</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                    <asp:TextBox ID="txt_acp1_FullName" placeholder="(Mandatory) Full Name" runat="server" CssClass="form-control" MaxLength="70"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">Contact Number</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                    <asp:TextBox ID="txt_acp1_MobileNumber" placeholder="(Mandatory) Contact Number" runat="server" CssClass="form-control" MaxLength="40"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">Email Address</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                    <asp:TextBox ID="txt_acp1_EmailAdd" placeholder="(Mandatory) Email Address" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-sm-7">
                                                    <h3 class="wizard-title">Authorized Contact Person 2</h3>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">Full Name</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                    <asp:TextBox ID="txt_acp2_FullName" placeholder="Full Name" runat="server" CssClass="form-control" MaxLength="70"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">Contact Number</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                    <asp:TextBox ID="txt_acp2_MobileNumber" placeholder="Contact Number" runat="server" CssClass="form-control" MaxLength="40"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">Email Address</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                    <asp:TextBox ID="txt_acp2_EmailAdd" placeholder="Email Address" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-sm-7">
                                                    <h3 class="wizard-title">Billing Contact Person</h3>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">Full Name</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                    <asp:TextBox ID="txt_bill_FullName" placeholder="Full Name" runat="server" CssClass="form-control" MaxLength="70"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">Contact Number</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                    <asp:TextBox ID="txt_bill_MobileNumber" placeholder="Contact Number" runat="server" CssClass="form-control" MaxLength="40"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">Email Address</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                    <asp:TextBox ID="txt_bill_EmailAdd" placeholder="Email Address" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-sm-12">
                                                    <button data-wizard="#wizard1" class="btn btn-secondary btn-space wizard-previous">Previous</button>
                                                    <button data-wizard="#wizard1" class="btn btn-primary btn-space wizard-next">Next Step</button>
                                                </div>
                                            </div>
                                        </div>


                                        <%--<div data-step="2" class="step-pane">
                                            <div class="form-group row">
                                                <div class="col-sm-7">
                                                    <h3 class="wizard-title">List of Company Director / Proprietor</h3>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <table id="table1" class="table table-striped table-hover table-fw-widget">
                                                    <thead>
                                                        <tr id="TB_column" runat="server">
                                                            <th></th>
                                                            <th>Director Name</th>
                                                            <th>Identity Card / Passport No.</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr class="odd gradeX">
                                                            <td>i</td>
                                                            <td>
                                                                <asp:TextBox ID="txt_i_director_name" placeholder="Full Name" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox></td>
                                                            <td>
                                                                <asp:TextBox ID="txt_i_director_id" placeholder="Identity Card / Passport No." runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox></td>
                                                        </tr>
                                                        <tr class="odd gradeX">
                                                            <td>ii</td>
                                                            <td>
                                                                <asp:TextBox ID="txt_ii_director_name" placeholder="Full Name" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox></td>
                                                            <td>
                                                                <asp:TextBox ID="txt_ii_director_id" placeholder="Identity Card / Passport No." runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox></td>
                                                        </tr>
                                                        <tr class="odd gradeX">
                                                            <td>iii</td>
                                                            <td>
                                                                <asp:TextBox ID="txt_iii_director_name" placeholder="Full Name" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox></td>
                                                            <td>
                                                                <asp:TextBox ID="txt_iii_director_id" placeholder="Identity Card / Passport No." runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox></td>
                                                        </tr>
                                                        <tr class="odd gradeX">
                                                            <td>iv</td>
                                                            <td>
                                                                <asp:TextBox ID="txt_iv_director_name" placeholder="Full Name" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox></td>
                                                            <td>
                                                                <asp:TextBox ID="txt_iv_director_id" placeholder="Identity Card / Passport No." runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox></td>
                                                        </tr>
                                                        <tr class="odd gradeX">
                                                            <td>v</td>
                                                            <td>
                                                                <asp:TextBox ID="txt_v_director_name" placeholder="Full Name" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox></td>
                                                            <td>
                                                                <asp:TextBox ID="txt_v_director_id" placeholder="Identity Card / Passport No." runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox></td>
                                                        </tr>
                                                    </tbody>
                                                </table>

                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-7">
                                                    <h3 class="wizard-title">List of Company Share Holders</h3>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <table id="table2" class="table table-striped table-hover table-fw-widget">
                                                    <thead>
                                                        <tr id="Tr1" runat="server">
                                                            <th></th>
                                                            <th>Shareholder Name</th>
                                                            <th>Identity Card / Passport No.</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr class="odd gradeX">
                                                            <td>i</td>
                                                            <td>
                                                                <asp:TextBox ID="txt_i_share_name" placeholder="Full Name" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox></td>
                                                            <td>
                                                                <asp:TextBox ID="txt_i_share_id" placeholder="Identity Card / Passport No." runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox></td>
                                                        </tr>
                                                        <tr class="odd gradeX">
                                                            <td>ii</td>
                                                            <td>
                                                                <asp:TextBox ID="txt_ii_share_name" placeholder="Full Name" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox></td>
                                                            <td>
                                                                <asp:TextBox ID="txt_ii_share_id" placeholder="Identity Card / Passport No." runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox></td>
                                                        </tr>
                                                        <tr class="odd gradeX">
                                                            <td>iii</td>
                                                            <td>
                                                                <asp:TextBox ID="txt_iii_share_name" placeholder="Full Name" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox></td>
                                                            <td>
                                                                <asp:TextBox ID="txt_iii_share_id" placeholder="Identity Card / Passport No." runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox></td>
                                                        </tr>
                                                        <tr class="odd gradeX">
                                                            <td>iv</td>
                                                            <td>
                                                                <asp:TextBox ID="txt_iv_share_name" placeholder="Full Name" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox></td>
                                                            <td>
                                                                <asp:TextBox ID="txt_iv_share_id" placeholder="Identity Card / Passport No." runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox></td>
                                                        </tr>
                                                        <tr class="odd gradeX">
                                                            <td>v</td>
                                                            <td>
                                                                <asp:TextBox ID="txt_v_share_name" placeholder="Full Name" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox></td>
                                                            <td>
                                                                <asp:TextBox ID="txt_v_share_id" placeholder="Identity Card / Passport No." runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-sm-12">
                                                    <button data-wizard="#wizard1" class="btn btn-secondary btn-space wizard-previous">Previous</button>
                                                    <button data-wizard="#wizard1" class="btn btn-primary btn-space wizard-next">Next Step</button>
                                                </div>
                                            </div>
                                        </div>--%>


                                        <div data-step="3" class="step-pane">
                                            <div class="form-group row">
                                                <div class="col-sm-7">
                                                    <h3 class="wizard-title">Business Profile</h3>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-4 col-form-label text-left text-sm-right">Doing Business As (DBA) Name / Trading Name</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                    <asp:TextBox ID="txt_businessName" placeholder="(Mandatory) Doing Business As (DBA) Name / Trading Name" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-4 col-form-label text-left text-sm-right">Online Store URL (if any)</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                    <asp:TextBox ID="txt_onlinestoreURL" placeholder="Online Store URL (if any)" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-4 col-form-label text-left text-sm-right">Type of products / services sold</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                    <asp:TextBox ID="txt_TypeOfProducts" placeholder="Type of products /  services sold" runat="server" CssClass="form-control" MaxLength="150"></asp:TextBox>
                                                </div>
                                            </div>

                                            <%--<div class="form-group row">
                                                <label class="col-12 col-sm-4 col-form-label text-left text-sm-right">Target Market(s)</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                    <label class="custom-control custom-checkbox">
                                                        <input type="checkbox" name="checkbox-stacked" id="cb_market_Malaysia" runat="server" class="custom-control-input" /><span class="custom-control-label">Malaysia</span>
                                                    </label>
                                                    <label class="custom-control custom-checkbox">
                                                        <input type="checkbox" name="checkbox-stacked" id="cb_market_Indonesia" runat="server" class="custom-control-input" /><span class="custom-control-label">Indonesia</span>
                                                    </label>
                                                    <label class="custom-control custom-checkbox">
                                                        <input type="checkbox" name="checkbox-stacked" id="cb_market_Thailand" runat="server" class="custom-control-input" /><span class="custom-control-label">Thailand</span>
                                                    </label>
                                                    <label class="custom-control custom-checkbox">
                                                        <input type="checkbox" name="checkbox-stacked" id="cb_market_Singapore" runat="server" class="custom-control-input" /><span class="custom-control-label">Singapore</span>
                                                    </label>
                                                    <label class="custom-control custom-checkbox">
                                                        <input type="checkbox" name="checkbox-stacked" id="cb_market_China" runat="server" class="custom-control-input" /><span class="custom-control-label">China</span>
                                                    </label>
                                                    <label class="custom-control custom-checkbox">
                                                        <input type="checkbox" name="checkbox-stacked" id="cb_market_Philippines" runat="server" class="custom-control-input" /><span class="custom-control-label">Philippines</span>
                                                    </label>
                                                    <label class="custom-control custom-checkbox">
                                                        <input type="checkbox" name="checkbox-stacked" id="cb_market_Others" runat="server" class="custom-control-input" /><span class="custom-control-label">Others</span>
                                                    </label>
                                                </div>
                                            </div>--%>

                                            <div class="form-group row">
                                                <label class="col-12 col-sm-5 col-form-label text-left text-sm-right">How long does customer wait before product / service to be delivered?</label>
                                                <div class="col-12 col-sm-4 col-lg-6">
                                                    <label class="custom-control custom-radio">
                                                        <input type="radio" name="radio-stacked" id="rd_Instant" runat="server" class="custom-control-input" /><span class="custom-control-label">Instant</span>
                                                    </label>
                                                    <label class="custom-control custom-radio">
                                                        <input type="radio" name="radio-stacked" id="rd_1_3_days" runat="server" class="custom-control-input" /><span class="custom-control-label">1-3 days</span>
                                                    </label>
                                                    <label class="custom-control custom-radio">
                                                        <input type="radio" name="radio-stacked" id="rd_4_10_days" runat="server" class="custom-control-input" /><span class="custom-control-label">4-10 days</span>
                                                    </label>
                                                    <label class="custom-control custom-radio">
                                                        <input type="radio" name="radio-stacked" id="rd_11_30_days" runat="server" class="custom-control-input" /><span class="custom-control-label">11-30 days</span>
                                                    </label>
                                                    <label class="custom-control custom-radio">
                                                        <input type="radio" name="radio-stacked" id="rd_30_days" runat="server" class="custom-control-input" /><span class="custom-control-label">> 30 days</span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-4 col-form-label text-left text-sm-right">Shopping cart / E-Commerce System / POS Used</label>
                                                <div class="col-12 col-sm-4 col-lg-4">
                                                    <asp:DropDownList ID="DD_Plugin" runat="server" CssClass="select2"></asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-12">
                                                    <button data-wizard="#wizard1" class="btn btn-secondary btn-space wizard-previous">Previous</button>
                                                    <button data-wizard="#wizard1" class="btn btn-primary btn-space wizard-next">Next Step</button>
                                                </div>
                                            </div>
                                        </div>


                                        <div data-step="4" class="step-pane">
                                            <div class="form-group row">
                                                <div class="col-sm-7">
                                                    <h3 class="wizard-title">Merchant Account Setup Information</h3>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">Send payment notification via email?</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                    <asp:DropDownList ID="DD_Notify" runat="server" CssClass="select2"></asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">Payment Notification Email</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                    <asp:TextBox ID="txt_IPNEMAIL" placeholder="Payment Notification Email" runat="server" CssClass="form-control" MaxLength="200"></asp:TextBox>
                                                    <label class="col-12 col-sm-3 col-form-label text-left">example: abc@gmail.com, def@gmail.com, ghi@gmail.com</label>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-7">
                                                    <h3 class="wizard-title">Merchant Customer Service Info</h3>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">Email Address</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                    <asp:TextBox ID="txt_cs_EmailAddr" placeholder="Email Address" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">Contact Number</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                    <asp:TextBox ID="txt_cs_MobileNumber" placeholder="Contact Number" runat="server" CssClass="form-control" MaxLength="30"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-sm-12">
                                                    <button data-wizard="#wizard1" class="btn btn-secondary btn-space wizard-previous">Previous</button>
                                                    <button data-wizard="#wizard1" class="btn btn-primary btn-space wizard-next">Next Step</button>
                                                </div>
                                            </div>
                                        </div>


                                        <div data-step="5" class="step-pane">
                                            <div class="form-group row">
                                                <div class="col-sm-7">
                                                    <h3 class="wizard-title">Bank Account Information</h3>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">Bank Name</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                    <asp:TextBox ID="txt_BankName" placeholder="Bank Name" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">Bank Account No.</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                    <asp:TextBox ID="txt_BankAccountNo" placeholder="Bank Account No." runat="server" CssClass="form-control" MaxLength="30"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">Bank Account Name</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                    <asp:TextBox ID="txt_BankAccountName" placeholder="Bank Account Name" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">SWIFT Code</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                    <asp:TextBox ID="txt_SWIFTCode" placeholder="SWIFT Code" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">Bank Address</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                    <asp:TextBox ID="txt_BankAddress" placeholder="Bank Address" runat="server" CssClass="form-control" TextMode="MultiLine" Height="100" MaxLength="500"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">Bank Country</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                    <asp:DropDownList ID="DD_BankCountry" runat="server" CssClass="select2">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-sm-12">
                                                    <button data-wizard="#wizard1" class="btn btn-secondary btn-space wizard-previous">Previous</button>
                                                    <button data-wizard="#wizard1" class="btn btn-primary btn-space wizard-next">Next Step</button>
                                                </div>
                                            </div>
                                        </div>


                                        <div data-step="6" class="step-pane">
                                            <div class="form-group row">
                                                <div class="col-sm-7">
                                                    <h3 class="wizard-title">Office Use</h3>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">Merchant Category Code (MCC)</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                    <asp:DropDownList ID="DD_MCCCode" runat="server" CssClass="select2"></asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">Per Transaction Limit</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                    <asp:TextBox ID="txt_TransactionLimit" placeholder="Transaction Limit (Default 2500)" runat="server" CssClass="form-control" MaxLength="20" TextMode="Number"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">Merchant ID</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                    <asp:TextBox ID="txt_MerchantID" placeholder="Merchant ID" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-sm-right" for="file-2">Merchant Logo</label>
                                                <div class="col-12 col-sm-6">
                                                    <input class="inputfile" id="file-2" type="file" name="file-2" accept="image/png" data-multiple-caption="{count} files selected" onchange="readURL(this);" />
                                                    <label class="btn-primary" for="file-2"><i class="mdi mdi-upload"></i><span>Browse files...</span></label>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-sm-right" for="file-2">Logo must be in png file</label>
                                                <div class="col-12 col-sm-6">
                                                    <img id="img_MerchantLogo" src="../../assets/img/120x270.png" style="width: 80%; max-width: 150px" runat="server" />
                                                    <%--<asp:Image ID="img_MerchantLogo" runat="server" class="img-thumbnail" src="../../assets/img/120x270.png" max-width="150px" alt="Placeholder" />--%>
                                                </div>
                                            </div>

                                            <%-- ADD START SUKI 20181128 [Add in showMerchantAddr, showMerchantLogo, IsAgent, default currency setting] --%>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">Merchant Setting</label>
                                                <div class="col-12 col-sm-6">
                                                    <label class="custom-control custom-checkbox">
                                                        <input type="checkbox" name="checkbox-stacked" id="cb_showMerchantAddr" checked="checked" runat="server" class="custom-control-input" /><span class="custom-control-label">Show Merchant Address</span>
                                                    </label>
                                                    <label class="custom-control custom-checkbox">
                                                        <input type="checkbox" name="checkbox-stacked" id="cb_showMerchantLogo" checked="checked" runat="server" class="custom-control-input" /><span class="custom-control-label">Show Merchant Logo</span>
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">Is Agent</label>
                                                <div class="col-12 col-sm-6">
                                                    <label class="custom-control custom-checkbox">
                                                        <input type="checkbox" name="checkbox-stacked" id="cb_Agent" runat="server" class="custom-control-input" /><span class="custom-control-label">Agent</span>
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">Default Currency</label>
                                                <div class="col-12 col-sm-6">
                                                    <label class="custom-control custom-checkbox">
                                                    <asp:DropDownList ID="DD_Currency" runat="server" CssClass="select2"></asp:DropDownList>
                                                    </label>
                                                </div>
                                            </div>
                                            <%-- ADD E N D SUKI 20181128 [Add in showMerchantAddr, showMerchantLogo, IsAgent, default currency  setting] --%>

                                            <div class="form-group row">
                                                <div class="col-sm-12">
                                                    <button data-wizard="#wizard1" class="btn btn-secondary btn-space wizard-previous">Previous</button>
                                                    <asp:Button ID="btn_Complete" runat="server" Text="Complete" CssClass="btn btn-success btn-space" OnClick="btn_Complete_Click" />
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-center" id="footer">
                Copyright © 2019 Paydibs Sdn. Bhd. All Rights Reserved.
        </div>
    </form>
    <script src="../../assets/lib/customize/General.js"></script>
    <script src="../../assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/bootstrap/dist/js/bootstrap.bundle.min.js" type="text/javascript"></script>
    <script src="../../assets/js/app.js" type="text/javascript"></script>
    <script src="../../assets/lib/fuelux/js/wizard.js" type="text/javascript"></script>
    <script src="../../assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/select2/js/select2.full.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/bootstrap-slider/bootstrap-slider.min.js" type="text/javascript"></script>
    <script src="../../assets/js/app-form-wizard.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            //initialize the javascript
            App.init();
            App.wizard();

            $('#txt_BusinessRegName').keyup(function () {
                //var x = $('#txt_BusinessRegName').val();
                //$('#txt_MerchantID').val(x);
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "AddMerchant.aspx/ServerSideMethod",
                    data: "{BusinessRegName: '" + $('#txt_BusinessRegName').val() + "'}",
                    dataType: "json",
                    async: true,
                    cache: false,
                    success: function (msg) {
                        $('#txt_MerchantID').val(msg.d);
                    },
                    error: function (result) {
                        $('#txt_MerchantID').val('');
                    }
                });
            });
        });

        $(function () {
            $('#dl_ErrorDialog').hide();
            var msg = $('#<%= HiddenField1.ClientID %>').val();
            var classType = $('#<%= HiddenField2.ClientID %>').val();
            var type = "alert-danger";
            if (msg != "") {
                if (classType != "") type = classType;
                $('#dl_ErrorDialog').addClass(type);
                document.getElementById('dl_message').innerText = msg;
                $('#dl_ErrorDialog').show();
            }
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#img_MerchantLogo')
                        .attr('src', e.target.result)
                        .width(270)
                        .height(120);
                };

                reader.readAsDataURL(input.files[0]);
                //$('#txt_MerchantLogo').val(input.files[0].val());
            }
        }

    </script>
</body>
</html>
