﻿<%@ page language="C#" autoeventwireup="true" inherits="Backoffice_MMS_BuyNow, App_Web_eblqs4q2" validaterequest="false" %>

<!DOCTYPE html>
<%@ Register TagPrefix="uc" TagName="Left_Side_Bar" Src="~/UserControl/LeftSideBar.ascx" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Paydibs-Checkout Buy Now Button</title>
    <link rel="stylesheet" type="text/css" href="../../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/material-design-icons/css/material-design-iconic-font.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/select2/css/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/bootstrap-slider/css/bootstrap-slider.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/datatables/datatables.net-bs4/css/dataTables.bootstrap4.css" />
    <link rel="stylesheet" href="../../assets/css/app.css" type="text/css" />

    <script src="../../assets/lib/qrcodejs/qrcode.js"></script>
    <script src="../../assets/lib/qrcodejs/qrcode.min.js"></script>
    <script src="../../assets/lib/qrcodejs/jquery.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:HiddenField ID="HiddenField2" runat="server" />
        <asp:HiddenField ID="HiddenField1" runat="server" />
        <div class="be-wrapper be-collapsible-sidebar be-collapsible-sidebar-collapsed">
            <uc:Left_Side_Bar ID="Left_Side_Bar" runat="server" />
            <div class="be-content">
                <div class="main-content container-fluid">
                    <div role="alert" class="alert alert-contrast alert-dismissible" id="dl_ErrorDialog">
                        <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
                        <div class="message" id="dl_message">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-border-color card-border-color-primary">
                                <div class="card-header card-header-divider font-weight-bold" id="headerName" runat="server">
                                    Buy Now
                                </div>
                                <div class="card-body">
                                    <div class="filter-container">
                                        <div class="row">
                                            <div class="col-4">
                                                <label class="control-label font-weight-bold">Order Description</label>
                                            </div>
                                            <div class="col-6">
                                                <asp:TextBox ID="txt_orderDesc" placeholder="Order Description" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-4">
                                                <label class="control-label font-weight-bold">Order Number</label>
                                            </div>
                                            <div class="col-6">
                                                <asp:TextBox ID="txt_orderNo" placeholder="e.g: Invoice Number/ Reference Number" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-4">
                                                <label class="control-label font-weight-bold">Currency</label>
                                            </div>
                                            <div class="col-6">
                                                <asp:DropDownList ID="DD_Currency" runat="server" CssClass="select2"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-4">
                                                <label class="control-label font-weight-bold">Amount</label>
                                            </div>
                                            <div class="col-6">
                                                <asp:TextBox ID="txt_Amt" placeholder="Amount" runat="server" CssClass="form-control" Onkeypress="return validateFloatKeyPress(this,event)"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="row" style="margin-top: 10px;">
                                            <div class="col-7"></div>
                                            <div class="col-0 ">
                                                <asp:Button ID="btn_Create" runat="server" Text="Create" CssClass="btn btn-primary btn-xl right" OnClick="btn_Create_Click" />
                                                <asp:Button ID="btn_Reset" runat="server" Text="Reset" CssClass="btn btn-primary btn-xl right" OnClick="btn_Reset_Click" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <asp:Panel ID="panel_qrCode" runat="server" Visible="false">
                        <div class="row">
                            <div class="col-12 col-lg-12">
                                <div class="card card-border-color card-border-color-primary">
                                    <div class="card-header card-header-divider font-weight-bold">
                                        Paydibs SDN BHD QRCODE
                                    </div>

                                    <div class="col-12 col-lg-8 table-filters pb-0 pb-xl-4">
                                        <div class="filter-container">
                                            Payment Link
                                            <asp:TextBox ID="txtQrCode" onClick="this.select()" runat="server" ReadOnly="true" TextMode="MultiLine" Style="word-wrap: break-word;" Width="100%" Height="100px"></asp:TextBox>
                                            <br />
                                            <br />
                                            <asp:Label ID="lblInfo" Text="Please click the following box to select all button scripts.&lt;br/&gt;Right click and choose Copy to copy the scripts. Then, paste into your page. &nbsp;&nbsp;" runat="server" />

                                            <asp:TextBox ID="txt_BtnScript" runat="server" CssClass="form-control" onClick="this.select()" ReadOnly="true" TextMode="MultiLine" Height="200px" Style="word-wrap: break-word;"></asp:TextBox>
                                            <br />
                                            Scan payment link here .
                                            <br />
                                            Right click and choose 'Save image as...' to download qrcode as image.
                                         <div id="qrCode" style="width: 200px; height: 200px; margin-top: 5px;"></div>

                                            <script type="text/javascript">
                                                var qrcode = new QRCode(document.getElementById("qrCode"), {
                                                    width: 200,
                                                    height: 200
                                                });

                                                function makeCode() {
                                                    var elText = document.getElementById("txtQrCode");
                                                    if (!elText.value) {
                                                        alert("Input a text");
                                                        elText.focus();
                                                        return;
                                                    }
                                                    qrcode.makeCode(elText.value);
                                                }
                                                makeCode();

                                                $("#text").
                                                    on("blur", function () {
                                                        makeCode();
                                                    }).
                                                    on("keydown", function (e) {
                                                        if (e.keyCode == 13) {
                                                            makeCode();
                                                        }
                                                    });
                                            </script>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>      
            </div>
            <div class="text-center" id="footer">
                Copyright © 2018 Paydibs Sdn. Bhd. All Rights Reserved.
            </div>
        </div>



    </form>
    <script src="../../assets/lib/customize/General.js"></script>
    <script src="../../assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/bootstrap/dist/js/bootstrap.bundle.min.js" type="text/javascript"></script>
    <script src="../../assets/js/app.js" type="text/javascript"></script>
    <script src="../../assets/lib/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/select2/js/select2.full.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/bootstrap-slider/bootstrap-slider.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="../../assets/js/app-table-filters.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net/js/jquery.dataTables.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-bs4/js/dataTables.bootstrap4.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/dataTables.buttons.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/buttons.html5.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/buttons.flash.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/buttons.print.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/buttons.colVis.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js" type="text/javascript"></script>
    <script src="../../assets/js/app-tables-datatables.js" type="text/javascript"></script>
    <link href="../../assets/lib/Bootstrap-4-Tag-Input-Plugin-jQuery/tagsinput.css" rel="stylesheet" />
    <script src="../../assets/lib/Bootstrap-4-Tag-Input-Plugin-jQuery/tagsinput.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            App.init();
            App.tableFilters();
            App.dataTables();

        });

        $(function () {
            $('#dl_ErrorDialog').hide();
            var msg = $('#<%= HiddenField1.ClientID %>').val();
            var classType = $('#<%= HiddenField2.ClientID %>').val();
            var type = "alert-danger";
            if (msg != "") {
                if (classType != "") type = classType;
                $('#dl_ErrorDialog').addClass(type);
                document.getElementById('dl_message').innerText = msg;
                $('#dl_ErrorDialog').show();
            }
        });

        function validateFloatKeyPress(el, evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            var number = el.value.split('.');
            if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }

            if (number.length > 1 && charCode == 46) {
                return false;
            }

            var caratPos = getSelectionStart(el);
            var dotPos = el.value.indexOf(".");
            if (caratPos > dotPos && dotPos > -1 && (number[1].length > 1)) {
                return false;
            }
            return true;
        }

        function getSelectionStart(o) {
            if (o.createTextRange) {
                var r = document.selection.createRange().duplicate()
                r.moveEnd('character', o.value.length)
                if (r.text == '') return o.value.length
                return o.value.lastIndexOf(r.text)
            } else return o.selectionStart
        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
    </script>
</body>
</html>
