﻿<%@ page title="TRANSACTIONS" language="C#" autoeventwireup="true" masterpagefile="~/MasterPage/DocMasterPage.master" inherits="Documentation_Transactions_Index, App_Web_wo22q0wf" %>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2 style="margin-bottom:20px"><%: Title %></h2>
    <h4>Transactions</h4>
    <p>To view or download transactions report.</p><br />
    <p>Transactions Menu:</p>
    <ul>
        <li>Details Report - Contain both online banking and credit card transaction records</li>
        <li>Online Banking Report - Only online banking transaction records</li>
        <li>Credit Card Report - Only credit card transaction records</li>
    </ul>
    <img src="../../assets/img/doc_Img/transactions/transactions_menu.png" class="img_info size_img300" /><br />

    <h4>Details Report</h4>
    <img src="../../assets/img/doc_Img//transactions/details_trx_report.png" class="img_info size_img800" /><br />
    <p>Filter by:</p>
    <ul>
        <li>Select Merchant Name</li>
        <li>Date Since/To: Date range to select for generate report</li>
        <li>
            <p>Select Status: </p>
            <ul style="list-style-type: square;display: inline-block;">
                <li>Success</li>
                <li>Failed</li>
                <li>Pending</li>
                <li>Other</li>
            </ul>
        </li>
    </ul>
    <p>Click on 'Get Report' button to get the records.</p>
    <h4>Advanced Search</h4>
    <p>To search for specific record, click on 'Advanced Search' button</p>
    <img src="../../assets/img/doc_Img/reseller/reseller_adv_search.png" class="img_info size_img800" /><br />
    <p>Select/ Enter info to Search then click on 'OK' button</p>
    <h4>Download Details Report</h4>
    <p>To download report, click on &#39;Download Report&#39; button.</p>
    <p>Report example in Excel format</p>
    <img src="../../assets/img/doc_Img/transactions/details_trx_report_download.png" class="img_info size_img800" /><br />


    <h4>Online Banking Report</h4>
    <img src="../../assets/img/doc_Img/transactions/trans_online_banking.png" class="img_info size_img800" /><br /><br />
    <p>Report example in Excel format</p>
    <img src="../../assets/img/doc_Img/transactions/trans_online_banking_download.png" class="img_info size_img800" /><br />

    <h4>Credit Card Report</h4>
    <img src="../../assets/img/doc_Img/transactions/trans_credit_card.png" class="img_info size_img800" /><br /><br />
    <p>Report example in Excel format</p>
    <img src="../../assets/img/doc_Img/transactions/trans_credit_card_download.png" class="img_info size_img800" /><br />
    <div id="next_previous" style="margin-top:50px">
        <a runat="server" href="/Documentation/User/ForgotPassword.aspx" class="previous">&laquo; Previous :  Forgot Password</a>
        <a runat="server" href="/Documentation/Transactions/Reseller.aspx" class="next">Next: Reseller  &raquo;</a>
    </div>
</asp:Content>
