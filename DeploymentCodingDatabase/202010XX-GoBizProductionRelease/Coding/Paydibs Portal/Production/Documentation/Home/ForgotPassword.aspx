﻿<%@ page title="FORGOT PASSWORD" language="C#" masterpagefile="~/MasterPage/DocMasterPage.master" autoeventwireup="true" inherits="Documentation_User_ForgotPassword, App_Web_53ghmbg1" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
   <h2><%: Title %></h2>
    <p>At Login page, click link <span style='color:blue'>Forgot Password?</span></p>
    <img src="../../assets/img/doc_Img/user/login.png" class="img_info size_img300" /><br /><br />
    <p>Insert UserID and click on Forgot Password button</p>
    <img src="../../assets/img/doc_Img/user/forgot_pass.png" class="img_info size_img300" /><br /><br />
    <p>An email will send to User ID.</p>
    <img src="../../assets/img/doc_Img/user/forgot_pass_suc.png" class="img_info size_img300" /><br /><br />
   
    <p>Example Email:</p>
    <img src="../../assets/img/doc_Img/user/account_info_temp_pass.png" class="img_info size_img500" /><br />
    <p>Click on the link in email (Step 2) and it will redirect to portal login page.</p><br />
    <p>Login with temporary password received from email.</p><br />
    <p>Follow by enter new password and confirm new password in 'Change Password Page’. Click on update password button.</p><br />
    <img src="../../assets/img/doc_Img/user/forgot_pass_new.png" class="img_info size_img300" /><br />
    <p>You may now login using the new password.</p>

    <div id="next_previous" style="margin-top:30px">
        <a runat="server" href="/Documentation/Home/UserAccount.aspx" class="previous">&laquo; Previous :  User Account</a>
        <a runat="server" href="/Documentation/Transactions/Index.aspx" class="next">Next: Transactions &raquo;</a>
    </div>
</asp:Content>

