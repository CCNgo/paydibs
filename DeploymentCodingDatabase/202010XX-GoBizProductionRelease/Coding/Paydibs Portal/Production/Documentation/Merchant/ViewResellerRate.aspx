﻿<%@ page title="MERCHANT" language="C#" masterpagefile="~/MasterPage/DocMasterPage.master" autoeventwireup="true" inherits="Documentation_Merchant_ViewResellerRate, App_Web_opb0hncf" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <h4>View Reseller Rate</h4>
    <p>To view reseller rate details.</p>
    <p>View Reseller Rate Menu only can be viewable by who applied as a Paydibs Reseller.</p><br />
    <img src="../../assets/img/doc_Img/merchant/reseller_rate_menu.png" class="img_info size_img300" /><br />
    <h4>Details</h4>
    <ul>
        <li>Payment Method : Type of payment method</li>
        <li>Channel: Channel description</li>
        <li>Rate: Rate applied to this channel's transaction</li>
        <li>Fixed Fee:  Fixed fee to this channel's transaction</li>
        <li>Currency: Currency of channel</li>
    </ul>
    <img src="../../assets/img/doc_Img/merchant/reseller_rate.png" class="img_info size_img800" /><br />
    <div id="next_previous" style="margin-top:50px">
        <a runat="server" href="/Documentation/Merchant/ViewMerchantRate.aspx" class="previous">&laquo; Previous :  View Merchant Rate</a>
        <a runat="server" href="/Documentation/User/ViewUser.aspx" class="next">Next: View User  &raquo;</a>
    </div>
</asp:Content>

