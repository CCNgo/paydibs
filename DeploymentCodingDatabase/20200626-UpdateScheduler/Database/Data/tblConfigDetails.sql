USE [PGAdmin]

DECLARE @FKID uniqueidentifier 
SELECT @FKID = ID FROM [dbo].[tblConfig] WITH (NOLOCK)  WHERE Name = 'Paydibs.Scheduler'

IF NOT Exists (select fk_ID FROM [dbo].[tblConfigDetails] WITH (NOLOCK) WHERE fk_ID = @FKID AND KeyName = 'Paydibs.Scheduler#TransactionsPeriodHours')
BEGIN 
	INSERT INTO [dbo].[tblConfigDetails] 
	(fk_ID,KeyName,KeyValue,Status,CreatedBy,CreatedDateTime) 
	VALUES (@FKID,'Paydibs.Scheduler#TransactionsPeriodHours', '24', 1, 'xevos', GETDATE())
END



