USE [PGAdmin]
GO


IF NOT EXISTS(SELECT ID FROM [dbo].[tblConfig] WITH (NOLOCK) WHERE [Name] = 'Paydibs.Scheduler')
BEGIN
	INSERT INTO [dbo].[tblConfig]
			   ([ID]
			   ,[Name]
			   ,[Description]
			   ,[Status]
			   ,[CreatedBy]
			   ,[CreatedDateTime]
			   ,[UpdateBy]
			   ,[UpdatedDateTime])
		 VALUES
			   (NEWID()
			   ,'Paydibs.Scheduler'
			   ,'Paydibs Scheduler SETTINGS'
			   ,1
			   ,'Xevos'
			   ,GETDATE()
			   ,NULL
			   ,NULL)
END

GO