USE [PGAdmin]
GO

/****** Object:  StoredProcedure [dbo].[spRepTBLateRes_By_BankRespCode]    Script Date: 6/29/2020 10:36:38 AM ******/
DROP PROCEDURE [dbo].[spRepTBLateRes_By_BankRespCode]
GO

/****** Object:  StoredProcedure [dbo].[spRepTBLateRes_By_BankRespCode]    Script Date: 6/29/2020 10:36:38 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spRepTBLateRes_By_BankRespCode] 
	@PGatewayTxnID AS VARCHAR(30),
	@PMerchantID AS VARCHAR(30),
	@PSince AS VARCHAR(30),
	@PTo AS VARCHAR(30),
	@PBankRespCode AS VARCHAR(2),
	@PCurrentStatus AS VARCHAR(1)
	
AS
BEGIN
	DECLARE @sz_Select AS NVARCHAR(MAX)
	
	SET @sz_Select = 'SELECT A.GatewayTxnID, A.MerchantID, A.BankRespCode, A.BankRefNo, A.LastUpdated, C.RespMesg,'
	SET @sz_Select = @sz_Select + '(SELECT B.TxnDescription FROM OB..CL_TxnStatus B WHERE C.TxnStatus = B.TxnStatus) AS CurrentStatus '
	SET @sz_Select = @sz_Select + 'FROM OB..TB_LateRes(NOLOCK) A '
	SET @sz_Select = @sz_Select + 'LEFT JOIN OB..TB_PayRes C on A.GatewayTxnID = C.GatewayTxnID '

	
	SET @sz_Select = @sz_Select + 'WHERE A.LastUpdated BETWEEN ''' + CAST(@PSince AS VARCHAR(30)) + ''' AND ''' + CAST(@PTo  AS VARCHAR(30)) + ''' '
	
	IF (@PMerchantID <> '')
		SET @sz_Select = @sz_Select + 'AND A.MerchantID = ''' +@PMerchantID+ ''' '

	IF (@PGatewayTxnID <> '')
		SET @sz_Select = @sz_Select + 'AND A.GatewayTxnID = ''' + @PGatewayTxnID + ''' '

	SET @sz_Select = @sz_Select + 'AND A.BankRespCode  = ''' + @PBankRespCode + ''' '

	--Not Success
	SET @sz_Select = @sz_Select + 'AND C.TxnStatus  <> ''' + @PCurrentStatus + ''' '

	SET @sz_Select = @sz_Select + 'ORDER BY LastUpdated DESC'

	EXEC (@sz_Select);

END
GO


