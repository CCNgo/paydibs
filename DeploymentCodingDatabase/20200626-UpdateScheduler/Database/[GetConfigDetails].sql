USE [PGAdmin]
GO
/****** Object:  StoredProcedure [dbo].[GetConfigDetails]    Script Date: 29/06/2020 11:01:12 ******/
DROP PROCEDURE [GetConfigDetails]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author : Xevos
-- Create Date : 29/06/2020
-- Description : Get Configuration Details            
-- Use By : Scheduler
-- =============================================

CREATE PROCEDURE [GetConfigDetails]
@ConfigName [varchar](50)
AS
BEGIN
	SET NOCOUNT ON 	
	
	SELECT CD.KeyName, CD.KeyValue
    FROM [PGAdmin].dbo.tblConfig C
		INNER JOIN [PGAdmin].dbo.tblConfigDetails CD
		ON C.ID = CD.FK_ID AND C.[Status] = 1 And CD.[Status] = 1
    WHERE C.Name = @ConfigName
END
GO
