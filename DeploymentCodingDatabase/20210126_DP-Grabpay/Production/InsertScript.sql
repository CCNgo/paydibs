USE [OB]
GO
--Channel
declare @ChannelID AS INT = 8
declare @HostID AS INT = 10
declare @TxnStatusActionId As INT = 5

SET IDENTITY_INSERT [dbo].[Channel] ON 
INSERT [dbo].[Channel] ([ChannelID], [IPAddress], [PortNumber], [QueryIPAddress], [QueryPortNumber], 
[CancelIPAddress], [CancelPortNumber], [RevIPAddress], [RevPortNumber], [AckIPAddress], [AckPortNumber], 
[ReturnIPAddresses], [ReturnURL], [ReturnURL2], [TimeOut], [DBConnStr], [ServerCertName], [CfgFile], 
[Protocol], [Desc], [RecStatus]) 
VALUES (@ChannelID, N'https://gw.dragonpay.ph/Pay.aspx', 443, N'https://gw.dragonpay.ph/DragonPayWebService/MerchantService.asmx', 443, 
N'', 443, N'', 0, N'', 0, N'', N'', N'', 900, N'', N'', N'templates\DPGrabPay.txt', N'TLS', N'DPGrabPay', 1)
SET IDENTITY_INSERT [dbo].[Channel] OFF

--Host
SET IDENTITY_INSERT [dbo].[Host] ON 
INSERT [dbo].[Host] ([HostID], [HostName], [HostCode], [SvcTypeID], [PymtMethod], 
[SignBy], [PaymentTemplate], [SecondEntryTemplate], [MesgTemplate], [NeedReplyAcknowledgement], 
[NeedRedirectOTP], [Require2ndEntry], [AllowQuery], [AllowReversal], [MaxRefund], [ChannelID], 
[Timeout], [OTCRevTimeout], [OTCGenMethod], [TxnStatusActionID], [HostReplyMethod], [OSPymtCode], 
[GatewayTxnIDFormat], [MID], [MerchantPassword], [ReturnKey], [HashMethod], [SecretKey], [InitVector], 
[LogoPath], [RepStartTime], [QueryFlag], [SettlePeriod], [SettleTime], [HostMDR], [LogRes], [DateActivated], 
[DateDeactivated], [RecStatus], [Desc], [FixedCurrency], [IsExponent], [FloorAmt], [CeilingAmt], [HostDesc]) 
VALUES (@HostID, N'DPGrabPay', NULL, 2, N'OB', 0, N'RTTemplates\redirect_post.html', N'', N'', 0, 0, 0, 1, 0, 0, @ChannelID, 120, 0, 0, 
@TxnStatusActionId, 1, N'', 1, NULL, N'', N'', N'SHA1_2', N'', N'', N'DPGrabPay.png', N'00:00', 1, 1, NULL, 
CAST(0.00 AS Decimal(18, 2)), 1, CAST(N'2021-01-25T00:00:00.000' AS DateTime), CAST(N'2099-12-31 00:00:00.000' AS DateTime), 
1, N'DPGrabPay', NULL, 0, CAST(0.00 AS Decimal(18, 2)), CAST(2.00 AS Decimal(18, 2)), N'DPGrabPay')
SET IDENTITY_INSERT [dbo].[Host] OFF





   insert  into [OB].[dbo].[HostCountry]
	([CountryID]
      ,[HostID]
      ,[CurrencyCode]
      ,[RecStatus]
	) values
	(
	175,
	@HostID,
	'PHP',
	1
	)



--TxnStatusAction

--INSERT [dbo].[TxnStatusAction] ([ActionID], [TxnType], [TxnField], [TxnStatus], [Action], [Desc], [Priority], [RecStatus]) 
--VALUES ( @TxnStatusActionId, N'PAY;QUERY', NULL, N'S', 1, N'Success', 1, 1)
--INSERT [dbo].[TxnStatusAction] ([ActionID], [TxnType], [TxnField], [TxnStatus], [Action], [Desc], [Priority], [RecStatus]) 
--VALUES ( @TxnStatusActionId, N'PAY;QUERY', NULL, N'P', 3, N'Pending', 1, 1)
--INSERT [dbo].[TxnStatusAction] ([ActionID], [TxnType], [TxnField], [TxnStatus], [Action], [Desc], [Priority], [RecStatus]) 
--VALUES ( @TxnStatusActionId, N'PAY;QUERY', NULL, N'!S', 2, N'Failed', 1, 1)




USE [PG]

--Terminals_PreSetCredential
INSERT [dbo].[Terminals_PreSetCredential] ([HostID], [PaymentMethod], [Bank], [FieldName], [DBFieldName], [Encrypt], [DefaultValue], 
[Hex], [Currency]) VALUES (@HostID, N'OB', N'DPGrabPay', N'MerchantID', N'MID', N'0', N'PAYDIBS2', 0, 'PHP')
INSERT [dbo].[Terminals_PreSetCredential] ([HostID], [PaymentMethod], [Bank], [FieldName], [DBFieldName], [Encrypt], [DefaultValue], 
[Hex],[Currency]) VALUES (@HostID, N'OB', N'DPGrabPay', N'Secret Key (REQ)', N'Password', N'0', N'6A90E23BDCAE9DDF001F8570F5516FC6', 0, 'PHP')
INSERT [dbo].[Terminals_PreSetCredential] ([HostID], [PaymentMethod], [Bank], [FieldName], [DBFieldName], [Encrypt], [DefaultValue], 
[Hex],[Currency]) VALUES (@HostID, N'OB', N'DPGrabPay', N'Secret Key (RES)', N'ReturnKey', N'0', N'6A90E23BDCAE9DDF001F8570F5516FC6', 0, 'PHP')



  insert into [OB].[dbo].[HostGroup]
  (BankID, BankStatus, BankCode, HostID, LogoPath, BankDesc, DateModified, BankType)
  values 
  ('GRPY', 1, null, 10, 'ph-ob-GRPY.png', 'GrabPay', GETDATE(), 0)