DECLARE @HostID AS INT = 9
DECLARE @ChannelID AS INT = 7
DECLARE @TxnStatusActionID AS INT = 7

IF NOT Exists (select HostID FROM OB.dbo.Host WITH (NOLOCK) WHERE HostID = @HostID AND HostName = 'TNG')
BEGIN 
SET IDENTITY_INSERT OB.dbo.Host ON
INSERT INTO OB.dbo.Host([HostID], [HostName], [HostCode], [SvcTypeID], [PymtMethod], [SignBy], [PaymentTemplate], [SecondEntryTemplate], [MesgTemplate], [NeedReplyAcknowledgement], [NeedRedirectOTP], [Require2ndEntry], [AllowQuery], [AllowReversal], [MaxRefund], [ChannelID], [Timeout], [OTCRevTimeout], [OTCGenMethod], [TxnStatusActionID], [HostReplyMethod], [OSPymtCode], [GatewayTxnIDFormat], [MID], [MerchantPassword], [ReturnKey], [HashMethod], [SecretKey], [InitVector], [LogoPath], [RepStartTime], [QueryFlag], [SettlePeriod], [SettleTime], [HostMDR], [LogRes], [DateActivated], [DateDeactivated], [RecStatus], [Desc], [FixedCurrency], [IsExponent], [FloorAmt], [CeilingAmt], [HostDesc]) VALUES 
(@HostID, N'TNG', NULL, 2, N'WA', 0, N'RTTemplates\redirect_post.html', N'', N'', 0, 0, 0, 1, 1, -1, @ChannelID, 200, 0, 0, @TxnStatusActionID, 1, N'', 1, NULL, N'', N'', N'TNG', N'', N'', N'tng.png', N'00:00', 1, 1, NULL, CAST(0.00 AS Decimal(18, 2)), 1, CAST(N'2020-07-23T11:44:00.000' AS DateTime), CAST(N'2099-12-31T00:00:00.000' AS DateTime), 1, N'TnG eWallet', NULL, 0, CAST(-1.00 AS Decimal(18, 2)), CAST(-1.00 AS Decimal(18, 2)), N'TNG')
SET IDENTITY_INSERT OB.dbo.Host OFF
END