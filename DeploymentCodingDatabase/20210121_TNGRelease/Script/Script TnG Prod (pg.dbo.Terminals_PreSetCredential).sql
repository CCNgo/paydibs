SET IDENTITY_INSERT [dbo].[Terminals_PreSetCredential] ON 
INSERT [dbo].[Terminals_PreSetCredential] ([ID], [HostID], [PaymentMethod], [Bank], [FieldName], [DBFieldName], [Encrypt], [DefaultValue], 
[Hex],[Currency]) VALUES (37, 9, N'WA', N'TNG', N'Client Id', N'MID', N'0', N'MID', 0, 'MYR')
INSERT [dbo].[Terminals_PreSetCredential] ([ID], [HostID], [PaymentMethod], [Bank], [FieldName], [DBFieldName], [Encrypt], [DefaultValue], 
[Hex],[Currency]) VALUES (38, 9, N'WA', N'TNG', N'Client Secret', N'Password', N'0', N'Password', 0, 'MYR')
SET IDENTITY_INSERT [dbo].[Terminals_PreSetCredential] OFF
