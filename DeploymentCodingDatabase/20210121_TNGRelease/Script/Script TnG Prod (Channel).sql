DECLARE @ChannelID AS INT = 7
IF NOT Exists (select ChannelID FROM OB.dbo.Channel WITH (NOLOCK) WHERE ChannelID = @ChannelID AND IPAddress = 'http://paymentgatewayapi.paydibs.com/order/sales/tngsales')
BEGIN 
SET IDENTITY_INSERT OB.dbo.Channel ON
INSERT INTO OB.dbo.Channel([ChannelID], [IPAddress], [PortNumber], [QueryIPAddress], [QueryPortNumber], [CancelIPAddress], [CancelPortNumber], [RevIPAddress], [RevPortNumber], [AckIPAddress], [AckPortNumber], [ReturnIPAddresses], [ReturnURL], [ReturnURL2], [TimeOut], [DBConnStr], [ServerCertName], [CfgFile], [Protocol], [Desc], [RecStatus]) VALUES 
(@ChannelID, N'http://paymentgatewayapi.paydibs.com/order/sales/tngsales', 0, N'https://api.tngdigital.com.my/alipayplus/acquiring/order/query.htm', 0, N'', 0, N'', 0, N'', 0, N'', N'', N'', 1500, N'', N'', N'templates\TNG.txt', N'TLS12', N'TnG eWallet', 1)
SET IDENTITY_INSERT OB.dbo.Channel OFF
END