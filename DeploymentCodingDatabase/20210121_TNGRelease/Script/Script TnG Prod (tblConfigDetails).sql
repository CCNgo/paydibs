IF NOT Exists (select fk_ID FROM PGAdmin.dbo.tblConfigDetails WITH (NOLOCK) WHERE fk_ID = 'E73CC244-38E7-4A70-B248-D429A2D8ECF7' AND KeyName = 'Paydibs.PaymentGatewayAPI.TNG#clientId')
BEGIN 
INSERT INTO PGAdmin.dbo.tblConfigDetails ([fk_ID], [KeyName], [KeyValue], [Status], [CreatedBy], [CreatedDateTime], [UpdateBy], [UpdatedDateTime]) VALUES (N'E73CC244-38E7-4A70-B248-D429A2D8ECF7', N'Paydibs.PaymentGatewayAPI.TNG#clientId', N'2021011513093600009540', 1, N'ngo', CAST(N'2020-09-03T15:35:35.580' AS DateTime), NULL, NULL)
END

IF NOT Exists (select fk_ID FROM PGAdmin.dbo.tblConfigDetails WITH (NOLOCK) WHERE fk_ID = 'E73CC244-38E7-4A70-B248-D429A2D8ECF7' AND KeyName = 'Paydibs.PaymentGatewayAPI.TNG#clientSecret')
BEGIN 
INSERT INTO PGAdmin.dbo.tblConfigDetails ([fk_ID], [KeyName], [KeyValue], [Status], [CreatedBy], [CreatedDateTime], [UpdateBy], [UpdatedDateTime]) VALUES (N'E73CC244-38E7-4A70-B248-D429A2D8ECF7', N'Paydibs.PaymentGatewayAPI.TNG#clientSecret', N'2021011513093600009540UcNXtZ', 1, N'ngo', CAST(N'2020-09-03T15:35:35.580' AS DateTime), NULL, NULL)
END

IF NOT Exists (select fk_ID FROM PGAdmin.dbo.tblConfigDetails WITH (NOLOCK) WHERE fk_ID = 'E73CC244-38E7-4A70-B248-D429A2D8ECF7' AND KeyName = 'Paydibs.PaymentGatewayAPI.TNG#merchantId')
BEGIN 
INSERT INTO PGAdmin.dbo.tblConfigDetails ([fk_ID], [KeyName], [KeyValue], [Status], [CreatedBy], [CreatedDateTime], [UpdateBy], [UpdatedDateTime]) VALUES (N'E73CC244-38E7-4A70-B248-D429A2D8ECF7', N'Paydibs.PaymentGatewayAPI.TNG#merchantId', N'217120000003246741732', 1, N'ngo', CAST(N'2020-09-03T15:35:35.580' AS DateTime), NULL, NULL)
END

IF NOT Exists (select fk_ID FROM PGAdmin.dbo.tblConfigDetails WITH (NOLOCK) WHERE fk_ID = 'E73CC244-38E7-4A70-B248-D429A2D8ECF7' AND KeyName = 'Paydibs.PaymentGatewayAPI.TNG#productCode')
BEGIN 
INSERT INTO PGAdmin.dbo.tblConfigDetails ([fk_ID], [KeyName], [KeyValue], [Status], [CreatedBy], [CreatedDateTime], [UpdateBy], [UpdatedDateTime]) VALUES (N'E73CC244-38E7-4A70-B248-D429A2D8ECF7', N'Paydibs.PaymentGatewayAPI.TNG#productCode', N'51051000101000100001', 1, N'ngo', CAST(N'2020-09-03T15:35:35.580' AS DateTime), NULL, NULL)
END

IF NOT Exists (select fk_ID FROM PGAdmin.dbo.tblConfigDetails WITH (NOLOCK) WHERE fk_ID = 'E73CC244-38E7-4A70-B248-D429A2D8ECF7' AND KeyName = 'Paydibs.PaymentGatewayAPI.TNG#POSTURL')
BEGIN 
INSERT INTO PGAdmin.dbo.tblConfigDetails ([fk_ID], [KeyName], [KeyValue], [Status], [CreatedBy], [CreatedDateTime], [UpdateBy], [UpdatedDateTime]) VALUES (N'E73CC244-38E7-4A70-B248-D429A2D8ECF7', N'Paydibs.PaymentGatewayAPI.TNG#POSTURL', N'https://api.tngdigital.com.my/alipayplus/acquiring/order/create.htm', 1, N'ngo', CAST(N'2020-09-03T15:35:35.580' AS DateTime), NULL, NULL)
END

IF NOT Exists (select fk_ID FROM PGAdmin.dbo.tblConfigDetails WITH (NOLOCK) WHERE fk_ID = 'E73CC244-38E7-4A70-B248-D429A2D8ECF7' AND KeyName = 'Paydibs.PaymentGatewayAPI.TNG#URLPAY_RETURN')
BEGIN 
INSERT INTO PGAdmin.dbo.tblConfigDetails ([fk_ID], [KeyName], [KeyValue], [Status], [CreatedBy], [CreatedDateTime], [UpdateBy], [UpdatedDateTime]) VALUES (N'E73CC244-38E7-4A70-B248-D429A2D8ECF7', N'Paydibs.PaymentGatewayAPI.TNG#URLPAY_RETURN', N'https://paymentgatewayapi.paydibs.com/order/sales/orderFinishFinal', 1, N'ngo', CAST(N'2020-09-03T15:35:35.593' AS DateTime), NULL, NULL)
END

IF NOT Exists (select fk_ID FROM PGAdmin.dbo.tblConfigDetails WITH (NOLOCK) WHERE fk_ID = 'E73CC244-38E7-4A70-B248-D429A2D8ECF7' AND KeyName = 'Paydibs.PaymentGatewayAPI.TNG#privatekey')
BEGIN 
INSERT INTO PGAdmin.dbo.tblConfigDetails ([fk_ID], [KeyName], [KeyValue], [Status], [CreatedBy], [CreatedDateTime], [UpdateBy], [UpdatedDateTime]) VALUES (N'E73CC244-38E7-4A70-B248-D429A2D8ECF7', N'Paydibs.PaymentGatewayAPI.TNG#privatekey', N'MIIEpAIBAAKCAQEAvYCV8mn6d9YyDnWc+sQV5BHHUETUmSPl0OxB3dKe7gAL64gJqU/RssOrIerKS3MfEHov1ULBvmKFXPIzhHn302f4i1y9iVIIpbBdfguUdnKhtD8ANLVXoK35dRDI2KEgdV7vlejbvQ8SBYu6aj6/UMqY4KH68hw+2JmrwDsyq2XeW+On655kYYEOcDH2bZFpZ3f9mYuxaJw7t2fafR9U19eyEGvPPE0y8aIoiZ7ITo0cnbd2/pUR9T+NhDDbSseqOEmpLUqbFwofgq5H3fJgUQDsqFk0cCk49qHFaDNmP6F3ZVENHEwoGyjl75GLWlotwmV0ItJjufBCpwc/fY7QnQIDAQABAoIBACWZPKcDRfG+6uGZjlLm0t0UdfJEJdZYnAFuzsa/Qk9AbR0HObuBHb8VOYAaQJbluXUgeRscCqrGi2VcnMIdHQGx8EteVDDJuPN+4Z3fmE5EAjdsBJNwo3k663LBqMfqeDExa+HXeLmzIsU86viniHf0Ko7vNIYS+pkpLD2QwUXmMea0kGuMavWT5fAmPOL9Pxr6nRHoabURaiWa7ip9BWf7szTej9pvpOddldKj35iDFmwb0rlbvxo1I1pOv8lQFCqJbb6j7q9HneugoeRbyufH3PBLNyEsvZNgXDg1tTQ8ztyqMwB9Z+oXw2s6e3BlRVfu8NXxWMKrUvV3keqkU6ECgYEA3s8OU8W+PprlQTmyXA44BhciyRepc93X+xhmn4Zp5pA55F9Z/oahzmwgkzDUwntrIyjs/efk60yOvnEIWXPTn4NF7L6/gEOvaf9YQFpeDbZefJDpy438utS97h+XidCkGZYf+gOU4bkcybHUbZ2x2tuQEoeVf3bkUq9ju6THcAUCgYEA2btdtBCEfbOSxkMdZlVOSD0aeb0sfZOuyd03dHai2PzC7k2XhTUFFVtYVXSiNZ55Y7ABwnEQuuihvJlUDf7h+o0OcM9LoAJX1Kd92PPdawuCUB2TInjs8g04F8T/X+PMBuXzFZpfGuwasFo1jYG83QHhAGkCwnAhRDwuXYKy+bkCgYEAuq4FNQ9TTKT/PF0ZIaQQMJol3RLkOV5AXfOd2D8Ib9ObjcqOXoFzBL73tBuPx7sckwLxPfOTi8pDNzEBa+FuCBI2/hr73H3ZdRgqWxDcCOUp2XiZSqGWhPtGf0Mc9q1HMODlvdZOYal1g1BpycX8hIC9/6h2C6wTtWuaRlfTefkCgYEApyuSgEQGVbTgRlUVZ5Krrg8sGrhCRpBzLQ4qG8NSuSMoT2tHoMDajFE7+zC3sk9giEeEslO5MqiWNYXL4YsG2iYfJkIioFHsOZv3pfFm2V9WyEhMxbTux9GadbWNFYSDC/DVuIpz5unMfAZnA6TO6ykcJgJ6WetZv1gtiSJmZBECgYB9t4BwoTjzM1WwT/xW5if0amxen9XIXxP7HRIqG88pmshdVRK18kTCXok2LHMZer6HizVbIvjIrd48RbsKLkVEWvUupO9cWotjX2f/oVvCZlWyh+d4oeFR92DOCeXXSaQR4FnYhO3ojA++vj+65TuSQM80Qx+pGzmcUACpwfYHGQ==', 1, N'ngo', CAST(N'2020-09-03T15:35:35.593' AS DateTime), NULL, NULL)
END

IF NOT Exists (select fk_ID FROM PGAdmin.dbo.tblConfigDetails WITH (NOLOCK) WHERE fk_ID = 'E73CC244-38E7-4A70-B248-D429A2D8ECF7' AND KeyName = 'Paydibs.PaymentGatewayAPI.TNG#version')
BEGIN 
INSERT INTO PGAdmin.dbo.tblConfigDetails ([fk_ID], [KeyName], [KeyValue], [Status], [CreatedBy], [CreatedDateTime], [UpdateBy], [UpdatedDateTime]) VALUES (N'E73CC244-38E7-4A70-B248-D429A2D8ECF7', N'Paydibs.PaymentGatewayAPI.TNG#version', N'1.0', 1, N'ngo', CAST(N'2020-09-03T15:35:35.593' AS DateTime), NULL, NULL)
END

IF NOT Exists (select fk_ID FROM PGAdmin.dbo.tblConfigDetails WITH (NOLOCK) WHERE fk_ID = 'E73CC244-38E7-4A70-B248-D429A2D8ECF7' AND KeyName = 'Paydibs.PaymentGatewayAPI.TNG#URLNOTIFICATION')
BEGIN 
INSERT INTO PGAdmin.dbo.tblConfigDetails ([fk_ID], [KeyName], [KeyValue], [Status], [CreatedBy], [CreatedDateTime], [UpdateBy], [UpdatedDateTime]) VALUES (N'E73CC244-38E7-4A70-B248-D429A2D8ECF7', N'Paydibs.PaymentGatewayAPI.TNG#URLNOTIFICATION', N'https://paymentgatewayapi.paydibs.com/order/sales/orderFinish', 1, N'ngo', CAST(N'2020-09-04T15:55:43.243' AS DateTime), NULL, NULL)
END

IF NOT Exists (select fk_ID FROM PGAdmin.dbo.tblConfigDetails WITH (NOLOCK) WHERE fk_ID = 'E73CC244-38E7-4A70-B248-D429A2D8ECF7' AND KeyName = 'Paydibs.PaymentGatewayAPI.TNG#RefundUrl')
BEGIN 
INSERT INTO PGAdmin.dbo.tblConfigDetails ([fk_ID], [KeyName], [KeyValue], [Status], [CreatedBy], [CreatedDateTime], [UpdateBy], [UpdatedDateTime]) VALUES (N'E73CC244-38E7-4A70-B248-D429A2D8ECF7', N'Paydibs.PaymentGatewayAPI.TNG#RefundUrl', N'https://api.tngdigital.com.my/alipayplus/acquiring/refund/query.htm', 1, N'ngo', CAST(N'2020-09-04T15:55:43.243' AS DateTime), NULL, NULL)
END

IF NOT Exists (select fk_ID FROM PGAdmin.dbo.tblConfigDetails WITH (NOLOCK) WHERE fk_ID = 'E73CC244-38E7-4A70-B248-D429A2D8ECF7' AND KeyName = 'Paydibs.PaymentGatewayAPI.TNG#timeoutPeriod')
BEGIN 
INSERT INTO PGAdmin.dbo.tblConfigDetails ([fk_ID], [KeyName], [KeyValue], [Status], [CreatedBy], [CreatedDateTime], [UpdateBy], [UpdatedDateTime]) VALUES (N'E73CC244-38E7-4A70-B248-D429A2D8ECF7', N'Paydibs.PaymentGatewayAPI.TNG#timeoutPeriod', N'600', 1, N'ngo', CAST(N'2020-09-04T15:55:43.243' AS DateTime), NULL, NULL)
END

IF NOT Exists (select fk_ID FROM PGAdmin.dbo.tblConfigDetails WITH (NOLOCK) WHERE fk_ID = 'E73CC244-38E7-4A70-B248-D429A2D8ECF7' AND KeyName = 'Paydibs.PaymentGatewayAPI.TNG#QueryUrl')
BEGIN 
INSERT INTO PGAdmin.dbo.tblConfigDetails ([fk_ID], [KeyName], [KeyValue], [Status], [CreatedBy], [CreatedDateTime], [UpdateBy], [UpdatedDateTime]) VALUES (N'E73CC244-38E7-4A70-B248-D429A2D8ECF7', N'Paydibs.PaymentGatewayAPI.TNG#QueryUrl', N'https://api.tngdigital.com.my/alipayplus/acquiring/order/query.htm', 1, N'ngo', CAST(N'2020-09-04T15:55:43.243' AS DateTime), NULL, NULL)
END
