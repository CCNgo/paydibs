﻿using LoggerII;
using Newtonsoft.Json;
using PaydibsPortal.BLL;
using PaydibsPortal.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;

public partial class GetMerchantChannel : System.Web.UI.Page
{
    MMGMTController mc = new MMGMTController();
    HomeController hc = null;
    CurrentUserStat cus = null;
    Logger logger = new Logger();
    CLoggerII objLoggerII = new CLoggerII();
    protected string jdata { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session[SessionType.CurrentUser] == null)
        {
            Response.Redirect("Default.aspx");
            return;
        }

        cus = (CurrentUserStat)Session[SessionType.CurrentUser];
        hc = new HomeController(cus.DomainID);
        string url = Request.Url.AbsoluteUri;
        int sDomainID = Convert.ToInt32(Request.QueryString["Merchant"]);

        var logObject = new
        {
            LoginUser = cus.UserID,
            DomainID = sDomainID,
            URL = url
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();
            var channelList = mc.Do_Get_TerminalHost(sDomainID);

            if (channelList == null)
            {
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "GetMerchantChannel(Error) - " + logJson + mc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddMerchantRate), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + mc.system_err.ToString());
                return;
            }

            var ChannelType = (channelList.Select(s => new { s.TypeValue }).GroupBy(g => g.TypeValue)).ToList();
            if (ChannelType != null)
            {
                MerchantChannelList json = new MerchantChannelList();
                json.MID = sDomainID.ToString();
                foreach (var type in ChannelType)
                {
                    if (string.Compare(type.Key.Trim(), "OB") == 0)
                        json.OB = GetChannelList(sDomainID, type.Key, channelList);
                    if (string.Compare(type.Key.Trim(), "WA") == 0)
                        json.WA = GetChannelList(sDomainID, type.Key, channelList);
                    if (string.Compare(type.Key.Trim(), "CC") == 0)
                        json.CC = GetChannelList(sDomainID, type.Key, channelList);
                }

                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "GetMerchantChannel - " + logJson);
                hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.ViewLog, 800000, "GetMerchantChannel.aspx");
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ViewLog), MethodBase.GetCurrentMethod().Name, logJson);
                jdata = new JavaScriptSerializer().Serialize(json);
            }
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "GetMerchantChannel(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.AddMerchantRate, 800001, "GetMerchantChannel.aspx");
            //Added by DANIEl on 7 Jan 2019.
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddMerchantRate), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }

    protected List<ChannelInfo> GetChannelList(int MID, string type, TerminalHost[] data)
    {
        objLoggerII = logger.InitLogger();
        int length = data.Count();
        List<ChannelInfo> infolist = new List<ChannelInfo>();

        var channelRateList = mc.Do_Get_MerchantRateHost(MID);
        if (channelRateList == null)
        {
            var logObject = new
            {
                ErrorMessage = "Channel Rate List is empty"
            };
            var logJson = JsonConvert.SerializeObject(logObject);
            //HiddenField1.Value = mc.err_msg;
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                       "GetMerchantChannel(Error) - " + logJson + mc.system_err.ToString());
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddMerchantRate), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + mc.system_err.ToString());
            return null;
        }

        foreach (var ChannelData in data)
        {
            if (type == ChannelData.TypeValue)
            {
                ChannelInfo channelObject = new ChannelInfo();
                channelObject.CurrencyCode = ChannelData.CurrencyCode;
                channelObject.PaymentMethod = ChannelData.TypeValue;
                channelObject.HostId = ChannelData.HostID;
                channelObject.HostDesc = ChannelData.HostDesc;
                channelObject.Type = ChannelData.Type;
                channelObject.Rate = "0.0000";
                channelObject.FixedFee = "0.0000";
                foreach (var rateList in channelRateList)
                {
                    if (ChannelData.HostDesc.Trim() == rateList.HostName.Trim() && ChannelData.Type.Trim() == rateList.PymtMethod.Trim() && ChannelData.CurrencyCode.Trim() == rateList.Currency.Trim())
                    {
                        channelObject.Rate = (rateList.Rate).ToString();
                        channelObject.FixedFee = (rateList.FixedFee).ToString();
                    }
                }
                infolist.Add(channelObject);
            }
        }

        return infolist;
    }
}