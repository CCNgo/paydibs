﻿using GemBox.Spreadsheet;
using LoggerII;
using Newtonsoft.Json;
using PaydibsPortal.BLL;
using PaydibsPortal.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;

public partial class Backoffice_FDS_SummaryReportByMerchant : System.Web.UI.Page
{
    HomeController hc = null;
    FDSControllercs fds = null;
    CurrentUserStat cus = null;
    protected string totalSuccess { get; set; }
    protected string totalFailed { get; set; }
    protected string SuccessTransaction { get; set; }
    protected string FailedTransaction { get; set; }
    protected bool onlyone { get; set; }
    decimal value = 0.00M;

    protected void Page_Load(object sender, EventArgs e)
    {
        HiddenField1.Value = "";
        HiddenField2.Value = "";

        if (Session[SessionType.CurrentUser] == null)
        {
            Response.Redirect("../Dashboard.aspx");
            return;
        }

        cus = (CurrentUserStat)Session[SessionType.CurrentUser];
        hc = new HomeController(cus.DomainID);
        fds = new FDSControllercs();

        var permission = hc.LoadUserPermission(cus.UserID, Convert.ToInt16(OpsIDEnum.SummaryReportByMerchant));
        if (permission[0].IsUserAllowed == "0")
        {
            Response.Redirect("../../Backoffice/Dashboard.aspx");
            return;
        }

        if (!IsPostBack)
        {
            var merchant_L = hc.Do_GetMerchantList(1, 0);
            DD_merchant.DataSource = merchant_L;
            DD_merchant.DataTextField = "DomainDesc";
            DD_merchant.DataValueField = "DomainID";
            DD_merchant.DataBind();

            DateTime sSince = Convert.ToDateTime(TB_Since.Text.Trim() + " 00:00:00");
            DateTime sTo = Convert.ToDateTime(TB_To.Text.Trim() + " 23:59:59");
            TB_Since.Text = sSince.ToString("yyyy-MM-dd");
            TB_To.Text = sTo.ToString("yyyy-MM-dd");

            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.SummaryReportByMerchant), MethodBase.GetCurrentMethod().Name, "Page_Load Method");
        }
    }
    protected void btn_GetReport_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        int sDomainID = Convert.ToInt32(DD_merchant.SelectedValue);
        DateTime sSince = Convert.ToDateTime(TB_Since.Text.Trim() + " 00:00:00");
        DateTime sTo = Convert.ToDateTime(TB_To.Text.Trim() + " 23:59:59");

        var logObject = new
        {
            LoginUser = cus.UserID,
            DomainID = sDomainID,
            Since = sSince,
            To = sTo
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();
            string err_msg = GenerateMerchantSummaryReport(sSince, sTo, sDomainID);
            if (!(string.IsNullOrEmpty(err_msg)))
            {
                HiddenField1.Value = err_msg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                       "SummaryReport(Error) - " + logJson + fds.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.SummaryReportByMerchant), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + fds.system_err.ToString());
                return;
            }
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "SummaryReport - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.SummaryReportByMerchant, 800000, "SummaryReport.aspx");
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.SummaryReportByMerchant), MethodBase.GetCurrentMethod().Name, logJson);
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "SummaryReport(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.SummaryReportByMerchant, 800001, "SummaryReport.aspx");
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.SummaryReportByMerchant), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }
    protected void btn_Download_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        int sDomainID = Convert.ToInt32(DD_merchant.SelectedValue);
        string sDomainName = DD_merchant.SelectedItem.Text;
        DateTime sSince = Convert.ToDateTime(TB_Since.Text.Trim() + " 00:00:00");
        DateTime sTo = Convert.ToDateTime(TB_To.Text.Trim() + " 23:59:59");
        var logObject = new
        {
            LoginUser = cus.UserID,
            DomainID = sDomainID,
            DomainName = sDomainName,
            Since = sSince,
            To = sTo
        };
        var logJson = JsonConvert.SerializeObject(logObject);
        try
        {
            objLoggerII = logger.InitLogger();
            Merchant_Summary_Report[] data = fds.Do_MerchantSummaryReport(sSince, sTo, sDomainID);
            data = data.Where(d => d.Success > value || d.Failed > value).ToArray();
            if (data == null)
            {
                HiddenField1.Value = fds.err_msg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                       "SummaryReport(Error) - " + logJson + fds.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.SummaryReportByMerchant), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + fds.system_err.ToString());
                return;
            }

            ExcelFile ef = new ExcelFile();
            string PhysicalPath = Server.MapPath("Report/Report.xls");
            string SaveAsFileName = "SummaryReport_Merchant_" + sDomainName + "_" + sSince.ToString("dd-MM-yyyy") + "-" + sTo.ToString("dd-MM-yyyy") + ".xls";
            string SavePhysicalPath = Server.MapPath("Report/" + SaveAsFileName);
            ef.LoadXls(PhysicalPath);

            DataTable dt = ToDataTable(data);
            ExcelWorksheet ws = ef.Worksheets[0];
            ws.InsertDataTable(dt, "A1", true);
            ws.Cells["B" + (data.Count() + 2)].Value = "=SUM(B2:B" + (data.Count() + 1) + ")";
            ws.Cells["C" + (data.Count() + 2)].Value = "=SUM(C2:C" + (data.Count() + 1) + ")";
            ws.Cells["D" + (data.Count() + 2)].Value = "=SUM(D2:D" + (data.Count() + 1) + ")";
            ws.Cells["E" + (data.Count() + 2)].Value = "=SUM(E2:E" + (data.Count() + 1) + ")";
            ef.SaveXls(SavePhysicalPath);

            System.IO.FileInfo file = new System.IO.FileInfo(Server.MapPath("Report/" + SaveAsFileName));
            if (file.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                Response.AddHeader("Content-Length", file.Length.ToString());
                Response.ContentType = "application/vnd.ms-excel";
                Response.WriteFile(file.FullName);

                Response.Flush();
                file.Delete();
            }
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "SummaryReport(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.SummaryReportByMerchant, 800001, "SummaryReport.aspx");
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.SummaryReportByMerchant), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }
    string GenerateMerchantSummaryReport(DateTime PSince, DateTime PTo, int PDomainID)
    {
        try
        {

            Merchant_Summary_Report[] data = fds.Do_MerchantSummaryReport(PSince, PTo, PDomainID);
            data = data.Where(d => d.Success > value || d.Failed > value).ToArray();
            if (data.Count() == 1)
                onlyone = false;
            else
                onlyone = true;
            Repeater2.DataSource = data;
            Repeater2.DataBind();
            //decimal _sumSuccess = data.Sum(s => s.Success);
            //decimal _sumFailed = data.Sum(f => f.Failed);
            totalSuccess = data.Sum(s => s.Success).ToString();
            SuccessTransaction = data.Sum(s => s.TotalSuccess).ToString();
            totalFailed = data.Sum(f => f.Failed).ToString();
            FailedTransaction = data.Sum(s => s.TotalFailed).ToString();
            if (data == null)
                return fds.err_msg;

            return string.Empty;
        }
        catch (Exception ex)
        {
            return ex.Message.ToString();
        }
    }

    public static DataTable ToDataTable<T>(IList<T> data)
    {
        PropertyDescriptorCollection properties =
            TypeDescriptor.GetProperties(typeof(T));
        DataTable table = new DataTable();
        foreach (PropertyDescriptor prop in properties)
            table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
        foreach (T item in data)
        {
            DataRow row = table.NewRow();
            foreach (PropertyDescriptor prop in properties)
                row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
            table.Rows.Add(row);
        }
        return table;
    }
}