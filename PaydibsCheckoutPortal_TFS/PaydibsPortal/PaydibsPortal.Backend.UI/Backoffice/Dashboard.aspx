﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Dashboard.aspx.cs" Inherits="Backoffice_Default" %>

<!DOCTYPE html>
<%@ Register TagPrefix="uc" TagName="Left_Side_Bar" Src="~/UserControl/LeftSideBar.ascx" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Paydibs-Checkout Dashboard</title>
    <link rel="stylesheet" type="text/css" href="../../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/material-design-icons/css/material-design-iconic-font.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/select2/css/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/bootstrap-slider/css/bootstrap-slider.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/morrisjs/morris.css" />
    <link rel="stylesheet" href="../../assets/css/app.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:HiddenField ID="HiddenField2" runat="server" />
        <asp:HiddenField ID="HiddenField1" runat="server" />
        <div class="be-wrapper be-collapsible-sidebar be-collapsible-sidebar-collapsed">
            <uc:Left_Side_Bar ID="Left_Side_Bar" runat="server" />
            <div class="be-content">
                <div class="main-content container-fluid">
                    <div role="alert" class="alert alert-contrast alert-dismissible" id="dl_ErrorDialog">
                        <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
                        <div class="message" id="dl_message">
                        </div>
                    </div>
                    <!-- Setting
================================================== -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-table card-border-color card-border-color-primary">
                                <div class="row table-filters-container">
                                    <div class="col-12 col-lg-12 col-xl-5">
                                        <div class="row">
                                            <div class="col-12 col-lg-8 table-filters pb-0 pb-xl-4">
                                                <span class="table-filter-title">Merchant</span>
                                                <div class="filter-container">
                                                    <label class="control-label">Select a Merchant</label>
                                                    <asp:DropDownList ID="DD_merchant" runat="server" CssClass="select2"></asp:DropDownList>
                                                </div>
                                            </div>

                                            <%--ADD START DANIEL 20181120 [Added currency drop down]--%>    
                                            <div class="col-12 col-lg-4 table-filters pb-0 pb-xl-4">
                                                <span class="table-filter-title" style="height:18px"></span>
                                                <div class="filter-container">
                                                    <label class="control-label">Currency</label>
                                                    <asp:DropDownList ID="DD_Currency" runat="server" CssClass="select2"></asp:DropDownList>
                                                </div>
                                            </div>
                                            <%--ADD E N D DANIEL 20181120 [Added currency drop down]--%>
                                        </div>                                                                                 
                                    </div>
                                    <div class="col-12 col-lg-12 col-xl-5">
                                        <div class="row">
                                            <div class="col-12 col-lg-12 table-filters pb-0 pb-xl-6">
                                                <span class="table-filter-title">Date</span>
                                                <div class="filter-container">
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <label class="control-label">Since:</label>
                                                            <asp:TextBox ID="TB_Since" runat="server" data-date-format="yyyy-mm-dd" data-min-view="2" CssClass="form-control date datetimepicker"></asp:TextBox>
                                                        </div>
                                                        <div class="col-6">
                                                            <label class="control-label">To:</label>
                                                            <asp:TextBox ID="TB_To" runat="server" data-date-format="yyyy-mm-dd" data-min-view="2" CssClass="form-control date datetimepicker"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-12 col-xl-2">
                                        <div class="row">
                                            <div class="col-12 col-lg-6 table-filters pb-0 pb-xl-4">
                                                <span class="table-filter-title"></span>
                                                <div class="filter-container" style="margin-top: 45px">
                                                    <asp:Button ID="btn_GetReport" runat="server" Text="Get Report" CssClass="btn btn-primary btn-xl" OnClick="btn_GetReport_Click" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- Summary (Total End-User Payments, Total Net Avenue, Transaction Success Rate, Average Purchase)
================================================== -->
                    <div class="row">
                        <div class="col-12 col-lg-6 col-xl-6">
                            <div class="widget widget-tile">
                                <div id="spark1" class="chart sparkline"></div>
                                <div class="data-info">
                                    <%-- CHG START SUKI 20181130 [Change display title] --%>
                                    <%--<div class="desc">Total Payment(s)</div>--%>
                                    <div class="desc">Total Payment Value</div>
                                    <%-- CHG E N D SUKI 20181130 [Change display title] --%>
                                    <div class="value">
                                        <span class="indicator indicator-equal mdi mdi-chevron-right"></span><span data-toggle="counter-dec" data-end="<%=Total_EndUser_Payment %>" class="number">0</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <%--ADD START DANIEL 20181120 [Added new column Total Transaction]--%>
                           <div class="col-12 col-lg-6 col-xl-6">
                            <div class="widget widget-tile">
                                <div id="spark2" class="chart sparkline"></div>
                                <div class="data-info">
                                    <div class="desc">Total Transaction(s)</div>
                                    <div class="value">
                                        <span class="indicator indicator-equal mdi mdi-chevron-right"></span><span data-toggle="counter" data-end="<%=Total_Transaction %>" class="number">0</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%--ADD E N D DANIEL 20181120 [Added new column Total Transaction]--%>

                        <%--DEL START DANIEL 20181120 [Deleted all columns except Total Payments column]--%>
                       <%-- <div class="col-12 col-lg-6 col-xl-3">
                            <div class="widget widget-tile">
                                <div id="spark2" class="chart sparkline"></div>
                                <div class="data-info">
                                    <div class="desc">Total Net Fees</div>
                                    <div class="value">
                                        <span class="indicator indicator-positive mdi mdi-chevron-up"></span><span data-toggle="counter-dec" data-end="<%=Total_Net_Revenue %>" class="number">0</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-lg-6 col-xl-3">
                            <div class="widget widget-tile">
                                <div id="spark3" class="chart sparkline"></div>
                                <div class="data-info">
                                    <div class="desc">Transaction Success Rate</div>
                                    <div class="value">
                                        <span class="indicator indicator-positive mdi mdi-chevron-up"></span><span data-toggle="counter" data-end="<%=Conversion_Rate %>" data-suffix="%" class="number">0</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-lg-6 col-xl-3">
                            <div class="widget widget-tile">
                                <div id="spark4" class="chart sparkline"></div>
                                <div class="data-info">
                                    <div class="desc">Average Purchase</div>
                                    <div class="value">
                                        <span class="indicator indicator-positive mdi mdi-chevron-up"></span><span data-toggle="counter-dec" data-end="<%=Average_Purchase %>" class="number">0</span>
                                    </div>
                                </div>
                            </div>
                        </div>--%>
                        <%--DEL E N D DANIEL 20181120 [Deleted all columns except Total Payments column]--%>
                    </div>
                    <!-- Total Revenue
================================================== -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-divider">
                                    <div class="tools"><span class="icon mdi mdi-chevron-down"></span><span class="icon mdi mdi-refresh-sync"></span><span class="icon mdi mdi-close"></span></div>
                                    <span class="title font-weight-bold">Total Revenue</span>
                                </div>
                                <div class="card-body">
                                    <div id="line-chart" style="height: 250px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Day of Time
================================================== -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-divider">
                                    <div class="tools"><span class="icon mdi mdi-chevron-down"></span><span class="icon mdi mdi-refresh-sync"></span><span class="icon mdi mdi-close"></span></div>
                                    <span class="title font-weight-bold">Time of the day</span>
                                </div>
                                <div class="card-body">
                                    <div id="line-chart2" style="height: 250px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Channel Usage
================================================== -->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header card-header-divider">
                                    <div class="tools"><span class="icon mdi mdi-chevron-down"></span><span class="icon mdi mdi-refresh-sync"></span><span class="icon mdi mdi-close"></span></div>
                                    <%-- CHG START SUKI 20181122 [Modified display name] --%>
                                    <%--<span class="title font-weight-bold">Channel Usage</span>--%>
                                    <span class="title font-weight-bold">Channel Usage by Transaction Volume</span>
                                    <%-- CHG E N D SUKI 20181122 [Modified display name] --%>
                                </div>
                                <div class="card-body">
                                    <div id="donut-chart" style="height: 250px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center" id="footer">
                Copyright © 2019 Paydibs Sdn. Bhd. All Rights Reserved.
            </div>
        </div>
    </form>
    <script src="../../assets/lib/customize/General.js"></script>
    <script src="../../assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/bootstrap/dist/js/bootstrap.bundle.min.js" type="text/javascript"></script>
    <script src="../../assets/js/app.js" type="text/javascript"></script>
    <script src="../../assets/lib/jquery-flot/jquery.flot.js" type="text/javascript"></script>
    <script src="../../assets/lib/jquery-flot/jquery.flot.pie.js" type="text/javascript"></script>
    <script src="../../assets/lib/jquery-flot/jquery.flot.time.js" type="text/javascript"></script>
    <script src="../../assets/lib/jquery-flot/jquery.flot.resize.js" type="text/javascript"></script>
    <script src="../../assets/lib/jquery-flot/plugins/jquery.flot.orderBars.js" type="text/javascript"></script>
    <script src="../../assets/lib/jquery-flot/plugins/curvedLines.js" type="text/javascript"></script>
    <script src="../../assets/lib/jquery-flot/plugins/jquery.flot.tooltip.js" type="text/javascript"></script>
    <script src="../../assets/lib/jquery.sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/countup/countUp.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/jqvmap/jquery.vmap.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
    <script src="../../assets/js/app-dashboard.js" type="text/javascript"></script>
    <script src="../../assets/lib/raphael/raphael.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/morrisjs/morris.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/select2/js/select2.full.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/bootstrap-slider/bootstrap-slider.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="../../assets/js/app-table-filters.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            ////initialize the javascript
            App.init();
            //App.dashboard();
            App.tableFilters();
            $('#dl_ErrorDialog').hide();

            $('#TB_Since').change(function () {
                checkDate()
            });
            $('#TB_To').change(function () {
                checkDate()
            });
            function checkDate() {
                var startDate = new Date($("#TB_Since").val());
                var endDate = new Date($("#TB_To").val());
                if (endDate < startDate) {
                    $('#dl_ErrorDialog').addClass("alert-danger");
                    document.getElementById('dl_message').innerText = "Please select valid Date Range";
                    $('#dl_ErrorDialog').show();
                }
                else {
                    $('#dl_ErrorDialog').hide();
                }
            }

            //Basic?  
            $('[data-toggle="counter"]').each(function (i, e) {
                var _el = $(this);
                var prefix = '';
                var suffix = '';
                var start = 0;
                var end = 0;
                var decimals = 0;
                var duration = 2.5;

                if (_el.data('prefix')) { prefix = _el.data('prefix'); }

                if (_el.data('suffix')) { suffix = _el.data('suffix'); }

                if (_el.data('start')) { start = _el.data('start'); }

                if (_el.data('end')) { end = _el.data('end'); }

                if (_el.data('decimals')) { decimals = _el.data('decimals'); }

                if (_el.data('duration')) { duration = _el.data('duration'); }

                var count = new CountUp(_el.get(0), start, end, decimals, duration, {
                    suffix: suffix,
                    prefix: prefix,
                });

                count.start();
            });

            $('[data-toggle="counter-dec"]').each(function (i, e) {
                var _el = $(this);
                var prefix = '';
                var suffix = '';
                var start = 0;
                var end = 0;
                var decimals = 2;
                var duration = 2.5;

                if (_el.data('prefix')) { prefix = _el.data('prefix'); }

                if (_el.data('suffix')) { suffix = _el.data('suffix'); }

                if (_el.data('start')) { start = _el.data('start'); }

                if (_el.data('end')) { end = _el.data('end'); }

                if (_el.data('decimals')) { decimals = _el.data('decimals'); }

                if (_el.data('duration')) { duration = _el.data('duration'); }

                var count = new CountUp(_el.get(0), start, end, decimals, duration, {
                    suffix: suffix,
                    prefix: prefix,
                });

                count.start();
            });

            $('.toggle-loading').on('click', function () {
                var parent = $(this).parents('.widget, .panel');

                if (parent.length) {
                    parent.addClass('be-loading-active');

                    setTimeout(function () {
                        parent.removeClass('be-loading-active');
                    }, 3000);
                }
            });
            //Basic ^^^^^^^^^
            var color1 = "#dd5138";
            var color2 = "#fbce36";
            var color3 = "#3b4274";
            var color4 = "#44d9e6";

            //Total End-User Payments
            $('#spark1').sparkline(<%= Total_EndUser_Payment_Data %>, {
                width: '85',
                height: '35',
                lineColor: color1,
                highlightSpotColor: color4,
                highlightLineColor: color1,
                fillColor: false,
                spotColor: false,
                minSpotColor: false,
                maxSpotColor: false,
                lineWidth: 1.15
            });

           //Total Transactions
            $("#spark2").sparkline(<%= Total_Transaction_Data %>,{
                 width: '85',
                height: '35',
                lineColor: color1,
                highlightSpotColor: color4,
                highlightLineColor: color1,
                fillColor: false,
                spotColor: false,
                minSpotColor: false,
                maxSpotColor: false,
                lineWidth: 1.15
               });


     //DEL START DANIEL 20181120 [All but Total Payment is removed]
            
            //Total Net Revenue
         <%--   $("#spark2").sparkline(<%= Total_Net_Revenue_Data %>, {
                type: 'bar',
                width: '85',
                height: '35',
                barWidth: 3,
                barSpacing: 3,
                chartRangeMin: 0,
                barColor: color2
            });--%>

            //Conversion Rate
      <%--      $('#spark3').sparkline(<%= Conversion_Rate_Data %>, {
                type: 'bar',
                width: '85',
                height: '35',
                barWidth: 3,
                barSpacing: 3,
                chartRangeMin: 0,
                barColor: color2
            });--%>

            //Average Purchase
<%--            $("#spark4").sparkline(<%= Average_Purchase_Data %>, {
                width: '85',
                height: '35',
                lineColor: color1,
                highlightSpotColor: color4,
                highlightLineColor: color1,
                fillColor: false,
                spotColor: false,
                minSpotColor: false,
                maxSpotColor: false,
                lineWidth: 1.15
            });--%>
        
        //DEL E N D DANIEL 20181120 [All but Total Payment is removed]

            //Total Revenue
            new Morris.Line({
                element: 'line-chart',
                data: <%= Total_Revenue_Data %>,
                xkey: 'DateCreated',
                ykeys: ['TxnAmt', 'Qty'],
                labels: ['Transaction Amount', 'Transaction Count'],
                lineColors: [color1, color2, color3],
                hideHover: 'auto',
                parseTime: false,
                resize: true
            });

            //Day of Time
            new Morris.Line({
                element: 'line-chart2',
                data: <%= Time_Of_The_Day_Data %>,
                xkey: 'HourCreated',
                ykeys: ['TxnAmt', 'Qty'],
                labels: ['Transaction Amount', 'Transaction Count'],
                lineColors: [color1, color2, color3],
                hideHover: 'auto',
                parseTime: false,
                resize: true
            });

            //Channel Usage
            new Morris.Donut({
                element: 'donut-chart',
                data: [<%= Channel_Usage_Data %>],
                colors: [color1, color2, color3, color4],
                formatter: function (y) { return y + " %" },
                hideHover: 'auto',
                resize: true
            });
        });

        function widget_tooltipPosition(id, top) {
            $('#' + id).bind("plothover", function (event, pos, item) {
                var widthToolTip = $('.tooltip-chart').width();
                if (item) {
                    $(".tooltip-chart")
                        .css({ top: item.pageY - top, left: item.pageX - (widthToolTip / 2) })
                        .fadeIn(200);
                } else {
                    $(".tooltip-chart").hide();
                }
            });
        }

        //ADD START DANIEL 20190109 [Display message at page]
             $(function () {
            $('#dl_ErrorDialog').hide();
            var msg = $('#<%= HiddenField1.ClientID %>').val();
            var classType = $('#<%= HiddenField2.ClientID %>').val();
            var type = "alert-danger";
            if (msg != "") {
                if (classType != "") type = classType;
                $('#dl_ErrorDialog').addClass(type);
                document.getElementById('dl_message').innerText = msg;
                $('#dl_ErrorDialog').show();
                 }
        //ADD E N D DANIEL 20190109 [Display message at page]

        });
    </script>
</body>
</html>
