﻿using LoggerII;
using Newtonsoft.Json;
using PaydibsPortal.BLL;
using PaydibsPortal.DAL;
using System;
using System.Collections;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web.Configuration;
using System.Web.UI.WebControls;

public partial class Backoffice_MMS_EmailPayment : System.Web.UI.Page
{
    string errorMsg = string.Empty;
    HomeController hc = null;
    MMGMTController mc = new MMGMTController();
    MMSController mmsc = new MMSController();
    CurrentUserStat cus = null;
    string pymtCheckOut = WebConfigurationManager.AppSettings["PymtCheckOutURL"];

    protected void Page_Load(object sender, EventArgs e)
    {
        HiddenField1.Value = string.Empty;
        HiddenField2.Value = string.Empty;

        if (Session[SessionType.CurrentUser] == null)
        {
            Response.Redirect("../../Default.aspx");
            return;
        }

        cus = (CurrentUserStat)Session[SessionType.CurrentUser];
        hc = new HomeController(cus.DomainID);

        // ADD START DANIEL 20190318 [Check permission on every page load]
        var permission = hc.LoadUserPermission(cus.UserID, Convert.ToInt16(OpsIDEnum.EmailPayment));
        if (permission[0].IsUserAllowed == "0")
        {
            Response.Redirect("../../Backoffice/Dashboard.aspx");
            return;
        }
        // ADD E N D DANIEL 20190318 [Check permission on every page load]

        if (!IsPostBack)
        {
            var currencyList = mc.Do_GetCurrencyCountry(1);
            DD_Currency.DataSource = currencyList;
            DD_Currency.DataValueField = "CurrCode";
            DD_Currency.DataTextField = "CurrCode";
            DD_Currency.DataBind();
            DD_Currency.SelectedValue = "MYR";

            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EmailPayment), MethodBase.GetCurrentMethod().Name, "Page_Load Method");
        }
    }

    protected void btn_Create_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();
        var logObject = new
        {
            LoginUser = cus.UserID,
            CustomerName = txtCustName.Text,
            CustomerContactNumber = txtCustContactNumber.Text,
            CustomerEmailAddress = txtCustEmailAddress.Value,
            //Modified By Daniel 2 May 2019. Set default value to 0 --START
            //EmailReminder = RB_emailReminderList.Items[0].Selected == true ? 1 : 0,
            EmailReminder = 0, 
            //--END
            OrderDescription = txt_orderDesc.Text,
            OrderNumber = txt_orderNo.Text,
            Currency = DD_Currency.SelectedValue,
            Amount = txt_Amt.Text
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            string strToken = string.Empty;
            ArrayList paramList = new ArrayList();
            Label lbl_merchantUrl = new Label();
            Label lbl_pymtLink = new Label();

            //GET PDBS account if SuperAdmin is CurrentUserID
            var getMerchantInfo = mc.Do_GetMerchantInfo(Convert.ToInt32(cus.DomainID), true);
            lbl_merchantUrl.Text = getMerchantInfo.WebSiteURL;


            string emailHtmlPath = "emailPaymentTemplate.html";

            if (isHTMLTagsInTextBox(txt_orderDesc.Text.Trim()))
            {
                HiddenField1.Value = "A potentially dangerous value was detected! (" + txt_orderDesc.Text.Trim() + ")";
                return;
            }
            if (isHTMLTagsInTextBox(txt_orderNo.Text.Trim()))
            {
                HiddenField1.Value = "A potentially dangerous value was detected! (" + txt_orderNo.Text.Trim() + ")";
                return;
            }

            if (string.IsNullOrEmpty(txt_Amt.Text))
            {
                txt_Amt.Text = "0.00";
                HiddenField1.Value = "Amount cannot be 0.00";
                return;
            }

            if (!isContactNumberValid(txtCustContactNumber.Text))
            {
                HiddenField1.Value = "Wrong phone numbers format. (" + txtCustContactNumber.Text.Trim() + ")";
                txtCustContactNumber.Focus();
                return;
            }

            if (isFieldsValidated() == true)
            {
               
                    if (isEmailValid(txtCustEmailAddress.Value.Trim()) == false)
                    {
                        HiddenField1.Value = "Invalid Email Format! (" + txtCustEmailAddress.Value.Trim() + ")";
                        return;
                    }
                    paramList.Add(txtCustName.Text.Trim());//0
                    paramList.Add(txtCustContactNumber.Text.Trim());//1
                    paramList.Add(txtCustEmailAddress.Value.Trim());//2
                    paramList.Add(txt_orderDesc.Text.Trim());//3
                    paramList.Add(txt_orderNo.Text.Trim());//4
                    paramList.Add(DD_Currency.SelectedValue);//5
                    paramList.Add(txt_Amt.Text.Trim());//6
                    //Modified By Daniel 2 May 2019. Set email Reminder to 0 -- START
                    //paramList.Add(RB_emailReminderList.SelectedValue);//7
                    paramList.Add("0");//7
                    //--END
                    paramList.Add(getMerchantInfo.MerchantID);//8
                    paramList.Add(getMerchantInfo.WebSiteURL);//9
                    paramList.Add(getMerchantInfo.Desc);//10

                    mmsc.GenerateButtonScript(paramList, ref strToken, true);
                    if (!string.IsNullOrEmpty(mmsc.system_err))
                    {
                        HiddenField1.Value = mmsc.err_msg;
                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                        "EmailPayment(Error) - " + logJson + mmsc.system_err.ToString());
                        hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EmailPayment), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + mmsc.system_err.ToString());
                        return;
                    }
                    string custName = txtCustName.Text.Trim().Replace(" ", "+");
                    //CHG START DANIEL 20190218 [Remove unnecessary codes]
                    //string qrContent = pymtCheckOut + "?TokenType=BNB&CustName=&CustEmail=&CustPhone=&Token=" + strToken;
                    //lbl_pymtLink.Text = qrContent.Replace("CustName=", "CustName=" + custName);
                    //lbl_pymtLink.Text = lbl_pymtLink.Text.Replace("CustEmail=", "CustEmail=" + txtCustEmailAddress.Value.Trim());
                    //lbl_pymtLink.Text = lbl_pymtLink.Text.Replace("CustPhone=", "CustPhone=" + txtCustContactNumber.Text.Trim());
                    //After call GenerateButtonScript only add paramList[11] to get token return
                    string[] emailList = txtCustEmailAddress.Value.Split(',');
                    string qrContent = pymtCheckOut + "?TokenType=BNB&CustName={0}&CustEmail={1}&CustPhone={2}&Token=" + strToken;

                    lbl_pymtLink.Text = String.Format(qrContent, custName, emailList[0], txtCustContactNumber.Text.Trim());

                    //CHG E N D DANIEL 20190218 [Remove unnecessary codes]
                    paramList.Add(lbl_pymtLink.Text);//11
                    Session["emailParams"] = paramList;
                    Session["createdToken"] = strToken;
                    panel_emailTemplate.Visible = true;
                    using (var emailHTML = new StreamReader(Server.MapPath(emailHtmlPath)))
                    {
                        lbl_body.Text = emailHTML.ReadToEnd();
                    }

                    lbl_body.Text = lbl_body.Text.Replace("{LogoPath}", System.Configuration.ConfigurationManager.AppSettings["LogoPath"]);
                    lbl_body.Text = lbl_body.Text.Replace("{custName}", (string)paramList[0]);
                    lbl_body.Text = lbl_body.Text.Replace("{orderNo}", (string)paramList[4]);
                    lbl_body.Text = lbl_body.Text.Replace("{orderDesc}", (string)paramList[3]);
                    lbl_body.Text = lbl_body.Text.Replace("{currency}", (string)paramList[5]);
                    lbl_body.Text = lbl_body.Text.Replace("{amt}", Convert.ToDecimal((string)paramList[6]).ToString("F"));
                    lbl_body.Text = lbl_body.Text.Replace("{paymentLink}", lbl_pymtLink.Text);
                    lbl_body.Text = lbl_body.Text.Replace("{merchantName}", getMerchantInfo.Desc);
                    lbl_body.Text = lbl_body.Text.Replace("{merchantUrl}", lbl_merchantUrl.Text);
                    lbl_body.Text = lbl_body.Text.Replace("{merchantID}", getMerchantInfo.MerchantID);


                    string defEmailSubject = "Payment for " + getMerchantInfo.Desc + " :  Order ID: " + (string)paramList[4];
                    txt_Subject.Text = defEmailSubject;
                    txt_Subject.Focus();

                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            "EmailPayment - " + logJson);
                    hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.EmailPayment, 800000, "EmailPayment.aspx");
                    hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EmailPayment), MethodBase.GetCurrentMethod().Name, logJson);
            }
            else
                HiddenField1.Value = errorMsg;
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                     "EmailPayment(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.EmailPayment, 800001, "EmailPayment.aspx");
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EmailPayment), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }

    private bool isFieldsValidated()
    {
        bool bFlag = false;
       
            if (txtCustName.Text != string.Empty && txtCustContactNumber.Text != string.Empty && txtCustEmailAddress.Value != string.Empty && txt_orderDesc.Text != string.Empty && txt_orderNo.Text != string.Empty)
            {
                if (double.Parse(txt_Amt.Text) > 0)
                    bFlag = true;
                else
                {
                    errorMsg = "Amount cannot be 0.00";
                    bFlag = false;
                }
            }
            else
            {
                errorMsg = "Please fill in the fields given.";
                bFlag = false;
            }
        
        return bFlag;
    }

    private bool isHTMLTagsInTextBox(string s)
    {
        bool bResult = false;

        Regex regex1 = new Regex(@"<\s*([^ >]+)[^>]*>.*?<\s*/\s*\1\s*>");
        Regex regex2 = new Regex(@"<[^>]+>");

        if (regex1.IsMatch(s) || regex2.IsMatch(s))
        {
            bResult = true;
        }
        return bResult;
    }

    private bool isEmailValid(string s)
    {
        bool bResult = false;
        string[] emailList = s.Split(',');
        string pattern = "^([0-9a-zA-Z]([-\\.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$";

        for (int i = 0; i < emailList.Length; i++)
        {
            if (Regex.IsMatch(emailList[i], pattern))
            {
                bResult = true;
            }
            else
            {
                bResult = false;
                break;
            }

        }
        return bResult;
    }

    private bool isContactNumberValid(string s)
    {
        bool bResult = false;

        Regex regex = new Regex(@"^[0-9]*$");
        if (s.Length > 9)
        {
            if (regex.IsMatch(s))
            {
                bResult = true;
            }
        }
        return bResult;
    }

    protected void btn_SendEmail_click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();
        Label lbl_pymtLink = new Label();

        string sentStatusReturn = string.Empty;
        ArrayList emailParams = (ArrayList)Session["emailParams"];


        var logObject = new
        {
            LoginUser = cus.UserID,
            CustomerName = emailParams[0].ToString(),        //customer name
            CustomerContactNumber = emailParams[1].ToString(),        //customer contact number
            CustomerEmailAddress = emailParams[2].ToString(),        //customer email address
            OrderDescription = emailParams[3].ToString(),        //order description
            OrderNumber = emailParams[4].ToString(),        //order number
            Currency = emailParams[5].ToString(),        // currency code
            Amount = emailParams[6].ToString(),        //amount
            EmailReminder = emailParams[7].ToString(),        //email reminder yes no
            MerchantID = emailParams[8].ToString(),        // merchant id
            WebSiteUrl = emailParams[9].ToString(),        // website url
            MerchantName = emailParams[10].ToString(),       //merchant name
        };
        var logJson = JsonConvert.SerializeObject(logObject);


        try
        {
            objLoggerII = logger.InitLogger();

            //CHG START DANIEL 20190218 [Assign multiple emails to string array];
            //var sendEmail = mmsc.Do_Email(emailParams, , txt_Subject.Text, ref sentStatusReturn);

            string[] emails = emailParams[2].ToString().Split(',');
            string qrContent = pymtCheckOut + "?TokenType=BNB&CustName={0}&CustEmail={1}&CustPhone={2}&Token=" + Session["createdToken"];
            bool sendEmail = false;

            for (int i = 0; i < emails.Length; i++)
            {
                lbl_pymtLink.Text = String.Format(qrContent, logObject.CustomerName.Replace(" ", "+"), emails[i], txtCustContactNumber.Text.Trim());
                sendEmail = mmsc.Do_Email(emailParams, lbl_pymtLink.Text, emails[i], txt_Subject.Text, ref sentStatusReturn);
            }

            //CHG E N D DANIEL 20190218 [Assign multiple emails to string array];

            if (sendEmail)
            {
                if (string.IsNullOrEmpty(sentStatusReturn))
                {
                    //store into token_paymentlink table
                    //CHG START DANIEL 20190218 [Foreach to insert multiple emails]
                    //bool insertPymyLink = mmsc.Do_InsertPaymentLink(emailParams, Session["createdToken"].ToString(), 1);
                    bool insertPymyLink = false;

                    foreach (var email in emails)
                    {
                        insertPymyLink = mmsc.Do_InsertPaymentLink(emailParams, email, Session["createdToken"].ToString(), 1);
                    }
                    //CHG E N D DANIEL 20190218 [Foreach to insert multiple emails]
                    if (!insertPymyLink)
                    {
                        HiddenField1.Value = mmsc.err_msg;
                        objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            "EmailPayment(Error) - " + logJson + mmsc.system_err.ToString());
                        hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EmailPayment), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + mmsc.system_err.ToString());
                        return;
                    }

                    //TODO DANIEL: send acknowledge to merchant
                    //***//

                    HiddenField1.Value = "Email(s) sent successfully.";
                    HiddenField2.Value = AlertType.Success;
                }
                else
                {
                    HiddenField1.Value = sentStatusReturn;
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                        "EmailPayment(Error) - " + logJson + sentStatusReturn);
                    hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EmailPayment), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + sentStatusReturn);
                    return;
                }
            }
            else
            {
                HiddenField1.Value = mmsc.err_msg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                        "EmailPayment(Error) - " + logJson + mmsc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EmailPayment), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + mmsc.system_err.ToString());
                return;
            }

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                          "EmailPayment - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.EmailPayment, 800000, "EmailPayment.aspx");
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EmailPayment), MethodBase.GetCurrentMethod().Name, logJson);
        }
        catch (Exception ex)
        {
            HiddenField1.Value = ex.Message;
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                     "EmailPayment(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.EmailPayment, 800001, "EmailPayment.aspx");
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EmailPayment), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }

    protected void btn_Reset_Click(object sender, EventArgs e)
    {
        txtCustName.Text = string.Empty;
        txtCustContactNumber.Text = string.Empty;
        txtCustEmailAddress.Value = string.Empty;
        //RB_emailReminderList.SelectedIndex = 1;
        txt_orderDesc.Text = string.Empty;
        txt_orderNo.Text = string.Empty;
        DD_Currency.SelectedValue = "MYR";
        txt_Amt.Text = string.Empty;
    }
}
