﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EmailPayment.aspx.cs" Inherits="Backoffice_MMS_EmailPayment" ValidateRequest="false" %>

<!DOCTYPE html>
<%@ Register TagPrefix="uc" TagName="Left_Side_Bar" Src="~/UserControl/LeftSideBar.ascx" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Paydibs-Checkout Email Payment</title>
    <link rel="stylesheet" type="text/css" href="../../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/material-design-icons/css/material-design-iconic-font.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/select2/css/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/bootstrap-slider/css/bootstrap-slider.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/datatables/datatables.net-bs4/css/dataTables.bootstrap4.css" />
    <link rel="stylesheet" href="../../assets/css/app.css" type="text/css" />

    <script src="../../assets/lib/qrcodejs/qrcode.js"></script>
    <script src="../../assets/lib/qrcodejs/qrcode.min.js"></script>
    <script src="../../assets/lib/qrcodejs/jquery.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:HiddenField ID="HiddenField2" runat="server" />
        <asp:HiddenField ID="HiddenField1" runat="server" />
        <div class="be-wrapper be-collapsible-sidebar be-collapsible-sidebar-collapsed">
            <uc:Left_Side_Bar ID="Left_Side_Bar" runat="server" />
            <div class="be-content">
                <div class="main-content container-fluid">
                    <div role="alert" class="alert alert-contrast alert-dismissible" id="dl_ErrorDialog">
                        <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
                        <div class="message" id="dl_message">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-border-color card-border-color-primary">
                                <div class="card-header card-header-divider font-weight-bold" id="headerName" runat="server">
                                    Email Payment
                                </div>
                                <div class="card-body">
                                    <div class="filter-container">
                                        <asp:Panel ID="panel_emailDetails" runat="server">
                                            <div class="row">
                                                <div class="col-4">
                                                    <label class="control-label font-weight-bold">Customer Name</label>
                                                </div>
                                                <div class="col-6">
                                                    <asp:TextBox ID="txtCustName" placeholder="Customer Name" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-4">
                                                    <label class="control-label font-weight-bold">Customer Contact Number</label>
                                                </div>
                                                <div class="col-6">
                                                    <asp:TextBox ID="txtCustContactNumber" placeholder="Customer Contact Number" runat="server" CssClass="form-control" MaxLength="13" onkeypress="return isNumberKey(event)"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-4">
                                                    <label class="control-label font-weight-bold">Customer Email Address</label>
                                                </div>
                                                <div class="col-6">
                                                    <input type="text" data-role="tagsinput" id="txtCustEmailAddress" runat="server" class="form-control" />

                                                </div>
                                            </div>

                                                <%--Commented By Daniel 2 May 2019. Hide temporarily--%>
                                          <%--  <div class="row" style="margin-top: 20px; margin-bottom: 10px;">
                                                <div class="col-4">
                                                    <label class="control-label font-weight-bold">Email Reminder</label>
                                                </div>

                                              <div class="col-6">
                                                    <asp:RadioButtonList ID="RB_emailReminderList" runat="server">
                                                        <asp:ListItem Value="1">Yes, after three days</asp:ListItem>
                                                        <asp:ListItem Value="0" Selected="True">No</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </div>
                                            </div>--%>
                                        </asp:Panel>


                                        <div class="row">
                                            <div class="col-4">
                                                <label class="control-label font-weight-bold">Order Description</label>
                                            </div>
                                            <div class="col-6">
                                                <asp:TextBox ID="txt_orderDesc" placeholder="Order Description" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-4">
                                                <label class="control-label font-weight-bold">Order Number</label>
                                            </div>
                                            <div class="col-6">
                                                <asp:TextBox ID="txt_orderNo" placeholder="e.g: Invoice Number/ Reference Number" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-4">
                                                <label class="control-label font-weight-bold">Currency</label>
                                            </div>
                                            <div class="col-6">
                                                <asp:DropDownList ID="DD_Currency" runat="server" CssClass="select2"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-4">
                                                <label class="control-label font-weight-bold">Amount</label>
                                            </div>
                                            <div class="col-6">
                                                <asp:TextBox ID="txt_Amt" placeholder="Amount" runat="server" CssClass="form-control" Onkeypress="return validateFloatKeyPress(this,event)"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="row" style="margin-top: 10px;">
                                            <div class="col-7"></div>
                                            <div class="col-0 ">
                                                <asp:Button ID="btn_Create" runat="server" Text="Generate Email Template" CssClass="btn btn-primary btn-xl right" OnClick="btn_Create_Click" />
                                                <asp:Button ID="btn_Reset" runat="server" Text="Reset" CssClass="btn btn-primary btn-xl right" OnClick="btn_Reset_Click" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <asp:Panel ID="panel_emailTemplate" runat="server" Visible="false">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card card-border-color card-border-color-primary">
                                    <div class="card-header card-header-divider font-weight-bold">
                                        Email Preview
                                    </div>
                                    <div class="card-body">
                                        <div class="filter-container">
                                            <div class="col-12 col-lg-20 table-filters pb-0 pb-xl-4">
                                                <div class="row">
                                                    <p style="font-size: 20px">Subject :</p>
                                                    <asp:TextBox Style="padding-right: 10px; margin-bottom: 20px; align-content: center;" ID="txt_Subject" runat="server" CssClass="form-control"></asp:TextBox>

                                                </div>
                                                <div class="row">
                                                    <asp:Label ID="lbl_body" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-top: 10px;">
                                                <div class="col-9"></div>
                                                <div class="col-0 ">
                                                    <asp:Button ID="btn_SendEmail" Text="Send" runat="server" CssClass="btn btn-primary btn-xl right" OnClick="btn_SendEmail_click" />
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </div>
        </div>
        <div class="text-center" id="footer">
            Copyright © 2019 Paydibs Sdn. Bhd. All Rights Reserved.
        </div>
    </form>
    <script src="../../assets/lib/customize/General.js"></script>
    <script src="../../assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/bootstrap/dist/js/bootstrap.bundle.min.js" type="text/javascript"></script>
    <script src="../../assets/js/app.js" type="text/javascript"></script>
    <script src="../../assets/lib/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/select2/js/select2.full.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/bootstrap-slider/bootstrap-slider.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="../../assets/js/app-table-filters.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net/js/jquery.dataTables.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-bs4/js/dataTables.bootstrap4.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/dataTables.buttons.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/buttons.html5.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/buttons.flash.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/buttons.print.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/buttons.colVis.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js" type="text/javascript"></script>
    <script src="../../assets/js/app-tables-datatables.js" type="text/javascript"></script>
    <link href="../../assets/lib/Bootstrap-4-Tag-Input-Plugin-jQuery/tagsinput.css" rel="stylesheet" />
    <script src="../../assets/lib/Bootstrap-4-Tag-Input-Plugin-jQuery/tagsinput.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            App.init();
            App.tableFilters();
            App.dataTables();

            $('.bootstrap-tagsinput').css("line-height", '30px');
            $('.bootstrap-tagsinput').css("border-radius", '0px');
        });

        $(function () {
            $('#dl_ErrorDialog').hide();
            var msg = $('#<%= HiddenField1.ClientID %>').val();
            var classType = $('#<%= HiddenField2.ClientID %>').val();
            var type = "alert-danger";
            if (msg != "") {
                if (classType != "") type = classType;
                $('#dl_ErrorDialog').addClass(type);
                document.getElementById('dl_message').innerText = msg;
                $('#dl_ErrorDialog').show();
            }
        });

        function validateFloatKeyPress(el, evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            var number = el.value.split('.');
            if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }

            if (number.length > 1 && charCode == 46) {
                return false;
            }

            var caratPos = getSelectionStart(el);
            var dotPos = el.value.indexOf(".");
            if (caratPos > dotPos && dotPos > -1 && (number[1].length > 1)) {
                return false;
            }
            return true;
        }

        function getSelectionStart(o) {
            if (o.createTextRange) {
                var r = document.selection.createRange().duplicate()
                r.moveEnd('character', o.value.length)
                if (r.text == '') return o.value.length
                return o.value.lastIndexOf(r.text)
            } else return o.selectionStart
        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

    //$('#txtCustName').on('keydown', function() {

    //        var charCode = (evt.which) ? evt.which : event.keyCode;
    //        var email = document.getElementById('txtCustName').value
    //        var pattern = "/([A-Z0-9a-z_-][^@])+?@[^$#<>?]+?\.[\w]{2,4}/";
    //        if (charCode == 13)
    //        {
    //             e.preventDefault();
    //            if (!pattern.test(email).toLowerCase()) {
    //                $('#dl_ErrorDialog').addClass("alert-danger");
    //                document.getElementById('dl_message').innerText = "Wrong email";
    //                $('#dl_ErrorDialog').show();
    //            }
    //        }
    //    })

    </script>
</body>
</html>
