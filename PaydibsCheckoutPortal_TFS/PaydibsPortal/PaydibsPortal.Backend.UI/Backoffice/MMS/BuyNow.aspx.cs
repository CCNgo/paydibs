﻿using LoggerII;
using Newtonsoft.Json;
using PaydibsPortal.BLL;
using PaydibsPortal.DAL;
using System;
using System.Collections;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web.Configuration;
using System.Web.UI.WebControls;

public partial class Backoffice_MMS_BuyNow : System.Web.UI.Page
{
    string errorMsg = string.Empty;
    HomeController hc = null;
    MMGMTController mc = new MMGMTController();
    MMSController mmsc = new MMSController();
    CurrentUserStat cus = null;
    string pymtCheckOut = WebConfigurationManager.AppSettings["PymtCheckOutURL"];

    protected void Page_Load(object sender, EventArgs e)
    {
        HiddenField1.Value = string.Empty;
        HiddenField2.Value = string.Empty;

        if (Session[SessionType.CurrentUser] == null)
        {
            Response.Redirect("../../Default.aspx");
            return;
        }

        cus = (CurrentUserStat)Session[SessionType.CurrentUser];
        hc = new HomeController(cus.DomainID);

        // ADD START DANIEL 20190318 [Check permission on every page load]
        var permission = hc.LoadUserPermission(cus.UserID, Convert.ToInt16(OpsIDEnum.BuyNow));
        if (permission[0].IsUserAllowed == "0")
        {
            Response.Redirect("../../Backoffice/Dashboard.aspx");
            return;
        }
        // ADD E N D DANIEL 20190318 [Check permission on every page load]

        if (!IsPostBack)
        {
            var currencyList = mc.Do_GetCurrencyCountry(1);
            DD_Currency.DataSource = currencyList;
            DD_Currency.DataValueField = "CurrCode";
            DD_Currency.DataTextField = "CurrCode";
            DD_Currency.DataBind();
            DD_Currency.SelectedValue = "MYR";

            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.BuyNow), MethodBase.GetCurrentMethod().Name, "Page_Load Method");
        }
    }

    protected void btn_Create_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        var logObject = new
        {
            LoginUser = cus.UserID,
            OrderDescription = txt_orderDesc.Text,
            OrderNumber = txt_orderNo.Text,
            Currency = DD_Currency.SelectedValue,
            Amount = txt_Amt.Text
        };

        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            string strToken = string.Empty;
            ArrayList paramList = new ArrayList();
            Label lbl_merchantUrl = new Label();
            Label lbl_pymtLink = new Label();

            //Get PDBS account if SuperAdmin as CurrentUserID
            //CHG START DANIEL 20190315 [GET PDBS account if SuperAdmin is CurrentUserID]
            //var getMerchantInfo = mc.Do_GetMerchantInfo(Convert.ToInt32(cus.DomainID == 1 ? 3 : cus.DomainID));
            var getMerchantInfo = mc.Do_GetMerchantInfo(Convert.ToInt32(cus.DomainID), true);
            //CHG E N D DANIEL 20190315 [GET PDBS account if SuperAdmin is CurrentUserID]
            lbl_merchantUrl.Text = getMerchantInfo.WebSiteURL;

            if (isHTMLTagsInTextBox(txt_orderDesc.Text.Trim()))
            {
                HiddenField1.Value = "A potentially dangerous value was detected! (" + txt_orderDesc.Text.Trim() + ")";
                return;
            }
            if (isHTMLTagsInTextBox(txt_orderNo.Text.Trim()))
            {
                HiddenField1.Value = "A potentially dangerous value was detected! (" + txt_orderNo.Text.Trim() + ")";
                return;
            }

            if (string.IsNullOrEmpty(txt_Amt.Text))
            {
                txt_Amt.Text = "0.00";
                HiddenField1.Value = "Amount cannot be 0.00";
                return;
            }

            if (isFieldsValidated() == true)
            {
                paramList.Add(txt_orderDesc.Text.Trim());//0
                paramList.Add(txt_orderNo.Text.Trim());//1
                paramList.Add(DD_Currency.SelectedValue);//2
                paramList.Add(txt_Amt.Text.Trim());//3
                paramList.Add(getMerchantInfo.MerchantID);//4
                txt_BtnScript.Text = mmsc.GenerateButtonScript(paramList, ref strToken);
                if (!string.IsNullOrEmpty(mmsc.system_err))
                {
                    HiddenField1.Value = mmsc.err_msg;
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                        "BuyNow(Error) - " + logJson + mmsc.system_err.ToString());
                    hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.BuyNow), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + mmsc.system_err.ToString());
                    return;
                }

                string qrContent = pymtCheckOut + "?TokenType=BNB&Token=" + strToken;
                txtQrCode.Text = qrContent;
                panel_qrCode.Visible = true;
                HiddenField1.Value = "Buy Now Button created successfully.";
                HiddenField2.Value = AlertType.Success;

                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                        "BuyNow - " + logJson);
                hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.BuyNow, 800000, "BuyNow.aspx");
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.BuyNow), MethodBase.GetCurrentMethod().Name, logJson);
            }
            else
                HiddenField1.Value = errorMsg;
        }
        catch (Exception ex)
        {
            HiddenField1.Value = ex.Message;
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                     "BuyNow(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.BuyNow, 800001, "BuyNow.aspx");
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.BuyNow), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }

    private bool isFieldsValidated()
    {
        bool bFlag = false;

        if (txt_orderDesc.Text != string.Empty && txt_orderNo.Text != string.Empty)
        {
            if (double.Parse(txt_Amt.Text) > 0)
                bFlag = true;
            else
            {
                errorMsg = "Amount cannot be 0.00";
                bFlag = false;
            }
        }
        else
        {
            errorMsg = "Please fill in the fields given.";
            bFlag = false;
        }

        return bFlag;
    }

    private bool isHTMLTagsInTextBox(string s)
    {
        bool bResult = false;

        Regex regex1 = new Regex(@"<\s*([^ >]+)[^>]*>.*?<\s*/\s*\1\s*>");
        Regex regex2 = new Regex(@"<[^>]+>");

        if (regex1.IsMatch(s) || regex2.IsMatch(s))
        {
            bResult = true;
        }
        return bResult;
    }

    private bool isContactNumberValid(string s)
    {
        bool bResult = false;

        Regex regex = new Regex(@"^[0-9]*$");
        if (s.Length > 9)
        {
            if (regex.IsMatch(s))
            {
                bResult = true;
            }
        }
        return bResult;
    }

    protected void btn_Reset_Click(object sender, EventArgs e)
    {
        txt_orderDesc.Text = string.Empty;
        txt_orderNo.Text = string.Empty;
        DD_Currency.SelectedValue = "MYR";
        txt_Amt.Text = string.Empty;
    }
}
