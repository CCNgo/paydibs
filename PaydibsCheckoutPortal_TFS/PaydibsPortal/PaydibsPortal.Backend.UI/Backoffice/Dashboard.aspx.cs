﻿using LoggerII;
using Newtonsoft.Json;
using PaydibsPortal.BLL;
using PaydibsPortal.DAL;
using System;
using System.Reflection;

public partial class Backoffice_Default : System.Web.UI.Page
{
    HomeController hc = null;
    MMGMTController mc = new MMGMTController();
    CurrentUserStat cus = null;

    public decimal Total_EndUser_Payment = 0;
    public int Total_Transaction = 0;
    public decimal Total_Net_Revenue = 0;
    public decimal Conversion_Rate = 0;
    public decimal Average_Purchase = 0;

    public string Total_EndUser_Payment_Data = "[]";
    public string Total_Transaction_Data = "[]";
    public string Total_Net_Revenue_Data = "[]";
    public string Conversion_Rate_Data = "[]";
    public string Average_Purchase_Data = "[]";
    public string Total_Revenue_Data = "0";
    public string Time_Of_The_Day_Data = "[{\"HourCreated\":\"0000\",\"DateCreated\":null,\"Qty\":0,\"TxnAmt\":0.0,\"NetFee\":0.0},{\"HourCreated\":\"0100\",\"DateCreated\":null,\"Qty\":0,\"TxnAmt\":0.0,\"NetFee\":0.0},{\"HourCreated\":\"0200\",\"DateCreated\":null,\"Qty\":0,\"TxnAmt\":0.0,\"NetFee\":0.0},{\"HourCreated\":\"0300\",\"DateCreated\":null,\"Qty\":0,\"TxnAmt\":0.0,\"NetFee\":0.0},{\"HourCreated\":\"0400\",\"DateCreated\":null,\"Qty\":0,\"TxnAmt\":0.0,\"NetFee\":0.0},{\"HourCreated\":\"0500\",\"DateCreated\":null,\"Qty\":0,\"TxnAmt\":0.0,\"NetFee\":0.0},{\"HourCreated\":\"0600\",\"DateCreated\":null,\"Qty\":0,\"TxnAmt\":0.0,\"NetFee\":0.0},{\"HourCreated\":\"0700\",\"DateCreated\":null,\"Qty\":0,\"TxnAmt\":0.0,\"NetFee\":0.0},{\"HourCreated\":\"0800\",\"DateCreated\":null,\"Qty\":0,\"TxnAmt\":0.0,\"NetFee\":0.0},{\"HourCreated\":\"0900\",\"DateCreated\":null,\"Qty\":0,\"TxnAmt\":0.0,\"NetFee\":0.0},{\"HourCreated\":\"1000\",\"DateCreated\":null,\"Qty\":0,\"TxnAmt\":0.0,\"NetFee\":0.0},{\"HourCreated\":\"1100\",\"DateCreated\":null,\"Qty\":0,\"TxnAmt\":0.0,\"NetFee\":0.0},{\"HourCreated\":\"1200\",\"DateCreated\":null,\"Qty\":0,\"TxnAmt\":0.0,\"NetFee\":0.0},{\"HourCreated\":\"1300\",\"DateCreated\":null,\"Qty\":0,\"TxnAmt\":0.0,\"NetFee\":0.0},{\"HourCreated\":\"1400\",\"DateCreated\":null,\"Qty\":0,\"TxnAmt\":0.0,\"NetFee\":0.0},{\"HourCreated\":\"1500\",\"DateCreated\":null,\"Qty\":0,\"TxnAmt\":0.0,\"NetFee\":0.0},{\"HourCreated\":\"1600\",\"DateCreated\":null,\"Qty\":0,\"TxnAmt\":0.0,\"NetFee\":0.0},{\"HourCreated\":\"1700\",\"DateCreated\":null,\"Qty\":0,\"TxnAmt\":0.0,\"NetFee\":0.0},{\"HourCreated\":\"1800\",\"DateCreated\":null,\"Qty\":0,\"TxnAmt\":0.0,\"NetFee\":0.0},{\"HourCreated\":\"1900\",\"DateCreated\":null,\"Qty\":0,\"TxnAmt\":0.0,\"NetFee\":0.0},{\"HourCreated\":\"2000\",\"DateCreated\":null,\"Qty\":0,\"TxnAmt\":0.0,\"NetFee\":0.0},{\"HourCreated\":\"2100\",\"DateCreated\":null,\"Qty\":0,\"TxnAmt\":0.0,\"NetFee\":0.0},{\"HourCreated\":\"2200\",\"DateCreated\":null,\"Qty\":0,\"TxnAmt\":0.0,\"NetFee\":0.0}]";
    public string Channel_Usage_Data = "";

    DateTime sSince = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd") + " 00:00:00");
    DateTime sTo = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd") + " 23:59:59");

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session[SessionType.CurrentUser] == null)
        {
            Response.Redirect("../");
            return;
        }

        cus = (CurrentUserStat)Session[SessionType.CurrentUser];
        hc = new HomeController(cus.DomainID);

        if (!IsPostBack)
        {
            var merchant_L = hc.Do_GetMerchantList(1, 0);
            DD_merchant.DataSource = merchant_L;
            DD_merchant.DataTextField = "DomainDesc";
            DD_merchant.DataValueField = "DomainID";
            DD_merchant.DataBind();

            //ADD START DANIEL 20181120 [Bind data into DD_Currency. Default currency is MYR]
            var currencyList = mc.Do_GetCurrencyCountry(1);
            DD_Currency.DataSource = currencyList;
            DD_Currency.DataTextField = "CurrCode";
            DD_Currency.DataValueField = "CurrCode";
            DD_Currency.DataBind();
            DD_Currency.SelectedValue = "MYR";
            //ADD E N D DANIEL 20181120 [Bind data into DD_Currency. Default currency is MYR]

            TB_Since.Text = sSince.ToString("yyyy-MM-dd");
            TB_To.Text = sTo.ToString("yyyy-MM-dd");

            GenerateDashBoard(sSince, sTo, cus.DomainID, DD_Currency.SelectedValue);

            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.Dashboard), MethodBase.GetCurrentMethod().Name, "Page_Load Method");
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
    }

    protected void btn_GetReport_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        if (TB_Since.Text.Trim() == "")
            TB_Since.Text = sSince.ToString("yyyy-MM-dd");

        if (TB_To.Text.Trim() == "")
            TB_To.Text = sTo.ToString("yyyy-MM-dd");

        int sDomainID = Convert.ToInt32(DD_merchant.SelectedValue);
        sSince = Convert.ToDateTime(TB_Since.Text.Trim() + " 00:00:00");
        sTo = Convert.ToDateTime(TB_To.Text.Trim() + " 23:59:59");

        var logObject = new
        {
            LoginUser = cus.UserID,
            Since = TB_Since.Text,
            To = TB_To.Text
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            //CHG START DANIEL 20190109 [Insert into AuditTrail]
            //GenerateDashBoard(sSince, sTo, sDomainID, DD_Currency.SelectedValue);
            string errMsg = GenerateDashBoard(sSince, sTo, sDomainID, DD_Currency.SelectedValue);
            if (!string.IsNullOrEmpty(errMsg))
            {
                HiddenField1.Value = errMsg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "Dashboard(Error) - " + logJson + hc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.Dashboard), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + hc.system_err.ToString());
                return;
            }
            //CHG E N D DANIEL 20190109 [Insert into AuditTrail]

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "Dashboard - " + logJson);
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.Dashboard), MethodBase.GetCurrentMethod().Name, logJson);
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "Dashboard(Error) - " + logJson + ex.ToString());
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.Dashboard), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
    }

    //CHG START DANIEL 20190109 [Change method type to String]
    //void GenerateDashBoard(DateTime PSince, DateTime PTo, int PDomainID, string PCurrency)
    string GenerateDashBoard(DateTime PSince, DateTime PTo, int PDomainID, string PCurrency)
    //CHG E N D DANIEL 20190109 [Change method type to String]
    {
        var Result = hc.Do_DashBoardReport(PSince, PTo, PDomainID, PCurrency);
        if (!Result)
        {
            //CHG START DANIEL 20190109 [return HomeController error message]
            //return;
            return hc.err_msg;
            //CHG E N D DANIEL 20190109 [return HomeController error message]
        }
        else
        {
            Total_EndUser_Payment = hc.Total_EndUser_Payment;

            //ADD START DANIEL 20181120 [To refer to HomeController]
            Total_Transaction = hc.Total_Transaction;
            Total_Transaction_Data = "[" + string.Join(",", hc.Total_Transaction_Data) + "]";
            //ADD E N D DANIEL 20181120 [To refer to HomeController]

            Total_EndUser_Payment_Data = "[" + string.Join(",", hc.Total_EndUser_Payment_Data) + "]";

            //DEL START DANIEL 20181120 [Removed all columns except 'Total Payment' column]   

            //Total_Net_Revenue = hc.Total_Net_Revenue;
            //Conversion_Rate = hc.Conversion_Rate;
            //Average_Purchase = hc.Average_Purchase;

            //Total_Net_Revenue_Data = "[" + string.Join(",", hc.Total_Net_Revenue_Data) + "]";
            //Conversion_Rate_Data = "[" + string.Join(",", hc.Conversion_Rate_Data) + "]";
            //Average_Purchase_Data = "[" + string.Join(",", hc.Average_Purchase_Data) + "]";

            //DEL E N D DANIEL 20181120 [Removed all columns except 'Total Payment' column]   

            Total_Revenue_Data = hc.Total_Revenue_Data;
            Time_Of_The_Day_Data = hc.Time_Of_The_Day_Data;
            Channel_Usage_Data = hc.Channel_Usage_Data;
        }
        //ADD START DANIEL 20190109 [Add return type string]
        return string.Empty;
        //ADD E N D DANIEL 20190109 [Add return type string]
    }
}