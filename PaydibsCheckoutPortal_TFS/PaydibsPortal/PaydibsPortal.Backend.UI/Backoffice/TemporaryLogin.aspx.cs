﻿using PaydibsPortal.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Backoffice_TemporaryLogin : System.Web.UI.Page
{
    HomeController hc = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        var reqUserID = Request.QueryString["userID"];
        var reqController = Request.QueryString["controller"];
        var reqAction = Request.QueryString["action"];

        hc = new HomeController(1);
        reqUserID = hc.tempLoginDecrypt(reqUserID);
        var cmsUser_List = hc.getUserDomainID(reqUserID);

        Session[SessionType.CurrentUser] = new CurrentUserStat(reqUserID, Convert.ToInt32(cmsUser_List.DomainID));
        Response.Redirect("../Backoffice/"+ reqController + "/"+reqAction+".aspx");
    }
}