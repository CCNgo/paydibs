﻿using LoggerII;
using Newtonsoft.Json;
using PaydibsPortal.BLL;
using PaydibsPortal.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.UI.WebControls;

public partial class Backoffice_MMGMT_EditChannel : System.Web.UI.Page
{
    MMGMTController mc = new MMGMTController();
    HomeController hc = null;
    CurrentUserStat cus = null;
    string error_msg = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        HiddenField1.Value = "";
        HiddenField2.Value = "";

        if (Session[SessionType.CurrentUser] == null)
        {
            Response.Redirect("../../Default.aspx");
            return;
        }

        cus = (CurrentUserStat)Session[SessionType.CurrentUser];
        hc = new HomeController(cus.DomainID);

        // ADD START SUKI 20190104 [Check permission on every page load]
        var permission = hc.LoadUserPermission(cus.UserID, Convert.ToInt16(OpsIDEnum.EditChannel));
        if (permission[0].IsUserAllowed == "0")
        {
            Response.Redirect("../../Backoffice/Dashboard.aspx");
            return;
        }
        // ADD E N D SUKI 20190104 [Check permission on every page load]

        if (!IsPostBack)
        {
            var merchant_L = hc.Do_GetMerchantList(1, 0);
            DD_merchant.DataSource = merchant_L;
            DD_merchant.DataTextField = "DomainDesc";
            DD_merchant.DataValueField = "DomainID";
            DD_merchant.DataBind();
            //ADD START DANIEL 20190125 [Add --Select-- at Dropdown]
            DD_merchant.Items.Insert(0, new ListItem("--SELECT--", "0"));
            //ADD E N D DANIEL 20190125 [Add --Select-- at Dropdown]

            DD_PymtMethod.Enabled = false;
            DD_PymtMethod.Items.Clear();

            btn_GetInfo.Enabled = true;

            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditChannel), MethodBase.GetCurrentMethod().Name, "Page_Load Method");
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
    }
    protected void btn_GetInfo_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        var sDomainID = Convert.ToInt32(DD_merchant.SelectedValue);
        var sPaymentMethod = DD_PymtMethod.SelectedValue;
        var sChannelType = mc.Do_Get_TerminalHost(sDomainID, sPaymentMethod, 1);

        HideCredentialFields();
        DD_Channel.Enabled = true;

        var logObject = new
        {
            LoginUser = cus.UserID,
            DomainID = sDomainID,
            PaymentType = sPaymentMethod
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        //ADD START DANIEL 20190110 [Insert into AuditTrail]
        if (sChannelType == null)
        {
            HiddenField1.Value = mc.err_msg;
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                       "EditChannel(Error) - " + logJson + mc.system_err.ToString());
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditChannel), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + mc.system_err.ToString());
            return;
        }
        //ADD E N D DANIEL 20190110 [Insert into AuditTrail]

        try
        {
            objLoggerII = logger.InitLogger();

            DD_Channel.DataSource = sChannelType;
            DD_Channel.DataTextField = "HostDesc";
            DD_Channel.DataValueField = "HostID";
            DD_Channel.DataBind();
            DD_Channel.Items.Insert(0, new ListItem("--SELECT--", ""));
            //ADD START DANIEL 201901223 [Set enabled to false]
            DD_Channel.Enabled = false;
            //ADD E N D DANIEL 201901223 [Set enabled to false]

            //CHG START DANIEL 20190110 [Insert into AuditTrail]
            //GenerateChannelList(sDomainID, sPaymentMethod, 1);
            string errMsg = GenerateChannelList(sDomainID, sPaymentMethod, 1);
            if (!string.IsNullOrEmpty(errMsg))
            {
                HiddenField1.Value = errMsg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "EditChannel(Error) - " + logJson + mc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditChannel), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + mc.system_err.ToString());
                return;
            }
            //CHG E N D DANIEL 20190110 [Insert into AuditTrail]

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                          "EditChannel - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.EditChannel, 800000, "EditChannel.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditChannel), MethodBase.GetCurrentMethod().Name, logJson);
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "EditChannel(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.EditChannel, 800001, "EditChannel.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditChannel), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }

    protected void btn_Update_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        //ADD START DANIEL 20190124 [Hide panel after update button click]
        Panel_CredentialBody.Visible = false;
        //ADD E N D DANIEL 20190124 [Hide panel after update button click]

        List<string> cardTypeList = new List<string>();
        if (cb_Visa.Checked) cardTypeList.Add("Visa");
        if (cb_Master.Checked) cardTypeList.Add("Master");
        if (cb_Amex.Checked) cardTypeList.Add("Amex");
        //DEL START DANIEL 20190103 [Remove unsupported card type profile]
        //if (cb_Diners.Checked) cardTypeList.Add("Diners");
        //if (cb_JCB.Checked) cardTypeList.Add("JCB");
        //if (cb_CUP.Checked) cardTypeList.Add("CUP");
        //if (cb_MasterPass.Checked) cardTypeList.Add("MasterPass");
        //if (cb_VisaCheckout.Checked) cardTypeList.Add("VisaCheckout");
        //if (cb_SamsungPay.Checked) cardTypeList.Add("SamsungPay");
        //DEL E N D DANIEL 20190103 [Remove unsupported card type profile]

        string cardTypes = string.Join(",", cardTypeList);

        var val = "";
        var txt = "";
        foreach (ListItem listItem in LB_CardGroup.Items)
        {
            if (listItem.Selected)
            {
                if (val != "")
                    val = val + ",";
                if (txt != "")
                    txt = txt + ",";

                val = val + listItem.Value;
                txt = txt + listItem.Text;
            }
        }

        //Added by Daniel 9 Apr 2019. Check if cardgroup is empty
        if (DD_PymtMethod.SelectedValue.Trim().Equals("CC"))
        {
            if (string.IsNullOrEmpty(val))
            {
                HiddenField1.Value = "Card group cannot empty. Please select card group.";
                return;
            }
        }

        var logObject = new
        {
            LoginUser = cus.UserID,
            DomainID = Convert.ToInt32(DD_merchant.SelectedValue),
            PaymentType = DD_PymtMethod.SelectedItem.Text,
            Channel = DD_Channel.SelectedItem.Text,
            Credential1 = txt_Credential1.Text,
            Credential2 = txt_Credential2.Text,
            Credential3 = txt_Credential3.Text,
            Credential4 = txt_Credential4.Text,
            Credential5 = txt_Credential5.Text,
            Credential6 = txt_Credential6.Text,
            CardType = cardTypes,
            Currency = string.IsNullOrEmpty(DD_Currency.SelectedValue) ? DD_CurrencyOB.SelectedItem.Text : DD_Currency.SelectedItem.Text,

            //DEL START DANIEL 20190104 [Priority field is not needed yet]
            //Priority = txt_Priority.Text,
            //DEL E N D DANIEL 20190104 [Priority field is not needed yet]

            CardGroup = txt,
            Status = Convert.ToInt32(DD_Status.SelectedValue)
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            int mode = 0;
            var channelData = mc.Do_Get_TerminalHost(logObject.DomainID);

            //ADD START DANIEL 20190110 [Insert into AuditTrail]
            if (channelData == null)
            {
                HiddenField1.Value = mc.err_msg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "EditChannel(Error) - " + logJson + mc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditChannel), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + mc.system_err.ToString());
                return;
            }
            //ADD E N D DANIEL 20190110 [Insert into AuditTrail]

            foreach (var channel in channelData)
            {
                if (channel.Type == "Credit Card")
                {
                    mode = 1;
                    break;
                }
            }

            string credential1 = string.Empty, credential2 = string.Empty, credential3 = string.Empty, credential4 = string.Empty,
                credential5 = string.Empty, credential6 = string.Empty;

            try
            {
                if (hid_Encrypt1.Value == "1")
                    credential1 = mc.Do_Encrypt3DES(logObject.Credential1);
                else
                    credential1 = logObject.Credential1;

                if (hid_Encrypt2.Value == "1")
                    credential2 = mc.Do_Encrypt3DES(logObject.Credential2);
                else
                    credential2 = logObject.Credential2;

                if (hid_Encrypt3.Value == "1")
                    credential3 = mc.Do_Encrypt3DES(logObject.Credential3);
                else
                    credential3 = logObject.Credential3;

                if (hid_Encrypt4.Value == "1")
                    credential4 = mc.Do_Encrypt3DES(logObject.Credential4);
                else
                    credential4 = logObject.Credential4;

                if (hid_Encrypt5.Value == "1")
                    credential5 = mc.Do_Encrypt3DES(logObject.Credential5);
                else
                    credential5 = logObject.Credential5;

                if (hid_Encrypt6.Value == "1")
                    credential6 = mc.Do_Encrypt3DES(logObject.Credential6);
                else
                    credential6 = logObject.Credential6;

                if (mc.err_msg != string.Empty)
                {
                    HiddenField1.Value = mc.err_msg;
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "EditChannel(Error) - " + logJson + mc.system_err.ToString());
                    //ADD START DANIEl 20190107 [Insert into AuditTrail]
                    hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditChannel), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + mc.system_err.ToString());
                    //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
                    return;
                }
            }
            catch (Exception ex)
            {
                //ADD START DANIEL 20190108 [Assign Exception message] 
                HiddenField1.Value = ex.Message;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                              "EditChannel(Error) - " + logJson + ex.Message.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditChannel), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.Message.ToString());

                //ADD E N D DANIEL 20190108 [Assign Exception message] 
            }

            //CHG START DANIEL 20190104 [Unsupported card type profiles are removed and set to 0 as default value. Priority is set to 1 as default value]
            //bool editChannel = mc.Do_EditChannel(
            //       logObject.DomainID, DD_PymtMethod.SelectedValue.Trim(), Convert.ToInt32(DD_Channel.SelectedValue), LB_Credential1.Text, credential1,
            //                       LB_Credential2.Text, credential2, LB_Credential3.Text, credential3, LB_Credential4.Text, credential4,
            //                       LB_Credential5.Text, credential5, LB_Credential6.Text, credential6,
            //                       logObject.Currency, Convert.ToInt32(txt_Priority.Text), val, (cb_Visa.Checked ? 1 : 0), (cb_Master.Checked ? 1 : 0),
            //                       (cb_Amex.Checked ? 1 : 0), (cb_Diners.Checked ? 1 : 0),
            //                       (cb_JCB.Checked ? 1 : 0), (cb_CUP.Checked ? 1 : 0), (cb_MasterPass.Checked ? 1 : 0), (cb_VisaCheckout.Checked ? 1 : 0),
            //                       (cb_SamsungPay.Checked ? 1 : 0), Convert.ToInt32(DD_Status.SelectedValue), mode);

            bool editChannel = mc.Do_EditChannel(
                  logObject.DomainID, DD_PymtMethod.SelectedValue.Trim(), Convert.ToInt32(DD_Channel.SelectedValue), LB_Credential1.Text, credential1,
                                  LB_Credential2.Text, credential2, LB_Credential3.Text, credential3, LB_Credential4.Text, credential4,
                                  LB_Credential5.Text, credential5, LB_Credential6.Text, credential6,
                                  logObject.Currency, 1, val, (cb_Visa.Checked ? 1 : 0), (cb_Master.Checked ? 1 : 0),
                                  (cb_Amex.Checked ? 1 : 0), 0, 0, 0, 0, 0, 0, Convert.ToInt32(DD_Status.SelectedValue), mode);

            //CHG E N D DANIEL 20190104 [Unsupported card type profiles are removed and set to 0 as default value. Priority is set to 1 as default value] 

            if (!editChannel)
            {
                HiddenField1.Value = mc.err_msg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "EditChannel(Error) - " + logJson + mc.system_err.ToString());
                //ADD START DANIEl 20190107 [Insert into AuditTrail]
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditChannel), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + mc.system_err.ToString());
                //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
                return;
            }

            HideCredentialFields();

            //CHG START DANIEL 20190110 [Insert into AuditTrail]
            //GenerateChannelList(logObject.DomainID, DD_PymtMethod.SelectedValue, 1);
            string errMsg = GenerateChannelList(logObject.DomainID, DD_PymtMethod.SelectedValue, 1);
            if (!string.IsNullOrEmpty(errMsg))
            {
                HiddenField1.Value = errMsg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "EditChannel(Error) - " + logJson + mc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditChannel), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + mc.system_err.ToString());
                return;
            }
            //CHG E N D DANIEL 20190110 [Insert into AuditTrail]

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "EditChannel - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.EditChannel, 800000, "EditChannel.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditChannel), MethodBase.GetCurrentMethod().Name, logJson);
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]

            HiddenField1.Value = "Edit Channel Successful.";
            HiddenField2.Value = AlertType.Success;
        }
        catch (Exception ex)
        {
            HiddenField1.Value = "Failed To Edit Channel.";
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "EditChannel(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.EditChannel, 800001, "EditChannel.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditChannel), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }

    protected void btn_Cancel_Click(object sender, EventArgs e)
    {
        HideCredentialFields();
    }

    protected void OnDD_MerchantChanged(object sender, EventArgs e)
    {
        var sDomainID = Convert.ToInt32(DD_merchant.SelectedValue);
        var sPaymentType = mc.Do_Get_TerminalHost(sDomainID, "ALL", 1);
        var sPaymentGroup = sPaymentType.GroupBy(c => c.TypeValue).Select(c => c.First());

        var ddlControl = (DropDownList)sender;

        if (ddlControl.SelectedIndex > 0)
        {
            DD_PymtMethod.Enabled = true;
            DD_PymtMethod.DataSource = sPaymentGroup;
            DD_PymtMethod.DataTextField = "Type";
            DD_PymtMethod.DataValueField = "TypeValue";
            DD_PymtMethod.DataBind();

            HideCredentialFields();
            DD_Channel.Enabled = false;
        }
        else
        {
            HideCredentialFields();
            DD_Channel.Enabled = false;
            DD_PymtMethod.Enabled = false;
            DD_PymtMethod.Items.Clear();
        }
        //ADD START DANIEL 20190125 [List bind to null when merchant changed]
        RP_Channel.DataSource = null;
        RP_Channel.DataBind();
        //ADD E N D DANIEL 20190125 [List bind to null when merchant changed]
    }

    //DEL START DANIEL 20190123 [Remove OnSelectedIndexChanged]
    //protected void OnDD_ChannelChanged(object sender, EventArgs e)
    //{
    //    var ddlControl = (DropDownList)sender;
    //    List<string[]> statusList = new List<string[]> { new string[] { "Activate", "1" }, new string[] { "Deactivate", "0" } };

    //    if (ddlControl.SelectedIndex > 0)
    //    {
    //        var sDomainID = Convert.ToInt32(DD_merchant.SelectedValue);
    //        var sHostID = Convert.ToInt32(DD_Channel.SelectedValue);
    //        var currencyList = mc.Do_GetCurrencyCountry(1);

    //        //ADD START DANIEL 20190123 [Get TerminalHost Currency]
    //        var terminalCurrency = mc.Do_Get_TerminalHost(sDomainID, DD_PymtMethod.SelectedValue);
    //        //ADD E N D DANIEL 20190123 [Get TerminalHost Currency]

    //        var terminalCredential = GetCredential(DD_PymtMethod.SelectedValue, sHostID, "MYR");

    //        var selectedCardGroup = mc.Do_Get_PGM_CardGroup(sDomainID, sHostID);

    //        DD_Status.DataSource = from status in statusList
    //                               select new
    //                               {
    //                                   Text = status[0],
    //                                   Value = status[1]
    //                               };
    //        DD_Status.DataTextField = "Text";
    //        DD_Status.DataValueField = "Value";
    //        DD_Status.DataBind();

    //        var getStatusList = mc.Do_Get_TerminalHost(sDomainID, DD_PymtMethod.SelectedValue, 1);
    //        var getStatus = getStatusList.Where(s => s.HostID == sHostID).Select(s => s.RecStatus).First();
    //        DD_Status.ClearSelection();
    //        DD_Status.SelectedValue = getStatus;
    //        string currencyCode = string.Empty;
    //        Panel_Priority_Status.Visible = true;

    //        if (terminalCredential != null)
    //        {
    //            currencyCode = terminalCredential.Select(c => c.CurrencyCode).First().ToString();
    //        }

    //        if (DD_PymtMethod.SelectedItem.Text == "Credit Card")
    //        {
    //            DD_Currency.DataSource = currencyList;
    //            DD_Currency.DataTextField = "CurrCode";
    //            DD_Currency.DataValueField = "CurrCode";
    //            DD_Currency.DataBind();

    //            if (!string.IsNullOrEmpty(currencyCode))
    //                DD_Currency.Items.FindByValue(currencyCode).Selected = true;

    //            var cardGroupList = mc.Do_GetPGMCardGroup();
    //            LB_CardGroup.DataSource = cardGroupList;
    //            LB_CardGroup.DataTextField = "CardGrpDesc";
    //            LB_CardGroup.DataValueField = "CardGrpID";
    //            LB_CardGroup.DataBind();

    //            GetCardType(sDomainID);

    //            foreach (var cardGroup in selectedCardGroup)
    //            {
    //                for (int i = 0; i < LB_CardGroup.Items.Count - 1; i++)
    //                {
    //                    if (LB_CardGroup.Items[i].Text == cardGroup.CardGrpDesc)
    //                    {
    //                        LB_CardGroup.Items[i].Selected = true;
    //                    }
    //                }
    //            }
    //            Panel_Credential.Visible = true;
    //            Panel_Update.Visible = true;
    //            Panel_CC.Visible = true;
    //            Panel_OB.Visible = false;
    //        }
    //        else
    //        {
    //            DD_CurrencyOB.DataSource = currencyList;
    //            DD_CurrencyOB.DataTextField = "CurrCode";
    //            DD_CurrencyOB.DataValueField = "CurrCode";
    //            DD_CurrencyOB.DataBind();
    //            if (!string.IsNullOrEmpty(currencyCode))
    //                DD_CurrencyOB.Items.FindByValue(currencyCode).Selected = true;

    //            Panel_Credential.Visible = true;
    //            Panel_Update.Visible = true;
    //            Panel_CC.Visible = false;
    //            Panel_OB.Visible = true;
    //        }
    //    }
    //    else
    //    {
    //        Panel_CC.Visible = false;
    //        Panel_OB.Visible = false;
    //        Panel_Priority_Status.Visible = false;
    //        Panel_Credential.Visible = false;
    //        Panel_Update.Visible = false;
    //    }
    //}
    //DEL E N D DANIEL 20190123 [Remove OnSelectedIndexChanged]

    void GetCardType(int PDomainID)
    {
        CardTypeProfile[] cardTypeProfile = mc.Do_GetCardTypeProfile(PDomainID);
        if (cardTypeProfile != null)
        {
            if (cardTypeProfile.Length > 0)
            {
                if (cardTypeProfile[0].VISA == 1)
                {
                    cb_Visa.Checked = true;
                }
                if (cardTypeProfile[0].MasterCard == 1)
                {
                    cb_Master.Checked = true;
                }
                if (cardTypeProfile[0].AMEX == 1)
                {
                    cb_Amex.Checked = true;
                }

                //DEL START DANIEL 20190104 [Remove unsupported card type profiles]
                //if (cardTypeProfile[0].Diners == 1)
                //{
                //    cb_Diners.Checked = true;
                //}
                //if (cardTypeProfile[0].JCB == 1)
                //{
                //    cb_JCB.Checked = true;
                //}
                //if (cardTypeProfile[0].CUP == 1)
                //{
                //    cb_CUP.Checked = true;
                //}
                //if (cardTypeProfile[0].MasterPass == 1)
                //{
                //    cb_MasterPass.Checked = true;
                //}
                //if (cardTypeProfile[0].VisaCheckout == 1)
                //{
                //    cb_VisaCheckout.Checked = true;
                //}
                //if (cardTypeProfile[0].SamsungPay == 1)
                //{
                //    cb_SamsungPay.Checked = true;
                //}
                //DEL E N D DANIEL 20190104 [Remove unsupported card type profiles]
            }
        }
    }

    TerminalCredential[] GetCredential(string PPymtMethodCode, int HostID, string PCurrencyCode)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();
        //var CredentialList = mc.Do_GetHostCredential(PPymtMethodCode, HostID);
        var CredentialList = mc.Do_GetHostCredential(PPymtMethodCode, HostID, PCurrencyCode);// Modified by Daniel 23 Apr 2019. Add PCurrencyCode

        //ADD START DANIEL 20190111 [return null if CredentialList is null]
        if (CredentialList == null)
            return null;
        //ADD E N D DANIEL 20190111 [return null if CredentialList is null]

        int i = 1;
        List<string> DBFieldName = new List<string>();

        foreach (var credential in CredentialList)
        {
            ((Label)this.FindControl("LB_Credential" + (i))).Text = credential.FieldName;
            ((Label)this.FindControl("LB_Credential" + (i))).Visible = true;
            ((TextBox)this.FindControl("txt_Credential" + (i))).Visible = true;
            ((HiddenField)this.FindControl("hid_Encrypt" + (i))).Value = credential.Encrypt.ToString();
            ((HiddenField)this.FindControl("hid_Hex" + (i))).Value = credential.Hex.ToString();

            DBFieldName.Add(credential.DBFieldName);
            i++;
        }

        var DbFieldNameList = string.Join(",", DBFieldName);
        //CHG START DANIEL 20190123 [Add parameter PCurrencyCode]
        //var terminalCredentials = mc.Do_GetTerminalCredentials(Convert.ToInt32(DD_merchant.SelectedValue), DD_PymtMethod.SelectedValue.Trim(),
        //                                Convert.ToInt32(DD_Channel.SelectedValue), DbFieldNameList);
        var terminalCredentials = mc.Do_GetTerminalCredentials(Convert.ToInt32(DD_merchant.SelectedValue), PPymtMethodCode,
                                     HostID, DbFieldNameList, PCurrencyCode);

        //CHG E N D DANIEL 20190123 [Add parameter PCurrencyCode]

        //ADD START KENT LOONG 20190228 [Add checking for Terminal Credentials]
        if (terminalCredentials == null)
        {
            objLoggerII = logger.InitLogger();
            HiddenField1.Value = mc.err_msg;
            var logObject = new
            {
                ErrorMessage = " Credentials not found "
            };
            var logJson = JsonConvert.SerializeObject(logObject);

            objLoggerII = logger.InitLogger();
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                       "EditChannel(Error) - " + logJson + mc.system_err.ToString());
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditChannel), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + mc.system_err.ToString());
            return null;
        }
        //ADD E N D DANIEL 20190110 [Add checking for Terminal Credentials]

        //DEL START DANIEL 20190104 [Priority field is not needed yet]
        //txt_Priority.Visible = true;
        //DEL E N D DANIEL 20190104 [Priority field is not needed yet]

        var oriPassword = string.Empty;
        var oriReturnKey = string.Empty;
        var oriPayeeCode = string.Empty;
        var oriMID = string.Empty;
        var oriAcquirerID = string.Empty;
        var oriSecretKey = string.Empty;
        var oriAirlineCode = string.Empty;
        var oriSendKeyPath = string.Empty;
        var oriReturnKeyPath = string.Empty;

        for (int a = 1; a <= DBFieldName.Count(); a++)
        {
            foreach (var credentials in terminalCredentials)
            {
                string defValue = string.Empty;
                if (((HiddenField)this.FindControl("hid_Encrypt" + (a))).Value.Equals("1"))
                {
                    if (((HiddenField)this.FindControl("hid_Hex" + (a))).Value.Equals("1"))
                    {
                        oriPassword = mc.Do_Decrypt3DEStoHex(credentials.Password);
                        oriReturnKey = mc.Do_Decrypt3DEStoHex(credentials.ReturnKey);
                        oriPayeeCode = mc.Do_Decrypt3DEStoHex(credentials.PayeeCode);
                        oriMID = mc.Do_Decrypt3DEStoHex(credentials.MID);
                        oriAcquirerID = mc.Do_Decrypt3DEStoHex(credentials.AcquirerID);
                        oriSecretKey = mc.Do_Decrypt3DEStoHex(credentials.SecretKey);
                        oriAirlineCode = mc.Do_Decrypt3DEStoHex(credentials.AirlineCode);
                        oriSendKeyPath = mc.Do_Decrypt3DEStoHex(credentials.SendKeyPath);
                        oriReturnKeyPath = mc.Do_Decrypt3DEStoHex(credentials.ReturnKeyPath);
                    }
                    else
                    {
                        oriPassword = mc.Do_Decrypt3DES(credentials.Password);
                        oriReturnKey = mc.Do_Decrypt3DES(credentials.ReturnKey);
                        oriPayeeCode = mc.Do_Decrypt3DES(credentials.PayeeCode);
                        oriMID = mc.Do_Decrypt3DES(credentials.MID);
                        oriAcquirerID = mc.Do_Decrypt3DES(credentials.AcquirerID);
                        oriSecretKey = mc.Do_Decrypt3DES(credentials.SecretKey);
                        oriAirlineCode = mc.Do_Decrypt3DES(credentials.AirlineCode);
                        oriSendKeyPath = mc.Do_Decrypt3DES(credentials.SendKeyPath);
                        oriReturnKeyPath = mc.Do_Decrypt3DES(credentials.ReturnKeyPath);
                    }
                }
                else
                {
                    oriPassword = credentials.Password;
                    oriReturnKey = credentials.ReturnKey;
                    oriPayeeCode = credentials.PayeeCode;
                    oriMID = credentials.MID;
                    oriAcquirerID = credentials.AcquirerID;
                    oriSecretKey = credentials.SecretKey;
                    oriAirlineCode = credentials.AirlineCode;
                    oriSendKeyPath = credentials.SendKeyPath;
                    oriReturnKeyPath = credentials.ReturnKeyPath;
                }

                if (DBFieldName[a - 1].Equals("Password"))
                    ((TextBox)this.FindControl("txt_Credential" + (a))).Text = oriPassword;
                else if (DBFieldName[a - 1].Equals("ReturnKey"))
                    ((TextBox)this.FindControl("txt_Credential" + (a))).Text = oriReturnKey;
                else if (DBFieldName[a - 1].Equals("PayeeCode"))
                    ((TextBox)this.FindControl("txt_Credential" + (a))).Text = oriPayeeCode;
                else if (DBFieldName[a - 1].Equals("MID"))
                    ((TextBox)this.FindControl("txt_Credential" + (a))).Text = oriMID;
                else if (DBFieldName[a - 1].Equals("AcquirerID"))
                    ((TextBox)this.FindControl("txt_Credential" + (a))).Text = oriAcquirerID;
                else if (DBFieldName[a - 1].Equals("SecretKey"))
                    ((TextBox)this.FindControl("txt_Credential" + (a))).Text = oriSecretKey;
                else if (DBFieldName[a - 1].Equals("AirlineCode"))
                    ((TextBox)this.FindControl("txt_Credential" + (a))).Text = oriAirlineCode;
                else if (DBFieldName[a - 1].Equals("SendKeyPath"))
                    ((TextBox)this.FindControl("txt_Credential" + (a))).Text = oriSendKeyPath;
                else if (DBFieldName[a - 1].Equals("ReturnKeyPath"))
                    ((TextBox)this.FindControl("txt_Credential" + (a))).Text = oriReturnKeyPath;
                else
                    ((TextBox)this.FindControl("txt_Credential" + (a))).Text = string.Empty;

                //DEL START DANIEL 20190104 [Priority field is not needed yet]
                //Priority.Text = credentials.Priority.ToString();
                //DEL E N D DANIEL 20190104 [Priority field is not needed yet]
            }
        }
        return terminalCredentials ?? null;
    }

    //CHG START DANIEL 20190110 [Change method type to string]
    //void GenerateChannelList(int PDomainID, string PPymtMethod, int PStatusYesNo)
    string GenerateChannelList(int PDomainID, string PPymtMethod, int PStatusYesNo)
    //CHG E N D DANIEL 20190110 [Change method type to string]
    {
        //CHG START DANIEL 20190123 [Change paymentMethod]
        //var channelData = mc.Do_Get_TerminalHost(PDomainID, "ALL", PStatusYesNo);
        var channelData = mc.Do_Get_TerminalHost(PDomainID, PPymtMethod, PStatusYesNo);
        //CHG E N D DANIEL 20190123 [Change paymentMethod]

        //ADD START DANIEL 20190110 [If null, return MGMT controller error message]
        if (channelData == null)
            return mc.err_msg;
        //ADD E N D DANIEL 20190110 [If null, return MGMT controller error message]

        RP_Channel.DataSource = channelData;
        RP_Channel.DataBind();

        //ADD START DANIEL 20190110 [Add return string]
        return string.Empty;
        //ADD E N D DANIEL 20190110 [Add return string]
    }

    void HideCredentialFields()
    {
        Panel_CC.Visible = false;
        Panel_OB.Visible = false;
        Panel_Priority_Status.Visible = false;
        Panel_Credential.Visible = false;
        Panel_Update.Visible = false;
        DD_Channel.Enabled = false;
        DD_Channel.Items.Clear();
        //ADD START DANIEL 20190131 [Hide panel credential body]
        Panel_CredentialBody.Visible = false;
        //ADD E N D DANIEL 20190131 [Hide panel credential body]
    }

    //ADD START DANIEL 20190123 [Get info when edit clicked]
    protected void RP_Channel_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName.Equals("Edit"))
        {
            string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
            string pymtType = commandArgs[0];
            int hostID = Convert.ToInt32(commandArgs[1]);
            string currency = commandArgs[2];
            Panel_CredentialBody.Visible = true;

            List<string[]> statusList = new List<string[]> { new string[] { "Activate", "1" }, new string[] { "Deactivate", "0" } };

            var sDomainID = Convert.ToInt32(DD_merchant.SelectedValue);
            var currencyList = mc.Do_GetCurrencyCountry(1);
            var terminalCurrency = mc.Do_Get_TerminalHost(sDomainID, DD_PymtMethod.SelectedValue);
            var terminalCredential = GetCredential(DD_PymtMethod.SelectedValue, hostID, currency);
            if (terminalCredential == null)
            {
                Panel_CredentialBody.Visible = false;
                return;
            }

            //var selectedCardGroup = mc.Do_Get_PGM_CardGroup(sDomainID, hostID);
            var selectedCardGroup = mc.Do_Get_PGM_CardGroup(sDomainID, hostID, currency); //Modified by Daniel 29 Apr 2019. Cater for currency.

            DD_Status.DataSource = from status in statusList
                                   select new
                                   {
                                       Text = status[0],
                                       Value = status[1]
                                   };
            DD_Status.DataTextField = "Text";
            DD_Status.DataValueField = "Value";
            DD_Status.DataBind();

            var getStatusList = mc.Do_Get_TerminalHost(sDomainID, DD_PymtMethod.SelectedValue, 1);
            var getStatus = getStatusList.Where(s => (s.HostID == hostID) && (s.CurrencyCode.Equals(currency))).Select(s => s.RecStatus).First();

            DD_Status.ClearSelection();
            DD_Status.SelectedValue = getStatus;

            if (getStatus.Equals("0"))
            {
                txt_Credential1.Enabled = false;
                txt_Credential2.Enabled = false;
                txt_Credential3.Enabled = false;
                txt_Credential4.Enabled = false;
                txt_Credential5.Enabled = false;
                txt_Credential6.Enabled = false;
            }
            else
            {
                txt_Credential1.Enabled = true;
                txt_Credential2.Enabled = true;
                txt_Credential3.Enabled = true;
                txt_Credential4.Enabled = true;
                txt_Credential5.Enabled = true;
                txt_Credential6.Enabled = true;
            }
            string currencyCode = string.Empty;
            Panel_Priority_Status.Visible = true;

            DD_Channel.DataSource = getStatusList;
            DD_Channel.DataTextField = "HostDesc";
            DD_Channel.DataValueField = "HostID";
            DD_Channel.DataBind();
            DD_Channel.Items.FindByValue(hostID.ToString()).Selected = true;

            if (DD_PymtMethod.SelectedItem.Text == "Credit Card")
            {
                DD_Currency.DataSource = currencyList;
                DD_Currency.DataTextField = "CurrCode";
                DD_Currency.DataValueField = "CurrCode";
                DD_Currency.DataBind();
                DD_Currency.Items.FindByValue(currency).Selected = true;
                DD_Currency.Enabled = false;

                var cardGroupList = mc.Do_GetPGMCardGroup();
                LB_CardGroup.DataSource = cardGroupList;
                LB_CardGroup.DataTextField = "CardGrpDesc";
                LB_CardGroup.DataValueField = "CardGrpID";
                LB_CardGroup.DataBind();

                GetCardType(sDomainID);

                foreach (var cardGroup in selectedCardGroup)
                {
                    for (int i = 0; i < LB_CardGroup.Items.Count - 1; i++)
                    {
                        if (LB_CardGroup.Items[i].Text == cardGroup.CardGrpDesc)
                        {
                            LB_CardGroup.Items[i].Selected = true;
                        }
                    }
                }
                Panel_Credential.Visible = true;
                Panel_Update.Visible = true;
                Panel_CC.Visible = true;
                Panel_OB.Visible = false;
            }
            else
            {
                DD_CurrencyOB.DataSource = currencyList;
                DD_CurrencyOB.DataTextField = "CurrCode";
                DD_CurrencyOB.DataValueField = "CurrCode";
                DD_CurrencyOB.DataBind();
                DD_CurrencyOB.Items.FindByValue(currency).Selected = true;
                DD_CurrencyOB.Enabled = false;

                Panel_Credential.Visible = true;
                Panel_Update.Visible = true;
                Panel_CC.Visible = false;
                Panel_OB.Visible = true;
            }
        }
    }
    //ADD E N D DANIEL 20190123 [Get info when edit clicked]

    //ADD START DANIEL 20190124 [Add OnSelectedIndexChanged. When changed to Activate, textbox enabled =true]
    protected void OnChanged_DD_Status(object sender, EventArgs e)
    {
        var control = (DropDownList)sender;
        if (control.SelectedIndex == 1)
        {
            txt_Credential1.Enabled = false;
            txt_Credential2.Enabled = false;
            txt_Credential3.Enabled = false;
            txt_Credential4.Enabled = false;
            txt_Credential5.Enabled = false;
            txt_Credential6.Enabled = false;
        }
        else
        {
            txt_Credential1.Enabled = true;
            txt_Credential2.Enabled = true;
            txt_Credential3.Enabled = true;
            txt_Credential4.Enabled = true;
            txt_Credential5.Enabled = true;
            txt_Credential6.Enabled = true;
        }

    }
    //ADD E N D DANIEL 20190124 [Add OnSelectedIndexChanged. When changed to Activate, textbox enabled =true]
}

