﻿using LoggerII;
using Newtonsoft.Json;
using PaydibsPortal.BLL;
using PaydibsPortal.DAL;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Web;
using System.Web.Configuration;

public partial class Backoffice_MMGMT_EditMerchant : System.Web.UI.Page
{
    MMGMTController mc = new MMGMTController();
    HomeController hc = null;
    CurrentUserStat cus = null;
    static int BusinessProfileID = 0;
    string error_msg = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        HiddenField1.Value = "";
        HiddenField2.Value = "";

        if (Session[SessionType.CurrentUser] == null)
        {
            Response.Redirect("../../Default.aspx");
            return;
        }

        cus = (CurrentUserStat)Session[SessionType.CurrentUser];
        hc = new HomeController(cus.DomainID);

        // ADD START SUKI 20190104 [Check permission on every page load]
        var permission = hc.LoadUserPermission(cus.UserID, Convert.ToInt16(OpsIDEnum.EditMerchant));
        if (permission[0].IsUserAllowed == "0")
        {
            Response.Redirect("../../Backoffice/Dashboard.aspx");
            return;
        }
        // ADD E N D SUKI 20190104 [Check permission on every page load]

        if (!IsPostBack)
        {
            var merchant_L = hc.Do_GetMerchantList(1, 0);
            DD_merchant.DataSource = merchant_L;
            DD_merchant.DataTextField = "DomainDesc";
            DD_merchant.DataValueField = "DomainID";
            DD_merchant.DataBind();

            DD_BusinesssState.DataSource = mc.Do_GetCLState();
            DD_BusinesssState.DataTextField = "State";
            DD_BusinesssState.DataValueField = "PKID";
            DD_BusinesssState.DataBind();

            DD_Country.DataSource = mc.Do_GetCLCountry();
            DD_Country.DataTextField = "CountryName";
            DD_Country.DataValueField = "CountryID";
            DD_Country.DataBind();

            DD_Plugin.DataSource = mc.Do_GetCLPlugin();
            DD_Plugin.DataTextField = "PluginName";
            DD_Plugin.DataValueField = "PluginID";
            DD_Plugin.DataBind();

            DD_Notify.DataSource = mc.Do_GetCLNotifEmailRcv();
            DD_Notify.DataTextField = "NotifEmailRcvDesc";
            DD_Notify.DataValueField = "NotifEmailRcvID";
            DD_Notify.DataBind();

            DD_BankCountry.DataSource = mc.Do_GetCLCountry();
            DD_BankCountry.DataTextField = "CountryName";
            DD_BankCountry.DataValueField = "CountryID";
            DD_BankCountry.DataBind();

            int i = 0;
            foreach (var r in mc.Do_GetCLCountry())
            {
                if (r.CountryName == "Malaysia")
                {
                    i = r.CountryID - 1;
                    break;
                }
            }
            DD_Country.SelectedIndex = i;
            DD_BankCountry.SelectedIndex = i;

            DD_MCCCode.DataSource = mc.Do_GetCLMCCCode();
            DD_MCCCode.DataTextField = "Description";
            DD_MCCCode.DataValueField = "MCCCode";
            DD_MCCCode.DataBind();

            // ADD START SUKI 20181128 [Add in showMerchantAddr, showMerchantLogo, IsAgent, default currency setting]
            var currencyList = mc.Do_GetCurrencyCountry(1);
            DD_Currency.DataSource = currencyList;
            DD_Currency.DataTextField = "CurrCode";
            DD_Currency.DataValueField = "CurrCode";
            DD_Currency.DataBind();
            DD_Currency.SelectedValue = "MYR";
            // ADD E N D SUKI 20181128 [Add in showMerchantAddr, showMerchantLogo, IsAgent, default currency setting]

            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditMerchant), MethodBase.GetCurrentMethod().Name, "Page_Load Method");
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
    }

    protected void btn_GetInfo_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        var logObject = new
        {
            LoginUser = cus.UserID,
            DomainID = Convert.ToInt32(DD_merchant.SelectedValue)
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            //CHG START DANIEL 20190110 [Insert into AuditTrail]
            //GetMerchantInfo(logObject.DomainID);
            string errMsg = GetMerchantInfo(logObject.DomainID);

            if (!string.IsNullOrEmpty(errMsg))
            {
                HiddenField1.Value = errMsg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            "EditMerchant(Error) - " + logJson + mc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditMerchant), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + mc.system_err.ToString());
                return;
            }
            //CHG E N D DANIEL 20190110 [Insert into AuditTrail]

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "EditMerchant - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.EditMerchant, 800000, "EditMerchant.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditMerchant), MethodBase.GetCurrentMethod().Name, logJson);
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "EditMerchant(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.EditMerchant, 800001, "EditMerchant.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditMerchant), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }

    protected void btn_Cancel_Click(object sender, EventArgs e)
    {

    }

    protected void btn_Complete_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        #region Mandatory Checking
        string chkMessage = string.Empty;
        if (string.IsNullOrEmpty(txt_BusinessRegNo.Text))
        {
            chkMessage += "A) Comany Registration No." + System.Environment.NewLine;
        }
        if (string.IsNullOrEmpty(txt_BusinessAdd1.Text))
        {
            chkMessage += "A) Address" + System.Environment.NewLine;
        }
        if (string.IsNullOrEmpty(txt_OfficeNumber.Text))
        {
            chkMessage += "A) Office Contact Number" + System.Environment.NewLine;
        }
        if (string.IsNullOrEmpty(txt_acp1_FullName.Text))
        {
            chkMessage += "A) Authorized Contact Person Name" + System.Environment.NewLine;
        }
        if (string.IsNullOrEmpty(txt_acp1_MobileNumber.Text))
        {
            chkMessage += "A) Authorized Contact Person Contact No." + System.Environment.NewLine;
        }
        if (string.IsNullOrEmpty(txt_acp1_EmailAdd.Text))
        {
            chkMessage += "A) Authorized Contact Person Email" + System.Environment.NewLine;
        }
        if (string.IsNullOrEmpty(txt_businessName.Text))
        {
            chkMessage += "C) Trading Name" + System.Environment.NewLine;
        }
        if (!string.IsNullOrEmpty(chkMessage))
        {
            HiddenField1.Value = "Please enter mandatory field:" + System.Environment.NewLine + chkMessage;
            return;
        }
        #endregion

        #region A) Company Info
        string BusinessRegName = txt_BusinessRegName.Text.Trim();
        string BusinessRegNo = txt_BusinessRegNo.Text.Trim();
        string GSTNo = ""; //txt_GSTNo.Text.Trim();
        string TypeOfBusiness = string.Empty;
        //if (rd_TypeOfBusiness_SP.Checked) TypeOfBusiness = "Sole Proprietorship";
        //else if (rd_TypeOfBusiness_CO.Checked) TypeOfBusiness = "Corporation (Sdn Bhd/ Bhd)";
        //else if (rd_TypeOfBusiness_PS.Checked) TypeOfBusiness = "Partnership";
        //else if (rd_TypeOfBusiness_NPO.Checked) TypeOfBusiness = "Non-Profit Organization";
        //else if (rd_TypeOfBusiness_Other.Checked) TypeOfBusiness = "Other";

        string BusinessAdd1 = txt_BusinessAdd1.Text.Trim();
        string BusinessAdd2 = txt_BusinessAdd2.Text.Trim();
        string BusinessAdd3 = txt_BusinessAdd3.Text.Trim();
        string BusinessCity = txt_BusinessCity.Text.Trim();
        string BusinessState = DD_BusinesssState.SelectedValue;
        string BusinessZipcode = txt_BusinessZipcode.Text.Trim();
        string BusinessCountry = DD_Country.SelectedValue;
        string OfficeNumber = txt_OfficeNumber.Text.Trim();

        string ACP1Name = txt_acp1_FullName.Text.Trim();
        string ACP1Mobile = txt_acp1_MobileNumber.Text.Trim();
        string ACP1Email = txt_acp1_EmailAdd.Text.Trim();

        string ACP2Name = txt_acp2_FullName.Text.Trim();
        string ACP2Mobile = txt_acp2_MobileNumber.Text.Trim();
        string ACP2Email = txt_acp2_EmailAdd.Text.Trim();

        string billName = txt_bill_FullName.Text.Trim();
        string billMobile = txt_bill_MobileNumber.Text.Trim();
        string billEmail = txt_bill_EmailAdd.Text.Trim();
        #endregion

        #region B) List of Company Director / Proprietor
        string D_FullName_i = ""; //txt_i_director_name.Text.Trim();
        string D_ID_i = ""; //txt_i_director_id.Text.Trim();
        string D_FullName_ii = ""; //txt_ii_director_name.Text.Trim();
        string D_ID_ii = ""; //txt_ii_director_id.Text.Trim();
        string D_FullName_iii = ""; //txt_iii_director_name.Text.Trim();
        string D_ID_iii = ""; //txt_iii_director_id.Text.Trim();
        string D_FullName_iv = ""; //txt_iv_director_name.Text.Trim();
        string D_ID_iv = ""; //txt_iv_director_id.Text.Trim();
        string D_FullName_v = ""; //txt_v_director_name.Text.Trim();
        string D_ID_v = ""; //txt_v_director_id.Text.Trim();

        string S_FullName_i = ""; //txt_i_share_name.Text.Trim();
        string S_ID_i = ""; //txt_i_share_id.Text.Trim();
        string S_FullName_ii = ""; //txt_ii_share_name.Text.Trim();
        string S_ID_ii = ""; //txt_ii_share_id.Text.Trim();
        string S_FullName_iii = ""; //txt_iii_share_name.Text.Trim();
        string S_ID_iii = ""; //txt_iii_share_id.Text.Trim();
        string S_FullName_iv = ""; //txt_iv_share_name.Text.Trim();
        string S_ID_iv = ""; //txt_iv_share_id.Text.Trim();
        string S_FullName_v = ""; //txt_v_share_name.Text.Trim();
        string S_ID_v = ""; //txt_v_share_id.Text.Trim();
        #endregion

        #region C) Business Profile
        string TradingName = txt_businessName.Text.Trim();
        string WebSiteURL = txt_onlinestoreURL.Text.Trim();
        string TypeOfProducts = txt_TypeOfProducts.Text.Trim();

        List<string> targetMarkets_L = new List<string>();
        //if (cb_market_Malaysia.Checked) targetMarkets_L.Add("Malaysia");
        //if (cb_market_Indonesia.Checked) targetMarkets_L.Add("Indonesia");
        //if (cb_market_Thailand.Checked) targetMarkets_L.Add("Thailand");
        //if (cb_market_Singapore.Checked) targetMarkets_L.Add("Singapore");
        //if (cb_market_China.Checked) targetMarkets_L.Add("China");
        //if (cb_market_Philippines.Checked) targetMarkets_L.Add("Philippines");
        //if (cb_market_Others.Checked) targetMarkets_L.Add("Other");
        string targetMarkets = string.Join(",", targetMarkets_L);

        string DeliveredProduct = string.Empty;
        if (rd_Instant.Checked) DeliveredProduct = "Instant";
        else if (rd_1_3_days.Checked) DeliveredProduct = "1-3 days";
        else if (rd_4_10_days.Checked) DeliveredProduct = "4-10 days";
        else if (rd_11_30_days.Checked) DeliveredProduct = "11-30 days";
        else if (rd_30_days.Checked) DeliveredProduct = "> 30 days";

        string PluginID = DD_Plugin.SelectedValue;
        #endregion

        #region D) Merchant Account
        string NotifyEmailFlag = DD_Notify.SelectedValue;
        string PaymentNotifyEmail = txt_IPNEMAIL.Text.Trim();

        string CSEmail = txt_cs_EmailAddr.Text.Trim();
        string CSMobile = txt_cs_MobileNumber.Text.Trim();
        #endregion

        #region E) Bank Account
        string BankName = txt_BankName.Text.Trim();
        string BankAccountNo = txt_BankAccountNo.Text.Trim();
        string BankAccountName = txt_BankAccountName.Text.Trim();
        string SWIFTCode = txt_SWIFTCode.Text.Trim();
        string BankAddress = txt_BankAddress.Text.Trim();
        string BankCountry = DD_BankCountry.SelectedItem.Text;
        #endregion

        #region F) Office
        string MCCCode = DD_MCCCode.SelectedValue;
        decimal TxnLimit = (txt_TransactionLimit.Text.Trim() == "" ? 3000 : Convert.ToDecimal(txt_TransactionLimit.Text));
        string MerchantID = txt_MerchantID.Text.Trim();
        // ADD START SUKI 20181128 [Add in showMerchantAddr, showMerchantLogo, IsAgent, default currency setting]
        int showMerchantAddr = (cb_showMerchantAddr.Checked == true) ? 1 : 0;
        int showMerchantLogo = (cb_showMerchantLogo.Checked == true) ? 1 : 0;
        int isAgent = (cb_Agent.Checked == true) ? 1 : 0;
        string defaultCurrency = DD_Currency.SelectedValue;
        // ADD E N D SUKI 20181128 [Add in showMerchantAddr, showMerchantLogo, IsAgent, default currency setting]
        #endregion

        #region LogJson
        var logObject = new
        {
            LoginUser = cus.UserID,
            aBusinessRegName = BusinessRegName,
            aBusinessRegNo = BusinessRegNo,
            aGSTNo = GSTNo,
            aTypeOfBusiness = TypeOfBusiness,
            aBusinessAdd1 = BusinessAdd1,
            aBusinessAdd2 = BusinessAdd2,
            aBusinessAdd3 = BusinessAdd3,
            aBusinessCity = BusinessCity,
            aBusinessState = BusinessState,
            aBusinessZipcode = BusinessZipcode,
            aBusinessCountry = BusinessCountry,
            aOfficeNumber = OfficeNumber,
            aACP1Name = ACP1Name,
            aACP1Mobile = ACP1Mobile,
            aACP1Email = ACP1Email,
            aACP2Name = ACP2Name,
            aACP2Mobile = ACP2Mobile,
            aACP2Email = ACP2Email,
            abillName = billName,
            abillMobile = billMobile,
            abillEmail = billEmail,
            bD_FullName_i = D_FullName_i,
            bD_ID_i = D_ID_i,
            bD_FullName_ii = D_FullName_ii,
            bD_ID_ii = D_ID_ii,
            bD_FullName_iii = D_FullName_iii,
            bD_ID_iii = D_ID_iii,
            bD_FullName_iv = D_FullName_iv,
            bD_ID_iv = D_ID_iv,
            bD_FullName_v = D_FullName_v,
            bD_ID_v = D_ID_v,
            bS_FullName_i = S_FullName_i,
            bS_ID_i = S_ID_i,
            bS_FullName_ii = S_FullName_ii,
            bS_ID_ii = S_ID_ii,
            bS_FullName_iii = S_FullName_iii,
            bS_ID_iii = S_ID_iii,
            bS_FullName_iv = S_FullName_iv,
            bS_ID_iv = S_ID_iv,
            bS_FullName_v = S_FullName_v,
            bS_ID_v = S_ID_v,
            cTradingName = TradingName,
            cWebSiteURL = WebSiteURL,
            cTypeOfProducts = TypeOfProducts,
            ctargetMarkets = targetMarkets,
            cDeliveredProduct = DeliveredProduct,
            cPluginID = PluginID,
            dNotifyEmailFlag = NotifyEmailFlag,
            dPaymentNotifyEmail = PaymentNotifyEmail,
            dCSEmail = CSEmail,
            dCSMobile = CSMobile,
            eBankName = BankName,
            eBankAccountNo = BankAccountNo,
            eBankAccountName = BankAccountName,
            eSWIFTCode = SWIFTCode,
            eBankAddress = BankAddress,
            eBankCountry = BankCountry,
            fMCCCode = MCCCode,
            fTxnLimit = TxnLimit,
            fMerchantID = MerchantID,
            // ADD START SUKI 20181128 [Add in showMerchantAddr, showMerchantLogo, IsAgent, default currency setting]
            fShowMerchantAddr = showMerchantAddr,
            fShowMerchantLogo = showMerchantLogo,
            fIsAgent = isAgent,
            fDefaultCurrency = defaultCurrency
            // ADD E N D SUKI 20181128 [Add in showMerchantAddr, showMerchantLogo, IsAgent, default currency setting]
        };
        var logJson = JsonConvert.SerializeObject(logObject);
        #endregion

        try
        {
            objLoggerII = logger.InitLogger();

            // CHG START SUKI 20181128 [Add in showMerchantAddr, showMerchantLogo, IsAgent, default currency setting]
            //bool updMerchant = mc.Do_Update_Merchant(MerchantID, TradingName, BusinessRegName, MCCCode, ACP1Name, ACP1Mobile, ACP1Email, CSEmail, CSMobile, BusinessAdd1, BusinessAdd2,
            //            BusinessAdd3, int.Parse(BusinessCountry), BusinessCity, BusinessState, BusinessZipcode, WebSiteURL, int.Parse(PluginID), int.Parse(NotifyEmailFlag), PaymentNotifyEmail,
            //            BusinessRegNo, GSTNo, TypeOfBusiness, OfficeNumber, ACP2Name, ACP2Mobile, ACP2Email, billName, billMobile, billEmail, TypeOfProducts, targetMarkets, DeliveredProduct,
            //            cus.UserID, D_FullName_i, D_ID_i, D_FullName_ii, D_ID_ii, D_FullName_iii, D_ID_iii, D_FullName_iv, D_ID_iv, D_FullName_v, D_ID_v, S_FullName_i, S_ID_i, S_FullName_ii,
            //            S_ID_ii, S_FullName_iii, S_ID_iii, S_FullName_iv, S_ID_iv, S_FullName_v, S_ID_v, BankName, BankAccountName, BankAccountNo, SWIFTCode, BankAddress, BankCountry, TxnLimit, BusinessProfileID);
            bool updMerchant = mc.Do_Update_Merchant(MerchantID, TradingName, BusinessRegName, MCCCode, ACP1Name, ACP1Mobile, ACP1Email, CSEmail, CSMobile, BusinessAdd1, BusinessAdd2,
                        BusinessAdd3, int.Parse(BusinessCountry), BusinessCity, BusinessState, BusinessZipcode, WebSiteURL, int.Parse(PluginID), int.Parse(NotifyEmailFlag), PaymentNotifyEmail,
                        BusinessRegNo, GSTNo, TypeOfBusiness, OfficeNumber, ACP2Name, ACP2Mobile, ACP2Email, billName, billMobile, billEmail, TypeOfProducts, targetMarkets, DeliveredProduct,
                        cus.UserID, D_FullName_i, D_ID_i, D_FullName_ii, D_ID_ii, D_FullName_iii, D_ID_iii, D_FullName_iv, D_ID_iv, D_FullName_v, D_ID_v, S_FullName_i, S_ID_i, S_FullName_ii,
                        S_ID_ii, S_FullName_iii, S_ID_iii, S_FullName_iv, S_ID_iv, S_FullName_v, S_ID_v, BankName, BankAccountName, BankAccountNo, SWIFTCode, BankAddress, BankCountry, TxnLimit, BusinessProfileID,
                        showMerchantAddr, showMerchantLogo, isAgent, defaultCurrency);
            // CHG E N D SUKI 20181128 [Add in showMerchantAddr, showMerchantLogo, IsAgent, default currency setting]
            if (!updMerchant)
            {
                HiddenField1.Value = mc.err_msg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "EditMerchant(Error) - " + logJson + mc.system_err.ToString());
                //ADD START DANIEl 20190107 [Insert into AuditTrail]
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditMerchant), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + mc.system_err.ToString());
                //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
                return;
            }

            bool saveMerchantLogo = SaveMerchantLogo();
            if (saveMerchantLogo == false)
            {
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "EditMerchant(Error) - Save Merchant Logo - " + logJson + error_msg);
                //ADD START DANIEl 20190107 [Insert into AuditTrail]
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditMerchant), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + error_msg);
                //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
            }

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "EditMerchant - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.EditMerchant, 800000, "EditMerchant.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditMerchant), MethodBase.GetCurrentMethod().Name, logJson);
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]

            HiddenField1.Value = "Edit Merchant Successful!";
            HiddenField2.Value = AlertType.Success;
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "EditMerchant(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.EditMerchant, 800001, "EditMerchant.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditMerchant), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }

    //CHG START DANIEL 20190110 [Change method type to string]
    //void GetMerchantInfo(int PDomainID)
    string GetMerchantInfo(int PDomainID)
    //CHG E N D DANIEL 20190110 [Change method type to string]
    {
        var merchant = mc.Do_GetMerchantInfo(PDomainID);
        if (merchant != null)
        {
            #region A) Company Info
            txt_BusinessRegName.Text = merchant.DomainDesc;
            txt_BusinessRegNo.Text = merchant.CompanyRegNo;
            //txt_GSTNo.Text = merchant.GSTNo;

            //switch (merchant.TypeOfBusiness)
            //{
            //    case "Sole Proprietorship":
            //        rd_TypeOfBusiness_SP.Checked = true;
            //        break;
            //    case "Corporation (Sdn Bhd/ Bhd)":
            //        rd_TypeOfBusiness_CO.Checked = true;
            //        break;
            //    case "Partnership":
            //        rd_TypeOfBusiness_PS.Checked = true;
            //        break;
            //    case "Non-Profit Organization":
            //        rd_TypeOfBusiness_NPO.Checked = true;
            //        break;
            //    case "Other":
            //        rd_TypeOfBusiness_Other.Checked = true;
            //        break;
            //    default:
            //        break;
            //}

            txt_BusinessAdd1.Text = merchant.Addr1;
            txt_BusinessAdd2.Text = merchant.Addr2;
            txt_BusinessAdd3.Text = merchant.Addr3;
            txt_BusinessCity.Text = merchant.City;
            DD_BusinesssState.SelectedIndex = DD_BusinesssState.Items.IndexOf(DD_BusinesssState.Items.FindByText(merchant.State));
            txt_BusinessZipcode.Text = merchant.PostCode;
            DD_Country.SelectedIndex = DD_Country.Items.IndexOf(DD_Country.Items.FindByText(merchant.Country));
            txt_OfficeNumber.Text = merchant.OfficeNumber;

            txt_acp1_FullName.Text = merchant.MainContactName;
            txt_acp1_MobileNumber.Text = merchant.MainContactNo;
            txt_acp1_EmailAdd.Text = merchant.MainContactEmail;
            txt_acp2_FullName.Text = merchant.ACPName;
            txt_acp2_MobileNumber.Text = merchant.ACPContactNo;
            txt_acp2_EmailAdd.Text = merchant.ACPEmail;
            txt_bill_FullName.Text = merchant.BillingName;
            txt_bill_MobileNumber.Text = merchant.BillingContactNo;
            txt_bill_EmailAdd.Text = merchant.BillingEmail;
            #endregion

            #region B) List of Company Director / Proprietor
            //var businessOwners = mc.Do_Get_BusinessOwners(merchant.BusinessProfileID);
            //if (businessOwners != null)
            //{
            //    foreach (var owners in businessOwners)
            //    {
            //        if (owners.Type == "Director/ Proprietor")
            //        {
            //            switch (owners.Seq)
            //            {
            //                case 1:
            //                    txt_i_director_name.Text = owners.FullName;
            //                    txt_i_director_id.Text = owners.Desc;
            //                    break;
            //                case 2:
            //                    txt_ii_director_name.Text = owners.FullName;
            //                    txt_ii_director_id.Text = owners.Desc;
            //                    break;
            //                case 3:
            //                    txt_iii_director_name.Text = owners.FullName;
            //                    txt_iii_director_id.Text = owners.Desc;
            //                    break;
            //                case 4:
            //                    txt_iv_director_name.Text = owners.FullName;
            //                    txt_iv_director_id.Text = owners.Desc;
            //                    break;
            //                case 5:
            //                    txt_v_director_name.Text = owners.FullName;
            //                    txt_v_director_id.Text = owners.Desc;
            //                    break;
            //                default:
            //                    break;
            //            }
            //        }

            //        if (owners.Type == "Share Holder")
            //        {
            //            switch (owners.Seq)
            //            {
            //                case 1:
            //                    txt_i_share_name.Text = owners.FullName;
            //                    txt_i_share_id.Text = owners.Desc;
            //                    break;
            //                case 2:
            //                    txt_ii_share_name.Text = owners.FullName;
            //                    txt_ii_share_id.Text = owners.Desc;
            //                    break;
            //                case 3:
            //                    txt_iii_share_name.Text = owners.FullName;
            //                    txt_iii_share_id.Text = owners.Desc;
            //                    break;
            //                case 4:
            //                    txt_iv_share_name.Text = owners.FullName;
            //                    txt_iv_share_id.Text = owners.Desc;
            //                    break;
            //                case 5:
            //                    txt_v_share_name.Text = owners.FullName;
            //                    txt_v_share_id.Text = owners.Desc;
            //                    break;
            //                default:
            //                    break;
            //            }
            //        }
            //    }
            //}
            #endregion

            #region C) Business Profile
            BusinessProfileID = merchant.BusinessProfileID;
            txt_businessName.Text = merchant.Desc;
            txt_onlinestoreURL.Text = merchant.WebSiteURL;
            txt_TypeOfProducts.Text = merchant.TypeOfProducts;

            //if (!string.IsNullOrEmpty(merchant.TargetMarkets))
            //{
            //    var targetMarket = merchant.TargetMarkets.Split(',');
            //    foreach (var target in targetMarket)
            //    {
            //        if (target == "Malaysia") cb_market_Malaysia.Checked = true;
            //        if (target == "Indonesia") cb_market_Indonesia.Checked = true;
            //        if (target == "Thailand") cb_market_Thailand.Checked = true;
            //        if (target == "Singapore") cb_market_Singapore.Checked = true;
            //        if (target == "China") cb_market_China.Checked = true;
            //        if (target == "Philippines") cb_market_Philippines.Checked = true;
            //        if (target == "Other") cb_market_Others.Checked = true;
            //    }
            //}

            switch (merchant.DeliveredPeriod)
            {
                case "Instant":
                    rd_Instant.Checked = true;
                    break;
                case "1-3 days":
                    rd_1_3_days.Checked = true;
                    break;
                case "4-10 days":
                    rd_4_10_days.Checked = true;
                    break;
                case "11-30 days":
                    rd_11_30_days.Checked = true;
                    break;
                case "> 30 days":
                    rd_30_days.Checked = true;
                    break;
                default:
                    break;
            }

            DD_Plugin.SelectedValue = merchant.PluginID.ToString();
            #endregion

            #region D) Merchant Account
            DD_Notify.SelectedIndex = DD_Notify.Items.IndexOf(DD_Notify.Items.FindByText(merchant.NotifEmailRcvDesc));
            txt_IPNEMAIL.Text = merchant.NotifyEmailAddr;

            txt_cs_EmailAddr.Text = merchant.EmailAddr;
            txt_cs_MobileNumber.Text = merchant.ContactNo;
            #endregion

            #region E) Bank Account
            txt_BankName.Text = merchant.BankName;
            txt_BankAccountNo.Text = merchant.BankAccNo;
            txt_BankAccountName.Text = merchant.BankAccHolder;
            txt_SWIFTCode.Text = merchant.SWIFTCode;
            txt_BankAddress.Text = merchant.BankAddress;
            DD_BankCountry.SelectedIndex = DD_BankCountry.Items.IndexOf(DD_BankCountry.Items.FindByText(merchant.BankCountry));
            #endregion

            #region F) Office
            DD_MCCCode.SelectedIndex = DD_MCCCode.Items.IndexOf(DD_MCCCode.Items.FindByText(merchant.MCCCode));
            txt_TransactionLimit.Text = merchant.PerTxnAmtLimit.ToString();
            txt_MerchantID.Text = merchant.MerchantID;
            string path = WebConfigurationManager.AppSettings["LogoPath"];
            path = path + merchant.MerchantID + "-logo.png";
            img_MerchantLogo.Src = path;

            // ADD START SUKI 20181128 [Add in showMerchantAddr, showMerchantLogo, IsAgent, default currency setting]
            if (merchant.ShowMerchantAddr == 1)
                cb_showMerchantAddr.Checked = true;
            else
                cb_showMerchantAddr.Checked = false;

            if (merchant.ShowMerchantLogo == 1)
                cb_showMerchantLogo.Checked = true;
            else
                cb_showMerchantLogo.Checked = false;

            if (merchant.IsAgent == 1)
                cb_Agent.Checked = true;
            else
                cb_Agent.Checked = false;

            DD_Currency.SelectedIndex = DD_Currency.Items.IndexOf(DD_Currency.Items.FindByText(merchant.DefaultCurrencyCode));
            // ADD E N D SUKI 20181128 [Add in showMerchantAddr, showMerchantLogo, IsAgent, default currency setting]
            #endregion
        }
        //ADD START DANIEL 20190110 [If null, return MGMT controller error message]
        else
            return mc.err_msg;

        return string.Empty;
        //ADD START DANIEL 20190110 [If null, return MGMT controller error message]
    }

    private bool SaveMerchantLogo()
    {
        try
        {
            if (Request.Files["file-2"] != null)
            {
                HttpPostedFile MyFile = Request.Files["file-2"];
                if (MyFile.ContentLength > 0)
                {
                    Bitmap bmp = new Bitmap(MyFile.InputStream);
                    Bitmap resized = new Bitmap(bmp, new Size(bmp.Width, bmp.Height));

                    string path = WebConfigurationManager.AppSettings["SaveLogoPath"];
                    if (string.IsNullOrEmpty(path))
                        path = Server.MapPath("~/assets/img/merchantlogo/");

                    path = Path.Combine(path + txt_MerchantID.Text + "-logo.png");
                    resized.Save(path, System.Drawing.Imaging.ImageFormat.Png);
                }
            }
            return true;
        }
        catch (Exception ex)
        {
            error_msg = ex.Message.ToString();
            return false;
        }
    }

}