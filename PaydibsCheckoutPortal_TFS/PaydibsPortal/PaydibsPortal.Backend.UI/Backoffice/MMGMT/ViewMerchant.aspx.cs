﻿using LoggerII;
using Newtonsoft.Json;
using PaydibsPortal.BLL;
using PaydibsPortal.DAL;
using System;
using System.Linq;
using System.Reflection;
using System.Web.Configuration;

public partial class ViewMerchant : System.Web.UI.Page
{
    MMGMTController mc = new MMGMTController();
    HomeController hc = null;
    CurrentUserStat cus = null;
    //ADD START DANIEL 20190215 [Initialize CMSController] 
    CMSController cc = new CMSController();
    //ADD E N D DANIEL 20190215 [Initialize CMSController] 
    protected void Page_Load(object sender, EventArgs e)
    {
        //ADD START DANIEL 20190110 [Initialize HiddenField1 and HiddenField2]
        HiddenField1.Value = "";
        HiddenField2.Value = "";
        //ADD E N D DANIEL 20190110 [Initialize HiddenField1 and HiddenField2]

        if (Session[SessionType.CurrentUser] == null)
        {
            Response.Redirect("../../Default.aspx");
            return;
        }

        cus = (CurrentUserStat)Session[SessionType.CurrentUser];
        hc = new HomeController(cus.DomainID);

        // ADD START SUKI 20190104 [Check permission on every page load]
        var permission = hc.LoadUserPermission(cus.UserID, Convert.ToInt16(OpsIDEnum.ViewMerchant));
        if (permission[0].IsUserAllowed == "0")
        {
            Response.Redirect("../../Backoffice/Dashboard.aspx");
            return;
        }
        // ADD E N D SUKI 20190104 [Check permission on every page load]

        if (!IsPostBack)
        {
            ResetControls();

            var merchant_L = hc.Do_GetMerchantList(1, 0);
            DD_merchant.DataSource = merchant_L;
            DD_merchant.DataTextField = "DomainDesc";
            DD_merchant.DataValueField = "DomainID";
            DD_merchant.DataBind();

            GetMerchantInfo(cus.DomainID);
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ViewMerchant), MethodBase.GetCurrentMethod().Name, "Page_Load Method");
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
    }

    protected void btn_GetInfo_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        var logObject = new
        {
            LoginUser = cus.UserID,
            DomainID = Convert.ToInt32(DD_merchant.SelectedValue)
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            int sDomainID = Convert.ToInt32(DD_merchant.SelectedValue);

            //CHG START DANIEL 20190110 [Insert into AuditTrail]
            //GetMerchantInfo(sDomainID);
            string errMsg = GetMerchantInfo(sDomainID);

            if (!string.IsNullOrEmpty(errMsg))
            {
                HiddenField1.Value = errMsg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            "ViewMerchant(Error) - " + logJson + mc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ViewMerchant), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + mc.system_err.ToString());
                return;
            }
            //CHG E N D DANIEL 20190110 [Insert into AuditTrail]

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "ViewMerchant - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.ViewMerchant, 800000, "ViewMerchant.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ViewMerchant), MethodBase.GetCurrentMethod().Name, logJson);
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "ViewMerchant(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.ViewMerchant, 800001, "ViewMerchant.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ViewMerchant), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }

    //CHG START DANIEL 20190110 [Change method type to string]
    //void GetMerchantInfo(int PDomainID)
    string GetMerchantInfo(int PDomainID)
    //CHG E N D DANIEL 20190110 [Change method type to string]
    {
        var Merchant = mc.Do_GetMerchantInfo(PDomainID);
        if (Merchant != null)
        {
            LB_MerchantID.Text = Merchant.MerchantID;
            LB_MerchantPwd.Text = getMerchantPwd(Merchant.MerchantID);
            if (string.IsNullOrEmpty(LB_MerchantPwd.Text.Trim()))
            {
                //ADD START DANIEL 20190215 [Get usergrouplist. UserGroup under SuperAdmin]
                UserGroup[] getUserGroupList = null;
                if (cus.DomainID == 1)
                    getUserGroupList = cc.Do_GetUserGroup(Convert.ToInt32(DD_merchant.SelectedValue), cus.UserID);
                //ADD E N D DANIEL 20190215 [Get usergrouplist. UserGroup under SuperAdmin]

                LB_Pwd.Visible = false;
                LB_MerchantPwd.Visible = false;

                //CHG START DANIEL 20190215 [If usergroup = "Fraud", then reveal Per Transaction Limit]
                //LB_TrxLimit.Visible = false;
                //LB_Limit.Visible = false;
                if (getUserGroupList != null)
                {
                    if (getUserGroupList.Select(s => s.GroupName.Equals("Fraud")).First())
                    {
                        LB_TrxLimit.Visible = true;
                        LB_Limit.Visible = true;
                        LB_Limit.Text = Merchant.PerTxnAmtLimit.ToString();
                    }
                    else
                    {
                        LB_TrxLimit.Visible = false;
                        LB_Limit.Visible = false;
                    }
                }
                else
                {
                    LB_TrxLimit.Visible = false;
                    LB_Limit.Visible = false;
                }
                //CHG E N D DANIEL 20190215 [If usergroup = "Fraud", then reveal Per Transaction Limit]
            }
            else
            {
                LB_Pwd.Visible = true;
                LB_MerchantPwd.Visible = true;
                LB_TrxLimit.Visible = true;
                LB_Limit.Visible = true;
                LB_Limit.Text = Merchant.PerTxnAmtLimit.ToString();
            }
            LB_DateActivated.Text = Merchant.DateActivated.ToShortDateString();
            LB_CompanyName.Text = Merchant.DomainDesc;
            LB_RegisteredNo.Text = Merchant.CompanyRegNo;
            //LB_GSTNo.Text = Merchant.GSTNo;
            LB_Address1.Text = Merchant.Addr1 + " " + Merchant.Addr2 + " " + Merchant.Addr3;
            LB_Address2.Text = Merchant.PostCode + " " + Merchant.City + " " + Merchant.State + " " + Merchant.Country;
            //LB_ContactNo.Text = Merchant.ContactNo;
            LB_ContactNo.Text = Merchant.OfficeNumber;
            //LB_BusinessType.Text = Merchant.TypeOfBusiness;

            LB_ACP1_Name.Text = Merchant.MainContactName;
            LB_ACP1_Contact.Text = Merchant.MainContactNo;
            LB_ACP1_Email.Text = Merchant.MainContactEmail;
            LB_ACP2_Name.Text = Merchant.ACPName;
            LB_ACP2_Contact.Text = Merchant.ACPContactNo;
            LB_ACP2_Email.Text = Merchant.ACPEmail;
            LB_Bill_Name.Text = Merchant.BillingName;
            LB_Bill_Contact.Text = Merchant.BillingContactNo;
            LB_Bill_Email.Text = Merchant.BillingEmail;

            LB_MCCCode.Text = Merchant.MCCCode;
            string path = WebConfigurationManager.AppSettings["LogoPath"];
            path = path + Merchant.MerchantID + "-logo.png";
            img_MerchantLogo.Src = path;

            //var businessOwners = mc.Do_Get_BusinessOwners(Merchant.BusinessProfileID);
            //if (businessOwners != null)
            //{
            //    foreach (var owners in businessOwners)
            //    {
            //        if (owners.Type == "Director/ Proprietor")
            //        {
            //            switch (owners.Seq)
            //            {
            //                case 1:
            //                    LB_i_director_name.Text = owners.FullName;
            //                    LB_i_director_id.Text = owners.Desc;
            //                    break;
            //                case 2:
            //                    LB_ii_director_name.Text = owners.FullName;
            //                    LB_ii_director_id.Text = owners.Desc;
            //                    break;
            //                case 3:
            //                    LB_iii_director_name.Text = owners.FullName;
            //                    LB_iii_director_id.Text = owners.Desc;
            //                    break;
            //                case 4:
            //                    LB_iv_director_name.Text = owners.FullName;
            //                    LB_iv_director_id.Text = owners.Desc;
            //                    break;
            //                case 5:
            //                    LB_v_director_name.Text = owners.FullName;
            //                    LB_v_director_id.Text = owners.Desc;
            //                    break;
            //                default:
            //                    break;
            //            }
            //        }

            //        if (owners.Type == "Share Holder")
            //        {
            //            switch (owners.Seq)
            //            {
            //                case 1:
            //                    LB_i_shareholder_name.Text = owners.FullName;
            //                    LB_i_shareholder_id.Text = owners.Desc;
            //                    break;
            //                case 2:
            //                    LB_ii_shareholder_name.Text = owners.FullName;
            //                    LB_ii_shareholder_id.Text = owners.Desc;
            //                    break;
            //                case 3:
            //                    LB_iii_shareholder_name.Text = owners.FullName;
            //                    LB_iii_shareholder_id.Text = owners.Desc;
            //                    break;
            //                case 4:
            //                    LB_iv_shareholder_name.Text = owners.FullName;
            //                    LB_iv_shareholder_id.Text = owners.Desc;
            //                    break;
            //                case 5:
            //                    LB_v_shareholder_name.Text = owners.FullName;
            //                    LB_v_shareholder_id.Text = owners.Desc;
            //                    break;
            //                default:
            //                    break;
            //            }
            //        }
            //    }
            //}

            LB_TradingName.Text = Merchant.Desc;
            LB_WebsiteURL.Text = Merchant.WebSiteURL;
            LB_ProductType.Text = Merchant.TypeOfProducts;
            //LB_TargetMarket.Text = Merchant.TargetMarkets;
            LB_DeliveryPeriod.Text = Merchant.DeliveredPeriod;
            LB_Plugin.Text = Merchant.PluginName;

            LB_NotifyEmailFlag.Text = Merchant.NotifEmailRcvDesc;
            LB_NotifyEmail.Text = Merchant.NotifyEmailAddr;
            LB_CS_Contact.Text = Merchant.ContactNo;
            LB_CS_Email.Text = Merchant.EmailAddr;

            LB_BankName.Text = Merchant.BankName;
            LB_AccountNo.Text = Merchant.BankAccNo;
            LB_AccountName.Text = Merchant.BankAccHolder;
            LB_SwiftCode.Text = Merchant.SWIFTCode;
            LB_BankAddress.Text = Merchant.BankAddress;
            LB_BankCountry.Text = Merchant.BankCountry;
        }
        else
        {
            ResetControls();
            //ADD START DANIEL 20190110 [If null, return MGMT controller error message]
            return mc.err_msg;
            //ADD E N D DANIEL 20190110 [If null, return MGMT controller error message]
        }
        //ADD START DANIEL 20190110 [Add return string]
        return string.Empty;
        //ADD E N D DANIEL 20190110 [Add return string]
    }

    string getMerchantPwd(string PMerchantID)
    {
        if (cus.DomainID == 1)
            return mc.Do_GetMerchantPwd(cus.UserID, PMerchantID);
        else
            return string.Empty;
    }

    void ResetControls()
    {
        LB_MerchantID.Text = "[--]";
        LB_MerchantPwd.Text = "[--]";
        LB_Pwd.Visible = false;
        LB_MerchantPwd.Visible = false;
        LB_DateActivated.Text = "[--]";
        LB_CompanyName.Text = "[--]";
        LB_RegisteredNo.Text = "[--]";
        //LB_GSTNo.Text = "[--]";
        LB_Address1.Text = "[--]";
        LB_Address2.Text = "[--]";
        LB_ContactNo.Text = "[--]";
        //LB_BusinessType.Text = "[--]";

        LB_ACP1_Name.Text = "[--]";
        LB_ACP1_Contact.Text = "[--]";
        LB_ACP1_Email.Text = "[--]";
        LB_ACP2_Name.Text = "[--]";
        LB_ACP2_Contact.Text = "[--]";
        LB_ACP2_Email.Text = "[--]";
        LB_Bill_Name.Text = "[--]";
        LB_Bill_Contact.Text = "[--]";
        LB_Bill_Email.Text = "[--]";

        LB_MCCCode.Text = "[--]";
        img_MerchantLogo.Src = "~/assets/img/120x270.png";

        //LB_i_director_name.Text = "[--]";
        //LB_i_director_id.Text = "[--]"; ;
        //LB_ii_director_name.Text = "[--]";
        //LB_ii_director_id.Text = "[--]";
        //LB_iii_director_name.Text = "[--]";
        //LB_iii_director_id.Text = "[--]";
        //LB_iv_director_name.Text = "[--]";
        //LB_iv_director_id.Text = "[--]";
        //LB_v_director_name.Text = "[--]";
        //LB_v_director_id.Text = "[--]";

        //LB_i_shareholder_name.Text = "[--]";
        //LB_i_shareholder_id.Text = "[--]";
        //LB_ii_shareholder_name.Text = "[--]";
        //LB_ii_shareholder_id.Text = "[--]";
        //LB_iii_shareholder_name.Text = "[--]";
        //LB_iii_shareholder_id.Text = "[--]";
        //LB_iv_shareholder_name.Text = "[--]";
        //LB_iv_shareholder_id.Text = "[--]";
        //LB_v_shareholder_name.Text = "[--]";
        //LB_v_shareholder_id.Text = "[--]";

        LB_TradingName.Text = "[--]";
        LB_WebsiteURL.Text = "[--]";
        LB_ProductType.Text = "[--]";
        //LB_TargetMarket.Text = "[--]";
        LB_DeliveryPeriod.Text = "[--]";
        LB_Plugin.Text = "[--]";

        LB_NotifyEmailFlag.Text = "[--]";
        LB_NotifyEmail.Text = "[--]";
        LB_CS_Contact.Text = "[--]";
        LB_CS_Email.Text = "[--]";

        LB_BankName.Text = "[--]";
        LB_AccountNo.Text = "[--]";
        LB_AccountName.Text = "[--]";
        LB_SwiftCode.Text = "[--]";
        LB_BankAddress.Text = "[--]";
        LB_BankCountry.Text = "[--]";
        LB_Limit.Text = "[--]";
    }
}