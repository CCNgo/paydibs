﻿using LoggerII;
using Newtonsoft.Json;
using PaydibsPortal.BLL;
using PaydibsPortal.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.UI.WebControls;

public partial class Backoffice_MMGMT_AddMerchantRate : System.Web.UI.Page
{
    MMGMTController mc = new MMGMTController();
    HomeController hc = null;
    CurrentUserStat cus = null;
    string error_msg = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        HiddenField1.Value = "";
        HiddenField2.Value = "";

        if (Session[SessionType.CurrentUser] == null)
        {
            //CHG START DANIEL 20190218 [Redirect to Default]
            //Response.Redirect("../");
            Response.Redirect("../../Default.aspx");
            //CHG E N D DANIEL 20190218 [Redirect to Default]
            return;
        }

        cus = (CurrentUserStat)Session[SessionType.CurrentUser];
        hc = new HomeController(cus.DomainID);

        // ADD START SUKI 20190104 [Check permission on every page load]
        var permission = hc.LoadUserPermission(cus.UserID, Convert.ToInt16(OpsIDEnum.AddMerchantRate));
        if (permission[0].IsUserAllowed == "0")
        {
            Response.Redirect("../../Backoffice/Dashboard.aspx");
            return;
        }
        // ADD E N D SUKI 20190104 [Check permission on every page load]

        if (!IsPostBack)
        {
            var merchant_L = hc.Do_GetMerchantList(1, 0);
            DD_merchant.DataSource = merchant_L;
            DD_merchant.DataTextField = "DomainDesc";
            DD_merchant.DataValueField = "DomainID";
            DD_merchant.DataBind();

            DropDownList1.DataSource = merchant_L;
            DropDownList1.DataTextField = "DomainDesc";
            DropDownList1.DataValueField = "DomainID";
            DropDownList1.DataBind();

            //if (Request.QueryString["Merchant"])
            if (!string.IsNullOrEmpty(Request.QueryString["Merchant"]))
            {
                DD_merchant.SelectedValue = Request.QueryString["Merchant"];
            }

            //DEL START DANIEL 20181203 [Removed because only need to get the data when button Get Info clicked]
            //var pymtMethodList = mc.Do_GetCLPymtMethod();
            //DD_PymtMethod.DataSource = pymtMethodList;
            //DD_PymtMethod.DataTextField = "PymtMethodDesc";
            //DD_PymtMethod.DataValueField = "PymtMethodCode";
            //DD_PymtMethod.DataBind();

            //var channelList = mc.Do_Get_TerminalHost(Convert.ToInt32(DD_merchant.SelectedValue));
            //DD_Channel.DataSource = channelList;
            //DD_Channel.DataTextField = "HostDesc";
            //DD_Channel.DataValueField = "HostID";
            //DD_Channel.DataBind();
            //DEL E N D DANIEL 20181203 [Removed because only need to get the data when button Get Info clicked]

            //DEL START KENT LOONG 20190218 [REMOVE unnecessary code]
            var currencyList = mc.Do_GetCurrencyCountry(1);
            DD_Currency.DataSource = currencyList;
            DD_Currency.DataTextField = "CurrCode";
            DD_Currency.DataValueField = "CurrCode";
            DD_Currency.DataBind();
            //ADD START DANIEL 20181130[Set default currency to MYR]
            DD_Currency.SelectedValue = "MYR";
            //ADD E N D DANIEL 20181130[Set default currency to MYR]

            //GenerateMerchantRate(Convert.ToInt32(DD_merchant.SelectedValue));
            //DEL START KENT LOONG 20190218 [REMOVE unnecessary code]
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddMerchantRate), MethodBase.GetCurrentMethod().Name, "Page_Load Method");
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]

        }
    }

    protected void btn_GetInfo_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        int sDomainID = Convert.ToInt32(DropDownList1.SelectedValue);

        clearInputField();
        DD_PymtMethod.Enabled = true;

        var logObject = new
        {
            LoginUser = cus.UserID,
            DomainID = sDomainID
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            var channelList = mc.Do_Get_TerminalHost(sDomainID);

            if (channelList == null)
            {
                HiddenField1.Value = mc.err_msg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "AddMerchantRate(Error) - " + logJson + mc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddMerchantRate), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + mc.system_err.ToString());
                return;
            }

            var channelGroup = channelList.GroupBy(c => c.TypeValue)
             .Select(g => g.First());

            DD_PymtMethod.DataSource = channelGroup;
            DD_PymtMethod.DataTextField = "Type";
            DD_PymtMethod.DataValueField = "TypeValue";
            DD_PymtMethod.DataBind();
            DD_PymtMethod.Items.Insert(0, new ListItem("--SELECT--", ""));

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                          "AddMerchantRate - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.AddMerchantRate, 800000, "AddMerchantRate.aspx");
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddMerchantRate), MethodBase.GetCurrentMethod().Name, logJson);
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "AddMerchantRate(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.AddMerchantRate, 800001, "AddMerchantRate.aspx");
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddMerchantRate), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }
    //ADD START KENT LOONG 20190220 [BUTTON ON CLICK ADD NEW RATE]
    protected void btn_Add_New_Rate(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();
        int sDomainID = Convert.ToInt32(DD_merchant.SelectedValue);
        bool addRate = false;
        object logJson = null;
        decimal fromAmt = 0;
        decimal toAmt = 0;
        DateTime startDate = DateTime.Now;

        try
        {
            objLoggerII = logger.InitLogger();
            var HostList = mc.Do_Get_TerminalHost(sDomainID);
            if (HostList == null)
            {
                HiddenField1.Value = mc.err_msg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "AddMerchantRate(Error) - " + logJson + mc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddMerchantRate), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + mc.system_err.ToString());
                return;
            }

            var RateList = mc.Do_Get_MerchantRateHost(sDomainID);
            if (RateList == null)
            {
                HiddenField1.Value = mc.err_msg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "AddMerchantRate(Error) - " + logJson + mc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddMerchantRate), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + mc.system_err.ToString());
                return;
            }

            List<ChannelInfo> channelList = new List<ChannelInfo>();
            foreach (var host in HostList)
            {
                ChannelInfo channelObject = new ChannelInfo();
                channelObject.CurrencyCode = host.CurrencyCode;
                channelObject.PaymentMethod = host.TypeValue;
                channelObject.HostId = host.HostID;
                channelObject.HostDesc = host.HostDesc;
                channelObject.Type = host.Type;
                channelObject.Rate = string.Empty;
                channelObject.FixedFee = string.Empty;
                foreach (var rate in RateList)
                {
                    if (host.HostDesc.Trim() == rate.HostName.Trim() && host.Type.Trim() == rate.PymtMethod.Trim() && host.CurrencyCode.Trim() == rate.Currency.Trim())
                    {
                        channelObject.Rate = (rate.Rate).ToString();
                        channelObject.FixedFee = (rate.FixedFee).ToString();
                    }
                }
                channelList.Add(channelObject);
            }

            List<ChannelInfo> channelObjList = (List<ChannelInfo>)Newtonsoft.Json.JsonConvert.DeserializeObject(TextBox1.Text, typeof(List<ChannelInfo>));
            bool addMerchantRate = false;
            foreach (ChannelInfo channel in channelObjList)
            {
                foreach (var compare in channelList)
                {
                    if (channel.PaymentMethod == compare.PaymentMethod && channel.HostId == compare.HostId && channel.CurrencyCode == compare.CurrencyCode)
                    {
                        if (Convert.ToDecimal(channel.FixedFee).ToString("0.0000") != compare.FixedFee || ((Convert.ToDecimal(channel.Rate)) / 100).ToString("0.0000") != compare.Rate)
                        {
                            var logObject = new
                            {
                                FromAmt = fromAmt,
                                ToAmt = toAmt,
                                PaymentMethod = channel.PaymentMethod,
                                Channel = channel.HostId,
                                StartDate = startDate,
                                Rate = Convert.ToDecimal(channel.Rate),
                                FixedRate = Convert.ToDecimal(channel.FixedFee),
                                Currency = channel.CurrencyCode
                            };
                            logJson = JsonConvert.SerializeObject(logObject);

                            addMerchantRate = mc.Insert_MerchantRate(fromAmt, toAmt, sDomainID.ToString(), channel.PaymentMethod, channel.HostId, startDate, Convert.ToDecimal(channel.Rate), Convert.ToDecimal(channel.FixedFee), channel.CurrencyCode);

                            if (!addMerchantRate)
                            {
                                HiddenField1.Value = mc.err_msg;
                                objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                           "AddMerchantRate(Error) - " + logJson + mc.system_err.ToString());
                                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddMerchantRate), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + mc.system_err.ToString());
                                return;
                            }
                            addRate = true;
                            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "AddMerchantRate - " + logJson);
                            hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.AddMerchantRate, 800000, "AddMerchantRate.aspx");
                            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddMerchantRate), MethodBase.GetCurrentMethod().Name, logJson.ToString());
                        }
                    }
                }
            }
            if (addRate)
            {
                HiddenField1.Value = "Add Merchant Rate Successful!";
                HiddenField2.Value = AlertType.Success;
            }
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                "AddMerchantRate(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.AddMerchantRate, 800001, "AddMerchantRate.aspx");
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddMerchantRate), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
        }
        finally
        {
            objLoggerII.Dispose();
        }

    }
    //ADD E N D KENT LOONG 20190220 [BUTTON ON CLICK ADD NEW RATE]
    //ADD START KENT LOONG 20190221 [BUTTON ON CLICK ADD FUTURE RATE]
    protected void btn_Add_Future_Rate(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();
        int sDomainID = Convert.ToInt32(DD_merchant.SelectedValue);
        bool addRate = false;
        object logJson = null;

        try
        {
            objLoggerII = logger.InitLogger();
            List<ChannelInfo> channelObjList = (List<ChannelInfo>)Newtonsoft.Json.JsonConvert.DeserializeObject(TextBox2.Text, typeof(List<ChannelInfo>));

            if (channelObjList.Count() <= 0)
            {
                var logObject = new
                {
                    channelObjList = "channelObjList is empty."
                };
                logJson = JsonConvert.SerializeObject(logObject);
                HiddenField1.Value = "channelObjList is empty";
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "AddMerchantRate(Error) - " + logJson + mc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddMerchantRate), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + mc.system_err.ToString());
                return;
            }
            bool addMerchantRate = false;
            decimal fromAmt = 0;
            decimal toAmt = 0;

            foreach (ChannelInfo channel in channelObjList)
            {
                DateTime startDate = DateTime.Parse(channel.StartDate);
                var logObject = new
                {
                    FromAmt = fromAmt,
                    ToAmt = toAmt,
                    PaymentMethod = channel.PaymentMethod,
                    Channel = channel.HostId,
                    StartDate = startDate,
                    Rate = Convert.ToDecimal(channel.Rate),
                    FixedRate = Convert.ToDecimal(channel.FixedFee),
                    Currency = channel.CurrencyCode
                };
                logJson = JsonConvert.SerializeObject(logObject);

                addMerchantRate = mc.Insert_MerchantRate(fromAmt, toAmt, sDomainID.ToString(), channel.PaymentMethod, channel.HostId, startDate, Convert.ToDecimal(channel.Rate), Convert.ToDecimal(channel.FixedFee), channel.CurrencyCode);

                if (!addMerchantRate)
                {
                    HiddenField1.Value = mc.err_msg;
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "AddMerchantRate(Error) - " + logJson + mc.system_err.ToString());
                    hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddMerchantRate), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + mc.system_err.ToString());
                    return;
                }
                addRate = true;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "AddMerchantRate - " + logJson);
                hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.AddMerchantRate, 800000, "AddMerchantRate.aspx");
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddMerchantRate), MethodBase.GetCurrentMethod().Name, logJson.ToString());

            }


            if (addRate)
            {
                HiddenField1.Value = "Successful Add New Effective Rate!";
                HiddenField2.Value = AlertType.Success;
            }
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                "AddMerchantRate(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.AddMerchantRate, 800001, "AddMerchantRate.aspx");
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddMerchantRate), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }
    //ADD START KENT LOONG 20190221 [BUTTON ON CLICK ADD FUTURE RATE]

    //DEL START KENT LOONG 20190218 [Remove unnecessary code]
    //protected void btn_GetInfo_Click(object sender, EventArgs e)
    //{
    //    Logger logger = new Logger();
    //    CLoggerII objLoggerII = new CLoggerII();

    //    int sDomainID = Convert.ToInt32(DD_merchant.SelectedValue);

    //    //ADD START DANIEL 20181205 [Call method to clear all input fields]
    //    clearInputField();
    //    //ADD E N D DANIEL 20181205 [Call method to clear all input fields]

    //    //DEL START DANIEL 20181205 [Remove all for a better alternative to clear input field]
    //    //ADD START DANIEL 20181130 [Clear the channel dropdown item whenever "Get Info" button clicked]
    //    //DD_PymtMethod.Items.Clear();
    //    //DD_Channel.Items.Clear();
    //    //ADD E N D DANIEL 20181130 [Clear the channel dropdown item whenever "Get Info" button clicked]

    //    //ADD START DANIEL 20181205 [Clear textbox for rate and fixed rate]
    //    //txt_Rate.Text = string.Empty;
    //    //txt_FixedRate.Text = string.Empty;
    //    //ADD E N D DANIEL 20181205 [Clear textbox for rate and fixed rate]
    //    //DEL E N D DANIEL 20181205 [Remove all for a better alternative to clear input field]

    //    //DEL START KENT LOONG 20190218 [Remove unnecessary code]
    //    //ADD START DANIEL 20190116 [Enable DDL Payment method]
    //    //DD_PymtMethod.Enabled = true;
    //    //ADD E N D DANIEL 20190116 [Enable DDL Payment method]
    //    //DEL E N D KENT LOONG 20190218 [Remove unnecessary code]
    //    var logObject = new
    //    {
    //        LoginUser = cus.UserID,
    //        DomainID = sDomainID
    //    };
    //    var logJson = JsonConvert.SerializeObject(logObject);

    //    try
    //    {
    //        objLoggerII = logger.InitLogger();

    //        //CHG START DANIEL 20190110 [Insert into AuditTrail]
    //        //GenerateMerchantRate(sDomainID);
    //        string errMsg = GenerateMerchantRate(sDomainID);
    //        if (!string.IsNullOrEmpty(errMsg))
    //        {
    //            HiddenField1.Value = errMsg;
    //            objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
    //                       "AddMerchantRate(Error) - " + logJson + mc.system_err.ToString());
    //            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddMerchantRate), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + mc.system_err.ToString());
    //            return;
    //        }
    //        //CHG E N D DANIEL 20190110 [Insert into AuditTrail]

    //        //CHG START DANIEL 20181130 [Change DataTextField and DataValueField for channel]
    //        //var channelList = mc.Do_Get_TerminalHost(sDomainID);
    //        //DD_Channel.DataSource = channelList;
    //        //DD_Channel.DataTextField = "HostDesc";
    //        //DD_Channel.DataValueField = "HostID";
    //        //DD_Channel.DataBind();

    //        //CHG START DANIEL 20181205 [Pass 1 for parameter PMode]
    //        //var channelList = mc.Do_Get_TerminalHost(sDomainID);
    //        //CHG START DANIEL 20181206 [Remove PMode parameter 1]
    //        //var channelList = mc.Do_Get_TerminalHost(sDomainID, 1);
    //        var channelList = mc.Do_Get_TerminalHost(sDomainID);
    //        //CHG E N D DANIEL 20181206 [Remove PMode parameter 1]
    //        //CHG E N D DANIEL 20181205 [Pass 1 for parameter PMode]

    //        //ADD START DANIEL 20190110 [Insert into AuditTrail]
    //        if (channelList == null)
    //        {
    //            HiddenField1.Value = mc.err_msg;
    //            objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
    //                       "AddMerchantRate(Error) - " + logJson + mc.system_err.ToString());
    //            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddMerchantRate), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + mc.system_err.ToString());
    //            return;
    //        }
    //        //ADD E N D DANIEL 20190110 [Insert into AuditTrail]

    //        //ADD START DANIEL 20181206 [Add linq statement to select distinct result of duplicate values]
    //        var channelGroup = channelList.GroupBy(c => c.TypeValue)
    //         .Select(g => g.First());
    //        //ADD E N D DANIEL 20181206 [Add linq statement to select distinct result of duplicate values]

    //        DD_PymtMethod.DataSource = channelGroup;
    //        DD_PymtMethod.DataTextField = "Type";
    //        DD_PymtMethod.DataValueField = "TypeValue";
    //        DD_PymtMethod.DataBind();

    //        //ADD START DANIEL 20190116 [Add new listitem --SELECT--]
    //        DD_PymtMethod.Items.Insert(0, new ListItem("--SELECT--", ""));
    //        //ADD E N D DANIEL 20190116 [Add new listitem --SELECT--]

    //        //CHG E N D DANIEL 20181130 [Change DataTextField and DataValueField for channel]

    //        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
    //                      "AddMerchantRate - " + logJson);
    //        hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.AddMerchantRate, 800000, "AddMerchantRate.aspx");
    //        //ADD START DANIEl 20190107 [Insert into AuditTrail]
    //        hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddMerchantRate), MethodBase.GetCurrentMethod().Name, logJson);
    //        //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
    //    }
    //    catch (Exception ex)
    //    {
    //        objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
    //                           "AddMerchantRate(Error) - " + logJson + ex.ToString());
    //        hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.AddMerchantRate, 800001, "AddMerchantRate.aspx");
    //        //ADD START DANIEl 20190107 [Insert into AuditTrail]
    //        hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddMerchantRate), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
    //        //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
    //    }
    //    finally
    //    {
    //        objLoggerII.Dispose();
    //    }
    //}
    //DEL E N D KENT LOONG 20190218 [Remove unnecessary code]

    //DEL START KENT LOONG 20190218 [Remove unnecessary code]
    //ADD START DANIEL 20190116 [Add OnSelectedIndexChanged Method]
    protected void OnChanged_DD_PymtMethod(object sender, EventArgs e)
    {
        var control = (DropDownList)sender;
        var channelList = mc.Do_Get_TerminalHost(Convert.ToInt32(DropDownList1.SelectedValue), DD_PymtMethod.SelectedValue);

        if (control.SelectedIndex > 0)
        {
            DD_Channel.Enabled = true;
            btn_AddRate.Enabled = true;
            btn_AddRate.Enabled = false;
            txt_Rate.Text = string.Empty;
            txt_FixedRate.Text = string.Empty;

            DD_Channel.DataSource = channelList;
            DD_Channel.DataTextField = "HostDesc";
            DD_Channel.DataValueField = "HostID";
            DD_Channel.DataBind();
            DD_Channel.Items.Insert(0, new ListItem("--SELECT--", ""));
        }
        else
        {
            btn_AddRate.Enabled = false;
            DD_Channel.Enabled = false;
            DD_Channel.Items.Clear();
            txt_Rate.Text = string.Empty;
            txt_FixedRate.Text = string.Empty;
        }
    }
    //ADD E N D DANIEL 20190116 [Add OnSelectedIndexChanged Method]
    //DEL E N D KENT LOONG 20190218 [Remove unnecessary code]

    //DEL START KENT LOONG 20190218 [Remove unnecessary code]
    //ADD START DANIEL 20190116 [Add OnSelectedIndexChanged Method]
    protected void OnChanged_DD_Channel(object sender, EventArgs e)
    {
        var control = (DropDownList)sender;

        if (control.SelectedIndex > 0)
        {
            btn_AddRate.Enabled = true;
            txt_Rate.Text = string.Empty;
            txt_FixedRate.Text = string.Empty;
        }
        else
        {
            btn_AddRate.Enabled = false;
            txt_Rate.Text = string.Empty;
            txt_FixedRate.Text = string.Empty;
        }
    }
    //ADD E N D DANIEL 20190116 [Add OnSelectedIndexChanged Method]
    //DEL E N D KENT LOONG 20190218 [Remove unnecessary code]

    //DEL START DANIEL 20190116 [Remove btn_GetChannel_Click method]
    //protected void btn_GetChannel_Click(object sender, EventArgs e)
    //{
    //    //CHG START DANIEL 20190108 [Insert into log , accesslog and audit trail]

    //    //CHG START DANIEL 20181130 [Get the channel only when Get button click]
    //    //var channelList = mc.Do_Get_HostByType(DD_PymtMethod.SelectedValue);
    //    //DD_Channel.DataSource = channelList;
    //    //DD_Channel.DataTextField = "HostDesc";
    //    //DD_Channel.DataValueField = "HostID";
    //    //DD_Channel.DataBind();
    //    //int sDomainID = Convert.ToInt32(DD_merchant.SelectedValue);
    //    //CHG START DANIEL 20181205 [Change method to Do_Get_TerminalHost]
    //    //var channelList = mc.Do_Get_HostByType_DomainID(DD_PymtMethod.SelectedValue, sDomainID);
    //    //CHG START DANIEL 20181206 [Remove parameter PMode 0]
    //    //var channelList = mc.Do_Get_TerminalHost(sDomainID, 0, DD_PymtMethod.SelectedValue);
    //    //var channelList = mc.Do_Get_TerminalHost(sDomainID, DD_PymtMethod.SelectedValue);
    //    //CHG E N D DANIEL 20181206 [Remove parameter PMode 0]
    //    //CHG E N D DANIEL 20181205 [Change method to Do_Get_TerminalHost]
    //    //DD_Channel.DataSource = channelList;
    //    //DD_Channel.DataTextField = "HostDesc";
    //    //DD_Channel.DataValueField = "HostID";
    //    //DD_Channel.DataBind();
    //    //CHG E N D DANIEL 20181130 [Get the channel only when Get button click]
    //    //ADD START DANIEL 20181205 [Clear textbox for rate and fixed rate]
    //    //txt_Rate.Text = string.Empty;
    //    //txt_FixedRate.Text = string.Empty;
    //    //ADD E N D DANIEL 20181205 [Clear textbox for rate and fixed rate]

    //    Logger logger = new Logger();
    //    CLoggerII objLoggerII = new CLoggerII();
    //    int sDomainID = Convert.ToInt32(DD_merchant.SelectedValue);

    //    var logObject = new
    //    {
    //        LoginUser = cus.UserID,
    //        DomainID = sDomainID
    //    };
    //    var logJson = JsonConvert.SerializeObject(logObject);

    //    try
    //    {
    //        objLoggerII = logger.InitLogger();
    //        var channelList = mc.Do_Get_TerminalHost(sDomainID, DD_PymtMethod.SelectedValue);

    //        //ADD START DANIEL 20190110 [Insert into AuditTrail]
    //        if (channelList == null)
    //        {
    //            HiddenField1.Value = mc.err_msg;
    //            objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
    //                       "AddMerchantRate(Error) - " + logJson + mc.system_err.ToString());
    //            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddMerchantRate), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + mc.system_err.ToString());
    //            return;
    //        }
    //        //ADD E N D DANIEL 20190110 [Insert into AuditTrail]

    //        DD_Channel.DataSource = channelList;
    //        DD_Channel.DataTextField = "HostDesc";
    //        DD_Channel.DataValueField = "HostID";
    //        DD_Channel.DataBind();
    //        txt_Rate.Text = string.Empty;
    //        txt_FixedRate.Text = string.Empty;

    //        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
    //                  "AddMerchantRate - " + logJson);
    //        hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.AddMerchantRate, 800000, "AddMerchantRate.aspx");
    //        hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddMerchantRate), MethodBase.GetCurrentMethod().Name, logJson);
    //    }
    //    catch (Exception ex)
    //    {
    //        objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
    //                            "AddMerchantRate(Error) - " + logJson + ex.ToString());
    //        hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.AddMerchantRate, 800001, "AddMerchantRate.aspx");
    //        hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddMerchantRate), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
    //    }
    //    finally
    //    {
    //        objLoggerII.Dispose();
    //    }

    //    //CHG START DANIEL 20190108 [Insert into log , accesslog and audit trail]
    //}
    //DEL E N D DANIEL 20190116 [Remove btn_GetChannel_Click method]

    //DEL START KENT LOONG 20190218 [Remove btn_AddRate_Click method]
    protected void btn_AddRate_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        decimal fromAmt = 0;
        decimal toAmt = 0;
        string merchantID = DD_merchant.SelectedItem.Value;
        string paymentMethod = DD_PymtMethod.SelectedValue;

        //CHG START DANIEL 20181204 [Convert string to decimal and only allow number input. Better improve validations]

        //int channel = Convert.ToInt32(DD_Channel.SelectedValue);
        //DateTime startDate = DateTime.Now;

        //CHG START DANIEL 20181130 [Enforce validation for Rate and Fixed Rate field]
        //decimal rate = Convert.ToDecimal(txt_Rate.Text.Trim());
        //decimal fixedRate = Convert.ToDecimal(txt_FixedRate.Text.Trim());
        //decimal rate = Convert.ToDecimal(string.IsNullOrEmpty(txt_Rate.Text.Trim()) ? "0" : txt_Rate.Text.Trim());
        //decimal fixedRate = Convert.ToDecimal(string.IsNullOrEmpty(txt_FixedRate.Text.Trim()) ? "0" : txt_FixedRate.Text.Trim());
        //CHG E N D DANIEL 20181130 [Enforce validation for Rate and Fixed Rate field]

        int channel = !string.IsNullOrEmpty(DD_Channel.SelectedValue) ? Convert.ToInt32(DD_Channel.SelectedValue) : 0;
        DateTime startDate = DateTime.Now;
        double num;
        string inputRate = txt_Rate.Text.Trim();
        string inputFixedRate = txt_FixedRate.Text.Trim();
        decimal rate = 0;
        decimal fixedRate = 0;

        if (double.TryParse(inputRate, out num))
        {
            rate = Convert.ToDecimal(inputRate);
        }
        //ADD START DANIEL 20190109 [If not integer or decimal, prompt error message]
        else
        {
            HiddenField1.Value = "Please enter integer or decimal only.";
            return;
        }
        //ADD E N D DANIEL 20190109 [If not integer or decimal, prompt error message]


        if (double.TryParse(inputFixedRate, out num))
        {
            fixedRate = Convert.ToDecimal(inputFixedRate);
        }
        //ADD START DANIEL 20190109 [If not integer or decimal, prompt error message]
        else
        {
            HiddenField1.Value = "Please enter integer or decimal only";
            return;
        }
        //ADD E N D DANIEL 20190109 [If not integer or decimal, prompt error message]

        //CHG E N D DANIEL 20181204 [Convert string to decimal and only allow number input. Better improve validations]

        string currency = DD_Currency.SelectedValue;

        var logObject = new
        {
            FromAmt = fromAmt,
            ToAmt = toAmt,
            PaymentMethod = paymentMethod,
            Channel = channel,
            StartDate = startDate,
            Rate = rate,
            FixedRate = fixedRate,
            Currency = currency
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            //CHG START DANIEL 20181217 [If rate and fixedrate empty , return both 0. Which will prompt error]
            //bool addMerchantRate = mc.Insert_MerchantRate(fromAmt, toAmt, merchantID, paymentMethod, channel, startDate, rate, fixedRate, currency);
            bool addMerchantRate = false;
            //////if (!string.IsNullOrEmpty(inputRate) && !string.IsNullOrEmpty(inputFixedRate))
            //////{
            //////    addMerchantRate = mc.Insert_MerchantRate(fromAmt, toAmt, merchantID, paymentMethod, channel, startDate, rate, fixedRate, currency);
            //////}
            //////else
            //////{
            //////    addMerchantRate = mc.Insert_MerchantRate(fromAmt, toAmt, merchantID, paymentMethod, channel, startDate, 0, 0, currency);
            //////}
            //CHG E N D DANIEL 20181217 [If rate and fixedrate empty , return both 0. Which will prompt error]

            if (!addMerchantRate)
            {
                HiddenField1.Value = mc.err_msg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "AddMerchantRate(Error) - " + logJson + mc.system_err.ToString());
                //ADD START DANIEl 20190107 [Insert into AuditTrail]
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddMerchantRate), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + mc.system_err.ToString());
                //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
                return;
            }

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                              "AddMerchantRate - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.AddMerchantRate, 800000, "AddMerchantRate.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddMerchantRate), MethodBase.GetCurrentMethod().Name, logJson);
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]

            //CHG START DANIEL 20190110 [Insert into AuditTrail]
            //GenerateMerchantRate(Convert.ToInt32(DD_merchant.SelectedValue));
            //string errMsg = GenerateMerchantRate(Convert.ToInt32(DD_merchant.SelectedValue));
            //if (!string.IsNullOrEmpty(errMsg))
            //{
            //    HiddenField1.Value = errMsg;
            //    objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
            //               "AddMerchantRate(Error) - " + logJson + mc.system_err.ToString());
            //    hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddMerchantRate), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + mc.system_err.ToString());
            //    return;
            //}
            //CHG E N D DANIEL 20190110 [Insert into AuditTrail]
            HiddenField1.Value = "Add Merchant Rate Successful!";
            HiddenField2.Value = AlertType.Success;
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                "AddMerchantRate(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.AddMerchantRate, 800001, "AddMerchantRate.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddMerchantRate), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        finally
        {
            objLoggerII.Dispose();
        }
        //ADD START DANIEL 20181205 [Call method to clear all input fields.]
        clearInputField();
        //ADD E N D DANIEL 20181205 [Call method to clear all input fields.]
    }
    //DEL END KENT LOONG 20190218 [Remove btn_AddRate_Click method]

    //DEL START KENT LOONG 20190218 [Remove unnecessary code]
    //ADD START DANIEL 20181205 [Create new method to clear all input fields]
    void clearInputField()
    {
        DD_Channel.Items.Clear();
        txt_Rate.Text = string.Empty;
        txt_FixedRate.Text = string.Empty;

        //ADD START DANIEL 20190116 [Clear Field]
        DD_Channel.Enabled = false;
        btn_AddRate.Enabled = false;
        DD_PymtMethod.ClearSelection();
        //ADD E N D DANIEL 20190116 [Clear Field]
    }
    //ADD E N D DANIEL 20181205 [Create new method to clear all input fields]
    //DEL E N D KENT LOONG 20190218 [Remove unnecessary code]

    //DEL START KENT LOONG 20190218 [Remove unnecessary code]
    //CHG START DANIEL 20190110 [Change method type to string]
    //void GenerateMerchantRate(int PDomainID)
    //string GenerateMerchantRate(int PDomainID)
    ////CHG E N D DANIEL 20190110 [Change method type to string]
    //{
    //    var RateData = mc.Do_Get_MerchantRateHost(PDomainID);
    //    //ADD START DANIEL 20190110 [If null, return MGMT controller error message]
    //    if (RateData == null)
    //        return mc.err_msg;
    //    //ADD E N D DANIEL 20190110 [If null, return MGMT controller error message]

    //    //CHG START DANIEL 20181211 [pass RateData to method ConvertRateToPercentage to return converted percentage rate]
    //    //RP_Rate.DataSource = RateData;
    //    RP_Rate.DataSource = ConvertRateToPercentage(RateData);
    //    //CHG E N D DANIEL 20181211 [pass RateData to method ConvertRateToPercentage to return converted percentage rate]
    //    RP_Rate.DataBind();

    //    //ADD START DANIEL 20190110 [Add return string]
    //    return string.Empty;
    //    //ADD E N D DANIEL 20190110 [Add return string]
    //}


    //ADD START DANIEL 20181211 [Add method ConvertRateToPercentage to convert decimal Rate to percentage]
    //IEnumerable<MerchantRateHost> ConvertRateToPercentage(MerchantRateHost[] RateData)
    //{
    //    return RateData.Select(r => new MerchantRateHost
    //    {
    //        PymtMethod = r.PymtMethod,
    //        HostName = r.HostName,
    //        StartDate = r.StartDate,
    //        Rate = r.Rate * 100,
    //        FixedFee = r.FixedFee,
    //        Currency = r.Currency
    //    });
    //}
    //ADD E N D DANIEL 20181211 [Add method ConvertRateToPercentage to convert decimal Rate to percentage]
    //DEL E N D KENT LOONG 20190218 [Remove unnecessary code]
}