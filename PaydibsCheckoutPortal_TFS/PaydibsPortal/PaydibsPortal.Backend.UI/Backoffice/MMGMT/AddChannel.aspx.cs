﻿using LoggerII;
using Newtonsoft.Json;
using PaydibsPortal.BLL;
using PaydibsPortal.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.UI.WebControls;

public partial class Backoffice_MMGMT_AddChannel : System.Web.UI.Page
{
    MMGMTController mc = new MMGMTController();
    HomeController hc = null;
    CurrentUserStat cus = null;
    string error_msg = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        HiddenField1.Value = "";
        HiddenField2.Value = "";

        if (Session[SessionType.CurrentUser] == null)
        {
            Response.Redirect("../../Default.aspx");
            return;
        }

        cus = (CurrentUserStat)Session[SessionType.CurrentUser];
        hc = new HomeController(cus.DomainID);

        // ADD START SUKI 20190104 [Check permission on every page load]
        var permission = hc.LoadUserPermission(cus.UserID, Convert.ToInt16(OpsIDEnum.AddChannel));
        if (permission[0].IsUserAllowed == "0")
        {
            Response.Redirect("../../Backoffice/Dashboard.aspx");
            return;
        }
        // ADD E N D SUKI 20190104 [Check permission on every page load]

        if (!IsPostBack)
        {
            var merchant_L = hc.Do_GetMerchantList(1, 0);
            DD_merchant.DataSource = merchant_L;
            DD_merchant.DataTextField = "DomainDesc";
            DD_merchant.DataValueField = "DomainID";
            DD_merchant.DataBind();

            DD_CurrencyCode.Enabled = false;

            //DEL START DANIEL 20190122 [Remove unnecessary codes]
            //var pymtMethodList = mc.Do_GetCLPymtMethod();
            //DD_PymtMethod.DataSource = pymtMethodList;
            //DD_PymtMethod.DataTextField = "PymtMethodDesc";
            //DD_PymtMethod.DataValueField = "PymtMethodCode";
            //DD_PymtMethod.DataBind();
            //DEL E N D DANIEL 20190122 [Remove unnecessary codes]

            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddChannel), MethodBase.GetCurrentMethod().Name, "Page_Load Method");
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
    }

    protected void btn_GetInfo_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        int sDomainID = Convert.ToInt32(DD_merchant.SelectedValue);

        //ADD START DANIEL 20190122 [Bind pymtMethodList when GetInfo button clicked]
        var pymtMethodList = mc.Do_GetCLPymtMethod();
        DD_PymtMethod.DataSource = pymtMethodList;
        DD_PymtMethod.DataTextField = "PymtMethodDesc";
        DD_PymtMethod.DataValueField = "PymtMethodCode";
        DD_PymtMethod.DataBind();
        DD_PymtMethod.Items.Insert(0, new ListItem("--SELECT--", ""));

        DD_PymtMethod.Enabled = true;
        DD_Channel.Enabled = false;
        DD_Channel.Items.Clear();

        DD_CurrencyCode.Items.Clear();
        DD_CurrencyCode.Enabled = false;
        //ADD E N D DANIEL 20190122 [Bind pymtMethodList when GetInfo button clicked]

        var logObject = new
        {
            LoginUser = cus.UserID,
            DomainID = sDomainID
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            DD_Channel.Items.Clear();

            //CHG START DANIEL 20190110 [Insert into AuditTrail]
            //GenerateChannelList(sDomainID);
            string errMsg = GenerateChannelList(sDomainID);
            if (!string.IsNullOrEmpty(errMsg))
            {
                HiddenField1.Value = errMsg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "AddChannel(Error) - " + logJson + mc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddChannel), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + mc.system_err.ToString());
                return;
            }
            //CHG E N D DANIEL 20190110 [Insert into AuditTrail]

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                          "AddChannel - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.AddChannel, 800000, "AddChannel.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddChannel), MethodBase.GetCurrentMethod().Name, logJson);
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "AddChannel(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.AddChannel, 800001, "AddChannel.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddChannel), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }

    //CHG START DANIEL 20190110 [Change method type to string]
    //void GenerateChannelList(int PDomainID)
    string GenerateChannelList(int PDomainID)
    //CHG E N D DANIEL 20190110 [Change method type to string]
    {
        // ADD START SUKI 20181220 [Clear screen before get info]
        ClearScreen();
        // ADD E N D SUKI 20181220 [Clear screen before get info]

        //ADD START DANIEL 20190110 [If null, return MGMT error message]

        //CHG START DANIEL 20190130 [Add parameter PymtMethod = "ALL" and PStatusYesNo = 1: Do not return by status]
        //var channelData = mc.Do_Get_TerminalHost(PDomainID);
        var channelData = mc.Do_Get_TerminalHost(PDomainID, "ALL", 1);
        //CHG E N D DANIEL 20190130 [Add parameter PymtMethod = "ALL" and PStatusYesNo = 1: Do not return by status]

        if (channelData == null)
            return mc.err_msg;
        //ADD E N D DANIEL 20190110 [If null, return MGMT error message]

        //CHG START DANIEL 20190131 [Sort list by payment type]
        //RP_Channel.DataSource = channelData;
        var sortChannel = channelData.OrderBy(x => x.Type).ThenBy(y => y.HostDesc).ToList();
        RP_Channel.DataSource = sortChannel;
        //CHG E N D DANIEL 20190131 [Sort list by payment type]
        RP_Channel.DataBind();

        //ADD START DANIEL 20190110 [Add return string]
        return string.Empty;
        //ADD E N D DANIEL 20190110 [Add return string]
    }

    //ADD START DANIEL 20190122 [Add OnChanged_DD_PymtMethod method to get channel based on payment method]
    protected void OnChanged_DD_PymtMethod(object sender, EventArgs e)
    {
        var control = (DropDownList)sender;
        //var channelList = mc.Do_Get_HostByType(DD_PymtMethod.SelectedValue); Removed by Daniel 23 Apr 2019

        if (control.SelectedIndex > 0)
        {
            //Deleted by Daniel 22 Apr 2019
            //DD_Channel.DataSource = channelList;
            //DD_Channel.DataTextField = "HostDesc";
            //DD_Channel.DataValueField = "HostID";
            //DD_Channel.DataBind();
            //DD_Channel.Items.Insert(0, new ListItem("--SELECT--", ""));
            //DD_Channel.Enabled = true;
            var currency_L = mc.Do_GetCurrencyCountry(2, DD_PymtMethod.SelectedValue.Trim());// PMode = 2. Get Currency From Terminals_PresetCredential
            DD_CurrencyCode.DataSource = currency_L;
            DD_CurrencyCode.DataValueField = "CurrCode";
            DD_CurrencyCode.DataTextField = "CurrCode";
            DD_CurrencyCode.DataBind();
            DD_CurrencyCode.Items.Insert(0, new ListItem("--SELECT--", ""));

            Panel_Credential.Visible = false;
            Panel_CC.Visible = false;
            Panel_OB.Visible = false;
            Panel_Add.Visible = false;
            DD_CurrencyCode.Enabled = true;
        }
        else
        {
            //DEL START DANIEL 20190130 [Clear the fields when click --SELECT--]
            Panel_Credential.Visible = false;
            Panel_CC.Visible = false;
            Panel_OB.Visible = false;
            Panel_Add.Visible = false;
            DD_CurrencyCode.Enabled = false;
            DD_CurrencyCode.Items.Clear();
            //DEL E N D DANIEL 20190130 [Clear the fields when click --SELECT--]

            DD_Channel.Enabled = false;
            DD_Channel.Items.Clear();
        }
    }
    //ADD E N D DANIEL 20190122 [Add OnChanged_DD_PymtMethod method to get channel based on payment method]

    //ADD START DANIEL 20190122 [Add OnChanged_DD_Channel method to get credential based on channel]
    protected void OnChanged_DD_Channel(object sender, EventArgs e)
    {
        var control = (DropDownList)sender;
        var sDomainID = Convert.ToInt32(DD_merchant.SelectedValue);

        if (control.SelectedIndex > 0)
        {
            //DEL START DANIEL 20190123 [Remove unnecessary codes]
            //var channelData = mc.Do_Get_TerminalHost(sDomainID);
            //foreach (var terminal in channelData)
            //{
            //    if (terminal.Type == DD_PymtMethod.SelectedItem.Text)
            //    {
            //        if (terminal.HostID == Convert.ToInt32(DD_Channel.SelectedValue))
            //        {
            //            HiddenField1.Value = "This Channel already added.";
            //            return;
            //        }
            //    }
            //    else
            //        continue;
            //}
            //DEL E N D DANIEL 20190123 [Remove unnecessary codes]

            //ADD START DANIEL 20190131 [Clear credentials when channel changed]
            ClearCredential();
            //ADD E N D DANIEL 20190131 [Clear credentials when channel changed]

            GetCredential(DD_PymtMethod.SelectedValue, Convert.ToInt32(DD_Channel.SelectedValue), DD_CurrencyCode.SelectedValue);

            if (DD_PymtMethod.SelectedItem.Text == "Credit Card")
            {
                //Deleted By Daniel 22 Apr 2019.
                //var currencyList = mc.Do_GetCurrencyCountry(1);
                //DD_Currency.DataSource = currencyList;
                //DD_Currency.DataTextField = "CurrCode";
                //DD_Currency.DataValueField = "CurrCode";
                //DD_Currency.DataBind();
                ////DD_Currency.SelectedValue = "MYR";
                //DD_CurrencyOB.Items.FindByValue("MYR").Selected = true; //Modified By Daniel 22 Apr 2019. 

                var cardGroupList = mc.Do_GetPGMCardGroup();
                LB_CardGroup.DataSource = cardGroupList;
                LB_CardGroup.DataTextField = "CardGrpDesc";
                LB_CardGroup.DataValueField = "CardGrpID";
                LB_CardGroup.DataBind();

                //Removed by Daniel 9 Apr 2019. Not needed.
                //GetCardType(sDomainID);

                Panel_CC.Visible = true;
                Panel_OB.Visible = false;
            }
            else
            {
                //Deleted By Daniel 22 Apr 2019
                //var currencyList = mc.Do_GetCurrencyCountry(1);
                //DD_CurrencyOB.DataSource = currencyList;
                //DD_CurrencyOB.DataTextField = "CurrCode";
                //DD_CurrencyOB.DataValueField = "CurrCode";
                //DD_CurrencyOB.DataBind();
                
                ////DD_Currency.SelectedValue = "MYR";
                //DD_CurrencyOB.Items.FindByValue("MYR").Selected = true; //Modified By Daniel 22 Apr 2019. 
                Panel_CC.Visible = false;
                Panel_OB.Visible = true;
            }
            Panel_Credential.Visible = true;
            Panel_Add.Visible = true;
        }
        else
        {
            Panel_Credential.Visible = false;
            Panel_CC.Visible = false;
            Panel_OB.Visible = false;
            Panel_Add.Visible = false;
        }
    }
    //ADD E N D DANIEL 20190122 [Add OnChanged_DD_Channel method to get credential based on channel]

    //DEL START DANIEL 20190122 [Remove unnecessary codes]
    //protected void btn_GetChannel_Click(object sender, EventArgs e)
    //{
    //    Logger logger = new Logger();
    //    CLoggerII objLoggerII = new CLoggerII();

    //    var logObject = new
    //    {
    //        LoginUser = cus.UserID,
    //        DomainID = Convert.ToInt32(DD_merchant.SelectedValue),
    //        PaymentType = DD_PymtMethod.SelectedItem.Text
    //    };

    //    var logJson = JsonConvert.SerializeObject(logObject);

    //    try
    //    {
    //        objLoggerII = logger.InitLogger();

    //        var channelList = mc.Do_Get_HostByType(DD_PymtMethod.SelectedValue);

    //        //ADD START DANIEL 20190111 [Insert into AuditTrail]
    //        if(channelList == null)
    //        {
    //            HiddenField1.Value = mc.err_msg;
    //            objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
    //                       "AddChannel(Error) - " + logJson + mc.system_err.ToString());
    //            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddChannel), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + mc.system_err.ToString());
    //            return;
    //        }
    //        //ADD E N D DANIEL 20190111 [Insert into AuditTrail]

    //        DD_Channel.DataSource = channelList;
    //        DD_Channel.DataTextField = "HostDesc";
    //        DD_Channel.DataValueField = "HostID";
    //        DD_Channel.DataBind();

    //        //ADD START DANIEl 20190107 [Insert into AuditTrail,Log and AccessLog]
    //        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
    //                      "AddChannel - " + logJson);
    //        hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.AddChannel, 800000, "AddChannel.aspx");
    //        hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddChannel), MethodBase.GetCurrentMethod().Name, logJson);
    //        //ADD E N D DANIEl 20190107 [Insert into AuditTrail,Log and AccessLog]
    //    }
    //    catch (Exception ex)
    //    {
    //        objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
    //                           "AddChannel(Error) - " + logJson + ex.ToString());
    //        hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.AddChannel, 800001, "AddChannel.aspx");
    //        //ADD START DANIEl 20190107 [Insert into AuditTrail]
    //        hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddChannel), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
    //        //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
    //    }
    //    finally
    //    {
    //        objLoggerII.Dispose();
    //    }
    //}

    //protected void btn_GetCredential_Click(object sender, EventArgs e)
    //{
    //    Logger logger = new Logger();
    //    CLoggerII objLoggerII = new CLoggerII();

    //    var logObject = new
    //    {
    //        LoginUser = cus.UserID,
    //        DomainID = Convert.ToInt32(DD_merchant.SelectedValue),
    //        PaymentType = DD_PymtMethod.SelectedItem.Text,
    //        ChannelType = DD_Channel.SelectedItem.Text
    //    };
    //    var logJson = JsonConvert.SerializeObject(logObject);

    //    try
    //    {
    //        objLoggerII = logger.InitLogger();

    //        ClearCredential();

    //        var channelData = mc.Do_Get_TerminalHost(logObject.DomainID);
    //        //ADD START DANIEL 20190110 [Insert into AuditTrail]
    //        if(channelData == null)
    //        {
    //            HiddenField1.Value = mc.err_msg;
    //            objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
    //                       "AddChannel(Error) - " + logJson + mc.system_err.ToString());
    //            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddChannel), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + mc.system_err.ToString());
    //            return;
    //        }
    //        //ADD E N D DANIEL 20190110 [Insert into AuditTrail]
    //        foreach (var terminal in channelData)
    //        {
    //            if (terminal.Type == DD_PymtMethod.SelectedItem.Text)
    //            {
    //                if (terminal.HostID == Convert.ToInt32(DD_Channel.SelectedValue))
    //                {
    //                    HiddenField1.Value = "This Channel already added.";
    //                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
    //                               "AddChannel(Error) - " + logJson + mc.system_err.ToString());
    //                    //ADD START DANIEl 20190107 [Insert into AuditTrail]
    //                    hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddChannel), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + HiddenField1.Value);
    //                    //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
    //                    return;
    //                }
    //            }
    //            else
    //            {
    //                continue;
    //            }
    //        }

    //        GetCredential(DD_PymtMethod.SelectedValue, Convert.ToInt32(DD_Channel.SelectedValue));

    //        if (DD_PymtMethod.SelectedItem.Text == "Credit Card")
    //        {
    //            var currencyList = mc.Do_GetCurrencyCountry(1);
    //            DD_Currency.DataSource = currencyList;
    //            DD_Currency.DataTextField = "CurrCode";
    //            DD_Currency.DataValueField = "CurrCode";
    //            DD_Currency.DataBind();
    //            DD_Currency.SelectedValue = "MYR";

    //            var cardGroupList = mc.Do_GetPGMCardGroup();
    //            LB_CardGroup.DataSource = cardGroupList;
    //            LB_CardGroup.DataTextField = "CardGrpDesc";
    //            LB_CardGroup.DataValueField = "CardGrpID";
    //            LB_CardGroup.DataBind();

    //            GetCardType(logObject.DomainID);

    //            Panel_CC.Visible = true;
    //            Panel_OB.Visible = false;
    //        }
    //        else
    //        {
    //            var currencyList = mc.Do_GetCurrencyCountry(1);
    //            DD_CurrencyOB.DataSource = currencyList;
    //            DD_CurrencyOB.DataTextField = "CurrCode";
    //            DD_CurrencyOB.DataValueField = "CurrCode";
    //            DD_CurrencyOB.DataBind();
    //            DD_CurrencyOB.SelectedValue = "MYR";

    //            Panel_CC.Visible = false;
    //            Panel_OB.Visible = true;
    //        }
    //        Panel_Credential.Visible = true;
    //        Panel_Add.Visible = true;

    //        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
    //                       "AddChannel - " + logJson);
    //        hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.AddChannel, 800000, "AddChannel.aspx");
    //        //ADD START DANIEl 20190107 [Insert into AuditTrail]
    //        hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddChannel), MethodBase.GetCurrentMethod().Name, logJson);
    //        //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
    //    }
    //    catch (Exception ex)
    //    {
    //        objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
    //                           "AddChannel(Error) - " + logJson + ex.ToString());
    //        hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.AddChannel, 800001, "AddChannel.aspx");
    //        //ADD START DANIEl 20190107 [Insert into AuditTrail]
    //        hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddChannel), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
    //        //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
    //    }
    //    finally
    //    {
    //        objLoggerII.Dispose();
    //    }
    //}
    //DEL E N D DANIEL 20190122 [Remove unnecessary codes]

    void GetCredential(string PPymtMethodCode, int HostID, string PCurrencyCode)
    {
        var CredentialList = mc.Do_GetHostCredential(PPymtMethodCode, HostID, PCurrencyCode);

        int i = 1;
        foreach (var credential in CredentialList)
        {
            string defValue = string.Empty;
            if (credential.Encrypt == 1)
            {
                if (credential.Hex == 1)
                    defValue = mc.Do_Decrypt3DEStoHex(credential.DefaultValue);
                else
                    defValue = mc.Do_Decrypt3DES(credential.DefaultValue);
            }
            else
            {
                defValue = credential.DefaultValue;
            }

            ((Label)this.FindControl("LB_Credential" + (i))).Text = credential.FieldName;
            ((Label)this.FindControl("LB_Credential" + (i))).Visible = true;
            ((TextBox)this.FindControl("txt_Credential" + (i))).Text = defValue;
            ((TextBox)this.FindControl("txt_Credential" + (i))).Visible = true;
            ((HiddenField)this.FindControl("hid_Encrypt" + (i))).Value = credential.Encrypt.ToString();
            ((HiddenField)this.FindControl("hid_Hex" + (i))).Value = credential.Hex.ToString();
            i++;
        }
    }

    void ClearCredential()
    {
        for (int j = 1; j <= 6; j++)
        {
            ((Label)this.FindControl("LB_Credential" + (j))).Text = "Credential" + j;
            ((TextBox)this.FindControl("txt_Credential" + (j))).Text = "";
            ((Label)this.FindControl("LB_Credential" + (j))).Visible = false;
            ((TextBox)this.FindControl("txt_Credential" + (j))).Visible = false;
            ((HiddenField)this.FindControl("hid_Encrypt" + (j))).Value = "";
            ((HiddenField)this.FindControl("hid_Hex" + (j))).Value = "";
        }
    }

    //Removed by Daniel on 9 Apr 2019. Not needed
    //void GetCardType(int PDomainID)
    //{
    //    CardTypeProfile[] cardTypeProfile = mc.Do_GetCardTypeProfile(PDomainID);

    //    if (cardTypeProfile.Length > 0)
    //    {
    //        if (cardTypeProfile[0].VISA == 1)
    //        {
    //            cb_Visa.Checked = true;
    //            //DEL START DANIEL 20190131 [Don't disable the checkbox]
    //            //cb_Visa.Attributes.Add("disabled", "true");
    //            //DEL E N D  DANIEL 20190131 [Don't disable the checkbox]
    //        }
    //        if (cardTypeProfile[0].MasterCard == 1)
    //        {
    //            cb_Master.Checked = true;
    //            //DEL START DANIEL 20190131 [Don't disable the checkbox]
    //            //cb_Master.Attributes.Add("disabled", "true");
    //            //DEL E N D DANIEL 20190131 [Don't disable the checkbox]
    //        }
    //        if (cardTypeProfile[0].AMEX == 1)
    //        {
    //            cb_Amex.Checked = true;
    //            //DEL START DANIEL 20190131 [Don't disable the checkbox]
    //            //cb_Amex.Attributes.Add("disabled", "true");
    //            //DEL E N D DANIEL 20190131 [Don't disable the checkbox]
    //        }

    //        //DEL START DANIEL 20190122 [Hide unsupported card type profile]
    //        //if (cardTypeProfile[0].Diners == 1)
    //        //{
    //        //    cb_Diners.Checked = true;
    //        //    cb_Diners.Attributes.Add("disabled", "true");
    //        //}
    //        //if (cardTypeProfile[0].JCB == 1)
    //        //{
    //        //    cb_JCB.Checked = true;
    //        //    cb_JCB.Attributes.Add("disabled", "true");
    //        //}
    //        //if (cardTypeProfile[0].CUP == 1)
    //        //{
    //        //    cb_CUP.Checked = true;
    //        //    cb_CUP.Attributes.Add("disabled", "true");
    //        //}
    //        //if (cardTypeProfile[0].MasterPass == 1)
    //        //{
    //        //    cb_MasterPass.Checked = true;
    //        //    cb_MasterPass.Attributes.Add("disabled", "true");
    //        //}
    //        //if (cardTypeProfile[0].VisaCheckout == 1)
    //        //{
    //        //    cb_VisaCheckout.Checked = true;
    //        //    cb_VisaCheckout.Attributes.Add("disabled", "true");
    //        //}
    //        //if (cardTypeProfile[0].SamsungPay == 1)
    //        //{
    //        //    cb_SamsungPay.Checked = true;
    //        //    cb_SamsungPay.Attributes.Add("disabled", "true");
    //        //}
    //        //DEL E N D DANIEL 20190122 [Hide unsupported card type profile]
    //    }
    //}

    protected void btn_AddHost_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        List<string> cardTypeList = new List<string>();
        if (cb_Visa.Checked) cardTypeList.Add("Visa");
        if (cb_Master.Checked) cardTypeList.Add("Master");
        if (cb_Amex.Checked) cardTypeList.Add("Amex");

        //DEL START DANIEL 20190122 [Hide Unsupported card type profile]
        //if (cb_Diners.Checked) cardTypeList.Add("Diners");
        //if (cb_JCB.Checked) cardTypeList.Add("JCB");
        //if (cb_CUP.Checked) cardTypeList.Add("CUP");
        //if (cb_MasterPass.Checked) cardTypeList.Add("MasterPass");
        //if (cb_VisaCheckout.Checked) cardTypeList.Add("VisaCheckout");
        //if (cb_SamsungPay.Checked) cardTypeList.Add("SamsungPay");
        //DEL E N D DANIEL 20190122 [Hide Unsupported card type profile]
        string cardTypes = string.Join(",", cardTypeList);

        var val = "";
        var txt = "";
        foreach (ListItem listItem in LB_CardGroup.Items)
        {
            if (listItem.Selected)
            {
                if (val != "")
                    val = val + ",";
                if (txt != "")
                    txt = txt + ",";

                val = val + listItem.Value;
                txt = txt + listItem.Text;
            }
        }

        //Added by Daniel 9 Apr 2019. Check if cardgroup is empty
        if (DD_PymtMethod.SelectedValue.Trim().Equals("CC"))
        {
            if (string.IsNullOrEmpty(val))
            {
                HiddenField1.Value = "Card group cannot empty. Please select card group.";
                return;
            }
        }

        var logObject = new
        {
            LoginUser = cus.UserID,
            DomainID = Convert.ToInt32(DD_merchant.SelectedValue),
            PaymentType = DD_PymtMethod.SelectedItem.Text,
            Channel = DD_Channel.SelectedItem.Text,
            Credential1 = txt_Credential1.Text,
            Credential2 = txt_Credential2.Text,
            Credential3 = txt_Credential3.Text,
            Credential4 = txt_Credential4.Text,
            Credential5 = txt_Credential5.Text,
            Credential6 = txt_Credential6.Text,
            CardType = cardTypes,
            //Currency = string.IsNullOrEmpty(DD_Currency.SelectedValue) ? DD_CurrencyOB.SelectedItem.Text : DD_Currency.SelectedItem.Text,
            //Currency = !DD_PymtMethod.SelectedValue.Equals("CC ") ? DD_CurrencyOB.SelectedValue : DD_Currency.SelectedValue,
            Currency = DD_CurrencyCode.SelectedValue,
            CardGroup = txt
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            int mode = 0;
            var channelData = mc.Do_Get_TerminalHost(logObject.DomainID);

            //ADD START DANIEL 20190110 [Insert into AuditTrail]
            if (channelData == null)
            {
                HiddenField1.Value = mc.err_msg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "AddChannel(Error) - " + logJson + mc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddChannel), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + mc.system_err.ToString());
                return;
            }
            //ADD E N D DANIEL 20190110 [Insert into AuditTrail]

            foreach (var channel in channelData)
            {
                if (channel.Type == "Credit Card")
                {
                    mode = 1;
                    break;
                }
            }

            string credential1 = string.Empty, credential2 = string.Empty, credential3 = string.Empty, credential4 = string.Empty,
                credential5 = string.Empty, credential6 = string.Empty;

            try
            {
                if (hid_Encrypt1.Value == "1")
                    credential1 = mc.Do_Encrypt3DES(logObject.Credential1);
                else
                    credential1 = logObject.Credential1;

                if (hid_Encrypt2.Value == "1")
                    credential2 = mc.Do_Encrypt3DES(logObject.Credential2);
                else
                    credential2 = logObject.Credential2;

                if (hid_Encrypt3.Value == "1")
                    credential3 = mc.Do_Encrypt3DES(logObject.Credential3);
                else
                    credential3 = logObject.Credential3;

                if (hid_Encrypt4.Value == "1")
                    credential4 = mc.Do_Encrypt3DES(logObject.Credential4);
                else
                    credential4 = logObject.Credential4;

                if (hid_Encrypt5.Value == "1")
                    credential5 = mc.Do_Encrypt3DES(logObject.Credential5);
                else
                    credential5 = logObject.Credential5;

                if (hid_Encrypt6.Value == "1")
                    credential6 = mc.Do_Encrypt3DES(logObject.Credential6);
                else
                    credential6 = logObject.Credential6;

                if (mc.err_msg != string.Empty)
                {
                    HiddenField1.Value = mc.err_msg;
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "AddChannel(Error) - " + logJson + mc.system_err.ToString());
                    //ADD START DANIEl 20190107 [Insert into AuditTrail]
                    hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddChannel), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + mc.system_err.ToString());
                    //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
                    return;
                }
            }
            catch (Exception ex)
            {
                //ADD START DANIEL 20190108 [Assign exception message to HiddenField1]
                HiddenField1.Value = ex.Message;
                //ADD E N D DANIEL 20190108 [Assign exception message to HiddenField1]
            }

            //ADD START DANIEL 20190123 [Check if HostID and CurrencyCode existed, prompt error message]
            var checkExist = mc.Do_Get_TerminalHost(logObject.DomainID, DD_PymtMethod.SelectedValue, 1);


            foreach (var c in checkExist)
            {
                if (c.HostID.Equals(Convert.ToInt32(DD_Channel.SelectedValue)))
                {
                    //if (c.CurrencyCode.Equals(!DD_PymtMethod.SelectedValue.Equals("CC ") ? DD_CurrencyOB.SelectedValue : DD_Currency.SelectedValue))
                    if (c.CurrencyCode.Equals(DD_CurrencyCode.SelectedValue))
                    {
                        HiddenField1.Value = "This channel has already been added.";
                        return;
                    }
                }
                else
                {
                    continue;
                }

            }
            //ADD E N D DANIEL 20190123 [Check if HostID and CurrencyCode same, prompt error message]

            //CHG START DANIEL 20190122 [Unsupported card type profiles are removed and set to 0 as default value]
            //bool addChannel = mc.Do_AddChannel(logObject.DomainID, DD_PymtMethod.SelectedValue.Trim(), int.Parse(DD_Channel.SelectedValue), LB_Credential1.Text, credential1,
            //                    LB_Credential2.Text, credential2, LB_Credential3.Text, credential3, LB_Credential4.Text, credential4,
            //                    LB_Credential5.Text, credential5, LB_Credential6.Text, credential6,
            //                    logObject.Currency, 1, val, (cb_Visa.Checked ? 1 : 0), (cb_Master.Checked ? 1 : 0), (cb_Amex.Checked ? 1 : 0), (cb_Diners.Checked ? 1 : 0),
            //                    (cb_JCB.Checked ? 1 : 0), (cb_CUP.Checked ? 1 : 0), (cb_MasterPass.Checked ? 1 : 0), (cb_VisaCheckout.Checked ? 1 : 0), (cb_SamsungPay.Checked ? 1 : 0), mode);

            bool addChannel = mc.Do_AddChannel(logObject.DomainID, DD_PymtMethod.SelectedValue.Trim(), int.Parse(DD_Channel.SelectedValue), LB_Credential1.Text, credential1,
                   LB_Credential2.Text, credential2, LB_Credential3.Text, credential3, LB_Credential4.Text, credential4,
                   LB_Credential5.Text, credential5, LB_Credential6.Text, credential6,
                   logObject.Currency, 1, val, (cb_Visa.Checked ? 1 : 0), (cb_Master.Checked ? 1 : 0), (cb_Amex.Checked ? 1 : 0),
                   0, 0, 0, 0, 0, 0, mode);
            //CHG E N D DANIEL 20190122 [Unsupported card type profiles are removed and set to 0 as default value]

            if (!addChannel)
            {
                HiddenField1.Value = mc.err_msg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "AddChannel(Error) - " + logJson + mc.system_err.ToString());
                //ADD START DANIEl 20190107 [Insert into AuditTrail]
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddChannel), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + mc.system_err.ToString());
                //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
                return;
            }

            // DEL START SUKI 20181220 [Unnecessary code]
            //ClearScreen();
            //GenerateChannelList(logObject.DomainID);
            // DEL E N D SUKI 20181220 [Unnecessary code]

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "AddChannel - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.AddChannel, 800000, "AddChannel.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddChannel), MethodBase.GetCurrentMethod().Name, logJson);
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]


            //CHG START DANIEL 20190110 [Insert into AuditTrail]
            //GenerateChannelList(logObject.DomainID);
            string errMsg = GenerateChannelList(logObject.DomainID);
            if (!string.IsNullOrEmpty(errMsg))
            {
                HiddenField1.Value = errMsg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "AddChannel(Error) - " + logJson + mc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddChannel), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + mc.system_err.ToString());
                return;
            }
            //CHG E N D DANIEL 20190110 [Insert into AuditTrail]

            //ADD START KENT LOONG 20190219 [Add Redirect URL]
            string url = "../MMGMT/AddMerchantRate.aspx?Merchant=" + DD_merchant.SelectedValue;
            //ADD E N D KENT LOONG 20190219 [Add Redirect URL]
            HiddenField1.Value = "<h4>Add Channel Successful. Please proceed to <a href='" + url + "'>Add Merchant Rate.</a></h4>";
            HiddenField2.Value = AlertType.Success;

            //ADD START DANIEL 20190130 [Enhance the flow for end user]
            DD_PymtMethod.ClearSelection();
            DD_Channel.Items.Clear();
            DD_Channel.Enabled = false;
            ClearScreen();
            //ADD E N D DANIEL 20190130 [Enhance the flow for end user]
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "AddChannel(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.AddChannel, 800001, "AddChannel.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddChannel), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        finally
        {
            objLoggerII.Dispose();
        }

    }

    private void ClearScreen()
    {
        DD_Channel.Items.Clear();
        Panel_CC.Visible = false;
        Panel_OB.Visible = false;
        Panel_Credential.Visible = false;
        Panel_Add.Visible = false;

        cb_Visa.Checked = false;
        cb_Master.Checked = false;
        cb_Amex.Checked = false;
    }

    //Added by Daniel 23 Apr 2019 . Add currency dropdown
    protected void OnChanged_DD_Currency(object sender, EventArgs e)
    {
        var control = (DropDownList)sender;
        var channelList = mc.Do_Get_HostByType(DD_PymtMethod.SelectedValue, DD_CurrencyCode.SelectedValue.Trim());
        if (control.SelectedIndex > 0)
        {
            DD_Channel.DataSource = channelList;
            DD_Channel.DataTextField = "HostDesc";
            DD_Channel.DataValueField = "HostID";
            DD_Channel.DataBind();
            DD_Channel.Items.Insert(0, new ListItem("--SELECT--", ""));
            DD_Channel.Enabled = true;
            Panel_Credential.Visible = false;
            Panel_CC.Visible = false;
            Panel_OB.Visible = false;
            Panel_Add.Visible = false;
        }
        else
        {
            Panel_Credential.Visible = false;
            Panel_CC.Visible = false;
            Panel_OB.Visible = false;
            Panel_Add.Visible = false;

            DD_Channel.Enabled = false;
            DD_Channel.Items.Clear();
        }
    }
}