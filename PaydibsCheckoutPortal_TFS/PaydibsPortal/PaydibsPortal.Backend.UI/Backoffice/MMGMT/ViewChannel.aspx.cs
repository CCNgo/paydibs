﻿using LoggerII;
using Newtonsoft.Json;
using PaydibsPortal.BLL;
using PaydibsPortal.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

public partial class Backoffice_MMGMT_ViewChannel : System.Web.UI.Page
{
    MMGMTController mc = new MMGMTController();
    HomeController hc = null;
    CurrentUserStat cus = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session[SessionType.CurrentUser] == null)
        {
            Response.Redirect("../../Default.aspx");
            return;
        }

        cus = (CurrentUserStat)Session[SessionType.CurrentUser];
        hc = new HomeController(cus.DomainID);

        // ADD START SUKI 20190104 [Check permission on every page load]
        var permission = hc.LoadUserPermission(cus.UserID, Convert.ToInt16(OpsIDEnum.ViewChannel));
        if (permission[0].IsUserAllowed == "0")
        {
            Response.Redirect("../../Backoffice/Dashboard.aspx");
            return;
        }
        // ADD E N D SUKI 20190104 [Check permission on every page load]

        if (!IsPostBack)
        {
            var merchant_L = hc.Do_GetMerchantList(1, 0);
            DD_merchant.DataSource = merchant_L;
            DD_merchant.DataTextField = "DomainDesc";
            DD_merchant.DataValueField = "DomainID";
            DD_merchant.DataBind();

            GenerateMerchantChannel(Convert.ToInt32(DD_merchant.SelectedValue));
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ViewChannel), MethodBase.GetCurrentMethod().Name, "Page_Load Method");
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
    }

    protected void btn_GetInfo_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        int sDomainID = Convert.ToInt32(DD_merchant.SelectedValue);

        var logObject = new
        {
            LoginUser = cus.UserID,
            DomainID = sDomainID
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            //CHG START DANIEL 20190110 [Insert into AuditTrail]
            //GenerateMerchantChannel(sDomainID);
            string errMsg = GenerateMerchantChannel(sDomainID);

            if (!string.IsNullOrEmpty(errMsg))
            {
                HiddenField1.Value = errMsg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                              "ViewChannel(Error) - " + logJson + mc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ViewChannel), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + mc.system_err.ToString());
                return;
            }
            //CHG E N D DANIEL 20190110 [Insert into AuditTrail]

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                          "ViewChannel - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.ViewChannel, 800000, "ViewChannel.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ViewChannel), MethodBase.GetCurrentMethod().Name, logJson);
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "ViewChannel(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.ViewChannel, 800001, "ViewChannel.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ViewChannel), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }

    //CHG START DANIEL 20190110 [Change method type to string]
    //void GenerateMerchantChannel(int PDomainID)
    string GenerateMerchantChannel(int PDomainID)
    //CHG E N D DANIEL 20190110 [Change method type to string]
    {
        //CHG START DANIEL 20190213 [Add parameter PymtMethod = "ALL" and PStatusYesNo = 1: Do not return by status]
        //var ChannelData = mc.Do_Get_TerminalHost(PDomainID);
        var ChannelData = mc.Do_Get_TerminalHost(PDomainID, "ALL", 1);
        //CHG E N D DANIEL 20190213 [Add parameter PymtMethod = "ALL" and PStatusYesNo = 1: Do not return by status]

        //Added by Daniel 10 May 2019. If Not Admin, show CreditCard = Visa /MasterCard
        if (cus.DomainID != 1)
        {
            ChannelData.Where(c => c.Type == "Credit Card").ToList().ForEach(cc => cc.HostDesc = "Visa/MasterCard");
        }

        //ADD START DANIEL 20190110 [If null, return MGMT controller error message]
        if (ChannelData == null)
            return mc.err_msg;
        //ADD E N D DANIEL 20190110 [If null, return MGMT controller error message]

        RP_Channel.DataSource = ChannelData;
        RP_Channel.DataBind();

        //ADD START DANIEL 20190110 [Add return string]
        return string.Empty;
        //ADD E N D DANIEL 20190110 [Add return string]
    }
}