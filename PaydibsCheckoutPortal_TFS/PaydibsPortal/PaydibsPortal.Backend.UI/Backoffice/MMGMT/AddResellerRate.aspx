﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddResellerRate.aspx.cs" Inherits="Backoffice_MMGMT_AddResellerRate" %>

<!DOCTYPE html>
<%@ Register TagPrefix="uc" TagName="Left_Side_Bar" Src="~/UserControl/LeftSideBar.ascx" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Paydibs-Checkout Add Reseller Rate</title>
    <link rel="stylesheet" type="text/css" href="../../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/material-design-icons/css/material-design-iconic-font.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/select2/css/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/bootstrap-slider/css/bootstrap-slider.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/datatables/datatables.net-bs4/css/dataTables.bootstrap4.css" />
    <link rel="stylesheet" href="../../assets/css/app.css" type="text/css" />


</head>
<body>
    <form id="form1" runat="server">
        <asp:HiddenField ID="HiddenField2" runat="server" />
        <asp:HiddenField ID="HiddenField1" runat="server" />
        <div class="be-wrapper be-collapsible-sidebar be-collapsible-sidebar-collapsed">
            <uc:Left_Side_Bar ID="Left_Side_Bar" runat="server" />
            <div class="be-content">
                <div class="main-content container-fluid">
                    <div role="alert" class="alert alert-contrast alert-dismissible" id="dl_ErrorDialog">
                        <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
                        <div class="message" id="dl_message">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 ">
                            <div class="card card-table card-border-color card-border-color-primary">
                                <div class="row table-filters-container">
                                    <div class="col-12 col-lg-12 col-xl-6">
                                        <div class="row">
                                            <div class="col-12 col-lg-8 table-filters pb-0 pb-xl-4">
                                                <span class="table-filter-title">Reseller</span>
                                                <div class="filter-container">
                                                    <label class="control-label">Select Reseller:</label>
                                                    <asp:DropDownList ID="DD_merchant" runat="server" CssClass="select2"></asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-12 col-lg-2 table-filters pb-0 pb-xl-4">
                                                <span class="table-filter-title"></span>
                                                <div class="filter-container" style="margin-top: 45px">
                                                    <asp:Button ID="btn_GetInfo" runat="server" Text="Get Info" CssClass="btn btn-primary btn-xl" OnClick="btn_GetInfo_Click" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-border-color card-border-color-primary">
                                <div class="card-header card-header-divider font-weight-bold">
                                    Add Reseller Rate
                                </div>
                                <div class="card-body">
                                    <div class="filter-container">
                                        <div class="row">
                                            <div class="col-4">
                                                <label class="control-label font-weight-bold">Type</label>
                                            </div>
                                            <div class="col-6">
                                                <asp:DropDownList ID="DD_PymtMethod" runat="server" CssClass="select2" OnSelectedIndexChanged="OnChanged_DD_PymtMethod" AutoPostBack="true" Enabled="false"></asp:DropDownList>
                                            </div>
                                         <%--   <div class="col-2">
                                                <asp:Button ID="btn_GetChannel" runat="server" Text="Get" CssClass="btn btn-primary btn-xl" OnClick="btn_GetChannel_Click" />
                                            </div>--%>
                                        </div>
                                        <div class="row">
                                            <div class="col-4">
                                                <label class="control-label font-weight-bold">Channel</label>
                                            </div>
                                            <div class="col-6">
                                                <asp:DropDownList ID="DD_Channel" runat="server" CssClass="select2" OnSelectedIndexChanged="OnChanged_DD_Channel" AutoPostBack="true" Enabled="false"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-4">
                                                <label class="control-label font-weight-bold">Rate (%)</label>
                                            </div>
                                            <div class="col-6">
                                                <asp:TextBox ID="txt_Rate" placeholder="(Mandatory) Rate (%)" runat="server" CssClass="form-control" Onkeypress="return isNumberKey(event)"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-4">
                                                <label class="control-label font-weight-bold">Fixed Rate</label>
                                            </div>
                                            <div class="col-6">
                                                <asp:TextBox ID="txt_FixedRate" placeholder="(Mandatory) Fixed Rate" runat="server" CssClass="form-control" Onkeypress="return isNumberKey(event)"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-4">
                                                <label class="control-label font-weight-bold">Effective Date</label>
                                            </div>
                                            <div class="col-6">
                                                <asp:TextBox ID="txt_EffectiveDate" type="Date" runat="server" CssClass="form-control" Onkeypress="return isNumberKey(event)"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-4">
                                                <label class="control-label font-weight-bold">Currency</label>
                                            </div>
                                            <div class="col-6">
                                                <asp:DropDownList ID="DD_Currency" runat="server" CssClass="select2"></asp:DropDownList>
                                            </div>
                                            <div class="col-2">
                                                <asp:Button ID="btn_AddRate" runat="server" Text="Add" CssClass="btn btn-primary btn-xl" OnClientClick="if (!RateInputValidation()) { return false;};" OnClick="btn_AddRate_Click" Enabled="false"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class=" col-md-12">
                            <div class="card card-border-color card-border-color-primary">
                                <div class="card-header card-header-divider font-weight-bold">
                                    Reseller Rate
                                </div>
                                <nav>
	                              <div class="nav nav-tabs" id="nav-tab" role="tablist">
		                            <a class="nav-item nav-link active" id="nav-CurrentRate-tab" data-toggle="tab" href="#nav-CurrentRate" role="tab" aria-controls="nav-CurrentRate" aria-selected="true">Current Rate</a>
		                            <a class="nav-item nav-link" id="nav-FutureRate-tab" data-toggle="tab" href="#nav-FutureRate" role="tab" aria-controls="nav-FutureRate" aria-selected="false">New Effective Rate</a>
		                            <%--<a class="nav-item nav-link" id="nav-addPromotion-tab" data-toggle="tab" href="#nav-addPromotion" role="tab" aria-controls="nav-addPromotion" aria-selected="false">Add Promotion Rate</a>--%>
	                              </div>
	                            </nav>
                                <div class="tab-content" id="nav-tabContent">
                                    <div class="tab-pane fade show active" id="nav-CurrentRate" role="tabpanel" aria-labelledby="nav-addRate-tab">
                                        <div>
                                            <asp:Repeater ID="RP_Rate" runat="server">
                                                <HeaderTemplate>
                                                    <table id="table1" class="table table-striped table-hover table-fw-widget">
                                                        <tr class="odd gradeX">
                                                            <th>Payment Method</th>
                                                            <th>Channel</th>
                                                            <th>Rate (%)</th>
                                                            <th>Fixed Fee</th>
                                                            <th>Currency</th>
                                                        </tr>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr class="odd gradeX">
                                                        <td><%#  Eval("PymtMethod") %></td>
                                                        <td><%#  Eval("HostName") %></td>
                                                        <td><%#  Eval("Rate") %></td>
                                                        <td><%#  Eval("FixedFee") %></td>
                                                        <td><%#  Eval("Currency") %></td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </table>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="nav-FutureRate" role="tabpanel" aria-labelledby="nav-FutureRate-tab">
                                        <div>
                                            <asp:Repeater ID="RP_FutureRate" runat="server">
                                                <HeaderTemplate>
                                                    <table id="table1" class="table table-striped table-hover table-fw-widget">
                                                        <tr class="odd gradeX">
                                                            <th>Payment Method</th>
                                                            <th>Channel</th>
                                                            <th>Rate (%)</th>
                                                            <th>Fixed Fee</th>
                                                            <th>Currency</th>
                                                            <th>Effective Date</th>
                                                        </tr>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr class="odd gradeX">
                                                        <td><%#  Eval("PymtMethod") %></td>
                                                        <td><%#  Eval("HostName") %></td>
                                                        <td><%#  Eval("Rate") %></td>
                                                        <td><%#  Eval("FixedFee") %></td>
                                                        <td><%#  Eval("Currency") %></td>
                                                        <td><%#  Eval("StartDate") %></td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </table>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center" id="footer">
                Copyright © 2019 Paydibs Sdn. Bhd. All Rights Reserved.
            </div>
        </div>
    </form>
    <script src="../../assets/lib/customize/General.js"></script>
    <script src="../../assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/bootstrap/dist/js/bootstrap.bundle.min.js" type="text/javascript"></script>
    <script src="../../assets/js/app.js" type="text/javascript"></script>
    <script src="../../assets/lib/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/select2/js/select2.full.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/bootstrap-slider/bootstrap-slider.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="../../assets/js/app-table-filters.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net/js/jquery.dataTables.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-bs4/js/dataTables.bootstrap4.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/dataTables.buttons.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/buttons.html5.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/buttons.flash.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/buttons.print.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/buttons.colVis.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js" type="text/javascript"></script>
    <script src="../../assets/js/app-tables-datatables.js" type="text/javascript"></script>
    <script type="text/javascript">
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear();
        var textBoxCss = "#e57373";

        if(dd<10) {
            dd = '0'+dd
        } 

        if(mm<10) {
            mm = '0'+mm
        } 
        var todaydate = yyyy + "-" + mm + "-" + dd;
        $(document).ready(function () {
            App.init();
            App.tableFilters();
            App.dataTables();
        });
        $("#txt_EffectiveDate").attr({
            "min" : todaydate
        });
        $(function () {
            $('#dl_ErrorDialog').hide();
            var msg = $('#<%= HiddenField1.ClientID %>').val();
            var classType = $('#<%= HiddenField2.ClientID %>').val();
            var type = "alert-danger";
            if (msg != "") {
                if (classType != "") type = classType;
                $('#dl_ErrorDialog').addClass(type);
                document.getElementById('dl_message').innerText = msg;
                $('#dl_ErrorDialog').show();
            }
        });

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }

        function RateInputValidation(){
            if ($("#txt_EffectiveDate").val() != "") {
                return true
            } else {
                return false
            }
        }

    </script>

</body>
</html>
