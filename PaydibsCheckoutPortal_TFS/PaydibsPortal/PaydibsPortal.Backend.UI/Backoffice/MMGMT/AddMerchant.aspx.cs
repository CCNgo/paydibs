﻿using LoggerII;
using Newtonsoft.Json;
using PaydibsPortal.BLL;
using PaydibsPortal.DAL;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Configuration;
using System.Web.Services;

public partial class AddMerchant : System.Web.UI.Page
{
    HomeController hc = null;
    MMGMTController mc = new MMGMTController();
    CurrentUserStat cus = null;
    public static string merchantID = string.Empty;
    string error_msg = string.Empty;
    public int counter = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        HiddenField1.Value = "";
        HiddenField2.Value = "";

        if (Session[SessionType.CurrentUser] == null)
        {
            Response.Redirect("../../Default.aspx");
            return;
        }

        cus = (CurrentUserStat)Session[SessionType.CurrentUser];
        hc = new HomeController(cus.DomainID);

        // ADD START SUKI 20190104 [Check permission on every page load]
        var permission = hc.LoadUserPermission(cus.UserID, Convert.ToInt16(OpsIDEnum.AddMerchant));
        if (permission[0].IsUserAllowed == "0")
        {
            Response.Redirect("../../Backoffice/Dashboard.aspx");
            return;
        }
        // ADD E N D SUKI 20190104 [Check permission on every page load]

        if (!IsPostBack)
        {
            DD_BusinesssState.DataSource = mc.Do_GetCLState();
            DD_BusinesssState.DataTextField = "State";
            DD_BusinesssState.DataValueField = "PKID";
            DD_BusinesssState.DataBind();

            DD_Country.DataSource = mc.Do_GetCLCountry();
            DD_Country.DataTextField = "CountryName";
            DD_Country.DataValueField = "CountryID";
            DD_Country.DataBind();

            DD_Plugin.DataSource = mc.Do_GetCLPlugin();
            DD_Plugin.DataTextField = "PluginName";
            DD_Plugin.DataValueField = "PluginID";
            DD_Plugin.DataBind();

            DD_Notify.DataSource = mc.Do_GetCLNotifEmailRcv();
            DD_Notify.DataTextField = "NotifEmailRcvDesc";
            DD_Notify.DataValueField = "NotifEmailRcvID";
            DD_Notify.DataBind();
            DD_Notify.SelectedIndex = 3;

            DD_BankCountry.DataSource = mc.Do_GetCLCountry();
            DD_BankCountry.DataTextField = "CountryName";
            DD_BankCountry.DataValueField = "CountryID";
            DD_BankCountry.DataBind();

            int i = 0;
            foreach (var r in mc.Do_GetCLCountry())
            {
                if (r.CountryName == "Malaysia")
                {
                    i = r.CountryID - 1;
                    break;
                }
            }
            DD_Country.SelectedIndex = i;
            DD_BankCountry.SelectedIndex = i;

            DD_MCCCode.DataSource = mc.Do_GetCLMCCCode();
            DD_MCCCode.DataTextField = "Description";
            DD_MCCCode.DataValueField = "MCCCode";
            DD_MCCCode.DataBind();

            // ADD START SUKI 20181128 [Add in showMerchantAddr, showMerchantLogo, IsAgent, default currency setting]
            var currencyList = mc.Do_GetCurrencyCountry(1);
            DD_Currency.DataSource = currencyList;
            DD_Currency.DataTextField = "CurrCode";
            DD_Currency.DataValueField = "CurrCode";
            DD_Currency.DataBind();
            DD_Currency.SelectedValue = "MYR";
            // ADD E N D SUKI 20181128 [Add in showMerchantAddr, showMerchantLogo, IsAgent, default currency setting]

            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddMerchant), MethodBase.GetCurrentMethod().Name, "Page_Load Method");
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
    }

    [WebMethod]
    public static string ServerSideMethod(string BusinessRegName)
    {
        if (BusinessRegName.Trim().Length > 0)
        {
            BusinessRegName = Regex.Replace(BusinessRegName, @"[^0-9A-Za-z]", "");
            string NameTrimSpace = BusinessRegName.Replace(" ", "");
            var SplitNo = NameTrimSpace.Length / 6;
            string aa1 = NameTrimSpace.Substring(0, 1);
            string aa2 = NameTrimSpace.Substring(SplitNo * 1, 1);
            string aa3 = NameTrimSpace.Substring(SplitNo * 2, 1);
            string aa4 = NameTrimSpace.Substring(SplitNo * 3, 1);
            string aa5 = NameTrimSpace.Substring(SplitNo * 4, 1);
            string aa6 = NameTrimSpace.Substring(SplitNo * 5, 1);
            merchantID = (aa1 + aa2 + aa3 + aa4 + aa5 + aa6).ToUpper();

            // ADD START SUKI 20181203 [To Check Merchant ID existed in CMS_Domain]
            AddMerchant addMerchant = new AddMerchant();
            merchantID = addMerchant.chkMerchantID(merchantID);
            // ADD E N D SUKI 20181203 [To Check Merchant ID existed in CMS_Domain]

            return merchantID;
        }
        else
            return "";
    }

    // ADD START SUKI 20181203 [To Check Merchant ID existed in CMS_Domain]
    public string chkMerchantID(string merchantID)
    {
        string output = merchantID;
        if (!mc.checkCMSDomain(merchantID))
        {
            counter++;
            if (counter > 50)
                merchantID = merchantID.Substring(0, 5) + "Z";

            output = "";
            int arraysize = merchantID.Length;
            int[] randomArray = new int[arraysize];

            for (int i = 0; i < arraysize; i++)
            {
                randomArray[i] = i;
            }

            Shuffle(randomArray);

            for (int i = 0; i < arraysize; i++)
            {
                output += merchantID[randomArray[i]];
            }
            return chkMerchantID(output);
        }
        else
        {
            return output;
        }
    }

    public void Shuffle<T>(T[] array)
    {
        Random random = new Random();
        for (int i = 0; i < array.Length; i++)
        {
            int j = random.Next(i, array.Length); // Don't select from the entire array on subsequent loops
            T temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }
    }
    // ADD E N D SUKI 20181203 [To Check Merchant ID existed in CMS_Domain]

    protected void btn_Complete_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        #region Mandatory Checking
        string chkMessage = string.Empty;
        if (string.IsNullOrEmpty(txt_BusinessRegName.Text))
        {
            chkMessage += "A) Company Registered Name" + System.Environment.NewLine;
        }
        if (string.IsNullOrEmpty(txt_BusinessRegNo.Text))
        {
            chkMessage += "A) Company Registration No." + System.Environment.NewLine;
        }
        if (string.IsNullOrEmpty(txt_BusinessAdd1.Text))
        {
            chkMessage += "A) Address" + System.Environment.NewLine;
        }
        if (string.IsNullOrEmpty(txt_OfficeNumber.Text))
        {
            chkMessage += "A) Office Contact Number" + System.Environment.NewLine;
        }
        if (string.IsNullOrEmpty(txt_acp1_FullName.Text))
        {
            chkMessage += "A) Authorized Contact Person Name" + System.Environment.NewLine;
        }
        if (string.IsNullOrEmpty(txt_acp1_MobileNumber.Text))
        {
            chkMessage += "A) Authorized Contact Person Contact No." + System.Environment.NewLine;
        }
        if (string.IsNullOrEmpty(txt_acp1_EmailAdd.Text))
        {
            chkMessage += "A) Authorized Contact Person Email" + System.Environment.NewLine;
        }
        if (string.IsNullOrEmpty(txt_businessName.Text))
        {
            chkMessage += "C) Trading Name" + System.Environment.NewLine;
        }
        if (!string.IsNullOrEmpty(chkMessage))
        {
            HiddenField1.Value = "Please enter mandatory field:" + System.Environment.NewLine + chkMessage;
            return;
        }
        #endregion

        #region A) Company Info
        string BusinessRegName = txt_BusinessRegName.Text.Trim();
        string BusinessRegNo = txt_BusinessRegNo.Text.Trim();
        string GSTNo = ""; // txt_GSTNo.Text.Trim();
        string TypeOfBusiness = string.Empty;
        //if (rd_TypeOfBusiness_SP.Checked) TypeOfBusiness = "Sole Proprietorship";
        //else if (rd_TypeOfBusiness_CO.Checked) TypeOfBusiness = "Corporation (Sdn Bhd/ Bhd)";
        //else if (rd_TypeOfBusiness_PS.Checked) TypeOfBusiness = "Partnership";
        //else if (rd_TypeOfBusiness_NPO.Checked) TypeOfBusiness = "Non-Profit Organization";
        //else if (rd_TypeOfBusiness_Other.Checked) TypeOfBusiness = "Other";

        string BusinessAdd1 = txt_BusinessAdd1.Text.Trim();
        string BusinessAdd2 = txt_BusinessAdd2.Text.Trim();
        string BusinessAdd3 = txt_BusinessAdd3.Text.Trim();
        string BusinessCity = txt_BusinessCity.Text.Trim();
        string BusinessState = DD_BusinesssState.SelectedValue;
        string BusinessZipcode = txt_BusinessZipcode.Text.Trim();
        string BusinessCountry = DD_Country.SelectedValue;
        string OfficeNumber = txt_OfficeNumber.Text.Trim();

        string ACP1Name = txt_acp1_FullName.Text.Trim();
        string ACP1Mobile = txt_acp1_MobileNumber.Text.Trim();
        string ACP1Email = txt_acp1_EmailAdd.Text.Trim();

        string ACP2Name = txt_acp2_FullName.Text.Trim();
        string ACP2Mobile = txt_acp2_MobileNumber.Text.Trim();
        string ACP2Email = txt_acp2_EmailAdd.Text.Trim();

        string billName = txt_bill_FullName.Text.Trim();
        string billMobile = txt_bill_MobileNumber.Text.Trim();
        string billEmail = txt_bill_EmailAdd.Text.Trim();
        #endregion

        #region B) List of Company Director / Proprietor
        string D_FullName_i = ""; //txt_i_director_name.Text.Trim();
        string D_ID_i = ""; //txt_i_director_id.Text.Trim();
        string D_FullName_ii = ""; //txt_ii_director_name.Text.Trim();
        string D_ID_ii = ""; //txt_ii_director_id.Text.Trim();
        string D_FullName_iii = ""; //txt_iii_director_name.Text.Trim();
        string D_ID_iii = ""; //txt_iii_director_id.Text.Trim();
        string D_FullName_iv = ""; //txt_iv_director_name.Text.Trim();
        string D_ID_iv = ""; //txt_iv_director_id.Text.Trim();
        string D_FullName_v = ""; //txt_v_director_name.Text.Trim();
        string D_ID_v = ""; //txt_v_director_id.Text.Trim();

        string S_FullName_i = ""; //txt_i_share_name.Text.Trim();
        string S_ID_i = ""; //txt_i_share_id.Text.Trim();
        string S_FullName_ii = ""; //txt_ii_share_name.Text.Trim();
        string S_ID_ii = ""; //txt_ii_share_id.Text.Trim();
        string S_FullName_iii = ""; //txt_iii_share_name.Text.Trim();
        string S_ID_iii = ""; //txt_iii_share_id.Text.Trim();
        string S_FullName_iv = ""; //txt_iv_share_name.Text.Trim();
        string S_ID_iv = ""; //txt_iv_share_id.Text.Trim();
        string S_FullName_v = ""; //txt_v_share_name.Text.Trim();
        string S_ID_v = ""; //txt_v_share_id.Text.Trim();
        #endregion

        #region C) Business Profile
        string TradingName = txt_businessName.Text.Trim();
        string WebSiteURL = txt_onlinestoreURL.Text.Trim();
        string TypeOfProducts = txt_TypeOfProducts.Text.Trim();

        List<string> targetMarkets_L = new List<string>();
        //if (cb_market_Malaysia.Checked) targetMarkets_L.Add("Malaysia");
        //if (cb_market_Indonesia.Checked) targetMarkets_L.Add("Indonesia");
        //if (cb_market_Thailand.Checked) targetMarkets_L.Add("Thailand");
        //if (cb_market_Singapore.Checked) targetMarkets_L.Add("Singapore");
        //if (cb_market_China.Checked) targetMarkets_L.Add("China");
        //if (cb_market_Philippines.Checked) targetMarkets_L.Add("Philippines");
        //if (cb_market_Others.Checked) targetMarkets_L.Add("Other");
        string targetMarkets = string.Join(",", targetMarkets_L);

        string DeliveredProduct = string.Empty;
        if (rd_Instant.Checked) DeliveredProduct = "Instant";
        else if (rd_1_3_days.Checked) DeliveredProduct = "1-3 days";
        else if (rd_4_10_days.Checked) DeliveredProduct = "4-10 days";
        else if (rd_11_30_days.Checked) DeliveredProduct = "11-30 days";
        else if (rd_30_days.Checked) DeliveredProduct = "> 30 days";

        string PluginID = DD_Plugin.SelectedValue;
        #endregion

        #region D) Merchant Account
        string NotifyEmailFlag = DD_Notify.SelectedValue;
        string PaymentNotifyEmail = txt_IPNEMAIL.Text.Trim();

        string CSEmail = txt_cs_EmailAddr.Text.Trim();
        string CSMobile = txt_cs_MobileNumber.Text.Trim();
        #endregion

        #region E) Bank Account
        string BankName = txt_BankName.Text.Trim();
        string BankAccountNo = txt_BankAccountNo.Text.Trim();
        string BankAccountName = txt_BankAccountName.Text.Trim();
        string SWIFTCode = txt_SWIFTCode.Text.Trim();
        string BankAddress = txt_BankAddress.Text.Trim();
        string BankCountry = DD_BankCountry.SelectedItem.Text;
        #endregion

        #region F) Office
        string MCCCode = DD_MCCCode.SelectedValue;
        //decimal TxnLimit = (txt_TransactionLimit.Text.Trim() == "" ? 3000 : Convert.ToDecimal(txt_TransactionLimit.Text));
        decimal TxnLimit = (txt_TransactionLimit.Text.Trim() == "" ? 2500 : Convert.ToDecimal(txt_TransactionLimit.Text)); //Modified by Daniel 24 Apr 2019 . Change 3000 to 2500.
        if (string.IsNullOrEmpty(merchantID))
        {
            ServerSideMethod(BusinessRegName);
        }
        string MerchantID = merchantID;// txt_MerchantID.Text.Trim();
        // ADD START SUKI 20181128 [Add in showMerchantAddr, showMerchantLogo, IsAgent, default currency setting]
        int showMerchantAddr = (cb_showMerchantAddr.Checked == true) ? 1 : 0;
        int showMerchantLogo = (cb_showMerchantLogo.Checked == true) ? 1 : 0;
        int isAgent = (cb_Agent.Checked == true) ? 1 : 0;
        string defaultCurrency = DD_Currency.SelectedValue;
        // ADD E N D SUKI 20181128 [Add in showMerchantAddr, showMerchantLogo, IsAgent, default currency setting]
        #endregion

        #region LogJson
        var logObject = new
        {
            LoginUser = cus.UserID,
            aBusinessRegName = BusinessRegName,
            aBusinessRegNo = BusinessRegNo,
            aGSTNo = GSTNo,
            aTypeOfBusiness = TypeOfBusiness,
            aBusinessAdd1 = BusinessAdd1,
            aBusinessAdd2 = BusinessAdd2,
            aBusinessAdd3 = BusinessAdd3,
            aBusinessCity = BusinessCity,
            aBusinessState = BusinessState,
            aBusinessZipcode = BusinessZipcode,
            aBusinessCountry = BusinessCountry,
            aOfficeNumber = OfficeNumber,
            aACP1Name = ACP1Name,
            aACP1Mobile = ACP1Mobile,
            aACP1Email = ACP1Email,
            aACP2Name = ACP2Name,
            aACP2Mobile = ACP2Mobile,
            aACP2Email = ACP2Email,
            abillName = billName,
            abillMobile = billMobile,
            abillEmail = billEmail,
            bD_FullName_i = D_FullName_i,
            bD_ID_i = D_ID_i,
            bD_FullName_ii = D_FullName_ii,
            bD_ID_ii = D_ID_ii,
            bD_FullName_iii = D_FullName_iii,
            bD_ID_iii = D_ID_iii,
            bD_FullName_iv = D_FullName_iv,
            bD_ID_iv = D_ID_iv,
            bD_FullName_v = D_FullName_v,
            bD_ID_v = D_ID_v,
            bS_FullName_i = S_FullName_i,
            bS_ID_i = S_ID_i,
            bS_FullName_ii = S_FullName_ii,
            bS_ID_ii = S_ID_ii,
            bS_FullName_iii = S_FullName_iii,
            bS_ID_iii = S_ID_iii,
            bS_FullName_iv = S_FullName_iv,
            bS_ID_iv = S_ID_iv,
            bS_FullName_v = S_FullName_v,
            bS_ID_v = S_ID_v,
            cTradingName = TradingName,
            cWebSiteURL = WebSiteURL,
            cTypeOfProducts = TypeOfProducts,
            ctargetMarkets = targetMarkets,
            cDeliveredProduct = DeliveredProduct,
            cPluginID = PluginID,
            dNotifyEmailFlag = NotifyEmailFlag,
            dPaymentNotifyEmail = PaymentNotifyEmail,
            dCSEmail = CSEmail,
            dCSMobile = CSMobile,
            eBankName = BankName,
            eBankAccountNo = BankAccountNo,
            eBankAccountName = BankAccountName,
            eSWIFTCode = SWIFTCode,
            eBankAddress = BankAddress,
            eBankCountry = BankCountry,
            fMCCCode = MCCCode,
            fTxnLimit = TxnLimit,
            fMerchantID = MerchantID,
            // ADD START SUKI 20181128 [Add in showMerchantAddr, showMerchantLogo, IsAgent, default currency setting]
            fShowMerchantAddr = showMerchantAddr,
            fShowMerchantLogo = showMerchantLogo,
            fIsAgent = isAgent,
            fDefaultCurrency = defaultCurrency
            // ADD E N D SUKI 20181128 [Add in showMerchantAddr, showMerchantLogo, IsAgent, default currency setting]
        };
        var logJson = JsonConvert.SerializeObject(logObject);
        #endregion

        try
        {
            objLoggerII = logger.InitLogger();

            // CHG START SUKI 20181128 [Add in showMerchantAddr, showMerchantLogo, IsAgent, default currency setting]
            //bool addMerchant = mc.Do_RegisterNewMerchant(MerchantID, TradingName, BusinessRegName, MCCCode, ACP1Name, ACP1Mobile, ACP1Email, CSEmail, CSMobile, BusinessAdd1, BusinessAdd2,
            //            BusinessAdd3, int.Parse(BusinessCountry), BusinessCity, BusinessState, BusinessZipcode, WebSiteURL, int.Parse(PluginID), int.Parse(NotifyEmailFlag), PaymentNotifyEmail,
            //            BusinessRegNo, GSTNo, TypeOfBusiness, OfficeNumber, ACP2Name, ACP2Mobile, ACP2Email, billName, billMobile, billEmail, TypeOfProducts, targetMarkets, DeliveredProduct,
            //            cus.UserID, D_FullName_i, D_ID_i, D_FullName_ii, D_ID_ii, D_FullName_iii, D_ID_iii, D_FullName_iv, D_ID_iv, D_FullName_v, D_ID_v, S_FullName_i, S_ID_i, S_FullName_ii,
            //            S_ID_ii, S_FullName_iii, S_ID_iii, S_FullName_iv, S_ID_iv, S_FullName_v, S_ID_v, BankName, BankAccountName, BankAccountNo, SWIFTCode, BankAddress, BankCountry, TxnLimit);
            bool addMerchant = mc.Do_RegisterNewMerchant(MerchantID, TradingName, BusinessRegName, MCCCode, ACP1Name, ACP1Mobile, ACP1Email, CSEmail, CSMobile, BusinessAdd1, BusinessAdd2,
            BusinessAdd3, int.Parse(BusinessCountry), BusinessCity, BusinessState, BusinessZipcode, WebSiteURL, int.Parse(PluginID), int.Parse(NotifyEmailFlag), PaymentNotifyEmail,
            BusinessRegNo, GSTNo, TypeOfBusiness, OfficeNumber, ACP2Name, ACP2Mobile, ACP2Email, billName, billMobile, billEmail, TypeOfProducts, targetMarkets, DeliveredProduct,
            cus.UserID, D_FullName_i, D_ID_i, D_FullName_ii, D_ID_ii, D_FullName_iii, D_ID_iii, D_FullName_iv, D_ID_iv, D_FullName_v, D_ID_v, S_FullName_i, S_ID_i, S_FullName_ii,
            S_ID_ii, S_FullName_iii, S_ID_iii, S_FullName_iv, S_ID_iv, S_FullName_v, S_ID_v, BankName, BankAccountName, BankAccountNo, SWIFTCode, BankAddress, BankCountry, TxnLimit,
            showMerchantAddr, showMerchantLogo, isAgent, defaultCurrency);
            // CHG E N D SUKI 20181128 [Add in showMerchantAddr, showMerchantLogo, IsAgent, default currency setting]
            if (!addMerchant)
            {
                HiddenField1.Value = mc.err_msg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "AddMerchant(Error) - " + logJson + mc.system_err.ToString());
                //ADD START DANIEl 20190107 [Insert into AuditTrail]
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddMerchant), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + mc.system_err.ToString());
                //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
                return;
            }

            bool saveMerchantLogo = SaveMerchantLogo();
            if (saveMerchantLogo == false)
            {
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "AddMerchant(Error) - Save Merchant Logo - " + logJson + error_msg);
                //ADD START DANIEl 20190107 [Insert into AuditTrail]
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddMerchant), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + error_msg);
                //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
            }

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "AddMerchant - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.AddMerchant, 800000, "AddMerchant.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddMerchant), MethodBase.GetCurrentMethod().Name, logJson);
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]

            HiddenField1.Value = "Merchant Register Successful! Merchant ID: " + MerchantID + ". Please proceed to Add Channel.";
            HiddenField2.Value = AlertType.Success;
            Response.AddHeader("REFRESH", "5;URL=../MMGMT/AddChannel.aspx");
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "AddMerchant(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.AddMerchant, 800001, "AddMerchant.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddMerchant), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }

    private bool SaveMerchantLogo()
    {
        try
        {
            if (Request.Files["file-2"] != null)
            {
                HttpPostedFile MyFile = Request.Files["file-2"];
                if (MyFile.ContentLength > 0)
                {
                    Bitmap bmp = new Bitmap(MyFile.InputStream);
                    Bitmap resized = new Bitmap(bmp, new Size(bmp.Width, bmp.Height));

                    string path = WebConfigurationManager.AppSettings["SaveLogoPath"];
                    if (string.IsNullOrEmpty(path))
                        path = Server.MapPath("~/assets/img/merchantlogo/");

                    resized.Save(path + merchantID + "-logo.png", System.Drawing.Imaging.ImageFormat.Png);
                }
            }
            return true;
        }
        catch (Exception ex)
        {
            error_msg = ex.Message.ToString();
            return false;
        }
    }
}