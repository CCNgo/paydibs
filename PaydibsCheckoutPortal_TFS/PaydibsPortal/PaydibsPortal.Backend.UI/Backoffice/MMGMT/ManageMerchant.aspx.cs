﻿using LoggerII;
using Newtonsoft.Json;
using PaydibsPortal.BLL;
using PaydibsPortal.DAL;
using System;
using System.Reflection;
using System.Web.UI.WebControls;

public partial class Backoffice_MMGMT_ManageMerchant : System.Web.UI.Page
{
    MMGMTController mc = new MMGMTController();
    HomeController hc = null;
    CurrentUserStat cus = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        HiddenField1.Value = "";
        HiddenField2.Value = "";

        if (Session[SessionType.CurrentUser] == null)
        {
            Response.Redirect("../../Default.aspx");
            return;
        }

        cus = (CurrentUserStat)Session[SessionType.CurrentUser];
        hc = new HomeController(cus.DomainID);

        // ADD START SUKI 20190104 [Check permission on every page load]
        var permission = hc.LoadUserPermission(cus.UserID, Convert.ToInt16(OpsIDEnum.ManageMerchant));
        if (permission[0].IsUserAllowed == "0")
        {
            Response.Redirect("../../Backoffice/Dashboard.aspx");
            return;
        }
        // ADD E N D SUKI 20190104 [Check permission on every page load]

        if (!IsPostBack)
        {
            var merchant_L = hc.Do_GetMerchantList(99, 0);
            DD_merchant.DataSource = merchant_L;
            DD_merchant.DataTextField = "DomainDesc";
            DD_merchant.DataValueField = "DomainID";
            DD_merchant.DataBind();

            GenerateList(cus.DomainID);
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ManageMerchant), MethodBase.GetCurrentMethod().Name, "Page_Load Method");
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
    }

    protected void btn_Search_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        int sDomainID = Convert.ToInt32(DD_merchant.SelectedValue);

        var logObject = new
        {
            LoginUser = cus.UserID,
            DomainID = sDomainID
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            //CHG START DANIEL 20190110 [Insert into AuditTrail]
            //GenerateList(sDomainID);
            string errMsg = GenerateList(sDomainID);
            if (!string.IsNullOrEmpty(errMsg))
            {
                HiddenField1.Value = errMsg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            "ManageMerchant(Error) - " + logJson + mc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ManageMerchant), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + mc.system_err.ToString());
                return;
            }
            //CHG E N D DANIEL 20190110 [Insert into AuditTrail]

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "ManageMerchant - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.ManageMerchant, 800000, "ManageMerchant.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ManageMerchant), MethodBase.GetCurrentMethod().Name, logJson);
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "ManageMerchant(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.ManageMerchant, 800001, "ManageMerchant.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ManageMerchant), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }

    //CHG START DANIEL 20190110 [Change method type to string]
    //void GenerateList(int PDomainID)
    string GenerateList(int PDomainID)
    //CHG E N D DANIEL 20190110 [Change method type to string]
    {
        var data = mc.Do_GetMerchantList(PDomainID);

        //ADD START DANIEL 20190110 [If null, return MGMT controller error message]
        if (data == null)
            return mc.err_msg;
        //ADD E N D DANIEL 20190110 [If null, return MGMT controller error message]
        Repeater2.DataSource = data;
        Repeater2.DataBind();

        //ADD START DANIEL 20190110 [Add return string]
        return string.Empty;
        //ADD E N D DANIEL 20190110 [Add return string]
    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        var thisButton = (LinkButton)sender;
        var arg = thisButton.CommandArgument;
        var cmd = thisButton.CommandName;

        var logObject = new
        {
            LoginUser = cus.UserID,
            DomainID = arg,
            Status = cmd.ToString(),
            ModifiedBy = cus.UserID.ToString()
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            bool rst = false;
            switch (cmd)
            {
                case "Terminate":
                    rst = mc.Do_Domain_Update_Status(int.Parse(arg.ToString()), userStatus.Terminated, cus.UserID.ToString());
                    break;
                case "Active":
                    rst = mc.Do_Domain_Update_Status(int.Parse(arg.ToString()), userStatus.Active, cus.UserID.ToString());
                    break;
                //ADD START DANIEL 20190122 [New switch case "Suspend"]
                case "Suspend":
                    rst = mc.Do_Domain_Update_Status(int.Parse(arg.ToString()), userStatus.Suspended, cus.UserID.ToString());
                    break;
                //ADD E N D DANIEL 20190122 [New switch case "Suspend"]
                default:
                    break;
            }

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "ManageMerchant - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.ManageMerchant, 800000, "ManageMerchant.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ManageMerchant), MethodBase.GetCurrentMethod().Name, logJson);
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]

            if (!rst)
            {
                //CHG START DANIEL 20190110 [Insert into AuditTrail]
                //HiddenField1.Value = hc.err_msg;
                HiddenField1.Value = mc.err_msg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                          "ManageMerchant(Error) - " + logJson + mc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ManageMerchant), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + mc.system_err.ToString());
                //CHG E N D DANIEL 20190110 [Insert into AuditTrail]
                return;
            }

            //CHG START DANIEL 20190110 [Insert into AuditTrail]
            //GenerateList(Convert.ToInt32(DD_merchant.SelectedValue));
            string errMsg = GenerateList(Convert.ToInt32(DD_merchant.SelectedValue));
            if (!string.IsNullOrEmpty(errMsg))
            {
                HiddenField1.Value = errMsg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            "ManageMerchant(Error) - " + logJson + mc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ManageMerchant), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + mc.system_err.ToString());
                return;
            }
            //CHG E N D DANIEL 20190110 [Insert into AuditTrail]
            HiddenField1.Value = "Update successful!";
            HiddenField2.Value = AlertType.Success;
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "ManageMerchant(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.ManageMerchant, 800001, "ManageMerchant.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ManageMerchant), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }
}