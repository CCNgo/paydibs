﻿using LoggerII;
using Newtonsoft.Json;
using PaydibsPortal.BLL;
using PaydibsPortal.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.UI.WebControls;

public partial class Backoffice_MMGMT_AddResellerRate : System.Web.UI.Page
{
    MMGMTController mc = new MMGMTController();
    HomeController hc = null;
    CurrentUserStat cus = null;
    string error_msg = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        HiddenField1.Value = "";
        HiddenField2.Value = "";

        if (Session[SessionType.CurrentUser] == null)
        {
            //CHG START DANIEL 20190218 [Redirect to Default]
            //Response.Redirect("../");
            Response.Redirect("../../Default.aspx");
            //CHG E N D DANIEL 20190218 [Redirect to Default]
            return;
        }

        cus = (CurrentUserStat)Session[SessionType.CurrentUser];
        hc = new HomeController(cus.DomainID);

        var permission = hc.LoadUserPermission(cus.UserID, Convert.ToInt16(OpsIDEnum.AddResellerRate));

        if (permission[0].IsUserAllowed == "0")
        {
            Response.Redirect("../../Backoffice/Dashboard.aspx");
            return;
        }

        if (!IsPostBack)
        {
            //PMode = 4 : isReseller - True
            var merchant_L = hc.Do_GetMerchantList(1, 4);
            DD_merchant.DataSource = merchant_L;
            DD_merchant.DataTextField = "DomainDesc";
            DD_merchant.DataValueField = "DomainID";
            DD_merchant.DataBind();

            var currencyList = mc.Do_GetCurrencyCountry(1);
            DD_Currency.DataSource = currencyList;
            DD_Currency.DataTextField = "CurrCode";
            DD_Currency.DataValueField = "CurrCode";
            DD_Currency.DataBind();
            DD_Currency.SelectedValue = "MYR";

            GenerateResellerRate(Convert.ToInt32(DD_merchant.SelectedValue));
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddResellerRate), MethodBase.GetCurrentMethod().Name, "Page_Load Method");
        }
    }

    protected void btn_GetInfo_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        int sDomainID = Convert.ToInt32(DD_merchant.SelectedValue);

        clearInputField();
        DD_PymtMethod.Enabled = true;

        var logObject = new
        {
            LoginUser = cus.UserID,
            DomainID = sDomainID
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            string errMsg = GenerateResellerRate(sDomainID);
            if (!string.IsNullOrEmpty(errMsg))
            {
                HiddenField1.Value = errMsg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "AddResellerRate(Error) - " + logJson + mc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddResellerRate), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + mc.system_err.ToString());
                return;
            }

            //DEL START KENT LOONG 20190221[WRONG SP]
            //var channelList = mc.Do_Get_TerminalHost(sDomainID);

            //if (channelList == null)
            //{
            //    HiddenField1.Value = mc.err_msg;
            //    objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
            //               "AddResellerRate(Error) - " + logJson + mc.system_err.ToString());
            //    hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddResellerRate), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + mc.system_err.ToString());
            //    return;
            //}

            //var channelGroup = channelList.GroupBy(c => c.TypeValue).Select(g => g.First());
            //DEL E N D KENT LOONG 20190221[WRONG SP]

            //ADD START KENT LOONG 20190221[GET ALL PAYMENT METHOD]
            var pymtMethodList = mc.Do_GetCLPymtMethod();

            if (pymtMethodList == null)
            {
                HiddenField1.Value = mc.err_msg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "AddResellerRate(Error) - " + logJson + mc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddResellerRate), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + mc.system_err.ToString());
                return;
            }

            DD_PymtMethod.DataSource = pymtMethodList;
            DD_PymtMethod.DataTextField = "PymtMethodDesc";
            DD_PymtMethod.DataValueField = "PymtMethodCode";
            DD_PymtMethod.DataBind();
            DD_PymtMethod.Items.Insert(0, new ListItem("--SELECT--", ""));
            //ADD E N D KENT LOONG 20190221[GET ALL PAYMENT METHOD]

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                          "AddResellerRate - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.AddResellerRate, 800000, "AddResellerRate.aspx");
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddResellerRate), MethodBase.GetCurrentMethod().Name, logJson);
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "AddResellerRate(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.AddResellerRate, 800001, "AddResellerRate.aspx");
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddResellerRate), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }

    protected void OnChanged_DD_PymtMethod(object sender, EventArgs e)
    {
        var control = (DropDownList)sender;
        //DEL START KENT LOONG 20190221 [WRONG SP]
        //var channelList = mc.Do_Get_TerminalHost(Convert.ToInt32(DD_merchant.SelectedValue), DD_PymtMethod.SelectedValue);
        //DEL E N D KENT LOONG 20190221 [WRONG SP]

        //ADD START KENT LOONG 20190221 [GET ALL HOST BY PAYMENT METHOD]
        var hostList = mc.Do_Get_HostByType(DD_PymtMethod.SelectedValue);
        //ADD END KENT LOONG 20190221 [GET ALL HOST BY PAYMENT METHOD]
        if (control.SelectedIndex > 0)
        {
            DD_Channel.Enabled = true;
            btn_AddRate.Enabled = true;
            btn_AddRate.Enabled = false;
            txt_Rate.Text = string.Empty;
            txt_FixedRate.Text = string.Empty;

            DD_Channel.DataSource = hostList;
            DD_Channel.DataTextField = "HostDesc";
            DD_Channel.DataValueField = "HostID";
            DD_Channel.DataBind();
            DD_Channel.Items.Insert(0, new ListItem("--SELECT--", ""));
        }
        else
        {
            btn_AddRate.Enabled = false;
            DD_Channel.Enabled = false;
            DD_Channel.Items.Clear();
            txt_Rate.Text = string.Empty;
            txt_FixedRate.Text = string.Empty;
        }
    }

    protected void OnChanged_DD_Channel(object sender, EventArgs e)
    {
        var control = (DropDownList)sender;

        if (control.SelectedIndex > 0)
        {
            btn_AddRate.Enabled = true;
            txt_Rate.Text = string.Empty;
            txt_FixedRate.Text = string.Empty;
        }
        else
        {
            btn_AddRate.Enabled = false;
            txt_Rate.Text = string.Empty;
            txt_FixedRate.Text = string.Empty;
        }
    }

    protected void btn_AddRate_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        decimal fromAmt = 0;
        decimal toAmt = 0;
        string merchantID = DD_merchant.SelectedItem.Value;
        string paymentMethod = DD_PymtMethod.SelectedValue;

        int channel = !string.IsNullOrEmpty(DD_Channel.SelectedValue) ? Convert.ToInt32(DD_Channel.SelectedValue) : 0;
        DateTime startDate;
        double num;
        string inputRate = txt_Rate.Text.Trim();
        string inputFixedRate = txt_FixedRate.Text.Trim();
        DateTime effectiveDate = DateTime.ParseExact(txt_EffectiveDate.Text, "yyyy-MM-dd", null);
        decimal rate = 0;
        decimal fixedRate = 0;

        clearInputField();
        if (effectiveDate.Date == DateTime.Now.Date)
        {
            startDate = DateTime.Now;
        }
        else
        {
            startDate = effectiveDate;
        }
        if (double.TryParse(inputRate, out num))
        {
            rate = Convert.ToDecimal(inputRate);
        }
        else
        {
            HiddenField1.Value = "Please enter integer or decimal only.";
            return;
        }

        if (double.TryParse(inputFixedRate, out num))
        {
            fixedRate = Convert.ToDecimal(inputFixedRate);
        }
        else
        {
            HiddenField1.Value = "Please enter integer or decimal only";
            return;
        }

        string currency = DD_Currency.SelectedValue;

        var logObject = new
        {
            FromAmt = fromAmt,
            ToAmt = toAmt,
            PaymentMethod = paymentMethod,
            Channel = channel,
            StartDate = startDate,
            Rate = rate,
            FixedRate = fixedRate,
            Currency = currency
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            bool addResellerRate = false;
            if (!string.IsNullOrEmpty(inputRate) && !string.IsNullOrEmpty(inputFixedRate))
            {
                //Parameter isReseller = True
                addResellerRate = mc.Insert_MerchantRate(fromAmt, toAmt, merchantID, paymentMethod, channel, startDate, rate, fixedRate, currency, true);
            }
            else
            {
                addResellerRate = mc.Insert_MerchantRate(fromAmt, toAmt, merchantID, paymentMethod, channel, startDate, 0, 0, currency, true);
            }

            if (!addResellerRate)
            {
                HiddenField1.Value = mc.err_msg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "AddResellerRate(Error) - " + logJson + mc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddResellerRate), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + mc.system_err.ToString());
                return;
            }

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                              "AddResellerRate - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.AddResellerRate, 800000, "AddResellerRate.aspx");
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddResellerRate), MethodBase.GetCurrentMethod().Name, logJson);

            string errMsg = GenerateResellerRate(Convert.ToInt32(DD_merchant.SelectedValue));
            if (!string.IsNullOrEmpty(errMsg))
            {
                HiddenField1.Value = errMsg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "AddResellerRate(Error) - " + logJson + mc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddResellerRate), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + mc.system_err.ToString());
                return;
            }
            HiddenField1.Value = "Add Reseller Rate Successful!";
            HiddenField2.Value = AlertType.Success;
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                "AddResellerRate(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.AddResellerRate, 800001, "AddResellerRate.aspx");
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddResellerRate), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
        }
        finally
        {
            objLoggerII.Dispose();
        }
        clearInputField();
    }

    void clearInputField()
    {
        txt_Rate.Text = string.Empty;
        txt_FixedRate.Text = string.Empty;
        DD_Channel.Items.Clear();
        DD_Channel.Enabled = false;
        DD_PymtMethod.ClearSelection();
        btn_AddRate.Enabled = false;
    }

    string GenerateResellerRate(int PDomainID)
    {
        //PMode = 1: isReseller -True
        var RateData = mc.Do_Get_MerchantRateHost(PDomainID, 1);

        if (RateData == null)
            return mc.err_msg;

        RP_Rate.DataSource = ConvertRateToPercentage(RateData);
        RP_Rate.DataBind();

        var FutureRateData = mc.Do_Get_MerchantFutureRateHost(PDomainID, 1);
        if (FutureRateData == null)
            return mc.err_msg;
        //var FutureRate = FutureRateData.Select(fr => new { fr.PymtMethod,fr.HostName,fr.Rate,fr.FixedFee,fr.StartDate,fr.Currency}).ToArray();
        foreach (var data in FutureRateData)
        {
            data.Rate = data.Rate * 100;
        }
        RP_FutureRate.DataSource = FutureRateData;
        RP_FutureRate.DataBind();
        return string.Empty;
    }

    IEnumerable<MerchantRateHost> ConvertRateToPercentage(MerchantRateHost[] RateData)
    {
        return RateData.Select(r => new MerchantRateHost
        {
            PymtMethod = r.PymtMethod,
            HostName = r.HostName,
            StartDate = r.StartDate,
            Rate = r.Rate * 100,
            FixedFee = r.FixedFee,
            Currency = r.Currency
        });
    }

}