﻿using LoggerII;
using Newtonsoft.Json;
using PaydibsPortal.BLL;
using PaydibsPortal.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

public partial class Backoffice_MMGMT_ViewMerchantRate : System.Web.UI.Page
{
    MMGMTController mc = new MMGMTController();
    HomeController hc = null;
    CurrentUserStat cus = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        //ADD START DANIEL 201901 [Initialize HiddenField1 and 2]
        HiddenField1.Value = "";
        HiddenField2.Value = "";
        //ADD E N D DANIEL 201901 [Initialize HiddenField1 and 2]

        if (Session[SessionType.CurrentUser] == null)
        {
            Response.Redirect("../../Default.aspx");
            return;
        }

        cus = (CurrentUserStat)Session[SessionType.CurrentUser];
        hc = new HomeController(cus.DomainID);

        // ADD START SUKI 20190104 [Check permission on every page load]
        var permission = hc.LoadUserPermission(cus.UserID, Convert.ToInt16(OpsIDEnum.ViewMerchantRate));
        if (permission[0].IsUserAllowed == "0")
        {
            Response.Redirect("../../Backoffice/Dashboard.aspx");
            return;
        }
        // ADD E N D SUKI 20190104 [Check permission on every page load]

        if (!IsPostBack)
        {
            var merchant_L = hc.Do_GetMerchantList(1, 0);
            DD_merchant.DataSource = merchant_L;
            DD_merchant.DataTextField = "DomainDesc";
            DD_merchant.DataValueField = "DomainID";
            DD_merchant.DataBind();

            GenerateMerchantRate(Convert.ToInt32(DD_merchant.SelectedValue));
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ViewMerchantRate), MethodBase.GetCurrentMethod().Name, "Page_Load Method");
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
    }

    protected void btn_GetInfo_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        int sDomainID = Convert.ToInt32(DD_merchant.SelectedValue);

        var logObject = new
        {
            LoginUser = cus.UserID,
            DomainID = sDomainID
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            //CHG START DANIEL 20190110 [Insert into AuditTrail]
            //GenerateMerchantRate(sDomainID);
            string errMsg = GenerateMerchantRate(sDomainID);
            if (!string.IsNullOrEmpty(errMsg))
            {
                HiddenField1.Value = errMsg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                              "ViewMerchantRate(Error) - " + logJson + mc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ViewMerchantRate), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + mc.system_err.ToString());
            }
            //CHG E N d DANIEL 20190110 [Insert into AuditTrail]

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                          "ViewMerchantRate - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.ViewMerchantRate, 800000, "ViewMerchantRate.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ViewMerchantRate), MethodBase.GetCurrentMethod().Name, logJson);
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "ViewMerchantRate(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.ViewMerchantRate, 800001, "ViewMerchantRate.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ViewMerchantRate), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }

    //CHG START DANIEL 20190110 [Change methodvtype to string]
    //void GenerateMerchantRate(int PDomainID)
    string GenerateMerchantRate(int PDomainID)
    //CHG E N D DANIEL 20190110 [Change method type to string]
    {
        var RateData = mc.Do_Get_MerchantRateHost(PDomainID);

        //ADD START DANIEL 20190110 [If null, return MGMT controller error message]
        if (RateData == null)
            return mc.err_msg;
        //ADD E N D DANIEL 20190110 [If null, return MGMT controller error message]

        //CHG START DANIEL 20181211 [pass RateData to method ConvertRateToPercentage to return converted percentage rate]
        //RP_Rate.DataSource = RateData;

        //RP_Rate.DataSource = ConvertRateToPercentage(RateData);
        //CHG E N D DANIEL 20181211 [pass RateData to method ConvertRateToPercentage to return converted percentage rate]
        //RP_Rate.DataBind();
        //ADD START KENT LOONG 20190405 [add Domain checking]
        if (cus.DomainID == 1)
        {
            var FutureRateData = mc.Do_Get_MerchantFutureRateHost(PDomainID);
            foreach (var rate in FutureRateData)
            {
                rate.Rate = rate.Rate * 100;
                rate.redirectURL = "../MMGMT/EditMerchantRate.aspx?PKID=" + rate.PKID;
            }
            RP_Rate.DataSource = ConvertRateToPercentage(RateData);
            RP_Rate.DataBind();
            Repeater1.DataSource = FutureRateData;
            Repeater1.DataBind();
        }
        else
        {
            navviewFutureRatetab.Visible = false;
            foreach (var data in RateData)
            {
                if (data.HostName == "RHB MPGS" || data.HostName == "ABMB")
                    data.HostName = "Visa/MasterCard";
            }
            RP_Rate.DataSource = ConvertRateToPercentage(RateData);
            RP_Rate.DataBind();
        }
        //ADD E N D KENT LOONG 20190405 [add Domain checking]

        //ADD START DANIEL 20190110 [Add return string]
        return string.Empty;
        //ADD E N D DANIEL 20190110 [Add return string]
    }

    //ADD START DANIEL 20181211 [Add method ConvertRateToPercentage to convert decimal Rate to percentage]
    IEnumerable<MerchantRateHost> ConvertRateToPercentage(MerchantRateHost[] RateData)
    {
        return RateData.Select(r => new MerchantRateHost
        {
            PymtMethod = r.PymtMethod,
            HostName = r.HostName,
            StartDate = r.StartDate,
            Rate = r.Rate * 100,
            FixedFee = r.FixedFee,
            Currency = r.Currency
        });
    }
    //ADD E N D DANIEL 20181211 [Add method ConvertRateToPercentage to convert decimal Rate to percentage]
}