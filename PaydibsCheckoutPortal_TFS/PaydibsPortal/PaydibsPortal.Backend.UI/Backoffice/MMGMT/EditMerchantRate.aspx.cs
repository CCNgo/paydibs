﻿using LoggerII;
using Newtonsoft.Json;
using PaydibsPortal.BLL;
using PaydibsPortal.DAL;
using System;
using System.Reflection;

public partial class Backoffice_MMGMT_EditMerchantRate : System.Web.UI.Page
{
    MMGMTController mc = new MMGMTController();
    HomeController hc = null;
    CurrentUserStat cus = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        HiddenField1.Value = "";
        HiddenField2.Value = "";

        if (Session[SessionType.CurrentUser] == null)
        {   
            Response.Redirect("../../Default.aspx");
            return;
        }

        cus = (CurrentUserStat)Session[SessionType.CurrentUser];
        hc = new HomeController(cus.DomainID);

        var permission = hc.LoadUserPermission(cus.UserID, Convert.ToInt16(OpsIDEnum.ViewMerchantRate));
        if (permission[0].IsUserAllowed == "0")
        {
            Response.Redirect("../../Backoffice/Dashboard.aspx");
            return;
        }

        int sDomainID = Convert.ToInt32(Request.QueryString["PKID"]);
        if (sDomainID < 0)
        {
            Response.Redirect("../../Backoffice/Dashboard.aspx");
            return;
        }
        if (!IsPostBack)
        {
            MerchantFutureRateHost Rate = mc.Do_Get_MerchantFutureRateBYPKID(sDomainID);
            if (Rate == null)
            {
                HiddenField1.Value = "Invalid PKID";
                return;
            }
            //value to show
            DD_merchant.Text = Rate.DomainDesc + "(" + Rate.MerchantID + ")";
            DD_PymtMethod.Text = Rate.PymtMethod;
            DD_Channel.Text = Rate.HostName;
            txt_Rate.Text = (Rate.Rate * 100).ToString();
            txt_FixedRate.Text = Rate.FixedFee.ToString();
            txt_StartDate.Text = Rate.StartDate.ToString();
            DD_Currency.Text = Rate.Currency;
            btn_AddRate.Enabled = true;

            //Hidden value
            PKIDHidden.Value = Rate.PKID;
            MerchantIdHidden.Value = Rate.MerchantID;
            PymtMethodCodeHidden.Value = Rate.PymtMethodCode;
            HostIDHidden.Value = Rate.HostID;
            RateHidden.Value = Rate.Rate.ToString();
            FixedFeeHidden.Value = Rate.FixedFee.ToString();
            StartDateHidden.Value = Rate.StartDate.ToString();
            CurrencyHidden.Value = Rate.Currency;

            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditMerchantRate), MethodBase.GetCurrentMethod().Name, "Page_Load Method");
        }
    }

    protected void btn_GetUpdate_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();
        object logJson = null;

        try
        {
            bool validUpdate = false;
            int pkid = int.Parse(PKIDHidden.Value);
            decimal rate = Convert.ToDecimal(txt_Rate.Text);
            rate = rate / 100;
            decimal fixedFee = Convert.ToDecimal(txt_FixedRate.Text);
            var check = mc.Do_Get_MerchantFutureRateBYPKID(pkid);
            if (check == null)
            {
                HiddenField1.Value = "Invalid PKID";
            }
            else
            {
                validUpdate = true;
                if (check.MerchantID != MerchantIdHidden.Value)
                    validUpdate = false;
                if (check.PymtMethodCode != PymtMethodCodeHidden.Value)
                    validUpdate = false;
                if (check.HostID != HostIDHidden.Value)
                    validUpdate = false;
                if (check.StartDate.ToString() != StartDateHidden.Value)
                    validUpdate = false;
                if (check.Currency != CurrencyHidden.Value)
                    validUpdate = false;
                if (!validUpdate)
                    HiddenField1.Value = "Unable to update please contact admin.";
            }
            var logObject = new
            {
                Pkid = pkid,
                Rate = rate,
                FixedFee = fixedFee
            };
            logJson = JsonConvert.SerializeObject(logObject);
            objLoggerII = logger.InitLogger();

            if (validUpdate == true && rate == check.Rate && fixedFee == check.FixedFee)
            {
                HiddenField1.Value = "No Changes.";
                validUpdate = false;
            }

            if (validUpdate)
            {
                var updateMerchantFutureRate = mc.Do_Update_MerchantFutureRateBYPKID(pkid, rate, fixedFee);
                if (!updateMerchantFutureRate)
                {
                    HiddenField1.Value = mc.err_msg;
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "EditMerchantRate(Error) - " + logJson + mc.system_err.ToString());
                    hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditMerchantRate), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + mc.system_err.ToString());
                    return;
                }
                else
                {
                    HiddenField1.Value = "Add Merchant Future Rate Successful!";
                    HiddenField2.Value = AlertType.Success;
                }
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "EditMerchantRate - " + logJson);
                hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.EditMerchantRate, 800000, "EditMerchantRate.aspx");
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditMerchantRate), MethodBase.GetCurrentMethod().Name, logJson.ToString());
            }
            else
            {
                var errorlogObject = new
                {
                    Pkid = pkid,
                    Rate = rate,
                    FixedFee = fixedFee,
                    ErrorMessage = HiddenField1.Value.ToString()
                };
                logJson = JsonConvert.SerializeObject(errorlogObject);
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(), "EditMerchantRate - " + logJson);
                hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.EditMerchantRate, 800000, "EditMerchantRate.aspx");
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditMerchantRate), MethodBase.GetCurrentMethod().Name, logJson.ToString());
            }

        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                "EditMerchantRate(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.EditMerchantRate, 800001, "EditMerchantRate.aspx");
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditMerchantRate), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }
}