﻿using LoggerII;
using Newtonsoft.Json;
using PaydibsPortal.BLL;
using PaydibsPortal.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

public partial class Backoffice_MMGMT_ViewResellerRate : System.Web.UI.Page
{
    MMGMTController mc = new MMGMTController();
    HomeController hc = null;
    CurrentUserStat cus = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        HiddenField1.Value = "";
        HiddenField2.Value = "";

        if (Session[SessionType.CurrentUser] == null)
        {
            Response.Redirect("../../Default.aspx");
            return;
        }

        cus = (CurrentUserStat)Session[SessionType.CurrentUser];
        hc = new HomeController(cus.DomainID);

        var permission = hc.LoadUserPermission(cus.UserID, Convert.ToInt16(OpsIDEnum.ViewResellerRate));
        if (permission[0].IsUserAllowed == "0")
        {
            Response.Redirect("../../Backoffice/Dashboard.aspx");
            return;
        }

        if (!IsPostBack)
        {
            //Parameter PMode = 4 : Show Reseller List
            var merchant_L = hc.Do_GetMerchantList(1, 4);
            DD_merchant.DataSource = merchant_L;
            DD_merchant.DataTextField = "DomainDesc";
            DD_merchant.DataValueField = "DomainID";
            DD_merchant.DataBind();

            GenerateResellerRate(Convert.ToInt32(DD_merchant.SelectedValue));
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ViewResellerRate), MethodBase.GetCurrentMethod().Name, "Page_Load Method");
        }
    }

    protected void btn_GetInfo_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        int sDomainID = Convert.ToInt32(DD_merchant.SelectedValue);

        var logObject = new
        {
            LoginUser = cus.UserID,
            DomainID = sDomainID
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            string errMsg = GenerateResellerRate(sDomainID);
            if (!string.IsNullOrEmpty(errMsg))
            {
                HiddenField1.Value = errMsg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                              "ViewResellerRate(Error) - " + logJson + mc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ViewResellerRate), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + mc.system_err.ToString());
            }

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                          "ViewResellerRate - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.ViewResellerRate, 800000, "ViewResellerRate.aspx");
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ViewResellerRate), MethodBase.GetCurrentMethod().Name, logJson);

        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "ViewResellerRate(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.ViewResellerRate, 800001, "ViewResellerRate.aspx");
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ViewResellerRate), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }

    string GenerateResellerRate(int PDomainID)
    {
        var RateData = mc.Do_Get_MerchantRateHost(PDomainID, 1);

        if (RateData == null)
            return mc.err_msg;
        if (cus.DomainID == 1)
        {
            RP_Rate.DataSource = ConvertRateToPercentage(RateData);
            RP_Rate.DataBind();
        }
        else
        {
            foreach(var data in RateData)
            {
                if(data.HostName == "RHB MPGS" || data.HostName == "ABMB")
                    data.HostName = "Visa/MasterCard";
            }
            RP_Rate.DataSource = ConvertRateToPercentage(RateData);
            RP_Rate.DataBind();
        }
        return string.Empty;
    }

    IEnumerable<MerchantRateHost> ConvertRateToPercentage(MerchantRateHost[] RateData)
    {
        return RateData.Select(r => new MerchantRateHost
        {
            PymtMethod = r.PymtMethod,
            HostName = r.HostName,
            StartDate = r.StartDate,
            Rate = r.Rate * 100,
            FixedFee = r.FixedFee,
            Currency = r.Currency
        });
    }
}