﻿using GemBox.Spreadsheet;
using Kenji.Apps;
using LoggerII;
using Newtonsoft.Json;
using PaydibsPortal.BLL;
using PaydibsPortal.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Web.Script.Serialization;

public partial class Backoffice_TXN_TransactionsFeeReport : System.Web.UI.Page
{
    HomeController hc = null;
    TXNController tc = null;
    CurrentUserStat cus = null;
    LinqTool linq = new LinqTool();
    MMGMTController mc = new MMGMTController();

    protected void Page_Load(object sender, EventArgs e)
    {
        HiddenField1.Value = "";
        HiddenField2.Value = "";
        if (Session[SessionType.CurrentUser] == null)
        {
            Response.Redirect("../Dashboard.aspx");
            return;
        }

        cus = (CurrentUserStat)Session[SessionType.CurrentUser];
        hc = new HomeController(cus.DomainID);
        tc = new TXNController(cus.DomainID);
        var permission = hc.LoadUserPermission(cus.UserID, Convert.ToInt16(OpsIDEnum.TransactionsFeeReport));
        if (permission[0].IsUserAllowed == "0")
        {
            Response.Redirect("../../Backoffice/Dashboard.aspx");
            return;
        }

        if (!IsPostBack)
        {
            var merchant_L = hc.Do_GetMerchantList(1, 0);
            DD_merchant.DataSource = merchant_L;
            DD_merchant.DataTextField = "DomainDesc";
            DD_merchant.DataValueField = "DomainID";
            DD_merchant.DataBind();

            DateTime sSince = Convert.ToDateTime(TB_Since.Text.Trim() + " 00:00:00");
            DateTime sTo = Convert.ToDateTime(TB_To.Text.Trim() + " 23:59:59");
            TB_Since.Text = sSince.ToString("yyyy-MM-dd");
            TB_To.Text = sTo.ToString("yyyy-MM-dd");

            //ADD START DANIEL 20190313 [Bind data for CurrencyCode]
            var currencyList = mc.Do_GetCurrencyCountry(1);
            DD_Currency.DataSource = currencyList;
            DD_Currency.DataTextField = "CurrCode";
            DD_Currency.DataValueField = "CurrCode";
            DD_Currency.DataBind();
            DD_Currency.SelectedValue = "MYR";
            //ADD E N D DANIEL 20190313 [Bind data for CurrencyCode]

            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.TransactionsFeeReport), MethodBase.GetCurrentMethod().Name, "Page_Load Method");
        }
    }

    protected void btn_GetReport_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        int sDomainID = Convert.ToInt32(DD_merchant.SelectedValue);
        DateTime sSince = Convert.ToDateTime(TB_Since.Text.Trim() + " 00:00:00");
        DateTime sTo = Convert.ToDateTime(TB_To.Text.Trim() + " 00:00:00");

        var logObject = new
        {
            LoginUser = cus.UserID,
            DomainID = sDomainID,
            Since = sSince,
            To = sTo,
            //ADD START DANIEL 20190313 [Add Currency value]
            Currency = DD_Currency.SelectedValue
            //ADD E N D DANIEL 20190313 [Add Currency value]
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            //CHG START DANIEL 20190313 [Add parameter currency code]
            //string err_msg = GenerateTransactionsFeeReport(sSince, sTo, sDomainID);
            string err_msg = GenerateTransactionsFeeReport(sSince, sTo, sDomainID, logObject.Currency);
            //CHG E N D DANIEL 20190313 [Add parameter currency code]
            if (!(string.IsNullOrEmpty(err_msg)))
            {
                HiddenField1.Value = err_msg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                       "TransactionsFeeReport(Error) - " + logJson + tc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.TransactionsFeeReport), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + tc.system_err.ToString());
                return;
            }

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "TransactionsFeeReport - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.TransactionsFeeReport, 800000, "TransactionsFeeReport.aspx");
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.TransactionsFeeReport), MethodBase.GetCurrentMethod().Name, logJson);
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "TransactionsFeeReport(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.TransactionsFeeReport, 800001, "TransactionsFeeReport.aspx");
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.TransactionsFeeReport), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }

    protected void btn_Download_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        int sDomainID = Convert.ToInt32(DD_merchant.SelectedValue);
        string sDomainName = DD_merchant.SelectedItem.Text;
        DateTime sSince = Convert.ToDateTime(TB_Since.Text.Trim() + " 00:00:00");
        DateTime sTo = Convert.ToDateTime(TB_To.Text.Trim() + " 00:00:00");

        var logObject = new
        {
            LoginUser = cus.UserID,
            DomainID = sDomainID,
            DomainName = sDomainName,
            Since = sSince,
            To = sTo,
            //ADD START DANIEL 20190313 [Add Currency value]
            Currency = DD_Currency.SelectedValue
            //ADD E N D DANIEL 20190313 [Add Currency value]
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            //CHG START DANIEL 20190313 [Add parameter Currency]
            //Finance_Report[] data = tc.Do_TransactionsFeeReport(sSince, sTo, sDomainID);
            //Finance_Report[] data = tc.Do_TransactionsFeeReport(sSince, sTo, sDomainID, logObject.Currency);
            TxnFeeReport[] data = tc.Do_TransactionsFeeReport(sSince, sTo, sDomainID, logObject.Currency); //Modified by Daniel 12 Apr 2019 . Change model TxnFeeReport.
            //CHG E N D DANIEL 20190313 [Add parameter Currency]

            if (data == null)
            {
                HiddenField1.Value = tc.err_msg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                       "TransactionsFeeReport(Error) - " + logJson + tc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.TransactionsFeeReport), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + tc.system_err.ToString());
                return;
            }

            foreach (var d in data)
            {
                if (!string.IsNullOrEmpty(d.CardPAN))
                {
                    d.CardPAN = DecryptAES256(d.CardPAN);
                    d.HostDesc = GetCCChannelName(d.CardPAN, d.HostDesc);
                }
            }

            ExcelFile ef = new ExcelFile();
            string PhysicalPath = Server.MapPath("Report/Report.xls");
            string SaveAsFileName = "TransactionsFeeReport_Merchant_" + sDomainName + "_" + sSince.ToString("dd-MM-yyyy") + "-" + sTo.ToString("dd-MM-yyyy") + ".xls";
            string SavePhysicalPath = Server.MapPath("Report/" + SaveAsFileName);
            ef.LoadXls(PhysicalPath);

            DataTable dt = linq.LINQToDataTable(data);
            ExcelWorksheet ws = ef.Worksheets[0];
            ws.InsertDataTable(dt, "A1", true);

            ef.SaveXls(SavePhysicalPath);

            System.IO.FileInfo file = new System.IO.FileInfo(Server.MapPath("Report/" + SaveAsFileName));
            if (file.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                Response.AddHeader("Content-Length", file.Length.ToString());
                Response.ContentType = "application/vnd.ms-excel";
                Response.WriteFile(file.FullName);

                Response.Flush();
                file.Delete();
                Response.End();
            }

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "TransactionsFeeReportDownload - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.TransactionsFeeReport, 800000, "TransactionsFeeReport.aspx");
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.TransactionsFeeReport), MethodBase.GetCurrentMethod().Name, logJson);
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "TransactionsFeeReportDownload(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.TransactionsFeeReport, 800001, "TransactionsFeeReport.aspx");
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.TransactionsFeeReport), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }

    //CHG START DANIEL 20190313 [Add parameter PCurrencyCode]
    //string GenerateTransactionsFeeReport(DateTime PSince, DateTime PTo, int PDomainID)
    string GenerateTransactionsFeeReport(DateTime PSince, DateTime PTo, int PDomainID, string PCurrencyCode)
    //CHG E N D DANIEL 20190313 [Add parameter PCurrencyCode]
    {
        try
        {
            //CHG START DANIEL 20190313 [Add parameter PCurrencyCode]
            //Finance_Report[] data = tc.Do_TransactionsFeeReport(PSince, PTo, PDomainID);
            //Finance_Report[] data = tc.Do_TransactionsFeeReport(PSince, PTo, PDomainID, PCurrencyCode); 
            TxnFeeReport[] data = tc.Do_TransactionsFeeReport(PSince, PTo, PDomainID, PCurrencyCode); //Modified by Daniel 12 Apr 2019 . Change model TxnFeeReport.
            //CHG E N D DANIEL 20190313 [Add parameter PCurrencyCode]

            Repeater2.DataSource = data;
            Repeater2.DataBind();

            if (data == null)
                return tc.err_msg;

            return string.Empty;
        }
        catch (Exception ex)
        {
            return ex.Message.ToString();
        }
    }

    private static readonly Encoding encoding = Encoding.UTF8;
    public string DecryptAES256(string CCNumber)
    {
        if (CCNumber.Length < 20 || string.IsNullOrEmpty(CCNumber))
        {
            return CCNumber;
        }
        try
        {
            RijndaelManaged aes = new RijndaelManaged();
            aes.KeySize = 256;
            aes.BlockSize = 128;
            aes.Padding = PaddingMode.PKCS7;
            aes.Mode = CipherMode.CBC;
            aes.Key = encoding.GetBytes(System.Configuration.ConfigurationManager.AppSettings["AES256Key"]);

            // Base 64 decode
            byte[] base64Decoded = Convert.FromBase64String(CCNumber);
            string base64DecodedStr = encoding.GetString(base64Decoded);

            // JSON Decode base64Str
            JavaScriptSerializer ser = new JavaScriptSerializer();
            var payload = ser.Deserialize<Dictionary<string, string>>(base64DecodedStr);

            aes.IV = Convert.FromBase64String(payload["iv"]);

            ICryptoTransform AESDecrypt = aes.CreateDecryptor(aes.Key, aes.IV);
            byte[] buffer = Convert.FromBase64String(payload["value"]);

            string DecryptCCNumber = encoding.GetString(AESDecrypt.TransformFinalBlock(buffer, 0, buffer.Length));
            string MaskedCCNumber = string.Empty;
            if (!string.IsNullOrEmpty(DecryptCCNumber))
            {
                MaskedCCNumber = DecryptCCNumber.Substring(0, 6) + "".PadRight(DecryptCCNumber.Length - 6 - 4, 'X') + DecryptCCNumber.Substring(DecryptCCNumber.Length - 4, 4);
            }
            return MaskedCCNumber;
        }
        catch (Exception e)
        {
            throw new Exception("Error decrypting: " + e.Message);
        }
    }
    public string GetCCChannelName(string CCNumber, string channelName)
    {
        if (channelName != "VISA" && channelName != "MASTER" && channelName != "JCB" && channelName != "AMEX" && channelName != "Diners"
            && string.IsNullOrEmpty(CCNumber) && string.IsNullOrWhiteSpace(CCNumber))
        {
            return channelName;
        }

        CCNumber = DecryptAES256(CCNumber);
        string ChannelName = string.Empty;

        if (string.IsNullOrEmpty(CCNumber) || string.IsNullOrWhiteSpace(CCNumber)) //Due to empty after Decryption
        {
            return channelName;
        }

        if (CCNumber.Substring(0, 1) == "4")
        {
            ChannelName = "VISA";
        }
        else if (CCNumber.Substring(0, 1) == "5" || CCNumber.Substring(0, 1) == "2")
        {
            ChannelName = "MASTER";
        }
        else if (CCNumber.Substring(0, 2) == "35")
        {
            ChannelName = "JCB";
        }
        else if (CCNumber.Substring(0, 1) == "34" || CCNumber.Substring(0, 2) == "37")
        {
            ChannelName = "AMEX";
        }
        else if (CCNumber.Substring(0, 2) == "36" || CCNumber.Substring(0, 2) == "38")
        {
            ChannelName = "Diners";
        }
        else
        {
            ChannelName = "UnknownCC";
        }

        return ChannelName;
        //--SET @sz_SelectCC = @sz_SelectCC + '(CASE WHEN LEFT(txn.CardPAN, 1) = 4 THEN ''VISA'' '
        //--SET @sz_SelectCC = @sz_SelectCC + 'WHEN (LEFT(txn.CardPAN, 1) = 5 OR LEFT(txn.CardPAN, 1) = 2) THEN ''MASTER'' '
        //--SET @sz_SelectCC = @sz_SelectCC + 'WHEN (LEFT(txn.CardPAN, 2) = 35) THEN ''JCB'' '
        //--SET @sz_SelectCC = @sz_SelectCC + 'WHEN (LEFT(txn.CardPAN, 2) = 34 OR LEFT(txn.CardPAN, 2) = 37) THEN ''AMEX''  '
        //--SET @sz_SelectCC = @sz_SelectCC + 'WHEN (LEFT(txn.CardPAN, 2) = 36 OR LEFT(txn.CardPAN, 2) = 38) THEN ''Diners'' '
    }
}
