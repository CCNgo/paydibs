﻿using GemBox.Spreadsheet;
using Kenji.Apps;
using LoggerII;
using Newtonsoft.Json;
using PaydibsPortal.BLL;
using PaydibsPortal.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Backoffice_TXN_ResellerMarginReport : System.Web.UI.Page
{
    HomeController hc = null;
    TXNController tc = null;
    CurrentUserStat cus = null;
    LinqTool linq = new LinqTool();
    MMGMTController mc = new MMGMTController();

    protected void Page_Load(object sender, EventArgs e)
    {
        HiddenField1.Value = "";
        HiddenField2.Value = "";
        if (Session[SessionType.CurrentUser] == null)
        {
            Response.Redirect("../Dashboard.aspx");
            return;
        }

        cus = (CurrentUserStat)Session[SessionType.CurrentUser];
        hc = new HomeController(cus.DomainID);
        tc = new TXNController(cus.DomainID);

        var permission = hc.LoadUserPermission(cus.UserID, Convert.ToInt16(OpsIDEnum.ResellerMarginReport));
        if (permission[0].IsUserAllowed == "0")
        {
            Response.Redirect("../../Backoffice/Dashboard.aspx");
            return;
        }

        if (!IsPostBack)
        {
            var merchant_L = hc.Do_GetMerchantList(1, 4);
            DD_merchant.DataSource = merchant_L;
            DD_merchant.DataTextField = "DomainDesc";
            DD_merchant.DataValueField = "DomainID";
            DD_merchant.DataBind();
            DateTime sSince = Convert.ToDateTime(TB_Since.Text.Trim() + " 00:00:00");
            DateTime sTo = Convert.ToDateTime(TB_To.Text.Trim() + " 23:59:59");
            TB_Since.Text = sSince.ToString("yyyy-MM-dd");
            TB_To.Text = sTo.ToString("yyyy-MM-dd");

            //ADD START DANIEL 20190313 [Bind data for CurrencyCode]
            var currencyList = mc.Do_GetCurrencyCountry(1);
            DD_Currency.DataSource = currencyList;
            DD_Currency.DataTextField = "CurrCode";
            DD_Currency.DataValueField = "CurrCode";
            DD_Currency.DataBind();
            DD_Currency.SelectedValue = "MYR";
            //ADD E N D DANIEL 20190313 [Bind data for CurrencyCode]

            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ResellerMarginReport), MethodBase.GetCurrentMethod().Name, "Page_Load Method");
        }
    }

    protected void btn_GetReport_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        int sDomainID = Convert.ToInt32(DD_merchant.SelectedValue);
        DateTime sSince = Convert.ToDateTime(TB_Since.Text.Trim() + " 00:00:00");
        DateTime sTo = Convert.ToDateTime(TB_To.Text.Trim() + " 23:59:59");

        var logObject = new
        {
            LoginUser = cus.UserID,
            DomainID = sDomainID,
            Since = sSince,
            To = sTo,
            //ADD START DANIEL 20190313 [Add Currency value]
            Currency = DD_Currency.SelectedValue
            //ADD E N D DANIEL 20190313 [Add Currency value]
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            //CHG START DANIEL 20190313 [Add parameter CurrencyCode]
            //string err_msg = GenerateResellerMarginReport(sSince, sTo, sDomainID);
            string err_msg = GenerateResellerMarginReport(sSince, sTo, sDomainID, logObject.Currency);
            //CHG E N D DANIEL 20190313 [Add parameter CurrencyCode]

            if (!(string.IsNullOrEmpty(err_msg)))
            {
                HiddenField1.Value = err_msg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                       "ResellerMarginReport(Error) - " + logJson + tc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ResellerMarginReport), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + tc.system_err.ToString());
                return;
            }

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "ResellerMarginReport - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.ResellerMarginReport, 800000, "ResellerMarginReport.aspx");
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ResellerMarginReport), MethodBase.GetCurrentMethod().Name, logJson);
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "ResellerMarginReport(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.ResellerMarginReport, 800001, "ResellerMarginReport.aspx");
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ResellerMarginReport), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }

    protected void btn_Download_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        int sDomainID = Convert.ToInt32(DD_merchant.SelectedValue);
        string sDomainName = DD_merchant.SelectedItem.Text;
        DateTime sSince = Convert.ToDateTime(TB_Since.Text.Trim() + " 00:00:00");
        DateTime sTo = Convert.ToDateTime(TB_To.Text.Trim() + " 23:59:59");

        var logObject = new
        {
            LoginUser = cus.UserID,
            DomainID = sDomainID,
            DomainName = sDomainName,
            Since = sSince,
            To = sTo,
            //ADD START DANIEL 20190313 [Add Currency value]
            Currency = DD_Currency.SelectedValue
            //ADD E N D DANIEL 20190313 [Add Currency value]
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            //CHG START DANIEL 20190313 [Add Currency value]
            //Finance_Report[] data = tc.Do_TransactionsFeeReport(sSince, sTo, sDomainID);
            //Commented By Daniel 11 Apr 2019. 
            //Finance_Report[] data = tc.Do_TxnRateReseller(sSince, sTo, sDomainID, logObject.Currency);
            //CHG E N D DANIEL 20190313 [Add Currency value]

            //Added By Daniel 11 Apr 2019.
            ResellerMarginReport[] data = tc.Do_TxnRateReseller(sSince, sTo, sDomainID, logObject.Currency);

            if (data == null)
            {
                HiddenField1.Value = tc.err_msg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                       "ResellerMarginReport(Error) - " + logJson + tc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ResellerMarginReport), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + tc.system_err.ToString());
                return;
            }

            ExcelFile ef = new ExcelFile();
            string PhysicalPath = Server.MapPath("Report/Report.xls");
            string SaveAsFileName = "ResellerMarginReport_Merchant_" + sDomainName + "_" + sSince.ToString("dd-MM-yyyy") + "-" + sTo.ToString("dd-MM-yyyy") + ".xls";
            string SavePhysicalPath = Server.MapPath("Report/" + SaveAsFileName);
            ef.LoadXls(PhysicalPath);

            DataTable dt = linq.LINQToDataTable(data);
            ExcelWorksheet ws = ef.Worksheets[0];
            ws.InsertDataTable(dt, "A1", true);

            ef.SaveXls(SavePhysicalPath);

            System.IO.FileInfo file = new System.IO.FileInfo(Server.MapPath("Report/" + SaveAsFileName));
            if (file.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                Response.AddHeader("Content-Length", file.Length.ToString());
                Response.ContentType = "application/vnd.ms-excel";
                Response.WriteFile(file.FullName);

                Response.Flush();
                file.Delete();
                Response.End();
            }

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "ResellerMarginReportDownload - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.ResellerMarginReport, 800000, "ResellerMarginReport.aspx");
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ResellerMarginReport), MethodBase.GetCurrentMethod().Name, logJson);
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "ResellerMarginReportDownload(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.ResellerMarginReport, 800001, "ResellerMarginReport.aspx");
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ResellerMarginReport), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }

    //CHG START DANIEL 20190313 [Add parameter PCurrencyCode Currency]
    //string GenerateResellerMarginReport(DateTime PSince, DateTime PTo, int PDomainID)
    string GenerateResellerMarginReport(DateTime PSince, DateTime PTo, int PDomainID , string PCurrencyCode)
    //CHG E N D DANIEL 20190313 [Add parameter PCurrencyCode Currency]
    {
        try
        {
            //CHG START DANIEL 20190313 [Add parameter PCurrencyCode Currency]
            //Finance_Report[] data = tc.Do_TxnRateReseller(PSince, PTo, PDomainID);
            //Finance_Report[] data = tc.Do_TxnRateReseller(PSince, PTo, PDomainID, PCurrencyCode);
            //CHG E N D DANIEL 20190313 [Add parameter PCurrencyCode Currency]

            //Added By Daniel 11 Apr 2019.
            ResellerMarginReport[] data = tc.Do_TxnRateReseller(PSince, PTo, PDomainID, PCurrencyCode);

            Repeater2.DataSource = data;
            Repeater2.DataBind();

            if (data == null)
                return tc.err_msg;

            return string.Empty;
        }
        catch (Exception ex)
        {
            return ex.Message.ToString();
        }
    }
}
