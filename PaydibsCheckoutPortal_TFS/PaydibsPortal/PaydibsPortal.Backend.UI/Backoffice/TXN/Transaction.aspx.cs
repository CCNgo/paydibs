﻿using System;
using System.Data;
using PaydibsPortal.BLL;
using PaydibsPortal.DAL;
using GemBox.Spreadsheet;
using Kenji.Apps;
using LoggerII;
using Newtonsoft.Json;
using System.Web.Configuration;
using System.Web.UI.WebControls;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Text;

public partial class Backoffice_Transaction : System.Web.UI.Page
{
    HomeController hc = null;
    TXNController tc = null;
    CurrentUserStat cus = null;
    LinqTool linq = new LinqTool();
    MMGMTController mc = new MMGMTController();

    protected void Page_Load(object sender, EventArgs e)
    {
        //ADD START DANIEL 20190104 [Initialize hidden fields]
        HiddenField1.Value = "";
        HiddenField2.Value = "";
        //ADD START DANIEL 20190104 [Initialize hidden fields]

        if (Session[SessionType.CurrentUser] == null)
        {
            Response.Redirect("../../Default.aspx");
            return;
        }

        cus = (CurrentUserStat)Session[SessionType.CurrentUser];
        hc = new HomeController(cus.DomainID);
        tc = new TXNController(cus.DomainID);

        // ADD START SUKI 20190104 [Check permission on every page load]
        var permission = hc.LoadUserPermission(cus.UserID, Convert.ToInt16(OpsIDEnum.Transaction));
        if (permission[0].IsUserAllowed == "0")
        {
            Response.Redirect("../../Backoffice/Dashboard.aspx");
            return;
        }
        // ADD E N D SUKI 20190104 [Check permission on every page load]

        if (!IsPostBack)
        {
            //CHG START DANIEL 20190404 [Cater for merchant that has sub-merchants but not reseller]
            //var merchant_L = hc.Do_GetMerchantList(1, 1);
            CMS_Domain[] merchant_L = null;
            bool isMainMerchant = tc.Do_Get_CL_Plugin_IsReseller(cus.DomainID);

            if(isMainMerchant)
                //Display Main merchant and sub- merchants(Not reseller)
                merchant_L = hc.Do_GetMerchantList(1, 0);
            else
                //If Reseller and normal merchant. Display only own trading name
                merchant_L = hc.Do_GetMerchantList(1, 1);

            //CHG E N D DANIEL 20190404 [Cater for merchant that has sub-merchants but not reseller]

            DD_merchant.DataSource = merchant_L;
            DD_merchant.DataTextField = "DomainDesc";
            DD_merchant.DataValueField = "DomainID";
            DD_merchant.DataBind();

            DateTime sSince = Convert.ToDateTime(TB_Since.Text.Trim() + " 00:00:00");
            DateTime sTo = Convert.ToDateTime(TB_To.Text.Trim() + " 23:59:59");
            TB_Since.Text = sSince.ToString("yyyy-MM-dd");
            TB_To.Text = sTo.ToString("yyyy-MM-dd");

            //var pymtMethodList = mc.Do_GetCLPymtMethod();
            //LB_PymtMethod.DataSource = pymtMethodList;
            //LB_PymtMethod.DataTextField = "PymtMethodDesc";
            //LB_PymtMethod.DataValueField = "PymtMethodCode";
            //LB_PymtMethod.DataBind();

            var currencyList = mc.Do_GetCurrencyCountry(0);
            DD_Currency.DataSource = currencyList;
            DD_Currency.DataTextField = "CurrCode";
            DD_Currency.DataValueField = "CurrCode";
            DD_Currency.DataBind();

            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.Transaction), MethodBase.GetCurrentMethod().Name, "Page_Load Method");
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
    }

    protected void btn_Cancel_Click(object sender, EventArgs e)
    {
        TB_PaymentID.Text = string.Empty;
        TB_GatewayTxnID.Text = string.Empty;
        TB_CustEmail.Text = string.Empty;
        TB_OrderId.Text = string.Empty;
        //TB_Channel.Text = string.Empty;

        //var pymtMethodList = mc.Do_GetCLPymtMethod();
        //LB_PymtMethod.DataSource = pymtMethodList;
        //LB_PymtMethod.DataTextField = "PymtMethodDesc";
        //LB_PymtMethod.DataValueField = "PymtMethodCode";
        //LB_PymtMethod.DataBind();

        var currencyList = mc.Do_GetCurrencyCountry(0);
        DD_Currency.DataSource = currencyList;
        DD_Currency.DataTextField = "CurrCode";
        DD_Currency.DataValueField = "CurrCode";
        DD_Currency.DataBind();
    }

    protected void btn_GetReport_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        int sDomainID = Convert.ToInt32(DD_merchant.SelectedValue);
        DateTime sSince = Convert.ToDateTime(TB_Since.Text.Trim() + " 00:00:00");
        DateTime sTo = Convert.ToDateTime(TB_To.Text.Trim() + " 00:00:00");
        bool sSuccess = CB_success.Checked;
        bool sFailed = CB_failed.Checked;
        bool sPending = CB_pending.Checked;
        bool sRefunded = CB_refunded.Checked;
        bool sOther = CB_other.Checked;
        string sPaymentID = TB_PaymentID.Text.Trim();
        string sGatewayTxnID = TB_GatewayTxnID.Text.Trim();
        string sCustEmail = TB_CustEmail.Text.Trim();
        string sOrderID = TB_OrderId.Text.Trim();
        string sChannelType = string.Empty;
        //foreach (ListItem listItem in LB_PymtMethod.Items)
        //{
        //    if (listItem.Selected)
        //    {
        //        if (sChannelType != string.Empty)
        //            sChannelType = sChannelType + ",";
        //        sChannelType = sChannelType + "'" + listItem.Value + "'";
        //    }
        //}
        if (sChannelType == "")
        {
            var pymtMethodList = mc.Do_GetCLPymtMethod();
            foreach (var pymtMethod in pymtMethodList)
            {
                if (sChannelType != string.Empty)
                    sChannelType = sChannelType + ",";
                sChannelType = sChannelType + "'" + pymtMethod.PymtMethodCode + "'";
            }
        }
        string sChannel = string.Empty; //TB_Channel.Text.Trim();
        string sCurrency = DD_Currency.SelectedValue == "ALL" ? "" : DD_Currency.SelectedValue;

        var logObject = new
        {
            LoginUser = cus.UserID,
            DomainID = sDomainID,
            Since = sSince,
            To = sTo,
            Success = sSuccess,
            Failed = sFailed,
            Pending = sPending,
            Other = sOther,
            PaymentID = sPaymentID,
            GatewayTxnID = sGatewayTxnID,
            CustEmail = sCustEmail,
            OrderID = sOrderID,
            ChannelType = sChannelType,
            Channel = sChannel,
            Currency = sCurrency
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            string err_msg = GenerateDashBoard(sSince, sTo, sDomainID, sSuccess, sFailed, sPending, sRefunded, sOther, sPaymentID, sGatewayTxnID, sCustEmail, sOrderID, sChannelType, sChannel, sCurrency);

            if (!(string.IsNullOrEmpty(err_msg)))
            {
                //CHG START DANIEL 20190104 [Assign TXN controller error message to HiddenField1 and change err_msg to tc.system_err]
                //objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                //               "Transaction(Error) - " + logJson + err_msg.ToString());

                HiddenField1.Value = err_msg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                       "Transaction(Error) - " + logJson + tc.system_err.ToString());
                //ADD START START DANIEl 20190107 [Insert into AuditTrail]
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.Transaction), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + tc.system_err.ToString());
                //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
                return;
                //CHG E N D DANIEL 20190104 [Assign TXN controller error message to HiddenField1 and change err_msg to tc.system_err]
            }

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "Transaction - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.Transaction, 800000, "Transaction.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.Transaction), MethodBase.GetCurrentMethod().Name, logJson);
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "Transaction(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.Transaction, 800001, "Transaction.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.Transaction), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }

    protected void btn_Download_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        int sDomainID = Convert.ToInt32(DD_merchant.SelectedValue);
        string sDomainName = DD_merchant.SelectedItem.Text;
        DateTime sSince = Convert.ToDateTime(TB_Since.Text.Trim() + " 00:00:00");
        DateTime sTo = Convert.ToDateTime(TB_To.Text.Trim() + " 00:00:00");
        bool sSuccess = CB_success.Checked;
        bool sFailed = CB_failed.Checked;
        bool sPending = CB_pending.Checked;
        bool sRefunded = CB_refunded.Checked;
        bool sOther = CB_other.Checked;
        string sPaymentID = TB_PaymentID.Text.Trim();
        string sGatewayTxnID = TB_GatewayTxnID.Text.Trim();
        string sCustEmail = TB_CustEmail.Text.Trim();
        string sOrderID = TB_OrderId.Text.Trim();
        string sChannelType = string.Empty;
        //foreach (ListItem listItem in LB_PymtMethod.Items)
        //{
        //    if (listItem.Selected)
        //    {
        //        if (sChannelType != string.Empty)
        //            sChannelType = sChannelType + ",";
        //        sChannelType = sChannelType + "'" + listItem.Value + "'";
        //    }
        //}
        if (sChannelType == "")
        {
            var pymtMethodList = mc.Do_GetCLPymtMethod();

            //ADD START DANIEL 20190111 [Insert into AuditTrail]
            //ADD E N D DANIEL 20190111 [Insert into AuditTrail]
            foreach (var pymtMethod in pymtMethodList)
            {
                if (sChannelType != string.Empty)
                    sChannelType = sChannelType + ",";
                sChannelType = sChannelType + "'" + pymtMethod.PymtMethodCode + "'";
            }
        }
        string sChannel = string.Empty; //TB_Channel.Text.Trim();
        string sCurrency = DD_Currency.SelectedValue == "ALL" ? "" : DD_Currency.SelectedValue;

        var logObject = new
        {
            LoginUser = cus.UserID,
            DomainID = sDomainID,
            DomainName = sDomainName,
            Since = sSince,
            To = sTo,
            Success = sSuccess,
            Failed = sFailed,
            Pending = sPending,
            Other = sOther,
            PaymentID = sPaymentID,
            GatewayTxnID = sGatewayTxnID,
            CustEmail = sCustEmail,
            OrderID = sOrderID,
            ChannelType = sChannelType,
            Channel = sChannel,
            Currency = sCurrency
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            string POBRefundedStatus = WebConfigurationManager.AppSettings["OBRefundedStatus"];
            string POBPendingStatus = WebConfigurationManager.AppSettings["OBPendingStatus"];
            string POBOtherStatus = WebConfigurationManager.AppSettings["OBOtherStatus"];
            string PPGRefundedStatus = WebConfigurationManager.AppSettings["PGRefundedStatus"];
            string PPGPendingStatus = WebConfigurationManager.AppSettings["PGPendingStatus"];
            string PPGOtherStatus = WebConfigurationManager.AppSettings["PGOtherStatus"];

            //CHG START DANIEL 20181128 [Add on parameter for pmode = 1]
            //TxnRate_Transaction[] data = tc.Do_TransactionReport(sSince, sTo, sDomainID, sSuccess, sFailed, sPending, sOther,
            //    POBPendingStatus, POBOtherStatus, PPGPendingStatus, PPGOtherStatus, sPaymentID, sGatewayTxnID, sCustEmail, sOrderID, sChannelType, sChannel, sCurrency);

            //CHG START DANIEl 20181129 [Change TxnRate_Transaction to dynamic type]
            //TxnRate_Transaction[] data = tc.Do_TransactionReport(sSince, sTo, sDomainID, sSuccess, sFailed, sPending, sOther,
            //  POBPendingStatus, POBOtherStatus, PPGPendingStatus, PPGOtherStatus, sPaymentID, sGatewayTxnID, sCustEmail, sOrderID, sChannelType, sChannel, sCurrency, 1);
            dynamic[] data = tc.Do_TransactionReport(sSince, sTo, sDomainID, sSuccess, sFailed, sPending, sRefunded, sOther,
               POBRefundedStatus, POBPendingStatus, POBOtherStatus, PPGRefundedStatus, PPGPendingStatus, PPGOtherStatus, sPaymentID, sGatewayTxnID, sCustEmail, sOrderID, sChannelType, sChannel, sCurrency, 1);
            //CHG E N D DANIEl 20181129 [Change TxnRate_Transaction to dynamic type]
            //CHG END DANIEL 20181128 [Add on parameter for pmode = 1]

            //ADD START DANIEL 20190104 [Display error message and store into log if data is null]
            if (data == null)
            {
                HiddenField1.Value = tc.err_msg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                       "Transaction(Error) - " + logJson + tc.system_err.ToString());
                //ADD START START DANIEl 20190107 [Insert into AuditTrail]
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.Transaction), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + tc.system_err.ToString());
                //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
                return;
            }
            //ADD E N D DANIEL 20190104 [Display error message and store into log if data is null]

            ExcelFile ef = new ExcelFile();
            string PhysicalPath = Server.MapPath("Report/Report.xls");
            string SaveAsFileName = "TransactionReport_Merchant_" + sDomainName + "_" + sSince.ToString("dd-MM-yyyy") + "-" + sTo.ToString("dd-MM-yyyy") + ".xls";
            string SavePhysicalPath = Server.MapPath("Report/" + SaveAsFileName);
            ef.LoadXls(PhysicalPath);

            if (data != null)
            {
                //string regex = "(?:0\x20E3|1\x20E3|2\x20E3|3\x20E3|4\x20E3|5\x20E3|6\x20E3|7\x20E3|8\x20E3|9\x20E3|#\x20E3|\\*\x20E3|\xD83C(?:\xDDE6\xD83C(?:\xDDE8|\xDDE9|\xDDEA|\xDDEB|\xDDEC|\xDDEE|\xDDF1|\xDDF2|\xDDF4|\xDDF6|\xDDF7|\xDDF8|\xDDF9|\xDDFA|\xDDFC|\xDDFD|\xDDFF)|\xDDE7\xD83C(?:\xDDE6|\xDDE7|\xDDE9|\xDDEA|\xDDEB|\xDDEC|\xDDED|\xDDEE|\xDDEF|\xDDF1|\xDDF2|\xDDF3|\xDDF4|\xDDF6|\xDDF7|\xDDF8|\xDDF9|\xDDFB|\xDDFC|\xDDFE|\xDDFF)|\xDDE8\xD83C(?:\xDDE6|\xDDE8|\xDDE9|\xDDEB|\xDDEC|\xDDED|\xDDEE|\xDDF0|\xDDF1|\xDDF2|\xDDF3|\xDDF4|\xDDF5|\xDDF7|\xDDFA|\xDDFB|\xDDFC|\xDDFD|\xDDFE|\xDDFF)|\xDDE9\xD83C(?:\xDDEA|\xDDEC|\xDDEF|\xDDF0|\xDDF2|\xDDF4|\xDDFF)|\xDDEA\xD83C(?:\xDDE6|\xDDE8|\xDDEA|\xDDEC|\xDDED|\xDDF7|\xDDF8|\xDDF9|\xDDFA)|\xDDEB\xD83C(?:\xDDEE|\xDDEF|\xDDF0|\xDDF2|\xDDF4|\xDDF7)|\xDDEC\xD83C(?:\xDDE6|\xDDE7|\xDDE9|\xDDEA|\xDDEB|\xDDEC|\xDDED|\xDDEE|\xDDF1|\xDDF2|\xDDF3|\xDDF5|\xDDF6|\xDDF7|\xDDF8|\xDDF9|\xDDFA|\xDDFC|\xDDFE)|\xDDED\xD83C(?:\xDDF0|\xDDF2|\xDDF3|\xDDF7|\xDDF9|\xDDFA)|\xDDEE\xD83C(?:\xDDE8|\xDDE9|\xDDEA|\xDDF1|\xDDF2|\xDDF3|\xDDF4|\xDDF6|\xDDF7|\xDDF8|\xDDF9)|\xDDEF\xD83C(?:\xDDEA|\xDDF2|\xDDF4|\xDDF5)|\xDDF0\xD83C(?:\xDDEA|\xDDEC|\xDDED|\xDDEE|\xDDF2|\xDDF3|\xDDF5|\xDDF7|\xDDFC|\xDDFE|\xDDFF)|\xDDF1\xD83C(?:\xDDE6|\xDDE7|\xDDE8|\xDDEE|\xDDF0|\xDDF7|\xDDF8|\xDDF9|\xDDFA|\xDDFB|\xDDFE)|\xDDF2\xD83C(?:\xDDE6|\xDDE8|\xDDE9|\xDDEA|\xDDEB|\xDDEC|\xDDED|\xDDF0|\xDDF1|\xDDF2|\xDDF3|\xDDF4|\xDDF5|\xDDF6|\xDDF7|\xDDF8|\xDDF9|\xDDFA|\xDDFB|\xDDFC|\xDDFD|\xDDFE|\xDDFF)|\xDDF3\xD83C(?:\xDDE6|\xDDE8|\xDDEA|\xDDEB|\xDDEC|\xDDEE|\xDDF1|\xDDF4|\xDDF5|\xDDF7|\xDDFA|\xDDFF)|\xDDF4\xD83C\xDDF2|\xDDF5\xD83C(?:\xDDE6|\xDDEA|\xDDEB|\xDDEC|\xDDED|\xDDF0|\xDDF1|\xDDF2|\xDDF3|\xDDF7|\xDDF8|\xDDF9|\xDDFC|\xDDFE)|\xDDF6\xD83C\xDDE6|\xDDF7\xD83C(?:\xDDEA|\xDDF4|\xDDF8|\xDDFA|\xDDFC)|\xDDF8\xD83C(?:\xDDE6|\xDDE7|\xDDE8|\xDDE9|\xDDEA|\xDDEC|\xDDED|\xDDEE|\xDDEF|\xDDF0|\xDDF1|\xDDF2|\xDDF3|\xDDF4|\xDDF7|\xDDF8|\xDDF9|\xDDFB|\xDDFD|\xDDFE|\xDDFF)|\xDDF9\xD83C(?:\xDDE6|\xDDE8|\xDDE9|\xDDEB|\xDDEC|\xDDED|\xDDEF|\xDDF0|\xDDF1|\xDDF2|\xDDF3|\xDDF4|\xDDF7|\xDDF9|\xDDFB|\xDDFC|\xDDFF)|\xDDFA\xD83C(?:\xDDE6|\xDDEC|\xDDF2|\xDDF8|\xDDFE|\xDDFF)|\xDDFB\xD83C(?:\xDDE6|\xDDE8|\xDDEA|\xDDEC|\xDDEE|\xDDF3|\xDDFA)|\xDDFC\xD83C(?:\xDDEB|\xDDF8)|\xDDFD\xD83C\xDDF0|\xDDFE\xD83C(?:\xDDEA|\xDDF9)|\xDDFF\xD83C(?:\xDDE6|\xDDF2|\xDDFC)))|[\xA9\xAE\x203C\x2049\x2122\x2139\x2194-\x2199\x21A9\x21AA\x231A\x231B\x2328\x23CF\x23E9-\x23F3\x23F8-\x23FA\x24C2\x25AA\x25AB\x25B6\x25C0\x25FB-\x25FE\x2600-\x2604\x260E\x2611\x2614\x2615\x2618\x261D\x2620\x2622\x2623\x2626\x262A\x262E\x262F\x2638-\x263A\x2648-\x2653\x2660\x2663\x2665\x2666\x2668\x267B\x267F\x2692-\x2694\x2696\x2697\x2699\x269B\x269C\x26A0\x26A1\x26AA\x26AB\x26B0\x26B1\x26BD\x26BE\x26C4\x26C5\x26C8\x26CE\x26CF\x26D1\x26D3\x26D4\x26E9\x26EA\x26F0-\x26F5\x26F7-\x26FA\x26FD\x2702\x2705\x2708-\x270D\x270F\x2712\x2714\x2716\x271D\x2721\x2728\x2733\x2734\x2744\x2747\x274C\x274E\x2753-\x2755\x2757\x2763\x2764\x2795-\x2797\x27A1\x27B0\x27BF\x2934\x2935\x2B05-\x2B07\x2B1B\x2B1C\x2B50\x2B55\x3030\x303D\x3297\x3299]|\xD83C[\xDC04\xDCCF\xDD70\xDD71\xDD7E\xDD7F\xDD8E\xDD91-\xDD9A\xDE01\xDE02\xDE1A\xDE2F\xDE32-\xDE3A\xDE50\xDE51\xDF00-\xDF21\xDF24-\xDF93\xDF96\xDF97\xDF99-\xDF9B\xDF9E-\xDFF0\xDFF3-\xDFF5\xDFF7-\xDFFF]|\xD83D[\xDC00-\xDCFD\xDCFF-\xDD3D\xDD49-\xDD4E\xDD50-\xDD67\xDD6F\xDD70\xDD73-\xDD79\xDD87\xDD8A-\xDD8D\xDD90\xDD95\xDD96\xDDA5\xDDA8\xDDB1\xDDB2\xDDBC\xDDC2-\xDDC4\xDDD1-\xDDD3\xDDDC-\xDDDE\xDDE1\xDDE3\xDDEF\xDDF3\xDDFA-\xDE4F\xDE80-\xDEC5\xDECB-\xDED0\xDEE0-\xDEE5\xDEE9\xDEEB\xDEEC\xDEF0\xDEF3]|\xD83E[\xDD10-\xDD18\xDD80-\xDD84\xDDC0]";
                foreach (var d in data)
                {
                    string result = ReplaceNonBmpWithHex(d.CustName);
                    //string result = Regex.Replace(input, regex, "");
                    d.CustName = result;
                } 
            }

            DataTable dt = linq.LINQToDataTable(data);

            ExcelWorksheet ws = ef.Worksheets[0];
            ws.InsertDataTable(dt, "A1", true);

            ef.SaveXls(SavePhysicalPath);

            System.IO.FileInfo file = new System.IO.FileInfo(Server.MapPath("Report/" + SaveAsFileName));
            if (file.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                Response.AddHeader("Content-Length", file.Length.ToString());
                Response.ContentType = "application/vnd.ms-excel";
                Response.WriteFile(file.FullName);

                Response.Flush();
                file.Delete();
                Response.End();
            }

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "TransactionDownload - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.Transaction, 800000, "Transaction.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.Transaction), MethodBase.GetCurrentMethod().Name, logJson);
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "TransactionDownload(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.Transaction, 800001, "Transaction.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.Transaction), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }

    string GenerateDashBoard(DateTime PSince, DateTime PTo, int PDomainID, bool PSuccess, bool PFailed, bool PPending, bool PRefunded, bool POther, string PPaymentID, string PGatewayTxnID,
        string PCustEmail, string POrderID, string PChannelType, string PChannel, string PCurrency)
    {
        try
        {
            string POBRefundedStatus = WebConfigurationManager.AppSettings["OBRefundedStatus"];
            string POBPendingStatus = WebConfigurationManager.AppSettings["OBPendingStatus"];
            string POBOtherStatus = WebConfigurationManager.AppSettings["OBOtherStatus"];
            string PPGRefundedStatus = WebConfigurationManager.AppSettings["PGRefundedStatus"];
            string PPGPendingStatus = WebConfigurationManager.AppSettings["PGPendingStatus"];
            string PPGOtherStatus = WebConfigurationManager.AppSettings["PGOtherStatus"];

            //CHG START DANIEl 20181126 [Add last parameter, 1 for int PMode]
            //TxnRate_Transaction[] data = tc.Do_TransactionReport(PSince, PTo, PDomainID, PSuccess, PFailed, PPending, POther,
            //   POBPendingStatus, POBOtherStatus, PPGPendingStatus, PPGOtherStatus, PPaymentID, PGatewayTxnID, PCustEmail, POrderID, PChannelType, PChannel, PCurrency);
            //CHG START DANIEl 20181129 [Change TxnRate_Transaction to dynamic type]
            //TxnRate_Transaction[] data = tc.Do_TransactionReport(PSince, PTo, PDomainID, PSuccess, PFailed, PPending, POther,
            //                POBPendingStatus, POBOtherStatus, PPGPendingStatus, PPGOtherStatus, PPaymentID, PGatewayTxnID, PCustEmail, POrderID, PChannelType, PChannel, PCurrency, 1);
            dynamic[] data = tc.Do_TransactionReport(PSince, PTo, PDomainID, PSuccess, PFailed, PPending, PRefunded, POther,
                    POBRefundedStatus, POBPendingStatus, POBOtherStatus, PPGRefundedStatus, PPGPendingStatus, PPGOtherStatus, PPaymentID, PGatewayTxnID, PCustEmail, POrderID, PChannelType, PChannel, PCurrency, 1);
            //CHG E N D DANIEl 20181129 [Change TxnRate_Transaction to dynamic type]
            //CHG END DANIEl 20181126 [Add last parameter, 1 for int PMode]

            //ADD START DANIEL 20190104 [Return pgAdminDal error message if any]
            if (data == null)
                return tc.err_msg;
            //ADD E N D DANIEL 20190104 [Return pgAdminDal error message if any]

            Repeater2.DataSource = data;
            Repeater2.DataBind();

            return string.Empty;
        }
        catch (Exception ex)
        {
            return ex.Message.ToString();
        }
    }

    static string ReplaceNonBmpWithHex(string input)
    {
        // TODO: If most string don't have any non-BMP characters, consider
        // an optimization of checking for high/low surrogate characters first,
        // and return input if there aren't any.
        StringBuilder builder = new StringBuilder(input.Length);
        for (int i = 0; i < input.Length; i++)
        {
            char c = input[i];
            // A surrogate pair is a high surrogate followed by a low surrogate
            if (char.IsHighSurrogate(c))
            {
                if (i == input.Length - 1)
                {
                    //throw new ArgumentException($"High surrogate at end of string");
                }
                // Fetch the low surrogate, advancing our counter
                i++;
                char d = input[i];
                if (!char.IsLowSurrogate(d))
                {
                    //throw new ArgumentException($"Unmatched low surrogate at index {i - 1}");
                }
                uint highTranslated = (uint)((c - 0xd800) * 0x400);
                uint lowTranslated = (uint)(d - 0xdc00);
                uint utf32 = (uint)(highTranslated + lowTranslated + 0x10000);
                builder.AppendFormat("U{0:X8}", utf32);
            }
            // We should never see a low surrogate on its own
            else if (char.IsLowSurrogate(c))
            {
                //throw new ArgumentException($"Unmatched low surrogate at index {i}");
            }
            // Most common case: BMP character; just append it.
            else
            {
                builder.Append(c);
            }
        }
        return builder.ToString();
    }
}