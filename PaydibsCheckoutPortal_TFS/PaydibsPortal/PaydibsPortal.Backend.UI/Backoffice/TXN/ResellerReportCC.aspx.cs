﻿using GemBox.Spreadsheet;
using Kenji.Apps;
using LoggerII;
using Newtonsoft.Json;
using PaydibsPortal.BLL;
using PaydibsPortal.DAL;
using System;
using System.Data;
using System.Reflection;
using System.Web.Configuration;

public partial class Backoffice_TXN_ResellerReportCC : System.Web.UI.Page
{
    HomeController hc = null;
    TXNController tc = null;
    CurrentUserStat cus = null;
    LinqTool linq = new LinqTool();
    MMGMTController mc = new MMGMTController();

    protected void Page_Load(object sender, EventArgs e)
    {
        //ADD START DANIEL 20190104 [Initialize hidden fields]
        HiddenField1.Value = "";
        HiddenField2.Value = "";
        //ADD START DANIEL 20190104 [Initialize hidden fields]

        if (Session[SessionType.CurrentUser] == null)
        {
            Response.Redirect("../../Default.aspx");
            return;
        }

        cus = (CurrentUserStat)Session[SessionType.CurrentUser];
        hc = new HomeController(cus.DomainID);
        tc = new TXNController(cus.DomainID);

        // ADD START SUKI 20190104 [Check permission on every page load]
        var permission = hc.LoadUserPermission(cus.UserID, Convert.ToInt16(OpsIDEnum.ResellerCC));
        if (permission[0].IsUserAllowed == "0")
        {
            Response.Redirect("../../Backoffice/Dashboard.aspx");
            return;
        }
        // ADD E N D SUKI 20190104 [Check permission on every page load]

        if (!IsPostBack)
        {
            var merchant_L = hc.Do_GetMerchantList(1, 2);
            DD_merchant.DataSource = merchant_L;
            DD_merchant.DataTextField = "DomainDesc";
            DD_merchant.DataValueField = "DomainID";
            DD_merchant.DataBind();

            DateTime sSince = Convert.ToDateTime(TB_Since.Text.Trim() + " 00:00:00");
            DateTime sTo = Convert.ToDateTime(TB_To.Text.Trim() + " 23:59:59");
            TB_Since.Text = sSince.ToString("yyyy-MM-dd");
            TB_To.Text = sTo.ToString("yyyy-MM-dd");

            var currencyList = mc.Do_GetCurrencyCountry(0);
            DD_Currency.DataSource = currencyList;
            DD_Currency.DataTextField = "CurrCode";
            DD_Currency.DataValueField = "CurrCode";
            DD_Currency.DataBind();

            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ResellerCC), MethodBase.GetCurrentMethod().Name, "Page_Load Method");
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
    }

    protected void btn_Cancel_Click(object sender, EventArgs e)
    {
        TB_PaymentID.Text = string.Empty;
        TB_GatewayTxnID.Text = string.Empty;
        TB_CustEmail.Text = string.Empty;
        TB_OrderId.Text = string.Empty;

        var currencyList = mc.Do_GetCurrencyCountry(0);
        DD_Currency.DataSource = currencyList;
        DD_Currency.DataTextField = "CurrCode";
        DD_Currency.DataValueField = "CurrCode";
        DD_Currency.DataBind();
    }

    protected void btn_GetReport_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        int sDomainID = Convert.ToInt32(DD_merchant.SelectedValue);
        DateTime sSince = Convert.ToDateTime(TB_Since.Text.Trim() + " 00:00:00");
        DateTime sTo = Convert.ToDateTime(TB_To.Text.Trim() + " 23:59:59");
        bool sSuccess = CB_success.Checked;
        bool sFailed = CB_failed.Checked;
        bool sPending = CB_pending.Checked;
        bool sRefunded = CB_refunded.Checked;
        bool sOther = CB_other.Checked;
        string sPaymentID = TB_PaymentID.Text.Trim();
        string sGatewayTxnID = TB_GatewayTxnID.Text.Trim();
        string sCustEmail = TB_CustEmail.Text.Trim();
        string sOrderID = TB_OrderId.Text.Trim();
        string sChannelType = string.Empty;

        if (sChannelType == "")
        {
            var pymtMethodList = mc.Do_GetCLPymtMethod();
            foreach (var pymtMethod in pymtMethodList)
            {
                if (pymtMethod.PymtMethodCode.Equals("CC "))
                    sChannelType = sChannelType + "'" + pymtMethod.PymtMethodCode + "'";
            }
        }
        string sChannel = string.Empty;
        string sCurrency = DD_Currency.SelectedValue == "ALL" ? "" : DD_Currency.SelectedValue;

        var logObject = new
        {
            LoginUser = cus.UserID,
            DomainID = sDomainID,
            Since = sSince,
            To = sTo,
            Success = sSuccess,
            Failed = sFailed,
            Pending = sPending,
            Other = sOther,
            PaymentID = sPaymentID,
            GatewayTxnID = sGatewayTxnID,
            CustEmail = sCustEmail,
            OrderID = sOrderID,
            ChannelType = sChannelType,
            Channel = sChannel,
            Currency = sCurrency
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            string err_msg = GenerateDashBoard(sSince, sTo, sDomainID, sSuccess, sFailed, sPending, sRefunded, sOther, sPaymentID, sGatewayTxnID, sCustEmail, sOrderID, sChannelType, sChannel, sCurrency);
            if (!(string.IsNullOrEmpty(err_msg)))
            {
                //CHG START DANIEL 20190104 [Assign TXN controller error message to HiddenField1 and change err_msg to tc.system_err]
                //objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                //               "ResellerCC(Error) - " + logJson + err_msg.ToString());

                HiddenField1.Value = err_msg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                       "ResellerCC(Error) - " + logJson + tc.system_err.ToString());
                //ADD START START DANIEl 20190107 [Insert into AuditTrail]
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ResellerCC), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + tc.system_err.ToString());
                //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
                return;
                //CHG E N D DANIEL 20190104 [Assign TXN controller error message to HiddenField1 and change err_msg to tc.system_err]
            }

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "ResellerCC - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.ResellerCC, 800000, "ResellerCC.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ResellerCC), MethodBase.GetCurrentMethod().Name, logJson);
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "ResellerCC(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.ResellerCC, 800001, "ResellerCC.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ResellerCC), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }

    protected void btn_Download_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        int sDomainID = Convert.ToInt32(DD_merchant.SelectedValue);
        string sDomainName = DD_merchant.SelectedItem.Text;
        DateTime sSince = Convert.ToDateTime(TB_Since.Text.Trim() + " 00:00:00");
        DateTime sTo = Convert.ToDateTime(TB_To.Text.Trim() + " 23:59:59");
        bool sSuccess = CB_success.Checked;
        bool sFailed = CB_failed.Checked;
        bool sPending = CB_pending.Checked;
        bool sRefunded = CB_refunded.Checked;
        bool sOther = CB_other.Checked;
        string sPaymentID = TB_PaymentID.Text.Trim();
        string sGatewayTxnID = TB_GatewayTxnID.Text.Trim();
        string sCustEmail = TB_CustEmail.Text.Trim();
        string sOrderID = TB_OrderId.Text.Trim();
        string sChannelType = string.Empty;

        //CHG START DANIEL 20181128 [To set payment method into "CC "]
        //if (sChannelType == "")
        //{
        //    var pymtMethodList = mc.Do_GetCLPymtMethod();
        //    foreach (var pymtMethod in pymtMethodList)
        //    {
        //        if (sChannelType != string.Empty)
        //            sChannelType = sChannelType + ",";
        //        sChannelType = sChannelType + "'" + pymtMethod.PymtMethodCode + "'";
        //    }
        //}

        if (sChannelType == "")
        {
            var pymtMethodList = mc.Do_GetCLPymtMethod();
            foreach (var pymtMethod in pymtMethodList)
            {
                if (pymtMethod.PymtMethodCode.Equals("CC "))
                    sChannelType = sChannelType + "'" + pymtMethod.PymtMethodCode + "'";
            }
        }
        //CHG END DANIEL 20181128 [To set payment method into "CC "]

        string sChannel = string.Empty;
        string sCurrency = DD_Currency.SelectedValue == "ALL" ? "" : DD_Currency.SelectedValue;

        var logObject = new
        {
            LoginUser = cus.UserID,
            DomainID = sDomainID,
            DomainName = sDomainName,
            Since = sSince,
            To = sTo,
            Success = sSuccess,
            Failed = sFailed,
            Pending = sPending,
            Other = sOther,
            PaymentID = sPaymentID,
            GatewayTxnID = sGatewayTxnID,
            CustEmail = sCustEmail,
            OrderID = sOrderID,
            ChannelType = sChannelType,
            Channel = sChannel,
            Currency = sCurrency
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            string POBRefundedStatus = WebConfigurationManager.AppSettings["OBRefundedStatus"];
            string POBPendingStatus = WebConfigurationManager.AppSettings["OBPendingStatus"];
            string POBOtherStatus = WebConfigurationManager.AppSettings["OBOtherStatus"];
            string PPGRefundedStatus = WebConfigurationManager.AppSettings["PGRefundedStatus"];
            string PPGPendingStatus = WebConfigurationManager.AppSettings["PGPendingStatus"];
            string PPGOtherStatus = WebConfigurationManager.AppSettings["PGOtherStatus"];

            //CHG START DANIEL 20181128 [Add on parameter pmode = 2]
            //TxnRate_Transaction[] data = tc.Do_TransactionReport(sSince, sTo, sDomainID, sSuccess, sFailed, sPending, sOther,
            //    POBPendingStatus, POBOtherStatus, PPGPendingStatus, PPGOtherStatus, sPaymentID, sGatewayTxnID, sCustEmail, sOrderID, sChannelType, sChannel, sCurrency);

            //CHG START DANIEl 20181129 [Change TxnRate_Transaction to dynamic type]
            //TxnRate_Transaction[] data = tc.Do_TransactionReport(sSince, sTo, sDomainID, sSuccess, sFailed, sPending, sOther,
            //   POBPendingStatus, POBOtherStatus, PPGPendingStatus, PPGOtherStatus, sPaymentID, sGatewayTxnID, sCustEmail, sOrderID, sChannelType, sChannel, sCurrency, 2);

            //CHG START DANIEL 20190103 [If current user id is 1 , PMode = 1 else PMode = 2]
            //dynamic[] data = tc.Do_TransactionReport(sSince, sTo, sDomainID, sSuccess, sFailed, sPending, sOther,
            //  POBPendingStatus, POBOtherStatus, PPGPendingStatus, PPGOtherStatus, sPaymentID, sGatewayTxnID, sCustEmail, sOrderID, sChannelType, sChannel, sCurrency, 2);
            dynamic[] data = tc.Do_TransactionReport(sSince, sTo, sDomainID, sSuccess, sFailed, sPending, sRefunded, sOther,
                POBRefundedStatus, POBPendingStatus, POBOtherStatus, PPGRefundedStatus, PPGPendingStatus, PPGOtherStatus, sPaymentID, sGatewayTxnID, sCustEmail, sOrderID, sChannelType, sChannel, sCurrency, cus.DomainID == 1 ? 1 : 2);
            //CHG E N D DANIEL 20190103 [If current user id is 1 , PMode = 1 else PMode = 2]
            //CHG E N D DANIEl 20181129 [Change TxnRate_Transaction to dynamic type]
            //CHG END DANIEL 20181128 [Add on parameter pmode = 2]

            //ADD START DANIEL 20190104 [Display error message and store into log if data is null]
            if (data == null)
            {
                HiddenField1.Value = tc.err_msg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                       "ResellerCC(Error) - " + logJson + tc.system_err.ToString());
                //ADD START START DANIEl 20190107 [Insert into AuditTrail]
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ResellerCC), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + tc.system_err.ToString());
                //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
                return;
            }
            //ADD E N D DANIEL 20190104 [Display error message and store into log if data is null]

            ExcelFile ef = new ExcelFile();
            string PhysicalPath = Server.MapPath("Report/Report.xls");
            //CHG START DANIEL 20181128 [Change report name to ResellerReportCC]
            //string SaveAsFileName = "ResellerReport_Merchant_" + sDomainName + "_" + sSince.ToString("dd-MM-yyyy") + "-" + sTo.ToString("dd-MM-yyyy") + ".xls";
            string SaveAsFileName = "ResellerReportCC_Merchant_" + sDomainName + "_" + sSince.ToString("dd-MM-yyyy") + "-" + sTo.ToString("dd-MM-yyyy") + ".xls";
            //CHG END DANIEL 20181128 [Change report name to ResellerReportCC]
            string SavePhysicalPath = Server.MapPath("Report/" + SaveAsFileName);
            ef.LoadXls(PhysicalPath);

            DataTable dt = linq.LINQToDataTable(data);
            GemBox.Spreadsheet.ExcelWorksheet ws = ef.Worksheets[0];
            ws.InsertDataTable(dt, "A1", true);

            ef.SaveXls(SavePhysicalPath);

            System.IO.FileInfo file = new System.IO.FileInfo(Server.MapPath("Report/" + SaveAsFileName));
            if (file.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                Response.AddHeader("Content-Length", file.Length.ToString());
                Response.ContentType = "application/vnd.ms-excel";
                Response.WriteFile(file.FullName);

                Response.Flush();
                file.Delete();
                Response.End();
            }

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "ResellerCCDownload - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.ResellerCC, 800000, "ResellerCC.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ResellerCC), MethodBase.GetCurrentMethod().Name, logJson);
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "ResellerCCDownload(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.ResellerCC, 800001, "ResellerCC.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ResellerCC), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }

    string GenerateDashBoard(DateTime PSince, DateTime PTo, int PDomainID, bool PSuccess, bool PFailed, bool PPending, bool PRefunded, bool POther, string PPaymentID, string PGatewayTxnID,
        string PCustEmail, string POrderID, string PChannelType, string PChannel, string PCurrency)
    {
        try
        {
            string POBRefundedStatus = WebConfigurationManager.AppSettings["OBRefundedStatus"];
            string POBPendingStatus = WebConfigurationManager.AppSettings["OBPendingStatus"];
            string POBOtherStatus = WebConfigurationManager.AppSettings["OBOtherStatus"];
            string PPGRefundedStatus = WebConfigurationManager.AppSettings["PGRefundedStatus"];
            string PPGPendingStatus = WebConfigurationManager.AppSettings["PGPendingStatus"];
            string PPGOtherStatus = WebConfigurationManager.AppSettings["PGOtherStatus"];

            //CHG START DANIEl 20181129 [Change TxnRate_Transaction to dynamic type]
            //TxnRate_Transaction[] data = tc.Do_TransactionReport(PSince, PTo, PDomainID, PSuccess, PFailed, PPending, POther,
            //   POBPendingStatus, POBOtherStatus, PPGPendingStatus, PPGOtherStatus, PPaymentID, PGatewayTxnID, PCustEmail, POrderID, PChannelType, PChannel, PCurrency, 2);

            //CHG START DANIEL 20190103 [If current user id is 1 , PMode = 1 else PMode = 2]
            //dynamic[] data = tc.Do_TransactionReport(PSince, PTo, PDomainID, PSuccess, PFailed, PPending, POther,
            //    POBPendingStatus, POBOtherStatus, PPGPendingStatus, PPGOtherStatus, PPaymentID, PGatewayTxnID, PCustEmail, POrderID, PChannelType, PChannel, PCurrency, 2);
            dynamic[] data = tc.Do_TransactionReport(PSince, PTo, PDomainID, PSuccess, PFailed, PPending, PRefunded, POther,
               POBRefundedStatus, POBPendingStatus, POBOtherStatus, PPGRefundedStatus, PPGPendingStatus, PPGOtherStatus, PPaymentID, PGatewayTxnID, PCustEmail, POrderID, PChannelType, PChannel, PCurrency, cus.DomainID == 1 ? 1 : 2);
            //CHG E N D DANIEL 20190103 [If current user id is 1 , PMode = 1 else PMode = 2]

            //CHG E N D DANIEl 20181129 [Change TxnRate_Transaction to dynamic type]

            //ADD START DANIEL 20190104 [Return pgAdminDal error message if any]
            if (data == null)
                return tc.err_msg;
            //ADD E N D DANIEL 20190104 [Return pgAdminDal error message if any]

            Repeater2.DataSource = data;
            Repeater2.DataBind();

            return string.Empty;
        }
        catch (Exception ex)
        {
            return ex.Message.ToString();
        }
    }
}