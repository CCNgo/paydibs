﻿using GemBox.Spreadsheet;
using Kenji.Apps;
using LoggerII;
using Newtonsoft.Json;
using PaydibsPortal.BLL;
using PaydibsPortal.DAL;
using System;
using System.Data;
using System.Reflection;
using System.Web.Configuration;

public partial class Backoffice_TXN_OnlineBanking : System.Web.UI.Page
{
    HomeController hc = null;
    TXNController tc = null;
    CurrentUserStat cus = null;
    LinqTool linq = new LinqTool();
    MMGMTController mc = new MMGMTController();

    protected void Page_Load(object sender, EventArgs e)
    {

        //ADD START DANIEL 20190104 [Initialize hidden fields]
        HiddenField1.Value = "";
        HiddenField2.Value = "";
        //ADD START DANIEL 20190104 [Initialize hidden fields]
        if (Session[SessionType.CurrentUser] == null)
        {
            Response.Redirect("../Dashboard.aspx");
            return;
        }

        cus = (CurrentUserStat)Session[SessionType.CurrentUser];
        hc = new HomeController(cus.DomainID);
        tc = new TXNController(cus.DomainID);

        // ADD START SUKI 20190104 [Check permission on every page load]
        var permission = hc.LoadUserPermission(cus.UserID, Convert.ToInt16(OpsIDEnum.TransactionOB));
        if (permission[0].IsUserAllowed == "0")
        {
            Response.Redirect("../../Backoffice/Dashboard.aspx");
            return;
        }
        // ADD E N D SUKI 20190104 [Check permission on every page load]

        if (!IsPostBack)
        {
            //Default selected dropdown index = 1 (Online Banking) and the listbox is hidden
            //DEL START DANIEL 20181129 [Remove listbox payment method]
            //LB_PymtMethod.Visible = false;
            //DEL E N D DANIEL 20181129 [Remove listbox payment method]

            //CHG START DANIEL 20190404 [Cater for merchant that has sub-merchants but not reseller]
            //var merchant_L = hc.Do_GetMerchantList(1, 1);
            CMS_Domain[] merchant_L = null;
            bool isMainMerchant = tc.Do_Get_CL_Plugin_IsReseller(cus.DomainID);

            if (isMainMerchant)
                //Display Main merchant and sub- merchants(Not reseller)
                merchant_L = hc.Do_GetMerchantList(1, 0);
            else
                //If Reseller and normal merchant. Display only own trading name
                merchant_L = hc.Do_GetMerchantList(1, 1);

            //CHG E N D DANIEL 20190404 [Cater for merchant that has sub-merchants but not reseller]
            DD_merchant.DataSource = merchant_L;
            DD_merchant.DataTextField = "DomainDesc";
            DD_merchant.DataValueField = "DomainID";
            DD_merchant.DataBind();

            DateTime sSince = Convert.ToDateTime(TB_Since.Text.Trim() + " 00:00:00");
            DateTime sTo = Convert.ToDateTime(TB_To.Text.Trim() + " 23:59:59");
            TB_Since.Text = sSince.ToString("yyyy-MM-dd");
            TB_To.Text = sTo.ToString("yyyy-MM-dd");

            //DEL START DANIEL 20181129 [Remove listbox payment method]
            //var pymtMethodList = mc.Do_GetCLPymtMethod();
            //LB_PymtMethod.DataSource = pymtMethodList;
            //LB_PymtMethod.DataTextField = "PymtMethodDesc";
            //LB_PymtMethod.DataValueField = "PymtMethodCode";
            //LB_PymtMethod.DataBind();
            //LB_PymtMethod.SelectedIndex = 1;
            //DEL E N D DANIEL 20181129 [Remove listbox payment method]

            var currencyList = mc.Do_GetCurrencyCountry(0);
            DD_Currency.DataSource = currencyList;
            DD_Currency.DataTextField = "CurrCode";
            DD_Currency.DataValueField = "CurrCode";
            DD_Currency.DataBind();

            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.TransactionOB), MethodBase.GetCurrentMethod().Name, "Page_Load Method");
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
    }

    protected void btn_Cancel_Click(object sender, EventArgs e)
    {
        TB_PaymentID.Text = string.Empty;
        TB_GatewayTxnID.Text = string.Empty;
        TB_CustEmail.Text = string.Empty;
        TB_OrderId.Text = string.Empty;
        TB_Channel.Text = string.Empty;
        var currencyList = mc.Do_GetCurrencyCountry(0);
        DD_Currency.DataSource = currencyList;
        DD_Currency.DataTextField = "CurrCode";
        DD_Currency.DataValueField = "CurrCode";
        DD_Currency.DataBind();
    }

    protected void btn_GetReport_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        int sDomainID = Convert.ToInt32(DD_merchant.SelectedValue);
        DateTime sSince = Convert.ToDateTime(TB_Since.Text.Trim() + " 00:00:00");
        DateTime sTo = Convert.ToDateTime(TB_To.Text.Trim() + " 00:00:00");

        bool sSuccess = CB_success.Checked;
        bool sFailed = CB_failed.Checked;
        bool sPending = CB_pending.Checked;
        bool sRefunded = CB_refunded.Checked;
        bool sOther = CB_other.Checked;
        string sPaymentID = TB_PaymentID.Text.Trim();
        string sGatewayTxnID = TB_GatewayTxnID.Text.Trim();
        string sCustEmail = TB_CustEmail.Text.Trim();
        string sOrderID = TB_OrderId.Text.Trim();
        string sChannelType = string.Empty;

        //DEL START DANIEL 20181129 [Remove old filtering option]
        //foreach (ListItem listItem in LB_PymtMethod.Items)
        //{
        //    if (listItem.Selected)
        //    {
        //        if (sChannelType != string.Empty)
        //            sChannelType = sChannelType + ",";
        //        sChannelType = sChannelType + "'" + listItem.Value + "'";
        //    }
        //}
        //DEL E N D DANIEL 20181129 [Remove old filtering option]

        //ADD START DANIEL 20181129 [Set filtering option to OB and WA]
        if (sChannelType == "")
        {
            var pymtMethodList = mc.Do_GetCLPymtMethod();
            foreach (var pymtMethod in pymtMethodList)
            {
                if (sChannelType != string.Empty)
                    sChannelType = sChannelType + ",";

                if (pymtMethod.PymtMethodCode.Equals("OB ") || pymtMethod.PymtMethodCode.Equals("WA "))
                    sChannelType = sChannelType + "'" + pymtMethod.PymtMethodCode + "'";
            }
        }
        //ADD E N D DANIEL 20181129 [Set filtering option to OB and WA]

        string sChannel = TB_Channel.Text.Trim();
        string sCurrency = DD_Currency.SelectedValue == "ALL" ? "" : DD_Currency.SelectedValue;

        var logObject = new
        {
            LoginUser = cus.UserID,
            DomainID = sDomainID,
            Since = sSince,
            To = sTo,
            Success = sSuccess,
            Failed = sFailed,
            Pending = sPending,
            Refunded = sRefunded,
            Other = sOther,
            PaymentID = sPaymentID,
            GatewayTxnID = sGatewayTxnID,
            CustEmail = sCustEmail,
            OrderID = sOrderID,
            ChannelType = sChannelType,
            Channel = sChannel,
            Currency = sCurrency
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            string err_msg = GenerateDashBoard(sSince, sTo, sDomainID, sSuccess, sFailed, sPending, sRefunded, sOther, sPaymentID, sGatewayTxnID, sCustEmail, sOrderID, sChannelType, sChannel, sCurrency);
            if (!(string.IsNullOrEmpty(err_msg)))
            {
                //CHG START DANIEL 20190104 [Assign TXN controller error message to HiddenField1 and change err_msg to tc.system_err]
                //objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                //               "TransactionOB(Error) - " + logJson + err_msg.ToString());

                HiddenField1.Value = err_msg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                       "TransactionOB(Error) - " + logJson + tc.system_err.ToString());
                //ADD START START DANIEl 20190107 [Insert into AuditTrail]
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.TransactionOB), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + tc.system_err.ToString());
                //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
                return;
                //CHG E N D DANIEL 20190104 [Assign TXN controller error message to HiddenField1 and change err_msg to tc.system_err]
            }

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "TransactionOB - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.TransactionOB, 800000, "TransactionOB.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.TransactionOB), MethodBase.GetCurrentMethod().Name, logJson);
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "TransactionOB(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.TransactionOB, 800001, "TransactionOB.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.TransactionOB), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }

    protected void btn_Download_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        int sDomainID = Convert.ToInt32(DD_merchant.SelectedValue);
        string sDomainName = DD_merchant.SelectedItem.Text;
        DateTime sSince = Convert.ToDateTime(TB_Since.Text.Trim() + " 00:00:00");
        DateTime sTo = Convert.ToDateTime(TB_To.Text.Trim() + " 00:00:00");
        bool sSuccess = CB_success.Checked;
        bool sFailed = CB_failed.Checked;
        bool sRefunded = CB_refunded.Checked;
        bool sPending = CB_pending.Checked;
        bool sOther = CB_other.Checked;
        string sPaymentID = TB_PaymentID.Text.Trim();
        string sGatewayTxnID = TB_GatewayTxnID.Text.Trim();
        string sCustEmail = TB_CustEmail.Text.Trim();
        string sOrderID = TB_OrderId.Text.Trim();
        string sChannelType = string.Empty;
        //ADD START DANIEL 20181128 [To set payment method into "CC "]
        //DEL START DANIEL 20181129 [Remove old filtering option]
        //foreach (ListItem listItem in LB_PymtMethod.Items)
        //{
        //    if (listItem.Selected)
        //    {
        //        if (sChannelType != string.Empty)
        //            sChannelType = sChannelType + ",";
        //        sChannelType = sChannelType + "'" + listItem.Value + "'";
        //    }
        //}
        //DEL E N D DANIEL 20181129 [Remove old filtering option]
        //ADD END DANIEL 20181128 [To set payment method into "CC "]

        //ADD START DANIEL 20181129 [Set filtering option to OB and WA]
        if (sChannelType == "")
        {
            var pymtMethodList = mc.Do_GetCLPymtMethod();
            foreach (var pymtMethod in pymtMethodList)
            {
                if (sChannelType != string.Empty)
                    sChannelType = sChannelType + ",";

                if (pymtMethod.PymtMethodCode.Equals("OB ") || pymtMethod.PymtMethodCode.Equals("WA "))
                    sChannelType = sChannelType + "'" + pymtMethod.PymtMethodCode + "'";
            }
        }
        //ADD E N D DANIEL 20181129 [Set filtering option to OB and WA]
        string sChannel = TB_Channel.Text.Trim();
        string sCurrency = DD_Currency.SelectedValue == "ALL" ? "" : DD_Currency.SelectedValue;

        var logObject = new
        {
            LoginUser = cus.UserID,
            DomainID = sDomainID,
            DomainName = sDomainName,
            Since = sSince,
            To = sTo,
            Success = sSuccess,
            Failed = sFailed,
            Pending = sPending,
            Other = sOther,
            PaymentID = sPaymentID,
            GatewayTxnID = sGatewayTxnID,
            CustEmail = sCustEmail,
            OrderID = sOrderID,
            ChannelType = sChannelType,
            Channel = sChannel,
            Currency = sCurrency
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            string POBRefundededStatus = WebConfigurationManager.AppSettings["OBRefundedStatus"];
            string POBPendingStatus = WebConfigurationManager.AppSettings["OBPendingStatus"];
            string POBOtherStatus = WebConfigurationManager.AppSettings["OBOtherStatus"];
            string PPGRefundedStatus = WebConfigurationManager.AppSettings["PGRefundedStatus"];
            string PPGPendingStatus = WebConfigurationManager.AppSettings["PGPendingStatus"];
            string PPGOtherStatus = WebConfigurationManager.AppSettings["PGOtherStatus"];

            //CHG START DANIEL 20181128 [Add on parameter pmode = 1]
            //TxnRate_Transaction[] data = tc.Do_TransactionReport(sSince, sTo, sDomainID, sSuccess, sFailed, sPending, sOther,
            //    POBPendingStatus, POBOtherStatus, PPGPendingStatus, PPGOtherStatus, sPaymentID, sGatewayTxnID, sCustEmail, sOrderID, sChannelType, sChannel, sCurrency);

            //CHG START DANIEl 20181129 [Change TxnRate_Transaction to dynamic type]
            //TxnRate_Transaction[] data = tc.Do_TransactionReport(sSince, sTo, sDomainID, sSuccess, sFailed, sPending, sOther,
            //   POBPendingStatus, POBOtherStatus, PPGPendingStatus, PPGOtherStatus, sPaymentID, sGatewayTxnID, sCustEmail, sOrderID, sChannelType, sChannel, sCurrency, 1);
            dynamic[] data = tc.Do_TransactionReport(sSince, sTo, sDomainID, sSuccess, sFailed, sPending, sRefunded, sOther,
                POBRefundededStatus, POBPendingStatus, POBOtherStatus, PPGRefundedStatus, PPGPendingStatus, PPGOtherStatus, sPaymentID, sGatewayTxnID, sCustEmail, sOrderID, sChannelType, sChannel, sCurrency, 1);
            //CHG E N D DANIEl 20181129 [Change TxnRate_Transaction to dynamic type]

            //CHG END DANIEL 20181128 [Add on parameter pmode = 1]

            //ADD START DANIEL 20190104 [Display error message and store into log if data is null]
            if (data == null)
            {
                HiddenField1.Value = tc.err_msg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                       "TransactionOB(Error) - " + logJson + tc.system_err.ToString());
                //ADD START START DANIEl 20190107 [Insert into AuditTrail]
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.TransactionOB), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + tc.system_err.ToString());
                //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
                return;
            }
            //ADD E N D DANIEL 20190104 [Display error message and store into log if data is null]

            ExcelFile ef = new ExcelFile();
            string PhysicalPath = Server.MapPath("Report/Report.xls");
            string SaveAsFileName = "TransactionOBReport_Merchant_" + sDomainName + "_" + sSince.ToString("dd-MM-yyyy") + "-" + sTo.ToString("dd-MM-yyyy") + ".xls";
            string SavePhysicalPath = Server.MapPath("Report/" + SaveAsFileName);
            ef.LoadXls(PhysicalPath);

            DataTable dt = linq.LINQToDataTable(data);
            ExcelWorksheet ws = ef.Worksheets[0];
            ws.InsertDataTable(dt, "A1", true);

            ef.SaveXls(SavePhysicalPath);

            System.IO.FileInfo file = new System.IO.FileInfo(Server.MapPath("Report/" + SaveAsFileName));
            if (file.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                Response.AddHeader("Content-Length", file.Length.ToString());
                Response.ContentType = "application/vnd.ms-excel";
                Response.WriteFile(file.FullName);

                Response.Flush();
                file.Delete();
                Response.End();
            }

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "TransactionOBDownload - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.TransactionOB, 800000, "TransactionOB.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.TransactionOB), MethodBase.GetCurrentMethod().Name, logJson);
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "TransactionOBDownload(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.TransactionOB, 800001, "TransactionOB.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.TransactionOB), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }

    string GenerateDashBoard(DateTime PSince, DateTime PTo, int PDomainID, bool PSuccess, bool PFailed, bool PPending, bool PRefunded, bool POther, string PPaymentID, string PGatewayTxnID,
        string PCustEmail, string POrderID, string PChannelType, string PChannel, string PCurrency)
    {
        try
        {
            string POBRefundedStatus = WebConfigurationManager.AppSettings["OBRefundedStatus"];
            string POBPendingStatus = WebConfigurationManager.AppSettings["OBPendingStatus"];
            string POBOtherStatus = WebConfigurationManager.AppSettings["OBOtherStatus"];
            string PPGRefundedStatus = WebConfigurationManager.AppSettings["PGRefundedStatus"];
            string PPGPendingStatus = WebConfigurationManager.AppSettings["PGPendingStatus"];
            string PPGOtherStatus = WebConfigurationManager.AppSettings["PGOtherStatus"];

            //CHG START DANIEl 20181127 [Add last parameter, 1 for int PMode]
            //TxnRate_Transaction[] data = tc.Do_TransactionReport(PSince, PTo, PDomainID, PSuccess, PFailed, PPending, POther,
            //    POBPendingStatus, POBOtherStatus, PPGPendingStatus, PPGOtherStatus, PPaymentID, PGatewayTxnID, PCustEmail, POrderID, PChannelType, PChannel, PCurrency);
            //CHG START DANIEl 20181129 [Change TxnRate_Transaction to dynamic type]
            //TxnRate_Transaction[] data = tc.Do_TransactionReport(PSince, PTo, PDomainID, PSuccess, PFailed, PPending, POther,
            //                POBPendingStatus, POBOtherStatus, PPGPendingStatus, PPGOtherStatus, PPaymentID, PGatewayTxnID, PCustEmail, POrderID, PChannelType, PChannel, PCurrency, 1);
            dynamic[] data = tc.Do_TransactionReport(PSince, PTo, PDomainID, PSuccess, PFailed, PPending, PRefunded, POther,
                    POBRefundedStatus, POBPendingStatus, POBOtherStatus, PPGRefundedStatus, PPGPendingStatus, PPGOtherStatus, PPaymentID, PGatewayTxnID, PCustEmail, POrderID, PChannelType, PChannel, PCurrency, 1);
            //CHG E N D DANIEl 20181129 [Change TxnRate_Transaction to dynamic type]
            //CHG END DANIEl 20181127 [Add last parameter, 1 for int PMode]

            //ADD START DANIEL 20190104 [Return pgAdminDal error message if any]
            if (data == null)
                return tc.err_msg;
            //ADD E N D DANIEL 20190104 [Return pgAdminDal error message if any]

            Repeater2.DataSource = data;
            Repeater2.DataBind();

            return string.Empty;
        }
        catch (Exception ex)
        {
            return ex.Message.ToString();
        }
    }

    //protected void CB_all_Click(object sender, EventArgs e) 
    //{
    //    CB_all.InputAttributes.Add("class", "custom-control-label");

    //    if (CB_all.Checked == true)
    //    {
    //        CB_success.Checked = true;
    //        CB_failed.Checked = true;
    //        CB_pending.Checked = true;
    //        CB_refunded.Checked = true;
    //        CB_other.Checked = true;

    //        CB_success.Disabled = true;
    //        CB_failed.Disabled = true;
    //        CB_pending.Disabled = true;
    //        CB_refunded.Disabled = true;
    //        CB_other.Disabled = true;
    //    }
    //    else
    //    {
    //        CB_success.Checked = false;
    //        CB_failed.Checked = false;
    //        CB_pending.Checked = false;
    //        CB_refunded.Checked = false;
    //        CB_other.Checked = false;

    //        CB_success.Disabled = false;
    //        CB_failed.Disabled = false;
    //        CB_pending.Disabled = false;
    //        CB_refunded.Disabled = false;
    //        CB_other.Disabled = false;
    //    }
    //}
}

