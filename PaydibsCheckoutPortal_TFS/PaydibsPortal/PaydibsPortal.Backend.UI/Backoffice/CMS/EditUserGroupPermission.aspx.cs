﻿using LoggerII;
using Newtonsoft.Json;
using PaydibsPortal.BLL;
using PaydibsPortal.DAL;
using System;
using System.Reflection;
using System.Web.UI.WebControls;

public partial class Backoffice_CMS_EditUserGroupPermission : System.Web.UI.Page
{
    HomeController hc = null;
    CMSController cc = new CMSController();
    CurrentUserStat cus = null;
    static int PGroupID = 0;
    int PStatus = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        HiddenField1.Value = "";
        HiddenField2.Value = "";

        if (Session[SessionType.CurrentUser] == null)
        {
            Response.Redirect("../../Default.aspx");
            return;
        }

        cus = (CurrentUserStat)Session[SessionType.CurrentUser];
        hc = new HomeController(cus.DomainID);

        // ADD START SUKI 20190104 [Check permission on every page load]
        var permission = hc.LoadUserPermission(cus.UserID, Convert.ToInt16(OpsIDEnum.EditUserGroupPermission));
        if (permission[0].IsUserAllowed == "0")
        {
            Response.Redirect("../../Backoffice/Dashboard.aspx");
            return;
        }
        // ADD E N D SUKI 20190104 [Check permission on every page load]

        if (!IsPostBack)
        {
            var merchant_L = hc.Do_GetMerchantList(1, 0);
            DD_merchant.DataSource = merchant_L;
            DD_merchant.DataTextField = "DomainDesc";
            DD_merchant.DataValueField = "DomainID";
            DD_merchant.DataBind();

            DD_UserGroup.Enabled = false;
            btn_Select.Enabled = false;
            //ADD START DANIEL 20190102 [If first load , btn_update is disabled]
            btn_update.Enabled = false;
            //ADD E N D DANIEL 20190102 [If first load , btn_update is disabled]

            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditUserGroupPermission), MethodBase.GetCurrentMethod().Name, "Page_Load Method");
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
    }

    protected void btn_Search_Click(object sender, EventArgs e)
    {
        //CHG START DANIEL 20181211 [Add logger function]

        //var sDomainID = Convert.ToInt32(DD_merchant.SelectedValue);
        //var userGroup = cc.Do_GetUserGroup(sDomainID);
        //DD_UserGroup.DataSource = userGroup;
        //DD_UserGroup.DataTextField = "GroupName";
        //DD_UserGroup.DataValueField = "GroupID";
        //DD_UserGroup.DataBind();

        //DD_UserGroup.Enabled = true;
        //btn_Select.Enabled = true;

        //ADD START DANIEL 20190102 [btn_update is disabled and report is binded to null when search button is clicked]
        btn_update.Enabled = false;
        BindReportToNull();
        //ADD E N D DANIEL 20190102 [btn_update is disabled and report is binded to null when search button is clicked]

        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        var sDomainID = Convert.ToInt32(DD_merchant.SelectedValue);

        var logObject = new
        {
            LoginUser = cus.UserID,
            DomainID = sDomainID
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            var userGroup = cc.Do_GetUserGroup(sDomainID);
            //ADD START DANIEl 20190109 [Insert into AuditTrail]
            if (userGroup == null)
            {
                HiddenField1.Value = cc.err_msg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "EditUserGroupPermission(Error) - " + logJson + cc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditUserGroupPermission), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + cc.system_err.ToString());
                return;
            }
            //ADD E N D DANIEl 20190109 [Insert into AuditTrail]
            DD_UserGroup.DataSource = userGroup;
            DD_UserGroup.DataTextField = "GroupName";
            DD_UserGroup.DataValueField = "GroupID";
            DD_UserGroup.DataBind();

            DD_UserGroup.Enabled = true;
            btn_Select.Enabled = true;

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "EditUserGroupPermission - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.EditUserGroupPermission, 800000, "EditUserGroupPermission.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditUserGroupPermission), MethodBase.GetCurrentMethod().Name, logJson);
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "EditUserGroupPermission(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.EditUserGroupPermission, 800001, "EditUserGroupPermission.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditUserGroupPermission), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        finally
        {
            objLoggerII.Dispose();
        }
        //CHG E N D DANIEL 20181211 [Add logger function]

    }

    protected void btn_Select_Click(object sender, EventArgs e)
    {
        //CHG START DANIEL 20181211 [Add logger function]

        //var userGroupID = Convert.ToInt32(DD_UserGroup.SelectedValue);
        //GenerateList(userGroupID);

        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        var userGroupID = Convert.ToInt32(DD_UserGroup.SelectedValue);

        var logObject = new
        {
            LoginUser = cus.UserID,
            DomainID = Convert.ToInt32(DD_merchant.SelectedValue),
            GroupID = userGroupID
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            //CHG START DANIEL 20190110 [Insert into AuditTrail]
            //GenerateList(userGroupID);
            string errMsg = GenerateList(userGroupID);
            if (!string.IsNullOrEmpty(errMsg))
            {
                HiddenField1.Value = errMsg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "EditUserGroupPermission(Error) - " + logJson + cc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditUserGroupPermission), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + cc.system_err.ToString());
                return;
            }
            //CHG E N D DANIEL 20190110 [Insert into AuditTrail]

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "EditUserGroupPermission - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.EditUserGroupPermission, 800000, "EditUserGroupPermission.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditUserGroupPermission), MethodBase.GetCurrentMethod().Name, logJson);
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "EditUserGroupPermission(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.EditUserGroupPermission, 800001, "EditUserGroupPermission.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditUserGroupPermission), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        finally
        {
            objLoggerII.Dispose();
        }

        //ADD START DANIEL 20190102 [Enable click for btn_update]
        btn_update.Enabled = true;
        //ADD E N ND DANIEL 20190102 [Enable click for btn_update]

        //CHG E N D DANIEL 20181211 [Add logger function]
    }

    protected void btn_update_Click(object sender, EventArgs e)
    {
        //CHG START DANIEL 20181211 [Add logger function and add update succesful message]

        //var userGroupID = Convert.ToInt32(DD_UserGroup.SelectedValue);
        //var operationGroup = cc.Do_GetOperationGroup(Convert.ToInt32(DD_UserGroup.SelectedValue));

        //foreach (RepeaterItem item in Repeater2.Items)
        //{
        //    var checkBox = (CheckBox)item.FindControl("cb_permission");
        //    var checkboxText = checkBox.Checked == true ? "1" : "0";
        //    var opsGroupID = ((Label)item.FindControl("lbl_id")).Text;
        //    var addUpdateUserGroupMap = cc.Do_Insert_Update_UserGroupMap(userGroupID, Convert.ToInt32(opsGroupID), Convert.ToInt32(checkboxText));
        //}

        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();
        var userGroupID = Convert.ToInt32(DD_UserGroup.SelectedValue);

        var logObject = new
        {
            LoginUser = cus.UserID,
            DomainID = Convert.ToInt32(DD_merchant.SelectedValue),
            GroupID = userGroupID,
            ModifiedBy = cus.UserID.ToString()
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();
            foreach (RepeaterItem item in Repeater2.Items)
            {
                var checkBox = (CheckBox)item.FindControl("cb_permission");
                var checkboxText = checkBox.Checked == true ? "1" : "0";
                var opsGroupID = ((Label)item.FindControl("lbl_id")).Text;
                cc.Do_Insert_Update_UserGroupMap(userGroupID, Convert.ToInt32(opsGroupID), Convert.ToInt32(checkboxText));

                //ADD START DANIEL 20190104 [Check for PgAdminDal error message]
                if (!string.IsNullOrEmpty(cc.system_err))
                {
                    HiddenField1.Value = cc.err_msg;
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "EditUserGroupPermission(Error) - " + logJson + cc.system_err.ToString());
                    //ADD START DANIEl 20190107 [Insert into AuditTrail]
                    hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditUserGroupPermission), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + cc.system_err.ToString());
                    //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
                    return;
                }
                //ADD START DANIEL 20190104 [Check for PgAdminDal error message]
            }

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "EditUserGroupPermission - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.EditUserGroupPermission, 800000, "EditUserGroupPermission.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditUserGroupPermission), MethodBase.GetCurrentMethod().Name, logJson);
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "EditUserGroupPermission(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.EditUserGroupPermission, 800001, "EditUserGroupPermission.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditUserGroupPermission), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        finally
        {
            objLoggerII.Dispose();
        }

        //ADD START DANIEl 20190102 [Turn off enable btn update and report is binded to null after updating]
        btn_update.Enabled = false;
        BindReportToNull();
        //ADD E N D DANIEl 20190102 [Turn off enable btn update and report is binded to null after updating]
        Repeater2.DataSource = null;
        Repeater2.DataBind();

        HiddenField1.Value = "Update successful!";
        HiddenField2.Value = AlertType.Success;
        //CHG E N D DANIEL 20181211 [Add logger function and add update succesful message]
    }

    //CHG START DANIEL 20190110 [Change method type to string]
    //void GenerateList(int PUserGroupID)
    string GenerateList(int PUserGroupID)
    //CHG E N D DANIEL 20190110 [Change method type to string]
    {
        var operationGroup = cc.Do_GetOperationGroup(Convert.ToInt32(DD_UserGroup.SelectedValue));

        //ADD START DANIEL 20190110 [If null, return CMS controller error message]
        if (operationGroup == null)
            return cc.err_msg;
        //ADD E N D DANIEL 20190110 [If null, return CMS controller error message]

        Repeater2.DataSource = operationGroup;
        Repeater2.DataBind();

        //ADD START DANIEL 20190110 [Add return string]
        return string.Empty;
        //ADD E N D DANIEL 20190110 [Add return string]
    }

    //ADD START DANIEL 20190102 [Add new method to bind report to null]
    void BindReportToNull()
    {
        Repeater2.DataSource = null;
        Repeater2.DataBind();
    }
    //ADD E N D DANIEL 20190102 [Add new method to bind report to null]

}