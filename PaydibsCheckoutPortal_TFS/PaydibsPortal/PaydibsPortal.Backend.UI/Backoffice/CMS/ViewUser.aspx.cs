﻿using LoggerII;
using Newtonsoft.Json;
using PaydibsPortal.BLL;
using PaydibsPortal.DAL;
using System;
using System.Reflection;

public partial class Backoffice_CMS_ViewUser : System.Web.UI.Page
{
    HomeController hc = null;
    CMSController cc = new CMSController();
    CurrentUserStat cus = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        HiddenField1.Value = "";
        HiddenField2.Value = "";

        if (Session[SessionType.CurrentUser] == null)
        {
            Response.Redirect("../../Default.aspx");
            return;
        }

        cus = (CurrentUserStat)Session[SessionType.CurrentUser];
        hc = new HomeController(cus.DomainID);

        // ADD START SUKI 20190104 [Check permission on every page load]
        var permission = hc.LoadUserPermission(cus.UserID, Convert.ToInt16(OpsIDEnum.ViewUser));
        if (permission[0].IsUserAllowed == "0")
        {
            Response.Redirect("../../Backoffice/Dashboard.aspx");
            return;
        }
        // ADD E N D SUKI 20190104 [Check permission on every page load]

        if (!IsPostBack)
        {
            var merchant_L = hc.Do_GetMerchantList(1, 0);
            DD_merchant.DataSource = merchant_L;
            DD_merchant.DataTextField = "DomainDesc";
            DD_merchant.DataValueField = "DomainID";
            DD_merchant.DataBind();

            GenerateList(cus.DomainID);
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ViewUser), MethodBase.GetCurrentMethod().Name, "Page_Load Method");
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
    }

    protected void btn_Search_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        var logObject = new
        {
            LoginUser = cus.UserID,
            DomainID = Convert.ToInt32(DD_merchant.SelectedValue)
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            //CHG START DANIEL 20190110 [Insert into AuditTrail]
            //GenerateList(Convert.ToInt32(DD_merchant.SelectedValue));
            string errMsg = GenerateList(Convert.ToInt32(DD_merchant.SelectedValue));

            if (!string.IsNullOrEmpty(errMsg))
            {
                HiddenField1.Value = errMsg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "ViewUser(Error) - " + logJson + cc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ViewUser), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + cc.system_err.ToString());
                return;
            }
            //CHG START DANIEL 20190110 [Insert into AuditTrail]

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "ViewUser - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.ViewUser, 800000, "ViewUser.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ViewUser), MethodBase.GetCurrentMethod().Name, logJson);
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "ViewUser(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.ViewUser, 800001, "ViewUser.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ViewUser), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }

    //CHG START DANIEL 20190110 [Change method type to string]
    //void GenerateList(int PDomainID)
    string GenerateList(int PDomainID)
    //CHG E N D DANIEL 20190110 [Change method type to string]
    {
        var userList = cc.Do_GetUser(PDomainID);

        //ADD START DANIEL 20190110 [If null, return CMS controller error message]
        if (userList == null)
            return cc.err_msg;
        //ADD E N D DANIEL 20190110 [If null, return CMS controller error message]

        Repeater2.DataSource = userList;
        Repeater2.DataBind();

        //ADD START DANIEL 20190110 [Add return string]
        return string.Empty;
        //ADD E N D DANIEL 20190110 [Add return string]
    }
}