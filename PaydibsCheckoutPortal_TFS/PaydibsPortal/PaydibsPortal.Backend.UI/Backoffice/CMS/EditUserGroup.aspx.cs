﻿using LoggerII;
using Newtonsoft.Json;
using PaydibsPortal.BLL;
using PaydibsPortal.DAL;
using System;
using System.Reflection;
using System.Web.UI.WebControls;

public partial class Backoffice_CMS_EditUserGroup : System.Web.UI.Page
{
    HomeController hc = null;
    CMSController cc = new CMSController();
    CurrentUserStat cus = null;
    static int PGroupID = 0;
    int PStatus = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        HiddenField1.Value = "";
        HiddenField2.Value = "";

        if (Session[SessionType.CurrentUser] == null)
        {
            Response.Redirect("../../Default.aspx");
            return;
        }

        cus = (CurrentUserStat)Session[SessionType.CurrentUser];
        hc = new HomeController(cus.DomainID);

        // ADD START SUKI 20190104 [Check permission on every page load]
        var permission = hc.LoadUserPermission(cus.UserID, Convert.ToInt16(OpsIDEnum.EditUserGroup));
        if (permission[0].IsUserAllowed == "0")
        {
            Response.Redirect("../../Backoffice/Dashboard.aspx");
            return;
        }
        // ADD E N D SUKI 20190104 [Check permission on every page load]

        if (!IsPostBack)
        {
            var merchant_L = hc.Do_GetMerchantList(1, 0);
            DD_merchant.DataSource = merchant_L;
            DD_merchant.DataTextField = "DomainDesc";
            DD_merchant.DataValueField = "DomainID";
            DD_merchant.DataBind();

            GenerateList(cus.DomainID);

            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditUserGroup), MethodBase.GetCurrentMethod().Name, "Page_Load Method");
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
    }

    protected void btn_Search_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        var logObject = new
        {
            LoginUser = cus.UserID,
            DomainID = Convert.ToInt32(DD_merchant.SelectedValue)
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            //CHG START DANIEL 20190109 [Insert into AuditTrail]
            //GenerateList(Convert.ToInt32(DD_merchant.SelectedValue));
            var errMsg = GenerateList(Convert.ToInt32(DD_merchant.SelectedValue));
            if (!string.IsNullOrEmpty(errMsg))
            {
                HiddenField1.Value = errMsg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "EditUserGroup(Error) - " + logJson + cc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditUserGroup), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + cc.system_err.ToString());
                return;
            }
            //CHG E N D DANIEL 20190109 [Insert into AuditTrail]

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "EditUserGroup - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.EditUserGroup, 800000, "EditUserGroup.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditUserGroup), MethodBase.GetCurrentMethod().Name, logJson);
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "EditUserGroup(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.EditUserGroup, 800001, "EditUserGroup.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditUserGroup), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }

    protected void lb_Edit_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        var thisButton = (LinkButton)sender;
        var arg = thisButton.CommandArgument;
        var cmd = thisButton.CommandName;

        var logObject = new
        {
            LoginUser = cus.UserID,
            GroupID = arg,
            ModifiedBy = cus.UserID.ToString()
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            var userGroup = cc.Do_GetUserGroup(Convert.ToInt32(DD_merchant.SelectedValue));
            //ADD START DANIEl 20190109 [Insert into AuditTrail]
            if (userGroup == null)
            {
                HiddenField1.Value = cc.err_msg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "EditUserGroup(Error) - " + logJson + cc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditUserGroup), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + cc.system_err.ToString());
                return;
            }
            //ADD E N D DANIEl 20190109 [Insert into AuditTrail]
            foreach (var group in userGroup)
            {
                if (group.GroupID == Convert.ToInt32(arg))
                {
                    txt_GroupDesc.Enabled = true;
                    DD_Status.Enabled = true;
                    btn_Update.Enabled = true;
                    btn_Cancel.Enabled = true;

                    PGroupID = Convert.ToInt32(arg);
                    txt_GroupName.Text = group.GroupName;
                    txt_GroupDesc.Text = group.GroupDesc;
                    DD_Status.SelectedIndex = group.Status == "Active" ? 1 : 0;
                    break;
                }
            }

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "EditUserGroup - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.EditUserGroup, 800000, "EditUserGroup.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditUserGroup), MethodBase.GetCurrentMethod().Name, logJson);
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "EditUserGroup(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.EditUserGroup, 800001, "EditUserGroup.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditUserGroup), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }

    protected void btn_Update_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        var logObject = new
        {
            LoginUser = cus.UserID,
            DomainID = Convert.ToInt32(DD_merchant.SelectedValue),
            GroupID = PGroupID,
            GroupName = txt_GroupName.Text,
            GroupDesc = txt_GroupDesc.Text,
            Status = Convert.ToInt32(DD_Status.SelectedValue)
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            //CHG START DANIEL 20190109 [Insert into AuditTrail]
            //GenerateList(Convert.ToInt32(DD_merchant.SelectedValue));
            var errMsg = GenerateList(Convert.ToInt32(DD_merchant.SelectedValue));
            if (!string.IsNullOrEmpty(errMsg))
            {
                HiddenField1.Value = errMsg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "EditUserGroup(Error) - " + logJson + cc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditUserGroup), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + cc.system_err.ToString());
                return;
            }
            //CHG E N D DANIEL 20190109 [Insert into AuditTrail]

            bool updateUserGroup = cc.Do_UpdateUserGroup(logObject.DomainID, logObject.GroupID, logObject.GroupDesc, logObject.Status, cus.UserID);
            if (!updateUserGroup)
            {
                HiddenField1.Value = cc.err_msg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "EditUserGroup(Error) - " + logJson + cc.system_err.ToString());
                //ADD START DANIEl 20190107 [Insert into AuditTrail]
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditUserGroup), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + cc.system_err.ToString());
                //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
                return;
            }

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "EditUserGroup - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.EditUserGroup, 800000, "EditUserGroup.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditUserGroup), MethodBase.GetCurrentMethod().Name, logJson);
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]

            GenerateList(Convert.ToInt32(DD_merchant.SelectedValue));

            HiddenField1.Value = "Edit User Group Successful!";
            HiddenField2.Value = AlertType.Success;
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "EditUserGroup(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.EditUserGroup, 800001, "EditUserGroup.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditUserGroup), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }

    protected void btn_Cancel_Click(object sender, EventArgs e)
    {
        GenerateList(Convert.ToInt32(DD_merchant.SelectedValue));
    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        var thisButton = (LinkButton)sender;
        var arg = thisButton.CommandArgument;
        var cmd = thisButton.CommandName;

        var logObject = new
        {
            LoginUser = cus.UserID,
            DomainID = Convert.ToInt32(DD_merchant.SelectedValue),
            GroupID = arg,
            Status = cmd.ToString(),
            ModifiedBy = cus.UserID.ToString()
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            var userGroup = cc.Do_GetUserGroup(Convert.ToInt32(DD_merchant.SelectedValue));
            //ADD START DANIEl 20190110 [Insert into AuditTrail]
            if (userGroup == null)
            {
                HiddenField1.Value = cc.err_msg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "EditUserGroup(Error) - " + logJson + cc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditUserGroup), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + cc.system_err.ToString());
                return;
            }
            //ADD E N D DANIEl 20190110 [Insert into AuditTrail]
            string groupDesc = string.Empty;
            foreach (var group in userGroup)
            {
                if (group.GroupID == Convert.ToInt32(arg))
                {
                    groupDesc = group.GroupDesc;
                    break;
                }
            }

            bool rst = false;
            switch (cmd)
            {
                case "Terminate":
                    rst = cc.Do_UpdateUserGroup(logObject.DomainID, int.Parse(arg.ToString()), groupDesc, userStatus.Terminated, cus.UserID.ToString());
                    break;
                case "Active":
                    rst = cc.Do_UpdateUserGroup(logObject.DomainID, int.Parse(arg.ToString()), groupDesc, userStatus.Active, cus.UserID.ToString());
                    break;
                default:
                    break;
            }

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "EditUserGroup - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.EditUserGroup, 800000, "EditUserGroup.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditUserGroup), MethodBase.GetCurrentMethod().Name, logJson);
            //ADD E N D DANIEl 20190107 [Insert into Audit

            if (!rst)
            {
                //CHG START DANIEl 20190107 [Insert into AuditTrail and Logger]
                //HiddenField1.Value = hc.err_msg;
                HiddenField1.Value = cc.err_msg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "EditUserGroup(Error) - " + logJson + cc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditUserGroup), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + cc.system_err.ToString());
                //CHG E N D DANIEl 20190107 [Insert into AuditTrail and Logger]
                return;
            }

             //CHG START DANIEL 20190109 [Insert into AuditTrail]
            //GenerateList(Convert.ToInt32(DD_merchant.SelectedValue));
            var errMsg = GenerateList(Convert.ToInt32(DD_merchant.SelectedValue));
            if (!string.IsNullOrEmpty(errMsg))
            {
                HiddenField1.Value = errMsg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "EditUserGroup(Error) - " + logJson + cc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditUserGroup), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + cc.system_err.ToString());
                return;
            }
            //CHG E N D DANIEL 20190109 [Insert into AuditTrail]

            HiddenField1.Value = "Update successful!";
            HiddenField2.Value = AlertType.Success;
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "EditUserGroup(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.EditUserGroup, 800001, "EditUserGroup.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditUserGroup), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }

    //CHG START DANIEL 20190109 [Change method type to string]
    //void GenerateList(int PDomainID)
    string GenerateList(int PDomainID)
    //CHG E N D DANIEL 20190109 [Change method type to string]
    {
        txt_GroupDesc.Enabled = false;
        DD_Status.Enabled = false;
        btn_Update.Enabled = false;
        btn_Cancel.Enabled = false;

        PGroupID = 0;
        txt_GroupName.Text = string.Empty;
        txt_GroupDesc.Text = string.Empty;
        PStatus = 0;

        var userGroup = cc.Do_GetUserGroup(PDomainID);
        //ADD START DANIEL 20190109 [If null, return CMS controller error msg]
        if (userGroup == null)
            return cc.err_msg;
        //ADD E N D DANIEL 20190109 [If null, return CMS controller error msg]
        Repeater2.DataSource = userGroup;
        Repeater2.DataBind();

        //ADD START DANIEL 20190109 [Return string type]
        return string.Empty;
        //ADD E N D DANIEL 20190109 [Return string type]
    }
}