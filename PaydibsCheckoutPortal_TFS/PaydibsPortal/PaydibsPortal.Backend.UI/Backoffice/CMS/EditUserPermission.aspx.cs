﻿using LoggerII;
using Newtonsoft.Json;
using PaydibsPortal.BLL;
using PaydibsPortal.DAL;
using System;
using System.Reflection;
using System.Web.UI.WebControls;

public partial class Backoffice_CMS_EditUserPermission : System.Web.UI.Page
{
    HomeController hc = null;
    CMSController cc = new CMSController();
    CurrentUserStat cus = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        HiddenField1.Value = "";
        HiddenField2.Value = "";
        if (Session[SessionType.CurrentUser] == null)
        {
            Response.Redirect("../../Default.aspx");
            return;
        }

        cus = (CurrentUserStat)Session[SessionType.CurrentUser];
        hc = new HomeController(cus.DomainID);

        // ADD START SUKI 20190104 [Check permission on every page load]
        var permission = hc.LoadUserPermission(cus.UserID, Convert.ToInt16(OpsIDEnum.EditUserPermission));
        if (permission[0].IsUserAllowed == "0")
        {
            Response.Redirect("../../Backoffice/Dashboard.aspx");
            return;
        }
        // ADD E N D SUKI 20190104 [Check permission on every page load]

        if (!IsPostBack)
        {
            var merchant_L = hc.Do_GetMerchantList(1, 0);
            DD_merchant.DataSource = merchant_L;
            DD_merchant.DataTextField = "DomainDesc";
            DD_merchant.DataValueField = "DomainID";
            DD_merchant.DataBind();

            DD_OpsGroup.Enabled = false;
            DD_UserID.Enabled = false;
            //ADD START DANIEL 20190102 [If first load , btn_update is disabled]
            btn_update.Enabled = false;
            //ADD E N D DANIEL 20190102 [If first load , btn_update is disabled]

            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditUserPermission), MethodBase.GetCurrentMethod().Name, "Page_Load Method");
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
    }

    protected void btn_Search_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        //ADD START DANIEL 20190102 [btn_update is disabled when search button is clicked]
        btn_update.Enabled = false;
        //ADD E N D DANIEL 20190102 [btn_update is disabled when search button is clicked]
        DD_UserGroup.Enabled = true;
        DD_UserID.Enabled = false;
        DD_OpsGroup.Enabled = false;
        DD_UserID.Items.Clear();
        DD_OpsGroup.Items.Clear();

        BindReportToNull();

        var sDomainID = Convert.ToInt32(DD_merchant.SelectedValue);

        var logObject = new
        {
            LoginUser = cus.UserID,
            DomainID = sDomainID
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            var userGroup = cc.Do_GetUserGroup(sDomainID);
            //ADD START START DANIEl 20190109 [Insert into AuditTrail]
            if (userGroup == null)
            {
                HiddenField1.Value = cc.err_msg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "EditUserPermission(Error) - " + logJson + cc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditUserPermission), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + cc.system_err.ToString());
                return;
            }
            //ADD E N D DANIEl 20190109 [Insert into AuditTrail]
            DD_UserGroup.DataSource = userGroup;
            DD_UserGroup.DataValueField = "GroupID";
            DD_UserGroup.DataTextField = "GroupName";
            DD_UserGroup.DataBind();
            DD_UserGroup.Items.Insert(0, new ListItem("--SELECT--", ""));

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "EditUserPermission - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.EditUserPermission, 800000, "EditUserPermission.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditUserPermission), MethodBase.GetCurrentMethod().Name, logJson);
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "EditUserPermission(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.EditUserPermission, 800001, "EditUserPermission.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditUserPermission), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }

    protected void btn_update_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();
        var sUserID = DD_UserID.SelectedValue;
        var sOpsGroupID = Convert.ToInt32(DD_OpsGroup.SelectedValue);

        var logObject = new
        {
            LoginUser = cus.UserID,
            DomainID = Convert.ToInt32(DD_merchant.SelectedValue),
            UserID = sUserID,
            OperationGroupID = sOpsGroupID
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            var mapValue = 0;
            foreach (RepeaterItem item in Repeater2.Items)
            {
                var bitIdx = ((Label)item.FindControl("lbl_bitIdx")).Text;
                var checkBox = (CheckBox)item.FindControl("cb_permission");
                var checkBoxText = checkBox.Checked == true ? "1" : "0";

                mapValue += ConvertBitIdxToMapValue(Convert.ToInt32(bitIdx), Convert.ToInt32(checkBoxText));
            }

            //CHG START DANIEL 20190102 [Store error messages from PGAdmin to log file if any]
            //cc.Do_Insert_Update_UserMap(sUserID, sOpsGroupID, mapValue);
            int insertUpdate_UserMap = cc.Do_Insert_Update_UserMap(sUserID, sOpsGroupID, mapValue);

            if (!string.IsNullOrEmpty(cc.system_err))
            {
                HiddenField1.Value = cc.err_msg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                "EditUserPermission(Error) - " + logJson + cc.system_err.ToString());
                hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.EditUserPermission, 800001, "EditUserPermission.aspx");
                //ADD START DANIEl 20190107 [Insert into AuditTrail]
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditUserPermission), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + cc.system_err.ToString());
                //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
                return;
            }
            //CHG E N D DANIEL 20190102 [Store error messages from PGAdmin to log file if any]

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "EditUserPermission - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.EditUserPermission, 800000, "EditUserPermission.aspx");

            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditUserPermission), MethodBase.GetCurrentMethod().Name, logJson);
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "EditUserPermission(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.EditUserPermission, 800001, "EditUserPermission.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditUserPermission), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        finally
        {
            objLoggerII.Dispose();
        }

        BindReportToNull();

        //ADD START DANIEl 20190102 [Turn off enable btn update after updating]
        btn_update.Enabled = false;
        //ADD E N D DANIEl 20190102 [Turn off enable btn update after updating]

        DD_OpsGroup.ClearSelection();
        HiddenField1.Value = "Update successful!";
        HiddenField2.Value = AlertType.Success;
    }

    //CHG START DANIEL 20190110 [Change method name to OnSelected_UserGroup]
    //protected void OnSelectedIndexChanged(object sender, EventArgs e)
    protected void OnSelected_UserGroup(object sender, EventArgs e)
    //CHG E N D DANIEL 20190110 [Change method name to OnSelected_UserGroup]
    {
        var sDomainID = Convert.ToInt32(DD_merchant.SelectedValue);
        var control = (DropDownList)sender;

        if (control.SelectedIndex > 0)
        {
            var userID = cc.Do_GetUser(sDomainID, Convert.ToInt32(control.SelectedValue));
            DD_UserID.DataSource = userID;
            DD_UserID.DataValueField = "UserID";
            DD_UserID.DataTextField = "UserID";
            DD_UserID.DataBind();
            DD_UserID.Items.Insert(0, new ListItem("--SELECT--", ""));

            DD_UserID.Enabled = true;
            DD_OpsGroup.Enabled = false;
            DD_OpsGroup.Items.Clear();
        }
        else
        {
            DD_UserID.Enabled = false;
            DD_UserID.Items.Clear();
            DD_OpsGroup.Enabled = false;
            DD_OpsGroup.Items.Clear();
        }

        BindReportToNull();
    }

    //CHG START DANIEL 20190110 [Change method name to OnSelected_UserID]
    //protected void OnSelectedIndexChanged_2(object sender, EventArgs e)
    protected void OnSelected_UserID(object sender, EventArgs e)
    //CHG E N D DANIEL 20190110 [Change method name to OnSelected_UserID]
    {
        var control = (DropDownList)sender;

        if (control.SelectedIndex > 0)
        {
            DD_OpsGroup.Enabled = true;

            var opsGroup = cc.Do_GetOperationGroup(Convert.ToInt32(DD_UserGroup.SelectedValue), 1);
            DD_OpsGroup.DataSource = opsGroup;
            DD_OpsGroup.DataValueField = "OpsGroupID";
            DD_OpsGroup.DataTextField = "OpsGroupName";
            DD_OpsGroup.DataBind();
            DD_OpsGroup.Items.Insert(0, new ListItem("--SELECT--", ""));
        }
        else
        {
            DD_OpsGroup.Enabled = false;
            DD_OpsGroup.Items.Clear();
        }

        BindReportToNull();
    }

    //CHG START DANIEL 20190110 [Change method name to OnSelected_OpsGroup]
    //protected void OnSelectedIndexChanged_3(object sender, EventArgs e)
    protected void OnSelected_OpsGroup(object sender, EventArgs e)
    //CHG E N D DANIEL 20190110 [Change method name to OnSelected_OpsGroup]
    {
        var control = (DropDownList)sender;
        var sUserID = DD_UserID.SelectedValue;
        var sOpsGroupID = Convert.ToInt32(DD_OpsGroup.SelectedValue.Equals("") ? "0" : DD_OpsGroup.SelectedValue);

        if (control.SelectedIndex > 0)
        {
            var userPermission = cc.Do_GetUser_Permission(sUserID, sOpsGroupID);
            Repeater2.DataSource = userPermission;
            Repeater2.DataBind();
            //ADD START DANIEL 20190102 [Enable btn_update after selected]
            btn_update.Enabled = true;
            //ADD E N D DANIEL 20190102 [Enable btn_update after selected]
        }
        else
        {
            BindReportToNull();
        }
    }

    void BindReportToNull()
    {
        Repeater2.DataSource = null;
        Repeater2.DataBind();
    }

    int ConvertBitIdxToMapValue(int bitIdx, int isChecked)
    {
        int mapValue = 0;

        if (isChecked == 1)
        {
            mapValue += Convert.ToInt32(Math.Pow(2, bitIdx - 1));
        }
        return mapValue;
    }
}