﻿using LoggerII;
using Newtonsoft.Json;
using PaydibsPortal.BLL;
using PaydibsPortal.DAL;
using System;
using System.Reflection;

public partial class Backoffice_CMS_AddUser : System.Web.UI.Page
{
    HomeController hc = null;
    CMSController cc = new CMSController();
    CurrentUserStat cus = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        HiddenField1.Value = "";
        HiddenField2.Value = "";

        if (Session[SessionType.CurrentUser] == null)
        {
            Response.Redirect("../../Default.aspx");
            return;
        }

        cus = (CurrentUserStat)Session[SessionType.CurrentUser];
        hc = new HomeController(cus.DomainID);

        // ADD START SUKI 20190104 [Check permission on every page load]
        var permission = hc.LoadUserPermission(cus.UserID, Convert.ToInt16(OpsIDEnum.AddUser));
        if (permission[0].IsUserAllowed == "0")
        {
            Response.Redirect("../../Backoffice/Dashboard.aspx");
            return;
        }
        // ADD E N D SUKI 20190104 [Check permission on every page load]

        if (!IsPostBack)
        {
            var merchant_L = hc.Do_GetMerchantList(1, 0);
            DD_merchant.DataSource = merchant_L;
            DD_merchant.DataTextField = "DomainDesc";
            DD_merchant.DataValueField = "DomainID";
            DD_merchant.DataBind();

            GenerateList(cus.DomainID);
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddUser), MethodBase.GetCurrentMethod().Name, "Page_Load Method");
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
    }

    protected void btn_Search_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        var logObject = new
        {
            LoginUser = cus.UserID,
            DomainID = Convert.ToInt32(DD_merchant.SelectedValue)
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            //CHG START DANIEL 20190110 [Insert into AuditTrail]
            //GenerateList(Convert.ToInt32(DD_merchant.SelectedValue));
            string errMsg = GenerateList(Convert.ToInt32(DD_merchant.SelectedValue));
            if (!string.IsNullOrEmpty(errMsg))
            {
                HiddenField1.Value = errMsg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "AddUser(Error) - " + logJson + cc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddUser), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + cc.system_err.ToString());
                return;
            }
            //CHG E N D DANIEL 20190110 [Insert into AuditTrail]

            var userGroup = cc.Do_GetUserGroup(Convert.ToInt32(DD_merchant.SelectedValue));
            //ADD START DANIEl 20190109 [Insert into AuditTrail]
            if (userGroup == null)
            {
                HiddenField1.Value = cc.err_msg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "AddUser(Error) - " + logJson + cc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddUser), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + cc.system_err.ToString());
                return;
            }
            //ADD E N D DANIEl 20190109 [Insert into AuditTrail]

            DD_UserGroup.DataSource = userGroup;
            DD_UserGroup.DataTextField = "GroupName";
            DD_UserGroup.DataValueField = "GroupID";
            DD_UserGroup.DataBind();

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "AddUser - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.AddUser, 800000, "AddUser.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddUser), MethodBase.GetCurrentMethod().Name, logJson);
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "AddUserGroup(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.AddUser, 800001, "AddUser.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddUser), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }

    protected void btn_Add_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        var logObject = new
        {
            LoginUser = cus.UserID,
            DomainID = Convert.ToInt32(DD_merchant.SelectedValue),
            GroupName = DD_UserGroup.SelectedItem.Text,
            UserID = txt_UserID.Text
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            if (string.IsNullOrEmpty(DD_UserGroup.SelectedItem.Text))
            {
                HiddenField1.Value = "Please select one user group.";
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "AddUser - " + logJson + HiddenField1.Value);
                //ADD START START DANIEl 20190107 [Insert into AuditTrail]
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddUser), MethodBase.GetCurrentMethod().Name, logJson + HiddenField1.Value);
                //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
                return;
            }

            //CHG START DANIEL 20190110 [Insert into AuditTrail]
            //GenerateList(Convert.ToInt32(DD_merchant.SelectedValue));
            string errMsg = GenerateList(Convert.ToInt32(DD_merchant.SelectedValue));
            if (!string.IsNullOrEmpty(errMsg))
            {
                HiddenField1.Value = errMsg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "AddUser(Error) - " + logJson + cc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddUser), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + cc.system_err.ToString());
                return;
            }
            //CHG E N D DANIEL 20190110 [Insert into AuditTrail]

            var userList = cc.Do_GetUser(Convert.ToInt32(DD_merchant.SelectedValue));
            //ADD START DANIEl 20190109 [Insert into AuditTrail]
            if (userList == null)
            {
                HiddenField1.Value = cc.err_msg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "AddUser(Error) - " + logJson + cc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddUser), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + cc.system_err.ToString());
                return;
            }
            //ADD E N D DANIEl 20190109 [Insert into AuditTrail]


            foreach (var row in userList)
            {
                if (row.UserID == txt_UserID.Text.Trim())
                {
                    HiddenField1.Value = "This user already exist.";
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "AddUser - " + logJson + HiddenField1.Value);
                    //ADD START START DANIEl 20190107 [Insert into AuditTrail]
                    hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddUser), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + HiddenField1.Value);
                    //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
                    return;
                }
            }

            bool addUser = cc.Do_AddUser(logObject.DomainID, DD_UserGroup.SelectedValue, logObject.UserID, cus.UserID);
            if (!addUser)
            {
                HiddenField1.Value = cc.err_msg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "AddUser(Error) - " + logJson + cc.system_err.ToString());
                //ADD START START DANIEl 20190107 [Insert into AuditTrail]
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddUser), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + cc.system_err.ToString());
                //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
                return;
            }

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "AddUser - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.AddUser, 800000, "AddUser.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddUser), MethodBase.GetCurrentMethod().Name, logJson);
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]

            GenerateList(Convert.ToInt32(DD_merchant.SelectedValue));

            HiddenField1.Value = "Add User Successful!";
            HiddenField2.Value = AlertType.Success;
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "AddUser(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.AddUser, 800001, "AddUser.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AddUser), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }

    //CHG START DANIEL 20190110 [Change method type to string]
    //void GenerateList(int PDomainID)
    string GenerateList(int PDomainID)
    //CHG E N D DANIEL 20190110 [Change method type to string]
    {
        var user = cc.Do_GetUser(PDomainID);

        //ADD START DANIEL 20190110 [If null, return CMS Controller error message]
        if (user == null)
            return cc.err_msg;
        //ADD E N D DANIEL 20190110 [If null, return CMS Controller error message]

        Repeater2.DataSource = user;
        Repeater2.DataBind();

        //ADD START DANIEL 20190110 [Add return string]
        return string.Empty;
        //ADD E N D DANIEL 20190110 [Add return string]
    }
}