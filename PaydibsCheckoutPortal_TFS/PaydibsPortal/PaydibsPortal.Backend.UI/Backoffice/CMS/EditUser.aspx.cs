﻿using LoggerII;
using Newtonsoft.Json;
using PaydibsPortal.BLL;
using PaydibsPortal.DAL;
using System;
using System.Net;
using System.Reflection;
using System.Security;
using System.Web.UI.WebControls;

public partial class Backoffice_CMS_EditUser : System.Web.UI.Page
{
    HomeController hc = null;
    CMSController cc = new CMSController();
    CurrentUserStat cus = null;
    static MMGMTController mc = new MMGMTController();
    static SecureString secureDefPwd = new NetworkCredential("", mc.Do_Decrypt3DES(System.Configuration.ConfigurationManager.AppSettings["DefaultPwd"])).SecurePassword;
 
    protected void Page_Load(object sender, EventArgs e)
    {
        HiddenField1.Value = "";
        HiddenField2.Value = "";

        if (Session[SessionType.CurrentUser] == null)
        {
            Response.Redirect("../../Default.aspx");
            return;
        }

        cus = (CurrentUserStat)Session[SessionType.CurrentUser];
        hc = new HomeController(cus.DomainID);

        // ADD START SUKI 20190104 [Check permission on every page load]
        var permission = hc.LoadUserPermission(cus.UserID, Convert.ToInt16(OpsIDEnum.EditUser));
        if (permission[0].IsUserAllowed == "0")
        {
            Response.Redirect("../../Backoffice/Dashboard.aspx");
            return;
        }
        // ADD E N D SUKI 20190104 [Check permission on every page load]

        if (!IsPostBack)
        {
            var merchant_L = hc.Do_GetMerchantList(1, 0);
            DD_merchant.DataSource = merchant_L;
            DD_merchant.DataTextField = "DomainDesc";
            DD_merchant.DataValueField = "DomainID";
            DD_merchant.DataBind();

            GenerateList(cus.DomainID);
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditUser), MethodBase.GetCurrentMethod().Name, "Page_Load Method");
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
    }

    protected void btn_Search_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        var logObject = new
        {
            LoginUser = cus.UserID,
            DomainID = Convert.ToInt32(DD_merchant.SelectedValue)
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            //CHG START DANIEL 20190110 [Insert into AuditTrail]
            //GenerateList(Convert.ToInt32(DD_merchant.SelectedValue));
            string errMsg = GenerateList(Convert.ToInt32(DD_merchant.SelectedValue));
            if (!string.IsNullOrEmpty(errMsg))
            {
                HiddenField1.Value = errMsg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "EditUser(Error) - " + logJson + cc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditUser), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + cc.system_err.ToString());
                return;
            }
            //CHG E N D DANIEL 20190110 [Insert into AuditTrail]

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "EditUser - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.EditUser, 800000, "EditUser.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditUser), MethodBase.GetCurrentMethod().Name, logJson);
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "EditUser(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.EditUser, 800001, "EditUser.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditUser), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }

    protected void lb_Edit_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        var thisButton = (LinkButton)sender;
        var arg = thisButton.CommandArgument;
        var cmd = thisButton.CommandName;

        var logObject = new
        {
            LoginUser = cus.UserID,
            UserID = arg,
            ModifiedBy = cus.UserID.ToString()
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            var userGroup = cc.Do_GetUserGroup(Convert.ToInt32(DD_merchant.SelectedValue));
            //ADD START START DANIEl 20190109 [Insert into AuditTrail]
            if (userGroup == null)
            {
                HiddenField1.Value = cc.err_msg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "EditUser(Error) - " + logJson + cc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditUser), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + cc.system_err.ToString());
                return;
            }
            //ADD E N D DANIEl 20190109 [Insert into AuditTrail]
            DD_UserGroup.DataSource = userGroup;
            DD_UserGroup.DataTextField = "GroupName";
            DD_UserGroup.DataValueField = "GroupID";
            DD_UserGroup.DataBind();

            var userList = cc.Do_GetUser(Convert.ToInt32(DD_merchant.SelectedValue));
            //ADD START START DANIEl 20190110 [Insert into AuditTrail]
            if (userList == null)
            {
                HiddenField1.Value = cc.err_msg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "EditUser(Error) - " + logJson + cc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditUser), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + cc.system_err.ToString());
                return;
            }
            //ADD E N D DANIEl 20190110 [Insert into AuditTrail]
            foreach (var user in userList)
            {
                if (user.UserID == arg)
                {
                    DD_UserGroup.Enabled = true;
                    DD_Status.Enabled = true;
                    btn_Update.Enabled = true;
                    btn_Cancel.Enabled = true;

                    DD_UserGroup.SelectedIndex = DD_UserGroup.Items.IndexOf(DD_UserGroup.Items.FindByText(user.GroupName));
                    txt_UserID.Text = user.UserID;
                    DD_Status.SelectedIndex = user.Status == "Active" ? 1 : 0;
                    break;
                }
            }

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "EditUser - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.EditUser, 800000, "EditUser.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditUser), MethodBase.GetCurrentMethod().Name, logJson);
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "EditUser(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.EditUser, 800001, "EditUser.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditUser), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }

    protected void btn_Update_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        var logObject = new
        {
            LoginUser = cus.UserID,
            DomainID = Convert.ToInt32(DD_merchant.SelectedValue),
            GroupName = DD_UserGroup.SelectedItem.Text,
            UserID = txt_UserID.Text,
            Status = Convert.ToInt32(DD_Status.SelectedValue)
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            //CHG START DANIEL 20190110 [Insert into AuditTrail]
            //GenerateList(Convert.ToInt32(DD_merchant.SelectedValue));
            string errMsg = GenerateList(Convert.ToInt32(DD_merchant.SelectedValue));
            if (!string.IsNullOrEmpty(errMsg))
            {
                HiddenField1.Value = errMsg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "EditUser(Error) - " + logJson + cc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditUser), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + cc.system_err.ToString());
                return;
            }
            //CHG E N D DANIEL 20190110 [Insert into AuditTrail]

            bool updateUser = cc.Do_UpdateUser(logObject.DomainID, DD_UserGroup.SelectedValue, logObject.UserID, logObject.Status, cus.UserID, 0);
            if (!updateUser)
            {
                HiddenField1.Value = cc.err_msg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "EditUser(Error) - " + logJson + cc.system_err.ToString());
                //ADD START DANIEl 20190107 [Insert into AuditTrail]
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditUser), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + cc.system_err.ToString());
                //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
                return;
            }

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "EditUser - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.EditUser, 800000, "EditUser.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditUser), MethodBase.GetCurrentMethod().Name, logJson);
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]

            GenerateList(Convert.ToInt32(DD_merchant.SelectedValue));

            HiddenField1.Value = "Edit User Successful!";
            HiddenField2.Value = AlertType.Success;
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "EditUser(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.EditUser, 800001, "EditUser.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditUser), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }

    protected void btn_Cancel_Click(object sender, EventArgs e)
    {
        GenerateList(Convert.ToInt32(DD_merchant.SelectedValue));
    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        var thisButton = (LinkButton)sender;
        var arg = thisButton.CommandArgument;
        var cmd = thisButton.CommandName;

        var logObject = new
        {
            LoginUser = cus.UserID,
            DomainID = Convert.ToInt32(DD_merchant.SelectedValue),
            UserID = arg,
            Status = cmd.ToString(),
            ModifiedBy = cus.UserID.ToString()
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            bool rst = false;
            switch (cmd)
            {
                //CHG START DANIEL 20190108 [Add new case "Reset_Password" to change to default password. Add Hidden1 and 2 to each case statement]
                //case "Terminate":
                //    rst = cc.Do_UpdateUser(logObject.DomainID, "0", logObject.UserID, userStatus.Terminated, cus.UserID.ToString(), 1);
                //    break;
                //case "Active":
                //    rst = cc.Do_UpdateUser(logObject.DomainID, "0", logObject.UserID, userStatus.Active, cus.UserID.ToString(), 1);
                //    break;
                case "Terminate":
                    {
                        rst = cc.Do_UpdateUser(logObject.DomainID, "0", logObject.UserID, userStatus.Terminated, cus.UserID.ToString(), 1);
                        //ADD START DANIEL 20190110 [Insert into AuditTrail]
                        if (!rst)
                        {
                            HiddenField1.Value = cc.err_msg;
                            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                   "EditUser(Error) - " + logJson + cc.system_err.ToString());

                            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AccountDetail), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + cc.system_err.ToString());
                            return;
                        }
                        //ADD E N D DANIEL 20190110 [Insert into AuditTrail]

                        HiddenField1.Value = "Update successful!";
                        HiddenField2.Value = AlertType.Success;
                        break;
                    }
                case "Active":
                    {
                        rst = cc.Do_UpdateUser(logObject.DomainID, "0", logObject.UserID, userStatus.Active, cus.UserID.ToString(), 1);
                        //ADD START DANIEL 20190110 [Insert into AuditTrail]
                        if (!rst)
                        {
                            HiddenField1.Value = cc.err_msg;
                            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                   "EditUser(Error) - " + logJson + cc.system_err.ToString());

                            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AccountDetail), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + cc.system_err.ToString());
                            return;
                        }
                        //ADD E N D DANIEL 20190110 [Insert into AuditTrail]

                        HiddenField1.Value = "Update successful!";
                        HiddenField2.Value = AlertType.Success;
                        break;
                    }
                case "Reset_Password":
                    {
                        //CHG START DANIEL 20190226 [Convert default pwd to secureString]
                        //rst = hc.Do_Update_User_Password(logObject.UserID, "user12345");
                        rst = hc.Do_Update_User_Password(logObject.UserID, new NetworkCredential("", secureDefPwd).Password);
                        //CHG E N D DANIEL 20190226 [Convert default pwd to secureString]

                        //ADD START DANIEL 20190110 [Insert into AuditTrail]
                        if (!rst)
                        {
                            HiddenField1.Value = hc.err_msg;
                            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                   "EditUser(Error) - " + logJson + hc.system_err.ToString());

                            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AccountDetail), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + hc.system_err.ToString());
                            return;
                        }
                        //ADD E N D DANIEL 20190110 [Insert into AuditTrail]

                        HiddenField1.Value = "Password changed succesfully to default password.";
                        HiddenField2.Value = AlertType.Success;
                        break;
                    }
                //CHG E N D DANIEL 20190108 [Add new case "Reset_Password" to change to default password. Add Hidden1 and 2 to each case statement]

                default:
                    break;
            }

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "EditUser - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.EditUser, 800000, "EditUser.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditUser), MethodBase.GetCurrentMethod().Name, logJson);
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]

            //DEL START DANIEL 20190110 [Move to switch case statement]
            //if (!rst)
            //{
            //    //CHG START DANIEl 20190107 [Insert into AuditTrail]
            //    //HiddenField1.Value = hc.err_msg;
            //    HiddenField1.Value = cc.err_msg;
            //    objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
            //           "EditUser(Error) - " + logJson + cc.system_err.ToString());

            //    hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AccountDetail), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + cc.system_err.ToString());
            //    //CHG E N D DANIEl 20190107 [Insert into AuditTrail]
            //    return;
            //}
            //DEL E N d DANIEL 20190110 [Move to switch case statement]

            //CHG START DANIEL 20190110 [Insert into AuditTrail]
            //GenerateList(Convert.ToInt32(DD_merchant.SelectedValue));
            string errMsg = GenerateList(Convert.ToInt32(DD_merchant.SelectedValue));
            if (!string.IsNullOrEmpty(errMsg))
            {
                HiddenField1.Value = errMsg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.WARNING, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "EditUser(Error) - " + logJson + cc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditUser), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + cc.system_err.ToString());
                return;
            }
            //CHG E N D DANIEL 20190110 [Insert into AuditTrail]

            //DEL START DANIEL 20190108 [Remove unnecessary codes]
            //HiddenField1.Value = "Update successful!";
            //HiddenField2.Value = AlertType.Success;
            //DEL E N D DANIEL 20190108 [Remove unnecessary codes]
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "EditUser(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.EditUser, 800001, "EditUser.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.EditUser), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }

    //CHG START DANIEL 20190110 [Change method type]
    //void GenerateList(int PDomainID)
    string GenerateList(int PDomainID)
    //CHG E N D DANIEL 20190110 [Change method type]
    {
        DD_UserGroup.Enabled = false;
        DD_Status.Enabled = false;
        btn_Update.Enabled = false;
        btn_Cancel.Enabled = false;
        txt_UserID.Text = string.Empty;

        var user = cc.Do_GetUser(PDomainID);

        //ADD START DANIEL 20190110 [If null, return CMS controller error message]
        if (user == null)
            return cc.err_msg;
        //ADD E N D DANIEL 20190110 [If null, return CMS controller error message]

        Repeater2.DataSource = user;
        Repeater2.DataBind();

        //ADD START DANIEL 20190110 [Add return string]
        return string.Empty;
        //ADD E N D DANIEL 20190110 [Add return string]
    }
}