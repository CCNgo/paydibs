﻿using LoggerII;
using Newtonsoft.Json;
using PaydibsPortal.BLL;
using PaydibsPortal.DAL;
using System;
using System.Net;
using System.Reflection;
using System.Security;

public partial class ChangePassword : System.Web.UI.Page
{
    HomeController hc = null;
    CurrentUserStat cus = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        HiddenField1.Value = "";
        HiddenField2.Value = "";

        if (Session[SessionType.CurrentUser] == null)
        {
            Response.Redirect("../../Default.aspx");
            return;
        }

        cus = (CurrentUserStat)Session[SessionType.CurrentUser];
        hc = new HomeController(cus.DomainID);

        // ADD START SUKI 20190104 [Check permission on every page load]
        var permission = hc.LoadUserPermission(cus.UserID, Convert.ToInt16(OpsIDEnum.ChangePassword));
        if (permission[0].IsUserAllowed == "0")
        {
            Response.Redirect("../../Backoffice/Dashboard.aspx");
            return;
        }
        // ADD E N D SUKI 20190104 [Check permission on every page load]

        //ADD START DANIEl 20190107 [Insert into AuditTrail]
        if (!IsPostBack)
        {
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ChangePassword), MethodBase.GetCurrentMethod().Name, "Page_Load Method");
            txt_currentPassword.Text = string.Empty;
        }
        //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
    }

    protected void btn_Change_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        //ADD START DANIEL 20190107 [Initialize logObject and assign to logJson]
        var logObject = new
        {
            LoginUser = cus.UserID
        };
        var logJson = JsonConvert.SerializeObject(logObject);
        //ADD E N D DANIEL 20190107 [Initialize logObject and assign to logJson]

        try
        {
            objLoggerII = logger.InitLogger();
            // CHG START KENT LOONG 20190325[SECURE STRING METHOD]
            //string currPwd = txt_currentPassword.Text.Trim();
            //string newPwd = txt_password.Text.Trim();
            //string confirmPwd = txt_confirmPassword.Text.Trim();
            SecureString currPwd = new NetworkCredential("", txt_currentPassword.Text.Trim()).SecurePassword;
            SecureString newPwd = new NetworkCredential("", txt_password.Text.Trim()).SecurePassword;
            SecureString confirmPwd = new NetworkCredential("", txt_confirmPassword.Text.Trim()).SecurePassword;
            // CHG E N D KENT LOONG 20190325[SECURE STRING METHOD]
            dynamic userID = hc.Do_Check_User_Login(cus.UserID, new NetworkCredential("", currPwd).Password, "");
            if (userID == null)
            {
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "ChangePassword - " + cus.UserID + hc.system_err);
                HiddenField1.Value = hc.err_msg;
                //ADD START DANIEl 20190107 [Insert into AuditTrail]
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ChangePassword), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + hc.system_err.ToString());
                //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
                return;
            }

            bool chkPwd = hc.Do_Check_User_Password(cus.UserID, new NetworkCredential("", newPwd).Password);
            if (!chkPwd)
            {
                HiddenField1.Value = hc.err_msg;
                if (hc.system_err == string.Empty)
                //CHG START DANIEl 20190107 [Insert into AuditTrail]
                //objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                //                            "ChangePassword(Error) - " + cus.UserID + hc.system_err.ToString());
                {

                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                          "ChangePassword(Error) - " + cus.UserID + hc.system_err.ToString());
                    hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ChangePassword), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + hc.system_err.ToString());
                }
                //CHG E N D DANIEl 20190107 [Insert into AuditTrail]
                return;
            }

            bool updPwd = hc.Do_Update_User_Password(cus.UserID, new NetworkCredential("", newPwd).Password);
            if (!updPwd)
            {
                HiddenField1.Value = hc.err_msg;
                if (hc.system_err == string.Empty)
                //CHG START DANIEL 20190108 [Insert into AuditTrail]
                //objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                //           "ChangePassword(Error) - " + cus.UserID + hc.system_err.ToString());
                {
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                                       "ChangePassword(Error) - " + cus.UserID + hc.system_err.ToString());
                    hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ChangePassword), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + hc.system_err.ToString());
                }
                //CHG START DANIEL 20190108 [Insert into AuditTrail]
                return;
            }

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "ChangePassword - " + cus.UserID);

            hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.ChangePassword, 800000, "ChangePassword.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ChangePassword), MethodBase.GetCurrentMethod().Name, logJson);
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]

            HiddenField1.Value = "You have successfully change your password! Please login with the new password now.";
            HiddenField2.Value = AlertType.Success;
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "ChangePassword(Error) - " + cus.UserID + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.ChangePassword, 800001, "ChangePassword.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ChangePassword), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        finally
        {
            objLoggerII.Dispose();
        }

    }
}