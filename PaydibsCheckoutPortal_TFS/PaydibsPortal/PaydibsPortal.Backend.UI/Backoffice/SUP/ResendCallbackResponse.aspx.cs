﻿using Kenji.Apps;
using LoggerII;
using Newtonsoft.Json;
using PaydibsPortal.BLL;
using PaydibsPortal.DAL;
using System;
using System.Reflection;
using System.Web.UI.WebControls;

public partial class Backoffice_SUP_ResendCallbackResponse : System.Web.UI.Page
{
    HomeController hc = null;
    SUPController sc = null;
    CurrentUserStat cus = null;
    LinqTool linq = new LinqTool();
    MMGMTController mc = new MMGMTController();

    protected void Page_Load(object sender, EventArgs e)
    {
        HiddenField1.Value = "";
        HiddenField2.Value = "";

        if (Session[SessionType.CurrentUser] == null)
        {
            Response.Redirect("../../Default.aspx");
            return;
        }

        cus = (CurrentUserStat)Session[SessionType.CurrentUser];
        hc = new HomeController(cus.DomainID);
        sc = new SUPController();

        var permission = hc.LoadUserPermission(cus.UserID, Convert.ToInt16(OpsIDEnum.ResendCallBackResponse));
        if (permission[0].IsUserAllowed == "0")
        {
            Response.Redirect("../../Backoffice/Dashboard.aspx");
            return;
        }

        if (!IsPostBack)
        {
            var pymtMethodList = mc.Do_GetCLPymtMethod();
            DD_Method.DataSource = pymtMethodList;
            DD_Method.DataTextField = "PymtMethodDesc";
            DD_Method.DataValueField = "PymtMethodCode";
            DD_Method.DataBind();

            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ResendCallBackResponse), MethodBase.GetCurrentMethod().Name, "Page_Load Method");
        }
    }

    protected void btn_GetResponse_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();


        var logObject = new
        {
            LoginUser = cus.UserID,

            GatewayTransactionID = tb_gatewayTxnID.Text.Trim(),
            PaymentMethod = DD_Method.SelectedValue
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            var getCallBack = sc.Do_Get_CallBack_Response(DD_Method.SelectedValue, tb_gatewayTxnID.Text.Trim());
            if (getCallBack == null)
            {
                HiddenField1.Value = sc.err_msg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                 "ResendCallBackResponse(Error) - " + logJson + sc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ResendCallBackResponse), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + sc.system_err.ToString());

                return;
            }

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                          "ResendCallBackResponse - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.ResendCallBackResponse, 800000, "ResendCallBackResponse.aspx");
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ResendCallBackResponse), MethodBase.GetCurrentMethod().Name, logJson);

            Repeater2.DataSource = getCallBack;
            Repeater2.DataBind();
        }
        catch (Exception ex)
        {
            HiddenField1.Value = ex.ToString();
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                             "ResendCallBackResponse(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.ResendCallBackResponse, 800001, "ResendCallBackResponse.aspx");
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ResendCallBackResponse), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }

    protected void Repeater_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            LinkButton lb_Send = (LinkButton)e.Item.FindControl("lb_Send");
            string[] commandArgs = new string[2];
            commandArgs = lb_Send.CommandArgument.ToString().Split(',');
            if (commandArgs[0].Equals(""))
            {
                lb_Send.Visible = false;
            }
        }
    }

    protected void btn_Send_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        LinkButton lb_Send = (LinkButton)(sender);
        string[] commandArgs = new string[2];
        commandArgs = lb_Send.CommandArgument.ToString().Split(',');

        string callbackUrl = commandArgs[0];
        string merchantID = commandArgs[1];
        string paymentID = commandArgs[2];
        string currCode = commandArgs[3];
        string txnAmt = commandArgs[4];
        string orderID = commandArgs[5];
        string txnStatus = commandArgs[6];
        string authCode = commandArgs[7];
        string txnID = commandArgs[8];
        string acqBank = commandArgs[9];
        string bankRefNo = commandArgs[10];
        string txnMsg = commandArgs[11];
        var merchantPwd = sc.getMerchantInfoByID(merchantID);
        string sign = sc.szComputeSHA512(merchantPwd + merchantID + paymentID + txnID +
                                          orderID + txnAmt + currCode + txnStatus + (!string.IsNullOrEmpty(authCode) ? authCode : string.Empty));

        var respParams = new
        {
            TxnType = "PAY",
            Method = DD_Method.SelectedValue.Trim(),
            MerchantID = merchantID,
            MerchantPymtID = paymentID,
            MerchantOrdID = orderID,
            MerchantTxnAmt = txnAmt,
            MerchantCurrCode = currCode,
            PTxnID = txnID,
            PTxnStatus = txnStatus,
            AcqBank = acqBank,
            PTxnMsg = txnMsg,
            AuthCode = authCode,
            BankRefNo = bankRefNo,
            Sign = sign
        };
        var jsonRespParam = JsonConvert.SerializeObject(respParams);

        var logObject = new
        {
            LoginUser = cus.UserID,

            TxnType = "PAY",
            Method = DD_Method.SelectedValue.Trim(),
            MerchantID = merchantID,
            MerchantPymtID = paymentID,
            MerchantOrdID = orderID,
            MerchantTxnAmt = txnAmt,
            MerchantCurrCode = currCode,
            PTxnID = txnID,
            PTxnStatus = txnStatus,
            AcqBank = acqBank,
            PTxnMsg = txnMsg,
            AuthCode = authCode,
            BankRefNo = bankRefNo,
            Sign = sign

        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            var postResponse = hc.Response_POST(callbackUrl, jsonRespParam);
            if (!string.IsNullOrEmpty(hc.err_msg))
            {
                HiddenField1.Value = hc.err_msg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                 "ResendCallBackResponse(Error) - " + logJson + sc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ResendCallBackResponse), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + hc.system_err.ToString());

                return;
            }
            else
            {
                HiddenField1.Value = postResponse;
                HiddenField2.Value = AlertType.Success;
            }

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                          "ResendCallBackResponse - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.ResendCallBackResponse, 800000, "ResendCallBackResponse.aspx");
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ResendCallBackResponse), MethodBase.GetCurrentMethod().Name, logJson);
        }
        catch (Exception ex)
        {
            HiddenField1.Value = ex.ToString();
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                             "ResendCallBackResponse(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.ResendCallBackResponse, 800001, "ResendCallBackResponse.aspx");
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ResendCallBackResponse), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
        }
        finally
        {
            objLoggerII.Dispose();
        }

        //    switch (System.Configuration.ConfigurationManager.AppSettings["ProtocolType"].ToUpper())
        //    {
        //        case "SSL3":
        //            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
        //            break;
        //        case "TLS":
        //            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;
        //            break;
        //        case "TLS11":
        //            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11;
        //            break;
        //        case "TLS12":
        //            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        //            break;
        //        default:
        //            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        //            break;
        //    }
        //    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

        //    //StringBuilder sb = new StringBuilder();
        //    //sb.Append("TxnType=QUERY");
        //    //sb.Append("&Method=" + DD_Method.SelectedValue.Trim());
        //    //sb.Append("&MerchantID=" + merchantID);
        //    //sb.Append("&MerchantPymtID=" + paymentID);
        //    //sb.Append("&MerchantTxnAmt=" + txnAmt);
        //    //sb.Append("&MerchantCurrCode=" + currCode);
        //    //sb.Append("&PTxnID=" + txnID);
        //    //sb.Append("&Sign=" + sign);

        //    WebRequest request = WebRequest.Create(callbackUrl);
        //    request.ContentType = "application/x-www-form-urlencoded";
        //    request.Method = "POST";
        //    //byte[] byteArray = Encoding.UTF8.GetBytes(sb.ToString());
        //    //request.ContentLength = byteArray.Length;
        //    //Stream dataStream = request.GetRequestStream();
        //    using (var streamWriter = new StreamWriter(request.GetRequestStream()))
        //    {
        //        streamWriter.Write(jsonRespParam);
        //    }

        //    string result = string.Empty;
        //    var httpResponse = (HttpWebResponse)request.GetResponse();

        //    if (httpResponse.StatusCode == HttpStatusCode.OK)
        //    {
        //        using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
        //        {
        //            result = streamReader.ReadToEnd();
        //        }
        //    }
        //    //Response.Write(result);
        //}
        //catch (WebException ex)
        //{
        //    var err = ex.ToString();
        //}
    }
}