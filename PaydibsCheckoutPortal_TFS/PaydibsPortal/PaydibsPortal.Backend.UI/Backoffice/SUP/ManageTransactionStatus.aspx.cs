﻿using Kenji.Apps;
using LoggerII;
using Newtonsoft.Json;
using PaydibsPortal.BLL;
using PaydibsPortal.DAL;
using System;
using System.Reflection;
using System.Web.UI.WebControls;

public partial class Backoffice_SUP_ManageTransactionStatus : System.Web.UI.Page
{
    HomeController hc = null;
    TXNController tc = null;
    SUPController sc = null;
    CurrentUserStat cus = null;
    LinqTool linq = new LinqTool();
    MMGMTController mc = new MMGMTController();
    public string cmd_gateTxnID = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        HiddenField1.Value = string.Empty;
        HiddenField2.Value = string.Empty;

        if (Session[SessionType.CurrentUser] == null)
        {
            Response.Redirect("../../Default.aspx");
            return;
        }
        cus = (CurrentUserStat)Session[SessionType.CurrentUser];
        hc = new HomeController(cus.DomainID);
        tc = new TXNController(cus.DomainID);
        sc = new SUPController();

        var permission = hc.LoadUserPermission(cus.UserID, Convert.ToInt16(OpsIDEnum.ManageTransactionStatus));
        if (permission[0].IsUserAllowed == "0")
        {
            Response.Redirect("../../Backoffice/Dashboard.aspx");
            return;
        }

        if (!IsPostBack)
        {
            var merchant_L = hc.Do_GetMerchantList(1, 1);
            DD_merchant.DataSource = merchant_L;
            DD_merchant.DataTextField = "DomainDesc";
            DD_merchant.DataValueField = "DomainID";
            DD_merchant.DataBind();

            DateTime sSince = Convert.ToDateTime(TB_Since.Text.Trim() + " 00:00:00");
            DateTime sTo = Convert.ToDateTime(TB_To.Text.Trim() + " 23:59:59");
            TB_Since.Text = sSince.ToString("yyyy-MM-dd");
            TB_To.Text = sTo.ToString("yyyy-MM-dd");

            var currencyList = mc.Do_GetCurrencyCountry(0);
            DD_Currency.DataSource = currencyList;
            DD_Currency.DataTextField = "CurrCode";
            DD_Currency.DataValueField = "CurrCode";
            DD_Currency.DataBind();
            DD_Currency.SelectedValue = "MYR";

            var pymtMethodList = mc.Do_GetCLPymtMethod();
            DD_PymtMethod.DataSource = pymtMethodList;
            DD_PymtMethod.DataTextField = "PymtMethodDesc";
            DD_PymtMethod.DataValueField = "PymtMethodCode";
            DD_PymtMethod.DataBind();
            DD_PymtMethod.SelectedValue = "OB ";

            string sGatewayTxnID = Request.QueryString["gatewayTxnID"];
            DateTime PSince = Convert.ToDateTime(Request.QueryString["since"]);
            DateTime PTo = Convert.ToDateTime(Request.QueryString["to"]);

            //Pass query string from ViewLateResponse
            Manage_Txn_Status[] data = null;
            if (!string.IsNullOrEmpty(sGatewayTxnID))
                data = GenerateList(1, sGatewayTxnID, PSince, PTo, "", "'OB '", "");

            Repeater2.DataSource = data;
            Repeater2.DataBind();

            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ManageTransactionStatus), MethodBase.GetCurrentMethod().Name, "Page_Load Method");
        }
    }

    protected void btn_GetInfo_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        DateTime PSince = Convert.ToDateTime(TB_Since.Text.Trim() + " 00:00:00");
        DateTime PTo = Convert.ToDateTime(TB_To.Text.Trim() + " 23:59:59");
        int PDomainID = Convert.ToInt32(DD_merchant.SelectedValue);
        string PPaymentID = TB_PaymentID.Text.Trim();
        string PGatewayTxnID = TB_GatewayTxnID.Text.Trim();
        string PPymtMethod = DD_PymtMethod.SelectedValue;
        string PCurrencyCode = DD_Currency.SelectedValue == "ALL" ? "" : DD_Currency.SelectedValue;

        var logObject = new
        {
            LoginUser = cus.UserID,
            DomainID = PDomainID,
            Since = PSince,
            To = PTo,
            GatewayTransactionID = PGatewayTxnID,
            PaymentMethod = PPymtMethod,
            CurrencyCode = PCurrencyCode,
            PaymentID = PPaymentID
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            GenerateList(PDomainID, PGatewayTxnID, PSince, PTo, PPaymentID, PPymtMethod, PCurrencyCode);

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                          "ManageTransactionStatus - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.ManageTransactionStatus, 800000, "ManageTransactionStatus.aspx");
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ManageTransactionStatus), MethodBase.GetCurrentMethod().Name, logJson);
        }
        catch (Exception ex)
        {
            HiddenField1.Value = ex.ToString();
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                             "ManageTransactionStatus(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.ManageTransactionStatus, 800001, "ManageTransactionStatus.aspx");
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ManageTransactionStatus), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }

    protected void btn_Cancel_Click(object sender, EventArgs e)
    {

    }

    protected void RP_Channel_DataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lbl_gatewayTxnID = (Label)e.Item.FindControl("lbl_gatewayTxnID");
            DropDownList ddl_ChgStatus = (DropDownList)e.Item.FindControl("ddl_ChgStatus");
            ddl_ChgStatus.Items.Insert(0, new ListItem("--SELECT--", "0"));
            ddl_ChgStatus.Items.Insert(1, new ListItem("Success", "1"));
            ddl_ChgStatus.Items.Insert(2, new ListItem("Refunded", "2"));
            ddl_ChgStatus.Items.Insert(3, new ListItem("Reversed", "3"));
            ddl_ChgStatus.SelectedIndexChanged += new EventHandler(ddl_ChgStatus_OnSelectedIndex);
        }
    }

    protected void ddl_ChgStatus_OnSelectedIndex(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        var ddlControl = (DropDownList)sender;
        var rptParent = ddlControl.NamingContainer;
        Label lbl_gatewayTxnID = (Label)rptParent.FindControl("lbl_gatewayTxnID");
        Label lbl_status = (Label)rptParent.FindControl("lbl_status");
        HiddenField hid_payRes = (HiddenField)rptParent.FindControl("hid_payRes");

        DateTime PSince = Convert.ToDateTime(TB_Since.Text.Trim() + " 00:00:00");
        DateTime PTo = Convert.ToDateTime(TB_To.Text.Trim() + " 23:59:59");
        int PDomainID = Convert.ToInt32(DD_merchant.SelectedValue);
        string PCurrencyCode = DD_Currency.SelectedValue == "ALL" ? "" : DD_Currency.SelectedValue;
        string PPaymentID = TB_PaymentID.Text.Trim();
        string PGatewayTxnID = TB_GatewayTxnID.Text.Trim();
        string PPymtMethod = DD_PymtMethod.SelectedValue;
        bool updPayRes_TxnRate = false;
        int txnState = 0;
        object logObject = null;
        string logJson = string.Empty;


        if (lbl_status.Text.Equals("Success"))
            txnState = Convert.ToInt32(TxnState.ModifiedFromSucces);
        else if (lbl_status.Text.Equals("Reversed"))
            txnState = Convert.ToInt32(TxnState.ModifiedFromReversal);
        else if (lbl_status.Text.Equals("Refunded"))
            txnState = Convert.ToInt32(TxnState.ModifiedFromRefunded);
        else
            txnState = Convert.ToInt32(TxnState.ModifiedFromFailed);

        switch (ddlControl.SelectedItem.Text)
        {
            case "--SELECT--": break;
            case "Success":
                {
                    if (lbl_status.Text.Equals("Success"))
                        HiddenField1.Value = "This transaction is already in status " + lbl_status.Text;
                    else
                    {
                        updPayRes_TxnRate = sc.Do_Update_PayRes_TxnRate(lbl_gatewayTxnID.Text, DD_PymtMethod.SelectedValue, Convert.ToInt32(TxnStatus.Success), txnState);
                        if (!updPayRes_TxnRate)
                        {
                            HiddenField1.Value = sc.err_msg;
                            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                             "ManageTransactionStatus(Error) - " + logJson + sc.system_err.ToString());
                            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ManageTransactionStatus), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + sc.system_err.ToString());

                            return;
                        }

                        HiddenField1.Value = "Transaction is succesfully updated from [" + lbl_status.Text + "] to [" + ddlControl.SelectedItem.Text + "].";
                        HiddenField2.Value = AlertType.Success;
                    }

                    logObject = new
                    {
                        LoginUser = cus.UserID,
                        DomainID = PDomainID,
                        Since = PSince,
                        To = PTo,
                        GatewayTransactionID = PGatewayTxnID,
                        PaymentMethod = PPymtMethod,
                        CurrencyCode = PCurrencyCode,
                        PaymentID = PPaymentID,
                        TransactionState = txnState,
                        TransactionStatus = TxnStatus.Success
                    };
                    logJson = JsonConvert.SerializeObject(logObject);

                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                             "ManageTransactionStatus - " + logJson);
                    hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.ManageTransactionStatus, 800000, "ManageTransactionStatus.aspx");
                    hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ManageTransactionStatus), MethodBase.GetCurrentMethod().Name, logJson);
                    break;
                }

            case "Refunded":
                {
                    if (lbl_status.Text.Equals("Refunded"))
                        HiddenField1.Value = "This transaction is already in status " + lbl_status.Text;
                    else
                    {
                        updPayRes_TxnRate = sc.Do_Update_PayRes_TxnRate(lbl_gatewayTxnID.Text, DD_PymtMethod.SelectedValue, Convert.ToInt32(TxnStatus.Refunded), txnState);
                        if (!updPayRes_TxnRate)
                        {
                            HiddenField1.Value = sc.err_msg;
                            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                             "ManageTransactionStatus(Error) - " + logJson + sc.system_err.ToString());
                            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ManageTransactionStatus), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + sc.system_err.ToString());

                            return;
                        }
                        HiddenField1.Value = "Transaction is succesfully updated from [" + lbl_status.Text + "] to [" + ddlControl.SelectedItem.Text + "].";
                        HiddenField2.Value = AlertType.Success;
                    }

                    logObject = new
                    {
                        LoginUser = cus.UserID,
                        DomainID = PDomainID,
                        Since = PSince,
                        To = PTo,
                        GatewayTransactionID = PGatewayTxnID,
                        PaymentMethod = PPymtMethod,
                        CurrencyCode = PCurrencyCode,
                        PaymentID = PPaymentID,
                        TransactionState = txnState,
                        TransactionStatus = TxnStatus.Refunded
                    };
                    logJson = JsonConvert.SerializeObject(logObject);

                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                             "ManageTransactionStatus - " + logJson);
                    hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.ManageTransactionStatus, 800000, "ManageTransactionStatus.aspx");
                    hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ManageTransactionStatus), MethodBase.GetCurrentMethod().Name, logJson);
                    break;
                }
            case "Reversed":
                {
                    if (lbl_status.Text.Equals("Reversed"))
                        HiddenField1.Value = "This transaction is already in status " + lbl_status.Text;
                    else
                    {
                        updPayRes_TxnRate = sc.Do_Update_PayRes_TxnRate(lbl_gatewayTxnID.Text, DD_PymtMethod.SelectedValue, Convert.ToInt32(TxnStatus.Reversed), txnState);
                        if (!updPayRes_TxnRate)
                        {
                            HiddenField1.Value = sc.err_msg;
                            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                             "ManageTransactionStatus(Error) - " + logJson + sc.system_err.ToString());
                            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ManageTransactionStatus), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + sc.system_err.ToString());

                            return;
                        }
                        HiddenField1.Value = "Transaction is succesfully updated from [" + lbl_status.Text + "] to [" + ddlControl.SelectedItem.Text + "].";
                        HiddenField2.Value = AlertType.Success;
                    }

                    logObject = new
                    {
                        LoginUser = cus.UserID,
                        DomainID = PDomainID,
                        Since = PSince,
                        To = PTo,
                        GatewayTransactionID = PGatewayTxnID,
                        PaymentMethod = PPymtMethod,
                        CurrencyCode = PCurrencyCode,
                        PaymentID = PPaymentID,
                        TransactionState = txnState,
                        TransactionStatus = TxnStatus.Reversed
                    };
                    logJson = JsonConvert.SerializeObject(logObject);

                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                             "ManageTransactionStatus - " + logJson);
                    hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.ManageTransactionStatus, 800000, "ManageTransactionStatus.aspx");
                    hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ManageTransactionStatus), MethodBase.GetCurrentMethod().Name, logJson);
                    break;
                }
                //Not used at the moment. 
                //case "Failed":
                //    {
                //        if (lbl_status.Text.Equals("Failed"))
                //            HiddenField1.Value = "This transaction is already in status " + lbl_status.Text;
                //        else
                //        {
                //            updPayRes_TxnRate = sc.Do_Update_PayRes_TxnRate(lbl_gatewayTxnID.Text, DD_PymtMethod.SelectedValue, Convert.ToInt32(TxnStatus.Failed), Convert.ToInt32(TxnState.ModifiedFromFailed));
                //            if (!updPayRes_TxnRate)
                //            {
                //                HiddenField1.Value = sc.err_msg;
                //                break;
                //            }
                //        }
                //        break;
                //    }

        }
        GenerateList(PDomainID, PGatewayTxnID, PSince, PTo, PPaymentID, PPymtMethod, PCurrencyCode);
    }

    public Manage_Txn_Status[] GenerateList(int PDomainID, string PGatewayTxnID, DateTime PSince, DateTime PTo, string PPaymentID, string PPymtMethod, string PCurrencyCode)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        var logObject = new
        {
            LoginUser = cus.UserID,
            DomainID = PDomainID,
            Since = PSince,
            To = PTo,
            GatewayTransactionID = PGatewayTxnID,
            PaymentMethod = PPymtMethod,
            CurrencyCode = PCurrencyCode,
            PaymentID = PPaymentID
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        var getTxnPayRes = sc.Do_Get_Pymt_Response(PDomainID, PGatewayTxnID, PSince, PTo, PPaymentID, PPymtMethod, PCurrencyCode);
        if (getTxnPayRes == null)
        {
            HiddenField1.Value = sc.err_msg;
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
             "ManageTransactionStatus(Error) - " + logJson + sc.system_err.ToString());
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ManageTransactionStatus), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + sc.system_err.ToString());

            return null;
        }

        Repeater2.DataSource = getTxnPayRes;
        Repeater2.DataBind();

        objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                            "ManageTransactionStatus - " + logJson);
        hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.ManageTransactionStatus, 800000, "ManageTransactionStatus.aspx");
        hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ManageTransactionStatus), MethodBase.GetCurrentMethod().Name, logJson);
        return getTxnPayRes;
    }
}
