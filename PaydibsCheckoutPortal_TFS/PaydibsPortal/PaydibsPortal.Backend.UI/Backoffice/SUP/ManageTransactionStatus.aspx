﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ManageTransactionStatus.aspx.cs" Inherits="Backoffice_SUP_ManageTransactionStatus" %>

<!DOCTYPE html>
<%@ Register TagPrefix="uc" TagName="Left_Side_Bar" Src="~/UserControl/LeftSideBar.ascx" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Paydibs-Checkout Manage Transaction Status</title>
    <link rel="stylesheet" type="text/css" href="../../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/material-design-icons/css/material-design-iconic-font.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/select2/css/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/bootstrap-slider/css/bootstrap-slider.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/datatables/datatables.net-bs4/css/dataTables.bootstrap4.css" />
    <link rel="stylesheet" href="../../assets/css/app.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:HiddenField ID="HiddenField2" runat="server" />
        <asp:HiddenField ID="HiddenField1" runat="server" />
        <div class="be-wrapper be-collapsible-sidebar be-collapsible-sidebar-collapsed">
            <uc:Left_Side_Bar ID="Left_Side_Bar" runat="server" />
            <div class="be-content">
                <div class="main-content container-fluid">
                    <div role="alert" class="alert alert-contrast alert-dismissible" id="dl_ErrorDialog">
                        <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
                        <div class="message" id="dl_message">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 ">
                            <div class="card card-table card-border-color card-border-color-primary">
                                <div class="card-header">
                                    <span class="font-weight-bold">Manage Transaction Status</span>
                                </div>
                                <div class="row table-filters-container">
                                    <div class="col-12 col-lg-12 col-xl-12">
                                        <div class="row">
                                            <div class="col-12 col-lg-3 table-filters pb-0 pb-xl-0">
                                                <span class="table-filter-title">Merchant</span>
                                                <div class="filter-container">
                                                    <asp:DropDownList ID="DD_merchant" runat="server" CssClass="select2"></asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-12 col-lg-3 table-filters pb-0 pb-xl-0">
                                                <span class="table-filter-title">Payment Method</span>
                                                <div class="filter-container">
                                                    <asp:DropDownList ID="DD_PymtMethod" runat="server" CssClass="select2"></asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-12 col-lg-3 table-filters pb-0 pb-xl-0">
                                                <span class="table-filter-title">Date</span>
                                                <div class="filter-container">
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <asp:TextBox ID="TB_Since" runat="server" data-date-format="yyyy-mm-dd" data-min-view="2" CssClass="form-control date datetimepicker"></asp:TextBox>
                                                        </div>
                                                        <div class="col-6">
                                                            <asp:TextBox ID="TB_To" runat="server" data-date-format="yyyy-mm-dd" data-min-view="2" CssClass="form-control date datetimepicker"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <%--  <div class="col-12 col-lg-6 table-filters pb-xl-0">
                                                <span class="table-filter-title">Status</span>
                                                <div class="filter-container">
                                                    <div class="row">
                                                        <div class="col-3">
                                                            <label class="custom-control custom-checkbox">
                                                                <input type="checkbox" checked="" class="custom-control-input" id="CB_success" runat="server" /><span class="custom-control-label">Success</span>
                                                            </label>
                                                        </div>
                                                        <div class="col-3">
                                                            <label class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="CB_failed" runat="server" /><span class="custom-control-label">Failed</span>
                                                            </label>
                                                        </div>
                                                        <div class="col-3">
                                                            <label class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="CB_pending" runat="server" /><span class="custom-control-label">Pending</span>
                                                            </label>
                                                        </div>
                                                        <div class="col-3">
                                                            <label class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="CB_other" runat="server" /><span class="custom-control-label">Other</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>--%>
                                        </div>
                                    </div>

                                    <div class="col-12 col-lg-12 col-xl-12">
                                        <div class="row">
                                            <div class="col-12 col-lg-12 table-filters pb-0 pb-xl-2">
                                                <div class="row">
                                                    <div class="col-2">
                                                        <button id="btnShowPopup" data-toggle="modal" data-target="#form-bp1" type="button" class="btn btn-primary btn-lg">Advanced Search</button>
                                                    </div>
                                                    <div class="col-5">
                                                    </div>
                                                    <div class="col-2">
                                                        <asp:Button ID="btn_GetReport" runat="server" Text="Get Info" CssClass="btn btn-primary btn-xl" OnClick="btn_GetInfo_Click" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!--Form Modals-->
                                        <div id="form-bp1" tabindex="-1" role="dialog" class="modal fade colored-header colored-header-primary">
                                            <div class="modal-dialog full-width">
                                                <div class="modal-content">
                                                    <div class="modal-header modal-header-colored">
                                                        <h4 class="modal-title">Advanced Search</h4>
                                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><span class="mdi mdi-close"></span></button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row no-margin-y">
                                                            <div class="form-group col-sm-2">
                                                                <label class="col-form-label">Payment ID</label>
                                                            </div>
                                                            <div class="form-group col-sm-3">
                                                                <asp:TextBox ID="TB_PaymentID" runat="server" CssClass="form-control" placeholder="Payment ID"></asp:TextBox>
                                                            </div>
                                                            <div class="form-group col-sm-1">
                                                            </div>
                                                            <div class="form-group col-sm-2">
                                                                <label class="col-form-label">Gateway Transaction ID</label>
                                                            </div>
                                                            <div class="form-group col-sm-3">
                                                                <asp:TextBox ID="TB_GatewayTxnID" runat="server" CssClass="form-control" placeholder="Gateway Transaction ID"></asp:TextBox>
                                                            </div>
                                                            <div class="form-group col-sm-1">
                                                            </div>
                                                        </div>
                                                        <%--    <div class="row no-margin-y">
                                                            <div class="form-group col-sm-2">
                                                                <label class="col-form-label">Customer Email</label>
                                                            </div>
                                                            <div class="form-group col-sm-3">
                                                                <asp:TextBox ID="TB_CustEmail" runat="server" CssClass="form-control" placeholder="Customer Email"></asp:TextBox>
                                                            </div>
                                                            <div class="form-group col-sm-1">
                                                            </div>
                                                            <div class="form-group col-sm-2">
                                                                <label class="col-form-label">Order Number</label>
                                                            </div>
                                                            <div class="form-group col-sm-3">
                                                                <asp:TextBox ID="TB_OrderId" runat="server" CssClass="form-control" placeholder="Order Number"></asp:TextBox>
                                                            </div>
                                                            <div class="form-group col-sm-1">
                                                            </div>
                                                        </div>--%>
                                                        <div class="row no-margin-y">
                                                            <div class="form-group col-sm-2">
                                                                <label class="col-form-label">Currency</label>
                                                            </div>
                                                            <div class="form-group col-sm-3">
                                                                <asp:DropDownList ID="DD_Currency" runat="server" CssClass="select2"></asp:DropDownList>
                                                            </div>
                                                            <div class="form-group col-sm-1">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <asp:Button ID="btn_OK" runat="server" Text="OK" CssClass="btn btn-primary btn-xl" OnClick="btn_GetInfo_Click" />
                                                        <asp:Button ID="btn_Cancel" runat="server" Text="Cancel" CssClass="btn btn-primary btn-xl" OnClick="btn_Cancel_Click" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Form Modals-->
                                    </div>
                                </div>
                                <!-- Content Goes Here
================================================== -->
                                <div class="card-body">
                                    <div class="table-responsive noSwipe">
                                        <table id="table4" class="table table-striped table-hover table-fw-widget">
                                            <thead>
                                                <tr id="TB_column" runat="server">
                                                    <th>Details</th>
                                                    <th>Transaction Status</th>
                                                    <th>Change Status</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="Repeater2" runat="server" OnItemDataBound="RP_Channel_DataBound">
                                                    <ItemTemplate>
                                                        <tr class="odd gradeX">
                                                            <td><b>Transaction Date      </b>: &nbsp;<%# Eval("DateCreated","{0:dd/MM/yyyy HH:mm:ss}") %><br />
                                                                <b>Merchant              </b>: &nbsp;<%# Eval("MerchantID") %><br />
                                                                <b>Payment ID            </b>: &nbsp;<%# Eval("PaymentID") %><br />
                                                                <b>Gateway Transaction ID</b> : 
                                                                <asp:Label ID="lbl_gatewayTxnID" runat="server" Text='<%#Eval("GatewayTxnID") %>'></asp:Label><br />
                                                                <b>Currency Code         </b>: &nbsp;<%# Eval("CurrencyCode") %><br />
                                                                <b>Transaction Amount    </b>: &nbsp;<%# Convert.ToDecimal(Eval("TxnAmt")).ToString("N") %><br />
                                                                <b>Response Message      </b>: &nbsp;<%# Eval("RespMesg") %></td>
                                                            <td>
                                                                <asp:Label ID="lbl_status" runat="server" Text=' <%# Eval("Status") %>' />
                                                                <%--<asp:HiddenField ID="hid_payRes" runat="server" Value='<%# Eval("TxnState") +","+ Eval("TxnStatus") +","+ Eval("MerchantTxnStatus")%>'/>--%>

                                                            </td>
                                                            <td class="text-centre">
                                                                <%-- <asp:Button ID="btn_ChgStatus" runat="server" CommandArgument='<%#Eval("Status")+","+ Eval("GatewayTxnID") %>' OnClick="btn_ChgStatus_Click" Text="Change Status" CssClass="btn btn btn-secondary"></asp:Button>
                                                                <button type="button" data-toggle="dropdown" id="btn_Open" runat="server" class="btn btn-secondary dropdown-toggle">Open <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                                                                <div role="menu" class="dropdown-menu">
                                                                    <asp:LinkButton ID="lb_Success" runat="server" CssClass="dropdown-item" CommandArgument='<%# Eval("Status") %>' CommandName="Success">Success</asp:LinkButton>
                                                                    <asp:LinkButton ID="lb_Fail" runat="server" CssClass="dropdown-item" CommandArgument='<%# Eval("Status") %>' CommandName="Fail">Fail</asp:LinkButton>
                                                                    <asp:LinkButton ID="lb_Refund" runat="server" CssClass="dropdown-item" CommandArgument='<%# Eval("Status") %>' CommandName="Refund">Refund</asp:LinkButton>
                                                                    <asp:LinkButton ID="lb_Void" runat="server" CssClass="dropdown-item" CommandArgument='<%# Eval("Status") %>' CommandName="Void">Void</asp:LinkButton>
                                                                </div>--%>

                                                                <asp:DropDownList ID="ddl_ChgStatus" runat="server" CssClass="select2" OnSelectedIndexChanged="ddl_ChgStatus_OnSelectedIndex" AutoPostBack="true"></asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center">
                Copyright © 2018 Paydibs Sdn. Bhd. All Rights Reserved.
            </div>
        </div>
    </form>
    <script src="../../assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/bootstrap/dist/js/bootstrap.bundle.min.js" type="text/javascript"></script>
    <script src="../../assets/js/app.js" type="text/javascript"></script>
    <script src="../../assets/lib/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/select2/js/select2.full.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/bootstrap-slider/bootstrap-slider.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="../../assets/js/app-table-filters.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net/js/jquery.dataTables.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-bs4/js/dataTables.bootstrap4.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/dataTables.buttons.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/buttons.html5.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/buttons.flash.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/buttons.print.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons/js/buttons.colVis.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/datatables/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js" type="text/javascript"></script>
    <script src="../../assets/js/app-tables-datatables.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            //initialize the javascript
            App.init();
            App.tableFilters();
            App.dataTables();
        });

        $(function () {
            $('#dl_ErrorDialog').hide();
            var msg = $('#<%= HiddenField1.ClientID %>').val();
            var classType = $('#<%= HiddenField2.ClientID %>').val();
            var type = "alert-danger";
            if (msg != "") {
                if (classType != "") type = classType;
                $('#dl_ErrorDialog').addClass(type);
                document.getElementById('dl_message').innerText = msg;
                $('#dl_ErrorDialog').show();
            }
        });
    </script>
</body>
</html>
