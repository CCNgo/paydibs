﻿using Kenji.Apps;
using LoggerII;
using Newtonsoft.Json;
using PaydibsPortal.BLL;
using PaydibsPortal.DAL;
using System;
using System.Reflection;
using System.Web.UI.WebControls;

public partial class Backoffice_SUP_ViewLateResponse : System.Web.UI.Page
{
    HomeController hc = null;
    SUPController sc = null;
    CurrentUserStat cus = null;
    LinqTool linq = new LinqTool();
    MMGMTController mc = new MMGMTController();

    protected void Page_Load(object sender, EventArgs e)
    {
        HiddenField1.Value = "";
        HiddenField2.Value = "";

        if (Session[SessionType.CurrentUser] == null)
        {
            Response.Redirect("../../Default.aspx");
            return;
        }

        cus = (CurrentUserStat)Session[SessionType.CurrentUser];
        hc = new HomeController(cus.DomainID);
        sc = new SUPController();

        var permission = hc.LoadUserPermission(cus.UserID, Convert.ToInt16(OpsIDEnum.ViewLateResponseTransactions));
        if (permission[0].IsUserAllowed == "0")
        {
            Response.Redirect("../../Backoffice/Dashboard.aspx");
            return;
        }

        if (!IsPostBack)
        {
            var merchantList = hc.Do_GetMerchantList(1, 0);
            DD_merchant.DataSource = merchantList;
            DD_merchant.DataTextField = "DomainDesc";
            DD_merchant.DataValueField = "DomainID";
            DD_merchant.DataBind();

            DateTime sSince = Convert.ToDateTime(TB_Since.Text.Trim() + " 00:00:00");
            DateTime sTo = Convert.ToDateTime(TB_To.Text.Trim() + " 23:59:59");
            TB_Since.Text = sSince.ToString("yyyy-MM-dd");
            TB_To.Text = sTo.ToString("yyyy-MM-dd");

            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ViewLateResponseTransactions), MethodBase.GetCurrentMethod().Name, "Page_Load Method");
        }
    }

    protected void btn_GetReport_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();
        DateTime sSince = Convert.ToDateTime(TB_Since.Text.Trim() + " 00:00:00");
        DateTime sTo = Convert.ToDateTime(TB_To.Text.Trim() + " 23:59:59");

        var logObject = new
        {
            LoginUser = cus.UserID,
            Since = sSince,
            To = sTo,
            GatewayTxnID = TB_GatewayTxnID.Text.Trim(),
        };
        var logJson = JsonConvert.SerializeObject(logObject);
        try
        {
            objLoggerII = logger.InitLogger();
            var getLateResponse = sc.Do_Get_LateResponse_Transaction(TB_GatewayTxnID.Text.Trim(), Convert.ToInt32(DD_merchant.SelectedValue), sSince, sTo);
            if (getLateResponse == null)
            {
                HiddenField1.Value = sc.err_msg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                       "ViewLateResponse(Error) - " + logJson + sc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ViewLateResponseTransactions), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + sc.system_err.ToString());
                return;
            }

            Repeater2.DataSource = getLateResponse;
            Repeater2.DataBind();

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "ViewLateResponse - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.ViewLateResponseTransactions, 800000, "ViewLateResponse.aspx");
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ViewLateResponseTransactions), MethodBase.GetCurrentMethod().Name, logJson);
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                         "ViewLateResponse(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.ViewLateResponseTransactions, 800001, "ViewLateResponse.aspx");
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ViewLateResponseTransactions), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
        }
        finally
        {
            objLoggerII.Dispose();
        }

    }

    protected void btn_Cancel_Click(object sender, EventArgs e)
    {
        TB_GatewayTxnID.Text = string.Empty;
    }

    protected void Row_Click(object sender, EventArgs e)
    {
        DateTime sSince = Convert.ToDateTime(TB_Since.Text.Trim() + " 00:00:00");
        DateTime sTo = Convert.ToDateTime(TB_To.Text.Trim() + " 23:59:59");
        LinkButton btn = (LinkButton)(sender);
        string gatewayTxnID = btn.CommandArgument;
        Response.Redirect("../SUP/ManageTransactionStatus.aspx?gatewayTxnID=" + gatewayTxnID + "&since=" + sSince + "&to=" + sTo);
    }


    //ADD START DANIEL 20190311 [If ManageTransactionStatus is not allowed, disable link]
    protected void Repeater_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            LinkButton lb_gatewayTxnID = (LinkButton)e.Item.FindControl("lb_gatewayTxnID");
            var permission = hc.LoadUserPermission(cus.UserID, Convert.ToInt16(OpsIDEnum.ManageTransactionStatus));
            if (permission[0].IsUserAllowed == "0")
                lb_gatewayTxnID.Enabled = false;

        }
    }
    //ADD E N D DANIEL 20190311 [If ManageTransactionStatus is not allowed, disable link]
}