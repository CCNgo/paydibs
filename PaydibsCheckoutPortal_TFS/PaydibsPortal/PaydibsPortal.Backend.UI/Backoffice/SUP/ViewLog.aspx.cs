﻿using LoggerII;
using Newtonsoft.Json;
using PaydibsPortal.BLL;
using PaydibsPortal.DAL;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Web.Configuration;
using System.Web.UI.WebControls;

public partial class Backoffice_SUP_ViewLog : System.Web.UI.Page
{
    HomeController hc = null;
    CurrentUserStat cus = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        HiddenField1.Value = "";
        HiddenField2.Value = "";

        if (Session[SessionType.CurrentUser] == null)
        {
            Response.Redirect("../../Default.aspx");
            return;
        }

        cus = (CurrentUserStat)Session[SessionType.CurrentUser];
        hc = new HomeController(cus.DomainID);

        // ADD START SUKI 20190104 [Check permission on every page load]
        var permission = hc.LoadUserPermission(cus.UserID, Convert.ToInt16(OpsIDEnum.ViewLog));
        if (permission[0].IsUserAllowed == "0")
        {
            Response.Redirect("../../Backoffice/Dashboard.aspx");
            return;
        }
        // ADD E N D SUKI 20190104 [Check permission on every page load]

        if (!IsPostBack)
        {
            DateTime sSince = Convert.ToDateTime(TB_Since.Text.Trim() + " 00:00:00");
            DateTime sTo = Convert.ToDateTime(TB_To.Text.Trim() + " 23:59:59");
            TB_Since.Text = sSince.ToString("yyyy-MM-dd");
            TB_To.Text = sTo.ToString("yyyy-MM-dd");

            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ViewLog), MethodBase.GetCurrentMethod().Name, "Page_Load Method");
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
    }

    protected void btn_Search_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        var logObject = new
        {
            LoginUser = cus.UserID,
            Since = Convert.ToDateTime(TB_Since.Text.Trim() + " 00:00:00"),
            To = Convert.ToDateTime(TB_To.Text.Trim() + " 23:59:59")
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            getFile();

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "ViewLog - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.ViewLog, 800000, "ViewLog.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ViewLog), MethodBase.GetCurrentMethod().Name, logJson);
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "ViewLog(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.ViewLog, 800001, "ViewLog.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ViewLog), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }

    void getFile()
    {
        try
        {
            var supportLogList = new List<SupportLog>();

            string PGAdminLogPath = WebConfigurationManager.AppSettings["LogPath"];
            string PGLogPath = WebConfigurationManager.AppSettings["PGLogPath"];
            string SchedulerLogPath = WebConfigurationManager.AppSettings["SchedulerLogPath"];
            string TNGSchedulerLogPath = WebConfigurationManager.AppSettings["TNGSchedulerLogPath"];
            string PaymentGatewayAPILogPath = WebConfigurationManager.AppSettings["PaymentGatewayAPILogPath"];

            DateTime sSince = Convert.ToDateTime(TB_Since.Text.Trim() + " 00:00:00");
            DateTime sTo = Convert.ToDateTime(TB_To.Text.Trim() + " 23:59:59");

            foreach (DateTime day in EachDay(sSince, sTo))
            {
                // Get PGAdmin Log
                string PGAdminPath = PGAdminLogPath + "_" + day.ToString("yyyyMMdd") + ".log";
                if (File.Exists(PGAdminPath))
                {
                    SupportLog supportLog = new SupportLog();
                    supportLog.FileName = PGAdminPath;
                    //ADD START KENT LOONG 20190212 [Show last modified date]
                    supportLog.FileTime = File.GetLastWriteTime(PGAdminPath).ToString("dd/MM/yyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                    //ADD E N D KENT LOONG 20190212 [Show last modified date]
                    supportLogList.Add(supportLog);
                }

                // Get PG Log
                string[] files = Directory.GetFiles(PGLogPath, "PPG*" + day.ToString("yyyyMMdd") + ".log", SearchOption.TopDirectoryOnly);
                foreach (var file in files)
                {
                    if (File.Exists(file))
                    {
                        SupportLog supportLog = new SupportLog();
                        supportLog.FileName = file;
                        //ADD START KENT LOONG 20190212 [Show last modified date]
                        supportLog.FileTime = File.GetLastWriteTime(file).ToString("dd/MM/yyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                        //ADD E N D KENT LOONG 20190212 [Show last modified date
                        supportLogList.Add(supportLog);
                    }
                }

                // Get Scheduler Log
                string[] SchedulerFiles = Directory.GetFiles(SchedulerLogPath, "Scheduler" + "_"  + day.ToString("yyyyMMdd") + ".log", SearchOption.TopDirectoryOnly);
                foreach (var file in SchedulerFiles)
                {
                    if (File.Exists(file))
                    {
                        SupportLog supportLog = new SupportLog();
                        supportLog.FileName = file;
                        supportLog.FileTime = File.GetLastWriteTime(file).ToString("dd/MM/yyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                        supportLogList.Add(supportLog);
                    }
                }

                // Get TNG Scheduler Log
                string[] TNGSchedulerFiles = Directory.GetFiles(TNGSchedulerLogPath, "SchedulerTNG" + "_" + day.ToString("yyyyMMdd") + ".log", SearchOption.TopDirectoryOnly);
                foreach (var file in TNGSchedulerFiles)
                {
                    if (File.Exists(file))
                    {
                        SupportLog supportLog = new SupportLog();
                        supportLog.FileName = file;
                        supportLog.FileTime = File.GetLastWriteTime(file).ToString("dd/MM/yyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                        supportLogList.Add(supportLog);
                    }
                }

                // Get PaymentGatewayAPI Log
                string[] PaymentGatewayAPIFiles = Directory.GetFiles(PaymentGatewayAPILogPath, "PaymentGatewayAPI" + "_" + day.ToString("yyyyMMdd") + ".log", SearchOption.TopDirectoryOnly);
                foreach (var file in PaymentGatewayAPIFiles)
                {
                    if (File.Exists(file))
                    {
                        SupportLog supportLog = new SupportLog();
                        supportLog.FileName = file;
                        supportLog.FileTime = File.GetLastWriteTime(file).ToString("dd/MM/yyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                        supportLogList.Add(supportLog);
                    }
                }
            }
            Repeater1.DataSource = supportLogList;
            Repeater1.DataBind();
        }
        catch (Exception ex)
        {
            HiddenField1.Value = ex.Message.ToString();
        }
    }

    public IEnumerable<DateTime> EachDay(DateTime from, DateTime thru)
    {
        for (var day = from.Date; day.Date <= thru.Date; day = day.AddDays(1))
            yield return day;
    }

    protected void lbtn_Download_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        var thisButton = (LinkButton)sender;
        var arg = thisButton.CommandArgument;
        var cmd = thisButton.CommandName;

        var logObject = new
        {
            LoginUser = cus.UserID,
            FileName = arg,
            DownloadUser = cus.UserID.ToString()
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            bool rst = false;
            rst = DownLoad(arg.ToString());

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "ViewLog - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.ViewLog, 800000, "ViewLog.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ViewLog), MethodBase.GetCurrentMethod().Name, logJson);
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]

            if (!rst)
            {
                HiddenField1.Value = hc.err_msg;
                //ADD START DANIEl 20190107 [Insert into AuditTrail]
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ViewLog), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + hc.err_msg.ToString());
                //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
                return;
            }
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "ViewLog(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.ViewLog, 800001, "ViewLog.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.ViewLog), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }

    public bool DownLoad(string FName)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        try
        {
            objLoggerII = logger.InitLogger();

            //string path = FName;
            //FileInfo file = new FileInfo(path);
            //if (file.Exists)
            //{
            //    byte[] Content = File.ReadAllBytes(path);
            //    Response.ContentType = "text/csv";
            //    Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
            //    Response.BufferOutput = true;
            //    //Response.OutputStream.Write(Content, 0, Content.Length);
            //    Response.WriteFile(path);
            //    Response.End();
            //}
            string path = FName;
            FileInfo filogfile = new FileInfo(path);
            using (FileStream stream = File.Open(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    while (!reader.EndOfStream)
                    {
                        var bytes = GetBytes(reader.ReadToEnd());
                        Response.OutputStream.Write(bytes, 0, bytes.Length);
                        Response.ContentType = "text/plain";
                        Response.AddHeader("Content-Disposition", "attachment;filename=" + filogfile.Name);
                        Response.End();
                        Response.Flush();
                        Response.Close();
                    }
                }
            }

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "ViewLog - Download " + FName + " success");
            return true;
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "ViewLog - Download " + FName + " failed" + ex.ToString());
            return false;
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }

    static byte[] GetBytes(string str)
    {
        byte[] bytes = new byte[str.Length * sizeof(char)];
        System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
        return bytes;
    }
}

