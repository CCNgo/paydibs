﻿using LoggerII;
using Newtonsoft.Json;
using PaydibsPortal.BLL;
using PaydibsPortal.DAL;
using System;
using System.Reflection;

public partial class Backoffice_AccountDetail : System.Web.UI.Page
{
    HomeController hc = null;
    CurrentUserStat cus = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        HiddenField1.Value = "";
        HiddenField2.Value = "";

        if (Session[SessionType.CurrentUser] == null)
        {
            Response.Redirect("../../Default.aspx");
            return;
        }

        cus = (CurrentUserStat)Session[SessionType.CurrentUser];
        hc = new HomeController(cus.DomainID);

        LB_UserGroup.Text = hc.Do_GetUserGroup(cus.DomainID).GroupDesc;

        if (!IsPostBack)
        {
            LB_UserID.Text = cus.UserID;

            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AccountDetail), MethodBase.GetCurrentMethod().Name, "Page_Load Method");
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
    }

    protected void lbtn_ResetPassword_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        var logObject = new
        {
            PUserID = cus.UserID
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            bool rr = hc.Do_User_Request_Reset_Pwd(cus.UserID);
            if (!rr)
            {
                HiddenField1.Value = hc.err_msg;
                if (hc.system_err == string.Empty)
                {
                    //CHG START DANIEl 20190107 [Insert into AuditTrail]
                    //objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                    //       "AccountDetails(Error) - " + logJson + hc.system_err.ToString());
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "AccountDetails(Error) - " + logJson + hc.system_err.ToString());

                    hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AccountDetail), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + hc.system_err.ToString());
                    //CHG E N D DANIEl 20190107 [Insert into AuditTrail]
                }
                return;
            }

            HiddenField1.Value = "You have successfully request to reset your password! We will send you an email soon.";
            HiddenField2.Value = AlertType.Success;

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "AccountDetails - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.AccountDetail, 800000, "AccountDetail.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AccountDetail), MethodBase.GetCurrentMethod().Name, logJson);
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                           "Login(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.AccountDetail, 800001, "AccountDetail.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.AccountDetail), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }
}