﻿using GemBox.Spreadsheet;
using Kenji.Apps;
using LoggerII;
using Newtonsoft.Json;
using PaydibsPortal.BLL;
using PaydibsPortal.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Web.Script.Serialization;

public partial class Backoffice_FIN_FinanceReport : System.Web.UI.Page
{
    HomeController hc = null;
    FINController fc = null;
    CurrentUserStat cus = null;
    LinqTool linq = new LinqTool();

    protected void Page_Load(object sender, EventArgs e)
    {
        //ADD START DANIEL 20190107 [Initialize hidden fields]
        HiddenField1.Value = "";
        HiddenField2.Value = "";
        //ADD START DANIEL 20190107 [Initialize hidden fields]
        if (Session[SessionType.CurrentUser] == null)
        {
            Response.Redirect("../Dashboard.aspx");
            return;
        }

        cus = (CurrentUserStat)Session[SessionType.CurrentUser];
        hc = new HomeController(cus.DomainID);
        fc = new FINController();

        // ADD START SUKI 20190104 [Check permission on every page load]
        var permission = hc.LoadUserPermission(cus.UserID, Convert.ToInt16(OpsIDEnum.FinanceReport));
        if (permission[0].IsUserAllowed == "0")
        {
            Response.Redirect("../../Backoffice/Dashboard.aspx");
            return;
        }
        // ADD E N D SUKI 20190104 [Check permission on every page load]

        if (!IsPostBack)
        {
            var merchant_L = hc.Do_GetMerchantList(1, 0);
            DD_merchant.DataSource = merchant_L;
            DD_merchant.DataTextField = "DomainDesc";
            DD_merchant.DataValueField = "DomainID";
            DD_merchant.DataBind();

            DateTime sSince = Convert.ToDateTime(TB_Since.Text.Trim() + " 00:00:00");
            DateTime sTo = Convert.ToDateTime(TB_To.Text.Trim() + " 23:59:59");
            TB_Since.Text = sSince.ToString("yyyy-MM-dd");
            TB_To.Text = sTo.ToString("yyyy-MM-dd");

            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.FinanceReport), MethodBase.GetCurrentMethod().Name, "Page_Load Method");
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
    }

    protected void btn_GetReport_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        int sDomainID = Convert.ToInt32(DD_merchant.SelectedValue);
        DateTime sSince = Convert.ToDateTime(TB_Since.Text.Trim() + " 00:00:00");
        DateTime sTo = Convert.ToDateTime(TB_To.Text.Trim() + " 00:00:00");

        var logObject = new
        {
            LoginUser = cus.UserID,
            DomainID = sDomainID,
            Since = sSince,
            To = sTo
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            string err_msg = GenerateFinanceReport(sSince, sTo, sDomainID);
            if (!(string.IsNullOrEmpty(err_msg)))
            {
                //CHG START DANIEL 20190107 [Assign FINController error message to HiddenField1 and change err_msg to fc.system_err]
                //objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                //             "FinanceReport(Error) - " + logJson + err_msg.ToString());

                HiddenField1.Value = err_msg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                       "FinanceReport(Error) - " + logJson + fc.system_err.ToString());
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.FinanceReport), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + fc.system_err.ToString());
                return;
                //CHG E N D DANIEL 20190107 [Assign FINController error message to HiddenField1 and change err_msg to fc.system_err]
            }

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "FinanceReport - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.FinanceReport, 800000, "FinanceReport.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.FinanceReport), MethodBase.GetCurrentMethod().Name, logJson);
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "FinanceReport(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 0, 100002, OpsIDEnum.FinanceReport, 800001, "FinanceReport.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.FinanceReport), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }

    protected void btn_Download_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        int sDomainID = Convert.ToInt32(DD_merchant.SelectedValue);
        string sDomainName = DD_merchant.SelectedItem.Text;
        DateTime sSince = Convert.ToDateTime(TB_Since.Text.Trim() + " 00:00:00");
        DateTime sTo = Convert.ToDateTime(TB_To.Text.Trim() + " 00:00:00");

        var logObject = new
        {
            LoginUser = cus.UserID,
            DomainID = sDomainID,
            DomainName = sDomainName,
            Since = sSince,
            To = sTo
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            Finance_Report[] data = fc.Do_FinanceReport(sSince, sTo, sDomainID);
            //ADD START DANIEL 20190107 [Display error message and store into log and AuditTrail if data is null]
            if (data == null)
            {
                HiddenField1.Value = fc.err_msg;
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                       "FinanceReport(Error) - " + logJson + fc.system_err.ToString());
                //ADD START START DANIEl 20190107 [Insert into AuditTrail]
                hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.FinanceReport), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + fc.system_err.ToString());
                //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
                return;
            }

            foreach (var d in data)
            {
                if (!string.IsNullOrEmpty(d.CardPAN))
                {
                    d.CardPAN = DecryptAES256(d.CardPAN);
                    d.HostDesc = GetCCChannelName(d.CardPAN, d.HostDesc);
                }
            }

            //ADD E N D DANIEL 20190107 [Display error message and store into log and AuditTrail if data is null]

            ExcelFile ef = new ExcelFile();
            string PhysicalPath = Server.MapPath("Report/Report.xls");
            string SaveAsFileName = "FinanceReport_Merchant_" + sDomainName + "_" + sSince.ToString("dd-MM-yyyy") + "-" + sTo.ToString("dd-MM-yyyy") + ".xls";
            string SavePhysicalPath = Server.MapPath("Report/" + SaveAsFileName);
            ef.LoadXls(PhysicalPath);

            DataTable dt = linq.LINQToDataTable(data);
            ExcelWorksheet ws = ef.Worksheets[0];
            ws.InsertDataTable(dt, "A1", true);

            ef.SaveXls(SavePhysicalPath);

            System.IO.FileInfo file = new System.IO.FileInfo(Server.MapPath("Report/" + SaveAsFileName));
            if (file.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                Response.AddHeader("Content-Length", file.Length.ToString());
                Response.ContentType = "application/vnd.ms-excel";
                Response.WriteFile(file.FullName);

                Response.Flush();
                file.Delete();
                Response.End();
            }

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "FinanceReportDownload - " + logJson);
            hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.FinanceReport, 800000, "FinanceReport.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.FinanceReport), MethodBase.GetCurrentMethod().Name, logJson);
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "FinanceReportDownload(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog(cus.UserID, 1, 100002, OpsIDEnum.FinanceReport, 800001, "FinanceReport.aspx");
            //ADD START DANIEl 20190107 [Insert into AuditTrail]
            hc.Do_Add_AuditTrail(cus.UserID, Convert.ToInt32(OpsIDEnum.FinanceReport), MethodBase.GetCurrentMethod().Name, logJson + " Error: " + ex.ToString());
            //ADD E N D DANIEl 20190107 [Insert into AuditTrail]
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }

    string GenerateFinanceReport(DateTime PSince, DateTime PTo, int PDomainID)
    {
        try
        {
            Finance_Report[] data = fc.Do_FinanceReport(PSince, PTo, PDomainID);

            Repeater2.DataSource = data;
            Repeater2.DataBind();

            //ADD START DANIEL 20190107 [Retrieve FINController error message]
            if (data == null)
                return fc.err_msg;
            //ADD E N D DANIEL 20190107 [Retrieve FINController error message]

            return string.Empty;
        }
        catch (Exception ex)
        {
            return ex.Message.ToString();
        }
    }

    private static readonly Encoding encoding = Encoding.UTF8;
    public string DecryptAES256(string CCNumber)
    {
        if (CCNumber.Length < 20 || string.IsNullOrEmpty(CCNumber))
        {
            return CCNumber;
        }
        try
        {
            RijndaelManaged aes = new RijndaelManaged();
            aes.KeySize = 256;
            aes.BlockSize = 128;
            aes.Padding = PaddingMode.PKCS7;
            aes.Mode = CipherMode.CBC;
            aes.Key = encoding.GetBytes(System.Configuration.ConfigurationManager.AppSettings["AES256Key"]);

            //Code from Ngo
            //cus = (CurrentUserStat)Session[SessionType.CurrentUser];
            //hc = new HomeController(cus.DomainID);
            //CCNumber = DecryptString(CCNumber, System.Configuration.ConfigurationManager.AppSettings["AES256Key"]);
            //string newCCNumber = hc.VGSOutboundConvert(CCNumber);


            // Base 64 decode
            byte[] base64Decoded = Convert.FromBase64String(CCNumber);
            string base64DecodedStr = encoding.GetString(base64Decoded);

            // JSON Decode base64Str
            JavaScriptSerializer ser = new JavaScriptSerializer();
            var payload = ser.Deserialize<Dictionary<string, string>>(base64DecodedStr);

            aes.IV = Convert.FromBase64String(payload["iv"]);

            ICryptoTransform AESDecrypt = aes.CreateDecryptor(aes.Key, aes.IV);
            byte[] buffer = Convert.FromBase64String(payload["value"]);

            string DecryptCCNumber = encoding.GetString(AESDecrypt.TransformFinalBlock(buffer, 0, buffer.Length));
            string MaskedCCNumber = string.Empty;
            if (!string.IsNullOrEmpty(DecryptCCNumber))
            {
                MaskedCCNumber = DecryptCCNumber.Substring(0, 6) + "".PadRight(DecryptCCNumber.Length - 6 - 4, 'X') + DecryptCCNumber.Substring(DecryptCCNumber.Length - 4, 4);
            }
            return MaskedCCNumber;
        }
        catch (Exception e)
        {
            throw new Exception("Error decrypting: " + e.Message);
        }
    }
    public string GetCCChannelName(string CCNumber, string channelName)
    {
        if (channelName != "VISA" && channelName != "MASTER" && channelName != "JCB" && channelName != "AMEX" && channelName != "Diners"
            && string.IsNullOrEmpty(CCNumber) && string.IsNullOrWhiteSpace(CCNumber))
        {
            return channelName;
        }

        CCNumber = DecryptAES256(CCNumber);
        string ChannelName = string.Empty;

        if (string.IsNullOrEmpty(CCNumber) || string.IsNullOrWhiteSpace(CCNumber)) //Due to empty after Decryption
        {
            return channelName;
        }

        if (CCNumber.Substring(0, 1) == "4")
        {
            ChannelName = "VISA";
        }
        else if (CCNumber.Substring(0, 1) == "5" || CCNumber.Substring(0, 1) == "2")
        {
            ChannelName = "MASTER";
        }
        else if (CCNumber.Substring(0, 2) == "35")
        {
            ChannelName = "JCB";
        }
        else if (CCNumber.Substring(0, 1) == "34" || CCNumber.Substring(0, 2) == "37")
        {
            ChannelName = "AMEX";
        }
        else if (CCNumber.Substring(0, 2) == "36" || CCNumber.Substring(0, 2) == "38")
        {
            ChannelName = "Diners";
        }
        else
        {
            ChannelName = "UnknownCC";
        }

        return ChannelName;
        //--SET @sz_SelectCC = @sz_SelectCC + '(CASE WHEN LEFT(txn.CardPAN, 1) = 4 THEN ''VISA'' '
        //--SET @sz_SelectCC = @sz_SelectCC + 'WHEN (LEFT(txn.CardPAN, 1) = 5 OR LEFT(txn.CardPAN, 1) = 2) THEN ''MASTER'' '
        //--SET @sz_SelectCC = @sz_SelectCC + 'WHEN (LEFT(txn.CardPAN, 2) = 35) THEN ''JCB'' '
        //--SET @sz_SelectCC = @sz_SelectCC + 'WHEN (LEFT(txn.CardPAN, 2) = 34 OR LEFT(txn.CardPAN, 2) = 37) THEN ''AMEX''  '
        //--SET @sz_SelectCC = @sz_SelectCC + 'WHEN (LEFT(txn.CardPAN, 2) = 36 OR LEFT(txn.CardPAN, 2) = 38) THEN ''Diners'' '
    }

    public string DecryptString(string text, string password)
    {
        byte[] baPwd = Encoding.UTF8.GetBytes(password);

        // Hash the password with SHA256
        byte[] baPwdHash = SHA256Managed.Create().ComputeHash(baPwd);

        byte[] baText = Convert.FromBase64String(text);

        byte[] baDecrypted = AES_Decrypt(baText, baPwdHash);

        // Remove salt
        int saltLength = GetSaltLength();
        byte[] baResult = new byte[baDecrypted.Length - saltLength];
        for (int i = 0; i < baResult.Length; i++)
            baResult[i] = baDecrypted[i + saltLength];

        string result = Encoding.UTF8.GetString(baResult);
        return result;
    }

    public byte[] AES_Decrypt(byte[] bytesToBeDecrypted, byte[] passwordBytes)
    {
        byte[] decryptedBytes = null;

        // Set your salt here, change it to meet your flavor:
        // The salt bytes must be at least 8 bytes.
        byte[] saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

        using (MemoryStream ms = new MemoryStream())
        {
            using (RijndaelManaged AES = new RijndaelManaged())
            {
                AES.KeySize = 256;
                AES.BlockSize = 128;

                var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);
                AES.Key = key.GetBytes(AES.KeySize / 8);
                AES.IV = key.GetBytes(AES.BlockSize / 8);

                AES.Mode = CipherMode.CBC;

                using (var cs = new CryptoStream(ms, AES.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(bytesToBeDecrypted, 0, bytesToBeDecrypted.Length);
                    cs.Close();
                }
                decryptedBytes = ms.ToArray();
            }
        }

        return decryptedBytes;
    }

    public static int GetSaltLength()
    {
        return 8;
    }
}