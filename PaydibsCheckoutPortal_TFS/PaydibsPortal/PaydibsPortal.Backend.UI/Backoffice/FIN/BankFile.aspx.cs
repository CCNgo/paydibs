﻿using PaydibsPortal.BLL;
using System;
using System.Configuration;

public partial class Backoffice_FIN_BankFile : System.Web.UI.Page
{
    HomeController hc = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session[SessionType.CurrentUser] == null)
        {
            Response.Redirect("../../Default.aspx");
            return;
        }

        CurrentUserStat cus = (CurrentUserStat)Session[SessionType.CurrentUser];
        hc = new HomeController(cus.DomainID);
        var userID = hc.tempLoginEncrypt(cus.UserID);
        Response.Redirect(ConfigurationManager.AppSettings["MVCTempLogin"]+"?userID=" + userID + "&strController=FIN&strAction=BankFile");
    }
}