﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Logout.aspx.cs" Inherits="Logout" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/img/logo-fav.png">
    <title>Paydibs-Checkout Logout</title>
    <link rel="stylesheet" type="text/css" href="assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css" />
    <link rel="stylesheet" type="text/css" href="assets/lib/material-design-icons/css/material-design-iconic-font.min.css" />
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="assets/css/app.css" type="text/css" />
</head>
<body class="be-splash-screen">
    <form id="form1" runat="server">
        <div class="be-wrapper be-error be-error-404">
            <div class="be-content">
                <div class="main-content container-fluid">
                    <div class="error-container">
                        <%-- <div class="error-number">404</div>--%>
                        <div class="error-description">You have logout successfully!</div>
                        <div class="error-goback-text">Would you like to clear cookies & cache?</div>
                        <div class="error-goback-button">
                            <asp:Button ID="Button1" runat="server" Text="Clean!" CssClass="btn btn-primary btn-xl" OnClick="Button1_Click" />
                        </div>
                        <div class="footer">&copy; <%=DateTime.Now.Year %> Paydibs Sdn Bhd</div>
                    </div>
                </div>
            </div>
            <div class="text-center" id="footer">
                Copyright © 2019 Paydibs Sdn. Bhd. All Rights Reserved.
            </div>
        </div>
        <script src="../../assets/lib/customize/General.js"></script>
        <script src="assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
        <script src="assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
        <script src="assets/lib/bootstrap/dist/js/bootstrap.bundle.min.js" type="text/javascript"></script>
        <script src="assets/js/app.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                //initialize the javascript
                App.init();
            });

    </script>
    </form>
</body>
</html>
