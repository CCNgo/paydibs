﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <link rel="shortcut icon" href="assets/img/logo-fav.png"/>
    <title>Paydibs-Checkout Login</title>
    <link rel="stylesheet" type="text/css" href="assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css" />
    <link rel="stylesheet" type="text/css" href="assets/lib/material-design-icons/css/material-design-iconic-font.min.css" />
    <link rel="stylesheet" href="assets/css/app.css" type="text/css" />
</head>
<body class="be-splash-screen">
    <form id="Form1" runat="server">
        <asp:HiddenField ID="HiddenField2" runat="server" />
        <asp:HiddenField ID="HiddenField1" runat="server" />
        <div class="be-wrapper be-login">
            <div class="be-content">
                <div class="main-content container-fluid">
                    <div class="splash-container">
                        <div role="alert" class="alert alert-contrast alert-dismissible" id="dl_ErrorDialog">
                            <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
                            <div class="message" id="dl_message">
                            </div>
                        </div>
                        <div class="card card-border-color card-border-color-primary">
                            <div class="card-header">
                                <%--CHG START DANIEL 20190124 [Change logo size and add motto]--%>
                                <%-- <img src="assets/img/logo-xx.png" alt="logo" width="102" height="" class="logo-img"/><span class="splash-description">Please enter your user information.</span>--%>
                               <%--CHG START DANIEL 20190220 [Change logo image]--%>
                                <%--<img src="assets/img/logo-xx.png" alt="logo" width="200" height="50" class="logo-img" />--%>
                                <%--<span class="splash-description" style="font-size:20px">Cashless Done Easy</span>--%>
                                <img src="assets/img/paydibs_logo-07.png" alt="logo" width="200" height="60" class="logo-img" />
                               <%--CHG E N D DANIEL 20190220 [Change logo image]--%>
                                <%--CHG E N D DANIEL 20190124 [Change logo size and add motto]--%>
                            </div>
                            <div> <span class="splash-description">Please enter your user information.</span></div>
                            <div class="card-body">
                                <div class="form-group">
                                    <asp:TextBox ID="txt_username" runat="server" placeholder="User ID" CssClass="form-control" required=""></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <asp:TextBox ID="txt_password" runat="server" placeholder="Password" CssClass="form-control" TextMode="Password" required=""></asp:TextBox>
                                </div>
                                <div class="form-group login-submit">

                                    <asp:Button ID="btn_SignIn" runat="server" Text="Sign In" CssClass="btn btn-primary btn-xl" OnClick="btn_SignIn_Click" />
                                </div>
                                <div class="form-group row login-tools">
                                    <div class="col-6 login-remember">
                                        <label class="custom-control custom-checkbox">
                                        </label>
                                    </div>
                                    <div class="col-6 login-forgot-password"><a href="ForgotPassword.aspx">Forgot Password?</a></div>
                                </div>
                            </div>
                        </div>
                        <%--<div class="splash-footer"><span>Don't have an account? <a href="pages-sign-up.html">Sign Up</a></span></div>--%>
                    </div>
                </div>
            </div>
            <div class="text-center" id="footer">
                Copyright © 2019 Paydibs Sdn. Bhd. All Rights Reserved.
            </div>
        </div>
        <script src="../../assets/lib/customize/General.js"></script>
        <script src="assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
        <script src="assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
        <script src="assets/lib/bootstrap/dist/js/bootstrap.bundle.min.js" type="text/javascript"></script>
        <script src="assets/js/app.js" type="text/javascript"></script>
        <script src="assets/lib/parsley/parsley.min.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                //initialize the javascript
                App.init();
                $('form').parsley();
            });

            $(function () {
                $('#dl_ErrorDialog').hide();
                var msg = $('#<%= HiddenField1.ClientID %>').val();
                var classType = $('#<%= HiddenField2.ClientID %>').val();
                if (msg != "") {
                    if (classType == "") classType = "alert-danger";
                    else classType = classType;
                    $('#dl_ErrorDialog').addClass(classType);
                    document.getElementById('dl_message').innerText = msg;
                    $('#dl_ErrorDialog').show();
                }
            });
        </script>
    </form>
</body>
</html>
