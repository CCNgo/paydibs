﻿using System;
using System.Drawing;
using System.IO;
using System.Web;
using PaydibsPortal.BLL;

public partial class Testing_Default2 : System.Web.UI.Page
{
    HomeController hc = new HomeController(0);
    protected void Page_Load(object sender, EventArgs e)
    {
        Session[SessionType.CurrentUser] = new CurrentUserStat("Admin", 1);


    }

    protected void btnUploadClick(object sender, EventArgs e)
    {
        HttpPostedFile file = Request.Files["myFile"];

        //check file was submitted
        if (file != null && file.ContentLength > 0)
        {
            string fname = Path.GetFileName(file.FileName);
            file.SaveAs(Server.MapPath(Path.Combine("~/assets/img/merchantlogo/", fname)));
        }
    }


    protected void btn_Complete_Click(object sender, EventArgs e)
    {
        int width = Convert.ToInt32(270);
        int height = Convert.ToInt32(120);

        if (Request.Files["file-2"] != null)
        {
            HttpPostedFile MyFile = Request.Files["file-2"];

            string TargetLocation = Server.MapPath("~/assets/img/merchantlogo/");
            if (MyFile.ContentLength > 0)
            {
                Bitmap bmp = new Bitmap(MyFile.InputStream);
                Bitmap resized = new Bitmap(bmp, new Size(width, height));

                string path = Server.MapPath("~/assets/img/merchantlogo/");
                resized.Save(path + "Merchantlogo1.png", System.Drawing.Imaging.ImageFormat.Png);

                ////Determining file name. You can format it as you wish.
                //string FileName = MyFile.FileName;
                ////Determining file size.
                //int FileSize = MyFile.ContentLength;
                ////Creating a byte array corresponding to file size.
                //byte[] FileByteArray = new byte[FileSize];
                ////Posted file is being pushed into byte array.
                //MyFile.InputStream.Read(FileByteArray, 0, FileSize);
                ////Uploading properly formatted file to server.
                //MyFile.SaveAs(TargetLocation + FileName);
            }

        }
        //Image img = Image.FromStream(FileUpload1.FileContent);
        //Bitmap original = (Bitmap)Image.FromStream(FileUpload1.FileContent);
        //Bitmap resized = new Bitmap(original, new Size(width, height));

        //string path = Server.MapPath("~/assets/img/merchantlogo/");
        //resized.Save(path + "Merchantlogo1.png", System.Drawing.Imaging.ImageFormat.Png);
    }
}