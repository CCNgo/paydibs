﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default2.aspx.cs" Inherits="Testing_Default2" %>

<!DOCTYPE html>
<%@ Register TagPrefix="uc" TagName="Left_Side_Bar" Src="~/UserControl/LeftSideBar.ascx" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <link rel="shortcut icon" href="../../assets/img/logo-fav.png" />
    <title>Paydis-Checkout New Merchant</title>
    <link rel="stylesheet" type="text/css" href="../../assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/bootstrap-slider/css/bootstrap-slider.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/material-design-icons/css/material-design-iconic-font.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/select2/css/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/lib/bootstrap-slider/css/bootstrap-slider.min.css" />
    <link rel="stylesheet" href="../../assets/css/app.css" type="text/css" />
</head>
<body>

    <form id="form1" runat="server">
        <div class="be-wrapper be-collapsible-sidebar be-collapsible-sidebar-collapsed">
            <uc:Left_Side_Bar ID="Left_Side_Bar" runat="server" />
            <div class="be-content">
                <div class="main-content container-fluid">
                    <div role="alert" class="alert alert-contrast alert-dismissible" id="dl_ErrorDialog">
                        <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
                        <div class="message" id="dl_message">
                        </div>
                    </div>
                    <div class="row wizard-row">
                        <div class="col-md-12 fuelux">
                            <div class="block-wizard card-border-color card-border-color-primary">
                                <div id="wizard1" class="wizard wizard-ux">
                                    <div class="steps-container">
                                        <ul class="steps">
                                            <li data-step="1" class="active">Section A - Company / Merchant Profile<span class="chevron"></span></li>
                                            <li data-step="2">Section B - Company Ownership Profile<span class="chevron"></span></li>
                                            <li data-step="3">Section C - Business Profile<span class="chevron"></span></li>
                                            <li data-step="4">Section D - Merchant Account Setup Information<span class="chevron"></span></li>
                                            <li data-step="5">Section E - Bank Account Information<span class="chevron"></span></li>
                                            <li data-step="6">Section F - Office Use<span class="chevron"></span></li>
                                        </ul>
                                    </div>
                                    <div class="actions">
                                        <button type="button" class="btn btn-xs btn-prev btn-secondary"><i class="icon mdi mdi-chevron-left"></i>Prev</button>
                                        <button type="button" data-last="Finish" class="btn btn-xs btn-next btn-secondary">Next<i class="icon mdi mdi-chevron-right"></i></button>
                                    </div>
                                    <div class="step-content">
                                        <div data-step="1" class="step-pane active">
                                            <div class="form-group row">
                                                <div class="col-sm-7">
                                                    <h3 class="wizard-title">Company Info</h3>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">Company Registered Name <small>(required)</small></label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                    <asp:TextBox ID="txt_BusinessRegName" placeholder="Company Registered Name" runat="server" CssClass="form-control" required="required"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">Company Registration No.</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                    <asp:TextBox ID="txt_BusinessRegNo" placeholder="Company Registration No." runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-12">
                                                    <button data-wizard="#wizard1" class="btn btn-secondary btn-space wizard-previous">Previous</button>
                                                    <button id="next1" data-wizard="#wizard1" class="btn btn-primary btn-space wizard-next">Next Step</button>
                                                </div>
                                            </div>
                                        </div>


                                        <div data-step="2" class="step-pane">
                                            <div class="form-group row">
                                                <div class="col-sm-7">
                                                    <h3 class="wizard-title">List of Company Director / Proprietor</h3>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <table id="table1" class="table table-striped table-hover table-fw-widget">
                                                    <thead>
                                                        <tr id="TB_column" runat="server">
                                                            <th></th>
                                                            <th>Director Name</th>
                                                            <th>Identity Card / Passport No.</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr class="odd gradeX">
                                                            <td>i</td>
                                                            <td>
                                                                <asp:TextBox ID="txt_i_director_name" placeholder="Full Name" runat="server" CssClass="form-control"></asp:TextBox></td>
                                                            <td>
                                                                <asp:TextBox ID="txt_i_director_id" placeholder="Identity Card / Passport No." runat="server" CssClass="form-control"></asp:TextBox></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-12">
                                                    <button data-wizard="#wizard1" class="btn btn-secondary btn-space wizard-previous">Previous</button>
                                                    <button data-wizard="#wizard1" class="btn btn-primary btn-space wizard-next">Next Step</button>
                                                </div>
                                            </div>
                                        </div>


                                        <div data-step="3" class="step-pane">
                                            <div class="form-group row">
                                                <div class="col-sm-7">
                                                    <h3 class="wizard-title">Business Profile</h3>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-4 col-form-label text-left text-sm-right">Doing Business As (DBA) Name / Trading Name</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                    <asp:TextBox ID="txt_businessName" placeholder="Doing Business As (DBA) Name / Trading Name" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-12">
                                                    <button data-wizard="#wizard1" class="btn btn-secondary btn-space wizard-previous">Previous</button>
                                                    <button data-wizard="#wizard1" class="btn btn-primary btn-space wizard-next">Next Step</button>
                                                </div>
                                            </div>
                                        </div>


                                        <div data-step="4" class="step-pane">
                                            <div class="form-group row">
                                                <div class="col-sm-7">
                                                    <h3 class="wizard-title">Merchant Account Setup Information</h3>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">Payment Notification Email</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                    <asp:TextBox ID="txt_IPNEMAIL" placeholder="Payment Notification Email" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-12">
                                                    <button data-wizard="#wizard1" class="btn btn-secondary btn-space wizard-previous">Previous</button>
                                                    <button data-wizard="#wizard1" class="btn btn-primary btn-space wizard-next">Next Step</button>
                                                </div>
                                            </div>
                                        </div>


                                        <div data-step="5" class="step-pane">
                                            <div class="form-group row">
                                                <div class="col-sm-7">
                                                    <h3 class="wizard-title">Bank Account Information</h3>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">Bank Name</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                    <asp:TextBox ID="txt_BankName" placeholder="Bank Name" MaxLength="50" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-12">
                                                    <button data-wizard="#wizard1" class="btn btn-secondary btn-space wizard-previous">Previous</button>
                                                    <button data-wizard="#wizard1" class="btn btn-primary btn-space wizard-next">Next Step</button>
                                                </div>
                                            </div>
                                        </div>


                                        <div data-step="6" class="step-pane">
                                            <div class="form-group row">
                                                <div class="col-sm-7">
                                                    <h3 class="wizard-title">Office Use</h3>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">Merchant Category Code (MCC)</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                    <asp:TextBox ID="txt_MCC" placeholder="Merchant Category Code (MCC)" runat="server" CssClass="form-control" required="" TextMode="Number"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-12">
                                                    <button data-wizard="#wizard1" class="btn btn-secondary btn-space wizard-previous">Previous</button>
                                                    <asp:Button ID="btn_Complete" runat="server" Text="Complete" CssClass="btn btn-success btn-space" OnClick="btn_Complete_Click" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <script src="../../assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/jquery/jquery.validate.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/bootstrap/dist/js/bootstrap.bundle.min.js" type="text/javascript"></script>
    <script src="../../assets/js/app.js" type="text/javascript"></script>
    <script src="../../assets/lib/fuelux/js/wizard.js" type="text/javascript"></script>
    <script src="../../assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/select2/js/select2.full.min.js" type="text/javascript"></script>
    <script src="../../assets/lib/bootstrap-slider/bootstrap-slider.min.js" type="text/javascript"></script>
    <script src="../../assets/js/app-form-wizard.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var navListItems = $('steps-container steps'),
                allWells = $('.setup-content'),
                allNextBtn = $('.step-pane');

            allWells.hide();

            navListItems.click(function (e) {
                e.preventDefault();
                var $target = $($(this).attr('href')),
                    $item = $(this);

                if (!$item.hasClass('disabled')) {
                    navListItems.removeClass('btn-primary').addClass('btn-default');
                    $item.addClass('btn-primary');
                    allWells.hide();
                    $target.show();
                    $target.find('input:eq(0)').focus();
                }
            });

            allNextBtn.click(function () {
                var curStep = $(this).closest(".setup-content"),
                    curStepBtn = curStep.attr("id"),
                    nextStepWizard = $('steps-container steps data-step=' + curStepBtn + '"]').parent().next().children("a"),
                    curInputs = curStep.find("input[type='text'],input[type='url']"),
                    isValid = true;

                $(".form-group").removeClass("has-error");
                for (var i = 0; i < curInputs.length; i++) {
                    if (!curInputs[i].validity.valid) {
                        isValid = false;
                        $(curInputs[i]).closest(".form-group").addClass("has-error");
                    }
                }

                if (isValid)
                    nextStepWizard.removeAttr('disabled').trigger('click');
            });

            $('steps-container steps a.btn-primary').trigger('click');
        });


    </script>
</body>
</html>
