﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kenji.Apps;
using System.Text;
using PaydibsPortal.DAL;
using NeoApp;
using System.Xml.Linq;

public partial class Testing_Default : System.Web.UI.Page
{
    PostData pd = new PostData();

    PGAdminDAL pgad = new PGAdminDAL();
    PGDAL pg = new PGDAL();
    OBDAL obd = new OBDAL();

    public string orderID = "";
    public string TxnType = "";
    public string MerchantID = "";
    public string MerchantPymtID = "";
    public string MerchantOrdID = "";
    public string MerchantOrdDesc = "";
    public string MerchantTxnAmt = "";
    public string MerchantCurrCode = "";
    public string MerchantRURL = "";
    public string CustIP = "";
    public string CustName = "";
    public string CustEmail = "";
    public string CustPhone = "";
    public string Sign = "";
    ///Conditional Field
    public string Method = "";
    public string AcqBank = "";
    public string MerchantName = "";
    public string MerchantCallBackURL = "";
    public string PageTimeout = "";

    string MerchantPassword = "pdbs12345";
    public string apiURL = "https://dev.paydibs.com/ppgsg/pymtcheckout.aspx";

    protected void Page_Load(object sender, EventArgs e)
    {
        //int iMapValue = Convert.ToInt32(oOpsMap[Convert.ToInt32(ddlOpsGroup.SelectedValue)]);

        //if ((Convert.ToInt32(Math.Pow(2, iBitIdx - 1)) & iMapValue) > 0) // do some math here
        int iMapValue = 0;

        for (int i = 1; i <= 9; i++)
        {
            int iBitIdx = i;

            if ((i == 1) || (i == 3) || (i == 5) || (i == 6))
            //if ((i != 2) && (i != 4))
            //if ((i <= 9))
            {
                // if checkbox checked, add the power of two for the bit index position - 1
                // each operation have its own bit index, every bit which set will be presented as 2 power of BitIndexPos - 1
                iMapValue += Convert.ToInt32(Math.Pow(2, iBitIdx - 1));
            }
        }


        orderID = "KT" + GUID.GetGUID(6);

        TxnType = "PAY";
        MerchantID = "PDBS";
        MerchantPymtID = orderID;
        MerchantOrdID = orderID;
        MerchantOrdDesc = "Test From Kenji,哈哈哈";
        MerchantTxnAmt = "10.00";
        MerchantCurrCode = "MYR";
        MerchantRURL = "http://localhost:51360/Testing/Default.aspx";
        CustIP = "103.12.152.124";
        CustName = "KENJI CHONG";
        CustEmail = "kenji@paydibs.com";
        CustPhone = "60127268539";
        ///Conditional Field
        Method = "";
        AcqBank = "";
        MerchantName = "";
        MerchantCallBackURL = "";
        PageTimeout = "";

        string baseString = MerchantPassword + TxnType + MerchantID + MerchantPymtID + MerchantOrdID +
                                  MerchantRURL + MerchantTxnAmt + MerchantCurrCode + CustIP + PageTimeout + MerchantCallBackURL;
        Sign = SHA.SHA512Encrypt(baseString);

        Encryption enc = new Encryption();

        string KEY = "e26b5be2-867a-4400-8079-612b4a2332f6";
        string ecr = enc.AESEncrypt("Data Source=192.168.2.168;Initial Catalog=PGAdmin_bk;Max Pool Size=350;Persist Security Info=True;User ID=paydibsPortal;Password=uUJfv1v@ck5Z2K1", KEY, KEY);
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        string TxnType = "QUERY";
        string Method = "OB";
        string MerchantID = "PDBS";
        string MerchantPymtID = "KTf9e327";
        string MerchantTxnAmt = "10.00";
        string MerchantCurrCode = "MYR";
        string baseString = MerchantPassword + MerchantID + MerchantPymtID + MerchantTxnAmt + MerchantCurrCode;
        string Sign = SHA.SHA512Encrypt(baseString);


        StringBuilder sb = new StringBuilder();
        sb.Append("TxnType=" + TxnType);
        sb.Append("&Method=" + Method);
        sb.Append("&MerchantID=" + MerchantID);
        sb.Append("&MerchantPymtID=" + MerchantPymtID);
        sb.Append("&MerchantTxnAmt=" + MerchantTxnAmt);
        sb.Append("&MerchantCurrCode=" + MerchantCurrCode);
        sb.Append("&Sign=" + Sign);


        string RtnRst = pd.Post_MSDN(apiURL, sb.ToString());
        Response.Write(RtnRst);
    }

    protected void Button2_Click(object sender, EventArgs e)
    {

        //string domainID        = ""; //Merchant Short Name, DOMAIN
        string domainName = "";
        string domainShortName = "";
        string userID = GUID.GetGUID(12);
        string PWD = MD5.GetMD5("1234qwer").ToLower();
        string LastName = "";
        string Title = "";
        string MobilePhone = "";
        string merchantPW = "";

        //DDD
        //var new_merchant = pg.Merchant_Insert(GUID.GetGUID(12), GUID.GetGUID(12), 0, 0, GUID.GetGUID(12), 0, "", "", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, GUID.GetGUID(12), GUID.GetGUID(12), GUID.GetGUID(12), GUID.GetGUID(12), GUID.GetGUID(12), GUID.GetGUID(12), GUID.GetGUID(12), GUID.GetGUID(12), GUID.GetGUID(12), GUID.GetGUID(12), GUID.GetGUID(12), GUID.GetGUID(3), 0, 0, 0, 0, 0, 0, 0, 0, 0, "", 0, DateTime.Now, DateTime.Now.AddYears(2), 0, "", "", 0, 0, GUID.GetGUID(12), "", 0, 0);
        //if (new_merchant != null)
        //{
        //    Response.Write("new_merchant: " + new_merchant.MerchantID + "<br/>");
        //}
        //var new_mer_rate = pg.MerchantRate_Insert(0, 100, "PDBS", "OB", 1, DateTime.Now, 0, 0, "MYR");
        //if (new_mer_rate != null)
        //{
        //    Response.Write("new_mer_rate: " + new_mer_rate.HostID + "<br/>");
        //}
        //var new_domain = pgad.CMS_Domain_Insert(GUID.GetGUID(12), GUID.GetGUID(3), GUID.GetGUID(3), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "REMARK", "KENJI");
        //if (new_domain != null)
        //{
        //    Response.Write("new_domain: " + new_domain.DomainID + "<br/>");
        //}
        //int domainID = new_domain.DomainID;

        //var new_contact = pgad.CMS_Contact_Insert(domainID, 0, "MR", "", "", "KENJI CHONG", "", "", "", "", "", "", "", "", "", "", new DateTime(1989, 05, 23), "M", "CHINESE", "", "", "", "", "", "", "", "", "", "", "", 0, "KENJI", "REMARK");
        //if (new_contact != null)
        //{
        //    Response.Write("new_contact: " + new_contact.DOB + "<br/>");
        //}
        //int contactID = new_contact.ContactID;


        //var new_user = pgad.CMS_User_Insert(GUID.GetGUID(12), PWD, contactID, 0, "KENJI", "REMARK");
        //if (new_user != null)
        //{
        //    Response.Write("new_user: " + new_user.UserID + "<br/>");
        //}
        //var new_useracct = pgad.CMS_UserAcct_Insert(0, domainID, 0, userID, 1, "KENJI", "REMARK");
        //if (new_useracct != null)
        //{
        //    Response.Write("new_useracct: " + new_useracct.UserID + "<br/>");
        //}
        //var new_role = pgad.tblUser_RolePermission_Insert(userID, domainID, true, true, true, true, true, true, true, true, "KENJI");
        //if (new_role != null)
        //{
        //    Response.Write("new_role: " + new_role.UserID + "<br/>");
        //}
        //var new_terminal = obd.Terminal_Insert(userID, "", "", "", "", "", "", "", "", "", "", "MYR", "", 0, 0, 0, DateTime.Now, DateTime.Now.AddYears(2), 0, 0);
        //if (new_terminal != null)
        //{
        //    Response.Write("new_terminal: " + new_terminal.MerchantID + "<br/>");
        //}

        //var new_merchant_info = pg.M3_MerchantInfo_Insert("", "", "", "", "", 0, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", 0, 0, "", "", "", "", "", "", "", 0);
        //if (new_merchant_info != null)
        //{
        //    Response.Write("new_merchant_info: " + new_merchant_info.PKID + "<br/>");
        //}

    }
}