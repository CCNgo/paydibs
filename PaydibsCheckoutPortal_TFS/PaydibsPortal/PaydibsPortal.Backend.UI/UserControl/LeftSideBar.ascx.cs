﻿using PaydibsPortal.BLL;
using System;
using System.Linq;
using System.Web.Configuration;

public partial class UserControl_LeftSideBar : System.Web.UI.UserControl
{
    #region Variable
    public string a_Dashboard = string.Empty;

    public string a_TransactionDetails = string.Empty;
    public string a_TransactionOB = string.Empty;
    public string a_TransactionCC = string.Empty;
    public string a_SettlementHistory = string.Empty;
    public string a_InternalReport = string.Empty;
    public string a_StatementAccount = string.Empty;
    // ADD START SUKI 20181127 [Add reseller report menu]
    public string a_ResellerDetails = string.Empty;
    public string a_ResellerOB = string.Empty;
    public string a_ResellerCC = string.Empty;
    // ADD E N D SUKI 20181127 [Add reseller report menu]
    //ADD START DANIEL 20190118 [Add TransactionFeeReport and ResellerMarginReport menu]
    public string a_TransactionsFeeReport = string.Empty;
    public string a_ResellerMarginReport = string.Empty;
    //ADD E N D DANIEL 20190118 [Add TransactionFeeReport and ResellerMarginReport menu]

    public string a_AddMerchant = string.Empty;
    public string a_EditMerchant = string.Empty;
    public string a_ViewMerchant = string.Empty;
    public string a_AddChannel = string.Empty;
    public string a_EditChannel = string.Empty;
    public string a_ViewChannel = string.Empty;
    public string a_AddMerchantRate = string.Empty;
    public string a_EditMerchantRate = string.Empty;
    public string a_ViewMerchantRate = string.Empty;
    public string a_ManageMerchant = string.Empty;
    //ADD START DANIEL 20190118 [Add AddResellerRate and ViewResellerRate menu]
    public string a_AddResellerRate = string.Empty;
    public string a_ViewResellerRate = string.Empty;
    //ADD E N D DANIEL 20190118 [Add AddResellerRate and ViewResellerRate menu]

    public string a_AddUserGroup = string.Empty;
    public string a_EditUserGroup = string.Empty;
    public string a_ViewUserGroup = string.Empty;
    public string a_EditUserGroupPermission = string.Empty;
    public string a_AddUser = string.Empty;
    public string a_EditUser = string.Empty;
    public string a_ViewUser = string.Empty;
    public string a_EditUserPermission = string.Empty;
    public string a_ChangePassword = string.Empty;

    public string a_EmailPayment = string.Empty;
    public string a_BuyNow = string.Empty;
    public string a_Refund = string.Empty;

    public string a_ViewFraudRules = string.Empty;
    public string a_EditFraudRules = string.Empty;
    public string a_ViewBlackListedCard = string.Empty;
    public string a_ManageBlackListedCard = string.Empty;
    public string a_ViewBlackListedCardRange = string.Empty;
    public string a_ManageBlackListedCardRange = string.Empty;
    public string a_ViewWhiteCard = string.Empty;
    public string a_ManageWhiteCard = string.Empty;
    public string a_ViewWhiteCardRange = string.Empty;
    public string a_ManageWhiteCardRange = string.Empty;
    public string a_ViewHotName = string.Empty;
    public string a_ManageHotName = string.Empty;
    public string a_ViewHotIP = string.Empty;
    public string a_ManageHotIP = string.Empty;
    public string a_ViewHotEmail = string.Empty;
    public string a_ManageHotEmail = string.Empty;
    public string a_ViewHotPhone = string.Empty;
    public string a_ManageHotPhone = string.Empty;
    public string a_ViewTransactionStatus = string.Empty;
    public string a_SummaryReportByMerchant = string.Empty; // Added by Daniel 29 Apr 2019. Declare variable a_SummaryReportByMerchant

    public string a_BankReconciliation = string.Empty;
    public string a_MechantSettlement = string.Empty;
    public string a_FinanceReport = string.Empty;
    public string a_BankFile = string.Empty;

    public string a_ViewLog = string.Empty;
    public string a_ViewLateResponse = string.Empty;
    public string a_ManageTransactionStatus = string.Empty;
    //ADD START DANIEL 20190327 [Add public string a_ResendCallBackResponse]
    public string a_ResendCallBackResponse = string.Empty;
    //ADD START DANIEL 20190327 [Add public string a_ResendCallBackResponse]

    public string a_HostStatusManagement = string.Empty;

    public string a_ViewAuditTrail = string.Empty;

    //ADD START DANIEL 20190312 [Add a_Documentation]
    public string a_Documentation = WebConfigurationManager.AppSettings["DocumentationPath"];
    //ADD E N D DANIEL 20190312 [Add a_Documentation]

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        HomeController hc = null;
        string currentPage = Request.Url.Segments[Request.Url.Segments.Count() - 1].ToLower();

        //li_transaction.Attributes.Remove("active");
        //li_transaction.Attributes.Remove("open");
        //li_sub_transaction.Attributes.Remove("active");
        //li_sub_setting.Attributes.Remove("active");

        var cus = (CurrentUserStat)Session[SessionType.CurrentUser];
        if (cus != null)
        {
            LB_UserID.Text = cus.UserID;
            lbl_UserID.Text = cus.UserID;
        }
        else
        {
            Response.Redirect("../../Default.aspx");
            return;
        }

        hc = new HomeController(cus.DomainID);
        var permission = hc.LoadUserPermission(cus.UserID);

        #region Load Permission
        foreach (var p in permission)
        {
            // Dashboard
            if (p.OpsID == 11)
            {
                if (p.Status == 0)
                {
                    li_Dashboard.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_Dashboard.Attributes.Add("hidden", "hidden");
                else
                    a_Dashboard = p.FileName;
            }

            // TXN
            if (p.OpsID == 21)
            {
                if (p.Status == 0)
                {
                    li_Transaction.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_Transaction.Attributes.Add("hidden", "hidden");
                else
                    a_TransactionDetails = p.FileName;
            }
            if (p.OpsID == 22)
            {
                if (p.Status == 0)
                {
                    li_SettlementHistory.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_SettlementHistory.Attributes.Add("hidden", "hidden");
                else
                    a_SettlementHistory = p.FileName;
            }
            if (p.OpsID == 23)
            {
                if (p.Status == 0)
                {
                    li_InternalReport.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_InternalReport.Attributes.Add("hidden", "hidden");
                else
                    a_InternalReport = p.FileName;
            }
            if (p.OpsID == 24)
            {
                if (p.Status == 0)
                {
                    li_StatementAccount.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_StatementAccount.Attributes.Add("hidden", "hidden");
                else
                    a_StatementAccount = p.FileName;
            }
            if (p.OpsID == 25)
            {
                if (p.Status == 0)
                {
                    li_TransactionOB.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_TransactionOB.Attributes.Add("hidden", "hidden");
                else
                    a_TransactionOB = p.FileName;
            }
            if (p.OpsID == 26)
            {
                if (p.Status == 0)
                {
                    li_TransactionCC.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_TransactionCC.Attributes.Add("hidden", "hidden");
                else
                    a_TransactionCC = p.FileName;
            }
            // ADD START SUKI 20181127 [Add reseller report menu]
            if (p.OpsID == 27)
            {
                if (p.Status == 0)
                {
                    li_ResellerDetails.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_ResellerDetails.Attributes.Add("hidden", "hidden");
                else
                    a_ResellerDetails = p.FileName;
            }
            if (p.OpsID == 28)
            {
                if (p.Status == 0)
                {
                    li_ResellerOB.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_ResellerOB.Attributes.Add("hidden", "hidden");
                else
                    a_ResellerOB = p.FileName;
            }
            if (p.OpsID == 29)
            {
                if (p.Status == 0)
                {
                    li_ResellerCC.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_ResellerCC.Attributes.Add("hidden", "hidden");
                else
                    a_ResellerCC = p.FileName;
            }
            // ADD E N D SUKI 20181127 [Add reseller report menu]

            //ADD START DANIEL 20190118 [Add TransactionFeesReport and ResellerMarginReport menu]
            if (p.OpsID == 210)
            {
                if (p.Status == 0)
                {
                    li_TransactionsFeeReport.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_TransactionsFeeReport.Attributes.Add("hidden", "hidden");
                else
                    a_TransactionsFeeReport = p.FileName;
            }
            if (p.OpsID == 211)
            {
                if (p.Status == 0)
                {
                    li_ResellerMarginReport.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_ResellerMarginReport.Attributes.Add("hidden", "hidden");
                else
                    a_ResellerMarginReport = p.FileName;
            }
            //ADD START DANIEL 20190118 [Add TransactionFeesReport and ResellerMarginReport menu]

            // MMGMT
            if (p.OpsID == 31)
            {
                if (p.Status == 0)
                {
                    li_AddMerchant.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_AddMerchant.Attributes.Add("hidden", "hidden");
                else
                    a_AddMerchant = p.FileName;
            }
            if (p.OpsID == 32)
            {
                if (p.Status == 0)
                {
                    li_EditMerchant.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_EditMerchant.Attributes.Add("hidden", "hidden");
                else
                    a_EditMerchant = p.FileName;
            }
            if (p.OpsID == 33)
            {
                if (p.Status == 0)
                {
                    li_ViewMerchant.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_ViewMerchant.Attributes.Add("hidden", "hidden");
                else
                    a_ViewMerchant = p.FileName;
            }
            if (p.OpsID == 34)
            {
                if (p.Status == 0)
                {
                    li_AddMerchantRate.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_AddMerchantRate.Attributes.Add("hidden", "hidden");
                else
                    a_AddMerchantRate = p.FileName;
            }
            if (p.OpsID == 35)
            {
                if (p.Status == 0)
                {
                    li_EditMerchantRate.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_EditMerchantRate.Attributes.Add("hidden", "hidden");
                else
                    a_EditMerchantRate = p.FileName;
            }
            if (p.OpsID == 36)
            {
                if (p.Status == 0)
                {
                    li_ViewMerchantRate.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_ViewMerchantRate.Attributes.Add("hidden", "hidden");
                else
                    a_ViewMerchantRate = p.FileName;
            }
            if (p.OpsID == 37)
            {
                if (p.Status == 0)
                {
                    li_ManageMerchant.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_ManageMerchant.Attributes.Add("hidden", "hidden");
                else
                    a_ManageMerchant = p.FileName;
            }
            if (p.OpsID == 38)
            {
                if (p.Status == 0)
                {
                    li_AddChannel.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_AddChannel.Attributes.Add("hidden", "hidden");
                else
                    a_AddChannel = p.FileName;
            }
            if (p.OpsID == 39)
            {
                if (p.Status == 0)
                {
                    li_EditChannel.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_EditChannel.Attributes.Add("hidden", "hidden");
                else
                    a_EditChannel = p.FileName;
            }
            if (p.OpsID == 310)
            {
                if (p.Status == 0)
                {
                    li_ViewChannel.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_ViewChannel.Attributes.Add("hidden", "hidden");
                else
                    a_ViewChannel = p.FileName;
            }

            //ADD START DANIEL 20190118 [Add AddResellerRate and ViewResellerRate menu]
            if (p.OpsID == 311)
            {
                if (p.Status == 0)
                {
                    li_AddResellerRate.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_AddResellerRate.Attributes.Add("hidden", "hidden");
                else
                    a_AddResellerRate = p.FileName;
            }
            if (p.OpsID == 312)
            {
                if (p.Status == 0)
                {
                    li_ViewResellerRate.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_ViewResellerRate.Attributes.Add("hidden", "hidden");
                else
                    a_ViewResellerRate = p.FileName;
            }
            //ADD E N D DANIEL 20190118 [Add AddResellerRate and ViewResellerRate menu]

            // CMS
            if (p.OpsID == 41)
            {
                if (p.Status == 0)
                {
                    li_AddUserGroup.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_AddUserGroup.Attributes.Add("hidden", "hidden");
                else
                    a_AddUserGroup = p.FileName;
            }
            if (p.OpsID == 42)
            {
                if (p.Status == 0)
                {
                    li_EditUserGroup.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_EditUserGroup.Attributes.Add("hidden", "hidden");
                else
                    a_EditUserGroup = p.FileName;
            }
            if (p.OpsID == 43)
            {
                if (p.Status == 0)
                {
                    li_ViewUserGroup.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_ViewUserGroup.Attributes.Add("hidden", "hidden");
                else
                    a_ViewUserGroup = p.FileName;
            }
            if (p.OpsID == 44)
            {
                if (p.Status == 0)
                {
                    li_EditUserGroupPermission.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_EditUserGroupPermission.Attributes.Add("hidden", "hidden");
                else
                    a_EditUserGroupPermission = p.FileName;
            }
            if (p.OpsID == 45)
            {
                if (p.Status == 0)
                {
                    li_AddUser.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_AddUser.Attributes.Add("hidden", "hidden");
                else
                    a_AddUser = p.FileName;
            }
            if (p.OpsID == 46)
            {
                if (p.Status == 0)
                {
                    li_EditUser.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_EditUser.Attributes.Add("hidden", "hidden");
                else
                    a_EditUser = p.FileName;
            }
            if (p.OpsID == 47)
            {
                if (p.Status == 0)
                {
                    li_ViewUser.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_ViewUser.Attributes.Add("hidden", "hidden");
                else
                    a_ViewUser = p.FileName;
            }
            if (p.OpsID == 48)
            {
                if (p.Status == 0)
                {
                    li_EditUserPermission.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_EditUserPermission.Attributes.Add("hidden", "hidden");
                else
                    a_EditUserPermission = p.FileName;
            }
            if (p.OpsID == 49)
            {
                if (p.Status == 0)
                {
                    li_ChangePassword.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_ChangePassword.Attributes.Add("hidden", "hidden");
                else
                    a_ChangePassword = p.FileName;
            }

            // MMS
            if (p.OpsID == 51)
            {
                if (p.Status == 0)
                {
                    li_EmailPayment.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_EmailPayment.Attributes.Add("hidden", "hidden");
                else
                    a_EmailPayment = p.FileName;
            }

            if (p.OpsID == 52)
            {
                if (p.Status == 0)
                {
                    li_BuyNow.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_BuyNow.Attributes.Add("hidden", "hidden");
                else
                    a_BuyNow = p.FileName;
            }

            if (p.OpsID == 53)
            {
                if (p.Status == 0)
                {
                    li_Refund.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_Refund.Attributes.Add("hidden", "hidden");
                else
                    a_Refund = p.FileName;
            }

            // FDS
            if (p.OpsID == 61)
            {
                if (p.Status == 0)
                {
                    li_ViewFraudRules.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_ViewFraudRules.Attributes.Add("hidden", "hidden");
                else
                    a_ViewFraudRules = p.FileName;
            }
            if (p.OpsID == 62)
            {
                if (p.Status == 0)
                {
                    li_EditFraudRules.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_EditFraudRules.Attributes.Add("hidden", "hidden");
                else
                    a_EditFraudRules = p.FileName;
            }
            if (p.OpsID == 63)
            {
                if (p.Status == 0)
                {
                    li_ViewBlackListedCard.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_ViewBlackListedCard.Attributes.Add("hidden", "hidden");
                else
                    a_ViewBlackListedCard = p.FileName;
            }
            if (p.OpsID == 64)
            {
                if (p.Status == 0)
                {
                    li_ManageBlackListedCard.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_ManageBlackListedCard.Attributes.Add("hidden", "hidden");
                else
                    a_ManageBlackListedCard = p.FileName;
            }
            if (p.OpsID == 65)
            {
                if (p.Status == 0)
                {
                    li_ViewBlackListedCardRange.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_ViewBlackListedCardRange.Attributes.Add("hidden", "hidden");
                else
                    a_ViewBlackListedCardRange = p.FileName;
            }
            if (p.OpsID == 66)
            {
                if (p.Status == 0)
                {
                    li_ManageBlackListedCardRange.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_ManageBlackListedCardRange.Attributes.Add("hidden", "hidden");
                else
                    a_ManageBlackListedCardRange = p.FileName;
            }
            if (p.OpsID == 67)
            {
                if (p.Status == 0)
                {
                    li_ViewWhiteCard.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_ViewWhiteCard.Attributes.Add("hidden", "hidden");
                else
                    a_ViewWhiteCard = p.FileName;
            }
            if (p.OpsID == 68)
            {
                if (p.Status == 0)
                {
                    li_ManageWhiteCard.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_ManageWhiteCard.Attributes.Add("hidden", "hidden");
                else
                    a_ManageWhiteCard = p.FileName;
            }
            if (p.OpsID == 69)
            {
                if (p.Status == 0)
                {
                    li_ViewWhiteCardRange.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_ViewWhiteCardRange.Attributes.Add("hidden", "hidden");
                else
                    a_ViewWhiteCardRange = p.FileName;
            }
            if (p.OpsID == 610)
            {
                if (p.Status == 0)
                {
                    li_ManageWhiteCardRange.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_ManageWhiteCardRange.Attributes.Add("hidden", "hidden");
                else
                    a_ManageWhiteCardRange = p.FileName;
            }
            if (p.OpsID == 611)
            {
                if (p.Status == 0)
                {
                    li_ViewHotName.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_ViewHotName.Attributes.Add("hidden", "hidden");
                else
                    a_ViewHotName = p.FileName;
            }
            if (p.OpsID == 612)
            {
                if (p.Status == 0)
                {
                    li_ManageHotName.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_ManageHotName.Attributes.Add("hidden", "hidden");
                else
                    a_ManageHotName = p.FileName;
            }
            if (p.OpsID == 613)
            {
                if (p.Status == 0)
                {
                    li_ViewHotIP.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_ViewHotIP.Attributes.Add("hidden", "hidden");
                else
                    a_ViewHotIP = p.FileName;
            }
            if (p.OpsID == 614)
            {
                if (p.Status == 0)
                {
                    li_ManageHotIP.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_ManageHotIP.Attributes.Add("hidden", "hidden");
                else
                    a_ManageHotIP = p.FileName;
            }
            if (p.OpsID == 615)
            {
                if (p.Status == 0)
                {
                    li_ViewHotEmail.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_ViewHotEmail.Attributes.Add("hidden", "hidden");
                else
                    a_ViewHotEmail = p.FileName;
            }
            if (p.OpsID == 616)
            {
                if (p.Status == 0)
                {
                    li_ManageHotEmail.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_ManageHotEmail.Attributes.Add("hidden", "hidden");
                else
                    a_ManageHotEmail = p.FileName;
            }
            if (p.OpsID == 617)
            {
                if (p.Status == 0)
                {
                    li_ViewHotPhone.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_ViewHotPhone.Attributes.Add("hidden", "hidden");
                else
                    a_ViewHotPhone = p.FileName;
            }
            if (p.OpsID == 618)
            {
                if (p.Status == 0)
                {
                    li_ManageHotPhone.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_ManageHotPhone.Attributes.Add("hidden", "hidden");
                else
                    a_ManageHotPhone = p.FileName;
            }
            if (p.OpsID == 619)
            {
                if (p.Status == 0)
                {
                    li_ViewTransactionStatus.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_ViewTransactionStatus.Attributes.Add("hidden", "hidden");
                else
                    a_ViewTransactionStatus = p.FileName;
            }

            //--START-- Added By Daniel 2019. Load permission Summary Report
            if (p.OpsID == 620)
            {
                if (p.Status == 0)
                {
                    li_SummaryReport.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_SummaryReport.Attributes.Add("hidden", "hidden");
                else
                    a_SummaryReportByMerchant = p.FileName;
            }
            //--END--

            //DEL START DANIEL 20190218 [Move to SUP]
            //if (p.OpsID == 620)
            //{
            //    if (p.Status == 0)
            //    {
            //        li_ManageTransactionStatus.Attributes.Add("hidden", "hidden");
            //        continue;
            //    }
            //    if (int.Parse(p.IsUserAllowed) == 0)
            //        li_ManageTransactionStatus.Attributes.Add("hidden", "hidden");
            //    else
            //        a_ManageTransactionStatus = p.FileName;
            //}
            //DEL E N D DANIEL 20190218 [Move to SUP]

            // FIN
            if (p.OpsID == 71)
            {
                if (p.Status == 0)
                {
                    li_BankReconciliation.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_BankReconciliation.Attributes.Add("hidden", "hidden");
                else
                    a_BankReconciliation = p.FileName;
            }
            if (p.OpsID == 72)
            {
                if (p.Status == 0)
                {
                    li_MechantSettlement.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_MechantSettlement.Attributes.Add("hidden", "hidden");
                else
                    a_MechantSettlement = p.FileName;
            }
            if (p.OpsID == 73)
            {
                if (p.Status == 0)
                {
                    li_FinanceReport.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_FinanceReport.Attributes.Add("hidden", "hidden");
                else
                    a_FinanceReport = p.FileName;
            }

            if (p.OpsID == 74)
            {
                if (p.Status == 0)
                {
                    li_BankFile.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_BankFile.Attributes.Add("hidden", "hidden");
                else
                    a_BankFile = p.FileName;
            }

            // SUP
            if (p.OpsID == 81)
            {
                if (p.Status == 0)
                {
                    li_ViewLog.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_ViewLog.Attributes.Add("hidden", "hidden");
                else
                    a_ViewLog = p.FileName;
            }
            if (p.OpsID == 82)
            {
                if (p.Status == 0)
                {
                    li_ViewLateResponse.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_ViewLateResponse.Attributes.Add("hidden", "hidden");
                else
                    a_ViewLateResponse = p.FileName;
            }
            //ADD START DANIEL 20190218 [Hide menu according to permission]
            if (p.OpsID == 83)
            {
                if (p.Status == 0)
                {
                    li_ManageTransactionStatus.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_ManageTransactionStatus.Attributes.Add("hidden", "hidden");
                else
                    a_ManageTransactionStatus = p.FileName;
            }
            //ADD E N D DANIEL 20190218 [Hide menu according to permission]

            //ADD START DANIEL 20190327 [Add permission for ResendCallBackResponse]
            if (p.OpsID == 84)
            {
                if (p.Status == 0)
                {
                    li_ResendCallBackResponse.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_ResendCallBackResponse.Attributes.Add("hidden", "hidden");
                else
                    a_ResendCallBackResponse = p.FileName;
            }
            //ADD E N D DANIEL 20190327 [Add permission for ResendCallBackResponse]



            // HOST
            if (p.OpsID == 91)
            {
                if (p.Status == 0)
                {
                    li_HostStatusManagement.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_HostStatusManagement.Attributes.Add("hidden", "hidden");
                else
                    a_HostStatusManagement = p.FileName;
            }

            // AUDIT
            if (p.OpsID == 101)
            {
                if (p.Status == 0)
                {
                    li_ViewAuditTrail.Attributes.Add("hidden", "hidden");
                    continue;
                }
                if (int.Parse(p.IsUserAllowed) == 0)
                    li_ViewAuditTrail.Attributes.Add("hidden", "hidden");
                else
                    a_ViewAuditTrail = p.FileName;
            }
        }
        #endregion

        #region Hide Parent Nodes
        if (string.IsNullOrEmpty(a_TransactionDetails) && string.IsNullOrEmpty(a_SettlementHistory) && string.IsNullOrEmpty(a_InternalReport) && string.IsNullOrEmpty(a_StatementAccount)
            // CHG START SUKI 20181127 [Add reseller report menu]
            //&& string.IsNullOrEmpty(a_TransactionOB) && string.IsNullOrEmpty(a_TransactionCC))
            && string.IsNullOrEmpty(a_TransactionOB) && string.IsNullOrEmpty(a_TransactionCC)
            && string.IsNullOrEmpty(a_ResellerDetails) && string.IsNullOrEmpty(a_ResellerOB) && string.IsNullOrEmpty(a_ResellerCC))
            // CHG E N D SUKI 20181127 [Add reseller report menu]
            li_TXN.Attributes.Add("hidden", "hidden");

        if (string.IsNullOrEmpty(a_TransactionDetails) && string.IsNullOrEmpty(a_TransactionOB) && string.IsNullOrEmpty(a_TransactionCC))
            li_Transaction.Attributes.Add("hidden", "hidden");

        // ADD START SUKI 20181127 [Add reseller report menu]
        if (string.IsNullOrEmpty(a_ResellerDetails) && string.IsNullOrEmpty(a_ResellerOB) && string.IsNullOrEmpty(a_ResellerCC))
            li_Reseller.Attributes.Add("hidden", "hidden");
        // ADD E N D SUKI 20181127 [Add reseller report menu]

        if (string.IsNullOrEmpty(a_AddMerchant) && string.IsNullOrEmpty(a_EditMerchant) && string.IsNullOrEmpty(a_ViewMerchant) &&
            string.IsNullOrEmpty(a_AddChannel) && string.IsNullOrEmpty(a_EditChannel) && string.IsNullOrEmpty(a_ViewChannel) &&
            string.IsNullOrEmpty(a_AddMerchantRate) && string.IsNullOrEmpty(a_EditMerchantRate) && string.IsNullOrEmpty(a_ViewMerchantRate) &&
            string.IsNullOrEmpty(a_ManageMerchant))
            li_MMGMT.Attributes.Add("hidden", "hidden");

        if (string.IsNullOrEmpty(a_AddUserGroup) && string.IsNullOrEmpty(a_EditUserGroup) && string.IsNullOrEmpty(a_ViewUserGroup) && string.IsNullOrEmpty(a_EditUserGroupPermission)
                && string.IsNullOrEmpty(a_AddUser) && string.IsNullOrEmpty(a_EditUser) && string.IsNullOrEmpty(a_ViewUser) && string.IsNullOrEmpty(a_EditUserPermission)
                && string.IsNullOrEmpty(a_ChangePassword))
            li_CMS.Attributes.Add("hidden", "hidden");

        if (string.IsNullOrEmpty(a_EmailPayment) && string.IsNullOrEmpty(a_BuyNow) && string.IsNullOrEmpty(a_Refund))
            li_MMS.Attributes.Add("hidden", "hidden");

        //CHG START DANIEL 20190218 [Remove ManageTransactionStatus]
        //if (string.IsNullOrEmpty(a_ViewFraudRules) && string.IsNullOrEmpty(a_EditFraudRules) && string.IsNullOrEmpty(a_ViewBlackListedCard) && string.IsNullOrEmpty(a_ManageBlackListedCard)
        //        && string.IsNullOrEmpty(a_ViewBlackListedCardRange) && string.IsNullOrEmpty(a_ManageBlackListedCardRange) && string.IsNullOrEmpty(a_ViewWhiteCard) && string.IsNullOrEmpty(a_ManageWhiteCard)
        //        && string.IsNullOrEmpty(a_ViewWhiteCardRange) && string.IsNullOrEmpty(a_ManageWhiteCardRange) && string.IsNullOrEmpty(a_ViewHotName) && string.IsNullOrEmpty(a_ManageHotName)
        //        && string.IsNullOrEmpty(a_ViewHotIP) && string.IsNullOrEmpty(a_ManageHotIP) && string.IsNullOrEmpty(a_ViewHotEmail) && string.IsNullOrEmpty(a_ManageHotEmail)
        //        && string.IsNullOrEmpty(a_ViewHotPhone) && string.IsNullOrEmpty(a_ManageHotPhone) && string.IsNullOrEmpty(a_ViewTransactionStatus) && string.IsNullOrEmpty(a_ManageTransactionStatus))
        if (string.IsNullOrEmpty(a_ViewFraudRules) && string.IsNullOrEmpty(a_EditFraudRules) && string.IsNullOrEmpty(a_ViewBlackListedCard) && string.IsNullOrEmpty(a_ManageBlackListedCard)
          && string.IsNullOrEmpty(a_ViewBlackListedCardRange) && string.IsNullOrEmpty(a_ManageBlackListedCardRange) && string.IsNullOrEmpty(a_ViewWhiteCard) && string.IsNullOrEmpty(a_ManageWhiteCard)
          && string.IsNullOrEmpty(a_ViewWhiteCardRange) && string.IsNullOrEmpty(a_ManageWhiteCardRange) && string.IsNullOrEmpty(a_ViewHotName) && string.IsNullOrEmpty(a_ManageHotName)
          && string.IsNullOrEmpty(a_ViewHotIP) && string.IsNullOrEmpty(a_ManageHotIP) && string.IsNullOrEmpty(a_ViewHotEmail) && string.IsNullOrEmpty(a_ManageHotEmail)
          && string.IsNullOrEmpty(a_ViewHotPhone) && string.IsNullOrEmpty(a_ManageHotPhone) && string.IsNullOrEmpty(a_ViewTransactionStatus) && string.IsNullOrEmpty(a_SummaryReportByMerchant))
            //CHG E N D DANIEL 20190218 [Remove ManageTransactionStatus]
            li_FDS.Attributes.Add("hidden", "hidden");

        //ADDED BY Daniel 3 May 2019. Hide parent SummaryReport if all is hidden --START
        if (string.IsNullOrEmpty(a_SummaryReportByMerchant))
        {
            li_SummaryReport.Attributes.Add("hidden", "hidden");
        }
        //--END
        if (string.IsNullOrEmpty(a_BankReconciliation) && string.IsNullOrEmpty(a_MechantSettlement) && string.IsNullOrEmpty(a_FinanceReport) && string.IsNullOrEmpty(a_BankFile))
            li_FIN.Attributes.Add("hidden", "hidden");

        //CHG START DANIEL 20190218 [Add ManageTransactionStatus]
        //if (string.IsNullOrEmpty(a_ViewLog) && string.IsNullOrEmpty(a_ViewLateResponse))
        if (string.IsNullOrEmpty(a_ViewLog) && string.IsNullOrEmpty(a_ViewLateResponse) && string.IsNullOrEmpty(a_ManageTransactionStatus) && string.IsNullOrEmpty(a_ResendCallBackResponse))
            li_SUP.Attributes.Add("hidden", "hidden");
        //CHG E N D DANIEL 20190218 [Add ManageTransactionStatus]

        if (string.IsNullOrEmpty(a_HostStatusManagement))
            li_HOST.Attributes.Add("hidden", "hidden");

        if (string.IsNullOrEmpty(a_ViewAuditTrail))
            li_AUDIT.Attributes.Add("hidden", "hidden");
        #endregion

        switch (currentPage)
        {
            case "Dashboard.aspx":
                li_Dashboard.Attributes.Add("class", "active");
                break;
            case "Transaction.aspx":
                li_TXN.Attributes.Add("class", "parent active open");
                li_Transaction.Attributes.Add("class", "active");
                break;

            //case "setting.aspx":
            //    li_transaction.Attributes.Add("class", "parent active open");
            //    li_sub_setting.Attributes.Add("class", "active");
            //    break;
            //case "merchantprofile.aspx":
            //    li_merchant.Attributes.Add("class", "parent active open");
            //    li_sub_merchant.Attributes.Add("class", "active");
            //    break;
            //case "useraccount.aspx":
            //    li_merchant.Attributes.Add("class", "parent active open");
            //    li_sub_useraccount.Attributes.Add("class", "active");
            //    break;
            //case "useraccdetail":
            //    li_merchant.Attributes.Add("class", "parent active open");
            //    li_sub_useraccount.Attributes.Add("class", "active");
            //    break;
            //case "statementreport.aspx":
            //    li_report.Attributes.Add("class", "parent active open");
            //    li_sub_statement.Attributes.Add("class", "active");
            //    break;
            //case "settlementreport.aspx":
            //    li_settlement.Attributes.Add("class", "parent active open");
            //    li_sub_settlement.Attributes.Add("class", "active");
            //    break;
            //case "settlementsetting.aspx":
            //    li_settlement.Attributes.Add("class", "parent active open");
            //    li_sub_settlementsetting.Attributes.Add("class", "active");
            //    break;
            default:
                break;
        }
    }
}