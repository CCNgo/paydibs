﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LeftSideBar.ascx.cs" Inherits="UserControl_LeftSideBar" %>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="../../assets/img/logo-fav.png">

<div class="be-wrapper be-collapsible-sidebar be-collapsible-sidebar-collapsed">
    <nav class="navbar navbar-expand fixed-top be-top-header">
        <div class="container-fluid">
            <div class="be-navbar-header">
                <a href="/Backoffice/Dashboard.aspx" class="navbar-brand"></a><a href="#" class="be-toggle-left-sidebar"><span class="icon mdi mdi-menu"></span></a>
            </div>
            <div class="be-right-navbar">
                <ul class="nav navbar-nav float-right be-user-nav">
                    <li class="nav-item dropdown"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle">
                        <img src="../../assets/img//avatar.png" alt="Avatar">
                        <span class="user-name">
                            <%--CHG START DANIEL 20190301 [Set current username]--%>
                            <%-- Túpac Amaru--%>
                            <asp:Label ID="lbl_UserID" runat="server" Text="Label"></asp:Label>
                            <%--CHG E N D DANIEL 20190301 [Set current username]--%>
                        </span></a>
                        <div role="menu" class="dropdown-menu">
                            <div class="user-info">
                                <div class="user-name">
                                    <asp:Label ID="LB_UserID" runat="server" Text="Label"></asp:Label>
                                </div>
                            </div>
                            <a href="../../Backoffice/AccountDetail.aspx" class="dropdown-item"><span class="icon mdi mdi-face"></span>Account</a>
                            <a href="../../Default.aspx" class="dropdown-item"><span class="icon mdi mdi-power"></span>Logout</a>
                        </div>
                    </li>
                </ul>
                <div class="page-title"><%--<span>Collapsible Sidebar</span>--%></div>
                <ul class="nav navbar-nav float-right be-icons-nav">
                    <%--<li class="nav-item dropdown"><a href="#" role="button" aria-expanded="false" class="nav-link be-toggle-right-sidebar"><span class="icon mdi mdi-settings"></span></a></li>--%>
                    <li class="nav-item dropdown"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><span class="icon mdi mdi-notifications"></span><%--<span class="indicator"></span>--%></a>
                        <ul class="dropdown-menu be-notifications">
                            <li>
                                <div class="title">Notifications<span class="badge badge-pill">0</span></div>
                                <div class="list">
                                    <div class="be-scroller">
                                        <div class="content">
                                            <ul>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="be-left-sidebar">
        <div class="left-sidebar-wrapper">
            <a href="#" class="left-sidebar-toggle">Collapsible Sidebar</a>
            <div class="left-sidebar-spacer">
                <div class="left-sidebar-scroll">
                    <div class="left-sidebar-content">
                        <ul class="sidebar-elements">
                            <li class="divider">Menu</li>
                            <li id="li_Dashboard" runat="server" title="Home"><a href="<%=a_Dashboard%>"><i class="icon mdi mdi-home"></i><span>Dashboard</span></a>
                            </li>

                            <li class="parent" id="li_TXN" runat="server" title="Transactions"><a href="#"><i class="icon mdi mdi-file-text"></i><span>Transactions</span></a>
                                <ul class="sub-menu">
                                    <li class="parent" id="li_Transaction" runat="server" title="Transactions"><a href="#"><span>Transactions</span></a>
                                        <ul class="sub-menu">
                                            <li id="li_TransactionDetails" runat="server"><a href="<%=a_TransactionDetails%>">Details Report</a>
                                            </li>
                                            <li id="li_TransactionOB" runat="server"><a href="<%=a_TransactionOB%>">Online Banking Report</a>
                                            </li>
                                            <li id="li_TransactionCC" runat="server"><a href="<%=a_TransactionCC%>">Credit Card Report</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li id="li_SettlementHistory" runat="server"><a href="<%=a_SettlementHistory%>">Settlement History</a>
                                    </li>
                                    <li id="li_InternalReport" runat="server"><a href="<%=a_InternalReport%>">Internal Report</a>
                                    </li>
                                    <li id="li_StatementAccount" runat="server"><a href="<%=a_StatementAccount%>">Statement of Account</a>
                                    </li>
                                    <%-- ADD START SUKI 20181127 [Add reseller report menu] --%>
                                    <li class="parent" id="li_Reseller" runat="server" title="Reseller"><a href="#"><span>Reseller</span></a>
                                        <ul class="sub-menu">
                                            <li id="li_ResellerDetails" runat="server"><a href="<%=a_ResellerDetails%>">Reseller Report</a>
                                            </li>
                                            <li id="li_ResellerOB" runat="server"><a href="<%=a_ResellerOB%>">Online Banking Report</a>
                                            </li>
                                            <li id="li_ResellerCC" runat="server"><a href="<%=a_ResellerCC%>">Credit Card Report</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <%-- ADD E N D SUKI 20181127 [Add reseller report menu] --%>

                                    <%--ADD START DANIEL 20190118 [Add TransactionsFeeReport and ResellerMarginReport menu]--%>
                                    <li id="li_TransactionsFeeReport" runat="server"><a href="<%=a_TransactionsFeeReport%>">Transactions Fee Report</a></li>
                                    <li id="li_ResellerMarginReport" runat="server"><a href="<%=a_ResellerMarginReport%>">Reseller Margin Report</a></li>
                                    <%--ADD E N D DANIEL 20190118 [Add TransactionsFeeReport and ResellerMarginReport menu]--%>
                                </ul>
                            </li>

                            <li class="parent" id="li_MMGMT" runat="server" title="Merchant"><a href="#"><i class="icon mdi mdi-assignment-account"></i><span>Merchant</span></a>
                                <ul class="sub-menu">
                                    <li id="li_AddMerchant" runat="server"><a href="<%=a_AddMerchant%>">Add Merchant</a>
                                    </li>
                                    <li id="li_EditMerchant" runat="server"><a href="<%=a_EditMerchant%>">Edit Merchant</a>
                                    </li>
                                    <li id="li_ViewMerchant" runat="server"><a href="<%=a_ViewMerchant%>">View Merchant</a>
                                    </li>
                                    <li id="li_AddChannel" runat="server"><a href="<%=a_AddChannel%>">Add Channel</a>
                                    </li>
                                    <li id="li_EditChannel" runat="server"><a href="<%=a_EditChannel%>">Edit Channel</a>
                                    </li>
                                    <li id="li_ViewChannel" runat="server"><a href="<%=a_ViewChannel%>">View Channel</a>
                                    </li>
                                    <li id="li_AddMerchantRate" runat="server"><a href="<%=a_AddMerchantRate%>">Add Merchant Rate</a>
                                    </li>
                                    <li id="li_EditMerchantRate" runat="server"><a href="<%=a_EditMerchantRate%>">Edit Merchant Rate</a>
                                    </li>
                                    <li id="li_ViewMerchantRate" runat="server"><a href="<%=a_ViewMerchantRate%>">View Merchant Rate</a>
                                    </li>
                                    <%--ADD START DANIEL 20190118 [Add AddResellerRate and ViewResellerRate menu]--%>
                                    <li id="li_AddResellerRate" runat="server"><a href="<%=a_AddResellerRate%>">Add Reseller Rate</a>
                                    </li>
                                    <li id="li_ViewResellerRate" runat="server"><a href="<%=a_ViewResellerRate%>">View Reseller Rate</a>
                                    </li>
                                    <%--ADD E N D DANIEL 20190118 [Add AddResellerRate and ViewResellerRate menu]--%>
                                    <li id="li_ManageMerchant" runat="server"><a href="<%=a_ManageMerchant%>">Manage Merchant</a>
                                    </li>
                                </ul>
                            </li>

                            <li class="parent" id="li_CMS" runat="server" title="User"><a href="#"><i class="icon mdi mdi-accounts"></i><span>User</span></a>
                                <ul class="sub-menu">
                                    <li id="li_AddUserGroup" runat="server"><a href="<%=a_AddUserGroup%>">Add User Group</a>
                                    </li>
                                    <li id="li_EditUserGroup" runat="server"><a href="<%=a_EditUserGroup%>">Edit User Group</a>
                                    </li>
                                    <li id="li_ViewUserGroup" runat="server"><a href="<%=a_ViewUserGroup%>">View User Group</a>
                                    </li>
                                    <li id="li_EditUserGroupPermission" runat="server"><a href="<%=a_EditUserGroupPermission%>">Edit User Group Permission</a>
                                    </li>
                                    <li id="li_AddUser" runat="server"><a href="<%=a_AddUser%>">Add User</a>
                                    </li>
                                    <li id="li_EditUser" runat="server"><a href="<%=a_EditUser%>">Edit User</a>
                                    </li>
                                    <li id="li_ViewUser" runat="server"><a href="<%=a_ViewUser%>">View User</a>
                                    </li>
                                    <li id="li_EditUserPermission" runat="server"><a href="<%=a_EditUserPermission%>">Edit User Permission</a>
                                    </li>
                                    <li id="li_ChangePassword" runat="server"><a href="<%=a_ChangePassword%>">Change Password</a>
                                    </li>
                                </ul>
                            </li>

                            <li class="parent" id="li_MMS" runat="server" title="Merchant Services"><a href="#"><i class="icon mdi mdi-folder-star"></i><span>Merchant Services</span></a>
                                <ul class="sub-menu">
                                    <li id="li_EmailPayment" runat="server"><a href="<%=a_EmailPayment%>">Email Payment (Single/Bulk)</a>
                                    </li>
                                    <li id="li_BuyNow" runat="server"><a href="<%=a_BuyNow%>">Buy Now Button</a>
                                    </li>
                                    <li id="li_Refund" runat="server"><a href="<%=a_Refund%>">Refund (Single/Bulk)</a>
                                    </li>
                                </ul>
                            </li>

                            <li class="parent" id="li_FDS" runat="server" title="Fraud Management"><a href="#"><i class="icon mdi mdi-shield-security"></i><span>Fraud Management</span></a>
                                <ul class="sub-menu">
                                    <li id="li_ViewFraudRules" runat="server"><a href="<%=a_ViewFraudRules%>">View Fraud Rules</a>
                                    </li>
                                    <li id="li_EditFraudRules" runat="server"><a href="<%=a_EditFraudRules%>">Edit Fraud Rules</a>
                                    </li>
                                    <li id="li_ViewBlackListedCard" runat="server"><a href="<%=a_ViewBlackListedCard%>">View BlackListed Card</a>
                                    </li>
                                    <li id="li_ManageBlackListedCard" runat="server"><a href="<%=a_ManageBlackListedCard%>">Manage BlackListed Card</a>
                                    </li>
                                    <li id="li_ViewBlackListedCardRange" runat="server"><a href="<%=a_ViewBlackListedCardRange%>">View BlackListed Card Range</a>
                                    </li>
                                    <li id="li_ManageBlackListedCardRange" runat="server"><a href="<%=a_ManageBlackListedCardRange%>">Manage BlackListed Card Range</a>
                                    </li>
                                    <li id="li_ViewWhiteCard" runat="server"><a href="<%=a_ViewWhiteCard%>">View White Card</a>
                                    </li>
                                    <li id="li_ManageWhiteCard" runat="server"><a href="<%=a_ManageWhiteCard%>">Manage White Card</a>
                                    </li>
                                    <li id="li_ViewWhiteCardRange" runat="server"><a href="<%=a_ViewWhiteCardRange%>">View White Card Range</a>
                                    </li>
                                    <li id="li_ManageWhiteCardRange" runat="server"><a href="<%=a_ManageWhiteCardRange%>">Manage White Card Range</a>
                                    </li>
                                    <li id="li_ViewHotName" runat="server"><a href="<%=a_ViewHotName%>">View Hot Name</a>
                                    </li>
                                    <li id="li_ManageHotName" runat="server"><a href="<%=a_ManageHotName%>">Manage Hot Name</a>
                                    </li>
                                    <li id="li_ViewHotIP" runat="server"><a href="<%=a_ViewHotIP%>">View Hot IP</a>
                                    </li>
                                    <li id="li_ManageHotIP" runat="server"><a href="<%=a_ManageHotIP%>">Manage Hot IP</a>
                                    </li>
                                    <li id="li_ViewHotEmail" runat="server"><a href="<%=a_ViewHotEmail%>">View Hot Email</a>
                                    </li>
                                    <li id="li_ManageHotEmail" runat="server"><a href="<%=a_ManageHotEmail%>">Manage Hot Email</a>
                                    </li>
                                    <li id="li_ViewHotPhone" runat="server"><a href="<%=a_ViewHotPhone%>">View Hot Phone</a>
                                    </li>
                                    <li id="li_ManageHotPhone" runat="server"><a href="<%=a_ManageHotPhone%>">Manage Hot Phone</a>
                                    </li>
                                    <li id="li_ViewTransactionStatus" runat="server"><a href="<%=a_ViewTransactionStatus%>">View Transaction Status</a>
                                    </li>

                                    <%----START . Added By Daniel 29 Apr 2019. Summary Report--%>
                                <%--    <li id="li_SummaryReport" runat="server"><a href="<%=a_SummaryReport%>">Summary Report</a>
                                    </li>
                           --%>
                                     <li class="parent" id="li_SummaryReport" runat="server" title="Fraud Management"><a href="#"><span> Summary Report</span></a>
                                <ul class="sub-menu">
                                    <li id="li_SummaryReportByMerchant" runat="server"><a href="<%=a_SummaryReportByMerchant%>">Summary Report By Merchant</a>
                                    </li>
                                </ul>
                            </li>
                                    <%----END--%>
                                    <%--DEL START DANIEL 20190218 [Move to Sup]--%>
                                    <%--  <li id="li_ManageTransactionStatus" runat="server"><a href="<%=a_ManageTransactionStatus%>">Manage Transaction Status</a>
                                    </li>--%>
                                    <%--DEL E N D DANIEL 20190218 [Move to Sup]--%>
                                 </ul>
                           </li>

                            <li class="parent" id="li_FIN" runat="server" title="Finance Management"><a href="#"><i class="icon mdi mdi-money"></i><span>Finance Management</span></a>
                                <ul class="sub-menu">
                                    <li id="li_BankReconciliation" runat="server"><a href="<%=a_BankReconciliation%>">Bank Reconciliation</a>
                                    </li>
                                    <li id="li_MechantSettlement" runat="server"><a href="<%=a_MechantSettlement%>">Mechant Settlement</a>
                                    </li>
                                    <li id="li_FinanceReport" runat="server"><a href="<%=a_FinanceReport%>">Finance Report</a>
                                    </li>
                                    <li id="li_BankFile" runat="server"><a href="<%=a_BankFile%>">Bank File</a>
                                    </li>
                                </ul>
                            </li>

                            <li class="parent" id="li_SUP" runat="server" title="Support"><a href="#"><i class="icon mdi mdi-download"></i><span>Support</span></a>
                                <ul class="sub-menu">
                                    <li id="li_ViewLog" runat="server"><a href="<%=a_ViewLog%>">View Log</a>
                                    </li>
                                    <li id="li_ViewLateResponse" runat="server"><a href="<%=a_ViewLateResponse%>">View Late Response Transaction</a>
                                    </li>
                                    <%--ADD START DANIEL 20190218 [Move to SUP]--%>
                                    <li id="li_ManageTransactionStatus" runat="server"><a href="<%=a_ManageTransactionStatus%>">Manage Transaction Status</a>
                                    </li>
                                    <%--ADD E N D DANIEL 20190218 [Move to SUP]--%>

                                     <%--ADD START DANIEL 20190327 [Move to SUP]--%>
                                    <li id="li_ResendCallBackResponse" runat="server"><a href="<%=a_ResendCallBackResponse%>">Resend Call Back Response</a>
                                    </li>
                                    <%--ADD E N D DANIEL 20190327 [Move to SUP]--%>
                                </ul>
                            </li>

                            <li class="parent" id="li_HOST" runat="server" title="Host Management"><a href="#"><i class="icon mdi mdi-card"></i><span>Host Management</span></a>
                                <ul class="sub-menu">
                                    <li id="li_HostStatusManagement" runat="server"><a href="<%=a_HostStatusManagement%>">Host Status Management</a>
                                    </li>
                                </ul>
                            </li>

                            <li class="parent" id="li_AUDIT" runat="server" title="Audit Trail"><a href="#"><i class="icon mdi mdi-badge-check"></i><span>Audit Trail</span></a>
                                <ul class="sub-menu">
                                    <li id="li_ViewAuditTrail" runat="server"><a href="<%=a_ViewAuditTrail%>">View Audit Trail</a>
                                    </li>
                                </ul>
                            </li>

                            <%--ADD START DANIEL 20190312 [Add Documentation in menu]--%>
                             <li class="parent" id="li_DOCUMENTATION" runat="server" title="Documentation"><a href="#"><i class="icon mdi mdi-book"></i><span>Documentation</span></a>
                                <ul class="sub-menu">
                                    <li id="li_Doc" runat="server"><a href="#" onclick="window.open('<%=a_Documentation %>'); return false" target="_blank">Portal Manual</a>
                                    </li>
                                </ul>
                            </li>
                            <%--ADD E N D DANIEL 20190312 [Add Documentation in menu]--%>
                        </ul>
                        <%--<ul class="sidebar-elements">
                            <li class="divider">Menu</li>
                            <li id="li_dashboard" runat="server"><a href="/Backoffice/Dashboard.aspx"><i class="icon mdi mdi-home"></i><span>Dashboard</span></a>
                            </li>
                            <li class="parent" id="li_transaction" runat="server"><a href="#"><i class="icon mdi mdi-money"></i><span>Transactions</span></a>
                                <ul class="sub-menu">
                                    <li id="li_sub_transaction" runat="server"><a href="../Backoffice/TXN/Transaction.aspx">Transactions</a>
                                    </li>
                                    <li id="li_sub_setting" runat="server"><a href="../Backoffice/Setting.aspx">Transaction Setting</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="parent" id="li_merchant" runat="server"><a href="#"><i class="icon mdi mdi-assignment-account"></i><span>Merchant</span></a>
                                <ul class="sub-menu">
                                    <li id="li_sub_merchant" runat="server"><a href="../Backoffice/MerchantProfile.aspx">Merchant Profile</a>
                                    </li>
                                    <li id="li_sub_useraccount" runat="server"><a href="../Backoffice/UserAccount.aspx">User Account</a>
                                    </li>
                                     <li id="li_sub_new_merchant" runat="server"><a href="../Backoffice/NewMerchant.aspx">New Merchant</a>
                                    </li>
                                </ul>
                            </li>
                            <%--<li class="parent" id="li_settlement" runat="server"><a href="#"><i class="icon mdi mdi-balance-wallet"></i><span>Settlements</span></a>
                                <ul class="sub-menu">
                                    <li id="li_sub_settlement" runat="server"><a href="../Backoffice/SettlementReport.aspx">Settlement Report</a>
                                    </li>
                                      <li id="li_sub_settlementsetting" runat="server"><a href="../Backoffice/SettlementSetting.aspx">Settlement Setting</a>
                                    </li>
                                </ul>
                            </li>--%>
                        <%--<li class="parent" id="li_report" runat="server"><a href="#"><i class="icon mdi mdi-assignment"></i><span>Report</span></a>
                                <ul class="sub-menu">
                                    <li id="li_sub_statement" runat="server"><a href="../Backoffice/StatementReport.aspx">Statement Report</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>--%>
                    </div>
                </div>
            </div>
            <%--  <div class="progress-widget">
                <div class="progress-data"><span class="progress-value">60%</span><span class="name">Current Project</span></div>
                <div class="progress">
                    <div style="width: 60%;" class="progress-bar progress-bar-primary"></div>
                </div>
            </div>--%>
        </div>
    </div>
    <%--   <div class="be-content">
        <div class="page-head">
            <h2 class="page-head-title">Collapsible Sidebar</h2>
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb page-head-nav">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">Layouts</a></li>
                    <li class="breadcrumb-item active">Collapsible Sidebar</li>
                </ol>
            </nav>
        </div>
    </div>--%>
    <nav class="be-right-sidebar">
        <div class="sb-content">
            <div class="tab-navigation">
                <ul role="tablist" class="nav nav-tabs nav-justified">
                    <li role="presentation" class="nav-item"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab" class="nav-link active">Chat</a></li>
                    <li role="presentation" class="nav-item"><a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab" class="nav-link">Todo</a></li>
                    <li role="presentation" class="nav-item"><a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab" class="nav-link">Settings</a></li>
                </ul>
            </div>
            <div class="tab-panel">
                <div class="tab-content">
                    <div id="tab1" role="tabpanel" class="tab-pane tab-chat active">
                        <div class="chat-contacts">
                            <div class="chat-sections">
                                <div class="be-scroller">
                                    <div class="content">
                                        <h2>Recent</h2>
                                        <div class="contact-list contact-list-recent">
                                            <div class="user">
                                                <a href="#">
                                                    <img src="../../assets/img//avatar1.png" alt="Avatar">
                                                    <div class="user-data"><span class="status away"></span><span class="name">Claire Sassu</span><span class="message">Can you share the...</span></div>
                                                </a>
                                            </div>
                                            <div class="user">
                                                <a href="#">
                                                    <img src="../../assets/img//avatar2.png" alt="Avatar">
                                                    <div class="user-data"><span class="status"></span><span class="name">Maggie jackson</span><span class="message">I confirmed the info.</span></div>
                                                </a>
                                            </div>
                                            <div class="user">
                                                <a href="#">
                                                    <img src="../../assets/img//avatar3.png" alt="Avatar">
                                                    <div class="user-data"><span class="status offline"></span><span class="name">Joel King		</span><span class="message">Ready for the meeti...</span></div>
                                                </a>
                                            </div>
                                        </div>
                                        <h2>Contacts</h2>
                                        <div class="contact-list">
                                            <div class="user">
                                                <a href="#">
                                                    <img src="../../assets/img//avatar4.png" alt="Avatar">
                                                    <div class="user-data2"><span class="status"></span><span class="name">Mike Bolthort</span></div>
                                                </a>
                                            </div>
                                            <div class="user">
                                                <a href="#">
                                                    <img src="../../assets/img//avatar5.png" alt="Avatar">
                                                    <div class="user-data2"><span class="status"></span><span class="name">Maggie jackson</span></div>
                                                </a>
                                            </div>
                                            <div class="user">
                                                <a href="#">
                                                    <img src="../../assets/img//avatar6.png" alt="Avatar">
                                                    <div class="user-data2"><span class="status offline"></span><span class="name">Jhon Voltemar</span></div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bottom-input">
                                <input type="text" placeholder="Search..." name="q"><span class="mdi mdi-search"></span>
                            </div>
                        </div>
                        <div class="chat-window">
                            <div class="title">
                                <div class="user">
                                    <img src="../../assets/img//avatar2.png" alt="Avatar">
                                    <h2>Maggie jackson</h2>
                                    <span>Active 1h ago</span>
                                </div>
                                <span class="icon return mdi mdi-chevron-left"></span>
                            </div>
                            <div class="chat-messages">
                                <div class="be-scroller">
                                    <div class="content">
                                        <ul>
                                            <li class="friend">
                                                <div class="msg">Hello</div>
                                            </li>
                                            <li class="self">
                                                <div class="msg">Hi, how are you?</div>
                                            </li>
                                            <li class="friend">
                                                <div class="msg">Good, I'll need support with my pc</div>
                                            </li>
                                            <li class="self">
                                                <div class="msg">Sure, just tell me what is going on with your computer?</div>
                                            </li>
                                            <li class="friend">
                                                <div class="msg">I don't know it just turns off suddenly</div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="chat-input">
                                <div class="input-wrapper">
                                    <span class="photo mdi mdi-camera"></span>
                                    <input type="text" placeholder="Message..." name="q" autocomplete="off"><span class="send-msg mdi mdi-mail-send"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="tab2" role="tabpanel" class="tab-pane tab-todo">
                        <div class="todo-container">
                            <div class="todo-wrapper">
                                <div class="be-scroller">
                                    <div class="todo-content">
                                        <span class="category-title">Today</span>
                                        <ul class="todo-list">
                                            <li>
                                                <label class="custom-checkbox custom-control custom-control-sm">
                                                    <span class="delete mdi mdi-delete"></span>
                                                    <input type="checkbox" checked="" class="custom-control-input"><span class="custom-control-label">Initialize the project</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="custom-checkbox custom-control custom-control-sm">
                                                    <span class="delete mdi mdi-delete"></span>
                                                    <input type="checkbox" class="custom-control-input"><span class="custom-control-label">Create the main structure							</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="custom-checkbox custom-control custom-control-sm">
                                                    <span class="delete mdi mdi-delete"></span>
                                                    <input type="checkbox" class="custom-control-input"><span class="custom-control-label">Updates changes to GitHub							</span>
                                                </label>
                                            </li>
                                        </ul>
                                        <span class="category-title">Tomorrow</span>
                                        <ul class="todo-list">
                                            <li>
                                                <label class="custom-checkbox custom-control custom-control-sm">
                                                    <span class="delete mdi mdi-delete"></span>
                                                    <input type="checkbox" class="custom-control-input"><span class="custom-control-label">Initialize the project							</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="custom-checkbox custom-control custom-control-sm">
                                                    <span class="delete mdi mdi-delete"></span>
                                                    <input type="checkbox" class="custom-control-input"><span class="custom-control-label">Create the main structure							</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="custom-checkbox custom-control custom-control-sm">
                                                    <span class="delete mdi mdi-delete"></span>
                                                    <input type="checkbox" class="custom-control-input"><span class="custom-control-label">Updates changes to GitHub							</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="custom-checkbox custom-control custom-control-sm">
                                                    <span class="delete mdi mdi-delete"></span>
                                                    <input type="checkbox" class="custom-control-input"><span title="This task is too long to be displayed in a normal space!" class="custom-control-label">This task is too long to be displayed in a normal space!							</span>
                                                </label>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="bottom-input">
                                <input type="text" placeholder="Create new task..." name="q"><span class="mdi mdi-plus"></span>
                            </div>
                        </div>
                    </div>
                    <div id="tab3" role="tabpanel" class="tab-pane tab-settings">
                        <div class="settings-wrapper">
                            <div class="be-scroller">
                                <span class="category-title">General</span>
                                <ul class="settings-list">
                                    <li>
                                        <div class="switch-button switch-button-sm">
                                            <input type="checkbox" checked="" name="st1" id="st1"><span>
                                                <label for="st1"></label>
                                            </span>
                                        </div>
                                        <span class="name">Available</span>
                                    </li>
                                    <li>
                                        <div class="switch-button switch-button-sm">
                                            <input type="checkbox" checked="" name="st2" id="st2"><span>
                                                <label for="st2"></label>
                                            </span>
                                        </div>
                                        <span class="name">Enable notifications</span>
                                    </li>
                                    <li>
                                        <div class="switch-button switch-button-sm">
                                            <input type="checkbox" checked="" name="st3" id="st3"><span>
                                                <label for="st3"></label>
                                            </span>
                                        </div>
                                        <span class="name">Login with Facebook</span>
                                    </li>
                                </ul>
                                <span class="category-title">Notifications</span>
                                <ul class="settings-list">
                                    <li>
                                        <div class="switch-button switch-button-sm">
                                            <input type="checkbox" name="st4" id="st4"><span>
                                                <label for="st4"></label>
                                            </span>
                                        </div>
                                        <span class="name">Email notifications</span>
                                    </li>
                                    <li>
                                        <div class="switch-button switch-button-sm">
                                            <input type="checkbox" checked="" name="st5" id="st5"><span>
                                                <label for="st5"></label>
                                            </span>
                                        </div>
                                        <span class="name">Project updates</span>
                                    </li>
                                    <li>
                                        <div class="switch-button switch-button-sm">
                                            <input type="checkbox" checked="" name="st6" id="st6"><span>
                                                <label for="st6"></label>
                                            </span>
                                        </div>
                                        <span class="name">New comments</span>
                                    </li>
                                    <li>
                                        <div class="switch-button switch-button-sm">
                                            <input type="checkbox" name="st7" id="st7"><span>
                                                <label for="st7"></label>
                                            </span>
                                        </div>
                                        <span class="name">Chat messages</span>
                                    </li>
                                </ul>
                                <span class="category-title">Workflow</span>
                                <ul class="settings-list">
                                    <li>
                                        <div class="switch-button switch-button-sm">
                                            <input type="checkbox" name="st8" id="st8"><span>
                                                <label for="st8"></label>
                                            </span>
                                        </div>
                                        <span class="name">Deploy on commit</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>

</div>


