﻿var MerchantID;
var channel = [], info = {};
var today = new Date();
today.setDate(today.getDate() + 1);
var dd = today.getDate();
var mm = today.getMonth() + 1;
var yyyy = today.getFullYear();
var textBoxCss = "#e57373";

if(dd<10) {
    dd = '0'+dd
} 

if(mm<10) {
    mm = '0'+mm
} 
var todaydate = yyyy + "-" + mm + "-" + dd;

function CheckInputValue(inputID) {
    if (inputID) {
        if (document.getElementById(inputID).value <= 0) {
            document.getElementById(inputID).style.backgroundColor = textBoxCss;
        } else {
            document.getElementById(inputID).style.backgroundColor = "White";
        }
    } else {
        if (channel.length > 0) {
            channel.forEach(function (list) {
                if (list.hasOwnProperty('CC')) {
                    if (list.CC > 0) {
                        for (var num = 0; num < list.CC; num++) {
                            if ($("#CC_Rate_" + num).val() <= 0) {
                                $("#CC_Rate_" + num).css({ "background-color": textBoxCss });
                            }
                            if ($("#CC_FixedFee_" + num).val() <= 0) {
                                $("#CC_FixedFee_" + num).css({ "background-color": textBoxCss });
                            }
                        }
                    }
                }
                else if (list.hasOwnProperty('OB')) {
                    if (list.OB > 0) {
                        for (var num = 0; num < list.OB; num++) {
                            if ($("#OB_Rate_" + num).val() <= 0) {
                                $("#OB_Rate_" + num).css({ "background-color": textBoxCss });
                            }
                            if ($("#OB_FixedFee_" + num).val() <= 0) {
                                $("#OB_FixedFee_" + num).css({ "background-color": textBoxCss });
                            }
                        }
                    }
                }
                else if (list.hasOwnProperty('WA')) {
                    if (list.WA > 0) {
                        for (var num = 0; num < list.WA; num++) {
                            if ($("#WA_Rate_" + num).val() <= 0) {
                                $("#WA_Rate_" + num).css({ "background-color": textBoxCss });
                            }
                            if ($("#WA_FixedFee_" + num).val() <= 0) {
                                $("#WA_FixedFee_" + num).css({ "background-color": textBoxCss });
                            }
                        }
                    }
                }
                else if (list.hasOwnProperty('OTC')) {
                    if (list.OTC > 0) {
                        for (var num = 0; num < list.OTC; num++) {
                            if ($("#OTC_Rate_" + num).val() <= 0) {
                                $("#OTC_Rate_" + num).css({ "background-color": textBoxCss });
                            }
                            if ($("#OTC_FixedFee_" + num).val() <= 0) {
                                $("#OTC_FixedFee_" + num).css({ "background-color": textBoxCss });
                            }
                        }
                    }
                }
                else if (list.hasOwnProperty('MO')) {
                    if (list.MO > 0) {
                        for (var num = 0; num < list.MO; num++) {
                            if ($("#MO_Rate_" + num).val() <= 0) {
                                $("#MO_Rate_" + num).css({ "background-color": textBoxCss });
                            }
                            if ($("#MO_FixedFee_" + num).val() <= 0) {
                                $("#MO_FixedFee_" + num).css({ "background-color": textBoxCss });
                            }
                        }
                    }
                }
            });
        }
    }
}

function AppendToFutureRate(merchantChannel) {
    var html = "";
            
    if (merchantChannel.hasOwnProperty('OB')) {
        if (merchantChannel.OB) {
            if (merchantChannel.OB.length > 0) {
                html = html + "<div id='FOB'><div class='card-header card-header-divider font-weight-bold'> Online Banking </div><div class='card-body'>";
                html = html + "<div><table id='forOB' class='table table-striped table-hover table-fw-widget'><tr class='odd gradeX'><th>Payment Method</th><th>Channel</th><th>Rate (%)</th><th>Fixed Fee</th><th>Start Date</th><th>Currency</th></tr>";
                (merchantChannel.OB).forEach(function (data, num) {
                    var a = data;
                    html = html + "<tr class='odd gradeX'><td id='FOB_method_" + num + "'>" + a.PaymentMethod
                        + "</td><td id='FOB_Host_Id_" + num + "' style='display:none'>" + a.HostId + "</td><td style='width: 150px;'>" + a.HostDesc
                        + "</td><td><input onpaste='return false;' type='number' step='.0001' min='0' max='100' id='FOB_Rate_" + num
                        + "'></td><td><input onpaste='return false;' type='number' step='.0001' min='0' id='FOB_FixedFee_" + num
                        + "'></td><td><input type='date' min='" + todaydate + "' onchange='checkDate(FOB_Start_Date_" + num 
                        + ")' id='FOB_Start_Date_" + num + "'></td><td id='FOB_CurrencyCode_Id_" + num + "' >" + a.CurrencyCode + "</td></tr>";
                });
                html = html + "</div></div>";
            }
            $("#merchantFutureRate").append(html);
            html = "";
        }
    }
    if (merchantChannel.hasOwnProperty('CC')) {
        if (merchantChannel.CC) {
            if (merchantChannel.CC.length > 0) {
                html = html + "<div id='FCC'><div class='card-header card-header-divider font-weight-bold'> Credit Card </div><div class='card-body'>";
                html = html + "<div><table id='forOB' class='table table-striped table-hover table-fw-widget'><tr class='odd gradeX'><th>Payment Method</th><th>Channel</th><th>Rate (%)</th><th>Fixed Fee</th><th>Start Date</th><th>Currency</th></tr>";
                (merchantChannel.CC).forEach(function (data, num) {
                    var a = data;
                    html = html + "<tr class='odd gradeX'><td id='FCC_method_" + num + "' >" + a.PaymentMethod
                        + "</td><td id='FCC_Host_Id_" + num + "' style='display:none'>" + a.HostId + "</td><td style='width: 150px;'>" + a.HostDesc
                        + "</td><td ><input onpaste='return false;' type='number' step='.0001' min='0' max='100' id='FCC_Rate_" + num
                        + "'></td><td ><input onpaste='return false;' type='number' step='.0001' min='0' id='FCC_FixedFee_" + num
                        + "'></td><td><input type='date' min='" + todaydate + "' onchange='checkDate(FCC_Start_Date_" + num 
                        + ")' id='FCC_Start_Date_" + num + "'></td><td id='FCC_CurrencyCode_Id_" + num + "' >" + a.CurrencyCode + "</td></tr>";
                });
                html = html + "</div></div>";
            }
            $("#merchantFutureRate").append(html);
            html = "";
        }
    }
    if (merchantChannel.hasOwnProperty('WA')) {
        if (merchantChannel.WA) {
            if (merchantChannel.WA.length > 0) {
                html = html + "<div id='FWA'><div class='card-header card-header-divider font-weight-bold'> E-Wallet </div><div class='card-body'>";
                html = html + "<div><table id='forWA' class='table table-striped table-hover table-fw-widget'><tr class='odd gradeX'><th>Payment Method</th><th>Channel</th><th>Rate (%)</th><th>Fixed Fee</th><th>Start Date</th><th>Currency</th></tr>";
                (merchantChannel.WA).forEach(function (data, num) {
                    var a = data;
                    html = html + "<tr class='odd gradeX'><td id='FWA_method_" + num + "' >" + a.PaymentMethod
                        + "</td><td id='FWA_Host_Id_" + num + "' style='display:none'>" + a.HostId + "</td><td style='width: 150px;'>" + a.HostDesc
                        + "</td><td ><input onpaste='return false;' type='number' step='.0001' min='0' max='100' id='FWA_Rate_" + num
                        + "'></td><td ><input onpaste='return false;' type='number' step='.0001' min='0' id='FWA_FixedFee_" + num
                        + "'></td><td><input type='date' min='" + todaydate + "' onchange='checkDate(FWA_Start_Date_" + num 
                        + ")' id='FWA_Start_Date_" + num + "'></td><td id='FWA_CurrencyCode_Id_" + num + "' >" + a.CurrencyCode + "</td></tr>";
                });
                html = html + "</div></div>";
            }
            $("#merchantFutureRate").append(html);
            html = "";
        }
    }
    if (merchantChannel.hasOwnProperty('OTC')) {
        if (merchantChannel.OTC) {
            if (merchantChannel.OTC.length > 0) {
                html = html + "<div id='FOTC'><div class='card-header card-header-divider font-weight-bold'> Over the Counter </div><div class='card-body'>";
                html = html + "<div><table class='table table-striped table-hover table-fw-widget'><tr class='odd gradeX'><th>Payment Method</th><th>Channel</th><th>Rate (%)</th><th>Fixed Fee</th><th>Start Date</th><th>Currency</th></tr>";
                (merchantChannel.OTC).forEach(function (data, num) {
                    var a = data;
                    html = html + "<tr class='odd gradeX'><td id='FOTC_method_" + num + "' >" + a.PaymentMethod
                        + "</td><td id='FOTC_Host_Id_" + num + "' style='display:none'>" + a.HostId + "</td><td style='width: 150px;'>" + a.HostDesc
                        + "</td><td ><input onpaste='return false;' type='number' step='.0001' min='0' max='100' id='FOTC_Rate_" + num
                        + "'></td><td ><input onpaste='return false;' type='number' step='.0001' min='0' id='FOTC_FixedFee_" + num
                        + "'></td><td><input type='date' min='" + todaydate + "' onchange='checkDate(FOTC_Start_Date_" + num 
                        + ")' id='FOTC_Start_Date_" + num + "'></td><td id='FOTC_CurrencyCode_Id_" + num + "' >" + a.CurrencyCode + "</td></tr>";
                });
                html = html + "</div></div>";
            }
            $("#merchantFutureRate").append(html);
            html = "";
        }
    }
    if (merchantChannel.hasOwnProperty('MO')) {
        if (merchantChannel.MO) {
            if (merchantChannel.MO.length > 0) {
                html = html + "<dic id='FMO'><div class='card-header card-header-divider font-weight-bold'> Motopay </div><div class='card-body'>";
                html = html + "<div><table class='table table-striped table-hover table-fw-widget'><tr class='odd gradeX'><th>Payment Method</th><th>Channel</th><th>Rate (%)</th><th>Fixed Fee</th><th>Start Date</th><th>Currency</th></tr>";
                (merchantChannel.MO).forEach(function (data, num) {
                    var a = data;
                    html = html + "<tr class='odd gradeX'><td id='FMO_method_" + num + "' >" + a.PaymentMethod
                        + "</td><td id='FMO_Host_Id_" + num + "' style='display:none'>" + a.HostId + "</td><td style='width: 150px;'>" + a.HostDesc
                        + "</td><td ><input onpaste='return false;' type='number' step='.0001' min='0' max='100' id='FMO_Rate_" + num
                        + "'></td><td ><input onpaste='return false;' type='number' step='.0001' min='0' id='FMO_FixedFee_" + num
                        + "'></td><td><input type='date' min='" + todaydate + "' onchange='checkDate(FMO_Start_Date_" + num 
                        + ")' id='FMO_Start_Date_" + num + "'></td><td id='FMO_CurrencyCode_Id_" + num + "' >" + a.CurrencyCode + "</td></tr>";
                });
                html = html + "</div></div>";
            }
            $("#merchantFutureRate").append(html);
            html = "";
        }
    }
    $("#Button1").prop("disabled",false);
}
function AppendToAddRate(merchantChannel) {
    var html = "";
    MerchantID = merchantChannel.MID;
    if (merchantChannel.hasOwnProperty('OB')) {
        if (merchantChannel.OB) {
            if (merchantChannel.OB.length > 0) {
                info = { OB: merchantChannel.OB.length };
                channel.push(info);
                html = html + "<div id='OB'><div class='card-header card-header-divider font-weight-bold'> Online Banking </div><div class='card-body'>";
                html = html + "<div><table id='forOB' class='table table-striped table-hover table-fw-widget'><tr class='odd gradeX'><th>Payment Method</th><th>Channel</th><th>Rate (%)</th><th>Fixed Fee</th><th>Currency</th></tr>";
                (merchantChannel.OB).forEach(function (data, num) {
                    var a = data;
                    html = html + "<tr class='odd gradeX'><td id='OB_method_" + num + "' style='width: 90px;'>" + a.PaymentMethod
                        + "</td><td id='OB_Host_Id_" + num + "' style='display:none'>" + a.HostId + "</td><td style='width: 90px;'>" + a.HostDesc
                        + "</td><td style='width: 90px;'><input onpaste='return false;' type='number' step='.0001' min='0' max='100' value='" + ConvertRateToPercentage(a.Rate)
                        + "' id='OB_Rate_" + num + "' onchange='CheckInputValue(this.id)' ></td><td style='width: 90px;'><input onpaste='return false;' type='number' step='.0001' min='0' value='" + a.FixedFee
                        + "' id='OB_FixedFee_" + num + "' onchange='CheckInputValue(this.id)' ></td><td id='OB_CurrencyCode_Id_" + num + "' style='width: 90px;'>" + a.CurrencyCode + "</td></tr>";
                });
                html = html + "</div></div>";
            }
            $("#merchantChannel").append(html);
            html = "";
        }
    }
    if (merchantChannel.hasOwnProperty('CC')) {
        if (merchantChannel.CC) {
            if (merchantChannel.CC.length > 0) {
                info = { CC: merchantChannel.CC.length };
                channel.push(info);
                html = html + "<div id='CC'><div class='card-header card-header-divider font-weight-bold'> Credit Card </div><div class='card-body'>";
                html = html + "<div><table id='forOB' class='table table-striped table-hover table-fw-widget'><tr class='odd gradeX'><th>Payment Method</th><th>Channel</th><th>Rate (%)</th><th>Fixed Fee</th><th>Currency</th></tr>";
                (merchantChannel.CC).forEach(function (data, num) {
                    var a = data;
                    html = html + "<tr class='odd gradeX'><td id='CC_method_" + num + "' style='width: 90px;'>" + a.PaymentMethod
                        + "</td><td id='CC_Host_Id_" + num + "' style='display:none'>" + a.HostId + "</td><td style='width: 90px;'>" + a.HostDesc
                        + "</td><td style='width: 90px;'><input onpaste='return false;' type='number' step='.0001' min='0' max='100' value='" + ConvertRateToPercentage(a.Rate)
                        + "' id='CC_Rate_" + num + "' onchange='CheckInputValue(this.id)' ></td><td style='width: 90px;'><input onpaste='return false;' type='number' step='.0001' min='0' value='" + a.FixedFee
                        + "' id='CC_FixedFee_" + num + "' onchange='CheckInputValue(this.id)' ></td><td id='CC_CurrencyCode_Id_" + num + "' style='width: 90px;'>" + a.CurrencyCode + "</td></tr>";
                });
                html = html + "</div></div>";
            }
            $("#merchantChannel").append(html);
            html = "";
        }
    }
    if (merchantChannel.hasOwnProperty('WA')) {
        if (merchantChannel.WA) {
            if (merchantChannel.WA.length > 0) {
                info = { WA: merchantChannel.WA.length };
                channel.push(info);
                html = html + "<div id='WA'><div class='card-header card-header-divider font-weight-bold'> E-Wallet </div><div class='card-body'>";
                html = html + "<div><table id='forWA' class='table table-striped table-hover table-fw-widget'><tr class='odd gradeX'><th>Payment Method</th><th>Channel</th><th>Rate (%)</th><th>Fixed Fee</th><th>Currency</th></tr>";
                (merchantChannel.WA).forEach(function (data, num) {
                    var a = data;
                    html = html + "<tr class='odd gradeX'><td id='WA_method_" + num + "' style='width: 90px;'>" + a.PaymentMethod
                        + "</td><td id='WA_Host_Id_" + num + "' style='display:none'>" + a.HostId + "</td><td style='width: 90px;'>" + a.HostDesc
                        + "</td><td style='width: 90px;'><input onpaste='return false;' type='number' step='.0001' min='0' max='100' value='" + ConvertRateToPercentage(a.Rate)
                        + "' id='WA_Rate_" + num + "' onchange='CheckInputValue(this.id)' ></td><td style='width: 90px;'><input onpaste='return false;' type='number' step='.0001' min='0' value='" + a.FixedFee
                        + "' id='WA_FixedFee_" + num + "' onchange='CheckInputValue(this.id)' ></td><td id='WA_CurrencyCode_Id_" + num + "' style='width: 90px;'>" + a.CurrencyCode + "</td></tr>";
                });
                html = html + "</div></div>";
            }
            $("#merchantChannel").append(html);
            html = "";
        }
    }
    if (merchantChannel.hasOwnProperty('OTC')) {
        if (merchantChannel.OTC) {
            if (merchantChannel.OTC.length > 0) {
                info = { OTC: merchantChannel.OTC.length };
                channel.push(info);
                html = html + "<div id='OTC'><div class='card-header card-header-divider font-weight-bold'> Over the Counter </div><div class='card-body'>";
                html = html + "<div><table class='table table-striped table-hover table-fw-widget'><tr class='odd gradeX'><th>Payment Method</th><th>Channel</th><th>Rate (%)</th><th>Fixed Fee</th><th>Currency</th></tr>";
                (merchantChannel.OTC).forEach(function (data, num) {
                    var a = data;
                    html = html + "<tr class='odd gradeX'><td id='OTC_method_" + num + "' style='width: 90px;'>" + a.PaymentMethod
                        + "</td><td id='OTC_Host_Id_" + num + "' style='display:none'>" + a.HostId + "</td><td style='width: 90px;'>" + a.HostDesc
                        + "</td><td style='width: 90px;'><input onpaste='return false;' type='number' step='.0001' min='0' max='100' value='" + ConvertRateToPercentage(a.Rate)
                        + "' id='OTC_Rate_" + num + "' onchange='CheckInputValue(this.id)' ></td><td style='width: 90px;'><input onpaste='return false;' type='number' step='.0001' min='0' value='" + a.FixedFee
                        + "' id='OTC_FixedFee_" + num + "' onchange='CheckInputValue(this.id)' ></td><td id='OTC_CurrencyCode_Id_" + num + "' style='width: 90px;'>" + a.CurrencyCode + "</td></tr>";
                });
                html = html + "</div></div>";
            }
            $("#merchantChannel").append(html);
            html = "";
        }
    }
    if (merchantChannel.hasOwnProperty('MO')) {
        if (merchantChannel.MO) {
            if (merchantChannel.MO.length > 0) {
                info = { MO: merchantChannel.MO.length };
                channel.push(info);
                html = html + "<dic id='MO'><div class='card-header card-header-divider font-weight-bold'> Motopay </div><div class='card-body'>";
                html = html + "<div><table class='table table-striped table-hover table-fw-widget'><tr class='odd gradeX'><th>Payment Method</th><th>Channel</th><th>Rate (%)</th><th>Fixed Fee</th><th>Currency</th></tr>";
                (merchantChannel.MO).forEach(function (data, num) {
                    var a = data;
                    html = html + "<tr class='odd gradeX'><td id='MO_method_" + num + "' style='width: 90px;'>" + a.PaymentMethod
                        + "</td><td id='MO_Host_Id_" + num + "' style='display:none'>" + a.HostId + "</td><td style='width: 90px;'>" + a.HostDesc
                        + "</td><td style='width: 90px;'><input onpaste='return false;' type='number' step='.0001' min='0' max='100' value='" + ConvertRateToPercentage(a.Rate)
                        + "' id='MO_Rate_" + num + "' onchange='CheckInputValue(this.id)' ></td><td style='width: 90px;'><input onpaste='return false;' type='number' step='.0001' min='0' value='" + a.FixedFee
                        + "' id='MO_FixedFee_" + num + "' onchange='CheckInputValue(this.id)' ></td><td id='MO_CurrencyCode_Id_" + num + "' style='width: 90px;'>" + a.CurrencyCode + "</td></tr>";
                });
                html = html + "</div></div>";
            }
            $("#merchantChannel").append(html);
            html = "";
        }
    }
    $("#Button").prop("disabled", false);
    CheckInputValue();
}

function ConvertRateToPercentage(rate) {
    return (rate*100).toFixed(4);
}

function checkDate(id) {
    var a = new Date(id.value);
    var b = new Date(todaydate);
    if (a < b) {
        id.value = todaydate;
        return false;
    }
    return true;
}

function getChannel() {

    if ($("#DD_merchant").val() > 0) {
        var cc = document.getElementById("CC");
        var Fcc = document.getElementById("FCC");
        var ob = document.getElementById("OB");
        var Fob = document.getElementById("FOB");
        var wa = document.getElementById("WA");
        var Fwa = document.getElementById("FWA");
        var otc = document.getElementById("OTC");
        var Fotc = document.getElementById("FOTC");
        var mo = document.getElementById("MO");
        var Fmo = document.getElementById("FMO");
        if (cc) {
            $("#CC").remove();
            if (Fcc) {$("#FCC").remove();}
        }
        if (ob) {
            $("#OB").remove();
            if (Fob) { $("#FOB").remove(); }
        }
        if (wa) {
            $("#WA").remove();
            if (Fwa) { $("#FWA").remove(); }
        }
        if (otc) {
            $("#OTC").remove();
            if (Fotc) { $("#FOTC").remove(); }
        }
        if (mo) {
            $("#MO").remove();
            if (Fmo) { $("#FMO").remove(); }
        }
        channel=[];
        info = {};
        $("#Button").prop("disabled",true);
        $("#Button1").prop("disabled",true);
        callApi($("#DD_merchant").val());
    }
}

function dataValidation() {
    var JsonObject = [];
    var data = {};
    if (channel.length > 0) {
        channel.forEach(function (list) {
            if (list.hasOwnProperty('CC')) {
                if (list.CC > 0) {
                    for (var num = 0; num < list.CC; num++) {
                        data = { PaymentMethod: $("#CC_method_" + num).html(), HostId: $("#CC_Host_Id_" + num).html(), Rate: $("#CC_Rate_" + num).val(), FixedFee: $("#CC_FixedFee_" + num).val(), CurrencyCode: $("#CC_CurrencyCode_Id_" + num).html() };
                        JsonObject.push(data);
                    }
                }
            }
            else if (list.hasOwnProperty('OB')) {
                if (list.OB > 0) {
                    for (var num = 0; num < list.OB; num++) {
                        data = { PaymentMethod: $("#OB_method_" + num).html(), HostId: $("#OB_Host_Id_" + num).html(), Rate: $("#OB_Rate_" + num).val(), FixedFee: $("#OB_FixedFee_" + num).val(), CurrencyCode: $("#OB_CurrencyCode_Id_" + num).html() };
                        JsonObject.push(data);
                    }
                }
            }
            else if (list.hasOwnProperty('WA')) {
                if (list.WA > 0) {
                    for (var num = 0; num < list.WA; num++) {
                        data = { PaymentMethod: $("#WA_method_" + num).html(), HostId: $("#WA_Host_Id_" + num).html(), Rate: $("#WA_Rate_" + num).val(), FixedFee: $("#WA_FixedFee_" + num).val(), CurrencyCode: $("#WA_CurrencyCode_Id_" + num).html() };
                        JsonObject.push(data);
                    }
                }
            }
            else if (list.hasOwnProperty('OTC')) {
                if (list.OTC > 0) {
                    for (var num = 0; num < list.OTC; num++) {
                        data = { PaymentMethod: $("#OTC_method_" + num).html(), HostId: $("#OTC_Host_Id_" + num).html(), Rate: $("#OTC_Rate_" + num).val(), FixedFee: $("#OTC_FixedFee_" + num).val(), CurrencyCode: $("#OTC_CurrencyCode_Id_" + num).html() };
                        JsonObject.push(data);
                    }
                }
            }
            else if (list.hasOwnProperty('MO')) {
                if (list.MO > 0) {
                    for (var num = 0; num < list.MO; num++) {
                        data = { PaymentMethod: $("#MO_method_" + num).val(), HostId: $("#MO_Host_Id_" + num).val(), Rate: $("#MO_Rate_" + num).val(), FixedFee: $("#MO_FixedFee_" + num).val(), CurrencyCode: $("#MO_CurrencyCode_Id_" + num).val() };
                        JsonObject.push(data);
                    }
                }
            }
        });
        $("#TextBox1").val(JSON.stringify(JsonObject));
    }
}

function futureRateInputValidation(){
    var JsonObject = [];
    var data = {};
    if (channel.length > 0) {
        channel.forEach(function (list) {
            if (list.hasOwnProperty('CC')) {
                if (list.CC > 0) {
                    for (var num = 0; num < list.CC; num++) {
                        if ($("FCC_Rate_" + num).val() != "" && $("#FCC_FixedFee_" + num).val() != "" && $("#FCC_Start_Date_"+ num).val() != "") {
                            data = {
                                PaymentMethod: $("#FCC_method_" + num).html(),
                                HostId: $("#FCC_Host_Id_" + num).html(),
                                Rate: $("#FCC_Rate_" + num).val(),
                                FixedFee: $("#FCC_FixedFee_" + num).val(),
                                StartDate: $("#FCC_Start_Date_" + num).val(),
                                CurrencyCode: $("#FCC_CurrencyCode_Id_" + num).html()
                            };
                            JsonObject.push(data);
                        }
                                
                    }
                }
            }
            else if (list.hasOwnProperty('OB')) {
                if (list.OB > 0) {
                    for (var num = 0; num < list.OB; num++) {
                        if ($("FOB_Rate_" + num).val() != "" && $("#FOB_FixedFee_" + num).val() != "" && $("#FOB_Start_Date_"+ num).val() != "") {
                            data = {
                                PaymentMethod: $("#FOB_method_" + num).html(),
                                HostId: $("#FOB_Host_Id_" + num).html(),
                                Rate: $("#FOB_Rate_" + num).val(),
                                FixedFee: $("#FOB_FixedFee_" + num).val(),
                                StartDate: $("#FOB_Start_Date_" + num).val(),
                                CurrencyCode: $("#FOB_CurrencyCode_Id_" + num).html()
                            };
                            JsonObject.push(data);
                        }
                                
                    }
                }
            }
            else if (list.hasOwnProperty('WA')) {
                if (list.WA > 0) {
                    for (var num = 0; num < list.WA; num++) {
                        if ($("FWA_Rate_" + num).val() != "" && $("#FWA_FixedFee_" + num).val() != "" && $("#FWA_Start_Date_"+ num).val() != "") {
                            data = {
                                PaymentMethod: $("#FWA_method_" + num).html(),
                                HostId: $("#FWA_Host_Id_" + num).html(),
                                Rate: $("#FWA_Rate_" + num).val(),
                                FixedFee: $("#FWA_FixedFee_" + num).val(),
                                StartDate: $("#FWA_Start_Date_" + num).val(),
                                CurrencyCode: $("#FWA_CurrencyCode_Id_" + num).html()
                            };
                            JsonObject.push(data);
                        }
                    }
                }
            }
            else if (list.hasOwnProperty('OTC')) {
                if (list.OTC > 0) {
                    for (var num = 0; num < list.OTC; num++) {
                        if ($("FOTC_Rate_" + num).val() != "" && $("#FOTC_FixedFee_" + num).val() != "" && $("#FOTC_Start_Date_" + num).val() != "") {
                            data = {
                                PaymentMethod: $("#FOTC_method_" + num).html(),
                                HostId: $("#FOTC_Host_Id_" + num).html(),
                                Rate: $("#FOTC_Rate_" + num).val(),
                                FixedFee: $("#FOTC_FixedFee_" + num).val(),
                                StartDate: $("#FOTC_Start_Date_" + num).val(),
                                CurrencyCode: $("#FOTC_CurrencyCode_Id_" + num).html()
                            };
                            JsonObject.push(data);
                        }
                    }
                }
            }
            else if (list.hasOwnProperty('MO')) {
                if (list.MO > 0) {
                    for (var num = 0; num < list.MO; num++) {
                        if ($("FMO_Rate_" + num).val() != "" && $("#FMO_FixedFee_" + num).val() != "" && $("#FMO_Start_Date_" + num).val() != "") {
                            data = {
                                PaymentMethod: $("#FMO_method_" + num).html(),
                                HostId: $("#FMO_Host_Id_" + num).html(),
                                Rate: $("#FMO_Rate_" + num).val(),
                                FixedFee: $("#FMO_FixedFee_" + num).val(),
                                StartDate: $("#FMO_Start_Date_" + num).val(),
                                CurrencyCode: $("#FMO_CurrencyCode_Id_" + num).html()
                            };
                            JsonObject.push(data);
                        }
                    }
                }
            }
        });
        if (JsonObject.length > 0) {
            $("#TextBox2").val(JSON.stringify(JsonObject));
            return true;
        } else {
            return false;
        }
    }
}