﻿<%@ Page Title="DASHBOARD" Language="C#" MasterPageFile="~/MasterPage/DocMasterPage.master" CodeFile="Index.aspx.cs" AutoEventWireup="true" Inherits="Documentation_Home_Index"  %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <p>Portal dashboard after login (Check Reference For Details)</p>
    <img src="../../assets/img/doc_Img/home/dashboard.png" class=" size_img800" />
    <img src="../../assets/img/doc_Img/home/total_revenue.png" class=" size_img800" />
    <img src="../../assets/img/doc_Img/home/times_of_the_day.png" class=" size_img800" />
    <img src="../../assets/img/doc_Img/home/channel_usage.png" class=" size_img800" />
    <h4>Reference</h4>
    <ol>
        <li>Filtered by: <br />
            Merchant: Merchant Account<br />
            Currency: Currency list<br />
            Date Since/To : Date range to select for generate report</li>
        <li>Total Payments Value: Total sales of the day</li>
        <li>Total Transaction(s): Total transactions of the day</li>
        <li>Total Revenue : Summary line chart of the total sales, transactions count and fees</li>
        <li>Time of the day : Summary line chart for sales of the day by hour</li>
        <li>Channel Usage : Channel usage of payment by percentage</li>
    </ol>
    <div id="next_previous" style="margin-top:20px;">
        <a runat="server" href="/Documentation/Home/Menu.aspx" class="next">Next: Menu &raquo;</a>
    </div>
</asp:Content>

