﻿<%@ Page Title="USER ACCOUNT" Language="C#" MasterPageFile="~/MasterPage/DocMasterPage.master" CodeFile="UserAccount.aspx.cs" AutoEventWireup="true" Inherits="Documentation_Home_UserAccount"  %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <p>User account located on dashboard top right. Click on avatar icon <img src="../../assets/img/doc_Img/home/person_icon.png" class="img_info" style="width:30px;margin-top:-1px"/></p><br />
    <img src="../../assets/img/doc_Img/home/menu_person.png" class="img_info" /><br /><br />
    <p><img src="../../assets/img/doc_Img/home/account.png" class="img_info" style="width:80px;margin-top:3px"/> - User account information and password reset</p>
    <p><img src="../../assets/img/doc_Img/home/logout.png" class="img_info" style="width:80px;margin-top:3px"/> - Sign out from portal</p>
    <img src="../../assets/img/doc_Img/user/account_info.png" class="img_info size_img500"/><br /><br />
    <p>For password reset, click on '<span style='color:blue'>Reset Password</span>'</p>
    <img src="../../assets/img/doc_Img/user/account_info_reset_pass.png" class="img_info size_img500"/><br /><br />
    <p>An email will send to User ID. <br /></p>
    <p>Email Example: </p>
    <img src="../../assets/img/doc_Img/user/account_info_temp_pass.png" class="img_info size_img800"/><br />
    <p>Click on the link in email (Step 2) and it will redirect to portal login page.</p><br />
    <p>Login with temporary password received from email.</p><br />
    <p>Follow by enter new password and confirm new password in 'Change Password Page’. Click on update password button.</p><br />
    <img src="../../assets/img/doc_Img/user/forgot_pass_new.png" class="img_info size_img300" />
    <p>You may now login using the new password.</p>

    <div id="next_previous" style="margin-top:30px">
        <a runat="server" href="/Documentation/Home/Menu.aspx" class="previous">&laquo; Previous :  Menu</a>
        <a runat="server" href="/Documentation/Home/ForgotPassword.aspx" class="next">Next: Forgot Password &raquo;</a>
    </div>
</asp:Content>
