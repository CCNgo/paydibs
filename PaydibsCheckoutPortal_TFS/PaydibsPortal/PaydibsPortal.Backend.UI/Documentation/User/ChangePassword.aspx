﻿<%@ Page Title="USER" Language="C#" MasterPageFile="~/MasterPage/DocMasterPage.master" CodeFile="ChangePassword.aspx.cs" AutoEventWireup="true" Inherits="Documentation_User_ChangePassword" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <h4>Change Password</h4>
    <p>You can change your current password for security reasons</p><br />
    <img src="../../assets/img/doc_Img/user/user_menu_change_pass.png" class="img_info size_img300" /><br /><br />
    <p>Enter your current password follow by new password and confirm new password.</p>
    <p>Click on 'Update Password' button. Your next login access will be using new password.</p>
    <img src="../../assets/img/doc_Img/user/user_change_pass.png" class="img_info size_img300" /><br />
    <div id="next_previous" style="margin-top:50px">
        <a runat="server" href="/Documentation/User/ViewUserGroup.aspx" class="previous">&laquo; Previous :  View User Group</a>
        <a runat="server" href="/Documentation/Management/EmailPayment.aspx" class="next">Next: Email Payment &raquo;</a>
    </div>
</asp:Content>

