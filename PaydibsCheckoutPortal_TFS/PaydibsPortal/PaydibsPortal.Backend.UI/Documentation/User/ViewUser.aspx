﻿<%@ Page Title="USER" Language="C#" MasterPageFile="~/MasterPage/DocMasterPage.master" CodeFile="ViewUser.aspx.cs" AutoEventWireup="true" Inherits="Documentation_User_ViewUser" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <h4>View User</h4>
    <p>To view user details.</p><br />
    <img src="../../assets/img/doc_Img/user/view_user_menu.png" class="img_info size_img300" /><br />
    <h4>List User</h4>
    <p>Click on 'Search' button, a list of user will be display</p>
    <img src="../../assets/img/doc_Img/user/user_view.png" class="img_info size_img800" /><br />
    <div id="next_previous" style="margin-top:50px">
        <a runat="server" href="/Documentation/Merchant/ViewResellerRate.aspx" class="previous">&laquo; Previous :  View Reseller Rate</a>
        <a runat="server" href="/Documentation/User/ViewUserGroup.aspx" class="next">Next: View User Group &raquo;</a>
    </div>
</asp:Content>

