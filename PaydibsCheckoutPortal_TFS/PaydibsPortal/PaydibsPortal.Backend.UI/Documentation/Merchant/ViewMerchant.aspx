﻿<%@ Page Title="MERCHANT" Language="C#" MasterPageFile="~/MasterPage/DocMasterPage.master" CodeFile="ViewMerchant.aspx.cs" AutoEventWireup="true" Inherits="Documentation_Merchant_ViewMerchant" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <h4>View Merchant</h4>
    <p>To view merchant details.</p><br />
    <img src="../../assets/img/doc_Img/merchant/view_merchant_menu.png" class="img_info size_img300" /><br />
    <h4>Details</h4>
    <p>Display all details of merchant. </p>
    <img src="../../assets/img/doc_Img/merchant/view_merchant_profile.png" class="img_info size_img800" /><br />
    <img src="../../assets/img/doc_Img/merchant/view_merchant_profile02.png" class="img_info size_img800" /><br />
    <div id="next_previous" style="margin-top:50px">
        <a runat="server" href="/Documentation/Transactions/ResellerMarginReport.aspx" class="previous">&laquo; Previous :  Reseller Margin Report</a>
        <a runat="server" href="/Documentation/Merchant/ViewChannel.aspx" class="next">Next: View Channel  &raquo;</a>
    </div>
</asp:Content>

