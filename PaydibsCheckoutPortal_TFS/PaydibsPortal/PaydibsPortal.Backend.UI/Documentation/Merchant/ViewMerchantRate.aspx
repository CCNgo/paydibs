﻿<%@ Page Title="MERCHANT" Language="C#" MasterPageFile="~/MasterPage/DocMasterPage.master" CodeFile="ViewMerchantRate.aspx.cs" AutoEventWireup="true" Inherits="Documentation_Merchant_ViewMerchantRate" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <h4>View Merchant Rate</h4>
    <p>To view merchant rate details.</p><br />
    <img src="../../assets/img/doc_Img/merchant/merchant_rate_menu.png" class="img_info size_img300" /><br />
    <h4>Details</h4>
    <ul>
        <li>Payment Method : Type of payment method</li>
        <li>Channel: Channel description</li>
        <li>Rate: Rate applied to this channel's transaction</li>
        <li>Fixed Fee: Fixed fee to this channel's transaction</li>
        <li>Currency: Currency of channel</li>
    </ul>
    <img src="../../assets/img/doc_Img/merchant/merchant_rate.png" class="img_info size_img800" /><br />
    <div id="next_previous" style="margin-top:50px">
        <a runat="server" href="/Documentation/Merchant/ViewChannel.aspx" class="previous">&laquo; Previous :  View Channel</a>
        <a runat="server" href="/Documentation/Merchant/ViewResellerRate.aspx" class="next">Next: View Reseller Rate  &raquo;</a>
    </div>
</asp:Content>

