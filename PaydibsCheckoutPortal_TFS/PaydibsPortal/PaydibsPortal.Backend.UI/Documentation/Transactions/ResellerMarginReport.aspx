﻿<%@ Page Title="TRANSACTIONS" Language="C#" CodeFile="ResellerMarginReport.aspx.cs" MasterPageFile="~/MasterPage/DocMasterPage.master" AutoEventWireup="true" Inherits="Documentation_Transactions_ResellerMarginReport" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <h4>Reseller Margin Report</h4>
    <p>Reseller Margin Report Menu only can be viewable by who applied as a Paydibs Reseller</p>
    <p>To view or download margin report. This report contain the details of fees applied on each transactions.</p><br />
    <img src="../../assets/img/doc_Img/transactions/reseller_margin_menu.png" class="img_info size_img300" /><br />
    <h4>Get Report</h4>
    <p>Filtered by:</p>
    <ul>
        <li>Select Merchant Name</li>
        <li>Date range From date and To date</li>
        <li>Currency</li>
    </ul>
    <p>Click on 'Get Report' button to get the records.</p>
    <img src="../../assets/img/doc_Img/transactions/reseller_margin.png" class="img_info size_img800" /><br />
    <h4>Download Report</h4>
    <p>To download report, click on &#39;Download Report&#39; button.</p>
    <p>Report example in Excel format</p>
    <img src="../../assets/img/doc_Img/transactions/reseller_margin_download.png" class="img_info size_img800" /><br />
    <div id="next_previous" style="margin-top:50px">
        <a runat="server" href="/Documentation/Transactions/TransactionsFeeReport.aspx" class="previous">&laquo; Previous :  Transactions Fee Report</a>
        <a runat="server" href="/Documentation/Merchant/ViewMerchant.aspx" class="next">Next: View Merchant  &raquo;</a>
    </div>
</asp:Content>

