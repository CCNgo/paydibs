﻿<%@ Page Title="MERCHANT SERVICES" Language="C#" CodeFile="BuyButton.aspx.cs" MasterPageFile="~/MasterPage/DocMasterPage.master" AutoEventWireup="true" Inherits="Documentation_Management_BuyButton" %>

    <asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
        <h2><%: Title %></h2>
        <h4>Buy Now Botton</h4>
        <p>To generate a payment link, script and QRCode for merchant to place on website.</p>
        <br />
        <img src="../../assets/img/doc_Img/management/buy_button_menu.png" class="img_info size_img300" /><br />
        <h4>Generate Buy Now Botton</h4>
        <p>Enter order details.</p>
        <ul>
            <li>Order Description: Order description</li>
            <li>Order Number: Order number / invoice number / reference number</li>
            <li>Currency: Payment currency</li>
            <li>Amount: Payment amount</li>
        </ul>
        <p>Click on 'Create' button to generate or 'Reset' button to start a new entry</p>
        <img src="../../assets/img/doc_Img/management/buy_button.png" class="img_info size_img800" /><br /><br />
        <p>Payment link, script and QRCode will be generated as below.</p>
        <img src="../../assets/img/doc_Img/management/buy_button_success.png" class="img_info size_img800" /><br />
        <div id="next_previous" style="margin-top:50px">
        <a runat="server" href="/Documentation/Management/EmailPayment.aspx" class="previous">&laquo; Previous :  Email Payment</a>
    </div>
    </asp:Content>

