﻿using System;
using LoggerII;
using System.Web.Configuration;
using System.IO;

/// <summary>
/// Summary description for Common
/// </summary>
public class Common
{
    public Common()
    {
        //
        // TODO: Add constructor logic here
        //

    }


}
public class AlertType
{
    public static string Danger = "alert-danger";
    public static string Success = "alert-success";
    public static string Warning = "alert-warning";
    public static string Default = "alert-default";
    public static string Primary = "alert-primary";
    public static string Info = "alert-info";
}
public class SessionType
{
    public static string CurrentUser = "CurrentUser";
}
public class CurrentUserStat
{
    public CurrentUserStat(string PUserID, int PDomainID)
    {
        UserID = PUserID;
        DomainID = PDomainID;
    }
    public string UserID { get; set; }
    public int DomainID { get; set; }
}

public class Logger
{
    public CLoggerII InitLogger()
    {
        //var objLoggerII = new CLoggerII();

        string szFI = string.Empty;
        string szPymtMethod = string.Empty;
        string szTxnType = string.Empty;
        string szLogFile = string.Empty;
        string szLogPath = string.Empty;
        int iLogLevel = 0;

        try
        {
            szLogPath = WebConfigurationManager.AppSettings["LogPath"];
            iLogLevel = int.Parse(WebConfigurationManager.AppSettings["LogLevel"]);

            if (string.IsNullOrEmpty(szLogPath))
                szLogPath = Environment.CurrentDirectory + Path.DirectorySeparatorChar + "Logs\\";

            if (iLogLevel < 0 || iLogLevel > 5)
                iLogLevel = 3;

            szLogFile = szLogPath;


            szLogFile = szLogFile + ".log";

            return (CLoggerII)CLoggerII.get_GetInstanceX(szLogFile, ((CLoggerII.LOG_SEVERITY)Enum.Parse(typeof(CLoggerII.LOG_SEVERITY), iLogLevel.ToString())), 255);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}