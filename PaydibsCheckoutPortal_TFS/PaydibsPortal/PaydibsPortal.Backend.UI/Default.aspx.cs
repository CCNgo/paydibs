﻿using System;
using PaydibsPortal.BLL;
using LoggerII;
using Newtonsoft.Json;
using System.Security;
using System.Net;

public partial class _Default : System.Web.UI.Page
{
    HomeController hc = new HomeController(0);

    protected void Page_Load(object sender, EventArgs e)
    {
        HiddenField1.Value = "";
        HiddenField2.Value = "";

        Session[SessionType.CurrentUser] = null;
    }

    protected void btn_SignIn_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();
        string str_username = txt_username.Text.Trim();
        //CHG START KENT LOONG 20190325 [SECURE STRING METHOD]
        //string str_password = txt_password.Text.Trim();
        SecureString str_password = new NetworkCredential("", txt_password.Text.Trim()).SecurePassword;
        //CHG E N D KENT LOONG 20190325 [SECURE STRING METHOD]
        string user_ipaddress = Request.UserHostAddress;

        var logObject = new
        {
            Username = str_username,
            IP = user_ipaddress
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            dynamic userID = hc.Do_Check_User_Login(str_username, new NetworkCredential("", str_password).Password, user_ipaddress);
            if (userID == null)
            {
                objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "Login - " + logJson + hc.system_err);
                HiddenField1.Value = hc.err_msg;
                return;
            }
            if (hc.system_err == "UpdatePassword")
            {
                Response.Redirect("UpdatePassword.aspx?uid=" + userID.UserID);
            }

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "Login - " + logJson);

            hc.Do_AddAccessLog(userID.UserID, 0, 100001, 0, 800000, "Login.aspx");

            Session[SessionType.CurrentUser] = new CurrentUserStat(userID.UserID, (int)userID.DomainID);

            hc.Do_Update_User_LastLogin(userID.UserID);
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "Login(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog("", 0, 100001, 0, 800001, "Login.aspx");
        }
        finally
        {
            objLoggerII.Dispose();
            if (Session[SessionType.CurrentUser] != null)
                Response.Redirect("Backoffice/Dashboard.aspx");
        }
    }
}