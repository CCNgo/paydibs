﻿using System;
using PaydibsPortal.BLL;
using LoggerII;
using System.Threading;
using System.Security;
using System.Net;

public partial class UpdatePassword : System.Web.UI.Page
{
    HomeController hc = new HomeController(0);
    string userID = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        userID = Request["uid"];
        if (string.IsNullOrEmpty(userID))
        {
            HiddenField1.Value = "Invalid Request";
            return;
        }
        bool veriRqst = hc.Do_Verify_User(userID);
        if (!veriRqst)
        {
            HiddenField1.Value = hc.err_msg;
            return;
        }
    }

    protected void btn_Update_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        try
        {
            objLoggerII = logger.InitLogger();
            //Modified by KENT LOONG 25 Mar 2019. String to SecureString request by PCI
            //string newPwd = txt_password.Text.Trim();
            //string confirmPwd = txt_confirmPassword.Text.Trim();
            SecureString newPwd = new NetworkCredential("", txt_password.Text.Trim()).SecurePassword;
            SecureString confirmPwd = new NetworkCredential("", txt_confirmPassword.Text.Trim()).SecurePassword;

            bool chkPwd = hc.Do_Check_User_Password(userID, new NetworkCredential("", newPwd).Password);
            if (!chkPwd)
            {
                HiddenField1.Value = hc.err_msg;
                if (hc.system_err == string.Empty)
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "UpdatePassword(Error) - " + userID + hc.system_err.ToString());
                return;
            }

            bool updPwd = hc.Do_Update_User_Password(userID, new NetworkCredential("", newPwd).Password);
            if (!updPwd)
            {
                HiddenField1.Value = hc.err_msg;
                if (hc.system_err == string.Empty)
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "UpdatePassword(Error) - " + userID + hc.system_err.ToString());
                return;
            }

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "UpdatePassword - " + userID);

            hc.Do_AddAccessLog(userID, 0, 100002, 0, 800000, "UpdatePassword.aspx");

            hc.Do_Update_User_LastLogin(userID);

            HiddenField1.Value = "You have successfully update your password! You may login with the new password now.";
            HiddenField2.Value = AlertType.Success;

            Response.AddHeader("REFRESH", "3;URL=../Default.aspx");
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "UpdatePassword(Error) - " + userID + ex.ToString());
            hc.Do_AddAccessLog(userID, 0, 100002, 0, 800001, "UpdatePassword.aspx");
        }
        finally
        {
            objLoggerII.Dispose();
        }
    }
}