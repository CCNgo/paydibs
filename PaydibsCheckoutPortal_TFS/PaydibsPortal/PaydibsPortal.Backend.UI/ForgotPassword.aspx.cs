﻿using System;
using PaydibsPortal.BLL;
using LoggerII;
using Newtonsoft.Json;

public partial class ForgotPassword : System.Web.UI.Page
{
    HomeController hc = new HomeController(0);
    protected void Page_Load(object sender, EventArgs e)
    {
        HiddenField1.Value = "";
        HiddenField2.Value = "";
    }

    protected void btn_forgotPassword_Click(object sender, EventArgs e)
    {
        Logger logger = new Logger();
        CLoggerII objLoggerII = new CLoggerII();

        string userID = txt_username.Text.Trim();

        var logObject = new
        {
            Username = userID
        };
        var logJson = JsonConvert.SerializeObject(logObject);

        try
        {
            objLoggerII = logger.InitLogger();

            bool rr = hc.Do_User_Request_Reset_Pwd(userID);
            if (!rr)
            {
                HiddenField1.Value = hc.err_msg;
                if (hc.system_err == string.Empty)
                    objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "UpdatePassword(Error) - " + logJson + hc.system_err.ToString());
                return;
            }
            HiddenField1.Value = "You have successfully request to reset yout password! We will send you an email soon.";
            HiddenField2.Value = AlertType.Success;

            objLoggerII.Log(CLoggerII.LOG_SEVERITY.INFORMATION, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "ForgotPassword - " + logJson);
            hc.Do_AddAccessLog(userID, 0, 100002, 0, 800000, "Forgot Password.aspx");
        }
        catch (Exception ex)
        {
            objLoggerII.Log(CLoggerII.LOG_SEVERITY.CRITICAL, DebugInfo.__FILE__(), DebugInfo.__METHOD__(), DebugInfo.__LINE__(),
                               "ForgotPassword(Error) - " + logJson + ex.ToString());
            hc.Do_AddAccessLog("", 0, 100002, 0, 800001, "Forgot Password.aspx");
        }
        finally
        {
            objLoggerII.Dispose();
        }

    }
}