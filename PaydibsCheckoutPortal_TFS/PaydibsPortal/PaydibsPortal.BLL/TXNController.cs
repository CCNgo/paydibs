﻿using PaydibsPortal.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web.Script.Serialization;

namespace PaydibsPortal.BLL
{
    public class TXNController
    {
        PGAdminDAL pgad = new PGAdminDAL();

        public string system_err = string.Empty;
        public string err_msg = string.Empty;

        //Properties
        int _DomainID { get; set; }

        public TXNController(int PDomainID)
        {
            _DomainID = PDomainID;
        }

        //Modified by DANIEL 26 Nov 2018. Add last parameter PMode . 0 for Default
        //public TxnRate_Transaction[] Do_TransactionReport(DateTime PSince, DateTime PTo, int PDomainID, bool PSuccess, bool PFailed, bool PPending, bool POther, 
        //    string POBPendingStatus, string POBOtherStatus, string PPGPendingStatus, string PPGOtherStatus, string PPaymentID, string PGatewayTxnID, 
        //    string PCustEmail, string POrderID, string PChannelType, string PChannel, string PCurrency)
        //Modified by DANIEL 29 Nov 2018. Change TxnRate_Transaction to dynamic type
        //public dynamic[] Do_TransactionReport(DateTime PSince, DateTime PTo, int PDomainID, bool PSuccess, bool PFailed, bool PPending, bool POther,
        //    string POBPendingStatus, string POBOtherStatus, string PPGPendingStatus, string PPGOtherStatus, string PPaymentID, string PGatewayTxnID,
        //    string PCustEmail, string POrderID, string PChannelType, string PChannel, string PCurrency, int PMode = 0)
        public dynamic[] Do_TransactionReport(DateTime PSince, DateTime PTo, int PDomainID, bool PSuccess, bool PFailed, bool PPending, bool PRefunded, bool POther,
            string POBRefundedStatus, string POBPendingStatus, string POBOtherStatus, string PPGRefundedStatus, string PPGPendingStatus, string PPGOtherStatus, string PPaymentID, string PGatewayTxnID,
            string PCustEmail, string POrderID, string PChannelType, string PChannel, string PCurrency, int PMode = 0)
        // PMode = 0 Reseller and Sub Merchant , 1 = Reseller only, 2 = ALL Submerchant Transaction and per Submerchant Transaction
        {
            try
            {
                List<int> status_L = new List<int>();
                if (PSuccess) status_L.Add(0);
                if (PFailed) status_L.Add(1);
                if (PRefunded) status_L.Add(10);

                //Modified by DANIEL 29 Nov 2018. Change TxnRate_Transaction to dynamic type
                //TxnRate_Transaction[] Data_Suc = null;
                //TxnRate_Transaction[] Data_Other = null;
                //TxnRate_Transaction[] Data_Pending = null;
                dynamic[] Data_Suc = null;
                dynamic[] Data_Other = null;
                dynamic[] Data_Pending = null;
                //dynamic[] Data_Refuneded = null; not using

                //Added by DANIEl 29 Nov 2018. Declare payment method variable [All, OB, CC, WA]
                string OB = "'OB ','WA '";
                string ALL = "'CC ','OB ','WA '";
                //string CC = "'CC '";      Commented by DANIEL 4 Jan 2019


                if (PDomainID == 1)
                {
                    //Modified by DANIEL 29 Nov 2018. return Data_Suc type to TxnRate_Transaction - START
                    //if ((PSuccess) || (PFailed))
                    //    Data_Suc = pgad.TB_TxnRate_SelectAll_Transaction("", PSince, PTo, status_L.ToArray(), PPaymentID, PGatewayTxnID,
                    //        PCustEmail, POrderID, PChannelType, PChannel, PCurrency);
                    //if (POther)
                    //    Data_Other = pgad.Rep_TBPayTxnRef_TBPayRes("", PSince, PTo, POBOtherStatus, PPGOtherStatus, PPaymentID, PGatewayTxnID,
                    //        PCustEmail, POrderID, PChannelType, PChannel, PCurrency);
                    //if (PPending)
                    //    Data_Pending = pgad.Rep_TBPayReq("", PSince, PTo, POBPendingStatus, PPGPendingStatus, PPaymentID, PGatewayTxnID,
                    //        PCustEmail, POrderID, PChannelType, PChannel, PCurrency);

                    if ((PSuccess) || (PFailed) || (PRefunded))
                    {
                        Data_Suc = pgad.TB_TxnRate_SelectAll_Transaction("", PSince, PTo, status_L.ToArray(), PPaymentID, PGatewayTxnID,
                                                   PCustEmail, POrderID, PChannelType, PChannel, PCurrency);

                        //Added by DANIEL 4 Jan 2019. Retrieve PGAdmin error message if any - START
                        system_err = pgad.system_err;
                        if (!string.IsNullOrEmpty(system_err))
                        {
                            err_msg = "An error occured. Please contact administrator.";
                            return null;
                        }
                        //Added by DANIEL 4 Jan 2019. Retrieve PGAdmin error message if any - END

                        Data_Suc = Data_Suc
                                                   .Select(x => new TxnRate_Transaction
                                                   {
                                                       DateCreated = (DateTime)x.DateCreated,
                                                       MerchantID = (string)x.MerchantID,
                                                       OrderNumber = (string)x.OrderNumber,
                                                       PaymentID = (string)x.PaymentID,
                                                       GatewayTxnID = (string)x.GatewayTxnID,
                                                       BankRefNo = (string)x.BankRefNo,
                                                       CurrencyCode = (string)x.CurrencyCode,
                                                       TxnAmt = (decimal)x.TxnAmt,
                                                       Status = (string)x.Status,
                                                       Channel = GetCCChannelName((string)x.CreditCardNo, (string)x.Channel),
                                                       CustEmail = (string)x.CustEmail,
                                                       CustName = (string)x.CustName,
                                                       CustPhone = (string)x.CustPhone,
                                                       AuthCode = (string)x.AuthCode,
                                                       ECI = (string)x.ECI, // Added By Daniel 30 Apr 2019. Add ECI column field.
                                                       CreditCardNo = DecryptAES256((string)x.CreditCardNo),
                                                       //ADD START DANIEl 20190102 [Add new field TradingName]
                                                       TradingName = (string)x.TradingName
                                                       //ADD START DANIEl 20190102 [Add new field TradingName]
                                                   }).ToArray();
                    }

                    if (POther)
                    {
                        PTo = PTo.AddDays(1);
                        Data_Other = pgad.Rep_TBPayTxnRef_TBPayRes("", PSince, PTo, POBOtherStatus, PPGOtherStatus, PPaymentID, PGatewayTxnID,
                            PCustEmail, POrderID, PChannelType, PChannel, PCurrency);

                        //Added by DANIEL 4 Jan 2019. Retrieve PGAdmin error message if any - START
                        system_err = pgad.system_err;
                        if (!string.IsNullOrEmpty(system_err))
                        {
                            err_msg = "An error occured. Please contact administrator.";
                            return null;
                        }
                        //Added by DANIEL 4 Jan 2019. Retrieve PGAdmin error message if any - END

                        Data_Other = Data_Other
                                                   .Select(x => new TxnRate_Transaction
                                                   {
                                                       DateCreated = (DateTime)x.DateCreated,
                                                       MerchantID = (string)x.MerchantID,
                                                       OrderNumber = (string)x.OrderNumber,
                                                       PaymentID = (string)x.PaymentID,
                                                       GatewayTxnID = (string)x.GatewayTxnID,
                                                       BankRefNo = (string)x.BankRefNo,
                                                       CurrencyCode = (string)x.CurrencyCode,
                                                       TxnAmt = (decimal)x.TxnAmt,
                                                       Status = (string)x.Status,
                                                       Channel = GetCCChannelName((string)x.CreditCardNo, (string)x.Channel),
                                                       CustEmail = (string)x.CustEmail,
                                                       CustName = (string)x.CustName,
                                                       CustPhone = (string)x.CustPhone,
                                                       AuthCode = (string)x.AuthCode,
                                                       ECI = (string)x.ECI, // Added By Daniel 30 Apr 2019. Add ECI column field.
                                                       CreditCardNo = DecryptAES256((string)x.CreditCardNo),
                                                       //ADD START DANIEl 20190102 [Add new field TradingName]
                                                       TradingName = (string)x.TradingName
                                                       //ADD START DANIEl 20190102 [Add new field TradingName]
                                                   }).ToArray();
                    }

                    if (PPending)
                    {
                        Data_Pending = pgad.Rep_TBPayReq("", PSince, PTo, POBPendingStatus, PPGPendingStatus, PPaymentID, PGatewayTxnID,
                           PCustEmail, POrderID, PChannelType, PChannel, PCurrency);

                        ///Added by DANIEL 4 Jan 2019. Retrieve PGAdmin error message if any - START
                        system_err = pgad.system_err;
                        if (!string.IsNullOrEmpty(system_err))
                        {
                            err_msg = "An error occured. Please contact administrator.";
                            return null;
                        }
                        //Added by DANIEL 4 Jan 2019. Retrieve PGAdmin error message if any - END

                        Data_Pending = Data_Pending
                                                   .Select(x => new TxnRate_Transaction
                                                   {
                                                       DateCreated = (DateTime)x.DateCreated,
                                                       MerchantID = (string)x.MerchantID,
                                                       OrderNumber = (string)x.OrderNumber,
                                                       PaymentID = (string)x.PaymentID,
                                                       GatewayTxnID = (string)x.GatewayTxnID,
                                                       BankRefNo = (string)x.BankRefNo,
                                                       CurrencyCode = (string)x.CurrencyCode,
                                                       TxnAmt = (decimal)x.TxnAmt,
                                                       Status = (string)x.Status,
                                                       Channel = GetCCChannelName((string)x.CreditCardNo, (string)x.Channel),
                                                       CustEmail = (string)x.CustEmail,
                                                       CustName = (string)x.CustName,
                                                       CustPhone = (string)x.CustPhone,
                                                       AuthCode = (string)x.AuthCode,
                                                       ECI = (string)x.ECI, // Added By Daniel 30 Apr 2019. Add ECI column field.
                                                       CreditCardNo = DecryptAES256((string)x.CreditCardNo),
                                                       TradingName = (string)x.TradingName  // Added by DANIEL 2 Jan 2019
                                                   }).ToArray();
                    }
                    //Modified by DANIEL 29 Nov 2018. return Data_Suc type to TxnRate_Transaction - END
                }
                else
                {
                    var domain_D = pgad.Get_CMS_Domain(1, PDomainID, " ");
                    if (domain_D == null)
                    {
                        err_msg = "Invalid Merchant ID";
                        return null;
                    }
                    else
                    {
                        //Modified by DANIEL 29 Nov 20181129. return Data_Suc, Data_Other, Data_Pending type to different models according to PMode and PChannel - START
                        //if ((PSuccess) || (PFailed))
                        //    //CHG START DANIEL 20181127 [Pass PMode parameter to TB_TxnRate_SelectAll_Transaction method]
                        //    //Data_Suc = pgad.TB_TxnRate_SelectAll_Transaction(domain_D[0].DomainShortName, PSince, PTo, status_L.ToArray(), PPaymentID, PGatewayTxnID,
                        //    //PCustEmail, POrderID, PChannelType, PChannel, PCurrency);
                        //    Data_Suc = pgad.TB_TxnRate_SelectAll_Transaction(domain_D[0].DomainShortName, PSince, PTo, status_L.ToArray(), PPaymentID, PGatewayTxnID,
                        //        PCustEmail, POrderID, PChannelType, PChannel, PCurrency, PMode);
                        ////CHG END DANIEL 20181127 [Pass PMode parameter to TB_TxnRate_SelectAll_Transaction method]
                        //if (POther)
                        //{
                        //    Data_Other = pgad.Rep_TBPayTxnRef_TBPayRes(domain_D[0].DomainShortName, PSince, PTo, POBOtherStatus, PPGOtherStatus, PPaymentID, PGatewayTxnID,
                        //    PCustEmail, POrderID, PChannelType, PChannel, PCurrency);
                        //}
                        //if (PPending)
                        //    Data_Pending = pgad.Rep_TBPayReq(domain_D[0].DomainShortName, PSince, PTo, POBPendingStatus, PPGPendingStatus, PPaymentID, PGatewayTxnID,
                        //    PCustEmail, POrderID, PChannelType, PChannel, PCurrency);

                        if ((PSuccess) || (PFailed) || (PRefunded))
                        {
                            Data_Suc = pgad.TB_TxnRate_SelectAll_Transaction(domain_D[0].DomainShortName, PSince, PTo, status_L.ToArray(), PPaymentID, PGatewayTxnID,
                                PCustEmail, POrderID, PChannelType, PChannel, PCurrency, PMode);

                            //Added by DANIEL 4 Jan 2019. Retrieve PGAdmin error message if any - START
                            system_err = pgad.system_err;
                            if (!string.IsNullOrEmpty(system_err))
                            {
                                err_msg = "An error occured. Please contact administrator.";
                                return null;
                            }
                            //Added by DANIEL 4 Jan 2019. Retrieve PGAdmin error message if any - END

                            if (PMode != 2)
                            {
                                if (PChannelType.Equals(ALL) || PChannelType.Equals(OB))
                                {
                                    Data_Suc = Data_Suc
                                                         .Select(x => new TxnRate_Transaction_No_CC
                                                         {
                                                             DateCreated = (DateTime)x.DateCreated,
                                                             MerchantID = (string)x.MerchantID,
                                                             OrderNumber = (string)x.OrderNumber,
                                                             PaymentID = (string)x.PaymentID,
                                                             GatewayTxnID = (string)x.GatewayTxnID,
                                                             BankRefNo = (string)x.BankRefNo,
                                                             CurrencyCode = (string)x.CurrencyCode,
                                                             TxnAmt = (decimal)x.TxnAmt,
                                                             Status = (string)x.Status,
                                                             Channel = GetCCChannelName((string)x.CreditCardNo, (string)x.Channel),
                                                             CustEmail = (string)x.CustEmail,
                                                             CustName = (string)x.CustName,
                                                             CustPhone = (string)x.CustPhone,
                                                             TradingName = (string)x.TradingName    //Added by DANIEL 2 Jan 2019
                                                         }).ToArray();
                                }
                                else
                                {
                                    Data_Suc = Data_Suc
                                                .Select(x => new TxnRate_Transaction
                                                {
                                                    DateCreated = (DateTime)x.DateCreated,
                                                    MerchantID = (string)x.MerchantID,
                                                    OrderNumber = (string)x.OrderNumber,
                                                    PaymentID = (string)x.PaymentID,
                                                    GatewayTxnID = (string)x.GatewayTxnID,
                                                    BankRefNo = (string)x.BankRefNo,
                                                    CurrencyCode = (string)x.CurrencyCode,
                                                    TxnAmt = (decimal)x.TxnAmt,
                                                    Status = (string)x.Status,
                                                    Channel = GetCCChannelName((string)x.CreditCardNo, (string)x.Channel),
                                                    CustEmail = (string)x.CustEmail,
                                                    CustName = (string)x.CustName,
                                                    CustPhone = (string)x.CustPhone,
                                                    AuthCode = (string)x.AuthCode,
                                                    ECI = (string)x.ECI, // Added By Daniel 30 Apr 2019. Add ECI column field.
                                                    CreditCardNo = DecryptAES256((string)x.CreditCardNo),
                                                    TradingName = (string)x.TradingName     //Added by DANIEL 2 Jan 2019
                                                }).ToArray();
                                }
                            }
                            else
                            {
                                if (PChannelType.Equals(ALL) || PChannelType.Equals(OB))
                                {
                                    Data_Suc = Data_Suc
                                                         .Select(x => new TxnRate_Transaction_No_Cust_CC
                                                         {
                                                             DateCreated = (DateTime)x.DateCreated,
                                                             MerchantID = (string)x.MerchantID,
                                                             OrderNumber = (string)x.OrderNumber,
                                                             PaymentID = (string)x.PaymentID,
                                                             GatewayTxnID = (string)x.GatewayTxnID,
                                                             BankRefNo = (string)x.BankRefNo,
                                                             CurrencyCode = (string)x.CurrencyCode,
                                                             TxnAmt = (decimal)x.TxnAmt,
                                                             Status = (string)x.Status,
                                                             Channel = (string)x.Channel,
                                                             TradingName = (string)x.TradingName        //Added by DANIEL 2 Jan 2019
                                                         }).ToArray();
                                }
                                else
                                {
                                    Data_Suc = Data_Suc
                                                   .Select(x => new TxnRate_Transaction_No_Cust
                                                   {
                                                       DateCreated = (DateTime)x.DateCreated,
                                                       MerchantID = (string)x.MerchantID,
                                                       OrderNumber = (string)x.OrderNumber,
                                                       PaymentID = (string)x.PaymentID,
                                                       GatewayTxnID = (string)x.GatewayTxnID,
                                                       BankRefNo = (string)x.BankRefNo,
                                                       CurrencyCode = (string)x.CurrencyCode,
                                                       TxnAmt = (decimal)x.TxnAmt,
                                                       Status = (string)x.Status,
                                                       Channel = GetCCChannelName((string)x.CreditCardNo, (string)x.Channel),
                                                       AuthCode = (string)x.AuthCode,
                                                       ECI = (string)x.ECI, // Added By Daniel 30 Apr 2019. Add ECI column field.
                                                       CreditCardNo = DecryptAES256((string)x.CreditCardNo),
                                                       TradingName = (string)x.TradingName      //Added by DANIEL 2 Jan 2019
                                                   }).ToArray();
                                }
                            }
                        }
                        if (POther)
                        {
                            PTo = PTo.AddDays(1);
                            Data_Other = pgad.Rep_TBPayTxnRef_TBPayRes(domain_D[0].DomainShortName, PSince, PTo, POBOtherStatus, PPGOtherStatus, PPaymentID, PGatewayTxnID,
                            PCustEmail, POrderID, PChannelType, PChannel, PCurrency, PMode);

                            //Added by DANIEL 4 Jan 2019. Retrieve PGAdmin error message if any - START
                            system_err = pgad.system_err;
                            if (!string.IsNullOrEmpty(system_err))
                            {
                                err_msg = "An error occured. Please contact administrator.";
                                return null;
                            }
                            //Added by DANIEL 4 Jan 2019. Retrieve PGAdmin error message if any - END

                            if (PMode != 2)
                            {
                                if (PChannelType.Equals(ALL) || PChannelType.Equals(OB))
                                {
                                    Data_Other = Data_Other
                                                         .Select(x => new TxnRate_Transaction_No_CC
                                                         {
                                                             DateCreated = (DateTime)x.DateCreated,
                                                             MerchantID = (string)x.MerchantID,
                                                             OrderNumber = (string)x.OrderNumber,
                                                             PaymentID = (string)x.PaymentID,
                                                             GatewayTxnID = (string)x.GatewayTxnID,
                                                             BankRefNo = (string)x.BankRefNo,
                                                             CurrencyCode = (string)x.CurrencyCode,
                                                             TxnAmt = (decimal)x.TxnAmt,
                                                             Status = (string)x.Status,
                                                             Channel = (string)x.Channel,
                                                             CustEmail = (string)x.CustEmail,
                                                             CustName = (string)x.CustName,
                                                             CustPhone = (string)x.CustPhone,
                                                             TradingName = (string)x.TradingName        //Added by DANIEL 2 Jan 2019
                                                         }).ToArray();
                                }
                                else
                                {
                                    Data_Other = Data_Other
                                                .Select(x => new TxnRate_Transaction
                                                {
                                                    DateCreated = (DateTime)x.DateCreated,
                                                    MerchantID = (string)x.MerchantID,
                                                    OrderNumber = (string)x.OrderNumber,
                                                    PaymentID = (string)x.PaymentID,
                                                    GatewayTxnID = (string)x.GatewayTxnID,
                                                    BankRefNo = (string)x.BankRefNo,
                                                    CurrencyCode = (string)x.CurrencyCode,
                                                    TxnAmt = (decimal)x.TxnAmt,
                                                    Status = (string)x.Status,
                                                    Channel = GetCCChannelName((string)x.CreditCardNo, (string)x.Channel),
                                                    CustEmail = (string)x.CustEmail,
                                                    CustName = (string)x.CustName,
                                                    CustPhone = (string)x.CustPhone,
                                                    AuthCode = (string)x.AuthCode,
                                                    ECI = (string)x.ECI,  // Added By Daniel 30 Apr 2019. Add ECI column field.
                                                    CreditCardNo = DecryptAES256((string)x.CreditCardNo),
                                                    TradingName = (string)x.TradingName     //Added by DANIEL 2 Jan 2019
                                                }).ToArray();
                                }
                            }
                            else
                            {
                                if (PChannelType.Equals(ALL) || PChannelType.Equals(OB))
                                {
                                    Data_Other = Data_Other
                                                         .Select(x => new TxnRate_Transaction_No_Cust_CC
                                                         {
                                                             DateCreated = (DateTime)x.DateCreated,
                                                             MerchantID = (string)x.MerchantID,
                                                             OrderNumber = (string)x.OrderNumber,
                                                             PaymentID = (string)x.PaymentID,
                                                             GatewayTxnID = (string)x.GatewayTxnID,
                                                             BankRefNo = (string)x.BankRefNo,
                                                             CurrencyCode = (string)x.CurrencyCode,
                                                             TxnAmt = (decimal)x.TxnAmt,
                                                             Status = (string)x.Status,
                                                             Channel = (string)x.Channel,
                                                             TradingName = (string)x.TradingName        //Added by DANIEL 2 Jan 2019
                                                         }).ToArray();
                                }
                                else
                                {
                                    Data_Other = Data_Other
                                                   .Select(x => new TxnRate_Transaction_No_Cust
                                                   {
                                                       DateCreated = (DateTime)x.DateCreated,
                                                       MerchantID = (string)x.MerchantID,
                                                       OrderNumber = (string)x.OrderNumber,
                                                       PaymentID = (string)x.PaymentID,
                                                       GatewayTxnID = (string)x.GatewayTxnID,
                                                       BankRefNo = (string)x.BankRefNo,
                                                       CurrencyCode = (string)x.CurrencyCode,
                                                       TxnAmt = (decimal)x.TxnAmt,
                                                       Status = (string)x.Status,
                                                       Channel = GetCCChannelName((string)x.CreditCardNo, (string)x.Channel),
                                                       AuthCode = (string)x.AuthCode,
                                                       ECI = (string)x.ECI,  // Added By Daniel 30 Apr 2019. Add ECI column field.
                                                       CreditCardNo = DecryptAES256((string)x.CreditCardNo),
                                                       TradingName = (string)x.TradingName      //Added by DANIEL 4 Jan 2019
                                                   }).ToArray();
                                }
                            }
                        }
                        if (PPending)
                        {
                            Data_Pending = pgad.Rep_TBPayReq(domain_D[0].DomainShortName, PSince, PTo, POBPendingStatus, PPGPendingStatus, PPaymentID, PGatewayTxnID,
                            PCustEmail, POrderID, PChannelType, PChannel, PCurrency, PMode);

                            //Added by DANIEL 4 Jan 2019. Retrieve PGAdmin error message if any - START
                            system_err = pgad.system_err;
                            if (!string.IsNullOrEmpty(system_err))
                            {
                                err_msg = "An error occured. Please contact administrator.";
                                return null;
                            }
                            //Added by DANIEL 4 Jan 2019. Retrieve PGAdmin error message if any - END

                            if (PMode != 2)
                            {
                                //if (PChannelType.Equals(ALL) || PChannelType.Equals(CC))      Modified by DANIEL 3 Jan 2019. Change CC to OB
                                if (PChannelType.Equals(ALL) || PChannelType.Equals(OB))
                                {
                                    Data_Pending = Data_Pending
                                                         .Select(x => new TxnRate_Transaction_No_CC
                                                         {
                                                             DateCreated = (DateTime)x.DateCreated,
                                                             MerchantID = (string)x.MerchantID,
                                                             OrderNumber = (string)x.OrderNumber,
                                                             PaymentID = (string)x.PaymentID,
                                                             GatewayTxnID = (string)x.GatewayTxnID,
                                                             BankRefNo = (string)x.BankRefNo,
                                                             CurrencyCode = (string)x.CurrencyCode,
                                                             TxnAmt = (decimal)x.TxnAmt,
                                                             Status = (string)x.Status,
                                                             Channel = GetCCChannelName((string)x.CreditCardNo, (string)x.Channel),
                                                             CustEmail = (string)x.CustEmail,
                                                             CustName = (string)x.CustName,
                                                             CustPhone = (string)x.CustPhone,
                                                             TradingName = (string)x.TradingName        //Added by DANIEL 2 Jan 2019
                                                         }).ToArray();
                                }
                                else
                                {
                                    Data_Pending = Data_Pending
                                                .Select(x => new TxnRate_Transaction
                                                {
                                                    DateCreated = (DateTime)x.DateCreated,
                                                    MerchantID = (string)x.MerchantID,
                                                    OrderNumber = (string)x.OrderNumber,
                                                    PaymentID = (string)x.PaymentID,
                                                    GatewayTxnID = (string)x.GatewayTxnID,
                                                    BankRefNo = (string)x.BankRefNo,
                                                    CurrencyCode = (string)x.CurrencyCode,
                                                    TxnAmt = (decimal)x.TxnAmt,
                                                    Status = (string)x.Status,
                                                    Channel = GetCCChannelName((string)x.CreditCardNo, (string)x.Channel),
                                                    CustEmail = (string)x.CustEmail,
                                                    CustName = (string)x.CustName,
                                                    CustPhone = (string)x.CustPhone,
                                                    AuthCode = (string)x.AuthCode,
                                                    ECI = (string)x.ECI,  // Added By Daniel 30 Apr 2019. Add ECI column field.
                                                    CreditCardNo = DecryptAES256((string)x.CreditCardNo),
                                                    TradingName = (string)x.TradingName     //Added by DANIEL 2 Jan 2019
                                                }).ToArray();
                                }
                            }
                            else
                            {
                                if (PChannelType.Equals(ALL) || PChannelType.Equals(OB))
                                {
                                    Data_Pending = Data_Pending
                                                         .Select(x => new TxnRate_Transaction_No_Cust_CC
                                                         {
                                                             DateCreated = (DateTime)x.DateCreated,
                                                             MerchantID = (string)x.MerchantID,
                                                             OrderNumber = (string)x.OrderNumber,
                                                             PaymentID = (string)x.PaymentID,
                                                             GatewayTxnID = (string)x.GatewayTxnID,
                                                             BankRefNo = (string)x.BankRefNo,
                                                             CurrencyCode = (string)x.CurrencyCode,
                                                             TxnAmt = (decimal)x.TxnAmt,
                                                             Status = (string)x.Status,
                                                             Channel = (string)x.Channel,
                                                             TradingName = (string)x.TradingName        //Added by DANIEL 2 Jan 2019
                                                         }).ToArray();
                                }
                                else
                                {
                                    Data_Pending = Data_Pending
                                                   .Select(x => new TxnRate_Transaction_No_Cust
                                                   {
                                                       DateCreated = (DateTime)x.DateCreated,
                                                       MerchantID = (string)x.MerchantID,
                                                       OrderNumber = (string)x.OrderNumber,
                                                       PaymentID = (string)x.PaymentID,
                                                       GatewayTxnID = (string)x.GatewayTxnID,
                                                       BankRefNo = (string)x.BankRefNo,
                                                       CurrencyCode = (string)x.CurrencyCode,
                                                       TxnAmt = (decimal)x.TxnAmt,
                                                       Status = (string)x.Status,
                                                       Channel = GetCCChannelName((string)x.CreditCardNo, (string)x.Channel),
                                                       AuthCode = (string)x.AuthCode,
                                                       ECI = (string)x.ECI, // Added By Daniel 30 Apr 2019. Add ECI column field.
                                                       CreditCardNo = DecryptAES256((string)x.CreditCardNo),
                                                       TradingName = (string)x.TradingName      //Added by DANIEL 2 Jan 2019
                                                   }).ToArray();
                                }
                            }
                        }
                        //Modified by DANIEL 29 Nov 20181129. return Data_Suc, Data_Other, Data_Pending type to different models according to PMode and PChannel - END 
                    }
                }
                if ((Data_Suc != null) && (Data_Other != null) && (Data_Pending != null))
                {
                    var mergedList = Data_Suc.Union(Data_Pending).ToArray();
                    mergedList = mergedList.Union(Data_Other).ToArray();
                    mergedList.OrderBy(p => p.DateCreated);
                    return mergedList;
                }
                else if ((Data_Suc != null) && (Data_Other != null))
                {
                    var mergedList = Data_Suc.Union(Data_Other).ToArray();
                    mergedList.OrderBy(p => p.DateCreated);
                    return mergedList;
                }
                else if ((Data_Suc != null) && (Data_Pending != null))
                {
                    var mergedList = Data_Suc.Union(Data_Pending).ToArray();
                    mergedList.OrderBy(p => p.DateCreated);
                    return mergedList;
                }
                else if ((Data_Pending != null) && (Data_Other != null))
                {
                    var mergedList = Data_Pending.Union(Data_Other).ToArray();
                    mergedList.OrderBy(p => p.DateCreated);
                    return mergedList;
                }
                else if (Data_Suc != null)
                {
                    return Data_Suc;
                }
                else if (Data_Pending != null)
                {
                    return Data_Pending;
                }
                else
                    return Data_Other;
            }
            catch (Exception ex)
            {
                err_msg = ex.Message;
                system_err = ex.Message.ToString();
                return null;
            }
        }

        //Added by DANIEL 17 Jan 2019. Add method Do_TransactionsFeeReport to get data
        //public Finance_Report[] Do_TransactionsFeeReport(DateTime PSince, DateTime PTo, int PDomainID)        Modified by DANIEL 13 Mar 2019. Add parameter PCurrencyCode
        //public Finance_Report[] Do_TransactionsFeeReport(DateTime PSince, DateTime PTo, int PDomainID, string PCurrencyCode = "")
        public TxnFeeReport[] Do_TransactionsFeeReport(DateTime PSince, DateTime PTo, int PDomainID, string PCurrencyCode = "") //Modified by Daniel 12 Apr 2019. Change model TxnFeeReport
        {
            try
            {
                TxnFeeReport[] Data_Suc = null;

                if (PDomainID == 1)
                {
                    //Data_Suc = pgad.TB_TxnRate_GetFinanceReport("", PSince, PTo, PCurrencyCode);        //Modified by DANIEL 13 Mar 2019. Add parameter PCurrencyCode
                    Data_Suc = pgad.TxnFeeReport("", PSince, PTo, PCurrencyCode);
                    system_err = pgad.system_err;
                    if (system_err != string.Empty)
                    {
                        err_msg = "An error occured. Please contact administrator.";
                        return null;
                    }
                }
                else
                {
                    var domain_D = pgad.Get_CMS_Domain(1, PDomainID, " ");

                    system_err = pgad.system_err;
                    if (system_err != string.Empty)
                        return null;

                    if (domain_D == null)
                    {
                        err_msg = "Invalid Merchant ID";
                        return null;
                    }
                    else
                    {
                        Data_Suc = pgad.TxnFeeReport(domain_D[0].DomainShortName, PSince, PTo, PCurrencyCode);   //Modified by DANIEL 13 Mar 2019. Add parameter PCurrencyCode

                        system_err = pgad.system_err;
                        if (system_err != string.Empty)
                        {
                            err_msg = "An error occured. Please contact administrator.";
                            return null;
                        }
                    }
                }
                return Data_Suc;
            }
            catch (Exception ex)
            {
                err_msg = ex.Message;
                system_err = ex.Message.ToString();
                return null;
            }
        }

        //Added by DANIEL 22 Jan 2019. Add method Do_TxnRateReseller to get data
        //public Finance_Report[] Do_TxnRateReseller(DateTime PSince, DateTime PTo, int PDomainID)  //Modified by DANIEL 13 Mar 2019. Add parameter PCurrencyCode
        //public Finance_Report[] Do_TxnRateReseller(DateTime PSince, DateTime PTo, int PDomainID, string PCurrencyCode = "") //Modified By Daniel 11 Apr 2019. Change to ResellerMarginReport.
        public ResellerMarginReport[] Do_TxnRateReseller(DateTime PSince, DateTime PTo, int PDomainID, string PCurrencyCode = "")
        {
            try
            {
                //Finance_Report[] Data_Suc = null; //Modified By Daniel 11 Apr 2019. Change to ResellerMarginReport
                ResellerMarginReport[] Data_Suc = null;

                if (PDomainID == 1)
                {
                    Data_Suc = pgad.TB_TxnRateReseller("", PSince, PTo, PCurrencyCode);     //Modified by DANIEL 13 Mar 2019. Add parameter PCurrencyCode

                    system_err = pgad.system_err;
                    if (system_err != string.Empty)
                    {
                        err_msg = "An error occured. Please contact administrator.";
                        return null;
                    }
                }
                else
                {
                    var domain_D = pgad.Get_CMS_Domain(1, PDomainID, " ");

                    system_err = pgad.system_err;
                    if (system_err != string.Empty)
                        return null;

                    if (domain_D == null)
                    {
                        err_msg = "Invalid Merchant ID";
                        return null;
                    }
                    else
                    {
                        Data_Suc = pgad.TB_TxnRateReseller(domain_D[0].DomainShortName, PSince, PTo, PCurrencyCode);    //Modified by DANIEL 13 Mar 2019. Add parameter PCurrencyCode

                        system_err = pgad.system_err;
                        if (system_err != string.Empty)
                        {
                            err_msg = "An error occured. Please contact administrator.";
                            return null;
                        }
                    }
                }
                return Data_Suc;
            }
            catch (Exception ex)
            {
                err_msg = ex.Message;
                system_err = ex.Message.ToString();
                return null;
            }
        }

        //Added by DANIEL 4 Apr 2019. Add Do_Get_CL_Plugin_IsReseller
        public bool Do_Get_CL_Plugin_IsReseller(int PDomainID)
        {
            try
            {
                var getCLPlugin_IsReseller = pgad.Do_Get_CL_Plugin_IsReseller(PDomainID);
                if (getCLPlugin_IsReseller != null)
                {
                    if (getCLPlugin_IsReseller.IsReseller.Equals("0"))
                        //Found in CL_Plugin but not reseller (Main merchant with sub-merchants)
                        return true;
                    else
                        //Found in CL_Plugin but Reseller = 1
                        return false;
                }
                //Record not found in CL_Plugin is normal merchant . 
                return false;
            }
            catch (Exception ex)
            {
                err_msg = ex.Message;
                system_err = ex.Message.ToString();
                return false;
            }
        }

        private static readonly Encoding encoding = Encoding.UTF8;
        public string DecryptAES256(string CCNumber)
        {
            if (CCNumber.Length < 20 || string.IsNullOrEmpty(CCNumber))
            {
                return CCNumber;
            }
            try
            {
                RijndaelManaged aes = new RijndaelManaged();
                aes.KeySize = 256;
                aes.BlockSize = 128;
                aes.Padding = PaddingMode.PKCS7;
                aes.Mode = CipherMode.CBC;
                aes.Key = encoding.GetBytes(System.Configuration.ConfigurationManager.AppSettings["AES256Key"]);

                // Base 64 decode
                byte[] base64Decoded = Convert.FromBase64String(CCNumber);
                string base64DecodedStr = encoding.GetString(base64Decoded);

                // JSON Decode base64Str
                JavaScriptSerializer ser = new JavaScriptSerializer();
                var payload = ser.Deserialize<Dictionary<string, string>>(base64DecodedStr);

                aes.IV = Convert.FromBase64String(payload["iv"]);

                ICryptoTransform AESDecrypt = aes.CreateDecryptor(aes.Key, aes.IV);
                byte[] buffer = Convert.FromBase64String(payload["value"]);

                string DecryptCCNumber = encoding.GetString(AESDecrypt.TransformFinalBlock(buffer, 0, buffer.Length));
                string MaskedCCNumber = string.Empty;
                if (!string.IsNullOrEmpty(DecryptCCNumber))
                {
                   MaskedCCNumber = DecryptCCNumber.Substring(0, 6) + "".PadRight(DecryptCCNumber.Length - 6 - 4, 'X') + DecryptCCNumber.Substring(DecryptCCNumber.Length - 4, 4);
                }
                return MaskedCCNumber;
            }
            catch (Exception e)
            {
                throw new Exception("Error decrypting: " + e.Message);
            }
        }
        public string GetCCChannelName(string CCNumber, string channelName)
        {
            if (channelName != "VISA" && channelName != "MASTER" && channelName != "JCB" && channelName != "AMEX" && channelName != "Diners" 
                && string.IsNullOrEmpty(CCNumber) && string.IsNullOrWhiteSpace(CCNumber))
            {
                return channelName;
            }

            CCNumber = DecryptAES256(CCNumber);
            string ChannelName = string.Empty;

            if (string.IsNullOrEmpty(CCNumber) || string.IsNullOrWhiteSpace(CCNumber)) //Due to empty after Decryption
            {
                return channelName;
            }

            if (CCNumber.Substring(0, 1) == "4")
            {
                ChannelName = "VISA";
            }
            else if (CCNumber.Substring(0, 1) == "5" || CCNumber.Substring(0, 1) == "2")
            {
                ChannelName = "MASTER";
            }
            else if (CCNumber.Substring(0, 2) == "35")
            {
                ChannelName = "JCB";
            }
            else if (CCNumber.Substring(0, 1) == "34" || CCNumber.Substring(0, 2) == "37")
            {
                ChannelName = "AMEX";
            }
            else if (CCNumber.Substring(0, 2) == "36" || CCNumber.Substring(0, 2) == "38")
            {
                ChannelName = "Diners";
            }
            else
            {
                ChannelName = "UnknownCC";
            }
            return ChannelName;
            //--SET @sz_SelectCC = @sz_SelectCC + '(CASE WHEN LEFT(txn.CardPAN, 1) = 4 THEN ''VISA'' '
            //--SET @sz_SelectCC = @sz_SelectCC + 'WHEN (LEFT(txn.CardPAN, 1) = 5 OR LEFT(txn.CardPAN, 1) = 2) THEN ''MASTER'' '
            //--SET @sz_SelectCC = @sz_SelectCC + 'WHEN (LEFT(txn.CardPAN, 2) = 35) THEN ''JCB'' '
            //--SET @sz_SelectCC = @sz_SelectCC + 'WHEN (LEFT(txn.CardPAN, 2) = 34 OR LEFT(txn.CardPAN, 2) = 37) THEN ''AMEX''  '
            //--SET @sz_SelectCC = @sz_SelectCC + 'WHEN (LEFT(txn.CardPAN, 2) = 36 OR LEFT(txn.CardPAN, 2) = 38) THEN ''Diners'' '
        }
    }
}
