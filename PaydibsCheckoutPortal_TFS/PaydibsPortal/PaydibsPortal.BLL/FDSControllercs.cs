﻿using PaydibsPortal.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaydibsPortal.BLL
{
    public class FDSControllercs
    {
        PGAdminDAL pgad = new PGAdminDAL();
        public string system_err = string.Empty;
        public string err_msg = string.Empty;

        public Merchant_Summary_Report[] Do_MerchantSummaryReport(DateTime PSince, DateTime PTo, int PDomainID)
        {
            try
            {
                Merchant_Summary_Report[] Data_Suc = null;

                if (PDomainID == 1)
                {
                    Data_Suc = pgad.TB_TxnRate_GetMerchantTransacionSummaryReport("", PSince, PTo);
                    system_err = pgad.system_err;
                    if (system_err != string.Empty)
                    {
                        err_msg = "An error occured. Please contact administrator.";
                        return null;
                    }
                }
                else
                {
                    var domain_D = pgad.Get_CMS_Domain(1, PDomainID, " ");

                    system_err = pgad.system_err;
                    if (system_err != string.Empty)
                        return null;

                    if (domain_D == null)
                    {
                        err_msg = "Invalid Merchant ID";
                        return null;
                    }
                    else
                    {
                        Data_Suc = pgad.TB_TxnRate_GetMerchantTransacionSummaryReport(domain_D[0].DomainShortName, PSince, PTo);
                        system_err = pgad.system_err;
                        if (system_err != string.Empty)
                        {
                            err_msg = "An error occured. Please contact administrator.";
                            return null;
                        }
                    }
                }
                return Data_Suc;
            }
            catch (Exception ex)
            {
                err_msg = ex.Message;
                system_err = ex.Message.ToString();
                return null;
            }
        }
    }
}
