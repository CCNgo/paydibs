﻿using Kenji.Apps;
using Newtonsoft.Json;
using PaydibsPortal.BLL.Helper;
using PaydibsPortal.DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Security;
using System.Text;

namespace PaydibsPortal.BLL
{
    public class HomeController
    {
        PGAdminDAL pgad = new PGAdminDAL();
        static Crypto aes = new Crypto();

        public string system_err = string.Empty;
        public string err_msg = string.Empty;
        private static SecureString terminalKey = new NetworkCredential("", System.Configuration.ConfigurationManager.AppSettings["TerminalKey"]).SecurePassword;
        private static SecureString terminalVector = new NetworkCredential("", System.Configuration.ConfigurationManager.AppSettings["TerminalVector"]).SecurePassword;
        private SecureString securePWKey = new NetworkCredential("",
            aes.szDecrypt3DES(System.Configuration.ConfigurationManager.AppSettings["PWKey"],
            new NetworkCredential("", terminalKey).Password,
            new NetworkCredential("", terminalVector).Password)).SecurePassword;
        private SecureString secureDefPwd = new NetworkCredential("",
            aes.szDecrypt3DES(System.Configuration.ConfigurationManager.AppSettings["DefaultPwd"],
            new NetworkCredential("", terminalKey).Password,
            new NetworkCredential("", terminalVector).Password)).SecurePassword;


        int _DomainID { get; set; }

        #region  Public Properties
        public int Total_Transaction { get; set; }              //Added by DANIEL 20 Nov 2018
        public int[] Total_Transaction_Data { get; set; }       //Added by DANIEL 20 Nov 2018
        public decimal Total_EndUser_Payment { get; set; }
        public decimal Total_Net_Revenue { get; set; }
        public decimal Conversion_Rate { get; set; }
        public decimal Average_Purchase { get; set; }
        public decimal[] Total_EndUser_Payment_Data { get; set; }
        public decimal[] Total_Net_Revenue_Data { get; set; }
        public decimal[] Conversion_Rate_Data { get; set; }
        public decimal[] Average_Purchase_Data { get; set; }
        public string Total_Revenue_Data { get; set; }
        public string Time_Of_The_Day_Data { get; set; }
        public string Day_Of_Week_Data { get; set; }
        public string Channel_Usage_Data { get; set; }
        #endregion

        //Construct
        public HomeController(int PDomainID)
        {
            _DomainID = PDomainID;
        }

        public dynamic Do_Check_User_Login(string PUser_Name, string PPassword, string PPIP_Address)
        {
            try
            {
                var ob = pgad.Get_CMS_User_Join_SelectDetail(PUser_Name);
                system_err = pgad.system_err;

                if (!(string.IsNullOrEmpty(system_err)))
                {
                    err_msg = "Failed Connection. Please contact administrator.";
                    return null;
                }

                if (ob == null)
                {
                    err_msg = "Invalid username or password.";
                    return null;
                }

                if (ob.DomainStatus == 0)       //if (ob.DomainStatus != 1). Modified by DANIEL 22 Jan 2019. Change conditional statement  0 = Terminated
                {
                    err_msg = "Domain not active. Please contact administrator.";
                    return null;
                }

                if (ob.PwdTemp != null)
                {
                    if (ob.PwdTemp == aes.EncryptData(PPassword, new NetworkCredential("", securePWKey).Password))
                    {
                        system_err = "UpdatePassword";
                        return ob;
                    }
                    else
                    {
                        err_msg = "Invalid Password.";
                        return null;
                    }
                }

                if (ob.Pwd != aes.EncryptData(PPassword, new NetworkCredential("", securePWKey).Password))
                {
                    err_msg = "Invalid Password.";
                    return null;
                }

                if (ob.Status == 0)     //if if (ob.Status != 1). Modified by DANIEL 22 Jan 2019. Change conditional statement  0 = Terminated
                {
                    var ob_stat = pgad.Get_CMS_CL_Status_By_Stat_ID(ob.Status);
                    //Added by DANIEL 9 Jan 20190. Get PGAdmin error message - START
                    system_err = pgad.system_err;
                    if (!(string.IsNullOrEmpty(system_err)))
                    {
                        err_msg = "Failed Connection. Please contact administrator.";
                        return null;
                    }
                    //Added by DANIEL 9 Jan 20190. Get PGAdmin error message - END

                    err_msg = PUser_Name + " user status : " + ob_stat.StatDesc + ". Please contact us for inquiry.";
                    return null;
                }

                if (ob.LastLogin != null)
                {
                    if (Convert.ToDateTime(ob.LastLogin) < DateTime.Now.AddMonths(-3))
                    {
                        err_msg = "Last login less than 3 months ago. Please contact administrator for login.";
                        return null;
                    }
                }

                return ob;
            }
            catch (Exception ex)
            {
                err_msg = "Invalid username or password.";
                system_err = ex.Message.ToString();
                return null;
            }
        }

        public bool Do_Update_User_LastLogin(string PUserID)
        {
            try
            {
                var userUpdate = pgad.CMS_User_Update_LastLogin(PUserID);
                system_err = pgad.system_err;
                if (system_err != string.Empty)
                    return false;
                else
                    return true;
            }
            catch (Exception ex)
            {
                err_msg = "Error! Please contact administrator";
                system_err = ex.Message.ToString();
                return false;
            }
        }

        public bool Do_Verify_User(string PUserID)
        {
            try
            {
                var user = pgad.CMS_User_SelectDetail(PUserID);
                system_err = pgad.system_err;
                if (user == null)
                {
                    err_msg = "Invalid user.";
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                err_msg = "Error! Please contact administrator";
                system_err = ex.Message.ToString();
                return false;
            }
        }

        public bool Do_Check_User_Password(string UserID, string newPwd)
        {
            try
            {
                //if (newPwd == "user12345"). Modified by DANIEL 26 Feb 2019. Convert default pwd to secureString request by PCI
                if (newPwd == new NetworkCredential("", secureDefPwd).Password)
                {
                    err_msg = "New password cannot be same as default password.";
                    return false;
                }

                //string encPwd = aes.EncryptData(newPwd, new NetworkCredential("", securePWKey).Password);
                CMS_UserPwdHistory[] userPwdHistory = pgad.Get_CMS_UserPwdHistory(UserID);
                system_err = pgad.system_err;
                if (system_err != string.Empty)
                    return false;
                else
                {
                    foreach (var row in userPwdHistory)
                    {
                        if (aes.EncryptData(newPwd, new NetworkCredential("", securePWKey).Password) == row.Pwd)
                        {
                            err_msg = "New password cannot be same as previous password.";
                            return false;
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                err_msg = "Error! Please contact administrator";
                system_err = ex.ToString();
                return false;
            }
        }

        public bool Do_Update_User_Password(string UserID, string newPwd)
        {
            try
            {
                var updPassword = pgad.CMS_User_Update_Password(UserID, aes.EncryptData(newPwd, new NetworkCredential("", securePWKey).Password));
                system_err = pgad.system_err;
                if (system_err != string.Empty)
                {
                    err_msg = "Error! Please contact administrator";        //Added by DANIEL 10 Jan 2019
                    return false;
                }
                else
                    return true;

            }
            catch (Exception ex)
            {
                err_msg = "Error! Please contact administrator";
                system_err = ex.ToString();
                return false;
            }
        }

        public bool Do_User_Request_Reset_Pwd(string PuserID)
        {
            try
            {
                var User_D = pgad.CMS_User_SelectDetail(PuserID);
                system_err = pgad.system_err;
                if (User_D == null)
                {
                    err_msg = "Invalid User ID";
                    return false;
                }
                if (!string.IsNullOrEmpty(User_D.PwdTemp))
                {
                    if (DateTime.Now <= Convert.ToDateTime(User_D.PwdTempChanged).AddMinutes(20))
                    {
                        err_msg = "You have request to change password at : " + User_D.PwdTempChanged + ". You may request again after minutes.";
                        return false;
                    }
                }

                //string TemPwd = GUID.GetGUID(10);     Modified by KENT LOONG 25 Mar 2019. Change to Secure String request by PCI
                SecureString TemPwd = new NetworkCredential("", GUID.GetGUID(10)).SecurePassword;
                //string EncTemPwd = aes.EncryptData(TemPwd, new NetworkCredential("", securePWKey).Password);
                bool rst = pgad.CMS_User_Update_PwdTemp(PuserID, aes.EncryptData(new NetworkCredential("", TemPwd).Password, new NetworkCredential("", securePWKey).Password));
                system_err = pgad.system_err;
                if (!rst)
                {
                    err_msg = "Failed to reset password.";
                    return false;
                }

                Do_Email(PuserID, new NetworkCredential("", TemPwd).Password);

                return true;
            }
            catch (Exception ex)
            {
                err_msg = "Error! Please contact administrator";
                system_err = ex.ToString();
                return false;
            }
        }

        private void Do_Email(string PUserID, string PTempPassword)
        {
            try
            {

                string Name = System.Configuration.ConfigurationManager.AppSettings["DefaultSenderName"];
                string SendFromEmail = System.Configuration.ConfigurationManager.AppSettings["DefaultSenderAddress"];
                //Modified by KENT LOONG 8 Mar 2019. BUG FIX SecureString and add decrypt to password
                SecureString SendFromEmail_password = new NetworkCredential("",
                    aes.szDecrypt3DES(System.Configuration.ConfigurationManager.AppSettings["DefaultSenderPassword"],
                    new NetworkCredential("", terminalKey).Password,
                    new NetworkCredential("", terminalVector).Password)).SecurePassword;
                //SecureString SendFromEmail_password = System.Configuration.ConfigurationManager.AppSettings["DefaultSenderPassword"];
                string SendFromHost = System.Configuration.ConfigurationManager.AppSettings["DefaultSenderHost"];
                string SendToEmail = PUserID;

                #region HTML Content
                StringBuilder sb = new StringBuilder();

                sb.Append("<div style='width:80%;max-width:800px; float:left; font-family:arial,sans-serif; '>");
                sb.Append("<div style='width:100%; margin: 0px auto; text-align:center; '>");
                sb.Append("<img src=\"https://portal.paydibs.com/assets/img/logo.png\" width='180' />");
                sb.Append("</div>");
                sb.Append("<div style=\"width:90%; margin:0px auto; border:1px solid #CCCCCC; padding: 20px;\">");
                //sb.Append("<div style=\"width:90%; margin:0px auto; border-bottom:1px solid #CCCCCC; border-top: 1px solid #CCCCCC; padding: 20px;\">");
                sb.Append("<div style=\"width:100%; \">");
                sb.Append("<p style=\"text-align: center; font-size: 24px; \">");

                sb.Append("<table>");

                sb.Append("<tr>");
                sb.Append("<td colspan='2' align='left'>Dear our value merchant,<br/><br/> ");
                sb.Append("</td>");
                sb.Append("</tr>");

                sb.Append("<tr>");
                sb.Append("<td colspan='2' align='left'>The password for your User ID (<u>" + PUserID + "</u>) has been successfully reset.<br/><br/> ");
                sb.Append("</td>");
                sb.Append("</tr>");

                sb.Append("<tr>");
                sb.Append("<td colspan='2' align='left'>Step 1: Please use the Temporary Password below to change your new password.<br/><br/> ");
                sb.Append("</td>");
                sb.Append("</tr>");

                sb.Append("<tr>");
                sb.Append("<td colspan='2' align='left'>Temporary Password: <b style='color:red;'>" + PTempPassword + "</b>");
                sb.Append("</td>");
                sb.Append("</tr>");

                sb.Append("<tr>");
                sb.Append("<td colspan='2' align='left'>Step 2: Please go to https://portal.paydibs.com/Default.aspx <br/><br/> ");
                sb.Append("</td>");
                sb.Append("</tr>");

                sb.Append("<tr>");
                sb.Append("<td colspan='2' align='left'>If you did not make this change or you believe an unauthorised person has accessed your account," +
                    " go to https://portal.paydibs.com/ResetPassword.aspx to reset your password without delay. Following this," +
                    " sign into your User ID account page at https://portal.paydibs.com to review and update your security settings.<br/><br/> ");
                sb.Append("</td>");
                sb.Append("</tr>");

                sb.Append("<tr>");
                sb.Append("<td colspan='2' align='left'>Sincerely,<br/><br/> Paydibs Checkout Support ");
                sb.Append("</td>");
                sb.Append("</tr>");

                sb.Append("</table>");
                sb.Append("</div>");
                sb.Append("</div></div>");
                #endregion

                SmtpClient client = new SmtpClient(SendFromHost, 587);
                MailAddress from = new MailAddress(SendFromEmail, Name);

                //Modified by KENT LOONG 8 Mar 2019. CHANGE PASSWORD TO SECURESTRING request by PCI
                //client.Credentials = new System.Net.NetworkCredential(SendFromEmail, new NetworkCredential("", securePWKey).Password);
                client.Credentials = new System.Net.NetworkCredential(SendFromEmail, new NetworkCredential("", SendFromEmail_password).Password);

                client.EnableSsl = true;

                MailAddress to = new MailAddress(SendToEmail);
                MailMessage message = new MailMessage(from, to);
                message.Subject = "Paydibs Checkout Reset Password";
                message.Body = sb.ToString();
                message.IsBodyHtml = true;

                string SendStatus = "";
                try
                {
                    client.Send(message);
                }
                catch (Exception ex)
                {
                    SendStatus = ex.ToString();
                }
            }
            catch (Exception ex)
            {
                err_msg = ex.Message;
            }
        }

        //Modified by SUKI 4 Jan 2019. Check permission on every page load
        public dynamic[] LoadUserPermission(string UserID, int POpsID = 0)
        {
            try
            {
                var permission = pgad.LoadPermission(UserID, POpsID);       //Modified by SUKI 4 Jan 2019. Added POpsID to check permission on every page load

                system_err = pgad.system_err;
                if (system_err != string.Empty)
                    return null;
                else
                    return permission;
            }
            catch (Exception ex)
            {
                system_err = ex.Message;
                return null;
            }
        }

        public CMS_Domain[] Do_GetMerchantList(int PStatus, int PMode)
        {
            // PStatus - 99:All, 1:Active, 0:Terminated
            // PMode - 0: Reseller and Sub Merchant, 1: Reseller only, 2: Reseller=ALL and Sub Merchant
            //PMode - 4: isReseller = True
            try
            {
                CMS_Domain[] finalResult = pgad.Get_CMS_Domain(PStatus, ((_DomainID == 1) ? 0 : _DomainID), " ", PMode);
                system_err = pgad.system_err;
                if (system_err != string.Empty)
                    return null;
                else
                {
                    foreach (var row in finalResult)
                    {
                        row.DomainDesc = row.DomainDesc + "(" + row.DomainShortName + ")";
                    }
                }
                return finalResult;
            }
            catch (Exception ex)
            {
                system_err = ex.Message;
                return null;
            }
        }

        public bool Do_DashBoardReport(DateTime PSince, DateTime PTo, int PMerchantID, string PCurrency)
        {
            try
            {
                DateTime todayS = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 00, 00, 00);
                DateTime todayE = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);

                Txn_Summ_Modal[] suc_Txn;
                Txn_Summ_Modal[] txn_All;
                Txn_Summ_Modal[] suc_Txn_H;
                TxnRate_Joined_Host_Summ[] suc_txn_Host;

                if (PMerchantID == 1)
                {
                    suc_Txn = pgad.Get_TB_TxnRate_Host_Status_0(" ", PSince, PTo, PCurrency);
                    //Added by DANIEL 9 Jan 2019. Get pgadmin error message - START
                    system_err = pgad.system_err;
                    if (system_err != string.Empty)
                    {
                        err_msg = "An error occured. Please contact administrator.";
                        return false;
                    }
                    //Added by DANIEL 9 Jan 2019. Get pgadmin error message - END

                    txn_All = pgad.Get_TB_TxnRate_Summary_Status_All(" ", PSince, PTo, PCurrency);

                    //Added by DANIEL 9 Jan 2019. Get pgadmin error message - START
                    system_err = pgad.system_err;
                    if (system_err != string.Empty)
                    {
                        err_msg = "An error occured. Please contact administrator.";
                        return false;
                    }
                    //Added by DANIEL 9 Jan 2019. Get pgadmin error message - END

                    suc_txn_Host = pgad.Get_Host_TB_TxnRate_Status_0(" ", PSince, PTo, PCurrency);

                    //Added by DANIEL 9 Jan 2019. Get pgadmin error message - START
                    system_err = pgad.system_err;
                    if (system_err != string.Empty)
                    {
                        err_msg = "An error occured. Please contact administrator.";
                        return false;
                    }
                    //Added by DANIEL 9 Jan 2019. Get pgadmin error message - END

                    suc_Txn_H = (PSince.Month == PTo.Month && PSince.Day == PTo.Day) ? pgad.Get_TB_TxnRate_Summary_Status_0(" ", PSince, PTo, PCurrency) : pgad.Get_TB_TxnRate_Summary_Status_0(" ", todayS, todayE, PCurrency);

                    //Added by DANIEL 9 Jan 2019. Get pgadmin error message - START
                    system_err = pgad.system_err;
                    if (system_err != string.Empty)
                    {
                        err_msg = "An error occured. Please contact administrator.";
                        return false;
                    }
                    //Added by DANIEL 9 Jan 2019. Get pgadmin error message - END
                }
                else
                {
                    var domain_D = pgad.Get_CMS_Domain(1, PMerchantID, " ");
                    if (domain_D == null)
                    {
                        system_err = pgad.system_err;       //Added by DANIEL 9 Jan 2019
                        err_msg = "Invalid Merchant ID";
                        return false;
                    }
                    else
                    {
                        suc_Txn = pgad.Get_TB_TxnRate_Host_Status_0(domain_D[0].DomainShortName, PSince, PTo, PCurrency);
                        //Added by DANIEL 9 Jan 2019. Get pgadmin error message - START
                        system_err = pgad.system_err;
                        if (system_err != string.Empty)
                        {
                            err_msg = "An error occured. Please contact administrator.";
                            return false;
                        }
                        //Added by DANIEL 9 Jan 2019. Get pgadmin error message - END

                        txn_All = pgad.Get_TB_TxnRate_Summary_Status_All(domain_D[0].DomainShortName, PSince, PTo, PCurrency);
                        //Added by DANIEL 9 Jan 2019. Get pgadmin error message - START
                        system_err = pgad.system_err;
                        if (system_err != string.Empty)
                        {
                            err_msg = "An error occured. Please contact administrator.";
                            return false;
                        }
                        //Added by DANIEL 9 Jan 2019. Get pgadmin error message - END

                        suc_txn_Host = pgad.Get_Host_TB_TxnRate_Status_0(domain_D[0].DomainShortName, PSince, PTo, PCurrency);
                        //Added by DANIEL 9 Jan 2019. Get pgadmin error message - START
                        system_err = pgad.system_err;
                        if (system_err != string.Empty)
                        {
                            err_msg = "An error occured. Please contact administrator.";
                            return false;
                        }
                        //Added by DANIEL 9 Jan 2019. Get pgadmin error message - END

                        suc_Txn_H = (PSince.Month == PTo.Month && PSince.Day == PTo.Day) ? pgad.Get_TB_TxnRate_Summary_Status_0(domain_D[0].DomainShortName, PSince, PTo, PCurrency) : pgad.Get_TB_TxnRate_Summary_Status_0(domain_D[0].DomainShortName, todayS, todayE, PCurrency);
                        //Added by DANIEL 9 Jan 2019. Get pgadmin error message - START
                        system_err = pgad.system_err;
                        if (system_err != string.Empty)
                        {
                            err_msg = "An error occured. Please contact administrator.";
                            return false;
                        }
                        //Added by DANIEL 9 Jan 2019. Get pgadmin error message - END
                    }
                }

                Do_CalculateSumm(txn_All, suc_Txn, suc_Txn_H, suc_txn_Host);
                return true;
            }
            catch (Exception ex)
            {
                err_msg = ex.Message;
                return false;
            }
        }

        void Do_CalculateSumm(Txn_Summ_Modal[] All_TxnRecord, Txn_Summ_Modal[] Suc_TxnRecord, Txn_Summ_Modal[] txnRecord_Hour, TxnRate_Joined_Host_Summ[] host_txn_record)
        {
            decimal TotalTxn = All_TxnRecord.Sum(p => p.Qty);
            decimal TotalSuc = Suc_TxnRecord.Sum(p => p.Qty);

            Total_Transaction = Convert.ToInt32(TotalSuc);      //Added by DANIEL 20 Nov 2018. To retrieve the total succesful transaction count
            Total_EndUser_Payment = Suc_TxnRecord.Sum(p => p.TxnAmt);
            Total_Net_Revenue = Suc_TxnRecord.Sum(p => p.NetFee);
            if (TotalTxn == 0)
            {
                Conversion_Rate = 0;
                Average_Purchase = 0;
            }
            else
            {
                Conversion_Rate = decimal.Divide(TotalSuc, TotalTxn) * 100;
                Average_Purchase = decimal.Divide(Total_EndUser_Payment, TotalTxn);
            }

            Total_Transaction_Data = Suc_TxnRecord.Select(p => p.Qty).ToArray();        //Added by Daneil 20 Nov 2018. Data to be display on dashboard
            Total_EndUser_Payment_Data = Suc_TxnRecord.Select(p => p.TxnAmt).ToArray();
            Total_Net_Revenue_Data = Suc_TxnRecord.Select(p => p.NetFee).ToArray();

            #region Generate Total Revenue Chart Data
            List<Txn_Summ_Modal> total_revenue = new List<Txn_Summ_Modal>();

            if (Suc_TxnRecord.Count() > 0)
            {
                //Modified by DANIEl 25 Feb 2019. Change linq query
                //var startDate = Convert.ToDateTime(Suc_TxnRecord.OrderBy(p => p.DateCreated).Select(p => p.DateCreated).ToArray()[0]);
                //var endDate = Convert.ToDateTime(Suc_TxnRecord.OrderByDescending(p => p.DateCreated).Select(p => p.DateCreated).ToArray()[0]);
                var startDate = Convert.ToDateTime(Suc_TxnRecord.Select(p => p.DateCreated).ToArray()[0]);
                var endDate = Convert.ToDateTime(Suc_TxnRecord.Select(p => p.DateCreated).ToArray()[Suc_TxnRecord.Count() - 1]);

                double diffDate = endDate.Subtract(startDate).TotalDays + 1;

                if (diffDate <= 0) diffDate = 1;

                for (int i = 0; i < diffDate; i++)
                {
                    Txn_Summ_Modal new_S = new Txn_Summ_Modal();

                    var revenue_Record = Suc_TxnRecord.Where(p => Convert.ToDateTime(p.DateCreated) == startDate.AddDays(i)).ToArray();

                    new_S.DateCreated = startDate.AddDays(i).ToString("dd-MMM");

                    if (revenue_Record.Count() <= 0)
                    {
                        new_S.TxnAmt = 0;
                        new_S.Qty = 0;
                        new_S.NetFee = 0;
                    }
                    else
                    {
                        new_S.TxnAmt = revenue_Record[0].TxnAmt;
                        new_S.Qty = revenue_Record[0].Qty;
                        new_S.NetFee = decimal.Round(revenue_Record[0].NetFee, 2, MidpointRounding.AwayFromZero);
                    }

                    total_revenue.Add(new_S);
                    //Suc_TxnRecord[i].DateCreated = Convert.ToDateTime(Suc_TxnRecord[i].DateCreated).ToString("dd-MMM");
                }
                Total_Revenue_Data = Newtonsoft.Json.JsonConvert.SerializeObject(total_revenue);
            }
            else
                Total_Revenue_Data = "0";
            #endregion

            #region Calculate conversion_Rate Data
            List<decimal> conversion_Rate = new List<decimal>();
            foreach (var item in All_TxnRecord)
            {
                var day_txn = item.Qty;
                var day_suc = Suc_TxnRecord.Where(p => p.DateCreated == item.DateCreated).Select(p => p.Qty).ToArray();
                if (day_suc.Count() <= 0)
                {
                    conversion_Rate.Add(0);
                }
                else
                {
                    int day_rate = Convert.ToInt32(decimal.Divide(day_suc[0], day_txn) * 100);
                    conversion_Rate.Add(day_rate);
                }
            }
            Conversion_Rate_Data = conversion_Rate.ToArray();
            #endregion

            #region Calculate Average_Purchase_Data
            List<decimal> average_purchases = new List<decimal>();
            foreach (var item in Suc_TxnRecord)
            {
                var day_req = item.Qty;
                var day_amt = item.TxnAmt;
                average_purchases.Add(decimal.Round(decimal.Divide(day_amt, day_req), 2, MidpointRounding.AwayFromZero));
            }
            Average_Purchase_Data = average_purchases.ToArray();
            #endregion

            #region Calculate Channel Usage
            StringBuilder sb = new StringBuilder();
            MMGMTController mc = new MMGMTController();
            var hostList = mc.Do_Get_HostByType("CC");
            var total_qty = host_txn_record.Sum(p => p.Qty);
            //Commented by KENT LOONG 25 Feb 2019. SEPERATE TO ONLINE BANKING AND CREDIT CARD
            //for (int i = 0; i < host_txn_record.Count(); i++)
            //{
            //    var usage_qty = host_txn_record[i].Qty;
            //    //var QtyVal = (decimal.Divide(total_qty, usage_qty) * 100).ToString("0.00");
            //    var QtyVal = (decimal.Divide(usage_qty, total_qty) * 100).ToString("0.00");
            //    string HostDesc = host_txn_record[i].HostDesc;
            //    if (i > 0) sb.Append(",");
            //    sb.Append("{label: '" + HostDesc + "' , value: " + QtyVal + "}");
            //}

            //Added by KENT LOONG 25 Feb 2019. SEPERATE TO ONLINE BANKING AND CREDIT CARD - START
            decimal pg_usage_qty = 0;
            foreach (var host in hostList)
            {
                foreach (var record in host_txn_record)
                {
                    if (record.HostDesc == host.HostDesc)
                    {
                        pg_usage_qty = pg_usage_qty + record.Qty;
                    }
                }
            }
            if (pg_usage_qty > 0)
            {
                var QtyVal = (decimal.Divide(pg_usage_qty, total_qty) * 100).ToString("0.00");
                string HostDesc = "Credit Card";
                sb.Append("{label: '" + HostDesc + "' , value: " + QtyVal + "}");
            }

            foreach (var record in host_txn_record)
            {
                bool isCC = false;
                foreach (var host in hostList)
                {
                    if (record.HostDesc == host.HostDesc)
                    {
                        isCC = true;
                    }
                }

                if (!isCC)
                {
                    var usage_qty = record.Qty;
                    var QtyVal = (decimal.Divide(usage_qty, total_qty) * 100).ToString("0.00");
                    string HostDesc = record.HostDesc;
                    if (sb.Length > 0)
                    {
                        sb.Append(",");
                    }
                    sb.Append("{label: '" + HostDesc + "' , value: " + QtyVal + "}");
                }
            }
            //Added by KENT LOONG 25 Feb 2019. SEPERATE TO ONLINE BANKING AND CREDIT CARD - END
            Channel_Usage_Data = sb.ToString();
            #endregion

            #region Calculate Time Of Day
            List<Txn_Summ_Modal> time_of_day = new List<Txn_Summ_Modal>();
            for (int i = 0; i < 23; i++)
            {
                Txn_Summ_Modal new_S = new Txn_Summ_Modal();

                var H_Record = txnRecord_Hour.Where(p => Convert.ToInt32(p.HourCreated) == i).ToArray();

                new_S.HourCreated = i.ToString("00") + "00";

                if (H_Record.Count() <= 0)
                {
                    new_S.TxnAmt = 0;
                    new_S.Qty = 0;
                    new_S.NetFee = 0;
                }
                else
                {
                    new_S.TxnAmt = H_Record[0].TxnAmt;
                    new_S.Qty = H_Record[0].Qty;
                    new_S.NetFee = decimal.Round(H_Record[0].NetFee, 2, MidpointRounding.AwayFromZero);
                }

                time_of_day.Add(new_S);
            }
            Time_Of_The_Day_Data = Newtonsoft.Json.JsonConvert.SerializeObject(time_of_day);
            #endregion
        }

        public UserGroup Do_GetUserGroup(int PDomainID)
        {
            try
            {
                var userGroup = pgad.CMS_UserGroup_By_DomainID(PDomainID)[0];
                system_err = pgad.system_err;
                if (system_err != string.Empty)
                    return null;
                else
                    return userGroup;
            }
            catch (Exception ex)
            {
                system_err = ex.Message;
                return null;
            }
        }

        #region Insert Access Log
        public bool Do_AddAccessLog(string PUserID, int PAcctID, int PMesgCode, OpsIDEnum opsIDEnum, int PResult, string PResultDesc)
        {
            return pgad.CMS_AccessLog_Insert(PUserID, PAcctID, PMesgCode, opsIDEnum, PResult, PResultDesc);
        }
        #endregion

        //Added by DANIEL 4 Jan 2019. Add method to insert record into PGAdmin..AuditTrail
        public bool Do_Add_AuditTrail(string PUserID, int POpsID, string PEvent, string PDescription)
        {
            return pgad.AuditTrail_Insert(PUserID, POpsID, PEvent, PDescription);
        }

       //Added by DANIEL 27 Mar 2019. Add method to POST api
        public string Response_POST(string PUrl, string PHttpString)
        {
            string retStatus = string.Empty;
            try
            {
                switch (System.Configuration.ConfigurationManager.AppSettings["ProtocolType"].ToUpper())
                {
                    case "SSL3":
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
                        break;
                    case "TLS":
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;
                        break;
                    case "TLS11":
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11;
                        break;
                    case "TLS12":
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                        break;
                    default:
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                        break;
                }
                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };


                //Modified By Daniel on 4 Apr 2019. WebRequest request = WebRequest.Create(PUrl);
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(PUrl);

                request.ContentType = "application/x-www-form-urlencoded";
                request.Method = "POST";
                //byte[] byteArray = Encoding.UTF8.GetBytes(sb.ToString());
                //request.ContentLength = byteArray.Length;
                //Stream dataStream = request.GetRequestStream();
                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    streamWriter.Write(PHttpString);
                }

                string result = string.Empty;
                var httpResponse = (HttpWebResponse)request.GetResponse();

                if (httpResponse.StatusCode == HttpStatusCode.OK)
                {
                    using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        result = streamReader.ReadToEnd();
                    }
                    retStatus = "Status: " + HttpStatusCode.OK.ToString();
                }
                else
                {
                    retStatus = "Status: " + httpResponse.StatusCode.ToString();
                }
            }
            catch (WebException ex)
            {
                err_msg = ex.ToString() + "PUrl: " +  PUrl;
            }
            return retStatus;
        }

        //Added by Daniel 3 May 2019. Temporary cross web apps login -- START
        public string tempLoginEncrypt(string str)
        {
            str = aes.EncryptData(str, new NetworkCredential("", securePWKey).Password);
            return str;
        }

        public string tempLoginDecrypt(string str)
        {
            str = aes.DecryptData(str, new NetworkCredential("", securePWKey).Password);
            return str;
        }
        //--END

        //Added by Daniel 6 May 2019. GetUser by UserID
        public CMS_User getUserDomainID(string userID)
        {
            var getCMSUser = pgad.CMS_User_SelectDetail(userID);
            return getCMSUser;
        }

        //public Permission[] LoadPermissions(string UserID, int POpsID = 0)
        //{
        //    try
        //    {
        //        var permission = pgad.LoadPermissions(UserID, POpsID);
        //        system_err = pgad.system_err;
        //        if (system_err != string.Empty)
        //            return null;
        //        else
        //            return permission;
        //    }
        //    catch (Exception ex)
        //    {
        //        system_err = ex.Message;
        //        return null;
        //    }
        //}

        public string VGSOutboundConvert(string CCNumber)
        {
            string aliasData = "{\"card_number\":\"" + CCNumber + "\"}";

            NetworkCredential credentials = new NetworkCredential(ConfigurationManager.AppSettings["VGSProxyName"], ConfigurationManager.AppSettings["VGSProxyPass"]);
            //NetworkCredential credentials = new NetworkCredential("US2ergWeupKZM7gj32snEVUM", "3962c9b1-7cbc-42e3-84ca-b56bbc7cb0ce");
            WebProxy proxy = new WebProxy(ConfigurationManager.AppSettings["VGSInboundRouteForOutbound"], false)
            {
                UseDefaultCredentials = false,
                Credentials = credentials,
            };
            //WebProxy proxy = new WebProxy("http://tntcdm4iz0p.sandbox.verygoodproxy.com:8080", false)
            //{
            //    UseDefaultCredentials = false,
            //    Credentials = credentials,
            //};
            var handler = new HttpClientHandler()
            {
                Proxy = proxy,
                PreAuthenticate = true,
                UseDefaultCredentials = false,
            };

            try
            {
                System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                HttpClient httpClient = new HttpClient(handler);
                StringContent content = new StringContent(aliasData, Encoding.UTF8, "application/json");
                var respond = httpClient.PostAsync(ConfigurationManager.AppSettings["VGSUpstreamHost"], content);
                //var respond = httpClient.PostAsync("https://echo.apps.verygood.systems/post", content);

                HttpResponseMessage response = respond.Result;
                var responseBody = response.Content.ReadAsStringAsync();

                var outBoundResponse = JsonConvert.DeserializeObject<VGSOutbound>(responseBody.Result);

                return outBoundResponse.json.cardNumber;

            }
            catch (Exception ex)
            {
                system_err = ex.Message;
            }
            return "";
        }

    }

    public class userStatus
    {
        public static int Pending = 0;
        public static int Active = 1;
        public static int Suspended = 2;
        public static int Terminated = 0;
        public static int Lockout = 4;
    }

    public class VGSOutbound
    {
        [JsonProperty("orderAmount")]
        public object args { get; set; }

        [JsonProperty("data")]
        public object data { get; set; }

        [JsonProperty("files")]
        public object files { get; set; }

        [JsonProperty("form")]
        public object form { get; set; }

        [JsonProperty("headers")]
        public object headers { get; set; }

        [JsonProperty("json")]
        public VGSOutboundJson json { get; set; }

        [JsonProperty("origin")]
        public object origin { get; set; }

        [JsonProperty("url")]
        public object url { get; set; }

    }
    public class VGSOutboundJson
    {
        [JsonProperty("card_number")]
        public string cardNumber { get; set; }
    }
}
