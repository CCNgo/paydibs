﻿using PaydibsPortal.BLL.Helper;
using PaydibsPortal.DAL;
using System;
using System.Net;
using System.Security;
using System.Security.Cryptography;
using System.Text;

namespace PaydibsPortal.BLL
{
    public class SUPController
    {
        PGAdminDAL pgad = new PGAdminDAL();
        static Crypto crypto = new Crypto();
        public string system_err = string.Empty;
        public string err_msg = string.Empty;

        private static SecureString terminalKey = new NetworkCredential("", System.Configuration.ConfigurationManager.AppSettings["TerminalKey"]).SecurePassword;
        private static SecureString terminalVector = new NetworkCredential("", System.Configuration.ConfigurationManager.AppSettings["TerminalVector"]).SecurePassword;
        private SecureString PWKey = new NetworkCredential("",
        crypto.szDecrypt3DES(System.Configuration.ConfigurationManager.AppSettings["PWKey"],
        new NetworkCredential("", terminalKey).Password,
        new NetworkCredential("", terminalVector).Password)).SecurePassword;
        public TB_LateRes[] Do_Get_LateResponse_Transaction(string PGatewayTxnID, int PDomainID, DateTime PSince, DateTime PTo)
        {
            try
            {
                var domain_D = pgad.Get_CMS_Domain(1, PDomainID, " ");

                system_err = pgad.system_err;
                if (system_err != string.Empty)
                    return null;

                if (domain_D == null)
                {
                    err_msg = "Invalid Merchant ID";
                    return null;
                }

                var getLateResponse = pgad.Get_TB_LateRes(PGatewayTxnID, PDomainID != 1 ? domain_D[0].DomainShortName : "", PSince, PTo);
                if (getLateResponse == null)
                {
                    system_err = pgad.system_err;
                    err_msg = "An error occured. Please contact administrator.";
                    return null;
                }
                return getLateResponse;
            }
            catch (Exception ex)
            {
                system_err = ex.Message;
                err_msg = "An error occured. Please contact administrator.";
                return null;
            }
        }

        public Manage_Txn_Status[] Do_Get_Pymt_Response(int PDomainID, string PGatewayTxnID, DateTime PSince, DateTime PTo, string PPymtID, string PPymtMethod, string PCurrencyCode)
        {
            try
            {
                var domain_D = pgad.Get_CMS_Domain(1, PDomainID, " ");
                system_err = pgad.system_err;
                if (system_err != string.Empty)
                    return null;

                if (domain_D == null)
                {
                    err_msg = "Invalid Merchant ID";
                    return null;
                }

                var getPayRes = pgad.Get_TB_PayRes(PDomainID != 1 ? domain_D[0].DomainShortName : "", PGatewayTxnID, PSince, PTo, PPymtID, PPymtMethod, PCurrencyCode);
                if (getPayRes == null)
                {
                    system_err = pgad.system_err;
                    err_msg = "An error occured. Please contact administrator.";
                    return null;
                }

                return getPayRes;
            }
            catch (Exception ex)
            {
                system_err = ex.Message;
                err_msg = "An error occured. Please contact administrator.";
                return null;
            }

        }

        public bool Do_Update_PayRes_TxnRate(string PGatewayTxnID, string PPymtMethod, int PStatus, int PState)
        {
            try
            {
                var updatePayResTxnRate = pgad.Do_Update_PayRes_TxnRate(PGatewayTxnID, PPymtMethod, PStatus, PState);
                if (!updatePayResTxnRate)
                {
                    system_err = pgad.system_err;
                    err_msg = "An error occured. Please contact administrator.";
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                system_err = ex.Message;
                err_msg = "An error occured. Please contact administrator.";
                return false;
            }

        }


        public CallBack_Response[] Do_Get_CallBack_Response(string PMethod, string PGatewayTxnID)
        {
            try
            {
                var getCallBackResp = pgad.Do_Get_CallBack_Response(PMethod, PGatewayTxnID);
                if (getCallBackResp == null)
                {
                    system_err = pgad.system_err;
                    err_msg = "An error occured. Please contact administrator.";
                    return null;
                }
                return getCallBackResp;
            }
            catch (Exception ex)
            {
                system_err = ex.Message;
                err_msg = "An error occured. Please contact administrator.";
                return null;
            }
        }

        public string szComputeSHA512(string responseParams)
        {
            string szHashValue = "";
            SHA512 objSHA512 = null;
            Byte[] btHashbuffer = null;
            try
            {
                objSHA512 = new SHA512Managed();
                objSHA512.ComputeHash(Encoding.ASCII.GetBytes(responseParams));
                btHashbuffer = objSHA512.Hash;
                szHashValue = szWriteHex(btHashbuffer).ToLower();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (objSHA512 != null)
                {
                    objSHA512 = null;
                }
            }
            return szHashValue;
        }

        private string szWriteHex(byte[] bt_Array)
        {
            string szHex = "";
            for (int iCounter = 0; iCounter < bt_Array.Length; iCounter++)
            {
                szHex += bt_Array[iCounter].ToString("X2");
            }
            return szHex;
        }

        public string getMerchantInfoByID(string PMerchantID)
        {
            try
            {
                var getMerchantInfo = pgad.Merchant_SelectDetail(PMerchantID);
                if (getMerchantInfo == null)
                {
                    system_err = pgad.system_err;
                    return null;
                }
                else
                {
                    var merchantPwd = getMerchantInfo.MerchantPassword;
                    return merchantPwd;
                }
            }
            catch (Exception ex)
            {
                system_err = ex.Message;
                err_msg = "An error occured. Please contact administrator.";
                return null;
            }
        }
    }
}
