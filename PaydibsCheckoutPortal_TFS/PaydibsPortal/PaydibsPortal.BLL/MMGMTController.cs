﻿using Kenji.Apps;
using PaydibsPortal.BLL.Helper;
using PaydibsPortal.DAL;
using System;
using System.Net;
using System.Security;

namespace PaydibsPortal.BLL
{
    public class MMGMTController
    {
        PGDAL pgd = new PGDAL();
        PGAdminDAL pgad = new PGAdminDAL();
        static Crypto aes = new Crypto();

        public string system_err = string.Empty;
        public string err_msg = string.Empty;
        private static SecureString terminalKey = new NetworkCredential("", System.Configuration.ConfigurationManager.AppSettings["TerminalKey"]).SecurePassword;
        private static SecureString terminalVector = new NetworkCredential("", System.Configuration.ConfigurationManager.AppSettings["TerminalVector"]).SecurePassword;
        private SecureString PWKey = new NetworkCredential("",
            aes.szDecrypt3DES(System.Configuration.ConfigurationManager.AppSettings["PWKey"],
            new NetworkCredential("", terminalKey).Password,
            new NetworkCredential("", terminalVector).Password)).SecurePassword;
        private SecureString secureDefPwd = new NetworkCredential("",
            aes.szDecrypt3DES(System.Configuration.ConfigurationManager.AppSettings["DefaultPwd"],
            new NetworkCredential("", terminalKey).Password,
            new NetworkCredential("", terminalVector).Password)).SecurePassword;

        public CL_State[] Do_GetCLState()
        {
            try
            {
                var state = pgd.GetCLState();
                system_err = pgd.system_err;
                if (system_err != string.Empty)
                    return null;
                else
                    return state;
            }
            catch (Exception ex)
            {
                err_msg = ex.Message;
                return null;
            }
        }

        public CLCountry[] Do_GetCLCountry()
        {
            try
            {
                var country = pgd.GetCLCountry();
                system_err = pgd.system_err;
                if (system_err != string.Empty)
                    return null;
                else
                    return country;
            }
            catch (Exception ex)
            {
                err_msg = ex.Message;
                return null;
            }
        }

        public CL_Plugin[] Do_GetCLPlugin()
        {
            try
            {
                var plugin = pgd.GetCLPlugin();
                system_err = pgd.system_err;
                if (system_err != string.Empty)
                    return null;
                else
                    return plugin;
            }
            catch (Exception ex)
            {
                err_msg = ex.Message;
                return null;
            }
        }

        public CL_NotifEmailRcv[] Do_GetCLNotifEmailRcv()
        {
            try
            {
                var notifEmail = pgd.GetCLNotifEmailRcv();
                system_err = pgd.system_err;
                if (system_err != string.Empty)
                    return null;
                else
                    return notifEmail;
            }
            catch (Exception ex)
            {
                err_msg = ex.Message;
                return null;
            }
        }

        public CL_MCCCode[] Do_GetCLMCCCode()
        {
            try
            {
                var mccCode = pgd.GetCLMCCCode();
                system_err = pgd.system_err;
                if (system_err != string.Empty)
                    return null;
                else
                    return mccCode;
            }
            catch (Exception ex)
            {
                err_msg = ex.Message;
                return null;
            }
        }


        //Modified by SUKI 28 Nov 2018. Add in showMerchantAddr, showMerchantLogo, IsAgent, default currency setting
        //public bool Do_RegisterNewMerchant(string merchantID, string TradingName, string RegistrationName, string MCCCode, string ACP1Name, string ACP1Mobile,
        //    string ACP1Email, string CSEmail, string CSMobile, string Addr1, string Addr2, string Addr3, int CountryID, string City, string State, string PostCode, string WebSiteURL,
        //    int PluginID, int PymtNotificationEmail, string NotifyEmailAddr, string CompanyRegNo, string GSTNo, string TypeOfBusiness, string OfficeNumber, string ACPName,
        //    string ACPContactNo, string ACPEmail, string BillContactName, string BillContactNo, string BillContactEmail, string TypeOfProducts, string TargetMarkets,
        //    string DeliveredPeriod, string CreateBy, string D_FullName_i, string D_ID_i, string D_FullName_ii, string D_ID_ii, string D_FullName_iii, string D_ID_iii,
        //    string D_FullName_vi, string D_ID_vi, string D_FullName_v, string D_ID_v, string S_FullName_i, string S_ID_i, string S_FullName_ii, string S_ID_ii, string S_FullName_iii,
        //    string S_ID_iii, string S_FullName_vi, string S_ID_vi, string S_FullName_v, string S_ID_v, string BankName, string BankAccHolder, string BankAccNo, string SWIFTCode,
        //    string BankAddress, string BankCountry, decimal TxnLimit)
        public bool Do_RegisterNewMerchant(string merchantID, string TradingName, string RegistrationName, string MCCCode, string ACP1Name, string ACP1Mobile,
            string ACP1Email, string CSEmail, string CSMobile, string Addr1, string Addr2, string Addr3, int CountryID, string City, string State, string PostCode, string WebSiteURL,
            int PluginID, int PymtNotificationEmail, string NotifyEmailAddr, string CompanyRegNo, string GSTNo, string TypeOfBusiness, string OfficeNumber, string ACPName,
            string ACPContactNo, string ACPEmail, string BillContactName, string BillContactNo, string BillContactEmail, string TypeOfProducts, string TargetMarkets,
            string DeliveredPeriod, string CreateBy, string D_FullName_i, string D_ID_i, string D_FullName_ii, string D_ID_ii, string D_FullName_iii, string D_ID_iii,
            string D_FullName_vi, string D_ID_vi, string D_FullName_v, string D_ID_v, string S_FullName_i, string S_ID_i, string S_FullName_ii, string S_ID_ii, string S_FullName_iii,
            string S_ID_iii, string S_FullName_vi, string S_ID_vi, string S_FullName_v, string S_ID_v, string BankName, string BankAccHolder, string BankAccNo, string SWIFTCode,
            string BankAddress, string BankCountry, decimal TxnLimit, int showMerchantAddr, int showMerchantLogo, int isAgent, string defaultCurrency)
        {
            try
            {
                CMS_Domain[] checkDomain = pgad.Get_CMS_Domain(99, 0, merchantID);
                if (checkDomain.Length != 0)
                {
                    system_err = pgad.system_err;
                    err_msg = "Existing Domain = " + merchantID;
                    return false;
                }
                var checkMer = pgad.Merchant_SelectDetail(merchantID);
                if (checkMer != null)
                {
                    system_err = pgad.system_err;
                    err_msg = "Existing MerchantID = " + merchantID;
                    return false;
                }

                //Added by DANIEL 28 Mar 2019. Check for existing email - START
                var checkEmail = pgad.Merchant_SelectDetailByEmail(ACP1Email);
                if (checkEmail != null)
                {
                    if (ACP1Email.ToLower().Equals(checkEmail.UserID.ToLower()))
                    {
                        system_err = pgad.system_err;
                        err_msg = "Existing Email = " + ACP1Email;
                        return false;
                    }
                }
                //Added by DANIEL 28 Mar 2019. Check for existing email - END

                //Commented by DANIEL 28 Mar 2019
                //ADD START DANIEL 20190123 [Check Existing CompanyRegName and CompanyRegNo]
                //var checkCompName = pgad.GetCMS_Domain_By_CompanyName(RegistrationName);
                //if (checkCompName != null)
                //{
                //    system_err = pgad.system_err;
                //    err_msg = "Existing Company Registraion Name = " + RegistrationName;
                //    return false;
                //}

                //var checkCompNo = pgad.GetCMS_BusinessProfile_By_CompanyRegNo(CompanyRegNo);
                //if (checkCompNo != null)
                //{
                //    system_err = pgad.system_err;
                //    err_msg = "Existing Company Registraion Number = " + CompanyRegNo;
                //    return false;
                //}

                //string MerchantPassword = GUID.GetGUID(10).ToUpper();     Modified by KENT LOONG 25 Mar 2019. SECURE STRING request by PCI
                SecureString MerchantPassword = new NetworkCredential("", GUID.GetGUID(10).ToUpper()).SecurePassword;
                string HCProfiles = string.Empty;

                //Commented by SUKI 28 Nov 2018. Remove add host on add merchant - START
                //// Create a root node
                //XElement hosts = new XElement("hosts");

                //// Add child nodes
                //XElement priority = new XElement("priority", "1");
                //XElement hostID = new XElement("hostID", "1");
                //XElement hostCurr = new XElement("hostCurr", "MYR");
                //XElement host = new XElement("host");
                //host.Add(priority);
                //host.Add(hostID);
                //host.Add(hostCurr);

                //var CredentialList = Do_GetHostCredential("OB", 1);

                //foreach (var credential in CredentialList)
                //{
                //    switch (credential.DBFieldName)
                //    {
                //        case "Password":
                //            XElement Password = new XElement("Password", credential.DefaultValue);
                //            host.Add(Password);
                //            break;
                //        case "ReturnKey":
                //            XElement ReturnKey = new XElement("ReturnKey", credential.DefaultValue);
                //            host.Add(ReturnKey);
                //            break;
                //        case "SecretKey":
                //            XElement SecretKey = new XElement("SecretKey", credential.DefaultValue);
                //            host.Add(SecretKey);
                //            break;
                //        case "SendKeyPath":
                //            XElement SendKeyPath = new XElement("SendKeyPath", credential.DefaultValue);
                //            host.Add(SendKeyPath);
                //            break;
                //        case "ReturnKeyPath":
                //            XElement ReturnKeyPath = new XElement("ReturnKeyPath", credential.DefaultValue);
                //            host.Add(ReturnKeyPath);
                //            break;
                //        case "InitVector":
                //            XElement InitVector = new XElement("InitVector", credential.DefaultValue);
                //            host.Add(InitVector);
                //            break;
                //        case "PayeeCode":
                //            XElement PayeeCode = new XElement("PayeeCode", credential.DefaultValue);
                //            host.Add(PayeeCode);
                //            break;
                //        case "MID":
                //            XElement MID = new XElement("MID", credential.DefaultValue);
                //            host.Add(MID);
                //            break;
                //        case "TID":
                //            XElement TID = new XElement("TID", credential.DefaultValue);
                //            host.Add(TID);
                //            break;
                //        case "AcquirerID":
                //            XElement AcquirerID = new XElement("AcquirerID", credential.DefaultValue);
                //            host.Add(AcquirerID);
                //            break;
                //        case "AirlineCode":
                //            XElement AirlineCode = new XElement("AirlineCode", credential.DefaultValue);
                //            host.Add(AirlineCode);
                //            break;
                //    }
                //}

                //hosts.Add(host);
                //HCProfiles = hosts.ToString();
                //Commented by SUKI 28 Nov 2018. Remove add host on add merchant - END

                //Modified by SUKI 28 Nov 2018. Add in showMerchantAddr, showMerchantLogo, IsAgent, default currency setting
                //var new_merchant = pgd.Merchant_Insert(merchantID, TradingName, RegistrationName, MerchantPassword, MCCCode, ACP1Name, ACP1Mobile, ACP1Email, CSEmail, CSMobile,
                //    Addr1, Addr2, Addr3, CountryID, City, State, PostCode, WebSiteURL, PluginID, HCProfiles, PymtNotificationEmail, NotifyEmailAddr, TxnLimit);
                var new_merchant = pgd.Merchant_Insert(merchantID, TradingName, RegistrationName, new NetworkCredential("", MerchantPassword).Password, MCCCode, ACP1Name, ACP1Mobile, ACP1Email, CSEmail, CSMobile,
                    Addr1, Addr2, Addr3, CountryID, City, State, PostCode, WebSiteURL, PluginID, HCProfiles, PymtNotificationEmail, NotifyEmailAddr, TxnLimit,
                    showMerchantAddr, showMerchantLogo);

                if (new_merchant != 0)
                {
                    system_err = pgd.system_err;
                    if (system_err != string.Empty)
                    {
                        err_msg = "Failed to Create new Merchant : " + RegistrationName;
                        return false;
                    }
                }

                //Commented by SUKI 28 Nov 20181128. Add in showMerchantAddr, showMerchantLogo, IsAgent, default currency setting
                //var updMerchantGroup = pgad.Update_MerchantGroup(merchantID, PluginID);

                //Modified by DANIEL 16 Jan 2019. Add in showMerchantAddr, showMerchantLogo
                var updateMerchantSetting = pgad.Update_MerchantSetting(merchantID, PluginID, isAgent, defaultCurrency, showMerchantAddr, showMerchantLogo);

                if (updateMerchantSetting == false)
                {
                    system_err = pgad.system_err;
                    if (system_err != string.Empty)
                    {
                        err_msg = "Failed to update merchant setting";
                        return false;
                    }
                }

                checkDomain = pgad.Get_CMS_Domain(99, 0, merchantID);
                if (checkDomain != null)
                {
                    var new_biz_profile = pgad.CMS_BusinessProfile_Insert(checkDomain[0].DomainID, CompanyRegNo, GSTNo, TypeOfBusiness, OfficeNumber, ACPName, ACPContactNo, ACPEmail,
                        BillContactName, BillContactNo, BillContactEmail, TypeOfProducts, TargetMarkets, DeliveredPeriod, CreateBy);
                    if (new_biz_profile <= 0)
                    {
                        system_err = pgad.system_err;
                        err_msg = "Failed to Create new Merchant Profile: " + RegistrationName;
                        return false;
                    }

                    Do_InsertUpdate_BusinessOwner(new_biz_profile, D_FullName_i, D_ID_i, D_FullName_ii, D_ID_ii, D_FullName_iii, D_ID_iii,
                                                D_FullName_vi, D_ID_vi, D_FullName_v, D_ID_v, S_FullName_i, S_ID_i, S_FullName_ii, S_ID_ii, S_FullName_iii,
                                                S_ID_iii, S_FullName_vi, S_ID_vi, S_FullName_v, S_ID_v, CreateBy);


                    var new_merchantBank = pgad.CMS_MerchantBank_InsertUpdate(checkDomain[0].DomainID, BankName, BankAccHolder, BankAccNo, SWIFTCode, BankAddress, BankCountry, CreateBy);

                    //Added by DANIEL 11 Jan 2019. Retrieve PgadDal error message - START
                    system_err = pgad.system_err;
                    if (!string.IsNullOrEmpty(system_err))
                    {
                        err_msg = "Failed to Create new Merchant Profile: " + RegistrationName;
                        return false;
                    }
                    //Added by DANIEL 11 Jan 2019. Retrieve PgadDal error message - END

                    var new_UserGroup = pgad.CMS_UserGroup_Insert(checkDomain[0].DomainID, "Administrator", "Administrator", CreateBy);
                    if (new_UserGroup.RetVal == 0)
                    {
                        //Modified by DANIEL 26 Feb 2019. Convert default pwd to secureString request by PCI
                        //string defaultPwd = aes.EncryptData("user12345", PWKey);
                        //string defaultPwd = aes.EncryptData(new NetworkCredential("", secureDefPwd).Password, new NetworkCredential("", PWKey).Password)

                        var new_User = pgad.CMS_User_Insert(ACP1Email, aes.EncryptData(new NetworkCredential("", secureDefPwd).Password, new NetworkCredential("", PWKey).Password), 0, 0, checkDomain[0].DomainID, new_UserGroup.GroupID, CreateBy, true);
                        if (!string.IsNullOrEmpty(pgad.system_err))
                        {
                            err_msg = "Failed to add merchant. Please contact administrator.";
                            return false;
                        }
                        var defaultPermission = pgad.CMS_UserGroupMap_UserMap_Insert(new_UserGroup.GroupID, ACP1Email);
                        if (!string.IsNullOrEmpty(pgad.system_err))
                        {
                            err_msg = "Failed to add merchant. Please contact administrator.";
                            return false;
                        }
                    }
                    else
                    {
                        err_msg = "Failed to create user group.";
                        system_err = new_UserGroup.RetVal.ToString();
                        return false;
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                system_err = ex.Message.ToString();
                err_msg = "Failed to add merchant. Please contact administrator.";
                return false;
            }
        }

        public bool Do_InsertUpdate_BusinessOwner(int BusinessProfileID, string D_FullName_i, string D_ID_i, string D_FullName_ii, string D_ID_ii, string D_FullName_iii, string D_ID_iii,
            string D_FullName_vi, string D_ID_vi, string D_FullName_v, string D_ID_v, string S_FullName_i, string S_ID_i, string S_FullName_ii, string S_ID_ii, string S_FullName_iii,
            string S_ID_iii, string S_FullName_vi, string S_ID_vi, string S_FullName_v, string S_ID_v, string ModifiedBy)
        {
            try
            {
                //Modified by DANIEL 25 Jan 2019. Check for error in PGAdminDaL - START
                //if (!string.IsNullOrEmpty(D_FullName_i) || !string.IsNullOrEmpty(D_ID_i)) pgad.CMS_BusinessOwner_InsertUpdate(BusinessProfileID, D_FullName_i, D_ID_i, "Director/ Proprietor", 1, ModifiedBy);
                //if (!string.IsNullOrEmpty(D_FullName_ii) || !string.IsNullOrEmpty(D_ID_ii)) pgad.CMS_BusinessOwner_InsertUpdate(BusinessProfileID, D_FullName_ii, D_ID_ii, "Director/ Proprietor", 2, ModifiedBy);
                //if (!string.IsNullOrEmpty(D_FullName_iii) || !string.IsNullOrEmpty(D_ID_iii)) pgad.CMS_BusinessOwner_InsertUpdate(BusinessProfileID, D_FullName_iii, D_ID_iii, "Director/ Proprietor", 3, ModifiedBy);
                //if (!string.IsNullOrEmpty(D_FullName_vi) || !string.IsNullOrEmpty(D_ID_vi)) pgad.CMS_BusinessOwner_InsertUpdate(BusinessProfileID, D_FullName_vi, D_ID_vi, "Director/ Proprietor", 4, ModifiedBy);
                //if (!string.IsNullOrEmpty(D_FullName_v) || !string.IsNullOrEmpty(D_ID_v)) pgad.CMS_BusinessOwner_InsertUpdate(BusinessProfileID, D_FullName_v, D_ID_v, "Director/ Proprietor", 5, ModifiedBy);

                //if (!string.IsNullOrEmpty(S_FullName_i) || !string.IsNullOrEmpty(S_ID_i)) pgad.CMS_BusinessOwner_InsertUpdate(BusinessProfileID, S_FullName_i, S_ID_i, "Share Holder", 1, ModifiedBy);
                //if (!string.IsNullOrEmpty(S_FullName_ii) || !string.IsNullOrEmpty(S_ID_ii)) pgad.CMS_BusinessOwner_InsertUpdate(BusinessProfileID, S_FullName_ii, S_ID_ii, "Share Holder", 2, ModifiedBy);
                //if (!string.IsNullOrEmpty(S_FullName_iii) || !string.IsNullOrEmpty(S_ID_iii)) pgad.CMS_BusinessOwner_InsertUpdate(BusinessProfileID, S_FullName_iii, S_ID_iii, "Share Holder", 3, ModifiedBy);
                //if (!string.IsNullOrEmpty(S_FullName_vi) || !string.IsNullOrEmpty(S_ID_vi)) pgad.CMS_BusinessOwner_InsertUpdate(BusinessProfileID, S_FullName_vi, S_ID_vi, "Share Holder", 4, ModifiedBy);
                //if (!string.IsNullOrEmpty(S_FullName_v) || !string.IsNullOrEmpty(S_ID_v)) pgad.CMS_BusinessOwner_InsertUpdate(BusinessProfileID, S_FullName_v, S_ID_v, "Share Holder", 5, ModifiedBy);

                //if (!string.IsNullOrEmpty(D_FullName_i) || !string.IsNullOrEmpty(D_ID_i))
                //{
                //    pgad.CMS_BusinessOwner_InsertUpdate(BusinessProfileID, D_FullName_i, D_ID_i, "Director/ Proprietor", 1, ModifiedBy);
                //    if()
                //}
                if (!string.IsNullOrEmpty(D_FullName_ii) || !string.IsNullOrEmpty(D_ID_ii))
                {
                    pgad.CMS_BusinessOwner_InsertUpdate(BusinessProfileID, D_FullName_ii, D_ID_ii, "Director/ Proprietor", 2, ModifiedBy);
                    system_err = pgad.system_err;
                    if (!string.IsNullOrEmpty(system_err))
                    {
                        err_msg = "Failed to add merchant. Please contact administrator.";
                        return false;
                    }
                }

                if (!string.IsNullOrEmpty(D_FullName_iii) || !string.IsNullOrEmpty(D_ID_iii))
                {
                    pgad.CMS_BusinessOwner_InsertUpdate(BusinessProfileID, D_FullName_iii, D_ID_iii, "Director/ Proprietor", 3, ModifiedBy);
                    system_err = pgad.system_err;
                    if (!string.IsNullOrEmpty(system_err))
                    {
                        err_msg = "Failed to add merchant. Please contact administrator.";
                        return false;
                    }
                }

                if (!string.IsNullOrEmpty(D_FullName_vi) || !string.IsNullOrEmpty(D_ID_vi))
                {
                    pgad.CMS_BusinessOwner_InsertUpdate(BusinessProfileID, D_FullName_vi, D_ID_vi, "Director/ Proprietor", 4, ModifiedBy);
                    system_err = pgad.system_err;
                    if (!string.IsNullOrEmpty(system_err))
                    {
                        err_msg = "Failed to add merchant. Please contact administrator.";
                        return false;
                    }
                }

                if (!string.IsNullOrEmpty(D_FullName_v) || !string.IsNullOrEmpty(D_ID_v))
                {
                    pgad.CMS_BusinessOwner_InsertUpdate(BusinessProfileID, D_FullName_v, D_ID_v, "Director/ Proprietor", 5, ModifiedBy); system_err = pgad.system_err;
                    if (!string.IsNullOrEmpty(system_err))
                    {
                        err_msg = "Failed to add merchant. Please contact administrator.";
                        return false;
                    }
                }

                if (!string.IsNullOrEmpty(S_FullName_i) || !string.IsNullOrEmpty(S_ID_i))
                {
                    pgad.CMS_BusinessOwner_InsertUpdate(BusinessProfileID, S_FullName_i, S_ID_i, "Share Holder", 1, ModifiedBy);
                    system_err = pgad.system_err;
                    if (!string.IsNullOrEmpty(system_err))
                    {
                        err_msg = "Failed to add merchant. Please contact administrator.";
                        return false;
                    }
                }

                if (!string.IsNullOrEmpty(S_FullName_ii) || !string.IsNullOrEmpty(S_ID_ii))
                {
                    pgad.CMS_BusinessOwner_InsertUpdate(BusinessProfileID, S_FullName_ii, S_ID_ii, "Share Holder", 2, ModifiedBy);
                    system_err = pgad.system_err;
                    if (!string.IsNullOrEmpty(system_err))
                    {
                        err_msg = "Failed to add merchant. Please contact administrator.";
                        return false;
                    }
                }

                if (!string.IsNullOrEmpty(S_FullName_iii) || !string.IsNullOrEmpty(S_ID_iii))
                {
                    pgad.CMS_BusinessOwner_InsertUpdate(BusinessProfileID, S_FullName_iii, S_ID_iii, "Share Holder", 3, ModifiedBy); system_err = pgad.system_err;
                    if (!string.IsNullOrEmpty(system_err))
                    {
                        err_msg = "Failed to add merchant. Please contact administrator.";
                        return false;
                    }
                }

                if (!string.IsNullOrEmpty(S_FullName_vi) || !string.IsNullOrEmpty(S_ID_vi))
                {
                    pgad.CMS_BusinessOwner_InsertUpdate(BusinessProfileID, S_FullName_vi, S_ID_vi, "Share Holder", 4, ModifiedBy); system_err = pgad.system_err;
                    if (!string.IsNullOrEmpty(system_err))
                    {
                        err_msg = "Failed to add merchant. Please contact administrator.";
                        return false;
                    }
                }

                if (!string.IsNullOrEmpty(S_FullName_v) || !string.IsNullOrEmpty(S_ID_v))
                {
                    pgad.CMS_BusinessOwner_InsertUpdate(BusinessProfileID, S_FullName_v, S_ID_v, "Share Holder", 5, ModifiedBy); system_err = pgad.system_err;
                    if (!string.IsNullOrEmpty(system_err))
                    {
                        err_msg = "Failed to add merchant. Please contact administrator.";
                        return false;
                    }
                }
                //Modified by DANIEL 25 Jan 2019. Check for error in PGAdminDaL - END
                return true;
            }
            catch (Exception ex)
            {
                system_err = ex.Message.ToString();
                err_msg = "Failed to add merchant. Please contact administrator.";
                return false;
            }
        }

        //public MerchantInfo Do_GetMerchantInfo(int PDomainID).    Modified by DANIEL 11 Mar 2019. Add parameter isPreset True: To bind account if is SuperAdmin
        public MerchantInfo Do_GetMerchantInfo(int PDomainID, bool isPreset = false)
        {
            try
            {
                var domain_D = pgad.Get_CMS_Domain(1, PDomainID, " ");
                system_err = pgad.system_err;
                if (system_err != string.Empty)
                    return null;
                else
                {
                    if (domain_D == null)
                    {
                        err_msg = "Invalid Merchant ID";
                        return null;
                    }
                }

                //MerchantInfo merchantInfo = pgad.Get_MerchantInfo(PDomainID);     Modified by DANIEL 11 Mar 2019. Add parameter isPreset True: To bind account if is SuperAdmin
                MerchantInfo merchantInfo = pgad.Get_MerchantInfo(PDomainID, isPreset);

                //Added by DANIEL 10 Jan 2019. Get PGAdminDal error message - START
                system_err = pgad.system_err;
                if (!string.IsNullOrEmpty(system_err))
                {
                    err_msg = "An error occured. Please contact administrator.";
                    return null;
                }
                else
                {
                    return merchantInfo;
                }
                //Added by DANIEL 10 Jan 2019. Get PGAdminDal error message - END
            }
            catch (Exception ex)
            {
                system_err = ex.Message.ToString();
                err_msg = "Failed to get merchant info. Please contact administrator.";
                return null;
            }
        }

        public string Do_GetMerchantPwd(string PUserID, string PMerchantID)
        {
            try
            {
                //var merchantPwd = pgad.Get_MerchantPwd(PUserID, PMerchantID);     Modified by DANIEL 2 Apr 2019. Implement SecureString request by PCI
                SecureString secMerchantPwd = new NetworkCredential("", pgad.Get_MerchantPwd(PUserID, PMerchantID)).SecurePassword;

                system_err = pgad.system_err;
                if (system_err != string.Empty)
                {
                    return null;
                }
                else
                {
                    //return merchantPwd;   Modified by DANIEL 2 Apr 2019. Implement SecureString request by PCI
                    return new NetworkCredential("", secMerchantPwd).Password;
                }
            }
            catch (Exception ex)
            {
                system_err = ex.Message.ToString();
                err_msg = "Failed to get merchant password. Please contact administrator.";
                return null;
            }
        }

        public DomainMerchant[] Do_GetMerchantList(int PMerchantID)
        {
            try
            {
                var merchantList = pgad.Get_CMS_Domain_Merchant(PMerchantID);
                system_err = pgad.system_err;
                if (system_err != string.Empty)
                {
                    err_msg = "Failed to get merchant information. Please contact administrator.";      //Added by DANIEL 10 Jan 2019
                    return null;
                }
                else
                {
                    return merchantList;
                }
            }
            catch (Exception ex)
            {
                system_err = ex.Message.ToString();
                err_msg = "An error occured. Please contact administrator.";
                return null;
            }
        }

        public bool Do_Domain_Update_Status(int PDomainID, int PStatus, string PModifyBy)
        {
            try
            {
                bool rst = pgad.Update_CMS_Domain_Status(PDomainID, PStatus, PModifyBy);
                if (!rst)
                {
                    system_err = pgad.system_err;
                    err_msg = "Failed to update domain status.";
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                system_err = ex.Message.ToString();
                err_msg = "An error occured. Please contact administrator.";
                return false;
            }
        }

        //Modified by DANIEL 16 Jan 2019. Add in isReseller = false
        //public bool Insert_MerchantRate(decimal FromAmt, decimal ToAmt, string MerchantID, string PymtMethod, int HostID, DateTime StartDate, decimal Rate, decimal FixedRate, string PCurrency)
        public bool Insert_MerchantRate(decimal FromAmt, decimal ToAmt, string MerchantID, string PymtMethod, int HostID, DateTime StartDate, decimal Rate, decimal FixedRate, string PCurrency, bool isReseller = false)
        {
            try
            {
                //var newMerchantRate = pgd.MerchantRate_Insert(FromAmt, ToAmt, MerchantID, PymtMethod, HostID, StartDate, Rate, FixedRate, PCurrency);

                //Modified by DANIEL 4 Dec 2018. Better improve validation - START
                //if (Rate != 0 && FixedRate != 0)
                //    pgd.MerchantRate_Insert(FromAmt, ToAmt, MerchantID, PymtMethod, HostID, StartDate, Rate, FixedRate, PCurrency);

                //Modified by DANIEL 5 Dec 2018. Change reference from pgd to pgad - START
                //if(HostID != 0)
                //{
                //    if (PymtMethod.Equals("CC"))
                //    {
                //        if (Rate > 0 && FixedRate >= 0)
                //        {
                //            Rate = Rate / 100;
                //            pgd.MerchantRate_Insert(FromAmt, ToAmt, MerchantID, PymtMethod, HostID, StartDate, Rate, FixedRate, PCurrency);
                //        }
                //        else
                //        {
                //            err_msg = "Failed to add merchant rate.";
                //            return false;
                //        }
                //    }
                //    else
                //    {
                //        if (Rate > 0 && FixedRate > 0)
                //        {
                //            Rate = Rate / 100;
                //            pgd.MerchantRate_Insert(FromAmt, ToAmt, MerchantID, PymtMethod, HostID, StartDate, Rate, FixedRate, PCurrency);
                //        }
                //        else
                //        {
                //            err_msg = "Failed to add merchant rate.";
                //            return false;
                //        }
                //    }
                //}
                //else
                //{
                //    err_msg = "Failed to add merchant rate.";
                //    return false;
                //}

                if (HostID != 0)
                {
                    //Modified by DANIEL 17 Desc 2018. Change conditional statement for both OB and CC . If both Rate and Fixed Rate = 0 or -1 , return false - START
                    //if (PymtMethod.Equals("CC"))
                    //{
                    //    if (Rate > 0 && FixedRate >= 0)
                    //    {
                    //        Rate = Rate / 100;
                    //        pgad.MerchantRate_Insert(FromAmt, ToAmt, MerchantID, PymtMethod, HostID, StartDate, Rate, FixedRate, PCurrency);
                    //    }
                    //    else
                    //    {
                    //        err_msg = "Failed to add merchant rate.";
                    //        return false;
                    //    }
                    //}
                    //else
                    //{
                    //    if (Rate > 0 && FixedRate > 0)
                    //    {
                    //        Rate = Rate / 100;
                    //        pgad.MerchantRate_Insert(FromAmt, ToAmt, MerchantID, PymtMethod, HostID, StartDate, Rate, FixedRate, PCurrency);
                    //    }
                    //    else
                    //    {
                    //        err_msg = "Failed to add merchant rate.";
                    //        return false;
                    //    }
                    //}

                    //Commented by KENT LOONG 19 Feb 2019. REMOVE check 0 - START
                    //if (Rate < 0 || FixedRate < 0)
                    //{
                    //    err_msg = "Failed to add merchant rate.";
                    //    return false;
                    //}
                    //else if (Rate == 0 && FixedRate == 0)
                    //{
                    //    err_msg = "Failed to add merchant rate.";
                    //    return false;
                    //}
                    //else
                    //{
                    //Commented by KENT LOONG 19 Feb 2019. REMOVE check 0 - END
                    Rate = Rate / 100;

                    //Modified by DANIEL 16 Jan 2019. Add isReseller
                    //int insertMerchantRate = pgad.MerchantRate_Insert(FromAmt, ToAmt, MerchantID, PymtMethod, HostID, StartDate, Rate, FixedRate, PCurrency);
                    int insertMerchantRate = pgad.MerchantRate_Insert(FromAmt, ToAmt, MerchantID, PymtMethod, HostID, StartDate, Rate, FixedRate, PCurrency, isReseller);

                    if (insertMerchantRate <= 0)
                    {
                        system_err = pgad.system_err;
                        err_msg = "Failed to add merchant rate.";
                        return false;
                    }

                    //}
                    //Modified by DANIEL 17 Desc 2018. Change conditional statement for both OB and CC . If both Rate and Fixed Rate = 0 or -1 , return false - END
                }
                else
                {
                    err_msg = "Failed to add merchant rate.";
                    return false;
                }
                //Modified by DANIEL 4 Dec 2018. Better improve validation - END
                //Modified by DANIEL 5 Dec 2018. Change reference from pgd to pgad - END

                return true;
            }
            catch (Exception ex)
            {
                system_err = ex.Message.ToString();
                err_msg = "Failed to add merchant rate. Please contact administrator.";
                return false;
            }
        }

        //public MerchantRateHost[] Do_Get_MerchantRateHost(int PDomainID)      Modified by DANIEL 14 Jan 2019. Add parameter isReseller = 0: False, 1 =  True
        public MerchantRateHost[] Do_Get_MerchantRateHost(int PDomainID, int isReseller = 0)
        {
            try
            {
                //var merchantRateHost = pgad.Get_MerchantRateHost(PDomainID);      Modified by DANIEL 14 Jan 2019. Add parameter isReseller
                var merchantRateHost = pgad.Get_MerchantRateHost(PDomainID, isReseller);

                system_err = pgad.system_err;
                if (system_err != string.Empty)
                {
                    err_msg = "Failed to get merchant rate. Please contact administrator.";     //Added by DANIEL 10 Jan 2019. 
                    return null;
                }
                else
                    return merchantRateHost;
            }
            catch (Exception ex)
            {
                system_err = ex.Message.ToString();
                err_msg = "An error occured. Please contact administrator.";
                return null;
            }
        }

        //Modified by DANIEL 6 Dec 2018. Remove parameter PMode
        //Modified by DANIEL 5 Dec 2018. add new parameter PPymtMethod ALL, OB, CC | PMode = 0 : Default , PMode = 1 : Top 1 Selection
        //public TerminalHost[] Do_Get_TerminalHost(int PDomainID)
        //public TerminalHost[] Do_Get_TerminalHost(int PDomainID, int PMode = 0, string PPymtMethod = "ALL")

        //Modified by DANIEL 26 Dec 2018. Add parameter PStatusYesNo = 0: Return by Status, PStatusYesNo = 1 : Do not return by status
        //public TerminalHost[] Do_Get_TerminalHost(int PDomainID, string PPymtMethod = "ALL")
        public TerminalHost[] Do_Get_TerminalHost(int PDomainID, string PPymtMethod = "ALL", int PStatusYesNo = 0)
        {
            try
            {
                //Modified by DANIEL 5 Dec 2018 . Add new parameter PMode, PPymtMethod. 6 Dec 2018 Remove PMode
                //return pgad.Get_TerminalHost(PDomainID)
                //return pgad.Get_TerminalHost(PDomainID, PMode, PPymtMethod);
                var terminalHost = pgad.Get_TerminalHost(PDomainID, PPymtMethod, PStatusYesNo);

                system_err = pgad.system_err;
                if (system_err != string.Empty)
                {
                    err_msg = "An error occured. Please contact administrator.";        //Added by DANIEL 10 Jan 2019 
                    return null;
                }
                else
                {
                    return terminalHost;
                }
            }
            catch (Exception ex)
            {
                system_err = ex.Message.ToString();
                err_msg = "An error occured. Please contact administrator.";
                return null;
            }
        }

        public BusinessOwner[] Do_Get_BusinessOwners(int PBusinessProfileID)
        {
            try
            {
                var businessOwner = pgad.CMS_BusinessOwner_Get(PBusinessProfileID);
                system_err = pgad.system_err;
                if (system_err != string.Empty)
                    return null;
                else
                    return businessOwner;
            }
            catch (Exception ex)
            {
                system_err = ex.Message.ToString();
                err_msg = "An error occured. Please contact administrator.";
                return null;
            }
        }

        //Modified by SUKI 28 Dec 20181. Add in showMerchantAddr, showMerchantLogo, IsAgent, default currency setting
        //public bool Do_Update_Merchant(string merchantID, string TradingName, string RegistrationName, string MCCCode, string ACP1Name, string ACP1Mobile,
        //    string ACP1Email, string CSEmail, string CSMobile, string Addr1, string Addr2, string Addr3, int CountryID, string City, string State, string PostCode, string WebSiteURL,
        //    int PluginID, int PymtNotificationEmail, string NotifyEmailAddr, string CompanyRegNo, string GSTNo, string TypeOfBusiness, string OfficeNumber, string ACPName,
        //    string ACPContactNo, string ACPEmail, string BillContactName, string BillContactNo, string BillContactEmail, string TypeOfProducts, string TargetMarkets,
        //    string DeliveredPeriod, string CreateBy, string D_FullName_i, string D_ID_i, string D_FullName_ii, string D_ID_ii, string D_FullName_iii, string D_ID_iii,
        //    string D_FullName_vi, string D_ID_vi, string D_FullName_v, string D_ID_v, string S_FullName_i, string S_ID_i, string S_FullName_ii, string S_ID_ii, string S_FullName_iii,
        //    string S_ID_iii, string S_FullName_vi, string S_ID_vi, string S_FullName_v, string S_ID_v, string BankName, string BankAccHolder, string BankAccNo, string SWIFTCode,
        //    string BankAddress, string BankCountry, decimal TxnLimit, int BusinessProfileID)
        public bool Do_Update_Merchant(string merchantID, string TradingName, string RegistrationName, string MCCCode, string ACP1Name, string ACP1Mobile,
            string ACP1Email, string CSEmail, string CSMobile, string Addr1, string Addr2, string Addr3, int CountryID, string City, string State, string PostCode, string WebSiteURL,
            int PluginID, int PymtNotificationEmail, string NotifyEmailAddr, string CompanyRegNo, string GSTNo, string TypeOfBusiness, string OfficeNumber, string ACPName,
            string ACPContactNo, string ACPEmail, string BillContactName, string BillContactNo, string BillContactEmail, string TypeOfProducts, string TargetMarkets,
            string DeliveredPeriod, string CreateBy, string D_FullName_i, string D_ID_i, string D_FullName_ii, string D_ID_ii, string D_FullName_iii, string D_ID_iii,
            string D_FullName_vi, string D_ID_vi, string D_FullName_v, string D_ID_v, string S_FullName_i, string S_ID_i, string S_FullName_ii, string S_ID_ii, string S_FullName_iii,
            string S_ID_iii, string S_FullName_vi, string S_ID_vi, string S_FullName_v, string S_ID_v, string BankName, string BankAccHolder, string BankAccNo, string SWIFTCode,
            string BankAddress, string BankCountry, decimal TxnLimit, int BusinessProfileID, int showMerchantAddr, int showMerchantLogo, int isAgent, string defaultCurrency)
        {
            try
            {
                //Added by DANIEL 28 Mar 2019. Check for existing email - START
                var checkMer = pgad.Merchant_SelectDetail(merchantID);
                var checkEmail = pgad.Merchant_SelectDetailByEmail(ACP1Email);
                if (!checkMer.MainContactEmail.ToLower().Trim().Equals(ACP1Email.ToLower().Trim()))
                {
                    if (checkEmail != null)
                    {
                        if (ACP1Email.ToLower().Trim().Equals(checkEmail.UserID.ToLower().Trim()))
                        {
                            system_err = pgad.system_err;
                            err_msg = "Existing Email = " + ACP1Email;
                            return false;
                        }
                    }
                }
                //Added by DANIEL 28 Mar 2019. Check for existing email - END

                var updMerchant = pgad.Merchant_Update(merchantID, TradingName, RegistrationName, MCCCode, ACP1Name, ACP1Mobile, ACP1Email, CSEmail, CSMobile,
                    Addr1, Addr2, Addr3, CountryID, City, State, PostCode, WebSiteURL, PluginID, PymtNotificationEmail, NotifyEmailAddr, TxnLimit);
                if (updMerchant != true)
                {
                    system_err = pgad.system_err;
                    err_msg = "Failed to update Merchant details.";
                    return false;
                }

                //Modified by SUKI 28 Nov 2018. Add in showMerchantAddr, showMerchantLogo, IsAgent, default currency setting
                //var updMerchantGroup = pgad.Update_MerchantGroup(merchantID, PluginID);

                //Modified by DANIEL 16 Jan 2019. Add in showMerchantAddr, showMerchantLogo
                var updateMerchantSetting = pgad.Update_MerchantSetting(merchantID, PluginID, isAgent, defaultCurrency, showMerchantAddr, showMerchantLogo);

                //Added by DANIEL 11 Jan 2019. If null, get PGADDal error message - START
                if (updateMerchantSetting != true)
                {
                    system_err = pgad.system_err;
                    err_msg = "Failed to update Merchant details.";
                    return false;
                }
                //Added by DANIEL 11 Jan 2019. If null, get PGADDal error message - END

                var checkDomain = pgad.Get_CMS_Domain(99, 0, merchantID);
                if (checkDomain != null)
                {
                    var upd_businessProfile = pgad.CMS_BusinessProfile_Insert(checkDomain[0].DomainID, CompanyRegNo, GSTNo, TypeOfBusiness, OfficeNumber, ACPName, ACPContactNo, ACPEmail,
                        BillContactName, BillContactNo, BillContactEmail, TypeOfProducts, TargetMarkets, DeliveredPeriod, CreateBy);
                    if (upd_businessProfile <= 0)
                    {
                        system_err = pgad.system_err;
                        err_msg = "Failed to update Merchant Profile.";
                        return false;
                    }

                    Do_InsertUpdate_BusinessOwner(BusinessProfileID, D_FullName_i, D_ID_i, D_FullName_ii, D_ID_ii, D_FullName_iii, D_ID_iii,
                                                    D_FullName_vi, D_ID_vi, D_FullName_v, D_ID_v, S_FullName_i, S_ID_i, S_FullName_ii, S_ID_ii, S_FullName_iii,
                                                    S_ID_iii, S_FullName_vi, S_ID_vi, S_FullName_v, S_ID_v, CreateBy);

                    var upd_merchantBank = pgad.CMS_MerchantBank_InsertUpdate(checkDomain[0].DomainID, BankName, BankAccHolder, BankAccNo, SWIFTCode, BankAddress, BankCountry, CreateBy);

                    //Added by DANIEL 11 Jan 2019. Retrieve PgadDal error message - START
                    system_err = pgad.system_err;
                    if (!string.IsNullOrEmpty(system_err))
                    {
                        err_msg = "Failed to Create new Merchant Profile: " + RegistrationName;
                        return false;
                    }
                    //Added by DANIEL 11 Jan 2019. Retrieve PgadDal error message - END

                    //Commented by DANIEL 11 Jan 2019. This code will cause error
                    //if (upd_merchantBank <= 0)
                    //{
                    //    system_err = pgad.system_err;
                    //    return false;
                    //}

                    var chk_User = pgad.CMS_User_SelectDetail(ACP1Email);
                    if (chk_User == null)
                    {
                        int groupID = 0;
                        var chk_UserGroup = pgad.CMS_UserGroup_By_DomainID(checkDomain[0].DomainID);
                        foreach (var userGroup in chk_UserGroup)
                        {
                            if (userGroup.GroupName == "Administrator")
                            {
                                groupID = userGroup.GroupID;
                                break;
                            }
                        }

                        //string defaultPwd = aes.EncryptData("user12345", PWKey);      Modified by DANIEL 26 Feb 2019. Convert default pwd to secureString request by PCI
                        string defaultPwd = aes.EncryptData(new NetworkCredential("", secureDefPwd).Password, new NetworkCredential("", PWKey).Password);

                        var new_User = pgad.CMS_User_Insert(ACP1Email, defaultPwd, 0, 0, checkDomain[0].DomainID, groupID, CreateBy);

                        //Commeneted by DANIEL 11 Feb 2019. Change to better validation method
                        //if (new_User <= 0)
                        //{
                        //    system_err = pgad.system_err;
                        //    return false;
                        //}

                        system_err = pgad.system_err;
                        if (system_err != string.Empty)
                        {
                            err_msg = "Failed to update Authorized Contact Person 1's Email.";
                            return false;
                        }
                        else
                        {
                            if (new_User.RetVal == 0)
                            {
                                return true;
                            }
                            else
                            {
                                err_msg = "Failed to update Authorized Contact Person 1's Email.";
                                return false;
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                system_err = ex.Message.ToString();
                err_msg = "Failed to update merchant. Please contact administrator.";
                return false;
            }
        }

        public CL_PymtMethod[] Do_GetCLPymtMethod()
        {
            try
            {
                var pymtMethod = pgad.GetCLPymtMethod();
                system_err = pgad.system_err;
                if (system_err != string.Empty)
                    return null;
                else
                    return pymtMethod;
            }
            catch (Exception ex)
            {
                err_msg = ex.Message;
                return null;
            }
        }

        //public ChannelHost[] Do_Get_HostByType(string PPymtMethod)
        public ChannelHost[] Do_Get_HostByType(string PPymtMethod, string PCurrencyCode = "") // Modified By Daniel 23 Apr 2019 . Add PCurrencyCode.
        {
            try
            {

                //var host = pgad.Get_HostByType(PPymtMethod);
                ChannelHost[] host = null; //Modified by Daniel 23 Apr 2019. START
                if (!string.IsNullOrEmpty(PCurrencyCode))
                    host = pgad.Get_HostByCurrency_PymtMethod(PPymtMethod, PCurrencyCode);
                else
                    host = pgad.Get_HostByType(PPymtMethod); //END

                system_err = pgad.system_err;
                if (system_err != string.Empty)
                {
                    err_msg = "An error occured. Please contact administrator.";        //Added by DANIEL 11 Jan 2019. 
                    return null;
                }
                else
                {
                    return host;
                }
            }
            catch (Exception ex)
            {
                system_err = ex.Message.ToString();
                err_msg = "An error occured. Please contact administrator.";
                return null;
            }
        }

        //Commented by SUKI 12 Dec 2018. Unnecessary code
        ////Added by DANIEL 3 Dec 2018. Add new method to get host by type and domain id
        //public ChannelHost[] Do_Get_HostByType_DomainID(string PPymtMethod, int PDomainID)
        //{
        //    try
        //    {
        //        return pgad.Get_HostByType_DomainID(PPymtMethod, PDomainID);
        //    }
        //    catch (Exception ex)
        //    {
        //        system_err = ex.Message.ToString();
        //        err_msg = "An error occured. Please contact administrator.";
        //        return null;
        //    }
        //}

        //public HostCredential[] Do_GetHostCredential(string PPymtMethodCode, int HostID)
        public HostCredential[] Do_GetHostCredential(string PPymtMethodCode, int HostID, string PCurrencyCode)// Modified By Daniel 23 APR 2019. Add PCurrencyCode parameters
        {
            try
            {
                var hostCredential = pgad.Get_HostCredential(PPymtMethodCode, HostID, PCurrencyCode);
                system_err = pgad.system_err;
                if (system_err != string.Empty)
                    return null;
                else
                    return hostCredential;
            }
            catch (Exception ex)
            {
                system_err = ex.Message.ToString();
                err_msg = "An error occured. Please contact administrator.";
                return null;
            }
        }

        //public CurrencyCode[] Do_GetCurrencyCountry(int PMode)
        public CurrencyCode[] Do_GetCurrencyCountry(int PMode, string PPymtMethod = "") // Modified by Daniel 23 Apr 2019. Add PPymtMethod param
        {
            try
            {
                //Remarks= PMode 0: have "ALL", 1: no "ALL" 2: Get currency from Terminals_PreSetCredentials

                //START--Modified by Daniel 23 Apr 2019. If PMode 2 ,Get currency from Terminals_PreSetCredentials. 
                //var currency = pgad.Get_CurrencyCountry(PMode); 
                CurrencyCode[] currency = null;
                if (PMode == 2)
                    currency = pgad.GetCurrency_PreSetCredentials(PPymtMethod);
                else
                    currency = pgad.Get_CurrencyCountry(PMode);
                // END--
                system_err = pgad.system_err;
                if (system_err != string.Empty)
                    return null;
                else
                    return currency;
            }
            catch (Exception ex)
            {
                system_err = ex.Message.ToString();
                err_msg = "An error occured. Please contact administrator.";
                return null;
            }
        }

        public PGMCardGroup[] Do_GetPGMCardGroup()
        {
            try
            {
                var cardGroup = pgad.Get_PGMCardGroup();
                system_err = pgad.system_err;
                if (system_err != string.Empty)
                    return null;
                else
                    return cardGroup;
            }
            catch (Exception ex)
            {
                system_err = ex.Message.ToString();
                err_msg = "An error occured. Please contact administrator.";
                return null;
            }
        }

        public CardTypeProfile[] Do_GetCardTypeProfile(int PDomainID)
        {
            try
            {
                var cardType = pgad.Get_CardTypeProfile(PDomainID);
                system_err = pgad.system_err;
                if (system_err != string.Empty)
                    return null;
                else
                    return cardType;
            }
            catch (Exception ex)
            {
                system_err = ex.Message.ToString();
                err_msg = "An error occured. Please contact administrator.";
                return null;
            }
        }

        public string Do_Encrypt3DES(string encryptTxt)
        {
            try
            {

                var encrypt = aes.szEncrypt3DES(encryptTxt, new NetworkCredential("", terminalKey).Password, new NetworkCredential("", terminalVector).Password);
                system_err = pgad.system_err;
                if (system_err != string.Empty)
                    return null;
                else
                    return encrypt;
            }
            catch (Exception ex)
            {
                system_err = ex.Message.ToString();
                err_msg = "Failed to encrypt credential. Please contact administrator.";
                return null;
            }
        }

        public string Do_Decrypt3DES(string decryptTxt)
        {
            try
            {
                var decrypt = aes.szDecrypt3DES(decryptTxt, new NetworkCredential("", terminalKey).Password, new NetworkCredential("", terminalVector).Password);
                system_err = pgad.system_err;
                if (system_err != string.Empty)
                    return null;
                else
                    return decrypt;
            }
            catch (Exception ex)
            {
                system_err = ex.Message.ToString();
                err_msg = "An error occured. Please contact administrator.";
                return null;
            }
        }

        public string Do_Decrypt3DEStoHex(string decryptTxt)
        {
            try
            {
                var hexDecrypt = aes.szDecrypt3DEStoHex(decryptTxt, new NetworkCredential("", terminalKey).Password, new NetworkCredential("", terminalVector).Password);
                system_err = pgad.system_err;
                if (system_err != string.Empty)
                    return null;
                else
                    return hexDecrypt;
            }
            catch (Exception ex)
            {
                system_err = ex.Message.ToString();
                err_msg = "An error occured. Please contact administrator.";
                return null;
            }
        }

        public bool Do_AddChannel(int PDomainID, string PPymtMethod, int PHostID, string PFieldName1, string PCredential1, string PFieldName2, string PCredential2,
            string PFieldName3, string PCredential3, string PFieldName4, string PCredential4, string PFieldName5, string PCredential5, string PFieldName6, string PCredential6,
            string PCurrencyCode, int PPriority, string PCardGroup, int PVisa, int PMasterCard, int PAMEX, int PDiners, int PJCB, int PCUP, int PMasterPass, int PVisaCheckout,
            int PSamsungPay, int mode)
        {
            try
            {
                // Insert OB,PG..Terminals
                var newChannel = pgad.Terminal_Insert(PDomainID, PPymtMethod, PHostID, PFieldName1, PCredential1, PFieldName2, PCredential2,
                    PFieldName3, PCredential3, PFieldName4, PCredential4, PFieldName5, PCredential5, PFieldName6, PCredential6, PCurrencyCode, PPriority);
                if (newChannel == 0)
                {

                    // Insert PG..PGM_Routing
                    if (PPymtMethod == "CC")
                    {
                        string[] cardGroupList = PCardGroup.Split(',');
                        foreach (var cardGroup in cardGroupList)
                        {
                            var newCardGroup = pgad.PGM_Routing_Insert(PDomainID, PHostID, int.Parse(cardGroup), PCurrencyCode, PPriority);
                            system_err = pgad.system_err;
                            if (system_err != string.Empty)
                            {
                                err_msg = "Failed to create terminal.";     //"Failed to update terminal.";     Modified by DANIEL 8 Jan 2019
                                return false;
                            }
                        }

                        // Insert PG..CardTypeProfile
                        var newCardTypeProfile = pgad.CardTypeProfile_Insert(PDomainID, PVisa, PMasterCard, PAMEX, PDiners, PJCB, PCUP, PMasterPass, PVisaCheckout, PSamsungPay);
                        system_err = pgad.system_err;
                        if (system_err != string.Empty)
                        {
                            err_msg = "Failed to create terminal.";     //"Failed to update terminal.";     Modified by DANIEL 8 Jan 2019
                            return false;
                        }

                        if (mode == 0)
                        {
                            // Insert PG..FDS_ContinueRejectRules
                            var newFDSContinueRejectRules = pgad.FDS_ContinueRejectRules_Insert(PDomainID);
                            system_err = pgad.system_err;
                            if (system_err != string.Empty)
                            {
                                err_msg = "Failed to create terminal.";     //"Failed to update terminal.";     Modified by DANIEL 8 Jan 2019
                                return false;
                            }

                            // Insert PG..FDS_FraudRules
                            var newFraudRules = pgad.FDS_FraudRules_Insert(PDomainID, PPriority);
                            system_err = pgad.system_err;
                            if (system_err != string.Empty)
                            {
                                err_msg = "Failed to create terminal.";     //"Failed to update terminal.";     Modified by DANIEL 8 Jan 2019
                                return false;
                            }
                        }
                    }
                    else
                    {
                        // Insert OB..HostCurrency
                        //Modified by DANIEL 21 Dec 2018. Add default value for parameter PPriority and PStatus
                        //var newHostCurrency = pgad.HostCurrency_Insert(PDomainID, PCurrencyCode, PHostID, PPymtMethod);
                        var newHostCurrency = pgad.HostCurrency_Insert(PDomainID, PCurrencyCode, PHostID, PPymtMethod, PPriority, 1);

                        system_err = pgad.system_err;
                        if (system_err != string.Empty)
                        {
                            err_msg = "Failed to create terminal.";     //"Failed to update terminal.";     Modified by DANIEL 8 Jan 2019
                            return false;
                        }
                    }
                    return true;
                }
                else
                {
                    system_err = pgad.system_err;
                    err_msg = "Failed to create terminal.";
                    return false;
                }
            }
            catch (Exception ex)
            {
                system_err = ex.Message;
                err_msg = "Failed to create terminal." + ex.ToString();     //Added by DANIEL 22 Jan 2019
                return false;
            }
        }

        //Added by SUKI 3 Dec 2018. To Check Merchant ID existed in CMS_Domain
        public bool checkCMSDomain(string merchantID)
        {
            CMS_Domain[] checkDomain = pgad.Get_CMS_Domain(99, 0, merchantID);
            if (checkDomain.Length != 0)
            {
                system_err = pgad.system_err;
                return false;
            }
            else
                return true;
        }

        //Added by DANIEL 19 Dec 2018. To get terminal credential list
        //Modified by DANIEL 23 Jan 2019. Add parameter PCurrencyCode
        //public TerminalCredential[] Do_GetTerminalCredentials(int PDomainID, string PPymtCode, int PHostID, string PCredential)
        public TerminalCredential[] Do_GetTerminalCredentials(int PDomainID, string PPymtCode, int PHostID, string PCredential, string PCurrencyCode)
        {
            try
            {
                var getTerminalCredentials = pgad.Get_Terminal_Credentials(PDomainID, PPymtCode, PHostID, PCredential, PCurrencyCode);  //Modified by DANIEL 23 Jan 2019. Add parameter PCurrencyCode

                system_err = pgad.system_err;
                if (!string.IsNullOrEmpty(system_err))
                {
                    err_msg = "Failed to retrieve Terminal Credentials.";
                    return null;
                }
                else
                    return getTerminalCredentials;
            }
            catch (Exception ex)
            {
                err_msg = ex.Message;
                return null;
            }
        }

        //Added by DANIEL 21 Dec 2018. To get list of pgm card group
        public PGM_CardGroup[] Do_Get_PGM_CardGroup(int PDomainID, int PHostID, string PCurrency)
        {
            try
            {
                var getPGM_CardGroup = pgad.Get_PGM_Routing(PDomainID, PHostID, PCurrency);
                system_err = pgad.system_err;
                if (system_err != string.Empty)
                {
                    err_msg = "Failed to retrieve Card Group.";
                    return null;
                }
                else
                    return getPGM_CardGroup;
            }
            catch (Exception ex)
            {
                err_msg = ex.Message;
                return null;
            }
        }

        //Added by DANIEL 21 Dec 2018. Update existing channel
        public bool Do_EditChannel(int PDomainID, string PPymtMethod, int PHostID, string PFieldName1, string PCredential1, string PFieldName2, string PCredential2,
           string PFieldName3, string PCredential3, string PFieldName4, string PCredential4, string PFieldName5, string PCredential5, string PFieldName6, string PCredential6,
           string PCurrencyCode, int PPriority, string PCardGroup, int PVisa, int PMasterCard, int PAMEX, int PDiners, int PJCB, int PCUP, int PMasterPass, int PVisaCheckout,
           int PSamsungPay, int PStatus, int mode)
        {
            try
            {
                // Update OB,PG..Terminals
                var updateTerminal = pgad.Terminal_Update(PDomainID, PPymtMethod, PHostID, PFieldName1, PCredential1, PFieldName2, PCredential2,
                                     PFieldName3, PCredential3, PFieldName4, PCredential4, PFieldName5, PCredential5,
                                     PFieldName6, PCredential6, PCurrencyCode, PPriority, PStatus);

                if (updateTerminal)
                {
                    if (PPymtMethod == "CC")
                    {
                        string[] cardGroupList = PCardGroup.Split(',');
                        var getPGMRouting = pgad.Get_PGM_Routing(PDomainID, PHostID, PCurrencyCode);

                        if (getPGMRouting == null)
                        {
                            system_err = pgad.system_err;
                            err_msg = "Failed to update terminal.";
                            return false;
                        }

                        //Delete PG..PGM_Routing, PToDelete = 1
                        for (int i = 0; i < getPGMRouting.Length; i++)
                        {
                            pgad.PGM_Routing_Insert(PDomainID, PHostID, 0, PCurrencyCode, PPriority, 1);

                            system_err = pgad.system_err;
                            if (!string.IsNullOrEmpty(system_err))
                            {
                                err_msg = "Failed to update terminal.";
                                return false;
                            }
                        }

                        //Insert PG..PGM_Routing
                        foreach (var cardGroup in cardGroupList)
                        {
                            if (!string.IsNullOrEmpty(cardGroup))
                                pgad.PGM_Routing_Insert(PDomainID, PHostID, Convert.ToInt32(cardGroup), PCurrencyCode, PPriority);

                            system_err = pgad.system_err;
                            if (!string.IsNullOrEmpty(system_err))
                            {
                                err_msg = "Failed to update terminal.";
                                return false;
                            }
                        }

                        // Insert PG..CardTypeProfile
                        var newCardTypeProfile = pgad.CardTypeProfile_Insert(PDomainID, PVisa, PMasterCard, PAMEX, PDiners, PJCB, PCUP, PMasterPass, PVisaCheckout, PSamsungPay);

                        system_err = pgad.system_err;
                        if (!string.IsNullOrEmpty(system_err))
                        {
                            err_msg = "Failed to update terminal.";
                            return false;
                        }
                    }

                    //Commented by DANIEL 23 Jan 2019. Remove unnecessary codes
                    //else
                    //{
                    //CHG START DANIEL 20190104 [Change PMode = 1: To Delete]
                    //To delete host currency record. PMode = 1
                    //var delHostCurrency = pgad.HostCurrency_Insert(PDomainID, PCurrencyCode, PHostID, PPymtMethod, PPriority, PStatus, 2);
                    //var delHostCurrency = pgad.HostCurrency_Insert(PDomainID, PCurrencyCode, PHostID, PPymtMethod, PPriority, PStatus, 1);
                    //CHG E N D DANIEL 20190104 [Change PMode = 1: To Delete]

                    //system_err = pgad.system_err;
                    //if (system_err != string.Empty)
                    //{
                    //    err_msg = "Failed to update terminal.";
                    //    return false;
                    //}

                    //CHG START DANIEL 20190104 [Remove '1' value. Default value for PMode = 0 : To insert]
                    //To insert HostCurrency
                    //var newHostCurrency = pgad.HostCurrency_Insert(PDomainID, PCurrencyCode, PHostID, PPymtMethod, PPriority, PStatus, 1);
                    //DEL START DANIEL 20190123 [Remove unnecessary codes]
                    //var newHostCurrency = pgad.HostCurrency_Insert(PDomainID, PCurrencyCode, PHostID, PPymtMethod, PPriority, PStatus);
                    //DEL E N D DANIEL 20190123 [Remove unnecessary codes]
                    //CHG E N D DANIEL 20190104 [Remove '1' value. Default value for PMode = 0 : To insert]

                    //    system_err = pgad.system_err;
                    //    if (system_err != string.Empty)
                    //    {
                    //        err_msg = "Failed to update terminal.";
                    //        return false;
                    //    }
                    //}

                    return true;
                }
                else
                {
                    system_err = pgad.system_err;
                    err_msg = "Failed to update terminal.";
                    return false;
                }
            }
            catch (Exception ex)
            {
                system_err = ex.Message;
                err_msg = "An error occured. Please contact administrator.";
                return false;
            }
        }

        //Added by KENT LOONG 11 Mar 2019. GET MERCHANT FUTURE RATE isReseller = 0: False, 1 =  True]
        public MerchantFutureRateHost[] Do_Get_MerchantFutureRateHost(int PDomainID, int isReseller = 0)
        {
            try
            {
                var merchantRateHost = pgad.Get_MerchantFutureRateHost(PDomainID, isReseller);
                system_err = pgad.system_err;
                if (system_err != string.Empty)
                {
                    err_msg = "Failed to get merchant future rate. Please contact administrator.";
                    return null;
                }
                else
                    return merchantRateHost;
            }
            catch (Exception ex)
            {
                system_err = ex.Message.ToString();
                err_msg = "An error occured. Please contact administrator.";
                return null;
            }
        }

        //Added by KENT LOONG 12 Mar 2019. GET MERCHANT FUTURE RATE BY PKID isReseller = 0: False, 1 =  True
        public MerchantFutureRateHost Do_Get_MerchantFutureRateBYPKID(int PKID, int isReseller = 0)
        {
            try
            {
                var merchantRateHost = pgad.Get_MerchantFutureRateByPKID(PKID, isReseller);
                system_err = pgad.system_err;
                if (system_err != string.Empty)
                {
                    err_msg = "Failed to get merchant future rate. Please contact administrator.";
                    return null;
                }
                else
                    return merchantRateHost;
            }
            catch (Exception ex)
            {
                system_err = ex.Message.ToString();
                err_msg = "An error occured. Please contact administrator.";
                return null;
            }
        }

        //Added by KENT LOONG 13 Mar 2019. GET MERCHANT FUTURE RATE BY PKID isReseller = 0: False, 1 =  True]
        public bool Do_Update_MerchantFutureRateBYPKID(int PKID, decimal Rate, decimal FixedRate, bool isReseller = false)
        {
            try
            {
                bool updateMerchantRate = pgad.MerchantFutureRate_Update(PKID, Rate, FixedRate, isReseller);
                if (!updateMerchantRate)
                {
                    system_err = pgad.system_err;
                    err_msg = "Failed to add merchant rate.";
                    return false;
                }
            }
            catch (Exception ex)
            {
                system_err = ex.Message.ToString();
                err_msg = "An error occured. Please contact administrator.";
                return false;
            }
            return true;
        }
    }
}
