﻿using PaydibsPortal.BLL.Helper;
using PaydibsPortal.DAL;
using System;
using System.Collections;
using System.Net;
using System.Net.Mail;
using System.Security;
using System.Text;

namespace PaydibsPortal.BLL
{
    public class MMSController
    {
        static Crypto crypto = new Crypto();
        PGAdminDAL pgad = new PGAdminDAL();
        public string system_err = string.Empty;
        public string err_msg = string.Empty;
        private string strActionServiceURL = System.Configuration.ConfigurationManager.AppSettings["PymtCheckOutURL"];
        private static SecureString terminalKey = new NetworkCredential("", System.Configuration.ConfigurationManager.AppSettings["TerminalKey"]).SecurePassword;
        private static SecureString terminalVector = new NetworkCredential("", System.Configuration.ConfigurationManager.AppSettings["TerminalVector"]).SecurePassword;
        string Name = System.Configuration.ConfigurationManager.AppSettings["DefaultSenderName"];
        string SendFromEmail = System.Configuration.ConfigurationManager.AppSettings["DefaultSenderAddress"];
        private static SecureString SendFromEmail_password = new NetworkCredential("",
            crypto.szDecrypt3DES(System.Configuration.ConfigurationManager.AppSettings["DefaultSenderPassword"],
            new NetworkCredential("", terminalKey).Password,
            new NetworkCredential("", terminalVector).Password)).SecurePassword;
        string SendFromHost = System.Configuration.ConfigurationManager.AppSettings["DefaultSenderHost"];
        string LogoPath = System.Configuration.ConfigurationManager.AppSettings["LogoPath"];

        public string GenerateButtonScript(ArrayList paramList, ref string strTokenReturn, bool isPymtLink = false)
        {
            /*ArrayList param :
		  * index 0 = order description
		  * index 1 = order number
		  * index 2 = currency
		  * index 3 = amount
		  * index 4 = merchant id
		  */

            //parameter to send
            string strOrderDesc = string.Empty;
            string strOrderNumber = string.Empty;
            string strCurrency = string.Empty;
            string strAmount = string.Empty;
            string strButtonSubmitValue = string.Empty;
            string strMerchantID = string.Empty;
            string strToken = string.Empty;
            string strTokenMD5 = string.Empty;
            string strFinalToken = string.Empty;

            string buyNowButton = string.Empty;

            try
            {
                if (paramList != null)
                {
                    if (!isPymtLink)
                    {
                        //Commented by DANIEL 18 Feb 2019
                        //if (strButtonSelected == "BNB")
                        //{
                        //    strButtonSubmitValue = "Buy Now";
                        //}
                        //else if (strButtonSelected == "PNB")
                        //{
                        //    strButtonSubmitValue = "Pay Now";
                        //}

                        strOrderDesc = paramList[0].ToString();
                        strOrderNumber = paramList[1].ToString();
                        strCurrency = paramList[2].ToString();
                        strAmount = paramList[3].ToString();
                        strMerchantID = paramList[4].ToString();

                        strToken = strMerchantID + strOrderDesc + strOrderNumber + strCurrency + strAmount;
                        strTokenMD5 = crypto.ComputeHash(strToken.ToLower(), "");
                        /*strFinalToken = strTokenMD5.Trim().Remove(strTokenMD5.Length - 2);*/ //the remove function will remove double equal sign at the end string
                        strTokenReturn = strTokenMD5;

                        buyNowButton = "<form action=\"" + strActionServiceURL + "\" method=\"post\" target=\"_top\">"
                                            + "<input type=\"hidden\" name=\"token\" value=\"" + strTokenMD5 + "\">"
                                            + "<input type=\"hidden\" name=\"TokenType\" value=\"BNB\">"
                                            + "<input type=\"submit\" value=\"Buy Now\" />"
                                            + "</form>";

                        var insertTokenBnB = pgad.Do_InsertTokenBNB(strMerchantID, strOrderDesc, strOrderNumber, strCurrency, Convert.ToDecimal(strAmount), strTokenMD5);
                        system_err = pgad.system_err;
                        if (!string.IsNullOrEmpty(system_err))
                        {
                            return err_msg = "An error occured. Please contact administrator.";
                        }
                    }
                    else
                    {
                        strOrderDesc = paramList[3].ToString();
                        strOrderNumber = paramList[4].ToString();
                        strCurrency = paramList[5].ToString();
                        strAmount = paramList[6].ToString();
                        strMerchantID = paramList[8].ToString();

                        strToken = strMerchantID + strOrderDesc + strOrderNumber + strCurrency + strAmount;
                        strTokenMD5 = crypto.ComputeHash(strToken.ToLower(), "");
                        /*strFinalToken = strTokenMD5.Trim().Remove(strTokenMD5.Length - 2);*/ //the remove function will remove double equal sign at the end string
                        strTokenReturn = strTokenMD5;
                    }
                }
            }
            catch (Exception ex)
            {
                system_err = ex.Message;
                err_msg = "An error occured. Please contact administrator.";
            }

            return buyNowButton;
        }

        public bool Do_InsertPaymentLink(ArrayList emailParams, string email, string strToken, int PRecStatus) //Modified by DANIEL 28 Feb 2019. Add parameter string email
        {
            //CustomerName = emailParams[0].ToString()
            //CustomerContactNumber = emailParams[1].ToString()  
            //CustomerEmailAddress = emailParams[2].ToString()  
            //OrderDescription = emailParams[3].ToString()
            //OrderNumber = emailParams[4].ToString()  
            //Currency = emailParams[5].ToString()
            //Amount = emailParams[6].ToString()
            //EmailReminder = emailParams[7].ToString()    
            //MerchantID = emailParams[8].ToString()      
            //WebSiteUrl = emailParams[9].ToString()      
            //MerchantName = emailParams[10].ToString()   
            //PaymentLink = emailParams[11].ToString()
            try
            {
                //Modified by DANIEL 18 Feb 2019. Change emailParams[1].ToString() to email
                //int insertPymtLink = pgad.Do_InsertPaymentLink(emailParams[0].ToString(), emailParams[1].ToString(), emailParams[2].ToString(), strToken, Convert.ToInt32(emailParams[7]), 1);
                int insertPymtLink = pgad.Do_InsertPaymentLink(emailParams[0].ToString(), emailParams[1].ToString(), email, strToken, Convert.ToInt32(emailParams[7]), 1);

                if (insertPymtLink <= 0)
                {
                    system_err = pgad.system_err;
                    err_msg = "An error occured. Please contact administrator.";
                    return false;
                }

                int insertBNB = pgad.Do_InsertTokenBNB(emailParams[8].ToString(), emailParams[3].ToString(), emailParams[4].ToString(), emailParams[5].ToString(), Convert.ToDecimal(emailParams[6].ToString()), strToken);

                //Commented by DANIEL 15 Mar 2019. Change condition
                //if (insertBNB <= 0)
                //{
                //    system_err = pgad.system_err;
                //    err_msg = "An error occured. Please contact administrator.";
                //    return false;
                //}

                system_err = pgad.system_err;
                if (!string.IsNullOrEmpty(system_err))
                {
                    err_msg = "An error occured. Please contact administrator.";
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                system_err = ex.Message;
                err_msg = "An error occured. Please contact administrator.";
                return false;
            }
        }

        public bool Do_Email(ArrayList emailParamLists, string pymtlink, string email, string PSubject, ref string sendStatus)
         {
            //    emailParamsLists[0].ToString(), //customer name
            //    emailParamsLists[1].ToString(), //customer contact number
            //    emailParamsLists[2].ToString(), //customer email address
            //    emailParamsLists[3].ToString(), //order description
            //    emailParamsLists[4].ToString(), //order number
            //    emailParamsLists[5].ToString(), // currency code
            //    emailParamsLists[6].ToString(), //amount
            //    emailParamsLists[7].ToString(), //email reminder yes no
            //    emailParamsLists[8].ToString(), // merchant id
            //    emailParamsLists[9].ToString(), // website url
            //    emailParamsLists[10].ToString(), //merchant name
            //    emailParamsLists[11].ToString(), //payment link
          
            try
            {
                //string Name = System.Configuration.ConfigurationManager.AppSettings["DefaultSenderName"];
                //string SendFromEmail = System.Configuration.ConfigurationManager.AppSettings["DefaultSenderAddress"];
                //string SendFromEmail_password = System.Configuration.ConfigurationManager.AppSettings["DefaultSenderPassword"];
                //string SendFromHost = System.Configuration.ConfigurationManager.AppSettings["DefaultSenderHost"];
                //string LogoPath = System.Configuration.ConfigurationManager.AppSettings["LogoPath"];
                string SendToEmail = email;

                #region HTML Content
                StringBuilder sb = new StringBuilder();
                 
                sb.Append("<div style='width:80%;max-width:800px; float:left; font-family:arial,sans-serif; '>");
                sb.Append("<div style='width:100%; margin: 0px auto; text-align:center; '>");
                sb.Append("<img src=\"" +LogoPath + emailParamLists[8] + "-logo.png\" width='180' />");
                sb.Append("</div>");
                sb.Append("<div style=\"width:90%; margin:0px auto; border:1px solid #CCCCCC; padding: 20px;\">");
                sb.Append("<div style=\"width:100%; \">");
                sb.Append("<p style=\"text-align: center; font-size: 24px; \">");

                sb.Append("<table>");

                sb.Append("<tr>");
                sb.Append("<td colspan='2' align='left'>Dear " + emailParamLists[0] + ",<br/><br/> ");
                sb.Append("</td>");
                sb.Append("</tr>");

                //sb.Append("<tr>");
                //sb.Append("<td colspan='2' align='left'>==============================<br/> ");
                //sb.Append("</td>");
                //sb.Append("</tr>");

                sb.Append("<tr>");
                sb.Append("<td colspan='2' align='left'>Below is your order details:- <br/><br/> ");
                sb.Append("</td>");
                sb.Append("</tr>");

                sb.Append("<tr>");
                sb.Append("<td colspan='2' align='left'>Order ID:  " + emailParamLists[4] + "<br/><br/>");
                sb.Append("</td>");
                sb.Append("</tr>");

                sb.Append("<tr>");
                sb.Append("<td colspan='2' align='left'>Order Description:  " + emailParamLists[3] + "<br/><br/>");
                sb.Append("</td>");
                sb.Append("</tr>");

                sb.Append("<tr>");
                sb.Append("<td colspan='2' align='left'>Amount:  " + emailParamLists[5] + " " + Convert.ToDecimal(emailParamLists[6]).ToString("F") + "<br/><br/><br/>");
                sb.Append("</td>");
                sb.Append("</tr>");


                //sb.Append("<tr>");
                //sb.Append("<td colspan='2' align='left'>==============================<br/> ");
                //sb.Append("</td>");
                //sb.Append("</tr>");

                sb.Append("<tr>");
                sb.Append("<td colspan='2' align='left'>To make payment for above order, please click or copy and paste the link below into web browser.<br /><br/>");
                sb.Append("</td>");
                sb.Append("</tr>");

                sb.Append("<tr>");
                //Modified by DANIEL 18 Feb 2019. Change to parameter pymtLink
                //sb.Append("<td colspan='2' align='left'><a href=" + emailParamLists[11] + ">" + emailParamLists[11] + " </a><br /><br /><br /><br />");
                sb.Append("<td colspan='2' align='left'><a href=" + pymtlink + ">" + pymtlink + " </a><br /><br /><br /><br />");
                sb.Append("</td>");
                sb.Append("</tr>");

                sb.Append("<tr>");
                sb.Append("<td colspan='2' align='left'>" + emailParamLists[8] + "<br /><br />");
                sb.Append("</td>");
                sb.Append("</tr>");

                sb.Append("<tr>");
                sb.Append("<td colspan='2' align='left'> <a href=" + emailParamLists[9] + ">" + emailParamLists[9] + "</a><br /><br />");
                sb.Append("</td>");
                sb.Append("</tr>");

                sb.Append("<tr>");
                sb.Append("<td colspan='2' align='left'> <small>Please do not reply to this email. This is auto generated email.</small><br />");
                sb.Append("</td>");
                sb.Append("</tr>");

                sb.Append("</table>");
                sb.Append("</div>");
                sb.Append("</div></div>");
                #endregion

                SmtpClient client = new SmtpClient(SendFromHost, 587);
                MailAddress from = new MailAddress(SendFromEmail, Name);
                client.Credentials = new System.Net.NetworkCredential(SendFromEmail, SendFromEmail_password);
                client.EnableSsl = true;

                MailMessage message = new MailMessage();
                message.From = new MailAddress(SendFromEmail, Name);
                message.To.Add(SendToEmail);
                message.Subject = PSubject;
                message.Body = sb.ToString();
                message.IsBodyHtml = true;

                string SendStatus = string.Empty;
                try
                {
                    client.Send(message);
                    return true;
                }
                catch (Exception ex)
                {
                    SendStatus = ex.ToString();
                    return false;
                }
            }
            catch (Exception ex)
            {
                system_err = ex.Message;
                err_msg = "An error occured. Please contact administrator.";
                return false;
            }
        }
    }
}
