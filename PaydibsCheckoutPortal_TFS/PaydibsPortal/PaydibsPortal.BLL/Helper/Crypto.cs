﻿using System;
using System.Text;
using System.Security.Cryptography;
using Microsoft.VisualBasic;
using System.IO;

namespace PaydibsPortal.BLL.Helper
{
    class Crypto
    {
        public string EncryptData(string textData, string Encryptionkey)
        {
            RijndaelManaged objrij = new RijndaelManaged();
            //set the mode for operation of the algorithm   
            objrij.Mode = CipherMode.CBC;
            //set the padding mode used in the algorithm.   
            objrij.Padding = PaddingMode.PKCS7;
            //set the size, in bits, for the secret key.   
            objrij.KeySize = 0x80;
            //set the block size in bits for the cryptographic operation.    
            objrij.BlockSize = 0x80;
            //set the symmetric key that is used for encryption & decryption.    
            byte[] passBytes = Encoding.UTF8.GetBytes(Encryptionkey);
            //set the initialization vector (IV) for the symmetric algorithm    
            byte[] EncryptionkeyBytes = new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

            int len = passBytes.Length;
            if (len > EncryptionkeyBytes.Length)
            {
                len = EncryptionkeyBytes.Length;
            }
            Array.Copy(passBytes, EncryptionkeyBytes, len);

            objrij.Key = EncryptionkeyBytes;
            objrij.IV = EncryptionkeyBytes;

            //Creates symmetric AES object with the current key and initialization vector IV.    
            ICryptoTransform objtransform = objrij.CreateEncryptor();
            byte[] textDataByte = Encoding.UTF8.GetBytes(textData);
            //Final transform the test string.  
            return Convert.ToBase64String(objtransform.TransformFinalBlock(textDataByte, 0, textDataByte.Length));
        }

        public string DecryptData(string EncryptedText, string Encryptionkey)
        {
            RijndaelManaged objrij = new RijndaelManaged();
            objrij.Mode = CipherMode.CBC;
            objrij.Padding = PaddingMode.PKCS7;

            objrij.KeySize = 0x80;
            objrij.BlockSize = 0x80;
            byte[] encryptedTextByte = Convert.FromBase64String(EncryptedText);
            byte[] passBytes = Encoding.UTF8.GetBytes(Encryptionkey);
            byte[] EncryptionkeyBytes = new byte[0x10];
            int len = passBytes.Length;
            if (len > EncryptionkeyBytes.Length)
            {
                len = EncryptionkeyBytes.Length;
            }
            Array.Copy(passBytes, EncryptionkeyBytes, len);
            objrij.Key = EncryptionkeyBytes;
            objrij.IV = EncryptionkeyBytes;
            byte[] TextByte = objrij.CreateDecryptor().TransformFinalBlock(encryptedTextByte, 0, encryptedTextByte.Length);
            return Encoding.UTF8.GetString(TextByte);  //it will return readable string  
        }

        public string szEncrypt3DES(string sz_ClearText, string sz_Key, string sz_InitVector)
        {
            TripleDESCryptoServiceProvider obj3DES = new TripleDESCryptoServiceProvider();
            byte[] btInput;
            byte[] btOutput;

            try
            {
                obj3DES.Padding = PaddingMode.PKCS7; // Default value
                obj3DES.Mode = CipherMode.CBC;       // Default value
                obj3DES.Key = btHexToByte(sz_Key.ToCharArray());

                if ((false == bValidHex(sz_InitVector)))
                    sz_InitVector = szStringToHex(sz_InitVector);
                obj3DES.IV = btHexToByte(sz_InitVector.ToCharArray());

                if ((false == bValidHex(sz_ClearText)))
                    sz_ClearText = szStringToHex(sz_ClearText);
                if ((true == sz_ClearText.Contains("-")))
                    sz_ClearText = szStringToHex(sz_ClearText);
                btInput = btHexToByte(sz_ClearText.ToCharArray());

                btOutput = btTransform(btInput, obj3DES.CreateEncryptor());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (!(obj3DES == null))
                    obj3DES = null/* TODO Change to default(_) if this is not a reference type */;
            }
            return szWriteHex(btOutput);
        }

        public string szDecrypt3DES(string sz_CipherText, string sz_Key, string sz_InitVector)
        {
            TripleDESCryptoServiceProvider obj3DES = new TripleDESCryptoServiceProvider();
            UTF8Encoding objUTF8 = new UTF8Encoding();
            byte[] btInput;
            byte[] btOutput;
            string szDecrypt3DES = "";

            try
            {
                obj3DES.Padding = PaddingMode.PKCS7; // Default value
                obj3DES.Mode = CipherMode.CBC;       // Default value
                obj3DES.Key = btHexToByte(sz_Key.ToCharArray());

                if ((false == bValidHex(sz_InitVector)))
                    sz_InitVector = szStringToHex(sz_InitVector);
                obj3DES.IV = btHexToByte(sz_InitVector.ToCharArray());

                sz_CipherText = Strings.Replace(sz_CipherText, Constants.vbCrLf, "");
                sz_CipherText = Strings.Replace(sz_CipherText, Constants.vbCr, "");
                sz_CipherText = Strings.Replace(sz_CipherText, Constants.vbLf, "");

                if ((false == bValidHex(sz_CipherText)))
                    sz_CipherText = szStringToHex(sz_CipherText);
                btInput = btHexToByte(sz_CipherText.ToCharArray());

                btOutput = btTransform(btInput, obj3DES.CreateDecryptor());

                return szDecrypt3DES = objUTF8.GetString(btOutput);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (!(obj3DES == null))
                    obj3DES = null/* TODO Change to default(_) if this is not a reference type */;
                if (!(objUTF8 == null))
                    objUTF8 = null;
            }
        }

        public string szDecrypt3DEStoHex(string sz_CipherText, string sz_Key, string sz_InitVector)
        {
            TripleDESCryptoServiceProvider obj3DES = new TripleDESCryptoServiceProvider();
            UTF8Encoding objUTF8 = new UTF8Encoding();
            byte[] btInput;
            byte[] btOutput;
            string szDecrypt3DEStoHex = "";

            try
            {
                obj3DES.Padding = PaddingMode.PKCS7; // Default value
                obj3DES.Mode = CipherMode.CBC;       // Default value
                obj3DES.Key = btHexToByte(sz_Key.ToCharArray());

                if ((false == bValidHex(sz_InitVector)))
                    sz_InitVector = szStringToHex(sz_InitVector);
                obj3DES.IV = btHexToByte(sz_InitVector.ToCharArray());

                sz_CipherText = Strings.Replace(sz_CipherText, Constants.vbCrLf, "");
                sz_CipherText = Strings.Replace(sz_CipherText, Constants.vbCr, "");
                sz_CipherText = Strings.Replace(sz_CipherText, Constants.vbLf, "");

                if ((false == bValidHex(sz_CipherText)))
                    sz_CipherText = szStringToHex(sz_CipherText);
                btInput = btHexToByte(sz_CipherText.ToCharArray());

                btOutput = btTransform(btInput, obj3DES.CreateDecryptor());

                return szDecrypt3DEStoHex = szWriteHex(btOutput);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (!(obj3DES == null))
                    obj3DES = null/* TODO Change to default(_) if this is not a reference type */;
                if (!(objUTF8 == null))
                    objUTF8 = null;
            }
        }

        public byte[] btHexToByte(char[] c_Hex)
        {
            string szHex = "";

            byte[] btOutput = new byte[c_Hex.Length / 2 - 1 + 1]; // Byte() = New Byte(c_Hex.Length / 2 - 1) {}
            int iOffset = 0;
            int iCounter = 0;

            iCounter = 0;
            while (iCounter < c_Hex.Length)
            {
                szHex = Convert.ToString(c_Hex[iCounter]) + Convert.ToString(c_Hex[iCounter + 1]);
                btOutput[iOffset] = byte.Parse(szHex, System.Globalization.NumberStyles.HexNumber);
                iOffset += 1;
                iCounter = iCounter + 2;
            }

            return btOutput;
        }

        public bool bValidHex(string sz_Txt)
        {
            string szch;
            int i;

            sz_Txt = sz_Txt.ToUpper();
            for (i = 1; i <= sz_Txt.Length; i++)
            {
                szch = sz_Txt.Substring((i - 1), 1);
                // See if the next character is a non-digit.
                double chkNumeric = 0;
                if ((false == Double.TryParse(szch, out chkNumeric)))
                {
                    //if ((szch >= "G"))
                    //    // This is not a letter.
                    //    return false;
                    if (String.Compare(szch, "G", StringComparison.InvariantCulture) > 0)
                        return false;
                }
            }
            return true;
        }

        public string szStringToHex(string sz_Text)
        {
            string szHex = "";
            var loopTo = sz_Text.Length - 1;
            for (int i = 0; i <= loopTo; i++)
                szHex += Strings.Asc(sz_Text.Substring(i, 1)).ToString("x").ToUpper();

            return szHex;
        }

        public byte[] btTransform(byte[] bt_Input, ICryptoTransform I_CryptoTransform)
        {
            // Create the necessary streams
            MemoryStream objMemStream = new MemoryStream();
            CryptoStream objCryptStream = new CryptoStream(objMemStream, I_CryptoTransform, CryptoStreamMode.Write);
            byte[] btResult;

            try
            {
                // Transform the bytes as requested
                objCryptStream.Write(bt_Input, 0, bt_Input.Length);
                objCryptStream.FlushFinalBlock();

                // Read the memory stream and convert it back into byte array
                objMemStream.Position = 0;

                btResult = new byte[System.Convert.ToInt32(objMemStream.Length - 1) + 1];
                objMemStream.Read(btResult, 0, System.Convert.ToInt32(btResult.Length));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                // close and release the streams
                objMemStream.Close();
                objCryptStream.Close();

                if (!(objMemStream == null))
                    objMemStream = null;
                if (!(objCryptStream == null))
                    objCryptStream = null/* TODO Change to default(_) if this is not a reference type */;
            }

            // hand back the result buffer
            return btResult;
        }

        public string szWriteHex(byte[] bt_Array)
        {
            string szHex = "";
            int iCounter;
            var loopTo = (bt_Array.Length - 1);
            for (iCounter = 0; iCounter <= loopTo; iCounter++)
                szHex += bt_Array[iCounter].ToString("X2");

            return szHex;
        }

        //ADD START DANIEL 20190128 [Add Compute Hash method]
     
		public string ComputeHash(string p_sPlainText, string p_sHashAlgo)
        {
            // Convert plain text into a byte array.
            byte[] aucPlainText = Encoding.UTF8.GetBytes(p_sPlainText);

            HashAlgorithm oHash;
            // Make sure hashing algorithm name is specified.
            if (null == p_sHashAlgo)
                p_sHashAlgo = "";

            // Initialize appropriate hashing algorithm class.
            switch (p_sHashAlgo.ToUpper())
            {
                case "SHA1":
                    oHash = new SHA1Managed();
                    break;

                case "SHA256":
                    oHash = new SHA256Managed();
                    break;

                case "SHA384":
                    oHash = new SHA384Managed();
                    break;

                case "SHA512":
                    oHash = new SHA512Managed();
                    break;

                default:
                    oHash = new MD5CryptoServiceProvider();
                    break;
            }

            // Compute oHash value of our plain text.
            byte[] aucHashValue = oHash.ComputeHash(aucPlainText);

            StringBuilder sb = new StringBuilder(aucHashValue.Length * 2);
            foreach (byte b in aucHashValue)
            {
                sb.AppendFormat("{0:x2}", b);
            }
            // Convert result into a base64-encoded string.
            string sHashValue = Convert.ToBase64String(aucHashValue);
            //string sHashValue = Convert.ToString (aucHashValue);

            return sHashValue;
        }

        //ADD E N D DANIEL 20190128 [Add Compute Hash method]

    }
}
