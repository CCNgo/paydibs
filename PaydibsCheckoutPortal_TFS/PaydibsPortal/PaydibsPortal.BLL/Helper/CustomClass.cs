﻿using System;
namespace PaydibsPortal.BLL.Helper
{
    public class CustomClass
    {
        public class userStatus
        {
            public static int Pending = 0;
            public static int Active = 1;
            public static int Suspended = 2;
            public static int Terminated = 3;
            public static int Lockout = 4;
        }
    }
}
