﻿using PaydibsPortal.BLL.Helper;
using PaydibsPortal.DAL;
using System;
using System.Net;
using System.Security;

namespace PaydibsPortal.BLL
{
    public class CMSController
    {
        PGAdminDAL pgad = new PGAdminDAL();
        private static Crypto aes = new Crypto();

        public string system_err = string.Empty;// System.Configuration.ConfigurationManager.AppSettings["PWKey"]
        public string err_msg = string.Empty; //aes.szDecrypt3DES(decryptTxt, new NetworkCredential("", terminalKey).Password, new NetworkCredential("", terminalVector).Password);
        private static SecureString terminalKey = new NetworkCredential("", System.Configuration.ConfigurationManager.AppSettings["TerminalKey"]).SecurePassword;
        private static SecureString terminalVector = new NetworkCredential("", System.Configuration.ConfigurationManager.AppSettings["TerminalVector"]).SecurePassword;
        private SecureString securePWKey = new NetworkCredential("", 
            aes.szDecrypt3DES(System.Configuration.ConfigurationManager.AppSettings["PWKey"],
            new NetworkCredential("", terminalKey).Password, 
            new NetworkCredential("", terminalVector).Password)).SecurePassword;
        private SecureString secureDefPwd = new NetworkCredential("",
            aes.szDecrypt3DES(System.Configuration.ConfigurationManager.AppSettings["DefaultPwd"],
            new NetworkCredential("", terminalKey).Password,
            new NetworkCredential("", terminalVector).Password)).SecurePassword;

        string errorMsg = "An error occured. Please contact administrator.";    //Added by DANIEL 9 Jan 2019.

        public UserGroup[] Do_GetUserGroup(int PDomainID, string PUserID = "")  //Modified by DANIEL 15 Feb 2019. Added parameter PUserID

        {
            try
            {
                var userGroup = pgad.CMS_UserGroup_By_DomainID(PDomainID, PUserID); //Modified by DANIEL 15 Feb 2019. Added parameter PUserID

                system_err = pgad.system_err;
                if (system_err != string.Empty)
                {
                    err_msg = errorMsg;     //Added by DANIEL 9 Jan 2019
                    return null;
                }
                else
                {
                    return userGroup;
                }
            }
            catch (Exception ex)
            {
                system_err = ex.Message;
                return null;
            }
        }

        public bool Do_AddUserGroup(int PDomainID, string PGroupName, string PGroupDesc, string PModifiedBy)
        {
            try
            {
                var newUserGroup = pgad.CMS_UserGroup_Insert(PDomainID, PGroupName, PGroupDesc, PModifiedBy);
                system_err = pgad.system_err;
                if (system_err != string.Empty)
                {
                    err_msg = errorMsg;     //Added by DANIEL 10 Jan 2019
                    return false;
                }
                else
                {
                    if (newUserGroup.RetVal == 0)
                    {
                        return true;
                    }
                    else
                    {
                        err_msg = "Failed to create user group.";
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                system_err = ex.Message;
                return false;
            }
        }

        //public User[] Do_GetUser(int PDomainID)       //Modified by DANIEL 11 Dec 2018. Added parameter PGroupID
        public User[] Do_GetUser(int PDomainID, int PGroupID = 0)   
        {
            try
            {
                var user = pgad.CMS_User_By_DomainID(PDomainID, PGroupID);
                system_err = pgad.system_err;
                if (system_err != string.Empty)
                {
                    err_msg = errorMsg;     //Added by DANIEL 10 Jan 2019
                    return null;
                }
                else
                {
                    return user;
                }
            }
            catch (Exception ex)
            {
                system_err = ex.Message;
                return null;
            }
        }

        public bool Do_AddUser(int PDomainID, string PUserGroup, string PUserID, string PModifiedBy)
        {
            try
            {
                //Commented by DANIEL 26 Feb 2019. Convert default pwd to secureString
                //string defaultPwd = aes.EncryptData("user12345", PWKey);
                //string defaultPwd = aes.EncryptData(new NetworkCredential("", secureDefPwd).Password, new NetworkCredential("", securePWKey).Password);

                var newUser = pgad.CMS_User_Insert(PUserID, aes.EncryptData(new NetworkCredential("", secureDefPwd).Password, new NetworkCredential("", securePWKey).Password), 0, 0, PDomainID, int.Parse(PUserGroup), PModifiedBy);

                //Commented by DANIEL 8 Jan 2019. Check if pgAdmin error message = not null
                //if (newUser.RetVal == 0)
                //{
                //    return true;
                //}
                //else
                //{
                //    err_msg = "Failed to create user.";
                //    return false;
                //}

                //Added by DANIEL 8 Jan 2019. Check if pgAdmin error message = not null - START
                system_err = pgad.system_err;
                if (system_err != string.Empty)
                {
                    err_msg = "Failed to create user.";
                    return false;
                }
                else
                {
                    if (newUser.RetVal == 0)
                    {
                        return true;
                    }
                    else
                    {
                        err_msg = "Failed to create user.";
                        return false;
                    }
                }
                //Added by DANIEL 8 Jan 2019. Check if pgAdmin error message = not null - END
            }
            catch (Exception ex)
            {
                system_err = ex.Message;
                return false;
            }

        }

        public bool Do_UpdateUserGroup(int PDomainID, int PGroupID, string PGroupDesc, int PGroupStatus, string PModifiedBy)
        {
            try
            {
                var updUserGroup = pgad.CMS_UserGroup_Update(PDomainID, PGroupID, PGroupDesc, PGroupStatus, PModifiedBy);

                //Commented by DANIEL 8 Jan 2019. Check if pgAdmin error message = not null
                //if (updUserGroup.RetVal == 0)
                //{
                //    return true;
                //}
                //else
                //{
                //    err_msg = "Failed to update user group.";
                //    return false;
                //}

                //Added by DANIEL 8 Jan 2019. Check if pgAdmin error message = not null - START
                system_err = pgad.system_err;
                if (system_err != string.Empty)
                {
                    err_msg = "Failed to update user group.";
                    return false;
                }
                else
                {
                    if (updUserGroup.RetVal == 0)
                    {
                        return true;
                    }
                    else
                    {
                        err_msg = "Failed to update user group.";
                        return false;
                    }
                }
                //Added by DANIEL 8 Jan 2019. Check if pgAdmin error message = not null - END
            }
            catch (Exception ex)
            {
                system_err = ex.Message;
                return false;
            }
        }

        public bool Do_UpdateUser(int PDomainID, string PUserGroup, string PUserID, int PStatus, string PModifiedBy, int mode)
        {
            try
            {

                var updUser = pgad.CMS_User_Update(PDomainID, int.Parse(PUserGroup), PUserID, PStatus, PModifiedBy, mode);

                //Commented by DANIEL 8 Jan 2019. Check if pgAdmin error message = not null
                //if (updUser.RetVal == 0)
                //{
                //    return true;
                //}
                //else
                //{
                //    err_msg = "Failed to update user.";
                //    return false;
                //}

                //Added by DANIEL 8 Jan 2019. Check if pgAdmin error message = not null - START
                system_err = pgad.system_err;
                if (system_err != string.Empty)
                {
                    err_msg = "Failed to update user.";
                    return false;
                }
                else
                {
                    if (updUser.RetVal == 0)
                    {
                        return true;
                    }
                    else
                    {
                        err_msg = "Failed to update user.";
                        return false;
                    }
                }
                //Added by DANIEL 8 Jan 2019. Check if pgAdmin error message = not null - END

            }
            catch (Exception ex)
            {
                system_err = ex.Message;
                return false;
            }
        }

        //Added by DANIEL 10 Dec 2018. Added new method to insert data into db CMS_UserGroupMap
        public int Do_Insert_Update_UserGroupMap(int PUserGroupID, int POpsGroupID, int PMapValue)
        {
            try
            {
                //Modified by DANIEL 4 Jan 2019. Add and pass parameter PMode to CMS_OpsIDEnumGroup_By_GroupID
                //return pgad.CMS_Insert_Update_OperationGroupMap(PUserGroupID, POpsGroupID, PMapValue);
                var insertUpdateOperationGroup = pgad.CMS_Insert_Update_UserGroupMap(PUserGroupID, POpsGroupID, PMapValue);
                system_err = pgad.system_err;
                if (!string.IsNullOrEmpty(system_err))
                {
                    //Modified by DANIEL 10 Jan 2019.
                    err_msg = "Failed to update user group permission."; //"Failed to insert or update user group pemission.";
                    return 0;
                }
                else
                { 
                    return insertUpdateOperationGroup;
                }

            }
            catch (Exception ex)
            {
                err_msg = ex.Message;       //Added by DANIEL 4 Jan 2019
                return 0;
            }
        }

        //Added by DANIEL 3 Dec 2018. Add this method to retrieve list of operation group
        public OperationGroup[] Do_GetOperationGroup(int PUserGroupID, int PMode = 0)   //Modified by DANIEL 13 Dec 2018. Add PMode 0 = All OpsGroup , PMode 1 = Allowed OpsGroup
        {
            try
            {
                //Modified by DANIEL 12 Dec 2018. Add and pass parameter PMode to CMS_OperationGroup_By_GroupID
                //return pgad.CMS_OperationGroup_By_GroupID(PUserGroupID);

                //Modified by DANIEl 2 Jan 2019. Retrieve error message from PGAdmin
                //return pgad.CMS_OperationGroup_By_GroupID(PUserGroupID, PMode);
                var getOperationGroup = pgad.CMS_OperationGroup_By_GroupID(PUserGroupID, PMode);
                system_err = pgad.system_err;
                if (!string.IsNullOrEmpty(system_err))
                {
                    err_msg = errorMsg;     //"Failed to get user group permission.";   Modified by DANIEL 10 Jan 2019
                    return null;
                }
                else
                {
                    return getOperationGroup;
                }
            }
            catch (Exception ex)
            {
                system_err = ex.Message;
                return null;
            }
        }

        //Added by DANIEL 12 Dec 2018. Add method Do_GetUser_Permission to retrieve user permission list
        //POpsGroupID 0 = Get user only . POpsGroupID 1 = Get User permission by allowed operation group
        public UserPermission[] Do_GetUser_Permission(string PUserID, int POpsGroupID = 0)
        {
            try
            {
                //MOdified by DANIEl 2 Jan 2019. Retrieve error message from PGAdmin
                //return pgad.CMS_User_Permission_By_UserID(PUserID, POpsGroupID);
                var getUserPermission = pgad.CMS_User_Permission_By_UserID(PUserID, POpsGroupID);

                system_err = pgad.system_err;
                if (!string.IsNullOrEmpty(system_err))
                {
                    err_msg = errorMsg;     //"Failed to get user pemission.";      Modified by DANIEL 10 Jan 2019
                    return null;
                }
                else
                {
                    return getUserPermission;
                }
            }
            catch (Exception ex)
            {
                system_err = ex.Message;
                return null;
            }
        }

        //Added by DANIEL 13 Dec 2018. Added new method to insert data into db CMS_UserMap
        public int Do_Insert_Update_UserMap(string PUserID, int POpsGroupID, int PMapValue)
        {
            try
            {
                //Modified by DANIEl 2 Jan 2019. Retrieve error message from PGAdmin
                //return pgad.CMS_Insert_Update_UserMap(PUserID, POpsGroupID, PMapValue);
                var insertUpdate_UserMap = pgad.CMS_Insert_Update_UserMap(PUserID, POpsGroupID, PMapValue);

                system_err = pgad.system_err;
                if (!string.IsNullOrEmpty(system_err))
                {
                    err_msg = errorMsg;     //"Failed to update user permission.";      Modified by DANIEL 10 Jan 2019
                    return 0;
                }
                else
                {
                    return insertUpdate_UserMap;
                }
            }
            catch (Exception ex)
            {
                err_msg = ex.Message;       //Added by DANIEL 2 Jan 2019
                return 0;
            }
        }
    }
}