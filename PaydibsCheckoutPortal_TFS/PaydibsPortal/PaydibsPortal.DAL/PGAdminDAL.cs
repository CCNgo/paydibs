﻿using Dapper;
using Kenji.Apps;
using NeoApp;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Configuration;

namespace PaydibsPortal.DAL
{
    public class PGAdminDAL
    {
        #region Definition
        MsSqlClient mssqlc = null;
        SqlBuilder sqlb = new SqlBuilder();
        string connStr = "";
        LinqTool linq = new LinqTool();
        public string errMsg = "";
        Encryption enc = new Encryption();
        public string system_err = string.Empty;
        #endregion

        #region Construct
        public PGAdminDAL()
        {
            string Ecnrypted_connStr = WebConfigurationManager.AppSettings["CONNSTR_PGAdmin"];
            connStr = Decrypt_ConnStr(Ecnrypted_connStr);
            mssqlc = new MsSqlClient(connStr);
        }
        #endregion

        #region Method
        /// <summary>
        /// Decrypt Connection String
        /// </summary>
        /// <param name="PEncrypted_ConnStr"></param>
        /// <returns>Decrypted Connection String From WEb.Config</returns>
        string Decrypt_ConnStr(string PEncrypted_ConnStr)
        {
            string KEY = "e26b5be2-867a-4400-8079-612b4a2332f6";
            //string testdata = "Server=localhost\\SQLEXPRESS01;Database=PGAdmin;Integrated Security=True;";
            //string testing = enc.AESDecrypt(testdata, KEY, KEY);
            //string testing2 = enc.AESEncrypt(testdata, KEY, KEY);
            //"CoCi8/Q5nF63hDqsFzhAwiyJAz22UJ/KGLozwPgHPKs/bJIuLPBs/xRjBH3b/U4BOei/Kd8bCqhlXOED3ipmIkR7nEN0F2R9t0SK6NQt7YQ="
            return enc.AESDecrypt(PEncrypted_ConnStr, KEY, KEY);
        }

        #region CMS_AccessLog
        public bool CMS_AccessLog_Insert(string PUserID, int PAcctID, int PMesgCode, OpsIDEnum opsIDEnum, int PResult, string PResultDesc)
        {
            try
            {
                using (var conn = new SqlConnection(connStr))
                {
                    conn.Open();

                    var p = new DynamicParameters();
                    p.Add("@PUserID", PUserID);
                    p.Add("@PAcctID", PAcctID);
                    p.Add("@PMesgCode", PMesgCode);
                    p.Add("@POpsID", opsIDEnum);
                    p.Add("@PResult", PResult);
                    p.Add("@PResultDesc", PResultDesc);
                    p.Add("@PDateCreated", DateTime.Now);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    conn.Execute("spInsertCMSAccessLog", p, commandType: CommandType.StoredProcedure);

                    int result = p.Get<int>("@ret");

                    return result == 0 ? true : false;
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return false;
            }
        }
        #endregion

        #region UserMap
        // CHG START SUKI 20190104 [Check permission on every page load]
        //public dynamic[] LoadPermission(string PUserID)
        public dynamic[] LoadPermission(string PUserID, int POpsID)
        // CHG E N D SUKI 20190104 [Check permission on every page load]
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();
                    p.Add("@PUserID", PUserID);
                    // ADD START SUKI 20190104 [Check permission on every page load]
                    p.Add("@POpsID", POpsID);
                    // ADD E N D SUKI 20190104 [Check permission on every page load]

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    var results = sqlConnection.Query<dynamic>("spLoadPermission", p, commandType: CommandType.StoredProcedure);

                    if (results.Count() > 0)
                        return results.ToArray();
                    else
                        return null;
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return null;
            }
        }

        public dynamic CMS_UserGroupMap_UserMap_Insert(int PGroupID, string PUserID)
        {
            try
            {
                using (var conn = new SqlConnection(connStr))
                {
                    conn.Open();

                    var p = new DynamicParameters();
                    p.Add("@PGroupID", PGroupID);
                    p.Add("@PUserID", PUserID);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);

                    var results = conn.Query<dynamic>("spInsertMerchantDefaultPermission", p, commandType: CommandType.StoredProcedure);
                    if (results.Count() > 0)
                        return results.ToArray()[0];
                    else
                        return null;
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return 0;
            }
        }
        #endregion

        #region CMS_User
        public dynamic Get_CMS_User_Join_SelectDetail(string PUserID)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();
                    p.Add("@PUserID", PUserID);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    var results = sqlConnection.Query<dynamic>("spGetCMSUserCMSUserGroupByUserID", p, commandType: CommandType.StoredProcedure);

                    if (results.Count() > 0)
                        return results.ToArray()[0];
                    else
                        return null;
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return null;
            }
        }

        public CMS_User CMS_User_SelectDetail(string PUserID)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();
                    p.Add("@PUserID", PUserID);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    var results = sqlConnection.Query<CMS_User>("spGetCMSUserByUserID", p, commandType: CommandType.StoredProcedure);

                    //CHG START DANIEL 20190211 [If no result, return null]
                    //return results.ToArray();
                    if (results.Count() > 0)
                        return results.ToArray()[0];
                    else
                        return null;
                    //CHG START DANIEL 20190211 [If no result, return null]
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return null;
            }
        }

        public bool CMS_User_Update_LastLogin(string PUserID)
        {
            try
            {
                using (var conn = new SqlConnection(connStr))
                {
                    conn.Open();

                    var p = new DynamicParameters();
                    p.Add("@PUserID", PUserID);
                    p.Add("@PLastLogin", DateTime.Now);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    conn.Execute("spUpdateCMSUserLastLogin", p, commandType: CommandType.StoredProcedure);

                    int result = p.Get<int>("@ret");

                    return result == 0 ? true : false;
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return false;
            }
        }

        public bool CMS_User_Update_Password(string PUserID, string PNewPwd)
        {
            try
            {
                using (var conn = new SqlConnection(connStr))
                {
                    conn.Open();

                    var p = new DynamicParameters();
                    p.Add("@PNewPwd", PNewPwd);
                    p.Add("@PwdChanged", DateTime.Now);
                    p.Add("@PUserID", PUserID);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    conn.Execute("spUpdateCMSUserForPwd", p, commandType: CommandType.StoredProcedure);

                    int result = p.Get<int>("@ret");

                    return result == 0 ? true : false;
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return false;
            }
        }

        public bool CMS_User_Update_PwdTemp(string PUserID, string PPwdTemp)
        {
            try
            {
                using (var conn = new SqlConnection(connStr))
                {
                    conn.Open();

                    var p = new DynamicParameters();
                    p.Add("@PPwdTemp", PPwdTemp);
                    p.Add("@PDateTimeNow", DateTime.Now);
                    p.Add("@PUserID", PUserID);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    conn.Execute("spUpdateCMSUserForPwdTemp", p, commandType: CommandType.StoredProcedure);

                    int result = p.Get<int>("@ret");

                    return result == 0 ? true : false;
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return false;
            }
        }

        public dynamic CMS_User_Insert(string PUserID, string PPwd, int PContactID, int PAddressID, int PDomainID, int PGroupID, string PModifiedBy, bool isFirstUser = false)
        {
            try
            {
                using (var conn = new SqlConnection(connStr))
                {
                    conn.Open();

                    var p = new DynamicParameters();
                    p.Add("@PUserID", PUserID);
                    p.Add("@PPwd", PPwd);
                    p.Add("@PContactID", PContactID);
                    p.Add("@PAddressID", PAddressID);
                    p.Add("@PDomainID", PDomainID);
                    p.Add("@PGroupID", PGroupID);
                    p.Add("@PDateCreated", DateTime.Now);
                    p.Add("@PModifiedBy", PModifiedBy);
                    p.Add("@isFirstUser", isFirstUser);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    var results = conn.Query<dynamic>("spInsertCMSUser", p, commandType: CommandType.StoredProcedure);

                    //CHG START DANIEL 20190211 [Return result list when inserted]
                    //return results.ToArray()[0];

                    if (results.Count() > 0)
                        return results.ToArray()[0];
                    else
                        return null;
                    //CHG E N D DANIEL 20190211 [Return result list when inserted]
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return 0;
            }
        }

        //CHG START DANIEL 20181211 [Add parameter for PGroupID]
        //public User[] CMS_User_By_DomainID(int PDomainID)
        public User[] CMS_User_By_DomainID(int PDomainID, int PGroupID)
        //CHG E N D DANIEL 20181211 [Add parameter for PGroupI]
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();
                    var p = new DynamicParameters();
                    p.Add("@PDomainID", PDomainID);
                    //ADD START DANIEL 20181211 [Add parameter for PGroupID]
                    p.Add("@PGroupID", PGroupID);
                    //ADD E N D DANIEL 20181211 [Add parameter for PGroupID]

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    var results = sqlConnection.Query<User>("spGetCMSUser", p, commandType: CommandType.StoredProcedure);

                    return results.ToArray();
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return null;
            }
        }

        public dynamic CMS_User_Update(int PDomainID, int PGroupID, string PUserID, int PStatus, string PModifiedBy, int mode)
        {
            try
            {
                using (var conn = new SqlConnection(connStr))
                {
                    conn.Open();

                    var p = new DynamicParameters();
                    p.Add("@PDomainID", PDomainID);
                    p.Add("@PGroupID", PGroupID);
                    p.Add("@PUserID", PUserID);
                    p.Add("@PStatus", PStatus);
                    p.Add("@PDateModified", DateTime.Now);
                    p.Add("@PModifiedBy", PModifiedBy);
                    p.Add("@mode", mode);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    var results = conn.Query<dynamic>("spUpdateCMSUser", p, commandType: CommandType.StoredProcedure);

                    return results.ToArray()[0];
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return 0;
            }
        }

        //ADD START DANIEL 20181212 [Add method CMS_User_Permission_By_UserID to access SQL Server]
        public UserPermission[] CMS_User_Permission_By_UserID(string PUserID, int POpsGroupID)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();
                    p.Add("@PUserID", PUserID);
                    p.Add("@POpsGroupID", POpsGroupID);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    var results = sqlConnection.Query<UserPermission>("spGetCMSUserByUserID", p, commandType: CommandType.StoredProcedure);

                    return results.ToArray();
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return null;
            }
        }
        //ADD E N D DANIEL 20181212 [Add method CMS_User_Permission_By_UserID to access SQL Server]
        #endregion

        #region CMS_CL_Status
        public CMS_CL_Status Get_CMS_CL_Status_By_Stat_ID(int PStatID)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();
                    p.Add("@PStatID", PStatID);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    var results = sqlConnection.Query<CMS_CL_Status>("spGetCMSCLStatusByStatID", p, commandType: CommandType.StoredProcedure);

                    return results.ToArray()[0];
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return null;
            }
        }
        #endregion

        #region CMS_UserPwdHistory
        public CMS_UserPwdHistory[] Get_CMS_UserPwdHistory(string PUserID)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();
                    p.Add("@PUserID", PUserID);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    var results = sqlConnection.Query<CMS_UserPwdHistory>("spGetCMSUserPwdHistory", p, commandType: CommandType.StoredProcedure);

                    return results.ToArray();
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return null;
            }
        }
        #endregion

        #region CMS_Domain
        public CMS_Domain[] Get_CMS_Domain(int PStatus, int PDomainID, string PDomainName, int PMode = 0)
        {
            // PMode - 0: Reseller and Sub Merchant, 1: Reseller only
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();
                    p.Add("@PStatus", PStatus);
                    p.Add("@PDomainID", PDomainID);
                    p.Add("@PDomainName", PDomainName);
                    p.Add("@PMode", PMode);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    var results = sqlConnection.Query<CMS_Domain>("spGetCMSDomain", p, commandType: CommandType.StoredProcedure);

                    return results.ToArray();
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return null;
            }
        }

        public DomainMerchant[] Get_CMS_Domain_Merchant(int PDomainID)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();
                    p.Add("@PDomainID", PDomainID);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    var results = sqlConnection.Query<DomainMerchant>("spGetCMSDomainMerchant", p, commandType: CommandType.StoredProcedure);

                    return results.ToArray();
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return null;
            }
        }

        public bool Update_CMS_Domain_Status(int PDomainID, int PStatus, string PModifiedBy)
        {
            try
            {
                using (var conn = new SqlConnection(connStr))
                {
                    conn.Open();

                    var p = new DynamicParameters();
                    p.Add("@PDomainID", PDomainID);
                    p.Add("@PStatus", PStatus);
                    p.Add("@PDateModified", DateTime.Now);
                    p.Add("@PModifiedBy", PModifiedBy);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    conn.Execute("spUpdateCMSDomainStatus", p, commandType: CommandType.StoredProcedure);

                    int result = p.Get<int>("@ret");

                    return (result == 0 ? true : false);
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return false;
            }
        }
        #endregion

        #region OB..TB_TxnRate
        public Txn_Summ_Modal[] Get_TB_TxnRate_Host_Status_0(string PMerchantID, DateTime PSince, DateTime PTo, string PCurrency)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();
                    p.Add("@PMerchantID", PMerchantID);
                    p.Add("@PSince", PSince);
                    p.Add("@PTo", PTo);
                    p.Add("@PCurrency", PCurrency);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    var results = sqlConnection.Query<Txn_Summ_Modal>("spGetTBTxnRateHostByMerchantIDDate", p, commandType: CommandType.StoredProcedure);

                    return results.ToArray();
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return null;
            }
        }

        public TxnRate_Joined_Host_Summ[] Get_Host_TB_TxnRate_Status_0(string PMerchantID, DateTime PSince, DateTime PTo, string PCurrency)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();
                    p.Add("@PMerchantID", PMerchantID);
                    p.Add("@PSince", PSince);
                    p.Add("@PTo", PTo);
                    p.Add("@PCurrency", PCurrency);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    var results = sqlConnection.Query<TxnRate_Joined_Host_Summ>("spGetHostTBTxnRateByMerchantIDDate", p, commandType: CommandType.StoredProcedure);

                    return results.ToArray();
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return null;
            }
        }

        public Txn_Summ_Modal[] Get_TB_TxnRate_Summary_Status_0(string PMerchantID, DateTime PSince, DateTime PTo, string PCurrency)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();
                    p.Add("@PMerchantID", PMerchantID);
                    p.Add("@PSince", PSince);
                    p.Add("@PTo", PTo);
                    p.Add("@PCurrency", PCurrency);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    var results = sqlConnection.Query<Txn_Summ_Modal>("spGetTBTxnRateSummaryByMerchantIDDate", p, commandType: CommandType.StoredProcedure);

                    return results.ToArray();
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return null;
            }
        }

        public Txn_Summ_Modal[] Get_TB_TxnRate_Summary_Status_All(string PMerchantID, DateTime PSince, DateTime PTo, string PCurrency)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();
                    p.Add("@PMerchantID", PMerchantID);
                    p.Add("@PSince", PSince);
                    p.Add("@PTo", PTo);
                    p.Add("@PCurrency", PCurrency);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    var results = sqlConnection.Query<Txn_Summ_Modal>("spGetTBTxnRateSummaryStatusAll", p, commandType: CommandType.StoredProcedure);

                    return results.ToArray();
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return null;
            }
        }

        //CHG START DANIEL 20181126 [Add last parameter PMode . 0 for Default]
        //public TxnRate_Transaction[] TB_TxnRate_SelectAll_Transaction(string PMerchantID, DateTime PSince, DateTime PTo, int[] PTxnStatus, string PPaymentID, string PGatewayTxnID,
        //    string PCustEmail, string POrderID, string PChannelType, string PChannel, string PCurrency)
        //CHG START DANIEL 20181129 [Change TxnRate_Transaction to dyanmic type]
        //public TxnRate_Transaction[] TB_TxnRate_SelectAll_Transaction(string PMerchantID, DateTime PSince, DateTime PTo, int[] PTxnStatus, string PPaymentID, string PGatewayTxnID,
        //    string PCustEmail, string POrderID, string PChannelType, string PChannel, string PCurrency, int PMode = 0)
        public dynamic[] TB_TxnRate_SelectAll_Transaction(string PMerchantID, DateTime PSince, DateTime PTo, int[] PTxnStatus, string PPaymentID, string PGatewayTxnID,
            string PCustEmail, string POrderID, string PChannelType, string PChannel, string PCurrency, int PMode = 0)
        //CHG E N D DANIEL 20181129 [Change TxnRate_Transaction to dyanmic type]
        // PMode = 0 Reseller and Sub Merchant , 1 = Reseller only
        //CHG END DANIEL 20181126 [Add last parameter PMode . 0 for Default]
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();
                    p.Add("@PMerchantID", PMerchantID);
                    p.Add("@PSince", PSince);
                    p.Add("@PTo", PTo);
                    p.Add("@PTxnStatus", string.Join(",", PTxnStatus));
                    p.Add("@PPaymentID", PPaymentID);
                    p.Add("@PGatewayTxnID", PGatewayTxnID);
                    p.Add("@PCustEmail", PCustEmail);
                    p.Add("@POrderID", POrderID);
                    p.Add("@PHostType", PChannelType);
                    p.Add("@PHostName", PChannel);
                    p.Add("@PCurrencyCode", PCurrency);
                    //ADD START DANIEL 20181126 [Add parameter PMode]
                    p.Add("@PMode", PMode);
                    //ADD END DANIEL 20181126 [Add parameter PMode]

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    //CHG START DANIEL 20181129 [Change TxnRate_Transaction to dynamic]
                    //var results = sqlConnection.Query<TxnRate_Transaction>("spRepTBTxnRate", p, commandType: CommandType.StoredProcedure);
                    var results = sqlConnection.Query<dynamic>("spRepTBTxnRate", p, commandType: CommandType.StoredProcedure, commandTimeout: 90);
                    //CHG E N D DANIEL 20181129 [Change TxnRate_Transaction to dynamic]

                    return results.ToArray();
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return null;
            }
        }

        //CHG START DANIEL 20190313 [Add parameter PCurrencyCode]
        //public Finance_Report[] TB_TxnRate_GetFinanceReport(string PMerchantID, DateTime PSince, DateTime PTo)
        public Finance_Report[] TB_TxnRate_GetFinanceReport(string PMerchantID, DateTime PSince, DateTime PTo, string PCurrencyCode = "")
        //CHG E N D DANIEL 20190313 [Add parameter PCurrencyCode]
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();
                    p.Add("@PMerchantID", PMerchantID);
                    p.Add("@PSince", PSince);
                    p.Add("@PTo", PTo);

                    //CHG START DANIEL 20190313 [Add parameter PCurrencyCode]
                    p.Add("@PCurrencyCode", PCurrencyCode);
                    //CHG START DANIEL 20190313 [Add parameter PCurrencyCode]

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    var results = sqlConnection.Query<Finance_Report>("spRepTBTxnRateFinance", p, commandType: CommandType.StoredProcedure, commandTimeout: 90);

                    return results.ToArray();
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return null;
            }
        }
        #endregion

        #region PG..TB_PayTxnRef
        //CHG START DANIEL 20181128 [Add last parameter PMode . 0 for Default]
        //public TxnRate_Transaction[] Rep_TBPayTxnRef_TBPayRes(string PMerchantID, DateTime PSince, DateTime PTo, string POBOtherStatus, string PPGOtherStatus, 
        //    string PPaymentID, string PGatewayTxnID, string PCustEmail, string POrderID, string PChannelType, string PChannel, string PCurrency)

        //CHG START DANIEL 2011129 [Change TxnRate_Transaction to dynamic type]
        //public TxnRate_Transaction[] Rep_TBPayTxnRef_TBPayRes(string PMerchantID, DateTime PSince, DateTime PTo, string POBOtherStatus, string PPGOtherStatus,
        //string PPaymentID, string PGatewayTxnID, string PCustEmail, string POrderID, string PChannelType, string PChannel, string PCurrency, int PMode = 0)
        public dynamic[] Rep_TBPayTxnRef_TBPayRes(string PMerchantID, DateTime PSince, DateTime PTo, string POBOtherStatus, string PPGOtherStatus,
         string PPaymentID, string PGatewayTxnID, string PCustEmail, string POrderID, string PChannelType, string PChannel, string PCurrency, int PMode = 0)
        //CHG E N D DANIEL 2011129 [Change TxnRate_Transaction to dynamic type]

        // PMode = 0 Reseller and Sub Merchant , 1 = Reseller only
        //CHG END DANIEL 20181126 [Add last parameter PMode . 0 for Default]
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();
                    p.Add("@PMerchantID", PMerchantID);
                    p.Add("@PSince", PSince);
                    p.Add("@PTo", PTo);
                    p.Add("@POBOtherStatus", POBOtherStatus);
                    p.Add("@PPGOtherStatus", PPGOtherStatus);
                    p.Add("@PPaymentID", PPaymentID);
                    p.Add("@PGatewayTxnID", PGatewayTxnID);
                    p.Add("@POrderID", POrderID);
                    p.Add("@PCustEmail", PCustEmail);
                    p.Add("@PHostType", PChannelType);
                    p.Add("@PHostName", PChannel);
                    p.Add("@PCurrencyCode", PCurrency);
                    //ADD START DANIEL 20181128 [Add parameter PMode]
                    p.Add("@PMode", PMode);
                    //ADD END DANIEL 20181128 [Add parameter PMode]

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    //CHG START DANIEL 20181129 [Change TxnRate_Transaction to dynamic]
                    
                    //var results = sqlConnection.Query<TxnRate_Transaction>("spRepTBPayTxnRefTBPayRes", p, commandType: CommandType.StoredProcedure);
                    var results = sqlConnection.Query<dynamic>("spRepTBPayTxnRefTBPayRes", p, commandType: CommandType.StoredProcedure, commandTimeout: 90);
                    //CHG E N D DANIEL 20181129 [Change TxnRate_Transaction to dynamic]

                    return results.ToArray();
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return null;
            }
        }
        #endregion

        #region PG..TB_PayReq
        //CHG START DANIEL 20181128 [Add last parameter PMode . 0 for Default]
        //public TxnRate_Transaction[] Rep_TBPayReq(string PMerchantID, DateTime PSince, DateTime PTo, string POBPendingStatus, string PPGPendingStatus,
        //    string PPaymentID, string PGatewayTxnID, string PCustEmail, string POrderID, string PChannelType, string PChannel, string PCurrency)

        //CHG START DANIEL 2011129 [Change TxnRate_Transaction to dynamic type]
        //public TxnRate_Transaction[] Rep_TBPayReq(string PMerchantID, DateTime PSince, DateTime PTo, string POBPendingStatus, string PPGPendingStatus,
        //        string PPaymentID, string PGatewayTxnID, string PCustEmail, string POrderID, string PChannelType, string PChannel, string PCurrency, int PMode = 0)
        public dynamic[] Rep_TBPayReq(string PMerchantID, DateTime PSince, DateTime PTo, string POBPendingStatus, string PPGPendingStatus,
         string PPaymentID, string PGatewayTxnID, string PCustEmail, string POrderID, string PChannelType, string PChannel, string PCurrency, int PMode = 0)
        //CHG E N D DANIEL 2011129 [Change TxnRate_Transaction to dynamic type]

        // PMode = 0 Reseller and Sub Merchant , 1 = Reseller only
        //CHG END DANIEL 20181126 [Add last parameter PMode . 0 for Default]
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();
                    p.Add("@PMerchantID", PMerchantID);
                    p.Add("@PSince", PSince);
                    p.Add("@PTo", PTo);
                    p.Add("@POBPendingStatus", POBPendingStatus);
                    p.Add("@PPGPendingStatus", PPGPendingStatus);
                    p.Add("@PPaymentID", PPaymentID);
                    p.Add("@PGatewayTxnID", PGatewayTxnID);
                    p.Add("@POrderID", POrderID);
                    p.Add("@PCustEmail", PCustEmail);
                    p.Add("@PHostType", PChannelType);
                    p.Add("@PHostName", PChannel);
                    p.Add("@PCurrencyCode", PCurrency);
                    //ADD START DANIEL 20181128 [Add parameter PMode]
                    p.Add("@PMode", PMode);
                    //ADD END DANIEL 20181128 [Add parameter PMode]

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    //CHG START DANIEL 20181129 [Change TxnRate_Transaction to dynamic]
                    //var results = sqlConnection.Query<TxnRate_Transaction>("spRepTBPayReq", p, commandType: CommandType.StoredProcedure);
                    var results = sqlConnection.Query<dynamic>("spRepTBPayReq", p, commandType: CommandType.StoredProcedure, commandTimeout: 90);
                    //CHG E N D DANIEL 20181129 [Change TxnRate_Transaction to dynamic]

                    return results.ToArray();
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return null;
            }
        }
        #endregion

        #region PG..Merchant
        public Merchant Merchant_SelectDetail(string PMerchantID)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();
                    p.Add("@PMerchantID", PMerchantID);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    var results = sqlConnection.Query<Merchant>("spGetMerchantByMerchantID", p, commandType: CommandType.StoredProcedure);

                    //CHG START DANIEL 20190111 [if no result, return null]
                    //return results.ToArray()[0];
                    return results.Count() == 0 ? null : results.ToArray()[0];
                    //CHG E N D DANIEL 20190111 [if no result, return null]
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return null;
            }
        }

        //CHG START DANIEL 20190311 [Add parameter isPreset True: To bind account if is SuperAdmin]
        //public MerchantInfo Get_MerchantInfo(int PDomainID)
        //CHG E N D DANIEL 20190311 [Add parameter isPreset True: To bind account if is SuperAdmin]
        public MerchantInfo Get_MerchantInfo(int PDomainID, bool isPreset)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();
                    p.Add("@PDomainID", PDomainID);
                    //ADD START DANIEL 20190311 [Add parameter isPreset]
                    p.Add("@isPreset", isPreset);
                    //ADD E M D DANIEL 20190311 [Add parameter isPreset]

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    var results = sqlConnection.Query<MerchantInfo>("spGetMerchantInfo", p, commandType: CommandType.StoredProcedure);

                    //CHG START DANIEL 20190116 [If not null, return results.ToArray()[0]]
                    //return results.ToArray()[0];
                    return results.Count() == 0 ? null : results.ToArray()[0];
                    //CHG E N D DANIEL 20190116 [If not null, return results.ToArray()[0]]
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return null;
            }
        }

        public string Get_MerchantPwd(string PUserID, string PMerchantID)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();
                    p.Add("@PUserID", PUserID);
                    p.Add("@PMerchantID", PMerchantID);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    var results = sqlConnection.Query<string>("spGetMerchantPwd", p, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    return results;
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return null;
            }
        }

        public bool Merchant_Update(string merchantID, string TradingName, string RegistrationName, string MCCCode, string ACP1Name, string ACP1Mobile,
            string ACP1Email, string CSEmail, string CSMobile, string Addr1, string Addr2, string Addr3, int CountryID, string City, string State, string PostCode, string WebSiteURL,
            int PluginID, int PymtNotificationEmail, string NotifyEmailAddr, decimal TxnLimit)
        {
            try
            {
                using (var conn = new SqlConnection(connStr))
                {
                    conn.Open();

                    var p = new DynamicParameters();
                    p.Add("@sz_MerchantID", merchantID);
                    p.Add("@sz_Desc", TradingName);
                    p.Add("@sz_RegistrationName", RegistrationName);
                    p.Add("@i_MCCCode", MCCCode);
                    p.Add("@sz_MCName", ACP1Name);
                    p.Add("@sz_MCMobile", ACP1Mobile);
                    p.Add("@sz_MCEmail", ACP1Email);
                    p.Add("@sz_EmailAddr", CSEmail);
                    p.Add("@sz_ContactNo", CSMobile);
                    p.Add("@sz_Addr1", Addr1);
                    p.Add("@sz_Addr2", Addr2);
                    p.Add("@sz_Addr3", Addr3);
                    p.Add("@i_CountryID", CountryID);
                    p.Add("@sz_City", City);
                    p.Add("@i_State", State);
                    p.Add("@sz_PostCode", PostCode);
                    p.Add("@sz_WebSiteURL", WebSiteURL);
                    p.Add("@i_PaymentPlugin", PluginID);
                    p.Add("@i_PymtNotificationEmail", PymtNotificationEmail);
                    p.Add("@sz_NotifyEmailAddr", NotifyEmailAddr);
                    p.Add("@sz_PerTxnAmtLimit", TxnLimit);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    conn.Execute("spUpdMerchant", p, commandType: CommandType.StoredProcedure);

                    int result = p.Get<int>("@ret");

                    return result == 0 ? true : false;
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return false;
            }
        }

        // DEL START SUKI 20181128 [Add in showMerchantAddr, showMerchantLogo, IsAgent, default currency setting]
        //public bool Update_MerchantGroup(string PMerchantID, int PPluginID)
        //{
        //    try
        //    {
        //        using (var conn = new SqlConnection(connStr))
        //        {
        //            conn.Open();

        //            var p = new DynamicParameters();
        //            p.Add("@PMerchantID", PMerchantID);
        //            p.Add("@PPluginID", PPluginID);

        //            p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
        //            conn.Execute("spUpdMerchantGroup", p, commandType: CommandType.StoredProcedure);

        //            int result = p.Get<int>("@ret");

        //            return result == 0 ? true : false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //}
        // DEL E N D SUKI 20181128 [Add in showMerchantAddr, showMerchantLogo, IsAgent, default currency setting]

        //CHG START DANIEL 20190116 [Add in showMerchantAddr, showMerchantLogo]
        //public bool Update_MerchantSetting(string PMerchantID, int PPluginID, int PIsAgent, string PDefaultCurrency)
        public bool Update_MerchantSetting(string PMerchantID, int PPluginID, int PIsAgent, string PDefaultCurrency, int PShowMerchantAddr, int PShowMerchantLogo)
        //CHG E N D DANIEL 20190116 [Add in showMerchantAddr, showMerchantLogo]
        {
            try
            {
                using (var conn = new SqlConnection(connStr))
                {
                    conn.Open();

                    var p = new DynamicParameters();
                    p.Add("@PMerchantID", PMerchantID);
                    p.Add("@PPluginID", PPluginID);
                    p.Add("@PIsAgent", PIsAgent);
                    p.Add("@PDefaultCurrency", PDefaultCurrency);

                    //ADD START DANIEL 20190116 [Add parameter PShowMerchantAddr and PShowMerchantLogo]
                    p.Add("@PShowMerchantAddr", PShowMerchantAddr);
                    p.Add("@PShowMerchantLogo", PShowMerchantLogo);
                    //ADD E N D DANIEL 20190116 [Add parameter PShowMerchantAddr and PShowMerchantLogo]

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    conn.Execute("spUpdateMerchantSetting", p, commandType: CommandType.StoredProcedure);

                    int result = p.Get<int>("@ret");

                    return result == 0 ? true : false;
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return false;
            }
        }
        #endregion

        #region CMS_BusinessProfile
        public int CMS_BusinessProfile_Insert(int DomainID, string CompanyRegNo, string GSTNo, string TypeOfBusiness, string OfficeNumber, string ACPName, string ACPContactNo,
            string ACPEmail, string BillContactName, string BillContactNo, string BillContactEmail, string TypeOfProducts, string TargetMarkets, string DeliveredPeriod, string CreateBy)
        {
            try
            {
                using (var conn = new SqlConnection(connStr))
                {
                    conn.Open();

                    var p = new DynamicParameters();
                    p.Add("@DomainID", DomainID);
                    p.Add("@CompanyRegNo", CompanyRegNo);
                    p.Add("@GSTNo", GSTNo);
                    p.Add("@TypeOfBusiness", TypeOfBusiness);
                    p.Add("@OfficeNumber", OfficeNumber);
                    p.Add("@ACPName", ACPName);
                    p.Add("@ACPContactNo", ACPContactNo);
                    p.Add("@ACPEmail", ACPEmail);
                    p.Add("@BillContactName", BillContactName);
                    p.Add("@BillContactNo", BillContactNo);
                    p.Add("@BillContactEmail", BillContactEmail);
                    p.Add("@TypeOfProducts", TypeOfProducts);
                    p.Add("@TargetMarkets", TargetMarkets);
                    p.Add("@DeliveredPeriod", DeliveredPeriod);
                    p.Add("@CreateDate", DateTime.Now);
                    p.Add("@Enable", true);
                    p.Add("@CreateBy", CreateBy);
                    p.Add("@newID", dbType: DbType.Int32, direction: ParameterDirection.Output);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    conn.Execute("spInsertUpdateCMSBusinessProfile", p, commandType: CommandType.StoredProcedure);

                    int result = p.Get<int>("@ret");
                    int newID = p.Get<int>("@newID");

                    return newID;
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return 0;
            }
        }
        #endregion

        #region CMS_BusinessOwner
        public int CMS_BusinessOwner_InsertUpdate(int BusinessProfileID, string FullName, string ID, string Type, int Seq, string ModifiedBy)
        {
            try
            {
                using (var conn = new SqlConnection(connStr))
                {
                    conn.Open();

                    var p = new DynamicParameters();
                    p.Add("@BusinessProfileID", BusinessProfileID);
                    p.Add("@FullName", FullName);
                    p.Add("@Desc", ID);
                    p.Add("@Type", Type);
                    p.Add("@Seq", Seq);
                    p.Add("@CreateDate", DateTime.Now);
                    p.Add("@ModifiedBy", ModifiedBy);
                    p.Add("@Enable", true);
                    p.Add("@newID", dbType: DbType.Int32, direction: ParameterDirection.Output);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    conn.Execute("spInsertUpdateCMSBusinessOwner", p, commandType: CommandType.StoredProcedure);

                    int result = p.Get<int>("@ret");
                    int newID = p.Get<int>("@newID");

                    return newID;
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return 0;
            }
        }

        public BusinessOwner[] CMS_BusinessOwner_Get(int PBusinessProfileID)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();
                    p.Add("@PBusinessProfileID", PBusinessProfileID);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    var results = sqlConnection.Query<BusinessOwner>("spGetCMSBusinessOwner", p, commandType: CommandType.StoredProcedure);

                    return results.ToArray();
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return null;
            }
        }
        #endregion

        #region CMS_MerchantBank
        public int CMS_MerchantBank_InsertUpdate(int DomainID, string BankName, string BankAccHolder, string BankAccNo, string SWIFTCode, string BankAddress, string BankCountry, string ModifiedBy)
        {
            try
            {
                using (var conn = new SqlConnection(connStr))
                {
                    conn.Open();

                    var p = new DynamicParameters();
                    p.Add("@DomainID", DomainID);
                    p.Add("@Alias", BankName + "-" + "MYR");
                    p.Add("@BankName", BankName);
                    p.Add("@BankAccHolder", BankAccHolder);
                    p.Add("@BankAccNo", BankAccNo);
                    p.Add("@SWIFTCode", SWIFTCode);
                    p.Add("@BankAddress", BankAddress);
                    p.Add("@BankCountry", BankCountry);
                    p.Add("@Currency", "MYR");
                    p.Add("@SettlementRoutine", 7);
                    p.Add("@MinimumWithrawal", 0);
                    p.Add("@LastSettlementDate", DateTime.Now);
                    p.Add("@Enable", true);
                    p.Add("@CreateDate", DateTime.Now);
                    p.Add("@ModifiedBy", ModifiedBy);


                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    conn.Execute("spInsertUpdateCMSMerchantBank", p, commandType: CommandType.StoredProcedure);

                    int result = p.Get<int>("@ret");

                    return result;
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return 0;
            }
        }
        #endregion

        #region CMS_UserGroup
        public dynamic CMS_UserGroup_Insert(int PDomainID, string PGroupName, string PGroupDesc, string PModifiedBy)
        {
            try
            {
                using (var conn = new SqlConnection(connStr))
                {
                    conn.Open();

                    var p = new DynamicParameters();
                    p.Add("@PDomainID", PDomainID);
                    p.Add("@PGroupName", PGroupName);
                    p.Add("@PGroupDesc", PGroupDesc);
                    p.Add("@PDateCreated", DateTime.Now);
                    p.Add("@PModifiedBy", PModifiedBy);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    var results = conn.Query<dynamic>("spInsertCMSUserGroup", p, commandType: CommandType.StoredProcedure);

                    return results.ToArray()[0];
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return 0;
            }
        }

        //CHG START DANIEL 20190215 [Add parameter PUserID]
        //public UserGroup[] CMS_UserGroup_By_DomainID(int PDomainID)
        public UserGroup[] CMS_UserGroup_By_DomainID(int PDomainID, string PUserID = "")
        //CHG E N D DANIEL 20190215 [Add parameter PUserID]
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();
                    p.Add("@PDomainID", PDomainID);
                    //ADD START DANIEL 20190215 [Add parameter PUserID]
                    p.Add("@PUserID", PUserID);
                    //ADD E N D DANIEL 20190215 [Add parameter PUserID]

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    var results = sqlConnection.Query<UserGroup>("spGetCMSUserGroup", p, commandType: CommandType.StoredProcedure);

                    return results.ToArray();
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return null;
            }
        }

        //ADD START DANIEL 20181203 [Add the method to retrieve list of operation group ]
        //CHG START DANIEL 20181213 [Add PMode 0 = All OpsGroup , PMode 1 = Allowed OpsGroup]
        //public OperationGroup[] CMS_OperationGroup_By_GroupID(int PUserGroupID)
        public OperationGroup[] CMS_OperationGroup_By_GroupID(int PUserGroupID, int PMode)
        //CHG E N D DANIEL 20181213 [Add PMode 0 = All OpsGroup , PMode 1 = Allowed OpsGroup]
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();
                    p.Add("@PUserGroupID", PUserGroupID);
                    //ADD START DANIEL 20181213 [Add parameter PMode]
                    p.Add("@PMode", PMode);
                    //ADD E N D DANIEL 20181213 [Add parameter PMode]

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    var results = sqlConnection.Query<OperationGroup>("spGetCMSOperationGroup", p, commandType: CommandType.StoredProcedure);
                    return results.ToArray();
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return null;
            }
        }
        //ADD E N D DANIEL 20181203 [Add the method to retrieve list of operation group ]

        //ADD START DANIEL 20181210 [Add method to insert or update into CMS_UserGroupMap]

        //CHG START DANIEL 20190104 [Change method name]
        //public int CMS_Insert_Update_OperationGroupMap(int PUserGroupID, int POpsGroupID, int PMapValue)
        public int CMS_Insert_Update_UserGroupMap(int PUserGroupID, int POpsGroupID, int PMapValue)
        //CHG E N D DANIEL 20190104 [Change method name]

        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();
                    p.Add("@PUserGroupID", PUserGroupID);
                    p.Add("@POpsGroupID", POpsGroupID);
                    p.Add("@PMapValue", PMapValue);
                    p.Add("@PDateCreated", DateTime.Now);
                    p.Add("@newID", dbType: DbType.Int32, direction: ParameterDirection.Output);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    sqlConnection.Execute("spInsertUpdateCMSUserGroupMap", p, commandType: CommandType.StoredProcedure);

                    int result = p.Get<int>("@ret");
                    int newID = p.Get<int>("@newID");

                    return newID;
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return 0;
            }
        }
        //ADD E N D DANIEL 20181210 [Add method to insert or update CMS_UserGroupMap]

        //ADD START DANIEL 20181213 [Add method to insert or update CMS_UserMap]
        public int CMS_Insert_Update_UserMap(string PUserID, int POpsGroupID, int PMapValue)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();
                    p.Add("@PUserID", PUserID);
                    p.Add("@POpsGroupID", POpsGroupID);
                    p.Add("@PMapValue", PMapValue);
                    p.Add("@PDateCreated", DateTime.Now);

                    //DEL START DANIEL 20190102 [Remove parameter newID as output]
                    //p.Add("@newID", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    //DEL E ND DANIEL 20190102 [Remove parameter newID as output]

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    sqlConnection.Execute("spInsertUpdateCMSUserMap", p, commandType: CommandType.StoredProcedure);

                    int result = p.Get<int>("@ret");

                    //DEL START DANIEL 20190102 [Remove newID assignment]
                    //int newID = p.Get<int>("@newID");
                    //DEL START DANIEL 20190102 [Remove newID assignment]

                    //CHG START DANIEL 20190102 [Change return newID to return result]
                    //return newID;
                    return result;
                    //CHG E N D DANIEL 20190102 [Change return newID to return result]
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return 0;
            }
        }
        //ADD E N D DANIEL 20181213 [Add method to insert or update CMS_UserMap]

        public dynamic CMS_UserGroup_Update(int PDomainID, int PGroupID, string PGroupDesc, int PGroupStatus, string PModifiedBy)
        {
            try
            {
                using (var conn = new SqlConnection(connStr))
                {
                    conn.Open();

                    var p = new DynamicParameters();
                    p.Add("@PDomainID", PDomainID);
                    p.Add("@PGroupID", PGroupID);
                    p.Add("@PGroupDesc", PGroupDesc);
                    p.Add("@PGroupStatus", PGroupStatus);
                    p.Add("@PDateModified", DateTime.Now);
                    p.Add("@PModifiedBy", PModifiedBy);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    var results = conn.Query<dynamic>("spUpdateCMSUserGroup", p, commandType: CommandType.StoredProcedure);

                    return results.ToArray()[0];
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return 0;
            }
        }
        #endregion

        #region PG..Terminal
        //CHG START DANIEL 20181205 [add new parameter PPymtMethod ALL, OB, CC | PMode = 0 : Default , PMode = 1 : Single Selection]
        //public TerminalHost[] Get_TerminalHost(int PDomainID)
        //CHG START DANIEL 20181206 [Remove parameter PMode]
        //public TerminalHost[] Get_TerminalHost(int PDomainID, int PMode, string PPymtMethod)

        //CHG START DANIEL 20181226 [Add parameter PStatus]
        //public TerminalHost[] Get_TerminalHost(int PDomainID, string PPymtMethod)
        public TerminalHost[] Get_TerminalHost(int PDomainID, string PPymtMethod, int PStatusYesNo)
        //CHG E N D DANIEL 20181226 [Add parameter PStatus]

        //CHG E N D DANIEL 20181206 [Remove parameter PMode]
        //CHG E N D DANIEL 20181205 [add new parameter PPymtMethod ALL, OB, CC | PMode = 0 : Default , PMode = 1 : Single Selection]
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();
                    p.Add("@PDomainID", PDomainID);
                    //ADD START DANIEl 20181205 [Add new parameter PPymtMethod, PMode]
                    p.Add("@PPymtMethod", PPymtMethod);
                    //DEL START DANIEL 20181206 [Remove parameter PMode]
                    //p.Add("@PMode", PMode);
                    //DEL E N D DANIEL 20181206 [Remove parameter PMode]

                    //ADD E N D DANIEl 20181205 [Add new parameter PPymtMethod, PMode]

                    //ADD START DANIEL 20181226 [Add new pararmeter PStatus]
                    p.Add("@PStatusYesNo", PStatusYesNo);
                    //ADD E N D DANIEL 20181226 [Add new pararmeter PStatus]

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);

                    var results = sqlConnection.Query<TerminalHost>("spGetTerminalHost", p, commandType: CommandType.StoredProcedure);

                    return results.ToArray();
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return null;
            }
        }

        public int Terminal_Insert(int PDomainID, string PPymtMethod, int PHostID, string PFieldName1, string PCredential1, string PFieldName2, string PCredential2,
            string PFieldName3, string PCredential3, string PFieldName4, string PCredential4, string PFieldName5, string PCredential5, string PFieldName6, string PCredential6,
            string PCurrencyCode, int PPriority)
        {
            try
            {
                using (var conn = new SqlConnection(connStr))
                {
                    conn.Open();

                    var p = new DynamicParameters();
                    p.Add("@PDomainID", PDomainID);
                    p.Add("@PPymtMethod", PPymtMethod);
                    p.Add("@PHostID", PHostID);
                    p.Add("@PFieldName1", PFieldName1);
                    p.Add("@PCredential1", PCredential1);
                    p.Add("@PFieldName2", PFieldName2);
                    p.Add("@PCredential2", PCredential2);
                    p.Add("@PFieldName3", PFieldName3);
                    p.Add("@PCredential3", PCredential3);
                    p.Add("@PFieldName4", PFieldName4);
                    p.Add("@PCredential4", PCredential4);
                    p.Add("@PFieldName5", PFieldName5);
                    p.Add("@PCredential5", PCredential5);
                    p.Add("@PFieldName6", PFieldName6);
                    p.Add("@PCredential6", PCredential6);
                    p.Add("@PCurrencyCode", PCurrencyCode);
                    p.Add("@PPriority", PPriority);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    conn.Execute("spInsertTerminal", p, commandType: CommandType.StoredProcedure);

                    int result = p.Get<int>("@ret");

                    return result;
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return 1;
            }
        }
        #endregion

        #region PG..MerchantRate
        //CHG START DANIEL 20190114 [Add parameter isReseller = 0: False, 1 =  True]
        //public MerchantRateHost[] Get_MerchantRateHost(int PDomainID)
        public MerchantRateHost[] Get_MerchantRateHost(int PDomainID, int isReseller = 0)
        //CHG E N D DANIEL 20190114 [Add parameter isReseller = 0: False, 1 =  True]
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();
                    p.Add("@PDomainID", PDomainID);

                    //ADD START DANIEL 20190114 [Add parameter isReseller]
                    p.Add("@isReseller", isReseller);
                    //ADD E N D DANIEL 20190114 [Add parameter isReseller]

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    var results = sqlConnection.Query<MerchantRateHost>("spGetMerchantRate", p, commandType: CommandType.StoredProcedure);

                    return results.ToArray();
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return null;
            }
        }

        //ADD START DANIEL 20181205 [Add MerchantRate_Insert to retrieve data]
        //CHG START DANIEL 20190116 [Add isReseller]
        //public int MerchantRate_Insert(decimal PFromAmt, decimal PToAmt, string PMerchantID, string PPymtMethod, int PHostID, DateTime PStartDate, decimal PRate, decimal PFixedFee, string PCurrency)
        public int MerchantRate_Insert(decimal PFromAmt, decimal PToAmt, string PMerchantID, string PPymtMethod, int PHostID, DateTime PStartDate, decimal PRate, decimal PFixedFee, string PCurrency, bool isReseller)
        //CHG E N D DANIEL 20190116 [Add isReseller]
        {
            try
            {
                using (var conn = new SqlConnection(connStr))
                {
                    conn.Open();

                    var p = new DynamicParameters();
                    p.Add("@FromAmt", PFromAmt);
                    p.Add("@ToAmt", PToAmt);
                    p.Add("@MerchantID", PMerchantID);
                    p.Add("@PymtMethod", PPymtMethod);
                    p.Add("@HostID", PHostID);
                    p.Add("@StartDate", PStartDate);
                    p.Add("@Rate", PRate);
                    p.Add("@FixedFee", PFixedFee);
                    p.Add("@Currency", PCurrency);

                    //ADD START DANIEL 20190116 [Add parameter isReseller]
                    p.Add("@isReseller", isReseller);
                    //ADD E N D DANIEL 20190116 [Add parameter isReseller]

                    p.Add("@newID", dbType: DbType.Int32, direction: ParameterDirection.Output);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    conn.Execute("spInsert_MerchantRate", p, commandType: CommandType.StoredProcedure);

                    int result = p.Get<int>("@ret");
                    int newID = p.Get<int>("@newID");

                    return newID;
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return 0;
            }
        }
        //ADD E N D DANIEL 20181205 [Add MerchantRate_Insert to retrieve data]


        //ADD START KENT LOONG 20190311 [GET Merchant Future Rate isReseller = 0: False, 1 =  True]
        public MerchantFutureRateHost[] Get_MerchantFutureRateHost(int PDomainID, int isReseller = 0)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();
                    p.Add("@PDomainID", PDomainID);
                    p.Add("@isReseller", isReseller);
                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    var results = sqlConnection.Query<MerchantFutureRateHost>("spGetMerchantFutureRate", p, commandType: CommandType.StoredProcedure);
                    return results.ToArray();
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return null;
            }
        }
        //ADD E N D KENT LOONG 20190311 [GET Merchant Future Rate isReseller = 0: False, 1 =  True]

        //ADD START KENT LOONG 20190312 [GET Merchant Future Rate BY PKID isReseller = 0: False, 1 =  True]
        public MerchantFutureRateHost Get_MerchantFutureRateByPKID(int PKID, int isReseller = 0)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();
                    p.Add("@PKID", PKID);
                    p.Add("@isReseller", isReseller);
                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    var results = sqlConnection.Query<MerchantFutureRateHost>("spGetMerchantFutureRateByPKID", p, commandType: CommandType.StoredProcedure);
                    return results.Count() == 0 ? null : results.ToArray()[0];
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return null;
            }
        }
        //ADD E N D KENT LOONG 20190312 [GET Merchant Future Rate BY PKID isReseller = 0: False, 1 =  True]
        #endregion

        #region PG..CL_PymtMethod
        public CL_PymtMethod[] GetCLPymtMethod()
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    var results = sqlConnection.Query<CL_PymtMethod>("spGetCLPymtMethod", p, commandType: CommandType.StoredProcedure);

                    return results.ToArray();
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return null;
            }
        }
        #endregion

        #region Host
        public ChannelHost[] Get_HostByType(string PPymtMethod)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();
                    p.Add("@PPymtMethod", PPymtMethod);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    var results = sqlConnection.Query<ChannelHost>("spGetHostByType", p, commandType: CommandType.StoredProcedure);

                    return results.ToArray();
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return null;
            }
        }

        //DEL START SUKI 20181212 [Unnecessary code]
        ////ADD START DANIEL 20181203 [Add method to get host by type and id]
        //public ChannelHost[] Get_HostByType_DomainID(string PPymtMethod, int PDomainID)
        //{
        //    try
        //    {
        //        using (var sqlConnection = new SqlConnection(connStr))
        //        {
        //            sqlConnection.Open();

        //            var p = new DynamicParameters();
        //            p.Add("@PPymtMethod", PPymtMethod);
        //            p.Add("@PDomainID", PDomainID);

        //            p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
        //            var results = sqlConnection.Query<ChannelHost>("spGetHostByType_DomainID", p, commandType: CommandType.StoredProcedure);

        //            return results.ToArray();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        system_err = ex.ToString();
        //        return null;
        //    }
        //}
        ////ADD E N D DANIEL 20181203 [Add method to get host by type and id]
        //DEL E N D SUKI 20181212 [Unnecessary code]
        #endregion

        #region PG..Terminals_PreSetCredential
        //public HostCredential[] Get_HostCredential(string PPymtMethodCode, int PHostID)
        public HostCredential[] Get_HostCredential(string PPymtMethodCode, int PHostID, string PCurrencyCode) // Modified by Daniel 23 Apr 2019. Add PCurrencyCode param
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();
                    p.Add("@PPymtMethodCode", PPymtMethodCode);
                    p.Add("@PHostID", PHostID);
                    p.Add("@PCurrencyCode", PCurrencyCode); // Added by Daniel 23 Apr 2019. Add PCurrencyCode param

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    var results = sqlConnection.Query<HostCredential>("spGetHostCredential", p, commandType: CommandType.StoredProcedure);

                    return results.ToArray();
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return null;
            }
        }
        #endregion

        #region PG..CurrencyCountry
        public CurrencyCode[] Get_CurrencyCountry(int PMode)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();
                    p.Add("@PMode", PMode);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    var results = sqlConnection.Query<CurrencyCode>("spGetCurrencyCountry", p, commandType: CommandType.StoredProcedure);

                    return results.ToArray();
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return null;
            }
        }
        #endregion

        #region PG..PGM_CardGroup
        public PGMCardGroup[] Get_PGMCardGroup()
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    var results = sqlConnection.Query<PGMCardGroup>("spGetPGMCardGroup", p, commandType: CommandType.StoredProcedure);

                    return results.ToArray();
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return null;
            }
        }
        #endregion

        #region PG..PGM_Routing
        //CHG START DANIEl 20181221 [Add parameter PToDelete = 1:  Delete record]
        //public int PGM_Routing_Insert(int PDomainID, int PHostID, int PCardGrpID, string PCurrencyCode, int PPriority)
        public int PGM_Routing_Insert(int PDomainID, int PHostID, int PCardGrpID, string PCurrencyCode, int PPriority, int PToDelete = 0)
        //CHG E N D DANIEl 20181221 [Add parameter PToDelete = true:  Delete record]

        {
            try
            {
                using (var conn = new SqlConnection(connStr))
                {
                    conn.Open();

                    var p = new DynamicParameters();
                    p.Add("@PDomainID", PDomainID);
                    p.Add("@PHostID", PHostID);
                    p.Add("@PCardGrpID", PCardGrpID);
                    p.Add("@PCurrencyCode", PCurrencyCode);
                    p.Add("@PPriority", PPriority);

                    //ADD START DANIEL 20181221 [Add parameter PToDelete]
                    p.Add("@PToDelete", PToDelete);
                    //ADD E N D DANIEL 20181221 [Add parameter PToDelete]

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    conn.Execute("spInsertPGMRouting", p, commandType: CommandType.StoredProcedure);

                    int result = p.Get<int>("@ret");

                    return result;
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return 0;
            }
        }
        #endregion

        #region PG..FDS_ContinueRejectRules
        public int FDS_ContinueRejectRules_Insert(int PDomainID)
        {
            try
            {
                using (var conn = new SqlConnection(connStr))
                {
                    conn.Open();

                    var p = new DynamicParameters();
                    p.Add("@PDomainID", PDomainID);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    conn.Execute("spInsertFDSContinueRejectRules", p, commandType: CommandType.StoredProcedure);

                    int result = p.Get<int>("@ret");

                    return result;
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return 0;
            }
        }
        #endregion

        #region PG..FDS_FraudRules
        public int FDS_FraudRules_Insert(int PDomainID, int PPriority)
        {
            try
            {
                using (var conn = new SqlConnection(connStr))
                {
                    conn.Open();

                    var p = new DynamicParameters();
                    p.Add("@PDomainID", PDomainID);
                    p.Add("@PPriority", PPriority);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    conn.Execute("spInsertFDSFraudRules", p, commandType: CommandType.StoredProcedure);

                    int result = p.Get<int>("@ret");

                    return result;
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return 0;
            }
        }
        #endregion

        #region CardTypeProfile
        public CardTypeProfile[] Get_CardTypeProfile(int PDomainID)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();
                    p.Add("@PDomainID", PDomainID);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    var results = sqlConnection.Query<CardTypeProfile>("spGetCardTypeProfile", p, commandType: CommandType.StoredProcedure);

                    return results.ToArray();
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return null;
            }
        }

        public int CardTypeProfile_Insert(int PDomainID, int PVisa, int PMasterCard, int PAMEX, int PDiners, int PJCB, int PCUP, int PMasterPass, int PVisaCheckout, int PSamsungPay)
        {
            try
            {
                using (var conn = new SqlConnection(connStr))
                {
                    conn.Open();

                    var p = new DynamicParameters();
                    p.Add("@PDomainID", PDomainID);
                    p.Add("@PVisa", PVisa);
                    p.Add("@PMasterCard", PMasterCard);
                    p.Add("@PAMEX", PAMEX);
                    p.Add("@PDiners", PDiners);
                    p.Add("@PJCB", PJCB);
                    p.Add("@PCUP", PCUP);
                    p.Add("@PMasterPass", PMasterPass);
                    p.Add("@PVisaCheckout", PVisaCheckout);
                    p.Add("@PSamsungPay", PSamsungPay);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    conn.Execute("spInsertUpdateCardTypeProfile", p, commandType: CommandType.StoredProcedure);

                    int result = p.Get<int>("@ret");

                    return result;
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return 0;
            }
        }
        #endregion

        #region OB..HostCurrency
        //CHG START DANIEL 20181221 [Add 3 parameters PPriority , PStatus, PMode = 0:Insert, PMode = 1:Edit, PMode =2:Delete]
        //public int HostCurrency_Insert(int PDomainID, string PCurrencyCode, int PHostID, string PPymtMethod)
        public int HostCurrency_Insert(int PDomainID, string PCurrencyCode, int PHostID, string PPymtMethod, int PPriority, int PStatus, int PMode = 0)
        //CHG E N D DANIEL 20181221 [Add 3 parameters PPriority , PStatus, PMode = 0:Insert, PMode = 1:Edit, PMode =2:Delete]

        {
            try
            {
                using (var conn = new SqlConnection(connStr))
                {
                    conn.Open();

                    var p = new DynamicParameters();
                    p.Add("@PDomainID", PDomainID);
                    p.Add("@PCurrencyCode", PCurrencyCode);
                    p.Add("@PHostID", PHostID);
                    p.Add("@PPymtMethod", PPymtMethod);

                    //ADD START DANIEL 20181221 [Add parameter PPriority and Pstatus and PToDelete]
                    p.Add("@PPriority", PPriority);
                    p.Add("@PStatus", PStatus);
                    p.Add("@PMode", PMode);
                    //ADD E N D DANIEL 20181221 [Add parameter PPriority and Pstatus and PToDelete]

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    conn.Execute("spInsertHostCurrency", p, commandType: CommandType.StoredProcedure);

                    int result = p.Get<int>("@ret");

                    return result;
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return 0;
            }
        }
        #endregion

        #endregion

        //Added by DANIEL 19 Dec 2018. Pass parameters to stored procedure spGetTerminalCredential and return results
        //public TerminalCredential[] Get_Terminal_Credentials(int PDomainID, string PPymtMethod, int PHostID, string PCredential)      Modified by DANIEL 23 Jan 2019. Add parameter PCurrencyCode
        public TerminalCredential[] Get_Terminal_Credentials(int PDomainID, string PPymtMethod, int PHostID, string PCredential, string PCurrencyCode)
        {
            try
            {
                using (var conn = new SqlConnection(connStr))
                {
                    conn.Open();

                    var p = new DynamicParameters();
                    p.Add("@PDomainID", PDomainID);
                    p.Add("@PPymtMethod", PPymtMethod);
                    p.Add("@PHostID", PHostID);
                    p.Add("@PCredential", PCredential);
                    p.Add("@PCurrencyCode", PCurrencyCode);         //Modified by DANIEL 23 Jan 2019. Add parameter PCurrencyCode
                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);

                    var results = conn.Query<TerminalCredential>("spGetTerminalCredential", p, commandType: CommandType.StoredProcedure);

                    return results.ToArray();
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return null;
            }
        }

       //Added by DANIEL 20 Dec 2018. Pass parameters to stored procedure spGetPGMRouting and return results
        //public PGM_CardGroup[] Get_PGM_Routing(int PDomainID, int PHostID)
        public PGM_CardGroup[] Get_PGM_Routing(int PDomainID, int PHostID, string PCurrency) //Modified By Daniel 39 Apr 2019. Add PCurrency.
        {
            try
            {
                using (var conn = new SqlConnection(connStr))
                {
                    conn.Open();

                    var p = new DynamicParameters();
                    p.Add("@PDomainID", PDomainID);
                    p.Add("@PHostID", PHostID);
                    //Added by Daniel 30 Apr 2019. Add PCurrency.
                    p.Add("@PCurrency", PCurrency);
                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    var results = conn.Query<PGM_CardGroup>("spGetPGMRouting", p, commandType: CommandType.StoredProcedure);

                    return results.ToArray();
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return null;
            }
        }

        //Added by DANIEl 21 Dec 2018. Pass parameters to stored procedure spUpdateTerminal and return results
        //Modified by DANIEL 21 Dec 2018. Change method type to bool
        //  public int Terminal_Update(int PDomainID, string PPymtMethod, int PHostID, string PFieldName1, string PCredential1, string PFieldName2, string PCredential2,
        //string PFieldName3, string PCredential3, string PFieldName4, string PCredential4, string PFieldName5, string PCredential5, string PFieldName6, string PCredential6,
        //string PCurrencyCode, int PPriority, int PStatus)
        public bool Terminal_Update(int PDomainID, string PPymtMethod, int PHostID, string PFieldName1, string PCredential1, string PFieldName2, string PCredential2,
        string PFieldName3, string PCredential3, string PFieldName4, string PCredential4, string PFieldName5, string PCredential5, string PFieldName6, string PCredential6,
        string PCurrencyCode, int PPriority, int PStatus)
        {
            try
            {
                using (var conn = new SqlConnection(connStr))
                {
                    conn.Open();

                    var p = new DynamicParameters();
                    p.Add("@PDomainID", PDomainID);
                    p.Add("@PPymtMethod", PPymtMethod);
                    p.Add("@PHostID", PHostID);
                    p.Add("@PFieldName1", PFieldName1);
                    p.Add("@PCredential1", PCredential1);
                    p.Add("@PFieldName2", PFieldName2);
                    p.Add("@PCredential2", PCredential2);
                    p.Add("@PFieldName3", PFieldName3);
                    p.Add("@PCredential3", PCredential3);
                    p.Add("@PFieldName4", PFieldName4);
                    p.Add("@PCredential4", PCredential4);
                    p.Add("@PFieldName5", PFieldName5);
                    p.Add("@PCredential5", PCredential5);
                    p.Add("@PFieldName6", PFieldName6);
                    p.Add("@PCredential6", PCredential6);
                    p.Add("@PCurrencyCode", PCurrencyCode);
                    p.Add("@PPriority", PPriority);
                    p.Add("@PStatus", PStatus);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);

                    conn.Execute("spUpdateTerminal", p, commandType: CommandType.StoredProcedure);

                    int result = p.Get<int>("@ret");
                    //return result;    Modified by DANIEL 2 Jan 2019. If return = 0 then return true
                    return result == 0 ? true : false;
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return false;       //return 1;  Modified by DANIEL 2 Jan 2019. Return 1 to return false
            }
        }

        //Added by DANIEL 4 Jan 2019. Insert records into AuditTrail table
        public bool AuditTrail_Insert(string PUserID, int POpsID, string PEvent, string PDescription)
        {
            try
            {
                using (var conn = new SqlConnection(connStr))
                {
                    conn.Open();
                    var p = new DynamicParameters();

                    p.Add("@PDateCreated", DateTime.Now);
                    p.Add("@PUserID", PUserID);
                    p.Add("@POpsID", POpsID);
                    p.Add("@PEvent", PEvent);
                    p.Add("@PDescription", PDescription);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    conn.Execute("spInsertAuditTrail", p, commandType: CommandType.StoredProcedure);

                    int result = p.Get<int>("@ret");
                    return result == 0 ? true : false;
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return false;
            }
        }

        //Addd by DANIEL 17 Jan 2019. Add method TB_TxnRateReseller to retrieve transaction rate reseller records
        //public Finance_Report[] TB_TxnRateReseller(string PMerchantID, DateTime PSince, DateTime PTo, string PCurrencyCode = "")    //Modified by DANIEL 13 Mar 2019. Add parameter PCurrencyCode
         public ResellerMarginReport[] TB_TxnRateReseller(string PMerchantID, DateTime PSince, DateTime PTo, string PCurrencyCode = "")  //Added by Daniel 11 Apr 2019. Change to ResellerMarginReport
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();
                    p.Add("@PMerchantID", PMerchantID);
                    p.Add("@PSince", PSince);
                    p.Add("@PTo", PTo);
                    p.Add("@PCurrencyCode", PCurrencyCode);     //Modified by DANIEL 13 Mar 2019. Add parameter PCurrencyCode
                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    var results = sqlConnection.Query<ResellerMarginReport>("spRepTB_TxnRateReseller", p, commandType: CommandType.StoredProcedure);

                    return results.ToArray();
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return null;
            }
        }

        //Added by DANIEL 28 Jan 2019. Insert record in BuyNowButton
        public int Do_InsertTokenBNB(string PMerchantID, string POrderDesc, string POrderNumber, string PCurrencyCode, decimal PAmount, string strToken)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();
                    p.Add("@PToken", strToken);
                    p.Add("@PMerchantID", PMerchantID);
                    p.Add("@POrderDesc", POrderDesc);
                    p.Add("@POrderNumber", POrderNumber);
                    p.Add("@PCurrencyCode", PCurrencyCode);
                    p.Add("@PAmount", PAmount);
                    p.Add("@PCreatedDay", DateTime.Now.ToString("yyyyMMdd"));
                    p.Add("@PDateCreated", DateTime.Now);
                    p.Add("@PDateModified", DateTime.Now);
                    //p.Add("@newID", dbType: DbType.Int32, direction: ParameterDirection.Output);      Commented by DANIEL 15 Mar 2019. @newID not needed
                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);

                    sqlConnection.Execute("spInsertTokenBNB", p, commandType: CommandType.StoredProcedure);

                    int result = p.Get<int>("@ret");

                    //Commented by DANIEL 15 Mar 2019. @newID not needed
                    //int newID = p.Get<int>("@newID");
                    //return newID;
                    return result;
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return 0;
            }
        }

        //Added by DANIEL 29 Jan 2019. Insert record in PaymentLink
        public int Do_InsertPaymentLink(string PCustName, string PCustContNo, string PCustEmail, string strToken, int PReminder, int PRecStatus)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();
                    p.Add("@PCustName", PCustName);
                    p.Add("@PCustContNo", PCustContNo);
                    p.Add("@PCustEmail", PCustEmail);
                    p.Add("@PToken", strToken);
                    p.Add("@PSendDate", DateTime.Now);
                    p.Add("@PExpiryDate", DateTime.Now.AddDays(7)); //Expired after 1 week
                    p.Add("@PReminder", PReminder);
                    p.Add("@PCreatedDay", DateTime.Now.ToString("yyyyMMdd"));
                    p.Add("@PDateCreated", DateTime.Now);
                    p.Add("@PDateModified", DateTime.Now);
                    p.Add("@PRecStatus", PRecStatus);
                    p.Add("@PExpiryDateInt", DateTime.Now.AddDays(7).ToString("yyyyMMdd"));

                    p.Add("@newID", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    sqlConnection.Execute("spInsertPaymentLink", p, commandType: CommandType.StoredProcedure);

                    int result = p.Get<int>("@ret");
                    int newID = p.Get<int>("@newID");

                    return newID;
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return 0;
            }
        }

        //Added by DANIEL 12 Feb 2019. Retrieve late response transaction
        public TB_LateRes[] Get_TB_LateRes(string PGatewayTxnID, string PMerchantID, DateTime PSince, DateTime PTo)
        {
            try
            {
                using (var conn = new SqlConnection(connStr))
                {
                    conn.Open();

                    var p = new DynamicParameters();
                    p.Add("@PGatewayTxnID", PGatewayTxnID);
                    p.Add("@PMerchantID", PMerchantID);
                    p.Add("@PSince", PSince);
                    p.Add("@PTo", PTo);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    var results = conn.Query<TB_LateRes>("spRepTBLateRes", p, commandType: CommandType.StoredProcedure);

                    return results.ToArray();
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return null;
            }
        }

        //Added by DANIEL 19 Feb 2019. Retrieve data from TB_PayRes
        public Manage_Txn_Status[] Get_TB_PayRes(string PMerchantID, string PGatewayTxnID, DateTime PSince, DateTime PTo, string PPaymentID, string PHostType, string PCurrencyCode)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();
                    p.Add("@PMerchantID", PMerchantID);
                    p.Add("@PHostType", PHostType);
                    p.Add("@PSince", PSince);
                    p.Add("@PTo", PTo);
                    p.Add("@PPaymentID", PPaymentID);
                    p.Add("@PGatewayTxnID", PGatewayTxnID);
                    p.Add("@PCurrencyCode", PCurrencyCode);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    var results = sqlConnection.Query<Manage_Txn_Status>("spRepTBPayRes", p, commandType: CommandType.StoredProcedure);

                    return results.ToArray();
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return null;
            }
        }

        //Added by DANIEL 21 Feb 2019. Change status and update TB_PayRes and TB_TxnRate
        public bool Do_Update_PayRes_TxnRate(string PGatewayTxnID, string PPymtMethod, int PStatus, int PState)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();
                    p.Add("@PGatewayTxnID", PGatewayTxnID);
                    p.Add("@PPymtMethod", PPymtMethod);
                    p.Add("@PStatus", PStatus);
                    p.Add("@PState", PState);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    sqlConnection.Execute("spUpdate_PayRes_TxnRate_Status", p, commandType: CommandType.StoredProcedure);

                    int result = p.Get<int>("@ret");

                    return result == 0 ? true : false;
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return false;
            }
        }

        //Added by KENT LOONG 13 Mar 2019. Add Merchant Future Rate Update isReseller
        public bool MerchantFutureRate_Update(int PHostID, decimal PRate, decimal PFixedFee, bool isReseller)
        {
            try
            {
                using (var conn = new SqlConnection(connStr))
                {
                    conn.Open();

                    var p = new DynamicParameters();
                    p.Add("@PPKID", PHostID);
                    p.Add("@PRate", PRate);
                    p.Add("@PFixedFee", PFixedFee);
                    p.Add("@isReseller", isReseller);
                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    conn.Execute("spUpdateMerchantFutureRate", p, commandType: CommandType.StoredProcedure);

                    int result = p.Get<int>("@ret");

                    return result == 0 ? true : false;
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return false;
            }
        }

        public CallBack_Response[] Do_Get_CallBack_Response(string PMethod, string PGatewayTxnID)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();
                    p.Add("@PMethod", PMethod);
                    p.Add("@PGatewayTxnID", PGatewayTxnID);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    var results = sqlConnection.Query<CallBack_Response>("spGetCallBackResponse", p, commandType: CommandType.StoredProcedure);

                    return results.ToArray();
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return null;
            }
        }

        //Added by DANIEL 28 Mar 2019. Add Merchant_SelectDetailByEmail
        public User Merchant_SelectDetailByEmail(string PEmailAddr)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();
                    p.Add("@PEmailAddr", PEmailAddr);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    var results = sqlConnection.Query<User>("spGetMerchant_User_ByEmailAddr", p, commandType: CommandType.StoredProcedure);
                    return results.Count() == 0 ? null : results.ToArray()[0];
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return null;
            }
        }

        //Commented by DANIEl 3 Apr 2019. Remove unnecessary codes
        //public Permission[] LoadPermissions(string PUserID, int POpsID)
        //{
        //    try
        //    {
        //        using (var sqlConnection = new SqlConnection(connStr))
        //        {
        //            sqlConnection.Open();

        //            var p = new DynamicParameters();
        //            p.Add("@PUserID", PUserID);
        //            p.Add("@POpsID", POpsID);

        //            p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
        //            var results = sqlConnection.Query<Permission>("spLoadPermission", p, commandType: CommandType.StoredProcedure);

        //            if (results.Count() > 0)
        //                return results.ToArray();
        //            else
        //                return null;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        system_err = ex.ToString();
        //        return null;
        //    }
        //}

        //Added by DANIEL 4 Apr 2019. Add Do_Get_CL_Plugin_IsReseller]]
        public CMS_User_CL_Plugin Do_Get_CL_Plugin_IsReseller(int PDomainID)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();
                    p.Add("@PDomainID", PDomainID);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    var results = sqlConnection.Query<CMS_User_CL_Plugin>("spGetCL_Plugin_IsReseller", p, commandType: CommandType.StoredProcedure);
                    return results.Count() == 0 ? null : results.ToArray()[0];
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return null;
            }
        }

        //Added by Daniel 12 Apr 2019. Add TxnFee Report to retrieve data.
        public TxnFeeReport[] TxnFeeReport(string PMerchantID, DateTime PSince, DateTime PTo, string PCurrencyCode = "")
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();
                    p.Add("@PMerchantID", PMerchantID);
                    p.Add("@PSince", PSince);
                    p.Add("@PTo", PTo);

                    p.Add("@PCurrencyCode", PCurrencyCode);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    var results = sqlConnection.Query<TxnFeeReport>("spRepTBTxnRateFinance", p, commandType: CommandType.StoredProcedure);

                    return results.ToArray();
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return null;
            }
        }

        public CurrencyCode[] GetCurrency_PreSetCredentials(string PPymtMethod) //Added by Daniel 23 Apr 2019. 
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();
                    p.Add("@PPymtMethod", PPymtMethod);
                    p.Add("@PCurrencyCode", "");

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    var results = sqlConnection.Query<CurrencyCode>("spGetHostOrCurrency_PreSetCredential", p, commandType: CommandType.StoredProcedure);

                    return results.ToArray();
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return null;
            }
        }

        public ChannelHost[] Get_HostByCurrency_PymtMethod(string PPymtMethod, string PCurrencyCode) //Added by Daniel 23 Apr 2019.
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();
                    p.Add("@PPymtMethod", PPymtMethod);
                    p.Add("@PCurrencyCode", PCurrencyCode);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    var results = sqlConnection.Query<ChannelHost>("spGetHostOrCurrency_PreSetCredential", p, commandType: CommandType.StoredProcedure);

                    return results.ToArray();
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return null;
            }
        }

        public Merchant_Summary_Report[] TB_TxnRate_GetMerchantTransacionSummaryReport(string PMerchantID, DateTime PSince, DateTime PTo, string PCurrencyCode = "")
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();
                    p.Add("@PMerchantID", PMerchantID);
                    p.Add("@PSince", PSince);
                    p.Add("@PTo", PTo);
                    p.Add("@PCurrencyCode", PCurrencyCode);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    var results = sqlConnection.Query<Merchant_Summary_Report>("spRepMerchantTransacionSummaryReport", p, commandType: CommandType.StoredProcedure);

                    return results.ToArray();
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return null;
            }
        }
    }
}
