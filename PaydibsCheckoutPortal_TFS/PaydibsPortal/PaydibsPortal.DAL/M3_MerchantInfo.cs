//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PaydibsPortal.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class M3_MerchantInfo
    {
        public int PKID { get; set; }
        public string merchant_id { get; set; }
        public string merchant_status { get; set; }
        public string merchant_short_name { get; set; }
        public string registration_id { get; set; }
        public string registration_name { get; set; }
        public Nullable<int> registration_date { get; set; }
        public string merchant_category_code { get; set; }
        public string tax_id { get; set; }
        public string biz_address1 { get; set; }
        public string biz_address2 { get; set; }
        public string biz_address3 { get; set; }
        public string biz_city { get; set; }
        public string biz_state { get; set; }
        public string biz_zip { get; set; }
        public string biz_country { get; set; }
        public string mail_address_as_biz { get; set; }
        public string mail_address1 { get; set; }
        public string mail_address2 { get; set; }
        public string mail_address3 { get; set; }
        public string mail_city { get; set; }
        public string mail_state { get; set; }
        public string mail_zip { get; set; }
        public string mail_country { get; set; }
        public string sales_code { get; set; }
        public string sales_geo_distribution { get; set; }
        public Nullable<decimal> max_tx_amount { get; set; }
        public Nullable<decimal> max_expected_weekly_sales { get; set; }
        public string settlement_bank_branch_code { get; set; }
        public string settlement_bank_branch_name { get; set; }
        public string account_no { get; set; }
        public string account_name { get; set; }
        public string payment_method { get; set; }
        public string update_status { get; set; }
        public string approve_by_date { get; set; }
        public int Processed { get; set; }
        public System.DateTime DateCreated { get; set; }
        public Nullable<System.DateTime> DateProcessed { get; set; }
        public string EditLockBy { get; set; }
        public Nullable<System.DateTime> EditLockTime { get; set; }
    }
}
