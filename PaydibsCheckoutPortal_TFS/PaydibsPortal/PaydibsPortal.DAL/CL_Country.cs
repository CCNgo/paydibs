
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace PaydibsPortal.DAL
{

using System;
    using System.Collections.Generic;
    
public partial class CL_Country
{

    public int PKID { get; set; }

    public string CountryName { get; set; }

    public string CountryCodeA2 { get; set; }

    public string CountryCodeA3 { get; set; }

    public string CountryFlagPath { get; set; }

    public Nullable<int> GMTDiffMins { get; set; }

    public int RecStatus { get; set; }

}

}
