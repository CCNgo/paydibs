//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PaydibsPortal.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class CMS_UserGroup
    {
        public int GroupID { get; set; }
        public string GroupName { get; set; }
        public string GroupDesc { get; set; }
        public int DomainID { get; set; }
        public Nullable<int> Status { get; set; }
        public System.DateTime DateCreated { get; set; }
        public System.DateTime DateModified { get; set; }
        public string ModifiedBy { get; set; }
        public string EditLockBy { get; set; }
        public Nullable<System.DateTime> EditLockTime { get; set; }
        public string Remarks { get; set; }
    }
}
