//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PaydibsPortal.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class BatchTransaction
    {
        public int PKID { get; set; }
        public string GatewayTxnID { get; set; }
        public string BatchNumber { get; set; }
        public int BatchPKID { get; set; }
        public string MerchantTxnID { get; set; }
        public Nullable<int> TokenID { get; set; }
        public int TxnState { get; set; }
        public decimal TxnAmount { get; set; }
        public int HostID { get; set; }
        public string CurrencyCode { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string CustName { get; set; }
        public string HostRefNo { get; set; }
        public string RespCode { get; set; }
        public string RespMsg { get; set; }
        public Nullable<int> TxnStatus { get; set; }
        public string MaskedCardPan { get; set; }
        public string UserDefine { get; set; }
        public string GatewayReserve1 { get; set; }
        public System.DateTime LastUpdated { get; set; }
    }
}
