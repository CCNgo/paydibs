﻿namespace PaydibsPortal.DAL
{
    public class Common
    {
    }

    public enum OpsIDEnum
    {
        Dashboard = 11,
        AccountDetail = 12,
        Transaction = 21,
        SettlementHistory = 22,
        InternalReport = 23,
        StatementAccount = 24,
        TransactionOB = 25,
        TransactionCC = 26,
        //Added by SUKI 27 Nov 2018. Add reseller report menu - START
        ResellerDetails = 27,
        ResellerOB = 28,
        ResellerCC = 29,
        //Added by SUKI 27 Nov 2018. Add reseller report menu - END

        TransactionsFeeReport = 210,    //Added by DANIEL 18 Jan 2019
        ResellerMarginReport = 211,     //Added by DANIEL 18 Jan 2019
        AddMerchant = 31,
        EditMerchant = 32,
        ViewMerchant = 33,
        AddMerchantRate = 34,
        EditMerchantRate = 35,
        ViewMerchantRate = 36,
        ManageMerchant = 37,
        AddChannel = 38,
        EditChannel = 39,
        ViewChannel = 310,
        AddResellerRate = 311,      //Added by DANIEL 16 Jan 2019
        ViewResellerRate = 312,     //Added by DANIEL 16 Jan 2019
        AddUserGroup = 41,
        EditUserGroup = 42,
        ViewUserGroup = 43,
        EditUserGroupPermission = 44,
        AddUser = 45,
        EditUser = 46,
        ViewUser = 47,
        EditUserPermission = 48,
        ChangePassword = 49,
        EmailPayment = 51,
        BuyNow = 52,
        Refund = 53,
        ViewFraudRules = 61,
        EditFraudRules = 62,
        ViewBlacklistedCard = 63,
        ManageBlacklistedCard = 64,
        ViewBlacklistedCardRange = 65,
        ManageBlacklistedCardRange = 66,
        ViewWhiteCard = 67,
        ManageWhiteCard = 68,
        ViewWhiteCardRange = 69,
        ManageWhiteCardRange = 610,
        ViewHotName = 611,
        ManageHotName = 612,
        ViewHotIP = 613,
        ManageHotIP = 614,
        ViewHotEmail = 615,
        ManageHotEmail = 616,
        ViewHotPhone = 617,
        ManageHotPhone = 618,
        ViewTransactionStatus = 619,
        SummaryReportByMerchant = 620, //Added by Daniel 29 Apr 2019
        //ManageTransactionStatus = 620,        Commented by DANIEL 18 Feb 2019
        BankReconcilation = 71,
        MerchantSettlement = 72,
        FinanceReport = 73,
        ViewLog = 81,
        ViewLateResponseTransactions = 82,
        ManageTransactionStatus = 83,       //Added by DANIEL 18 Feb 2019
        ResendCallBackResponse = 84,        //Added by DANIEL 35 Mar 2019
        HostStatusManagement = 91,
        ViewAuditTrail = 101
    }

    //Added by DANIEL 21 Feb 2019
    public enum TxnStatus
    {
        Success = 0,
        Failed = 1,
        Reversed = 9,
        Refunded = 10
    }
    //Added by DANIEL 21 Feb 2019
    public enum TxnState
    {
        ModifiedFromSucces = 40,
        ModifiedFromFailed = 41,
        ModifiedFromRefunded = 62,
        ModifiedFromReversal = 56
    }

}
