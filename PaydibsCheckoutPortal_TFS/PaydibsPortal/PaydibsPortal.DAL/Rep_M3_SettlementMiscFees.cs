//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PaydibsPortal.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Rep_M3_SettlementMiscFees
    {
        public int PKID { get; set; }
        public string M3MerchantID { get; set; }
        public int StlmntDay { get; set; }
        public string StlmntCurrency { get; set; }
        public string ItemID { get; set; }
        public string ItemName { get; set; }
        public int ItemQty { get; set; }
        public decimal ItemAmount { get; set; }
        public decimal TotAmount { get; set; }
        public string FilePath { get; set; }
        public Nullable<System.DateTime> DateCreated { get; set; }
        public Nullable<System.DateTime> DateModified { get; set; }
    }
}
