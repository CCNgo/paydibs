//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PaydibsPortal.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class TB_PayReq_3DData
    {
        public long DetailID { get; set; }
        public string ThreeDFlag { get; set; }
        public string ECI { get; set; }
        public string CAVV { get; set; }
        public string XID { get; set; }
    }
}
