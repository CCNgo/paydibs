
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace PaydibsPortal.DAL
{

using System;
    using System.Collections.Generic;
    
public partial class TB_PayReq
{

    public long PKID { get; set; }

    public int DetailID { get; set; }

    public string TxnType { get; set; }

    public string MerchantID { get; set; }

    public string GatewayID { get; set; }

    public string MerchantTxnID { get; set; }

    public string OrderNumber { get; set; }

    public string TxnAmount { get; set; }

    public decimal TxnAmt { get; set; }

    public string CurrencyCode { get; set; }

    public int HostID { get; set; }

    public string IssuingBank { get; set; }

    public string CardPAN { get; set; }

    public string GatewayTxnID { get; set; }

    public string Channel { get; set; }

    public int TxnTimeOut { get; set; }

    public int TxnState { get; set; }

    public int TxnStatus { get; set; }

    public int MerchantTxnStatus { get; set; }

    public string BankRespCode { get; set; }

    public string BankRefNo { get; set; }

    public string Param1 { get; set; }

    public string RespMesg { get; set; }

    public string MachineID { get; set; }

    public Nullable<int> Action { get; set; }

    public string OSPymtCode { get; set; }

    public Nullable<int> OSRet { get; set; }

    public Nullable<int> ErrSet { get; set; }

    public Nullable<int> ErrNum { get; set; }

    public string LockedBy { get; set; }

    public System.DateTime DateCreated { get; set; }

    public Nullable<System.DateTime> DateModified { get; set; }

    public int TxnDay { get; set; }

    public Nullable<int> OriTxnState { get; set; }

    public Nullable<int> OriMerchantTxnStatus { get; set; }

    public Nullable<int> OriTxnStatus { get; set; }

    public int RecStatus { get; set; }

}

}
