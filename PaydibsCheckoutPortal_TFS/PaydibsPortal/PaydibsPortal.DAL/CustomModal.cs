﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Security;

namespace PaydibsPortal.DAL
{
    public class Txn_Summ_Modal
    {
        public string HourCreated { get; set; }
        public string DateCreated { get; set; }
        public int Qty { get; set; }
        public decimal TxnAmt { get; set; }
        public decimal NetFee { get; set; }
    }

    public class TxnRate_Joined_Host_Summ
    {
        public decimal Qty { get; set; }
        public string HostDesc { get; set; }
    }

    public class TxnRate_Transaction
    {
        public DateTime DateCreated { get; set; }
        public string MerchantID { get; set; }
        public string OrderNumber { get; set; }
        public string PaymentID { get; set; }
        public string GatewayTxnID { get; set; }
        public string BankRefNo { get; set; }
        public string CurrencyCode { get; set; }
        public decimal TxnAmt { get; set; }
        public string Status { get; set; }
        public string Channel { get; set; }
        public string CustEmail { get; set; }
        public string CustName { get; set; }
        public string CustPhone { get; set; }
        public string AuthCode { get; set; }
        public string ECI { get; set; } //Added by Daniel 30 Apr 2019. Add ECI getter/setter properties
        public string CreditCardNo { get; set; }
        public string TradingName { get; set; }         //Added by DANIEL 2 Jan 2019

        //DELETED BY DANIEL 30 Apr 2019
        //Added by DANIEL 19 Feb 2019. Add getter/setter member RespMesg,TxnState,TxnStatus,MerchantTxnStatus - START
        //public string RespMesg { get; set; }
        //public string TxnState { get; set; }
        //public string TxnStatus { get; set; }
        //public string MerchantTxnStatus { get; set; }
        //Added by DANIEL 19 Feb 2019. Add getter/setter member RespMesg,TxnState,TxnStatus,MerchantTxnStatus - END

    }

    //Added by DANIEL 29 Nov 2018
    public class TxnRate_Transaction_No_Cust
    {
        public DateTime DateCreated { get; set; }
        public string MerchantID { get; set; }
        public string OrderNumber { get; set; }
        public string PaymentID { get; set; }
        public string GatewayTxnID { get; set; }
        public string BankRefNo { get; set; }
        public string CurrencyCode { get; set; }
        public decimal TxnAmt { get; set; }
        public string Status { get; set; }
        public string Channel { get; set; }
        public string AuthCode { get; set; }
        public string ECI { get; set; } //Added By Daniel 30 Apr 2019. Add ECI getter/setter properties.
        public string CreditCardNo { get; set; }
        public string TradingName { get; set; }     //Added by DANIEL 2 Jan 2019. Add ECI getter/setter properties.
    }
    //Added by DANIEL 29 Nov 2018
    public class TxnRate_Transaction_No_CC
    {
        public DateTime DateCreated { get; set; }
        public string MerchantID { get; set; }
        public string OrderNumber { get; set; }
        public string PaymentID { get; set; }
        public string GatewayTxnID { get; set; }
        public string BankRefNo { get; set; }
        public string CurrencyCode { get; set; }
        public decimal TxnAmt { get; set; }
        public string Status { get; set; }
        public string Channel { get; set; }
        public string CustEmail { get; set; }
        public string CustName { get; set; }
        public string CustPhone { get; set; }
        public string TradingName { get; set; }     //Added by DANIEL 2 Jan 2019
    }
    //Added by DANIEL 29 Nov 2018
    public class TxnRate_Transaction_No_Cust_CC
    {
        public DateTime DateCreated { get; set; }
        public string MerchantID { get; set; }
        public string OrderNumber { get; set; }
        public string PaymentID { get; set; }
        public string GatewayTxnID { get; set; }
        public string BankRefNo { get; set; }
        public string CurrencyCode { get; set; }
        public decimal TxnAmt { get; set; }
        public string Status { get; set; }
        public string Channel { get; set; }
        public string TradingName { get; set; }     //Added by DANIEL 2 Jan 2019
    }

    public class TxnRate_Summ
    {
        public string CurrencyCode { get; set; }
        public decimal totalAmt { get; set; }
        public decimal totalNetAmt { get; set; }
    }
    public class Terminal_Host
    {
        public int TerminalID { get; set; }
        public string MerchantID { get; set; }
        public string LogoPath { get; set; }
        public int HostID { get; set; }
        public string HostDesc { get; set; }
    }
    public class UserAcct_Listing
    {
        public int DomainID { get; set; }
        public string DomainName { get; set; }
        public string DomainShortName { get; set; }
        public string UserID { get; set; }
        public string Status { get; set; }
        public string StatName { get; set; }
        public DateTime DateCreated { get; set; }
        public string GroupName { get; set; }
        public string GroupDesc { get; set; }
        public string Remarks { get; set; }
    }
    public class Settlement_Log
    {
        public decimal Amount { get; set; }
        public string Currency { get; set; }
        public DateTime SettlementDate { get; set; }
        public string Status { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string Alias { get; set; }
        public string BankName { get; set; }
        public string BankAccHolder { get; set; }
        public string BankAccNo { get; set; }
    }

    public class CLCountry
    {
        public int CountryID { get; set; }
        public string CountryName { get; set; }
        public string CountryCodeA2 { get; set; }
        public string CountryCodeA3 { get; set; }
    }

    public class UserGroup
    {
        public int GroupID { get; set; }
        public string GroupName { get; set; }
        public string GroupDesc { get; set; }
        public int TotalUser { get; set; }
        public string Status { get; set; }
        public string DateCreated { get; set; }
    }

    public class User
    {
        public string GroupName { get; set; }
        public string UserID { get; set; }
        public string Status { get; set; }
        public string DateCreated { get; set; }
    }

    public class DomainMerchant
    {
        public int DomainID { get; set; }
        public string DomainName { get; set; }
        public string DomainDesc { get; set; }
        public string StatName { get; set; }
        public string Desc { get; set; }
    }

    public class MerchantInfo
    {
        public int DomainID { get; set; }
        public string DomainDesc { get; set; }
        public string MerchantID { get; set; }
        public string Desc { get; set; }
        public string WebSiteURL { get; set; }
        public string MainContactName { get; set; }
        public string MainContactNo { get; set; }
        public string MainContactEmail { get; set; }
        public string ContactNo { get; set; }
        public string EmailAddr { get; set; }
        public string NotifEmailRcvDesc { get; set; }
        public string NotifyEmailAddr { get; set; }
        public string CompanyRegNo { get; set; }
        public string GSTNo { get; set; }
        public string Addr1 { get; set; }
        public string Addr2 { get; set; }
        public string Addr3 { get; set; }
        public string City { get; set; }
        public string PostCode { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string OfficeNumber { get; set; }
        public string ACPName { get; set; }
        public string ACPContactNo { get; set; }
        public string ACPEmail { get; set; }
        public string BillingName { get; set; }
        public string BillingContactNo { get; set; }
        public string BillingEmail { get; set; }
        public string BankName { get; set; }
        public string BankAccHolder { get; set; }
        public string BankAccNo { get; set; }
        public string SWIFTCode { get; set; }
        public string BankAddress { get; set; }
        public string BankCountry { get; set; }
        public decimal PerTxnAmtLimit { get; set; }
        public string TypeOfBusiness { get; set; }
        public string TypeOfProducts { get; set; }
        public string TargetMarkets { get; set; }
        public string DeliveredPeriod { get; set; }
        public int PluginID { get; set; }
        public string MerchantGroup { get; set; }
        public int StateID { get; set; }
        public int CountryID { get; set; }
        public int BusinessProfileID { get; set; }
        public string MCCCode { get; set; }
        public string PluginName { get; set; }
        public DateTime DateActivated { get; set; }

        //Added by SUKI 28 Nov 2018. Add in showMerchantAddr, showMerchantLogo, IsAgent, default currency setting - START
        public int ShowMerchantAddr { get; set; }
        public int ShowMerchantLogo { get; set; }
        public int IsAgent { get; set; }
        public string DefaultCurrencyCode { get; set; }
        //Added by SUKI 28 Nov 2018. Add in showMerchantAddr, showMerchantLogo, IsAgent, default currency setting - END
    }

    public class SupportLog
    {
        public string FileName { get; set; }
        public string FileTime { get; set; }
    }

    public class MerchantRateHost
    {
        public string PymtMethod { get; set; }
        public string HostName { get; set; }
        public DateTime StartDate { get; set; }
        public decimal Rate { get; set; }
        public decimal FixedFee { get; set; }
        public string Currency { get; set; }
    }

    public class TerminalHost
    {
        public string Type { get; set; }
        public string TypeValue { get; set; }       //Added by DANIEL 3 Dec 2018
        public int HostID { get; set; }
        public string HostName { get; set; }
        public string HostDesc { get; set; }
        public string RecStatus { get; set; }       //Added by DANIEL 26 Dec 2018
        public string CurrencyCode { get; set; }    //Added by DANIEL 23 Jan 2019
        public string ProfileID { get; set; }       //Added by DANIEL 23 Jan 2019
    }

    public class Finance_Report
    {
        public DateTime DateCreated { get; set; }
        public string MerchantID { get; set; }

        //Modified by DANIEL 13 Mar 2019. Change MerchantTxnID to PaymentID
        //public string MerchantTxnID { get; set; }
        public string PaymentID { get; set; }

        public string GatewayTxnID { get; set; }
        public string BankRefNo { get; set; }
        public string HostDesc { get; set; }
        public string CurrencyCode { get; set; }
        public decimal TxnAmt { get; set; }
        //Added BY Daniel 9 May 2019. Add TransactedCurrencyCode and TransactedAmt--START
        public string TransactedCurrencyCode { get; set; }
        public decimal TransactedAmt { get; set; }
        //END
        public decimal MerchantRate { get; set; }
        public decimal MerchantFixedFee { get; set; }
        public decimal MerchantRateAmt { get; set; }
        public decimal MerchantFFeeAmt { get; set; }
        public decimal NetFee { get; set; }
        public decimal Total { get; set; }
        public string TradingName { get; set; }     //Added by DANIEL 6 Mar 2019
        ////Added by Xevos 14 May 2020
        public string BankName { get; set; }
        public string AccountNumber { get; set; }
        public string AccountName { get; set; }
        public string CardPAN { get; set; }
    }

    public class BusinessOwner
    {
        public string FullName { get; set; }
        public string Desc { get; set; }
        public string Type { get; set; }
        public int Seq { get; set; }
    }

    public class ChannelHost
    {
        public int HostID { get; set; }
        public string HostDesc { get; set; }
    }

    public class HostCredential
    {
        public string FieldName { get; set; }
        public string DBFieldName { get; set; }
        public string DefaultValue { get; set; }
        public int Encrypt { get; set; }
        public int Hex { get; set; }
    }

    public class CurrencyCode
    {
        public string CurrCode { get; set; }
    }

    public class PGMCardGroup
    {
        public int CardGrpID { get; set; }
        public string CardGrpDesc { get; set; }
    }

    //Added by DANIEl 3 Dec 2018. Add a new class called OperatioGroup to display list of operation group functions
    public class OperationGroup
    {
        public int UserGroupID { get; set; }
        public int OpsGroupID { get; set; }
        public string OpsGroupName { get; set; }
        public string OpsGroupDesc { get; set; }
        public int MapValue { get; set; }
    }

    //Added by DANIEl 12 Dec 2018. Add ViewModel UserPermission to return OpsGroupName and Permission to View
    public class UserPermission
    {
        public string OpsID { get; set; }
        public string OpsName { get; set; }
        public int IsUserAllowed { get; set; }
        public int BitIdx { get; set; }
        public int IsGroupAllowed { get; set; }
        public string FileName { get; set; }
    }

    //Added by DANIEl 19 Dec 2018. Add get set accessors to return terminal credential
    public class TerminalCredential
    {
        public int Priority { get; set; }
        public string CurrencyCode { get; set; }

        //Modified by DANIEl 2 Apr 2019. implement secure string request by PCI - START
        //public string Password { get; set; }
        private static SecureString secPwd;

        public string Password
        {
            get
            {
                return new NetworkCredential("", secPwd).Password;
            }
            set
            {
                secPwd = new NetworkCredential("", value).SecurePassword;
            }
        }
        //Modified by DANIEl 2 Apr 2019. implement secure string request by PCI - END

        public string ReturnKey { get; set; }
        public string PayeeCode { get; set; }
        public string MID { get; set; }
        public string AcquirerID { get; set; }
        public string SecretKey { get; set; }
        //Added by DANIEL 26 Feb 2019. Add get set member AirlineCode - START
        public string AirlineCode { get; set; }
        public string SendKeyPath { get; set; }
        public string ReturnKeyPath { get; set; }
        //Added by DANIEL 26 Feb 2019. Add get set member AirlineCode - END
    }

    //Add by KENT LOONG 15 Feb 2019. Add get set accessors to get channel list - START
    public class MerchantChannelList
    {
        public string MID { get; set; }
        public List<ChannelInfo> CC { get; set; }
        public List<ChannelInfo> OB { get; set; }
        public List<ChannelInfo> WA { get; set; }
    }

    public class ChannelInfo
    {
        public string PaymentMethod { get; set; }
        public int HostId { get; set; }
        public string Rate { get; set; }
        public string FixedFee { get; set; }
        public string CurrencyCode { get; set; }
        public string HostDesc { get; set; }
        public string Type { get; set; }
        public string StartDate { get; set; }
    }

    public class ChannelData
    {
        public string JsonData { get; set; }
    }
    //Add by KENT LOONG 15 Feb 2019. Add get set accessors to get channel list - END

    //Added by KENT LOONG 12 Mar 2019. Add Merchant Future Rate Modal
    public class MerchantFutureRateHost
    {
        public string PKID { get; set; }
        public string MerchantID { get; set; }
        public string DomainDesc { get; set; }
        public string PymtMethod { get; set; }
        public string PymtMethodCode { get; set; }
        public string HostID { get; set; }
        public string HostName { get; set; }
        public DateTime StartDate { get; set; }
        public decimal Rate { get; set; }
        public decimal FixedFee { get; set; }
        public string Currency { get; set; }
        public string redirectURL { get; set; }
    }

    //Added by DANIEL 22 Mar 2019. Add Callback response modal
    public class CallBack_Response
    {
        public string TxnType { get; set; }
        public string Method { get; set; }
        public string MerchantID { get; set; }
        public string PTxnID { get; set; }
        public string MerchantPymtID { get; set; }
        public string MerchantOrdID { get; set; }
        public decimal MerchantTxnAmt { get; set; }
        public string MerchantCurrCode { get; set; }
        public string PTxnStatus { get; set; }
        public string AuthCode { get; set; }
        public string AcqBank { get; set; }
        public string MerchantCallbackURL { get; set; }
        public string Sign { get; set; }
        public string PTxnMsg { get; set; }
        public string BankRefNo { get; set; }
    }

    //Commented by DANIEl 3 Apr 2019. Remove unnecessary codes
    //public class Permission
    //{
    //    public int OpsID { get; set; }
    //    public string OpsName { get; set; }
    //    public int Status { get; set; }
    //    public string FileName { get; set; }
    //    public string IsUserAllowed { get; set; }

    //    public Permission()
    //    {

    //    }

    //    public Permission(dynamic opsID, dynamic opsName, dynamic status, dynamic fileName)
    //    {
    //       OpsID = opsID;
    //       OpsName = opsName;
    //       Status = status;
    //       FileName = fileName;
    //    }
    //}

    //Added by DANIEL 4 Apr 2019. Add CMS_User_CL_Plugin model
    public class CMS_User_CL_Plugin
    {
        public string UserID { get; set; }
        public string MerchantID { get; set; }
        public string GroupID { get; set; }
        public string IsReseller { get; set; }
    }

    //Added by Daniel 11 Apr 2019. Add ResellerMarginReport Model
    public class ResellerMarginReport
    {
        public DateTime DateCreated { get; set; }
        public string MerchantID { get; set; }
        public string PaymentID { get; set; }
        public string GatewayTxnID { get; set; }
        public string BankRefNo { get; set; }
        public string HostDesc { get; set; }
        public string CurrencyCode { get; set; }
        public decimal TxnAmt { get; set; }
        //Added by Daniel 9 May 2019. Add FXTxnAmt and FXCurrencyCode --START
        public string TransactedCurrencyCode { get; set; }
        public string TransactedAmt { get; set; }
        //END
        public decimal MarginRate { get; set; }
        public decimal MerchantFixedFee { get; set; }
        public decimal MerchantRateAmt { get; set; }
        public decimal MerchantFFeeAmt { get; set; }
        public decimal NetFee { get; set; }
        public string TradingName { get; set; }
    }

    //Added by Daniel 11 Apr 2019. Add ResellerMarginReport Model
    public class TxnFeeReport
    {
        public DateTime DateCreated { get; set; }
        public string MerchantID { get; set; }
        public string PaymentID { get; set; }
        public string GatewayTxnID { get; set; }
        public string BankRefNo { get; set; }
        public string HostDesc { get; set; }
        public string CurrencyCode { get; set; }
        public decimal TxnAmt { get; set; }
        //Added by Daniel 9 May 2019. Add FXTxnAmt and FXCurrencyCode --START
        public string TransactedCurrencyCode { get; set; }
        public string TransactedAmt { get; set; }
        //END
        public decimal NetFee { get; set; }
        public decimal Total { get; set; }
        public string TradingName { get; set; }
        public string CardPAN { get; set; }
    }

    public class Merchant_Summary_Report
    {
        public string Merchant { get; set; }
        public decimal Success { get; set; }
        public int TotalSuccess { get; set; }
        public decimal Failed { get; set; }
        public int TotalFailed { get; set; }
    }

    //Added By Daniel 30 Apr 2019 . Model for ManageTxnStatus
    public class Manage_Txn_Status
    {
        public DateTime DateCreated { get; set; }
        public string MerchantID { get; set; }
        public string PaymentID { get; set; }
        public string GatewayTxnID { get; set; }
        public string CurrencyCode { get; set; }
        public decimal TxnAmt { get; set; }
        public string Status { get; set; }
        public string RespMesg { get; set; }
    }
}
