
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace PaydibsPortal.DAL
{

using System;
    using System.Collections.Generic;
    
public partial class Merchant_Host_RunningNo
{

    public string MerchantID { get; set; }

    public int HostID { get; set; }

    public decimal RunningNo { get; set; }

    public int TxnDay { get; set; }

}

}
