//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PaydibsPortal.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class CL_FraudScoreAction
    {
        public int ActionID { get; set; }
        public string Remarks { get; set; }
        public Nullable<int> RecStatus { get; set; }
        public string ActionColor { get; set; }
    }
}
