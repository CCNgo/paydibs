﻿using System;
using System.Linq;
using System.Data.SqlClient;
using System.Data;
using Kenji.Apps;
using NeoApp;
using Dapper;
using System.Web.Configuration;

namespace PaydibsPortal.DAL
{
    public class PGDAL
    {
        #region Defination
        MsSqlClient mssqlc = null;
        SqlBuilder sqlb = new SqlBuilder();
        string connStr = "";
        LinqTool linq = new LinqTool();
        public string errMsg = "";
        Encryption enc = new Encryption();
        public string system_err = string.Empty;
        #endregion

        #region Construct
        public PGDAL()
        {
            string Ecnrypted_connStr = WebConfigurationManager.AppSettings["CONNSTR_PG"];
            connStr = Decrypt_ConnStr(Ecnrypted_connStr);
            mssqlc = new MsSqlClient(connStr);
        }
        #endregion

        #region Method
        string Decrypt_ConnStr(string PEncrypted_ConnStr)
        {
            //Modified by OM 5 Apr. From key to token
            string TOKEN = "e26b5be2-867a-4400-8079-612b4a2332f6";
            return enc.AESDecrypt(PEncrypted_ConnStr, TOKEN, TOKEN);
        }

        #region CL_State
        public CL_State[] GetCLState()
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();
                    p.Add("@i_CountryID", 0);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    var results = sqlConnection.Query<CL_State>("spGetCLState", p, commandType: CommandType.StoredProcedure);

                    return results.ToArray();
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return null;
            }
        }
        #endregion

        #region CL_Country
        public CLCountry[] GetCLCountry()
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();
                    p.Add("@sz_CountryCode", "");
                    p.Add("@i_CountryID", 0);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    var results = sqlConnection.Query<CLCountry>("spGetCLCountry", p, commandType: CommandType.StoredProcedure);

                    return results.ToArray();
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return null;
            }
        }
        #endregion

        #region CL_Plugin
        public CL_Plugin[] GetCLPlugin()
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    var results = sqlConnection.Query<CL_Plugin>("spGetCLPlugin", p, commandType: CommandType.StoredProcedure);

                    return results.ToArray();
                }
            }
            catch (Exception ex)
            {
                system_err = ex.ToString();
                return null;
            }
        }
        #endregion

        #region CL_NotifEmailRcv
        public CL_NotifEmailRcv[] GetCLNotifEmailRcv()
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    var results = sqlConnection.Query<CL_NotifEmailRcv>("spGetCLNotifEmailRcv", p, commandType: CommandType.StoredProcedure);

                    return results.ToArray();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion

        #region CL_MCCCode
        public CL_MCCCode[] GetCLMCCCode()
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connStr))
                {
                    sqlConnection.Open();

                    var p = new DynamicParameters();

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    var results = sqlConnection.Query<CL_MCCCode>("spGetCLMCCCode", p, commandType: CommandType.StoredProcedure);

                    return results.ToArray();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion

        #region Merchant

        //Modified by SUKI 28 Nov 2018. Add in showMerchantAddr and showMerchantLogo
        //public int Merchant_Insert(string MerchantID, string TradingName, string RegistrationName, string MerchantPassword, string MCCCode,
        //   string ACP1Name, string ACP1Mobile, string ACP1Email, string CSEmail, string CSMobile, string Addr1, string Addr2, string Addr3,
        //   int CountryID, string City, string State, string PostCode, string WebSiteURL, int PluginID, string HCProfiles,
        //   int PymtNotificationEmail, string NotifyEmailAddr, decimal TxnLimit)
        public int Merchant_Insert(string MerchantID, string TradingName, string RegistrationName, string MerchantPassword, string MCCCode,
        string ACP1Name, string ACP1Mobile, string ACP1Email, string CSEmail, string CSMobile, string Addr1, string Addr2, string Addr3,
        int CountryID, string City, string State, string PostCode, string WebSiteURL, int PluginID, string HCProfiles,
        int PymtNotificationEmail, string NotifyEmailAddr, decimal TxnLimit, int showMerchantAddr, int showMerchantLogo)
        {
            try
            {
                using (var conn = new SqlConnection(connStr))
                {
                    conn.Open();

                    var p = new DynamicParameters();
                    p.Add("@sz_MerchantID", MerchantID);
                    p.Add("@sz_M3MerchantID", 0);
                    p.Add("@sz_Desc", TradingName);
                    p.Add("@sz_RegistrationName", RegistrationName);
                    p.Add("@sz_MerchantPassword", MerchantPassword);
                    p.Add("@i_MCCCode", MCCCode);
                    p.Add("@i_AcqBy", 1);
                    p.Add("@sz_MCName", ACP1Name);
                    p.Add("@sz_MCMobile", ACP1Mobile);
                    p.Add("@sz_MCEmail", ACP1Email);
                    p.Add("@sz_MCTelephone", ACP1Mobile);
                    p.Add("@sz_EmailAddr", CSEmail);
                    p.Add("@sz_ContactNo", CSMobile);
                    p.Add("@sz_Addr1", Addr1);
                    p.Add("@sz_Addr2", Addr2);
                    p.Add("@sz_Addr3", Addr3);
                    p.Add("@i_CountryID", CountryID);
                    p.Add("@sz_City", City);
                    p.Add("@i_State", State);
                    p.Add("@sz_PostCode", PostCode);
                    p.Add("@sz_WebSiteURL", WebSiteURL);
                    p.Add("@sz_PaymentTemplate", "");
                    p.Add("@sz_ErrorTemplate", "");
                    p.Add("@i_PymtPageTimeout_S", 600);
                    p.Add("@sz_SettleCurrency", "MYR");
                    p.Add("@i_AllowQuery", 1);
                    p.Add("@i_AllowReversal", 0);
                    p.Add("@i_AllowRpt", 0);
                    p.Add("@i_PaymentPlugin", PluginID);
                    p.Add("@i_AllowPayment", 0);
                    p.Add("@sz_CCHost", "");
                    p.Add("@i_CardTypeVISA", "");
                    p.Add("@i_CardTypeMasterCard", "");
                    p.Add("@i_CardTypeAMEX", "");
                    p.Add("@i_CardTypeDiners", "");
                    p.Add("@i_CardTypeJCB", "");
                    p.Add("@i_CardTypeCUP", "");
                    p.Add("@i_CardTypeMasterPass", "");
                    p.Add("@i_CardTypeVisaCheckout", "");
                    p.Add("@i_ThreeDAccept", 1);
                    p.Add("@i_VeloLimitExpression", 0);
                    p.Add("@i_VeloLimitCount", 0);
                    p.Add("@d_VeloLimitAmount", 0);
                    p.Add("@i_RespMethod", 1);
                    p.Add("@i_AllowCCOCP", 0);
                    p.Add("@i_AllowOB", 0);                             //p.Add("@i_AllowOB", 1);   Modified by SUKI 28 Nov 2018. Remove add host on add merchant
                    p.Add("@sz_HCProfiles", HCProfiles);
                    p.Add("@i_AllowOBFX", 0);
                    p.Add("@d_OBFXPrefRate", 0);
                    p.Add("@i_AllowWallet", 0);
                    p.Add("@i_AllowOTC", 0);
                    p.Add("@i_CollectBillAddr", 0);
                    p.Add("@i_CollectShipAddr", 0);
                    p.Add("@i_Receipt", 0);
                    p.Add("@d_PerTxnAmtLimit", TxnLimit);
                    p.Add("@i_ShowMerchantAddr", showMerchantAddr);     //p.Add("@i_ShowMerchantAddr", 1);      Modified by SUKI 28 Nov 2018. Add in showMerchantAddr and showMerchantLogo
                    p.Add("@i_ShowMerchantLogo", showMerchantLogo);     //p.Add("@i_ShowMerchantLogo", 1);      Modified by SUKI 28 Nov 2018. Add in showMerchantAddr and showMerchantLogo
                    p.Add("@i_AllowCallback", 1);
                    p.Add("@sz_S2SURL", "");
                    p.Add("@i_PymtNotificationSMS", 0);
                    p.Add("@i_PymtNotificationEmail", PymtNotificationEmail);
                    p.Add("@sz_NotifyEmailAddr", NotifyEmailAddr);
                    p.Add("@i_AllowFDS", 1);
                    p.Add("@d_FraudByAmt", 0);
                    p.Add("@i_AllowMaxMind", 0);
                    p.Add("@i_RecStatus", 1);
                    p.Add("@i_M3MerchInfoPKID", 0);
                    p.Add("@i_AllowExtFDS", 0);
                    p.Add("@sz_CMS_Users", ACP1Email);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    conn.Execute("mms_AddMerchant", p, commandType: CommandType.StoredProcedure);

                    int result = p.Get<int>("@ret");

                    return result;
                }
            }
            catch (Exception ex)
            {
                errMsg = ex.ToString();     //Added by DANIEL 11 Jan 2019
                return 0;
            }
        }
        #endregion

        #region MerchantRate
        public int MerchantRate_Insert(decimal PFromAmt, decimal PToAmt, string PMerchantID, string PPymtMethod, int PHostID, DateTime PStartDate, decimal PRate, decimal PFixedFee, string PCurrency)
        {
            try
            {
                //var item        = new MerchantRate();
                //item.FromAmt    = PFromAmt;
                //item.ToAmt      = PToAmt;
                //item.MerchantID = PMerchantID;
                //item.PymtMethod = PPymtMethod;
                //item.HostID     = PHostID;
                //item.StartDate  = PStartDate;
                //item.Rate       = PRate;
                //item.FixedFee   = PFixedFee;
                //item.Currency   = PCurrency;

                //string SqlStr = sqlb.Do_Generate_InsertQuery(item);
                //using (var sqlConnection = new SqlConnection(connStr))
                //{
                //    sqlConnection.Open();
                //    var newID = sqlConnection.Query<int>(SqlStr + " select cast(scope_identity() as int)", item).First();
                //    sqlConnection.Close();
                //    return newID;
                //}
                using (var conn = new SqlConnection(connStr))
                {
                    conn.Open();

                    var p = new DynamicParameters();
                    p.Add("@FromAmt", PFromAmt);
                    p.Add("@ToAmt", PToAmt);
                    p.Add("@MerchantID", PMerchantID);
                    p.Add("@PymtMethod", PPymtMethod);
                    p.Add("@HostID", PHostID);
                    p.Add("@StartDate", PStartDate);
                    p.Add("@Rate", PRate);
                    p.Add("@FixedFee", PFixedFee);
                    p.Add("@Currency", PCurrency);
                    p.Add("@newID", dbType: DbType.Int32, direction: ParameterDirection.Output);

                    p.Add("@ret", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    conn.Execute("spInsertMerchantRate", p, commandType: CommandType.StoredProcedure);

                    int result = p.Get<int>("@ret");
                    int newID = p.Get<int>("@newID");

                    return newID;
                }
            }
            catch (Exception)
            {
                return 0;
            }
        }
        #endregion

        #endregion
    }
}
