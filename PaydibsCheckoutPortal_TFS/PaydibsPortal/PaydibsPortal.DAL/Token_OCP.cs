//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PaydibsPortal.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Token_OCP
    {
        public int PKID { get; set; }
        public string Token { get; set; }
        public string MerchantID { get; set; }
        public string CardPAN { get; set; }
        public string CardExp { get; set; }
        public string CardHolder { get; set; }
        public string CustEmail { get; set; }
        public System.DateTime DateCreated { get; set; }
        public System.DateTime DateModified { get; set; }
        public int RecStatus { get; set; }
        public string OrderNumber { get; set; }
        public string MaskedCardPAN { get; set; }
    }
}
