﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace PaydibsPortal.DAL
{
    public class SqlBuilder
    {

        public string Do_Generate_InsertQuery(dynamic PClassObject)
        {
            try
            {
                Type myType               = PClassObject.GetType();
                IList<PropertyInfo> props = new List<PropertyInfo>(myType.GetProperties());
                StringBuilder sb          = new StringBuilder();
                StringBuilder sb2         = new StringBuilder();

                int InsertParam = 0;
                string TbName   = myType.Name;

                if (myType.Name == "Terminal") TbName = "Terminals";

                sb.Append("INSERT INTO " + TbName + "(");
                sb2.Append("VALUES(");

                for (int i = 0; i < props.Count(); i++)
                {
                    string ColName = props[i].Name;
                    string ColType = props[i].PropertyType.Name;

                    if (ColName == "Desc")
                    {
                        ColName = "[Desc]";
                    }

                    if (props[i].PropertyType.GenericTypeArguments.Count() > 0)
                    {
                        ColType = props[i].PropertyType.GenericTypeArguments[0].Name;
                    }

                    //var colType2   = props[i].PropertyType.GenericTypeArguments[0];
                    var ColValue = props[i].GetValue(PClassObject, null);
                    if (ColName == "IDN" || ColName == "PKID") continue;
                    if (TbName == "CMS_Contact") if (ColName == "ContactID") continue;
                    if (TbName == "CMS_Domain") if (ColName == "DomainID") continue;
                    if (TbName == "CMS_UserAcct") if (ColName == "AcctID") continue;
                    if (TbName == "Terminals") if (ColName == "TerminalID") continue;
                    //if (TbName == "Merchant") if (ColName == "MerchantID") continue;


                    if (ColValue != null)
                    {
                        if (InsertParam >= 1)
                        {
                            sb.Append(", ");
                            sb2.Append(", ");
                        }
                        sb.Append(ColName);
                        InsertParam++;

                        if (ColType.ToLower() == "int32") sb2.Append(ColValue);
                        else sb2.Append("N'" + ColValue + "'");
                    }


                }
                sb.Append(")");
                sb2.Append(")");

                return sb.ToString() + sb2.ToString();
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public string Do_Generate_UpdateQuery(dynamic PClassOjbect_ORI, dynamic PClassObject_Updated)
        {
            Type myType_ORI = PClassOjbect_ORI.GetType();
            IList<PropertyInfo> props_ori = new List<PropertyInfo>(myType_ORI.GetProperties());

            Type myType_Updated = PClassObject_Updated.GetType();
            //IList<PropertyInfo> props_updated = new List<PropertyInfo>(myType_Updated.GetProperties());

            StringBuilder sb = new StringBuilder();
            sb.Append("UPDATE " + myType_ORI.Name + " SET ");

            StringBuilder sb2 = new StringBuilder();

            int UpdatedParam = 0;

            for (int i = 0; i < props_ori.Count; i++)
            {
                string ColName = props_ori[i].Name;
                string ColType = props_ori[i].PropertyType.Name;
                var ColValue   = props_ori[i].GetValue(PClassOjbect_ORI, null);

                PropertyInfo props_updated = myType_Updated.GetProperty(ColName);
                var UpdateColValue         = props_updated.GetValue(PClassObject_Updated, null);



                if (ColValue != UpdateColValue)
                {
                    if (UpdatedParam > 0) sb2.Append(", ");
                    UpdatedParam++;

                    if (ColType.ToLower() == "int32") sb2.Append(ColName + "=" + UpdateColValue);
                    else sb2.Append(ColName + "=" + "N'" + UpdateColValue + "'");

                }
            }

            int IDN = myType_ORI.GetProperty("IDN").GetValue(PClassOjbect_ORI, null);
            sb.Append(sb2.ToString());
            sb.Append(" WHERE IDN = " + IDN);
            return sb.ToString();
        }
    }
}
