//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PaydibsPortal.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class MerchantVATRate
    {
        public long PKID { get; set; }
        public string MerchantID { get; set; }
        public string PymtMethod { get; set; }
        public int HostID { get; set; }
        public int StartDate { get; set; }
        public Nullable<decimal> Rate { get; set; }
        public Nullable<decimal> FixedFee { get; set; }
        public string Currency { get; set; }
        public int RecStatus { get; set; }
        public System.DateTime LastUpdated { get; set; }
    }
}
