﻿
using Kenji.Apps;
using NeoApp;
using System.Web.Configuration;

namespace PaydibsPortal.DAL
{
    public class OBDAL
    {
        #region Defination
        MsSqlClient mssqlc   = null;
        SqlBuilder sqlb      = new SqlBuilder();
        string connStr       = "";
        LinqTool linq        = new LinqTool();
        public string errMsg = "";
        Encryption enc       = new Encryption();
        #endregion

        #region Construct
        public OBDAL()
        {
            string Ecnrypted_connStr = WebConfigurationManager.AppSettings["CONNSTR_OB"];
            connStr                  = Decrypt_ConnStr(Ecnrypted_connStr);
            mssqlc                   = new MsSqlClient(connStr);
        }
        #endregion

        #region Method
        string Decrypt_ConnStr(string PEncrypted_ConnStr)
        {
            //Modified by OM 5 Apr 2019. from key to token
            string TOKEN = "e26b5be2-867a-4400-8079-612b4a2332f6";
            return enc.AESDecrypt(PEncrypted_ConnStr, TOKEN, TOKEN);
        }

        #endregion
    }
}
