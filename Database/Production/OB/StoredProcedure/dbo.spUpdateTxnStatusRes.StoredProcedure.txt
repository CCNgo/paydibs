USE [OB]
GO
/****** Object:  StoredProcedure [dbo].[spUpdateTxnStatusRes]    Script Date: 6/10/2020 11:47:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUpdateTxnStatusRes] 
(
	@i_TxnStatus AS INT,
	@sz_RespMesg AS VARCHAR(255),
	@sz_BankRefNo AS VARCHAR(40),
	@sz_BankRespCode AS VARCHAR(20),
	@sz_GatewayTxnID AS VARCHAR(30)
)
AS
	UPDATE TB_PayRes SET
		TxnStatus = @i_TxnStatus, 
		RespMesg = @sz_RespMesg,
		BankRefNo = @sz_BankRefNo,
		BankRespCode = @sz_BankRespCode,
		DateModified = GETDATE()
	WHERE GatewayTxnID = @sz_GatewayTxnID
	
	IF @@ROWCOUNT > 0
		RETURN 0
	ELSE BEGIN
		RETURN -1
	END

GO
