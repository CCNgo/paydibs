USE [OB]
GO
/****** Object:  StoredProcedure [dbo].[spGetTxnID]    Script Date: 6/10/2020 11:47:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE     PROCEDURE [dbo].[spGetTxnID] --6,'290','20120827',1.00
(                                                
	@i_HostID AS INT,
	@sz_HostTxnID AS VARCHAR(30),
	@sz_HostDate AS DATETIME,
	@d_Amt AS DECIMAL(18,2)
)
AS
	--Timezone Malaysia time 4pm then Thailand time 3pm (-1.0hr). Today date -1
	/*DECLARE @szSelect VarChar(200)
	DECLARE @szFromRes VarChar(200)
	DECLARE @szFromReq VarChar(200)
	DECLARE @szCriteria VarChar(200)*/
	DECLARE @szHostDay1 VarChar(8)
	DECLARE @szHostDay2 VarChar(8)
	DECLARE @szID VarChar(30)
	DECLARE @iCount Int
	
	SET @szHostDay1 = CONVERT(VARCHAR(8), DateAdd(Day, -1, @sz_HostDate), 112) --Thailand date
	SET @szHostDay2 = CONVERT(VARCHAR(8), @sz_HostDate, 112) --Malaysia date

	SELECT TOP 1 @szID = GatewayTxnID
	FROM TB_PayRes WITH (NOLOCK)
	WHERE TxnDay IN (@szHostDay1, @szHostDay2)  AND HostID = @i_HostID AND Right('000000' + @sz_HostTxnID, 6) = Right(GatewayTxnID, 6)
		AND TxnAmt = CONVERT(VARCHAR(50), @d_Amt)

	SET @iCount = @@ROWCOUNT

	IF @iCount = 0
	BEGIN
		SELECT TOP 1 @szID = GatewayTxnID
		FROM TB_PayReq WITH (NOLOCK)
		WHERE TxnDay IN (@szHostDay1, @szHostDay2) AND HostID = @i_HostID AND Right('000000' + @sz_HostTxnID, 6) = Right(GatewayTxnID, 6)
			AND TxnAmt = CONVERT(VARCHAR(50), @d_Amt)

		SET @iCount = @@ROWCOUNT
	END

	IF @iCount > 0
		SELECT @szID [GatewayTxnID]
			
	
	/*SET @szSelect = 'SELECT TOP 1 GatewayTxnID '
	SET @szFromRes = 'FROM TB_PayRes WITH (NOLOCK) '
	SET @szCriteria = 'WHERE HostID = ' + CONVERT(VARCHAR(8), @i_HostID) +' AND '

	IF @sz_HostTxnID <> ''
	BEGIN
		SET @szCriteria = @szCriteria + 'Right(''000000'' + ''' + @sz_HostTxnID + ''', 6) = Right(GatewayTxnID, 6) '
	END

	IF @sz_HostDate <> ''
	BEGIN
			SET @szHostDay1 = CONVERT(VARCHAR(8), DateAdd(Day, -1, @sz_HostDate), 112) --Thailand date
			SET @szHostDay2 = CONVERT(VARCHAR(8), @sz_HostDate, 112) --Malaysia date
			SET @szCriteria = @szCriteria + 'AND TxnDay IN (' + @szHostDay1 + ',' + @szHostDay2 + ') '
	END

	IF @d_Amt <> 0
	BEGIN
		SET @szCriteria = @szCriteria + 'AND TxnAmt = ' + CONVERT(VARCHAR(50), @d_Amt)
	END
	
	EXEC(@szSelect + @szFromRes + @szCriteria)

	--SET @szFromReq = 'FROM TB_PayReq WITH (NOLOCK) '
	--EXEC(@szSelect + @szFromReq + @szCriteria)
	--RETURN -1

	IF @@ROWCOUNT = 0
	BEGIN
		SET @szFromReq = 'FROM TB_PayReq WITH (NOLOCK) '
		EXEC(@szSelect + @szFromReq + @szCriteria)
	END

	/*IF @@ROWCOUNT > 0
	BEGIN 
		RETURN 0
	END
	ELSE
	BEGIN 
		RETURN -1
	END*/
*/
GO
