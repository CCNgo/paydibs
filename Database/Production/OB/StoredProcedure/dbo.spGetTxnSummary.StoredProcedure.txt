USE [OB]
GO
/****** Object:  StoredProcedure [dbo].[spGetTxnSummary]    Script Date: 6/10/2020 11:47:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetTxnSummary] 
(                                                
	@sz_StartDate varchar(25),
	@sz_EndDate varchar(25),
	@sz_ServiceID varchar(3),
	@sz_UserID varchar(30)=''
)
AS 

DECLARE @sz_Entity varchar(50)

SELECT @sz_Entity=Entity FROM IPGAdmin.dbo.CMS_User WHERE UserID = @sz_UserID

IF (@sz_ServiceID <> '')
BEGIN
	SELECT ServiceID MerchantID, (SELECT HostName FROM Host WHERE HostID=R.HostID) Host, Count(PKID) TotTxn, CurrencyCode, 
	SUM(CAST(TxnAmount AS Decimal(18,2))) TotAmt
	FROM TB_PayRes R WITH (NOLOCK)
	WHERE TxnDay BETWEEN  REPLACE(@sz_StartDate,'-','') AND REPLACE(@sz_EndDate,'-','')
		AND ServiceID = @sz_ServiceID
	GROUP BY ServiceID, HostID, CurrencyCode
	ORDER BY ServiceID, Host, CurrencyCode
END
ELSE
BEGIN
IF(@sz_Entity <> '' AND @sz_Entity <> '0')
	BEGIN
		SELECT ServiceID MerchantID, (SELECT HostName FROM Host WHERE HostID=R.HostID) Host, Count(PKID) TotTxn, CurrencyCode, 
		SUM(CAST(TxnAmount AS Decimal(18,2))) TotAmt
		FROM TB_PayRes R WITH (NOLOCK)
		WHERE TxnDay BETWEEN  REPLACE(@sz_StartDate,'-','') AND  REPLACE(@sz_EndDate,'-','')
		AND ServiceID IN(SELECT MerchantID COLLATE SQL_Latin1_General_CP1_CI_AS FROM IPG.dbo.Merchant WHERE AcqBy IN(SELECT item FROM FN_SPLIT(@sz_Entity,',')))
		GROUP BY ServiceID, HostID, CurrencyCode
		ORDER BY ServiceID, Host, CurrencyCode
	END
	ELSE
	BEGIN
		SELECT ServiceID MerchantID, (SELECT HostName FROM Host WHERE HostID=R.HostID) Host, Count(PKID) TotTxn, CurrencyCode, 
		SUM(CAST(TxnAmount AS Decimal(18,2))) TotAmt
		FROM TB_PayRes R WITH (NOLOCK)
		WHERE TxnDay BETWEEN  REPLACE(@sz_StartDate,'-','') AND  REPLACE(@sz_EndDate,'-','')
		GROUP BY ServiceID, HostID, CurrencyCode
		ORDER BY ServiceID, Host, CurrencyCode
	END
END

GO
