USE [OB]
GO
/****** Object:  StoredProcedure [dbo].[spGetReqTxn]    Script Date: 6/10/2020 11:47:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetReqTxn]
(
	@sz_GatewayTxnID AS VARCHAR(30)
)
AS
	DECLARE @RetCode Int
	DECLARE @RetDesc VarChar(160)

	DECLARE @szTxnType varchar(7), @szMerchantID varchar(30), @szMerchantTxnID varchar(30), @szTxnAmount varchar(18), @szBaseTxnAmount varchar(18),
			@szCurrencyCode char(3), @szBaseCurrencyCode varchar(3), @szIssuingBank varchar(20), @iHostID Int, @szLanguageCode char(2), @szGatewayID varchar(4), @szMachineID varchar(10), @szCardPAN varchar(20),
			@szOrderNumber varchar(20), @szSessionID varchar(100), @szMerchantReturnURL varchar(255), @szMerchantSupportURL varchar(255), @szMerchantApprovalURL varchar(255), @szMerchantUnApprovalURL varchar(255),
			@szMerchantCallbackURL varchar(255), @szParam1 varchar(100), @szParam2 varchar(100), @szMaskedCardPAN varchar(20),
			@szParam3 varchar(100), @szParam4 varchar(100), @szParam5 varchar(100),	@szParam6 varchar(50), @szParam7 varchar(50), @szHashMethod varchar(6), @szDateCreated varchar(23),
			@szOrderDesc nvarchar(100), @iAction Int, @szCustEmail varchar(60), @szCustName nvarchar(50), @szFXCurrencyCode varchar(3), @szFXTxnAmt varchar(18),
			@szTokenType varchar(3)	-- Added  26 Mar 2015, for BNB
			, @szBankRefNo varchar(40) -- Added BTP, 02 Dec 2016

	DECLARE @iTxnStatus Int, @iTxnState Int

	IF NOT EXISTS(SELECT 1 FROM TB_PayRes WHERE GatewayTxnID = @sz_GatewayTxnID)
	BEGIN 
		SELECT  @szTxnType = A.TxnType, @szMerchantID = A.MerchantID, @szMerchantTxnID = A.MerchantTxnID, @szTxnAmount = A.TxnAmount,
				@szCurrencyCode = A.CurrencyCode, @szIssuingBank = A.IssuingBank, @iHostID = A.HostID, @szDateCreated = CONVERT(VARCHAR, A.DateCreated, 120), @szLanguageCode = B.LanguageCode, @iTxnStatus = A.TxnStatus, @iTxnState = A.TxnState, @szGatewayID = A.GatewayID, @szMachineID = A.MachineID, @szCardPAN = A.CardPAN, @iAction = A.Action,
				@szOrderNumber = B.OrderNumber, @szSessionID = B.SessionID, @szMerchantReturnURL = B.MerchantReturnURL, @szMerchantSupportURL = ISNULL(B.MerchantSupportURL,''), @szMerchantApprovalURL = ISNULL(B.MerchantApprovalURL,''), @szMerchantUnApprovalURL = ISNULL(B.MerchantUnApprovalURL,''),
				@szMerchantCallbackURL = ISNULL(B.MerchantCallbackURL,''), @szBaseCurrencyCode = ISNULL(B.BaseCurrencyCode, ''), @szBaseTxnAmount = ISNULL(B.BaseTxnAmount, ''), @szOrderDesc = ISNULL(B.OrderDesc, ''),
				@szCustEmail = ISNULL(B.CustEmail, ''), @szCustName = ISNULL(B.CustName, ''),
				@szParam1 = ISNULL(C.Param1,''), @szParam2 = ISNULL(C.Param2,''), @szMaskedCardPAN = B.CardPAN,
				@szParam3 = ISNULL(C.Param3,''), @szParam4 = ISNULL(C.Param4,''), @szParam5 = ISNULL(C.Param5,''), @szParam6 = ISNULL(C.Param6,''), @szParam7 = ISNULL(C.Param7,''), 
				@szHashMethod = B.HashMethod,
				@szFXCurrencyCode = ISNULL(FX.FXCurrencyCode,''), @szFXTxnAmt = ISNULL(FX.FXTxnAmt,0),
				@szTokenType = ISNULL(D.TokenType,''),	-- Added  8 Apr 2015, for BNB
				@szBankRefNo = ISNULL(A.BankRefNo,'')	-- Added BTP, 02 Dec 2016
		FROM	TB_PayReq A WITH (NOLOCK)
				INNER JOIN TB_PayReq_Detail B WITH (NOLOCK) ON A.DetailID=B.PKID
				LEFT OUTER JOIN TB_Pay_Params C WITH (NOLOCK) ON C.DetailID=B.PKID AND C.ReqRes=1
				LEFT OUTER JOIN TB_PayFX FX WITH (NOLOCK) ON FX.DetailID=B.PKID
				LEFT OUTER JOIN TB_PayTxnToken D WITH (NOLOCK) ON D.DetailID=B.PKID		-- Added  8 Apr 2015, for BNB
		WHERE A.GatewayTxnID = @sz_GatewayTxnID

		IF @@ROWCOUNT = 1
		BEGIN
			SET @RetCode = 1
			SET @RetDesc = ''

			SELECT	@RetCode [RetCode], @RetDesc [RetDesc],
					@szTxnType [TxnType], RTrim(@szMerchantID) [MerchantID], @szMerchantTxnID [MerchantTxnID], @szTxnAmount [TxnAmount],
					@szCurrencyCode [CurrencyCode], @szIssuingBank [IssuingBank], @iHostID [HostID], @szDateCreated [DateCreated], @szLanguageCode [LanguageCode], @iTxnStatus [TxnStatus], @iTxnState [TxnState], @szGatewayID [GatewayID], @szMachineID [MachineID], @szCardPAN [CardPAN], @iAction [Action],
					@szOrderNumber [OrderNumber], @szSessionID [SessionID], @szMerchantReturnURL [MerchantReturnURL], @szMerchantSupportURL [MerchantSupportURL], @szMerchantApprovalURL [MerchantApprovalURL], @szMerchantUnApprovalURL [MerchantUnApprovalURL],
					@szMerchantCallbackURL [MerchantCallbackURL], @szCustEmail [CustEmail], @szCustName [CustName],
					@szParam1 [Param1], @szParam2 [Param2], @szMaskedCardPAN [MaskedCardPAN],
					@szParam3 [Param3], @szParam4 [Param4], @szParam5 [Param5], @szParam6 [Param6], @szParam7 [Param7], @szHashMethod [HashMethod],
					@szBaseCurrencyCode [BaseCurrencyCode], @szBaseTxnAmount [BaseTxnAmount], @szOrderDesc [OrderDesc],
					@szFXCurrencyCode [FXCurrencyCode], @szFXTxnAmt [FXTxnAmt], @szTokenType [TokenType]	-- Added  8 Apr 2015, for BNB
					,@szBankRefNo [BankRefNo] -- Added BTP, 02 Dec 2016
			RETURN 1
		END
		ELSE IF @@ROWCOUNT > 1
		BEGIN
			SET @RetCode = -2
			SET @RetDesc = 'Error: Multiple request transactions found.'
			
			SELECT @RetCode AS [RetCode], @RetDesc AS [RetDesc]
			
			RETURN -2
		END
		ELSE
		BEGIN
			SET @RetCode = -3
			SET @RetDesc = 'Error: No request transaction found.'
			
			SELECT @RetCode AS [RetCode], @RetDesc AS [RetDesc]
			
			RETURN -3
		END
	END
	ELSE
	BEGIN 
		SET @RetCode = -1
		SET @RetDesc = 'Transaction already existed in response table.'
		
		SELECT @RetCode AS [RetCode], @RetDesc AS [RetDesc]
		
		RETURN -1
	END
GO
