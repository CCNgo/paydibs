USE [OB]
GO
/****** Object:  StoredProcedure [dbo].[spGetMerchantWebSvcDetails]    Script Date: 6/10/2020 11:47:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetMerchantWebSvcDetails] --'A01'
(
	@MerchantID AS VARCHAR(3)
)
AS
BEGIN
SELECT [MerchantID]
      ,[WebSvcUrl]
      ,[WebSvcUID]
      ,[WebSvcPwd]
      ,[DateCreated]
      ,[DateUpdated]
      ,[RecStatus]
  FROM [dbo].[Merchant_WebSvc]
WHERE MerchantID = @MerchantID AND RecStatus=1
END
	--DECLARE @RetCode Int
	--DECLARE @RetDesc VarChar(160)

	--	SET @RetCode = 1
	--	SET @RetDesc = 'Record exists in system'	

		
	--BEGIN 
	--	SET @RetCode = 0
	--	SET @RetDesc = 'Transaction not exist'
		
	--	SELECT @RetCode AS [RetCode], @RetDesc AS [RetDesc]
		
	--	RETURN 0
	--END
GO
