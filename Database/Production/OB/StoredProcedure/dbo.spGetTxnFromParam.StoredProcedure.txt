USE [OB]
GO
/****** Object:  StoredProcedure [dbo].[spGetTxnFromParam]    Script Date: 6/10/2020 11:47:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetTxnFromParam]
(                                                
	@sz_Param AS VARCHAR(100),
	@i_HostID AS INT
)
AS
	DECLARE @iRet Int
	DECLARE @szRetDesc VarChar(160)

	SET @iRet = -1

	-- Select the latest transaction based on Param1	
	
	IF EXISTS(SELECT 1 FROM	TB_Pay_Params A WITH (NOLOCK)
			INNER JOIN TB_PayReq_Detail B WITH (NOLOCK) ON A.DetailID=B.PKID
			INNER JOIN TB_PayReq C WITH (NOLOCK) ON A.DetailID=C.DetailID
			LEFT OUTER JOIN TB_PayFX FX WITH (NOLOCK) ON FX.DetailID=A.DetailID
	WHERE	A.HostID = @i_HostID AND UPPER(A.Param1) = UPPER(@sz_Param) AND A.ReqRes = 1)
	
	BEGIN 		
			SELECT   TOP 1 0 RetCode, '' RetDesc, B.MerchantID MerchantID, B.MerchantTxnID MerchantTxnID, B.TxnAmount TxnAmount,
				B.CurrencyCode CurrencyCode, C.GatewayTxnID GatewayTxnID, B.OrderDesc OrderDesc, C.OrderNumber OrderNumber,
				SUBSTRING(CONVERT(VARCHAR, B.DateCreated, 120), 9, 2) + '/' + SUBSTRING(CONVERT(VARCHAR, B.DateCreated, 120), 6, 2) + '/' + SUBSTRING(CONVERT(VARCHAR, B.DateCreated, 120), 1, 4) + SUBSTRING(CONVERT(VARCHAR, B.DateCreated, 120), 11, 9) DateCreated,
				ISNULL(FX.FXCurrencyCode,'') FXCurrencyCode, ISNULL(FX.FXTxnAmt,0) FXTxnAmt,C.PKID PKID, C.TxnStatus TxnStatus, -1 QueryStatus
			FROM	TB_Pay_Params A WITH (NOLOCK)
					INNER JOIN TB_PayReq_Detail B WITH (NOLOCK) ON A.DetailID=B.PKID
					INNER JOIN TB_PayReq C WITH (NOLOCK) ON A.DetailID=C.DetailID
					LEFT OUTER JOIN TB_PayFX FX WITH (NOLOCK) ON FX.DetailID=A.DetailID
			WHERE	A.HostID = @i_HostID AND UPPER(A.Param1) = UPPER(@sz_Param) AND A.ReqRes = 1
			ORDER	BY A.PKID DESC	
			
			RETURN 1
	END
	ELSE
	BEGIN 
		IF EXISTS(SELECT  1 
				FROM	TB_Pay_Params A WITH (NOLOCK)
						INNER JOIN TB_PayReq_Detail B WITH (NOLOCK) ON A.DetailID=B.PKID
						INNER JOIN TB_PayRes C WITH (NOLOCK) ON A.DetailID=C.PKID
						LEFT OUTER JOIN TB_PayFX FX WITH (NOLOCK) ON FX.DetailID=A.DetailID
				WHERE	A.HostID = @i_HostID AND UPPER(A.Param1) = UPPER(@sz_Param) AND A.ReqRes = 2)	
			
			BEGIN 
				SELECT  TOP 1 0 RetCode, '' RetDesc, B.MerchantID MerchantID, B.MerchantTxnID MerchantTxnID, B.TxnAmount TxnAmount,
					B.CurrencyCode CurrencyCode, C.GatewayTxnID GatewayTxnID, B.OrderDesc OrderDesc, C.OrderNumber OrderNumber,
					SUBSTRING(CONVERT(VARCHAR, B.DateCreated, 120), 9, 2) + '/' + SUBSTRING(CONVERT(VARCHAR, B.DateCreated, 120), 6, 2) + '/' + SUBSTRING(CONVERT(VARCHAR, B.DateCreated, 120), 1, 4) + SUBSTRING(CONVERT(VARCHAR, B.DateCreated, 120), 11, 9) DateCreated,
					ISNULL(FX.FXCurrencyCode,'') FXCurrencyCode, ISNULL(FX.FXTxnAmt,0) FXTxnAmt, C.PKID PKID, C.TxnStatus TxnStatus, 1 QueryStatus
				FROM	TB_Pay_Params A WITH (NOLOCK)
						INNER JOIN TB_PayReq_Detail B WITH (NOLOCK) ON A.DetailID=B.PKID
						INNER JOIN TB_PayRes C WITH (NOLOCK) ON A.DetailID=C.PKID
						LEFT OUTER JOIN TB_PayFX FX WITH (NOLOCK) ON FX.DetailID=A.DetailID
				WHERE	A.HostID = @i_HostID AND UPPER(A.Param1) = UPPER(@sz_Param) AND A.ReqRes = 2
				ORDER	BY A.PKID DESC
				
				RETURN 1
			END
			ELSE
			BEGIN 
				SET @iRet = -1
				SET @szRetDesc = 'Transaction info not found based on Param1[' + @sz_Param + ']'
		
				SELECT @iRet AS [RetCode], @szRetDesc AS [RetDesc]
		
				RETURN -1
			END

	END


GO
