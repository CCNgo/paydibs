USE [OB]
GO
/****** Object:  StoredProcedure [dbo].[spGetReqDetail]    Script Date: 6/10/2020 11:47:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetReqDetail]
(
	@sz_MerchantID AS VARCHAR(30),
	@sz_MerchantTxnID AS VARCHAR(30)
)
AS
	DECLARE @RetCode Int
	DECLARE @RetDesc VarChar(160)

	DECLARE @szOrderNumber varchar(20), @szSessionID varchar(100), @szMerchantReturnURL varchar(255), @szMerchantSupportURL varchar(255),
			@szBaseCurrencyCode varchar(3), @szBaseTxnAmount varchar(18), @szOrderDesc varchar(100),
			@szMaskedCardPAN varchar(20), @szHashMethod varchar(4), @szIssuingBank varchar(20), @szCustPhone varchar(25)
			
	SELECT  @szOrderNumber = A.OrderNumber, @szSessionID = A.SessionID, @szMerchantReturnURL = A.MerchantReturnURL, @szMerchantSupportURL = A.MerchantSupportURL,
			@szBaseCurrencyCode = ISNULL(A.BaseCurrencyCode, ''), @szBaseTxnAmount = ISNULL(A.BaseTxnAmount, ''), @szOrderDesc = ISNULL(A.OrderDesc, ''),
			@szMaskedCardPAN = A.CardPAN, @szHashMethod = A.HashMethod, @szIssuingBank = A.IssuingBank, @szCustPhone = A.CustPhone
	FROM	TB_PayReq_Detail A WITH (NOLOCK)
	WHERE	A.MerchantID = @sz_MerchantID AND A.MerchantTxnID = @sz_MerchantTxnID

	IF @@ROWCOUNT = 1
	BEGIN
		SET @RetCode = 1
		SET @RetDesc = ''

		SELECT	@RetCode [RetCode], @RetDesc [RetDesc],
				@szOrderNumber [OrderNumber], @szSessionID [SessionID], @szMerchantReturnURL [MerchantReturnURL], @szMerchantSupportURL [MerchantSupportURL],
				@szBaseCurrencyCode [BaseCurrencyCode], @szBaseTxnAmount [BaseTxnAmount], @szOrderDesc [OrderDesc],
				@szMaskedCardPAN [MaskedCardPAN], @szHashMethod [HashMethod], @szIssuingBank [IssuingBank], @szCustPhone [CustPhone]
		RETURN 1
	END
	ELSE
	BEGIN
		SET @RetCode = -3
		SET @RetDesc = 'Error: No request transaction found.'
		
		SELECT @RetCode AS [RetCode], @RetDesc AS [RetDesc]
		
		RETURN -3
	END

GO
