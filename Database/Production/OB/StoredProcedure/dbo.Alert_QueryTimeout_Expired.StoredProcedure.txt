USE [OB]
GO
/****** Object:  StoredProcedure [dbo].[Alert_QueryTimeout_Expired]    Script Date: 6/10/2020 11:47:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Alert_QueryTimeout_Expired] 
	
AS
	DECLARE @iHostID INT, @szHostName VARCHAR(20), @iRow INT, @szHost VARCHAR(500) = 'null'
	DECLARE @szTxnID VARCHAR(40)

	SELECT HostID, HostName INTO #TmpTable FROM Host WHERE RecStatus = 1

	WHILE exists (SELECT * FROM #TmpTable)
	BEGIN

		SELECT TOP 1 @iHostID = HostID, @szHostName = HostName
		FROM #TmpTable
		ORDER BY HostID ASC
	
		SELECT @iRow = COUNT(PKID)
		FROM TB_PayRes WITH (NOLOCK) --DateCreated > GETDATE()-1 is to select ONLY today date. Because some old req txn still stuck in req table.
		--WHERE DateCreated > GETDATE()-1 AND DATEDIFF(MINUTE, DateCreated, GETDATE()) > 30
		WHERE DateCreated BETWEEN DATEADD(MINUTE, -30, GETDATE()) AND GETDATE() 
		--WHERE DATEDIFF(SECOND, DateCreated, GETDATE()) > 1800
						AND TxnStatus IN (4,6) AND HostID = @iHostID -- (4 = Query Host Timeout, 6 = Expired)
		
		IF @iRow > 10 BEGIN
			--Testing
			--SELECT TOP 1 @szTxnID = GatewayTxnID FROM TB_PayReq WITH (NOLOCK)
			--WHERE DateCreated > GETDATE()-1 AND DATEDIFF(MINUTE, DateCreated, GETDATE()) > 30
			--				AND TxnState = 2 AND TxnStatus = 30 AND HostID = @iHostID
							
			IF @szHost = 'null'
				SET @szHost = @szHostName --+ '-' + @szTxnID
			ELSE
				SET @szHost = @szHost + @szHostName /*+ '-' + @szTxnID*/ + ','
		END;

		DELETE #TmpTable
		WHERE HostID = @iHostID

	END

	DROP TABLE #TmpTable

	SELECT @szHost AS Host;
GO
