USE [OB]
GO
/****** Object:  StoredProcedure [dbo].[spGetLateRespTxn]    Script Date: 6/10/2020 11:47:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Create date: 6/10/2017
-- Description:	get Late Response transactions
/*
Test:
DECLARE	@return_value int

EXEC	@return_value = [dbo].[spGetLateRespTxn]
		@gatewaytxnid = N'FM000IPGHFMSIT170925001',
		@bankrefno = N'',
		@MerchantID = N'',
		@startdate = N'2016-06-01 00:00:00',
		@enddate = N'2017-10-09 00:00:00'

SELECT	'Return Value' = @return_value

GO
*/
-- =============================================
CREATE PROCEDURE [dbo].[spGetLateRespTxn] 
	@gatewaytxnid varchar(30) = null,
	@bankrefno varchar(40) = null,	
	@MerchantID char(3) = null,
	@startdate varchar(20) = null,
	@enddate varchar(20) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT A.*,
	LR.GatewayTxnID AS LR_GatewayTxnID,
	LR.MerchantID AS LR_MerchantID,
	LR.BankRespCode AS LR_BankRespCode,
	LR.BankRefNo AS LR_BankRefNo,
	LR.LastUpdated AS LR_DateCreated,
	(SELECT ST.TxnDescription FROM [OB].[dbo].CL_TxnStatus AS ST WHERE ST.TxnStatus=A.TxnStatus) As Status
	FROM [OB].[dbo].TB_LateRes AS LR
	INNER JOIN [OB].[dbo].TB_PayRes AS A ON A.GatewayTxnID = LR.GatewayTxnID
	WHERE LR.GatewayTxnID = CASE WHEN ( '' = ISNULL(@gatewaytxnid,'')) THEN LR.GatewayTxnID ELSE @gatewaytxnid END
	AND LR.BankRefNo = CASE WHEN ( '' = ISNULL(@bankrefno,'')) THEN LR.BankRefNo ELSE @bankrefno END
	AND LR.MerchantID = CASE WHEN ( '' = ISNULL(@MerchantID,'')) THEN LR.MerchantID ELSE @MerchantID END
	AND LR.LastUpdated BETWEEN CASE WHEN ( '' = ISNULL(@startdate,'')) THEN '1970-01-01 00:00:00' ELSE @startdate END AND CASE WHEN ( '' = ISNULL(@enddate,'')) THEN GETDATE() ELSE @enddate END
	
END
GO
