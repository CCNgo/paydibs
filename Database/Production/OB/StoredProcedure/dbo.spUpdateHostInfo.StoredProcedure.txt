USE [OB]
GO
/****** Object:  StoredProcedure [dbo].[spUpdateHostInfo]    Script Date: 6/10/2020 11:47:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUpdateHostInfo]
(
	@i_HostID AS INT,
	@i_QueryFlag AS INT
)
AS
	UPDATE Host SET	QueryFlag = @i_QueryFlag 
	WHERE HostID = @i_HostID
	
	IF @@ROWCOUNT > 0
		RETURN 0
	ELSE
		RETURN -1

GO
