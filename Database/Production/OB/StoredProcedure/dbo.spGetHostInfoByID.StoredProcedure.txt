USE [OB]
GO
/****** Object:  StoredProcedure [dbo].[spGetHostInfoByID]    Script Date: 6/10/2020 11:47:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetHostInfoByID]
(
	@sz_HostID AS VARCHAR(50)
)
AS
	SELECT	HostID, HostName, ISNULL(HostCode,'') HostCode, SvcTypeID, (SELECT SvcType FROM [PG]..CL_SvcType WHERE SvcTypeID=H.SvcTypeID) SvcType, PaymentTemplate, SecondEntryTemplate, MesgTemplate, NeedReplyAcknowledgement, NeedRedirectOTP, Require2ndEntry, AllowQuery, AllowReversal,
				H.ChannelID, H.[Timeout], H.RecStatus IsActive, TxnStatusActionID, HostReplyMethod, OSPymtCode, GatewayTxnIDFormat, DateActivated, DateDeactivated, H.[Desc],
				C.IPAddress PayURL, C.CfgFile, C.ReturnURL, C.ReturnURL2, C.ReturnIPAddresses ReturnIPAddresses, C.Protocol, MerchantPassword, ReturnKey, HashMethod, QueryFlag, SecretKey, InitVector, LogRes
		FROM Host H WITH (NOLOCK)
			INNER JOIN Channel C ON C.ChannelID = H.ChannelID
		WHERE HostID = @sz_HostID

	IF @@ROWCOUNT > 0
		RETURN 0
	ELSE
		RETURN -1

GO
