USE [OB]
GO
/****** Object:  StoredProcedure [dbo].[spUpdateTxnParams]    Script Date: 6/10/2020 11:47:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUpdateTxnParams]
(
	@sz_MerchantID AS VARCHAR(30),
	@sz_MerchantTxnID AS VARCHAR(30),
	@sz_Param1 AS VARCHAR(100),
	@sz_Param2 AS VARCHAR(100),
	@sz_Param3 AS VARCHAR(100),
	@sz_Param4 AS VARCHAR(100),
	@sz_Param5 AS VARCHAR(100),
	@i_TxnState AS INT,
	@i_ReqRes AS INT
)
AS
	DECLARE @szRetDesc VarChar(160)
	DECLARE @iRet AS INT

	DECLARE @szParam1 AS VARCHAR(100)
	DECLARE @szParam2 AS VARCHAR(100)
	DECLARE @szParam3 AS VARCHAR(100)
	DECLARE @szParam4 AS VARCHAR(100)
	DECLARE @szParam5 AS VARCHAR(100)
	DECLARE @iHostID AS INT
	DECLARE @iDetailID AS INT
	DECLARE @szIssuingBank AS VARCHAR(20)

	SET @iRet = -1

	SELECT @iDetailID = PKID, @szIssuingBank = IssuingBank
	FROM TB_PayReq_Detail WITH (NOLOCK)
	WHERE MerchantTxnID = @sz_MerchantTxnID AND MerchantID = @sz_MerchantID
	
	IF @@ROWCOUNT <= 0 BEGIN
		SET @szRetDesc = 'Error: MerchantID and MerchantTxnID not found'
		SELECT @szRetDesc AS [RetDesc], @iRet AS [RetCode], '' AS [IssuingBank]
		RETURN -1
	END

	BEGIN TRANSACTION UpdateTxnDetails

	SELECT @iHostID = HostID
	FROM Host
	WHERE Upper(HostName) = Upper(@szIssuingBank)

	SELECT @szParam1 = ISNULL(Param1,''), @szParam2 = ISNULL(Param2,''), @szParam3 = ISNULL(Param3,''), @szParam4 = ISNULL(Param4,''), @szParam5 = ISNULL(Param5,'')
	FROM TB_Pay_Params WITH (NOLOCK)
	WHERE DetailID = @iDetailID

	IF @@ROWCOUNT > 0 BEGIN
		IF (LOWER(@sz_Param1) <> LOWER(@szParam1)) BEGIN
			IF (LEN(@szParam1) > 0) BEGIN
				SET @szRetDesc = 'Error: Existing Param1 not empty, update not allowed'
				SELECT @szRetDesc AS [RetDesc], @iRet AS [RetCode], @szIssuingBank AS [IssuingBank]
				RETURN -1
			END
		END

		IF (LOWER(@sz_Param2) <> LOWER(@szParam2)) BEGIN
			IF (LEN(@szParam2) > 0) BEGIN
				SET @szRetDesc = 'Error: Existing Param2 not empty, update not allowed'
				SELECT @szRetDesc AS [RetDesc], @iRet AS [RetCode], @szIssuingBank AS [IssuingBank]
				RETURN -1
			END
		END

		IF (LOWER(@sz_Param3) <> LOWER(@szParam3)) BEGIN
			IF (LEN(@szParam3) > 0) BEGIN
				SET @szRetDesc = 'Error: Existing Param3 not empty, update not allowed'
				SELECT @szRetDesc AS [RetDesc], @iRet AS [RetCode], @szIssuingBank AS [IssuingBank]
				RETURN -1
			END
		END

		IF (LOWER(@sz_Param4) <> LOWER(@szParam4)) BEGIN
			IF (LEN(@szParam4) > 0) BEGIN
				SET @szRetDesc = 'Error: Existing Param4 not empty, update not allowed'
				SELECT @szRetDesc AS [RetDesc], @iRet AS [RetCode], @szIssuingBank AS [IssuingBank]
				RETURN -1
			END
		END

		IF (LOWER(@sz_Param5) <> LOWER(@szParam5)) BEGIN
			IF (LEN(@szParam5) > 0) BEGIN
				SET @szRetDesc = 'Error: Existing Param5 not empty, update not allowed'
				SELECT @szRetDesc AS [RetDesc], @iRet AS [RetCode], @szIssuingBank AS [IssuingBank]
				RETURN -1
			END
		END

		IF @sz_Param1 <> '' BEGIN
			UPDATE TB_Pay_Params SET Param1 = @sz_Param1, Param2 = @sz_Param2, Param3 = @sz_Param3, Param4 = @sz_Param4, Param5 = @sz_Param5
			WHERE DetailID = @iDetailID
		END ELSE BEGIN
			UPDATE TB_Pay_Params SET Param2 = @sz_Param2, Param3 = @sz_Param3, Param4 = @sz_Param4, Param5 = @sz_Param5
			WHERE DetailID = @iDetailID
		END

		IF @@ROWCOUNT <= 0 BEGIN
			SET @szRetDesc = 'Error: Update params'
			GOTO ERR_HANDLER
		END

		IF @i_TxnState > 0 BEGIN
			UPDATE TB_PayReq SET TxnState = @i_TxnState, DateModified = GETDATE()
			WHERE MerchantTxnID = @sz_MerchantTxnID AND MerchantID = @sz_MerchantID

			IF @@ROWCOUNT <= 0 BEGIN
				SET @szRetDesc = 'Error: Update transaction state'
				GOTO ERR_HANDLER
			END	
		END
	END
	ELSE BEGIN
		IF @sz_Param1 <> '' BEGIN
			INSERT INTO TB_Pay_Params
			(
				DetailID, MerchantID, MerchantTxnID, HostID, Param1, Param2, Param3, Param4, Param5, ReqRes
			)
			VALUES
			(
				@iDetailID, @sz_MerchantID, @sz_MerchantTxnID, @iHostID, @sz_Param1, @sz_Param2, @sz_Param3, @sz_Param4, @sz_Param5, @i_ReqRes
			)
		END ELSE BEGIN
			INSERT INTO TB_Pay_Params
			(
				DetailID, MerchantID, MerchantTxnID, HostID, Param2, Param3, Param4, Param5, ReqRes
			)
			VALUES
			(
				@iDetailID, @sz_MerchantID, @sz_MerchantTxnID, @iHostID, @sz_Param2, @sz_Param3, @sz_Param4, @sz_Param5, @i_ReqRes
			)
		END

		IF @@ROWCOUNT <= 0 BEGIN
			SET @szRetDesc = 'Error: Insert params'
			GOTO ERR_HANDLER
		END
	END

	SET @iRet = 0
	SET @szRetDesc = 'Transaction params updated'
	COMMIT TRANSACTION UpdateTxnDetails
	SELECT @szRetDesc AS [RetDesc], @iRet AS [RetCode], @szIssuingBank AS [IssuingBank]
	RETURN 0

ERR_HANDLER:
	IF @iRet = -1 BEGIN
		ROLLBACK TRANSACTION UpdateTxnDetails
		SELECT @szRetDesc AS [RetDesc], @iRet AS [RetCode], @szIssuingBank AS [IssuingBank]
		RETURN -1
	END

GO
