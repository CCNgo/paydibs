USE [OB]
GO
/****** Object:  StoredProcedure [dbo].[spUpdatePendingTxnReq]    Script Date: 6/10/2020 11:47:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUpdatePendingTxnReq]
	@sz_GatewayTxnID AS VARCHAR(30)

AS

DECLARE @RetCode Int

BEGIN TRANSACTION UpdatePendingTxnReq

If EXISTS (SELECT TxnStatus FROM TB_PayReq WITH (NOLOCK) WHERE GatewayTxnID = @sz_GatewayTxnID)
BEGIN
	If EXISTS (SELECT TxnStatus FROM TB_PayReq WITH (UPDLOCK) WHERE GatewayTxnID = @sz_GatewayTxnID AND TxnStatus = '30')
	BEGIN
		UPDATE TB_PayReq SET
			TxnStatus = '3'
		WHERE GatewayTxnID = @sz_GatewayTxnID AND TxnStatus = '30'

		IF @@ROWCOUNT = 1 BEGIN
			SET @RetCode = 0
		END
	END
	ELSE
	BEGIN 
		SET @RetCode = 1
		--ROLLBACK TRANSACTION UpdatePendingTxnReq
	END
END
ELSE IF EXISTS (SELECT 1 FROM TB_PayRes WITH (NOLOCK) WHERE GatewayTxnID = @sz_GatewayTxnID)
BEGIN
	SET @RetCode = 2
END
ELSE
BEGIN
	SET @RetCode = 3
END



COMMIT TRANSACTION UpdatePendingTxnReq

SELECT @RetCode AS [RetCode]

GO
