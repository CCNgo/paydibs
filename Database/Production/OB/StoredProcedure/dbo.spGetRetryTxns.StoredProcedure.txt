USE [OB]
GO
/****** Object:  StoredProcedure [dbo].[spGetRetryTxns]    Script Date: 6/10/2020 11:47:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetRetryTxns]
(                                                
	@i_HostID AS Int,
	@i_Action AS Int,
	@i_MaxRow As Int
)
AS
	DECLARE @iRowCount AS Int
	DECLARE @iHostTimeout_S AS Int	-- Added  30 Sept 2013

	SET ROWCOUNT 50 --@i_MaxRow		-- Modified  3 Oct 2013, changed i_MaxRow to 50

	-- Action 3 (Retry query Host); Action 4 (Retry confirm booking); Action 7 (Retry Reversal)
	IF (@i_Action > 0)
	BEGIN
		If (@i_HostID > 0)
		BEGIN
			SELECT	R.MerchantID, R.MerchantTxnID, R.OrderNumber, R.TxnAmount, R.CurrencyCode, R.IssuingBank,
					CONVERT(VARCHAR, R.DateCreated, 120) DateCreated,
					ISNULL(FX.FXCurrencyCode,'') FXCurrencyCode, ISNULL(FX.FXTxnAmt,0) FXTxnAmt
			FROM	TB_PayReq R WITH (NOLOCK)
			LEFT OUTER JOIN TB_PayFX FX WITH (NOLOCK) ON R.DetailID = FX.DetailID
			WHERE	(R.[Action]=@i_Action) AND R.HostID = @i_HostID AND (DATEDIFF(minute, R.DateModified, GETDATE()) > 3) --AND LockedBy = @sz_LockedBy --AND DATEADD(Second, @RecoveryInterval, DateLocked) <= @CurDate
			ORDER BY R.DateCreated ASC
		END
		ELSE
		BEGIN
			SELECT	R.MerchantID, R.MerchantTxnID, R.OrderNumber, R.TxnAmount, R.CurrencyCode, R.IssuingBank,
					CONVERT(VARCHAR, R.DateCreated, 120) DateCreated,
					ISNULL(FX.FXCurrencyCode,'') FXCurrencyCode, ISNULL(FX.FXTxnAmt,0) FXTxnAmt
			FROM	TB_PayReq R WITH (NOLOCK)
			LEFT OUTER JOIN TB_PayFX FX WITH (NOLOCK) ON R.DetailID = FX.DetailID
			WHERE	(R.[Action]=@i_Action) AND (DATEDIFF(minute, R.DateModified, GETDATE()) > 3) --AND LockedBy = @sz_LockedBy --AND DATEADD(Second, @RecoveryInterval, DateLocked) <= @CurDate
			ORDER BY R.DateCreated ASC
		END
	END
	ELSE If (@i_HostID > 0)
	BEGIN
		IF (@i_HostID <> 999) -- Temporarily put 999, coz right now no eGHL banks NOT support Query. Not Bangkok bank and UOB thailand that not support query currently AND Not KTB for AA & OB query KTB which 'll reply GHLDD via another session to OB Query Response URL which then reply AA, possible double query resp from OB to AA and cause double confirm booking at AA
		BEGIN
			-- Transactions that have existed in Request table for more than half an hour/30 minutes/1800 seconds
			-- Retry Query ONLY
			-- Added  30 Sept 2013
			-- @iHostTimeout_S: Gateway will finalize txn if after @iHostTimeout_S, Host still returned "pending/not found"
			-- @iHostTimeout_S - 300: Minus 5 minutes b4 timeout, (5x60)s=300s, from @iHostTimeout_S, to pick up txns to be queried
			-- Example: Bank's actual timeout: 15 minutes; After 20 minutes(25min-5min), Gateway queried bank, for 5 more minutes till 25 min(1500s @iHostTimeout_S)
			SELECT @iHostTimeout_S = [Timeout] FROM Host WHERE HostID = @i_HostID
			SET @iHostTimeout_S = @iHostTimeout_S - 300

			SELECT * FROM(
				SELECT	R.MerchantID, R.MerchantTxnID, R.OrderNumber, R.TxnAmount, R.CurrencyCode, R.IssuingBank,
						CONVERT(VARCHAR, R.DateCreated, 120) DateCreated,
						ISNULL(FX.FXCurrencyCode,'') FXCurrencyCode, ISNULL(FX.FXTxnAmt,0) FXTxnAmt
				FROM	TB_PayReq R WITH (NOLOCK)
				LEFT OUTER JOIN TB_PayFX FX WITH (NOLOCK) ON R.DetailID = FX.DetailID
				INNER JOIN [PG]..Merchant M WITH (NOLOCK) ON M.MerchantID = R.MerchantID COLLATE SQL_Latin1_General_CP1_CI_AS
				WHERE	M.TxnExpired_S < @iHostTimeout_S AND (DateDiff(second, R.DateCreated, GETDATE()) > M.TxnExpired_S-300) AND R.HostID = @i_HostID AND R.[Action] NOT IN (4, 7) --AND LockedBy = @sz_LockedBy --AND DATEADD(Second, @RecoveryInterval, DateLocked) <= @CurDate
			
				UNION ALL

				SELECT R.MerchantID, R.MerchantTxnID, R.OrderNumber, R.TxnAmount, R.CurrencyCode, R.IssuingBank,
				CONVERT(VARCHAR, R.DateCreated, 120) DateCreated,
				ISNULL(FX.FXCurrencyCode,'') FXCurrencyCode, ISNULL(FX.FXTxnAmt,0) FXTxnAmt
				FROM TB_PayReq R WITH (NOLOCK)
				INNER JOIN TB_PayOTC OT WITH (NOLOCK) ON R.DetailID = OT.DetailID
				LEFT OUTER JOIN TB_PayFX FX WITH (NOLOCK) ON R.DetailID = FX.DetailID
				INNER JOIN PG..Merchant M WITH (NOLOCK) ON M.MerchantID = R.MerchantID COLLATE SQL_Latin1_General_CP1_CI_AS
				WHERE DateDiff(second, GETDATE(), OT.DueTime) <= 300 AND R.HostID = @i_HostID AND R.[Action] NOT IN (4, 7)

				UNION ALL

				SELECT	R.MerchantID, R.MerchantTxnID, R.OrderNumber, R.TxnAmount, R.CurrencyCode, R.IssuingBank,
						CONVERT(VARCHAR, R.DateCreated, 120) DateCreated,
						ISNULL(FX.FXCurrencyCode,'') FXCurrencyCode, ISNULL(FX.FXTxnAmt,0) FXTxnAmt
				FROM	TB_PayReq R WITH (NOLOCK)
				LEFT OUTER JOIN TB_PayFX FX WITH (NOLOCK) ON R.DetailID = FX.DetailID
				WHERE	DateDiff(second, R.DateCreated, GETDATE()) > @iHostTimeout_S AND R.HostID = @i_HostID AND R.[Action] NOT IN (4, 7) --AND LockedBy = @sz_LockedBy --AND DATEADD(Second, @RecoveryInterval, DateLocked) <= @CurDate
			) T
			ORDER BY T.DateCreated ASC
		END
		ELSE
		BEGIN
			-- The bank will query OB GW for bookings customer placed that does not have query service for DD GW
			-- to query for status if txns existed in TB_PayReq table for more than half an hour.
			-- Therefore, retry for txms should be different, that is to only return txns which nearly
			-- reach 2 hours (e.g. 1.5 hours) because after 2 hours the booking will be freed and
			-- therefore should not be returned to BCA for customer to pay.
			-- Return transactions that have existed in Request table for 2 hours/120 minutes/7200 seconds
			-- to Retry Service so that it will send to OB GW which will then finalize it to TB_PayRes table
			-- as Expired Transaction
			-- Retry Query ONLY
			SELECT	R.MerchantID, R.MerchantTxnID, R.OrderNumber, R.TxnAmount, R.CurrencyCode, R.IssuingBank,
					CONVERT(VARCHAR, R.DateCreated, 120) DateCreated,
					ISNULL(FX.FXCurrencyCode,'') FXCurrencyCode, ISNULL(FX.FXTxnAmt,0) FXTxnAmt
			FROM	TB_PayReq R WITH (NOLOCK)
			INNER JOIN [PG]..Merchant M WITH (NOLOCK) ON R.MerchantID = M.MerchantID COLLATE SQL_Latin1_General_CP1_CI_AS
			LEFT OUTER JOIN TB_PayFX FX WITH (NOLOCK) ON R.DetailID = FX.DetailID
			WHERE	DateDiff(second, R.DateCreated, GETDATE()) >= M.TxnExpired_S AND R.HostID = @i_HostID AND R.[Action] <> 4 --AND LockedBy = @sz_LockedBy --AND DATEADD(Second, @RecoveryInterval, DateLocked) <= @CurDate
			--WHERE	DateDiff(second, DateCreated, GETDATE()) >= 172800 AND HostID = @i_HostID AND Action <> 4 --AND LockedBy = @sz_LockedBy --AND DATEADD(Second, @RecoveryInterval, DateLocked) <= @CurDate
			ORDER BY R.DateCreated ASC
		END
	END
	ELSE
	BEGIN
		-- Transactions that have existed in Request table for more than half an hour/30 minutes/1800 seconds
		-- Retry Query ONLY
		SELECT	R.MerchantID, R.MerchantTxnID, R.OrderNumber, R.TxnAmount, R.CurrencyCode, R.IssuingBank,
				CONVERT(VARCHAR, R.DateCreated, 120) DateCreated,
				ISNULL(FX.FXCurrencyCode,'') FXCurrencyCode, ISNULL(FX.FXTxnAmt,0) FXTxnAmt
		FROM	TB_PayReq R WITH (NOLOCK)
		LEFT OUTER JOIN TB_PayFX FX WITH (NOLOCK) ON R.DetailID = FX.DetailID
		WHERE	DateDiff(second, R.DateCreated, GETDATE()) > 1800 AND R.[Action] <> 4
		ORDER BY R.DateCreated ASC
	END

	--UPDATE TB_PayReq SET LockedBy = @sz_LockedBy

	SET @iRowCount = @@ROWCOUNT
	IF @iRowCount > 0
	BEGIN
		RETURN @iRowCount
	END
	ELSE
	BEGIN
		RETURN 0				
	END

	SET ROWCOUNT 0


GO
