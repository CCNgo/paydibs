USE [OB]
GO
/****** Object:  StoredProcedure [dbo].[spInsertMCCReqTxn]    Script Date: 6/10/2020 11:47:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spInsertMCCReqTxn] --51, 'AABUQK12041080015','A07','A07AABUQK12041080015','AABUQK','SGD','MYR',1.00,2.50,'4/10/2012 12:05:00 PM',20120410
(
	@i_HostID AS INTEGER,
	@sz_MerchantTxnID AS VARCHAR(30),
	@sz_MerchantID AS VARCHAR(30),
	@sz_GatewayTxnID AS VARCHAR(30),
	@sz_OrderNumber AS VARCHAR(20),
	@sz_CurrencyCode AS VARCHAR(3),
	@sz_BaseCurrencyCode AS VARCHAR(3),
	@d_TxnAmt AS DECIMAL(18,2),
	@d_BaseTxnAmt AS DECIMAL(18,2),
	--@sz_Param5 AS VARCHAR(100),
	@sz_DateCreated AS VARCHAR(23),
	@i_TxnDay AS INTEGER
)
AS
	DECLARE @RetCode Int
	DECLARE @RetDesc VarChar(160)
	DECLARE @szParam5 AS VARCHAR(100)
	--DECLARE @szSegmentID AS VARCHAR(50)
	--DECLARE @szRateQuoteID AS VARCHAR(50)

	--Check Param5 whether it is empty or not. If yes, stop proceed else insert txn.
	SELECT @szParam5 = ISNULL(Param5,'') FROM TB_Pay_Params WITH (NOLOCK)
	WHERE MerchantTxnID = @sz_MerchantTxnID AND MerchantID = @sz_MerchantID AND HostID = @i_HostID

	IF @szParam5 IS NULL OR @szParam5 = ''
	BEGIN
		--Check the uniqueness of merchanttxnid & MerchantID in TB_MCCRes table
		IF EXISTS(SELECT MerchantTxnID FROM TB_MCCRes WITH (NOLOCK)
				WHERE MerchantTxnID = @sz_MerchantTxnID AND MerchantID = @sz_MerchantID)
		BEGIN
			SET @RetCode = -1
			SET @RetDesc = 'Duplicate transaction in MCCRes table'
		END
		ELSE
		BEGIN
			BEGIN TRANSACTION INSERTTXNRES
			INSERT INTO TB_MCCRes 
			(
				HostID, MerchantTxnID, MerchantID, GatewayTxnID, OrderNumber, CurrencyCode, BaseCurrencyCode,
				TxnAmt, BaseTxnAmt, RateQuoteID, MCCStatus, TxnStatus, TxnDay, DateCreated, DateCompleted
			)
			VALUES  --CL_TxnStatus 2911 = Invalid/Empty RateQuoteID
			(
				@i_HostID, @sz_MerchantTxnID, @sz_MerchantID, @sz_GatewayTxnID, @sz_OrderNumber, @sz_CurrencyCode, @sz_BaseCurrencyCode, 
				@d_TxnAmt, @d_BaseTxnAmt, '-', '-', 2911, @i_TxnDay, @sz_DateCreated, GetDate()
			)

			IF @@ROWCOUNT > 0
			BEGIN
				COMMIT TRANSACTION INSERTTXNRES
			END
			ELSE
			BEGIN
				ROLLBACK TRANSACTION INSERTTXNRES
			END

			SET @RetCode = -1
			SET @RetDesc = 'Param5 (RateQuoteID) is empty'
		END
	END
	ELSE
	BEGIN
		--Check the uniqueness of merchanttxnid & MerchantID in TB_MCCReq table
		IF EXISTS(SELECT MerchantTxnID FROM TB_MCCReq WITH (NOLOCK)
				WHERE MerchantTxnID = @sz_MerchantTxnID AND MerchantID = @sz_MerchantID)
		BEGIN
			SET @RetCode = -1
			SET @RetDesc = 'Duplicate transaction'
		END
		ELSE
		BEGIN
			BEGIN TRANSACTION INSERTTXN
			--Split Param5 to SegmentID & RateQuoteID. Param5 received from AA in format SegmentID|RateQuoteID
			--SET @szSegmentID  = LEFT(@szParam5, NULLIF(CHARINDEX('|', @szParam5) - 1, -1))
			--SET @szRateQuoteID = RIGHT(@szParam5, ISNULL(NULLIF(CHARINDEX('|', REVERSE(@szParam5)) - 1, -1), LEN(@szParam5)))

			-- Initialize the Transaction
			INSERT INTO TB_MCCReq
			(
				HostID, MerchantTxnID, MerchantID, GatewayTxnID, OrderNumber, CurrencyCode, BaseCurrencyCode, 
				TxnAmt, BaseTxnAmt, /*SegmentID,*/ RateQuoteID, DateCreated, TxnDay
			) 
			VALUES
			(
				@i_HostID, @sz_MerchantTxnID, @sz_MerchantID, @sz_GatewayTxnID, @sz_OrderNumber, @sz_CurrencyCode, @sz_BaseCurrencyCode, 
				@d_TxnAmt, @d_BaseTxnAmt, /*@szSegmentID, @szRateQuoteID,*/ @szParam5, @sz_DateCreated, @i_TxnDay
			)
	
			IF @@ROWCOUNT > 0
			BEGIN
				COMMIT TRANSACTION INSERTTXN
				SET @RetCode = 0
				SET @RetDesc = 'Insert MCC transaction success'
			END
			ELSE
			BEGIN
				ROLLBACK TRANSACTION INSERTTXN
				SET @RetCode = -1
				SET @RetDesc = 'Insert MCC transaction failed'
			END
		END
	END
	
	SELECT @RetCode [RetCode], @RetDesc [RetDesc]

GO
