USE [OB]
GO
/****** Object:  StoredProcedure [dbo].[spGetTxnResult2]    Script Date: 6/10/2020 11:47:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetTxnResult2]
(
      @sz_MerchantID AS VARCHAR(30),
      @sz_MerchantTxnID AS VARCHAR(30),
      @sz_CurrencyCode AS CHAR(3)
)
AS
      DECLARE @RetCode Int
      DECLARE @RetDesc VarChar(160)
	  DECLARE @iDetailID Int	-- Added  3 Oct 2013

	  SET @iDetailID = 0		-- Added  3 Oct 2013

      --DECLARE @dtTxnDate datetime

	  -- Added  @iPKID, 5 Sept 2013
      DECLARE	@iPKID bigint, @szTxnType varchar(7), @szMerchantID char(3), @szMerchantTxnID varchar(30),
				@szOrderNumber varchar(20), @szTxnAmount varchar(18), @szBaseTxnAmount varchar(18), @szCurrencyCode char(3), @szBaseCurrencyCode varchar(3), @szIssuingBank varchar(20),
				@szSessionID varchar(100), @szParam1 varchar(100), @szParam2 varchar(100),
				@szParam3 varchar(100), @szParam4 varchar(100), @szParam5 varchar(100), @szGatewayTxnID varchar(30),
				@szParam6 varchar(50), @szParam7 varchar(50),
				@iTxnState int, @iTxnStatus int, @iMerchantTxnStatus int, @szBankRefNo varchar(30), @szRespMesg varchar(255), 
				@szHashMethod varchar(6), @szHashValue varchar(200), @iAction int, @iDuration int,
				@szGatewayID varchar(4), @szCardPAN varchar(20), @szMachineID varchar(10), @szMerchantReturnURL varchar(255), @szMerchantSupportURL varchar(255), @szMerchantCallbackURL varchar(255),
				@szOSPymtCode varchar(3), @szBankRespCode varchar(20), @szDateCreated varchar(23), @iOSRet int, @iErrSet int, @iErrNum int, @iHostID int,
				@szLanguageCode varchar(2), @szTxnState varchar(50), @szTxnStatus varchar(50), @szOrderDesc nvarchar(100),
				@szTtlRefundAmt varchar(18)	--Added by OoiMei, 15 Sept 2015

	  SELECT @iDetailID = PKID FROM TB_PayReq_Detail WHERE MerchantTxnID = @sz_MerchantTxnID AND MerchantID = @sz_MerchantID AND CurrencyCode = @sz_CurrencyCode

	  IF @@ROWCOUNT = 1
      BEGIN 
            SELECT  @iPKID = A.PKID, @szTxnType = A.TxnType, @szMerchantID = A.MerchantID, @szMerchantTxnID = A.MerchantTxnID,
					@szOrderNumber = A.OrderNumber,     @szTxnAmount = A.TxnAmount, @szCurrencyCode = A.CurrencyCode,
					@szIssuingBank = A.IssuingBank,
					@szParam1 = ISNULL(B.Param1, ''), @szParam2 = ISNULL(B.Param2, ''), @szParam3 = ISNULL(B.Param3, ''),
					@szParam4 = ISNULL(B.Param4, ''), @szParam5 = ISNULL(B.Param5, ''), @szParam6 = ISNULL(B.Param6, ''),
					@szParam7 = ISNULL(B.Param7, ''),
					@szGatewayTxnID = A.GatewayTxnID, @iTxnState = A.TxnState, @iTxnStatus = A.TxnStatus, @iMerchantTxnStatus = A.MerchantTxnStatus,
					@szTxnState = (SELECT StateName FROM dbo.CL_TxnState WHERE TxnState = A.TxnState), @szTxnStatus = (SELECT TxnDescription FROM dbo.CL_TxnStatus WHERE TxnStatus= A.TxnStatus),
					@szBankRespCode = ISNULL(A.BankRespCode, ''), @szBankRefNo = ISNULL(A.BankRefNo, ''), @szRespMesg = ISNULL(A.RespMesg, ''),
					@szHashMethod = A.HashMethod, @szHashValue = A.HashValue, @iAction = 0, @iDuration = 0,
					@szGatewayID = A.GatewayID, @szCardPAN = A.CardPAN, @szMachineID = A.MachineID,
					@iOSRet = A.OSRet, @iErrSet = A.ErrSet, @iErrNum = A.ErrNum, @iHostID = A.HostID, @szDateCreated = CONVERT(VARCHAR, A.DateCreated, 120),
					@szLanguageCode = C.LanguageCode, @szSessionID = ISNULL(C.SessionID, ''), @szBaseTxnAmount = ISNULL(C.BaseTxnAmount, ''), @szBaseCurrencyCode = ISNULL(C.BaseCurrencyCode, ''),
					@szOrderDesc = C.OrderDesc, @szMerchantCallbackURL = ISNULL(C.MerchantCallbackURL,''), @szTtlRefundAmt = A.TotRefundAmt --Added by OoiMei, 15 Sept 2015
            FROM  TB_PayRes A WITH (NOLOCK) 
			INNER JOIN TB_PayReq_Detail C WITH (NOLOCK) ON C.PKID=A.PKID
			LEFT OUTER JOIN TB_Pay_Params B WITH (NOLOCK) ON B.DetailID=A.PKID AND B.ReqRes=2                        		
			WHERE A.PKID = @iDetailID	-- Added  3 Oct 2013

            IF @@ROWCOUNT = 1 
            BEGIN
                  SET @RetCode = 1
                  SET @RetDesc = 'Transaction exists'  

					-- Added on 27 Jun 2013, cater for Query return reversed status
					-- Added on 4 Sept 2013, Reversal Pending 31
					-- Added on 28 Apr 2015, Auth Success 15; Capture Success 16
					IF (@iTxnStatus = 9 OR @iTxnStatus = 31 OR @iTxnStatus = 15 OR @iTxnStatus = 16) BEGIN 
						SET @iMerchantTxnStatus = @iTxnStatus
					END

                  SELECT      @RetCode [RetCode], @RetDesc [RetDesc],
                              @iPKID [PKID], @szTxnType [TxnType], @szMerchantID [MerchantID], @szMerchantTxnID [MerchantTxnID],
                              @szOrderNumber [OrderNumber], @szTxnAmount [TxnAmount], @szCurrencyCode [CurrencyCode],
                              @szIssuingBank [IssuingBank], @szSessionID [SessionID],
                              @szParam1 [Param1], @szParam2 [Param2], @szParam3 [Param3], @szParam4 [Param4], @szParam5 [Param5],
                              @szParam6 [Param6], @szParam7 [Param7],
                              @szGatewayTxnID [GatewayTxnID], @iTxnState [TxnState], @iTxnStatus [TxnStatus], @iMerchantTxnStatus [MerchantTxnStatus],
                              @szBankRespCode [BankRespCode], @szBankRefNo [BankRefNo], @szRespMesg [RespMesg], @szHashMethod [HashMethod], @szHashValue [HashValue],
                              @iAction [Action], @iDuration [Duration], @szGatewayID [GatewayID], @szCardPAN [CardPAN], @szMachineID [MachineID], '' [MerchantReturnURL], '' [MerchantSupportURL], '' [OSPymtCode],
                              @iOSRet [OSRet], @iErrSet [ErrSet], @iErrNum [ErrNum], @iHostID [HostID], @szDateCreated [DateCreated],
                              @szLanguageCode [LanguageCode], @szBaseTxnAmount [BaseTxnAmount], @szBaseCurrencyCode [BaseCurrencyCode],
                              @szTxnState [TxnStateName], @szTxnStatus [TxnStatusName], @szOrderDesc [OrderDesc], @szMerchantCallbackURL [MerchantCallbackURL],
							  @szTtlRefundAmt [TotRefundAmt] --Added by OoiMei, 15 Sept 2015
                  RETURN 1
            END
            ELSE IF @@ROWCOUNT > 1
            BEGIN
                  SET @RetCode = 2
                  SET @RetDesc = 'Error: Multiple transactions found'

                  SELECT @RetCode AS [RetCode], @RetDesc AS [RetDesc]

                  RETURN 2                            
            END
            ELSE
            BEGIN
 SELECT  @iPKID = A.PKID, @szTxnType = A.TxnType, @szMerchantID = A.MerchantID, @szMerchantTxnID = A.MerchantTxnID, 
                          @szOrderNumber = B.OrderNumber, @szTxnAmount = A.TxnAmount, @szCurrencyCode = A.CurrencyCode,
                          @szIssuingBank = A.IssuingBank, @szSessionID = B.SessionID,
                          @szParam1 = C.Param1, @szParam2 = C.Param2, @szParam3 = C.Param3, @szParam4 = C.Param4, @szParam5 = C.Param5,
                          @szParam6 = C.Param6, @szParam7 = C.Param7,
                          @szGatewayTxnID = A.GatewayTxnID, @iTxnState = A.TxnState, @iTxnStatus = A.TxnStatus, @iMerchantTxnStatus = A.MerchantTxnStatus,
                          @szTxnState = (SELECT StateName FROM dbo.CL_TxnState WHERE TxnState = A.TxnState), @szTxnStatus = (SELECT TxnDescription FROM dbo.CL_TxnStatus WHERE TxnStatus= A.TxnStatus),
                          @szBankRespCode = ISNULL(A.BankRespCode, ''), @szBankRefNo = ISNULL(A.BankRefNo, ''), @szRespMesg = ISNULL(A.RespMesg, ''),
                          @szHashMethod = B.HashMethod, @szHashValue = '', @iAction = A.[Action], @iDuration = DateDiff(second, A.DateCreated, GetDate()),
                          @szGatewayID = B.GatewayID, @szCardPAN = B.CardPAN, @szMachineID = A.MachineID, @szMerchantReturnURL = B.MerchantReturnURL, @szMerchantSupportURL = B.MerchantSupportURL, @szOSPymtCode = ISNULL(A.OSPymtCode,''),
                          @iOSRet = A.OSRet, @iErrSet = A.ErrSet, @iErrNum = A.ErrNum, @iHostID = A.HostID, @szDateCreated = CONVERT(VARCHAR, A.DateCreated, 120),
                          @szLanguageCode = B.LanguageCode, @szBaseTxnAmount = ISNULL(B.BaseTxnAmount, ''), @szBaseCurrencyCode = ISNULL(B.BaseCurrencyCode, ''),
                          @szOrderDesc = B.OrderDesc, @szMerchantCallbackURL = ISNULL(B.MerchantCallbackURL, '')
                  FROM  TB_PayReq A WITH (NOLOCK)
                              INNER JOIN TB_PayReq_Detail B WITH (NOLOCK) ON A.DetailID=B.PKID
                              LEFT OUTER JOIN TB_Pay_Params C WITH (NOLOCK) ON C.DetailID=B.PKID AND C.ReqRes=1
                  WHERE A.DetailID = @iDetailID		-- Added  3 Oct 2013

                  IF @@ROWCOUNT = 1
                  BEGIN
                        SET @RetCode = -1
                        SET @RetDesc = 'Transaction exists' --'Transaction not yet completed'      

						-- Added on 4 Sept 2013, Reversal Pending 31
						-- Added on 22 Sept 2013, Reversed 9
						IF (@iTxnStatus = 9 OR @iTxnStatus = 31) BEGIN 
							SET @iMerchantTxnStatus = @iTxnStatus
						END

                        SELECT      @RetCode [RetCode], @RetDesc [RetDesc],
                                    @iPKID [PKID], @szTxnType [TxnType], @szMerchantID [MerchantID], @szMerchantTxnID [MerchantTxnID],
                                    @szOrderNumber [OrderNumber], @szTxnAmount [TxnAmount], @szCurrencyCode [CurrencyCode],
                                    @szIssuingBank [IssuingBank], @szSessionID [SessionID],
                                    @szParam1 [Param1], @szParam2 [Param2], @szParam3 [Param3], @szParam4 [Param4], @szParam5 [Param5],@szParam6 [Param6], @szParam7 [Param7],
                                    @szGatewayTxnID [GatewayTxnID], @iTxnState [TxnState], @iTxnStatus [TxnStatus], @iMerchantTxnStatus [MerchantTxnStatus],
                                    @szBankRespCode [BankRespCode], @szBankRefNo [BankRefNo],  @szRespMesg [RespMesg], @szHashMethod [HashMethod], @szHashValue [HashValue],
                                    @iAction [Action], @iDuration [Duration], @szGatewayID [GatewayID], @szCardPAN [CardPAN], @szMachineID [MachineID], @szMerchantReturnURL [MerchantReturnURL], @szMerchantSupportURL [MerchantSupportURL], @szOSPymtCode [OSPymtCode],
                                    @iOSRet [OSRet], @iErrSet [ErrSet], @iErrNum [ErrNum], @iHostID [HostID], @szDateCreated [DateCreated],
                                    @szLanguageCode [LanguageCode], @szBaseTxnAmount [BaseTxnAmount], @szBaseCurrencyCode [BaseCurrencyCode],
                                    @szTxnState [TxnStateName], @szTxnStatus [TxnStatusName],  @szOrderDesc [OrderDesc],  @szMerchantCallbackURL [MerchantCallbackURL]
                        RETURN -1
                  END
                  ELSE IF @@ROWCOUNT > 1
                  BEGIN
                        SET @RetCode = -2
                        SET @RetDesc = 'Error: Multiple request transactions  found'

                        SELECT @RetCode AS [RetCode], @RetDesc AS [RetDesc]

                        RETURN -2                           
                  END                     
                  ELSE
                  BEGIN
                        SET @RetCode = -3                         
                        SET @RetDesc = 'Error: Internal storage error for transaction'

                        SELECT @RetCode AS [RetCode], @RetDesc AS [RetDesc]

                        RETURN -3
                  END
            END
      END
      ELSE
      BEGIN 
            SET @RetCode = 0
            SET @RetDesc = 'Transaction not exists'

            SELECT @RetCode AS [RetCode], @RetDesc AS [RetDesc]

            RETURN 0
      END


set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON


GO
