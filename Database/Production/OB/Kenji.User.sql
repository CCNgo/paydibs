USE [OB]
GO
/****** Object:  User [Kenji]    Script Date: 6/10/2020 11:47:05 AM ******/
CREATE USER [Kenji] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [Kenji]
GO
