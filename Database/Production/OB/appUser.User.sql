USE [OB]
GO
/****** Object:  User [appUser]    Script Date: 6/10/2020 11:47:05 AM ******/
CREATE USER [appUser] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_datareader] ADD MEMBER [appUser]
GO
ALTER ROLE [db_denydatawriter] ADD MEMBER [appUser]
GO
