USE [OB]
GO
/****** Object:  UserDefinedTableType [dbo].[BankTbl]    Script Date: 6/10/2020 11:47:05 AM ******/
CREATE TYPE [dbo].[BankTbl] AS TABLE(
	[BankID] [varchar](10) NULL,
	[BankStatus] [int] NULL,
	[HostID] [int] NULL
)
GO
