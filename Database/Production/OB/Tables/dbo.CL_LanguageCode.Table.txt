USE [OB]
GO
/****** Object:  Table [dbo].[CL_LanguageCode]    Script Date: 6/10/2020 11:47:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CL_LanguageCode](
	[LanguageCode] [char](2) NOT NULL,
	[Description] [varchar](50) NULL,
	[RecStatus] [int] NOT NULL,
 CONSTRAINT [PK_CL_LanguageCode] PRIMARY KEY CLUSTERED 
(
	[LanguageCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CL_LanguageCode] ADD  CONSTRAINT [DF_CL_LanguageCode_RecStatus]  DEFAULT ((1)) FOR [RecStatus]
GO
