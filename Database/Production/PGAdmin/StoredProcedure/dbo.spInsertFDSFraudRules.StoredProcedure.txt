USE [PGAdmin]
GO
/****** Object:  StoredProcedure [dbo].[spInsertFDSFraudRules]    Script Date: 6/10/2020 12:15:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spInsertFDSFraudRules] 
	@PDomainID AS INT,
	@PPriority AS INT
AS
BEGIN
	DECLARE @PMerchantID AS VARCHAR(30)
	
	SELECT @PMerchantID = DomainName
	FROM CMS_Domain (NOLOCK)
	WHERE DomainID = @PDomainID

	INSERT INTO PG..FDS_FraudRules(MerchantID, RuleName, Window, Expression, Limit, Amount, 
	[Priority], [Action], TxnType, [Services], RecActive)
	--VALUES(@PMerchantID, 'Card Velocity Limit', 1, -1, 5, 10000.00, 
	VALUES(@PMerchantID, 'Card Velocity Limit', 1, -1, 2, 5000.00, -- Modified by Daniel 23 Apr 2019. Change Limit to 2. Amount to 5000.00
	@PPriority, 1, 1, 1, 1)
END
GO
