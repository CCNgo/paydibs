USE [PGAdmin]
GO

/****** Object:  StoredProcedure [dbo].[spRepTBPayTxnRefTBPayRes]    Script Date: 7/3/2020 5:15:22 PM ******/
DROP PROCEDURE [dbo].[spRepTBPayTxnRefTBPayRes]
GO

/****** Object:  StoredProcedure [dbo].[spRepTBPayTxnRefTBPayRes]    Script Date: 7/3/2020 5:15:22 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spRepTBPayTxnRefTBPayRes]
	@PMerchantID AS CHAR(30),
	@PSince AS datetime,
	@PTo AS datetime,
	@POBOtherStatus AS VARCHAR(MAX),
	@PPGOtherStatus AS VARCHAR(MAX),
	@PPaymentID AS VARCHAR(30),
	@PGatewayTxnID AS VARCHAR(30),
	@PCustEmail AS VARCHAR(60),
	@POrderID AS VARCHAR(20),
	@PHostType AS VARCHAR(MAX),
	@PHostName AS VARCHAR(20),
	@PCurrencyCode AS CHAR(3),
	-- ADD START DANIEL 20181128 [Added new parameter PMode]
	@PMode AS INT = 0 
	-- ADD END DANIEL 20181128 [Added new parameter PMode]
AS
BEGIN
	DECLARE @sz_Select AS VARCHAR(MAX),
		@sz_SelectOB AS NVARCHAR(MAX),
		@sz_SelectCC AS NVARCHAR(MAX),
		@MerchantIDList AS VARCHAR(MAX)

	/*ADD START DANIEL 20181128 [PMode = 1 to capture only reseller and leave out sub-merchant transaction
								 PMode = 2 to display all sub-merchant transaction and per sub-merchant transaction]*/
	IF(@PMode = 1)
		BEGIN
			SET @MerchantIDList = NULL
		END
	ELSE
		IF(@PMode = 2)
			BEGIN
				SELECT @MerchantIDList = COALESCE(@MerchantIDList + ''', ''', '') + MerchantID 
						FROM PG..Merchant (NOLOCK)
						WHERE RecStatus = 1
						AND MerchantGroup = @PMerchantID
						AND MerchantID <> MerchantGroup	
			END
		ELSE
	/*ADD END DANIEL 20181128 [PMode = 1 to capture only reseller and leave out sub-merchant transaction
							   PMode = 2 to display all sub-merchant transaction and per sub-merchant transaction]*/
			BEGIN
				SELECT @MerchantIDList = COALESCE(@MerchantIDList + ''', ''', '') + MerchantID 
				FROM PG..Merchant (NOLOCK)
				WHERE RecStatus = 1
				AND MerchantGroup = @PMerchantID
			END

	IF (@MerchantIDList IS NULL)
		BEGIN
			SET @MerchantIDList = '''' + @PMerchantID + ''''
		END
	ELSE
		BEGIN
			SET @MerchantIDList = '''' + @MerchantIDList + ''''
		END

	-- OB
	IF ((CHARINDEX('OB', @PHostType, 0) > 0) OR (CHARINDEX('WA', @PHostType, 0) > 0) 
		OR (CHARINDEX('OTC', @PHostType, 0) > 0) OR (CHARINDEX('MO', @PHostType, 0) > 0))
		BEGIN
			SET @sz_SelectOB = 'SELECT txn.DateCreated, (txn.MerchantID COLLATE SQL_Latin1_General_CP1_CI_AS) AS MerchantID, '
			--ADD START DANIEL 20190103 [Add Pg..Merchant Desc in select statement]
			SET @sz_SelectOB = @sz_SelectOB + '(merc.[Desc] COLLATE SQL_Latin1_General_CP1_CI_AS) AS TradingName, '
			--ADD E N D DANIEL 20190103 [Add Pg..Merchant Desc in select statement]
			SET @sz_SelectOB = @sz_SelectOB + '(txn.OrderNumber COLLATE SQL_Latin1_General_CP1_CI_AS) AS OrderNumber, '
			SET @sz_SelectOB = @sz_SelectOB + '(txn.MerchantTxnID COLLATE SQL_Latin1_General_CP1_CI_AS) AS PaymentID, '
			SET @sz_SelectOB = @sz_SelectOB + '(txn.GatewayTxnID COLLATE SQL_Latin1_General_CP1_CI_AS) AS GatewayTxnID, '
			SET @sz_SelectOB = @sz_SelectOB + ''' ''  AS BankRefNo, '
			SET @sz_SelectOB = @sz_SelectOB + '(txn.CurrencyCode COLLATE SQL_Latin1_General_CP1_CI_AS) AS CurrencyCode, '
			SET @sz_SelectOB = @sz_SelectOB + 'txn.TxnAmt, (TxnDescription COLLATE SQL_Latin1_General_CP1_CI_AS) AS TxnDescription, txn.TxnStatus, '
			SET @sz_SelectOB = @sz_SelectOB + '(stat.Remarks COLLATE SQL_Latin1_General_CP1_CI_AS) AS [Status], '
			SET @sz_SelectOB = @sz_SelectOB + '(HostDesc COLLATE SQL_Latin1_General_CP1_CI_AS) AS Channel, '
			
			--ADD START DANIEL 20181128 [Add conditional statement. If not fulfill, remove select customer info statement]
			--SET @sz_SelectOB = @sz_SelectOB + 'CustEmail, CustName, CustPhone, Host.PymtMethod AS ChannelType, '
			IF(@PMode != 2)
				BEGIN
					SET @sz_SelectOB = @sz_SelectOB + 'CustEmail, CustName, CustPhone, ' 
				END
			
			SET @sz_SelectOB = @sz_SelectOB + 'Host.PymtMethod AS ChannelType, '
			--ADD E N D DANIEL 20181128 [Add conditional statement. If not fulfill, remove select customer info statement]	
			SET @sz_SelectOB = @sz_SelectOB + ' '' '' AS AuthCode, '' '' AS CreditCardNo, '' '' AS ECI '			
			SET @sz_SelectOB = @sz_SelectOB + 'FROM PG.dbo.TB_PayTxnRef txn (NOLOCK) '
			--ADD START DANIEl 20190103 [Left join Pg..Merchant]
			SET @sz_SelectOB = @sz_SelectOB + 'LEFT JOIN PG..Merchant merc (NOLOCK) ON (txn.MerchantID COLLATE SQL_Latin1_General_CP1_CI_AS) = (merc.MerchantID COLLATE SQL_Latin1_General_CP1_CI_AS) '
			--ADD E N D DANIEl 20190103 [Left join Pg..Merchant]
			SET @sz_SelectOB = @sz_SelectOB + 'LEFT JOIN OB.dbo.CL_TxnStatus stat (NOLOCK) ON txn.TxnStatus = stat.TxnStatus '
			SET @sz_SelectOB = @sz_SelectOB + 'LEFT JOIN OB.dbo.Host (NOLOCK) ON Host.HostID = txn.HostID '
			SET @sz_SelectOB = @sz_SelectOB + 'LEFT JOIN OB.dbo.TB_PayReq_Detail reqd (NOLOCK) ON reqd.MerchantID = txn.MerchantID COLLATE SQL_Latin1_General_CP1_CI_AS '
			SET @sz_SelectOB = @sz_SelectOB + 'AND reqd.MerchantTxnID = txn.MerchantTxnID COLLATE SQL_Latin1_General_CP1_CI_AS '
			SET @sz_SelectOB = @sz_SelectOB + 'WHERE txn.DateCreated BETWEEN ''' + CAST(@PSince AS VARCHAR(30)) + ''' AND ''' + CAST((@PTo+1) AS VARCHAR(30)) + ''' '
			
			--ADD START DANIEL 20190111 [Only get records in which GatewayTxnID is not exist/same in TB_PayRes]
			SET @sz_SelectOB = @sz_SelectOB + 'AND NOT EXISTS (SELECT * FROM OB..TB_PayRes res where res.MerchantTxnID = txn.MerchantTxnID COLLATE SQL_Latin1_General_CP1_CI_AS) '
			--ADD E N D DANIEL 20190111 [Only get records in which GatewayTxnID is not exist/same in TB_PayRes]

			SET @sz_SelectOB = @sz_SelectOB + 'AND txn.TxnStatus IN (' + @POBOtherStatus + ') '

			--SET @sz_SelectOB = @sz_SelectOB + 'AND txn.PymtMethod = ''OB'' ' 

			IF (@PMerchantID <> '')
				SET @sz_SelectOB = @sz_SelectOB + 'AND txn.MerchantID IN (' + @MerchantIDList + ') '

			IF (@POrderID <> '')
				SET @sz_SelectOB = @sz_SelectOB + 'AND txn.OrderNumber = ''' + @POrderID + ''' '

			IF (@PCustEmail <> '')
				SET @sz_SelectOB = @sz_SelectOB + 'AND reqd.CustEmail = ''' + @PCustEmail + ''' '

			IF (@PHostType <> '')
				SET @sz_SelectOB = @sz_SelectOB + 'AND Host.PymtMethod IN (' + @PHostType + ') '

			IF (@PHostName <> '')
				SET @sz_SelectOB = @sz_SelectOB + 'AND Host.HostName LIKE ''%' + @PHostName + '%'' '

			IF (@PCurrencyCode <> '')
				SET @sz_SelectOB = @sz_SelectOB + 'AND txn.CurrencyCode = ''' + @PCurrencyCode + ''' '

			IF (@PPaymentID <> '')
				SET @sz_SelectOB = @sz_SelectOB + 'AND txn.MerchantTxnID = ''' + @PPaymentID + ''' '

			IF (@PGatewayTxnID <> '')
				SET @sz_SelectOB = @sz_SelectOB + 'AND txn.GatewayTxnID = ''' + @PGatewayTxnID + ''' '

			--SET @sz_SelectOB = @sz_SelectOB + 'AND (HostDesc <> ''' + 'Alliance Bank' + ''' ' + 'AND HostDesc <> ''' + 'RHB'  + ''' ' +  ')'

			SET @sz_SelectOB = @sz_SelectOB + 'UNION '
	
			SET @sz_SelectOB = @sz_SelectOB + 'SELECT res.DateCreated, (res.MerchantID COLLATE SQL_Latin1_General_CP1_CI_AS) AS MerchantID, '
			--ADD START DANIEL 20190103 [Add Pg..Merchant Desc in select statement]
			SET @sz_SelectOB = @sz_SelectOB + '(merc.[Desc] COLLATE SQL_Latin1_General_CP1_CI_AS) AS TradingName, '
			--ADD E N D DANIEL 20190103 [Add Pg..Merchant Desc in select statement]
			SET @sz_SelectOB = @sz_SelectOB + '(res.OrderNumber COLLATE SQL_Latin1_General_CP1_CI_AS) AS OrderNumber, '
			SET @sz_SelectOB = @sz_SelectOB + '(res.MerchantTxnID COLLATE SQL_Latin1_General_CP1_CI_AS) AS PaymentID, '
			SET @sz_SelectOB = @sz_SelectOB + '(res.GatewayTxnID COLLATE SQL_Latin1_General_CP1_CI_AS) AS GatewayTxnID, '
			SET @sz_SelectOB = @sz_SelectOB + ''' ''  AS BankRefNo, '
			SET @sz_SelectOB = @sz_SelectOB + '(res.CurrencyCode COLLATE SQL_Latin1_General_CP1_CI_AS) AS CurrencyCode, '
			SET @sz_SelectOB = @sz_SelectOB + 'res.TxnAmt, (TxnDescription COLLATE SQL_Latin1_General_CP1_CI_AS) AS TxnDescription, res.TxnStatus, '
			SET @sz_SelectOB = @sz_SelectOB + '(stat.Remarks COLLATE SQL_Latin1_General_CP1_CI_AS) AS [Status], '
			SET @sz_SelectOB = @sz_SelectOB + '(HostDesc COLLATE SQL_Latin1_General_CP1_CI_AS) AS Channel, '
			
			--ADD START DANIEL 20181128 [Add conditional statement. If not fulfill, remove select customer info statement]	
			--SET @sz_SelectOB = @sz_SelectOB + 'CustEmail, CustName, CustPhone, Host.PymtMethod AS ChannelType, '
			IF(@PMode != 2)
				BEGIN
					SET @sz_SelectOB = @sz_SelectOB + 'CustEmail, CustName, CustPhone, '
				END
			SET @sz_SelectOB = @sz_SelectOB + 'Host.PymtMethod AS ChannelType,'
			--ADD E N D DANIEL 20181128 [Add conditional statement. If not fulfill, remove select customer info statement]	

			SET @sz_SelectOB = @sz_SelectOB + ' '' '' AS AuthCode, res.CardPAN AS CreditCardNo, '' '' AS ECI '			
			SET @sz_SelectOB = @sz_SelectOB + 'FROM OB.dbo.TB_PayRes res (NOLOCK) '
			--ADD START DANIEl 20190103 [Left join Pg..Merchant]
			SET @sz_SelectOB = @sz_SelectOB + 'LEFT JOIN PG..Merchant merc (NOLOCK) ON (res.MerchantID COLLATE SQL_Latin1_General_CP1_CI_AS) = (merc.MerchantID COLLATE SQL_Latin1_General_CP1_CI_AS) '
			--ADD E N D DANIEl 20190103 [Left join Pg..Merchant]
			SET @sz_SelectOB = @sz_SelectOB + 'LEFT JOIN OB.dbo.CL_TxnStatus stat (NOLOCK) ON res.TxnStatus = stat.TxnStatus '
			SET @sz_SelectOB = @sz_SelectOB + 'LEFT JOIN OB.dbo.Host (NOLOCK) ON Host.HostID = res.HostID '
			SET @sz_SelectOB = @sz_SelectOB + 'LEFT JOIN OB.dbo.TB_PayReq_Detail reqd (NOLOCK) ON reqd.MerchantID = res.MerchantID COLLATE SQL_Latin1_General_CP1_CI_AS '
			SET @sz_SelectOB = @sz_SelectOB + 'AND reqd.MerchantTxnID = res.MerchantTxnID COLLATE SQL_Latin1_General_CP1_CI_AS '
			SET @sz_SelectOB = @sz_SelectOB + 'WHERE res.DateCreated BETWEEN ''' + CAST(@PSince AS VARCHAR(30)) + ''' AND ''' + CAST((@PTo+1) AS VARCHAR(30)) + ''' '
			SET @sz_SelectOB = @sz_SelectOB + 'AND res.TxnStatus IN (' + @POBOtherStatus + ') '

			IF (@PMerchantID <> '')
				SET @sz_SelectOB = @sz_SelectOB + 'AND res.MerchantID IN (' + @MerchantIDList + ') '

			IF (@POrderID <> '')
				SET @sz_SelectOB = @sz_SelectOB + 'AND res.OrderNumber = ''' + @POrderID + ''' '

			IF (@PCustEmail <> '')
				SET @sz_SelectOB = @sz_SelectOB + 'AND reqd.CustEmail = ''' + @PCustEmail + ''' '

			IF (@PHostType <> '')
				SET @sz_SelectOB = @sz_SelectOB + 'AND Host.PymtMethod IN (' + @PHostType + ') '

			IF (@PHostName <> '')
				SET @sz_SelectOB = @sz_SelectOB + 'AND Host.HostName LIKE ''%' + @PHostName + '%'' '

			IF (@PCurrencyCode <> '')
				SET @sz_SelectOB = @sz_SelectOB + 'AND res.CurrencyCode = ''' + @PCurrencyCode + ''' '

			IF (@PPaymentID <> '')
				SET @sz_SelectOB = @sz_SelectOB + 'AND res.MerchantTxnID = ''' + @PPaymentID + ''' '

			IF (@PGatewayTxnID <> '')
				SET @sz_SelectOB = @sz_SelectOB + 'AND res.GatewayTxnID = ''' + @PGatewayTxnID + ''' '

			--SET @sz_SelectOB = @sz_SelectOB + 'AND (HostDesc <> ''' + 'Alliance Bank' + ''' ' + 'AND HostDesc <> ''' + 'RHB'  + ''' ' +  ')'

		END


	-- PG
	IF (CHARINDEX('CC', @PHostType, 0) > 0)
		BEGIN
			SET @sz_SelectCC = 'SELECT txn.DateCreated, (txn.MerchantID COLLATE SQL_Latin1_General_CP1_CI_AS) AS MerchantID, '
			--ADD START DANIEL 20190103 [Add Pg..Merchant Desc in select statement]
			SET @sz_SelectCC = @sz_SelectCC + '(merc.[Desc] COLLATE SQL_Latin1_General_CP1_CI_AS) AS TradingName, '
			--ADD E N D DANIEL 20190103 [Add Pg..Merchant Desc in select statement]
			SET @sz_SelectCC = @sz_SelectCC + '(txn.OrderNumber COLLATE SQL_Latin1_General_CP1_CI_AS) AS OrderNumber, '
			SET @sz_SelectCC = @sz_SelectCC + '(txn.MerchantTxnID COLLATE SQL_Latin1_General_CP1_CI_AS) AS PaymentID, '
			SET @sz_SelectCC = @sz_SelectCC + '(txn.GatewayTxnID COLLATE SQL_Latin1_General_CP1_CI_AS) AS GatewayTxnID, '
			SET @sz_SelectCC = @sz_SelectCC + ''' ''  AS BankRefNo, '
			SET @sz_SelectCC = @sz_SelectCC + '(txn.CurrencyCode COLLATE SQL_Latin1_General_CP1_CI_AS) AS CurrencyCode, '
			SET @sz_SelectCC = @sz_SelectCC + 'txn.TxnAmt, (TxnDescription COLLATE SQL_Latin1_General_CP1_CI_AS) AS TxnDescription, txn.TxnStatus, '
			SET @sz_SelectCC = @sz_SelectCC + '(stat.Remarks COLLATE SQL_Latin1_General_CP1_CI_AS) AS [Status], '
			SET @sz_SelectCC = @sz_SelectCC + '(HostDesc COLLATE SQL_Latin1_General_CP1_CI_AS) AS Channel, '
			
			--ADD START DANIEL 20181128 [Add conditional statement. If not fulfil, remove select customer info statement]
			--SET @sz_SelectCC = @sz_SelectCC + ''' '' AS CustEmail, '' '' AS CustName, '' '' AS CustPhone, ''CC'' AS ChannelType, '
			IF(@PMode != 2)
				BEGIN
					SET @sz_SelectCC = @sz_SelectCC + ''' '' AS CustEmail, '' '' AS CustName, '' '' AS CustPhone, '
				END
			SET @sz_SelectCC = @sz_SelectCC + '''CC'' AS ChannelType, '	
			--ADD END DANIEL 20181128 [Add conditional statement. If not fulfil, remove select customer info statement]	
			
			SET @sz_SelectCC = @sz_SelectCC + ''' '' AS AuthCode, '' '' AS CreditCardNo, '' '' AS ECI '	
			SET @sz_SelectCC = @sz_SelectCC + 'FROM PG.dbo.TB_PayTxnRef txn (NOLOCK) '
			--ADD START DANIEl 20190103 [Left join Pg..Merchant]
			SET @sz_SelectCC = @sz_SelectCC + 'LEFT JOIN PG..Merchant merc (NOLOCK) ON (txn.MerchantID COLLATE SQL_Latin1_General_CP1_CI_AS) = (merc.MerchantID COLLATE SQL_Latin1_General_CP1_CI_AS) '
			--ADD E N D DANIEl 20190103 [Left join Pg..Merchant]
			SET @sz_SelectCC = @sz_SelectCC + 'LEFT JOIN PG.dbo.CL_TxnStatus stat (NOLOCK) ON txn.TxnStatus = stat.TxnStatus '
			SET @sz_SelectCC = @sz_SelectCC + 'LEFT JOIN PG.dbo.Host (NOLOCK) ON Host.HostID = txn.HostID '
			SET @sz_SelectCC = @sz_SelectCC + 'LEFT JOIN PG.dbo.TB_PayReq_Detail reqd (NOLOCK) ON reqd.MerchantID = txn.MerchantID COLLATE SQL_Latin1_General_CP1_CI_AS '
			SET @sz_SelectCC = @sz_SelectCC + 'AND reqd.MerchantTxnID = txn.MerchantTxnID COLLATE SQL_Latin1_General_CP1_CI_AS '
			SET @sz_SelectCC = @sz_SelectCC + 'WHERE txn.DateCreated BETWEEN ''' + CAST(@PSince AS VARCHAR(30)) + ''' AND ''' + CAST((@PTo+1) AS VARCHAR(30)) + ''' '
			
			--ADD START DANIEL 20190111 [Only get records in which GatewayTxnID is not exist/same in TB_PayRes]
			SET @sz_SelectCC = @sz_SelectCC + 'AND NOT EXISTS (SELECT * FROM PG..TB_PayRes res where res.MerchantTxnID = txn.MerchantTxnID) '
			--ADD E N D DANIEL 20190111 [Only get records in which GatewayTxnID is not exist/same in TB_PayRes]

			SET @sz_SelectCC = @sz_SelectCC + 'AND txn.TxnStatus IN (' + @PPGOtherStatus + ') '
			--SET @sz_SelectCC = @sz_SelectCC + 'AND txn.PymtMethod = ''CC'' '

			IF (@PMerchantID <> '')
				SET @sz_SelectCC = @sz_SelectCC + 'AND txn.MerchantID IN (' + @MerchantIDList + ') '

			IF (@POrderID <> '')
				SET @sz_SelectCC = @sz_SelectCC + 'AND txn.OrderNumber = ''' + @POrderID + ''' '

			IF (@PCustEmail <> '')
				SET @sz_SelectCC = @sz_SelectCC + 'AND reqd.CustEmail = ''' + @PCustEmail + ''' '

			IF (@PHostName <> '')
				SET @sz_SelectCC = @sz_SelectCC + 'AND Host.HostName LIKE ''%' + @PHostName + '%'' '

			IF (@PCurrencyCode <> '')
				SET @sz_SelectCC = @sz_SelectCC + 'AND txn.CurrencyCode = ''' + @PCurrencyCode + ''' '

			IF (@PPaymentID <> '')
				SET @sz_SelectCC = @sz_SelectCC + 'AND txn.MerchantTxnID = ''' + @PPaymentID + ''' '

			IF (@PGatewayTxnID <> '')
				SET @sz_SelectCC = @sz_SelectCC + 'AND txn.GatewayTxnID = ''' + @PGatewayTxnID + ''' '

				SET @sz_SelectCC = @sz_SelectCC + 'AND (HostDesc <> ''' + 'Alliance Bank' + ''' ' + 'AND HostDesc <> ''' + 'RHB'  + ''' ' +  ')'

			SET @sz_SelectCC = @sz_SelectCC + 'UNION '
	
			SET @sz_SelectCC = @sz_SelectCC + 'SELECT res.DateCreated, (res.MerchantID COLLATE SQL_Latin1_General_CP1_CI_AS) AS MerchantID, '
			--ADD START DANIEL 20190103 [Add Pg..Merchant Desc in select statement]
			SET @sz_SelectCC = @sz_SelectCC + '(merc.[Desc] COLLATE SQL_Latin1_General_CP1_CI_AS) AS TradingName, '
			--ADD E N D DANIEL 20190103 [Add Pg..Merchant Desc in select statement]
			SET @sz_SelectCC = @sz_SelectCC + '(res.OrderNumber COLLATE SQL_Latin1_General_CP1_CI_AS) AS OrderNumber, '
			SET @sz_SelectCC = @sz_SelectCC + '(res.MerchantTxnID COLLATE SQL_Latin1_General_CP1_CI_AS) AS PaymentID, '
			SET @sz_SelectCC = @sz_SelectCC + '(res.GatewayTxnID COLLATE SQL_Latin1_General_CP1_CI_AS) AS GatewayTxnID, '
			SET @sz_SelectCC = @sz_SelectCC + ''' ''  AS BankRefNo, '
			SET @sz_SelectCC = @sz_SelectCC + '(res.CurrencyCode COLLATE SQL_Latin1_General_CP1_CI_AS) AS CurrencyCode, '
			SET @sz_SelectCC = @sz_SelectCC + 'res.TxnAmt, (TxnDescription COLLATE SQL_Latin1_General_CP1_CI_AS) AS TxnDescription, res.TxnStatus, '
			SET @sz_SelectCC = @sz_SelectCC + '(stat.Remarks COLLATE SQL_Latin1_General_CP1_CI_AS) AS [Status], '
			SET @sz_SelectCC = @sz_SelectCC + '(CASE WHEN LEFT(res.CardPAN, 1) = 4 THEN ''VISA'' '
			SET @sz_SelectCC = @sz_SelectCC + 'WHEN (LEFT(res.CardPAN, 1) = 5 OR LEFT(res.CardPAN, 1) = 2) THEN ''MASTER'' '
			SET @sz_SelectCC = @sz_SelectCC + 'WHEN (LEFT(res.CardPAN, 2) = 35) THEN ''JCB'' '
			SET @sz_SelectCC = @sz_SelectCC + 'WHEN (LEFT(res.CardPAN, 2) = 34 OR LEFT(res.CardPAN, 2) = 37) THEN ''AMEX''  '
			SET @sz_SelectCC = @sz_SelectCC + 'WHEN (LEFT(res.CardPAN, 2) = 36 OR LEFT(res.CardPAN, 2) = 38) THEN ''Diners'' '
			SET @sz_SelectCC = @sz_SelectCC + 'ELSE '''' '
			SET @sz_SelectCC = @sz_SelectCC + 'END) AS Channel, '

			--ADD START DANIEL 20181128 [Add conditional statement. If not fulfil, remove select customer info statement]	
			--SET @sz_SelectCC = @sz_SelectCC + ''' '' AS CustEmail, '' '' AS CustName, '' '' AS CustPhone, ''CC'' AS ChannelType, '
			IF(@PMode != 2)
				BEGIN
					SET @sz_SelectCC = @sz_SelectCC + ''' '' AS CustEmail, '' '' AS CustName, '' '' AS CustPhone, '
				END
			SET @sz_SelectCC = @sz_SelectCC + '''CC'' AS ChannelType, '	

			--ADD E N D DANIEL 20181128 [Add conditional statement. If not fulfil, remove select customer info statement]	

			SET @sz_SelectCC = @sz_SelectCC + 'res.AuthCode, (res.CardPAN COLLATE SQL_Latin1_General_CP1_CI_AS) AS CreditCardNo, ECI '	
			SET @sz_SelectCC = @sz_SelectCC + 'FROM PG.dbo.TB_PayRes res (NOLOCK) '
			--ADD START DANIEl 20190103 [Left join Pg..Merchant]
			SET @sz_SelectCC = @sz_SelectCC + 'LEFT JOIN PG..Merchant merc (NOLOCK) ON (res.MerchantID COLLATE SQL_Latin1_General_CP1_CI_AS) = (merc.MerchantID COLLATE SQL_Latin1_General_CP1_CI_AS) '
			--ADD E N D DANIEl 20190103 [Left join Pg..Merchant]
			SET @sz_SelectCC = @sz_SelectCC + 'LEFT JOIN PG.dbo.CL_TxnStatus stat (NOLOCK) ON res.TxnStatus = stat.TxnStatus '
			SET @sz_SelectCC = @sz_SelectCC + 'LEFT JOIN PG.dbo.Host (NOLOCK) ON Host.HostID = res.HostID '
			SET @sz_SelectCC = @sz_SelectCC + 'LEFT JOIN PG.dbo.TB_PayReq_Detail reqd (NOLOCK) ON reqd.MerchantID = res.MerchantID COLLATE SQL_Latin1_General_CP1_CI_AS '
			SET @sz_SelectCC = @sz_SelectCC + 'AND reqd.MerchantTxnID = res.MerchantTxnID COLLATE SQL_Latin1_General_CP1_CI_AS '
			SET @sz_SelectCC = @sz_SelectCC + 'WHERE res.DateCreated BETWEEN ''' + CAST(@PSince AS VARCHAR(30)) + ''' AND ''' + CAST((@PTo+1) AS VARCHAR(30)) + ''' '
			SET @sz_SelectCC = @sz_SelectCC + 'AND res.TxnStatus IN (' + @PPGOtherStatus + ') '

			IF (@PMerchantID <> '')
				SET @sz_SelectCC = @sz_SelectCC + 'AND res.MerchantID IN (' + @MerchantIDList + ') '

			IF (@POrderID <> '')
				SET @sz_SelectCC = @sz_SelectCC + 'AND res.OrderNumber = ''' + @POrderID + ''' '

			IF (@PCustEmail <> '')
				SET @sz_SelectCC = @sz_SelectCC + 'AND reqd.CustEmail = ''' + @PCustEmail + ''' '

			IF (@PHostName <> '')
				SET @sz_SelectCC = @sz_SelectCC + 'AND Host.HostName LIKE ''%' + @PHostName + '%'' '

			IF (@PCurrencyCode <> '')
				SET @sz_SelectCC = @sz_SelectCC + 'AND res.CurrencyCode = ''' + @PCurrencyCode + ''' '

			IF (@PPaymentID <> '')
				SET @sz_SelectCC = @sz_SelectCC + 'AND res.MerchantTxnID = ''' + @PPaymentID + ''' '

			IF (@PGatewayTxnID <> '')
					SET @sz_SelectCC = @sz_SelectCC + 'AND res.GatewayTxnID = ''' + @PGatewayTxnID + ''' '

			--SET @sz_SelectCC = @sz_SelectCC + 'AND (HostDesc <> ''' + 'Alliance Bank' + ''' ' + 'AND HostDesc <> ''' + 'RHB'  + ''' ' +  ')'
		END


	IF (((CHARINDEX('OB', @PHostType, 0) > 0) OR (CHARINDEX('WA', @PHostType, 0) > 0) 
	OR (CHARINDEX('OTC', @PHostType, 0) > 0) OR (CHARINDEX('MO', @PHostType, 0) > 0)) 
	AND (CHARINDEX('CC', @PHostType, 0) > 0))
		BEGIN
			SET @sz_Select = @sz_SelectOB + ' UNION ' + @sz_SelectCC --remove duplicate
			print (@sz_SelectOB);
			print (@sz_SelectCC);
		END
	ELSE IF ((CHARINDEX('OB', @PHostType, 0) > 0) OR (CHARINDEX('WA', @PHostType, 0) > 0) 
	OR (CHARINDEX('OTC', @PHostType, 0) > 0) OR (CHARINDEX('MO', @PHostType, 0) > 0)) 
		BEGIN
			SET @sz_Select = @sz_SelectOB
		END
	ELSE
		BEGIN
			SET @sz_Select = @sz_SelectCC
		END
	 
	SET @sz_Select = @sz_Select + 'ORDER BY DateCreated DESC'

	EXEC (@sz_Select);
	print (@sz_Select);
END
GO


