USE [PGAdmin]
GO
/****** Object:  StoredProcedure [dbo].[spGetTBTxnRateSummaryByMerchantIDDate]    Script Date: 6/10/2020 12:15:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetTBTxnRateSummaryByMerchantIDDate] 
	@PMerchantID AS CHAR(30),
	@PSince AS DATETIME,
	@PTo AS DATETIME,
	@PCurrency AS CHAR(3)
AS
BEGIN
	SELECT COUNT(*) AS Qty, SUM(TxnAmt) AS TxnAmt, sum(NetFee) AS NetFee, DATEPART(HOUR, DateCreated) AS HourCreated INTO #TempTxnRateSummary
	FROM OB..TB_TxnRate (NOLOCK)
	WHERE DateCreated BETWEEN @PSince AND @PTo
	AND ((@PMerchantID = NULL OR @PMerchantID = ' ') OR (@PMerchantID <> ' ' AND MerchantID = @PMerchantID))
	AND TxnStatus = 0
	AND CurrencyCode = @PCurrency
	GROUP BY DATEPART(HOUR, DateCreated)
	UNION ALL
	SELECT COUNT(*) AS Qty, SUM(TxnAmt) AS TxnAmt, sum(NetFee) AS NetFee, DATEPART(HOUR, DateCreated) AS HourCreated 
	FROM PG..TB_TxnRate (NOLOCK)
	WHERE DateCreated BETWEEN @PSince AND @PTo
	AND ((@PMerchantID = NULL OR @PMerchantID = ' ') OR (@PMerchantID <> ' ' AND MerchantID = @PMerchantID))
	AND TxnStatus = 0
	AND CurrencyCode = @PCurrency
	GROUP BY DATEPART(HOUR, DateCreated)

	SELECT SUM(Qty) AS Qty, SUM(TxnAmt) AS TxnAmt, sum(NetFee) AS NetFee, HourCreated
	FROM #TempTxnRateSummary
	GROUP BY HourCreated

	DROP TABLE #TempTxnRateSummary
END
GO
