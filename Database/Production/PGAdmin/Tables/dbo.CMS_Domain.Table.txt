USE [PGAdmin]
GO
/****** Object:  Table [dbo].[CMS_Domain]    Script Date: 6/10/2020 12:15:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CMS_Domain](
	[DomainID] [int] IDENTITY(1,1) NOT NULL,
	[DomainName] [nvarchar](50) NOT NULL,
	[DomainShortName] [nvarchar](30) NOT NULL,
	[DomainDesc] [nvarchar](100) NULL,
	[UIDLen] [int] NOT NULL,
	[PwdLen] [int] NOT NULL,
	[ComplexPwd] [int] NOT NULL,
	[MultipleLogin] [int] NOT NULL,
	[MaxFailedAttempt] [int] NOT NULL,
	[PwdAge] [int] NOT NULL,
	[PwdHist] [int] NOT NULL,
	[UniqueUser] [int] NOT NULL,
	[IdleLoginAge] [int] NOT NULL,
	[Flag1] [int] NULL,
	[Flag2] [int] NULL,
	[Flag3] [int] NULL,
	[AccumulativeFailedAttempts] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateModified] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](50) NULL,
	[EditLockBy] [nvarchar](50) NULL,
	[EditLockTime] [datetime] NULL,
	[Remarks] [nvarchar](200) NULL,
 CONSTRAINT [PK__CMS_Domain__5CD6CB2B] PRIMARY KEY CLUSTERED 
(
	[DomainID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Domain_DID_DSName]    Script Date: 6/10/2020 12:15:07 PM ******/
CREATE NONCLUSTERED INDEX [IX_Domain_DID_DSName] ON [dbo].[CMS_Domain]
(
	[DomainID] ASC,
	[DomainShortName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CMS_Domain] ADD  CONSTRAINT [DF__CMS_Domai__UIDLe__5DCAEF64]  DEFAULT ((0)) FOR [UIDLen]
GO
ALTER TABLE [dbo].[CMS_Domain] ADD  CONSTRAINT [DF__CMS_Domai__PwdLe__5EBF139D]  DEFAULT ((7)) FOR [PwdLen]
GO
ALTER TABLE [dbo].[CMS_Domain] ADD  CONSTRAINT [DF__CMS_Domai__Compl__5FB337D6]  DEFAULT ((0)) FOR [ComplexPwd]
GO
ALTER TABLE [dbo].[CMS_Domain] ADD  CONSTRAINT [DF__CMS_Domai__Multi__60A75C0F]  DEFAULT ((1)) FOR [MultipleLogin]
GO
ALTER TABLE [dbo].[CMS_Domain] ADD  CONSTRAINT [DF__CMS_Domai__MaxFa__619B8048]  DEFAULT ((3)) FOR [MaxFailedAttempt]
GO
ALTER TABLE [dbo].[CMS_Domain] ADD  CONSTRAINT [DF__CMS_Domai__PwdAg__628FA481]  DEFAULT ((0)) FOR [PwdAge]
GO
ALTER TABLE [dbo].[CMS_Domain] ADD  CONSTRAINT [DF__CMS_Domai__PwdHi__6383C8BA]  DEFAULT ((5)) FOR [PwdHist]
GO
ALTER TABLE [dbo].[CMS_Domain] ADD  CONSTRAINT [DF__CMS_Domai__Uniqu__6477ECF3]  DEFAULT ((1)) FOR [UniqueUser]
GO
ALTER TABLE [dbo].[CMS_Domain] ADD  CONSTRAINT [DF__CMS_Domai__IdleL__656C112C]  DEFAULT ((0)) FOR [IdleLoginAge]
GO
ALTER TABLE [dbo].[CMS_Domain] ADD  CONSTRAINT [DF_CMS_Domain_Flag1]  DEFAULT ((0)) FOR [Flag1]
GO
ALTER TABLE [dbo].[CMS_Domain] ADD  CONSTRAINT [DF_CMS_Domain_Flag2]  DEFAULT ((0)) FOR [Flag2]
GO
ALTER TABLE [dbo].[CMS_Domain] ADD  CONSTRAINT [DF_CMS_Domain_Flag3]  DEFAULT ((0)) FOR [Flag3]
GO
ALTER TABLE [dbo].[CMS_Domain] ADD  CONSTRAINT [DF_CMS_Domain_AccumulativeFailedAttempts]  DEFAULT ((0)) FOR [AccumulativeFailedAttempts]
GO
ALTER TABLE [dbo].[CMS_Domain] ADD  CONSTRAINT [DF__CMS_Domai__Statu__66603565]  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[CMS_Domain] ADD  CONSTRAINT [DF__CMS_Domai__DateC__6754599E]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [dbo].[CMS_Domain] ADD  CONSTRAINT [DF__CMS_Domai__DateM__68487DD7]  DEFAULT (getdate()) FOR [DateModified]
GO
