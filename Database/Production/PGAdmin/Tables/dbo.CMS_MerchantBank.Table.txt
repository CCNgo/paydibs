USE [PGAdmin]
GO
/****** Object:  Table [dbo].[CMS_MerchantBank]    Script Date: 6/10/2020 12:15:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CMS_MerchantBank](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DomainID] [int] NOT NULL,
	[Alias] [nvarchar](50) NULL,
	[BankName] [nvarchar](50) NULL,
	[BankAccHolder] [nvarchar](50) NULL,
	[BankAccNo] [nvarchar](30) NULL,
	[SWIFTCode] [nvarchar](20) NULL,
	[BankAddress] [nvarchar](500) NULL,
	[BankCountry] [nvarchar](50) NULL,
	[Currency] [nvarchar](3) NULL,
	[SettlementRoutine] [int] NOT NULL,
	[MinimumWithrawal] [decimal](18, 2) NOT NULL,
	[NextSettlementDate] [datetime] NULL,
	[LastSettlementDate] [datetime] NULL,
	[LastModifiedDate] [datetime] NULL,
	[LastModifiedBy] [nvarchar](50) NULL,
	[Enable] [bit] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_CMS_MerchantBank] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CMS_MerchantBank] ADD  CONSTRAINT [DF_CMS_MerchantBank_SettlementRoutine]  DEFAULT ((0)) FOR [SettlementRoutine]
GO
ALTER TABLE [dbo].[CMS_MerchantBank] ADD  CONSTRAINT [DF_CMS_MerchantBank_MinimumWithrawal]  DEFAULT ((0.00)) FOR [MinimumWithrawal]
GO
