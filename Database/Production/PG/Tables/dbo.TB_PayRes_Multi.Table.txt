USE [PG]
GO
/****** Object:  Table [dbo].[TB_PayRes_Multi]    Script Date: 6/10/2020 12:07:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TB_PayRes_Multi](
	[PKID] [bigint] IDENTITY(1,1) NOT NULL,
	[PayResID] [bigint] NOT NULL,
	[TxnType] [varchar](7) NOT NULL,
	[MerchantID] [varchar](30) NOT NULL,
	[MerchantTxnID] [varchar](30) NOT NULL,
	[TxnAmount] [decimal](18, 2) NOT NULL,
	[TxnStatus] [int] NOT NULL,
	[BankRefNo] [varchar](30) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[DateCompleted] [datetime] NULL,
 CONSTRAINT [PK_TB_PayRes_Multi] PRIMARY KEY CLUSTERED 
(
	[PKID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_TB_PayRes_Multi_MerchantID]    Script Date: 6/10/2020 12:07:37 PM ******/
CREATE NONCLUSTERED INDEX [IX_TB_PayRes_Multi_MerchantID] ON [dbo].[TB_PayRes_Multi]
(
	[MerchantID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_TB_PayRes_Multi_MerchantTxnID]    Script Date: 6/10/2020 12:07:37 PM ******/
CREATE NONCLUSTERED INDEX [IX_TB_PayRes_Multi_MerchantTxnID] ON [dbo].[TB_PayRes_Multi]
(
	[MerchantTxnID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_TB_PayRes_Multi_PayResID]    Script Date: 6/10/2020 12:07:37 PM ******/
CREATE NONCLUSTERED INDEX [IX_TB_PayRes_Multi_PayResID] ON [dbo].[TB_PayRes_Multi]
(
	[PayResID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_TB_PayRes_Multi_TxnStatus]    Script Date: 6/10/2020 12:07:37 PM ******/
CREATE NONCLUSTERED INDEX [IX_TB_PayRes_Multi_TxnStatus] ON [dbo].[TB_PayRes_Multi]
(
	[TxnStatus] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TB_PayRes_Multi] ADD  CONSTRAINT [DF_TB_PayRes_Multi_TxnAmount]  DEFAULT ((0.00)) FOR [TxnAmount]
GO
