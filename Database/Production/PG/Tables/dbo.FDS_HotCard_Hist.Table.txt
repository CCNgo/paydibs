USE [PG]
GO
/****** Object:  Table [dbo].[FDS_HotCard_Hist]    Script Date: 6/10/2020 12:07:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FDS_HotCard_Hist](
	[PKID] [int] NOT NULL,
	[CardPANMin] [varchar](50) NOT NULL,
	[CardPANMax] [varchar](50) NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
	[FDSTxnStatus] [int] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateModified] [datetime] NOT NULL,
	[ModifiedBy] [varchar](32) NULL,
	[RecActive] [int] NOT NULL,
	[EditLockBy] [nvarchar](50) NULL,
	[EditLockTime] [datetime] NULL,
	[Remarks] [varchar](500) NULL
) ON [PRIMARY]
GO
