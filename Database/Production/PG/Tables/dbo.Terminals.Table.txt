USE [PG]
GO
/****** Object:  Table [dbo].[Terminals]    Script Date: 6/10/2020 12:07:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Terminals](
	[TerminalID] [int] IDENTITY(1,1) NOT NULL,
	[MerchantID] [varchar](30) NOT NULL,
	[Password] [varchar](300) NULL,
	[ReturnKey] [varchar](300) NULL,
	[SecretKey] [varchar](300) NULL,
	[SendKeyPath] [varchar](100) NULL,
	[ReturnKeyPath] [varchar](100) NULL,
	[InitVector] [varchar](16) NULL,
	[PayeeCode] [nvarchar](50) NULL,
	[MID] [varchar](30) NULL,
	[TID] [varchar](20) NULL,
	[AcquirerID] [nvarchar](100) NULL,
	[CurrencyCode] [char](3) NULL,
	[AirlineCode] [varchar](400) NULL,
	[HostID] [int] NOT NULL,
	[GroupID] [int] NULL,
	[HIID] [int] NULL,
	[Priority] [int] NOT NULL,
	[DateActivated] [datetime] NOT NULL,
	[DateDeactivated] [datetime] NOT NULL,
	[RecStatus] [int] NOT NULL,
	[FXCurrencyCode] [char](3) NULL,
	[MPVCO] [int] NULL,
	[MerchantHostTimeout] [int] NULL,
 CONSTRAINT [PK_Terminals] PRIMARY KEY CLUSTERED 
(
	[TerminalID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_TB_Terminals_MerchantID]    Script Date: 6/10/2020 12:07:37 PM ******/
CREATE NONCLUSTERED INDEX [IX_TB_Terminals_MerchantID] ON [dbo].[Terminals]
(
	[MerchantID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Terminals_HostID]    Script Date: 6/10/2020 12:07:37 PM ******/
CREATE NONCLUSTERED INDEX [IX_Terminals_HostID] ON [dbo].[Terminals]
(
	[HostID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Terminals_MID]    Script Date: 6/10/2020 12:07:37 PM ******/
CREATE NONCLUSTERED INDEX [IX_Terminals_MID] ON [dbo].[Terminals]
(
	[MID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Terminals_RecStatus]    Script Date: 6/10/2020 12:07:37 PM ******/
CREATE NONCLUSTERED INDEX [IX_Terminals_RecStatus] ON [dbo].[Terminals]
(
	[RecStatus] ASC
)
INCLUDE ( 	[MerchantID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Terminals] ADD  CONSTRAINT [DF_Terminals_Password]  DEFAULT ('') FOR [Password]
GO
ALTER TABLE [dbo].[Terminals] ADD  CONSTRAINT [DF_Terminals_ReturnKey]  DEFAULT ('') FOR [ReturnKey]
GO
ALTER TABLE [dbo].[Terminals] ADD  CONSTRAINT [DF_Terminals_SecretKey]  DEFAULT ('') FOR [SecretKey]
GO
ALTER TABLE [dbo].[Terminals] ADD  CONSTRAINT [DF_Terminals_SendKeyPath]  DEFAULT ('') FOR [SendKeyPath]
GO
ALTER TABLE [dbo].[Terminals] ADD  CONSTRAINT [DF_Terminals_ReturnKeyPath]  DEFAULT ('') FOR [ReturnKeyPath]
GO
ALTER TABLE [dbo].[Terminals] ADD  CONSTRAINT [DF_Terminals_InitVector]  DEFAULT ('') FOR [InitVector]
GO
ALTER TABLE [dbo].[Terminals] ADD  CONSTRAINT [DF_Terminals_PayeeCode]  DEFAULT ('') FOR [PayeeCode]
GO
ALTER TABLE [dbo].[Terminals] ADD  CONSTRAINT [DF_Terminals_MID]  DEFAULT ('') FOR [MID]
GO
ALTER TABLE [dbo].[Terminals] ADD  CONSTRAINT [DF_Terminals_TID]  DEFAULT ('') FOR [TID]
GO
ALTER TABLE [dbo].[Terminals] ADD  CONSTRAINT [DF_Terminals_AcquirerID]  DEFAULT ('') FOR [AcquirerID]
GO
ALTER TABLE [dbo].[Terminals] ADD  CONSTRAINT [DF_Terminals_CurrencyCode]  DEFAULT ('') FOR [CurrencyCode]
GO
ALTER TABLE [dbo].[Terminals] ADD  CONSTRAINT [DF_Terminals_AirlineCode]  DEFAULT ('') FOR [AirlineCode]
GO
ALTER TABLE [dbo].[Terminals] ADD  CONSTRAINT [DF_Terminals_GroupID]  DEFAULT ((0)) FOR [GroupID]
GO
ALTER TABLE [dbo].[Terminals] ADD  CONSTRAINT [DF_Terminals_InstFlag]  DEFAULT ((0)) FOR [HIID]
GO
ALTER TABLE [dbo].[Terminals] ADD  CONSTRAINT [DF_Terminals_DateActivated]  DEFAULT (getdate()) FOR [DateActivated]
GO
ALTER TABLE [dbo].[Terminals] ADD  CONSTRAINT [DF_Terminals_DateDeactivated]  DEFAULT ('2099-02-28 18:12:58.320') FOR [DateDeactivated]
GO
ALTER TABLE [dbo].[Terminals] ADD  CONSTRAINT [DF_Terminals_RecStatus]  DEFAULT ((1)) FOR [RecStatus]
GO
ALTER TABLE [dbo].[Terminals] ADD  CONSTRAINT [DF_Terminals_MPVCO]  DEFAULT ((0)) FOR [MPVCO]
GO
ALTER TABLE [dbo].[Terminals] ADD  CONSTRAINT [DF_Terminals_MerchantHostTimeout]  DEFAULT ((0)) FOR [MerchantHostTimeout]
GO
