USE [PG]
GO
/****** Object:  Table [dbo].[FDS_HotEmail]    Script Date: 6/10/2020 12:07:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FDS_HotEmail](
	[PKID] [int] IDENTITY(1,1) NOT NULL,
	[HotEmail] [varchar](60) NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
	[MerchantID] [varchar](30) NOT NULL,
	[RecActive] [int] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateModified] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](50) NULL,
	[EditLockBy] [nvarchar](50) NULL,
	[EditLockTime] [datetime] NULL,
	[Remarks] [nvarchar](500) NULL,
 CONSTRAINT [PK_FDS_HotEmail] PRIMARY KEY CLUSTERED 
(
	[PKID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_FDS_HotEmail_HotEmail_MerchantID]    Script Date: 6/10/2020 12:07:37 PM ******/
CREATE NONCLUSTERED INDEX [IX_FDS_HotEmail_HotEmail_MerchantID] ON [dbo].[FDS_HotEmail]
(
	[HotEmail] ASC,
	[MerchantID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FDS_HotEmail] ADD  CONSTRAINT [DF_FDS_HotEmail_StartDate]  DEFAULT (getdate()) FOR [StartDate]
GO
ALTER TABLE [dbo].[FDS_HotEmail] ADD  CONSTRAINT [DF_FDS_HotEmail_EndDate]  DEFAULT ('31 December 2099 23:59:59') FOR [EndDate]
GO
ALTER TABLE [dbo].[FDS_HotEmail] ADD  CONSTRAINT [DF_FDS_HotEmail_ServiceID]  DEFAULT ('') FOR [MerchantID]
GO
ALTER TABLE [dbo].[FDS_HotEmail] ADD  CONSTRAINT [DF_FDS_HotEmail_RecActive]  DEFAULT ((1)) FOR [RecActive]
GO
ALTER TABLE [dbo].[FDS_HotEmail] ADD  CONSTRAINT [DF_FDS_HotEmail_DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [dbo].[FDS_HotEmail] ADD  CONSTRAINT [DF_FDS_HotEmail_DateModifed]  DEFAULT (getdate()) FOR [DateModified]
GO
