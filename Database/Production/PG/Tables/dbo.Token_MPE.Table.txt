USE [PG]
GO
/****** Object:  Table [dbo].[Token_MPE]    Script Date: 6/10/2020 12:07:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Token_MPE](
	[PKID] [int] IDENTITY(1,1) NOT NULL,
	[MerchantID] [varchar](30) NOT NULL,
	[ConsumerUserID] [varchar](100) NOT NULL,
	[LongAccessToken] [varchar](60) NOT NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
 CONSTRAINT [PK_Token_MPE] PRIMARY KEY CLUSTERED 
(
	[PKID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Token_MPE_MerchantID_ConsumerUserID]    Script Date: 6/10/2020 12:07:37 PM ******/
CREATE NONCLUSTERED INDEX [IX_Token_MPE_MerchantID_ConsumerUserID] ON [dbo].[Token_MPE]
(
	[MerchantID] ASC,
	[ConsumerUserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
