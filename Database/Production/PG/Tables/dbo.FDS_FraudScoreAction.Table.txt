USE [PG]
GO
/****** Object:  Table [dbo].[FDS_FraudScoreAction]    Script Date: 6/10/2020 12:07:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FDS_FraudScoreAction](
	[PKID] [int] IDENTITY(1,1) NOT NULL,
	[MerchantID] [varchar](30) NOT NULL,
	[ScoreFrom] [int] NOT NULL,
	[ScoreTo] [int] NOT NULL,
	[ActionID] [int] NOT NULL,
 CONSTRAINT [PK_FDS_FraudScoreAction] PRIMARY KEY CLUSTERED 
(
	[PKID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_FDS_FraudScoreAction_MerchantID]    Script Date: 6/10/2020 12:07:37 PM ******/
CREATE NONCLUSTERED INDEX [IX_FDS_FraudScoreAction_MerchantID] ON [dbo].[FDS_FraudScoreAction]
(
	[MerchantID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
