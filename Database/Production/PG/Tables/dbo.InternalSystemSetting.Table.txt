USE [PG]
GO
/****** Object:  Table [dbo].[InternalSystemSetting]    Script Date: 6/10/2020 12:07:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InternalSystemSetting](
	[PKID] [int] NOT NULL,
	[SettingDesc] [varchar](50) NULL,
	[Value] [varchar](50) NULL,
	[LastUpdated] [datetime] NULL,
 CONSTRAINT [PK_InternalSystemSetting] PRIMARY KEY CLUSTERED 
(
	[PKID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[InternalSystemSetting] ADD  CONSTRAINT [DF_InternalSystemSetting_LastUpdated]  DEFAULT (getdate()) FOR [LastUpdated]
GO
