USE [PG]
GO
/****** Object:  Table [dbo].[CL_CardType]    Script Date: 6/10/2020 12:07:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CL_CardType](
	[CardPrefix] [varchar](2) NOT NULL,
	[CardType] [varchar](15) NOT NULL,
	[LogoPath] [varchar](50) NULL,
	[CardTypeCode] [varchar](2) NULL
) ON [PRIMARY]
GO
