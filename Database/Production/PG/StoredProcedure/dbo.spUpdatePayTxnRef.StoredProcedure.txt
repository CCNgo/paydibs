USE [PG]
GO
/****** Object:  StoredProcedure [dbo].[spUpdatePayTxnRef]    Script Date: 6/10/2020 12:07:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUpdatePayTxnRef]
(
	@i_TxnStatus As INT,
	@i_HostID As INT,
	@sz_IssuingBank As VARCHAR(30),
	@i_PKID As INT,
	@sz_GatewayTxnID As VARCHAR(30),
	@sz_MerchantTxnID As VARCHAR(30),
	@sz_MerchantID As VARCHAR(30),
	@sz_PymtMethod As VARCHAR(3)	
)
AS
	UPDATE TB_PayTxnRef SET TxnStatus = @i_TxnStatus, HostID = @i_HostID, HostName = @sz_IssuingBank, 
	TxnPKID = @i_PKID, GatewayTxnID = @sz_GatewayTxnID, DateModified = GETDATE()
	WHERE MerchantTxnID = @sz_MerchantTxnID AND MerchantID = @sz_MerchantID AND UPPER(PymtMethod) = UPPER(@sz_PymtMethod)
	
	IF @@ROWCOUNT > 0
		RETURN 0
	ELSE
		RETURN -1
GO
