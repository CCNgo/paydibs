USE [PG]
GO
/****** Object:  StoredProcedure [dbo].[spGetHostTerminalByMerchantID]    Script Date: 6/10/2020 12:07:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetHostTerminalByMerchantID]--'POP','CC'
(    
	@sz_MerchantID varchar(30),
	@sz_PaymentMethod varchar(3)
)
AS 

BEGIN 

IF(@sz_PaymentMethod = 'OB') 
BEGIN
	SELECT DISTINCT A.HostID, B.HostName
	  FROM [OB].[dbo].[Terminals] A WITH (NOLOCK)
	  LEFT JOIN [OB].[dbo].[Host] B WITH (NOLOCK) ON B.HostID = A.HostID 
	WHERE A.MerchantID = @sz_MerchantID
END
ELSE IF(@sz_PaymentMethod = 'CC') 
BEGIN
	SELECT DISTINCT A.HostID, B.HostName
	  FROM [PG].[dbo].[Terminals] A WITH (NOLOCK)
	  LEFT JOIN [PG].[dbo].[Host] B WITH (NOLOCK) ON B.HostID = A.HostID 
	WHERE A.MerchantID = @sz_MerchantID
END
END
GO
