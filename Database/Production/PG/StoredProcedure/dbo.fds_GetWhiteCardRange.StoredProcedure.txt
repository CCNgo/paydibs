USE [PG]
GO
/****** Object:  StoredProcedure [dbo].[fds_GetWhiteCardRange]    Script Date: 6/10/2020 12:07:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[fds_GetWhiteCardRange]
	@i_PKID int,
	@sz_CardPrefix1 varchar (6),
	@sz_CardPrefix2 varchar (6),
	@i_MaxRow int,
	@sz_MerchantID varchar(30) --Added by OoiMei 30 Aug 2013

AS

	IF (@sz_MerchantID <> '')	--Specific merchant
	BEGIN
		IF (@sz_CardPrefix1 <> '' AND @sz_CardPrefix2 <> '')
			SELECT	PKID, CardPANMin, CardPANMax, MerchantID, StartDate, EndDate, CONVERT (varchar, DateCreated, 20) DateCreated, 
				CONVERT (varchar, DateModified, 20) DateModified, ModifiedBy, RecActive, Remarks
			FROM [dbo].[FDS_WhiteCard]
			WHERE CardPANMin = @sz_CardPrefix1 AND CardPANMax = @sz_CardPrefix2 AND RecActive = 1 AND MerchantID = @sz_MerchantID

		ELSE IF (@sz_CardPrefix1 <> '' AND @sz_CardPrefix2 = '')
			SELECT	PKID, CardPANMin, CardPANMax, MerchantID, StartDate, EndDate, CONVERT (varchar, DateCreated, 20) DateCreated, 
				CONVERT (varchar, DateModified, 20) DateModified, ModifiedBy, RecActive, Remarks
			FROM [dbo].[FDS_WhiteCard]
			WHERE CardPANMin = @sz_CardPrefix1 AND RecActive = 1 AND MerchantID = @sz_MerchantID

		ELSE IF (@i_PKID > 0)
			SELECT	PKID, CardPANMin, CardPANMax, MerchantID, StartDate, EndDate, CONVERT (varchar, DateCreated, 20) DateCreated, 
				CONVERT (varchar, DateModified, 20) DateModified, ModifiedBy, RecActive, Remarks
			FROM [dbo].[FDS_WhiteCard]
			WHERE PKID = @i_PKID AND RecActive = 1 AND MerchantID = @sz_MerchantID

		ELSE
		BEGIN
			SET ROWCOUNT @i_MaxRow
			SELECT	PKID, CardPANMin, CardPANMax, MerchantID, StartDate, EndDate, CONVERT (varchar, DateCreated, 20) DateCreated, 
				CONVERT (varchar, DateModified, 20) DateModified, ModifiedBy, RecActive, Remarks
			FROM [dbo].[FDS_WhiteCard]
			WHERE LEN (CardPANMin) = 6 AND LEN (CardPANMax) = 6 AND RecActive = 1 AND MerchantID = @sz_MerchantID
			ORDER BY PKID DESC
			SET ROWCOUNT 0
		END
	END
	ELSE
	BEGIN	--all
		IF (@sz_CardPrefix1 <> '' AND @sz_CardPrefix2 <> '')
			SELECT	PKID, CardPANMin, CardPANMax, MerchantID, StartDate, EndDate, CONVERT (varchar, DateCreated, 20) DateCreated, 
				CONVERT (varchar, DateModified, 20) DateModified, ModifiedBy, RecActive, Remarks
			FROM [dbo].[FDS_WhiteCard]
			WHERE CardPANMin = @sz_CardPrefix1 AND CardPANMax = @sz_CardPrefix2 AND RecActive = 1

		ELSE IF (@sz_CardPrefix1 <> '' AND @sz_CardPrefix2 = '')
			SELECT	PKID, CardPANMin, CardPANMax, MerchantID, StartDate, EndDate, CONVERT (varchar, DateCreated, 20) DateCreated, 
				CONVERT (varchar, DateModified, 20) DateModified, ModifiedBy, RecActive, Remarks
			FROM [dbo].[FDS_WhiteCard]
			WHERE CardPANMin = @sz_CardPrefix1 AND RecActive = 1

		ELSE IF (@i_PKID > 0)
			SELECT	PKID, CardPANMin, CardPANMax, MerchantID, StartDate, EndDate, CONVERT (varchar, DateCreated, 20) DateCreated, 
				CONVERT (varchar, DateModified, 20) DateModified, ModifiedBy, RecActive, Remarks
			FROM [dbo].[FDS_WhiteCard]
			WHERE PKID = @i_PKID AND RecActive = 1 

		ELSE
		BEGIN
			SET ROWCOUNT @i_MaxRow
			SELECT	PKID, CardPANMin, CardPANMax, MerchantID, StartDate, EndDate, CONVERT (varchar, DateCreated, 20) DateCreated, 
				CONVERT (varchar, DateModified, 20) DateModified, ModifiedBy, RecActive, Remarks
			FROM [dbo].[FDS_WhiteCard]
			WHERE LEN (CardPANMin) = 6 AND LEN (CardPANMax) = 6  AND RecActive = 1
			ORDER BY PKID DESC
			SET ROWCOUNT 0
		END
	END
GO
