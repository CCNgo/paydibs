USE [PG]
GO
/****** Object:  StoredProcedure [dbo].[spGetTxnsFromParam]    Script Date: 6/10/2020 12:07:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetTxnsFromParam]
(                                                
	@sz_Param AS VARCHAR(100),
	@sz_MerchantID AS VARCHAR(30),
	@i_HostID AS INT
)
AS
	DECLARE @iRet Int
	DECLARE @szRetDesc VarChar(160)

	SET @iRet = -1

	-- ONLY return txns that have been existing in OB GW for NOT MORE THAN 2 HOURS, e.g. not more than
	-- 1 hour 30 minutes/90 minutes/5400 seconds because any bookings more than 2 hours will be automatically freed
	-- giving half an hour time for customer to complete payment and OB GW to confirm booking accordingly
	-- !!!!!!!!! NOTE: As per AirAsia's request on 23 June 2009, set it to 2 days(minus half an hour) (24+23.5)hours x 60minutes x 60 seconds for TESTING purposes only !!!!!
	SELECT  0 RetCode, '' RetDesc, B.MerchantID MerchantID, B.MerchantTxnID MerchantTxnID, B.TxnAmount TxnAmount,
			B.CurrencyCode CurrencyCode, C.GatewayTxnID GatewayTxnID, B.OrderDesc OrderDesc,
			SUBSTRING(CONVERT(VARCHAR, B.DateCreated, 120), 9, 2) + '/' + SUBSTRING(CONVERT(VARCHAR, B.DateCreated, 120), 6, 2) + '/' + SUBSTRING(CONVERT(VARCHAR, B.DateCreated, 120), 1, 4) + SUBSTRING(CONVERT(VARCHAR, B.DateCreated, 120), 11, 9) DateCreated
	FROM	TB_Pay_Params A WITH (NOLOCK)
			INNER JOIN TB_PayReq_Detail B WITH (NOLOCK) ON A.DetailID=B.PKID
			INNER JOIN TB_PayReq C WITH (NOLOCK) ON A.DetailID=C.DetailID
	WHERE	A.HostID = @i_HostID AND A.MerchantID = @sz_MerchantID AND UPPER(A.Param1) = UPPER(@sz_Param) AND A.ReqRes = 1 AND DateDiff(second, B.DateCreated, GETDATE()) < 5400 AND C.Action <> 4 --DateDiff(second, B.DateCreated, GETDATE()) < 171000
	
	IF @@ROWCOUNT > 0
	BEGIN 
		RETURN 1
	END
	ELSE
	BEGIN 
		SET @iRet = -1
		SET @szRetDesc = 'Transaction info not found'
		
		SELECT @iRet AS [RetCode], @szRetDesc AS [RetDesc]
		
		RETURN -1
	END
GO
