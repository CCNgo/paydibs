USE [PG]
GO
/****** Object:  StoredProcedure [dbo].[spGetTerminalCurrency]    Script Date: 6/10/2020 12:07:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetTerminalCurrency] 
(
	@sz_MerchantID AS VARCHAR(30)
)
AS

BEGIN
	SELECT DISTINCT(CurrencyCode) 
	FROM [PG]..[Terminals] WITH (NOLOCK) 
	WHERE MerchantID=@sz_MerchantID 
	AND RecStatus=1
	AND CurrencyCode<>''
	AND CurrencyCode IS NOT NULL
	UNION
	SELECT DISTINCT(CurrencyCode)COLLATE SQL_Latin1_General_CP1_CI_AS 
	FROM [OB]..[Terminals] WITH (NOLOCK) 
	WHERE MerchantID=@sz_MerchantID 
	AND RecStatus=1 
	AND CurrencyCode<>''
	AND CurrencyCode IS NOT NULL
END
GO
