USE [PG]
GO
/****** Object:  StoredProcedure [dbo].[sop_isTokenValid]    Script Date: 6/10/2020 12:07:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jawad Humayun
-- Create date: 30/5/2017
-- Description:	Checks the validity of SOP Token
-- =============================================
CREATE PROCEDURE [dbo].[sop_isTokenValid] 
	-- Add the parameters for the stored procedure here
	@sz_Token						VARCHAR(64),
	@sz_MerchantID					VARCHAR(3),
	@sz_CustIP						VARCHAR(20),
	@sz_TokenTimeout				INT=600
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   SELECT 
		DATEDIFF(second, SOPToken.DateCreated, GETDATE())-@sz_TokenTimeout As ExpiredSec,
		SOPToken.CardPan,
		SOPToken.CardExp,
		SOPToken.CVV,
		SOPToken.CardHolder
	FROM dbo.Token_SOP As SOPToken WITH (NOLOCK)
	WHERE SOPToken.Token = @sz_Token
	AND SOPToken.CustIP = @sz_CustIP
	AND SOPToken.MerchantID = @sz_MerchantID
	AND SOPToken.SOP_Status = 2;

	IF(@@ROWCOUNT <> 0)
		RETURN 0
	ELSE
		RETURN -1
END
GO
