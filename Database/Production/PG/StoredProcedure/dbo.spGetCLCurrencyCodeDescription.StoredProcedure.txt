USE [PG]
GO
/****** Object:  StoredProcedure [dbo].[spGetCLCurrencyCodeDescription]    Script Date: 6/10/2020 12:07:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetCLCurrencyCodeDescription]
AS

BEGIN
	select CurrencyCode, Description, (CurrencyCode + ' - ' + Description) CombinedDescription 
	FROM [CL_CurrencyCode] WITH (NOLOCK)
END
GO
