USE [PG]
GO
/****** Object:  StoredProcedure [dbo].[spGetTxnParam]    Script Date: 6/10/2020 12:07:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetTxnParam]
(
      @sz_MerchantID AS VARCHAR(30),
      @sz_MerchantTxnID AS VARCHAR(30),
	  @i_ReqRes AS INT
)
AS
	DECLARE @RetCode Int
	DECLARE @RetDesc VarChar(160)
	DECLARE @szParam1 varchar(100)
	DECLARE @szParam10 varchar(100)	--Added by OoiMei. 26 May 2016. Installment

    SELECT  TOP 1 @szParam1 = ISNULL(Param1, ''), @szParam10 = ISNULL(Param10, '')
    FROM  TB_Pay_Params WITH (NOLOCK)
	WHERE MerchantTxnID = @sz_MerchantTxnID AND MerchantID = @sz_MerchantID AND ReqRes = @i_ReqRes

    IF @@ROWCOUNT = 1 
    BEGIN
          SET @RetCode = 1
          SET @RetDesc = 'Transaction exists'  

          SELECT      @RetCode [RetCode], @RetDesc [RetDesc],
                      @szParam1 [Param1], @szParam10 [Param10]
          RETURN 1
    END
    ELSE
    BEGIN
		SET @RetCode = 0
		SET @RetDesc = 'Transaction not exists'

		SELECT @RetCode AS [RetCode], @RetDesc AS [RetDesc],
			   '' [Param1], '' [Param10]

		RETURN 0
    END
GO
