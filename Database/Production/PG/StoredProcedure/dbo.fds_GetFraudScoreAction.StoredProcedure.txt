USE [PG]
GO
/****** Object:  StoredProcedure [dbo].[fds_GetFraudScoreAction]    Script Date: 6/10/2020 12:07:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[fds_GetFraudScoreAction]
(
	@sz_MerchantID AS VARCHAR(30),
    @i_RespCode AS INT
)
AS
SELECT MerchantID, ScoreFrom, ScoreTo, ActionID 
FROM FDS_FraudScoreAction  with (nolock)
WHERE MerchantID=@sz_MerchantID AND @i_RespCode >= ScoreFrom AND  @i_RespCode <=ScoreTo 
GO
