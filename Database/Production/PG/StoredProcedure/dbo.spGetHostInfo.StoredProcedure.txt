USE [PG]
GO
/****** Object:  StoredProcedure [dbo].[spGetHostInfo]    Script Date: 6/10/2020 12:07:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetHostInfo]
(
	@sz_HostName AS VARCHAR(20)
)
AS
	IF @sz_HostName <> ''
	BEGIN
		SELECT	HostID, HostName, H.[Desc] HostDesc, ISNULL(HostCode,'') HostCode, SvcTypeID, (SELECT SvcType FROM CL_SvcType WHERE SvcTypeID=H.SvcTypeID) SvcType, SignBy, PaymentTemplate, SecondEntryTemplate, MesgTemplate, NeedReplyAcknowledgement, NeedRedirectOTP, Require2ndEntry, AllowQuery, AllowReversal,
		(SELECT CountryName FROM CL_Country WHERE PKID=H.CountryID) CountryName,
				H.ChannelID, H.[Timeout], C.[Timeout] ChannelTimeout,H.RecStatus IsActive, TxnStatusActionID, HostReplyMethod, OSPymtCode, GatewayTxnIDFormat, SettlePeriod, DateActivated, DateDeactivated, H.[Desc],
				C.IPAddress PayURL, C.CfgFile, C.ReturnURL, C.ReturnURL2, C.ReturnIPAddresses ReturnIPAddresses, C.Protocol, MerchantPassword, ReturnKey, HashMethod, QueryFlag, SecretKey, InitVector, LogRes,C.QueryIPAddress QueryURL, H.SelectedCurrExpnt,
				MPI
		FROM Host H WITH (NOLOCK)
			INNER JOIN Channel C ON C.ChannelID = H.ChannelID
		WHERE Upper(HostName) = Upper(@sz_HostName)
		AND GetDate() >= DateActivated AND GetDate() <= DateDeactivated --AND H.RecStatus = 1
	END
	ELSE
	BEGIN
		SELECT	HostID, HostName, H.[Desc] HostDesc, ISNULL(HostCode,'') HostCode, SvcTypeID, (SELECT SvcType FROM CL_SvcType WHERE SvcTypeID=H.SvcTypeID) SvcType, SignBy, PaymentTemplate, SecondEntryTemplate, MesgTemplate, NeedReplyAcknowledgement, NeedRedirectOTP, Require2ndEntry, AllowQuery, AllowReversal,
		(SELECT CountryName FROM CL_Country WHERE PKID=H.CountryID) CountryName,
				H.ChannelID, H.[Timeout], C.[Timeout] ChannelTimeout, H.RecStatus IsActive, TxnStatusActionID, HostReplyMethod, OSPymtCode, GatewayTxnIDFormat, SettlePeriod, DateActivated, DateDeactivated, H.[Desc],
				C.IPAddress PayURL, C.CfgFile, C.ReturnURL, C.ReturnURL2, C.ReturnIPAddresses ReturnIPAddresses, C.Protocol, MerchantPassword, ReturnKey, HashMethod, QueryFlag, SecretKey, InitVector, LogRes,C.QueryIPAddress QueryURL, H.SelectedCurrExpnt,
				MPI
		FROM Host H WITH (NOLOCK)
			INNER JOIN Channel C ON C.ChannelID = H.ChannelID
		WHERE GetDate() >= DateActivated AND GetDate() <= DateDeactivated --AND H.RecStatus = 1
	END

	IF @@ROWCOUNT > 0
		RETURN 0
	ELSE
		RETURN -1
GO
