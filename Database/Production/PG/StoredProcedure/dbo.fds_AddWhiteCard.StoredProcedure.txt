USE [PG]
GO
/****** Object:  StoredProcedure [dbo].[fds_AddWhiteCard]    Script Date: 6/10/2020 12:07:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[fds_AddWhiteCard]
	@sz_CardPAN varchar (50),
	@sz_DateCreated varchar (23),
	@sz_ModifiedBy nvarchar (50),
	@sz_Remarks varchar (500),
	@sz_MerchantID varchar(30)
AS
	DECLARE @iRetVal int,
			@iPKID int

	-- Added MerchantID by OoiMei 27 Aug 2013
	IF EXISTS (SELECT PKID FROM [dbo].[FDS_WhiteCard] 
				WHERE @sz_CardPAN BETWEEN CardPANMin AND CardPANMax AND RecActive = 1 AND MerchantID = @sz_MerchantID)
	BEGIN
		SET @iRetVal = 40001
		GOTO CleanUp
	END

	INSERT INTO [dbo].[FDS_WhiteCard] (CardPANMin, CardPANMax, MerchantID, DateCreated, DateModified, ModifiedBy, RecActive, Remarks)
	VALUES (@sz_CardPAN, @sz_CardPAN, @sz_MerchantID, @sz_DateCreated, @sz_DateCreated, @sz_ModifiedBy, 1, @sz_Remarks)

	IF (@@ROWCOUNT = 0)
	BEGIN
		SET @iRetVal = 40004
		GOTO CleanUp
	END
	ELSE 
	BEGIN
		SET @iRetVal = 0
		SET @iPKID = SCOPE_IDENTITY ()
	END

CleanUp:
	SELECT @iRetVal RetVal, @iPKID PKID
GO
