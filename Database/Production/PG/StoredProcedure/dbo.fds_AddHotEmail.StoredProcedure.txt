USE [PG]
GO
/****** Object:  StoredProcedure [dbo].[fds_AddHotEmail]    Script Date: 6/10/2020 12:07:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[fds_AddHotEmail]
	@sz_Email varchar (60),
	@sz_DateCreated varchar (23),
	@sz_ModifiedBy nvarchar (50),
	@sz_MerchantID varchar(30) 
AS
	DECLARE @iRetVal int,
			@iPKID int,
			@iLockHour int

	IF EXISTS (SELECT PKID FROM [dbo].[FDS_HotEmail] WHERE HotEmail = @sz_Email AND RecActive = 1 AND MerchantID = @sz_MerchantID)
	BEGIN
		SET @iRetVal = 40001
		GOTO CleanUp
	END

	--SELECT TOP 1 @iLockHour = LockWindow FROM dbo.FDS_ContinueRejectRules WHERE MerchantID = @sz_MerchantID

	INSERT INTO [dbo].[FDS_HotEmail] (HotEmail, MerchantID, DateCreated, DateModified, RecActive)
	VALUES (@sz_Email, @sz_MerchantID, @sz_DateCreated, @sz_DateCreated, 1)

	IF (@@ROWCOUNT = 0)
	BEGIN
		SET @iRetVal = 40004
		GOTO CleanUp
	END
	BEGIN
		SET @iRetVal = 0
		SET @iPKID = SCOPE_IDENTITY ()
	END

CleanUp:
	SELECT @iRetVal RetVal, @iPKID PKID
GO
