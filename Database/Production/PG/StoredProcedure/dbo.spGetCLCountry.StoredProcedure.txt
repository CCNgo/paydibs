USE [PG]
GO
/****** Object:  StoredProcedure [dbo].[spGetCLCountry]    Script Date: 6/10/2020 12:07:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetCLCountry] 
(
	@sz_CountryCode As Char(2),
	@i_CountryID AS integer,
	@sz_MerchantID As VarChar(30)=''	--Added 24 May 2016
)
AS
	IF (@sz_CountryCode <> '')
	BEGIN
		SELECT PKID CountryID, CountryName, CountryCodeA2, CountryCodeA3
		FROM CL_Country M WITH (NOLOCK)
		WHERE UPPER(CountryCodeA2) = UPPER(@sz_CountryCode) AND RecStatus = 1
	END ELSE IF (@i_CountryID > 0) BEGIN
		SELECT PKID CountryID, CountryName, CountryCodeA2, CountryCodeA3
		FROM CL_Country M WITH (NOLOCK)
		WHERE PKID = @i_CountryID AND RecStatus = 1
	END ELSE IF (@sz_MerchantID <> '') BEGIN	--Added 24 May 2016 135,221,175
		SELECT C.PKID CountryID, C.CountryName, C.CountryCodeA2, C.CountryCodeA3
		FROM Merchant M WITH (NOLOCK), CL_Acquirer A, CL_Country C
		WHERE MerchantID = @sz_MerchantID AND M.AcqBy=A.AcqID AND A.AcqCountryID=C.PKID
		UNION ALL
		SELECT PKID CountryID, CountryName, CountryCodeA2, CountryCodeA3
		FROM CL_Country M WITH (NOLOCK)
		WHERE PKID NOT IN (
			SELECT C.PKID
			FROM Merchant M WITH (NOLOCK), CL_Acquirer A, CL_Country C
			WHERE MerchantID = @sz_MerchantID AND M.AcqBy=A.AcqID AND A.AcqCountryID=C.PKID
		) AND M.RecStatus = 1
	END ELSE BEGIN
		SELECT PKID CountryID, CountryName, CountryCodeA2, CountryCodeA3
		FROM CL_Country M WITH (NOLOCK)
		WHERE RecStatus = 1
	END

	IF @@ROWCOUNT > 0
		RETURN 0
	ELSE
		RETURN -1
GO
