USE [PG]
GO
/****** Object:  StoredProcedure [dbo].[Token_GetMPE]    Script Date: 6/10/2020 12:07:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Token_GetMPE] --'om','test1'
(
	@sz_MerchantID AS VARCHAR(30),
	@sz_Token AS VARCHAR(100) --ConsumerUserID
)
AS
	DECLARE @RetCode Int
	DECLARE @RetDesc VarChar(50)
	DECLARE @LongAccessToken AS varchar(60)
	DECLARE @ReqToken as varchar(60)
	DECLARE @PairingToken as varchar(60)

	SELECT TOP 1 @LongAccessToken = LongAccessToken /*, @ReqToken = ReqToken, @PairingToken = PairingToken*/
	FROM Token_MPE WITH (NOLOCK)
	WHERE MerchantID = @sz_MerchantID AND ConsumerUserID=@sz_Token
	
	IF @@ROWCOUNT > 0
		--IF '' <> @LongAccessToken
		BEGIN 
			SET @RetCode = 1
			SET @RetDesc = 'Token record found'

			SELECT @RetCode [RetCode], @RetDesc [RetDesc], @LongAccessToken [LongAccessToken]/*, @ReqToken [ReqToken], @PairingToken [PairingReqToken]*/

			RETURN 0
		END
	ELSE
		BEGIN
			SET @RetCode = 0
			SET @RetDesc = 'Token record not found'

			SELECT @RetCode [RetCode], @RetDesc [RetDesc], '' [LongAccessToken]/*, '' [ReqToken], '' [PairingReqToken]*/

			RETURN -1
		END
GO
