USE [PG]
GO
/****** Object:  StoredProcedure [dbo].[fds_GetCL_Window]    Script Date: 6/10/2020 12:07:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[fds_GetCL_Window]
	@i_WindowID int
AS
	IF (@i_WindowID > 0)
		SELECT WindowID, [Name] FROM [dbo].[FDS_CL_Window]
		WHERE WindowID = @i_WindowID
	ELSE
		SELECT WindowID, [Name] FROM [dbo].[FDS_CL_Window]
		ORDER BY [Name]
GO
