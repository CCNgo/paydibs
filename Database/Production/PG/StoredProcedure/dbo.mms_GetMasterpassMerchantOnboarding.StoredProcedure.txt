USE [PG]
GO
/****** Object:  StoredProcedure [dbo].[mms_GetMasterpassMerchantOnboarding]    Script Date: 6/10/2020 12:07:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[mms_GetMasterpassMerchantOnboarding]--'GHL,X30,MMS'
( 
	@sz_MerchantID AS VARCHAR(MAX)
)
AS

BEGIN

DECLARE @sz_MerchantIDValue VARCHAR(MAX)

SET @sz_MerchantIDValue = REPLACE(@sz_MerchantID,'[ALL]','GHL')

SELECT DISTINCT A.[MerchantID]
      ,A.[ReqFilePath]
      ,A.[ResFilePath]
      ,A.[ResIsProcessed]
      --,A.[DateCreated]
      --,A.[DateModified]
      ,A.[RecStatus] 
	  ,(SELECT [Description] FROM [IPG].[dbo].[CL_MCCCode] WHERE MCCCode = B.MCCCode) MCCCodeDesc
	  ,B.[Desc] 'TradingName',C.DomainDesc 'RegistrationName'
	  ,B.WebSiteURL,B.MainContactNo,B.MainContactEmail
	  ,D.City,D.Addr1,D.Addr2,D.Addr3,D.PostCode
	  ,E.[State]
	  ,F.CountryName,F.CountryCodeA2,F.PhoneCountryCode
	  ,G.VISA,G.MasterCard,G.AMEX,
	  (SELECT DISTINCT PayeeCode FROM [IPG].[dbo].[Terminals] WITH (NOLOCK) WHERE MerchantID = A.[MerchantID] AND HostID = 21) CheckoutID
FROM [IPG].[dbo].[Mpass_MerchantOnboarding] A
LEFT JOIN [IPG].[dbo].[Merchant] B ON B.MerchantID = A.MerchantID
LEFT JOIN [IPGAdmin].[dbo].[CMS_Domain] C ON ISNULL(NULLIF(C.DomainShortName,'[ALL]'),'GHL') = A.MerchantID
LEFT JOIN [IPG].[dbo].[Address] D ON D.PKID = B.AddrID
LEFT JOIN [IPG].[dbo].[CL_State] E ON E.PKID = D.StateID
LEFT JOIN [IPG].[dbo].[CL_Country] F ON F.PKID = E.CountryID
INNER JOIN [IPG].[dbo].[CardTypeProfile] G ON G.ProfileID = B.CardTypeProfileID
WHERE A.MerchantID IN(SELECT item FROM FN_SPLIT(@sz_MerchantIDValue,','))

ORDER BY A.MerchantID ASC

END
GO
