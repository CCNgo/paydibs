USE [PG]
GO
/****** Object:  StoredProcedure [dbo].[spGetMerchantIDbyTxnID]    Script Date: 6/10/2020 12:07:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetMerchantIDbyTxnID]
(
	@sz_TxnID AS VARCHAR(30)
)
AS

DECLARE @sz_Header VARCHAR(100),	--Modified by OoiMei, 1 Sept 2015. 20 to 100.
		@sz_Criteria VARCHAR (500)

	SET @sz_Header = 'SELECT MerchantID FROM TB_PayReq WITH (NOLOCK) '
	
	SET @sz_Criteria = 'WHERE'
	IF (@sz_TxnID <> '')
	BEGIN
		SET @sz_Criteria = @sz_Criteria + ' GatewayTxnID = ''' + @sz_TxnID + ''''
	END

	EXEC (@sz_Header + @sz_Criteria)

	IF @@ROWCOUNT > 0
		RETURN 0
	ELSE
		RETURN -1
GO
