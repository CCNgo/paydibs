USE [PG]
GO
/****** Object:  StoredProcedure [dbo].[mms_UpdateTerminalCredential]    Script Date: 6/10/2020 12:07:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[mms_UpdateTerminalCredential]
(
	@sz_TerminalID AS VARCHAR(10),
	@sz_HostID AS VARCHAR(10),
	@sz_MerchantID AS VARCHAR(30),
	@sz_CurrencyCode AS VARCHAR(3)=NULL,
	@sz_Password AS VARCHAR(300)=NULL,
	@sz_ReturnKey AS VARCHAR(300)=NULL,
	@sz_SecretKey AS VARCHAR(300)=NULL,
	@sz_SendKeyPath AS VARCHAR(100)=NULL,
	@sz_ReturnKeyPath AS VARCHAR(100)=NULL,
	@sz_InitVector AS VARCHAR(16)=NULL,
	@sz_PayeeCode AS VARCHAR(50)=NULL,
	@sz_MID AS VARCHAR(20)=NULL,
	@sz_TID AS VARCHAR(20)=NULL,
	@sz_AcquirerID AS VARCHAR(100)=NULL,
	@sz_AirlineCode AS VARCHAR(400)=NULL
)
AS

	DECLARE @iRetVal int
	
	set @iRetVal = -1
	
	UPDATE Terminals SET
      --[MerchantID]=ISNULL(@sz_MerchantID, [MerchantID])
      [Password]=ISNULL(@sz_Password,[Password])
      ,[ReturnKey]=ISNULL(@sz_ReturnKey,[ReturnKey])
      ,[SecretKey]=ISNULL(@sz_SecretKey,[SecretKey])
      ,[SendKeyPath]=ISNULL(@sz_SendKeyPath,[SendKeyPath])
      ,[ReturnKeyPath]=ISNULL(@sz_ReturnKeyPath,[ReturnKeyPath])
      ,[InitVector]=ISNULL(@sz_InitVector,[InitVector])
      ,[PayeeCode]=ISNULL(@sz_PayeeCode,[PayeeCode])
      ,[MID]=ISNULL(@sz_MID,[MID])
      ,[TID]=ISNULL(@sz_TID,[TID])
      ,[AcquirerID]=ISNULL(@sz_AcquirerID,[AcquirerID])
      ,[CurrencyCode]=ISNULL(@sz_CurrencyCode,[CurrencyCode])
	  ,[AirlineCode]=ISNULL(@sz_AirlineCode,[AirlineCode])
      --,[GroupID]
      --,[Priority]
      --,[DateActivated]
      --,[DateDeactivated]
      --,[RecStatus]
    WHERE [TerminalID]=@sz_TerminalID 
    AND [HostID] = @sz_HostID
    AND [MerchantID] = @sz_MerchantID
    AND [CurrencyCode] = @sz_CurrencyCode
    
    
	
	IF @@ROWCOUNT <> 0 
	BEGIN
		set @iRetVal = 0
		--RETURN -2
	END
	ELSE
		set @iRetVal = -1
	
SELECT @iRetVal RetVal

GO
