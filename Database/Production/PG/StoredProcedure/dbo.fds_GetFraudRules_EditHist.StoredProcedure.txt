USE [PG]
GO
/****** Object:  StoredProcedure [dbo].[fds_GetFraudRules_EditHist]    Script Date: 6/10/2020 12:07:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[fds_GetFraudRules_EditHist]
	@i_All int,
	@sz_MerchantID varchar(30)
AS

	IF (@i_All = 0)
		SELECT	TOP 10 HistID, PKID, (SELECT Name FROM [dbo].[FDS_CL_Window] WHERE WindowID = F.Window_Old) Window_Old,
				(SELECT Name FROM [dbo].[FDS_CL_Window] WHERE WindowID = F.Window_New) Window_New, Limit_Old, Limit_New, Amount_Old, Amount_New,
				CONVERT (varchar, DateCreated, 20) DateCreated, CreatedBy
		FROM [dbo].[FDS_FraudRules_EditHist] F
		WHERE MerchantID = @sz_MerchantID
		ORDER BY HistID DESC
	ELSE
		SELECT	TOP 1000 HistID, PKID, (SELECT Name FROM [dbo].[FDS_CL_Window] WHERE WindowID = F.Window_Old) Window_Old,
				(SELECT Name FROM [dbo].[FDS_CL_Window] WHERE WindowID = F.Window_New) Window_New, Limit_Old, Limit_New, Amount_Old, Amount_New,
				CONVERT (varchar, DateCreated, 20) DateCreated, CreatedBy
		FROM [dbo].[FDS_FraudRules_EditHist] F
		WHERE MerchantID = @sz_MerchantID
		ORDER BY HistID DESC
GO
