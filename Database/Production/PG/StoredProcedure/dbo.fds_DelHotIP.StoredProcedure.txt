USE [PG]
GO
/****** Object:  StoredProcedure [dbo].[fds_DelHotIP]    Script Date: 6/10/2020 12:07:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[fds_DelHotIP]
	@i_PKID int,
	@sz_DateModified varchar(23),
	@sz_ModifiedBy nvarchar(50),
	@sz_MerchantID varchar(30)
AS

	DECLARE @szEditLockBy nvarchar(50),
		@iRetVal int
	
	--Added by OoiMei dated 3 Sept 2013. To check the PKID belong to the correct DomainShortName=ServiceID
	IF NOT EXISTS(SELECT PKID FROM [dbo].[FDS_HotIP] WHERE PKID = @i_PKID AND MerchantID = @sz_MerchantID)
	BEGIN
		SET @iRetVal = 40002	--No Such Record {UA_CL_ErrMsg}
		GOTO CleanUp
	END

	SELECT @szEditLockBy = EditLockBy FROM [dbo].[FDS_HotIP] WHERE PKID = @i_PKID
	IF (@@ROWCOUNT = 0)
	BEGIN
		SET @iRetVal = 40002
		GOTO CleanUp
	END
	
	IF (@szEditLockBy <> @sz_ModifiedBy OR @szEditLockBy IS NULL) 
	BEGIN
		SET @iRetVal = 40007
		GOTO CleanUp
	END 
	 
	DELETE	[dbo].[FDS_HotIP] WHERE PKID = @i_PKID
	 
	IF (@@ROWCOUNT = 0)
		SET @iRetVal = 40006
	ELSE
		SET @iRetVal = 0
	
CleanUp:
	SELECT @iRetVal RetVal
GO
