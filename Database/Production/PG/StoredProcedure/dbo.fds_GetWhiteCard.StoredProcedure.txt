USE [PG]
GO
/****** Object:  StoredProcedure [dbo].[fds_GetWhiteCard]    Script Date: 6/10/2020 12:07:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[fds_GetWhiteCard]
	@i_PKID int,
	@sz_CardPAN varchar (50),
	@i_MaxRow int,
	@sz_MerchantID varchar(30) --Added by OoiMei 27 Aug 2013
AS

	IF (@sz_MerchantID <> '')	--Specific merchant
	BEGIN
		IF (@sz_CardPAN <> '')
			SELECT	PKID, CardPANMin, CardPANMax, MerchantID, StartDate, EndDate, CONVERT (varchar, DateCreated, 20) DateCreated, 
					CONVERT (varchar, DateModified, 20) DateModified, ModifiedBy, RecActive, Remarks
			FROM [dbo].[FDS_WhiteCard]
			WHERE @sz_CardPAN BETWEEN CardPANMin AND CardPANMax AND RecActive = 1 AND MerchantID = @sz_MerchantID

		ELSE IF (@i_PKID > 0)
			SELECT	PKID, CardPANMin, CardPANMax, MerchantID, StartDate, EndDate, CONVERT (varchar, DateCreated, 20) DateCreated, 
					CONVERT (varchar, DateModified, 20) DateModified, ModifiedBy, RecActive, Remarks
			FROM [dbo].[FDS_WhiteCard]
			WHERE PKID = @i_PKID AND RecActive = 1 AND MerchantID = @sz_MerchantID

		ELSE
		BEGIN
			SET ROWCOUNT @i_MaxRow
			SELECT	PKID, CardPANMin, CardPANMax, MerchantID, StartDate, EndDate, CONVERT (varchar, DateCreated, 20) DateCreated, 
					CONVERT (varchar, DateModified, 20) DateModified, ModifiedBy, RecActive, Remarks
			FROM [dbo].[FDS_WhiteCard]
			WHERE LEN (CardPANMin) > 6 AND LEN (CardPANMax) > 6 AND RecActive = 1 AND MerchantID = @sz_MerchantID
			ORDER BY PKID DESC
			SET ROWCOUNT 0
		END
	END
	ELSE
	BEGIN	--all
		IF (@sz_CardPAN <> '')
			SELECT	PKID, CardPANMin, CardPANMax, MerchantID, StartDate, EndDate, CONVERT (varchar, DateCreated, 20) DateCreated, 
					CONVERT (varchar, DateModified, 20) DateModified, ModifiedBy, RecActive, Remarks
			FROM [dbo].[FDS_WhiteCard]
			WHERE @sz_CardPAN BETWEEN CardPANMin AND CardPANMax AND RecActive = 1

		ELSE IF (@i_PKID > 0)
			SELECT	PKID, CardPANMin, CardPANMax, MerchantID, StartDate, EndDate, CONVERT (varchar, DateCreated, 20) DateCreated, 
					CONVERT (varchar, DateModified, 20) DateModified, ModifiedBy, RecActive, Remarks
			FROM [dbo].[FDS_WhiteCard]
			WHERE PKID = @i_PKID AND RecActive = 1

		ELSE
		BEGIN
			SET ROWCOUNT @i_MaxRow
			SELECT	PKID, CardPANMin, CardPANMax, MerchantID, StartDate, EndDate, CONVERT (varchar, DateCreated, 20) DateCreated, 
					CONVERT (varchar, DateModified, 20) DateModified, ModifiedBy, RecActive, Remarks
			FROM [dbo].[FDS_WhiteCard]
			WHERE LEN (CardPANMin) > 6 AND LEN (CardPANMax) > 6 AND RecActive = 1
			ORDER BY PKID DESC
			SET ROWCOUNT 0
		END
	END
GO
