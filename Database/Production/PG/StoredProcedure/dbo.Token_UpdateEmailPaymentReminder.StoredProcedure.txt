USE [PG]
GO
/****** Object:  StoredProcedure [dbo].[Token_UpdateEmailPaymentReminder]    Script Date: 6/10/2020 12:07:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Token_UpdateEmailPaymentReminder]
(
	@i_PKID AS INT,
	@i_Reminder AS INT,
	@i_RecStatus AS INT,
	@dt_DateModified AS DATETIME
)
AS
	
	UPDATE Token_EmailPayment SET Reminder=@i_Reminder, RecStatus=@i_RecStatus, DateModified=@dt_DateModified WHERE PKID=@i_PKID
	IF @@ROWCOUNT <= 0 BEGIN
		RETURN -1
	END
	IF @@ERROR <> 0 BEGIN
		RETURN -1
	END
RETURN 0
GO
