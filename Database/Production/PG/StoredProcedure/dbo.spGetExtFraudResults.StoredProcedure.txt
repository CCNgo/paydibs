USE [PG]
GO
/****** Object:  StoredProcedure [dbo].[spGetExtFraudResults]    Script Date: 6/10/2020 12:07:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetExtFraudResults]--'','','','',20160101,20170614,0
(                                                
	@sz_DomainShortName varchar(15)='',
	@sz_MerchantTxnID varchar(30)='',
	@sz_GatewayTxnID varchar(40)='',
	@sz_CustEmail varchar(50)='',
	@sz_StartDate int,
	@sz_EndDate int,
	@i_FraudAction int=0
)
AS

BEGIN
	IF (@sz_MerchantTxnID = '')
	BEGIN
		SET @sz_MerchantTxnID = NULL
	END
	IF (@sz_GatewayTxnID = '')
	BEGIN
		SET @sz_GatewayTxnID = NULL
	END
	
	IF (@sz_DomainShortName = '')
	BEGIN
		SET @sz_DomainShortName = NULL
	END
	
	IF (@sz_CustEmail = '')
	BEGIN
		SET @sz_CustEmail = NULL
	END
	
	IF((@sz_GatewayTxnID <> '') OR (@sz_MerchantTxnID <> '') OR (@sz_CustEmail <> ''))
	BEGIN
		SET @sz_StartDate = NULL
		SET @sz_EndDate = NULL
	END
	
	IF(@i_FraudAction >0)
	BEGIN
		SELECT A.[PKID],A.[MerchantID],A.[MerchantTxnID],A.[GatewayTxnID],A.[ResponseCode],A.[ResponseMsg],A.[TxnDay]
		,A.[DateCreated],A.[DateModified],C.Remarks,C.ActionColor,A.[ActionID],D.TxnAmt,D.OrderDesc,E.CustIP
		FROM [PG].[dbo].[FDS_ExtFraudResults] A WITH (NOLOCK)
		--LEFT JOIN [PG].[dbo].[Merchant] B WITH (NOLOCK) ON A.MerchantID = B.MerchantID 
		LEFT JOIN [PG].[dbo].[CL_FraudScoreAction] C WITH (NOLOCK) ON A.ActionID = C.ActionID  
		LEFT JOIN [PG].[dbo].[TB_PayReq_Detail] D WITH (NOLOCK) ON D.MerchantTxnID = A.MerchantTxnID AND D.MerchantID = A.MerchantID
		LEFT JOIN [PG].[dbo].[FDS_Detail] E WITH (NOLOCK) ON E.PayReqDetailID = D.PKID 
		--LEFT JOIN [PG].[dbo].[FDS_TxnRef] F WITH (NOLOCK) ON F.PKID = E.PayReqDetailID 
		WHERE A.TxnDay BETWEEN ISNULL(@sz_StartDate,A.TxnDay) AND ISNULL(@sz_EndDate,A.TxnDay)
		AND A.MerchantTxnID = ISNULL(@sz_MerchantTxnID,A.MerchantTxnID)
		AND A.GatewayTxnID = ISNULL(@sz_GatewayTxnID,A.GatewayTxnID)
		AND E.CustEmail = ISNULL(@sz_CustEmail,E.CustEmail)
		AND A.MerchantID = ISNULL(@sz_DomainShortName,A.MerchantID)
		AND A.ActionID = ISNULL(@i_FraudAction,A.ActionID)
		ORDER BY DateCreated DESC
	END
	ELSE
	BEGIN
		SELECT A.[PKID],A.[MerchantID],A.[MerchantTxnID],A.[GatewayTxnID],A.[ResponseCode],A.[ResponseMsg],A.[TxnDay]
		,A.[DateCreated],A.[DateModified],C.Remarks,C.ActionColor,A.[ActionID],D.TxnAmt,D.OrderDesc,E.CustIP
		FROM [PG].[dbo].[FDS_ExtFraudResults] A WITH (NOLOCK)
		--LEFT JOIN [PG].[dbo].[Merchant] B WITH (NOLOCK) ON A.MerchantID = B.MerchantID 
		LEFT JOIN [PG].[dbo].[CL_FraudScoreAction] C WITH (NOLOCK) ON A.ActionID = C.ActionID  
		LEFT JOIN [PG].[dbo].[TB_PayReq_Detail] D WITH (NOLOCK) ON D.MerchantTxnID = A.MerchantTxnID  AND D.MerchantID = A.MerchantID
		LEFT JOIN [PG].[dbo].[FDS_Detail] E WITH (NOLOCK) ON E.PayReqDetailID = D.PKID 
		--LEFT JOIN [PG].[dbo].[FDS_TxnRef] F WITH (NOLOCK) ON F.PKID = E.PayReqDetailID 
		WHERE A.TxnDay BETWEEN ISNULL(@sz_StartDate,A.TxnDay) AND ISNULL(@sz_EndDate,A.TxnDay)
		AND A.MerchantTxnID = ISNULL(@sz_MerchantTxnID,A.MerchantTxnID)
		AND A.GatewayTxnID = ISNULL(@sz_GatewayTxnID,A.GatewayTxnID)
		AND E.CustEmail = ISNULL(@sz_CustEmail,E.CustEmail)
		AND A.MerchantID = ISNULL(@sz_DomainShortName,A.MerchantID)
		ORDER BY DateCreated DESC
	END
END
GO
