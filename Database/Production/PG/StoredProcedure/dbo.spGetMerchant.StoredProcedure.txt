USE [PG]
GO
/****** Object:  StoredProcedure [dbo].[spGetMerchant]    Script Date: 6/10/2020 12:07:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetMerchant]
(
	@sz_MerchantID AS VARCHAR(30),
	@sz_PromoCode AS VARCHAR(10)=null
)
AS
	SELECT MerchantID, [Desc] MerchantName, MerchantPassword, MCCCode, PaymentTemplate, ErrorTemplate, AllowPayment, AllowQuery, AllowReversal, 
			AllowFDS, AllowMaxMind, AllowOB, AllowOTC, AllowWallet, AllowOCP, AllowCallBack, AllowFX, FXRate, RouteByParam1, CollectBillAddr, CollectShipAddr, 
			FraudByAmt, PerTxnAmtLimit, DailyTotAmtLimit, MthlyTotAmtLimit, HitLimitAct, ThreeDAccept, SelfMPI, PymtPageTimeout_S,
			CardTypeProfileID, HCProfileID, ISNULL(OTCExpiryHour, 0) OTCExpiryHour, M.AcqBy AcqByID, 
			NeedAddOSPymt, TxnExpired_S, ISNULL(ContactNo,'') ContactNo, ISNULL(EmailAddr,'') EmailAddr, ISNULL(NotifyEmailAddr,'') NotifyEmailAddr, ISNULL(ValidDomain, '') ValidDomain, ISNULL(WebSiteURL,'') WebSiteURL,
			MainContactName,MainContactNo,MainContactMobile,MainContactEmail,
			SvcTypeID, ISNULL((SELECT SvcType FROM CL_SvcType ST WHERE ST.SvcTypeID = M.SvcTypeID AND ST.RecStatus = 1),'') SvcType,
			AutoReversal, RespMethod, Receipt, PymtNotificationSMS, PymtNotificationEmail, SettleCurrency, SettlePeriod, ShowMerchantAddr, ShowMerchantLogo, ISNULL(ExtraCSS, '') ExtraCSS,
			ISNULL(A.Addr1,'') Addr1 , CASE A.Addr2 WHEN '' THEN NULL ELSE A.Addr2 END/*ISNULL(A.Addr2, '')*/ Addr2, CASE A.Addr3 WHEN '' THEN NULL ELSE A.Addr3  END/*ISNULL(A.Addr3, '')*/ Addr3, CASE A.PostCode WHEN '' THEN NULL ELSE A.PostCode  END/*ISNULL(A.PostCode, '')*/ PostCode, /*Modified by OoiMei 11 Aug 2017*/
			ISNULL(A.City,'') City, CASE S.State WHEN '' THEN NULL ELSE S.State END/*ISNULL(S.State, '')*/ [State], ISNULL((SELECT C.CountryName FROM CL_Country C WHERE M.CountryID = C.PKID),'') Country,ISNULL((SELECT C.CountryCodeA2 FROM CL_Country C WHERE M.CountryID = C.PKID),'') Country2,
			ISNULL((SELECT MA.AcqName FROM CL_Acquirer MA WHERE MA.AcqID = M.AcqBy), '') AcqBy,
			ISNULL((SELECT C.CountryCodeA2 FROM CL_Acquirer MA, CL_Country C WHERE MA.AcqID = M.AcqBy AND MA.AcqCountryID = C.PKID),'') AcqCountryCode,
			--ISNULL((SELECT MA.FDSCustomerID FROM CL_Acquirer MA WHERE MA.AcqID = M.AcqBy), '') FDSCustomerID,
			--ISNULL((SELECT MA.FDSAuthCode FROM CL_Acquirer MA WHERE MA.AcqID = M.AcqBy), '') FDSAuthCode,
			ISNULL((SELECT CASE WHEN M.AllowExtFDS=2 THEN MA.FDSCustomerIDSim ELSE MA.FDSCustomerID END FROM CL_Acquirer MA WHERE MA.AcqID = M.AcqBy), '') FDSCustomerID,
			ISNULL((SELECT CASE WHEN M.AllowExtFDS=2 THEN MA.FDSAuthCodeSim ELSE MA.FDSAuthCode END FROM CL_Acquirer MA WHERE MA.AcqID = M.AcqBy), '') FDSAuthCode,
			ISNULL((SELECT MA.ChargeBackTeamEmail FROM CL_Acquirer MA WHERE MA.AcqID = M.AcqBy), '') ChargeBackTeamEmail,
			ISNULL(CT.VISA,0) VISA, ISNULL(CT.MasterCard,0) MasterCard, ISNULL(CT.AMEX,0) AMEX, ISNULL(CT.Diners,0) Diners, ISNULL(CT.JCB,0) JCB, ISNULL(CT.CUP,0) CUP, ISNULL(ReturnCardData, 0) ReturnCardData,
			--ISNULL((SELECT TOP 1 1 FROM [dbo].[Promotion] WITH (NOLOCK) WHERE MerchantID = @sz_MerchantID AND Convert(date, getdate()) BETWEEN FromDate AND ToDate AND PromoCode=CASE WHEN @sz_PromoCode is not null THEN @sz_PromoCode ELSE PromoCode END AND InputCode=CASE WHEN @sz_PromoCode is not null THEN 1 ELSE 0 END),-1) PROMO,
			ISNULL((SELECT TOP 1 1 FROM [dbo].[Promotion] WITH (NOLOCK) WHERE MerchantID = @sz_MerchantID AND Convert(date, getdate()) BETWEEN FromDate AND ToDate AND PromoCode=ISNULL(@sz_PromoCode, PromoCode) AND InputCode=CASE WHEN @sz_PromoCode is not null THEN 1 ELSE 0 END),-1) PROMO,
			AllowInstallment, AllowExtFDS, M.CountryID, Protocol /*nick modified 13/04/2016*/, ISNULL(CT.MasterPass,0) MasterPass /*added by OoiMei 20 Sept 2016*/, ISNULL(AllowMPI, 0) AllowMPI /*added 21 Dec 2016*/, ISNULL(CT.VisaCheckout,0) VisaCheckout /*added by OoiMei 17 Mar 2017*/
			, ISNULL(CT.SamsungPay,0) SamsungPay /*added 15 Oct 2017*/
			, M.CVVRequire  /* added TIK FX*/
	FROM	Merchant M WITH (NOLOCK)
			LEFT OUTER JOIN [Address] A WITH (NOLOCK) ON A.PKID = M.AddrID
			LEFT OUTER JOIN CL_State S WITH (NOLOCK) ON S.PKID = A.StateID
			LEFT OUTER JOIN CardTypeProfile CT WITH (NOLOCK) ON CT.ProfileID = M.CardTypeProfileID
	WHERE M.MerchantID = @sz_MerchantID 
	AND GetDate() >= M.DateActivated AND GetDate() <= M.DateDeactivated    
	AND M.RecStatus = 1

	IF @@ROWCOUNT > 0
		RETURN 0
	ELSE
		RETURN -1
GO
