USE [PG]
GO
/****** Object:  StoredProcedure [dbo].[uspBatchUpdateStatus]    Script Date: 6/10/2020 12:07:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Description:	Update Batch Status
-- =============================================
CREATE PROCEDURE [dbo].[uspBatchUpdateStatus]
	@BatchPKID int,
	@BatchNumber varchar(30),
	@BatchStatus int=-1,
	@FileReceived int=-1,
	@ProcessedOK int=-1,
	@ProcessedFail int=-1,
	@ProcessedAmount decimal(18,2)=-1,
	@HostSentOK int=-1,
	@HostSentFail int=-1,
	@HostSentAmount  decimal(18,2)=-1,
	@HostResponse int=-1,
	@HostResponseAmount  decimal(18,2)=-1,
	@HostStatusPKID int=-1,
	@Action int=0
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @CurrentBatchStatus int =-100,
			@iReturn int =-1,
			@ReturnDesc nvarchar(80) =''
	IF @Action=0
		BEGIN			
			SELECT @CurrentBatchStatus=[BatchStatus] FROM [dbo].[BatchProcessStatus] WHERE [BatchPKID]=@BatchPKID AND [BatchNumber]=@BatchNumber
			IF @CurrentBatchStatus=-100
				BEGIN
					INSERT INTO [dbo].[BatchProcessStatus]
						   ([BatchPKID]
						   ,[BatchNumber]
						   ,[BatchStatus]
						   ,[FileReceived]
						   ,[ProcessedOK]
						   ,[ProcessedFail]
						   ,[ProcessedAmount]
						   ,[HostSentOK]
						   ,[HostSentFail]
						   ,[HostSentAmount]
						   ,[HostResponse]
						   ,[HostResponseAmount]
						   ,[HostStatusPKID]
						   ,[CreatedDate]
						   ,[LastUpdated])
					 VALUES
						   (@BatchPKID
						   ,@BatchNumber
						   ,@BatchStatus
						   ,@FileReceived
						   ,@ProcessedOK
						   ,@ProcessedFail
						   ,@ProcessedAmount
						   ,@HostSentOK
						   ,@HostSentFail
						   ,@HostSentAmount
						   ,@HostResponse
						   ,@HostResponseAmount
						   ,null
						   ,Getdate()
						   ,Getdate())
					IF @@ERROR = 0
						BEGIN
							SET @iReturn=0
						END
				END
			ELSE IF @BatchStatus>@CurrentBatchStatus
				BEGIN
					UPDATE [dbo].[BatchProcessStatus]
					   SET [BatchStatus] = @BatchStatus
			   			  ,[ProcessedOK]=CASE WHEN @ProcessedOK<>-1 THEN @ProcessedOK ELSE [ProcessedOK] END
						  ,[ProcessedFail]=CASE WHEN @ProcessedFail<>-1 THEN @ProcessedFail ELSE [ProcessedFail] END
						  ,[ProcessedAmount]=CASE WHEN @ProcessedAmount<>-1 THEN @ProcessedAmount ELSE [ProcessedAmount] END
						  ,[HostSentOK]=CASE WHEN @HostSentOK<>-1 THEN @HostSentOK ELSE [HostSentOK] END
						  ,[HostSentFail]=CASE WHEN @HostSentFail<>-1 THEN @HostSentFail ELSE [HostSentFail] END
						  ,[HostSentAmount]=CASE WHEN @HostSentAmount<>-1 THEN @HostSentAmount ELSE [HostSentAmount] END
						  ,[HostResponse]=CASE WHEN @HostResponse <>-1 THEN @HostResponse  ELSE [HostResponse] END
						  ,[HostResponseAmount]=CASE WHEN @HostResponseAmount<>-1 THEN @HostResponseAmount ELSE [HostResponseAmount] END
						  ,[LastUpdated] = GETDATE()
					 WHERE [BatchPKID]=@BatchPKID AND [BatchNumber]=@BatchNumber
					 IF @@ERROR = 0
						BEGIN
							SET @iReturn=0
						END
				END
			ELSE IF @CurrentBatchStatus=@BatchStatus
				BEGIN
					SET @iReturn=1	
					SET @ReturnDesc='Update status ['  + CONVERT(varchar,@BatchStatus) + '] and current status ['+ CONVERT(varchar,@CurrentBatchStatus) + '] is same'		
				END
			ELSE IF @CurrentBatchStatus>@BatchStatus
				BEGIN
					SET @iReturn=2	
					SET @ReturnDesc='Update status ['  + CONVERT(varchar,@BatchStatus) + '] and current status ['+ CONVERT(varchar,@CurrentBatchStatus) + '] greater then update status'		
				END
			SELECT ReturnCode=@iReturn,ReturnDesc=@ReturnDesc
		END
	ELSE IF @Action=1
		BEGIN
			SELECT @CurrentBatchStatus=[BatchStatus] FROM [dbo].[BatchProcessStatus] WHERE [BatchPKID]=@BatchPKID AND [BatchNumber]=@BatchNumber
			IF @BatchStatus>@CurrentBatchStatus OR @BatchStatus=-1
				BEGIN
					UPDATE [dbo].[BatchProcessStatus]
					   SET [BatchStatus] = @BatchStatus
						  ,[HostStatusPKID]=CASE WHEN @HostStatusPKID<>-1 THEN @HostStatusPKID ELSE [HostStatusPKID] END    
						  ,[LastUpdated] = getdate()		 
					WHERE [BatchPKID] = @BatchPKID AND [BatchNumber] = @BatchNumber
					IF @@ERROR = 0
						BEGIN
							SET @iReturn=0
						END
				END
			ELSE IF @CurrentBatchStatus=@BatchStatus
				BEGIN
					SET @iReturn=1	
					SET @ReturnDesc='Update status ['  + CONVERT(varchar,@BatchStatus) + '] and current status ['+ CONVERT(varchar,@CurrentBatchStatus) + '] is same'		
				END
			ELSE IF @CurrentBatchStatus>@BatchStatus
				BEGIN
					SET @iReturn=2	
					SET @ReturnDesc='Update status ['  + CONVERT(varchar,@BatchStatus) + '] and current status ['+ CONVERT(varchar,@CurrentBatchStatus) + '] greater then update status'		
				END
			SELECT ReturnCode=@iReturn,ReturnDesc=@ReturnDesc		
		END
	ELSE IF @Action=2--Force Flush
		BEGIN
			SELECT @CurrentBatchStatus=[BatchStatus] FROM [dbo].[BatchProcessStatus] WHERE [BatchPKID]=@BatchPKID AND [BatchNumber]=@BatchNumber
				IF @CurrentBatchStatus<>-1				
					BEGIN
						UPDATE [dbo].[BatchProcessStatus]
						   SET [BatchStatus] = @BatchStatus
			   				  ,[ProcessedOK]=CASE WHEN @ProcessedOK<>-1 THEN @ProcessedOK ELSE [ProcessedOK] END
							  ,[ProcessedFail]=CASE WHEN @ProcessedFail<>-1 THEN @ProcessedFail ELSE [ProcessedFail] END
							  ,[ProcessedAmount]=CASE WHEN @ProcessedAmount<>-1 THEN @ProcessedAmount ELSE [ProcessedAmount] END
							  ,[HostSentOK]=CASE WHEN @HostSentOK<>-1 THEN @HostSentOK ELSE [HostSentOK] END
							  ,[HostSentFail]=CASE WHEN @HostSentFail<>-1 THEN @HostSentFail ELSE [HostSentFail] END
							  ,[HostSentAmount]=CASE WHEN @HostSentAmount<>-1 THEN @HostSentAmount ELSE [HostSentAmount] END
							  ,[HostResponse]=CASE WHEN @HostResponse <>-1 THEN @HostResponse  ELSE [HostResponse] END
							  ,[HostResponseAmount]=CASE WHEN @HostResponseAmount<>-1 THEN @HostResponseAmount ELSE [HostResponseAmount] END
							  ,[LastUpdated] = GETDATE()
						 WHERE [BatchPKID]=@BatchPKID AND [BatchNumber]=@BatchNumber
						 IF @@ERROR = 0
							BEGIN
								SET @iReturn=0
							END
					END
			SELECT ReturnCode=@iReturn,ReturnDesc=@ReturnDesc
		END
	ELSE IF @Action=3--Check duplcate batch received
		BEGIN			
			IF NOT EXISTS(SELECT 1 FROM [dbo].[BatchProcessStatus] WHERE [BatchPKID]=@BatchPKID AND [BatchNumber]=@BatchNumber)
				BEGIN					
					SET @iReturn=0
				END
			ELSE 
				BEGIN
					SET @iReturn=1						
				END
			
			SELECT ReturnCode=@iReturn		
		END
END










GO
