USE [PG]
GO
/****** Object:  StoredProcedure [dbo].[fds_GetHotName]    Script Date: 6/10/2020 12:07:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[fds_GetHotName]
	@i_PKID int,
	@sz_Name varchar (100),
	@i_MaxRow int,
	@sz_MerchantID varchar(30)
AS
	
	IF (@sz_MerchantID <> '') --specific merchant
	BEGIN
		IF (@sz_Name <> '')
			SELECT	PKID, HotName, StartDate, EndDate, MerchantID, CONVERT (varchar, DateCreated, 20) DateCreated,
					CONVERT (varchar, DateModified, 20) DateModified, RecActive
			FROM [dbo].[FDS_HotName]
			WHERE UPPER (HotName) LIKE '%' + UPPER (@sz_Name) + '%' AND RecActive = 1 AND MerchantID = @sz_MerchantID

		ELSE IF (@i_PKID > 0)
					SELECT	PKID, HotName, StartDate, EndDate, MerchantID, CONVERT (varchar, DateCreated, 20) DateCreated,
					CONVERT (varchar, DateModified, 20) DateModified, RecActive 
			FROM [dbo].[FDS_HotName]
			WHERE PKID = @i_PKID AND RecActive = 1 AND MerchantID = @sz_MerchantID

		ELSE
		BEGIN
			SET ROWCOUNT @i_MaxRow
			SELECT	PKID, HotName, StartDate, EndDate, MerchantID, CONVERT (varchar, DateCreated, 20) DateCreated,
					CONVERT (varchar, DateModified, 20) DateModified, RecActive
			FROM [dbo].[FDS_HotName] 
			WHERE RecActive = 1 AND MerchantID = @sz_MerchantID
			ORDER BY PKID DESC
			SET ROWCOUNT 0
		END
	END
	ELSE
	BEGIN --all
		IF (@sz_Name <> '')
		SELECT	PKID, HotName, StartDate, EndDate, MerchantID, CONVERT (varchar, DateCreated, 20) DateCreated,
				CONVERT (varchar, DateModified, 20) DateModified, RecActive
		FROM [dbo].[FDS_HotName]
		WHERE UPPER (HotName) LIKE '%' + UPPER (@sz_Name) + '%' AND RecActive = 1

		ELSE IF (@i_PKID > 0)
					SELECT	PKID, HotName, StartDate, EndDate, MerchantID, CONVERT (varchar, DateCreated, 20) DateCreated,
					CONVERT (varchar, DateModified, 20) DateModified, RecActive
			FROM [dbo].[FDS_HotName]
			WHERE PKID = @i_PKID AND RecActive = 1 

		ELSE
		BEGIN
			SET ROWCOUNT @i_MaxRow
			SELECT	PKID, HotName, StartDate, EndDate, MerchantID, CONVERT (varchar, DateCreated, 20) DateCreated,
					CONVERT (varchar, DateModified, 20) DateModified, RecActive
			FROM [dbo].[FDS_HotName] 
			WHERE RecActive = 1 
			ORDER BY PKID DESC
			SET ROWCOUNT 0
		END
	END
GO
