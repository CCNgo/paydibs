USE [PG]
GO
/****** Object:  StoredProcedure [dbo].[spGetCancelBookingTxns]    Script Date: 6/10/2020 12:07:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetCancelBookingTxns]
(                                                
	@i_MaxRow As Int
)
AS
	DECLARE @iRowCount AS Int
	DECLARE @szDate AS VARCHAR(10)

	SET ROWCOUNT @i_MaxRow

	SET @szDate = CONVERT(VARCHAR(10), GETDATE()-1, 121)
	--SET @szDate = CONVERT(VARCHAR(10), GETDATE()-5, 121)
	--SET @szDate = CONVERT(VARCHAR(10), GETDATE(), 121)

	-- TxnStatus 5: Bank returned Transaction not processed/unknown status
	-- TxnStatus 4: Timeout Query reply/Unable to connect bank
	-- TxnStatus 1: Transaction Failed due to rejection by bank AND not due to other reasons, e.g. payment approved but booking unconfirmed/internal errors
	SELECT PKID, MerchantTxnID, GatewayTxnID, OrderNumber, TxnState, TxnStatus, BankRespCode, RespMesg, OSRet, DateCreated, DateCompleted
	FROM TB_PayRes WITH (NOLOCK)
	WHERE (DateCreated BETWEEN @szDate + ' 00:00:00' AND @szDate + ' 23:59:59.997') AND TxnStatus NOT IN (0,3) AND OSRet NOT IN (1,2,3,-9) AND TxnState NOT IN (50,51)
	--WHERE (DateCreated BETWEEN @szDate + ' 00:00:00' AND @szDate + ' 17:40:00') AND TxnStatus NOT IN (0,3) AND OSRet NOT IN (1,2,3,-9) AND TxnState NOT IN (50,51)

	SET @iRowCount = @@ROWCOUNT
	IF @iRowCount > 0
	BEGIN
		RETURN @iRowCount
	END
	ELSE
	BEGIN
		RETURN 0
	END

	SET ROWCOUNT 0
GO
