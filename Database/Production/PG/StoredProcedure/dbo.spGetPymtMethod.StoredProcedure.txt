USE [PG]
GO
/****** Object:  StoredProcedure [dbo].[spGetPymtMethod]    Script Date: 6/10/2020 12:07:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetPymtMethod]
( @PymtMethodCode CHAR(3) )
AS
BEGIN
	SET NOCOUNT ON;
	SELECT PymtMethodDesc FROM CL_PymtMethod
	WHERE PymtMethodCode = @PymtMethodCode AND RecStatus = 1
END
GO
