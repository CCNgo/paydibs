USE [PG]
GO
/****** Object:  StoredProcedure [dbo].[spGetCurrency]    Script Date: 6/10/2020 12:07:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetCurrency] 
(
	@sz_CurrencyCode AS VARCHAR(5), 
	@sz_Currency AS VARCHAR(5) OUTPUT
)
AS

IF (isnumeric(@sz_CurrencyCode) = 1) 
BEGIN
	SELECT @sz_Currency = CurrencyCode
	FROM CL_CurrencyCode WITH (NOLOCK) 
	WHERE Currency = @sz_CurrencyCode
END
ELSE
BEGIN
	SELECT @sz_Currency = Currency
	FROM CL_CurrencyCode WITH (NOLOCK) 
	WHERE UPPER(CurrencyCode) = UPPER(@sz_CurrencyCode)
END

if @@ROWCOUNT > 0
	RETURN 0
else
	RETURN 1
GO
