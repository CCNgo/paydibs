USE [PG]
GO
/****** Object:  StoredProcedure [dbo].[spGetCLCardTypeProfileCardColumnsOnly]    Script Date: 6/10/2020 12:07:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetCLCardTypeProfileCardColumnsOnly]
AS
	SELECT name 'CardName' FROM SYS.COLUMNS 
	WHERE [object_id]=OBJECT_ID('dbo.CardTypeProfile')
	AND column_id>1 --since 1 - ProfileID
GO
